using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace CrplControlLibrary
{
    public partial class SLDatePicker : DateTimePicker, IBaseControl
    {
        private string m_strDataFieldMapping = string.Empty;
        private bool pbIsRequired;
        private bool pbIsDefaultOnFocus;
        private bool _isNull;
        private string _nullValue;
        private DateTimePickerFormat _format = DateTimePickerFormat.Long;
        private string _customFormat;
        private string _formatAsString;
        private bool IsEnabled = true;
        private bool hasChanges = false;

        public SLDatePicker(IContainer container)
            : base()
        {
            container.Add(this);

            InitializeComponent();

            base.Format = DateTimePickerFormat.Custom;
            NullValue = " ";
            this.Format = DateTimePickerFormat.Long;



        }

        #region IBaseControl Members

        public bool HasChanges
        {
            get { return hasChanges && Enabled && CustomEnabled; }
            set { hasChanges = value; }
        }

        public string DataFieldMapping
        {
            get
            {
                return m_strDataFieldMapping;
            }
            set
            {
                m_strDataFieldMapping = value;
            }
        }

        [DefaultValue(false)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets required field validation of control.")]
        public bool IsRequired
        {
            get
            { return pbIsRequired; }
            set
            {
                pbIsRequired = value;
            }
        }

        [DefaultValue(false)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets Default Date in Picker on Focus.")]
        public bool IsDefaultOnFocus
        {
            get
            { return pbIsDefaultOnFocus; }
            set
            {
                pbIsDefaultOnFocus = value;
            }
        }

        [Browsable(true)]
        [Description("Date Time Picker Enable/Disable")]
        [Category("Custom Behaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }

        public new Object Value
        {
            get
            {
                if (_isNull)
                    return null;
                else
                    if (base.CustomFormat != string.Empty)
                    {
                        if (base.CustomFormat.Contains("hh") || base.CustomFormat.Contains("HH"))
                        {
                            return base.Value;
                        }
                        else
                        {
                            return base.Value.Date;
                        }
                    }
                return base.Value;
            }
            set
            {
                if (value == null || value == DBNull.Value || value.ToString().Equals("") || value.ToString().Equals(" ") || value.Equals(DateTime.MinValue))
                {
                    SetToNullValue();
                }
                else
                {
                    SetToDateTimeValue();
                    base.Value = (DateTime)value;
                }
            }
        }
        #endregion

        public String NullValue
        {
            get { return _nullValue; }
            set { _nullValue = value; }
        }

        public new String CustomFormat
        {
            get { return _customFormat; }
            set { _customFormat = value; }
        }

        public new DateTimePickerFormat Format
        {
            get { return _format; }
            set
            {
                _format = value;
                SetFormat();
                OnFormatChanged(EventArgs.Empty);
            }
        }
        private string FormatAsString
        {
            get { return _formatAsString; }
            set
            {
                _formatAsString = value;
                base.CustomFormat = value;
            }
        }

        #region --EventHandler Segment--

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            if (this.IsRequired && this.Value == null)
            {
                MessageBox.Show("You Can't leave the field empty", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Focus();
                return;
            }
        }
        Boolean numberKeyPressed = false;
        Boolean selectionComplete = false;
        private const int WM_REFLECT = 0x2000;
        private const int WM_NOTIFY = 0x004E;
        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x0100;
        //&H100



        protected override void OnKeyDown(KeyEventArgs e)
        {
           if(this.pbIsDefaultOnFocus)
           {
                if (this.Value == null)
            {
                this.Value = DateTime.Now;
            }
          }
            numberKeyPressed = (e.Modifiers == Keys.None && ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode != Keys.Back && e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9)));
            selectionComplete = false;
            base.OnKeyDown(e);
        }


        private struct NMHDR
        {
            public IntPtr hWnd;
            public IntPtr id;
            public int code;

        }
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_REFLECT + WM_NOTIFY)
            {
                NMHDR hdr = (NMHDR)m.GetLParam(typeof(NMHDR));
                if (hdr.code == -759) //date chosen (by keyboard)
                    selectionComplete = true;
            }
            base.WndProc(ref m);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                this.Value = NullValue;
                OnValueChanged(EventArgs.Empty);
            }

            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter) && this.Value == null && this.IsDefaultOnFocus == true)
            {
                this.Value = DateTime.Now;
                Message m = new Message();
                m.HWnd = this.Handle;
                m.LParam = IntPtr.Zero;
                m.WParam = new IntPtr((int)Keys.Right); //right arrow key
                m.Msg = WM_KEYDOWN;
                WndProc(ref m);
            }
            base.OnKeyUp(e);

            if (numberKeyPressed && selectionComplete &&
                (e.Modifiers == Keys.None &&
                ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9)
                || (e.KeyCode != Keys.Back && e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))))
            {
                Message m = new Message();
                m.HWnd = this.Handle;
                m.LParam = IntPtr.Zero;
                m.WParam = new IntPtr((int)Keys.Right); //right arrow key
                m.Msg = WM_KEYDOWN;
                WndProc(ref m);
                //m.Msg = WM_KEYUP;
                //WndProc(ref m);
                numberKeyPressed = false;
                selectionComplete = false;
            }
        }
        protected override void OnCloseUp(EventArgs e)
        {
            if (Control.MouseButtons == MouseButtons.None && _isNull)
            {
                SetToDateTimeValue();
                _isNull = false;
            }
            base.OnCloseUp(e);
        }

        protected override void OnValueChanged(EventArgs eventargs)
        {
            if (this.CustomFormat != null && !this.CustomFormat.Contains("dd") && this.Value != null && ((DateTime)this.Value).Day != 1)
            {
                int dayDiff = (((DateTime)this.Value).Day * (-1)) + 1;
                ((DateTime)this.Value).AddDays(dayDiff);
            }
            HasChanges = true;
            base.OnValueChanged(eventargs);
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            Form frm = this.GetParentForm(this);
            if (frm != null)
            {
                ToolStrip tls = (ToolStrip)frm.Controls["tlbMain"];
                if (tls != null && tls.Items["tbtClose"] != null && tls.Items["tbtClose"].Pressed)
                {
                    frm.AutoValidate = AutoValidate.Disable;
                    e.Cancel = true;
                    return;
                }
            }
            base.OnValidating(e);
        }

        #endregion

        #region Custom Functions
        private Form GetParentForm(Control parent)
        {
            Form form = parent as Form;
            if (form != null)
            {
                return form;
            }
            if (parent != null)
            {
                // Walk up the control hierarchy
                return GetParentForm(parent.Parent);
            }
            return null; // Control is not on a Form
        }

        private void SetFormat()
        {
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            DateTimeFormatInfo dtf = ci.DateTimeFormat;
            switch (_format)
            {
                case DateTimePickerFormat.Long:
                    FormatAsString = dtf.LongDatePattern;
                    break;
                case DateTimePickerFormat.Short:
                    FormatAsString = dtf.ShortDatePattern;
                    break;
                case DateTimePickerFormat.Time:
                    FormatAsString = dtf.ShortTimePattern;
                    break;
                case DateTimePickerFormat.Custom:
                    FormatAsString = this.CustomFormat;
                    break;
            }
        }
        private void SetToDateTimeValue()
        {
            if (_isNull)
            {
                SetFormat();
                _isNull = false;
                base.OnValueChanged(new EventArgs());
            }
        }
        private void SetToNullValue()
        {
            _isNull = true;
            base.CustomFormat = (_nullValue == null || _nullValue == String.Empty)
                                ? " " : "'" + NullValue + "'";
        }

        #endregion
    }
}