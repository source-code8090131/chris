using System;
using System.Drawing;
using System.Drawing.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;

namespace CrplControlLibrary
{
    public enum TextType
    {
        String = 0,
        StringAndDigit = 1,
        Integer = 2,
        Double = 3,
        PhoneNumber = 4,
        DateTime = 5,
        Email = 6,
        URL = 7,
        Address = 8,
        DigitAndHyphen = 9,
        AllCharacters = 10,
        Amount = 11,
        NIC         // Code by Faisal 02/03/2011
    }

    //[ToolboxBitmap(typeof(TextBox))]
    public partial class CrplTextBox : TextBox
    {
        private TextType _TextType;
        private bool _AllowSpace = true;
        private string _Prefix = "";
        private string _Postfix = "";
        private TextBox _ContinuationTextBox;
        private bool AttrtibutesApplied = false;
        private string _NumberFormat = "###,###,##0.00";

        public CrplTextBox()
        {
            InitializeComponent();
            RegisterEvents();
        }

        public CrplTextBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            this.GotFocus += new EventHandler(CrplTextBox_GotFocus);
            this.Leave += new EventHandler(CrplTextBox_Leave);
            this.KeyPress += new KeyPressEventHandler(CrplTextBox_KeyPress);
            this.KeyUp += new KeyEventHandler(CrplTextBox_KeyUp);
            //this.HandleCreated += new EventHandler(CrplTextBox_HandleCreated);
        }

        //private void RegisterEnableParentEvent(Control ctrl)
        //{
        //    if (ctrl.Parent != null)
        //    {
        //        ctrl.Parent.EnabledChanged += new EventHandler(Parent_EnabledChanged);
        //        this.RegisterEnableParentEvent(ctrl.Parent);
        //    }
        //}

        //public void Parent_EnabledChanged(object sender, EventArgs e)
        //{
        //    bool parentState;

        //    if (sender is Control)
        //        parentState = (sender as Control).Enabled;
        //    else
        //        parentState = (sender as Form).Enabled;

        //    if (parentState && this.Enabled && !this.IsAnyParentDisabled(this))
        //    {
        //        SetStyle(ControlStyles.UserPaint, false);
        //        BackColor = Color.White;
        //        this.ApplyDefaultAttributes();
        //    }
        //    else
        //    {
        //        SetStyle(ControlStyles.UserPaint, true);

        //        BackColor = Color.FromKnownColor(KnownColor.Control);
        //        ForeColor = Color.Black;
        //        BorderStyle = BorderStyle.FixedSingle;
        //    }
        //}

        private void CrplTextBox_HandleCreated(object sender, EventArgs e)
        {
            //this.RegisterEnableParentEvent(this);
            //this.Parent_EnabledChanged(this.GetDisabledParent(this), new EventArgs());

            //SetStyle(ControlStyles.UserPaint, !this.Enabled);
            //this.ApplyDefaultAttributes();
        }

        private void ApplyDefaultAttributes()
        {
            if (!this.AttrtibutesApplied)
            {
                //Start By Shamraiz Ahmad On 22-Feb-2008
                if (this._TextType == TextType.Email)
                {
                    this.CharacterCasing = CharacterCasing.Lower;
                }
                else
                {
                    this.CharacterCasing = CharacterCasing.Upper;
                }
                //End By Shamraiz Ahmad On 22-Feb-2008
                this.BorderStyle = BorderStyle.FixedSingle;
                this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                if (this._TextType == TextType.Amount || this._TextType == TextType.Double)
                {
                    this.TextAlign = HorizontalAlignment.Right;
                }

                this.AttrtibutesApplied = true;
            }
        }

        //public new bool Enabled
        //{
        //    get { return base.Enabled; }
        //    set
        //    {
        //        base.Enabled = value;

        //        if (value)
        //        {
        //            SetStyle(ControlStyles.UserPaint, !value);
        //            BackColor = Color.White;
        //            this.ApplyDefaultAttributes();
        //        }
        //        else
        //        {
        //            if (this.IsHandleCreated)
        //                SetStyle(ControlStyles.UserPaint, !value);

        //            BackColor = Color.FromKnownColor(KnownColor.Control);
        //            ForeColor = Color.Black;
        //            BorderStyle = BorderStyle.FixedSingle;
        //        }
        //    }
        //}

        //private object GetDisabledParent(Control ctrl)
        //{
        //    if (ctrl.Parent != null)
        //    {
        //        if (ctrl.Parent.Enabled)
        //            return this.GetDisabledParent(ctrl.Parent);
        //        else
        //            return ctrl.Parent;
        //    }
        //    else
        //        return this.Parent;
        //}

        //private bool IsAnyParentDisabled(Control ctrl)
        //{
        //    if (ctrl.Parent != null)
        //    {
        //        if (ctrl.Parent.Enabled)
        //            return this.IsAnyParentDisabled(ctrl.Parent);
        //        else
        //            return true;
        //    }
        //    else
        //        return false;
        //}

        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    // Draw the normal textbox
        //    base.OnPaint(e);
        //    // Turn on anti-aliasing for the text, otherwise it looks jagged.
        //    e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
        //    // Draw the text in the box as an image. This is ok, because we ONLY
        //    // call OnPaint when the textbox is disabled.
        //    e.Graphics.DrawString(Text, Font, new SolidBrush(Color.Black), 0.65f, 2f);

        //    e.Graphics.DrawRectangle(new Pen(Color.Black), e.ClipRectangle.X, e.ClipRectangle.Y,
        //        e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);
        //}


        public override string Text
        {
            get
            {
                this.ApplyDefaultAttributes();

                return base.Text;
            }
            set
            {
                this.ApplyDefaultAttributes();

                if (value == null)
                {
                    value = "";
                }
                if (this._TextType == TextType.Amount && value != "")
                {
                    value = Convert.ToDouble(value).ToString(this._NumberFormat);

                    double d = 0;
                    double.TryParse(value.ToString(), out d);
                    if (d == 0)
                        value = "";
                }
                base.Text = value.ToUpper();
            }
        }

        public TextType TextType
        {
            set
            {
                this._TextType = value;
            }
            get
            {
                return this._TextType;
            }
        }

        public bool AllowSpace
        {
            set
            {
                this._AllowSpace = value;
            }
            get
            {
                return this._AllowSpace;
            }
        }

        public string Prefix
        {
            set
            {
                this._Prefix = value;
            }
            get
            {
                return this._Prefix;
            }
        }

        public string Postfix
        {
            set
            {
                this._Postfix = value;
            }
            get
            {
                return this._Postfix;
            }
        }

        public TextBox ContinuationTextBox
        {
            set
            {
                this._ContinuationTextBox = value;
            }
            get
            {
                return this._ContinuationTextBox;
            }
        }

        public string NumberFormat
        {
            set
            {
                this._NumberFormat = value;
            }
            get
            {
                return this._NumberFormat;
            }
        }


        private void CrplTextBox_GotFocus(object sender, EventArgs e)
        {
            //Start By Shamraiz Ahmad On 27-Aug-2007
            if (this._TextType == TextType.Amount)
            {
                double dblAmount = Convert.ToDouble(this.Text == "" ? "0" : this.Text);

                this.Text = dblAmount > 0 ? dblAmount.ToString(this._NumberFormat) : "";
            }
            //End By Shamraiz Ahmad On 27-Aug-2007
            this.SelectAll();
        }

        private void CrplTextBox_Leave(object sender, EventArgs e)
        {
            //Start By Shamraiz Ahmad On 27-Aug-2007
            if (this._TextType == TextType.Amount)
            {
                this.Text = Convert.ToDouble(this.Text == "" ? "0" : this.Text).ToString(this._NumberFormat);
                double amnt = 0;
                double.TryParse(this.Text, out amnt);
                if (amnt == 0)
                {
                    this.Text = "";
                }
            }
            //End By Shamraiz Ahmad On 27-Aug-2007

            // Code by Faisal 02/03/2011
            else if (this.TextType == TextType.NIC)
            {
                if (this.Text != string.Empty && this.Text.Length != 13)
                {
                    this.Focus();
                }
                else if (this.Text[3] != '-' || this.Text[6] != '-')
                {
                    this.Focus();
                }
            }
            // End Code by Faisal 02/03/2011
        }

        private void CrplTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            //  Start          By Shamraiz
            if (e.KeyChar == 13)
            {
                SendKeys.Send("{TAB}");
            }
            if (e.KeyChar == 3 || e.KeyChar == 24 || e.KeyChar == 22)
            {
                e.Handled = false;
                return;
            }
            else if (e.KeyChar == 27)
            {
                //if (this.Parent.Parent != null)
                //{
                //    this.Parent.Parent.Dispose();
                //}
                //else if (this.Parent != null)
                //{
                //    this.Parent.Dispose();
                //}
            }
            //  End         By Shamraiz

            switch (this._TextType)
            {
                case TextType.String:
                    //e.Handled = this.IsInvalidString(e.KeyChar);
                    e.Handled = false;
                    break;
                case TextType.StringAndDigit:
                    e.Handled = this.IsInvalidStringORDigit(e.KeyChar);
                    break;
                case TextType.Integer:
                    e.Handled = this.IsInvalidInteger(e.KeyChar);
                    break;
                case TextType.Double:
                    e.Handled = this.IsInvalidDouble(e.KeyChar);
                    break;
                case TextType.Amount:
                    e.Handled = this.IsInvalidDouble(e.KeyChar);
                    break;
                case TextType.PhoneNumber:
                    e.Handled = this.IsInvalidPhoneNumber(e.KeyChar);
                    break;
                case TextType.DateTime:
                    e.Handled = this.IsInvalidDateTime(e.KeyChar);
                    break;
                case TextType.Email:
                    e.Handled = this.IsInvalidEmail(e.KeyChar);
                    break;
                case TextType.URL:
                    e.Handled = this.IsInvalidURL(e.KeyChar);
                    break;
                case TextType.Address:
                    //e.Handled = this.IsInvalidAddress(e.KeyChar);
                    e.Handled = false;
                    break;
                case TextType.DigitAndHyphen:
                    e.Handled = this.IsDigitORHyphen(e.KeyChar);
                    break;
                // Code by Faisal 02/03/2011
                case TextType.NIC:
                    if (e.KeyChar != '\b' && this.Text.Length >= 13)
                    {
                        e.Handled = true;
                        return;
                    }

                    e.Handled = this.IsDigitORHyphen(e.KeyChar);

                    if (this.Text.Length == 3 || this.Text.Length == 6)
                    {
                        if (e.KeyChar != '\b' && !this.IsHypn(e.KeyChar))
                        {
                            e.Handled = true;
                        }
                    }
                    else if (this.IsHypn(e.KeyChar))
                    {
                        e.Handled = true;
                    }
                    break;
                // End Code by Faisal 02/03/2011
                case TextType.AllCharacters:
                    e.Handled = false;
                    break;
            }
        }

        private void CrplTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (this._ContinuationTextBox != null && this.Text.Length == this.MaxLength)
            {
                this._ContinuationTextBox.Focus();
            }
            else if (this.Text.Length == this.MaxLength)
            {
                beepTime();
            }
        }

        private bool IsInvalidString(int keyCode)
        {
            bool isInvalid = true;

            if (IsChar(keyCode) || IsBackSpace(keyCode) || (IsSpceBar(keyCode) && this._AllowSpace))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidStringORDigit(int keyCode)
        {
            bool isInvalid = true;

            if (IsChar(keyCode) || IsNumber(keyCode) || IsBackSpace(keyCode) ||
                (IsSpceBar(keyCode) && this._AllowSpace))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidInteger(int keyCode)
        {
            bool isInvalid = true;

            if (IsNumber(keyCode) || IsBackSpace(keyCode))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidDouble(int keyCode)
        {
            bool isInvalid = true;

            if (IsNumber(keyCode) || IsBackSpace(keyCode))
                isInvalid = false;
            else if (IsDot(keyCode))
            {
                int dotOccurrance = 0;

                foreach (char ch in this.Text)
                {
                    if (IsDot(ch))
                        dotOccurrance++;
                }
                isInvalid = dotOccurrance > 0;
            }
            else if (IsHypn(keyCode))
            {
                this.Text = '-' + this.Text;
                int HypnOccurance = 0;
                  foreach (char ch in this.Text)
                  {
                      if (IsHypn(ch))
                          HypnOccurance++;
                  }
                  isInvalid = HypnOccurance > 0;
                  this.Select(this.Text.Length, 0);
               
            }

            return isInvalid;
        }

        private bool IsInvalidPhoneNumber(int keyCode)
        {
            bool isInvalid = true;

            if (IsNumber(keyCode) || IsBackSpace(keyCode) || (IsSpceBar(keyCode) && this._AllowSpace)
                || IsChar(keyCode) || IsHypn(keyCode) || IsBracket(keyCode))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidDateTime(int keyCode)
        {
            bool isInvalid = true;

            if (IsNumber(keyCode) || IsDot(keyCode) || IsBackSpace(keyCode) || IsHypn(keyCode)
                || IsSlashFrwd(keyCode) || (IsSpceBar(keyCode) && this._AllowSpace) || IsColon(keyCode))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidEmail(int keyCode)
        {
            bool isInvalid = true;

            if (IsChar(keyCode) || IsNumber(keyCode) || IsUnderscore(keyCode) || IsDot(keyCode)
                || IsBackSpace(keyCode) || IsHypn(keyCode))
                isInvalid = false;
            else if (IsAtRate(keyCode))
            {
                int atRateOccurrance = 0;

                foreach (char ch in this.Text)
                {
                    if (IsAtRate(ch))
                        atRateOccurrance++;
                }

                isInvalid = atRateOccurrance > 0;
            }

            return isInvalid;
        }

        private bool IsInvalidURL(int keyCode)
        {
            bool isInvalid = true;

            if (IsChar(keyCode) || IsNumber(keyCode) || IsUnderscore(keyCode) || IsDot(keyCode)
                || IsBackSpace(keyCode) || IsHypn(keyCode) || IsAmpersand(keyCode) || IsPercent(keyCode)
                || IsQuestionMark(keyCode) || IsHashSign(keyCode) || IsEqualChar(keyCode) || IsSlashFrwd(keyCode)
                || IsColon(keyCode))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidAddress(int keyCode)
        {
            bool isInvalid = true;

            if (IsChar(keyCode) || IsNumber(keyCode) || IsBackSpace(keyCode) || IsHypn(keyCode)
                || IsHashSign(keyCode) || IsComma(keyCode) || IsSlashFrwd(keyCode)
                || (IsSpceBar(keyCode) && this._AllowSpace))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsDigitORHyphen(int keyCode)
        {
            bool isInvalid = true;

            if (IsNumber(keyCode) || IsBackSpace(keyCode) || IsHypn(keyCode))
                isInvalid = false;

            return isInvalid;
        }


        /// <summary>
        /// it validates the char type if text type is CharOnly
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsChar(int KeyCode)
        {
            if ((KeyCode > 96 && KeyCode < 123) || (KeyCode > 64 && KeyCode < 91))
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is NumOnly
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsNumber(int KeyCode)
        {
            if (KeyCode > 47 && KeyCode < 58)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is BackSpace
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsBackSpace(int KeyCode)
        {
            if (KeyCode == 8)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is DotOnly
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsDot(int KeyCode)
        {
            if (KeyCode == 46)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is IsSpecialChar
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsSpecialChar(int KeyCode)
        {
            if (KeyCode > 32 && KeyCode < 39 || KeyCode > 39 && KeyCode < 44 || KeyCode > 44 && KeyCode < 48 || KeyCode > 57 && KeyCode < 65 || KeyCode > 90 && KeyCode < 96 || KeyCode > 122 && KeyCode < 127)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is SpceBar
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsSpceBar(int KeyCode)
        {
            if (KeyCode == 32)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is Hypn
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsHypn(int KeyCode)
        {
            if (KeyCode == 45)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is SlashFrwd
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsSlashFrwd(int KeyCode)
        {
            if (KeyCode == 47)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is AtRate
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsAtRate(int KeyCode)
        {
            if (KeyCode == 64)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsPercent(int KeyCode)
        {
            if (KeyCode == 37)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsAmpersand(int KeyCode)
        {
            if (KeyCode == 38)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsQuestionMark(int KeyCode)
        {
            if (KeyCode == 63)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsHashSign(int KeyCode)
        {
            if (KeyCode == 35)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsUnderscore(int KeyCode)
        {
            if (KeyCode == 95)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsBracket(int KeyCode)
        {
            if (KeyCode == 40 || KeyCode == 41)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsColon(int KeyCode)
        {
            if (KeyCode == 58)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsComma(int KeyCode)
        {
            if (KeyCode == 44)
                return true;
            else
                return false;
        }

        /// <summary>
        /// it validates the char type if text type is Percent
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsEqualChar(int KeyCode)
        {
            if (KeyCode == 61)
                return true;
            else
                return false;
        }

        [DllImport("Kernel32.dll")]
        public static extern bool Beep(UInt32 frequency, UInt32 duration);

        public static void beepTime()
        {
            //Beep(400, 120);
        }
    }
}
