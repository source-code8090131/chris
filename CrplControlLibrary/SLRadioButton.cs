﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace CrplControlLibrary
{
    public class SLRadioButton : RadioButton
    {
         #region --Field Segment--
        private string m_strDataFieldMapping = string.Empty;
      
        private bool IsEnabled = true;
          #endregion

       

        #region IBaseControl Members

        public string DataFieldMapping
        {
            get
            {
                return m_strDataFieldMapping;
            }
            set
            {
                m_strDataFieldMapping = value;
            }
        }

           
        [Browsable(true)]
        [Description("Text Enable/Disable")]
        [Category("Custom Behaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }
        
        #endregion

        #region --EventHandler Segment--


        #endregion
    }
}
