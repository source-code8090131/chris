using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace CrplControlLibrary
{
    public class SLCheckBox : CheckBox, IBaseControl
    {
        #region --Field Segment--
        private string m_strDataFieldMapping = string.Empty;
        private bool pbIsRequired;
        private bool IsEnabled = true;

        #endregion
        
        #region IBaseControl Members

        public string DataFieldMapping
        {
            get
            {
                return m_strDataFieldMapping;
            }
            set
            {
                m_strDataFieldMapping = value;
            }
        }

        [DefaultValue(false)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets required field validation of control.")]
        public bool IsRequired
        {
            get
            { return pbIsRequired; }
            set
            {
                pbIsRequired = value;
            }
        }

        [Browsable(true)]
        [Description("Text Enable/Disable")]
        [Category("Custom Behaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }

        protected override void OnLostFocus(EventArgs e)
        {
            if (this.IsRequired && this.Checked == false)
            {
                MessageBox.Show("You Can't leave field empty", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Focus();
            }
            base.OnLostFocus(e);
        }

        #endregion

        #region --Event Segment--
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
        }
        #endregion
    }
}
