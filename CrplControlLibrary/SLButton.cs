﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CrplControlLibrary
{
    public class SLButton : Button
    {
        private string m_strActionType = string.Empty;

        public SLButton()
        { 
            //this.Click += new EventHandler(SLButton_Click);
        }
        public string ActionType
        {
            get
            {
                return m_strActionType;
            }
            set
            {
                m_strActionType = value;
            }
        }
       
        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            //this.PerformClick();
        }
      
    }
}
