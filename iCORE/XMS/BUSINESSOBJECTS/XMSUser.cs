// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL


using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class XMSUser
    {
        // fields to be set before quering XMS
        private string soeid = null;
        private string soepassword = null;
        private string domain = null;
        private string systemname = null;
        private string webserviceslink = null;

        // fields returnd by XMS
        private int iRet_str = 0;
        private int templateid = 0;
        private int accessmatrixcount = 0;
        private int businessgroupid = 0;
        private int classid = 0;
        private string dbserver = null;
        private int defaultdeptid = 0;
        string apppassword = null;
        string apppassword_decrypted = null;
        string ErrDesc_str = null;


        // Container for XMSScreens Objects
        SortedList hash_screens;
        XMSScreens xmsscreensobj = null;

        private string objectstate = null;

        // setter methods - before quering xms

        public XMSUser()
        {
            hash_screens = new SortedList();
        }


        public void setSoeId(string soeid)
        {
            this.soeid = soeid;
        }
        public void setPassword(string soepassword)
        {
            this.soepassword = soepassword;
        }
        public void setDomain(string domain)
        {
            this.domain = domain;
        }
        public void setSystemName(string systemname)
        {
            this.systemname = systemname;
        }
        public void setWebServicesLink(string webserviceslink)
        {
            this.webserviceslink = webserviceslink;
        }

        // getter methods - before quering xms

        public string getSoeId()
        {
            return this.soeid;
        }
        public string getPassword()
        {
            return this.soepassword;
        }
        public string getDomain()
        {
            return this.domain;
        }
        public string getSystemName()
        {
            return this.systemname;
        }
        public string getWebServicesLink()
        {
            return this.webserviceslink;
        }

        // setter methods - fields returned from xms             

        public void setIRetStr(int iRet_str)
        {
            this.iRet_str = iRet_str;
        }
        public void setErrDescStr(string ErrDesc_str)
        {
            this.ErrDesc_str = ErrDesc_str;
        }
        public void setTemplateId(int templateid)
        {
            this.templateid = templateid;
        }
        public void setAccessMatrixCount(int accessmatrixcount)
        {
            this.accessmatrixcount = accessmatrixcount;
        }
        public void setBusinessGroupId(int businessgroupid)
        {
            this.businessgroupid = businessgroupid;
        }
        public void setClassId(int classid)
        {
            this.classid = classid;
        }
        public void setDBbserver(string dbserver)
        {
            this.dbserver = dbserver;
        }
        public void setDefaultDeptId(int defaultdeptid)
        {
            this.defaultdeptid = defaultdeptid;
        }
        public void setAppPassword(string apppassword)
        {
            this.apppassword = apppassword;
        }
        public void setAppPasswordDecrypted(string apppassword_decrypted)
        {
            this.apppassword_decrypted = apppassword_decrypted;
        }

        // getter methods - fields returned from xms             
        public int getIRetStr()
        {
            return this.iRet_str;
        }
        public string getErrDescStr()
        {
            return this.ErrDesc_str;
        }
        public int getTemplateId()
        {
            return this.templateid;
        }
        public int getAccessMatrixCount()
        {
            return this.accessmatrixcount;
        }
        public int getBusinessGroupId()
        {
            return this.businessgroupid;
        }
        public int getClassId()
        {
            return this.classid;
        }
        public string getDBbserver()
        {
            return this.dbserver;
        }
        public int getDefaultDeptId()
        {
            return this.defaultdeptid;
        }
        public string getAppPassword()
        {
            return this.apppassword;
        }
        public string getAppPasswordDecrypted()
        {
            return this.apppassword_decrypted;
        }

        public SortedList getHashScreens()
        {
            return this.hash_screens;
        }

        public void addScreenToHash(string screenid)
        {
            // filing the hashtable with screen ids returned from XMS
            xmsscreensobj = new XMSScreens();
            xmsscreensobj.setFunctionId(screenid);
            hash_screens.Add(screenid, xmsscreensobj);
        }

        public void setObjectState(string objectstate)
        {
            this.objectstate = objectstate;
        }
        public string getObjectState()
        {
            return this.objectstate;
        }

    }
}
