// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL


using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    class ByPassXMS
    {
        XMS.DATAOBJECTS.ConnectionBean xmsconnbean = null;

        public XMSUser fillXMXUser(string soeid, XMS.DATAOBJECTS.ConnectionBean xmsconnbeanobj)
        {
            this.xmsconnbean = xmsconnbeanobj;
            XMSUser xmsuser = new XMSUser();
            xmsuser.setAccessMatrixCount(4);
            xmsuser.setAppPassword("hello");
            xmsuser.setAppPasswordDecrypted("hello");
            xmsuser.setBusinessGroupId(1);
            xmsuser.setClassId(1);
            xmsuser.setDBbserver("hello");
            xmsuser.setDefaultDeptId(1);
            xmsuser.setDomain("hello");
            xmsuser.setIRetStr(1);
            xmsuser.setObjectState("hello");
            xmsuser.setPassword("hello");
            xmsuser.setSoeId(soeid);
            xmsuser.setSystemName("hello");
            xmsuser.setTemplateId(1);
            xmsuser.setWebServicesLink("hello");

            //  XMS.DATAOBJECTS.ConnectionBean conbean = new XMS.DATAOBJECTS.ConnectionBean("xms", "sb13446", "sb13446");
            //XMS.DATAOBJECTS.ConnectionBean conbean = new XMS.DATAOBJECTS.ConnectionBean("xms", "bm69820", "Citibank1");
            SqlConnection conn = this.xmsconnbean.getDatabaseConnection();
            string str_sql = "select functionid from  " + Properties.Settings.Default.MenuTable; // "where temp_soeid = '" + soeid + "'";
            SqlDataReader reader = this.xmsconnbean.getData(str_sql, conn);
            string screenid = null;
            while (reader.Read())
            {
                int i = (Int32)(reader[0]);
                screenid = i.ToString();
                xmsuser.addScreenToHash(screenid);

            }

            this.xmsconnbean.closeConn(conn);
            return xmsuser;

        }


    }
}
