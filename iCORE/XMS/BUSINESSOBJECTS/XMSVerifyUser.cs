// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFULL
//#define PRODUCTION
#define DEV
using System;
using System.Collections.Generic;
using System.Text;
using Citigroup.SST.XMS;
using System.Configuration;

namespace iCORE.XMS.BUSINESSOBJECTS
{
    public class XMSVerifyUser
    {
        BUSINESSOBJECTS.XMSUser xmsuserobj = null;


        public string VerifyUser(PRESENTATIONOBJECTS.FORMBEANS.Login_FormBean formbeanobj)
        {
            xmsuserobj = new XMSUser();

            int iRet;
            Citigroup.SST.XMS.XmsInterface xmsinterface;
            Citigroup.SST.XMS.InterOpCls interop;
            interop = new Citigroup.SST.XMS.InterOpCls();
            xmsinterface = (Citigroup.SST.XMS.XmsInterface)interop;

            string soeid = formbeanobj.getSoeId();
            string password = formbeanobj.getSoePassword();
            string domain = "SSO";
            string systemname = ConfigurationManager.AppSettings["systemname"];

            string webserviceslink = "";

            // below is dev web service link
            //string webserviceslink = "https://xsecurity-dev.eur.nsroot.net/xmswebservices/xmsappacl.asmx?op=UserAuthNAccessFullRetrievalUsingPWD";


            //#if UAT
            // below is uat web service link
            webserviceslink = ConfigurationManager.AppSettings["XMSService"];//"https://xsecurity-uat.eur.nsroot.net/xmswebservices/xmsappacl.asmx?op=UserAuthNAccessFullRetrievalUsingPWD";
            //#endif
            //below is production web services link
            // Fault Web service URl mention below for Production
            //string webserviceslink = "https://extensiblesecuritymanagement.emea.citigroup.net/XMSWebApp/xmsappacl.asmx?op=UserAuthNAccessFullRetrievalUsingPWD";
            // Correct Web service URL mention below for Production
            // Najia:(26-March-2007)sending for PROD v1040, MVA Production issue:9238554
            //                             , MMRR Live Implementation   

            // Najia:(24-April-2007)sending for PROD v1070, CBG live implementation            
            // Najia:(25-April-2007)sending for PROD v1080, FXRR Problem fix: VT#9855764
            // Najia:(9-May-2007)sending for PROD v1090, MMRR Problem fix: VT#9986432

            //#if PRODUCTION
            // Adnan T:(10-Oct-2009) sending for PROD v iCORE_CMG_300
            //webserviceslink = "https://extensiblesecuritymanagement.emea.citigroup.net/XMSWebServices/xmsappacl.asmx?op=UserAuthNAccessFullRetrievalUsingPWD";

            //#endif      
            string errordesc = null;
            xmsuserobj.setSoeId(soeid);
            xmsuserobj.setPassword(password);
            xmsuserobj.setDomain(domain);
            xmsuserobj.setSystemName(systemname);
            xmsuserobj.setObjectState("NotFilledState");

            xmsinterface.SetWSUrl(webserviceslink);
            xmsinterface.XMSUserAuthNAccessFullRetrievalUsingPwd(systemname, soeid, domain, password);
            iRet = xmsinterface.ErrorCode();
            errordesc = xmsinterface.ErrorDesc();
            xmsuserobj.setIRetStr(iRet);
            xmsuserobj.setErrDescStr(errordesc);
            if (iRet == 0)
            {
                string xms_soeid = xmsinterface.SoeId();
                int templateid = xmsinterface.TemplateId();
                int accessmatrixcount = xmsinterface.AccessMatrixCount();
                int businessgroupid = xmsinterface.BusinessGroupId();
                int classid = xmsinterface.ClassId();
                string dbserver = xmsinterface.DbServer();
                int defaultdeptid = xmsinterface.DefaultDeptId();


                xmsuserobj.setTemplateId(templateid);
                xmsuserobj.setAccessMatrixCount(accessmatrixcount);
                xmsuserobj.setBusinessGroupId(businessgroupid);
                xmsuserobj.setClassId(classid);
                xmsuserobj.setDBbserver(dbserver);
                xmsuserobj.setDefaultDeptId(defaultdeptid);

                //App pasword with encrypt function
                string apppassword = xmsinterface.AppUserPwd();
                // app pasword with encrypt function
                string apppassword1 = Citigroup.SST.XMS.AppEncrypt.Encrypt(apppassword);
                xmsuserobj.setAppPassword(apppassword);
                xmsuserobj.setAppPasswordDecrypted(apppassword1);


                // code for reading access matrix count
                for (int accessmatrixcount1 = 0; accessmatrixcount1 < xmsinterface.AccessMatrixCount(); accessmatrixcount1++)
                {
                    xmsuserobj.addScreenToHash(xmsinterface.AppId(accessmatrixcount1).ToString());
                }
                xmsuserobj.setObjectState("FilledState");
            }
            else
            {
                //nothing
            }
            return iRet.ToString() + errordesc;



        }
        public BUSINESSOBJECTS.XMSUser getXMSUser()
        {
            return xmsuserobj;
        }



    }
}
