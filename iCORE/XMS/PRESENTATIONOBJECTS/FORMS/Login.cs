// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL
//#define PRODUCTION
#define DEV
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Configuration;
using Citigroup.SST.XMS;


namespace iCORE.XMS.PRESENTATIONOBJECTS.FORMS
{
    public partial class Login : Form
    {
        private int xmsErrCode = 0;
        private string xmsErrDesc = null;

        public Login()
        {
            InitializeComponent();
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("en-GB");
        }

        private void button1_Click(object sender, EventArgs e)
        {
#if PRODUCTION
            try
            {

                BUSINESSOBJECTS.XMSUser xmsuser = null;
                BUSINESSOBJECTS.XMSScreens xmsscreens = null;

                string soeid = null;
                string soepassword = null;
                soeid = txtSoeid.Text;
                soepassword = txtPassword.Text;


                // starting of if statement
                if (soeid == "" | soepassword == "")
                {
                    MessageBox.Show("Field cannot be left blank");
                }
                else
                {
                    // complete code for authentication starts here

                    //***************************************************
                    
                    FORMBEANS.Login_FormBean formbeanobj = new FORMBEANS.Login_FormBean();
                    formbeanobj.setSoeId(soeid);
                    formbeanobj.setSoePassword(soepassword);
                    LINKER.XMSController controller = new LINKER.XMSController();
                    xmsuser = controller.callXMSVerifyUserService(formbeanobj);
                    string accessmatrixcount = xmsuser.getAccessMatrixCount().ToString();
                    string busineessgroupid_str = null;
                    xmsErrCode = xmsuser.getIRetStr();
                    xmsErrDesc = xmsuser.getErrDescStr();

                    
                    if (xmsuser.getIRetStr().ToString().Equals("0"))
                    {
                        // every thing is fine 


                        
                        // replace business group id with 3 with 1

                        if (xmsuser.getBusinessGroupId().ToString().Equals("1"))
                        {

                            busineessgroupid_str = "cbg";
                        }
                        else if (xmsuser.getBusinessGroupId().ToString().Equals("2"))
                        {

                            busineessgroupid_str = "fincon";
                        }
                        else if (xmsuser.getBusinessGroupId().ToString().Equals("3"))
                        {
                            busineessgroupid_str = "trops";
                        }
                        else if (xmsuser.getBusinessGroupId().ToString().Equals("4"))
                        {
                            busineessgroupid_str = "trops";
                        }
                        else if (xmsuser.getBusinessGroupId().ToString().Equals("5"))
                        {
                            busineessgroupid_str = "cmg";
                        }                        
                        else
                        {
                            MessageBox.Show("Business group not found");
                        }
                        //***************************************************
                        
                        
                        
                        //MessageBox.Show("Error Code returned:" + xmsuser.getIRetStr().ToString());
                        /*MessageBox.Show("XMS User state:" + accessmatrixcount);
                        MessageBox.Show("XMS User State" + xmsuser.getObjectState());
                        MessageBox.Show("Business Group Id:" + xmsuser.getBusinessGroupId().ToString());
                        MessageBox.Show("User id" + xmsuser.getSoeId());
                        MessageBox.Show("Password" + xmsuser.getAppPasswordDecrypted());
                        
                        SortedList hashscreens = xmsuser.getHashScreens();
                        
                         MessageBox.Show(xmsuser.getIRetStr().ToString());
                        IEnumerator ie = hashscreens.Values.GetEnumerator();
                        while (ie.MoveNext() && ie.Current != null)
                        {
                            xmsscreens = (BUSINESSOBJECTS.XMSScreens)ie.Current;
                            MessageBox.Show("Function id returned from hash:" + xmsscreens.getFunctionId());
                        }
                        */

                        // complete code for authentication ends here

                        // passing business specific database conection bean object from here to midi form           
                        ////////XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean("trops", "bm69820", "Citibank1");
                        string xmsuser_str = xmsuser.getSoeId();
                        string xmspassword_str = xmsuser.getAppPasswordDecrypted();
                       


                        String database = ConfigurationManager.AppSettings["database"];
                        XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean(database, xmsuser_str, xmspassword_str);
                        // passing xms base connection refrence from here to midi. this refrence will only be used by 
                        // main menu for xms specific purposes.
                        //////XMS.DATAOBJECTS.ConnectionBean xmsconnbean = new XMS.DATAOBJECTS.ConnectionBean("xms", "bm69820", "Citibank1");
                        XMS.DATAOBJECTS.ConnectionBean xmsconnbean = new XMS.DATAOBJECTS.ConnectionBean("xms", xmsuser_str, xmspassword_str);
                        

                        // temporary bypass xms            
                        //////BUSINESSOBJECTS.ByPassXMS bypass = new BUSINESSOBJECTS.ByPassXMS();
                        //////xmsuser = bypass.fillXMXUser(txtSoeid.Text, xmsconnbean);
                        //////MessageBox.Show("hash screen count" + xmsuser.getHashScreens().Count);

                        //===================================================================================
                        // This code will be used for debugging purpose
                        /*
                        string complete_path = "C:/iCORE-Logs/";
                        string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                        string path = complete_path + filename;
                        StreamWriter sw = File.AppendText(path);
                        sw.WriteLine("=====================================================================");
                        sw.WriteLine("Time of logging:" + DateTime.Now);
                        sw.WriteLine("Value Return from XMS Web services for Login user");
                        sw.WriteLine("Soe ID: " + xmsuser.getSoeId());
                        sw.WriteLine("Business Group Id: " + xmsuser.getBusinessGroupId().ToString());
                        sw.WriteLine("Template ID :" + xmsuser.getTemplateId().ToString());
                        // write screen collection id
                        sw.WriteLine(" Screen Count :" + xmsuser.getHashScreens().Count);
                        SortedList screencollection_sortedlist = xmsuser.getHashScreens();
                        IEnumerator ie = screencollection_sortedlist.Values.GetEnumerator();
                        while (ie.MoveNext() && ie.Current != null)
                        {
                            BUSINESSOBJECTS.XMSScreens screen_obj = (BUSINESSOBJECTS.XMSScreens)(ie.Current);
                            string screenid = screen_obj.getFunctionId();
                            sw.WriteLine("Screen Id : " + screenid);
                        }
                        sw.WriteLine("Class Id :" + xmsuser.getClassId().ToString());
                        sw.WriteLine("Department Id : " + xmsuser.getDefaultDeptId().ToString());
                        sw.WriteLine("Xms Error Code : " + xmsErrCode);
                        sw.WriteLine("XMS Error Description : " + xmsErrDesc);
                        sw.WriteLine("======================================================================");
                        sw.Close();
                        */
                        
                        //======================================================================
                        // code to diplay Midi form
                        XMS.PRESENTATIONOBJECTS.FORMS.MainMenu main_obj = new XMS.PRESENTATIONOBJECTS.FORMS.MainMenu(connbean, xmsconnbean);
                        main_obj.passLoginFormReference(this);
                        main_obj.Text = "iCORE-Intranet Centralized Operations Repository Engine  Login UserID : " + soeid + " Server:" + ConfigurationManager.AppSettings["Datasource"]+ " Date:"+ DateTime.Now.ToString("dd/MM/yyyy");
                        main_obj.Show();
                        main_obj.setXMSUser(xmsuser);
                    
                    }
                    else
                    {
                        //MessageBox.Show("XMS Authentication failed. Error Code:" + xmsuser.getIRetStr().ToString());
                        MessageBox.Show("XMS Exception Error Code : " + xmsErrCode + "\n" +
                                        "XMS Exception Error Description :" + xmsErrDesc);
                    }
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show("Exception encountered:" + excep.Message);
            }

            // xms authentication logic is commented below
            
            //int iRet;
            //Citigroup.SST.XMS.XmsInterface xmsinterface;
            //Citigroup.SST.XMS.InterOpCls interop;
            //interop = new Citigroup.SST.XMS.InterOpCls();
            //xmsinterface = (Citigroup.SST.XMS.XmsInterface)interop;
            //string soeid = txtSoeid.Text;
            //string password = txtPassword.Text;
            //string domain = "SSO";            
            //string systemname = "iCORE";
            //string webserviceslink = "https://xsecurity-dev.eur.nsroot.net/xmswebservices/xmsappacl.asmx?op=UserAuthNAccessFullRetrievalUsingPWD";
            //xmsinterface.SetWSUrl(webserviceslink);
            //xmsinterface.XMSUserAuthNAccessFullRetrievalUsingPwd(systemname, soeid, domain, password);
            //iRet = xmsinterface.ErrorCode();
            //MessageBox.Show("Error Code:" + iRet.ToString());

            //string xms_soeid = xmsinterface.SoeId();
            //int templateid = xmsinterface.TemplateId();
            //int accessmatrixcount = xmsinterface.AccessMatrixCount();
            //int businessgroupid = xmsinterface.BusinessGroupId();
            //int classid = xmsinterface.ClassId();
            //string dbserver = xmsinterface.DbServer();
            //int defaultdeptid = xmsinterface.DefaultDeptId();

            //MessageBox.Show("template id:" + templateid.ToString());
            //MessageBox.Show("accessmatrixcount:" + accessmatrixcount.ToString());
            //MessageBox.Show("businessgroupid:" + businessgroupid.ToString());
            //MessageBox.Show("classid:" + classid.ToString());
            //MessageBox.Show("dbserver:" + dbserver.ToString());


            //// code for reading access matrix count
            //for (int accessmatrixcount1 = 0; accessmatrixcount1 < xmsinterface.AccessMatrixCount(); accessmatrixcount1++)
            //{
            //    MessageBox.Show("Screen Id:" + xmsinterface.AppId(accessmatrixcount1).ToString());
            //}

            //// App pasword with encrypt function
            //string apppassword = xmsinterface.AppUserPwd();
            //MessageBox.Show("XMS without encrypt function :" + apppassword);
            //// app pasword with encrypt function
            //string apppassword1 = Citigroup.SST.XMS.AppEncrypt.Encrypt(apppassword);
            //MessageBox.Show("XMS with encrypt function return:" + apppassword1);   
#endif

#if DEV
            try
            {

                BUSINESSOBJECTS.XMSUser xmsuser = null;
                BUSINESSOBJECTS.XMSScreens xmsscreens = null;

                string soeid = null;
                string soepassword = null;
                soeid = txtSoeid.Text;
                soepassword = txtPassword.Text;


                // starting of if statement
                if (soeid == "" | soepassword == "")
                {
                    MessageBox.Show("Field cannot be left blank");
                }
                else
                {
                    // complete code for authentication starts here
                    /*
                    FORMBEANS.Login_FormBean formbeanobj = new FORMBEANS.Login_FormBean();
                    formbeanobj.setSoeId(soeid);
                    formbeanobj.setSoePassword(soepassword);
                    LINKER.XMSController controller = new LINKER.XMSController();
                    xmsuser = controller.callXMSVerifyUserService(formbeanobj);
                    string accessmatrixcount = xmsuser.getAccessMatrixCount().ToString();
                    string busineessgroupid_str = null;
                    */
                    // replace business group id with 3 with 1
                    /*
                    if (xmsuser.getBusinessGroupId().ToString().Equals("1"))
                    {

                        busineessgroupid_str = "cbg";
                    }
                    else if (xmsuser.getBusinessGroupId().ToString().Equals("2"))
                    {

                        busineessgroupid_str = "fincon";
                    }
                    else if (xmsuser.getBusinessGroupId().ToString().Equals("3"))
                    {
                        busineessgroupid_str = "trops";
                    }

                    MessageBox.Show("Error Code returned:" + xmsuser.getIRetStr().ToString());
                    MessageBox.Show("XMS User state:" + accessmatrixcount);
                    MessageBox.Show("XMS User State" + xmsuser.getObjectState());
                    MessageBox.Show("Business Group Id:" + xmsuser.getBusinessGroupId().ToString());
                    MessageBox.Show("User id" + xmsuser.getSoeId());
                    MessageBox.Show("Password" + xmsuser.getAppPasswordDecrypted());
                    SortedList hashscreens = xmsuser.getHashScreens();
                    MessageBox.Show(xmsuser.getIRetStr().ToString());
                    IEnumerator ie = hashscreens.Values.GetEnumerator();
                    while (ie.MoveNext() && ie.Current != null)
                    {
                        xmsscreens = (BUSINESSOBJECTS.XMSScreens)ie.Current;
                        MessageBox.Show("Function id returned from hash:" + xmsscreens.getFunctionId());
                    }
                    */



                    // complete code for authentication ends here

                    // passing business specific database conection bean object from here to midi form           
                    //XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean("trops", "bm69820", "Citibank1");
                    //XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean("fincon", "bm69820", "Citibank1");
                    //String UserID = ConfigurationManager.AppSettings["UserID"];
                    //String Password = ConfigurationManager.AppSettings["Password"];

                    String database = ConfigurationManager.AppSettings["database"];
                    XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean(database, soeid, soepassword);
                    // XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean("cbg", "bm69820", "Citibank1");
                    /*
                    string xmsuser_str = xmsuser.getSoeId();
                    string xmspassword_str = xmsuser.getAppPasswordDecrypted();
                    XMS.DATAOBJECTS.ConnectionBean connbean = new XMS.DATAOBJECTS.ConnectionBean(busineessgroupid_str, xmsuser_str, xmspassword_str);
                   */
                    // passing xms base connection refrence from here to midi. this refrence will only be used by 
                    // main menu for xms specific purposes.
                    XMS.DATAOBJECTS.ConnectionBean xmsconnbean = new XMS.DATAOBJECTS.ConnectionBean("xms", soeid, soepassword);
                    /*
                     XMS.DATAOBJECTS.ConnectionBean xmsconnbean = new XMS.DATAOBJECTS.ConnectionBean("xms", xmsuser_str, xmspassword_str);
                     */


                    // temporary bypass xms            
                    BUSINESSOBJECTS.ByPassXMS bypass = new BUSINESSOBJECTS.ByPassXMS();
                    xmsuser = bypass.fillXMXUser(txtSoeid.Text, xmsconnbean);
                    //MessageBox.Show("hash screen count" + xmsuser.getHashScreens().Count);


                    // code to diplay Midi form
                    XMS.PRESENTATIONOBJECTS.FORMS.MainMenu main_obj = new XMS.PRESENTATIONOBJECTS.FORMS.MainMenu(connbean, xmsconnbean);
                    main_obj.passLoginFormReference(this);
                    main_obj.Show();
                    main_obj.setXMSUser(xmsuser);
                    main_obj.Text = "iCORE-Intranet Centralized Operations Repository Engine  Login UserID : " + soeid + " Server:" + ConfigurationManager.AppSettings["Datasource"] + " Date:" + DateTime.Now.ToString("dd/MM/yyyy");

                }
            }
            catch (Exception excep)
            {
                MessageBox.Show("Exception encountered:" + excep.Message);
            }
#endif
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to Exit the application";
            string caption = "Exit Form";
            MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
            DialogResult result;

            // Displays the MessageBox.

            result = MessageBox.Show(this, message, caption, buttons,
                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1,
                MessageBoxOptions.RightAlign);

            if (result == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        #region Code by Faisal Iqbal [2010-09-28]

        private void Login_Shown(object sender, EventArgs e)
        {
//#if DEV
//            this.txtSoeid.Text = "sb13446";
//            this.txtPassword.Text = "sb";
//            button1.PerformClick();
//#endif
        }
        
        #endregion


    }
}