namespace iCORE.XMS.PRESENTATIONOBJECTS.FORMS
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("iCORE", -2, -2);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.treeMainMenu = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // treeMainMenu
            // 
            this.treeMainMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeMainMenu.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeMainMenu.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeMainMenu.HideSelection = false;
            this.treeMainMenu.Location = new System.Drawing.Point(0, 0);
            this.treeMainMenu.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.treeMainMenu.Name = "treeMainMenu";
            treeNode1.ImageIndex = -2;
            treeNode1.Name = "iCORE";
            treeNode1.SelectedImageIndex = -2;
            treeNode1.Text = "iCORE";
            this.treeMainMenu.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.treeMainMenu.Size = new System.Drawing.Size(218, 546);
            this.treeMainMenu.TabIndex = 0;
            this.treeMainMenu.TabStop = false;
            this.treeMainMenu.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(883, 546);
            this.Controls.Add(this.treeMainMenu);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCORE -iCentrallized Operations Repository Engine";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainMenu_FormClosing);
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeMainMenu;

    }
}