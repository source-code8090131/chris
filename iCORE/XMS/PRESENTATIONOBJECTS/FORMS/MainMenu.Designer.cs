namespace iCORE.XMS.PRESENTATIONOBJECTS.FORMS
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("iCORE", -2, -2);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.treeMainMenu = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.splLeft = new System.Windows.Forms.Splitter();
            this.SuspendLayout();
            // 
            // treeMainMenu
            // 
            this.treeMainMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeMainMenu.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeMainMenu.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.treeMainMenu.HideSelection = false;
            this.treeMainMenu.Location = new System.Drawing.Point(0, 0);
            this.treeMainMenu.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.treeMainMenu.Name = "treeMainMenu";
            treeNode1.ImageIndex = -2;
            treeNode1.Name = "iCORE";
            treeNode1.SelectedImageIndex = -2;
            treeNode1.Text = "iCORE";
            this.treeMainMenu.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.treeMainMenu.Size = new System.Drawing.Size(220, 546);
            this.treeMainMenu.TabIndex = 0;
            this.treeMainMenu.TabStop = false;
            this.treeMainMenu.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            this.treeMainMenu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.treeMainMenu_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(483, -15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // splLeft
            // 
            this.splLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splLeft.Location = new System.Drawing.Point(220, 0);
            this.splLeft.Name = "splLeft";
            this.splLeft.Size = new System.Drawing.Size(3, 546);
            this.splLeft.TabIndex = 4;
            this.splLeft.TabStop = false;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(883, 546);
            this.Controls.Add(this.splLeft);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeMainMenu);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainMenu_FormClosing);
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeMainMenu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splLeft;

    }
}