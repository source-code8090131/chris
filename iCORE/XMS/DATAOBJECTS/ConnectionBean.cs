// IN THE NAME OF ALLAH THE BENEFICIENT THE MERCIFUL
//#define PRODUCTION
#define DEV
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Configuration;



namespace iCORE.XMS.DATAOBJECTS
{
    public class ConnectionBean
    {

        String constr = null;
        private string m_UserId;

        public string UserId
        {
            get { return m_UserId; }
        }
        private string m_Password;

        public string Pwd
        {
            get { return m_Password; }
        }

        private string M_DbName;

        public string DbName
        {
            get { return M_DbName; }
        }
        private string M_Server;

        public string M_ServerName
        {
            get { return M_Server; }

        }

        public ConnectionBean(String databasetype)
        {

            if (databasetype.Equals("trops"))
            {
                constr = global::iCORE.Properties.Settings.Default.tropsConnectionString;
            }
            else if (databasetype.Equals("fincon"))
            {
                constr = global::iCORE.Properties.Settings.Default.tropsConnectionString;
            }
            else if (databasetype.Equals("cbg"))
            {
                constr = global::iCORE.Properties.Settings.Default.tropsConnectionString;
            }
            //Najia - 30-Nov-2007: new database name added for cmg.The dummy connection string added for all 
            // cmg Application would be cmgReports     
            else if (databasetype.Equals("cmg"))
            {
                //constr = global::iCORE.Properties.Settings.Default.CMGReports;
            }
            else if (databasetype.Equals("xms"))
            {
                //constr = global::iCORE.Properties.Settings.Default.xmsConnectionString;
            }
        }

        // over loaded method for connection bean 
        // this constructor is used when using xms authentication
        public ConnectionBean(string databasetype, string userid, string password)
        {
            // below link is Dev Database
            //constr = "Data Source=khicmesd005dv;Initial Catalog=" + databasetype + ";User ID=" + userid + ";Password=" + password;
            //#if UAT
            //            // below link is UAT Database
            //            //Data Source is changed by SL as recommended by Adnan Tariq
            //            //constr = "Data Source=169.162.105.22,2431;Initial Catalog=" + databasetype + ";User ID=" + userid + ";Password=" + password;
            //            constr = "Data Source=169.190.162.176,2431;Initial Catalog=" + databasetype + ";User ID=" + userid + ";Password=" + password;
            //            System.Diagnostics.Debug.Print(constr);
            //            //System.Diagnostics.Debug.Print(constr);
            //#endif

            // below link is Production Database
            // Najia:(26-March-2007)sending for PROD v1040, MVA Production issue:9238554
            // , MMRR Live Implementation   
            // Najia:(24-April-2007)sending for PROD v1070, CBG Module live implementation
            // Najia:(25-April-2007)sending for PROD v1080, FXRR module Problem fix:VT#:9855764 
            // Najia:(9-May-2007)sending for PROD v1090, MMRR module Problem fix:VT#:9986432           

            //#if PRODUCTION
            // Adnan T: (30-Oct-2009) from PROD iCORE_CMG_300
            m_UserId = userid;
            m_Password = password;

            //constr = "Data Source=169.162.105.170,2431;Initial Catalog=" + databasetype + ";User ID=" + userid + ";Password=" + password;
            String DataSource = ConfigurationManager.AppSettings["DataSource"];
            String DataBase = ConfigurationManager.AppSettings["Database"];
            M_DbName = DataBase;
            M_Server = DataSource;
            constr = "Data Source=" + DataSource + ";Initial Catalog=" + DataBase + ";User ID=" + userid + ";Password=a$" + password;
            //constr = "Data Source=" + DataSource + ";Initial Catalog=" + DataBase + ";User ID=" + userid + ";Password=" + password;
            //#endif

//#if DEV
           // constr = "Data Source=169.162.105.150,2431;Initial Catalog=" + databasetype + ";User ID=" + userid + ";Password=" + password;
//#endif
            // for COB
            //constr = "Data Source=169.162.35.24,2431;Initial Catalog=" + databasetype + ";User ID=" + userid + ";Password=" + password;
        }

        public SqlConnection getDatabaseConnection()
        {
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = constr;
            sqlconn.Open();
            return sqlconn;
        }
        public SqlConnection getDatabaseConnection(int timeout)
        {
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = constr + ";Connect Timeout=" + timeout + ";";
            sqlconn.Open();
            return sqlconn;
        }


        public int doInsertUpdate(String str_sql, SqlConnection sqlconn)
        {
            int i = 0;
            try
            {
                SqlCommand insert_command = new SqlCommand(str_sql, sqlconn);
                i = insert_command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                StreamWriter sw = getLogfile();
                sw.WriteLine("------------------------------------------------------------------");
                sw.WriteLine("Time of logging:" + DateTime.Now);
                sw.WriteLine("Exception come in Connection bean method doInsertUpdate");
                sw.WriteLine(ex.Message);
                sw.WriteLine("------------------------------------------------------------------");
                sw.Close();

            }
            return i;
        }

        public SqlDataReader getData(String str_sql, SqlConnection sqlconn)
        {
            SqlCommand selectcommand = sqlconn.CreateCommand();
            selectcommand.CommandText = str_sql;
            SqlDataReader selectreader = selectcommand.ExecuteReader();
            return selectreader;
        }
        public DataTable getDataTable(String str_sql, SqlConnection sqlconn)
        {
            SqlCommand selectedcommand = new SqlCommand();
            selectedcommand = sqlconn.CreateCommand();
            selectedcommand.CommandText = str_sql;
            selectedcommand.CommandType = CommandType.Text;
            DataTable dt = new DataTable("DataTable");
            SqlDataAdapter da = new SqlDataAdapter(selectedcommand);
            da.Fill(dt);
            return dt;
        }
        public void closeReader(SqlDataReader sqlreader)
        {

            sqlreader.Close();
            sqlreader.Dispose();
        }

        public void closeConn(SqlConnection sqlconn)
        {
            sqlconn.Close();
            sqlconn.Dispose();
        }
        public StreamWriter getLogfile()
        {
            string complete_path = "C:/iCORE-Logs/FXRR-Logs/";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            StreamWriter sw = File.AppendText(path);
            return sw;
        }
    }
}
