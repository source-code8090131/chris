using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using iCORE.Common;

/*****************************************************************
  Class Name:  ReportSQLManager 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Umair Mufti
  Created Date: 01-09-2010
  Version No: 1.0
  Modification: <Mention the modification in the class >
  Modified By:
  Modification Date:
  Version No: <The version No after modification>

*****************************************************************/

namespace iCORE.Common
{
    class ReportSQLManager : SQLManager
    {
        private static DataTable dtParams = new DataTable();
        private static XMS.DATAOBJECTS.ConnectionBean connbean = null;

        #region --Method--

        /// <summary>
        /// SetConnectionBean
        /// </summary>
        /// <param name="connbean_obj"></param>
        public static void SetConnectionBean(XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            if (connbean == null)
                connbean = connbean_obj;
        }

        /// <summary>
        /// GetConnectionBean
        /// </summary>
        /// <returns></returns>
        public static XMS.DATAOBJECTS.ConnectionBean GetConnectionBean()
        {
            return connbean;
        }

        /// <summary>
        /// SelectDataTable
        /// </summary>
        /// <param name="dtParams"></param>
        /// <returns></returns>
        public static Result SelectDataTable(DataTable dtParams)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                connbean = SQLManager.GetConnectionBean();
                conn = connbean.getDatabaseConnection();

                //for (int index = 0; index < dtParams.Columns.Count; index++)
                {
                    cmd = new SqlCommand(dtParams.TableName, conn);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    CreateCommandParameters(ref cmd, dtParams);
                    
                    adp = new SqlDataAdapter(cmd);
                    dst = new DataSet();

                    adp.Fill(dst, dtParams.TableName);

                    foreach (DataTable dtl in dst.Tables)
                    {
                        rslt.dstResult.Tables.Add(dtl.Copy());
                    }
                }
                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// CreateCommandParameters
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        private static void CreateCommandParameters(ref SqlCommand cmd, DataTable dtlParams)
        {
            CreateCommandParameters(ref cmd, dtlParams, 0);
        }

        /// <summary>
        /// CreateCommandParameters
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        /// <param name="rowIndex"></param>
        private static void CreateCommandParameters(ref SqlCommand cmd, DataTable dtlParams, int rowIndex)
        {
            int i = 0;
            SqlParameter parameter = null;

            try
            {
                cmd.Parameters.Clear();

                foreach (DataColumn dcl in dtlParams.Columns)
                {
                    parameter = new SqlParameter(dcl.ColumnName, GetDBType(dcl.DataType.ToString()));
                    parameter.Value = dtlParams.Rows[rowIndex][i];

                    if (Convert.ToBoolean(dcl.ExtendedProperties["IsOutParam"]))
                        parameter.Direction = ParameterDirection.InputOutput;
                    else
                        parameter.Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(parameter);
                    i++;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateCommandParameters", ex);
            }
        }

        /// <summary>
        /// GetDBType
        /// </summary>
        /// <param name="strSystemType"></param>
        /// <returns></returns>
        private static SqlDbType GetDBType(string strSystemType)
        {
            SqlDbType dbType = SqlDbType.VarChar;

            switch (strSystemType)
            {
                case "System.String":
                    dbType = SqlDbType.VarChar;
                    break;
                case "System.Int64":
                    dbType = SqlDbType.BigInt;
                    break;
                case "System.Int32":
                    dbType = SqlDbType.Int;
                    break;
                case "System.DateTime":
                    dbType = SqlDbType.DateTime;
                    break;
                case "System.Double":
                    dbType = SqlDbType.Decimal;
                    break;
                case "System.Boolean":
                    dbType = SqlDbType.Bit;
                    break;
                case "System.Byte[]":
                    dbType = SqlDbType.Binary;
                    break;
                case "System.Single":
                    dbType = SqlDbType.Float;
                    break;
                case "System.Int16":
                    dbType = SqlDbType.SmallInt;
                    break;
                case "System.Decimal":
                    dbType = SqlDbType.Decimal;
                    break;
            }

            return dbType;
        }

        #endregion
    }

}
