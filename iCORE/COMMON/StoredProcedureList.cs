using System;
using System.Collections.Generic;
using System.Text;

/*****************************************************************
  Class Name:  StoredProcedureList 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Shamraiz Ahmad
  Created Date: 22-08-2007
  Version No: 1.0
  Modification: Add code of all SP names                
  Modified By: Ambreen
  Modification Date:23-08-2007 
  Modification: Add code of dependencies GetAll                
  Modified By: Maria
  Modification Date:23-08-2007
  Modified By: Shamraiz Ahmad
  Modification Date:22-02-2008
  Version No: <The version No after modification>

*****************************************************************/

namespace iCORE.Common
{
    public class StoredProcedureList
    {
        public const string SP_PREFIX = "CIB_SP_";
                

        //public const string ACCOUNT_ADD = "Account_Add";
        //public const string ACCOUNT_UPDATE = "Account_Update";
        //public const string ACCOUNT_DELETE = "Account_Delete";
        //public const string ACCOUNT_GET = "Account_Get";
        //public const string ACCOUNT_GET_ALL = "ACCOUNT_GETALL";
        //public const string ACCOUNT_GETBYCODE = "ACCOUNT_GETBYCODE";
        //public const string ACCOUNT_ISDUPLICATE = "ACCOUNT_ISDUPLICATE";
        //public const string ACCOUNT_ISNAMEDUPLICATE = "ACCOUNT_ISNAMEDUPLICATE";
        
    }
}
