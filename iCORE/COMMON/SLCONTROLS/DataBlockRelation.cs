using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Text;

namespace iCORE.COMMON.SLCONTROLS
{
    /// <summary>
    /// 
    /// </summary>
    [Browsable(true)]
    [Serializable]
    [TypeConverter(typeof(DataBlockRelationConverter))]
    public class DataBlockRelation
    {
        [Browsable(true)]
        [Description("Gets or Sets the Panel name.")]
        [Category("RelationShip")]
        public SLPanel ChildPanel
        {
            get { return m_strPanelName; }
            set { m_strPanelName = value; }
        }
        private SLPanel m_strPanelName;

        //[Browsable(true)]
        //[Description("Gets or Sets the Relation Collection.")]
        //[Category("RelationShip")]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        //public List<DataBlocRelationMapper> Relation
        //{
        //    get
        //    { return m_lstRelation; }
        //    set
        //    { m_lstRelation = value; }

        //}
        //private List<DataBlocRelationMapper> m_lstRelation;
    }

    public class DataBlockRelationConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            if (destType == typeof(InstanceDescriptor))
                return true;
            return base.CanConvertTo(context, destType);
        }
        public override object ConvertTo(ITypeDescriptorContext context,
            System.Globalization.CultureInfo culture, object value, Type destType)
        {
            if (destType == typeof(InstanceDescriptor))
            {
                System.Reflection.ConstructorInfo ci =
                    typeof(DataBlockRelation).GetConstructor(
                    System.Type.EmptyTypes);
                return new InstanceDescriptor(ci, null, false);
            }
            return base.ConvertTo(context, culture, value, destType);
        }
    }

    public class DataBlocRelationMapper
    {
        [Browsable(true)]
        [Description("Gets or Sets the Source Item name.")]
        [Category("RelationShip")]
        public string SourceItem
        {
            get { return m_strSourceItem; }
            set { m_strSourceItem = value; }
        }
        private string m_strSourceItem;

        [Browsable(true)]
        [Description("Gets or Sets the Destination item name.")]
        [Category("RelationShip")]
        public string DestinationItem
        {
            get { return m_strDestinationItem; }
            set { m_strDestinationItem = value; }
        }
        private string m_strDestinationItem;
    }    
}

namespace iCORE.Common
{
    public class CustomAttributes : System.Attribute
    {
        private bool isForeignKey;
        public bool IsForeignKey
        {
            get { return isForeignKey; }
            set { isForeignKey = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


    }

    /*public class ModuleName : System.Attribute
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }*/

}
