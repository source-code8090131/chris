using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Data;
using CrplControlLibrary;
using System.Reflection;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.COMMON.SLCONTROLS
{
    public partial class SLPanelSimpleList : SLPanel
    {
        #region --Field Segment--
        DataTable m_oDT = new DataTable();
        private int m_currentRowIndex = 0;
        private BusinessEntity m_oEntityClass;
        public Mode operationMode = Mode.View;
        private bool m_IsValidPage = true;
        private string m_IsQueryMode;
        private IDataManager entityDataManager;
        private Cmn_TemplateCR oTemplate = new Cmn_TemplateCR();
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SLPanelSimpleList()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container"></param>
        public SLPanelSimpleList(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        /// <summary>
        /// Get or Set the panel data source
        /// </summary>
        public DataTable GridSource
        {
            get { return m_oDT; }
            set { m_oDT = value; }
        }

        /// <summary>
        /// Get or Set the current Row Index
        /// </summary>
        public int CurrentRowIndex
        {
            get { return m_currentRowIndex; }
            set { m_currentRowIndex = value; }
        }

        [Browsable(true)]
        [DefaultValue(true)]
        [Description("Confirms if form controls are validated successfully.")]
        public bool IsValidPage
        {
            get { return m_IsValidPage; }
        }
        
        /// <summary>
        /// Set Entity
        /// </summary>
        /// <param name="oDR"></param>
        public override void SetEntityObject(System.Data.DataRow oDR)
        {
            base.SetEntityObject(oDR);
        }

        /// <summary>
        /// Load Next Record
        /// </summary>
        /// <param name="ParentPanel"></param>
        public void LoadNextRecord(SLPanel ParentPanel)
        {
            BaseForm form = (BaseForm)this.FindForm();
            FillEntity(ParentPanel);
            FillNextRecord();
            base.LoadPanel(ParentPanel);
        }

        /// <summary>
        /// Load Previous Record
        /// </summary>
        /// <param name="ParentPanel"></param>
        public void LoadPreviousRecord(SLPanel ParentPanel)
        {
            BaseForm form = (BaseForm)this.FindForm();
            FillEntity(ParentPanel);
            FillPreviousRecord();
            base.LoadPanel(ParentPanel);
        }

        /// <summary>
        /// Load Panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        protected override void LoadPanel(SLPanel ParentPanel)
        {
            BaseForm form = (BaseForm)this.FindForm();
            FillEntity(ParentPanel);
            GetList(this, false);
            base.LoadPanel(ParentPanel);
        }

        /// <summary>
        /// Load Panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        /// <param name="oDR"></param>
        protected override void LoadPanel(SLPanel ParentPanel, DataRow oDR)
        {
            BaseForm oForm = (BaseForm)this.FindForm();
            oForm.SetPanelControlWithListItem(this.Controls, oDR);
        }

        /// <summary>
        /// Get List
        /// </summary>
        public void GetList(SLPanel oPanel, bool defaultLoad)
        {
            IDataManager dataManager = RuntimeClassLoader.GetDataManager("");
            Result rslt = new Result();

            rslt = dataManager.GetAll(this.CurrentBusinessEntity, this.SPName, "List");
            if (rslt.isSuccessful)
                m_oDT = rslt.dstResult.Tables[0];

            if (m_oDT != null && m_oDT.Rows.Count > 0)
            {
                // Clear datatable in order to display empty rows.
                if (defaultLoad)
                {
                    m_oDT.Clear();
                    m_oDT.AcceptChanges();
                }
                else
                {
                    BaseForm oForm = (BaseForm)this.FindForm();
                    oForm.SetPanelControlWithListItem(this.Controls, m_oDT.Rows[0]);
                    this.LoadConcurrentPanels(m_oDT.Rows[0]);
                    this.SetEntityObject(m_oDT.Rows[0]);
                    this.LoadDependentPanels();
                    m_currentRowIndex = 0;
                }
            }
            else
            {
                (this.FindForm() as BaseForm).ClearForm(this.Controls);
                this.ClearPanel(this.Parent as SLPanel);
                this.ClearConcurrentPanels();
                this.ClearDependentPanels();
            }
        }

        /// <summary>
        /// Get Query Record
        /// </summary>
        public void GetList(SLPanel oPanel, string actionType)
        {
            IDataManager dataManager = RuntimeClassLoader.GetDataManager("");
            Result rslt = new Result();

            rslt = dataManager.GetAll(this.CurrentBusinessEntity, this.SPName, actionType);
            if (rslt.isSuccessful)
                m_oDT = rslt.dstResult.Tables[0];

            if (m_oDT != null && m_oDT.Rows.Count > 0)
            {
                BaseForm oForm = (BaseForm)this.FindForm();
                oForm.SetPanelControlWithListItem(this.Controls, m_oDT.Rows[0]);
                this.LoadConcurrentPanels(m_oDT.Rows[0]);
                this.SetEntityObject(m_oDT.Rows[0]);
                this.LoadDependentPanels();
                m_currentRowIndex = 0;
            }
            else
            {
                (this.FindForm() as BaseForm).ClearForm(this.Controls);
                this.ClearPanel(this.Parent as SLPanel);
                this.ClearConcurrentPanels();
                this.ClearDependentPanels();
            }
        }

        /// <summary>
        /// Fill Next Record
        /// </summary>
        public void FillNextRecord()
        {
            BaseForm oForm = (BaseForm)this.FindForm();
            RemoveEmptyRows();
            if (m_oDT != null && m_oDT.Rows.Count > 0)
            {
                m_currentRowIndex = m_currentRowIndex + 1;
                if (m_currentRowIndex <= 0)
                    m_currentRowIndex = 1;

                if (m_currentRowIndex < m_oDT.Rows.Count && m_oDT.Rows.Count > 1)
                {
                    oForm.SetPanelControlWithListItem(this.Controls, m_oDT.Rows[m_currentRowIndex]);
                    this.LoadConcurrentPanels(m_oDT.Rows[m_currentRowIndex]);
                    this.SetEntityObject(m_oDT.Rows[m_currentRowIndex]);
                }
                else
                {
                    m_currentRowIndex = m_oDT.Rows.Count - 1;
                    oForm.SetPanelControlWithListItem(this.Controls, m_oDT.Rows[m_currentRowIndex]);
                    this.LoadConcurrentPanels(m_oDT.Rows[m_currentRowIndex]);
                    this.SetEntityObject(m_oDT.Rows[m_currentRowIndex]);
                    oForm.ShowInformationMessage("", ApplicationMessages.ATLASTRECORD);

                    if (this.EnableInsert == true)
                    {
                        if (MessageBox.Show("Do you want to insert new Record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            (this.FindForm() as BaseForm).ClearForm(this.Controls);
                            this.ClearBusinessEntity();
                            this.ClearConcurrentPanels();
                            this.ClearDependentPanels();
                            if (m_oDT.Rows[m_oDT.Rows.Count - 1]["ID"].ToString() != string.Empty)
                            {
                                DataRow drow = m_oDT.NewRow();
                                m_oDT.Rows.Add(drow);
                            }
                        }
                    }

                    m_currentRowIndex = m_oDT.Rows.Count - 1;
                }
            }
            else
            {
                //MessageBox.Show("No Record Found.. Please Insert Record", "Information",
                //                                                         MessageBoxButtons.OK,
                //                                                         MessageBoxIcon.Information);
                oForm.ShowInformationMessage("", ApplicationMessages.ATLASTRECORD);
                m_currentRowIndex = 0;
            }
        }

        /// <summary>
        /// Fill Previous Record
        /// </summary>
        public void FillPreviousRecord()
        {
            BaseForm oForm = (BaseForm)this.FindForm();
            RemoveEmptyRows();
            if (m_oDT != null && m_oDT.Rows.Count > 0)
            {
                m_currentRowIndex = m_currentRowIndex - 1;
                if (m_currentRowIndex >= m_oDT.Rows.Count - 1)
                    m_currentRowIndex = m_oDT.Rows.Count - 1;

                if (m_currentRowIndex >= 0)
                {
                    oForm.SetPanelControlWithListItem(this.Controls, m_oDT.Rows[m_currentRowIndex]);
                    this.LoadConcurrentPanels(m_oDT.Rows[m_currentRowIndex]);
                    this.SetEntityObject(m_oDT.Rows[m_currentRowIndex]);
                }
                else
                {
                    m_currentRowIndex = 0;
                    oForm.SetPanelControlWithListItem(this.Controls, m_oDT.Rows[m_currentRowIndex]);
                    this.LoadConcurrentPanels(m_oDT.Rows[m_currentRowIndex]);
                    this.SetEntityObject(m_oDT.Rows[m_currentRowIndex]);
                    oForm.ShowInformationMessage("", ApplicationMessages.ATFIRSTRECORD);
                    if (this.EnableInsert == true)
                    {
                        if (MessageBox.Show("Do you want to insert new Record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            (this.FindForm() as BaseForm).ClearForm(this.Controls);
                            this.ClearBusinessEntity();
                            this.ClearConcurrentPanels();
                            this.ClearDependentPanels();
                            if (m_oDT.Rows[m_oDT.Rows.Count - 1]["ID"].ToString() != string.Empty)
                            {
                                DataRow drow = m_oDT.NewRow();
                                m_oDT.Rows.InsertAt(drow, 0);//Add(drow);
                            }
                        }
                    }

                    m_currentRowIndex = 0;
                }
            }
            else
            {
                //MessageBox.Show("No Record Found.. Please Insert Record", "Information",
                //                                                         MessageBoxButtons.OK,
                //                                                         MessageBoxIcon.Information);
                oForm.ShowInformationMessage("", ApplicationMessages.ATFIRSTRECORD);
                m_currentRowIndex = 0;
            }
        }

        /// <summary>
        /// Remove Empty Rows from datatable
        /// </summary>
        private void RemoveEmptyRows()
        {
            if (m_oDT.Rows.Count > 0)
            {
                foreach (DataRow drw in m_oDT.Rows)
                {
                    if (drw.RowState == DataRowState.Added)
                    {
                        m_oDT.Rows.Remove(drw);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Set Screen Mode For Simple panel List on Master Detail Screen
        /// </summary>
        /// <param name="oPanel"></param>
        protected virtual void SetRecordMode(SLPanel oPanel)
        {
            System.Reflection.PropertyInfo newProperty;
            newProperty = oPanel.CurrentBusinessEntity.GetType().GetProperty("ID");
            if (newProperty != null && newProperty.CanRead)
            {
                Int32 ID;
                Int32.TryParse(newProperty.GetValue(oPanel.CurrentBusinessEntity, null).ToString(), out ID);
                if (ID > 0)
                    this.operationMode = Mode.Edit;
                else
                    this.operationMode = Mode.Add;
            }
        }

        /// <summary>
        /// Save Operation
        /// </summary>
        /// <param name="oPanel"></param>
        /// <returns></returns>
        public Boolean SaveRecords(SLPanel oPanel)
        {
            BaseForm oBaseForm = (BaseForm)this.FindForm();
            Boolean rslt = false;
            DataView oDvChangeSet = null;
            Result recordsInserted;
            recordsInserted.exp = null;
            recordsInserted.hstOutParams = null;
            recordsInserted.objResult = null;
            m_oEntityClass = oPanel.CurrentBusinessEntity;
            SetRecordMode(oPanel);
            if (this.operationMode == Mode.Add)
            {
                if (m_oDT.Rows.Count < 1)
                {
                    DataRow drow = m_oDT.NewRow();
                    m_oDT.Rows.Add(drow);
                    CurrentRowIndex = 0;
                }
                GetValuesFromPanel(oPanel.Controls);
                GetValuesFromConcurentPanel(oPanel);
                oDvChangeSet = m_oDT.DefaultView;
                oDvChangeSet.RowStateFilter = DataViewRowState.Added | DataViewRowState.ModifiedCurrent;
                if (m_IsValidPage)
                {
                    for (int indexer = 0; indexer < oDvChangeSet.Count; indexer++)
                    {
                        DataRow row = oDvChangeSet[indexer].Row;
                        foreach (PropertyInfo property in (this as SLPanel).CurrentBusinessEntity.GetType().GetProperties())
                        {
                            object[] customAttributes = property.GetCustomAttributes(typeof(CustomAttributes), false);
                            foreach (object attribute in customAttributes)
                            {
                                CustomAttributes storageAttributes = (CustomAttributes)attribute;
                                if (storageAttributes.IsForeignKey)
                                {
                                    row[property.Name] = property.GetValue((this as SLPanel).CurrentBusinessEntity, null);
                                    Control cntrl = Utilities.FindMasterPanel(oBaseForm, oPanel.Name);
                                    if (cntrl is SLPanel)
                                    {
                                        if ((cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name) != null)
                                            row[property.Name] = (cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name).GetValue((cntrl as SLPanel).CurrentBusinessEntity, null);
                                        if (m_oDT.Columns.Contains(property.Name))
                                            m_oDT.Rows[m_currentRowIndex][property.Name] = row[property.Name];
                                    }
                                }
                            }
                        }
                        if (row.Table.Columns.Contains("SOEID"))
                        {
                            oPanel.CurrentBusinessEntity.SoeId = (this.FindForm().MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                            m_oDT.Rows[m_currentRowIndex]["SOEID"] = oPanel.CurrentBusinessEntity.SoeId;
                        }
                    }
                }
                entityDataManager = RuntimeClassLoader.GetDataManager("");
                recordsInserted = entityDataManager.BatchUpdate(m_oEntityClass, oPanel.SPName, m_oDT);
            }
            else if (this.operationMode == Mode.Edit)
            {
                if (m_IsValidPage)
                {
                    if (m_oDT != null && m_oDT.DataSet != null)
                    {
                        GetValuesFromPanel(oPanel.Controls);
                        GetValuesFromConcurentPanel(oPanel);
                        if (m_oDT.DataSet.HasChanges() && m_oDT.GetChanges() != null)
                        {
                            oDvChangeSet = m_oDT.DefaultView;
                            oDvChangeSet.RowStateFilter = DataViewRowState.ModifiedCurrent;
                            for (int indexer = 0; indexer < oDvChangeSet.Count; indexer++)
                            {
                                DataRow row = oDvChangeSet[indexer].Row;
                                foreach (PropertyInfo property in (this as SLPanel).CurrentBusinessEntity.GetType().GetProperties())
                                {
                                    object[] customAttributes = property.GetCustomAttributes(typeof(CustomAttributes), false);
                                    foreach (object attribute in customAttributes)
                                    {
                                        CustomAttributes storageAttributes = (CustomAttributes)attribute;
                                        if (storageAttributes.IsForeignKey)
                                        {
                                            row[property.Name] = property.GetValue((this as SLPanel).CurrentBusinessEntity, null);
                                            Control cntrl = Utilities.FindMasterPanel(oBaseForm, oPanel.Name);
                                            if (cntrl is SLPanel)
                                            {
                                                if ((cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name) != null)
                                                    row[property.Name] = (cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name).GetValue((cntrl as SLPanel).CurrentBusinessEntity, null);
                                                if (m_oDT.Columns.Contains(property.Name))
                                                    m_oDT.Rows[m_currentRowIndex][property.Name] = row[property.Name];
                                            }
                                        }
                                    }
                                }
                                if (row.Table.Columns.Contains("SOEID"))
                                {
                                    oPanel.CurrentBusinessEntity.SoeId = (this.FindForm().MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                                    m_oDT.Rows[m_currentRowIndex]["SOEID"] = oPanel.CurrentBusinessEntity.SoeId;
                                }
                            }
                            entityDataManager = RuntimeClassLoader.GetDataManager("");
                            recordsInserted = entityDataManager.BatchUpdate(m_oEntityClass, oPanel.SPName, m_oDT);
                        }
                    }
                }
            }
            if (m_IsValidPage)
            {
                if (Convert.ToInt32(recordsInserted.objResult) > 0)
                {
                    m_oDT.AcceptChanges();
                    this.SetPanelEntity(m_oDT);
                    oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                    this.BindingContext[m_oDT].EndCurrentEdit();
                    rslt = true;
                    if (Convert.ToInt32(recordsInserted.objResult) > 0)
                        oBaseForm.SetMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Number of records affected : " + recordsInserted.objResult.ToString(), String.Empty, MessageType.Information);
                }

                else if ((recordsInserted.exp is System.Data.SqlClient.SqlException) && (recordsInserted.exp as System.Data.SqlClient.SqlException).Number == 50000)
                {
                    (this.FindForm() as BaseForm).LogError("SaveRecords", recordsInserted.exp);
                    oBaseForm.SetMessage(String.Empty, recordsInserted.exp.Message, String.Empty, MessageType.Error);
                    oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                    this.BindingContext[m_oDT].EndCurrentEdit();
                }
                else if (recordsInserted.exp != null)
                {
                    oBaseForm.LogError("SaveRecords", recordsInserted.exp);
                    oBaseForm.SetMessage(String.Empty, "Error in updating records.", String.Empty, MessageType.Error);
                    oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                    this.BindingContext[m_oDT].EndCurrentEdit();
                }
            }
            return rslt;
        }

        /// <summary>
        /// Delete Operation
        /// </summary>
        /// <param name="oPanel"></param>
        /// <returns></returns>
        public void DeleteRecords(SLPanel oPanel)
        {
            BaseForm oBaseForm = (BaseForm)this.FindForm();
            Result rslt;
            m_oEntityClass = oPanel.CurrentBusinessEntity;
            if (m_oDT != null)
            {
                if (m_oDT.Columns.Contains("ID") && m_oDT.Rows.Count > 0)
                {
                    if (m_oDT.Rows[m_currentRowIndex]["ID"] != null && m_oDT.Rows[m_currentRowIndex]["ID"].ToString() != string.Empty)
                    {
                        if (Convert.ToInt32(m_oDT.Rows[m_currentRowIndex]["ID"]) > 0)
                        {
                            GetValuesFromPanel(oPanel.Controls);
                            GetValuesFromConcurentPanel(oPanel);
                            entityDataManager = RuntimeClassLoader.GetDataManager("");
                            rslt = entityDataManager.Delete(oPanel.CurrentBusinessEntity, oPanel.SPName, "Delete");
                            if (rslt.isSuccessful)
                            {
                                oBaseForm.SetMessage("Record ", ApplicationMessages.DELETE_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                                m_oDT.Rows.Remove(m_oDT.Rows[m_currentRowIndex]);
                                m_oDT.AcceptChanges();
                            }
                            else if (rslt.exp.Message.Equals(ApplicationMessages.DBMessages.DeleteRecord.GetHashCode().ToString()) == true)
                                oBaseForm.SetMessage("", ApplicationMessages.DELETE_MASTER_RECORD, String.Empty, MessageType.Error);
                            else
                                oBaseForm.SetMessage("Record ", ApplicationMessages.DELETE_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set Entity Property
        /// </summary>
        /// <param name="pstrName"></param>
        /// <param name="pstrValue"></param>
        protected void SetPanelEntity(string pstrName, string pstrValue)
        {
            if (m_oDT != null)
            {
                if (m_oDT.Columns.Contains(pstrName))
                {
                    m_oEntityClass = this.CurrentBusinessEntity;
                    PropertyInfo newProperty = this.CurrentBusinessEntity.GetType().GetProperty(pstrName);
                    string TypeName;
                    if (newProperty != null && newProperty.CanWrite)
                    {
                        TypeName = newProperty.PropertyType.Name;
                        if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            TypeName = newProperty.PropertyType.GetGenericArguments()[0].Name;
                        }
                        switch (TypeName)
                        {
                            case "DateTime":
                                DateTime oDateTime;
                                if (pstrValue.Equals("") || Convert.ToDateTime(pstrValue) == DateTime.MinValue)
                                    newProperty.SetValue(m_oEntityClass, null, null);
                                else if (DateTime.TryParse(pstrValue, out oDateTime))
                                {
                                    newProperty.SetValue(m_oEntityClass, Convert.ToDateTime(pstrValue), null);
                                    m_oDT.Rows[m_currentRowIndex][pstrName] = pstrValue;
                                }
                                break;
                            case "Int32":
                                Int32 oInt32;
                                if (Int32.TryParse(pstrValue, out oInt32))
                                {
                                    newProperty.SetValue(m_oEntityClass, Convert.ToInt32(pstrValue), null);
                                    m_oDT.Rows[m_currentRowIndex][pstrName] = pstrValue;
                                }
                                break;
                            case "Double":
                                Double ValueDoubleType;
                                if (Double.TryParse(pstrValue, out ValueDoubleType))
                                {
                                    newProperty.SetValue(m_oEntityClass, Convert.ToDouble(pstrValue), null);
                                    m_oDT.Rows[m_currentRowIndex][pstrName] = pstrValue;
                                }
                                break;
                            case "Decimal":
                                Decimal ValueDecimalType;
                                if (Decimal.TryParse(pstrValue, out ValueDecimalType))
                                {
                                    newProperty.SetValue(m_oEntityClass, Convert.ToDecimal(pstrValue), null);
                                    m_oDT.Rows[m_currentRowIndex][pstrName] = pstrValue;
                                }
                                else if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                                {
                                    newProperty.SetValue(m_oEntityClass, null, null);
                                    m_oDT.Rows[m_currentRowIndex][pstrName] = DBNull.Value;
                                }
                                break;
                            default:
                                newProperty.SetValue(m_oEntityClass, pstrValue, null);
                                m_oDT.Rows[m_currentRowIndex][pstrName] = pstrValue;
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set Entity Property from Datatable
        /// </summary>
        /// <param name="pstrName"></param>
        /// <param name="pstrValue"></param>
        protected void SetPanelEntity(DataTable dtl)
        {
            string pstrValue;
            string pstrName;
            if (dtl != null)
            {
                foreach (DataColumn dc in dtl.Columns)
                {
                    pstrValue = m_oDT.Rows[m_currentRowIndex][dc.ColumnName].ToString();
                    pstrName = dc.ColumnName.ToString();
                    PropertyInfo newProperty = m_oEntityClass.GetType().GetProperty(pstrName);
                    string TypeName;
                    if (newProperty != null && newProperty.CanWrite)
                    {
                        TypeName = newProperty.PropertyType.Name;
                        if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            TypeName = newProperty.PropertyType.GetGenericArguments()[0].Name;
                        }
                        switch (TypeName)
                        {
                            case "DateTime":
                                DateTime oDateTime;
                                if (pstrValue.Equals("") || Convert.ToDateTime(pstrValue) == DateTime.MinValue)
                                    newProperty.SetValue(m_oEntityClass, null, null);
                                else if (DateTime.TryParse(pstrValue, out oDateTime))
                                    newProperty.SetValue(m_oEntityClass, Convert.ToDateTime(pstrValue), null);

                                break;
                            case "Int32":
                                Int32 oInt32;
                                if (Int32.TryParse(pstrValue, out oInt32))
                                    newProperty.SetValue(m_oEntityClass, Convert.ToInt32(pstrValue), null);

                                break;
                            case "Double":
                                Double ValueDoubleType;
                                if (Double.TryParse(pstrValue, out ValueDoubleType))
                                    newProperty.SetValue(m_oEntityClass, Convert.ToDouble(pstrValue), null);

                                break;
                            case "Decimal":
                                Decimal ValueDecimalType;
                                if (Decimal.TryParse(pstrValue, out ValueDecimalType))
                                    newProperty.SetValue(m_oEntityClass, Convert.ToDecimal(pstrValue), null);

                                break;
                            default:
                                newProperty.SetValue(m_oEntityClass, pstrValue, null);

                                break;
                        }
                    }
                }

            }


        }

        /// <summary>
        /// 
        /// Get Values From Panel Controls,In Order To Fill Panel Entity. 
        /// </summary>
        /// <param name="oCtrls"></param>
        public void GetValuesFromPanel(Control.ControlCollection oCtrls)
        {
            BaseForm oBaseForm = (BaseForm)this.FindForm();
            foreach (Control oLoopControl in oCtrls)
            {
                if (oLoopControl is GroupBox)
                {
                    foreach (Control innerLoopControl in oLoopControl.Controls)
                    {
                        if (innerLoopControl is SLTextBox)
                        {
                            if (((SLTextBox)innerLoopControl).AssociatedLookUpName.Equals(string.Empty) == false)
                            {
                                // BaseForm_Leave(oLoopControl, null);
                            }
                            if (((SLTextBox)innerLoopControl).IsRequired)
                            {
                                if (InputValidator.IsNotEmpty("", oBaseForm.errorProvider1, (SLTextBox)innerLoopControl))
                                {
                                    SetPanelEntity(((SLTextBox)innerLoopControl).DataFieldMapping, innerLoopControl.Text);

                                }
                                else
                                {
                                    this.m_IsValidPage = false;
                                }
                            }
                            else
                            {
                                SetPanelEntity(((SLTextBox)innerLoopControl).DataFieldMapping, innerLoopControl.Text);
                            }
                        }
                        else if (innerLoopControl is SLComboBox)
                        {
                            SetPanelEntity(((SLComboBox)innerLoopControl).DataFieldMapping, ((SLComboBox)innerLoopControl).SelectedValue == null ? DBNull.Value.ToString() : ((SLComboBox)innerLoopControl).SelectedValue.ToString());
                        }
                        else if (innerLoopControl is SLCheckBox)
                        {
                            if (((SLCheckBox)oLoopControl).Checked)
                                SetPanelEntity(((SLCheckBox)innerLoopControl).DataFieldMapping, "Y");
                            else
                                SetPanelEntity(((SLCheckBox)innerLoopControl).DataFieldMapping, "N");
                        }
                        else if (innerLoopControl is SLRadioButton)
                        {
                            SetPanelEntity(((SLRadioButton)innerLoopControl).DataFieldMapping, ((SLRadioButton)innerLoopControl).Checked ? "1" : "0");
                        }
                        else if (innerLoopControl is SLDatePicker)
                        {
                            SetPanelEntity(((SLDatePicker)innerLoopControl).DataFieldMapping, ((SLDatePicker)innerLoopControl).Value == null ? "" : ((SLDatePicker)innerLoopControl).Value.ToString());
                        }
                    }
                }
                if (oLoopControl is SLTextBox)
                {
                    if (((SLTextBox)oLoopControl).AssociatedLookUpName.Equals(string.Empty) == false)
                    {
                        // BaseForm_Leave(oLoopControl, null);
                    }
                    if (((SLTextBox)oLoopControl).IsRequired)
                    {
                        if (InputValidator.IsNotEmpty("", oBaseForm.errorProvider1, (SLTextBox)oLoopControl))
                        {
                            SetPanelEntity(((SLTextBox)oLoopControl).DataFieldMapping, oLoopControl.Text);

                        }
                        else
                        {
                            this.m_IsValidPage = false;
                        }
                    }
                    else
                    {
                        SetPanelEntity(((SLTextBox)oLoopControl).DataFieldMapping, oLoopControl.Text);
                    }
                }
                else if (oLoopControl is SLComboBox)
                {
                    SetPanelEntity(((SLComboBox)oLoopControl).DataFieldMapping, ((SLComboBox)oLoopControl).SelectedValue == null ? DBNull.Value.ToString() : ((SLComboBox)oLoopControl).SelectedValue.ToString());
                }
                else if (oLoopControl is SLCheckBox)
                {
                    if (((SLCheckBox)oLoopControl).Checked)
                        SetPanelEntity(((SLCheckBox)oLoopControl).DataFieldMapping, "Y");
                    else
                        SetPanelEntity(((SLCheckBox)oLoopControl).DataFieldMapping, "N");
                }
                else if (oLoopControl is SLRadioButton)
                {
                    SetPanelEntity(((SLRadioButton)oLoopControl).DataFieldMapping, ((SLRadioButton)oLoopControl).Checked ? "1" : "0");
                }
                else if (oLoopControl is SLDatePicker)
                {
                    if (((SLDatePicker)oLoopControl).IsRequired)
                    {
                        if (InputValidator.IsNotEmpty("", oBaseForm.errorProvider1, (SLDatePicker)oLoopControl))
                        {
                            SetPanelEntity(((SLDatePicker)oLoopControl).DataFieldMapping, ((SLDatePicker)oLoopControl).Value == null ? "" : ((SLDatePicker)oLoopControl).Value.ToString());
                        }
                        else
                        {
                            this.m_IsValidPage = false;
                        }
                    }
                    else
                        SetPanelEntity(((SLDatePicker)oLoopControl).DataFieldMapping, ((SLDatePicker)oLoopControl).Value == null ? "" : ((SLDatePicker)oLoopControl).Value.ToString());
                }
            }
        }

        /// <summary>
        /// 
        /// Get Values From Concurent Panel Controls,In Order To Fill Panel Entity. 
        /// </summary>
        /// <param name="oCtrls"></param>
        public void GetValuesFromConcurentPanel(SLPanel oPanel)
        {
            if (oPanel.ConcurrentPanels != null && oPanel.ConcurrentPanels.Count > 0)
            {
                foreach (SLPanel parallelPanel in oPanel.ConcurrentPanels)
                {
                    GetValuesFromPanel(parallelPanel.Controls);
                }
            }
        }

        ///// <summary>
        ///// Return Dirty Data State
        ///// </summary>
        ///// <returns></returns>
        //public override bool HasChanges()
        //{
        //    Boolean hasChanges = false;
        //    hasChanges = HasChanges(this.Controls, hasChanges);
        //    return hasChanges;
        //}

        ///// <summary>
        ///// Reset Changes
        ///// </summary>
        //public override void ResetPanelChanges()
        //{
        //    ResetPanelChanges(this.Controls);
        //}

        ///// <summary>
        ///// Find changes in Panel and its child panels
        ///// </summary>
        ///// <param name="oCtrls"></param>
        ///// <param name="hasChanges"></param>
        ///// <returns></returns>
        //private bool HasChanges(Control.ControlCollection oCtrls, bool hasChanges)
        //{
        //    if (this.GridSource.DataSet.HasChanges())
        //        return true;
        //    if (this.PanelBlockType == BlockType.DataBlock)
        //    {
        //        foreach (Control oLoopControl in oCtrls)
        //        {
        //            switch (oLoopControl.GetType().Name.ToString())
        //            {
        //                case "GroupBox":
        //                    hasChanges = HasChanges(oLoopControl.Controls, hasChanges);
        //                    break;
        //                case "TabControl":
        //                    hasChanges = HasChanges(oLoopControl.Controls, hasChanges);
        //                    break;
        //                case "SLPanelSimple":
        //                    hasChanges = HasChanges(oLoopControl.Controls, hasChanges);
        //                    break;
        //                case "SLTextBox":
        //                    hasChanges = (oLoopControl as SLTextBox).HasChanges || hasChanges;
        //                    break;
        //                case "SLComboBox":
        //                    hasChanges = (oLoopControl as SLComboBox).HasChanges || hasChanges;
        //                    break;
        //                case "SLCheckBox":
        //                    hasChanges = (oLoopControl as SLCheckBox).HasChanges || hasChanges;
        //                    break;
        //                case "SLRadioButton":
        //                    hasChanges = (oLoopControl as SLRadioButton).HasChanges || hasChanges;
        //                    break;
        //                case "SLDatePicker":
        //                    hasChanges = (oLoopControl as SLDatePicker).HasChanges || hasChanges;
        //                    break;
        //            }

        //            if (hasChanges)
        //                return hasChanges;
        //        }
        //    }
        //    if (this.ConcurrentPanels != null)
        //    {
        //        foreach (SLPanel childPanel in this.ConcurrentPanels)
        //        {
        //            hasChanges = hasChanges || childPanel.HasChanges();
        //            if (hasChanges)
        //                return hasChanges;
        //        }
        //    }
        //    if (this.DependentPanels != null)
        //    {
        //        foreach (SLPanel childPanel in this.DependentPanels)
        //        {
        //            hasChanges = hasChanges || childPanel.HasChanges();
        //            if (hasChanges)
        //                return hasChanges;
        //        }
        //    }            
        //    return hasChanges;
        //}

        /////// <summary>
        /////// Rest Changes in Panel
        /////// </summary>
        /////// <param name="oCtrls"></param>
        ////public void ResetPanelChanges(Control.ControlCollection oCtrls)
        ////{
        ////    foreach (Control oLoopControl in oCtrls)
        ////    {
        ////        switch (oLoopControl.GetType().Name.ToString())
        ////        {
        ////            case "GroupBox":
        ////                ResetPanelChanges(oLoopControl.Controls);
        ////                break;
        ////            case "SLTextBox":
        ////                ((SLTextBox)oLoopControl).HasChanges = false;
        ////                break;
        ////            case "SLComboBox":
        ////                ((SLComboBox)oLoopControl).HasChanges = false;
        ////                break;
        ////            case "SLCheckBox":
        ////                ((SLCheckBox)oLoopControl).HasChanges = false;
        ////                break;
        ////            case "SLRadioButton":
        ////                ((SLRadioButton)oLoopControl).HasChanges = false;
        ////                break;
        ////            case "SLDatePicker":
        ////                ((SLDatePicker)oLoopControl).HasChanges = false;
        ////                break;
        ////        }
        ////    }
        ////    if (this.ConcurrentPanels != null)
        ////    {
        ////        foreach (SLPanel childPanel in this.ConcurrentPanels)
        ////        {
        ////            childPanel.ResetPanelChanges();
        ////        }
        ////    }
        ////    if (this.DependentPanels != null)
        ////    {
        ////        foreach (SLPanel childPanel in this.DependentPanels)
        ////        {
        ////            childPanel.ResetPanelChanges();
        ////        }
        ////    }            
      //  }
    //
    }
}
