using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.Common
{
    /// <summary>
    /// Utility functions
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Find Parent SL Panel
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public static Control FindParentSLPanel(Control root)
        {
            Control current = root;
            while (!(current is System.Windows.Forms.Form))
            {
                if (current == null)
                    break;
                if (current.GetType().BaseType.Equals(typeof(SLPanel)))
                    return current;
                current = current.Parent;
            }
            return null;
        }

        /// <summary>
        /// Recursive find control method
        /// </summary>
        /// <param name="cntrl"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control cntrl, string name)
        {
            if (cntrl.Name.Equals(name,StringComparison.CurrentCultureIgnoreCase))
                return cntrl;

            foreach (Control c in cntrl.Controls)
            {
                Control tmp = FindControlRecursive(c, name);
                if (tmp != null)
                    return tmp;
            }
            return null;
        }
        
        /// <summary>
        /// Find Master Panel
        /// </summary>
        /// <param name="root"></param>
        /// <param name="dependentPanelName"></param>
        /// <returns></returns>
        public static Control FindMasterPanel(Control root, string dependentPanelName)
        {
            if (root is SLPanel)
            {
                if((root as SLPanel).DependentPanels != null)
                foreach (SLPanel panel in (root as SLPanel).DependentPanels)
                {
                    if (panel.Name == dependentPanelName)
                        return root;
                }                
            }

            foreach (Control c in root.Controls)
            {
                Control tmp = FindMasterPanel(c, dependentPanelName);
                if (tmp != null)
                    return tmp;
            }
            return null;
        }

        /// <summary>
        /// FinalSettlementPanel
        /// </summary>
        /// <param name="root"></param>
        /// <param name="FinalSettlementPanel"></param>
        /// <returns></returns>
        public static string FinalSettlementPanel(string PF_YEAR_WISE, string PF_YEAR_DETAIL, double d_PF_SUM, string AmountInWords, string disINFO_SETTLE, int PRNO,double Upto_Dec_Last_Two_Year, double Upto_Dec_Last_One_Year)
        {
            string html = "<!DOCTYPE html>"
+ "<html>"
+ "<head>"
+ "<title></title>"
 + "</head>"
 + "<body>"
 + "<p style='margin-left:35px;font-weight:bold;font-size:24px;padding-top:30px;'>TRUSTEES CITIBANK N.A.EMPLOYEE�S PROVIDENT FUND <br><br> PROVIDENT FUND</p>"
+ "<p style='margin-left:35px;font-weight:bold;font-size:24px;padding-top:5px;'>Employee <italic>#</italic>" + PRNO + "</p>"
+ "<table style='width:70%; border: 1px solid black;margin-left:35px; border-collapse: collapse;'>"
   + "<tr Style='border: 1px solid black;'>"
     + "<th Style='border: 1px solid black;font-size:20px;'>PERIOD </th>"
      + "<th Style='border: 1px solid black;font-size:20px;'>EMPLOYEE </th>"
       + "<th Style='border: 1px solid black;font-size:20px;'>BANK </th>"
          + "<th Style='border: 1px solid black;font-size:20px;'>INTEREST </th>"
             + "<th Style='border: 1px solid black;font-size:20px;'>BALANCE </th>"
        + "</tr>" +
      
            PF_YEAR_WISE
                            + "</table>"

                            + "<div style='width:50%;margin-left: 1132px;margin-top: -123px;margin-right: 0px;'>"
                            + "<p Style='border-bottom:solid black; font-size:20px; text-align:right;margin-left:74px;margin-right:600px;'><B>Upto DEC "+ (DateTime.Now.Year -2 )  + " </B> <br>" + string.Format("{0:#,##0.00}", Upto_Dec_Last_Two_Year) + " </p>"
                                   + "<p Style='border-bottom:solid black; font-size:20px; text-align:right;margin-left:74px;margin-right:589px;margin-top:5px;'><B>Upto DEC " + (DateTime.Now.Year - 1) + " </B><br>" + string.Format("{0:#,##0.00}", Upto_Dec_Last_One_Year) + "</p>"
                                          + "</div>" +


   PF_YEAR_DETAIL
   
    + "<div Style='width:100%;margin-top:10px;margin-bottom:-20px; margin-left:25px;margin-right:70px;'>"
    + "<h3 Style='font-size:24px;text-align:center;'>UNDERTAKING </h3>"
    + "<p Style='font-size:18px;text-align:left;'>I acknowledge receipt of <B>PKR "+ d_PF_SUM + "� (Rupees: " + AmountInWords + " Only) </B>  in full and final settlement of all my dues with �The Trustees Citibank Pakistan Provident Fund' </P>"
+ "<p Style='font-size:18px;text-align:left;;margin-top: 52px;'>2.5 % Zakat deduction on own contribution = &#160;&#160;&#160;&#160;&#160;&#160;&#160; - <br><B> (CZ - 50) Provided </B></p>"
    + "</div>"
    + "<div Style='width:30%;margin-top:10px;margin-bottom:-20px; margin-left:620px;height:150px;'>"
    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:10px; margin-right:30px;margin-top:20px;'> </div>"
     + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:0px;'> </div>"
     + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:18px;text-align:left;'>Trustee Approval </p>"
      + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:50px;'> </div>"
      + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:18px;text-align:left;'>Checker</p>"
         + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:20px; margin-right:30px;margin-top:-38px;'></div>"
        + "<p Style='width:150px;height=20px;margin-left:20px; margin-right:30px;margin-top:10px;font-size:18px;text-align:left;'>Maker</p>"
        + "</div>" +


//        + "<div Style='margin-left:25px;margin-top:30px;width:80%;'>"
//        + "<h1 Style='font-size:12px;text-align:left;margin-left:50px;margin-top:20px;'>FINAL DUES SETTLEMENT</h1>"
//         + "<p Style='font-size:12px;text-align:left;margin-left:50px;margin-top:20px;'> <B>Employee # 3587 <br> Level: Assistant Manager <br> ISL - CMGCC </B>"
//+ "</p>"
//+ "<table style='width:45%;margin-left:45px; border-collapse: collapse;margin-top:10px;'>"
//+ "<tr>"
//+ "<td Style='font-size:12px;width:0px;'>Date of Joining <br> Date of Leaving + <br> Years of Service <br> Annual Package </td> "
//   + "<td Style='font-size:12px;width:0px;'> = <br> = <br> = <br> = </td>"
//   + "<td Style='font-size:12px;width:0px;'>July 17, 2007 <br>September 30, 2020 + <br>13.22 <br>1,559,472 </td>"
//          + "</tr>"
//          + "</table>"
//          + "<table style='width:45%;margin-left:45px; border-collapse: collapse;margin-top:10px; font-size:12px;'>"
//          + "<tr>"
//          + "<td> - Ex Gratia Payment </td>"
//            + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td>Rs. </td>"
//+ "<td>3,218,018.00</td>"
// + "</tr>"
// + "<tr>"
// + "<td>(Less: Tax on Ex Gratia)</td>"
// + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td>Rs.</td>"
//+ "<td Style='border-bottom:1px solid black;'> &#160;&#160;252,378.00</td>"
//+"</tr>"
//+ "<tr>"
//+ "<td><B>Net of Tax + </B></td>"
//   + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> <B> Rs.+ </B></td>"
//  + "<td> <B>2,965,640.00 + </B></td>"
//     + "</tr>"
//     + "</table>"
//       + "</div>"
//       + "<div Style='margin-left:25px;'>"
//       + "<h1 Style='font-size:12px;text-align:left;'>ADD: </h1>"
//         + "<table style='width:45%; margin-left:28px; border-collapse: collapse;margin-top:10px; font-size:12px;'>"
//       + "<tr>"
//       + "<td> - Provident Fund Settlement</td>"
//         + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td>Rs.</td>"
//+ "<td>2,455,397.99</td>"
// + "</tr>"
// + "<tr>"
// + "<td> - Gratuity Fund Settlement</td>"
//   + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td>Rs.</td>"
//+ "<td>1,488,949.21</td>"
// + "</tr>"
// + "<tr>"
// + "<td> - Leave Encashment for 6 Days</td>"
//   + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td>Rs.</td>"
//   + "<td>25, 635.16</td>"
//   + "</tr>"
//   + "<tr>"
//   + "<td> - Notice Period Pay</td>"
//   + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td>Rs.</td>"
//   + "<td>129, 956.00</td>"
//   + "</tr>"
//   + "<tr>"
//   + "<td> - BAF loan subsidy</td>"
//   + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"

            //+"<td>Rs.</td>"
            //   + "<td Style='border-bottom:1px solid black;'>1, 967.87</td>"
            //   + "</tr>"
            //   + "<tr>"
            //   + "<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td Style='border-bottom:1px solid black;'>4, 101, 906.23</td>"
            //   + "</tr>"
            //   + "</table>"
            //     + "</div>"
            //      + "<div Style='margin-left:25px;'>"
            //     + "<h1 Style='font-size:12px;text-align:left;'>LESS:</h1>"
            //     + "<table style='width:45%; margin-left:45px; border-collapse: collapse;margin-top:10px; font-size:12px;'>"
            //   + "<tr>"
            //   + "<td> - Advance Paid Relocation Deduction - Net</td>"
            //   + "<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td>Rs.</td>"
            //   + "<td>522, 667.00</td>"
            //   + "</tr>"
            //   + "<tr>"
            //   + "<td> - Outstanding Balance of Vehicle Loan</td>"
            //   + "<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td>Rs.</td>"
            //   + "<td Style='border-bottom:1px solid black;'> &#160;&#160;98,068.00</td>"
            //+"</tr>"
            //   + "<tr>"
            //   + "<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td Style='border-bottom:1px solid black;'> &#160;&#160;620,735.00</td>"
            //+"</tr>"
            //   + "<tr>"
            //   + "<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td Style='border-bottom:1px solid black;'> &#160;&#160;&#160;&#160;&#160;&#160;3,481,171.23</td>"
            //+"</tr>"
            //   + "<tr>"
            //   + "<td><B>Net amount due to from the bank+</B></td>"
            //      + "<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td> &#160;&#160;&#160;</td>"
            //+"<td><B>Rs.</B></td>"
            //   + "<td Style='border-bottom:1.5px solid black;'><B> &#160;&#160;&#160;&#160;&#160;&#160;6,446,811.23</B></td>"
            //+"</tr>"
            //   + "</table>"
            //   + "</div>"

            disINFO_SETTLE

   //+ "<div Style='width:70%;margin-top:10px;margin-bottom:-20px; margin-left:25px;margin-right:70px;'>"
   //  + "<h3 Style='font-size:12px;text-align:center;'>UNDERTAKING</h3>"
   //  + "<p Style='font-size:12px;text-align:left;'>I acknowledge receipt of <B>PKR 6, 446, 811.23 </B>� (Rupees: Six Million Four Hundred Forty Six Thousand Eight Hundred Eleven And Paisa Twenty <br>"
   //+ "Three Only) in settlement of all my dues with the Citibank N.A.</p>"
   //+ "<p Style='font-size:12px;text-align:left;'>I further confirm that no dues of any nature whatsoever are receivable from the bank </p>"
   //   + "</div>"
   //   + "<div Style='width:30%;margin-top:10px;margin-bottom:-20px; margin-left:620px;height:150px;'>"
   //   + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:100px; margin-right:30px;margin-top:50px;'> </div>"
   //    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:50px;'> </div>"
   //    + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:12px;text-align:left;'>Checker <B>Imran Khan </B></p>"
   //       + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:20px; margin-right:30px;margin-top:-38px;'></div>"
   //      + "<p Style='width:150px;height=20px;margin-left:20px; margin-right:30px;margin-top:10px;font-size:12px;text-align:left;'>Maker<B>Farmanullah Khan </B> </p>"
   //      + "</div>"


//         + "<div Style='page-break-before: always;margin-left:25px;margin-top:30px;width:100%;'>"
//         + "<h1 Style='font-size:12px;text-align:left;margin-left:50px;margin-top:20px;'>TRUSTEES CITIBANK N.A.EMPLOYEE�S GRATUITY FUND <br>GRATUITY FUND</h1>"
//           + "<p Style='font-size:12px;text-align:left;margin-left:50px;margin-top:20px;'><B>Employee # 3587</B></p>"
//+ "<table style='width:50%;margin-left:45px; border-collapse: collapse;margin-top:10px;margin-right:2px;'>"
//            + "<tr>"
//            + "<td Style='font-size:12px;width:0px;'>Date of Joining <br>Date of Leaving<br>Annual Salary<br>Last Drawn Monthly Basic Salary<br>130% of Basic Monthly Salary (A)<br>Length of Service (B) <br> Gratuity (A*B)</td> "
//               + "<td Style='font-size:12px;width:0px;'>:<br>:<br>:<br>:<br>:<br>:<br>:</td>"
//                + "<td Style='font-size:12px;width:0px;text-align:right;'>July 17, 2007<br> September 30, 2020 <br>1,559,472<br>86,637.33<br>112,628.53<br>13.22<br>1,488,949.21</td>"
//                             + "</tr>"
//                             + "<tr>"
//                             + "<td> &#160;&#160;&#160;</td>"
//+"<td> &#160;&#160;&#160;</td>"
//+"<td  Style='font-size:12px;border-bottom:2px solid black;border-top:1px solid black;text-align:right;'>1,488,949.21</td>"
//+ "</tr>"
//+ "</table>" + "</div>"
// + "<div Style='width:70%;margin-top:20px;margin-bottom:-20px; margin-left:25px;margin-right:70px;'>"
//  + "<h3 Style='font-size:12px;text-align:center;'>UNDERTAKING</h3>"
//   + "<p Style='font-size:12px;text-align:center;'>I acknowledge receipt of <B>PKR 1,488,949.21 � (Rupees: One Million Four Hundred Eighty Eight Thousand Nine Hundred <br>"
//  + "Forty Nine And Paisa Twenty One only) +</B>being Gratuity Fund Payment from Citibank N.A.Pakistan. </P>"
//  + "</div>"
//  + "<div Style='width:30%;margin-top:20px; margin-left:620px;height:150px;'>"
//  + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:10px; margin-right:30px;margin-top:20px;'></div>"
//   + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:0px;'></div>"
//   + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:12px;text-align:left;'>Trustee Approval</p>"
//    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:50px;'></div>"
//    + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:12px;text-align:left;'>Checker<B>Imran Khan+</B></p>"
//       + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:20px; margin-right:30px;margin-top:-38px;'> </div>"
//      + "<p Style='width:150px;height=20px;margin-left:20px; margin-right:30px;margin-top:10px;font-size:12px;text-align:left;'>Maker<B>Farmanullah Khan +</B></p>"
//      + "</div>"


    + "</body>"
    + "</html>";
            
            return html;
        }
    }
}
