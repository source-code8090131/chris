using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Collections;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON;
using iCORE.XMS.DATAOBJECTS;  




namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    public partial class ReportViewer : Form
    {
        #region --Constant--
        public const string m_ParamType = "ParamType";
        public const string m_Values = "Values";
        public const string m_ParamName = "ParamName";
        public const string m_DateTimeParameter = "DateTimeParameter";
        #endregion

        public string m_DataSource;
        public string m_Database;
        public string m_UserID;
        public string m_SP;
        public string m_Password;
        public XmlDocument xDoc;
        public string subPath;
        public string m_RptName;
        public string m_RptPath;
        public string m_SPPrefix = "CIB_RPT_SP_";
        public string m_SPName = string.Empty;
        public string m_Sub_SPName = string.Empty;
        public DataSet Ds = new DataSet();
        CommonDataManager oCmnDataMgr = new CommonDataManager();
        SQLManager sqlManager = new SQLManager();
        public static XMS.DATAOBJECTS.ConnectionBean connbean = null;
        ReportDocument crDoc = new ReportDocument();
        int sub_spCount = 0;
        public string subsp = string.Empty;
        SqlConnection conn = new SqlConnection();
      
        public ReportViewer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set Parameters and Load Report
        /// </summary>
        /// <param name="datatable"></param>
        public void LoadParameters(DataTable datatable)
        {
            crystalReportViewer1.ReportSource = null;
            
            BaseRptForm brpt = new BaseRptForm();
            //MessageBox.Show(m_RptPath);
              crDoc.Load(m_RptPath);

             int subrptCount = crDoc.Subreports.Count;
             // for sub report //
             if (subrptCount > 0)
             {
                 foreach (ReportDocument rptCollect in crDoc.Subreports)
                 {
                     DataSet dsSubSP = new DataSet();
                     ReportDocument crDoc1 = new ReportDocument();
                     brpt.m_rootPath = brpt.apppath.Substring(0, brpt.apppath.Length - 10);
                     subPath = brpt.m_rootPath + brpt.m_ModulePath + crDoc.Subreports[sub_spCount].Name + ".rpt";
                //     crDoc1.Load(subPath);
                     subsp = m_SPPrefix + crDoc.Subreports[sub_spCount].Name;
                   //  dsSubSP = SelectDataSet(subsp);
                  //   SetSubRpt(subsp, dsSubSP, crDoc1);
                     sub_spCount++;
                 }
             }
           
                        
             // for main report //
             m_SPName = m_SPPrefix + m_SP;
             datatable.TableName = m_SPName;
             Ds = oCmnDataMgr.LoadReportParametersSQL(datatable);
               
            //if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
            if (Ds != null && Ds.Tables.Count > 0)
            {   crDoc.SetDataSource(Ds.Tables[0]);
                crystalReportViewer1.ReportSource = crDoc;
                //crystalReportViewer1.ServerName = "OMPRSRV1";
                crystalReportViewer1.LogOnInfo[0].ConnectionInfo.UserID = "sa";
                crystalReportViewer1.LogOnInfo[0].ConnectionInfo.Password = "sql@2005";
                crystalReportViewer1.LogOnInfo[0].ConnectionInfo.DatabaseName = "cib";
                crystalReportViewer1.RefreshReport();
              
            }
            else
            {
                MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

       //------------------------------------  Sub Report Starts------------------------------------//

   /*     public void SetSubRpt(string subsp, DataSet ds, ReportDocument crDoc1)
        {
         
            if (ds != null && ds.Tables.Count > 0)
            {
                
                crDoc1.SetDataSource(ds.Tables[0]);
                crystalReportViewer1.ReportSource = crDoc1;
            
            }
            else
            {
                MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }
      
        /// <summary>
        /// Method for Populating SUBREPORTs via its SP //
        /// </summary>
       
        public DataSet SelectDataSet(string spName)
        {
           // Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = new SqlConnection();
            DataSet dst = null;
  
            conn.ConnectionString = "Data Source=OMPSRV1;Initial Catalog=CIB;User Id=sa;Password=sql@2005";
            conn.Open();
            cmd = new SqlCommand(spName, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            adp = new SqlDataAdapter(cmd);
            dst = new DataSet();
            adp.Fill(dst);
            return dst ;
        }
        */
        //------------------------------------ Sub Report  END --------------------------------------------//

    
    }
}