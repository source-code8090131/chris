using System;
using System.Collections.Generic;
using System.Text;

/*****************************************************************
  Class Name:  ApplicationMessages 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Shamraiz Ahmad
  Created Date: 22-08-2007
  Version No: 1.0
  Modification: <Mention the modification in the class >               
  Modified By:
  Modification Date:
  Version No: <The version No after modification>

*****************************************************************/

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    public class ApplicationMessages
    {
        public enum DBMessages: int 
        { 
            DuplicateRecord = 1000,
            DMLFailed = 1010,
            DeleteRecord = 5000,
        }

        public const string OBJECTIDENTIFIER = "[Object Name]";
        public const string ADD_SUCCESS_MESSAGE = OBJECTIDENTIFIER + " Added Successfully.";
        public const string ADD_FAILURE_MESSAGE = "Error Occurred Adding " + OBJECTIDENTIFIER + ".";
        public const string UPDATE_SUCCESS_MESSAGE = OBJECTIDENTIFIER + " Updated Successfully.";
        public const string UPDATE_FAILURE_MESSAGE = "Error Occurred Updating " + OBJECTIDENTIFIER + ".";
        public const string DELETE_SUCCESS_MESSAGE = "Record Deleted Successfully.";
        public const string DELETE_CONFIRMATION_MESSAGE = " Do You Want To Delete This Record?" ;
        public const string DELETE_FAILURE_MESSAGE = "Reference records found in other tables against " + OBJECTIDENTIFIER + ".";
        public const string DUPLICATE_MESSAGE = OBJECTIDENTIFIER + " Already Exists.";
        public const string AUTHORIZE_FAILURE_MESSAGE =  "Login User Not Allowed To Authorize.";
        public const string AUTHORIZE_SUCCESSFUL_MESSAGE = "Record Authorized Successfully.";
        public const string UNAUTHORIZE_FAILURE_MESSAGE = "Login User Not Allowed To UnAuthorize.";
        public const string UNAUTHORIZE_SUCCESSFUL_MESSAGE = "Record UnAuthorized Successfully.";
        public const string UNAUTHORIZE_DEPENDENCIES_MESSAGE = "Record cannot be unAuthorized, because there are some authorized reference records exist.";
        public const string RECORD_NOT_FOUND_MESSAGE = "Record Not Found.";
        public const string CITI_BRANCH_DRAFT_UPDATED = "Citi Branch Draft has been Successfully Liguidated";
        public const string CITI_BRANCH_DRAFT_UPDATED_Faliure = "Citi Branch Draft has not been  Liguidated";
        public const string SELECT_FILE_MESSAGE= "Select File To Process.";
        public const string SELECT_COURIER_MESSAGE = "Courier Not Selected.";
        public const string FILE_ALREADY_PARSED_MESSAGE = "File Already Parsed.";
        public const string FILE_ALREADY_UPLOADED_MESSAGE = "File Already Uploaded.";
        public const string FILE_NOT_VALID_MESSAGE = "File Is Not Valid.";
        public const string FILE_NAME_NOT_VALID_MESSAGE = "File Name Is Not Valid.";
        public const string FILE_PROCESSED_SUCCESS_MESSAGE = "File Processed Successfully.";
        public const string FILE_GENERATION_SUCCESS_MESSAGE = "File Generated Successfully.";
        public const string FILE_GENERATION_FAILURE_MESSAGE = "File Can Not Be Generated.";
        public const string FILE_RECORDS_NOT_AVAILABLE_MESSAGE = "Records Not Available For File Generation.";
        public const string FILE_SAVE_FAILURE_MESSAGE = "File Can Not Be Saved";
        public const string FILE_CUSTOMER_NOT_SELECTED_MESSAGE = "Customer Not Selected";
        public const string FILE_BANK_NOT_SELECTED_MESSAGE = "Bank Not Selected";
        public const string REISSUE_SUCCESS_MESSAGE = OBJECTIDENTIFIER + " Re Issued Successfully.";
        public const string REISSUE_FAILURE_MESSAGE = "Error Occurred Re Issuing " + OBJECTIDENTIFIER + ".";
        public const string RETURN_SUCCESS_MESSAGE = OBJECTIDENTIFIER + " Returned to Customer Successfully.";
        public const string RETURN_FAILURE_MESSAGE = "Error Occurred Returning to Customer " + OBJECTIDENTIFIER + ".";
        public const string REVERSE_SUCCESS_MESSAGE = OBJECTIDENTIFIER + " Reversed Successfully.";
        public const string REVERSE_FAILURE_MESSAGE = "Error Occurred Reversing " + OBJECTIDENTIFIER + ".";
        public const string DRAFTASSIGN_SUCCESS_MESSAGE = OBJECTIDENTIFIER + " Assigned Successfully.";
        public const string DRAFTASSIGN_FAILURE_MESSAGE = "Error Occurred Assigning " + OBJECTIDENTIFIER + ".";
        public const string DRAFTASSIGN_UNAVAILABLE_MESSAGE = "There Are No Drafts To Assign.";
        public const string CONFIRM_STOP_PAYMENT = "Do you really want to Stop Payment for Draft.";
        public const string No_Data_In_Report = "Report can't be shown because there is No record in the Database accoring to given Criteria.";
        public const string SELECT_MESSAGE = OBJECTIDENTIFIER + " Is Not Selected.";
        public const string GENERATE_DAT_SUCCESS = "File saved at " + OBJECTIDENTIFIER + ".";
        public const string GENERATE_DAT_FAILURE = "File Cannot be Generated as no records are avaliable for selected dates.";
        public const string DISCREPANCY_REJECT_SUCCESS_MESSAGE = "Rejection of discrepancy completed successfully.";
        public const string DISCREPANCY_REJECT_FAILURE_MESSAGE = "Error occurred during the rejection of discrepancy.";
        public const string DISCREPANCY_ACCEPT_SUCCESS_MESSAGE = "Discrepancy accepted successfully.";
        public const string DISCREPANCY_ACCEPT_FAILURE_MESSAGE = "Error occurred accepting the discrepancy.";
        public const string SERIAL_FINISHED_MESSAGE = "Serial allocated for this Customer and Corresponding Bank Has Been Finished. " + 
            "\nPlease allocate another serial to print the remaining drafts.";
        public const string PRINTING_SUCCESS_MESSAGE = "All drafts printed successfully.";
        public const string PRINTING_FAILURE_MESSAGE = "Error occurred during the printing of drafts.";
        public const string EXCEPTION_MESSAGE = "An error occurred during execution. " +
            "\nPlease contact your software administrator.";
        public const string CONFIRM_FUNDING_AUTHORIZATION_MESSAGE = "Are you sure you want to authorize funding?";
        public const string FUNDING_AUTHORIZATION_SUCCESS_MESSAGE = "Funds authorized successfully.";
        public const string FUNDING_AUTHORIZATION_FAILURE_MESSAGE = "Error occurred authorizing the funds.";
        public const string NO_RECORDS_FOUND_MESSAGE = "No record found.";
        public const string NO_SHEETS_FOUND = "No sheets avaliable for selected Business Partner.";
        public const string Cheque_Sheet_Amount_NotEqual = "Every Cheque must have Sheet processed and Cheque Amount must be equal to sum of Sheets.";
        public const string ALLTRANSACTIONSSHOULDHAVESECTIONS = "All Uploaded Transactions should have sections allocated.";
       // public const string ATLASTRECORD = "At Last Record";
        public const string ERRORINTEGERCHARACTER = "You entered an unacceptable character in a number item.";
        public const string MUST_BE_NON_NEGATIVE = "Length can't be entered in negative.";
        public const string LENGTH_NOT_REQUIRED = "Length can't be entered for Selected Data Type.";
        public const string DELETE_MASTER_RECORD = "Cannot delete master record when matching detail records exist.";
        public const string TERM_ALREADY_SELECTED = "You have already selected this term.";
        public const string LENGTH_CANNOT_EXCEED = "Lenght of Value can't exceed from the predefined length.";
        public const string CANNOT_DELETE_MAPPING_ACCOUNT_CODE = "Can't delete Mapping Acc Code.";
        public const string SET_NO_POSITIVE = "Set No Must Be Greater Or Equal To Zero.";
        public const string SET_NO_ALREADY_EXIST = "Set No Already Exist.";
        public const string SET_NAME_ALREADY_EXIST = "Set Name Already Exists.";
        public const string ATFIRSTRECORD = "At First Record";
        public const string ATLASTRECORD = "At Last Record";
        public const string MARKUPRATEDAYS = "Mark Up Rate should be Greater than Zero.";
    }
}
