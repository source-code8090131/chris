using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    /// <summary>
    /// SimpleForm
    /// </summary>
    public partial class SimpleForm : BaseForm
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SimpleForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mainmenu"></param>
        /// <param name="connbean_obj"></param>
        public SimpleForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            this.connbean = connbean_obj;
            this.SetConnectionBean();
            InitializeComponent();
            this.MdiParent = mainmenu;
        }
        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        /// <summary>
        /// CommonOnLoadMethods
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            CreateToolbar();
            if (null != this.Controls.Owner)
            {
                this.pcbCitiGroup.Location = new Point(this.Controls.Owner.Right - 80, this.pcbCitiGroup.Top);
                this.pcbCitiGroup.BringToFront();
            }

        }
        /// <summary>
        /// DoToolbarActions
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            SLPanel  oPanel=GetCurrentPanelBlock;
            if (oPanel is SLPanelSimple)
            {
                switch (actionType)
                {
                    case "Save":
                        if (this.GetCurrentPanelBlock.EnableInsert || this.GetCurrentPanelBlock.EnableUpdate)
                            base.Save_Click(this.GetCurrentPanelBlock, actionType);
                        this.Message.ShowMessage();
                        break;
                    case "Add":
                        base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                        this.m_intPKID = 0;
                        break;
                    case "Edit":
                        base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                        break;
                    case "Delete":
                        if (ShowConfirmationMessage("", ApplicationMessages.DELETE_CONFIRMATION_MESSAGE))
                        {
                            if (this.GetCurrentPanelBlock.EnableDelete)
                            {
                                base.Delete_Click(this.GetCurrentPanelBlock, actionType);
                                if (!this.Message.HasErrors())
                                {
                                    base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                                    this.GetCurrentPanelBlock.ClearBusinessEntity();
                                    this.m_intPKID = 0;
                                }
                                this.Message.ShowMessage();
                            }
                        }
                        break;
                    case "List":
                        base.ShowList(this.GetCurrentPanelBlock, actionType);
                        base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                        break;
                    case "Cancel":
                        base.FormCancelMode(this.GetCurrentPanelBlock.Controls);
                        base.FormCancelMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                        this.GetCurrentPanelBlock.ClearBusinessEntity();
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                pnl.ClearBusinessEntity(); ;
                        this.m_intPKID = 0;
                        base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                base.FormAddMode(pnl.Controls);
                        break;
                    case "AddNew":
                        base.FormCancelMode(this.GetCurrentPanelBlock.Controls);
                        base.FormCancelMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                        this.GetCurrentPanelBlock.ClearBusinessEntity();
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                pnl.ClearBusinessEntity(); ;
                        this.m_intPKID = 0;
                        base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                base.FormAddMode(pnl.Controls);
                        break;
                }
            }
            else if (oPanel is SLPanelSimpleList)
            {
                switch (actionType)
                {
                    case "Save":
                        if (this.GetCurrentPanelBlock.EnableInsert || this.GetCurrentPanelBlock.EnableUpdate)
                            //base.Save_Click(this.GetCurrentPanelBlock, actionType);
                            if ((GetCurrentPanelBlock as SLPanelSimpleList).SaveRecords(GetCurrentPanelBlock as SLPanel))
                            {
                                // GetCurrentPanelBlock.ResetPanelChanges();
                                // operationMode = Mode.Edit;
                            }
                        this.Message.ShowMessage();
                        break;
                    case "Add":
                        base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                        this.m_intPKID = 0;
                        break;
                    case "Edit":
                        base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                        break;
                    case "Delete":
                        if (ShowConfirmationMessage("", ApplicationMessages.DELETE_CONFIRMATION_MESSAGE))
                        {
                            if (this.GetCurrentPanelBlock.EnableDelete)
                            {
                                (oPanel as SLPanelSimpleList).DeleteRecords(oPanel);
                                //base.Delete_Click(this.GetCurrentPanelBlock, actionType);
                                if (!this.Message.HasErrors())
                                {
                                    base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                                    this.GetCurrentPanelBlock.ClearBusinessEntity();
                                    this.m_intPKID = 0;
                                }
                                this.Message.ShowMessage();
                            }
                        }
                        break;
                    case "List":
                        base.ShowList(this.GetCurrentPanelBlock, actionType);
                        //base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                        break;
                    case "Cancel":
                        base.FormCancelMode(this.GetCurrentPanelBlock.Controls);
                        base.FormCancelMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                        this.GetCurrentPanelBlock.ClearBusinessEntity();
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                pnl.ClearBusinessEntity(); ;
                        this.m_intPKID = 0;
                        base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                base.FormAddMode(pnl.Controls);
                        
                        break;
                    case "AddNew":
                        base.FormCancelMode(this.GetCurrentPanelBlock.Controls);
                        base.FormCancelMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                        this.GetCurrentPanelBlock.ClearBusinessEntity();
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                pnl.ClearBusinessEntity(); ;
                        this.m_intPKID = 0;
                        base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                        if (GetCurrentPanelBlock.ConcurrentPanels != null)
                            foreach (SLPanel pnl in GetCurrentPanelBlock.ConcurrentPanels)
                                base.FormAddMode(pnl.Controls);

                        break;
                    //case "ClearScreen":
                    //    if (GetCurrentPanelBlock.EnableQuery == true)
                    //    {
                    //        base.FormEditMode(this.Controls);
                    //        GetCurrentPanelBlock.ClearBusinessEntity();
                    //        GetCurrentPanelBlock.ClearPanelControls(GetCurrentPanelBlock.Controls);
                    //        this.m_intPKID = 0;
                    //        currentQueryMode = Enumerations.eQueryMode.EnterQuery;
                    //    }
                    //    else if (GetCurrentPanelBlock.EnableQuery == false)
                    //    {
                    //        base.FormCancelMode(this.Controls);
                    //        base.FormEditMode(this.Controls);
                    //        GetCurrentPanelBlock.ClearBusinessEntity();
                    //        GetCurrentPanelBlock.ClearPanelControls(GetCurrentPanelBlock.Controls);
                    //        this.m_intPKID = 0;
                    //        ResetFormChanges();
                    //    }
                    //    break;

                }
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        /// <summary>
        /// Form Keyup Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void SimpleForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
                DoToolbarActions(this.Controls, "List");
            else
                base.OnFormKeyup(sender, e);
        }
        /// <summary>
        /// CreateToolbar
        /// </summary>
        protected override void CreateToolbar()
        {
            base.CreateToolbar();
            tbtEdit.Visible = false;
        }
    }
}