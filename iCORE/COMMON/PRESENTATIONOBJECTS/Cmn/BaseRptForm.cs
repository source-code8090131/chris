//#define PRODUCTION
#define DEV
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Globalization;
using System.Xml.Xsl;

using System.Xml.XPath;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    /// <summary>
    /// Base Report Form
    /// </summary>
    public partial class BaseRptForm : BaseForm
    {
        #region --Variable--
        private bool m_DynamicSerchCtrl;
        public string m_rptFile;
        public string m_subName;
        private bool m_matrixRpt;
        private bool m_subRpt;
        private string m_XSLFoleder = ConfigurationManager.AppSettings["xslFolderName"];
        public string m_Prefix = "CIB_RPT_SP_";
#if PRODUCTION
            public string apppath          = Application.ExecutablePath;
            public string m_ModulePath     = "\\CIB\\PRESENTATIONOBJECTS\\Report\\";
#endif
#if DEV
        public string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
        public string m_ModulePath = "CIB\\PRESENTATIONOBJECTS\\Report\\";
        //public string m_ModulePath = "CHRIS_1.1.37\\CIB\\PRESENTATIONOBJECTS\\Report\\";
#endif

        private bool m_IsValidPage = true;
        private bool m_IsValid = true;
        private string m_SPName;
        public bool m_Subrpt;
        public string m_rootPath = string.Empty;
        private string m_XSLPath = string.Empty;
        public string m_rptFilePath = string.Empty;
        private string ctrlval = string.Empty;
        private DataSet Ds = new DataSet();
        private SqlConnection m_ConObj;
        CommonDataManager oCmnDataMgr = new CommonDataManager();
        public DataTable dataTable = new DataTable();
        private CrystalDecisions.CrystalReports.Engine.ReportDocument crDoc;


        #endregion

        #region --Properties--

        public string RptFileName
        {
            get { return m_rptFile; }
            set { m_rptFile = value; }
        }

        public bool DynamicSearchCrtl
        {
            get { return m_DynamicSerchCtrl; }
            set { m_DynamicSerchCtrl = value; }
        }

        public bool MatrixReport
        {
            get { return m_matrixRpt; }
            set { m_matrixRpt = value; }
        }


        //private string[] _colect = new string[]{};
        private string[] _colect = new string[100];
        public string[] Colect
        {
            get { return _colect; }
            set { _colect = value; }
        }

        public bool SubReport
        {
            get { return m_subRpt; }
            set { m_subRpt = value; }
        }




        #endregion

        #region --Constructor Segment--

        public BaseRptForm()
        {
            InitializeComponent();
        }

        public BaseRptForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region --Methods--

        /// <summary>
        /// Run Report
        /// </summary>
        public virtual void RunReport()
        {
            m_rootPath = apppath.Substring(0, apppath.Length - 10);
            ReportViewer rptViewer = new ReportViewer();
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";
            //MessageBox.Show(m_rptFilePath);
            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;
                                    ctrlval = ctrls.Text;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue.ToString();

                                }
                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrls.Text == "")
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                    else if (ctrlval.Length == 6)
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    }
                                    else if (ctrlval.Length == 8)
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable);
                    rptViewer.ShowDialog(this);
                }

                else if (SubReport == true && dataTable.Rows.Count > 0)
                {
                    rptViewer.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }
        }

        /// <summary>
        /// CAll Matrix Report
        /// </summary>
        private void CallMatrixReport()
        {
            m_XSLPath = m_rootPath + m_ModulePath + m_rptFile + ".xsl";

            Ds = oCmnDataMgr.LoadReportParametersSQL(dataTable);
            //string apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            if (!Directory.Exists(apppath + m_XSLFoleder))
            {
                DirectoryInfo di = Directory.CreateDirectory(apppath + m_XSLFoleder);
            }

            //Create the FileStream to write with.
            System.IO.FileStream fs = new System.IO.FileStream(apppath + m_XSLFoleder + m_rptFile + ".xml", System.IO.FileMode.Create);

            //Create an XmlTextWriter for the FileStream.
            System.Xml.XmlTextWriter xtw = new System.Xml.XmlTextWriter(fs, System.Text.Encoding.Unicode);

            //Add processing instructions to the beginning of the XML file, one of which indicates a style sheet.
            xtw.WriteProcessingInstruction("xml", "version='1.0'");
            xtw.WriteProcessingInstruction("xml-stylesheet",
                "type='text/xsl' href='" + m_XSLPath + "'");

            //Write the XML from the dataset to the file.
            Ds.WriteXml(xtw);
            System.IO.FileStream fstrm = new System.IO.FileStream(apppath + m_XSLFoleder + m_rptFile + ".htm", System.IO.FileMode.Create);

            //Create an XmlTextWriter for the FileStream.
            System.Xml.XmlTextWriter xtxtw = new System.Xml.XmlTextWriter(fstrm, System.Text.Encoding.Unicode);

            //Transform the XML using the stylesheet.
            XmlDataDocument xmlDoc = new XmlDataDocument(Ds);
            System.Xml.Xsl.XslCompiledTransform xslTran = new System.Xml.Xsl.XslCompiledTransform();
            xslTran.Load(m_XSLPath);
            xslTran.Transform(xmlDoc, null, xtxtw);

            //Open the HTML file in Excel.
            Excel.Application oExcel = new Excel.Application();
            oExcel.Visible = true;
            oExcel.UserControl = true;
            Excel.Workbooks oBooks = oExcel.Workbooks;
            object oOpt = System.Reflection.Missing.Value;
            oBooks.Open(apppath + m_XSLFoleder + m_rptFile + ".htm", oOpt, oOpt, oOpt,
                oOpt, oOpt, oOpt, oOpt, oOpt, oOpt, oOpt, oOpt,
                oOpt, oOpt, oOpt);

        }
        
        /// <summary>
        /// RunCustomReport
        /// </summary>
        /// <returns></returns>
        public DataSet RunCustomReport()
        {
            m_rootPath = apppath.Substring(0, apppath.Length - 10);
            ReportViewer rptViewer = new ReportViewer();
            m_rptFilePath = m_rootPath + m_ModulePath + m_rptFile + ".rpt";
            m_SPName = m_Prefix + RptFileName;
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();

            foreach (Control ctrlGrpBox in this.Controls)
            {
                if ((ctrlGrpBox is System.Windows.Forms.GroupBox))
                {
                    foreach (Control ctrls in ctrlGrpBox.Controls)
                    {
                        for (int index = 0; index < dataTable.Columns.Count; index++)
                        {
                            if ((ctrls is CrplControlLibrary.SLTextBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    if (ctrls.Text != "")
                                    {
                                        if (((CrplControlLibrary.SLTextBox)ctrls).IsDate == true)
                                        {
                                            if (InputValidator.IsValidDateFormat(ctrls.Text.ToString(), this.errorProvider1, ctrls))
                                            {
                                                ctrlval = ctrls.Text;
                                                string date = ctrlval.ToString();
                                                string m_Month = ctrlval.Substring(0, 2);
                                                string m_Year = ctrlval.Substring(2, 4);
                                                string m_Date = m_Month + "-01-" + m_Year;

                                                dataTable.Rows[0][dataTable.Columns[index].ColumnName] = m_Date;
                                                m_IsValidPage = true;
                                            }
                                            else
                                            {
                                                m_IsValidPage = false;
                                            }
                                        }
                                        else
                                        {
                                            ctrlval = ctrls.Text;
                                            dataTable.Rows[0][dataTable.Columns[index].ColumnName] = ctrlval.ToString();
                                        }
                                    }
                                    else
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                }
                            }
                            if ((ctrls is ComboBox))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ComboBox cmb = (ComboBox)ctrls;
                                    dataTable.Rows[0][dataTable.Columns[index].ColumnName] = cmb.SelectedValue.ToString();
                                }
                            }

                            if ((ctrls is CrplControlLibrary.SLDatePicker))
                            {
                                if (dataTable.Columns[index].ColumnName.ToUpper() == ctrls.Name.ToString().ToUpper())
                                {
                                    ctrlval = ctrls.Text;
                                    if (ctrls.Text == "")
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DBNull.Value;
                                    }
                                    else if (ctrlval.Length == 6)
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "MMyyyy", CultureInfo.InvariantCulture);
                                    }
                                    else if (ctrlval.Length == 8)
                                    {
                                        dataTable.Rows[0][dataTable.Columns[index].ColumnName] = DateTime.ParseExact(ctrlval, "ddMMyyyy", CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (m_IsValidPage == true)
            {
                /*Open Report Viewer Control & Pass RprName n rptPath*/
                rptViewer.m_RptName = m_rptFile;
                rptViewer.m_SP = m_rptFile;
                rptViewer.m_RptPath = m_rptFilePath;


                if (MatrixReport == true)
                {
                    CallCustomMatrixReport();
                }
                else if (MatrixReport == false && dataTable.Rows.Count > 0)
                {
                    rptViewer.LoadParameters(dataTable);
                    rptViewer.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show("No Record Exists with these Inputs", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //return;
                }

            }

            return Ds;
        }

        /// <summary>
        /// CallCustomMatrixReport
        /// </summary>
        /// <returns></returns>
        private DataSet CallCustomMatrixReport()
        {
            m_XSLPath = m_rootPath + m_ModulePath + m_rptFile + ".xsl";
            Ds = oCmnDataMgr.LoadReportParametersSQL(dataTable);
            //string apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            if (!Directory.Exists(apppath + m_XSLFoleder))
            {
                DirectoryInfo di = Directory.CreateDirectory(apppath + m_XSLFoleder);
            }

            return Ds;
        }

        /// <summary>
        /// CommonOnLoadMethods
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            CreateToolbar();
        }

        /// <summary>
        /// Create Toolbar
        /// </summary>
        protected override void CreateToolbar()
        {
            base.CreateToolbar();
            tbtAdd.Visible = tbtCancel.Visible = tbtDelete.Visible = tbtEdit.Visible = tbtList.Visible = tbtSave.Visible = false;
        }

        #endregion

        #region --Events--
        /// <summary>
        /// btnCallReport Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnCallReport_Click(object sender, EventArgs e)
        {
            /*Call Report*/
            RunReport();
        }
        /// <summary>
        /// btnCloseReport Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnCloseReport_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}

