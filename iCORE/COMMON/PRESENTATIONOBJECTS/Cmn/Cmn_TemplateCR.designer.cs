namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    partial class Cmn_TemplateCR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.tbtAdd = new System.Windows.Forms.ToolStripButton();
            this.tbtEdit = new System.Windows.Forms.ToolStripButton();
            this.tbtSave = new System.Windows.Forms.ToolStripButton();
            this.tbtDelete = new System.Windows.Forms.ToolStripButton();
            this.tbtList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtCancel = new System.Windows.Forms.ToolStripButton();
            this.tbtClose = new System.Windows.Forms.ToolStripButton();
            this.tlbMain = new System.Windows.Forms.ToolStrip();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.tlbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // tbtAdd
            // 
            this.tbtAdd.Image = global::iCORE.Properties.Resources.Add_Record;
            this.tbtAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtAdd.Name = "tbtAdd";
            this.tbtAdd.Size = new System.Drawing.Size(30, 33);
            this.tbtAdd.Text = "&Add";
            this.tbtAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtEdit
            // 
            this.tbtEdit.Enabled = false;
            this.tbtEdit.Image = global::iCORE.Properties.Resources.Edit_Record;
            this.tbtEdit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtEdit.Name = "tbtEdit";
            this.tbtEdit.Size = new System.Drawing.Size(29, 33);
            this.tbtEdit.Text = "&Edit";
            this.tbtEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtSave
            // 
            this.tbtSave.Enabled = false;
            this.tbtSave.Image = global::iCORE.Properties.Resources.Save_Record;
            this.tbtSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtSave.Name = "tbtSave";
            this.tbtSave.Size = new System.Drawing.Size(35, 33);
            this.tbtSave.Text = "&Save";
            this.tbtSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtDelete
            // 
            this.tbtDelete.Enabled = false;
            this.tbtDelete.Image = global::iCORE.Properties.Resources.Delete;
            this.tbtDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtDelete.Name = "tbtDelete";
            this.tbtDelete.Size = new System.Drawing.Size(42, 33);
            this.tbtDelete.Text = "&Delete";
            this.tbtDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtList
            // 
            this.tbtList.Image = global::iCORE.Properties.Resources.List_Record;
            this.tbtList.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtList.Name = "tbtList";
            this.tbtList.Size = new System.Drawing.Size(27, 33);
            this.tbtList.Text = "&List";
            this.tbtList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 36);
            // 
            // tbtCancel
            // 
            this.tbtCancel.Enabled = false;
            this.tbtCancel.Image = global::iCORE.Properties.Resources.Cancel_Record;
            this.tbtCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtCancel.Name = "tbtCancel";
            this.tbtCancel.Size = new System.Drawing.Size(43, 33);
            this.tbtCancel.Text = "Ca&ncel";
            this.tbtCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtClose
            // 
            this.tbtClose.Image = global::iCORE.Properties.Resources.Close;
            this.tbtClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtClose.Name = "tbtClose";
            this.tbtClose.Size = new System.Drawing.Size(37, 33);
            this.tbtClose.Text = "&Close";
            this.tbtClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tlbMain
            // 
            this.tlbMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtAdd,
            this.tbtEdit,
            this.tbtSave,
            this.tbtDelete,
            this.tbtList,
            this.toolStripSeparator1,
            this.tbtCancel,
            this.tbtClose});
            this.tlbMain.Location = new System.Drawing.Point(0, 0);
            this.tlbMain.Name = "tlbMain";
            this.tlbMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tlbMain.Size = new System.Drawing.Size(669, 36);
            this.tlbMain.TabIndex = 0;
            this.tlbMain.Text = "WHT Toolbar";
            this.tlbMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tlbMain_ItemClicked);
            // 
            // Cmn_TemplateCR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.tlbMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Cmn_TemplateCR";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "iCORE MMSEC - <Form Description>";
            this.Load += new System.EventHandler(this.CRS_Cmn_TemplateCR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.tlbMain.ResumeLayout(false);
            this.tlbMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal System.Windows.Forms.ErrorProvider errorProvider1;
        protected internal System.Windows.Forms.ToolStrip tlbMain;
        protected internal System.Windows.Forms.ToolStripButton tbtAdd;
        protected internal System.Windows.Forms.ToolStripButton tbtEdit;
        protected internal System.Windows.Forms.ToolStripButton tbtSave;
        protected internal System.Windows.Forms.ToolStripButton tbtDelete;
        protected internal System.Windows.Forms.ToolStripButton tbtList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        protected internal System.Windows.Forms.ToolStripButton tbtCancel;
        protected internal System.Windows.Forms.ToolStripButton tbtClose;



    }
}

