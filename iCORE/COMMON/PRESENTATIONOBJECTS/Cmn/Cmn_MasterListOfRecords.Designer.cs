﻿namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    partial class Cmn_MasterListOfRecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cmn_MasterListOfRecords));
            this.grbListOfRecords = new System.Windows.Forms.GroupBox();
            this.dgvListOfRecords = new System.Windows.Forms.DataGridView();
            this.panelSearchListOfRecord = new System.Windows.Forms.Panel();
            this.lblToDateSearch = new System.Windows.Forms.Label();
            this.lblFromDateSearch = new System.Windows.Forms.Label();
            this.dtpToDateSearch = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpFromDateSearch = new CrplControlLibrary.SLDatePicker(this.components);
            this.chkBooleanSearch = new System.Windows.Forms.CheckBox();
            this.txtDecimalSearch = new CrplControlLibrary.CrplTextBox(this.components);
            this.txtDigitSearch = new CrplControlLibrary.CrplTextBox(this.components);
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.txbStringSearch = new CrplControlLibrary.CrplTextBox(this.components);
            this.pcbCitiGroup = new System.Windows.Forms.PictureBox();
            this.tltListOfRecords = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dgvSearch = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.grpSearch = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grbListOfRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListOfRecords)).BeginInit();
            this.panelSearchListOfRecord.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.grpSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbListOfRecords
            // 
            this.grbListOfRecords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbListOfRecords.Controls.Add(this.dgvListOfRecords);
            this.grbListOfRecords.Controls.Add(this.panelSearchListOfRecord);
            this.grbListOfRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbListOfRecords.Location = new System.Drawing.Point(34, 275);
           // this.grbListOfRecords.Margin = new System.Windows.Forms.Padding(4);
            this.grbListOfRecords.Name = "grbListOfRecords";
            this.grbListOfRecords.Padding = new System.Windows.Forms.Padding(4);
            this.grbListOfRecords.Size = new System.Drawing.Size(841, 446);
            this.grbListOfRecords.TabIndex = 2;
            this.grbListOfRecords.TabStop = false;
            this.grbListOfRecords.Text = "List of Records";
            // 
            // dgvListOfRecords
            // 
            this.dgvListOfRecords.AllowUserToAddRows = false;
            this.dgvListOfRecords.AllowUserToDeleteRows = false;
            this.dgvListOfRecords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvListOfRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListOfRecords.Location = new System.Drawing.Point(16, 30);
            this.dgvListOfRecords.Margin = new System.Windows.Forms.Padding(4);
            this.dgvListOfRecords.MultiSelect = false;
            this.dgvListOfRecords.Name = "dgvListOfRecords";
            this.dgvListOfRecords.ReadOnly = true;
            this.dgvListOfRecords.Size = new System.Drawing.Size(825, 353);
            this.dgvListOfRecords.TabIndex = 0;
            this.dgvListOfRecords.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListOfRecords_RowDoubleClick);
            this.dgvListOfRecords.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListOfRecords_RowDoubleClick);
            this.dgvListOfRecords.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvListOfRecords_RowHeaderMouseDoubleClick);
            // 
            // panelSearchListOfRecord
            // 
            this.panelSearchListOfRecord.Controls.Add(this.lblToDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.lblFromDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.dtpToDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.dtpFromDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.chkBooleanSearch);
            this.panelSearchListOfRecord.Controls.Add(this.txtDecimalSearch);
            this.panelSearchListOfRecord.Controls.Add(this.txtDigitSearch);
            this.panelSearchListOfRecord.Controls.Add(this.cmbSearch);
            this.panelSearchListOfRecord.Controls.Add(this.lblSearch);
            this.panelSearchListOfRecord.Controls.Add(this.txbStringSearch);
            this.panelSearchListOfRecord.Location = new System.Drawing.Point(8, 391);
            this.panelSearchListOfRecord.Margin = new System.Windows.Forms.Padding(4);
            this.panelSearchListOfRecord.Name = "panelSearchListOfRecord";
            this.panelSearchListOfRecord.Size = new System.Drawing.Size(813, 47);
            this.panelSearchListOfRecord.TabIndex = 13;
            // 
            // lblToDateSearch
            // 
            this.lblToDateSearch.AutoSize = true;
            this.lblToDateSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDateSearch.Location = new System.Drawing.Point(560, 12);
            this.lblToDateSearch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToDateSearch.Name = "lblToDateSearch";
            this.lblToDateSearch.Size = new System.Drawing.Size(26, 18);
            this.lblToDateSearch.TabIndex = 12;
            this.lblToDateSearch.Text = "To";
            // 
            // lblFromDateSearch
            // 
            this.lblFromDateSearch.AutoSize = true;
            this.lblFromDateSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDateSearch.Location = new System.Drawing.Point(352, 12);
            this.lblFromDateSearch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFromDateSearch.Name = "lblFromDateSearch";
            this.lblFromDateSearch.Size = new System.Drawing.Size(44, 18);
            this.lblFromDateSearch.TabIndex = 11;
            this.lblFromDateSearch.Text = "From";
            // 
            // dtpToDateSearch
            // 
            this.dtpToDateSearch.CustomEnabled = true;
            this.dtpToDateSearch.CustomFormat = "dd/MM/yyyy";
            this.dtpToDateSearch.DataFieldMapping = "";
            this.dtpToDateSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDateSearch.HasChanges = false;
            this.dtpToDateSearch.Location = new System.Drawing.Point(595, 12);
            this.dtpToDateSearch.Margin = new System.Windows.Forms.Padding(4);
            this.dtpToDateSearch.Name = "dtpToDateSearch";
            this.dtpToDateSearch.NullValue = " ";
            this.dtpToDateSearch.Size = new System.Drawing.Size(140, 24);
            this.dtpToDateSearch.TabIndex = 10;
            this.dtpToDateSearch.Value = new System.DateTime(2011, 4, 8, 0, 0, 0, 0);
            this.dtpToDateSearch.ValueChanged += new System.EventHandler(this.dtpToDateSearch_ValueChanged);
            // 
            // dtpFromDateSearch
            // 
            this.dtpFromDateSearch.CustomEnabled = true;
            this.dtpFromDateSearch.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDateSearch.DataFieldMapping = "";
            this.dtpFromDateSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDateSearch.HasChanges = false;
            this.dtpFromDateSearch.Location = new System.Drawing.Point(403, 12);
            this.dtpFromDateSearch.Margin = new System.Windows.Forms.Padding(4);
            this.dtpFromDateSearch.Name = "dtpFromDateSearch";
            this.dtpFromDateSearch.NullValue = " ";
            this.dtpFromDateSearch.Size = new System.Drawing.Size(140, 24);
            this.dtpFromDateSearch.TabIndex = 9;
            this.dtpFromDateSearch.Value = new System.DateTime(2011, 4, 8, 0, 0, 0, 0);
            this.dtpFromDateSearch.ValueChanged += new System.EventHandler(this.dtpFromDateSearch_ValueChanged);
            // 
            // chkBooleanSearch
            // 
            this.chkBooleanSearch.AutoSize = true;
            this.chkBooleanSearch.Location = new System.Drawing.Point(352, 12);
            this.chkBooleanSearch.Margin = new System.Windows.Forms.Padding(4);
            this.chkBooleanSearch.Name = "chkBooleanSearch";
            this.chkBooleanSearch.Size = new System.Drawing.Size(113, 22);
            this.chkBooleanSearch.TabIndex = 8;
            this.chkBooleanSearch.Text = "checkBox1";
            this.chkBooleanSearch.UseVisualStyleBackColor = true;
            this.chkBooleanSearch.CheckedChanged += new System.EventHandler(this.chkBooleanSearch_CheckedChanged);
            // 
            // txtDecimalSearch
            // 
            this.txtDecimalSearch.AllowSpace = false;
            this.txtDecimalSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDecimalSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDecimalSearch.ContinuationTextBox = null;
            this.txtDecimalSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDecimalSearch.Location = new System.Drawing.Point(352, 12);
            this.txtDecimalSearch.Margin = new System.Windows.Forms.Padding(4);
            this.txtDecimalSearch.Name = "txtDecimalSearch";
            this.txtDecimalSearch.NumberFormat = "###,###,##0.00";
            this.txtDecimalSearch.Postfix = "";
            this.txtDecimalSearch.Prefix = "";
            this.txtDecimalSearch.Size = new System.Drawing.Size(378, 24);
            this.txtDecimalSearch.TabIndex = 7;
            this.txtDecimalSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDecimalSearch.TextType = CrplControlLibrary.TextType.Double;
            this.txtDecimalSearch.Visible = false;
            this.txtDecimalSearch.TextChanged += new System.EventHandler(this.txtDecimalSearch_TextChanged);
            // 
            // txtDigitSearch
            // 
            this.txtDigitSearch.AllowSpace = false;
            this.txtDigitSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDigitSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDigitSearch.ContinuationTextBox = null;
            this.txtDigitSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDigitSearch.Location = new System.Drawing.Point(352, 12);
            this.txtDigitSearch.Margin = new System.Windows.Forms.Padding(4);
            this.txtDigitSearch.Name = "txtDigitSearch";
            this.txtDigitSearch.NumberFormat = "###,###,##0.00";
            this.txtDigitSearch.Postfix = "";
            this.txtDigitSearch.Prefix = "";
            this.txtDigitSearch.Size = new System.Drawing.Size(378, 24);
            this.txtDigitSearch.TabIndex = 6;
            this.txtDigitSearch.TextType = CrplControlLibrary.TextType.Integer;
            this.txtDigitSearch.Visible = false;
            // 
            // cmbSearch
            // 
            this.cmbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSearch.FormattingEnabled = true;
            this.cmbSearch.Location = new System.Drawing.Point(80, 12);
            this.cmbSearch.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSearch.Name = "cmbSearch";
            this.cmbSearch.Size = new System.Drawing.Size(265, 25);
            this.cmbSearch.TabIndex = 1;
            this.tltListOfRecords.SetToolTip(this.cmbSearch, "Select Search Criteria here.");
            this.cmbSearch.SelectedIndexChanged += new System.EventHandler(this.cmbSearch_SelectedIndexChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearch.Location = new System.Drawing.Point(16, 12);
            this.lblSearch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(55, 18);
            this.lblSearch.TabIndex = 1;
            this.lblSearch.Text = "Search";
            // 
            // txbStringSearch
            // 
            this.txbStringSearch.AllowSpace = true;
            this.txbStringSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txbStringSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txbStringSearch.ContinuationTextBox = null;
            this.txbStringSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbStringSearch.Location = new System.Drawing.Point(352, 12);
            this.txbStringSearch.Margin = new System.Windows.Forms.Padding(4);
            this.txbStringSearch.Name = "txbStringSearch";
            this.txbStringSearch.NumberFormat = "###,###,##0.00";
            this.txbStringSearch.Postfix = "";
            this.txbStringSearch.Prefix = "";
            this.txbStringSearch.Size = new System.Drawing.Size(378, 24);
            this.txbStringSearch.TabIndex = 3;
            this.txbStringSearch.TextType = CrplControlLibrary.TextType.String;
            this.txbStringSearch.Visible = false;
            this.txbStringSearch.TextChanged += new System.EventHandler(this.txbStringSearch_TextChanged);
            // 
            // pcbCitiGroup
            // 
            this.pcbCitiGroup.Image = ((System.Drawing.Image)(resources.GetObject("pcbCitiGroup.Image")));
            this.pcbCitiGroup.Location = new System.Drawing.Point(568, 6);
            this.pcbCitiGroup.Name = "pcbCitiGroup";
            this.pcbCitiGroup.Size = new System.Drawing.Size(64, 64);
            this.pcbCitiGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pcbCitiGroup.TabIndex = 1;
            this.pcbCitiGroup.TabStop = false;
            this.tltListOfRecords.SetToolTip(this.pcbCitiGroup, "CitiGroup");
            // 
            // tltListOfRecords
            // 
            this.tltListOfRecords.IsBalloon = true;
            this.tltListOfRecords.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(397, 729);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click_1);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(528, 729);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // grpSearch
            // 
            this.grpSearch.Controls.Add(this.dgvSearch);
            this.grpSearch.Controls.Add(this.btnSearch);
            this.grpSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.Location = new System.Drawing.Point(42, 65);
            this.grpSearch.Margin = new System.Windows.Forms.Padding(4);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Padding = new System.Windows.Forms.Padding(4);
            this.grpSearch.Size = new System.Drawing.Size(828, 162);
            this.grpSearch.TabIndex = 9;
            this.grpSearch.TabStop = false;
            this.grpSearch.Text = "Search Records";
            this.grpSearch.Visible = false;
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Location = new System.Drawing.Point(16, 22);
            this.dgvSearch.Margin = new System.Windows.Forms.Padding(4);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.Size = new System.Drawing.Size(796, 100);
            this.dgvSearch.TabIndex = 6;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(672, 126);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(140, 28);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // Cmn_MasterListOfRecords
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.CancelButton = this.btnCancel;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 769);
            this.Controls.Add(this.grpSearch);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.grbListOfRecords);
            this.Name = "Cmn_MasterListOfRecords";
            this.Text = "iCORE DDS - List of Records";
            this.Load += new System.EventHandler(this.CRS_Cmn_MasterListOfRecords_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CRS_Cmn_MasterListOfRecords_KeyDown);
            this.Controls.SetChildIndex(this.grbListOfRecords, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.grpSearch, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grbListOfRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListOfRecords)).EndInit();
            this.panelSearchListOfRecord.ResumeLayout(false);
            this.panelSearchListOfRecord.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.grpSearch.ResumeLayout(false);
           // ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbListOfRecords;
        private System.Windows.Forms.PictureBox pcbCitiGroup;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.DataGridView dgvListOfRecords;
        private System.Windows.Forms.ComboBox cmbSearch;
        private System.Windows.Forms.ToolTip tltListOfRecords;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private CrplControlLibrary.CrplTextBox txbStringSearch;
        private CrplControlLibrary.CrplTextBox txtDigitSearch;
        private CrplControlLibrary.CrplTextBox txtDecimalSearch;
        private System.Windows.Forms.CheckBox chkBooleanSearch;
        private CrplControlLibrary.SLDatePicker dtpToDateSearch;
        private CrplControlLibrary.SLDatePicker dtpFromDateSearch;

        //private System.Windows.Forms.Panel panelSearchListOfRecord;
        private System.Windows.Forms.Label lblToDateSearch;
        private System.Windows.Forms.Label lblFromDateSearch;
        //private CrplControlLibrary.SLDatePicker dtpToDateSearch;
        //private CrplControlLibrary.SLDatePicker dtpFromDateSearch;
        //private System.Windows.Forms.CheckBox chkBooleanSearch;
        //private CrplControlLibrary.CrplTextBox txtDecimalSearch;
        //private CrplControlLibrary.CrplTextBox txtDigitSearch;
        //private System.Windows.Forms.ComboBox cmbSearch;
        //private System.Windows.Forms.Label lblSearch;
        //private CrplControlLibrary.CrplTextBox txbStringSearch;
        //private System.Windows.Forms.Button btnOK;
        //private System.Windows.Forms.Button btnCancel;
        //private System.Windows.Forms.GroupBox grpSearch;
        private System.Windows.Forms.DataGridView dgvSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox grpSearch;
        private System.Windows.Forms.Panel panelSearchListOfRecord;
    }
}