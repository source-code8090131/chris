﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections.Specialized;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using System.Reflection;

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    public enum mListType
    {
        M_SetupList = 1,
        LookupList = 2,
        AuthUnAuthList = 3
    }

    public partial class Cmn_MasterListOfRecords : Cmn_TemplateCR
    {
        private string _DataManagerName;
        private int _Id;
        private BusinessEntity m_oBusinessEntity;
        private DataRow _SelectedRecord;
        private DataTable recordsTable;
        private ListType _ListType = ListType.SetupList;
        StringCollection m_dicColumns;
        private char m_chrSeparator = '|';
        private string m_strColumnsToHide = "";
        private object[] _Values;
        private string m_strSPName;
        private string m_strActionType;
        private string m_strSearchText = string.Empty;
        private string m_strSearchColumn;
        private int m_RecordCount;
        private bool _isCustomSearch = false;

        protected internal string DataManagerName
        {
            set
            {
                this._DataManagerName = value;
            }
        }

        protected internal int Id
        {
            set
            {
                this._Id = value;
            }
            get
            {
                return this._Id;
            }
        }
        protected internal int RowCount
        {
            set
            {
                this.m_RecordCount = value;
            }
            get
            {
                return this.m_RecordCount;
            }
        }
        protected internal BusinessEntity Entity
        {
            set
            {
                this.m_oBusinessEntity = value;
            }
        }

        protected internal DataRow SelectedRecord
        {
            get
            {
                return this._SelectedRecord;
            }
        }

        protected internal DataTable DataSource
        {
            set
            {
                this.recordsTable = value;
            }
        }

        protected internal ListType ListType
        {
            set
            {
                this._ListType = value;
            }
        }

        protected internal object[] Values
        {
            set
            {
                this._Values = value;
            }
        }

        /// <summary>
        /// Enable Search
        /// </summary>
        protected internal bool IsCustomSearch
        {
            set
            {
                this._isCustomSearch = value;
            }
            get
            {
                return this._isCustomSearch;
            }
        }

        [Browsable(true)]
        [Description("Sets the comma separated column names to be hide from the grid.")]
        protected internal string ColumnToHide
        {
            get { return m_strColumnsToHide; }
            set
            {
                m_strColumnsToHide = value;
                this.AddColumnsToHide(m_strColumnsToHide);
            }
        }

        [Browsable(true)]
        [Description("Sets the string to be search in the grid.")]
        protected internal string SearchText
        {
            get { return m_strSearchText; }
            set
            {
                m_strSearchText = value;
            }
        }

        [Browsable(true)]
        [Description("Sets the string to be search in the grid.")]
        protected internal string SearchColumn
        {
            get { return m_strSearchColumn; }
            set
            {
                m_strSearchColumn = value;
            }
        }

        /// <summary>
        /// <Class constructor that is called when the RPS_F_ListOfRecords
        /// Interface instance is created.>       
        /// </summary>
        /// <Param></Param>
        /// <returns> None</returns>    
        public Cmn_MasterListOfRecords()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Overloaded Constructor
        /// </summary>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        public Cmn_MasterListOfRecords(string pstrSPName, string pstrActionType)
        {
            InitializeComponent();
            this.m_strSPName = pstrSPName;
            this.m_strActionType = pstrActionType;

            //XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mdiForm =
            //   this.MdiParent as XMS.PRESENTATIONOBJECTS.FORMS.MainMenu;

            //string userID = mdiForm.getXmsUser().getSoeId();
           // string userId = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
        }

        /// <summary>
        ///< Class Load event that is called when the RPS_F_ListOfRecords
        /// Interface is loaded.>       
        /// </summary>
        /// <Param sender = sender object,e = Event object></Param>
        /// <returns> None</returns>
        private void CRS_Cmn_MasterListOfRecords_Load(object sender, EventArgs e)
        {
            if (this._ListType == ListType.SetupList && this._isCustomSearch)
            {
                grpSearch.Visible = true;
                grbListOfRecords.Visible = false;
                this.PopulateSearchGrid();
            }
            else
            {
                //*********************** changing Layout************************
                //this.grbListOfRecords.SuspendLayout();
                this.grbListOfRecords.Location = new System.Drawing.Point(16, 65);
                this.grbListOfRecords.Size = new System.Drawing.Size(621, 484);
                this.dgvListOfRecords.Size = new System.Drawing.Size(597, 390);
                panelSearchListOfRecord.Location = new System.Drawing.Point(12, 430);
                //this.grbListOfRecords.ResumeLayout(true);

                this.PopulateGrid();
            }
            //if(this.m_strSearchText.Length > 0)
            //{
            //    SearchSelectedString();
            //}
        }

        /// <summary>
        ///< Method to populate grid.>       
        /// </summary>
        /// <Param></Param>
        /// <returns> None</returns>
        private void PopulateGrid()
        {
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(_DataManagerName);
            Result rslt = new Result();

            if (this.recordsTable == null)
            {
                if (this._ListType == ListType.SetupList) //ListType.AuthUnAuthList)
                {
                    if (this.m_oBusinessEntity == null)
                        rslt = dataManager.GetAll();
                    else
                    rslt = dataManager.GetAll(m_oBusinessEntity, this.m_strSPName, this.m_strActionType);
                }
                else if (this._ListType == ListType.AuthUnAuthList) //ListType.SetupList)
                {
                    if (this._Values == null || this._Values.Length == 0)
                        rslt = dataManager.GetAllUnAuthorized();
                    else
                        rslt = dataManager.GetAllUnAuthorized(this._Values);
                }
                else if (this._ListType == ListType.LookupList)
                {
                    if (this._Values == null || this._Values.Length == 0)
                        rslt = dataManager.GetAllAuthorized();
                    else
                        rslt = dataManager.GetAllAuthorized(this._Values);
                }

                if (rslt.isSuccessful)
                    this.recordsTable = rslt.dstResult.Tables[0];
            }

            if (this.recordsTable != null && this.recordsTable.Rows.Count == 0)
            {
                this.ShowInformationMessage("", ApplicationMessages.NO_RECORDS_FOUND_MESSAGE);
                this.RowCount = 0;
                this.Close();
            }
            else if (this.recordsTable != null)
            {
                this.dgvListOfRecords.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                this.dgvListOfRecords.DataSource = recordsTable;
                this.RowCount = recordsTable.Rows.Count;
                for (int i = 0; i < this.dgvListOfRecords.Columns.Count; i++)
                {
                    dgvListOfRecords.Columns[i].HeaderText = ConvertToPascal(dgvListOfRecords.Columns[i].Name).Replace("_", " ");
                    if (this.dgvListOfRecords.Columns[i].Name.ToUpper().Equals("ID"))
                        this.dgvListOfRecords.Columns[i].Visible = false;
                    else
                    {
                        //this.cmbSearch.Items.Add(dgvListOfRecords.Columns[i].Name);
                        this.cmbSearch.Items.Add(ConvertToPascal(dgvListOfRecords.Columns[i].Name));
                    }
                }
                this.HideGridColumns();
                if (this._Id > 0)
                {
                    for (int i = 0; i < this.dgvListOfRecords.Rows.Count; i++)
                    {
                        if (this.dgvListOfRecords.Rows[i].Cells[0].Value.ToString().Equals(this._Id.ToString()))
                        {
                            foreach (DataGridViewCell dgvc in dgvListOfRecords.Rows[i].Cells)
                            {
                                if (dgvc.Visible)
                                {
                                    this.dgvListOfRecords.CurrentCell = dgvc;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataGridViewCell dgvc in dgvListOfRecords.Rows[0].Cells)
                    {
                        if (dgvc.Visible)
                        {
                            this.dgvListOfRecords.CurrentCell = dgvc;
                            break;
                        }
                    }
                }
            }

            if (this.cmbSearch.Items.Count > 0)
                this.cmbSearch.SelectedIndex = 0;

            this.tlbMain.Visible = false;

            FocusOnFilterControl();
        }

        /// <summary>
        /// Hidden columns
        /// </summary>
        /// <param name="pstrParamName"></param>
        protected void AddColumnsToHide(string pstrParamName)
        {
            if (m_dicColumns == null)
                m_dicColumns = new StringCollection();

            string[] strColumn = pstrParamName.Split(m_chrSeparator);
            int iLoop = 0;

            for (; iLoop < strColumn.Length; iLoop++)
            {
                if (!m_dicColumns.Contains(strColumn[iLoop]))
                    m_dicColumns.Add(strColumn[iLoop].Trim());
            }
        }

        /// <summary>
        /// Hide Columns
        /// </summary>
        private void HideGridColumns()
        {
            if (m_dicColumns != null && m_dicColumns.Count > 0)
            {
                int iLoop = 0;
                for (; iLoop < m_dicColumns.Count; iLoop++)
                {
                    if (this.dgvListOfRecords.Columns.Contains(m_dicColumns[iLoop]))
                    {
                        this.dgvListOfRecords.Columns[m_dicColumns[iLoop]].Visible = false;
                        string val = this.dgvListOfRecords.Columns[m_dicColumns[iLoop]].HeaderText.Replace(" ", "_");
                        this.cmbSearch.Items.Remove(val);
                    }
                }
            }
        }

        /// <summary>
        /// Grid Double click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvListOfRecords_RowDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                this.SetValues(e.RowIndex);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// Grid Header Double click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvListOfRecords_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.SetValues(e.RowIndex);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Pass seleted record to calling screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click_1(object sender, EventArgs e)
        {

            int rowIndex = 0;

            if (this.dgvListOfRecords.Rows.Count == 0)
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else if (this.dgvListOfRecords.SelectedCells.Count > 0 || this.dgvListOfRecords.SelectedRows.Count > 0)
            {
                rowIndex = this.dgvListOfRecords.SelectedRows.Count > 0 ?
                    this.dgvListOfRecords.SelectedRows[0].Index : this.dgvListOfRecords.SelectedCells[0].RowIndex;

                this.SetValues(rowIndex);
            }

            //int rowIndex = 0;

            //if (this.dgvListOfRecords.SelectedCells.Count > 0 || this.dgvListOfRecords.SelectedRows.Count > 0)
            //{
            //    rowIndex = this.dgvListOfRecords.SelectedRows.Count > 0 ?
            //        this.dgvListOfRecords.SelectedRows[0].Index : this.dgvListOfRecords.SelectedCells[0].RowIndex;

            //    this.SetValues(rowIndex);
            //}
        }
        /// <summary>
        ///< Method to set values.>       
        /// </summary>
        /// <Param rowIndex = Index of the row></Param>
        /// <returns> None</returns>
        private void SetValues(int rowIndex)
        {
            if (rowIndex != -1)
            {
                this._SelectedRecord = ((DataRowView)dgvListOfRecords.CurrentRow.DataBoundItem).Row;

                try
                {
                    this._Id = Convert.ToInt32(this.dgvListOfRecords.Rows[rowIndex].Cells[0].Value);
                }
                catch { }
            }
        }

        /// <summary>
        ///< Event on selected index changed.>       
        /// </summary>
        /// <Param sender = sender object,e = Event object></Param>
        /// <returns> None</returns>
        private void cmbSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSystemType = this.recordsTable.Columns[cmbSearch.Text].DataType.ToString();

            switch (strSystemType)
            {
                case "System.String":
                    this.HideAll();
                    this.txbStringSearch.Visible = true;
                    break;
                case "System.Int16":
                case "System.Int32":
                case "System.Int64":
                    this.HideAll();
                    this.txtDigitSearch.Visible = true;
                    break;
                case "System.DateTime":
                    this.HideAll();
                    this.lblFromDateSearch.Visible = dtpFromDateSearch.Visible = lblToDateSearch.Visible =
                        this.dtpToDateSearch.Visible = true;
                    break;
                case "System.Single":
                case "System.Double":
                case "System.Decimal":
                    this.HideAll();
                    this.txtDecimalSearch.Visible = true;
                    break;
                case "System.Boolean":
                    this.HideAll();
                    this.chkBooleanSearch.Visible = true;
                    this.chkBooleanSearch.Text = cmbSearch.Text;
                    break;
            }
        }

        /// <summary>
        /// Record Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txbStringSearch_TextChanged(object sender, EventArgs e)
        {
            this.dgvListOfRecords.DataSource = new DataView(recordsTable,
                cmbSearch.Text + " LIKE '" + this.txbStringSearch.Text + "%'", "", DataViewRowState.CurrentRows);
        }

        /// <summary>
        /// Record Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDecimalSearch_TextChanged(object sender, EventArgs e)
        {
            this.dgvListOfRecords.DataSource = new DataView(recordsTable, txtDecimalSearch.Text.Length == 0 ? "" :
                cmbSearch.Text + " = " + this.txtDecimalSearch.Text, "", DataViewRowState.CurrentRows);
        }

        /// <summary>
        /// Record Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDigitSearch_TextChanged(object sender, EventArgs e)
        {
            this.dgvListOfRecords.DataSource = new DataView(recordsTable, txtDigitSearch.Text.Length == 0 ? "" :
                cmbSearch.Text + " = " + this.txtDigitSearch.Text, "", DataViewRowState.CurrentRows);
        }

        /// <summary>
        /// Record Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkBooleanSearch_CheckedChanged(object sender, EventArgs e)
        {
            this.dgvListOfRecords.DataSource = new DataView(recordsTable,
                cmbSearch.Text + " = " + (this.chkBooleanSearch.Checked ? 1 : 0), "", DataViewRowState.CurrentRows);
        }

        /// <summary>
        /// Record Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpFromDateSearch_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.dgvListOfRecords.DataSource = new DataView(recordsTable,
                    cmbSearch.Text + " >= #" + ((DateTime)this.dtpFromDateSearch.Value).ToString("MM/dd/yyyy") + " 00:00:00# AND " +
                    cmbSearch.Text + " <= #" + ((DateTime)this.dtpToDateSearch.Value).ToString("MM/dd/yyyy") + " 23:59:59#", "",
                    DataViewRowState.CurrentRows);
            }
            catch (Exception err)
            {
            }
        }

        /// <summary>
        /// Record Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpToDateSearch_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.dgvListOfRecords.DataSource = new DataView(recordsTable,
                    cmbSearch.Text + " >= #" + ((DateTime)this.dtpFromDateSearch.Value).ToString("MM/dd/yyyy") + " 00:00:00# AND " +
                    cmbSearch.Text + " <= #" + ((DateTime)this.dtpToDateSearch.Value).ToString("MM/dd/yyyy") + " 23:59:59#", "",
                    DataViewRowState.CurrentRows);
            }
            catch (Exception err)
            {
            }
        }
        /// <summary>
        ///<Method to hide controls.>       
        /// </summary>
        /// <Param sender = sender object,e = Event object></Param>
        /// <returns> None</returns>
        private void HideAll()
        {
            this.txbStringSearch.Visible = this.txtDigitSearch.Visible = this.txtDecimalSearch.Visible =
                this.chkBooleanSearch.Visible = this.lblFromDateSearch.Visible = this.dtpFromDateSearch.Visible =
                this.lblToDateSearch.Visible = this.dtpToDateSearch.Visible = false;
        }

        /// <summary>
        /// Handles Enter Key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CRS_Cmn_MasterListOfRecords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                this.btnOK_Click_1(this.btnOK, new EventArgs());

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// This function converts a string into "Pascal Case" format.
        /// Please note that each word in the string has to be separated by either a space (" "), hyphen ("-") or underscore ("_"). 
        /// </summary>
        /// <param name="orginalString">Original string that is to be converted.</param>
        /// <returns>Result string converted to camel case.</returns>
        private String ConvertToPascal(String originalString)
        {
            String oString = String.Empty;
            int iLength = originalString.Length - 1;
            int p = 0;
            Char iChar;
            bool upperFlag = false;

            originalString = originalString.ToLower();
            while (p <= iLength)
            {
                //strip each character out of the string one by one
                iChar = originalString.Substring(p, 1).ToCharArray()[0];

                // change first character to upper case 
                if (p.Equals(0))
                    upperFlag = true;

                // If a blank, hyphen or underscore field encountered, raise the flag to mark the next character as upper case 
                if (iChar.Equals(' ') || iChar.Equals('-') || iChar.Equals('_'))
                    upperFlag = true;
                else
                {
                    if (upperFlag)
                    {
                        iChar = iChar.ToString().ToUpper().ToCharArray()[0];
                        upperFlag = false;
                    }
                }
                oString = oString + iChar;
                p += 1;
            }
            return (oString);
        }
        /// <summary>
        /// Gets 1st datarow to b passed for Filling Entity
        /// </summary>
        /// 
        private void FillEntityFromDataRow()
        {
            DataRow drow = ((DataRowView)dgvSearch.Rows[0].DataBoundItem).Row;
            SetEntityProperties(drow);
        }

        /// <summary>
        /// Set Entity Properties
        /// </summary>
        /// <param name="oDR"></param>
        private void SetEntityProperties(DataRow oDR)
        {
            PropertyInfo newProperty;

            foreach (DataColumn oColLoop in oDR.Table.Columns)
            {
                newProperty = m_oBusinessEntity.GetType().GetProperty(oColLoop.ColumnName);

                if (newProperty != null && newProperty.CanWrite)
                {
                    switch (newProperty.PropertyType.Name)
                    {
                        case "DateTime":
                            DateTime oDateTime;
                            if (DateTime.TryParse(oDR[oColLoop.ColumnName].ToString(), out oDateTime))
                                newProperty.SetValue(m_oBusinessEntity, Convert.ToDateTime(oDR[oColLoop.ColumnName].ToString()), null);
                            break;
                        case "Int32":
                            Int32 oInt32;
                            if (Int32.TryParse(oDR[oColLoop.ColumnName].ToString(), out oInt32))
                                newProperty.SetValue(m_oBusinessEntity, Convert.ToInt32(oDR[oColLoop.ColumnName].ToString()), null);
                            break;
                        case "Single":
                            Single oSingle;
                            if (Single.TryParse(oDR[oColLoop.ColumnName].ToString(), out oSingle))
                                newProperty.SetValue(m_oBusinessEntity, Convert.ToSingle(oDR[oColLoop.ColumnName].ToString()), null);
                            break;

                        case "Double":
                            Double oDouble;
                            if (Double.TryParse(oDR[oColLoop.ColumnName].ToString(), out oDouble))
                                newProperty.SetValue(m_oBusinessEntity, Convert.ToDouble(oDR[oColLoop.ColumnName].ToString()), null);
                            break;
                        case "Boolean":
                            bool oBool;
                            if (bool.TryParse(oDR[oColLoop.ColumnName].ToString(), out oBool))
                                newProperty.SetValue(m_oBusinessEntity, Convert.ToBoolean(oDR[oColLoop.ColumnName]), null);
                            break;
                        default:
                            newProperty.SetValue(m_oBusinessEntity, oDR[oColLoop.ColumnName].ToString(), null);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// gets Sp params and populates grid for Searching.
        /// </summary>
        private void PopulateSearchGrid()
        {
            try
            {
                IDataManager dataManager = RuntimeClassLoader.GetDataManager(_DataManagerName);
                Result rslt = new Result();
                if (this._ListType == ListType.SetupList)
                {
                    DataSet dst = new DataSet();
                    dst.Tables.Add(SQLManager.GetSPParams(this.m_strSPName));
                    if (dst.Tables[0] != null)
                    {
                        DataRow drow = dst.Tables[0].NewRow();
                        dst.Tables[0].Rows.Add(drow);
                        this.dgvSearch.DataSource = dst.Tables[0];

                        for (int i = 0; i < this.dgvSearch.Columns.Count; i++)
                        {
                            dgvSearch.Columns[i].HeaderText = ConvertToPascal(dgvSearch.Columns[i].Name).Replace("_", " ");
                            if (this.dgvSearch.Columns[i].Name.ToUpper().Equals("ID") || this.dgvSearch.Columns[i].Name.ToUpper().Equals("ACTIONTYPE") || this.dgvSearch.Columns[i].Name.ToUpper().Equals("ORETVAL"))
                                this.dgvSearch.Columns[i].Visible = false;

                        }

                    }
                }
            }
            catch (Exception e)
            { }
        }


        /// <summary>
        /// FillEntityFromDataRow(),SetEntityProperties(),PopulateSearchGrid()
        /// ,SearchFilterExist() and handler btnSearch_Click are added for Search In LOV.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSearch.DataSource as DataTable != null)
                {
                    if (dgvSearch.EndEdit())
                    {
                        if (SearchFilterExist())
                        {
                            recordsTable = null;
                            FillEntityFromDataRow();
                            PopulateGrid();
                            grbListOfRecords.Visible = true;
                        }
                        else
                        {
                            if (DialogResult.Yes == MessageBox.Show("Empty Search Filter will Fetch bulk Data.Do you want to Continue?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                recordsTable = null;
                                FillEntityFromDataRow();
                                PopulateGrid();
                                grbListOfRecords.Visible = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// SearchFilterExist
        /// </summary>
        /// <returns></returns>
        private bool SearchFilterExist()
        {
            bool searchFilterExist = false;
            if (dgvSearch.DataSource as DataTable != null)
            {
                foreach (DataGridViewColumn dcol in dgvSearch.Columns)
                {
                    if (dcol.Visible)
                    {
                        if (dgvSearch[dcol.Name, 0].Value != null && dgvSearch[dcol.Name, 0].Value.ToString() != string.Empty)
                        {
                            return true;
                        }
                    }
                }
                return searchFilterExist;
            }
            return false;

        }

        /// <summary>
        /// Set Record Filters
        /// </summary>
        private void SearchSelectedString()
        {
            string strSystemType = this.recordsTable.Columns[this.m_strSearchColumn].DataType.ToString();

            switch (strSystemType)
            {
                case "System.String":
                    this.HideAll();
                    this.txbStringSearch.Visible = true;
                    this.txbStringSearch.Text = this.SearchText;
                    txbStringSearch_TextChanged(null, null);
                    break;
                case "System.Int16":
                case "System.Int32":
                case "System.Int64":
                    this.HideAll();
                    this.txtDigitSearch.Visible = true;
                    this.txtDigitSearch.Text = this.SearchText;
                    txtDigitSearch_TextChanged(null, null);
                    break;
                case "System.DateTime":
                    this.HideAll();
                    this.lblFromDateSearch.Visible = dtpFromDateSearch.Visible = lblToDateSearch.Visible =
                        this.dtpToDateSearch.Visible = true;
                    break;
                case "System.Single":
                case "System.Double":
                case "System.Decimal":
                    this.HideAll();
                    this.txtDecimalSearch.Visible = true;
                    txtDecimalSearch.Text = this.SearchText;
                    txtDecimalSearch_TextChanged(null, null);
                    break;
                case "System.Boolean":
                    this.HideAll();
                    this.chkBooleanSearch.Visible = true;
                    this.chkBooleanSearch.Text = cmbSearch.Text;
                    break;
            }

            int rowIndex = 0;

            if (this.dgvListOfRecords.Rows.Count == 0)
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else if (this.dgvListOfRecords.SelectedCells.Count > 0 || this.dgvListOfRecords.SelectedRows.Count > 0)
            {
                //rowIndex = this.dgvListOfRecords.SelectedRows.Count > 0 ?
                //    this.dgvListOfRecords.SelectedRows[0].Index : this.dgvListOfRecords.SelectedCells[0].RowIndex;

                if (dgvListOfRecords.Rows.Count == 1)
                {
                    this.SetValues(0);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }

            }
        }

        /// <summary>
        /// Sets the focus on the first visible Filter control on List Popup.
        /// </summary>
        private void FocusOnFilterControl()
        {
            if (dtpFromDateSearch.Visible)
            {
                dtpFromDateSearch.Focus();
                dtpFromDateSearch.Select();
            }
            else if (chkBooleanSearch.Visible)
            {
                chkBooleanSearch.Focus();
                chkBooleanSearch.Select();
            }
            else if (txbStringSearch.Visible)
            {
                txbStringSearch.Focus();
                txbStringSearch.Select();
            }
            else if (txtDigitSearch.Visible)
            {
                txtDigitSearch.Focus();
                txtDigitSearch.Select();
            }
            else if (txtDecimalSearch.Visible)
            {
                txtDecimalSearch.Focus();
                txtDecimalSearch.Select();
            }
        }

        //private void btnOK_Click_1(object sender, EventArgs e)
        //{

        //}

        //private void Cmn_MasterListOfRecords_Load(object sender, EventArgs e)
        //{

        //}
    }
}
