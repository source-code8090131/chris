using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using CrplControlLibrary;

/*****************************************************************
  Class Name:  InputValidator 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Shamraiz Ahmad
  Created Date: 22-08-2007
  Version No: 1.0
  Modification: <Mention the modification in the class >               
  Modified By:
  Modification Date:
  Version No: <The version No after modification>

*****************************************************************/


namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    public enum Mode
    {
        View = 0,
        Edit = 1,
        Add = 2,
        Cancel=3
    }
    public class InputValidator
    {
        public const string NUMBER_REGEX = @"\d+";
        public const string DECIMAL_REGEX = @"\d+(\.\d+)?";
        public const string ONEDECIMAL_REGEX = @"\d+.\d{1}";
        public const string TWODECIMAL_REGEX = @"\d+.\d{2}";
        public const string NICNO_REGEX = @"\d{5}-\d{7}-\d";
        public const string OldNICNO_REGEX = @"\d{3}-\d{2}-\d{6}";
        public const string ACCOUNTNO_REGEX = @"\d{1}-\d{6}-\d{3}";
        public const string PASSPORTNO_REGEX = @"";
        public const string EMAIL_REGEX = @"[a-zA-Z0-9][a-zA-Z0-9_\-]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,3}";
        public const string URL_REGEX = @"(http://)?(www\.)?[a-zA-Z0-9][a-zA-Z0-9_\-]+\.[a-zA-Z]{2,3}/?\S*(?x)";
        public const string TELEPHONE_REGEX = @"\((\d{3})\)\s*(\d{3}(?:-|\s*)\d{4})(?x)";
        public const string CELLNO_REGEX = @"0\d{3}-\d{7}";
        public const string NTNNO_REGEX = @"";
        public const string DATE_REGEX = @"\d{1,2}/\d{1,2}/\d{4}";
        public const string DATE_FORMAT_REGEX = @"\d{6}";
        public const string TIME_REGEX = @"\d{1,2}:\d{1,2}:\d{1,2}";
        public const string DATETIME_REGEX = @"\d{1,2}/\d{1,2}/\d{4}\s\d{1,2}:\d{1,2}:\d{1,2}((\s(AM))|(\s(PM)))?";
        public const string ZIPCODE_REGEX = @"\d{5}";

        private const string numberFailureMessage = " is not valid.";
        private const string decimalFailureMessage = " is not valid.";
        private const string nicNoFailureMessage = "NIC No is not valid.";
        private const string oldNICNoFailureMessage = "NIC No is not valid.";
        private const string passportNoFailureMessage = "Passport No is not valid.";
        private const string emailFailureMessage = "Email Address is not valid.";
        private const string urlFailureMessage = "URL is not valid.";
        private const string telephoneNoFailureMessage = " format is not valid.";
        private const string cellNoFailureMessage = "Cell No format is not valid.";
        private const string ntnNoFailureMessage = "NTN No is not valid.";
        private const string dateFailureMessage = "Date is not valid.";
        private const string timeFailureMessage = "Time is not valid.";
        private const string dateTimeFailureMessage = "DateTime is not valid.";
        private const string zipCodeFailureMessage = "ZipCode is not valid.";

        private const string emptyMessage = " cannot be empty.";
        private const string selectionMessage = "Please select an item from ";

        public static bool IsValidNumericValue(string numericValue, string controlCaption, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(numericValue, NUMBER_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, controlCaption + numberFailureMessage);
            }

            return result;
        }

        public static bool IsValidDecimalValue(string decimalValue, string controlCaption, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(decimalValue, DECIMAL_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, controlCaption + decimalFailureMessage);
            }

            return result;
        }

        public static bool IsValidNICNo(string nicNo, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(nicNo, NICNO_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, nicNoFailureMessage);
            }

            return result;
        }

        public static bool IsValidOldNICNo(string oldNICNo, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(oldNICNo, OldNICNO_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, oldNICNoFailureMessage);
            }

            return result;
        }

        public static bool IsValidPassportNo(string passportNo, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(passportNo, PASSPORTNO_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, passportNoFailureMessage);
            }

            return result;
        }

        public static bool IsValidEmail(string email, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(email, EMAIL_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, emailFailureMessage);
            }

            return result;
        }

        public static bool IsValidURL(string url, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(url, URL_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, urlFailureMessage);
            }

            return result;
        }

        public static bool IsValidTelephoneNo(string telephoneNo, string controlCaption, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(telephoneNo, TELEPHONE_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, controlCaption + telephoneNoFailureMessage);
            }

            return result;
        }

        public static bool IsValidCellNo(string cellNo, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(cellNo, CELLNO_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, cellNoFailureMessage);
            }

            return result;
        }

        public static bool IsValidNTNNo(string ntnNo, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(ntnNo, NTNNO_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, ntnNoFailureMessage);
            }

            return result;
        }

        public static bool IsValidDate(string date, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(date, DATE_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, dateFailureMessage);
            }
            else
            {
                try
                {
                    DateTime.Parse(date);
                }
                catch
                {
                    result = false;
                    errProvider.SetError(ctrl, dateFailureMessage);
                }
            }

            return result;
        }

        //Added By Umair for mmyyyy Date Format
        public static bool IsValidDateFormat(string date, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(date, DATE_FORMAT_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, dateFailureMessage);
            }
            else
            {
                try
                {
                    string m_Date = date.ToString();
                    string m_Month = m_Date.Substring(0, 2);
                    string m_Year = m_Date.Substring(2, 4);
                    int m_InValidMonth = 12;
                    int m_InValidYear = 1900;
                    
                    if (Convert.ToInt16(m_Month) <= m_InValidMonth && Convert.ToInt16(m_Year) >= m_InValidYear)
                    {
                        
                    }
                    else
                    {
                        result = false;
                        errProvider.SetError(ctrl, dateFailureMessage);
                    }

                    //DateTime.Parse(date);

                }
                catch
                {
                    result = false;
                    errProvider.SetError(ctrl, dateFailureMessage);
                }
            }

            return result;
        }



        public static bool IsValidTime(string time, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(time, TIME_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, timeFailureMessage);
            }
            else
            {
                try
                {
                    string[] arr = time.Split(':');
                    int hour;
                    int minute;
                    int second;

                    hour = Convert.ToInt32(arr[0]);
                    minute = Convert.ToInt32(arr[1]);
                    second = Convert.ToInt32(arr[2]);

                    if (!(hour >= 0 && hour < 24 && minute >= 0 && minute < 59 && second >= 0 && second < 59))
                    {
                        result = false;
                        errProvider.SetError(ctrl, timeFailureMessage);
                    }
                }
                catch
                {
                    result = false;
                    errProvider.SetError(ctrl, timeFailureMessage);
                }
            }

            return result;
        }

        public static bool IsValidDateTime(string dateTime, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(dateTime, DATETIME_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, dateTimeFailureMessage);
            }
            else
            {
                try
                {
                    DateTime.Parse(dateTime);
                }
                catch
                {
                    result = false;
                    errProvider.SetError(ctrl, dateTimeFailureMessage);
                }
            }

            return result;
        }

        public static bool IsZipCode(string zipCode, ErrorProvider errProvider, Control ctrl)
        {
            bool result = true;

            if (!IsValidInput(zipCode, ZIPCODE_REGEX))
            {
                result = false;
                errProvider.SetError(ctrl, zipCodeFailureMessage);
            }

            return result;
        }

        public static bool IsValidInput(string input, string regex)
        {
            Match mat = Regex.Match(input, regex);

            return mat.Success && mat.Value.Equals(input);
        }

        public static bool IsSelected(string controlCaption, ErrorProvider errProvider, ComboBox cmb)
        {
            bool result = true;

            if (cmb.SelectedText.Equals("") || cmb.SelectedValue.Equals("0"))
            {
                errProvider.SetError(cmb, selectionMessage + controlCaption + ".");
                result = false;
            }

            return result;
        }

        public static bool IsNotEmpty(string controlCaption, ErrorProvider errProvider, TextBox txt)
        {
            bool result = true;

            txt.Text = txt.Text.Trim();

            if (txt.Text.Equals(""))
            {
                errProvider.SetError(txt, controlCaption + emptyMessage);
                result = false;
            }
                //Following Else added by Khateeb
                //Date: 29th June 2007
                //Purpose: This statement ensures that the status of Error Provider is updated
            else 
            {
                errProvider.SetError(txt, "");
                result = true;
            }

            return result;
        }

        public static bool IsNotEmpty(string controlCaption, ErrorProvider errProvider, SLDatePicker dtPicker)
        {
            bool result = true;

            if (dtPicker.Value == null)
            {
                errProvider.SetError(dtPicker, controlCaption + emptyMessage);
                result = false;
            }
            else
            {
                errProvider.SetError(dtPicker, "");
                result = true;
            }

            return result;
        }

#region Grid Validation

        public static bool IsEmpty(string controlCaption, DataGridViewCellValidatingEventArgs e, DataGridView DG )
        {
           bool result = false;
          // if (DG.Columns[e.ColumnIndex].Name == controlName)
            {
                if (String.IsNullOrEmpty(e.FormattedValue.ToString()))
                {
                    e.Cancel = true;
                    MessageBox.Show(controlCaption + " must not be empty");
                    result = true;

                }
                
            }
            return result;
        }

        public static bool IsDate(string controlCaption, string controlName,  DataGridViewCellValidatingEventArgs e, DataGridView DG)
        {
            bool result = true;
            if (DG.Columns[e.ColumnIndex].Name == controlName)
            {
                if (!IsValidInput(e.FormattedValue.ToString(), DATE_REGEX))
                {
                    e.Cancel = true;
                    MessageBox.Show("Entry Date Format MM/DD/YYYY of " + controlCaption );
                    result = false;
                }
                result = true;
            }
            return result;
        }
#endregion

    }
}
