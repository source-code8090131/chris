using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Data;
using Microsoft.Office.Interop.Excel;

namespace iCORE.Common
{
    class ExcelReportManager : IDisposable 
    {
        private String str_FileName;
        private String str_ReportHeader;
        private String str_FilePath;
        private String str_FileExt = ".xls";
        private Application appExcel;
        private Workbooks wbks;
        Sheets shts;

        public String FileName
        {
            get { return str_FileName; }
            set { str_FileName = value; }
        }

        public String FilePath
        {
            get { return str_FilePath; }
            set { str_FilePath = value; }
        }
        
        public String ReportHeader
        {
            get { return str_ReportHeader; }
            set { str_ReportHeader = value; }
        }
        
        public Application AppExcel
        {
            get { return appExcel; }
        }
       
        public Workbooks WrkBooks
        {
            get { return wbks; }
        }

        public ExcelReportManager()
        {
            appExcel = new Application();
            if (appExcel != null)
            {
                appExcel.Visible = false;
            }
            else
                throw new Exception("ERROR: EXCEL application couldn't be started!");
        }

        /// <summary>
        /// AddWorkBook
        /// </summary>
        /// <returns></returns>
        private _Workbook AddWorkBook()
        {
            try
            {
                if (appExcel != null)
                {
                    wbks = appExcel.Workbooks;
                    _Workbook wbk = wbks.Add(XlWBATemplate.xlWBATWorksheet);
                    return wbk;
                }
                else
                    throw new Exception("ERROR: EXCEL application couldn't be started!, Failed to Add Work Book");
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetWorkBookSheet
        /// </summary>
        /// <param name="wb"></param>
        /// <returns></returns>
        private _Worksheet GetWorkBookSheet(_Workbook wb)
        {
            try
            {
                if (appExcel != null)
                {
                    shts = wb.Sheets;
                    _Worksheet wbsht = (_Worksheet)shts.get_Item(1);
                    return wbsht;
                }
                else
                    throw new Exception("ERROR: EXCEL application couldn't be started!, Failed to Add Sheet");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GenerateExcelFile
        /// </summary>
        /// <param name="dsExcel"></param>
        /// <param name="lRptParam"></param>
        /// <param name="lColumnName"></param>
        public void GenerateExcelFile(DataSet dsExcel, List<Dictionary<String, String>> lRptParam, List<String> lColumnName)
        {
            int iRowNumber = 1;

            // Add the new sheet and got the handler of current sheet
            _Workbook wbk = AddWorkBook();
            _Worksheet wsht = GetWorkBookSheet(wbk);
            AddReportHeader(wsht, iRowNumber++);
            foreach (Dictionary<String, String> dictRptParam in lRptParam)
                AddRepotParameterList(wsht, iRowNumber++, dictRptParam);
            iRowNumber++;
            AddRepotColumnName(wsht, iRowNumber++, lColumnName);
            iRowNumber++;
            AddRepotColumnData(wsht, ref iRowNumber, dsExcel);

            object misValue = System.Reflection.Missing.Value;
            try
            {
                wbk.SaveAs(str_FilePath + str_FileName + str_FileExt, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlNoChange, misValue, misValue, misValue, misValue, misValue);
            }
            catch
            {
                wbk.SaveCopyAs(str_FilePath + str_FileName + str_FileExt);
            }
            wbk.Close(true, misValue, misValue);
            appExcel.Quit();

            releaseObject(wsht);
            releaseObject(wbk);
            releaseObject(appExcel);
        }

        /// <summary>
        /// AddReportHeader
        /// </summary>
        /// <param name="wbksht"></param>
        /// <param name="iRow"></param>
        private void AddReportHeader(_Worksheet wbksht, int iRow)
        {
            try
            {
                int iColumn = 1;
                if (appExcel != null && wbksht != null)
                {
                    wbksht.Cells[iRow, iColumn] = str_ReportHeader;
                }
                else
                    throw new Exception("ERROR: EXCEL application couldn't be started!, Failed to insert report header");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// AddRepotParameterList
        /// </summary>
        /// <param name="wbksht"></param>
        /// <param name="iRow"></param>
        /// <param name="dRptParam"></param>
        private void AddRepotParameterList(_Worksheet wbksht, int iRow, Dictionary<String, String> dRptParam)
        {
            try
            {
                if (appExcel != null && wbksht != null)
                {
                    int iColumn = 1;
                    foreach (KeyValuePair<String, String> kvPair in dRptParam)
                    {
                        wbksht.Cells[iRow, iColumn] = (kvPair.Key.Equals("") ? "" : (kvPair.Key + ": ")) + kvPair.Value;
                        iColumn += 3;
                    }
                }
                else
                    throw new Exception("ERROR: EXCEL application couldn't be started!, Failed to insert report parameter");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// AddRepotColumnName
        /// </summary>
        /// <param name="wbksht"></param>
        /// <param name="iRow"></param>
        /// <param name="lColumnName"></param>
        private void AddRepotColumnName(_Worksheet wbksht, int iRow, List<String> lColumnName)
        {
            try
            {
                if (appExcel != null && wbksht != null)
                {
                    int iColumn = 1;
                    foreach (String sColumnName in lColumnName)
                    {
                        wbksht.Cells[iRow, iColumn++] = sColumnName;
                    }
                }
                else
                    throw new Exception("ERROR: EXCEL application couldn't be started!, Failed to insert report parameter");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// AddRepotColumnData
        /// </summary>
        /// <param name="wbksht"></param>
        /// <param name="iRow"></param>
        /// <param name="ds"></param>
        private void AddRepotColumnData(_Worksheet wbksht, ref int iRow, DataSet ds)
        {
            try
            {
                if (appExcel != null && wbksht != null)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            int iColumn = 1;
                            foreach (object obj in dr.ItemArray)
                                if (obj.GetType() == typeof(DateTime))
                                    wbksht.Cells[iRow, iColumn++] = ((DateTime)obj).ToString("dd-MMM-yyyy");
                                else
                                    wbksht.Cells[iRow, iColumn++] = obj.ToString();
                            iRow++;
                        }
                    }
                }
                else
                    throw new Exception("ERROR: EXCEL application couldn't be started!, Failed to insert report parameter");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #region IDisposable Members
        public void Dispose()
        {
            //foreach (_Workbook wbk in appExcel.Workbooks)
            //    foreach (_Worksheet wrksht in wbk.Sheets)
            //        releaseObject(wrksht);
            //foreach (_Workbook wbk in appExcel.Workbooks)
            //    releaseObject(wbk);
            //releaseObject(appExcel);
        }
       
        /// <summary>
        /// releaseObject
        /// </summary>
        /// <param name="obj"></param>
        private void releaseObject(object obj)
        {
            try
            {
                Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion
    }
}
