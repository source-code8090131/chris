

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUNDCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PFUND"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/31/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PFUNDCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_PF_PR_P_NO;
        private String m_PF_YEAR;
        private decimal m_PF_EMP_CONT;
        private decimal m_PF_BNK_CONT;
        private decimal m_PF_INTEREST;
        private decimal m_PF_PREMIUM;
        private String m_Year;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PF_PR_P_NO"
        public decimal PF_PR_P_NO
        {
            get { return m_PF_PR_P_NO; }
            set { m_PF_PR_P_NO = value; }
        }
        #endregion

        #region "PF_YEAR"
        public String PF_YEAR
        {
            get { return m_PF_YEAR; }
            set { m_PF_YEAR = value; }
        }
        #endregion

        #region "PF_EMP_CONT"
        public decimal PF_EMP_CONT
        {
            get { return m_PF_EMP_CONT; }
            set { m_PF_EMP_CONT = value; }
        }
        #endregion

        #region "PF_BNK_CONT"
        public decimal PF_BNK_CONT
        {
            get { return m_PF_BNK_CONT; }
            set { m_PF_BNK_CONT = value; }
        }
        #endregion

        #region "PF_INTEREST"
        public decimal PF_INTEREST
        {
            get { return m_PF_INTEREST; }
            set { m_PF_INTEREST = value; }
        }
        #endregion

        #region "PF_PREMIUM"
        public decimal PF_PREMIUM
        {
            get { return m_PF_PREMIUM; }
            set { m_PF_PREMIUM = value; }
        }
        #endregion

        #region "Year"
        public string Year
        {
            get { return m_Year; }
            set { m_Year = value; }

        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PF_PR_P_NO = "PF_PR_P_NO";
        public static readonly string _PF_YEAR = "PF_YEAR";
        public static readonly string _PF_EMP_CONT = "PF_EMP_CONT";
        public static readonly string _PF_BNK_CONT = "PF_BNK_CONT";
        public static readonly string _PF_INTEREST = "PF_INTEREST";
        public static readonly string _PF_PREMIUM = "PF_PREMIUM";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
