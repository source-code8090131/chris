

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabQueryCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "BONUS_TAB"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Ahad Zubair]	01/07/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class BonusTabQueryCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"

        private Double m_PR_P_NO;
        private DateTime m_BO_10_D;
        private DateTime m_BO_INC_D;
        private decimal m_BO_INC_AMOUNT;
        private decimal m_BO_10C_BONUS;
        private String m_BO_INC_APP;
        private String m_BO_10C_APP;
        private String m_BO_10_USE;
        private String m_BO_INC_USE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion
        #endregion

        #region "BO_10_D"
        public DateTime BO_10_D
        {
            get { return m_BO_10_D; }
            set { m_BO_10_D = value; }
        }
        #endregion

        #region "BO_INC_D"
        public DateTime BO_INC_D
        {
            get { return m_BO_INC_D; }
            set { m_BO_INC_D = value; }
        }
        #endregion

        #region "BO_INC_AMOUNT"
        public decimal BO_INC_AMOUNT
        {
            get { return m_BO_INC_AMOUNT; }
            set { m_BO_INC_AMOUNT = value; }
        }
        #endregion

        #region "BO_10C_BONUS"
        public decimal BO_10C_BONUS
        {
            get { return m_BO_10C_BONUS; }
            set { m_BO_10C_BONUS = value; }
        }
        #endregion

        #region "BO_INC_APP"
        public String BO_INC_APP
        {
            get { return m_BO_INC_APP; }
            set { m_BO_INC_APP = value; }
        }
        #endregion

        #region "BO_10C_APP"
        public String BO_10C_APP
        {
            get { return m_BO_10C_APP; }
            set { m_BO_10C_APP = value; }
        }
        #endregion

        #region "BO_10_USE"
        public String BO_10_USE
        {
            get { return m_BO_10_USE; }
            set { m_BO_10_USE = value; }
        }
        #endregion

        #region "BO_INC_USE"
        public String BO_INC_USE
        {
            get { return m_BO_INC_USE; }
            set { m_BO_INC_USE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "BO_P_NO";
        public static readonly string _BO_10_D = "BO_10_D";
        public static readonly string _BO_INC_D = "BO_INC_D";
        public static readonly string _BO_INC_AMOUNT = "BO_INC_AMOUNT";
        public static readonly string _BO_10C_BONUS = "BO_10C_BONUS";
        public static readonly string _BO_INC_APP = "BO_INC_APP";
        public static readonly string _BO_10C_APP = "BO_10C_APP";
        public static readonly string _BO_10_USE = "BO_10_USE";
        public static readonly string _BO_INC_USE = "BO_INC_USE";
        public static readonly string _ID = "ID";
        #endregion

      

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
