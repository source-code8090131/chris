

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LoanTaxCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "dbo.LOAN_TAX"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Saad Saleem]	02/15/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LoanTaxCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private DateTime? m_TAX_YEAR_FROM;
        private DateTime? m_TAX_YEAR_TO;
        private decimal? m_TAX_PER;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "TAX_YEAR_FROM"
        public DateTime? TAX_YEAR_FROM
        {
            get { return m_TAX_YEAR_FROM; }
            set { m_TAX_YEAR_FROM = value; }
        }
        #endregion

        #region "TAX_YEAR_TO"
        public DateTime? TAX_YEAR_TO
        {
            get { return m_TAX_YEAR_TO; }
            set { m_TAX_YEAR_TO = value; }
        }
        #endregion

        #region "TAX_PER"
        public decimal? TAX_PER
        {
            get { return m_TAX_PER; }
            set { m_TAX_PER = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _TAX_YEAR_FROM = "TAX_YEAR_FROM";
        public static readonly string _TAX_YEAR_TO = "TAX_YEAR_TO";
        public static readonly string _TAX_PER = "TAX_PER";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
