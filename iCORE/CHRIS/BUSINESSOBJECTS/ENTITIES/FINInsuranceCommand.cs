
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FIN_INSURANCECommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FIN_INSURANCE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	12/14/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FINInsuranceCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_FN_FINANCE_NO;
        private String m_FN_POLICY_NO;
        private String m_FN_INSUR_COMP;
        private DateTime m_FN_POLICY_ISS_DATE;
        private DateTime m_FN_POLICY_EXP_DATE;
        private String m_FN_VEHC_REG_NO;
        private String m_FN_VEHC_ENG_NO;
        private String m_FN_VEHC_CHS_NO;
        private String m_FN_VEHC_MAKE;
        private String m_FN_VEHC_MODEL;
        private String m_FN_Type;
        private Double m_PA_INS_PREMIUM;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "FN_FINANCE_NO"
        public String FN_FINANCE_NO
        {
            get { return m_FN_FINANCE_NO; }
            set { m_FN_FINANCE_NO = value; }
        }
        #endregion

        #region "FN_POLICY_NO"
        public String FN_POLICY_NO
        {
            get { return m_FN_POLICY_NO; }
            set { m_FN_POLICY_NO = value; }
        }
        #endregion

        #region "FN_INSUR_COMP"
        public String FN_INSUR_COMP
        {
            get { return m_FN_INSUR_COMP; }
            set { m_FN_INSUR_COMP = value; }
        }
        #endregion

        #region "FN_POLICY_ISS_DATE"
        public DateTime FN_POLICY_ISS_DATE
        {
            get { return m_FN_POLICY_ISS_DATE; }
            set { m_FN_POLICY_ISS_DATE = value; }
        }
        #endregion

        #region "FN_POLICY_EXP_DATE"
        public DateTime FN_POLICY_EXP_DATE
        {
            get { return m_FN_POLICY_EXP_DATE; }
            set { m_FN_POLICY_EXP_DATE = value; }
        }
        #endregion

        #region "FN_VEHC_REG_NO"
        public String FN_VEHC_REG_NO
        {
            get { return m_FN_VEHC_REG_NO; }
            set { m_FN_VEHC_REG_NO = value; }
        }
        #endregion

        #region "FN_VEHC_ENG_NO"
        public String FN_VEHC_ENG_NO
        {
            get { return m_FN_VEHC_ENG_NO; }
            set { m_FN_VEHC_ENG_NO = value; }
        }
        #endregion

        #region "FN_VEHC_CHS_NO"
        public String FN_VEHC_CHS_NO
        {
            get { return m_FN_VEHC_CHS_NO; }
            set { m_FN_VEHC_CHS_NO = value; }
        }
        #endregion

        #region "FN_VEHC_MAKE"
        public String FN_VEHC_MAKE
        {
            get { return m_FN_VEHC_MAKE; }
            set { m_FN_VEHC_MAKE = value; }
        }
        #endregion

        #region "FN_VEHC_MODEL"
        public String FN_VEHC_MODEL
        {
            get { return m_FN_VEHC_MODEL; }
            set { m_FN_VEHC_MODEL = value; }
        }
        #endregion

        #region "FN_Type"
        public String FN_Type
        {
            get { return m_FN_Type; }
            set { m_FN_Type = value; }
        }
        #endregion

        #region "PA_INS_PREMIUM"
        public Double PA_INS_PREMIUM
        {
            get { return m_PA_INS_PREMIUM; }
            set { m_PA_INS_PREMIUM = value; }
        }
         #endregion


        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _FN_FINANCE_NO = "FN_FINANCE_NO";
        public static readonly string _FN_POLICY_NO = "FN_POLICY_NO";
        public static readonly string _FN_INSUR_COMP = "FN_INSUR_COMP";
        public static readonly string _FN_POLICY_ISS_DATE = "FN_POLICY_ISS_DATE";
        public static readonly string _FN_POLICY_EXP_DATE = "FN_POLICY_EXP_DATE";
        public static readonly string _FN_VEHC_REG_NO = "FN_VEHC_REG_NO";
        public static readonly string _FN_VEHC_ENG_NO = "FN_VEHC_ENG_NO";
        public static readonly string _FN_VEHC_CHS_NO = "FN_VEHC_CHS_NO";
        public static readonly string _FN_VEHC_MAKE = "FN_VEHC_MAKE";
        public static readonly string _FN_VEHC_MODEL = "FN_VEHC_MODEL";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}