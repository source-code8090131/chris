

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ShadowsalaryCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "SHADOW_SALARY"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/19/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class ShadowsalaryCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_SHS_PR_NO;
        private DateTime m_SHS_TRANSFER_DATE;
        private String m_SHS_CITY;
        private String m_SHS_COUNTRY;
        private String m_SHS_DESIG;
        private String m_SHS_LEVEL;
        private DateTime m_SHS_INC_LAST_DATE;
        private DateTime m_SHS_INC_CRNT_DATE;
        private decimal m_SHS_IS_TIR;
        private decimal m_SHS_ANL_PKG;
        private String m_SHS_RANK_HC;
        private decimal m_SHS_INC_PERCENT;
        private String m_SHS_INC_TYPE;
        private decimal m_SHS_REV_PKG;
        private decimal m_SHS_ANL_SHD_SAL;
        private String m_SHS_LOC_RANK;
        private DateTime m_SHS_LOC_DATE;
        private decimal m_SHS_LOC_TIR;
        private decimal m_SHS_LOC_INC;
        private decimal m_SHS_LOC_SALARY;
        private String m_SHS_REMARKS;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SHS_PR_NO"
        public decimal SHS_PR_NO
        {
            get { return m_SHS_PR_NO; }
            set { m_SHS_PR_NO = value; }
        }
        #endregion

        #region "SHS_TRANSFER_DATE"
        public DateTime SHS_TRANSFER_DATE
        {
            get { return m_SHS_TRANSFER_DATE; }
            set { m_SHS_TRANSFER_DATE = value; }
        }
        #endregion

        #region "SHS_CITY"
        public String SHS_CITY
        {
            get { return m_SHS_CITY; }
            set { m_SHS_CITY = value; }
        }
        #endregion

        #region "SHS_COUNTRY"
        public String SHS_COUNTRY
        {
            get { return m_SHS_COUNTRY; }
            set { m_SHS_COUNTRY = value; }
        }
        #endregion

        #region "SHS_DESIG"
        public String SHS_DESIG
        {
            get { return m_SHS_DESIG; }
            set { m_SHS_DESIG = value; }
        }
        #endregion

        #region "SHS_LEVEL"
        public String SHS_LEVEL
        {
            get { return m_SHS_LEVEL; }
            set { m_SHS_LEVEL = value; }
        }
        #endregion

        #region "SHS_INC_LAST_DATE"
        public DateTime SHS_INC_LAST_DATE
        {
            get { return m_SHS_INC_LAST_DATE; }
            set { m_SHS_INC_LAST_DATE = value; }
        }
        #endregion

        #region "SHS_INC_CRNT_DATE"
        public DateTime SHS_INC_CRNT_DATE
        {
            get { return m_SHS_INC_CRNT_DATE; }
            set { m_SHS_INC_CRNT_DATE = value; }
        }
        #endregion

        #region "SHS_IS_TIR"
        public decimal SHS_IS_TIR
        {
            get { return m_SHS_IS_TIR; }
            set { m_SHS_IS_TIR = value; }
        }
        #endregion

        #region "SHS_ANL_PKG"
        public decimal SHS_ANL_PKG
        {
            get { return m_SHS_ANL_PKG; }
            set { m_SHS_ANL_PKG = value; }
        }
        #endregion

        #region "SHS_RANK_HC"
        public String SHS_RANK_HC
        {
            get { return m_SHS_RANK_HC; }
            set { m_SHS_RANK_HC = value; }
        }
        #endregion

        #region "SHS_INC_PERCENT"
        public decimal SHS_INC_PERCENT
        {
            get { return m_SHS_INC_PERCENT; }
            set { m_SHS_INC_PERCENT = value; }
        }
        #endregion

        #region "SHS_INC_TYPE"
        public String SHS_INC_TYPE
        {
            get { return m_SHS_INC_TYPE; }
            set { m_SHS_INC_TYPE = value; }
        }
        #endregion

        #region "SHS_REV_PKG"
        public decimal SHS_REV_PKG
        {
            get { return m_SHS_REV_PKG; }
            set { m_SHS_REV_PKG = value; }
        }
        #endregion

        #region "SHS_ANL_SHD_SAL"
        public decimal SHS_ANL_SHD_SAL
        {
            get { return m_SHS_ANL_SHD_SAL; }
            set { m_SHS_ANL_SHD_SAL = value; }
        }
        #endregion

        #region "SHS_LOC_RANK"
        public String SHS_LOC_RANK
        {
            get { return m_SHS_LOC_RANK; }
            set { m_SHS_LOC_RANK = value; }
        }
        #endregion

        #region "SHS_LOC_DATE"
        public DateTime SHS_LOC_DATE
        {
            get { return m_SHS_LOC_DATE; }
            set { m_SHS_LOC_DATE = value; }
        }
        #endregion

        #region "SHS_LOC_TIR"
        public decimal SHS_LOC_TIR
        {
            get { return m_SHS_LOC_TIR; }
            set { m_SHS_LOC_TIR = value; }
        }
        #endregion

        #region "SHS_LOC_INC"
        public decimal SHS_LOC_INC
        {
            get { return m_SHS_LOC_INC; }
            set { m_SHS_LOC_INC = value; }
        }
        #endregion

        #region "SHS_LOC_SALARY"
        public decimal SHS_LOC_SALARY
        {
            get { return m_SHS_LOC_SALARY; }
            set { m_SHS_LOC_SALARY = value; }
        }
        #endregion

        #region "SHS_REMARKS"
        public String SHS_REMARKS
        {
            get { return m_SHS_REMARKS; }
            set { m_SHS_REMARKS = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SHS_PR_NO = "SHS_PR_NO";
        public static readonly string _SHS_TRANSFER_DATE = "SHS_TRANSFER_DATE";
        public static readonly string _SHS_CITY = "SHS_CITY";
        public static readonly string _SHS_COUNTRY = "SHS_COUNTRY";
        public static readonly string _SHS_DESIG = "SHS_DESIG";
        public static readonly string _SHS_LEVEL = "SHS_LEVEL";
        public static readonly string _SHS_INC_LAST_DATE = "SHS_INC_LAST_DATE";
        public static readonly string _SHS_INC_CRNT_DATE = "SHS_INC_CRNT_DATE";
        public static readonly string _SHS_IS_TIR = "SHS_IS_TIR";
        public static readonly string _SHS_ANL_PKG = "SHS_ANL_PKG";
        public static readonly string _SHS_RANK_HC = "SHS_RANK_HC";
        public static readonly string _SHS_INC_PERCENT = "SHS_INC_PERCENT";
        public static readonly string _SHS_INC_TYPE = "SHS_INC_TYPE";
        public static readonly string _SHS_REV_PKG = "SHS_REV_PKG";
        public static readonly string _SHS_ANL_SHD_SAL = "SHS_ANL_SHD_SAL";
        public static readonly string _SHS_LOC_RANK = "SHS_LOC_RANK";
        public static readonly string _SHS_LOC_DATE = "SHS_LOC_DATE";
        public static readonly string _SHS_LOC_TIR = "SHS_LOC_TIR";
        public static readonly string _SHS_LOC_INC = "SHS_LOC_INC";
        public static readonly string _SHS_LOC_SALARY = "SHS_LOC_SALARY";
        public static readonly string _SHS_REMARKS = "SHS_REMARKS";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
