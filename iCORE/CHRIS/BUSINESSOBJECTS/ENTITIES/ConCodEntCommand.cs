

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ConCodEntCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "CONTRACTER"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	01/28/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class ConCodEntCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_CONTRA_CODE;
        private String m_SP_CONTRA_NAME;
        private String m_SP_CONTRA_COMP_NAME;
        private String m_SP_CONTRA_ADD1;
        private String m_SP_CONTRA_ADD2;
        private String m_SP_CONTRA_ADD3;
        private String m_SP_CONTRA_PHONE1;
        private String m_SP_CONTRA_PHONE2;
        private DateTime m_SP_CONTRA_AGRE_DATE;
        private DateTime m_SP_CONTRA_EXP_DATE;
        private decimal m_SP_CONTRA_NID;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_CONTRA_CODE"
        public String SP_CONTRA_CODE
        {
            get { return m_SP_CONTRA_CODE; }
            set { m_SP_CONTRA_CODE = value; }
        }
        #endregion

        #region "SP_CONTRA_NAME"
        public String SP_CONTRA_NAME
        {
            get { return m_SP_CONTRA_NAME; }
            set { m_SP_CONTRA_NAME = value; }
        }
        #endregion

        #region "SP_CONTRA_COMP_NAME"
        public String SP_CONTRA_COMP_NAME
        {
            get { return m_SP_CONTRA_COMP_NAME; }
            set { m_SP_CONTRA_COMP_NAME = value; }
        }
        #endregion

        #region "SP_CONTRA_ADD1"
        public String SP_CONTRA_ADD1
        {
            get { return m_SP_CONTRA_ADD1; }
            set { m_SP_CONTRA_ADD1 = value; }
        }
        #endregion

        #region "SP_CONTRA_ADD2"
        public String SP_CONTRA_ADD2
        {
            get { return m_SP_CONTRA_ADD2; }
            set { m_SP_CONTRA_ADD2 = value; }
        }
        #endregion

        #region "SP_CONTRA_ADD3"
        public String SP_CONTRA_ADD3
        {
            get { return m_SP_CONTRA_ADD3; }
            set { m_SP_CONTRA_ADD3 = value; }
        }
        #endregion

        #region "SP_CONTRA_PHONE1"
        public String SP_CONTRA_PHONE1
        {
            get { return m_SP_CONTRA_PHONE1; }
            set { m_SP_CONTRA_PHONE1 = value; }
        }
        #endregion

        #region "SP_CONTRA_PHONE2"
        public String SP_CONTRA_PHONE2
        {
            get { return m_SP_CONTRA_PHONE2; }
            set { m_SP_CONTRA_PHONE2 = value; }
        }
        #endregion

        #region "SP_CONTRA_AGRE_DATE"
        public DateTime SP_CONTRA_AGRE_DATE
        {
            get { return m_SP_CONTRA_AGRE_DATE; }
            set { m_SP_CONTRA_AGRE_DATE = value; }
        }
        #endregion

        #region "SP_CONTRA_EXP_DATE"
        public DateTime SP_CONTRA_EXP_DATE
        {
            get { return m_SP_CONTRA_EXP_DATE; }
            set { m_SP_CONTRA_EXP_DATE = value; }
        }
        #endregion

        #region "SP_CONTRA_NID"
        public decimal SP_CONTRA_NID
        {
            get { return m_SP_CONTRA_NID; }
            set { m_SP_CONTRA_NID = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_CONTRA_CODE = "SP_CONTRA_CODE";
        public static readonly string _SP_CONTRA_NAME = "SP_CONTRA_NAME";
        public static readonly string _SP_CONTRA_COMP_NAME = "SP_CONTRA_COMP_NAME";
        public static readonly string _SP_CONTRA_ADD1 = "SP_CONTRA_ADD1";
        public static readonly string _SP_CONTRA_ADD2 = "SP_CONTRA_ADD2";
        public static readonly string _SP_CONTRA_ADD3 = "SP_CONTRA_ADD3";
        public static readonly string _SP_CONTRA_PHONE1 = "SP_CONTRA_PHONE1";
        public static readonly string _SP_CONTRA_PHONE2 = "SP_CONTRA_PHONE2";
        public static readonly string _SP_CONTRA_AGRE_DATE = "SP_CONTRA_AGRE_DATE";
        public static readonly string _SP_CONTRA_EXP_DATE = "SP_CONTRA_EXP_DATE";
        public static readonly string _SP_CONTRA_NID = "SP_CONTRA_NID";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
