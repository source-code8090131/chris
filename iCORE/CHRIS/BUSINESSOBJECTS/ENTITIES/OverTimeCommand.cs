

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OverTimeCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "OVERTIME"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Framework]	02/21/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class OverTimeCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private double m_OT_P_NO;
        private DateTime m_OT_DATE_FILTER;
        private DateTime m_OT_DATE;
        private String m_OT_SEGMENT;
        private String m_OT_DEPT;
        private String m_OT_DEFAULT;
        private String m_OT_TIME_IN;
        private String m_OT_TIME_OUT;
        private decimal? m_OT_SINGLE_HR;
        private decimal? m_OT_DOUBLE_HR;
        private decimal? m_OT_SD_WEEK;
        private String m_OT_MEAL_CODE;
        private decimal? m_OT_MEAL_AMT;
        private String m_OT_CONV_CODE;
        private decimal? m_OT_CONV_AMT;
        private String m_OT_FRI_HOL;
        private String m_SH_SHIFT;
        private decimal? m_SH_AMOUNT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "OT_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public double OT_P_NO
        {
            get { return m_OT_P_NO; }
            set { m_OT_P_NO = value; }
        }
        #endregion

        #region "OT_DATE_FILTER"
        [CustomAttributes(IsForeignKey = true)]
        public DateTime OT_DATE_FILTER
        {
            get { return m_OT_DATE_FILTER; }
            set { m_OT_DATE_FILTER = value; }
        }
        #endregion

        #region "OT_DATE"
        public DateTime OT_DATE
        {
            get { return m_OT_DATE; }
            set { m_OT_DATE = value; }
        }
        #endregion

        #region "OT_SEGMENT"
        public String OT_SEGMENT
        {
            get { return m_OT_SEGMENT; }
            set { m_OT_SEGMENT = value; }
        }
        #endregion

        #region "OT_DEPT"
        public String OT_DEPT
        {
            get { return m_OT_DEPT; }
            set { m_OT_DEPT = value; }
        }
        #endregion

        #region "OT_DEFAULT"
        public String OT_DEFAULT
        {
            get { return m_OT_DEFAULT; }
            set { m_OT_DEFAULT = value; }
        }
        #endregion

        #region "OT_TIME_IN"
        public String OT_TIME_IN
        {
            get { return m_OT_TIME_IN; }
            set { m_OT_TIME_IN = value; }
        }
        #endregion

        #region "OT_TIME_OUT"
        public String OT_TIME_OUT
        {
            get { return m_OT_TIME_OUT; }
            set { m_OT_TIME_OUT = value; }
        }
        #endregion

        #region "OT_SINGLE_HR"
        public decimal? OT_SINGLE_HR
        {
            get { return m_OT_SINGLE_HR; }
            set { m_OT_SINGLE_HR = value; }
        }
        #endregion

        #region "OT_DOUBLE_HR"
        public decimal? OT_DOUBLE_HR
        {
            get { return m_OT_DOUBLE_HR; }
            set { m_OT_DOUBLE_HR = value; }
        }
        #endregion

        #region "OT_SD_WEEK"
        public decimal? OT_SD_WEEK
        {
            get { return m_OT_SD_WEEK; }
            set { m_OT_SD_WEEK = value; }
        }
        #endregion

        #region "OT_MEAL_CODE"
        public String OT_MEAL_CODE
        {
            get { return m_OT_MEAL_CODE; }
            set { m_OT_MEAL_CODE = value; }
        }
        #endregion

        #region "OT_MEAL_AMT"
        public decimal? OT_MEAL_AMT
        {
            get { return m_OT_MEAL_AMT; }
            set { m_OT_MEAL_AMT = value; }
        }
        #endregion

        #region "OT_CONV_CODE"
        public String OT_CONV_CODE
        {
            get { return m_OT_CONV_CODE; }
            set { m_OT_CONV_CODE = value; }
        }
        #endregion

        #region "OT_CONV_AMT"
        public decimal? OT_CONV_AMT
        {
            get { return m_OT_CONV_AMT; }
            set { m_OT_CONV_AMT = value; }
        }
        #endregion

        #region "OT_FRI_HOL"
        public String OT_FRI_HOL
        {
            get { return m_OT_FRI_HOL; }
            set { m_OT_FRI_HOL = value; }
        }
        #endregion

        #region "SH_SHIFT"
        public String SH_SHIFT
        {
            get { return m_SH_SHIFT; }
            set { m_SH_SHIFT = value; }
        }
        #endregion

        #region "SH_AMOUNT"
        public decimal? SH_AMOUNT
        {
            get { return m_SH_AMOUNT; }
            set { m_SH_AMOUNT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _OT_P_NO = "OT_P_NO";
        public static readonly string _OT_DATE_FILTER = "OT_DATE_FILTER";
        public static readonly string _OT_DATE = "OT_DATE";
        public static readonly string _OT_SEGMENT = "OT_SEGMENT";
        public static readonly string _OT_DEPT = "OT_DEPT";
        public static readonly string _OT_DEFAULT = "OT_DEFAULT";
        public static readonly string _OT_TIME_IN = "OT_TIME_IN";
        public static readonly string _OT_TIME_OUT = "OT_TIME_OUT";
        public static readonly string _OT_SINGLE_HR = "OT_SINGLE_HR";
        public static readonly string _OT_DOUBLE_HR = "OT_DOUBLE_HR";
        public static readonly string _OT_SD_WEEK = "OT_SD_WEEK";
        public static readonly string _OT_MEAL_CODE = "OT_MEAL_CODE";
        public static readonly string _OT_MEAL_AMT = "OT_MEAL_AMT";
        public static readonly string _OT_CONV_CODE = "OT_CONV_CODE";
        public static readonly string _OT_CONV_AMT = "OT_CONV_AMT";
        public static readonly string _OT_FRI_HOL = "OT_FRI_HOL";
        public static readonly string _SH_SHIFT = "SH_SHIFT";
        public static readonly string _SH_AMOUNT = "SH_AMOUNT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}


