

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PRCountryCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PR_COUNTRY"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	12/06/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PRCountryCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_PR_COUNTRY_CODE;
        private String m_PR_COUNTRY_DESC;
        private String m_PR_COUNTRY_AC;
        private String m_PR_CITIMAIL;
        private String m_PR_MAKER_NAME;
        private DateTime m_PR_MAKER_DATE;
        private String m_PR_MAKER_TIME;
        private String m_PR_MAKER_LOC;
        private String m_PR_MAKER_TERM;
        private String m_PR_AUTH_NAME;
        private DateTime m_PR_AUTH_DATE;
        private String m_PR_AUTH_TIME;
        private String m_PR_AUTH_LOC;
        private String m_PR_AUTH_TERM;
        private String m_PR_AUTH_FLAG;
        private String m_PR_STATUS;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_COUNTRY_CODE"
        public String PR_COUNTRY_CODE
        {
            get { return m_PR_COUNTRY_CODE; }
            set { m_PR_COUNTRY_CODE = value; }
        }
        #endregion

        #region "PR_COUNTRY_DESC"
        public String PR_COUNTRY_DESC
        {
            get { return m_PR_COUNTRY_DESC; }
            set { m_PR_COUNTRY_DESC = value; }
        }
        #endregion

        #region "PR_COUNTRY_AC"
        public String PR_COUNTRY_AC
        {
            get { return m_PR_COUNTRY_AC; }
            set { m_PR_COUNTRY_AC = value; }
        }
        #endregion

        #region "PR_CITIMAIL"
        public String PR_CITIMAIL
        {
            get { return m_PR_CITIMAIL; }
            set { m_PR_CITIMAIL = value; }
        }
        #endregion

        #region "PR_MAKER_NAME"
        public String PR_MAKER_NAME
        {
            get { return m_PR_MAKER_NAME; }
            set { m_PR_MAKER_NAME = value; }
        }
        #endregion

        #region "PR_MAKER_DATE"
        public DateTime PR_MAKER_DATE
        {
            get { return m_PR_MAKER_DATE; }
            set { m_PR_MAKER_DATE = value; }
        }
        #endregion

        #region "PR_MAKER_TIME"
        public String PR_MAKER_TIME
        {
            get { return m_PR_MAKER_TIME; }
            set { m_PR_MAKER_TIME = value; }
        }
        #endregion

        #region "PR_MAKER_LOC"
        public String PR_MAKER_LOC
        {
            get { return m_PR_MAKER_LOC; }
            set { m_PR_MAKER_LOC = value; }
        }
        #endregion

        #region "PR_MAKER_TERM"
        public String PR_MAKER_TERM
        {
            get { return m_PR_MAKER_TERM; }
            set { m_PR_MAKER_TERM = value; }
        }
        #endregion

        #region "PR_AUTH_NAME"
        public String PR_AUTH_NAME
        {
            get { return m_PR_AUTH_NAME; }
            set { m_PR_AUTH_NAME = value; }
        }
        #endregion

        #region "PR_AUTH_DATE"
        public DateTime PR_AUTH_DATE
        {
            get { return m_PR_AUTH_DATE; }
            set { m_PR_AUTH_DATE = value; }
        }
        #endregion

        #region "PR_AUTH_TIME"
        public String PR_AUTH_TIME
        {
            get { return m_PR_AUTH_TIME; }
            set { m_PR_AUTH_TIME = value; }
        }
        #endregion

        #region "PR_AUTH_LOC"
        public String PR_AUTH_LOC
        {
            get { return m_PR_AUTH_LOC; }
            set { m_PR_AUTH_LOC = value; }
        }
        #endregion

        #region "PR_AUTH_TERM"
        public String PR_AUTH_TERM
        {
            get { return m_PR_AUTH_TERM; }
            set { m_PR_AUTH_TERM = value; }
        }
        #endregion

        #region "PR_AUTH_FLAG"
        public String PR_AUTH_FLAG
        {
            get { return m_PR_AUTH_FLAG; }
            set { m_PR_AUTH_FLAG = value; }
        }
        #endregion

        #region "PR_STATUS"
        public String PR_STATUS
        {
            get { return m_PR_STATUS; }
            set { m_PR_STATUS = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_COUNTRY_CODE = "PR_COUNTRY_CODE";
        public static readonly string _PR_COUNTRY_DESC = "PR_COUNTRY_DESC";
        public static readonly string _PR_COUNTRY_AC = "PR_COUNTRY_AC";
        public static readonly string _PR_CITIMAIL = "PR_CITIMAIL";
        public static readonly string _PR_MAKER_NAME = "PR_MAKER_NAME";
        public static readonly string _PR_MAKER_DATE = "PR_MAKER_DATE";
        public static readonly string _PR_MAKER_TIME = "PR_MAKER_TIME";
        public static readonly string _PR_MAKER_LOC = "PR_MAKER_LOC";
        public static readonly string _PR_MAKER_TERM = "PR_MAKER_TERM";
        public static readonly string _PR_AUTH_NAME = "PR_AUTH_NAME";
        public static readonly string _PR_AUTH_DATE = "PR_AUTH_DATE";
        public static readonly string _PR_AUTH_TIME = "PR_AUTH_TIME";
        public static readonly string _PR_AUTH_LOC = "PR_AUTH_LOC";
        public static readonly string _PR_AUTH_TERM = "PR_AUTH_TERM";
        public static readonly string _PR_AUTH_FLAG = "PR_AUTH_FLAG";
        public static readonly string _PR_STATUS = "PR_STATUS";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
