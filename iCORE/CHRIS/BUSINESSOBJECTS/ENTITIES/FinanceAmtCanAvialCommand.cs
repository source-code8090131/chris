
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceAmtCanAvialCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "Amount Can Avial"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	03/04/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion


namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    class FinanceAmtCanAvialCommand
    {
        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private int m_ID;
        private Double m_AMT_OF_LOAN_CAN_AVAIL;
        private Double m_AMT_OF_LOAN_CAN_AVAIL_CAP;
        private Double m_FN_AMT_AVAILED;
        private Double m_TOT_MONTH_INST;
        private String m_EXCP_FLAG;
        private String m_EXP_REM_OPT;
        private String m_EXCP_REM;
        private String m_REMARKS;
        private Double m_CREDIT_RATIO;
        private Double m_LESS_RATIO_AVAILED;
        private Double m_AVAILABLE_RATIO;
        private Double m_FACTOR;
        private Double m_INS_COV_AMT;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "INS_COV_AMT"
        public Double INS_COV_AMT
        {
            get { return m_INS_COV_AMT; }
            set { m_INS_COV_AMT = value; }
        }
        #endregion
        #region "FACTOR"
        public Double FACTOR
        {
            get { return m_FACTOR; }
            set { m_FACTOR = value; }
        }
        #endregion
        #region "AVAILABLE_RATIO"
        public Double AVAILABLE_RATIO
        {
            get { return m_AVAILABLE_RATIO; }
            set { m_AVAILABLE_RATIO = value; }
        }
        #endregion
        #region "LESS_RATIO_AVAILED"
        public Double LESS_RATIO_AVAILED
        {
            get { return m_LESS_RATIO_AVAILED; }
            set { m_LESS_RATIO_AVAILED = value; }
        }
        #endregion
        #region "CREDIT_RATIO"
        public Double CREDIT_RATIO
        {
            get { return m_CREDIT_RATIO; }
            set { m_CREDIT_RATIO = value; }
        }
        #endregion
        #region "EXP_REM_OPT"
        public String EXP_REM_OPT
        {
            get { return m_EXP_REM_OPT; }
            set { m_EXP_REM_OPT = value; }
        }
        #endregion
        #region "FN_AMT_AVAILED"
        public Double FN_AMT_AVAILED
        {
            get { return m_FN_AMT_AVAILED; }
            set { m_FN_AMT_AVAILED = value; }
        }
        #endregion
        #region "AMT_OF_LOAN_CAN_AVAIL"
        public Double AMT_OF_LOAN_CAN_AVAIL
        {
            get { return m_AMT_OF_LOAN_CAN_AVAIL; }
            set { m_AMT_OF_LOAN_CAN_AVAIL = value; }
        }
        #endregion
        #region "AMT_OF_LOAN_CAN_AVAIL_CAP"
        public Double AMT_OF_LOAN_CAN_AVAIL_CAP
        {
            get { return m_AMT_OF_LOAN_CAN_AVAIL_CAP; }
            set { m_AMT_OF_LOAN_CAN_AVAIL_CAP = value; }
        }
        #endregion
        #region "EXCP_FLAG"
        public String EXCP_FLAG
        {
            get { return m_EXCP_FLAG; }
            set { m_EXCP_FLAG = value; }
        }
        #endregion
        #region "TOT_MONTH_INST"
        public Double TOT_MONTH_INST
        {
            get { return m_TOT_MONTH_INST; }
            set { m_TOT_MONTH_INST = value; }
        }
        #endregion
        #region "EXCP_REM"
        public String EXCP_REM
        {
            get { return m_EXCP_REM; }
            set { m_EXCP_REM = value; }
        }
        #endregion
        #region "REMARKS"
        public String REMARKS
        {
            get { return m_REMARKS; }
            set { m_REMARKS = value; }
        }
        #endregion
        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        //public static readonly string _AMT_OF_LOAN_CAN_AVAIL = "fn_fin_ent";
        //public static readonly string _AMT_OF_LOAN_CAN_AVAIL_CAP = "";
        //public static readonly string _FN_AMT_AVAILED = "";
        //public static readonly string _TOT_MONTH_INST = "";
        //public static readonly string _EXCP_FLAG = "EXCP_FLAG";
        //public static readonly string _EXP_REM_OPT = "";
        //public static readonly string _EXCP_REM = "EXCP_REM";
        //public static readonly string _REMARKS = "fn_remarks";
        //public static readonly string _CREDIT_RATIO = "";
        //public static readonly string _LESS_RATIO_AVAILED = "";
        //public static readonly string _AVAILABLE_RATIO = "";
        //public static readonly string _FACTOR = "";
        //public static readonly string _INS_COV_AMT = "";
         
        //public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

    }
}
