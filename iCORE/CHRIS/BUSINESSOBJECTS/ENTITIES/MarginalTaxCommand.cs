

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MarginalTaxCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "dbo.MARGINAL_TAX"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Saad Saleem]	02/15/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class MarginalTaxCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private DateTime m_MT_DATE_FROM;
        private DateTime m_MT_DATE_TO;
        private decimal m_MT_AMT_FROM;
        private decimal m_MT_AMT_TO;
        private decimal m_MT_PERCENTAGE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "MT_DATE_FROM"
        public DateTime MT_DATE_FROM
        {
            get { return m_MT_DATE_FROM; }
            set { m_MT_DATE_FROM = value; }
        }
        #endregion

        #region "MT_DATE_TO"
        public DateTime MT_DATE_TO
        {
            get { return m_MT_DATE_TO; }
            set { m_MT_DATE_TO = value; }
        }
        #endregion

        #region "MT_AMT_FROM"
        public decimal MT_AMT_FROM
        {
            get { return m_MT_AMT_FROM; }
            set { m_MT_AMT_FROM = value; }
        }
        #endregion

        #region "MT_AMT_TO"
        public decimal MT_AMT_TO
        {
            get { return m_MT_AMT_TO; }
            set { m_MT_AMT_TO = value; }
        }
        #endregion

        #region "MT_PERCENTAGE"
        public decimal MT_PERCENTAGE
        {
            get { return m_MT_PERCENTAGE; }
            set { m_MT_PERCENTAGE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _MT_DATE_FROM = "MT_DATE_FROM";
        public static readonly string _MT_DATE_TO = "MT_DATE_TO";
        public static readonly string _MT_AMT_FROM = "MT_AMT_FROM";
        public static readonly string _MT_AMT_TO = "MT_AMT_TO";
        public static readonly string _MT_PERCENTAGE = "MT_PERCENTAGE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
