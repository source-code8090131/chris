#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion


namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES 
{
    public class PFSearchEntityCommand : BusinessEntity
    {

        private decimal m_PFD_PR_P_NO;
        private String m_dummy_year;
        private int m_ID;

        #region "PFD_PR_P_NO"
        public decimal PFD_PR_P_NO
        {
            get { return m_PFD_PR_P_NO; }
            set { m_PFD_PR_P_NO = value; }
        }
        #endregion
        #region "dummy_year"
          public String dummy_year
        {
            get { return m_dummy_year; }
            set { m_dummy_year = value; }
        }
        #endregion
        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion
        public static readonly string _PFD_PR_P_NO = "PFD_PR_P_NO";
        public static readonly string _ID = "ID";
    }
}
