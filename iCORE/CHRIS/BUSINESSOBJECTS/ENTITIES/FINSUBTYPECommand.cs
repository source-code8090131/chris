
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINSUBTYPECommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FIN_SUBTYPE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/20/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FINSUBTYPECommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_FN_TYPE;
        private String m_FN_SUBTYPE;
        private String m_FN_SUBDESC;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        [CustomAttributes(IsForeignKey = true)]
        #region "FN_TYPE"
        public String FN_TYPE
        {
            get { return m_FN_TYPE; }
            set { m_FN_TYPE = value; }
        }
        #endregion

        #region "FN_SUBTYPE"
        public String FN_SUBTYPE
        {
            get { return m_FN_SUBTYPE; }
            set { m_FN_SUBTYPE = value; }
        }
        #endregion

        #region "FN_SUBDESC"
        public String FN_SUBDESC
        {
            get { return m_FN_SUBDESC; }
            set { m_FN_SUBDESC = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _FN_TYPE = "FN_TYPE";
        public static readonly string _FN_SUBTYPE = "FN_SUBTYPE";
        public static readonly string _FN_SUBDESC = "FN_SUBDESC";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
