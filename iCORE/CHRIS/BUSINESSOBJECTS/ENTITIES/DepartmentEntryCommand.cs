

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEPT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Anila]	03/26/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class DepartmentEntryCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_SEGMENT_D;
        private String m_SP_GROUP_D;
        private String m_SP_DEPT;
        private String m_SP_DESC_D1;
        private String m_SP_DESC_D2;
        private String m_SP_DEPT_HEAD;
        private DateTime m_SP_DATE_FROM_D;
        private DateTime m_SP_DATE_TO_D;
        private String m_RC;
        private String m_SP_RC_APA;
        private int m_ID;
        private String m_SP_COST_CENTER;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_SEGMENT_D"
        public String SP_SEGMENT_D
        {
            get { return m_SP_SEGMENT_D; }
            set { m_SP_SEGMENT_D = value; }
        }
        #endregion

        #region "SP_GROUP_D"
        public String SP_GROUP_D
        {
            get { return m_SP_GROUP_D; }
            set { m_SP_GROUP_D = value; }
        }
        #endregion

        #region "SP_DEPT"
        public String SP_DEPT
        {
            get { return m_SP_DEPT; }
            set { m_SP_DEPT = value; }
        }
        #endregion

     

        #region "SP_DESC_D1"
        public String SP_DESC_D1
        {
            get { return m_SP_DESC_D1; }
            set { m_SP_DESC_D1 = value; }
        }
        #endregion

        #region "SP_DESC_D2"
        public String SP_DESC_D2
        {
            get { return m_SP_DESC_D2; }
            set { m_SP_DESC_D2 = value; }
        }
        #endregion

        #region "SP_DEPT_HEAD"
        public String SP_DEPT_HEAD
        {
            get { return m_SP_DEPT_HEAD; }
            set { m_SP_DEPT_HEAD = value; }
        }
        #endregion

        #region "SP_DATE_FROM_D"
        public DateTime SP_DATE_FROM_D
        {
            get { return m_SP_DATE_FROM_D; }
            set { m_SP_DATE_FROM_D = value; }
        }
        #endregion

        #region "SP_DATE_TO_D"
        public DateTime SP_DATE_TO_D
        {
            get { return m_SP_DATE_TO_D; }
            set { m_SP_DATE_TO_D = value; }
        }
        #endregion

        #region "RC"
        public String RC
        {
            get { return m_RC; }
            set { m_RC = value; }
        }
        #endregion

        #region "SP_RC_APA"
        public String SP_RC_APA
        {
            get { return m_SP_RC_APA; }
            set { m_SP_RC_APA = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "SP_COST_CENTER"
        public String SP_COST_CENTER
        {
            get { return m_SP_COST_CENTER; }
            set { m_SP_COST_CENTER = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_SEGMENT_D = "SP_SEGMENT_D";
        public static readonly string _SP_GROUP_D = "SP_GROUP_D";
        public static readonly string _SP_DEPT = "SP_DEPT";
        public static readonly string _SP_DESC_D1 = "SP_DESC_D1";
        public static readonly string _SP_DESC_D2 = "SP_DESC_D2";
        public static readonly string _SP_DEPT_HEAD = "SP_DEPT_HEAD";
        public static readonly string _SP_DATE_FROM_D = "SP_DATE_FROM_D";
        public static readonly string _SP_DATE_TO_D = "SP_DATE_TO_D";
        public static readonly string _RC = "RC";
        public static readonly string _SP_RC_APA = "SP_RC_APA";
        public static readonly string _ID = "ID";
        public static readonly string _SP_COST_CENTER = "SP_COST_CENTER";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
