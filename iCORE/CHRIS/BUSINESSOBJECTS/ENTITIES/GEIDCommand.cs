

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GEIDCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "GEID"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	11/29/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class GEIDCommand : BusinessEntity
    {

       #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_GEI_LAST_NAME;
        private String m_GEI_FIRST_NAME;
        private String m_GEI_MIDDLE_NAME;
        private DateTime m_GEI_BIRTH_DATE;
        private decimal m_GEI_PR_P_NO;
        private String m_GEI_COUNT_PAY_LOC;
        private String m_GEI_REC_TYPE;
        private decimal m_GEI_GEID_NO;
        private DateTime m_GEI_DATE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "GEI_LAST_NAME"
        public String GEI_LAST_NAME
        {
            get { return m_GEI_LAST_NAME; }
            set { m_GEI_LAST_NAME = value; }
        }
        #endregion

        #region "GEI_FIRST_NAME"
        public String GEI_FIRST_NAME
        {
            get { return m_GEI_FIRST_NAME; }
            set { m_GEI_FIRST_NAME = value; }
        }
        #endregion

        #region "GEI_MIDDLE_NAME"
        public String GEI_MIDDLE_NAME
        {
            get { return m_GEI_MIDDLE_NAME; }
            set { m_GEI_MIDDLE_NAME = value; }
        }
        #endregion

        #region "GEI_BIRTH_DATE"
        public DateTime GEI_BIRTH_DATE
        {
            get { return m_GEI_BIRTH_DATE; }
            set { m_GEI_BIRTH_DATE = value; }
        }
        #endregion

        #region "GEI_PR_P_NO"
        public decimal GEI_PR_P_NO
        {
            get { return m_GEI_PR_P_NO; }
            set { m_GEI_PR_P_NO = value; }
        }
        #endregion

        #region "GEI_COUNT_PAY_LOC"
        public String GEI_COUNT_PAY_LOC
        {
            get { return m_GEI_COUNT_PAY_LOC; }
            set { m_GEI_COUNT_PAY_LOC = value; }
        }
        #endregion

        #region "GEI_REC_TYPE"
        public String GEI_REC_TYPE
        {
            get { return m_GEI_REC_TYPE; }
            set { m_GEI_REC_TYPE = value; }
        }
        #endregion

        #region "GEI_GEID_NO"
        public decimal GEI_GEID_NO
        {
            get { return m_GEI_GEID_NO; }
            set { m_GEI_GEID_NO = value; }
        }
        #endregion

        #region "GEI_DATE"
        public DateTime GEI_DATE
        {
            get { return m_GEI_DATE; }
            set { m_GEI_DATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _GEI_LAST_NAME = "GEI_LAST_NAME";
        public static readonly string _GEI_FIRST_NAME = "GEI_FIRST_NAME";
        public static readonly string _GEI_MIDDLE_NAME = "GEI_MIDDLE_NAME";
        public static readonly string _GEI_BIRTH_DATE = "GEI_BIRTH_DATE";
        public static readonly string _GEI_PR_P_NO = "GEI_PR_P_NO";
        public static readonly string _GEI_COUNT_PAY_LOC = "GEI_COUNT_PAY_LOC";
        public static readonly string _GEI_REC_TYPE = "GEI_REC_TYPE";
        public static readonly string _GEI_GEID_NO = "GEI_GEID_NO";
        public static readonly string _GEI_DATE = "GEI_DATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

       #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

    }
}
