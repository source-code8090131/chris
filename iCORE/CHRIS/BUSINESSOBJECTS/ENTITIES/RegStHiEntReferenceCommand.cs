

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntReferenceCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "EMP_REFERENCE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	01/22/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class RegStHiEntReferenceCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private decimal m_REF_CODE;
        private String m_REF_NAME;
        private String m_REF_ADD1;
        private String m_REF_ADD2;
        private String m_REF_ADD3;
        private String m_REF_LET_SNT;
        private String m_REF_ANS_REC;
        private String m_REF_FLAG;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "REF_CODE"
        public decimal REF_CODE
        {
            get { return m_REF_CODE; }
            set { m_REF_CODE = value; }
        }
        #endregion

        #region "REF_NAME"
        public String REF_NAME
        {
            get { return m_REF_NAME; }
            set { m_REF_NAME = value; }
        }
        #endregion

        #region "REF_ADD1"
        public String REF_ADD1
        {
            get { return m_REF_ADD1; }
            set { m_REF_ADD1 = value; }
        }
        #endregion

        #region "REF_ADD2"
        public String REF_ADD2
        {
            get { return m_REF_ADD2; }
            set { m_REF_ADD2 = value; }
        }
        #endregion

        #region "REF_ADD3"
        public String REF_ADD3
        {
            get { return m_REF_ADD3; }
            set { m_REF_ADD3 = value; }
        }
        #endregion

        #region "REF_LET_SNT"
        public String REF_LET_SNT
        {
            get { return m_REF_LET_SNT; }
            set { m_REF_LET_SNT = value; }
        }
        #endregion

        #region "REF_ANS_REC"
        public String REF_ANS_REC
        {
            get { return m_REF_ANS_REC; }
            set { m_REF_ANS_REC = value; }
        }
        #endregion

        #region "REF_FLAG"
        public String REF_FLAG
        {
            get { return m_REF_FLAG; }
            set { m_REF_FLAG = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _REF_CODE = "REF_CODE";
        public static readonly string _REF_NAME = "REF_NAME";
        public static readonly string _REF_ADD1 = "REF_ADD1";
        public static readonly string _REF_ADD2 = "REF_ADD2";
        public static readonly string _REF_ADD3 = "REF_ADD3";
        public static readonly string _REF_LET_SNT = "REF_LET_SNT";
        public static readonly string _REF_ANS_REC = "REF_ANS_REC";
        public static readonly string _REF_FLAG = "REF_FLAG";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
