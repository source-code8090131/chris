

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PaymentEntryCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "OMI_CLAIM"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	04/27/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PaymentEntryCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_OMI_P_NO;
        private decimal m_OMI_MONTH;
        private decimal m_OMI_YEAR;
        private decimal m_OMI_TOTAL_CLAIMS;
        private decimal m_OMI_TOTAL_CLAIM;
        private decimal m_OMI_TOTAL_RECEV;
        private decimal m_OMI_SELF;
        private decimal m_OMI_SPOUSE;
        private decimal m_OMI_CHILD;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "OMI_P_NO"
        public decimal OMI_P_NO
        {
            get { return m_OMI_P_NO; }
            set { m_OMI_P_NO = value; }
        }
        #endregion

        #region "OMI_MONTH"
        public decimal OMI_MONTH
        {
            get { return m_OMI_MONTH; }
            set { m_OMI_MONTH = value; }
        }
        #endregion

        #region "OMI_YEAR"
        public decimal OMI_YEAR
        {
            get { return m_OMI_YEAR; }
            set { m_OMI_YEAR = value; }
        }
        #endregion

        #region "OMI_TOTAL_CLAIMS"
        public decimal OMI_TOTAL_CLAIMS
        {
            get { return m_OMI_TOTAL_CLAIMS; }
            set { m_OMI_TOTAL_CLAIMS = value; }
        }
        #endregion

        #region "OMI_TOTAL_CLAIM"
        public decimal OMI_TOTAL_CLAIM
        {
            get { return m_OMI_TOTAL_CLAIM; }
            set { m_OMI_TOTAL_CLAIM = value; }
        }
        #endregion

        #region "OMI_TOTAL_RECEV"
        public decimal OMI_TOTAL_RECEV
        {
            get { return m_OMI_TOTAL_RECEV; }
            set { m_OMI_TOTAL_RECEV = value; }
        }
        #endregion

        #region "OMI_SELF"
        public decimal OMI_SELF
        {
            get { return m_OMI_SELF; }
            set { m_OMI_SELF = value; }
        }
        #endregion

        #region "OMI_SPOUSE"
        public decimal OMI_SPOUSE
        {
            get { return m_OMI_SPOUSE; }
            set { m_OMI_SPOUSE = value; }
        }
        #endregion

        #region "OMI_CHILD"
        public decimal OMI_CHILD
        {
            get { return m_OMI_CHILD; }
            set { m_OMI_CHILD = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _OMI_P_NO = "OMI_P_NO";
        public static readonly string _OMI_MONTH = "OMI_MONTH";
        public static readonly string _OMI_YEAR = "OMI_YEAR";
        public static readonly string _OMI_TOTAL_CLAIMS = "OMI_TOTAL_CLAIMS";
        public static readonly string _OMI_TOTAL_CLAIM = "OMI_TOTAL_CLAIM";
        public static readonly string _OMI_TOTAL_RECEV = "OMI_TOTAL_RECEV";
        public static readonly string _OMI_SELF = "OMI_SELF";
        public static readonly string _OMI_SPOUSE = "OMI_SPOUSE";
        public static readonly string _OMI_CHILD = "OMI_CHILD";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
