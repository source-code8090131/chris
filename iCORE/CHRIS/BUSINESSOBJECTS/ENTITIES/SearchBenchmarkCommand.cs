using System;
using System.Collections.Generic;
using System.Text;
using iCORE.Common;

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class SearchBenchmarkCommand : BusinessEntity
    {


        #region "--Field Segment--"

        private Nullable<DateTime> m_BEN_DATE;
        #endregion "--Field Segment--"



        #region "BEN_DATE"
        public Nullable<DateTime> BEN_DATE
        {
            get { return m_BEN_DATE; }
            set { m_BEN_DATE = value; }
        }
        #endregion

    }
}
