

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DESIGCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DESIG"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/26/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class DESIGCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_DESG;
        private String m_SP_BRANCH;
        private String m_SP_LEVEL;
        private String m_SP_CATEGORY;
        private String m_SP_DESC_DG1;
        private String m_SP_DESC_DG2;
        private decimal m_SP_CONFIRM;
        private decimal m_SP_MIN;
        private decimal m_SP_MID;
        private decimal m_SP_MAX;
        private decimal m_SP_INCREMENT;
        private DateTime m_SP_EFFECTIVE;
        private String m_SP_CURRENT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_DESG"
        public String SP_DESG
        {
            get { return m_SP_DESG; }
            set { m_SP_DESG = value; }
        }
        #endregion

        #region "SP_BRANCH"
        public String SP_BRANCH
        {
            get { return m_SP_BRANCH; }
            set { m_SP_BRANCH = value; }
        }
        #endregion

        #region "SP_LEVEL"
        public String SP_LEVEL
        {
            get { return m_SP_LEVEL; }
            set { m_SP_LEVEL = value; }
        }
        #endregion

        #region "SP_CATEGORY"
        public String SP_CATEGORY
        {
            get { return m_SP_CATEGORY; }
            set { m_SP_CATEGORY = value; }
        }
        #endregion

        #region "SP_DESC_DG1"
        public String SP_DESC_DG1
        {
            get { return m_SP_DESC_DG1; }
            set { m_SP_DESC_DG1 = value; }
        }
        #endregion

        #region "SP_DESC_DG2"
        public String SP_DESC_DG2
        {
            get { return m_SP_DESC_DG2; }
            set { m_SP_DESC_DG2 = value; }
        }
        #endregion

        #region "SP_CONFIRM"
        public decimal SP_CONFIRM
        {
            get { return m_SP_CONFIRM; }
            set { m_SP_CONFIRM = value; }
        }
        #endregion

        #region "SP_MIN"
        public decimal SP_MIN
        {
            get { return m_SP_MIN; }
            set { m_SP_MIN = value; }
        }
        #endregion

        #region "SP_MID"
        public decimal SP_MID
        {
            get { return m_SP_MID; }
            set { m_SP_MID = value; }
        }
        #endregion

        #region "SP_MAX"
        public decimal SP_MAX
        {
            get { return m_SP_MAX; }
            set { m_SP_MAX = value; }
        }
        #endregion

        #region "SP_INCREMENT"
        public decimal SP_INCREMENT
        {
            get { return m_SP_INCREMENT; }
            set { m_SP_INCREMENT = value; }
        }
        #endregion

        #region "SP_EFFECTIVE"
        public DateTime SP_EFFECTIVE
        {
            get { return m_SP_EFFECTIVE; }
            set { m_SP_EFFECTIVE = value; }
        }
        #endregion

        #region "SP_CURRENT"
        public String SP_CURRENT
        {
            get { return m_SP_CURRENT; }
            set { m_SP_CURRENT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_DESG = "SP_DESG";
        public static readonly string _SP_BRANCH = "SP_BRANCH";
        public static readonly string _SP_LEVEL = "SP_LEVEL";
        public static readonly string _SP_CATEGORY = "SP_CATEGORY";
        public static readonly string _SP_DESC_DG1 = "SP_DESC_DG1";
        public static readonly string _SP_DESC_DG2 = "SP_DESC_DG2";
        public static readonly string _SP_CONFIRM = "SP_CONFIRM";
        public static readonly string _SP_MIN = "SP_MIN";
        public static readonly string _SP_MID = "SP_MID";
        public static readonly string _SP_MAX = "SP_MAX";
        public static readonly string _SP_INCREMENT = "SP_INCREMENT";
        public static readonly string _SP_EFFECTIVE = "SP_EFFECTIVE";
        public static readonly string _SP_CURRENT = "SP_CURRENT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
