

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PERSONAL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	01/20/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class RegStHiEntPERSONALCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_PR_ADD1;
        private String m_PR_ADD2;
        private String m_PR_PHONE1;
        private String m_PR_PHONE2;
        private DateTime m_PR_D_BIRTH;
        private String m_PR_SEX;
        private String m_PR_MARITAL;
        private String m_PR_SPOUSE;
        private DateTime m_PR_BIRTH_SP;
        private decimal m_PR_NO_OF_CHILD;
        private String m_PR_ID_CARD_NO;
        private String m_PR_USER_IS_PRIME;
        private String m_PR_GROUP_LIFE;
        private String m_PR_GROUP_HOSP;
        private String m_PR_LANG_1;
        private String m_PR_LANG_2;
        private String m_PR_LANG_3;
        private String m_PR_LANG_4;
        private String m_PR_LANG_5;
        private String m_PR_LANG_6;
        private String m_PR_OPD;
        private DateTime m_PR_MARRIAGE;
        private String m_PR_NATIONALITY;
        private String m_PR_PER_ADDR;
        private String m_PR_MOBILE_NO;
        private String m_PR_PLC_BIRTH;
        private String m_PR_CRNT_PHONE;
        private String m_PR_FATHER_NAME;
        private String m_PR_MOTHER_NAME;
        private String m_PR_OLD_ID_CARD_NO;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PR_ADD1"
        public String PR_ADD1
        {
            get { return m_PR_ADD1; }
            set { m_PR_ADD1 = value; }
        }
        #endregion

        #region "PR_ADD2"
        public String PR_ADD2
        {
            get { return m_PR_ADD2; }
            set { m_PR_ADD2 = value; }
        }
        #endregion

        #region "PR_PHONE1"
        public String PR_PHONE1
        {
            get { return m_PR_PHONE1; }
            set { m_PR_PHONE1 = value; }
        }
        #endregion

        #region "PR_PHONE2"
        public String PR_PHONE2
        {
            get { return m_PR_PHONE2; }
            set { m_PR_PHONE2 = value; }
        }
        #endregion

        #region "PR_D_BIRTH"
        public DateTime PR_D_BIRTH
        {
            get { return m_PR_D_BIRTH; }
            set { m_PR_D_BIRTH = value; }
        }
        #endregion

        #region "PR_SEX"
        public String PR_SEX
        {
            get { return m_PR_SEX; }
            set { m_PR_SEX = value; }
        }
        #endregion

        #region "PR_MARITAL"
        public String PR_MARITAL
        {
            get { return m_PR_MARITAL; }
            set { m_PR_MARITAL = value; }
        }
        #endregion

        #region "PR_SPOUSE"
        public String PR_SPOUSE
        {
            get { return m_PR_SPOUSE; }
            set { m_PR_SPOUSE = value; }
        }
        #endregion

        #region "PR_BIRTH_SP"
        public DateTime PR_BIRTH_SP
        {
            get { return m_PR_BIRTH_SP; }
            set { m_PR_BIRTH_SP = value; }
        }
        #endregion

        #region "PR_NO_OF_CHILD"
        public decimal PR_NO_OF_CHILD
        {
            get { return m_PR_NO_OF_CHILD; }
            set { m_PR_NO_OF_CHILD = value; }
        }
        #endregion

        #region "PR_ID_CARD_NO"
        public String PR_ID_CARD_NO
        {
            get { return m_PR_ID_CARD_NO; }
            set { m_PR_ID_CARD_NO = value; }
        }
        #endregion

        #region "PR_USER_IS_PRIME"
        public String PR_USER_IS_PRIME
        {
            get { return m_PR_USER_IS_PRIME; }
            set { m_PR_USER_IS_PRIME = value; }
        }
        #endregion

        #region "PR_GROUP_LIFE"
        public String PR_GROUP_LIFE
        {
            get { return m_PR_GROUP_LIFE; }
            set { m_PR_GROUP_LIFE = value; }
        }
        #endregion

        #region "PR_GROUP_HOSP"
        public String PR_GROUP_HOSP
        {
            get { return m_PR_GROUP_HOSP; }
            set { m_PR_GROUP_HOSP = value; }
        }
        #endregion

        #region "PR_LANG_1"
        public String PR_LANG_1
        {
            get { return m_PR_LANG_1; }
            set { m_PR_LANG_1 = value; }
        }
        #endregion

        #region "PR_LANG_2"
        public String PR_LANG_2
        {
            get { return m_PR_LANG_2; }
            set { m_PR_LANG_2 = value; }
        }
        #endregion

        #region "PR_LANG_3"
        public String PR_LANG_3
        {
            get { return m_PR_LANG_3; }
            set { m_PR_LANG_3 = value; }
        }
        #endregion

        #region "PR_LANG_4"
        public String PR_LANG_4
        {
            get { return m_PR_LANG_4; }
            set { m_PR_LANG_4 = value; }
        }
        #endregion

        #region "PR_LANG_5"
        public String PR_LANG_5
        {
            get { return m_PR_LANG_5; }
            set { m_PR_LANG_5 = value; }
        }
        #endregion

        #region "PR_LANG_6"
        public String PR_LANG_6
        {
            get { return m_PR_LANG_6; }
            set { m_PR_LANG_6 = value; }
        }
        #endregion

        #region "PR_OPD"
        public String PR_OPD
        {
            get { return m_PR_OPD; }
            set { m_PR_OPD = value; }
        }
        #endregion

        #region "PR_MARRIAGE"
        public DateTime PR_MARRIAGE
        {
            get { return m_PR_MARRIAGE; }
            set { m_PR_MARRIAGE = value; }
        }
        #endregion

        #region "PR_NATIONALITY"
        public String PR_NATIONALITY
        {
            get { return m_PR_NATIONALITY; }
            set { m_PR_NATIONALITY = value; }
        }
        #endregion

        #region "PR_PER_ADDR"
        public String PR_PER_ADDR
        {
            get { return m_PR_PER_ADDR; }
            set { m_PR_PER_ADDR = value; }
        }
        #endregion

        #region "PR_MOBILE_NO"
        public String PR_MOBILE_NO
        {
            get { return m_PR_MOBILE_NO; }
            set { m_PR_MOBILE_NO = value; }
        }
        #endregion

        #region "PR_PLC_BIRTH"
        public String PR_PLC_BIRTH
        {
            get { return m_PR_PLC_BIRTH; }
            set { m_PR_PLC_BIRTH = value; }
        }
        #endregion

        #region "PR_CRNT_PHONE"
        public String PR_CRNT_PHONE
        {
            get { return m_PR_CRNT_PHONE; }
            set { m_PR_CRNT_PHONE = value; }
        }
        #endregion

        #region "PR_FATHER_NAME"
        public String PR_FATHER_NAME
        {
            get { return m_PR_FATHER_NAME; }
            set { m_PR_FATHER_NAME = value; }
        }
        #endregion

        #region "PR_MOTHER_NAME"
        public String PR_MOTHER_NAME
        {
            get { return m_PR_MOTHER_NAME; }
            set { m_PR_MOTHER_NAME = value; }
        }
        #endregion

        #region "PR_OLD_ID_CARD_NO"
        public String PR_OLD_ID_CARD_NO
        {
            get { return m_PR_OLD_ID_CARD_NO; }
            set { m_PR_OLD_ID_CARD_NO = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _PR_ADD1 = "PR_ADD1";
        public static readonly string _PR_ADD2 = "PR_ADD2";
        public static readonly string _PR_PHONE1 = "PR_PHONE1";
        public static readonly string _PR_PHONE2 = "PR_PHONE2";
        public static readonly string _PR_D_BIRTH = "PR_D_BIRTH";
        public static readonly string _PR_SEX = "PR_SEX";
        public static readonly string _PR_MARITAL = "PR_MARITAL";
        public static readonly string _PR_SPOUSE = "PR_SPOUSE";
        public static readonly string _PR_BIRTH_SP = "PR_BIRTH_SP";
        public static readonly string _PR_NO_OF_CHILD = "PR_NO_OF_CHILD";
        public static readonly string _PR_ID_CARD_NO = "PR_ID_CARD_NO";
        public static readonly string _PR_USER_IS_PRIME = "PR_USER_IS_PRIME";
        public static readonly string _PR_GROUP_LIFE = "PR_GROUP_LIFE";
        public static readonly string _PR_GROUP_HOSP = "PR_GROUP_HOSP";
        public static readonly string _PR_LANG_1 = "PR_LANG_1";
        public static readonly string _PR_LANG_2 = "PR_LANG_2";
        public static readonly string _PR_LANG_3 = "PR_LANG_3";
        public static readonly string _PR_LANG_4 = "PR_LANG_4";
        public static readonly string _PR_LANG_5 = "PR_LANG_5";
        public static readonly string _PR_LANG_6 = "PR_LANG_6";
        public static readonly string _PR_OPD = "PR_OPD";
        public static readonly string _PR_MARRIAGE = "PR_MARRIAGE";
        public static readonly string _PR_NATIONALITY = "PR_NATIONALITY";
        public static readonly string _PR_PER_ADDR = "PR_PER_ADDR";
        public static readonly string _PR_MOBILE_NO = "PR_MOBILE_NO";
        public static readonly string _PR_PLC_BIRTH = "PR_PLC_BIRTH";
        public static readonly string _PR_CRNT_PHONE = "PR_CRNT_PHONE";
        public static readonly string _PR_FATHER_NAME = "PR_FATHER_NAME";
        public static readonly string _PR_MOTHER_NAME = "PR_MOTHER_NAME";
        public static readonly string _PR_OLD_ID_CARD_NO = "PR_OLD_ID_CARD_NO";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
