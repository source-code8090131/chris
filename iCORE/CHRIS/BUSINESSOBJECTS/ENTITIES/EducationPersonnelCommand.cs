

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.EducationPersonnelCommand 
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "EDUCATION"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[AHAD ZUBAIR]	01/27/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class EducationPersonnelCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_E_NO;
        private decimal m_PR_E_YEAR;
        private String m_PR_DEGREE;
        private String m_PR_GRADE;
        private String m_PR_COLLAGE;
        private String m_PR_CITY;
        private String m_PR_COUNTRY;
        private String m_PR_DEG_TYPE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_E_NO; }
            set { m_PR_E_NO = value; }
        }
        #endregion

        #region "PR_E_YEAR"
        public decimal PR_E_YEAR
        {
            get { return m_PR_E_YEAR; }
            set { m_PR_E_YEAR = value; }
        }
        #endregion

        #region "PR_DEGREE"
        public String PR_DEGREE
        {
            get { return m_PR_DEGREE; }
            set { m_PR_DEGREE = value; }
        }
        #endregion

        #region "PR_GRADE"
        public String PR_GRADE
        {
            get { return m_PR_GRADE; }
            set { m_PR_GRADE = value; }
        }
        #endregion

        #region "PR_COLLAGE"
        public String PR_COLLAGE
        {
            get { return m_PR_COLLAGE; }
            set { m_PR_COLLAGE = value; }
        }
        #endregion

        #region "PR_CITY"
        public String PR_CITY
        {
            get { return m_PR_CITY; }
            set { m_PR_CITY = value; }
        }
        #endregion

        #region "PR_COUNTRY"
        public String PR_COUNTRY
        {
            get { return m_PR_COUNTRY; }
            set { m_PR_COUNTRY = value; }
        }
        #endregion

        #region "PR_DEG_TYPE"
        public String PR_DEG_TYPE
        {
            get { return m_PR_DEG_TYPE; }
            set { m_PR_DEG_TYPE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_E_NO";
        public static readonly string _PR_E_YEAR = "PR_E_YEAR";
        public static readonly string _PR_DEGREE = "PR_DEGREE";
        public static readonly string _PR_GRADE = "PR_GRADE";
        public static readonly string _PR_COLLAGE = "PR_COLLAGE";
        public static readonly string _PR_CITY = "PR_CITY";
        public static readonly string _PR_COUNTRY = "PR_COUNTRY";
        public static readonly string _PR_DEG_TYPE = "PR_DEG_TYPE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
