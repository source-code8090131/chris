

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeductionDetailsCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEDUCTION_DETAILS"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Anila]	01/25/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class DeductionDetailsCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
       
      
        private decimal m_SP_P_NO;
        private String m_SP_REMARKS;
        private decimal m_SP_DED_AMT;
        private int m_ID;
        private string m_PR_NAME;

        //added for creating foreign key relation
        private String m_SP_DED_CODE;

        #endregion "--Field Segment--"

        #region "--Property Segment--"

      

        #region "SP_P_NO"
        public decimal SP_P_NO
        {
            get { return m_SP_P_NO; }
            set { m_SP_P_NO = value; }
        }
        #endregion

        #region "SP_REMARKS"
        public String SP_REMARKS
        {
            get { return m_SP_REMARKS; }
            set { m_SP_REMARKS = value; }
        }
        #endregion

        #region "SP_DED_AMT"
        public decimal SP_DED_AMT
        {
            get { return m_SP_DED_AMT; }
            set { m_SP_DED_AMT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "PR_NAME"
        public String PR_NAME
        {
            get { return m_PR_NAME; }
            set { m_PR_NAME = value; }
        }
        #endregion

      
        #region "SP_DED_CODE"
        [CustomAttributes(IsForeignKey = true)]
        public String SP_DED_CODE
        {
            get { return m_SP_DED_CODE; }
            set { m_SP_DED_CODE = value; }
        }
        #endregion


        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_DED_CODE = "SP_DED_CODE";
        public static readonly string _SP_P_NO = "SP_P_NO";
        public static readonly string _SP_REMARKS = "SP_REMARKS";
        public static readonly string _SP_DED_AMT = "SP_DED_AMT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
