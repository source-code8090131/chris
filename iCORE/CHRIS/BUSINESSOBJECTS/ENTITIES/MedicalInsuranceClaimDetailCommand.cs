

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MedicalInsuranceClaimDetailCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "CLAIM_GHI"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Saud Akhter]	04/27/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class MedicalInsuranceClaimDetailCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_CLA_POLICY;
        private decimal m_CLA_P_NO;
        private decimal m_CLA_S_NO;
        private DateTime m_CLA_DATE;
        private decimal m_CLA_CLAIMED_AMT;
        private decimal m_CLA_REC_AMT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "CLA_POLICY"
        [CustomAttributes(IsForeignKey = true)]
        public String CLA_POLICY
        {
            get { return m_CLA_POLICY; }
            set { m_CLA_POLICY = value; }
        }
        #endregion

        #region "CLA_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public decimal CLA_P_NO
        {
            get { return m_CLA_P_NO; }
            set { m_CLA_P_NO = value; }
        }
        #endregion

        #region "CLA_S_NO"
        public decimal CLA_S_NO
        {
            get { return m_CLA_S_NO; }
            set { m_CLA_S_NO = value; }
        }
        #endregion

        #region "CLA_DATE"
        public DateTime CLA_DATE
        {
            get { return m_CLA_DATE; }
            set { m_CLA_DATE = value; }
        }
        #endregion

        #region "CLA_CLAIMED_AMT"
        public decimal CLA_CLAIMED_AMT
        {
            get { return m_CLA_CLAIMED_AMT; }
            set { m_CLA_CLAIMED_AMT = value; }
        }
        #endregion

        #region "CLA_REC_AMT"
        public decimal CLA_REC_AMT
        {
            get { return m_CLA_REC_AMT; }
            set { m_CLA_REC_AMT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _CLA_POLICY = "CLA_POLICY";
        public static readonly string _CLA_P_NO = "CLA_P_NO";
        public static readonly string _CLA_S_NO = "CLA_S_NO";
        public static readonly string _CLA_DATE = "CLA_DATE";
        public static readonly string _CLA_CLAIMED_AMT = "CLA_CLAIMED_AMT";
        public static readonly string _CLA_REC_AMT = "CLA_REC_AMT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}