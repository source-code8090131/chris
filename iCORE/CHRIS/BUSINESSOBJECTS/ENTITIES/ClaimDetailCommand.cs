

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ClaimDetailCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "OMI_CLAIM_DETAIL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	04/27/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class ClaimDetailCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_OMI_P_NO;
        private decimal m_OMI_MONTH;
        private decimal m_OMI_YEAR;
        private decimal m_OMI_CLAIM_NO;
        private DateTime m_OMI_BILL_DATE;
        private String m_OMI_PATIENTS_NAME;
        private String m_OMI_RELATION;
        private decimal m_OMI_CLAIM_AMOUNT;
        private decimal m_OMI_RECEV_AMOUNT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "OMI_P_NO"
        public decimal OMI_P_NO
        {
            get { return m_OMI_P_NO; }
            set { m_OMI_P_NO = value; }
        }
        #endregion

        #region "OMI_MONTH"
        public decimal OMI_MONTH
        {
            get { return m_OMI_MONTH; }
            set { m_OMI_MONTH = value; }
        }
        #endregion

        #region "OMI_YEAR"
        public decimal OMI_YEAR
        {
            get { return m_OMI_YEAR; }
            set { m_OMI_YEAR = value; }
        }
        #endregion

        #region "OMI_CLAIM_NO"
        public decimal OMI_CLAIM_NO
        {
            get { return m_OMI_CLAIM_NO; }
            set { m_OMI_CLAIM_NO = value; }
        }
        #endregion

        #region "OMI_BILL_DATE"
        public DateTime OMI_BILL_DATE
        {
            get { return m_OMI_BILL_DATE; }
            set { m_OMI_BILL_DATE = value; }
        }
        #endregion

        #region "OMI_PATIENTS_NAME"
        public String OMI_PATIENTS_NAME
        {
            get { return m_OMI_PATIENTS_NAME; }
            set { m_OMI_PATIENTS_NAME = value; }
        }
        #endregion

        #region "OMI_RELATION"
        public String OMI_RELATION
        {
            get { return m_OMI_RELATION; }
            set { m_OMI_RELATION = value; }
        }
        #endregion

        #region "OMI_CLAIM_AMOUNT"
        public decimal OMI_CLAIM_AMOUNT
        {
            get { return m_OMI_CLAIM_AMOUNT; }
            set { m_OMI_CLAIM_AMOUNT = value; }
        }
        #endregion

        #region "OMI_RECEV_AMOUNT"
        public decimal OMI_RECEV_AMOUNT
        {
            get { return m_OMI_RECEV_AMOUNT; }
            set { m_OMI_RECEV_AMOUNT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _OMI_P_NO = "OMI_P_NO";
        public static readonly string _OMI_MONTH = "OMI_MONTH";
        public static readonly string _OMI_YEAR = "OMI_YEAR";
        public static readonly string _OMI_CLAIM_NO = "OMI_CLAIM_NO";
        public static readonly string _OMI_BILL_DATE = "OMI_BILL_DATE";
        public static readonly string _OMI_PATIENTS_NAME = "OMI_PATIENTS_NAME";
        public static readonly string _OMI_RELATION = "OMI_RELATION";
        public static readonly string _OMI_CLAIM_AMOUNT = "OMI_CLAIM_AMOUNT";
        public static readonly string _OMI_RECEV_AMOUNT = "OMI_RECEV_AMOUNT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
