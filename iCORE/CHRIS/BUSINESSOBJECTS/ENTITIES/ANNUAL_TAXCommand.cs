/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ANNUAL_TAXCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "ANNUAL_TAX"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Tahnia]	01/17/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class ANNUAL_TAXCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_F_PNO;
        //private decimal m_AN_P_NO;
        private String m_AN_P_NAME;
        private String m_AN_TAX_NUM;
        private String m_AN_ADD1;
        private String m_AN_ADD2;
        private String m_AN_DESIG;
        private String m_AN_BRANCH;
        private String m_AN_CATEGORY;
        private String m_AN_LEVEL;
        private decimal m_AN_ASSES_FROM;
        private decimal m_AN_ASSES_TO;
        private DateTime m_AN_TAX_FROM_DATE;
        private DateTime m_AN_TAX_TO_DATE;
        private decimal m_AN_10C_BONUS;
        private decimal m_AN_INC_BONUS;
        private decimal m_AN_BASIC;
        private decimal m_AN_HOUSE_RENT;
        private decimal m_AN_CONV;
        private decimal m_AN_CONV_ADDED;
        private decimal m_AN_ZAKAT;
        private decimal m_AN_OVT;
        private decimal m_AN_TAX_ASR;
        private decimal m_AN_TAX_PAID;
        private decimal m_AN_TAXABLE_INC;
        private decimal m_AN_PFUND;
        private decimal m_AN_REBATE;
        private decimal m_AN_TAX_FROM_AMT;
        private decimal m_AN_FIX_TAX;
        private decimal m_AN_REM_AMT;
        private decimal m_AN_TAX_REM;
        private decimal m_AN_SUR;
        private decimal m_AN_PF_BAL;
        private String m_AN_FLAG;
        private decimal m_AN_TAXABLE_ALL;
        private decimal m_AN_LFA;
        private decimal m_AN_REFUND_AMT;
        private String m_AN_REFUND_FOR;
        private decimal m_AN_GHA_ALL;
        private decimal m_AN_EDUCATION;
        private decimal m_AN_SER_SAL;
        private decimal m_AN_L_H_WATER;
        private decimal m_AN_RNT_FREE_UNFURNISHED;
        private decimal m_AN_RNT_FREE_FURNISHED;
        private decimal m_PRIVATE;
        private decimal m_GRATUITY;
        private decimal m_UNFURNISHED_SQ;
        private decimal m_FURNISHED_SQ;
        private decimal m_AN_SEVARANCE_AMT;
        private decimal m_AN_SUBS_LOAN_TAX;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "F_PNO"
        public decimal F_PNO
        {
            get { return m_F_PNO; }
            set { m_F_PNO = value; }
        }
        #endregion

        #region "AN_P_NAME"
        public String AN_P_NAME
        {
            get { return m_AN_P_NAME; }
            set { m_AN_P_NAME = value; }
        }
        #endregion

        #region "AN_TAX_NUM"
        public String AN_TAX_NUM
        {
            get { return m_AN_TAX_NUM; }
            set { m_AN_TAX_NUM = value; }
        }
        #endregion

        #region "AN_ADD1"
        public String AN_ADD1
        {
            get { return m_AN_ADD1; }
            set { m_AN_ADD1 = value; }
        }
        #endregion

        #region "AN_ADD2"
        public String AN_ADD2
        {
            get { return m_AN_ADD2; }
            set { m_AN_ADD2 = value; }
        }
        #endregion

        #region "AN_DESIG"
        public String AN_DESIG
        {
            get { return m_AN_DESIG; }
            set { m_AN_DESIG = value; }
        }
        #endregion

        #region "AN_BRANCH"
        public String AN_BRANCH
        {
            get { return m_AN_BRANCH; }
            set { m_AN_BRANCH = value; }
        }
        #endregion

        #region "AN_CATEGORY"
        public String AN_CATEGORY
        {
            get { return m_AN_CATEGORY; }
            set { m_AN_CATEGORY = value; }
        }
        #endregion

        #region "AN_LEVEL"
        public String AN_LEVEL
        {
            get { return m_AN_LEVEL; }
            set { m_AN_LEVEL = value; }
        }
        #endregion

        #region "AN_ASSES_FROM"
        public decimal AN_ASSES_FROM
        {
            get { return m_AN_ASSES_FROM; }
            set { m_AN_ASSES_FROM = value; }
        }
        #endregion

        #region "AN_ASSES_TO"
        public decimal AN_ASSES_TO
        {
            get { return m_AN_ASSES_TO; }
            set { m_AN_ASSES_TO = value; }
        }
        #endregion

        #region "AN_TAX_FROM_DATE"
        public DateTime AN_TAX_FROM_DATE
        {
            get { return m_AN_TAX_FROM_DATE; }
            set { m_AN_TAX_FROM_DATE = value; }
        }
        #endregion

        #region "AN_TAX_TO_DATE"
        public DateTime AN_TAX_TO_DATE
        {
            get { return m_AN_TAX_TO_DATE; }
            set { m_AN_TAX_TO_DATE = value; }
        }
        #endregion

        #region "AN_10C_BONUS"
        public decimal AN_10C_BONUS
        {
            get { return m_AN_10C_BONUS; }
            set { m_AN_10C_BONUS = value; }
        }
        #endregion

        #region "AN_INC_BONUS"
        public decimal AN_INC_BONUS
        {
            get { return m_AN_INC_BONUS; }
            set { m_AN_INC_BONUS = value; }
        }
        #endregion

        #region "AN_BASIC"
        public decimal AN_BASIC
        {
            get { return m_AN_BASIC; }
            set { m_AN_BASIC = value; }
        }
        #endregion

        #region "AN_HOUSE_RENT"
        public decimal AN_HOUSE_RENT
        {
            get { return m_AN_HOUSE_RENT; }
            set { m_AN_HOUSE_RENT = value; }
        }
        #endregion

        #region "AN_CONV"
        public decimal AN_CONV
        {
            get { return m_AN_CONV; }
            set { m_AN_CONV = value; }
        }
        #endregion

        #region "AN_CONV_ADDED"
        public decimal AN_CONV_ADDED
        {
            get { return m_AN_CONV_ADDED; }
            set { m_AN_CONV_ADDED = value; }
        }
        #endregion

        #region "AN_ZAKAT"
        public decimal AN_ZAKAT
        {
            get { return m_AN_ZAKAT; }
            set { m_AN_ZAKAT = value; }
        }
        #endregion

        #region "AN_OVT"
        public decimal AN_OVT
        {
            get { return m_AN_OVT; }
            set { m_AN_OVT = value; }
        }
        #endregion

        #region "AN_TAX_ASR"
        public decimal AN_TAX_ASR
        {
            get { return m_AN_TAX_ASR; }
            set { m_AN_TAX_ASR = value; }
        }
        #endregion

        #region "AN_TAX_PAID"
        public decimal AN_TAX_PAID
        {
            get { return m_AN_TAX_PAID; }
            set { m_AN_TAX_PAID = value; }
        }
        #endregion

        #region "AN_TAXABLE_INC"
        public decimal AN_TAXABLE_INC
        {
            get { return m_AN_TAXABLE_INC; }
            set { m_AN_TAXABLE_INC = value; }
        }
        #endregion

        #region "AN_PFUND"
        public decimal AN_PFUND
        {
            get { return m_AN_PFUND; }
            set { m_AN_PFUND = value; }
        }
        #endregion

        #region "AN_REBATE"
        public decimal AN_REBATE
        {
            get { return m_AN_REBATE; }
            set { m_AN_REBATE = value; }
        }
        #endregion

        #region "AN_TAX_FROM_AMT"
        public decimal AN_TAX_FROM_AMT
        {
            get { return m_AN_TAX_FROM_AMT; }
            set { m_AN_TAX_FROM_AMT = value; }
        }
        #endregion

        #region "AN_FIX_TAX"
        public decimal AN_FIX_TAX
        {
            get { return m_AN_FIX_TAX; }
            set { m_AN_FIX_TAX = value; }
        }
        #endregion

        #region "AN_REM_AMT"
        public decimal AN_REM_AMT
        {
            get { return m_AN_REM_AMT; }
            set { m_AN_REM_AMT = value; }
        }
        #endregion

        #region "AN_TAX_REM"
        public decimal AN_TAX_REM
        {
            get { return m_AN_TAX_REM; }
            set { m_AN_TAX_REM = value; }
        }
        #endregion

        #region "AN_SUR"
        public decimal AN_SUR
        {
            get { return m_AN_SUR; }
            set { m_AN_SUR = value; }
        }
        #endregion

        #region "AN_PF_BAL"
        public decimal AN_PF_BAL
        {
            get { return m_AN_PF_BAL; }
            set { m_AN_PF_BAL = value; }
        }
        #endregion

        #region "AN_FLAG"
        public String AN_FLAG
        {
            get { return m_AN_FLAG; }
            set { m_AN_FLAG = value; }
        }
        #endregion

        #region "AN_TAXABLE_ALL"
        public decimal AN_TAXABLE_ALL
        {
            get { return m_AN_TAXABLE_ALL; }
            set { m_AN_TAXABLE_ALL = value; }
        }
        #endregion

        #region "AN_LFA"
        public decimal AN_LFA
        {
            get { return m_AN_LFA; }
            set { m_AN_LFA = value; }
        }
        #endregion

        #region "AN_REFUND_AMT"
        public decimal AN_REFUND_AMT
        {
            get { return m_AN_REFUND_AMT; }
            set { m_AN_REFUND_AMT = value; }
        }
        #endregion

        #region "AN_REFUND_FOR"
        public String AN_REFUND_FOR
        {
            get { return m_AN_REFUND_FOR; }
            set { m_AN_REFUND_FOR = value; }
        }
        #endregion

        #region "AN_GHA_ALL"
        public decimal AN_GHA_ALL
        {
            get { return m_AN_GHA_ALL; }
            set { m_AN_GHA_ALL = value; }
        }
        #endregion

        #region "AN_EDUCATION"
        public decimal AN_EDUCATION
        {
            get { return m_AN_EDUCATION; }
            set { m_AN_EDUCATION = value; }
        }
        #endregion

        #region "AN_SER_SAL"
        public decimal AN_SER_SAL
        {
            get { return m_AN_SER_SAL; }
            set { m_AN_SER_SAL = value; }
        }
        #endregion

        #region "AN_L_H_WATER"
        public decimal AN_L_H_WATER
        {
            get { return m_AN_L_H_WATER; }
            set { m_AN_L_H_WATER = value; }
        }
        #endregion

        #region "AN_RNT_FREE_UNFURNISHED"
        public decimal AN_RNT_FREE_UNFURNISHED
        {
            get { return m_AN_RNT_FREE_UNFURNISHED; }
            set { m_AN_RNT_FREE_UNFURNISHED = value; }
        }
        #endregion

        #region "AN_RNT_FREE_FURNISHED"
        public decimal AN_RNT_FREE_FURNISHED
        {
            get { return m_AN_RNT_FREE_FURNISHED; }
            set { m_AN_RNT_FREE_FURNISHED = value; }
        }
        #endregion

        #region "PRIVATE"
        public decimal PRIVATE
        {
            get { return m_PRIVATE; }
            set { m_PRIVATE = value; }
        }
        #endregion

        #region "GRATUITY"
        public decimal GRATUITY
        {
            get { return m_GRATUITY; }
            set { m_GRATUITY = value; }
        }
        #endregion

        #region "UNFURNISHED_SQ"
        public decimal UNFURNISHED_SQ
        {
            get { return m_UNFURNISHED_SQ; }
            set { m_UNFURNISHED_SQ = value; }
        }
        #endregion

        #region "FURNISHED_SQ"
        public decimal FURNISHED_SQ
        {
            get { return m_FURNISHED_SQ; }
            set { m_FURNISHED_SQ = value; }
        }
        #endregion

        #region "AN_SEVARANCE_AMT"
        public decimal AN_SEVARANCE_AMT
        {
            get { return m_AN_SEVARANCE_AMT; }
            set { m_AN_SEVARANCE_AMT = value; }
        }
        #endregion

        #region "AN_SUBS_LOAN_TAX"
        public decimal AN_SUBS_LOAN_TAX
        {
            get { return m_AN_SUBS_LOAN_TAX; }
            set { m_AN_SUBS_LOAN_TAX = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _AN_P_NO = "AN_P_NO";
        public static readonly string _AN_P_NAME = "AN_P_NAME";
        public static readonly string _AN_TAX_NUM = "AN_TAX_NUM";
        public static readonly string _AN_ADD1 = "AN_ADD1";
        public static readonly string _AN_ADD2 = "AN_ADD2";
        public static readonly string _AN_DESIG = "AN_DESIG";
        public static readonly string _AN_BRANCH = "AN_BRANCH";
        public static readonly string _AN_CATEGORY = "AN_CATEGORY";
        public static readonly string _AN_LEVEL = "AN_LEVEL";
        public static readonly string _AN_ASSES_FROM = "AN_ASSES_FROM";
        public static readonly string _AN_ASSES_TO = "AN_ASSES_TO";
        public static readonly string _AN_TAX_FROM_DATE = "AN_TAX_FROM_DATE";
        public static readonly string _AN_TAX_TO_DATE = "AN_TAX_TO_DATE";
        public static readonly string _AN_10C_BONUS = "AN_10C_BONUS";
        public static readonly string _AN_INC_BONUS = "AN_INC_BONUS";
        public static readonly string _AN_BASIC = "AN_BASIC";
        public static readonly string _AN_HOUSE_RENT = "AN_HOUSE_RENT";
        public static readonly string _AN_CONV = "AN_CONV";
        public static readonly string _AN_CONV_ADDED = "AN_CONV_ADDED";
        public static readonly string _AN_ZAKAT = "AN_ZAKAT";
        public static readonly string _AN_OVT = "AN_OVT";
        public static readonly string _AN_TAX_ASR = "AN_TAX_ASR";
        public static readonly string _AN_TAX_PAID = "AN_TAX_PAID";
        public static readonly string _AN_TAXABLE_INC = "AN_TAXABLE_INC";
        public static readonly string _AN_PFUND = "AN_PFUND";
        public static readonly string _AN_REBATE = "AN_REBATE";
        public static readonly string _AN_TAX_FROM_AMT = "AN_TAX_FROM_AMT";
        public static readonly string _AN_FIX_TAX = "AN_FIX_TAX";
        public static readonly string _AN_REM_AMT = "AN_REM_AMT";
        public static readonly string _AN_TAX_REM = "AN_TAX_REM";
        public static readonly string _AN_SUR = "AN_SUR";
        public static readonly string _AN_PF_BAL = "AN_PF_BAL";
        public static readonly string _AN_FLAG = "AN_FLAG";
        public static readonly string _AN_TAXABLE_ALL = "AN_TAXABLE_ALL";
        public static readonly string _AN_LFA = "AN_LFA";
        public static readonly string _AN_REFUND_AMT = "AN_REFUND_AMT";
        public static readonly string _AN_REFUND_FOR = "AN_REFUND_FOR";
        public static readonly string _AN_GHA_ALL = "AN_GHA_ALL";
        public static readonly string _AN_EDUCATION = "AN_EDUCATION";
        public static readonly string _AN_SER_SAL = "AN_SER_SAL";
        public static readonly string _AN_L_H_WATER = "AN_L_H_WATER";
        public static readonly string _AN_RNT_FREE_UNFURNISHED = "AN_RNT_FREE_UNFURNISHED";
        public static readonly string _AN_RNT_FREE_FURNISHED = "AN_RNT_FREE_FURNISHED";
        public static readonly string _PRIVATE = "PRIVATE";
        public static readonly string _GRATUITY = "GRATUITY";
        public static readonly string _UNFURNISHED_SQ = "UNFURNISHED_SQ";
        public static readonly string _FURNISHED_SQ = "FURNISHED_SQ";
        public static readonly string _AN_SEVARANCE_AMT = "AN_SEVARANCE_AMT";
        public static readonly string _AN_SUBS_LOAN_TAX = "AN_SUBS_LOAN_TAX";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}

