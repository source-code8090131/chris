

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEPT_CONT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	01/10/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class DeptContTransferCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_TR_NO;
        private String m_PR_SEGMENT;
        private String m_PR_DEPT;
        private decimal m_PR_CONTRIB;
        private decimal m_PR_MENU_OPTION;
        private String m_PR_TYPE;
        private DateTime m_PR_EFFECTIVE;
        private int m_ID;
        private decimal m_PR_TRANSFER;
        #endregion "--Field Segment--"

        #region "--Property Segment--"
        
        [CustomAttributes(IsForeignKey = true)]
        #region "PR_P_NO"       
        public Double PR_TR_NO
        {
            get { return m_PR_TR_NO; }
            set { m_PR_TR_NO = value; }
        }
        #endregion

        #region "PR_SEGMENT"
        public String PR_SEGMENT
        {
            get { return m_PR_SEGMENT; }
            set { m_PR_SEGMENT = value; }
        }
        #endregion

        #region "PR_DEPT"
        public String PR_DEPT
        {
            get { return m_PR_DEPT; }
            set { m_PR_DEPT = value; }
        }
        #endregion

        #region "PR_CONTRIB"
        public decimal PR_CONTRIB
        {
            get { return m_PR_CONTRIB; }
            set { m_PR_CONTRIB = value; }
        }
        #endregion
      
        #region "PR_MENU_OPTION"
        public decimal PR_MENU_OPTION
        {
            get { return m_PR_MENU_OPTION; }
            set { m_PR_MENU_OPTION = value; }
        }
        #endregion

        #region "PR_TYPE"
        public String PR_TYPE
        {
            get { return m_PR_TYPE; }
            set { m_PR_TYPE = value; }
        }
        #endregion

        [CustomAttributes(IsForeignKey = true)]
        #region "PR_EFFECTIVE" 
        public DateTime PR_EFFECTIVE
        {
            get { return m_PR_EFFECTIVE; }
            set { m_PR_EFFECTIVE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        //[CustomAttributes(IsForeignKey = true)]
        //#region "PR_TRANSFER"
        //public decimal PR_TRANSFER
        //{
        //    get { return m_PR_TRANSFER; }
        //    set { m_PR_TRANSFER = value; }
        //}
        //#endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_TR_NO = "PR_TR_NO";
        public static readonly string _PR_SEGMENT = "PR_SEGMENT";
        public static readonly string _PR_DEPT = "PR_DEPT";
        public static readonly string _PR_CONTRIB = "PR_CONTRIB";
        public static readonly string _PR_MENU_OPTION = "PR_MENU_OPTION";
        public static readonly string _PR_TYPE = "PR_TYPE";
        public static readonly string _PR_EFFECTIVE = "PR_EFFECTIVE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
