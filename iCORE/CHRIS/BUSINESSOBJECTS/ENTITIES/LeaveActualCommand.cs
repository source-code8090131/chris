

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LEAVE_ACTUALCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "LEAVE_ACTUAL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/12/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LEAVE_ACTUALCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_LEV_PNO;
        private String m_LEV_LV_TYPE;
        private DateTime m_LEV_START_DATE;
        private DateTime m_LEV_END_DATE;
        private String m_LEV_MED_CERTIF;
        private String m_LEV_APPROVED;
        private String m_LEV_REMARKS;
        private DateTime m_W_JOINING;
        private int m_w_pl_adv;
        private int m_lc_pl_bal;
        private int m_lc_cl_bal;
        private int m_lc_sl_bal;
        private int m_lc_ml_bal;
        private int m_lc_pl_avail;
        private int m_lc_ml_avail;
        private int m_lc_cl_avail;
        private int m_lc_sl_avail;
        private int m_pl;
        private int m_cl;
        private int m_ml;
        private int m_sl;
        private int m_slh;        
        private string m_lc_mandatory;
        private string m_LA_ID;

       
        
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "LA_ID"
        public string LA_ID
                {
                    get { return m_LA_ID; }
                    set { m_LA_ID = value; }
                }
        #endregion
                
        #region "LEV_PNO"
                public decimal LEV_PNO
        {
            get { return m_LEV_PNO; }
            set { m_LEV_PNO = value; }
        }
        #endregion
        #region "LEV_LV_TYPE"
        public String LEV_LV_TYPE
        {
            get { return m_LEV_LV_TYPE; }
            set { m_LEV_LV_TYPE = value; }
        }
        #endregion
        #region "LEV_START_DATE"
        public DateTime LEV_START_DATE
        {
            get { return m_LEV_START_DATE; }
            set { m_LEV_START_DATE = value; }
        }
        #endregion
        #region "LEV_END_DATE"
        public DateTime LEV_END_DATE
        {
            get { return m_LEV_END_DATE; }
            set { m_LEV_END_DATE = value; }
        }
        #endregion
        #region "W_JOINING"
        public DateTime W_JOINING
        {
            get { return m_W_JOINING; }
            set { m_W_JOINING = value; }
        }
        #endregion
        #region "LEV_MED_CERTIF"
        public String LEV_MED_CERTIF
        {
            get { return m_LEV_MED_CERTIF; }
            set { m_LEV_MED_CERTIF = value; }
        }
        #endregion
        #region "LEV_APPROVED"
        public String LEV_APPROVED
        {
            get { return m_LEV_APPROVED; }
            set { m_LEV_APPROVED = value; }
        }
        #endregion
        #region "LEV_REMARKS"
        public String LEV_REMARKS
        {
            get { return m_LEV_REMARKS; }
            set { m_LEV_REMARKS = value; }
        }


        #endregion        
        # region "w_pl_adv"
        public int w_pl_adv
        {
            get { return m_w_pl_adv;}
            set {m_w_pl_adv = value;}

        }
        #endregion
        #region "lc_pl_bal"
        public int lc_pl_bal
        {
            get { return m_lc_pl_bal; }
            set { m_lc_pl_bal = value; }
        }
        #endregion
        #region "lc_sl_bal"
        public int lc_sl_bal
        {
            get { return m_lc_sl_bal; }
            set { m_lc_sl_bal = value; }
        }
        #endregion
        #region "lc_cl_bal"
        public int lc_cl_bal
        {
            get { return m_lc_cl_bal; }
            set { m_lc_cl_bal = value; }
        }
        #endregion
        #region "lc_ml_bal"
        public int lc_ml_bal
        {
            get { return m_lc_ml_bal; }
            set { m_lc_ml_bal = value; }
        }
        #endregion
        #region "lc_pl_avail"
        public int lc_pl_avail
        {
            get { return m_lc_pl_avail; }
            set { m_lc_pl_avail = value; }
        }
        #endregion
        #region "lc_ml_avail"
        public int lc_ml_avail
        {
            get { return m_lc_ml_avail; }
            set { m_lc_ml_avail = value; }
        }
        #endregion
        #region "lc_sl_avail"
        public int lc_sl_avail
        {
            get { return m_lc_sl_avail; }
            set { m_lc_sl_avail = value; }
        }
         #endregion
        #region "lc_cl_avail"
        public int lc_cl_avail
        {
            get { return m_lc_cl_avail; }
            set { m_lc_cl_avail = value; }
        }
        #endregion
        #region "pl"
        public int pl
        {
            get { return m_pl; }
            set { m_pl = value; }
        }
        #endregion
        #region "cl"
        public int cl
        {
            get { return m_cl; }
            set { m_cl = value; }
        }
        #endregion
        #region "ml"
        public int ml
        {
            get { return m_ml; }
            set { m_ml = value; }
        }
        #endregion
        #region "sl"
        public int sl
        {
            get { return m_sl; }
            set { m_sl = value; }
        }
        #endregion
        #region "slh"
        public int slh
        {
            get { return m_slh; }
            set { m_slh = value; }
        }
        #endregion
        #region "lc_mandatory"
        public string lc_mandatory
        {
            get { return m_lc_mandatory; }
            set { m_lc_mandatory = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _LEV_PNO = "LEV_PNO";
        public static readonly string _LEV_LV_TYPE = "LEV_LV_TYPE";
        public static readonly string _LEV_START_DATE = "LEV_START_DATE";
        public static readonly string _LEV_END_DATE = "LEV_END_DATE";
        public static readonly string _LEV_MED_CERTIF = "LEV_MED_CERTIF";
        public static readonly string _LEV_APPROVED = "LEV_APPROVED";
        public static readonly string _LEV_REMARKS = "LEV_REMARKS";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
