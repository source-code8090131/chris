

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.INCOMECommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "INCOME"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/25/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class INCOMECommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity" 

        #region "--Field Segment--"
        private double m_SP_AMT_FROM;
        private double m_SP_AMT_TO;
        private decimal m_SP_REBATE;
        private decimal m_SP_PERCEN;
        private decimal m_SP_TAX_AMT;
        private decimal m_SP_SURCHARGE;
        private decimal m_SP_FREBATE;
        private decimal m_SP_FPERCEN;
        private decimal m_SP_FTAX_AMT;
        private decimal m_SP_FSURCHARGE;
        private decimal m_SP_ADD_TAX;
        private decimal m_SP_ADD_TAX_AMT;
        private decimal m_TAX_RED_PER;
        private decimal m_FTAX_RED_PER;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_AMT_FROM"
        public double SP_AMT_FROM
        {
            get { return m_SP_AMT_FROM; }
            set { m_SP_AMT_FROM = value; }
        }
        #endregion

        #region "SP_AMT_TO"
        public double SP_AMT_TO
        {
            get { return m_SP_AMT_TO; }
            set { m_SP_AMT_TO = value; }
        }
        #endregion

        #region "SP_REBATE"
        public decimal SP_REBATE
        {
            get { return m_SP_REBATE; }
            set { m_SP_REBATE = value; }
        }
        #endregion

        #region "SP_PERCEN"
        public decimal SP_PERCEN
        {
            get { return m_SP_PERCEN; }
            set { m_SP_PERCEN = value; }
        }
        #endregion

        #region "SP_TAX_AMT"
        public decimal SP_TAX_AMT
        {
            get { return m_SP_TAX_AMT; }
            set { m_SP_TAX_AMT = value; }
        }
        #endregion

        #region "SP_SURCHARGE"
        public decimal SP_SURCHARGE
        {
            get { return m_SP_SURCHARGE; }
            set { m_SP_SURCHARGE = value; }
        }
        #endregion

        #region "SP_FREBATE"
        public decimal SP_FREBATE
        {
            get { return m_SP_FREBATE; }
            set { m_SP_FREBATE = value; }
        }
        #endregion

        #region "SP_FPERCEN"
        public decimal SP_FPERCEN
        {
            get { return m_SP_FPERCEN; }
            set { m_SP_FPERCEN = value; }
        }
        #endregion

        #region "SP_FTAX_AMT"
        public decimal SP_FTAX_AMT
        {
            get { return m_SP_FTAX_AMT; }
            set { m_SP_FTAX_AMT = value; }
        }
        #endregion

        #region "SP_FSURCHARGE"
        public decimal SP_FSURCHARGE
        {
            get { return m_SP_FSURCHARGE; }
            set { m_SP_FSURCHARGE = value; }
        }
        #endregion

        #region "SP_ADD_TAX"
        public decimal SP_ADD_TAX
        {
            get { return m_SP_ADD_TAX; }
            set { m_SP_ADD_TAX = value; }
        }
        #endregion

        #region "SP_ADD_TAX_AMT"
        public decimal SP_ADD_TAX_AMT
        {
            get { return m_SP_ADD_TAX_AMT; }
            set { m_SP_ADD_TAX_AMT = value; }
        }
        #endregion

        #region "TAX_RED_PER"
        public decimal TAX_RED_PER
        {
            get { return m_TAX_RED_PER; }
            set { m_TAX_RED_PER = value; }
        }
        #endregion

        #region "FTAX_RED_PER"
        public decimal FTAX_RED_PER
        {
            get { return m_FTAX_RED_PER; }
            set { m_FTAX_RED_PER = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_AMT_FROM = "SP_AMT_FROM";
        public static readonly string _SP_AMT_TO = "SP_AMT_TO";
        public static readonly string _SP_REBATE = "SP_REBATE";
        public static readonly string _SP_PERCEN = "SP_PERCEN";
        public static readonly string _SP_TAX_AMT = "SP_TAX_AMT";
        public static readonly string _SP_SURCHARGE = "SP_SURCHARGE";
        public static readonly string _SP_FREBATE = "SP_FREBATE";
        public static readonly string _SP_FPERCEN = "SP_FPERCEN";
        public static readonly string _SP_FTAX_AMT = "SP_FTAX_AMT";
        public static readonly string _SP_FSURCHARGE = "SP_FSURCHARGE";
        public static readonly string _SP_ADD_TAX = "SP_ADD_TAX";
        public static readonly string _SP_ADD_TAX_AMT = "SP_ADD_TAX_AMT";
        public static readonly string _TAX_RED_PER = "TAX_RED_PER";
        public static readonly string _FTAX_RED_PER = "FTAX_RED_PER";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
