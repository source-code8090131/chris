

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeductionCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEDUCTION"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Anila]	01/25/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class DeductionCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_DED_CODE;
        private String m_SP_BRANCH_D;
        private String m_SP_DED_DESC;
        private String m_SP_CATEGORY_D;
        private String m_SP_DESG_D;
        private String m_SP_LEVEL_D;
        private DateTime m_SP_VALID_FROM_D;
        private DateTime m_SP_VALID_TO_D;
        private decimal m_SP_DED_AMOUNT;
        private decimal? m_SP_DED_PER;
        private String m_SP_ALL_IND;
        private String m_SP_ACOUNT_NO_D;
        private int m_ID;
        private string m_PR_NAME;
        private string m_SP_ACC_DESC;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_DED_CODE"
        public String SP_DED_CODE
        {
            get { return m_SP_DED_CODE; }
            set { m_SP_DED_CODE = value; }
        }
        #endregion

        #region "SP_BRANCH_D"
        public String SP_BRANCH_D
        {
            get { return m_SP_BRANCH_D; }
            set { m_SP_BRANCH_D = value; }
        }
        #endregion

        #region "SP_DED_DESC"
        public String SP_DED_DESC
        {
            get { return m_SP_DED_DESC; }
            set { m_SP_DED_DESC = value; }
        }
        #endregion

        #region "SP_CATEGORY_D"
        public String SP_CATEGORY_D
        {
            get { return m_SP_CATEGORY_D; }
            set { m_SP_CATEGORY_D = value; }
        }
        #endregion

        #region "SP_DESG_D"
        public String SP_DESG_D
        {
            get { return m_SP_DESG_D; }
            set { m_SP_DESG_D = value; }
        }
        #endregion

        #region "SP_LEVEL_D"
        public String SP_LEVEL_D
        {
            get { return m_SP_LEVEL_D; }
            set { m_SP_LEVEL_D = value; }
        }
        #endregion

        #region "SP_VALID_FROM_D"
        public DateTime SP_VALID_FROM_D
        {
            get { return m_SP_VALID_FROM_D; }
            set { m_SP_VALID_FROM_D = value; }
        }
        #endregion

        #region "SP_VALID_TO_D"
        public DateTime SP_VALID_TO_D
        {
            get { return m_SP_VALID_TO_D; }
            set { m_SP_VALID_TO_D = value; }
        }
        #endregion

        #region "SP_DED_AMOUNT"
        public decimal SP_DED_AMOUNT
        {
            get { return m_SP_DED_AMOUNT; }
            set { m_SP_DED_AMOUNT = value; }
        }
        #endregion

        #region "SP_DED_PER"
        public decimal? SP_DED_PER
        {
            get { return m_SP_DED_PER; }
            set { m_SP_DED_PER = value; }
        }
        #endregion

        #region "SP_ALL_IND"
        public String SP_ALL_IND
        {
            get { return m_SP_ALL_IND; }
            set { m_SP_ALL_IND = value; }
        }
        #endregion

        #region "SP_ACOUNT_NO_D"
        public String SP_ACOUNT_NO_D
        {
            get { return m_SP_ACOUNT_NO_D; }
            set { m_SP_ACOUNT_NO_D = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "PR_NAME"
        public String PR_NAME
        {
            get { return m_PR_NAME; }
            set { m_PR_NAME = value; }
        }
        #endregion

        #region "SP_ACC_DESC"
        public String SP_ACC_DESC
        {
            get { return m_SP_ACC_DESC; }
            set { m_SP_ACC_DESC = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_DED_CODE = "SP_DED_CODE";
        public static readonly string _SP_BRANCH_D = "SP_BRANCH_D";
        public static readonly string _SP_DED_DESC = "SP_DED_DESC";
        public static readonly string _SP_CATEGORY_D = "SP_CATEGORY_D";
        public static readonly string _SP_DESG_D = "SP_DESG_D";
        public static readonly string _SP_LEVEL_D = "SP_LEVEL_D";
        public static readonly string _SP_VALID_FROM_D = "SP_VALID_FROM_D";
        public static readonly string _SP_VALID_TO_D = "SP_VALID_TO_D";
        public static readonly string _SP_DED_AMOUNT = "SP_DED_AMOUNT";
        public static readonly string _SP_DED_PER = "SP_DED_PER";
        public static readonly string _SP_ALL_IND = "SP_ALL_IND";
        public static readonly string _SP_ACOUNT_NO_D = "SP_ACOUNT_NO_D";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------

       
    }
}

