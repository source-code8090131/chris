

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LeaveActualCommandGrid
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "LEAVE_ACTUAL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	01/12/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LeaveActualCommandGrid : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private double m_LS_P_NO;
      //  private double m_LEV_PNO;
        private String m_LEV_LV_TYPE;
        private DateTime m_LEV_START_DATE;
        private DateTime m_LEV_END_DATE;
        private String m_LEV_MED_CERTIF;
        private String m_LEV_APPROVED;
        private String m_LEV_REMARKS;
        private String m_LS_MANDATORY;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"


        #region "LS_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public double LS_P_NO
        {
            get { return m_LS_P_NO; }
            set { m_LS_P_NO = value; }
        }
        #endregion

        //#region "LEV_PNO"
        
        //public double LEV_PNO
        //{
        //    get { return m_LEV_PNO; }
        //    set { m_LEV_PNO = value; }
        //}
        //#endregion

        #region "LEV_LV_TYPE"
        public String LEV_LV_TYPE
        {
            get { return m_LEV_LV_TYPE; }
            set { m_LEV_LV_TYPE = value; }
        }
        #endregion

        #region "LEV_START_DATE"
        public DateTime LEV_START_DATE
        {
            get { return m_LEV_START_DATE; }
            set { m_LEV_START_DATE = value; }
        }
        #endregion

        #region "LEV_END_DATE"
        public DateTime LEV_END_DATE
        {
            get { return m_LEV_END_DATE; }
            set { m_LEV_END_DATE = value; }
        }
        #endregion

        #region "LEV_MED_CERTIF"
        public String LEV_MED_CERTIF
        {
            get { return m_LEV_MED_CERTIF; }
            set { m_LEV_MED_CERTIF = value; }
        }
        #endregion

        #region "LEV_APPROVED"
        public String LEV_APPROVED
        {
            get { return m_LEV_APPROVED; }
            set { m_LEV_APPROVED = value; }
        }
        #endregion

        #region "LEV_REMARKS"
        public String LEV_REMARKS
        {
            get { return m_LEV_REMARKS; }
            set { m_LEV_REMARKS = value; }
        }
        #endregion

        #region "LS_MANDATORY"
        public String LS_MANDATORY
        {
            get { return m_LS_MANDATORY; }
            set { m_LS_MANDATORY = value; }
        }
        #endregion
        


        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _LS_P_NO = "LEV_PNO";
       // public static readonly string _LEV_PNO = "LEV_PNO";
        public static readonly string _LEV_LV_TYPE = "LEV_LV_TYPE";
        public static readonly string _LEV_START_DATE = "LEV_START_DATE";
        public static readonly string _LEV_END_DATE = "LEV_END_DATE";
        public static readonly string _LEV_MED_CERTIF = "LEV_MED_CERTIF";
        public static readonly string _LEV_APPROVED = "LEV_APPROVED";
        public static readonly string _LEV_REMARKS = "LEV_REMARKS";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
