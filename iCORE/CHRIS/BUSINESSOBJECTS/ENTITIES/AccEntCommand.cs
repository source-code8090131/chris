

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AccEntCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "ACCOUNT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	02/01/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class AccEntCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_SP_ACC_NO;
        private String m_SP_ACC_DESC;
        private String m_SP_ACC_DESC1;
        private String m_SP_ACC_TYPE;
        private String m_SP_ACC_GROUP;
        private decimal m_SP_ACC_AMOUNT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "SP_ACC_NO"
        public String SP_ACC_NO
        {
            get { return m_SP_ACC_NO; }
            set { m_SP_ACC_NO = value; }
        }
        #endregion

        #region "SP_ACC_DESC"
        public String SP_ACC_DESC
        {
            get { return m_SP_ACC_DESC; }
            set { m_SP_ACC_DESC = value; }
        }
        #endregion

        #region "SP_ACC_DESC1"
        public String SP_ACC_DESC1
        {
            get { return m_SP_ACC_DESC1; }
            set { m_SP_ACC_DESC1 = value; }
        }
        #endregion

        #region "SP_ACC_TYPE"
        public String SP_ACC_TYPE
        {
            get { return m_SP_ACC_TYPE; }
            set { m_SP_ACC_TYPE = value; }
        }
        #endregion

        #region "SP_ACC_GROUP"
        public String SP_ACC_GROUP
        {
            get { return m_SP_ACC_GROUP; }
            set { m_SP_ACC_GROUP = value; }
        }
        #endregion

        #region "SP_ACC_AMOUNT"
        public decimal SP_ACC_AMOUNT
        {
            get { return m_SP_ACC_AMOUNT; }
            set { m_SP_ACC_AMOUNT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _SP_ACC_NO = "SP_ACC_NO";
        public static readonly string _SP_ACC_DESC = "SP_ACC_DESC";
        public static readonly string _SP_ACC_DESC1 = "SP_ACC_DESC1";
        public static readonly string _SP_ACC_TYPE = "SP_ACC_TYPE";
        public static readonly string _SP_ACC_GROUP = "SP_ACC_GROUP";
        public static readonly string _SP_ACC_AMOUNT = "SP_ACC_AMOUNT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}