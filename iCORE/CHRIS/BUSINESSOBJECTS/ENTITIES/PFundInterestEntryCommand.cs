

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFundInterestEntryCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PFUND_INTEREST"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[AHAD ZUBAIR]	03/10/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PFundInterestEntryCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
      
        private String m_PFI_YEAR;
        private decimal m_PFI_RATE;
        private decimal m_PFI_RES_RATE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PFI_YEAR"
        public string PFI_YEAR
        {
            get { return m_PFI_YEAR; }
            set { m_PFI_YEAR = value; }
        }

        #endregion

        #region "PFI_RATE"
        public decimal PFI_RATE
        {
            get { return m_PFI_RATE; }
            set { m_PFI_RATE = value; }
        }
        #endregion

        #region "PFI_RES_RATE"
        public decimal PFI_RES_RATE
        {
            get { return m_PFI_RES_RATE; }
            set { m_PFI_RES_RATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PFI_YEAR = "PFI_YEAR";
        public static readonly string _PFI_RATE = "PFI_RATE";
        public static readonly string _PFI_RES_RATE = "PFI_RES_RATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
