

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SummerInternsCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "SUMMER_INTERNS"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	01/13/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class SummerInternsCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_I_NO;
        private String m_PR_BRANCH;
        private String m_PR_FIRST_NAME;
        private String m_PR_LAST_NAME;
        private String m_PR_UNIVERSITY;
        private String m_PR_CLASS;
        private DateTime m_PR_CON_FROM;
        private DateTime m_PR_CON_TO;
        private String m_PR_ADDRESS;
        private String m_PR_PHONE1;
        private String m_PR_PHONE2;
        private DateTime m_PR_DATE_BIRTH;
        private String m_PR_MAR_STATUS;
        private String m_PR_SEX;
        private String m_PR_NID_CARD;
        private Nullable<decimal> m_PR_STIPENED;
        private Nullable<decimal> m_PR_INTERNSHIP;
        private String m_PR_RECOM;
        private String m_PR_FLAG;
        private String m_PR_UserID;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_I_NO"
        public Double PR_P_NO
        {
            get { return m_PR_I_NO; }
            set { m_PR_I_NO = value; }
        }
        #endregion

        #region "PR_BRANCH"
        public String PR_BRANCH
        {
            get { return m_PR_BRANCH; }
            set { m_PR_BRANCH = value; }
        }
        #endregion

        #region "PR_FIRST_NAME"
        public String PR_FIRST_NAME
        {
            get { return m_PR_FIRST_NAME; }
            set { m_PR_FIRST_NAME = value; }
        }
        #endregion

        #region "PR_LAST_NAME"
        public String PR_LAST_NAME
        {
            get { return m_PR_LAST_NAME; }
            set { m_PR_LAST_NAME = value; }
        }
        #endregion

        #region "PR_UNIVERSITY"
        public String PR_UNIVERSITY
        {
            get { return m_PR_UNIVERSITY; }
            set { m_PR_UNIVERSITY = value; }
        }
        #endregion

        #region "PR_CLASS"
        public String PR_CLASS
        {
            get { return m_PR_CLASS; }
            set { m_PR_CLASS = value; }
        }
        #endregion

        #region "PR_CON_FROM"
        public DateTime PR_CON_FROM
        {
            get { return m_PR_CON_FROM; }
            set { m_PR_CON_FROM = value; }
        }
        #endregion

        #region "PR_CON_TO"
        public DateTime PR_CON_TO
        {
            get { return m_PR_CON_TO; }
            set { m_PR_CON_TO = value; }
        }
        #endregion

        #region "PR_ADDRESS"
        public String PR_ADDRESS
        {
            get { return m_PR_ADDRESS; }
            set { m_PR_ADDRESS = value; }
        }
        #endregion

        #region "PR_PHONE1"
        public String PR_PHONE1
        {
            get { return m_PR_PHONE1; }
            set { m_PR_PHONE1 = value; }
        }
        #endregion

        #region "PR_PHONE2"
        public String PR_PHONE2
        {
            get { return m_PR_PHONE2; }
            set { m_PR_PHONE2 = value; }
        }
        #endregion

        #region "PR_DATE_BIRTH"
        public DateTime PR_DATE_BIRTH
        {
            get { return m_PR_DATE_BIRTH; }
            set { m_PR_DATE_BIRTH = value; }
        }
        #endregion

        #region "PR_MAR_STATUS"
        public String PR_MAR_STATUS
        {
            get { return m_PR_MAR_STATUS; }
            set { m_PR_MAR_STATUS = value; }
        }
        #endregion

        #region "PR_SEX"
        public String PR_SEX
        {
            get { return m_PR_SEX; }
            set { m_PR_SEX = value; }
        }
        #endregion

        #region "PR_NID_CARD"
        public String PR_NID_CARD
        {
            get { return m_PR_NID_CARD; }
            set { m_PR_NID_CARD = value; }
        }
        #endregion

        #region "PR_STIPENED"
        public Nullable<decimal> PR_STIPENED
        {
            get { return m_PR_STIPENED; }
            set { m_PR_STIPENED = value; }
        }
        #endregion

        #region "PR_INTERNSHIP"
        public Nullable<decimal> PR_INTERNSHIP
        {
            get { return m_PR_INTERNSHIP; }
            set { m_PR_INTERNSHIP = value; }
        }
        #endregion

        #region "PR_RECOM"
        public String PR_RECOM
        {
            get { return m_PR_RECOM; }
            set { m_PR_RECOM = value; }
        }
        #endregion

        #region "PR_FLAG"
        public String PR_FLAG
        {
            get { return m_PR_FLAG; }
            set { m_PR_FLAG = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "PR_UserID"
        public String PR_UserID
        {
            get { return m_PR_UserID; }
            set { m_PR_UserID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_I_NO = "PR_I_NO";
        public static readonly string _PR_BRANCH = "PR_BRANCH";
        public static readonly string _PR_FIRST_NAME = "PR_FIRST_NAME";
        public static readonly string _PR_LAST_NAME = "PR_LAST_NAME";
        public static readonly string _PR_UNIVERSITY = "PR_UNIVERSITY";
        public static readonly string _PR_CLASS = "PR_CLASS";
        public static readonly string _PR_CON_FROM = "PR_CON_FROM";
        public static readonly string _PR_CON_TO = "PR_CON_TO";
        public static readonly string _PR_ADDRESS = "PR_ADDRESS";
        public static readonly string _PR_PHONE1 = "PR_PHONE1";
        public static readonly string _PR_PHONE2 = "PR_PHONE2";
        public static readonly string _PR_DATE_BIRTH = "PR_DATE_BIRTH";
        public static readonly string _PR_MAR_STATUS = "PR_MAR_STATUS";
        public static readonly string _PR_SEX = "PR_SEX";
        public static readonly string _PR_NID_CARD = "PR_NID_CARD";
        public static readonly string _PR_STIPENED = "PR_STIPENED";
        public static readonly string _PR_INTERNSHIP = "PR_INTERNSHIP";
        public static readonly string _PR_RECOM = "PR_RECOM";
        public static readonly string _PR_FLAG = "PR_FLAG";
        public static readonly string _ID = "ID";
        public static readonly string _PR_UserID = "PR_UserID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
