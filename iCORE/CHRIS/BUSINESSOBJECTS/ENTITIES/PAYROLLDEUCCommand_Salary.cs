

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLDEUCCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PAYROLL_DEDUC"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Ahad Zubair]	01/06/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PAYROLLDEUCCommand_Salary : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private DateTime m_PA_DATE;
        private String m_PD_DED_CODE;
        private decimal m_PD_DED_AMOUNT;
        private String m_PD_ACCOUNT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PD_PAY_DATE"
        [CustomAttributes(IsForeignKey = true)]
        public DateTime PA_DATE
        {
            get { return m_PA_DATE; }
            set { m_PA_DATE = value; }
        }
        #endregion

        #region "PD_DED_CODE"
        public String PD_DED_CODE
        {
            get { return m_PD_DED_CODE; }
            set { m_PD_DED_CODE = value; }
        }
        #endregion

        #region "PD_DED_AMOUNT"
        public decimal PD_DED_AMOUNT
        {
            get { return m_PD_DED_AMOUNT; }
            set { m_PD_DED_AMOUNT = value; }
        }
        #endregion

        #region "PD_ACCOUNT"
        public String PD_ACCOUNT
        {
            get { return m_PD_ACCOUNT; }
            set { m_PD_ACCOUNT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _PA_DATE = "PA_DATE";
        public static readonly string _PD_DED_CODE = "PD_DED_CODE";
        public static readonly string _PD_DED_AMOUNT = "PD_DED_AMOUNT";
        public static readonly string _PD_ACCOUNT = "PD_ACCOUNT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
