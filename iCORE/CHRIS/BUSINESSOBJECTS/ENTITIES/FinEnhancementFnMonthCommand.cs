
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.FinEnhancementFnMonthCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FN_Month"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/28/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FinEnhancementFnMonthCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_FN_M_BRANCH;
        private String m_FN_TYPE;
        private String m_FN_FIN_NO;
        private DateTime m_FN_MDATE;
        private decimal m_FN_DEBIT;
        private decimal m_FN_CREDIT;
        private decimal m_FN_PAY_LEFT;
        private decimal m_FN_BALANCE;
        private decimal m_FN_LOAN_BALANCE;
        private decimal m_FN_MARKUP;
        private String m_FN_LIQ_FLAG;
        private String m_FN_SUBTYPE;
        private int m_ID;
        private Double m_FN_C_RATIO_PER;
        private Double m_TOT_MONTHLY_INSTALL;
        private Nullable<DateTime> m_ExtraTime;
        private Nullable<DateTime> m_Start_Date;
        private Nullable<DateTime> m_End_Date;
        private Nullable<DateTime> m_Pay_Gen_Date;
        private Nullable<DateTime> m_Pay_Gen;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "FN_M_BRANCH"
        public String FN_M_BRANCH
        {
            get { return m_FN_M_BRANCH; }
            set { m_FN_M_BRANCH = value; }
        }
        #endregion

        #region "FN_TYPE"
        public String FN_TYPE
        {
            get { return m_FN_TYPE; }
            set { m_FN_TYPE = value; }
        }
        #endregion

        #region "FN_FIN_NO"
        public String FN_FIN_NO
        {
            get { return m_FN_FIN_NO; }
            set { m_FN_FIN_NO = value; }
        }
        #endregion

        #region "FN_MDATE"
        public DateTime FN_MDATE
        {
            get { return m_FN_MDATE; }
            set { m_FN_MDATE = value; }
        }
        #endregion

        #region "FN_DEBIT"
        public decimal FN_DEBIT
        {
            get { return m_FN_DEBIT; }
            set { m_FN_DEBIT = value; }
        }
        #endregion

        #region "FN_CREDIT"
        public decimal FN_CREDIT
        {
            get { return m_FN_CREDIT; }
            set { m_FN_CREDIT = value; }
        }
        #endregion

        #region "FN_PAY_LEFT"
        public decimal FN_PAY_LEFT
        {
            get { return m_FN_PAY_LEFT; }
            set { m_FN_PAY_LEFT = value; }
        }
        #endregion

        #region "FN_BALANCE"
        public decimal FN_BALANCE
        {
            get { return m_FN_BALANCE; }
            set { m_FN_BALANCE = value; }
        }
        #endregion

        #region "FN_LOAN_BALANCE"
        public decimal FN_LOAN_BALANCE
        {
            get { return m_FN_LOAN_BALANCE; }
            set { m_FN_LOAN_BALANCE = value; }
        }
        #endregion

        #region "FN_MARKUP"
        public decimal FN_MARKUP
        {
            get { return m_FN_MARKUP; }
            set { m_FN_MARKUP = value; }
        }
        #endregion

        #region "FN_LIQ_FLAG"
        public String FN_LIQ_FLAG
        {
            get { return m_FN_LIQ_FLAG; }
            set { m_FN_LIQ_FLAG = value; }
        }
        #endregion

        #region "FN_SUBTYPE"
        public String FN_SUBTYPE
        {
            get { return m_FN_SUBTYPE; }
            set { m_FN_SUBTYPE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "FN_C_RATIO_PER"
        public Double FN_C_RATIO_PER
        {
            get { return m_FN_C_RATIO_PER; }
            set { m_FN_C_RATIO_PER = value; }
        }
        #endregion


        #region "TOT_MONTHLY_INSTALL"
        public Double TOT_MONTHLY_INSTALL
        {
            get { return m_TOT_MONTHLY_INSTALL; }
            set { m_TOT_MONTHLY_INSTALL = value; }
        }
        #endregion


        #region "ExtraTime"
        public Nullable<DateTime> ExtraTime
        {
            get { return m_ExtraTime; }
            set { m_ExtraTime = value; }
        }
        #endregion

        #region "Start_Date"
        public Nullable<DateTime> Start_Date
        {
            get { return m_Start_Date; }
            set { m_Start_Date = value; }
        }
        #endregion


        #region "End_Date"
        public Nullable<DateTime> End_Date
        {
            get { return m_End_Date; }
            set { m_End_Date = value; }
        }
        #endregion


        #region "Pay_Gen_Date"
        public Nullable<DateTime> Pay_Gen_Date
        {
            get { return m_Pay_Gen_Date; }
            set { m_Pay_Gen_Date = value; }
        }
        #endregion

        #region "Pay_Gen"
        public Nullable<DateTime> Pay_Gen
        {
            get { return m_Pay_Gen; }
            set { m_Pay_Gen = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _FN_M_BRANCH = "FN_M_BRANCH";
        public static readonly string _FN_TYPE = "FN_TYPE";
        public static readonly string _FN_FIN_NO = "FN_FIN_NO";
        public static readonly string _FN_MDATE = "FN_MDATE";
        public static readonly string _FN_DEBIT = "FN_DEBIT";
        public static readonly string _FN_CREDIT = "FN_CREDIT";
        public static readonly string _FN_PAY_LEFT = "FN_PAY_LEFT";
        public static readonly string _FN_BALANCE = "FN_BALANCE";
        public static readonly string _FN_LOAN_BALANCE = "FN_LOAN_BALANCE";
        public static readonly string _FN_MARKUP = "FN_MARKUP";
        public static readonly string _FN_LIQ_FLAG = "FN_LIQ_FLAG";
        public static readonly string _FN_SUBTYPE = "FN_SUBTYPE";
        public static readonly string _ID = "ID";
        public static readonly string _FN_C_RATIO_PER = "FN_C_RATIO_PER";
        public static readonly string _TOT_MONTHLY_INSTALL = "TOT_MONTHLY_INSTALL";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
