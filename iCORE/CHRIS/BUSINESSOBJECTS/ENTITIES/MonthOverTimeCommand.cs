/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MonthOverTimeCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "MONTH_OVERTIME"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[FrameWork]	02/02/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class MonthOverTimeCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private decimal m_MO_YEAR;
        private decimal m_MO_MONTH;
        private String m_MO_SEGMENT;
        private String m_MO_DEPT;
        private decimal? m_MO_SINGLE_OT;
        private decimal? m_MO_DOUBLE_OT;
        private decimal? m_MO_SINGLE_COST;
        private decimal? m_MO_DOUBLE_COST;
        private decimal? m_MO_MEAL_COST;
        private decimal? m_MO_CONV_COST;
        private DateTime m_MO_DATE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "MO_YEAR"
        public decimal MO_YEAR
        {
            get { return m_MO_YEAR; }
            set { m_MO_YEAR = value; }
        }
        #endregion

        #region "MO_MONTH"
        public decimal MO_MONTH
        {
            get { return m_MO_MONTH; }
            set { m_MO_MONTH = value; }
        }
        #endregion

        #region "MO_SEGMENT"
        public String MO_SEGMENT
        {
            get { return m_MO_SEGMENT; }
            set { m_MO_SEGMENT = value; }
        }
        #endregion

        #region "MO_DEPT"
        public String MO_DEPT
        {
            get { return m_MO_DEPT; }
            set { m_MO_DEPT = value; }
        }
        #endregion

        #region "MO_SINGLE_OT"
        public decimal? MO_SINGLE_OT
        {
            get { return m_MO_SINGLE_OT; }
            set { m_MO_SINGLE_OT = value; }
        }
        #endregion

        #region "MO_DOUBLE_OT"
        public decimal? MO_DOUBLE_OT
        {
            get { return m_MO_DOUBLE_OT; }
            set { m_MO_DOUBLE_OT = value; }
        }
        #endregion

        #region "MO_SINGLE_COST"
        public decimal? MO_SINGLE_COST
        {
            get { return m_MO_SINGLE_COST; }
            set { m_MO_SINGLE_COST = value; }
        }
        #endregion

        #region "MO_DOUBLE_COST"
        public decimal? MO_DOUBLE_COST
        {
            get { return m_MO_DOUBLE_COST; }
            set { m_MO_DOUBLE_COST = value; }
        }
        #endregion

        #region "MO_MEAL_COST"
        public decimal? MO_MEAL_COST
        {
            get { return m_MO_MEAL_COST; }
            set { m_MO_MEAL_COST = value; }
        }
        #endregion

        #region "MO_CONV_COST"
        public decimal? MO_CONV_COST
        {
            get { return m_MO_CONV_COST; }
            set { m_MO_CONV_COST = value; }
        }
        #endregion

        #region "MO_DATE"
        public DateTime MO_DATE
        {
            get { return m_MO_DATE; }
            set { m_MO_DATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _MO_YEAR = "MO_YEAR";
        public static readonly string _MO_MONTH = "MO_MONTH";
        public static readonly string _MO_SEGMENT = "MO_SEGMENT";
        public static readonly string _MO_DEPT = "MO_DEPT";
        public static readonly string _MO_SINGLE_OT = "MO_SINGLE_OT";
        public static readonly string _MO_DOUBLE_OT = "MO_DOUBLE_OT";
        public static readonly string _MO_SINGLE_COST = "MO_SINGLE_COST";
        public static readonly string _MO_DOUBLE_COST = "MO_DOUBLE_COST";
        public static readonly string _MO_MEAL_COST = "MO_MEAL_COST";
        public static readonly string _MO_CONV_COST = "MO_CONV_COST";
        public static readonly string _MO_DATE = "MO_DATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
