

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LFA_TABCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "LFA_TAB"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Anila]	01/20/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class LFA_TABCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_LF_P_NO;
        private DateTime m_LF_DATE;
        private decimal m_LF_LEAV_BAL;
        private decimal? m_LF_LFA_AMOUNT;
        private String m_LF_APPROVED;
        private String m_LF_PRINT_REP;
        private int m_ID;

        private Double m_PR_P_NO;
        private String m_PR_NAME;
        private String m_PR_BRANCH;
        private String m_PR_CATEGORY;

        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "LF_P_NO"
        public decimal LF_P_NO
        {
            get { return m_LF_P_NO; }
            set { m_LF_P_NO = value; }
        }
        #endregion

        #region "LF_DATE"
        public DateTime LF_DATE
        {
            get { return m_LF_DATE; }
            set { m_LF_DATE = value; }
        }
        #endregion

        #region "LF_LEAV_BAL"
        public decimal LF_LEAV_BAL
        {
            get { return m_LF_LEAV_BAL; }
            set { m_LF_LEAV_BAL = value; }
        }
        #endregion


       

        #region "LF_LFA_AMOUNT"
        public decimal? LF_LFA_AMOUNT
        {
            get { return m_LF_LFA_AMOUNT; }
            set { m_LF_LFA_AMOUNT = value; }
        }
        #endregion

        #region "LF_APPROVED"
        public String LF_APPROVED
        {
            get { return m_LF_APPROVED; }
            set { m_LF_APPROVED = value; }
        }
        #endregion

        #region "LF_PRINT_REP"
        public String LF_PRINT_REP
        {
            get { return m_LF_PRINT_REP; }
            set { m_LF_PRINT_REP = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        //for personnel table fields

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion      

        #region "PR_NAME"
        public String PR_NAME
        {
            get { return m_PR_NAME; }
            set { m_PR_NAME = value; }
        }
        #endregion

        #region "PR_BRANCH"
        public String PR_BRANCH
        {
            get { return m_PR_BRANCH; }
            set { m_PR_BRANCH = value; }
        }
        #endregion

        #region "PR_CATEGORY"
        public String PR_CATEGORY
        {
            get { return m_PR_CATEGORY; }
            set { m_PR_CATEGORY = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _LF_P_NO = "LF_P_NO";
        public static readonly string _LF_DATE = "LF_DATE";
        public static readonly string _LF_LEAV_BAL = "LF_LEAV_BAL";
        public static readonly string _LF_LEAV_BAL11 = "LF_LEAV_BAL11";
        public static readonly string _LF_LFA_AMOUNT = "LF_LFA_AMOUNT";
        public static readonly string _LF_APPROVED = "LF_APPROVED";
        public static readonly string _LF_PRINT_REP = "LF_PRINT_REP";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}

