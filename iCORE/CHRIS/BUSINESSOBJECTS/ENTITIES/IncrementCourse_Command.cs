

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementCourse_Command
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "CTT_COURSE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	03/29/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class IncrementCourse_Command : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Nullable<decimal> m_PR_IN_NO;
        private Nullable<DateTime> m_PR_EFFECTIVE;
        private decimal m_CTT_YEAR;
        private String m_CTT_TITLE;
        private String m_CTT_INSTITUTE;
        private String m_CTT_GRADE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "CTT_PNO"
        [CustomAttributes(IsForeignKey = true)]
        public Nullable<decimal> PR_IN_NO
        {
            get { return m_PR_IN_NO; }
            set { m_PR_IN_NO = value; }
        }
        #endregion

        #region "PR_EFFECTIVE"
        [CustomAttributes(IsForeignKey = true)]
        public Nullable<DateTime> PR_EFFECTIVE
        {
            get { return m_PR_EFFECTIVE; }
            set { m_PR_EFFECTIVE = value; }
        }
        #endregion

        #region "CTT_YEAR"
        public decimal CTT_YEAR
        {
            get { return m_CTT_YEAR; }
            set { m_CTT_YEAR = value; }
        }
        #endregion

        #region "CTT_TITLE"
        public String CTT_TITLE
        {
            get { return m_CTT_TITLE; }
            set { m_CTT_TITLE = value; }
        }
        #endregion

        #region "CTT_INSTITUTE"
        public String CTT_INSTITUTE
        {
            get { return m_CTT_INSTITUTE; }
            set { m_CTT_INSTITUTE = value; }
        }
        #endregion

        #region "CTT_GRADE"
        public String CTT_GRADE
        {
            get { return m_CTT_GRADE; }
            set { m_CTT_GRADE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _CTT_PNO = "CTT_PNO";
        public static readonly string _CTT_DATE = "CTT_DATE";
        public static readonly string _CTT_YEAR = "CTT_YEAR";
        public static readonly string _CTT_TITLE = "CTT_TITLE";
        public static readonly string _CTT_INSTITUTE = "CTT_INSTITUTE";
        public static readonly string _CTT_GRADE = "CTT_GRADE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
