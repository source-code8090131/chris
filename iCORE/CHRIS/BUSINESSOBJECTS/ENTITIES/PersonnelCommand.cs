

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PERSONNEL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	11/29/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{         
    public class PersonnelCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_PR_BRANCH;
        private String m_PR_FIRST_NAME;
        private String m_PR_LAST_NAME;
        private String m_PR_DESIG;
        private String m_PR_LEVEL;
        private String m_PR_CATEGORY;
        private String m_PR_FUNC_TITTLE1;
        private String m_PR_FUNC_TITTLE2;
        private DateTime m_PR_JOINING_DATE;
        private DateTime m_PR_CONFIRM;
        private Double m_PR_ANNUAL_PACK;
        private String m_PR_NATIONAL_TAX;
        private String m_PR_ACCOUNT_NO;
        private String m_PR_BANK_ID;
        private DateTime m_PR_ID_ISSUE;
        private DateTime m_PR_EXPIRY;
        private decimal m_PR_TAX_INC;
        private decimal m_PR_TAX_PAID;
        private String m_PR_TAX_USED;
        private String m_PR_CLOSE_FLAG;
        private DateTime m_PR_CONFIRM_ON;
        private DateTime m_PR_EXPECTED;
        private String m_PR_TERMIN_TYPE;
        private DateTime m_PR_TERMIN_DATE;
        private String m_PR_REASONS;
        private String m_PR_RESIG_RECV;
        private String m_PR_APP_RESIG;
        private String m_PR_EXIT_INTER;
        private String m_PR_ID_RETURN;
        private String m_PR_NOTICE;
        private String m_PR_ARTONY;
        private String m_PR_DELETION;
        private String m_PR_SETTLE;
        private String m_PR_CONF_FLAG;
        private DateTime m_PR_LAST_INCREMENT;
        private DateTime m_PR_NEXT_INCREMENT;
        private DateTime m_PR_TRANSFER_DATE;
        private DateTime m_PR_PROMOTION_DATE;
        private String m_PR_NEW_BRANCH;
        private Double m_PR_NEW_ANNUAL_PACK;
        private decimal m_PR_TRANSFER;
        private decimal m_PR_MONTH_AWARD;
        private String m_PR_RELIGION;
        private decimal m_PR_ZAKAT_AMT;
        private String m_PR_MED_FLAG;
        private String m_PR_ATT_FLAG;
        private String m_PR_PAY_FLAG;
        private decimal m_PR_REP_NO;
        private decimal m_PR_REFUND_AMT;
        private String m_PR_REFUND_FOR;
        private String m_PR_NTN_CERT_REC;
        private String m_PR_NTN_CARD_REC;
        private String m_PR_EMP_TYPE;
        private String m_LOCATION;
        private String m_PR_UserID;
        //private String m_PR_SEGMENT;
        //private String m_PR_DEPT;
        //private String m_PR_CONTRIBUTION;
        //private String m_PR_COSTCODE;
        //private String m_PR_SOEID;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PR_BRANCH"
        public String PR_BRANCH
        {
            get { return m_PR_BRANCH; }
            set { m_PR_BRANCH = value; }
        }
        #endregion

        #region "PR_FIRST_NAME"
        public String PR_FIRST_NAME
        {
            get { return m_PR_FIRST_NAME; }
            set { m_PR_FIRST_NAME = value; }
        }
        #endregion

        #region "PR_LAST_NAME"
        public String PR_LAST_NAME
        {
            get { return m_PR_LAST_NAME; }
            set { m_PR_LAST_NAME = value; }
        }
        #endregion

        #region "PR_DESIG"
        public String PR_DESIG
        {
            get { return m_PR_DESIG; }
            set { m_PR_DESIG = value; }
        }
        #endregion

        #region "PR_LEVEL"
        public String PR_LEVEL
        {
            get { return m_PR_LEVEL; }
            set { m_PR_LEVEL = value; }
        }
        #endregion

        #region "PR_CATEGORY"
        public String PR_CATEGORY
        {
            get { return m_PR_CATEGORY; }
            set { m_PR_CATEGORY = value; }
        }
        #endregion

        #region "PR_FUNC_TITTLE1"
        public String PR_FUNC_TITTLE1
        {
            get { return m_PR_FUNC_TITTLE1; }
            set { m_PR_FUNC_TITTLE1 = value; }
        }
        #endregion

        #region "PR_FUNC_TITTLE2"
        public String PR_FUNC_TITTLE2
        {
            get { return m_PR_FUNC_TITTLE2; }
            set { m_PR_FUNC_TITTLE2 = value; }
        }
        #endregion

        #region "PR_JOINING_DATE"
        public DateTime PR_JOINING_DATE
        {
            get { return m_PR_JOINING_DATE; }
            set { m_PR_JOINING_DATE = value; }
        }
        #endregion

        #region "PR_CONFIRM"
        public DateTime PR_CONFIRM
        {
            get { return m_PR_CONFIRM; }
            set { m_PR_CONFIRM = value; }
        }
        #endregion

        #region "PR_ANNUAL_PACK"
        public Double PR_ANNUAL_PACK
        {
            get { return m_PR_ANNUAL_PACK; }
            set { m_PR_ANNUAL_PACK = value; }
        }
        #endregion

        #region "PR_NATIONAL_TAX"
        public String PR_NATIONAL_TAX
        {
            get { return m_PR_NATIONAL_TAX; }
            set { m_PR_NATIONAL_TAX = value; }
        }
        #endregion

        #region "PR_ACCOUNT_NO"
        public String PR_ACCOUNT_NO
        {
            get { return m_PR_ACCOUNT_NO; }
            set { m_PR_ACCOUNT_NO = value; }
        }
        #endregion

        #region "PR_BANK_ID"
        public String PR_BANK_ID
        {
            get { return m_PR_BANK_ID; }
            set { m_PR_BANK_ID = value; }
        }
        #endregion

        #region "PR_ID_ISSUE"
        public DateTime PR_ID_ISSUE
        {
            get { return m_PR_ID_ISSUE; }
            set { m_PR_ID_ISSUE = value; }
        }
        #endregion

        #region "PR_EXPIRY"
        public DateTime PR_EXPIRY
        {
            get { return m_PR_EXPIRY; }
            set { m_PR_EXPIRY = value; }
        }
        #endregion

        #region "PR_TAX_INC"
        public decimal PR_TAX_INC
        {
            get { return m_PR_TAX_INC; }
            set { m_PR_TAX_INC = value; }
        }
        #endregion

        #region "PR_TAX_PAID"
        public decimal PR_TAX_PAID
        {
            get { return m_PR_TAX_PAID; }
            set { m_PR_TAX_PAID = value; }
        }
        #endregion

        #region "PR_TAX_USED"
        public String PR_TAX_USED
        {
            get { return m_PR_TAX_USED; }
            set { m_PR_TAX_USED = value; }
        }
        #endregion

        #region "PR_CLOSE_FLAG"
        public String PR_CLOSE_FLAG
        {
            get { return m_PR_CLOSE_FLAG; }
            set { m_PR_CLOSE_FLAG = value; }
        }
        #endregion

        #region "PR_CONFIRM_ON"
        public DateTime PR_CONFIRM_ON
        {
            get { return m_PR_CONFIRM_ON; }
            set { m_PR_CONFIRM_ON = value; }
        }
        #endregion

        #region "PR_EXPECTED"
        public DateTime PR_EXPECTED
        {
            get { return m_PR_EXPECTED; }
            set { m_PR_EXPECTED = value; }
        }
        #endregion

        #region "PR_TERMIN_TYPE"
        public String PR_TERMIN_TYPE
        {
            get { return m_PR_TERMIN_TYPE; }
            set { m_PR_TERMIN_TYPE = value; }
        }
        #endregion

        #region "PR_TERMIN_DATE"
        public DateTime PR_TERMIN_DATE
        {
            get { return m_PR_TERMIN_DATE; }
            set { m_PR_TERMIN_DATE = value; }
        }
        #endregion

        #region "PR_REASONS"
        public String PR_REASONS
        {
            get { return m_PR_REASONS; }
            set { m_PR_REASONS = value; }
        }
        #endregion

        #region "PR_RESIG_RECV"
        public String PR_RESIG_RECV
        {
            get { return m_PR_RESIG_RECV; }
            set { m_PR_RESIG_RECV = value; }
        }
        #endregion

        #region "PR_APP_RESIG"
        public String PR_APP_RESIG
        {
            get { return m_PR_APP_RESIG; }
            set { m_PR_APP_RESIG = value; }
        }
        #endregion

        #region "PR_EXIT_INTER"
        public String PR_EXIT_INTER
        {
            get { return m_PR_EXIT_INTER; }
            set { m_PR_EXIT_INTER = value; }
        }
        #endregion

        #region "PR_ID_RETURN"
        public String PR_ID_RETURN
        {
            get { return m_PR_ID_RETURN; }
            set { m_PR_ID_RETURN = value; }
        }
        #endregion

        #region "PR_NOTICE"
        public String PR_NOTICE
        {
            get { return m_PR_NOTICE; }
            set { m_PR_NOTICE = value; }
        }
        #endregion

        #region "PR_ARTONY"
        public String PR_ARTONY
        {
            get { return m_PR_ARTONY; }
            set { m_PR_ARTONY = value; }
        }
        #endregion

        #region "PR_DELETION"
        public String PR_DELETION
        {
            get { return m_PR_DELETION; }
            set { m_PR_DELETION = value; }
        }
        #endregion

        #region "PR_SETTLE"
        public String PR_SETTLE
        {
            get { return m_PR_SETTLE; }
            set { m_PR_SETTLE = value; }
        }
        #endregion

        #region "PR_CONF_FLAG"
        public String PR_CONF_FLAG
        {
            get { return m_PR_CONF_FLAG; }
            set { m_PR_CONF_FLAG = value; }
        }
        #endregion

        #region "PR_LAST_INCREMENT"
        public DateTime PR_LAST_INCREMENT
        {
            get { return m_PR_LAST_INCREMENT; }
            set { m_PR_LAST_INCREMENT = value; }
        }
        #endregion

        #region "PR_NEXT_INCREMENT"
        public DateTime PR_NEXT_INCREMENT
        {
            get { return m_PR_NEXT_INCREMENT; }
            set { m_PR_NEXT_INCREMENT = value; }
        }
        #endregion

        #region "PR_TRANSFER_DATE"
        public DateTime PR_TRANSFER_DATE
        {
            get { return m_PR_TRANSFER_DATE; }
            set { m_PR_TRANSFER_DATE = value; }
        }
        #endregion

        #region "PR_PROMOTION_DATE"
        public DateTime PR_PROMOTION_DATE
        {
            get { return m_PR_PROMOTION_DATE; }
            set { m_PR_PROMOTION_DATE = value; }
        }
        #endregion

        #region "PR_NEW_BRANCH"
        public String PR_NEW_BRANCH
        {
            get { return m_PR_NEW_BRANCH; }
            set { m_PR_NEW_BRANCH = value; }
        }
        #endregion

        #region "PR_NEW_ANNUAL_PACK"
        public Double PR_NEW_ANNUAL_PACK
        {
            get { return m_PR_NEW_ANNUAL_PACK; }
            set { m_PR_NEW_ANNUAL_PACK = value; }
        }
        #endregion

        #region "PR_TRANSFER"
        public decimal PR_TRANSFER
        {
            get { return m_PR_TRANSFER; }
            set { m_PR_TRANSFER = value; }
        }
        #endregion

        #region "PR_MONTH_AWARD"
        public decimal PR_MONTH_AWARD
        {
            get { return m_PR_MONTH_AWARD; }
            set { m_PR_MONTH_AWARD = value; }
        }
        #endregion

        #region "PR_RELIGION"
        public String PR_RELIGION
        {
            get { return m_PR_RELIGION; }
            set { m_PR_RELIGION = value; }
        }
        #endregion

        #region "PR_ZAKAT_AMT"
        public decimal PR_ZAKAT_AMT
        {
            get { return m_PR_ZAKAT_AMT; }
            set { m_PR_ZAKAT_AMT = value; }
        }
        #endregion

        #region "PR_MED_FLAG"
        public String PR_MED_FLAG
        {
            get { return m_PR_MED_FLAG; }
            set { m_PR_MED_FLAG = value; }
        }
        #endregion

        #region "PR_ATT_FLAG"
        public String PR_ATT_FLAG
        {
            get { return m_PR_ATT_FLAG; }
            set { m_PR_ATT_FLAG = value; }
        }
        #endregion

        #region "PR_PAY_FLAG"
        public String PR_PAY_FLAG
        {
            get { return m_PR_PAY_FLAG; }
            set { m_PR_PAY_FLAG = value; }
        }
        #endregion

        #region "PR_REP_NO"
        public decimal PR_REP_NO
        {
            get { return m_PR_REP_NO; }
            set { m_PR_REP_NO = value; }
        }
        #endregion

        #region "PR_REFUND_AMT"
        public decimal PR_REFUND_AMT
        {
            get { return m_PR_REFUND_AMT; }
            set { m_PR_REFUND_AMT = value; }
        }
        #endregion

        #region "PR_REFUND_FOR"
        public String PR_REFUND_FOR
        {
            get { return m_PR_REFUND_FOR; }
            set { m_PR_REFUND_FOR = value; }
        }
        #endregion

        #region "PR_NTN_CERT_REC"
        public String PR_NTN_CERT_REC
        {
            get { return m_PR_NTN_CERT_REC; }
            set { m_PR_NTN_CERT_REC = value; }
        }
        #endregion

        #region "PR_NTN_CARD_REC"
        public String PR_NTN_CARD_REC
        {
            get { return m_PR_NTN_CARD_REC; }
            set { m_PR_NTN_CARD_REC = value; }
        }
        #endregion

        #region "PR_EMP_TYPE"
        public String PR_EMP_TYPE
        {
            get { return m_PR_EMP_TYPE; }
            set { m_PR_EMP_TYPE = value; }
        }
        #endregion

        #region "LOCATION"
        public String LOCATION
        {
            get { return m_LOCATION; }
            set { m_LOCATION = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #region "PR_UserID"
        public String PR_UserID
        {
            get { return m_PR_UserID; }
            set { m_PR_UserID = value; }
        }
        #endregion

        //#region "PR_SEGMENT"
        //public String PR_SEGMENT
        //{
        //    get { return m_PR_SEGMENT; }
        //    set { m_PR_SEGMENT = value; }
        //}
        //#endregion

        //#region "PR_DEPT"
        //public String PR_DEPT
        //{
        //    get { return m_PR_DEPT; }
        //    set { m_PR_DEPT = value; }
        //}
        //#endregion

        //#region "PR_CONTRIBUTION"
        //public String PR_CONTRIBUTION
        //{
        //    get { return m_PR_CONTRIBUTION; }
        //    set { m_PR_CONTRIBUTION = value; }
        //}
        //#endregion

        //#region "PR_COSTCODE"
        //public String PR_COSTCODE
        //{
        //    get { return m_PR_COSTCODE; }
        //    set { m_PR_COSTCODE = value; }
        //}
        //#endregion

        //#region "PR_SOEID"
        //public String PR_SOEID
        //{
        //    get { return m_PR_SOEID; }
        //    set { m_PR_SOEID = value; }
        //}
        //#endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _PR_BRANCH = "PR_BRANCH";
        public static readonly string _PR_FIRST_NAME = "PR_FIRST_NAME";
        public static readonly string _PR_LAST_NAME = "PR_LAST_NAME";
        public static readonly string _PR_DESIG = "PR_DESIG";
        public static readonly string _PR_LEVEL = "PR_LEVEL";
        public static readonly string _PR_CATEGORY = "PR_CATEGORY";
        public static readonly string _PR_FUNC_TITTLE1 = "PR_FUNC_TITTLE1";
        public static readonly string _PR_FUNC_TITTLE2 = "PR_FUNC_TITTLE2";
        public static readonly string _PR_JOINING_DATE = "PR_JOINING_DATE";
        public static readonly string _PR_CONFIRM = "PR_CONFIRM";
        public static readonly string _PR_ANNUAL_PACK = "PR_ANNUAL_PACK";
        public static readonly string _PR_NATIONAL_TAX = "PR_NATIONAL_TAX";
        public static readonly string _PR_ACCOUNT_NO = "PR_ACCOUNT_NO";
        public static readonly string _PR_BANK_ID = "PR_BANK_ID";
        public static readonly string _PR_ID_ISSUE = "PR_ID_ISSUE";
        public static readonly string _PR_EXPIRY = "PR_EXPIRY";
        public static readonly string _PR_TAX_INC = "PR_TAX_INC";
        public static readonly string _PR_TAX_PAID = "PR_TAX_PAID";
        public static readonly string _PR_TAX_USED = "PR_TAX_USED";
        public static readonly string _PR_CLOSE_FLAG = "PR_CLOSE_FLAG";
        public static readonly string _PR_CONFIRM_ON = "PR_CONFIRM_ON";
        public static readonly string _PR_EXPECTED = "PR_EXPECTED";
        public static readonly string _PR_TERMIN_TYPE = "PR_TERMIN_TYPE";
        public static readonly string _PR_TERMIN_DATE = "PR_TERMIN_DATE";
        public static readonly string _PR_REASONS = "PR_REASONS";
        public static readonly string _PR_RESIG_RECV = "PR_RESIG_RECV";
        public static readonly string _PR_APP_RESIG = "PR_APP_RESIG";
        public static readonly string _PR_EXIT_INTER = "PR_EXIT_INTER";
        public static readonly string _PR_ID_RETURN = "PR_ID_RETURN";
        public static readonly string _PR_NOTICE = "PR_NOTICE";
        public static readonly string _PR_ARTONY = "PR_ARTONY";
        public static readonly string _PR_DELETION = "PR_DELETION";
        public static readonly string _PR_SETTLE = "PR_SETTLE";
        public static readonly string _PR_CONF_FLAG = "PR_CONF_FLAG";
        public static readonly string _PR_LAST_INCREMENT = "PR_LAST_INCREMENT";
        public static readonly string _PR_NEXT_INCREMENT = "PR_NEXT_INCREMENT";
        public static readonly string _PR_TRANSFER_DATE = "PR_TRANSFER_DATE";
        public static readonly string _PR_PROMOTION_DATE = "PR_PROMOTION_DATE";
        public static readonly string _PR_NEW_BRANCH = "PR_NEW_BRANCH";
        public static readonly string _PR_NEW_ANNUAL_PACK = "PR_NEW_ANNUAL_PACK";
        public static readonly string _PR_TRANSFER = "PR_TRANSFER";
        public static readonly string _PR_MONTH_AWARD = "PR_MONTH_AWARD";
        public static readonly string _PR_RELIGION = "PR_RELIGION";
        public static readonly string _PR_ZAKAT_AMT = "PR_ZAKAT_AMT";
        public static readonly string _PR_MED_FLAG = "PR_MED_FLAG";
        public static readonly string _PR_ATT_FLAG = "PR_ATT_FLAG";
        public static readonly string _PR_PAY_FLAG = "PR_PAY_FLAG";
        public static readonly string _PR_REP_NO = "PR_REP_NO";
        public static readonly string _PR_REFUND_AMT = "PR_REFUND_AMT";
        public static readonly string _PR_REFUND_FOR = "PR_REFUND_FOR";
        public static readonly string _PR_NTN_CERT_REC = "PR_NTN_CERT_REC";
        public static readonly string _PR_NTN_CARD_REC = "PR_NTN_CARD_REC";
        public static readonly string _PR_EMP_TYPE = "PR_EMP_TYPE";
        public static readonly string _LOCATION = "LOCATION";
        public static readonly string _ID = "ID";
        public static readonly string _PR_UserID = "PR_UserID";
        //public static readonly string _PR_SEGMENT = "PR_SEGMENT";
        //public static readonly string _PR_DEPT = "PR_DEPT";
        //public static readonly string _PR_CONTRIBUTION = "PR_CONTRIBUTION";
        //public static readonly string _PR_COSTCODE = "PR_COSTCODE";
        //public static readonly string _PR_SOEID = "PR_SOEID";

        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
