
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BenchmarkCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "BENCHMARK"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/08/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class BenchmarkCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_BEN_FIN_TYPE;
        private Nullable<DateTime> m_BEN_DATE;
        private Nullable<Int32> m_BEN_RATE;
        private String m_w_description;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "BEN_FIN_TYPE"
        public String BEN_FIN_TYPE
        {
            get { return m_BEN_FIN_TYPE; }
            set { m_BEN_FIN_TYPE = value; }
        }
        #endregion
        [CustomAttributes(IsForeignKey = true)]
        #region "BEN_DATE"
        public Nullable<DateTime> BEN_DATE
        {
            get { return m_BEN_DATE; }
            set { m_BEN_DATE = value; }
        }
        #endregion

        #region "BEN_RATE"
        public Nullable<Int32> BEN_RATE
        {
            get { return m_BEN_RATE; }
            set { m_BEN_RATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

          #region "description"
        public String w_description
        {
            get { return m_w_description; }
            set { m_w_description = value; }
        }
        #endregion

        
        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _BEN_FIN_TYPE = "BEN_FIN_TYPE";
        public static readonly string _BEN_DATE = "BEN_DATE";
        public static readonly string _BEN_RATE = "BEN_RATE";
        public static readonly string _ID = "ID";
        public static readonly string _w_description = "w_description";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
