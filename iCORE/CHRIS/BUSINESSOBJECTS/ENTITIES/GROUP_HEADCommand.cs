

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_HEADCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "GROUP_HEAD"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	01/31/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class GROUP_HEADCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_GRHD_NO;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "GRHD_NO"
        public decimal GRHD_NO
        {
            get { return m_GRHD_NO; }
            set { m_GRHD_NO = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _GRHD_NO = "GRHD_NO";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
