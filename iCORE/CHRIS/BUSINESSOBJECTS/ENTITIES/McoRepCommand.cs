

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.mcorepCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "MCO_REP"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Umair Mufti]	03/17/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class mcorepCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private String m_DURATION;
        private decimal m_VEHICLE;
        private decimal m_EMERGENCY;
        private decimal m_STAFF;
        private decimal m_HOUSE;
        private double m_SNO;
        private DateTime m_RDATE;
        private String m_SEG;
        private String m_BRANCH;
        private double m_LON;
        private double m_FIN;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "DURATION"
        public String DURATION
        {
            get { return m_DURATION; }
            set { m_DURATION = value; }
        }
        #endregion

        #region "VEHICLE"
        public decimal VEHICLE
        {
            get { return m_VEHICLE; }
            set { m_VEHICLE = value; }
        }
        #endregion

        #region "EMERGENCY"
        public decimal EMERGENCY
        {
            get { return m_EMERGENCY; }
            set { m_EMERGENCY = value; }
        }
        #endregion

        #region "STAFF"
        public decimal STAFF
        {
            get { return m_STAFF; }
            set { m_STAFF = value; }
        }
        #endregion

        #region "HOUSE"
        public decimal HOUSE
        {
            get { return m_HOUSE; }
            set { m_HOUSE = value; }
        }
        #endregion

        #region "SNO"
        public double SNO
        {
            get { return m_SNO; }
            set { m_SNO = value; }
        }
        #endregion

        #region "RDATE"
        public DateTime RDATE
        {
            get { return m_RDATE; }
            set { m_RDATE = value; }
        }
        #endregion

        #region "SEG"
        public String SEG
        {
            get { return m_SEG; }
            set { m_SEG = value; }
        }
        #endregion

        #region "BRANCH"
        public String BRANCH
        {
            get { return m_BRANCH; }
            set { m_BRANCH = value; }
        }
        #endregion

        #region "LON"
        public double LON
        {
            get { return m_LON; }
            set { m_LON = value; }
        }
        #endregion

        #region "FIN"
        public double FIN
        {
            get { return m_FIN; }
            set { m_FIN = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _DURATION = "DURATION";
        public static readonly string _VEHICLE = "VEHICLE";
        public static readonly string _EMERGENCY = "EMERGENCY";
        public static readonly string _STAFF = "STAFF";
        public static readonly string _HOUSE = "HOUSE";
        public static readonly string _SNO = "SNO";
        public static readonly string _RDATE = "RDATE";
        public static readonly string _SEG = "SEG";
        public static readonly string _BRANCH = "BRANCH";
        public static readonly string _LON = "LON";
        public static readonly string _FIN = "FIN";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}