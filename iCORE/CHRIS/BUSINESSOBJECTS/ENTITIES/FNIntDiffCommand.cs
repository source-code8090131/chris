

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FNIntDiffCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "FN_INT_DIFF"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faisal Iqbal]	12/13/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FNIntDiffCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_FN_COUNTRY_CODE;
        private String m_FN_SEGMENT;
        private decimal m_FN_RENTAL_GUARANT;
        private String m_FN_MAKER_NAME;
        private DateTime m_FN_MAKER_DATE;
        private String m_FN_MAKER_TIME;
        private String m_FN_MAKER_LOC;
        private String m_FN_MAKER_TERM;
        private String m_FN_AUTH_NAME;
        private DateTime m_FN_AUTH_DATE;
        private String m_FN_AUTH_TIME;
        private String m_FN_AUTH_LOC;
        private String m_FN_AUTH_TERM;
        private String m_FN_AUTH_FLAG;
        private String m_FN_STATUS;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "FN_COUNTRY_CODE"
        public String FN_COUNTRY_CODE
        {
            get { return m_FN_COUNTRY_CODE; }
            set { m_FN_COUNTRY_CODE = value; }
        }
        #endregion

        #region "FN_SEGMENT"
        public String FN_SEGMENT
        {
            get { return m_FN_SEGMENT; }
            set { m_FN_SEGMENT = value; }
        }
        #endregion

        #region "FN_RENTAL_GUARANT"
        public decimal FN_RENTAL_GUARANT
        {
            get { return m_FN_RENTAL_GUARANT; }
            set { m_FN_RENTAL_GUARANT = value; }
        }
        #endregion

        #region "FN_MAKER_NAME"
        public String FN_MAKER_NAME
        {
            get { return m_FN_MAKER_NAME; }
            set { m_FN_MAKER_NAME = value; }
        }
        #endregion

        #region "FN_MAKER_DATE"
        public DateTime FN_MAKER_DATE
        {
            get { return m_FN_MAKER_DATE; }
            set { m_FN_MAKER_DATE = value; }
        }
        #endregion

        #region "FN_MAKER_TIME"
        public String FN_MAKER_TIME
        {
            get { return m_FN_MAKER_TIME; }
            set { m_FN_MAKER_TIME = value; }
        }
        #endregion

        #region "FN_MAKER_LOC"
        public String FN_MAKER_LOC
        {
            get { return m_FN_MAKER_LOC; }
            set { m_FN_MAKER_LOC = value; }
        }
        #endregion

        #region "FN_MAKER_TERM"
        public String FN_MAKER_TERM
        {
            get { return m_FN_MAKER_TERM; }
            set { m_FN_MAKER_TERM = value; }
        }
        #endregion

        #region "FN_AUTH_NAME"
        public String FN_AUTH_NAME
        {
            get { return m_FN_AUTH_NAME; }
            set { m_FN_AUTH_NAME = value; }
        }
        #endregion

        #region "FN_AUTH_DATE"
        public DateTime FN_AUTH_DATE
        {
            get { return m_FN_AUTH_DATE; }
            set { m_FN_AUTH_DATE = value; }
        }
        #endregion

        #region "FN_AUTH_TIME"
        public String FN_AUTH_TIME
        {
            get { return m_FN_AUTH_TIME; }
            set { m_FN_AUTH_TIME = value; }
        }
        #endregion

        #region "FN_AUTH_LOC"
        public String FN_AUTH_LOC
        {
            get { return m_FN_AUTH_LOC; }
            set { m_FN_AUTH_LOC = value; }
        }
        #endregion

        #region "FN_AUTH_TERM"
        public String FN_AUTH_TERM
        {
            get { return m_FN_AUTH_TERM; }
            set { m_FN_AUTH_TERM = value; }
        }
        #endregion

        #region "FN_AUTH_FLAG"
        public String FN_AUTH_FLAG
        {
            get { return m_FN_AUTH_FLAG; }
            set { m_FN_AUTH_FLAG = value; }
        }
        #endregion

        #region "FN_STATUS"
        public String FN_STATUS
        {
            get { return m_FN_STATUS; }
            set { m_FN_STATUS = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _FN_PR_P_NO = "PR_P_NO";
        public static readonly string _FN_COUNTRY_CODE = "FN_COUNTRY_CODE";
        public static readonly string _FN_SEGMENT = "FN_SEGMENT";
        public static readonly string _FN_RENTAL_GUARANT = "FN_RENTAL_GUARANT";
        public static readonly string _FN_MAKER_NAME = "FN_MAKER_NAME";
        public static readonly string _FN_MAKER_DATE = "FN_MAKER_DATE";
        public static readonly string _FN_MAKER_TIME = "FN_MAKER_TIME";
        public static readonly string _FN_MAKER_LOC = "FN_MAKER_LOC";
        public static readonly string _FN_MAKER_TERM = "FN_MAKER_TERM";
        public static readonly string _FN_AUTH_NAME = "FN_AUTH_NAME";
        public static readonly string _FN_AUTH_DATE = "FN_AUTH_DATE";
        public static readonly string _FN_AUTH_TIME = "FN_AUTH_TIME";
        public static readonly string _FN_AUTH_LOC = "FN_AUTH_LOC";
        public static readonly string _FN_AUTH_TERM = "FN_AUTH_TERM";
        public static readonly string _FN_AUTH_FLAG = "FN_AUTH_FLAG";
        public static readonly string _FN_STATUS = "FN_STATUS";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        public FNIntDiffCommand Clone()
        {
            FNIntDiffCommand obj = new FNIntDiffCommand();

            obj.FN_AUTH_DATE = this.FN_AUTH_DATE;
            obj.FN_AUTH_FLAG = this.FN_AUTH_FLAG;
            obj.FN_AUTH_LOC= this.FN_AUTH_LOC;
            obj.FN_AUTH_NAME= this.FN_AUTH_NAME;

            obj.FN_AUTH_TERM= this.FN_AUTH_TERM;
            obj.FN_AUTH_TIME= this.FN_AUTH_TIME;
            obj.FN_COUNTRY_CODE= this.FN_COUNTRY_CODE;
            obj.FN_MAKER_DATE= this.FN_MAKER_DATE;

            obj.FN_MAKER_LOC= this.FN_MAKER_LOC;
            obj.FN_MAKER_NAME = this.FN_MAKER_NAME;
            obj.FN_MAKER_TERM= this.FN_MAKER_TERM;
            obj.FN_MAKER_TIME = this.FN_MAKER_TIME;

            obj.FN_RENTAL_GUARANT= this.FN_RENTAL_GUARANT;
            obj.FN_SEGMENT= this.FN_SEGMENT;
            obj.FN_STATUS = this.FN_STATUS;
            obj.ID = this.ID;

            obj.PR_P_NO = this.PR_P_NO;
            obj.SearchFilter= this.SearchFilter;
            obj.SoeId= this.SoeId;

            return obj;

        }

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
