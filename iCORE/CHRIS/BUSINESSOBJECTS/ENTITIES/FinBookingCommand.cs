
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.finBookingCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "fin_Booking"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Faizan Ashraf]	12/20/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FinBookingCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private String m_FN_BRANCH;
        private String m_FN_TYPE;
        private Double m_FN_PAY_SCHED;
        private String m_FN_FIN_NO;
        private DateTime m_FN_START_DATE;
        private DateTime m_FN_END_DATE;
        private DateTime m_FN_EXTRA_TIME;
        private Nullable<Double> m_FN_AMT_AVAILED;
        private Nullable<Double> m_FN_MONTHLY_DED;
        private Nullable<Double> m_FN_C_RATIO_PER;
        private String m_FN_LIQUIDATE;
        private String m_EXCP_FLAG;
        private String m_EXCP_REM;
        private String m_FN_SUBTYPE;
        private String m_FN_MORTG_PROP_ADD;
        private String m_FN_MORTG_PROP_CITY;
        private Double m_FN_MORTG_PROP_VALSA;
        private Double m_FN_MORTG_PROP_VALVR;
        private DateTime m_PAY_GEN_DATE;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "FN_BRANCH"
        public String FN_BRANCH
        {
            get { return m_FN_BRANCH; }
            set { m_FN_BRANCH = value; }
        }
        #endregion

        #region "FN_TYPE"
        public String FN_TYPE
        {
            get { return m_FN_TYPE; }
            set { m_FN_TYPE = value; }
        }
        #endregion

        #region "FN_PAY_SCHED"
        public Double FN_PAY_SCHED
        {
            get { return m_FN_PAY_SCHED; }
            set { m_FN_PAY_SCHED = value; }
        }
        #endregion

        #region "FN_FIN_NO"
        public String FN_FIN_NO
        {
            get { return m_FN_FIN_NO; }
            set { m_FN_FIN_NO = value; }
        }
        #endregion

        #region "FN_START_DATE"
        public DateTime FN_START_DATE
        {
            get { return m_FN_START_DATE; }
            set { m_FN_START_DATE = value; }
        }
        #endregion

        #region "FN_END_DATE"
        public DateTime FN_END_DATE
        {
            get { return m_FN_END_DATE; }
            set { m_FN_END_DATE = value; }
        }
        #endregion

        #region "FN_EXTRA_TIME"
        public DateTime FN_EXTRA_TIME
        {
            get { return m_FN_EXTRA_TIME; }
            set { m_FN_EXTRA_TIME = value; }
        }
        #endregion

        #region "FN_AMT_AVAILED"
        public Nullable<Double> FN_AMT_AVAILED
        {
            get { return m_FN_AMT_AVAILED; }
            set { m_FN_AMT_AVAILED = value; }
        }
        #endregion

        #region "FN_MONTHLY_DED"
        public Nullable<Double> FN_MONTHLY_DED
        {
            get { return m_FN_MONTHLY_DED; }
            set { m_FN_MONTHLY_DED = value; }
        }
        #endregion

        #region "FN_C_RATIO_PER"
        public Nullable<Double> FN_C_RATIO_PER
        {
            get { return m_FN_C_RATIO_PER; }
            set { m_FN_C_RATIO_PER = value; }
        }
        #endregion

        #region "FN_LIQUIDATE"
        public String FN_LIQUIDATE
        {
            get { return m_FN_LIQUIDATE; }
            set { m_FN_LIQUIDATE = value; }
        }
        #endregion

        #region "EXCP_FLAG"
        public String EXCP_FLAG
        {
            get { return m_EXCP_FLAG; }
            set { m_EXCP_FLAG = value; }
        }
        #endregion

        #region "EXCP_REM"
        public String EXCP_REM
        {
            get { return m_EXCP_REM; }
            set { m_EXCP_REM = value; }
        }
        #endregion

        #region "FN_SUBTYPE"
        public String FN_SUBTYPE
        {
            get { return m_FN_SUBTYPE; }
            set { m_FN_SUBTYPE = value; }
        }
        #endregion

        #region "FN_MORTG_PROP_ADD"
        public String FN_MORTG_PROP_ADD
        {
            get { return m_FN_MORTG_PROP_ADD; }
            set { m_FN_MORTG_PROP_ADD = value; }
        }
        #endregion

        #region "FN_MORTG_PROP_CITY"
        public String FN_MORTG_PROP_CITY
        {
            get { return m_FN_MORTG_PROP_CITY; }
            set { m_FN_MORTG_PROP_CITY = value; }
        }
        #endregion

        #region "FN_MORTG_PROP_VALSA"
        public Double FN_MORTG_PROP_VALSA
        {
            get { return m_FN_MORTG_PROP_VALSA; }
            set { m_FN_MORTG_PROP_VALSA = value; }
        }
        #endregion

        #region "FN_MORTG_PROP_VALVR"
        public Double FN_MORTG_PROP_VALVR
        {
            get { return m_FN_MORTG_PROP_VALVR; }
            set { m_FN_MORTG_PROP_VALVR = value; }
        }
        #endregion

        #region "PAY_GEN_DATE"
        public DateTime PAY_GEN_DATE
        {
            get { return m_PAY_GEN_DATE; }
            set { m_PAY_GEN_DATE = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "FN_P_NO";
        public static readonly string _FN_BRANCH = "FN_BRANCH";
        public static readonly string _FN_TYPE = "FN_TYPE";
        public static readonly string _FN_PAY_SCHED = "FN_PAY_SCHED";
        public static readonly string _FN_FIN_NO = "FN_FINANCE_NO";
        public static readonly string _FN_START_DATE = "FN_START_DATE";
        public static readonly string _FN_END_DATE = "FN_END_DATE";
        public static readonly string _FN_EXTRA_TIME = "FN_EXTRA_TIME";
        public static readonly string _FN_AMT_AVAILED = "FN_AMT_AVAILED";
        public static readonly string _FN_MONTHLY_DED = "FN_MONTHLY_DED";
        public static readonly string _FN_C_RATIO_PER = "FN_C_RATIO_PER";
        public static readonly string _FN_LIQUIDATE = "FN_LIQUIDATE";
        public static readonly string _EXCP_FLAG = "EXCP_FLAG";
        public static readonly string _EXCP_REM = "EXCP_REM";
        public static readonly string _FN_SUBTYPE = "FN_SUBTYPE";
        public static readonly string _FN_MORTG_PROP_ADD = "FN_MORTG_PROP_ADD";
        public static readonly string _FN_MORTG_PROP_CITY = "FN_MORTG_PROP_CITY";
        public static readonly string _FN_MORTG_PROP_VALSA = "FN_MORTG_PROP_VALSA";
        public static readonly string _FN_MORTG_PROP_VALVR = "FN_MORTG_PROP_VALVR";
        public static readonly string _PAY_GEN_DATE = "PAY_GEN_DATE";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}