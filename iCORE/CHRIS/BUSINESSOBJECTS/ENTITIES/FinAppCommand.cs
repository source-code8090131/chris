
/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "DEPT_CONT"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	12/10/10	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class FinAppCommand : BusinessEntity
    {
        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;

        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        private String m_FN_FIN_NO;

        public String FN_FIN_NO
        {
            get { return m_FN_FIN_NO; }
            set { m_FN_FIN_NO = value; }
        }
        private Double m_FN_INSTALL;

        public Double FN_INSTALL
        {
            get { return m_FN_INSTALL; }
            set { m_FN_INSTALL = value; }
        }
        private Double m_FN_BALANCE;

        public Double FN_BALANCE
        {
            get { return m_FN_BALANCE; }
            set { m_FN_BALANCE = value; }
        }


        private String m_FN_LIQ_FLAG;

        public String FN_LIQ_FLAG
        {
            get { return m_FN_LIQ_FLAG; }
            set { m_FN_LIQ_FLAG = value; }
        }
        private Double m_FN_REDUCE_AMT;

        public Double FN_REDUCE_AMT
        {
            get { return m_FN_REDUCE_AMT; }
            set { m_FN_REDUCE_AMT = value; }
        }
        private Double m_FN_NEW_INST;

        public Double FN_NEW_INST
        {
            get { return m_FN_NEW_INST; }
            set { m_FN_NEW_INST = value; }
        }
        private Double m_FN_NEW_OS_BAL;

        public Double FN_NEW_OS_BAL
        {
            get { return m_FN_NEW_OS_BAL; }
            set { m_FN_NEW_OS_BAL = value; }
        }
        private int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

            
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PR_P_NO";
        public static readonly string _FN_FIN_NO = "FN_FIN_NO";
        public static readonly string _FN_INSTALL = "FN_INSTALL";
        public static readonly string _FN_BALANCE = "FN_BALANCE";
        public static readonly string _FN_LIQ_FLAG = "FN_LIQ_FLAG";
        public static readonly string _FN_REDUCE_AMT = "FN_REDUCE_AMT";
        public static readonly string _FN_NEW_INST = "FN_NEW_INST";
        public static readonly string _FN_NEW_OS_BAL = "FN_NEW_OS_BAL";
        public static readonly string _FN_SDATE = "FN_SDATE";
        public static readonly string _ID = "ID";
        #endregion

   




        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }

}
