

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AttritionTypeCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "ATTR_TYPE"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Nida Nazir]	02/01/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class AttritionTypeCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private decimal m_ATTR_TYPE_CODE;
        private decimal m_ATTR_CATEGORY_CODE;
        private String m_ATTR_TYPE_DESC;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "ATTR_TYPE_CODE"
        public decimal ATTR_TYPE_CODE
        {
            get { return m_ATTR_TYPE_CODE; }
            set { m_ATTR_TYPE_CODE = value; }
        }
        #endregion

        #region "ATTR_CATEGORY_CODE"
        public decimal ATTR_CATEGORY_CODE
        {
            get { return m_ATTR_CATEGORY_CODE; }
            set { m_ATTR_CATEGORY_CODE = value; }
        }
        #endregion

        #region "ATTR_TYPE_DESC"
        public String ATTR_TYPE_DESC
        {
            get { return m_ATTR_TYPE_DESC; }
            set { m_ATTR_TYPE_DESC = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _ATTR_TYPE_CODE = "ATTR_TYPE_CODE";
        public static readonly string _ATTR_CATEGORY_CODE = "ATTR_CATEGORY_CODE";
        public static readonly string _ATTR_TYPE_DESC = "ATTR_TYPE_DESC";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
