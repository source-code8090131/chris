

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PAYROLL"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Ahad Zubair]	01/05/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PAYROLLCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private DateTime m_PA_DATE;
        private decimal? m_PA_WORKED_DAYS;
        private decimal? m_PA_BASIC;
        private decimal? m_PA_HOUSE;
        private decimal? m_PA_CONV_OFF;
        private decimal? m_PA_PFUND;
        private decimal? m_PA_GRATUITY;
        private decimal? m_PA_INCOME_TAX;
        private decimal? m_PA_SAL_ADVANCE;
        private decimal? m_PA_F_INTREST;
        private decimal? m_PA_F_INSTALL;
        private decimal? m_PA_L_INTREST;
        private decimal? m_PA_L_INSTALL;
        private decimal? m_PA_EXCISE_LEVY;
        private decimal? m_PA_ZAKAT;
        private decimal? m_PA_INS_PREMIUM;
        private String m_PA_MONTH_POST;
        private String m_PA_REPORT_FLAG;
        private String m_PA_FLAG;
        private String m_PA_BRANCH;
        private String m_PA_CATEGORY;
        private decimal? m_PA_NET_PAY;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PA_DATE"
        public DateTime PA_DATE
        {
            get { return m_PA_DATE; }
            set { m_PA_DATE = value; }
        }
        #endregion

        #region "PA_WORKED_DAYS"
        public decimal? PA_WORKED_DAYS
        {
            get { return m_PA_WORKED_DAYS; }
            set { m_PA_WORKED_DAYS = value; }
        }
        #endregion

        #region "PA_BASIC"
        public decimal? PA_BASIC
        {
            get { return m_PA_BASIC; }
            set { m_PA_BASIC = value; }
        }
        #endregion

        #region "PA_HOUSE"
        public decimal? PA_HOUSE
        {
            get { return m_PA_HOUSE; }
            set { m_PA_HOUSE = value; }
        }
        #endregion

        #region "PA_CONV_OFF"
        public decimal? PA_CONV_OFF
        {
            get { return m_PA_CONV_OFF; }
            set { m_PA_CONV_OFF = value; }
        }
        #endregion

        #region "PA_PFUND"
        public decimal? PA_PFUND
        {
            get { return m_PA_PFUND; }
            set { m_PA_PFUND = value; }
        }
        #endregion

        #region "PA_GRATUITY"
        public decimal? PA_GRATUITY
        {
            get { return m_PA_GRATUITY; }
            set { m_PA_GRATUITY = value; }
        }
        #endregion

        #region "PA_INCOME_TAX"
        public decimal? PA_INCOME_TAX
        {
            get { return m_PA_INCOME_TAX; }
            set { m_PA_INCOME_TAX = value; }
        }
        #endregion

        #region "PA_SAL_ADVANCE"
        public decimal? PA_SAL_ADVANCE
        {
            get { return m_PA_SAL_ADVANCE; }
            set { m_PA_SAL_ADVANCE = value; }
        }
        #endregion

        #region "PA_F_INTREST"
        public decimal? PA_F_INTREST
        {
            get { return m_PA_F_INTREST; }
            set { m_PA_F_INTREST = value; }
        }
        #endregion

        #region "PA_F_INSTALL"
        public decimal? PA_F_INSTALL
        {
            get { return m_PA_F_INSTALL; }
            set { m_PA_F_INSTALL = value; }
        }
        #endregion

        #region "PA_L_INTREST"
        public decimal? PA_L_INTREST
        {
            get { return m_PA_L_INTREST; }
            set { m_PA_L_INTREST = value; }
        }
        #endregion

        #region "PA_L_INSTALL"
        public decimal? PA_L_INSTALL
        {
            get { return m_PA_L_INSTALL; }
            set { m_PA_L_INSTALL = value; }
        }
        #endregion

        #region "PA_EXCISE_LEVY"
        public decimal? PA_EXCISE_LEVY
        {
            get { return m_PA_EXCISE_LEVY; }
            set { m_PA_EXCISE_LEVY = value; }
        }
        #endregion

        #region "PA_ZAKAT"
        public decimal? PA_ZAKAT
        {
            get { return m_PA_ZAKAT; }
            set { m_PA_ZAKAT = value; }
        }
        #endregion

        #region "PA_INS_PREMIUM"
        public decimal? PA_INS_PREMIUM
        {
            get { return m_PA_INS_PREMIUM; }
            set { m_PA_INS_PREMIUM = value; }
        }
        #endregion

        #region "PA_MONTH_POST"
        public String PA_MONTH_POST
        {
            get { return m_PA_MONTH_POST; }
            set { m_PA_MONTH_POST = value; }
        }
        #endregion

        #region "PA_REPORT_FLAG"
        public String PA_REPORT_FLAG
        {
            get { return m_PA_REPORT_FLAG; }
            set { m_PA_REPORT_FLAG = value; }
        }
        #endregion

        #region "PA_FLAG"
        public String PA_FLAG
        {
            get { return m_PA_FLAG; }
            set { m_PA_FLAG = value; }
        }
        #endregion

        #region "PA_BRANCH"
        public String PA_BRANCH
        {
            get { return m_PA_BRANCH; }
            set { m_PA_BRANCH = value; }
        }
        #endregion

        #region "PA_CATEGORY"
        public String PA_CATEGORY
        {
            get { return m_PA_CATEGORY; }
            set { m_PA_CATEGORY = value; }
        }
        #endregion

        #region "PA_NET_PAY"
        public decimal? PA_NET_PAY
        {
            get { return m_PA_NET_PAY; }
            set { m_PA_NET_PAY = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PA_P_NO";
        public static readonly string _PA_DATE = "PA_DATE";
        public static readonly string _PA_WORKED_DAYS = "PA_WORKED_DAYS";
        public static readonly string _PA_BASIC = "PA_BASIC";
        public static readonly string _PA_HOUSE = "PA_HOUSE";
        public static readonly string _PA_CONV_OFF = "PA_CONV_OFF";
        public static readonly string _PA_PFUND = "PA_PFUND";
        public static readonly string _PA_GRATUITY = "PA_GRATUITY";
        public static readonly string _PA_INCOME_TAX = "PA_INCOME_TAX";
        public static readonly string _PA_SAL_ADVANCE = "PA_SAL_ADVANCE";
        public static readonly string _PA_F_INTREST = "PA_F_INTREST";
        public static readonly string _PA_F_INSTALL = "PA_F_INSTALL";
        public static readonly string _PA_L_INTREST = "PA_L_INTREST";
        public static readonly string _PA_L_INSTALL = "PA_L_INSTALL";
        public static readonly string _PA_EXCISE_LEVY = "PA_EXCISE_LEVY";
        public static readonly string _PA_ZAKAT = "PA_ZAKAT";
        public static readonly string _PA_INS_PREMIUM = "PA_INS_PREMIUM";
        public static readonly string _PA_MONTH_POST = "PA_MONTH_POST";
        public static readonly string _PA_REPORT_FLAG = "PA_REPORT_FLAG";
        public static readonly string _PA_FLAG = "PA_FLAG";
        public static readonly string _PA_BRANCH = "PA_BRANCH";
        public static readonly string _PA_CATEGORY = "PA_CATEGORY";
        public static readonly string _PA_NET_PAY = "PA_NET_PAY";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
