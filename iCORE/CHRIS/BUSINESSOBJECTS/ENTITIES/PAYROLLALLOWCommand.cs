

/* -----------------------------------------------------------------------------------------------------------
 * Project	 : SL.Framework.BusinessEntities
 * Class	 : iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLALLOWCommand
 * Company 	 : Copyright � 2010 Systems Ltd. All rights reserved.
 * ----------------------------------------------------------------------------------------------------------- */
/// <summary>
/// Business entity "PAYROLL_ALLOW"
/// </summary>
/// <remarks>
/// These code statements are auto generated to integrate with Citibank.Business architecture.
/// </remarks>
/// <history>
/// 	[Ahad Zubair]	01/06/11	Created
/// </history>
/// -----------------------------------------------------------------------------------------------------------

#region --System Namespaces--
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using iCORE.Common;
#endregion
#region --Company Namespaces--
#endregion

namespace iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES
{
    public class PAYROLLALLOWCommand : BusinessEntity
    {

        //-------------------------------------Start Code generation for Business-------------------------------------

        #region "Auto generated code for Business Entity"

        #region "--Field Segment--"
        private Double m_PR_P_NO;
        private DateTime m_PW_PAY_DATE;
        private String m_PW_ALL_CODE;
        private decimal m_PW_ALL_AMOUNT;
        private String m_PW_ACCOUNT;
        private int m_ID;
        #endregion "--Field Segment--"

        #region "--Property Segment--"

        #region "PR_P_NO"
        [CustomAttributes(IsForeignKey = true)]
        public Double PR_P_NO
        {
            get { return m_PR_P_NO; }
            set { m_PR_P_NO = value; }
        }
        #endregion

        #region "PW_PAY_DATE"
        public DateTime PW_PAY_DATE
        {
            get { return m_PW_PAY_DATE; }
            set { m_PW_PAY_DATE = value; }
        }
        #endregion

        #region "PW_ALL_CODE"
        public String PW_ALL_CODE
        {
            get { return m_PW_ALL_CODE; }
            set { m_PW_ALL_CODE = value; }
        }
        #endregion

        #region "PW_ALL_AMOUNT"
        public decimal PW_ALL_AMOUNT
        {
            get { return m_PW_ALL_AMOUNT; }
            set { m_PW_ALL_AMOUNT = value; }
        }
        #endregion

        #region "PW_ACCOUNT"
        public String PW_ACCOUNT
        {
            get { return m_PW_ACCOUNT; }
            set { m_PW_ACCOUNT = value; }
        }
        #endregion

        #region "ID"
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion

        #endregion --Public Properties--

        #region "--Column Mapping--"
        public static readonly string _PR_P_NO = "PW_P_NO";
        public static readonly string _PW_PAY_DATE = "PW_PAY_DATE";
        public static readonly string _PW_ALL_CODE = "PW_ALL_CODE";
        public static readonly string _PW_ALL_AMOUNT = "PW_ALL_AMOUNT";
        public static readonly string _PW_ACCOUNT = "PW_ACCOUNT";
        public static readonly string _ID = "ID";
        #endregion

        #endregion "Auto generated code"

        //-------------------------------------End Code generation for Business---------------------------------------

        //----------------------Region to keep all customized business related logic----------------------------

        #region "--Customize Business Methods--"

        #endregion "Customize Business Function"

        //-----------------------------------Customized region ends here----------------------------------------
    }
}
