using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_OMILimCalc : ChrisSimpleForm
    {
        #region --Varibale--
        string g_Child = string.Empty;

        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_OMILimCalc()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_OMILimCalc(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region --Method--

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                this.txtUser.Text       = this.userID;
                this.txtLocation.Text   = this.CurrentLocation;
                this.lblUser.Text       = this.UserName;
                this.txtDate.Text       = this.Now().ToString("dd/MM/yyyy");
                this.txtOption.Visible  = false;
                this.txt_W_Month.Focus();
                tbtAdd.Visible      = false;
                tbtDelete.Visible   = false;
                tbtEdit.Visible     = false;
                tbtList.Visible     = false;
                tbtSave.Visible     = false;

            }
            catch (Exception exp)
            {
                LogError(this.Name, "OnLoad", exp);
            }
        }

        public void Proc1()
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Clear();
                param.Add("OMI_MONTH", txt_W_Month.Text);
                param.Add("OMI_YEAR",  txt_W_Year.Text);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode                = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC1", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    foreach(DataRow dr  in rsltCode.dstResult.Tables[0].Rows)
                    {
                        Application.DoEvents();
                        txt_W_PNO.Text              = dr["PR_P_NO"].ToString();
                        txt_W_Name.Text             = dr["NAME"].ToString();
                        txt_W_Cat.Text              = dr["PR_CATEGORY"].ToString();
                        if (dr["PR_JOINING_DATE"].ToString() != "")
                            txt_W_Joining_Date.Value    = Convert.ToDateTime(dr["PR_JOINING_DATE"].ToString());

                        txt_W_Status.Text           = dr["PR_MARITAL"].ToString();

                        if (dr["PR_MARRIAGE"].ToString() != "")
                            txt_W_Marriage_Date.Value   = Convert.ToDateTime(dr["PR_MARRIAGE"].ToString());

                        Proc2();
                    }
                }
            }
            catch(Exception exp)
            {
                LogError(this.Name, "Proc1", exp);
            }
        }

        public void Proc2()
        {
            try
            {
                string pol_Date = "01/" + txt_W_Month.Text + "/" + txt_W_Year.Text;
                     

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("SEARCHFILTER", pol_Date);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode                = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC2", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 1)
                {
                    txt_W_OMI_Limit.Text    = rsltCode.dstResult.Tables[0].Rows[0]["POL_OMI_LIMIT"].ToString();
                    txt_W_Self.Text         = rsltCode.dstResult.Tables[0].Rows[0]["POL_OMI_SELF"].ToString();
                    txt_W_Spouse.Text       = rsltCode.dstResult.Tables[0].Rows[0]["POL_PER_DEPEN_SPOUSE"].ToString();
                    txt_W_Child.Text        = rsltCode.dstResult.Tables[0].Rows[0]["POL_PER_DEPEN_CHILD"].ToString();
                }
                else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    txt_W_OMI_Limit.Text = "0";
                }
                else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 1)
                {
                    MessageBox.Show("Problem in Policy_master Table Too many Rows..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                /* CALLING PROCEDURE TO PRO-RATE OMI LIMIT */
                g_Child = txt_OMI_Child.Text == string.Empty ? "0" : txt_OMI_Child.Text;

                Proc5();
                Proc6();

                int omi_limit = Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text ) + Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text ) + Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text );
                txt_W_OMI_Limit.Text = omi_limit.ToString();

                /* UPDATE OMI LIMITS EVERY TIME DUE TO MARRIAGE OR BIRTH OF CHILD*/
                Proc7();
                /*  END OF CALLING */

                //IF MESSAGE_CODE  = 40350  THEN
                //   :OMI_SELF         := :W_SELF;
                //   :OMI_SPOUSE       := :W_SPOUSE;
                //   :OMI_CHILD        := :W_CHILD;
                //END IF;


                Proc8();

                int os_Balance = Convert.ToInt32(txt_W_OMI_Limit.Text == string.Empty ? "0" : txt_W_OMI_Limit.Text) + Convert.ToInt32(txt_OMI_Total_Claim.Text == string.Empty ? "0" : txt_OMI_Total_Claim.Text);
                txt_W_OS_Balance.Text = os_Balance.ToString();
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc2", exp);
            }
        }

        public void Proc5()
        {
            try
            {
                double ww_Self     = 0;
                double ww_Spouse   = 0;
                DateTime dt_Joining = Convert.ToDateTime(txt_W_Joining_Date.Value);
                DateTime dt_Marriage = Convert.ToDateTime(txt_W_Marriage_Date.Value);

                string  YearEnd = "31/12/" + txt_W_Year.Text;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                /* JOINED IN THE CURRENT YEAR AND MARRIED AT THE TIME OF JOINING */
                if (dt_Joining.Year == Convert.ToInt32(txt_W_Year.Text) && txt_W_Status.Text == "M" && Convert.ToDateTime(txt_W_Marriage_Date.Value) < Convert.ToDateTime(txt_W_Joining_Date.Value))
                {
                    ww_Self     = ((Convert.ToInt32(txt_W_Self.Text) / 12) * (Math.Round( Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Joining_Date.Value))))));
                    ww_Spouse   = ((Convert.ToInt32(txt_W_Spouse.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Joining_Date.Value))))));
                }
                else if (txt_W_Status.Text == "M" && Convert.ToDateTime(txt_W_Marriage_Date.Value) > Convert.ToDateTime(txt_W_Joining_Date.Value) && dt_Marriage.Year == Convert.ToInt32(txt_W_Year.Text))
                {
                    /* MARRIED AFTER JOINING AND MARRIAGE IS IN CURRENT YEAR */

                    ww_Self     = Convert.ToDouble(txt_W_Self.Text);
                    ww_Spouse   = ((Convert.ToInt32(txt_W_Spouse.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Marriage_Date.Value))))));
                }
                else if (txt_W_Status.Text == "M" && dt_Joining.Year < Convert.ToInt32(txt_W_Year.Text) && (Convert.ToDateTime(txt_W_Marriage_Date.Value) < Convert.ToDateTime(txt_W_Year.Text) || dt_Marriage == null))
                {
                    ww_Self = Convert.ToDouble(txt_W_Self.Text);
                    ww_Spouse = Convert.ToDouble(txt_W_Spouse.Text);
                }
                else if((txt_W_Status.Text == "S" || txt_W_Status.Text == "D" || txt_W_Status.Text == "W") && (dt_Joining.Year == Convert.ToInt32(txt_W_Year.Text)))
                {
                    /* SINGLE AND JOINED IN THE CURRENT YEAR */
                    ww_Self = ((Convert.ToInt32(txt_W_Self.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Joining_Date.Value))))));
                }
                else if ((txt_W_Status.Text == "S" || txt_W_Status.Text == "D" || txt_W_Status.Text == "W") && dt_Joining.Year < Convert.ToInt32(txt_W_Year.Text))
                {
                    /* SINGLE AND JOINED BEFORE CURRENT YEAR*/
                    ww_Self = Convert.ToDouble(txt_W_Self.Text);
                }

                txt_W_Self.Text     = ww_Self.ToString();
                txt_W_Spouse.Text   = ww_Spouse.ToString();


                if ((Convert.ToInt32(txt_OMI_Spouse.Text == string.Empty ? "0" : txt_OMI_Spouse.Text ) == 0) && (Convert.ToInt32(txt_OMI_Spouse.Text == string.Empty ? "0" : txt_OMI_Spouse.Text) > 0))
                {
                    txt_OMI_Spouse.Text = txt_W_Spouse.Text;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc5", exp);
            }
        }

        public void Proc6()
        {
            try
            {
                string  YearEnd     = "31/12/" + txt_W_Year.Text;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_PNO.Text);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode                = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC6", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 1)
                {
                    double WW_CHILD = 0;
                    int W_OMI    = 0;

                    W_OMI = Convert.ToInt32(g_Child);

                    foreach (DataRow dr in rsltCode.dstResult.Tables[0].Rows)
                    {
                        if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year < Convert.ToInt32(txt_W_Year.Text))
                        {
                            WW_CHILD = WW_CHILD + Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text);
                        }
                        else if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year == Convert.ToInt32(txt_W_Year.Text))
                        {
                            WW_CHILD = (Convert.ToInt32(txt_W_Child.Text) / 12) * Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(dt_YearEnd, Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"])))) + WW_CHILD;

                        }

                        txt_W_Child.Text    = Convert.ToString((WW_CHILD - W_OMI) + W_OMI);
                        txt_OMI_Child.Text  = txt_W_Child.Text;

                    }
                    
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc6", exp);
            }
        }

        public void Proc7()
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_PNO.Text);
                param.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC7", param);
                param.Clear();
                
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    /*UPDATE*/

                    Dictionary<string, object> paramUp = new Dictionary<string, object>();
                    paramUp.Add("OMI_P_NO", txt_W_PNO.Text);
                    paramUp.Add("W_Date", Convert.ToDateTime(txtDate.Text).Year);
                    paramUp.Add("OMI_TOTAL_RECEV", txt_W_OMI_Limit.Text);

                    Result rsltCodeUp;
                    rsltCodeUp = cmnDM.Execute("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC7_UPDATE", paramUp);

                }
                else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    /*Insert*/

                    Dictionary<string, object> paramIns = new Dictionary<string, object>();
                    paramIns.Add("OMI_P_NO", txt_W_PNO.Text);
                    paramIns.Add("W_Date", Convert.ToDateTime(txtDate.Text).Year);
                    paramIns.Add("W_OMI_LIMIT", txt_W_OMI_Limit.Text);

                    Result rsltCodeIns;
                    rsltCodeIns = cmnDM.Execute("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC7_INSERT", paramIns);
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc7", exp);
            }
        }

        public void Proc8()
        {
            try
            {
                int SELF    = 0;
                int WIFE    = 0;
                int SON     = 0;

                /*Self*/
                Dictionary<string, object> paramSelf = new Dictionary<string, object>();
                paramSelf.Add("OMI_P_NO", txt_W_PNO.Text);
                paramSelf.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCodeS;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCodeS = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC8_SELF", paramSelf);

                if (rsltCodeS.isSuccessful && rsltCodeS.dstResult.Tables.Count > 0 && rsltCodeS.dstResult.Tables[0].Rows.Count > 0)
                {
                    SELF = Convert.ToInt32(rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString());
                }


                /*WIFE*/
                Dictionary<string, object> paramWife = new Dictionary<string, object>();
                paramWife.Add("OMI_P_NO", txt_W_PNO.Text);
                paramWife.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCodeW;
                rsltCodeW = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC8_WIFE", paramWife);

                if (rsltCodeW.isSuccessful && rsltCodeW.dstResult.Tables.Count > 0 && rsltCodeW.dstResult.Tables[0].Rows.Count > 0)
                {
                    WIFE = Convert.ToInt32(rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramSon = new Dictionary<string, object>();
                paramSon.Add("OMI_P_NO", txt_W_PNO.Text);
                paramSon.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCodeSon;
                rsltCodeSon = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC8_SON", paramSon);

                if (rsltCodeSon.isSuccessful && rsltCodeSon.dstResult.Tables.Count > 0 && rsltCodeSon.dstResult.Tables[0].Rows.Count > 0)
                {
                    SON = Convert.ToInt32(rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString());
                }

                txt_OMI_Self.Text       = Convert.ToString((Convert.ToInt32(txt_W_Self.Text     == string.Empty ? "0" : txt_W_Self.Text )) + SELF);
                txt_OMI_Spouse.Text     = Convert.ToString((Convert.ToInt32(txt_W_Spouse.Text   == string.Empty ? "0" : txt_W_Spouse.Text)) + WIFE);
                txt_OMI_Child.Text      = Convert.ToString((Convert.ToInt32(txt_W_Child.Text    == string.Empty ? "0" : txt_W_Child.Text)) + SON);
                txt_OMI_Total_Claim.Text = Convert.ToString(SELF + WIFE + SON);

            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc8", exp);
            }
        }

        #endregion

        #region --Events--

        private void txt_W_Month_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int month = Convert.ToInt32(txt_W_Month.Text == string.Empty ? "0" : txt_W_Month.Text);
                if (month == 0 && month > 12)
                {
                    MessageBox.Show("ENTER A VALID MONTH OR   ...... F6=EXIT", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_Month_Validating", exp);
            }
        }

        private void txt_W_Year_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int year = Convert.ToInt32(txt_W_Year.Text == string.Empty ? "0" : txt_W_Year.Text);
                if (year == 0)
                {
                    MessageBox.Show("ENTER YEAR OR F6=EXIT", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    Proc1();

                    this.Close();
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_Year_Validating", exp);
            }
        }

        private void txt_W_PNO_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                string Enroll = string.Empty;

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_MONTH", txt_W_Month.Text);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PNo_Validate", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txt_W_Name.Text             = rsltCode.dstResult.Tables[0].Rows[0]["NAME"].ToString();
                    Enroll                      = rsltCode.dstResult.Tables[0].Rows[0]["PR_OPD"].ToString();
                    txt_W_Cat.Text              = rsltCode.dstResult.Tables[0].Rows[0]["PR_CATEGORY"].ToString();
                    txt_W_Joining_Date.Value    = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString());
                    txt_W_Status.Text           = rsltCode.dstResult.Tables[0].Rows[0]["PR_MARITAL"].ToString();
                    txt_W_Marriage_Date.Value   = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_CATEGORY"].ToString());

                    if (Enroll == "N")
                    {
                        MessageBox.Show(txt_W_PNO.Text + " IS NOT ENROLLED IN OMI", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
                else
                {
                    MessageBox.Show("INVALID NO. [F9]=HELP [F6]=EXIT", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_PNO_Validating", exp);
            }
        }

        #endregion
    }
}