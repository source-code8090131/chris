namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_PayEntry_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_PayEntry_Detail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlClaimDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVClaim = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.OMI_CLAIM_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Relation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaimAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecvAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wdiffernce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Yyear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mmonth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlClaimDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVClaim)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 298);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // PnlClaimDetail
            // 
            this.PnlClaimDetail.ConcurrentPanels = null;
            this.PnlClaimDetail.Controls.Add(this.DGVClaim);
            this.PnlClaimDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlClaimDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlClaimDetail.DependentPanels = null;
            this.PnlClaimDetail.DisableDependentLoad = false;
            this.PnlClaimDetail.EnableDelete = true;
            this.PnlClaimDetail.EnableInsert = true;
            this.PnlClaimDetail.EnableQuery = false;
            this.PnlClaimDetail.EnableUpdate = true;
            this.PnlClaimDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ClaimDetailCommand";
            this.PnlClaimDetail.Location = new System.Drawing.Point(15, 71);
            this.PnlClaimDetail.MasterPanel = null;
            this.PnlClaimDetail.Name = "PnlClaimDetail";
            this.PnlClaimDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlClaimDetail.Size = new System.Drawing.Size(617, 216);
            this.PnlClaimDetail.SPName = "CHRIS_SP_OMI_CLAIM_DETAIL_MANAGER";
            this.PnlClaimDetail.TabIndex = 12;
            // 
            // DGVClaim
            // 
            this.DGVClaim.AllowUserToAddRows = false;
            this.DGVClaim.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVClaim.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVClaim.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVClaim.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OMI_CLAIM_NO,
            this.Relation,
            this.ClaimAmount,
            this.RecvAmount,
            this.wdiffernce,
            this.Yyear,
            this.Mmonth,
            this.PNo});
            this.DGVClaim.ColumnToHide = null;
            this.DGVClaim.ColumnWidth = null;
            this.DGVClaim.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVClaim.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVClaim.DisplayColumnWrapper = null;
            this.DGVClaim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVClaim.GridDefaultRow = 0;
            this.DGVClaim.Location = new System.Drawing.Point(0, 0);
            this.DGVClaim.Name = "DGVClaim";
            this.DGVClaim.ReadOnlyColumns = null;
            this.DGVClaim.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVClaim.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVClaim.Size = new System.Drawing.Size(617, 216);
            this.DGVClaim.SkippingColumns = null;
            this.DGVClaim.TabIndex = 0;
            this.DGVClaim.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVClaim_CellValidating);
            // 
            // OMI_CLAIM_NO
            // 
            this.OMI_CLAIM_NO.DataPropertyName = "OMI_CLAIM_NO";
            this.OMI_CLAIM_NO.HeaderText = "Claim No.";
            this.OMI_CLAIM_NO.Name = "OMI_CLAIM_NO";
            this.OMI_CLAIM_NO.ReadOnly = true;
            // 
            // Relation
            // 
            this.Relation.DataPropertyName = "OMI_RELATION";
            this.Relation.HeaderText = "Relation";
            this.Relation.Name = "Relation";
            this.Relation.ReadOnly = true;
            // 
            // ClaimAmount
            // 
            this.ClaimAmount.DataPropertyName = "OMI_CLAIM_AMOUNT";
            this.ClaimAmount.HeaderText = "Claim Amount";
            this.ClaimAmount.Name = "ClaimAmount";
            this.ClaimAmount.ReadOnly = true;
            // 
            // RecvAmount
            // 
            this.RecvAmount.DataPropertyName = "OMI_RECEV_AMOUNT";
            this.RecvAmount.HeaderText = "Received Amount";
            this.RecvAmount.MaxInputLength = 10;
            this.RecvAmount.Name = "RecvAmount";
            // 
            // wdiffernce
            // 
            this.wdiffernce.DataPropertyName = "WDiff";
            this.wdiffernce.HeaderText = "WDiff";
            this.wdiffernce.Name = "wdiffernce";
            this.wdiffernce.ReadOnly = true;
            this.wdiffernce.Visible = false;
            // 
            // Yyear
            // 
            this.Yyear.DataPropertyName = "OMI_YEAR";
            this.Yyear.HeaderText = "OMI_YEAR";
            this.Yyear.Name = "Yyear";
            this.Yyear.Visible = false;
            // 
            // Mmonth
            // 
            this.Mmonth.DataPropertyName = "OMI_MONTH";
            this.Mmonth.HeaderText = "OMI_MONTH";
            this.Mmonth.Name = "Mmonth";
            this.Mmonth.Visible = false;
            // 
            // PNo
            // 
            this.PNo.DataPropertyName = "OMI_P_NO";
            this.PNo.HeaderText = "omipNo";
            this.PNo.Name = "PNo";
            this.PNo.Visible = false;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "ID";
            this.Id.HeaderText = "ID";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // CHRIS_MedicalInsurance_PayEntry_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 358);
            this.Controls.Add(this.PnlClaimDetail);
            this.Name = "CHRIS_MedicalInsurance_PayEntry_Detail";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "CHRIS_MedicalInsurance_PayEntry_Detail";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlClaimDetail, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlClaimDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVClaim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlClaimDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVClaim;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn OMI_CLAIM_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Relation;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaimAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecvAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn wdiffernce;
        private System.Windows.Forms.DataGridViewTextBoxColumn Yyear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mmonth;
        private System.Windows.Forms.DataGridViewTextBoxColumn PNo;
    }
}