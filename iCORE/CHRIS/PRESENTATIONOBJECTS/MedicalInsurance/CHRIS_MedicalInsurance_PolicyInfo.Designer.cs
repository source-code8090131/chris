namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_PolicyInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.pnlPolicyInfoMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Opt = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.pnlPolicyInfoMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 325);
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.pnlPolicyInfoMain);
            this.slPanelSimple1.Controls.Add(this.label7);
            this.slPanelSimple1.Controls.Add(this.label6);
            this.slPanelSimple1.Controls.Add(this.label5);
            this.slPanelSimple1.Controls.Add(this.label4);
            this.slPanelSimple1.Controls.Add(this.txtDate);
            this.slPanelSimple1.Controls.Add(this.label3);
            this.slPanelSimple1.Controls.Add(this.label2);
            this.slPanelSimple1.Controls.Add(this.label1);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(12, 87);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(610, 250);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 11;
            // 
            // pnlPolicyInfoMain
            // 
            this.pnlPolicyInfoMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPolicyInfoMain.ConcurrentPanels = null;
            this.pnlPolicyInfoMain.Controls.Add(this.slTextBox1);
            this.pnlPolicyInfoMain.Controls.Add(this.txt_W_Opt);
            this.pnlPolicyInfoMain.Controls.Add(this.label8);
            this.pnlPolicyInfoMain.DataManager = null;
            this.pnlPolicyInfoMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPolicyInfoMain.DependentPanels = null;
            this.pnlPolicyInfoMain.DisableDependentLoad = false;
            this.pnlPolicyInfoMain.EnableDelete = true;
            this.pnlPolicyInfoMain.EnableInsert = true;
            this.pnlPolicyInfoMain.EnableQuery = false;
            this.pnlPolicyInfoMain.EnableUpdate = true;
            this.pnlPolicyInfoMain.EntityName = null;
            this.pnlPolicyInfoMain.Location = new System.Drawing.Point(40, 198);
            this.pnlPolicyInfoMain.MasterPanel = null;
            this.pnlPolicyInfoMain.Name = "pnlPolicyInfoMain";
            this.pnlPolicyInfoMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPolicyInfoMain.Size = new System.Drawing.Size(546, 36);
            this.pnlPolicyInfoMain.SPName = null;
            this.pnlPolicyInfoMain.TabIndex = 8;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "W_Opt";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(517, 9);
            this.slTextBox1.MaxLength = 2;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(1, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 1;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Opt
            // 
            this.txt_W_Opt.AllowSpace = true;
            this.txt_W_Opt.AssociatedLookUpName = "";
            this.txt_W_Opt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Opt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Opt.ContinuationTextBox = null;
            this.txt_W_Opt.CustomEnabled = true;
            this.txt_W_Opt.DataFieldMapping = "W_Opt";
            this.txt_W_Opt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Opt.GetRecordsOnUpDownKeys = false;
            this.txt_W_Opt.IsDate = false;
            this.txt_W_Opt.Location = new System.Drawing.Point(401, 9);
            this.txt_W_Opt.MaxLength = 2;
            this.txt_W_Opt.Name = "txt_W_Opt";
            this.txt_W_Opt.NumberFormat = "###,###,##0.00";
            this.txt_W_Opt.Postfix = "";
            this.txt_W_Opt.Prefix = "";
            this.txt_W_Opt.Size = new System.Drawing.Size(35, 20);
            this.txt_W_Opt.SkipValidation = false;
            this.txt_W_Opt.TabIndex = 0;
            this.txt_W_Opt.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Opt.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_W_Opt_PreviewKeyDown);
            this.txt_W_Opt.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Opt_Validating);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(65, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(192, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Your Chioce ............................";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(105, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "00) Exit ..........................";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(247, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "03) Out Patient Medical";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(247, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "02) Group Hospitalization";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(247, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "01) Group Life Insurance";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(512, 4);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(95, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 3;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(463, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(230, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "POLICY INFORMATION";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(247, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SET UP ENTRY";
            // 
            // CHRIS_MedicalInsurance_PolicyInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 385);
            this.Controls.Add(this.slPanelSimple1);
            this.Name = "CHRIS_MedicalInsurance_PolicyInfo";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_PolicyInfo";
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.pnlPolicyInfoMain.ResumeLayout(false);
            this.pnlPolicyInfoMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPolicyInfoMain;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txt_W_Opt;
        private CrplControlLibrary.SLTextBox slTextBox1;
    }
}