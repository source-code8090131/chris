namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_OutPatMedi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_OutPatMedi));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Run = new CrplControlLibrary.SLButton();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.P_COPIES = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.P_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.P_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.P_CAT = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(410, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 100;
            this.pictureBox1.TabStop = false;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(218, 77);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(172, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 1;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(218, 104);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(172, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 2;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(74, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "DesType";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(74, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "DesName";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(132, 298);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 8;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(219, 298);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 9;
            this.slButton1.Text = "Close";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(53, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(362, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(53, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(362, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameters";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(74, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 89;
            this.label2.Text = "DesFormat";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(74, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 91;
            this.label4.Text = "Copies";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(218, 129);
            this.Dest_Format.MaxLength = 40;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(172, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 3;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // P_COPIES
            // 
            this.P_COPIES.AllowSpace = true;
            this.P_COPIES.AssociatedLookUpName = "";
            this.P_COPIES.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_COPIES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_COPIES.ContinuationTextBox = null;
            this.P_COPIES.CustomEnabled = true;
            this.P_COPIES.DataFieldMapping = "";
            this.P_COPIES.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_COPIES.GetRecordsOnUpDownKeys = false;
            this.P_COPIES.IsDate = false;
            this.P_COPIES.Location = new System.Drawing.Point(218, 155);
            this.P_COPIES.MaxLength = 2;
            this.P_COPIES.Name = "P_COPIES";
            this.P_COPIES.NumberFormat = "###,###,##0.00";
            this.P_COPIES.Postfix = "";
            this.P_COPIES.Prefix = "";
            this.P_COPIES.Size = new System.Drawing.Size(172, 20);
            this.P_COPIES.SkipValidation = false;
            this.P_COPIES.TabIndex = 4;
            this.P_COPIES.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(74, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 95;
            this.label5.Text = "Enter Branch Code ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // P_BRANCH
            // 
            this.P_BRANCH.AllowSpace = true;
            this.P_BRANCH.AssociatedLookUpName = "";
            this.P_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_BRANCH.ContinuationTextBox = null;
            this.P_BRANCH.CustomEnabled = true;
            this.P_BRANCH.DataFieldMapping = "";
            this.P_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_BRANCH.GetRecordsOnUpDownKeys = false;
            this.P_BRANCH.IsDate = false;
            this.P_BRANCH.Location = new System.Drawing.Point(218, 181);
            this.P_BRANCH.MaxLength = 3;
            this.P_BRANCH.Name = "P_BRANCH";
            this.P_BRANCH.NumberFormat = "###,###,##0.00";
            this.P_BRANCH.Postfix = "";
            this.P_BRANCH.Prefix = "";
            this.P_BRANCH.Size = new System.Drawing.Size(172, 20);
            this.P_BRANCH.SkipValidation = false;
            this.P_BRANCH.TabIndex = 5;
            this.P_BRANCH.TextType = CrplControlLibrary.TextType.AllCharacters;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(74, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 96;
            this.label6.Text = "Enter Segment  ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // P_SEG
            // 
            this.P_SEG.AllowSpace = true;
            this.P_SEG.AssociatedLookUpName = "";
            this.P_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_SEG.ContinuationTextBox = null;
            this.P_SEG.CustomEnabled = true;
            this.P_SEG.DataFieldMapping = "";
            this.P_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_SEG.GetRecordsOnUpDownKeys = false;
            this.P_SEG.IsDate = false;
            this.P_SEG.Location = new System.Drawing.Point(218, 207);
            this.P_SEG.MaxLength = 3;
            this.P_SEG.Name = "P_SEG";
            this.P_SEG.NumberFormat = "###,###,##0.00";
            this.P_SEG.Postfix = "";
            this.P_SEG.Prefix = "";
            this.P_SEG.Size = new System.Drawing.Size(172, 20);
            this.P_SEG.SkipValidation = false;
            this.P_SEG.TabIndex = 6;
            this.P_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(74, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 13);
            this.label7.TabIndex = 97;
            this.label7.Text = "[C]=Clerk\'s [O]=Officers";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // P_CAT
            // 
            this.P_CAT.AllowSpace = true;
            this.P_CAT.AssociatedLookUpName = "";
            this.P_CAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_CAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_CAT.ContinuationTextBox = null;
            this.P_CAT.CustomEnabled = true;
            this.P_CAT.DataFieldMapping = "";
            this.P_CAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_CAT.GetRecordsOnUpDownKeys = false;
            this.P_CAT.IsDate = false;
            this.P_CAT.Location = new System.Drawing.Point(218, 233);
            this.P_CAT.MaxLength = 1;
            this.P_CAT.Name = "P_CAT";
            this.P_CAT.NumberFormat = "###,###,##0.00";
            this.P_CAT.Postfix = "";
            this.P_CAT.Prefix = "";
            this.P_CAT.Size = new System.Drawing.Size(172, 20);
            this.P_CAT.SkipValidation = false;
            this.P_CAT.TabIndex = 7;
            this.P_CAT.TextType = CrplControlLibrary.TextType.String;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.P_CAT);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.P_SEG);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.P_BRANCH);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.P_COPIES);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(458, 337);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // CHRIS_MedicalInsurance_OutPatMedi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 385);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_MedicalInsurance_OutPatMedi";
            this.Text = "CHRIS_MedicalInsurance_OutPatMedi";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLButton Run;
        private CrplControlLibrary.SLButton slButton1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private CrplControlLibrary.SLTextBox P_COPIES;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox P_BRANCH;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox P_SEG;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox P_CAT;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}