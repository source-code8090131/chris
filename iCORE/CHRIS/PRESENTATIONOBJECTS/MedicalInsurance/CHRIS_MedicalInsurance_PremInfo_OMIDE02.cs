using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PremInfo_OMIDE02 : ChrisSimpleForm
    {
        #region --Variable--
        bool validateForm = true;
        bool SaveCall     = false;
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_PremInfo_OMIDE02()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_PremInfo_OMIDE02(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region --Override Method--

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                base.IterateFormToEnableControls(pnlLifeInso.Controls, true);
                this.CurrentPanelBlock          = pnlLifeInso.Name;
                this.lblUserName.Text           = this.userID;
                this.txtOption.Visible          = false;
                this.txtDate.Text               = this.Now().ToString("dd/MM/yyyy");
                this.FunctionConfig.EnableF10   = true;
                this.FunctionConfig.F10         = Function.Save;
                tbtAdd.Visible                  = false;
                this.lbtnPolicyNo.SkipValidationOnLeave = false;
                txt_W_Policy.Select();
                txt_W_Policy.Focus();
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (txtID.Text != string.Empty)
            {
                base.m_intPKID      = Convert.ToInt32(txtID.Text);
                this.operationMode  = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            }
            if (txt_Pre_Premium_Amt.Text != string.Empty)
            {
                int indx    = 0;
                string Amnt = txt_Pre_Premium_Amt.Text;
                int length  = txt_Pre_Premium_Amt.Text.Length;

                if (txt_Pre_Premium_Amt.Text.Contains("."))
                {
                    indx = Amnt.IndexOf('.');
                }

                if ((length - (indx + 1)) > 2)
                {
                    MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    validateForm = false;
                    return;
                }
                else if ((indx - 1) >= 9)
                {
                    MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    validateForm = false;
                    return;
                }
            }
            if (actionType == "Save" && validateForm == true)
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                base.Cancel();
                txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                txt_W_Policy.Select();
                txt_W_Policy.Focus();
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override bool Save()
        {
            try
            {
                SaveCall = true;

                if (txt_Pre_Premium_Amt.Text != string.Empty)
                {
                    int indx    = 0;
                    string Amnt = txt_Pre_Premium_Amt.Text;
                    int length = txt_Pre_Premium_Amt.Text.Length;
                    bool decValue = false;
                    if (txt_Pre_Premium_Amt.Text.Contains("."))
                    {
                        indx = Amnt.IndexOf('.');
                        decValue = true;
                    }

                    if ((length - (indx + 1)) > 2 && decValue)
                    {
                        MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        validateForm = false;
                        return false;
                    }
                    else if ((indx - 1) >= 9 && decValue)
                    {
                        MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        validateForm = false;
                        return false;
                    }
                    else if (txt_Pre_Premium_Amt.Text.Length > 9 && !decValue)
                    {
                        MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        validateForm = false;
                        return false;
                    }
                }

                if (FindForm().Validate())
                {
                    if (txt_W_Policy.Text != string.Empty && txt_W_Type.Text != string.Empty && dtp_Pre_Payment_Date.Value != null && validateForm == true)
                    {
                        base.Save();
                    }
                }
                validateForm = true;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "Save()", exp);
            }
            return false;
        }

        #endregion

        #region --Events--
        
        private void txt_W_Type_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_Type.Text == string.Empty || (txt_W_Type.Text != "C" && txt_W_Type.Text != "O"))
            {
                MessageBox.Show("[C] FOR CLERICAL OR [O] FOR OFFICER ... [F6]=EXIT", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                validateForm = false;
                e.Cancel = true;
            }
            else if (txt_W_Type.Text == "C")
            {
                txt_W_Type_Desc.Text = "LERICAL";
            }
            else if (txt_W_Type.Text == "O")
            {
                txt_W_Type_Desc.Text = "FFICER";
            }
        }

        private void dtp_Pre_Payment_Date_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (dtp_Pre_Payment_Date.Value == null)
                {
                    MessageBox.Show("ENTER PAYMENT DATE ..........[F6]=EXIT", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    e.Cancel = true;
                }
                else
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PRE_POLICY", txt_W_Policy.Text);
                    param.Add("PRE_PAYMENT_DATE", dtp_Pre_Payment_Date.Value.ToString());

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "PAYMENT_DATE_MANUAL_FILL", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_Pre_Premium_Amt.Text    = rsltCode.dstResult.Tables[0].Rows[0]["PRE_PREMIUM_AMT"].ToString();
                        txt_Pre_Bill_No.Text        = rsltCode.dstResult.Tables[0].Rows[0]["PRE_BILL_NO"].ToString();
                        txt_Pre_Mc_No.Text          = rsltCode.dstResult.Tables[0].Rows[0]["PRE_MC_NO"].ToString();
                        txt_Pre_Subject.Text        = rsltCode.dstResult.Tables[0].Rows[0]["PRE_SUBJECT"].ToString();
                        txtID.Text                  = rsltCode.dstResult.Tables[0].Rows[0]["ID"].ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name ,"dtp_Pre_Payment_Date_Validating", exp);
            }

        }

        private void txt_Pre_Premium_Amt_Validating(object sender, CancelEventArgs e)
        {
            int indx        = 0;
            string Amnt     = txt_Pre_Premium_Amt.Text;
            int length      = txt_Pre_Premium_Amt.Text.Length;
            bool decValue = false;
            if (txt_Pre_Premium_Amt.Text.Contains("."))
            {
                indx = Amnt.IndexOf('.');
                decValue = true;
            }

            if ((length - (indx + 1)) > 2 && decValue)
            {
                MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                validateForm    = false;
                e.Cancel        = true;
                return;
            }
            else if ((indx - 1) >= 9 && decValue)
            {
                MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                validateForm = false;
                e.Cancel = true;
                return;
            }
            else if (txt_Pre_Premium_Amt.Text.Length > 9 && !decValue)
            {
                MessageBox.Show("Field must be of form 999999999.99.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                validateForm = false;
                e.Cancel = true;
                return;
            }



            if (txt_Pre_Premium_Amt.Text == string.Empty && !SaveCall)
            {
                MessageBox.Show("ENTER PREMIUM AMOUNT  [F6]=EXIT ", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                validateForm = false;
                e.Cancel = true;
            }

            SaveCall = false;
        }

        private void txt_Pre_Bill_No_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pre_Bill_No.Text == string.Empty)
            {
                MessageBox.Show("ENTER BILL NO.   [F6]=EXIT", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                validateForm    = false;
                e.Cancel        = true;
            }
        }

        private void txt_Pre_Subject_Validating(object sender, CancelEventArgs e)
        {
            if (!SaveCall)
            {
                MessageBox.Show("[F10]=SAVE  [F6]=EXIT W/O SAVE      ", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            SaveCall = false;
        }

        private void CHRIS_MedicalInsurance_PremInfo_OMIDE02_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (lbtnPaymentDate.Focused)
            {
                txt_Pre_Premium_Amt.Select();
                txt_Pre_Premium_Amt.Focus();
            }
        }

        #endregion
    }
}