using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PremInfo_GHIDE02 : ChrisSimpleForm
    {
        #region --Constructor--
        public CHRIS_MedicalInsurance_PremInfo_GHIDE02()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_PremInfo_GHIDE02(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region --Method--

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                base.IterateFormToEnableControls(pnlPremInfo.Controls, true);
                this.CurrentPanelBlock          = pnlPremInfo.Name;
                this.lblUserName.Text           = this.userID;
                this.txtOption.Visible          = false;
                this.txtDate.Text               = this.Now().ToString("dd/MM/yyyy");
                this.FunctionConfig.EnableF10   = true;
                this.FunctionConfig.F10         = Function.Save;
                tbtAdd.Visible                  = false;
                this.lbtnPolicyNo.SkipValidationOnLeave = false;
                txt_W_Policy.Select();
                txt_W_Policy.Focus();
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (txtID.Text != string.Empty)
            {
                base.m_intPKID      = Convert.ToInt32(txtID.Text);
                this.operationMode  = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Save")
            {
                base.Cancel();
                txtDate.Text        = this.Now().ToString("dd/MM/yyyy");
                txt_W_Policy.Select();
                txt_W_Policy.Focus();
            }
        }
        
        #endregion

        private void txt_W_Policy_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_Type.Text == string.Empty || (txt_W_Type.Text != "C" && txt_W_Type.Text != "O"))
            {
                MessageBox.Show("[C] FOR CLERICAL OR [O] FOR OFFICER ... [F6]=EXIT", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_W_Type.Text == "C")
            {
                txt_W_Type_Desc.Text = "LERICAL";
            }
            else if (txt_W_Type.Text == "O")
            {
                txt_W_Type_Desc.Text = "FFICER";
            }
        }

        private void dtp_Pre_Payment_Date_Validating(object sender, CancelEventArgs e)
        {
            if (dtp_Pre_Payment_Date.Value == null)
            {
                MessageBox.Show("ENTER PAYMENT DATE ..........ABORT BUTTON=EXIT", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PRE_POLICY", txt_W_Policy.Text);
                param.Add("PRE_PAYMENT_DATE", dtp_Pre_Payment_Date.Value.ToString());

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "PAYMENT_DATE_MANUAL_FILL", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txt_Pre_Premium_Amt.Text    = rsltCode.dstResult.Tables[0].Rows[0]["PRE_PREMIUM_AMT"].ToString();
                    txt_Pre_Bill_No.Text        = rsltCode.dstResult.Tables[0].Rows[0]["PRE_BILL_NO"].ToString();
                    txt_Pre_Mc_No.Text          = rsltCode.dstResult.Tables[0].Rows[0]["PRE_MC_NO"].ToString();
                    txt_Pre_Subject.Text        = rsltCode.dstResult.Tables[0].Rows[0]["PRE_SUBJECT"].ToString();
                    txtID.Text                  = rsltCode.dstResult.Tables[0].Rows[0]["ID"].ToString();
                }
            }
        }

        private void txt_Pre_Premium_Amt_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pre_Premium_Amt.Text == string.Empty)
            {
                MessageBox.Show("ENTER PREMIUM AMOUNT  [F6]=EXIT ", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txt_Pre_Bill_No_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pre_Bill_No.Text == string.Empty)
            {
                MessageBox.Show("ENTER BILL NO.   ABORT BUTTON=EXIT", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txt_Pre_Subject_Validating(object sender, CancelEventArgs e)
        {
            MessageBox.Show("[F10]=SAVE  [F6]=EXIT W/O SAVE      ", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
            e.Cancel = true;
        }

        private void CHRIS_MedicalInsurance_PremInfo_GHIDE02_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (lbtnPaymentDate.Focused)
            {
                txt_Pre_Premium_Amt.Select();
                txt_Pre_Premium_Amt.Focus();
            }
        }
    }
}