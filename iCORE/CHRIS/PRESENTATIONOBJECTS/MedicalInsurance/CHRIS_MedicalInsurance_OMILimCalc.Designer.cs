namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_OMILimCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlProcess = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_Child_DOB = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_W_Joining_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_W_Marriage_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_W_OS_Balance = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Status = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Cat = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txt_OMI_Total_Claim = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OMI_Limit = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txt_OMI_Child = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_Spouse = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_Self = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txt_W_Year = new CrplControlLibrary.SLTextBox(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_W_Child = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Spouse = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Self = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_PNO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Month = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlProcess.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(546, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(582, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 466);
            this.panel1.Size = new System.Drawing.Size(582, 60);
            // 
            // pnlProcess
            // 
            this.pnlProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProcess.ConcurrentPanels = null;
            this.pnlProcess.Controls.Add(this.txt_W_Child_DOB);
            this.pnlProcess.Controls.Add(this.txt_W_Joining_Date);
            this.pnlProcess.Controls.Add(this.txt_W_Marriage_Date);
            this.pnlProcess.Controls.Add(this.txt_W_OS_Balance);
            this.pnlProcess.Controls.Add(this.txt_W_Status);
            this.pnlProcess.Controls.Add(this.txt_W_Cat);
            this.pnlProcess.Controls.Add(this.txt_W_Name);
            this.pnlProcess.Controls.Add(this.label30);
            this.pnlProcess.Controls.Add(this.label31);
            this.pnlProcess.Controls.Add(this.label32);
            this.pnlProcess.Controls.Add(this.label33);
            this.pnlProcess.Controls.Add(this.label34);
            this.pnlProcess.Controls.Add(this.txt_OMI_Total_Claim);
            this.pnlProcess.Controls.Add(this.txt_W_OMI_Limit);
            this.pnlProcess.Controls.Add(this.label12);
            this.pnlProcess.Controls.Add(this.label24);
            this.pnlProcess.Controls.Add(this.label25);
            this.pnlProcess.Controls.Add(this.label26);
            this.pnlProcess.Controls.Add(this.label27);
            this.pnlProcess.Controls.Add(this.label28);
            this.pnlProcess.Controls.Add(this.label29);
            this.pnlProcess.Controls.Add(this.txt_OMI_Child);
            this.pnlProcess.Controls.Add(this.txt_OMI_Spouse);
            this.pnlProcess.Controls.Add(this.txt_OMI_Self);
            this.pnlProcess.Controls.Add(this.label23);
            this.pnlProcess.Controls.Add(this.label22);
            this.pnlProcess.Controls.Add(this.label21);
            this.pnlProcess.Controls.Add(this.txt_W_Year);
            this.pnlProcess.Controls.Add(this.label20);
            this.pnlProcess.Controls.Add(this.label19);
            this.pnlProcess.Controls.Add(this.label18);
            this.pnlProcess.Controls.Add(this.label17);
            this.pnlProcess.Controls.Add(this.label16);
            this.pnlProcess.Controls.Add(this.label15);
            this.pnlProcess.Controls.Add(this.label14);
            this.pnlProcess.Controls.Add(this.label11);
            this.pnlProcess.Controls.Add(this.label10);
            this.pnlProcess.Controls.Add(this.label9);
            this.pnlProcess.Controls.Add(this.label8);
            this.pnlProcess.Controls.Add(this.label7);
            this.pnlProcess.Controls.Add(this.label6);
            this.pnlProcess.Controls.Add(this.txt_W_Child);
            this.pnlProcess.Controls.Add(this.txt_W_Spouse);
            this.pnlProcess.Controls.Add(this.txt_W_Self);
            this.pnlProcess.Controls.Add(this.txt_W_PNO);
            this.pnlProcess.Controls.Add(this.txt_W_Month);
            this.pnlProcess.Controls.Add(this.label13);
            this.pnlProcess.Controls.Add(this.txtDate);
            this.pnlProcess.Controls.Add(this.txtUser);
            this.pnlProcess.Controls.Add(this.label5);
            this.pnlProcess.Controls.Add(this.txtLocation);
            this.pnlProcess.Controls.Add(this.label4);
            this.pnlProcess.Controls.Add(this.label1);
            this.pnlProcess.Controls.Add(this.label3);
            this.pnlProcess.Controls.Add(this.label2);
            this.pnlProcess.DataManager = null;
            this.pnlProcess.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlProcess.DependentPanels = null;
            this.pnlProcess.DisableDependentLoad = false;
            this.pnlProcess.EnableDelete = true;
            this.pnlProcess.EnableInsert = true;
            this.pnlProcess.EnableQuery = false;
            this.pnlProcess.EnableUpdate = true;
            this.pnlProcess.EntityName = null;
            this.pnlProcess.Location = new System.Drawing.Point(12, 85);
            this.pnlProcess.MasterPanel = null;
            this.pnlProcess.Name = "pnlProcess";
            this.pnlProcess.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlProcess.Size = new System.Drawing.Size(559, 407);
            this.pnlProcess.SPName = null;
            this.pnlProcess.TabIndex = 9;
            // 
            // txt_W_Child_DOB
            // 
            this.txt_W_Child_DOB.CustomEnabled = true;
            this.txt_W_Child_DOB.CustomFormat = "dd/MM/yyyy";
            this.txt_W_Child_DOB.DataFieldMapping = "W_Child_DOB";
            this.txt_W_Child_DOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_W_Child_DOB.HasChanges = true;
            this.txt_W_Child_DOB.Location = new System.Drawing.Point(452, 266);
            this.txt_W_Child_DOB.Name = "txt_W_Child_DOB";
            this.txt_W_Child_DOB.NullValue = " ";
            this.txt_W_Child_DOB.Size = new System.Drawing.Size(83, 20);
            this.txt_W_Child_DOB.TabIndex = 115;
            this.txt_W_Child_DOB.Value = new System.DateTime(2011, 6, 14, 0, 0, 0, 0);
            this.txt_W_Child_DOB.Visible = false;
            // 
            // txt_W_Joining_Date
            // 
            this.txt_W_Joining_Date.CustomEnabled = true;
            this.txt_W_Joining_Date.CustomFormat = "dd/MM/yyyy";
            this.txt_W_Joining_Date.DataFieldMapping = "W_Joining_Date";
            this.txt_W_Joining_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_W_Joining_Date.HasChanges = true;
            this.txt_W_Joining_Date.Location = new System.Drawing.Point(452, 246);
            this.txt_W_Joining_Date.Name = "txt_W_Joining_Date";
            this.txt_W_Joining_Date.NullValue = " ";
            this.txt_W_Joining_Date.Size = new System.Drawing.Size(83, 20);
            this.txt_W_Joining_Date.TabIndex = 114;
            this.txt_W_Joining_Date.Value = new System.DateTime(2011, 6, 14, 0, 0, 0, 0);
            this.txt_W_Joining_Date.Visible = false;
            // 
            // txt_W_Marriage_Date
            // 
            this.txt_W_Marriage_Date.CustomEnabled = true;
            this.txt_W_Marriage_Date.CustomFormat = "dd/MM/yyyy";
            this.txt_W_Marriage_Date.DataFieldMapping = "W_Marriage_Date";
            this.txt_W_Marriage_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_W_Marriage_Date.HasChanges = true;
            this.txt_W_Marriage_Date.Location = new System.Drawing.Point(452, 226);
            this.txt_W_Marriage_Date.Name = "txt_W_Marriage_Date";
            this.txt_W_Marriage_Date.NullValue = " ";
            this.txt_W_Marriage_Date.Size = new System.Drawing.Size(83, 20);
            this.txt_W_Marriage_Date.TabIndex = 113;
            this.txt_W_Marriage_Date.Value = new System.DateTime(2011, 6, 14, 0, 0, 0, 0);
            this.txt_W_Marriage_Date.Visible = false;
            // 
            // txt_W_OS_Balance
            // 
            this.txt_W_OS_Balance.AllowSpace = true;
            this.txt_W_OS_Balance.AssociatedLookUpName = "";
            this.txt_W_OS_Balance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OS_Balance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OS_Balance.ContinuationTextBox = null;
            this.txt_W_OS_Balance.CustomEnabled = true;
            this.txt_W_OS_Balance.DataFieldMapping = "W_OS_Balance";
            this.txt_W_OS_Balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OS_Balance.GetRecordsOnUpDownKeys = false;
            this.txt_W_OS_Balance.IsDate = false;
            this.txt_W_OS_Balance.IsRequired = true;
            this.txt_W_OS_Balance.Location = new System.Drawing.Point(452, 186);
            this.txt_W_OS_Balance.MaxLength = 25;
            this.txt_W_OS_Balance.Name = "txt_W_OS_Balance";
            this.txt_W_OS_Balance.NumberFormat = "###,###,##0.00";
            this.txt_W_OS_Balance.Postfix = "";
            this.txt_W_OS_Balance.Prefix = "";
            this.txt_W_OS_Balance.Size = new System.Drawing.Size(83, 20);
            this.txt_W_OS_Balance.SkipValidation = false;
            this.txt_W_OS_Balance.TabIndex = 112;
            this.txt_W_OS_Balance.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_OS_Balance.Visible = false;
            // 
            // txt_W_Status
            // 
            this.txt_W_Status.AllowSpace = true;
            this.txt_W_Status.AssociatedLookUpName = "";
            this.txt_W_Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Status.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Status.ContinuationTextBox = null;
            this.txt_W_Status.CustomEnabled = true;
            this.txt_W_Status.DataFieldMapping = "W_Status";
            this.txt_W_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Status.GetRecordsOnUpDownKeys = false;
            this.txt_W_Status.IsDate = false;
            this.txt_W_Status.IsRequired = true;
            this.txt_W_Status.Location = new System.Drawing.Point(452, 286);
            this.txt_W_Status.MaxLength = 25;
            this.txt_W_Status.Name = "txt_W_Status";
            this.txt_W_Status.NumberFormat = "###,###,##0.00";
            this.txt_W_Status.Postfix = "";
            this.txt_W_Status.Prefix = "";
            this.txt_W_Status.Size = new System.Drawing.Size(83, 20);
            this.txt_W_Status.SkipValidation = false;
            this.txt_W_Status.TabIndex = 111;
            this.txt_W_Status.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Status.Visible = false;
            // 
            // txt_W_Cat
            // 
            this.txt_W_Cat.AllowSpace = true;
            this.txt_W_Cat.AssociatedLookUpName = "";
            this.txt_W_Cat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Cat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Cat.ContinuationTextBox = null;
            this.txt_W_Cat.CustomEnabled = true;
            this.txt_W_Cat.DataFieldMapping = "W_Cat";
            this.txt_W_Cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Cat.GetRecordsOnUpDownKeys = false;
            this.txt_W_Cat.IsDate = false;
            this.txt_W_Cat.IsRequired = true;
            this.txt_W_Cat.Location = new System.Drawing.Point(452, 206);
            this.txt_W_Cat.MaxLength = 25;
            this.txt_W_Cat.Name = "txt_W_Cat";
            this.txt_W_Cat.NumberFormat = "###,###,##0.00";
            this.txt_W_Cat.Postfix = "";
            this.txt_W_Cat.Prefix = "";
            this.txt_W_Cat.Size = new System.Drawing.Size(83, 20);
            this.txt_W_Cat.SkipValidation = false;
            this.txt_W_Cat.TabIndex = 107;
            this.txt_W_Cat.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Cat.Visible = false;
            // 
            // txt_W_Name
            // 
            this.txt_W_Name.AllowSpace = true;
            this.txt_W_Name.AssociatedLookUpName = "";
            this.txt_W_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Name.ContinuationTextBox = null;
            this.txt_W_Name.CustomEnabled = true;
            this.txt_W_Name.DataFieldMapping = "W_Name";
            this.txt_W_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Name.GetRecordsOnUpDownKeys = false;
            this.txt_W_Name.IsDate = false;
            this.txt_W_Name.Location = new System.Drawing.Point(203, 140);
            this.txt_W_Name.MaxLength = 1;
            this.txt_W_Name.Name = "txt_W_Name";
            this.txt_W_Name.NumberFormat = "###,###,##0.00";
            this.txt_W_Name.Postfix = "";
            this.txt_W_Name.Prefix = "";
            this.txt_W_Name.ReadOnly = true;
            this.txt_W_Name.Size = new System.Drawing.Size(248, 20);
            this.txt_W_Name.SkipValidation = false;
            this.txt_W_Name.TabIndex = 3;
            this.txt_W_Name.TabStop = false;
            this.txt_W_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(186, 382);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(11, 13);
            this.label30.TabIndex = 105;
            this.label30.Text = ":";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(186, 381);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(11, 13);
            this.label31.TabIndex = 104;
            this.label31.Text = ":";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(186, 357);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(11, 13);
            this.label32.TabIndex = 103;
            this.label32.Text = ":";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(68, 381);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(109, 13);
            this.label33.TabIndex = 102;
            this.label33.Text = "Total Amt Claimed";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(84, 357);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(93, 13);
            this.label34.TabIndex = 101;
            this.label34.Text = "Total OMI Limit";
            // 
            // txt_OMI_Total_Claim
            // 
            this.txt_OMI_Total_Claim.AllowSpace = true;
            this.txt_OMI_Total_Claim.AssociatedLookUpName = "";
            this.txt_OMI_Total_Claim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_Total_Claim.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_Total_Claim.ContinuationTextBox = null;
            this.txt_OMI_Total_Claim.CustomEnabled = true;
            this.txt_OMI_Total_Claim.DataFieldMapping = "OMI_Total_Claim";
            this.txt_OMI_Total_Claim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_Total_Claim.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_Total_Claim.IsDate = false;
            this.txt_OMI_Total_Claim.Location = new System.Drawing.Point(203, 377);
            this.txt_OMI_Total_Claim.MaxLength = 25;
            this.txt_OMI_Total_Claim.Name = "txt_OMI_Total_Claim";
            this.txt_OMI_Total_Claim.NumberFormat = "###,###,##0.00";
            this.txt_OMI_Total_Claim.Postfix = "";
            this.txt_OMI_Total_Claim.Prefix = "";
            this.txt_OMI_Total_Claim.ReadOnly = true;
            this.txt_OMI_Total_Claim.Size = new System.Drawing.Size(117, 20);
            this.txt_OMI_Total_Claim.SkipValidation = false;
            this.txt_OMI_Total_Claim.TabIndex = 11;
            this.txt_OMI_Total_Claim.TabStop = false;
            this.txt_OMI_Total_Claim.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OMI_Limit
            // 
            this.txt_W_OMI_Limit.AllowSpace = true;
            this.txt_W_OMI_Limit.AssociatedLookUpName = "";
            this.txt_W_OMI_Limit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OMI_Limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OMI_Limit.ContinuationTextBox = null;
            this.txt_W_OMI_Limit.CustomEnabled = true;
            this.txt_W_OMI_Limit.DataFieldMapping = "W_OMI_Limit";
            this.txt_W_OMI_Limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OMI_Limit.GetRecordsOnUpDownKeys = false;
            this.txt_W_OMI_Limit.IsDate = false;
            this.txt_W_OMI_Limit.Location = new System.Drawing.Point(203, 353);
            this.txt_W_OMI_Limit.MaxLength = 25;
            this.txt_W_OMI_Limit.Name = "txt_W_OMI_Limit";
            this.txt_W_OMI_Limit.NumberFormat = "###,###,##0.00";
            this.txt_W_OMI_Limit.Postfix = "";
            this.txt_W_OMI_Limit.Prefix = "";
            this.txt_W_OMI_Limit.Size = new System.Drawing.Size(117, 20);
            this.txt_W_OMI_Limit.SkipValidation = false;
            this.txt_W_OMI_Limit.TabIndex = 2;
            this.txt_W_OMI_Limit.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(186, 332);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 13);
            this.label12.TabIndex = 98;
            this.label12.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(186, 331);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 97;
            this.label24.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(186, 307);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(11, 13);
            this.label25.TabIndex = 96;
            this.label25.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(186, 283);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(11, 13);
            this.label26.TabIndex = 95;
            this.label26.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(124, 331);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 94;
            this.label27.Text = "Childern";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(128, 307);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 13);
            this.label28.TabIndex = 93;
            this.label28.Text = "Spouse";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(144, 283);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(33, 13);
            this.label29.TabIndex = 92;
            this.label29.Text = "Self ";
            // 
            // txt_OMI_Child
            // 
            this.txt_OMI_Child.AllowSpace = true;
            this.txt_OMI_Child.AssociatedLookUpName = "";
            this.txt_OMI_Child.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_Child.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_Child.ContinuationTextBox = null;
            this.txt_OMI_Child.CustomEnabled = true;
            this.txt_OMI_Child.DataFieldMapping = "OMI_Child";
            this.txt_OMI_Child.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_Child.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_Child.IsDate = false;
            this.txt_OMI_Child.Location = new System.Drawing.Point(203, 327);
            this.txt_OMI_Child.MaxLength = 25;
            this.txt_OMI_Child.Name = "txt_OMI_Child";
            this.txt_OMI_Child.NumberFormat = "###,###,##0.00";
            this.txt_OMI_Child.Postfix = "";
            this.txt_OMI_Child.Prefix = "";
            this.txt_OMI_Child.ReadOnly = true;
            this.txt_OMI_Child.Size = new System.Drawing.Size(59, 20);
            this.txt_OMI_Child.SkipValidation = false;
            this.txt_OMI_Child.TabIndex = 9;
            this.txt_OMI_Child.TabStop = false;
            this.txt_OMI_Child.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_Spouse
            // 
            this.txt_OMI_Spouse.AllowSpace = true;
            this.txt_OMI_Spouse.AssociatedLookUpName = "";
            this.txt_OMI_Spouse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_Spouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_Spouse.ContinuationTextBox = null;
            this.txt_OMI_Spouse.CustomEnabled = true;
            this.txt_OMI_Spouse.DataFieldMapping = "OMI_Spouse";
            this.txt_OMI_Spouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_Spouse.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_Spouse.IsDate = false;
            this.txt_OMI_Spouse.Location = new System.Drawing.Point(203, 303);
            this.txt_OMI_Spouse.MaxLength = 25;
            this.txt_OMI_Spouse.Name = "txt_OMI_Spouse";
            this.txt_OMI_Spouse.NumberFormat = "###,###,##0.00";
            this.txt_OMI_Spouse.Postfix = "";
            this.txt_OMI_Spouse.Prefix = "";
            this.txt_OMI_Spouse.ReadOnly = true;
            this.txt_OMI_Spouse.Size = new System.Drawing.Size(59, 20);
            this.txt_OMI_Spouse.SkipValidation = false;
            this.txt_OMI_Spouse.TabIndex = 8;
            this.txt_OMI_Spouse.TabStop = false;
            this.txt_OMI_Spouse.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_Self
            // 
            this.txt_OMI_Self.AllowSpace = true;
            this.txt_OMI_Self.AssociatedLookUpName = "";
            this.txt_OMI_Self.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_Self.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_Self.ContinuationTextBox = null;
            this.txt_OMI_Self.CustomEnabled = true;
            this.txt_OMI_Self.DataFieldMapping = "OMI_Self";
            this.txt_OMI_Self.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_Self.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_Self.IsDate = false;
            this.txt_OMI_Self.Location = new System.Drawing.Point(203, 279);
            this.txt_OMI_Self.MaxLength = 12;
            this.txt_OMI_Self.Name = "txt_OMI_Self";
            this.txt_OMI_Self.NumberFormat = "###,###,##0.00";
            this.txt_OMI_Self.Postfix = "";
            this.txt_OMI_Self.Prefix = "";
            this.txt_OMI_Self.ReadOnly = true;
            this.txt_OMI_Self.Size = new System.Drawing.Size(59, 20);
            this.txt_OMI_Self.SkipValidation = false;
            this.txt_OMI_Self.TabIndex = 7;
            this.txt_OMI_Self.TabStop = false;
            this.txt_OMI_Self.TextType = CrplControlLibrary.TextType.String;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(57, 259);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 13);
            this.label23.TabIndex = 88;
            this.label23.Text = "Blance";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(43, 171);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 87;
            this.label22.Text = "OMI Limit";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(235, 82);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "/";
            // 
            // txt_W_Year
            // 
            this.txt_W_Year.AllowSpace = true;
            this.txt_W_Year.AssociatedLookUpName = "";
            this.txt_W_Year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Year.ContinuationTextBox = null;
            this.txt_W_Year.CustomEnabled = true;
            this.txt_W_Year.DataFieldMapping = "W_Year";
            this.txt_W_Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Year.GetRecordsOnUpDownKeys = false;
            this.txt_W_Year.IsDate = false;
            this.txt_W_Year.IsRequired = true;
            this.txt_W_Year.Location = new System.Drawing.Point(253, 79);
            this.txt_W_Year.MaxLength = 25;
            this.txt_W_Year.Name = "txt_W_Year";
            this.txt_W_Year.NumberFormat = "###,###,##0.00";
            this.txt_W_Year.Postfix = "";
            this.txt_W_Year.Prefix = "";
            this.txt_W_Year.Size = new System.Drawing.Size(47, 20);
            this.txt_W_Year.SkipValidation = false;
            this.txt_W_Year.TabIndex = 1;
            this.txt_W_Year.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Year.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Year_Validating);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(186, 120);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 13);
            this.label20.TabIndex = 84;
            this.label20.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(186, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = ":";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(186, 235);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 13);
            this.label18.TabIndex = 82;
            this.label18.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(186, 234);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 13);
            this.label17.TabIndex = 81;
            this.label17.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(186, 210);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 13);
            this.label16.TabIndex = 80;
            this.label16.Text = ":";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(186, 186);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 13);
            this.label15.TabIndex = 79;
            this.label15.Text = ":";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(186, 143);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(11, 13);
            this.label14.TabIndex = 78;
            this.label14.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(124, 234);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 76;
            this.label11.Text = "Childern";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(128, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 75;
            this.label10.Text = "Spouse";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(144, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 74;
            this.label9.Text = "Self ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(138, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 73;
            this.label8.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(90, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 72;
            this.label7.Text = "Personnel No.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(61, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 71;
            this.label6.Text = "Enter Month / Year";
            // 
            // txt_W_Child
            // 
            this.txt_W_Child.AllowSpace = true;
            this.txt_W_Child.AssociatedLookUpName = "";
            this.txt_W_Child.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Child.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Child.ContinuationTextBox = null;
            this.txt_W_Child.CustomEnabled = true;
            this.txt_W_Child.DataFieldMapping = "W_Child";
            this.txt_W_Child.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Child.GetRecordsOnUpDownKeys = false;
            this.txt_W_Child.IsDate = false;
            this.txt_W_Child.Location = new System.Drawing.Point(203, 230);
            this.txt_W_Child.MaxLength = 25;
            this.txt_W_Child.Name = "txt_W_Child";
            this.txt_W_Child.NumberFormat = "###,###,##0.00";
            this.txt_W_Child.Postfix = "";
            this.txt_W_Child.Prefix = "";
            this.txt_W_Child.ReadOnly = true;
            this.txt_W_Child.Size = new System.Drawing.Size(59, 20);
            this.txt_W_Child.SkipValidation = false;
            this.txt_W_Child.TabIndex = 6;
            this.txt_W_Child.TabStop = false;
            this.txt_W_Child.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Spouse
            // 
            this.txt_W_Spouse.AllowSpace = true;
            this.txt_W_Spouse.AssociatedLookUpName = "";
            this.txt_W_Spouse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Spouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Spouse.ContinuationTextBox = null;
            this.txt_W_Spouse.CustomEnabled = true;
            this.txt_W_Spouse.DataFieldMapping = "PRE_BILL_NO";
            this.txt_W_Spouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Spouse.GetRecordsOnUpDownKeys = false;
            this.txt_W_Spouse.IsDate = false;
            this.txt_W_Spouse.Location = new System.Drawing.Point(203, 206);
            this.txt_W_Spouse.MaxLength = 25;
            this.txt_W_Spouse.Name = "txt_W_Spouse";
            this.txt_W_Spouse.NumberFormat = "###,###,##0.00";
            this.txt_W_Spouse.Postfix = "";
            this.txt_W_Spouse.Prefix = "";
            this.txt_W_Spouse.ReadOnly = true;
            this.txt_W_Spouse.Size = new System.Drawing.Size(59, 20);
            this.txt_W_Spouse.SkipValidation = false;
            this.txt_W_Spouse.TabIndex = 5;
            this.txt_W_Spouse.TabStop = false;
            this.txt_W_Spouse.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Self
            // 
            this.txt_W_Self.AllowSpace = true;
            this.txt_W_Self.AssociatedLookUpName = "";
            this.txt_W_Self.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Self.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Self.ContinuationTextBox = null;
            this.txt_W_Self.CustomEnabled = true;
            this.txt_W_Self.DataFieldMapping = "W_Self";
            this.txt_W_Self.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Self.GetRecordsOnUpDownKeys = false;
            this.txt_W_Self.IsDate = false;
            this.txt_W_Self.Location = new System.Drawing.Point(203, 182);
            this.txt_W_Self.MaxLength = 12;
            this.txt_W_Self.Name = "txt_W_Self";
            this.txt_W_Self.NumberFormat = "###,###,##0.00";
            this.txt_W_Self.Postfix = "";
            this.txt_W_Self.Prefix = "";
            this.txt_W_Self.ReadOnly = true;
            this.txt_W_Self.Size = new System.Drawing.Size(59, 20);
            this.txt_W_Self.SkipValidation = false;
            this.txt_W_Self.TabIndex = 4;
            this.txt_W_Self.TabStop = false;
            this.txt_W_Self.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_PNO
            // 
            this.txt_W_PNO.AllowSpace = true;
            this.txt_W_PNO.AssociatedLookUpName = "";
            this.txt_W_PNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_PNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_PNO.ContinuationTextBox = null;
            this.txt_W_PNO.CustomEnabled = true;
            this.txt_W_PNO.DataFieldMapping = "W_PNO";
            this.txt_W_PNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_PNO.GetRecordsOnUpDownKeys = false;
            this.txt_W_PNO.IsDate = false;
            this.txt_W_PNO.Location = new System.Drawing.Point(203, 116);
            this.txt_W_PNO.MaxLength = 1;
            this.txt_W_PNO.Name = "txt_W_PNO";
            this.txt_W_PNO.NumberFormat = "###,###,##0.00";
            this.txt_W_PNO.Postfix = "";
            this.txt_W_PNO.Prefix = "";
            this.txt_W_PNO.ReadOnly = true;
            this.txt_W_PNO.Size = new System.Drawing.Size(77, 20);
            this.txt_W_PNO.SkipValidation = false;
            this.txt_W_PNO.TabIndex = 2;
            this.txt_W_PNO.TabStop = false;
            this.txt_W_PNO.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_PNO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_PNO_Validating);
            // 
            // txt_W_Month
            // 
            this.txt_W_Month.AllowSpace = true;
            this.txt_W_Month.AssociatedLookUpName = "";
            this.txt_W_Month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Month.ContinuationTextBox = null;
            this.txt_W_Month.CustomEnabled = true;
            this.txt_W_Month.DataFieldMapping = "W_Month";
            this.txt_W_Month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Month.GetRecordsOnUpDownKeys = false;
            this.txt_W_Month.IsDate = false;
            this.txt_W_Month.IsRequired = true;
            this.txt_W_Month.Location = new System.Drawing.Point(203, 79);
            this.txt_W_Month.MaxLength = 25;
            this.txt_W_Month.Name = "txt_W_Month";
            this.txt_W_Month.NumberFormat = "###,###,##0.00";
            this.txt_W_Month.Postfix = "";
            this.txt_W_Month.Prefix = "";
            this.txt_W_Month.Size = new System.Drawing.Size(29, 20);
            this.txt_W_Month.SkipValidation = false;
            this.txt_W_Month.TabIndex = 0;
            this.txt_W_Month.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Month.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Month_Validating);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(-29, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(693, 13);
            this.label13.TabIndex = 52;
            this.label13.Text = "_________________________________________________________________________________" +
                "_________________";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(469, 31);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(82, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 46;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(77, 12);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.Size = new System.Drawing.Size(82, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 44;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(252, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "PROCESS";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(77, 34);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.Size = new System.Drawing.Size(82, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 45;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(214, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 13);
            this.label4.TabIndex = 50;
            this.label4.Text = "GROUP HOSPITALIZATION";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "User  :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(426, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Location :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(403, 9);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(63, 13);
            this.label35.TabIndex = 116;
            this.label35.Text = "UserName :";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(462, 9);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(63, 13);
            this.lblUser.TabIndex = 117;
            this.lblUser.Text = "UserName :";
            // 
            // CHRIS_MedicalInsurance_OMILimCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 526);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.pnlProcess);
            this.Name = "CHRIS_MedicalInsurance_OMILimCalc";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_OMILimCalc";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlProcess, 0);
            this.Controls.SetChildIndex(this.label35, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlProcess.ResumeLayout(false);
            this.pnlProcess.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlProcess;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txt_W_Year;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txt_W_Child;
        private CrplControlLibrary.SLTextBox txt_W_Spouse;
        private CrplControlLibrary.SLTextBox txt_W_Self;
        private CrplControlLibrary.SLTextBox txt_W_PNO;
        private CrplControlLibrary.SLTextBox txt_W_Month;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txt_OMI_Total_Claim;
        private CrplControlLibrary.SLTextBox txt_W_OMI_Limit;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txt_OMI_Child;
        private CrplControlLibrary.SLTextBox txt_OMI_Spouse;
        private CrplControlLibrary.SLTextBox txt_OMI_Self;
        private CrplControlLibrary.SLTextBox txt_W_Name;
        private CrplControlLibrary.SLTextBox txt_W_Cat;
        private CrplControlLibrary.SLTextBox txt_W_Status;
        private CrplControlLibrary.SLTextBox txt_W_OS_Balance;
        private CrplControlLibrary.SLDatePicker txt_W_Joining_Date;
        private CrplControlLibrary.SLDatePicker txt_W_Marriage_Date;
        private CrplControlLibrary.SLDatePicker txt_W_Child_DOB;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblUser;
    }
}