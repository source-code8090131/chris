using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_EnrollmentLetter : ChrisTabularForm
    {
        #region --Variable--
        iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage1 frm_Enrol1;
        iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage2 frm_Enrol2;
        string pnlPage1 = "pnl1";
        string pnlPage2 = "pnl2";
           
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_EnrollmentLetter()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_EnrollmentLetter(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region --Method--

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                this.lblUserName.Text   = this.UserName;
                this.txtOption.Visible  = false;
                txtDate.Text            = this.Now().ToString("dd/MM/yyyy");
                txtChoice.Select();
                txtChoice.Focus();
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        #endregion

        #region --Events--
        private void txtChoice_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter) && txtChoice.Text != string.Empty)
            {
                int OptionNo = Convert.ToInt32(txtChoice.Text);

                if (OptionNo == 1)
                {
                    frm_Enrol1 = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage1(pnlPage1);
                    frm_Enrol1.Enabled = true;
                    frm_Enrol1.Show();
                }
                else if (OptionNo == 2)
                {
                    frm_Enrol1 = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage1(pnlPage2);
                    frm_Enrol1.Enabled = true;
                    frm_Enrol1.Show();
                }
                else if (OptionNo == 3)
                {
                    frm_Enrol2 = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage2(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
                    frm_Enrol2.Enabled = true;
                    frm_Enrol2.Show();
                }
            }
        }

        private void txtChoice_Validating(object sender, CancelEventArgs e)
        {
            if (txtChoice.Text != string.Empty)
            {
                int OptionNo = Convert.ToInt32(txtChoice.Text);

                if (OptionNo > 3)
                {
                    MessageBox.Show("ENTER A VALID OPTION ...........!", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion
    }
}