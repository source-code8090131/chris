using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_EnrollmentLetterPage1 : Form
    {
        #region --Variable--
        string PanelName = string.Empty;
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_EnrollmentLetterPage1(string pnlName)
        {
            InitializeComponent();
            PanelName = pnlName;
        }

        
        #endregion

        private void dtpFrom_Validating(object sender, CancelEventArgs e)
        {
            if (dtpFrom.Value == null)
            {
                MessageBox.Show("ENTER STARTING DATE OR   F6 TO EXIT", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void dtpTo_Validating(object sender, CancelEventArgs e)
        {
            if (dtpTo.Value == null)
            {
                MessageBox.Show("ENTER STARTING DATE OR [F6] TO EXIT", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (DateTime.Compare(Convert.ToDateTime(dtpFrom.Value), Convert.ToDateTime(dtpTo.Value)) > 0)
            {
                MessageBox.Show("END DATE SHOULD BE GREATER THEN THE STARTING DATE", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else
            {
                this.Hide();
            }
        }

        private void txtPr_P_No_Validating(object sender, CancelEventArgs e)
        {
            string dummy = string.Empty;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPr_P_No.Text);

            Result rsltCode;
            CmnDataManager cmnDM    = new CmnDataManager();
            rsltCode                = cmnDM.GetData("CHRIS_SP_EnrLettPERSONNEL_MANAGER", "Check_PR_P_NO", param);

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                dummy = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
            else
            {
                MessageBox.Show("INVALID PERSONNEL NO.  .........!", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void CHRIS_MedicalInsurance_EnrollmentLetterPage1_Load(object sender, EventArgs e)
        {
            if (PanelName == "pnl1")
            {
                pnl1.Visible    = true;
                pnl1.BringToFront();
                dtpFrom.Select();
                dtpFrom.Focus();
                pnl2.Visible    = false;
            }
            else if (PanelName == "pnl2")
            {
                pnl1.Visible    = false;
                pnl2.Visible    = true;
                pnl2.BringToFront();
                txtPr_P_No.Select();
                txtPr_P_No.Focus();
            }
        }
    }
}