using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PolicyInfo_Detail : ChrisSimpleForm
    {
        #region --Variable--
        string Choice   = string.Empty;
        string OptDesc  = string.Empty;
        bool Validate   = true;
        bool ValCheck   = true;
             
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_PolicyInfo_Detail()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_PolicyInfo_Detail(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, string Opt_Desc, string Option )
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            
            txt_W_Opt_Desc.Text = Opt_Desc.ToString();
            Choice              = Option.ToString();
            OptDesc             = Opt_Desc.ToString();
            txt_W_Opt.Text      = Choice.ToString();
            txtDate.Text        = this.Now().ToString("dd/MM/yyyy");
            txt_W_Policy.Select();
            txt_W_Policy.Focus();
        }

        #endregion

        #region --Method--

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                base.IterateFormToEnableControls(pnlPolicyInfoDetail.Controls, true);
                this.CurrentPanelBlock  = pnlPolicyInfoDetail.Name;
                txt_W_Opt.Text          = Choice.ToString();
                //txt_W_Policy.Select();
                //txt_W_Policy.Focus();
                txtDate.Text            = Now().ToString("dd/MM/yyyy");
                txtOption.Visible       = false;
                txt_W_Opt_Desc.Text     = OptDesc.ToString();
                tbtAdd.Visible          = false;
                tbtDelete.Visible       = false;
                tbtEdit.Visible         = false;
                tbtList.Visible         = false;
                tbtSave.Visible         = false;
                this.ShowOptionTextBox  = false;
                this.FunctionConfig.EnableF9    = true;
                this.FunctionConfig.EnableF10   = true;
                this.FunctionConfig.F10 = Function.Save;
                this.FunctionConfig.F9  = Function.Query;
                //txt_W_Policy.Select();
                //txt_W_Policy.Focus();

                if (Choice == "01")
                {
                    List<SLPanel> lstConcurrentPanels    = new List<SLPanel>();
                    lstConcurrentPanels.Add(pnlPage3);
                    pnlPolicyInfoDetail.ConcurrentPanels = lstConcurrentPanels;

                    pnlPage3.Visible = true;
                    pnlPage3.BringToFront();
                    ClearForm(pnlPage4.Controls);
                    ClearForm(pnlPage5.Controls);
                    ClearForm(pnlPage6.Controls);
                    pnlPage4.Visible = false;
                    pnlPage5.Visible = false;
                    pnlPage6.Visible = false;
                }
                else if (Choice == "02")
                {
                    List<SLPanel> lstConcurrentPanels    = new List<SLPanel>();
                    lstConcurrentPanels.Add(pnlPage5);
                    pnlPolicyInfoDetail.ConcurrentPanels = lstConcurrentPanels;

                    pnlPage3.Visible = false;
                    pnlPage5.BringToFront();

                    pnlPage4.Visible = false;
                    pnlPage5.Visible = true;
                    pnlPage6.Visible = false;

                    ClearForm(pnlPage3.Controls);
                    ClearForm(pnlPage4.Controls);
                    ClearForm(pnlPage6.Controls);
                }
                else if (Choice == "03")
                {
                    List<SLPanel> lstConcurrentPanels   = new List<SLPanel>();
                    lstConcurrentPanels.Add(pnlPage6);
                    pnlPolicyInfoDetail.ConcurrentPanels = lstConcurrentPanels;

                    pnlPage3.Visible = false;
                    pnlPage6.BringToFront();
                    pnlPage4.Visible = false;
                    pnlPage5.Visible = false;
                    pnlPage6.Visible = true;

                    ClearForm(pnlPage3.Controls);
                    ClearForm(pnlPage4.Controls);
                    ClearForm(pnlPage5.Controls);
                }

                //txt_W_Policy.Select();
                //txt_W_Policy.Focus();
            }
            catch (Exception exp)
            {
                LogError(this.Name, "OnLoad", exp);
            }
        }

        protected override bool Save()
        {
            if (Validate)
            {
                this.CurrentPanelBlock = pnlPolicyInfoDetail.Name;
                return base.Save();
            }
            return false;
        }

        protected override bool Query()
        {
            bool flag = false;
            txt_Pol_Branch.AssociatedLookUpName = lbtnBranch.Name;
            return flag;
        }
        
        #endregion

        #region --Events--

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Close")
            {
                this.Hide();
                return;
            }
            else if (actionType == "Cancel")
            {
                ClearForm(pnlPolicyInfoDetail.Controls);
                ClearForm(pnlPage3.Controls);
                ClearForm(pnlPage4.Controls);
                ClearForm(pnlPage5.Controls);
                ClearForm(pnlPage6.Controls);
                txt_W_Opt.Text      = Choice.ToString();
                txtDate.Text        = Now().ToString("dd/MM/yyyy");
                txt_W_Opt_Desc.Text = OptDesc.ToString();
                txt_W_Policy.Select();
                txt_W_Policy.Focus();
                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Save" )
            {
                base.Cancel();
                ValCheck = false;
                txtDate.Text        = Now().ToString("dd/MM/yyyy");
                txt_W_Opt_Desc.Text = OptDesc.ToString();
                txt_W_Policy.Select();
                txt_W_Policy.Focus();
            }
        }

        private void CHRIS_MedicalInsurance_PolicyInfo_Detail_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "W_POLICY_LOV")
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("POL_POLICY_NO", txt_W_Policy.Text);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "ConCurr_Policy", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    #region --pnl3--
                    txt_Pol_Cov_Multi.Text  = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Cov_Multi"].ToString();
                    txt_Pol_Max_Limit.Text  = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Max_Limit"].ToString();
                    txt_Pol_Max_Acc.Text    = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Max_Acc"].ToString();
                    #endregion

                    #region --pnl5--
                    txt_Pol_Ailment.Text    = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Ailment"].ToString();
                    txt_Pol_Delv_Limit.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Delv_Limit"].ToString();
                    txt_Pol_Room_Chg.Text   = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Room_Chg"].ToString();
                    #endregion

                    #region --pnl6--
                    txt_Pol_Omi_Limit.Text  = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Omi_Limit"].ToString();
                    txt_Pol_Omi_Self.Text   = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Omi_Self"].ToString();
                    txt_Pol_Per_Depen.Text  = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Per_Depen"].ToString();
                    txt_Pol_No_Depen.Text   = rsltCode.dstResult.Tables[0].Rows[0]["Pol_No_Depen"].ToString();
                    #endregion
                }

            }
        }

        private void txt_Pol_Ctt_Off_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_Ctt_Off.Text != "C" && txt_Pol_Ctt_Off.Text != "O")
            {
                MessageBox.Show("ENTER [C] FOR CLERICAL & [O] FOR OFFICER  [F6] EXIT....!", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Validate = false;
                e.Cancel = true;
            }

            if (txt_Pol_Ctt_Off.Text == "C")
            {
                txt_Pol_Ctt_Off_Description.Text = "LERICAL";

                if (Choice == "01")
                {
                    List<SLPanel> lstConcurrentPanels = new List<SLPanel>();
                    lstConcurrentPanels.Add(pnlPage4);
                    pnlPolicyInfoDetail.ConcurrentPanels = lstConcurrentPanels;

                    pnlPage4.Visible = true;
                    pnlPage3.Visible = false;
                    pnlPage5.Visible = false;
                    pnlPage6.Visible = false;
                }
            }
            else if (txt_Pol_Ctt_Off.Text == "O")
            {
                txt_Pol_Ctt_Off_Description.Text = "FFICER";

                //List<SLPanel> lstConcurrentPanels = new List<SLPanel>();
                //lstConcurrentPanels.Add(pnlPage3);
                //pnlPolicyInfoDetail.ConcurrentPanels = lstConcurrentPanels;

                //pnlPage4.Visible = false;
                //pnlPage3.Visible = true;
                //pnlPage5.Visible = false;
                //pnlPage6.Visible = false;
            }

            //if (Choice == "01" && txt_Pol_Ctt_Off.Text == "C")
            //{
            //    List<SLPanel> lstConcurrentPanels    = new List<SLPanel>();
            //    lstConcurrentPanels.Add(pnlPage4);
            //    pnlPolicyInfoDetail.ConcurrentPanels = lstConcurrentPanels;

            //    pnlPage4.Visible = true;
            //    pnlPage3.Visible = false;
            //    pnlPage5.Visible = false;
            //    pnlPage6.Visible = false;
            //}
        }

        private void txt_Pol_Branch_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_Branch.Text == string.Empty)
            {
                txt_Pol_Branch.Text = "All";
            }
            
        }

        private void txt_Pol_Segment_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_Segment.Text == string.Empty)
            {
                txt_Pol_Segment.Text = "All";
            }

            if (txt_Pol_Segment.Text != "ALL" && txt_Pol_Segment.Text != "GCB" && txt_Pol_Segment.Text != "GF")
            {
                MessageBox.Show("INVALID SEGMENT ............ENTER GF OR GCB", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Validate = false;
                e.Cancel = true;
            }
            else if (txt_Pol_Segment.Text == string.Empty)
            {
                txt_Pol_Segment.Text = "ALL";
                Validate = true;
            }
        }

        private void dtp_Pol_To_Validating(object sender, CancelEventArgs e)
        {
            if (DateTime.Compare(Convert.ToDateTime(dtp_Pol_From.Value), Convert.ToDateTime(dtp_Pol_To.Value)) > 0)
            {
                MessageBox.Show("TO DATE SHOULD BE GREATER THEN FROM DATE .......!", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Validate = false;
                e.Cancel = true;
            }
            else
            {
                Validate = true;
            }
        }

        private void txt_Pol_Max_Acc_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_Max_Acc.Text != string.Empty && ValCheck)
            {
                MessageBox.Show("[F10]= SAVE   [F6]=EXIT W/O SAVE", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ValCheck = true;
            }
        }

        private void txt_Pol_Branch_Enter(object sender, EventArgs e)
        {
            if (txt_Pol_Branch.Text != "ALL")
            {
                lbtnBranch.SkipValidationOnLeave = true;
            }
            else
            {
                //lbtnBranch.SkipValidationOnLeave = false;
            }
        }

        private void txt_Pol_Room_Chg_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_Room_Chg.Text != string.Empty && ValCheck)
            {
                MessageBox.Show("[F10]= SAVE   [F6]=EXIT W/O SAVE", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ValCheck = true;
            }
        }

        private void txt_Pol_No_Depen_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_No_Depen.Text != string.Empty && ValCheck)
            {
                MessageBox.Show("[F10]= SAVE   [F6]=EXIT W/O SAVE", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ValCheck = true;
            }
        }

        private void txt_Pol_Ctt_Acc_Validating(object sender, CancelEventArgs e)
        {
            if (txt_Pol_Ctt_Acc.Text != string.Empty && ValCheck)
            {
                MessageBox.Show("[F10]= SAVE   [F6]=EXIT W/O SAVE", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ValCheck = true;
            }
        }

        private void txt_W_Policy_Validating(object sender, CancelEventArgs e)
        {
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //param.Add("POL_POLICY_NO", txt_W_Policy.Text);

            //Result rsltCode;
            //CmnDataManager cmnDM = new CmnDataManager();
            //rsltCode = cmnDM.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "W_POLICY_LOV_EXISTS", param);

            //if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            //{
            //    lbtnPolicyNo.SkipValidationOnLeave = false;
            //    //txt_Pol_Branch.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Segment.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Policy_Type.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Ctt_Off.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //dtp_Pol_From.Value = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //dtp_Pol_To.Value = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Contact_Per.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Com_Name1.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Com_Name2.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Add1.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Add2.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Add3.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //    //txt_Pol_Add4.Text = rsltCode.dstResult.Tables[0].Rows[0]["Pol_Branch"].ToString();
            //}
            //else
            //{
            //    lbtnPolicyNo.SkipValidationOnLeave = true;
            //}
        }

        private void txt_W_Policy_Leave(object sender, EventArgs e)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("POL_POLICY_NO", txt_W_Policy.Text);

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "W_POLICY_LOV_EXISTS", param);

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            {
                lbtnPolicyNo.SkipValidationOnLeave = false;
            }
            else
            {
                lbtnPolicyNo.SkipValidationOnLeave = true;
            }
        }

        #endregion
    }
}