namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_CFEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(12, 87);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(620, 194);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 10;
            // 
            // CHRIS_MedicalInsurance_CFEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 368);
            this.Controls.Add(this.slPanelSimple1);
            this.Name = "CHRIS_MedicalInsurance_CFEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_CFEntry";
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
    }
}