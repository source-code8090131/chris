namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_PayEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_PayEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtUserName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnMonth = new CrplControlLibrary.LookupButton(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txt_W_Year = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Month = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTblBlkTwo = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvPayment = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalClaim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Month = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalRecv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OMI_SELF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OMI_SPOUSE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OMI_CHILD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.pnlTblBlkTwo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 488);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(437, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(0, 13);
            this.txtUserName.TabIndex = 120;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.W_LOC);
            this.panel2.Controls.Add(this.txtUser);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Location = new System.Drawing.Point(12, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(645, 75);
            this.panel2.TabIndex = 121;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(248, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "OUT PATIENT MEDICAL";
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(76, 49);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(108, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 32;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(76, 21);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(110, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 31;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Location :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "User :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(267, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "PAYMENT ENTRY";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(449, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Date :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(497, 21);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 28;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.ConcurrentPanels = null;
            this.pnlHead.Controls.Add(this.lbtnMonth);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.txt_W_Year);
            this.pnlHead.Controls.Add(this.txt_W_Month);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlHead.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlHead.DependentPanels = null;
            this.pnlHead.DisableDependentLoad = false;
            this.pnlHead.EnableDelete = true;
            this.pnlHead.EnableInsert = true;
            this.pnlHead.EnableQuery = false;
            this.pnlHead.EnableUpdate = true;
            this.pnlHead.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PaymentEntryCommand";
            this.pnlHead.Location = new System.Drawing.Point(12, 169);
            this.pnlHead.MasterPanel = null;
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlHead.Size = new System.Drawing.Size(645, 45);
            this.pnlHead.SPName = "CHRIS_SP_OMI_CLAIM_MANAGER";
            this.pnlHead.TabIndex = 34;
            // 
            // lbtnMonth
            // 
            this.lbtnMonth.ActionLOVExists = "";
            this.lbtnMonth.ActionType = "MonthYearLOV";
            this.lbtnMonth.ConditionalFields = "";
            this.lbtnMonth.CustomEnabled = true;
            this.lbtnMonth.DataFieldMapping = "";
            this.lbtnMonth.DependentLovControls = "";
            this.lbtnMonth.HiddenColumns = "";
            this.lbtnMonth.Image = ((System.Drawing.Image)(resources.GetObject("lbtnMonth.Image")));
            this.lbtnMonth.LoadDependentEntities = false;
            this.lbtnMonth.Location = new System.Drawing.Point(234, 13);
            this.lbtnMonth.LookUpTitle = null;
            this.lbtnMonth.Name = "lbtnMonth";
            this.lbtnMonth.Size = new System.Drawing.Size(26, 21);
            this.lbtnMonth.SkipValidationOnLeave = true;
            this.lbtnMonth.SPName = "CHRIS_SP_OMI_CLAIM_MANAGER";
            this.lbtnMonth.TabIndex = 25;
            this.lbtnMonth.TabStop = false;
            this.lbtnMonth.Tag = "";
            this.lbtnMonth.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(261, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "/";
            // 
            // txt_W_Year
            // 
            this.txt_W_Year.AllowSpace = true;
            this.txt_W_Year.AssociatedLookUpName = "";
            this.txt_W_Year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Year.ContinuationTextBox = null;
            this.txt_W_Year.CustomEnabled = true;
            this.txt_W_Year.DataFieldMapping = "OMI_YEAR";
            this.txt_W_Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Year.GetRecordsOnUpDownKeys = false;
            this.txt_W_Year.IsDate = false;
            this.txt_W_Year.Location = new System.Drawing.Point(278, 13);
            this.txt_W_Year.MaxLength = 4;
            this.txt_W_Year.Name = "txt_W_Year";
            this.txt_W_Year.NumberFormat = "###,###,##0";
            this.txt_W_Year.Postfix = "";
            this.txt_W_Year.Prefix = "";
            this.txt_W_Year.Size = new System.Drawing.Size(58, 20);
            this.txt_W_Year.SkipValidation = false;
            this.txt_W_Year.TabIndex = 1;
            this.txt_W_Year.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_Year.TextType = CrplControlLibrary.TextType.Double;
            this.txt_W_Year.Validated += new System.EventHandler(this.txt_W_Year_Validated);
            // 
            // txt_W_Month
            // 
            this.txt_W_Month.AllowSpace = true;
            this.txt_W_Month.AssociatedLookUpName = "lbtnMonth";
            this.txt_W_Month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Month.ContinuationTextBox = null;
            this.txt_W_Month.CustomEnabled = true;
            this.txt_W_Month.DataFieldMapping = "OMI_MONTH";
            this.txt_W_Month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Month.GetRecordsOnUpDownKeys = false;
            this.txt_W_Month.IsDate = false;
            this.txt_W_Month.Location = new System.Drawing.Point(184, 13);
            this.txt_W_Month.MaxLength = 4;
            this.txt_W_Month.Name = "txt_W_Month";
            this.txt_W_Month.NumberFormat = "###,###,##0.00";
            this.txt_W_Month.Postfix = "";
            this.txt_W_Month.Prefix = "";
            this.txt_W_Month.Size = new System.Drawing.Size(44, 20);
            this.txt_W_Month.SkipValidation = false;
            this.txt_W_Month.TabIndex = 0;
            this.txt_W_Month.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_Month.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Claim For The Month Of :";
            // 
            // pnlTblBlkTwo
            // 
            this.pnlTblBlkTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTblBlkTwo.ConcurrentPanels = null;
            this.pnlTblBlkTwo.Controls.Add(this.dgvPayment);
            this.pnlTblBlkTwo.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBlkTwo.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkTwo.DependentPanels = null;
            this.pnlTblBlkTwo.DisableDependentLoad = false;
            this.pnlTblBlkTwo.EnableDelete = true;
            this.pnlTblBlkTwo.EnableInsert = true;
            this.pnlTblBlkTwo.EnableQuery = false;
            this.pnlTblBlkTwo.EnableUpdate = true;
            this.pnlTblBlkTwo.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PaymentEntryCommand";
            this.pnlTblBlkTwo.Location = new System.Drawing.Point(12, 210);
            this.pnlTblBlkTwo.MasterPanel = null;
            this.pnlTblBlkTwo.Name = "pnlTblBlkTwo";
            this.pnlTblBlkTwo.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkTwo.Size = new System.Drawing.Size(645, 262);
            this.pnlTblBlkTwo.SPName = "CHRIS_SP_OMI_CLAIM_MANAGER";
            this.pnlTblBlkTwo.TabIndex = 122;
            // 
            // dgvPayment
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPayment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPayment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.P_NO,
            this.ID,
            this.PrName,
            this.TotalClaim,
            this.Year,
            this.Month,
            this.TotalRecv,
            this.OMI_SELF,
            this.OMI_SPOUSE,
            this.OMI_CHILD});
            this.dgvPayment.ColumnToHide = null;
            this.dgvPayment.ColumnWidth = null;
            this.dgvPayment.CustomEnabled = true;
            this.dgvPayment.DisplayColumnWrapper = null;
            this.dgvPayment.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvPayment.GridDefaultRow = 0;
            this.dgvPayment.Location = new System.Drawing.Point(3, 8);
            this.dgvPayment.Name = "dgvPayment";
            this.dgvPayment.ReadOnlyColumns = null;
            this.dgvPayment.RequiredColumns = null;
            this.dgvPayment.Size = new System.Drawing.Size(637, 248);
            this.dgvPayment.SkippingColumns = null;
            this.dgvPayment.TabIndex = 0;
            this.dgvPayment.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvPayment_CellValidating);
            // 
            // P_NO
            // 
            this.P_NO.DataPropertyName = "OMI_P_NO";
            dataGridViewCellStyle6.NullValue = null;
            this.P_NO.DefaultCellStyle = dataGridViewCellStyle6;
            this.P_NO.HeaderText = "P.No.";
            this.P_NO.MaxInputLength = 6;
            this.P_NO.Name = "P_NO";
            this.P_NO.ReadOnly = true;
            this.P_NO.Width = 115;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // PrName
            // 
            this.PrName.DataPropertyName = "PrName";
            this.PrName.HeaderText = "Name";
            this.PrName.Name = "PrName";
            this.PrName.ReadOnly = true;
            this.PrName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PrName.Width = 110;
            // 
            // TotalClaim
            // 
            this.TotalClaim.DataPropertyName = "OMI_TOTAL_CLAIM";
            dataGridViewCellStyle7.NullValue = null;
            this.TotalClaim.DefaultCellStyle = dataGridViewCellStyle7;
            this.TotalClaim.HeaderText = "Claimed Amount";
            this.TotalClaim.MaxInputLength = 10;
            this.TotalClaim.Name = "TotalClaim";
            this.TotalClaim.ReadOnly = true;
            this.TotalClaim.Width = 120;
            // 
            // Year
            // 
            this.Year.DataPropertyName = "OMI_YEAR";
            this.Year.HeaderText = "Year";
            this.Year.MaxInputLength = 4;
            this.Year.Name = "Year";
            this.Year.Visible = false;
            this.Year.Width = 110;
            // 
            // Month
            // 
            this.Month.DataPropertyName = "OMI_MONTH";
            this.Month.HeaderText = "Month";
            this.Month.Name = "Month";
            this.Month.Visible = false;
            // 
            // TotalRecv
            // 
            this.TotalRecv.DataPropertyName = "OMI_TOTAL_RECEV";
            dataGridViewCellStyle8.NullValue = null;
            this.TotalRecv.DefaultCellStyle = dataGridViewCellStyle8;
            this.TotalRecv.HeaderText = "Amount Received";
            this.TotalRecv.MaxInputLength = 10;
            this.TotalRecv.Name = "TotalRecv";
            this.TotalRecv.Width = 120;
            // 
            // OMI_SELF
            // 
            this.OMI_SELF.DataPropertyName = "OMI_SELF";
            this.OMI_SELF.HeaderText = "OMI_SELF";
            this.OMI_SELF.Name = "OMI_SELF";
            this.OMI_SELF.Visible = false;
            // 
            // OMI_SPOUSE
            // 
            this.OMI_SPOUSE.DataPropertyName = "OMI_SPOUSE";
            this.OMI_SPOUSE.HeaderText = "OMI_SPOUSE";
            this.OMI_SPOUSE.Name = "OMI_SPOUSE";
            this.OMI_SPOUSE.Visible = false;
            // 
            // OMI_CHILD
            // 
            this.OMI_CHILD.DataPropertyName = "OMI_CHILD";
            this.OMI_CHILD.HeaderText = "OMI_CHILD";
            this.OMI_CHILD.Name = "OMI_CHILD";
            this.OMI_CHILD.Visible = false;
            // 
            // CHRIS_MedicalInsurance_PayEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 548);
            this.Controls.Add(this.pnlTblBlkTwo);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.txtUserName);
            this.CurrentPanelBlock = "pnlTblBlkTwo";
            this.Name = "CHRIS_MedicalInsurance_PayEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_PayEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlTblBlkTwo, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlTblBlkTwo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox W_LOC;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtDate;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlHead;
        private CrplControlLibrary.SLTextBox txt_W_Year;
        private CrplControlLibrary.SLTextBox txt_W_Month;
        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkTwo;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvPayment;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.LookupButton lbtnMonth;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalClaim;
        private System.Windows.Forms.DataGridViewTextBoxColumn Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn Month;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalRecv;
        private System.Windows.Forms.DataGridViewTextBoxColumn OMI_SELF;
        private System.Windows.Forms.DataGridViewTextBoxColumn OMI_SPOUSE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OMI_CHILD;
    }
}