using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using CrplControlLibrary;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PayEntry : ChrisTabularForm
    {
        CHRIS_MedicalInsurance_PayEntry_Detail frmChild;
        #region --Constructor--
        public CHRIS_MedicalInsurance_PayEntry()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_PayEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();


        }
        #endregion


        #region --EVENTS--
        /// <summary>
        /// Set the User Name
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {

            try
            {
                base.OnLoad(e);
                this.txtUserName.Text = "UserName: " + this.UserName;
              
                Font newFontStyle = new Font(dgvPayment.Font, FontStyle.Bold);
                dgvPayment.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
                this.txtOption.Visible = false;
                this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                this.W_LOC.Text = this.CurrentLocation;
                this.txtUser.Text = this.userID;
                txt_W_Month.Select();
                txt_W_Month.Focus();
                this.PrName.Width = 310;
                tbtSave.Available = false;
                tbtList.Available = false;

                this.FunctionConfig.EnableF6 = true;
                this.FunctionConfig.F6 = Function.Quit;

                this.FunctionConfig.EnableF10 = true;
                this.FunctionConfig.F10 = Function.Save;
                
               

            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Delete")
            {
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        

        private void txt_W_Year_Validated(object sender, EventArgs e)
        {
            try
            {
                bool ChkValue;
                decimal Year;
                ChkValue = decimal.TryParse(txt_W_Year.Text, out Year);
                if (ChkValue)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("SP_YEAR", txt_W_Year.Text);
                    param.Add("SP_MONTH", txt_W_Month.Text);
                  


                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_OMI_CLAIM_MANAGER", "LoadGridByMonthYear", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables.Count > 0)
                    {

                        dgvPayment.GridSource = rsltCode.dstResult.Tables[0];
                        dgvPayment.DataSource = dgvPayment.GridSource;

                    }

                }

            }

            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_Year_Validated", exp);
            }
        }

        private void dgvPayment_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            decimal OmitotalClaim = 0;
            decimal OmiRecvClaim = 0;
            if (!this.dgvPayment.CurrentCell.IsInEditMode)
            {

                return;
            }
            if (e.ColumnIndex == 6)
            {
                if (dgvPayment.Rows[e.RowIndex].Cells["TotalClaim"].Value.ToString() != string.Empty)
                {
                    OmitotalClaim = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["TotalClaim"].Value.ToString());
                }

                if (dgvPayment.Rows[e.RowIndex].Cells["TotalRecv"].EditedFormattedValue.ToString() != string.Empty)
                {
                    OmiRecvClaim = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["TotalRecv"].EditedFormattedValue.ToString());
                }
                if (OmiRecvClaim > OmitotalClaim)
                {

                    MessageBox.Show("AMOUNT RECEIVED IS GREATER THEN THE CLAIMED AMOUNT");

                    e.Cancel = true;


                }
                else if (OmiRecvClaim != OmitotalClaim)
                {
                    //Load Child Grid

                   
                    decimal omi_pNo=0;
                    decimal omi_Month = 0;
                    decimal omi_Year = 0;
                    decimal omi_TotalAmt = 0;
                    decimal omi_SELF = 0;
                    decimal omi_SPOUSE = 0;
                    decimal omi_CHILD = 0;
                    decimal TotalfrmChild = 0;
                    DataTable DgClaim=null;
                    if (dgvPayment.Rows[e.RowIndex].Cells["P_NO"].Value.ToString() != string.Empty && dgvPayment.Rows[e.RowIndex].Cells["Month"].Value.ToString() != string.Empty && dgvPayment.Rows[e.RowIndex].Cells["Year"].Value.ToString() != string.Empty)
                    {
                        omi_pNo = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["P_NO"].Value.ToString());
                        omi_Month = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["Month"].Value.ToString());
                        omi_Year = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["Year"].Value.ToString());
                       

                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("OMI_P_NO", omi_pNo);
                        param.Add("OMI_MONTH", omi_Month);
                        param.Add("OMI_YEAR", omi_Year);



                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_OMI_CLAIM_DETAIL_MANAGER", "LoadChildGrid", param);
                       
                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables.Count > 0)
                        {

                            DgClaim = rsltCode.dstResult.Tables[0];
                            //DGVClaim.DataSource = DGVClaim.GridSource;

                        }

                        if (dgvPayment.Rows[e.RowIndex].Cells["TotalRecv"].Value.ToString() != string.Empty)
                        {
                            omi_TotalAmt = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["TotalRecv"].Value.ToString());
                        }

                        if (dgvPayment.Rows[e.RowIndex].Cells["OMI_SELF"].Value.ToString() != string.Empty)
                        {
                            omi_SELF = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["OMI_SELF"].Value.ToString());
                        }
                        if (dgvPayment.Rows[e.RowIndex].Cells["OMI_SPOUSE"].Value.ToString() != string.Empty)
                        {
                            omi_SPOUSE = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["OMI_SPOUSE"].Value.ToString());
                        }
                        if (dgvPayment.Rows[e.RowIndex].Cells["OMI_CHILD"].Value.ToString() != string.Empty)
                        {
                            omi_CHILD = decimal.Parse(dgvPayment.Rows[e.RowIndex].Cells["OMI_CHILD"].Value.ToString());
                        }

                    }

                    frmChild = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_PayEntry_Detail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean,  DgClaim, omi_TotalAmt, omi_SELF, omi_SPOUSE, omi_CHILD);
                    frmChild.MdiParent = null;
                    frmChild.ShowDialog();
                    TotalfrmChild = frmChild.getTotal;
                    if (TotalfrmChild != 0)
                    {
                        dgvPayment.Rows[e.RowIndex].Cells["TotalRecv"].Value = TotalfrmChild;
                    }
                    //Open Popup
                }
            }
        }

        #endregion


        protected override bool Save()
        {
            decimal omi_P_no=0;
            decimal omi_recv_Amt=0;
            DialogResult dRes = MessageBox.Show("Do You Want To Posting Records in Payroll [Y]es/[N]o. :", "Note"
                                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                rsltCode = cmnDM.Execute("CHRIS_SP_OMI_CLAIM_DETAIL_MANAGER", "DeleteAllowance", null);

                if (rsltCode.isSuccessful)
                {
                    Result rsltCode1;
                    Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
                 

                    foreach (DataGridViewRow rs in dgvPayment.Rows)
                    {

                        if (rs.Cells[0].Value != null)
                        {

                            omi_recv_Amt = decimal.Parse(rs.Cells["TotalRecv"].Value.ToString());

                            omi_P_no = decimal.Parse(rs.Cells["P_NO"].Value.ToString());


                            colsNVals1.Clear();
                            colsNVals1.Add("OMI_P_NO", omi_P_no);
                            colsNVals.Add("OMI_TOTAL_RECEV", omi_recv_Amt);

                            rsltCode1 = cmnDM.Execute("CHRIS_SP_OMI_CLAIM_MANAGER", "InsertAllowance", colsNVals1);

                            if (!rsltCode1.isSuccessful)
                            {
                                //MessageBox.Show("Error .. Records Posting in Payroll....");
                                //base.Cancel();
                                //return false;

                            }
                        }
                    }

                }
            }



            return false;
        }
    }
}