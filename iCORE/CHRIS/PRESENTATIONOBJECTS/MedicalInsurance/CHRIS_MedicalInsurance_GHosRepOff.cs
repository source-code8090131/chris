using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_GHosRepOff : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_MedicalInsurance_GHosRepOff()
        {
            InitializeComponent();
        }
        public CHRIS_MedicalInsurance_GHosRepOff(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.dest_format.Text = "WIDE180";
            this.copies.Text = "1";
            this.Dest_Name.Text = "c:\\iCORE-Spool\\gHI01.LIS";

        }

        private void Run_Click(object sender, EventArgs e)
        {
            {

                base.RptFileName = "ghi01download";
                //base.btnCallReport_Click(sender, e);


                    string DestName;

                    if (this.Dest_Name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_Name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");


            }

        }

        private void slButton1_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void cmbDescType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}