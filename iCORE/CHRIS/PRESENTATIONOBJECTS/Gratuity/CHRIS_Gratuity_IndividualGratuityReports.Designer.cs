namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    partial class CHRIS_Gratuity_IndividualGratuityReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Gratuity_IndividualGratuityReports));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.w_pto = new CrplControlLibrary.SLTextBox(this.components);
            this.slButton2 = new CrplControlLibrary.SLButton();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.w_pfrom = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.w_dept = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Copies = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(375, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 78;
            this.pictureBox2.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(16, 217);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(140, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "Enter To Personnel No.";
            // 
            // w_pto
            // 
            this.w_pto.AllowSpace = true;
            this.w_pto.AssociatedLookUpName = "";
            this.w_pto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_pto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_pto.ContinuationTextBox = null;
            this.w_pto.CustomEnabled = true;
            this.w_pto.DataFieldMapping = "";
            this.w_pto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_pto.GetRecordsOnUpDownKeys = false;
            this.w_pto.IsDate = false;
            this.w_pto.Location = new System.Drawing.Point(223, 215);
            this.w_pto.MaxLength = 8;
            this.w_pto.Name = "w_pto";
            this.w_pto.NumberFormat = "###,###,##0.00";
            this.w_pto.Postfix = "";
            this.w_pto.Prefix = "";
            this.w_pto.Size = new System.Drawing.Size(135, 20);
            this.w_pto.SkipValidation = false;
            this.w_pto.TabIndex = 22;
            this.w_pto.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // slButton2
            // 
            this.slButton2.ActionType = "";
            this.slButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton2.Location = new System.Drawing.Point(287, 319);
            this.slButton2.Name = "slButton2";
            this.slButton2.Size = new System.Drawing.Size(58, 23);
            this.slButton2.TabIndex = 21;
            this.slButton2.Text = "Close";
            this.slButton2.UseVisualStyleBackColor = true;
            this.slButton2.Click += new System.EventHandler(this.slButton2_Click);
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(223, 319);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(58, 23);
            this.slButton1.TabIndex = 20;
            this.slButton1.Text = "Run";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // w_brn
            // 
            this.w_brn.AllowSpace = true;
            this.w_brn.AssociatedLookUpName = "";
            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_brn.ContinuationTextBox = null;
            this.w_brn.CustomEnabled = true;
            this.w_brn.DataFieldMapping = "";
            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_brn.GetRecordsOnUpDownKeys = false;
            this.w_brn.IsDate = false;
            this.w_brn.Location = new System.Drawing.Point(223, 267);
            this.w_brn.MaxLength = 3;
            this.w_brn.Name = "w_brn";
            this.w_brn.NumberFormat = "###,###,##0.00";
            this.w_brn.Postfix = "";
            this.w_brn.Prefix = "";
            this.w_brn.Size = new System.Drawing.Size(135, 20);
            this.w_brn.SkipValidation = false;
            this.w_brn.TabIndex = 19;
            this.w_brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(16, 269);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(148, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "BRANCH CODE OR[ALL]";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(16, 243);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(161, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "SEGMENT CODE OR [ALL]";
            // 
            // w_seg
            // 
            this.w_seg.AllowSpace = true;
            this.w_seg.AssociatedLookUpName = "";
            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_seg.ContinuationTextBox = null;
            this.w_seg.CustomEnabled = true;
            this.w_seg.DataFieldMapping = "";
            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_seg.GetRecordsOnUpDownKeys = false;
            this.w_seg.IsDate = false;
            this.w_seg.Location = new System.Drawing.Point(223, 241);
            this.w_seg.MaxLength = 3;
            this.w_seg.Name = "w_seg";
            this.w_seg.NumberFormat = "###,###,##0.00";
            this.w_seg.Postfix = "";
            this.w_seg.Prefix = "";
            this.w_seg.Size = new System.Drawing.Size(135, 20);
            this.w_seg.SkipValidation = false;
            this.w_seg.TabIndex = 17;
            this.w_seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 191);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(152, 13);
            this.label19.TabIndex = 14;
            this.label19.Text = "Enter From Personnel No.";
            // 
            // w_pfrom
            // 
            this.w_pfrom.AllowSpace = true;
            this.w_pfrom.AssociatedLookUpName = "";
            this.w_pfrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_pfrom.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_pfrom.ContinuationTextBox = null;
            this.w_pfrom.CustomEnabled = true;
            this.w_pfrom.DataFieldMapping = "";
            this.w_pfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_pfrom.GetRecordsOnUpDownKeys = false;
            this.w_pfrom.IsDate = false;
            this.w_pfrom.Location = new System.Drawing.Point(223, 189);
            this.w_pfrom.MaxLength = 8;
            this.w_pfrom.Name = "w_pfrom";
            this.w_pfrom.NumberFormat = "###,###,##0.00";
            this.w_pfrom.Postfix = "";
            this.w_pfrom.Prefix = "";
            this.w_pfrom.Size = new System.Drawing.Size(135, 20);
            this.w_pfrom.SkipValidation = false;
            this.w_pfrom.TabIndex = 15;
            this.w_pfrom.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(16, 163);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(109, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Number Of Copies";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.w_dept);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.w_pto);
            this.groupBox1.Controls.Add(this.slButton2);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.w_brn);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.w_seg);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.w_pfrom);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Copies);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(442, 352);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 295);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 13);
            this.label8.TabIndex = 80;
            this.label8.Text = "DEPARTMENT CODE OR [ALL]";
            // 
            // w_dept
            // 
            this.w_dept.AllowSpace = true;
            this.w_dept.AssociatedLookUpName = "";
            this.w_dept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_dept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_dept.ContinuationTextBox = null;
            this.w_dept.CustomEnabled = true;
            this.w_dept.DataFieldMapping = "";
            this.w_dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_dept.GetRecordsOnUpDownKeys = false;
            this.w_dept.IsDate = false;
            this.w_dept.Location = new System.Drawing.Point(223, 293);
            this.w_dept.MaxLength = 3;
            this.w_dept.Name = "w_dept";
            this.w_dept.NumberFormat = "###,###,##0.00";
            this.w_dept.Postfix = "";
            this.w_dept.Prefix = "";
            this.w_dept.Size = new System.Drawing.Size(135, 20);
            this.w_dept.SkipValidation = false;
            this.w_dept.TabIndex = 79;
            this.w_dept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 269);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Enter Branch or [ALL]";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Enter segment or [ALL]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 191);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Enter From Personnel No.";
            // 
            // Copies
            // 
            this.Copies.AllowSpace = true;
            this.Copies.AssociatedLookUpName = "";
            this.Copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Copies.ContinuationTextBox = null;
            this.Copies.CustomEnabled = true;
            this.Copies.DataFieldMapping = "";
            this.Copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Copies.GetRecordsOnUpDownKeys = false;
            this.Copies.IsDate = false;
            this.Copies.Location = new System.Drawing.Point(223, 163);
            this.Copies.MaxLength = 2;
            this.Copies.Name = "Copies";
            this.Copies.NumberFormat = "###,###,##0.00";
            this.Copies.Postfix = "";
            this.Copies.Prefix = "";
            this.Copies.Size = new System.Drawing.Size(135, 20);
            this.Copies.SkipValidation = false;
            this.Copies.TabIndex = 11;
            this.Copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 137);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(113, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Destination Format";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Number Of Copies";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Destination Format";
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(223, 137);
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(135, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 8;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 111);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Destination Name";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(223, 111);
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(135, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 7;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Destination Name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Destination Type";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(223, 84);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(135, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Destination Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(118, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(208, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter values for the parameters";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(148, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Report Parameters";
            // 
            // CHRIS_Gratuity_IndividualGratuityReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 406);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_Gratuity_IndividualGratuityReports";
            this.Text = "iCORE CHRIS - Individual Gratuity Reports";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox w_pto;
        private CrplControlLibrary.SLButton slButton2;
        private CrplControlLibrary.SLButton slButton1;
        private CrplControlLibrary.SLTextBox w_brn;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox w_seg;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox w_pfrom;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox w_dept;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox Copies;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}