﻿//using HiQPdf;
using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    public partial class CHRIS_FINAL_SETTLEMENT_REPORT : Form
    {
        public CHRIS_FINAL_SETTLEMENT_REPORT()
        {
            InitializeComponent();
        }

        private void btnSaveFilePath_Click(object sender, EventArgs e)
        {
            if (folderBrwDialog.ShowDialog() == DialogResult.OK)
            {
                txtSaveFilePath.Text = folderBrwDialog.SelectedPath;
            }
        }

        private void btnGenReport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_PR_P_NO.Text))
            {
                MessageBox.Show("Please Enter Personnel Number.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(txtSaveFilePath.Text))
            {
                MessageBox.Show("Please Select Path To Save File.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int PRNO = Convert.ToInt32(txt_PR_P_NO.Text.Trim());

            DataTable dt_PF_YEAR_PFI_RATE = new DataTable();
            dt_PF_YEAR_PFI_RATE = SQLManager.CHRIS_SP_PF_YEAR_WISE_DETAIL_PFI_RATE().Tables[0];


            DataTable dt_PF_YEAR = new DataTable();
            
            dt_PF_YEAR = SQLManager.CHRIS_SP_PF_YEAR_WISE(PRNO).Tables[0];
            StringBuilder PF_YEAR = new StringBuilder();

            double d_EMP_CONT = 0;
            double d_BNK_CONT = 0;
            double d_PF_INT = 0;
            double d_PF_SUM = 0;

            foreach (System.Data.DataRow drowx in dt_PF_YEAR.Rows)
            {
                string PR_YEAR = string.IsNullOrEmpty(drowx["YEAR"].ToString().Trim()) ? "" : drowx["YEAR"].ToString().Trim();
                string EMP_CONT = string.IsNullOrEmpty(drowx["EMP_CONT"].ToString().Trim()) ? "" : drowx["EMP_CONT"].ToString().Trim();
                string BNK_CONT = string.IsNullOrEmpty(drowx["BNK_CONT"].ToString().Trim()) ? "" : drowx["BNK_CONT"].ToString().Trim();
                string PF_INT = string.IsNullOrEmpty(drowx["PF_INT"].ToString().Trim()) ? "" : drowx["PF_INT"].ToString().Trim();

                double sum_PF_VALUE = Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(PF_INT);

                string disPRYEAR = "<tr>"
                              + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
                              + "</tr>"
                              + "<tr Style='border: 1px solid black;'>"
                              + "<td Style='border: 1px solid black;font-size:12px; text-align:center;'>UPTO DEC " + PR_YEAR + "</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + EMP_CONT + "</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + BNK_CONT + "</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + PF_INT + "</td>"
                              + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + sum_PF_VALUE + "</td>"
                              + "</tr>"
                              + "<tr>";

                d_EMP_CONT = d_EMP_CONT + Convert.ToDouble(EMP_CONT);
                d_BNK_CONT = d_BNK_CONT + Convert.ToDouble(BNK_CONT);
                d_PF_INT = d_PF_INT + Convert.ToDouble(PF_INT);
                d_PF_SUM = d_PF_SUM + Convert.ToDouble(sum_PF_VALUE);

                PF_YEAR.Append(disPRYEAR);
            }


            #region Defination of PROVIDENT FUND
            DataTable dt_PF_YEAR_DETAIL = new DataTable();

            dt_PF_YEAR_DETAIL = SQLManager.CHRIS_SP_PF_YEAR_WISE_DETAIL(PRNO).Tables[0];
            StringBuilder PF_YEAR_DETAIL = new StringBuilder();

            string PF_YEAR_DETAIL_STRING = string.Empty;
            double PR_BAL_AMT = 0;

            #region Count of Months

            DataView cnt_Years = new DataView(dt_PF_YEAR_DETAIL);
            DataTable dist_Year_Month = cnt_Years.ToTable(true, "YEAR", "Months");
            int cnt_year_months = dist_Year_Month.Rows.Count;

            #endregion
            int countRows = 1;
            int TotalDays = 0;
            foreach (System.Data.DataRow drowx in dt_PF_YEAR_DETAIL.Rows)
            {
                PF_YEAR_DETAIL_STRING = string.Empty;
                string PR_YEAR = string.IsNullOrEmpty(drowx["YEAR"].ToString().Trim()) ? "" : drowx["YEAR"].ToString().Trim();
                string Months    = string.IsNullOrEmpty(drowx["Months"].ToString().Trim()) ? "" : drowx["Months"].ToString().Trim();
                string Days = string.IsNullOrEmpty(drowx["Days"].ToString().Trim()) ? "" : drowx["Days"].ToString().Trim();
                string EMP_CONT = string.IsNullOrEmpty(drowx["PF_EMP_CONT"].ToString().Trim()) ? "" : drowx["PF_EMP_CONT"].ToString().Trim();
                string BNK_CONT = string.IsNullOrEmpty(drowx["PF_BNK_CONT"].ToString().Trim()) ? "" : drowx["PF_BNK_CONT"].ToString().Trim();
                string Total_Cont = string.IsNullOrEmpty(drowx["Total_Cont"].ToString().Trim()) ? "" : drowx["Total_Cont"].ToString().Trim();
                string PF_INT = string.IsNullOrEmpty(drowx["PF_INTEREST"].ToString().Trim()) ? "" : drowx["PF_INTEREST"].ToString().Trim();

                int dt_PF_YEAR_COUNT = dt_PF_YEAR_DETAIL.Select("YEAR = " + PR_YEAR + "").Count();

                //double PR_TOTAL = (Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total));
                if (countRows == 1)
                {

                    DataRow[] yearDBRows = dt_PF_YEAR.Select("YEAR = " + PR_YEAR + "");
                    
                    string last_year_total = "";
                    foreach (DataRow rowFile in yearDBRows)
                    {
                        string last_EMP_CONT = string.IsNullOrEmpty(rowFile["EMP_CONT"].ToString().Trim()) ? "" : rowFile["EMP_CONT"].ToString().Trim();
                        string last_BNK_CONT = string.IsNullOrEmpty(rowFile["BNK_CONT"].ToString().Trim()) ? "" : rowFile["BNK_CONT"].ToString().Trim();
                        string last_PF_INT = string.IsNullOrEmpty(rowFile["PF_INT"].ToString().Trim()) ? "" : rowFile["PF_INT"].ToString().Trim();

                        last_year_total = Convert.ToString(Convert.ToDouble(last_EMP_CONT) + Convert.ToDouble(last_BNK_CONT) + Convert.ToDouble(last_PF_INT));

                        string txt_lastyear = "<table style='width:70%; border: 1px solid black;margin-left:35px; border-collapse: collapse;margin-top:10px;'>" 
                                              + "<tr Style='border: 1px solid black;'>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'> &#160;</th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'>EMP CONT</th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'>BANK CONT</th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'>TOTAL </th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'> &#160;</th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'>PR BAL</th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'> </th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'> </th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'>BALANCE </th>"
                                              + "<th Style='border: 1px solid black;font-size:12px;'> </th>"
                                              + "</tr>" +
                                                "<tr>"
                                              + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                             + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                             + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                             + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                             + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                              + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + last_year_total + "</td>"
                                               + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                                + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                                 + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                                  + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                                             + "</tr>";
                        PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + txt_lastyear;
                    }

                    string disPRYEAR = "<tr Style='border: 1px solid black;'>"
                                        + "<td Style='border: 1px solid black;font-size:12px; text-align:center;'>" + Months + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + EMP_CONT + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + BNK_CONT + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + Total_Cont + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + (last_year_total) + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>"+ Convert.ToString(Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total)) + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>X</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>"+ Days + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>"+ ((Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total)) * 31) + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                        + "</tr>";

                    PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + disPRYEAR;
                    PR_BAL_AMT = (Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total));
                }
                else
                {
                    string disPRYEAR = "<tr Style='border: 1px solid black;'>"
                                        + "<td Style='border: 1px solid black;font-size:12px; text-align:center;'>" + Months + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + EMP_CONT + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + BNK_CONT + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + Total_Cont + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + PR_BAL_AMT + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>"+ (Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + PR_BAL_AMT) + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>X</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>"+ Days + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'>" + ((Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + PR_BAL_AMT) * Convert.ToInt32(Days)) + "</td>"
                                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                        + "</tr>";

                    PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + disPRYEAR;
                    PR_BAL_AMT = (Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + PR_BAL_AMT);
                }

                //d_EMP_CONT = d_EMP_CONT + Convert.ToDouble(EMP_CONT);
                //d_BNK_CONT = d_BNK_CONT + Convert.ToDouble(BNK_CONT);
                //d_PF_INT = d_PF_INT + Convert.ToDouble(PF_INT);
                //d_PF_SUM = d_PF_SUM + Convert.ToDouble(sum_PF_VALUE);

                if(dt_PF_YEAR_COUNT == countRows)
                {
                    DataRow[] yearPFIRate = dt_PF_YEAR_PFI_RATE.Select("YEAR = " + PR_YEAR + "");

                    string yearRate = string.Empty;
                    string Rate = string.Empty;
                    foreach (DataRow rowFile in yearPFIRate)
                    {
                        yearRate = string.IsNullOrEmpty(rowFile["YEAR"].ToString().Trim()) ? "" : rowFile["YEAR"].ToString().Trim();
                        Rate = string.IsNullOrEmpty(rowFile["PFI_RATE"].ToString().Trim()) ? "" : rowFile["PFI_RATE"].ToString().Trim();
                    }

                        string getDiscountValue = "</table> <div ><table style='width:60%; border: 0px;margin-left:35px; border-collapse: collapse;margin-top:10px;'>"
                                            + "<tr Style=''>"
                                            + "<td Style='font-size:12px;'>Interest rate used for "+ PR_YEAR + ": "+ Rate + " % </td>"
                                            + "<td Style='font-size:12px;'> = </td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;text-align:center;'> &#160;&#160;&#160;</td>"
                                            + "<td Style='font-size:12px;'> <U>136, 085.47 </U> </td></table>";

                    PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + getDiscountValue;

                    countRows = 1;
                }
                else
                {
                    countRows = countRows + 1;
                }

                TotalDays = TotalDays + Convert.ToInt32(Days);
                PF_YEAR_DETAIL.Append(PF_YEAR_DETAIL_STRING);
            }


            #endregion


            string Total_PF_VALUE = "<tr>"
                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
                          + "</tr>"
                          + "<tr Style='border: 1px solid black;'>"
                          + "<td Style='border: 1px solid black;font-size:12px; text-align:center;font-weight:bold;'>TOTAL </td>"
                          + "<td Style='border: 1px solid black;font-size:12px;text-align:center;font-weight:bold;'>" + d_EMP_CONT + "</td>"
                          + "<td Style='border: 1px solid black;font-size:12px;text-align:center;font-weight:bold;'>" + d_BNK_CONT + " </td>"
                          + "<td Style='border: 1px solid black;font-size:12px;text-align:center;font-weight:bold;'>" + d_PF_INT + "</td>"
                          + "<td Style='border: 1px solid black;font-size:12px;text-align:center;font-weight:bold;'>" + d_PF_SUM + "</td>"
                          + "</tr>";

            PF_YEAR.Append(Total_PF_VALUE);
            string AmountInWords = ConvertAmountToWords(d_PF_SUM);

            #region FINAL DUES SETTLEMENT

            string disINFO_SETTLE = string.Empty;
            DataTable PR_INFO_FOR_SETTLEMENT = new DataTable();
            PR_INFO_FOR_SETTLEMENT = SQLManager.CHRIS_GET_PR_INFO_FOR_SETTLEMENT(PRNO).Tables[0];

            foreach (System.Data.DataRow drowx in PR_INFO_FOR_SETTLEMENT.Rows)
            {
                string PR_P_NO = string.IsNullOrEmpty(drowx["PR_P_NO"].ToString().Trim()) ? "" : drowx["PR_P_NO"].ToString().Trim();
                string PR_BRANCH = string.IsNullOrEmpty(drowx["PR_BRANCH"].ToString().Trim()) ? "" : drowx["PR_BRANCH"].ToString().Trim();
                string PR_DEPT = string.IsNullOrEmpty(drowx["PR_DEPT"].ToString().Trim()) ? "" : drowx["PR_DEPT"].ToString().Trim();
                string PR_FUNC_TITTLE1 = string.IsNullOrEmpty(drowx["PR_FUNC_TITTLE1"].ToString().Trim()) ? "" : drowx["PR_FUNC_TITTLE1"].ToString().Trim();
                string PR_FUNC_TITTLE2 = string.IsNullOrEmpty(drowx["PR_FUNC_TITTLE2"].ToString().Trim()) ? "" : drowx["PR_FUNC_TITTLE2"].ToString().Trim();
                string PR_JOINING_DATE = string.IsNullOrEmpty(drowx["PR_JOINING_DATE"].ToString().Trim()) ? "" : drowx["PR_JOINING_DATE"].ToString().Trim();
                string PR_ANNUAL_PACK = string.IsNullOrEmpty(drowx["PR_ANNUAL_PACK"].ToString().Trim()) ? "" : drowx["PR_ANNUAL_PACK"].ToString().Trim();
                string PR_TERMIN_DATE = string.IsNullOrEmpty(drowx["PR_TERMIN_DATE"].ToString().Trim()) ? "" : drowx["PR_TERMIN_DATE"].ToString().Trim();
                string GR_LIAB = string.IsNullOrEmpty(drowx["GR_LIAB"].ToString().Trim()) ? "" : drowx["GR_LIAB"].ToString().Trim();

                //double sum_PF_VALUE = Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(PF_INT);
                //DateTime.Now.ToString("MMMM dd")

                DateTime dtJoining = Convert.ToDateTime(PR_JOINING_DATE);
                string joiningDate = dtJoining.ToString("MMMM dd yyyy");
                string terminDate = string.Empty;
                DateTime dtTermin = new DateTime();
                double NoOfDays = 0;

                if (!string.IsNullOrEmpty(PR_TERMIN_DATE))
                {
                    dtTermin = Convert.ToDateTime(PR_TERMIN_DATE);
                    terminDate = dtTermin.ToString("MMMM dd yyyy");
                    NoOfDays = ((dtTermin - dtJoining).Days);                    
                }

                disINFO_SETTLE = "<div Style='page-break-before: always;margin-left:25px;margin-top:-30px;width:80%;'>"
                                        + "<h2 Style='font-size:12px;text-align:left;margin-left:50px;margin-top:20px;'>FINAL DUES SETTLEMENT</h2>"
                                        + "<p Style='font-size:12px;text-align:left;margin-left:50px;margin-top:20px;'> <B>Employee # "+ PR_P_NO + " <br> Level: "+ PR_FUNC_TITTLE1 +" " + PR_FUNC_TITTLE2 + " <br> "+ PR_BRANCH +" - "+ PR_DEPT + " </B>"
                                        + "</p>"
                                        + "<table style='width:45%;margin-left:45px; border-collapse: collapse;margin-top:10px;text-align:left;'>"
                                        + "<tr>"
                                        + "<td Style='font-size:12px;width:0px;'>Date of Joining <br> Date of Leaving + <br> Years of Service <br> Annual Package </td> "
                                        + "<td Style='font-size:12px;width:0px;'> = <br> = <br> = <br> = </td>"
                                        + "<td Style='font-size:12px;width:0px;'>"+ joiningDate + " <br>"+ terminDate + " + <br>"+ Math.Round((NoOfDays / 365),2) +" <br>"+ PR_ANNUAL_PACK + " </td>"
                                        + "</tr>"
                                        + "</table>"
                                        + "<table style='width:45%;margin-left:45px; border-collapse: collapse;margin-top:10px; font-size:12px;text-align:left;'>"
                                        + "<tr>"
                                        + "<td> - Ex Gratia Payment </td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs. </td>"
                                        + "<td>3,218,018.00</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td>(Less: Tax on Ex Gratia)</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td Style='border-bottom:1px solid black;'> &#160;&#160;252,378.00</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td><B>Net of Tax + </B></td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> <B> Rs.+ </B></td>"
                                        + "<td> <B>2,965,640.00 + </B></td>"
                                        + "</tr>"
                                        + "</table>"
                                        + "</div>"
                                        + "<div Style='margin-left:25px;'>"
                                        + "<h1 Style='font-size:12px;text-align:left;'>ADD: </h1>"
                                        + "<table style='width:45%; margin-left:28px; border-collapse: collapse;margin-top:10px; font-size:12px;'>"
                                        + "<tr>"
                                        + "<td> - Provident Fund Settlement</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td>"+ d_PF_SUM + "</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> - Gratuity Fund Settlement</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td>"+ GR_LIAB + "</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> - Leave Encashment for 6 Days</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td>25, 635.16</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> - Notice Period Pay</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td>129, 956.00</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> - BAF loan subsidy</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"

                                        + "<td>Rs.</td>"
                                        + "<td Style='border-bottom:1px solid black;'>1, 967.87</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td Style='border-bottom:1px solid black;'>4, 101, 906.23</td>"
                                        + "</tr>"
                                        + "</table>"
                                        + "</div>"
                                        + "<div Style='margin-left:25px;'>"
                                        + "<h1 Style='font-size:12px;text-align:left;'>LESS:</h1>"
                                        + "<table style='width:45%; margin-left:45px; border-collapse: collapse;margin-top:10px; font-size:12px;'>"
                                        + "<tr>"
                                        + "<td> - Advance Paid Relocation Deduction - Net</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td>522, 667.00</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> - Outstanding Balance of Vehicle Loan</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td>Rs.</td>"
                                        + "<td Style='border-bottom:1px solid black;'> &#160;&#160;98,068.00</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td Style='border-bottom:1px solid black;'> &#160;&#160;620,735.00</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td Style='border-bottom:1px solid black;'> &#160;&#160;&#160;&#160;&#160;&#160;3,481,171.23</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td><B>Net amount due to from the bank+</B></td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td> &#160;&#160;&#160;</td>"
                                        + "<td><B>Rs.</B></td>"
                                        + "<td Style='border-bottom:1.5px solid black;'><B> &#160;&#160;&#160;&#160;&#160;&#160;6,446,811.23</B></td>"
                                        + "</tr>"
                                        + "</table>"
                                        + "</div>";
            }

            #endregion



            HtmlTemplateToString(PF_YEAR.ToString(), "FINAL_SETTLEMENT", PF_YEAR_DETAIL.ToString(), d_PF_SUM, AmountInWords, disINFO_SETTLE, PRNO);

            MessageBox.Show("File Save Successfully");
        }

        public void HtmlTemplateToString(string getPFYear, string fileName, string PF_YEAR_DETAIL, double d_PF_SUM, string AmountInWords, string disINFO_SETTLE, int PRNO)
        {
           // string Body = ""; // Utilities.FinalSettlementPanel(getPFYear, PF_YEAR_DETAIL, d_PF_SUM, AmountInWords, disINFO_SETTLE, PRNO);

           //// Body = Body.Replace("@PF_YEAR_WISE@", getPFYear);

           // // instantiate the HiQPdf HTML to PDF converter
           // HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
           // //htmlToPdfConverter.HtmlLoadedTimeout = 1200;
           // htmlToPdfConverter.HtmlLoadedTimeout = 9999999;
           // htmlToPdfConverter.Document.PageSize = PdfPageSize.A4;
           // htmlToPdfConverter.Document.FitPageWidth = true;
           // htmlToPdfConverter.Document.ResizePageWidth = true;
           // htmlToPdfConverter.Document.PostCardMode = false;
           // htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Landscape;

           // // hide the button in the created PDF
           // htmlToPdfConverter.HiddenHtmlElements = new string[] { "#convertThisPageButtonDiv" };
           // htmlToPdfConverter.SerialNumber = "CEBhWVhs-bkRhanpp-enE5OCY4-KDkoOigw-ODAoOzkm-OTomMTEx-MQ==";

           // //byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(htmlToConvert, baseUrl);
           // byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(Body, "");

           // string path = txtSaveFilePath.Text + @"\";

           // System.IO.File.WriteAllBytes(System.IO.Path.Combine(path, fileName.Replace('/', '-') + ".pdf"), pdfBuffer);

        }

        public string ConvertAmountToWords(double amt)
        {
            string NumberWords = string.Empty;

            string withDecimal = Convert.ToString(amt);// string.IsNullOrEmpty(dsrow["Tax_Amount"].ToString()) ? "" : dsrow["Tax_Amount"].ToString().Trim();
            string[] cols = withDecimal.Split('.');

            int num = 0;
            string amtComaSplit = string.Empty;
            
            string amtNumber = string.Empty;
            string amtDecimal = string.Empty;
            if (cols.Length > 1)
            {
                amtNumber = cols[0];
                amtDecimal = cols[1];

                num = Convert.ToInt32(amtNumber);
                NumberWords = ConvertToWords(Convert.ToString(num));//ConvertNumbertoWords(num);
                string.Format("{0:#,##0.00}", double.Parse(withDecimal));
            }
            else
            {
                amtNumber = cols[0];
                num = Convert.ToInt32(amtNumber);
                NumberWords = ConvertToWords(Convert.ToString(num));// ConvertNumbertoWords(num);
            }


            return NumberWords;
        }

        private static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents    
                        endStr = "Paisa " + endStr;//Cents    
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }

        private static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        private static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }

        private static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }


        public static string ConvertNumbertoWords(int number)
        {
            if (number == 0)
                return "ZERO";
            if (number < 0)
                return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";

            if ((number / 1000000000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000000000) + " Billion ";
                number %= 1000000000;
            }

            if ((number / 10000000) > 0)
            {
                words += ConvertNumbertoWords(number / 10000000) + " ";
                number %= 10000000;
            }

            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000000) + " MILLION ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "")
                    words += " "; //AND
                string[] unitsMap = { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
                string[] tensMap = { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }

    }
}
