using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    public partial class CHRIS_Gratuity_HistoryUpdatingProcess : ChrisSimpleForm
    {

        #region Constructor
        public CHRIS_Gratuity_HistoryUpdatingProcess()
        {
            InitializeComponent();
        }
        public CHRIS_Gratuity_HistoryUpdatingProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #endregion

        #region Method
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();   

                this.txtOption.Visible = false;
                this.txtSystemDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                this.txtYesNo.Focus();
                lblUserName.Text = "User Name:   "+ this.UserName;
                txtUser.Text = this.userID;
                this.tbtAdd.Visible = false;
                this.tbtDelete.Visible = false;
                this.tbtList.Visible = false;

                this.txtUser.ReadOnly = true;
                this.txtLocation.ReadOnly = true;
                this.txtSystemDate.ReadOnly = true;

                this.txtYesNo.Select();
                this.txtYesNo.Focus();

                this.F10OptionText = "[F10] = Save Record";
                this.F6OptionText = "[F6] = Exit";
                this.ShowF10Option = true;
                this.ShowF6Option = true;

                this.pnlHead.SendToBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        #endregion

        #region Events
        private void txtYesNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.txtYesNo.Text != "Y") && (this.txtYesNo.Text != "N") && (e.KeyChar == '\r'))
            {
                MessageBox.Show("Enter [Y]es...or ..[N]o...");              
            }
            else if ((this.txtYesNo.Text.ToUpper() == "Y") && (e.KeyChar == '\r'))
            {
                try
                {
                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rsltCode = objCmnDataManager.Get("CHRIS_SP_GRATUITY_HISTORY_UPDATION_PROCESS", "");
                    if (rsltCode.isSuccessful)
                    {
                        MessageBox.Show("Process Completed");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        #endregion

    }
}