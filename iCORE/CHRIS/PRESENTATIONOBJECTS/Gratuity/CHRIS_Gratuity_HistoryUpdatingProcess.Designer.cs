namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    partial class CHRIS_Gratuity_HistoryUpdatingProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtYesNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSystemDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(642, 0);
            this.txtOption.Visible = false;
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.txtYesNo);
            this.slPanelSimple1.Controls.Add(this.label8);
            this.slPanelSimple1.Controls.Add(this.label6);
            this.slPanelSimple1.Controls.Add(this.label5);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(46, 124);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(578, 225);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 1;
            // 
            // txtYesNo
            // 
            this.txtYesNo.AllowSpace = true;
            this.txtYesNo.AssociatedLookUpName = "";
            this.txtYesNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYesNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtYesNo.ContinuationTextBox = null;
            this.txtYesNo.CustomEnabled = true;
            this.txtYesNo.DataFieldMapping = "";
            this.txtYesNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYesNo.GetRecordsOnUpDownKeys = false;
            this.txtYesNo.IsDate = false;
            this.txtYesNo.Location = new System.Drawing.Point(379, 101);
            this.txtYesNo.Name = "txtYesNo";
            this.txtYesNo.NumberFormat = "###,###,##0.00";
            this.txtYesNo.Postfix = "";
            this.txtYesNo.Prefix = "";
            this.txtYesNo.Size = new System.Drawing.Size(17, 20);
            this.txtYesNo.SkipValidation = false;
            this.txtYesNo.TabIndex = 112;
            this.txtYesNo.Text = "Y";
            this.txtYesNo.TextType = CrplControlLibrary.TextType.String;
            this.txtYesNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYesNo_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(399, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 13);
            this.label8.TabIndex = 111;
            this.label8.Text = "]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(365, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 13);
            this.label6.TabIndex = 110;
            this.label6.Text = "[";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(167, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 13);
            this.label5.TabIndex = 109;
            this.label5.Text = "Enter [Y]es / [N]o to start process";
            // 
            // txtSystemDate
            // 
            this.txtSystemDate.AllowSpace = true;
            this.txtSystemDate.AssociatedLookUpName = "";
            this.txtSystemDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSystemDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSystemDate.ContinuationTextBox = null;
            this.txtSystemDate.CustomEnabled = true;
            this.txtSystemDate.DataFieldMapping = "";
            this.txtSystemDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSystemDate.GetRecordsOnUpDownKeys = false;
            this.txtSystemDate.IsDate = false;
            this.txtSystemDate.Location = new System.Drawing.Point(479, 27);
            this.txtSystemDate.Name = "txtSystemDate";
            this.txtSystemDate.NumberFormat = "###,###,##0.00";
            this.txtSystemDate.Postfix = "";
            this.txtSystemDate.Prefix = "";
            this.txtSystemDate.Size = new System.Drawing.Size(79, 20);
            this.txtSystemDate.SkipValidation = false;
            this.txtSystemDate.TabIndex = 113;
            this.txtSystemDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(195, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 13);
            this.label2.TabIndex = 108;
            this.label2.Text = "Gratuity History Updation Process";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(233, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 107;
            this.label1.Text = "Gratuity Module";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(431, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 105;
            this.label7.Text = "Date :";
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Enabled = false;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(68, 9);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.Size = new System.Drawing.Size(87, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 104;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 103;
            this.label4.Text = "User :";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Enabled = false;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(68, 32);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.Size = new System.Drawing.Size(87, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 101;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 102;
            this.label3.Text = "Loc :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(472, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 112;
            this.lblUserName.Text = "User Name  :";
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.Controls.Add(this.txtSystemDate);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.label7);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Location = new System.Drawing.Point(46, 57);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(578, 67);
            this.pnlHead.TabIndex = 113;
            // 
            // CHRIS_Gratuity_HistoryUpdatingProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 432);
            this.Controls.Add(this.slPanelSimple1);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.lblUserName);
            this.Name = "CHRIS_Gratuity_HistoryUpdatingProcess";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Gratuity_HistoryUpdatingProcess";
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtYesNo;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox txtSystemDate;
        private System.Windows.Forms.Panel pnlHead;
    }
}