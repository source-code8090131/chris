namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    partial class CHRIS_Gratuity_GratuityReservesReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Gratuity_GratuityReservesReports));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PF_LESS_OTHER_DEDUCTIONS = new CrplControlLibrary.SLTextBox(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.PF_LESS_INCOME_TAX_DEDUCTION = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.PF_OUTSTANDING_VEHICLE_LOAN = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION = new CrplControlLibrary.SLTextBox(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS = new CrplControlLibrary.SLTextBox(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.PF_LESS_SALARY_REVERSAL_DAYS = new CrplControlLibrary.SLTextBox(this.components);
            this.PF_LESS_ALLOWNCES_REVERSAL = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.PF_ADD_OTHER_PAYMENTS_2 = new CrplControlLibrary.SLTextBox(this.components);
            this.PF_ADD_OTHER_PAYMENTS_1 = new CrplControlLibrary.SLTextBox(this.components);
            this.PF_ADD_INCENTIVE_AWARD = new CrplControlLibrary.SLTextBox(this.components);
            this.PF_ADD_DEFFERED_CASH = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PF_ADD_ALLOWNCES_PAYMENT = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSaveFilePath = new System.Windows.Forms.TextBox();
            this.btnSaveFilePath = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.PF_LESS_ADVANCE_STLMNT_PAID = new CrplControlLibrary.SLTextBox(this.components);
            this.slButton2 = new CrplControlLibrary.SLButton();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.txt_PR_NO_SETTLE = new CrplControlLibrary.SLTextBox(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.txt_OUT_BAL_SETTLE = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.PF_ADD_UNPAID_SALARY_DAYS = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PF_LESS_CITI_OUTSTANDING_LOAN = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.today = new CrplControlLibrary.SLDatePicker(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFinalSettlement = new System.Windows.Forms.Button();
            this.folderBrwDialog = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PF_LESS_OTHER_DEDUCTIONS);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.PF_LESS_INCOME_TAX_DEDUCTION);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.PF_OUTSTANDING_VEHICLE_LOAN);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.PF_LESS_BAF_CAR_LOAN_DEDUCTION);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.PF_LESS_SALARY_REVERSAL_DAYS);
            this.groupBox1.Controls.Add(this.PF_LESS_ALLOWNCES_REVERSAL);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.PF_ADD_OTHER_PAYMENTS_2);
            this.groupBox1.Controls.Add(this.PF_ADD_OTHER_PAYMENTS_1);
            this.groupBox1.Controls.Add(this.PF_ADD_INCENTIVE_AWARD);
            this.groupBox1.Controls.Add(this.PF_ADD_DEFFERED_CASH);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.PF_ADD_ALLOWNCES_PAYMENT);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtSaveFilePath);
            this.groupBox1.Controls.Add(this.btnSaveFilePath);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.PF_LESS_ADVANCE_STLMNT_PAID);
            this.groupBox1.Controls.Add(this.slButton2);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.txt_PR_NO_SETTLE);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txt_OUT_BAL_SETTLE);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.PF_ADD_BAF_LOAN_SUBSIDY_PAY);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.PF_ADD_UNPAID_SALARY_DAYS);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.PF_ADD_LEAVE_ENCASHMENT_DAYS);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.PF_LESS_CITI_OUTSTANDING_LOAN);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.PF_ADD_REVERSAL_OWN_CONT_DAYS);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.today);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(516, 457);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // PF_LESS_OTHER_DEDUCTIONS
            // 
            this.PF_LESS_OTHER_DEDUCTIONS.AllowSpace = true;
            this.PF_LESS_OTHER_DEDUCTIONS.AssociatedLookUpName = "";
            this.PF_LESS_OTHER_DEDUCTIONS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_OTHER_DEDUCTIONS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_OTHER_DEDUCTIONS.ContinuationTextBox = null;
            this.PF_LESS_OTHER_DEDUCTIONS.CustomEnabled = true;
            this.PF_LESS_OTHER_DEDUCTIONS.DataFieldMapping = "PF_LESS_OTHER_DEDUCTIONS";
            this.PF_LESS_OTHER_DEDUCTIONS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_OTHER_DEDUCTIONS.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_OTHER_DEDUCTIONS.IsDate = false;
            this.PF_LESS_OTHER_DEDUCTIONS.Location = new System.Drawing.Point(440, 417);
            this.PF_LESS_OTHER_DEDUCTIONS.MaxLength = 9;
            this.PF_LESS_OTHER_DEDUCTIONS.Name = "PF_LESS_OTHER_DEDUCTIONS";
            this.PF_LESS_OTHER_DEDUCTIONS.NumberFormat = "###,###,##0.00";
            this.PF_LESS_OTHER_DEDUCTIONS.Postfix = "";
            this.PF_LESS_OTHER_DEDUCTIONS.Prefix = "";
            this.PF_LESS_OTHER_DEDUCTIONS.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_OTHER_DEDUCTIONS.SkipValidation = false;
            this.PF_LESS_OTHER_DEDUCTIONS.TabIndex = 22;
            this.PF_LESS_OTHER_DEDUCTIONS.Text = "0";
            this.PF_LESS_OTHER_DEDUCTIONS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(248, 424);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(106, 13);
            this.label31.TabIndex = 108;
            this.label31.Text = "Other Deductions";
            // 
            // PF_LESS_INCOME_TAX_DEDUCTION
            // 
            this.PF_LESS_INCOME_TAX_DEDUCTION.AllowSpace = true;
            this.PF_LESS_INCOME_TAX_DEDUCTION.AssociatedLookUpName = "";
            this.PF_LESS_INCOME_TAX_DEDUCTION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_INCOME_TAX_DEDUCTION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_INCOME_TAX_DEDUCTION.ContinuationTextBox = null;
            this.PF_LESS_INCOME_TAX_DEDUCTION.CustomEnabled = true;
            this.PF_LESS_INCOME_TAX_DEDUCTION.DataFieldMapping = "PF_LESS_INCOME_TAX_DEDUCTION";
            this.PF_LESS_INCOME_TAX_DEDUCTION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_INCOME_TAX_DEDUCTION.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_INCOME_TAX_DEDUCTION.IsDate = false;
            this.PF_LESS_INCOME_TAX_DEDUCTION.Location = new System.Drawing.Point(440, 381);
            this.PF_LESS_INCOME_TAX_DEDUCTION.MaxLength = 9;
            this.PF_LESS_INCOME_TAX_DEDUCTION.Name = "PF_LESS_INCOME_TAX_DEDUCTION";
            this.PF_LESS_INCOME_TAX_DEDUCTION.NumberFormat = "###,###,##0.00";
            this.PF_LESS_INCOME_TAX_DEDUCTION.Postfix = "";
            this.PF_LESS_INCOME_TAX_DEDUCTION.Prefix = "";
            this.PF_LESS_INCOME_TAX_DEDUCTION.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_INCOME_TAX_DEDUCTION.SkipValidation = false;
            this.PF_LESS_INCOME_TAX_DEDUCTION.TabIndex = 21;
            this.PF_LESS_INCOME_TAX_DEDUCTION.Text = "0";
            this.PF_LESS_INCOME_TAX_DEDUCTION.TextType = CrplControlLibrary.TextType.String;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(250, 375);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(138, 39);
            this.label30.TabIndex = 106;
            this.label30.Text = "Income Tax deduct on \r\nSalary and Leave \r\nEncashment";
            // 
            // PF_OUTSTANDING_VEHICLE_LOAN
            // 
            this.PF_OUTSTANDING_VEHICLE_LOAN.AllowSpace = true;
            this.PF_OUTSTANDING_VEHICLE_LOAN.AssociatedLookUpName = "";
            this.PF_OUTSTANDING_VEHICLE_LOAN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_OUTSTANDING_VEHICLE_LOAN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_OUTSTANDING_VEHICLE_LOAN.ContinuationTextBox = null;
            this.PF_OUTSTANDING_VEHICLE_LOAN.CustomEnabled = true;
            this.PF_OUTSTANDING_VEHICLE_LOAN.DataFieldMapping = "PF_OUTSTANDING_VEHICLE_LOAN";
            this.PF_OUTSTANDING_VEHICLE_LOAN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_OUTSTANDING_VEHICLE_LOAN.GetRecordsOnUpDownKeys = false;
            this.PF_OUTSTANDING_VEHICLE_LOAN.IsDate = false;
            this.PF_OUTSTANDING_VEHICLE_LOAN.Location = new System.Drawing.Point(440, 354);
            this.PF_OUTSTANDING_VEHICLE_LOAN.MaxLength = 9;
            this.PF_OUTSTANDING_VEHICLE_LOAN.Name = "PF_OUTSTANDING_VEHICLE_LOAN";
            this.PF_OUTSTANDING_VEHICLE_LOAN.NumberFormat = "###,###,##0.00";
            this.PF_OUTSTANDING_VEHICLE_LOAN.Postfix = "";
            this.PF_OUTSTANDING_VEHICLE_LOAN.Prefix = "";
            this.PF_OUTSTANDING_VEHICLE_LOAN.Size = new System.Drawing.Size(70, 20);
            this.PF_OUTSTANDING_VEHICLE_LOAN.SkipValidation = false;
            this.PF_OUTSTANDING_VEHICLE_LOAN.TabIndex = 20;
            this.PF_OUTSTANDING_VEHICLE_LOAN.Text = "0";
            this.PF_OUTSTANDING_VEHICLE_LOAN.TextType = CrplControlLibrary.TextType.String;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(248, 355);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(173, 13);
            this.label29.TabIndex = 104;
            this.label29.Text = "Outstanding 0% Vehicle Loan";
            // 
            // PF_LESS_BAF_CAR_LOAN_DEDUCTION
            // 
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.AllowSpace = true;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.AssociatedLookUpName = "";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.ContinuationTextBox = null;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.CustomEnabled = true;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.DataFieldMapping = "PF_LESS_BAF_CAR_LOAN_DEDUCTION";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.IsDate = false;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Location = new System.Drawing.Point(440, 327);
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.MaxLength = 9;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Name = "PF_LESS_BAF_CAR_LOAN_DEDUCTION";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.NumberFormat = "###,###,##0.00";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Postfix = "";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Prefix = "";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.SkipValidation = false;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.TabIndex = 19;
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.Text = "0";
            this.PF_LESS_BAF_CAR_LOAN_DEDUCTION.TextType = CrplControlLibrary.TextType.String;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(249, 329);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(147, 13);
            this.label28.TabIndex = 102;
            this.label28.Text = "BAF Car Loan Deduction";
            // 
            // PF_LESS_BAF_HOUSE_LOAN_DEDUCTION
            // 
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.AllowSpace = true;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.AssociatedLookUpName = "";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.ContinuationTextBox = null;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.CustomEnabled = true;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.DataFieldMapping = "PF_LESS_BAF_HOUSE_LOAN_DEDUCTION";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.IsDate = false;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Location = new System.Drawing.Point(441, 303);
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.MaxLength = 9;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Name = "PF_LESS_BAF_HOUSE_LOAN_DEDUCTION";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.NumberFormat = "###,###,##0.00";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Postfix = "";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Prefix = "";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.SkipValidation = false;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.TabIndex = 18;
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Text = "0";
            this.PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.TextType = CrplControlLibrary.TextType.String;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(249, 305);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(147, 13);
            this.label27.TabIndex = 100;
            this.label27.Text = "BAF House Loan Deduct";
            // 
            // PF_LESS_PF_REVERSAL_OWN_CONT_DAYS
            // 
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.AllowSpace = true;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.AssociatedLookUpName = "";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.ContinuationTextBox = null;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.CustomEnabled = true;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.DataFieldMapping = "PF_LESS_PF_REVERSAL_OWN_CONT_DAYS";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.IsDate = false;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Location = new System.Drawing.Point(440, 274);
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.MaxLength = 9;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Name = "PF_LESS_PF_REVERSAL_OWN_CONT_DAYS";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.NumberFormat = "###,###,##0.00";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Postfix = "";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Prefix = "";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.SkipValidation = false;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.TabIndex = 17;
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Text = "0";
            this.PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(248, 270);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(125, 26);
            this.label26.TabIndex = 98;
            this.label26.Text = "PF Reversal on own \r\nContribution (Days)";
            // 
            // PF_LESS_SALARY_REVERSAL_DAYS
            // 
            this.PF_LESS_SALARY_REVERSAL_DAYS.AllowSpace = true;
            this.PF_LESS_SALARY_REVERSAL_DAYS.AssociatedLookUpName = "";
            this.PF_LESS_SALARY_REVERSAL_DAYS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_SALARY_REVERSAL_DAYS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_SALARY_REVERSAL_DAYS.ContinuationTextBox = null;
            this.PF_LESS_SALARY_REVERSAL_DAYS.CustomEnabled = true;
            this.PF_LESS_SALARY_REVERSAL_DAYS.DataFieldMapping = "PF_LESS_SALARY_REVERSAL_DAYS";
            this.PF_LESS_SALARY_REVERSAL_DAYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_SALARY_REVERSAL_DAYS.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_SALARY_REVERSAL_DAYS.IsDate = false;
            this.PF_LESS_SALARY_REVERSAL_DAYS.Location = new System.Drawing.Point(440, 247);
            this.PF_LESS_SALARY_REVERSAL_DAYS.MaxLength = 9;
            this.PF_LESS_SALARY_REVERSAL_DAYS.Name = "PF_LESS_SALARY_REVERSAL_DAYS";
            this.PF_LESS_SALARY_REVERSAL_DAYS.NumberFormat = "###,###,##0.00";
            this.PF_LESS_SALARY_REVERSAL_DAYS.Postfix = "";
            this.PF_LESS_SALARY_REVERSAL_DAYS.Prefix = "";
            this.PF_LESS_SALARY_REVERSAL_DAYS.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_SALARY_REVERSAL_DAYS.SkipValidation = false;
            this.PF_LESS_SALARY_REVERSAL_DAYS.TabIndex = 16;
            this.PF_LESS_SALARY_REVERSAL_DAYS.Text = "0";
            this.PF_LESS_SALARY_REVERSAL_DAYS.TextType = CrplControlLibrary.TextType.String;
            // 
            // PF_LESS_ALLOWNCES_REVERSAL
            // 
            this.PF_LESS_ALLOWNCES_REVERSAL.AllowSpace = true;
            this.PF_LESS_ALLOWNCES_REVERSAL.AssociatedLookUpName = "";
            this.PF_LESS_ALLOWNCES_REVERSAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_ALLOWNCES_REVERSAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_ALLOWNCES_REVERSAL.ContinuationTextBox = null;
            this.PF_LESS_ALLOWNCES_REVERSAL.CustomEnabled = true;
            this.PF_LESS_ALLOWNCES_REVERSAL.DataFieldMapping = "PF_LESS_ALLOWNCES_REVERSAL";
            this.PF_LESS_ALLOWNCES_REVERSAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_ALLOWNCES_REVERSAL.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_ALLOWNCES_REVERSAL.IsDate = false;
            this.PF_LESS_ALLOWNCES_REVERSAL.Location = new System.Drawing.Point(440, 224);
            this.PF_LESS_ALLOWNCES_REVERSAL.MaxLength = 9;
            this.PF_LESS_ALLOWNCES_REVERSAL.Name = "PF_LESS_ALLOWNCES_REVERSAL";
            this.PF_LESS_ALLOWNCES_REVERSAL.NumberFormat = "###,###,##0.00";
            this.PF_LESS_ALLOWNCES_REVERSAL.Postfix = "";
            this.PF_LESS_ALLOWNCES_REVERSAL.Prefix = "";
            this.PF_LESS_ALLOWNCES_REVERSAL.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_ALLOWNCES_REVERSAL.SkipValidation = false;
            this.PF_LESS_ALLOWNCES_REVERSAL.TabIndex = 15;
            this.PF_LESS_ALLOWNCES_REVERSAL.Text = "0";
            this.PF_LESS_ALLOWNCES_REVERSAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(248, 248);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(136, 13);
            this.label25.TabIndex = 95;
            this.label25.Text = "Salary Reversal (Days)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(248, 225);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(125, 13);
            this.label24.TabIndex = 94;
            this.label24.Text = "Allowances Reversal";
            // 
            // PF_ADD_OTHER_PAYMENTS_2
            // 
            this.PF_ADD_OTHER_PAYMENTS_2.AllowSpace = true;
            this.PF_ADD_OTHER_PAYMENTS_2.AssociatedLookUpName = "";
            this.PF_ADD_OTHER_PAYMENTS_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_OTHER_PAYMENTS_2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_OTHER_PAYMENTS_2.ContinuationTextBox = null;
            this.PF_ADD_OTHER_PAYMENTS_2.CustomEnabled = true;
            this.PF_ADD_OTHER_PAYMENTS_2.DataFieldMapping = "PF_ADD_OTHER_PAYMENTS_2";
            this.PF_ADD_OTHER_PAYMENTS_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_OTHER_PAYMENTS_2.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_OTHER_PAYMENTS_2.IsDate = false;
            this.PF_ADD_OTHER_PAYMENTS_2.Location = new System.Drawing.Point(160, 311);
            this.PF_ADD_OTHER_PAYMENTS_2.MaxLength = 9;
            this.PF_ADD_OTHER_PAYMENTS_2.Name = "PF_ADD_OTHER_PAYMENTS_2";
            this.PF_ADD_OTHER_PAYMENTS_2.NumberFormat = "###,###,##0.00";
            this.PF_ADD_OTHER_PAYMENTS_2.Postfix = "";
            this.PF_ADD_OTHER_PAYMENTS_2.Prefix = "";
            this.PF_ADD_OTHER_PAYMENTS_2.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_OTHER_PAYMENTS_2.SkipValidation = false;
            this.PF_ADD_OTHER_PAYMENTS_2.TabIndex = 10;
            this.PF_ADD_OTHER_PAYMENTS_2.Text = "0";
            this.PF_ADD_OTHER_PAYMENTS_2.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // PF_ADD_OTHER_PAYMENTS_1
            // 
            this.PF_ADD_OTHER_PAYMENTS_1.AllowSpace = true;
            this.PF_ADD_OTHER_PAYMENTS_1.AssociatedLookUpName = "";
            this.PF_ADD_OTHER_PAYMENTS_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_OTHER_PAYMENTS_1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_OTHER_PAYMENTS_1.ContinuationTextBox = null;
            this.PF_ADD_OTHER_PAYMENTS_1.CustomEnabled = true;
            this.PF_ADD_OTHER_PAYMENTS_1.DataFieldMapping = "PF_ADD_OTHER_PAYMENTS_1";
            this.PF_ADD_OTHER_PAYMENTS_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_OTHER_PAYMENTS_1.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_OTHER_PAYMENTS_1.IsDate = false;
            this.PF_ADD_OTHER_PAYMENTS_1.Location = new System.Drawing.Point(160, 288);
            this.PF_ADD_OTHER_PAYMENTS_1.MaxLength = 9;
            this.PF_ADD_OTHER_PAYMENTS_1.Name = "PF_ADD_OTHER_PAYMENTS_1";
            this.PF_ADD_OTHER_PAYMENTS_1.NumberFormat = "###,###,##0.00";
            this.PF_ADD_OTHER_PAYMENTS_1.Postfix = "";
            this.PF_ADD_OTHER_PAYMENTS_1.Prefix = "";
            this.PF_ADD_OTHER_PAYMENTS_1.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_OTHER_PAYMENTS_1.SkipValidation = false;
            this.PF_ADD_OTHER_PAYMENTS_1.TabIndex = 9;
            this.PF_ADD_OTHER_PAYMENTS_1.Text = "0";
            this.PF_ADD_OTHER_PAYMENTS_1.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // PF_ADD_INCENTIVE_AWARD
            // 
            this.PF_ADD_INCENTIVE_AWARD.AllowSpace = true;
            this.PF_ADD_INCENTIVE_AWARD.AssociatedLookUpName = "";
            this.PF_ADD_INCENTIVE_AWARD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_INCENTIVE_AWARD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_INCENTIVE_AWARD.ContinuationTextBox = null;
            this.PF_ADD_INCENTIVE_AWARD.CustomEnabled = true;
            this.PF_ADD_INCENTIVE_AWARD.DataFieldMapping = "PF_ADD_INCENTIVE_AWARD";
            this.PF_ADD_INCENTIVE_AWARD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_INCENTIVE_AWARD.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_INCENTIVE_AWARD.IsDate = false;
            this.PF_ADD_INCENTIVE_AWARD.Location = new System.Drawing.Point(160, 264);
            this.PF_ADD_INCENTIVE_AWARD.MaxLength = 9;
            this.PF_ADD_INCENTIVE_AWARD.Name = "PF_ADD_INCENTIVE_AWARD";
            this.PF_ADD_INCENTIVE_AWARD.NumberFormat = "###,###,##0.00";
            this.PF_ADD_INCENTIVE_AWARD.Postfix = "";
            this.PF_ADD_INCENTIVE_AWARD.Prefix = "";
            this.PF_ADD_INCENTIVE_AWARD.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_INCENTIVE_AWARD.SkipValidation = false;
            this.PF_ADD_INCENTIVE_AWARD.TabIndex = 8;
            this.PF_ADD_INCENTIVE_AWARD.Text = "0";
            this.PF_ADD_INCENTIVE_AWARD.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // PF_ADD_DEFFERED_CASH
            // 
            this.PF_ADD_DEFFERED_CASH.AllowSpace = true;
            this.PF_ADD_DEFFERED_CASH.AssociatedLookUpName = "";
            this.PF_ADD_DEFFERED_CASH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_DEFFERED_CASH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_DEFFERED_CASH.ContinuationTextBox = null;
            this.PF_ADD_DEFFERED_CASH.CustomEnabled = true;
            this.PF_ADD_DEFFERED_CASH.DataFieldMapping = "PF_ADD_DEFFERED_CASH";
            this.PF_ADD_DEFFERED_CASH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_DEFFERED_CASH.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_DEFFERED_CASH.IsDate = false;
            this.PF_ADD_DEFFERED_CASH.Location = new System.Drawing.Point(160, 242);
            this.PF_ADD_DEFFERED_CASH.MaxLength = 9;
            this.PF_ADD_DEFFERED_CASH.Name = "PF_ADD_DEFFERED_CASH";
            this.PF_ADD_DEFFERED_CASH.NumberFormat = "###,###,##0.00";
            this.PF_ADD_DEFFERED_CASH.Postfix = "";
            this.PF_ADD_DEFFERED_CASH.Prefix = "";
            this.PF_ADD_DEFFERED_CASH.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_DEFFERED_CASH.SkipValidation = false;
            this.PF_ADD_DEFFERED_CASH.TabIndex = 7;
            this.PF_ADD_DEFFERED_CASH.Text = "0";
            this.PF_ADD_DEFFERED_CASH.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1, 312);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 13);
            this.label23.TabIndex = 89;
            this.label23.Text = "Other Payments 2";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1, 289);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 13);
            this.label21.TabIndex = 88;
            this.label21.Text = "Other Payments 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 13);
            this.label10.TabIndex = 87;
            this.label10.Text = "Incentive Award";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "Deferred Cash";
            // 
            // PF_ADD_ALLOWNCES_PAYMENT
            // 
            this.PF_ADD_ALLOWNCES_PAYMENT.AllowSpace = true;
            this.PF_ADD_ALLOWNCES_PAYMENT.AssociatedLookUpName = "";
            this.PF_ADD_ALLOWNCES_PAYMENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_ALLOWNCES_PAYMENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_ALLOWNCES_PAYMENT.ContinuationTextBox = null;
            this.PF_ADD_ALLOWNCES_PAYMENT.CustomEnabled = true;
            this.PF_ADD_ALLOWNCES_PAYMENT.DataFieldMapping = "PF_ADD_ALLOWNCES_PAYMENT";
            this.PF_ADD_ALLOWNCES_PAYMENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_ALLOWNCES_PAYMENT.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_ALLOWNCES_PAYMENT.IsDate = false;
            this.PF_ADD_ALLOWNCES_PAYMENT.Location = new System.Drawing.Point(160, 197);
            this.PF_ADD_ALLOWNCES_PAYMENT.MaxLength = 8;
            this.PF_ADD_ALLOWNCES_PAYMENT.Name = "PF_ADD_ALLOWNCES_PAYMENT";
            this.PF_ADD_ALLOWNCES_PAYMENT.NumberFormat = "###,###,##0.00";
            this.PF_ADD_ALLOWNCES_PAYMENT.Postfix = "";
            this.PF_ADD_ALLOWNCES_PAYMENT.Prefix = "";
            this.PF_ADD_ALLOWNCES_PAYMENT.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_ALLOWNCES_PAYMENT.SkipValidation = false;
            this.PF_ADD_ALLOWNCES_PAYMENT.TabIndex = 5;
            this.PF_ADD_ALLOWNCES_PAYMENT.Text = "0";
            this.PF_ADD_ALLOWNCES_PAYMENT.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 84;
            this.label5.Text = "Allowances Payment";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(253, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 17);
            this.label12.TabIndex = 83;
            this.label12.Text = "LESS :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 82;
            this.label3.Text = "ADD :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 17);
            this.label11.TabIndex = 80;
            this.label11.Text = "Save File Path";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(-2, 151);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(161, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Leave Encashment in Days";
            // 
            // txtSaveFilePath
            // 
            this.txtSaveFilePath.Location = new System.Drawing.Point(120, 98);
            this.txtSaveFilePath.Name = "txtSaveFilePath";
            this.txtSaveFilePath.ReadOnly = true;
            this.txtSaveFilePath.Size = new System.Drawing.Size(226, 20);
            this.txtSaveFilePath.TabIndex = 1;
            // 
            // btnSaveFilePath
            // 
            this.btnSaveFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveFilePath.Location = new System.Drawing.Point(361, 97);
            this.btnSaveFilePath.Name = "btnSaveFilePath";
            this.btnSaveFilePath.Size = new System.Drawing.Size(37, 19);
            this.btnSaveFilePath.TabIndex = 2;
            this.btnSaveFilePath.Text = "...";
            this.btnSaveFilePath.UseVisualStyleBackColor = true;
            this.btnSaveFilePath.Click += new System.EventHandler(this.btnSaveFilePath_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(449, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 78;
            this.pictureBox2.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(248, 174);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(154, 26);
            this.label22.TabIndex = 23;
            this.label22.Text = "Advance Settlement Paid \r\nAdjustment";
            // 
            // PF_LESS_ADVANCE_STLMNT_PAID
            // 
            this.PF_LESS_ADVANCE_STLMNT_PAID.AllowSpace = true;
            this.PF_LESS_ADVANCE_STLMNT_PAID.AssociatedLookUpName = "";
            this.PF_LESS_ADVANCE_STLMNT_PAID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_ADVANCE_STLMNT_PAID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_ADVANCE_STLMNT_PAID.ContinuationTextBox = null;
            this.PF_LESS_ADVANCE_STLMNT_PAID.CustomEnabled = true;
            this.PF_LESS_ADVANCE_STLMNT_PAID.DataFieldMapping = "PF_LESS_ADVANCE_STLMNT_PAID";
            this.PF_LESS_ADVANCE_STLMNT_PAID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_ADVANCE_STLMNT_PAID.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_ADVANCE_STLMNT_PAID.IsDate = false;
            this.PF_LESS_ADVANCE_STLMNT_PAID.Location = new System.Drawing.Point(440, 172);
            this.PF_LESS_ADVANCE_STLMNT_PAID.MaxLength = 9;
            this.PF_LESS_ADVANCE_STLMNT_PAID.Name = "PF_LESS_ADVANCE_STLMNT_PAID";
            this.PF_LESS_ADVANCE_STLMNT_PAID.NumberFormat = "###,###,##0.00";
            this.PF_LESS_ADVANCE_STLMNT_PAID.Postfix = "";
            this.PF_LESS_ADVANCE_STLMNT_PAID.Prefix = "";
            this.PF_LESS_ADVANCE_STLMNT_PAID.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_ADVANCE_STLMNT_PAID.SkipValidation = false;
            this.PF_LESS_ADVANCE_STLMNT_PAID.TabIndex = 13;
            this.PF_LESS_ADVANCE_STLMNT_PAID.Text = "0";
            this.PF_LESS_ADVANCE_STLMNT_PAID.TextType = CrplControlLibrary.TextType.Integer;
            this.PF_LESS_ADVANCE_STLMNT_PAID.TextChanged += new System.EventHandler(this.P_END_TextChanged);
            // 
            // slButton2
            // 
            this.slButton2.ActionType = "";
            this.slButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton2.Location = new System.Drawing.Point(80, 419);
            this.slButton2.Name = "slButton2";
            this.slButton2.Size = new System.Drawing.Size(58, 23);
            this.slButton2.TabIndex = 24;
            this.slButton2.Text = "Close";
            this.slButton2.UseVisualStyleBackColor = true;
            this.slButton2.Click += new System.EventHandler(this.slButton2_Click);
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(13, 419);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(58, 23);
            this.slButton1.TabIndex = 23;
            this.slButton1.Text = "Run";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // txt_PR_NO_SETTLE
            // 
            this.txt_PR_NO_SETTLE.AllowSpace = true;
            this.txt_PR_NO_SETTLE.AssociatedLookUpName = "";
            this.txt_PR_NO_SETTLE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NO_SETTLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NO_SETTLE.ContinuationTextBox = null;
            this.txt_PR_NO_SETTLE.CustomEnabled = true;
            this.txt_PR_NO_SETTLE.DataFieldMapping = "";
            this.txt_PR_NO_SETTLE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NO_SETTLE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NO_SETTLE.IsDate = false;
            this.txt_PR_NO_SETTLE.Location = new System.Drawing.Point(120, 72);
            this.txt_PR_NO_SETTLE.MaxLength = 6;
            this.txt_PR_NO_SETTLE.Name = "txt_PR_NO_SETTLE";
            this.txt_PR_NO_SETTLE.NumberFormat = "###,###,##0.00";
            this.txt_PR_NO_SETTLE.Postfix = "";
            this.txt_PR_NO_SETTLE.Prefix = "";
            this.txt_PR_NO_SETTLE.Size = new System.Drawing.Size(135, 20);
            this.txt_PR_NO_SETTLE.SkipValidation = false;
            this.txt_PR_NO_SETTLE.TabIndex = 0;
            this.txt_PR_NO_SETTLE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(249, 205);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(172, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Outstanding Balance of Loan";
            // 
            // txt_OUT_BAL_SETTLE
            // 
            this.txt_OUT_BAL_SETTLE.AllowSpace = true;
            this.txt_OUT_BAL_SETTLE.AssociatedLookUpName = "";
            this.txt_OUT_BAL_SETTLE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OUT_BAL_SETTLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OUT_BAL_SETTLE.ContinuationTextBox = null;
            this.txt_OUT_BAL_SETTLE.CustomEnabled = true;
            this.txt_OUT_BAL_SETTLE.DataFieldMapping = "";
            this.txt_OUT_BAL_SETTLE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OUT_BAL_SETTLE.GetRecordsOnUpDownKeys = false;
            this.txt_OUT_BAL_SETTLE.IsDate = false;
            this.txt_OUT_BAL_SETTLE.Location = new System.Drawing.Point(440, 201);
            this.txt_OUT_BAL_SETTLE.MaxLength = 9;
            this.txt_OUT_BAL_SETTLE.Name = "txt_OUT_BAL_SETTLE";
            this.txt_OUT_BAL_SETTLE.NumberFormat = "###,###,##0.00";
            this.txt_OUT_BAL_SETTLE.Postfix = "";
            this.txt_OUT_BAL_SETTLE.Prefix = "";
            this.txt_OUT_BAL_SETTLE.Size = new System.Drawing.Size(70, 20);
            this.txt_OUT_BAL_SETTLE.SkipValidation = false;
            this.txt_OUT_BAL_SETTLE.TabIndex = 14;
            this.txt_OUT_BAL_SETTLE.Text = "0";
            this.txt_OUT_BAL_SETTLE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(31, 260);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(0, 13);
            this.label19.TabIndex = 14;
            // 
            // PF_ADD_BAF_LOAN_SUBSIDY_PAY
            // 
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.AllowSpace = true;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.AssociatedLookUpName = "";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.ContinuationTextBox = null;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.CustomEnabled = true;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.DataFieldMapping = "PF_ADD_BAF_LOAN_SUBSIDY_PAY";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.IsDate = false;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Location = new System.Drawing.Point(160, 219);
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.MaxLength = 9;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Name = "PF_ADD_BAF_LOAN_SUBSIDY_PAY";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.NumberFormat = "###,###,##0.00";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Postfix = "";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Prefix = "";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.SkipValidation = false;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.TabIndex = 6;
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Text = "0";
            this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "BAF loan subsidy";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(31, 232);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 12;
            // 
            // PF_ADD_UNPAID_SALARY_DAYS
            // 
            this.PF_ADD_UNPAID_SALARY_DAYS.AllowSpace = true;
            this.PF_ADD_UNPAID_SALARY_DAYS.AssociatedLookUpName = "";
            this.PF_ADD_UNPAID_SALARY_DAYS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_UNPAID_SALARY_DAYS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_UNPAID_SALARY_DAYS.ContinuationTextBox = null;
            this.PF_ADD_UNPAID_SALARY_DAYS.CustomEnabled = true;
            this.PF_ADD_UNPAID_SALARY_DAYS.DataFieldMapping = "PF_ADD_UNPAID_SALARY_DAYS";
            this.PF_ADD_UNPAID_SALARY_DAYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_UNPAID_SALARY_DAYS.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_UNPAID_SALARY_DAYS.IsDate = false;
            this.PF_ADD_UNPAID_SALARY_DAYS.Location = new System.Drawing.Point(160, 175);
            this.PF_ADD_UNPAID_SALARY_DAYS.MaxLength = 4;
            this.PF_ADD_UNPAID_SALARY_DAYS.Name = "PF_ADD_UNPAID_SALARY_DAYS";
            this.PF_ADD_UNPAID_SALARY_DAYS.NumberFormat = "###,###,##0.00";
            this.PF_ADD_UNPAID_SALARY_DAYS.Postfix = "";
            this.PF_ADD_UNPAID_SALARY_DAYS.Prefix = "";
            this.PF_ADD_UNPAID_SALARY_DAYS.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_UNPAID_SALARY_DAYS.SkipValidation = false;
            this.PF_ADD_UNPAID_SALARY_DAYS.TabIndex = 4;
            this.PF_ADD_UNPAID_SALARY_DAYS.Text = "0";
            this.PF_ADD_UNPAID_SALARY_DAYS.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(150, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Unpaid Salary No # Days";
            // 
            // PF_ADD_LEAVE_ENCASHMENT_DAYS
            // 
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.AllowSpace = true;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.AssociatedLookUpName = "";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.ContinuationTextBox = null;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.CustomEnabled = true;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.DataFieldMapping = "PF_ADD_LEAVE_ENCASHMENT_DAYS";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.IsDate = false;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Location = new System.Drawing.Point(160, 149);
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.MaxLength = 2;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Name = "PF_ADD_LEAVE_ENCASHMENT_DAYS";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.NumberFormat = "###,###,##0.00";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Postfix = "";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Prefix = "";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.SkipValidation = false;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.TabIndex = 3;
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Text = "0";
            this.PF_ADD_LEAVE_ENCASHMENT_DAYS.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(31, 180);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(248, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Citi Outstanding Loan";
            // 
            // PF_LESS_CITI_OUTSTANDING_LOAN
            // 
            this.PF_LESS_CITI_OUTSTANDING_LOAN.AllowSpace = true;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.AssociatedLookUpName = "";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.ContinuationTextBox = null;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.CustomEnabled = true;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.DataFieldMapping = "PF_LESS_CITI_OUTSTANDING_LOAN";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_LESS_CITI_OUTSTANDING_LOAN.GetRecordsOnUpDownKeys = false;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.IsDate = false;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Location = new System.Drawing.Point(440, 149);
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Name = "PF_LESS_CITI_OUTSTANDING_LOAN";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.NumberFormat = "###,###,##0.00";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Postfix = "";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Prefix = "";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Size = new System.Drawing.Size(70, 20);
            this.PF_LESS_CITI_OUTSTANDING_LOAN.SkipValidation = false;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.TabIndex = 12;
            this.PF_LESS_CITI_OUTSTANDING_LOAN.Text = "0";
            this.PF_LESS_CITI_OUTSTANDING_LOAN.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1, 339);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 26);
            this.label15.TabIndex = 6;
            this.label15.Text = "PF Reversal on Own \r\nContribution (Days)";
            // 
            // PF_ADD_REVERSAL_OWN_CONT_DAYS
            // 
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.AllowSpace = true;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.AssociatedLookUpName = "";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.ContinuationTextBox = null;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.CustomEnabled = true;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.DataFieldMapping = "PF_ADD_REVERSAL_OWN_CONT_DAYS";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.GetRecordsOnUpDownKeys = false;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.IsDate = false;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Location = new System.Drawing.Point(160, 337);
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Name = "PF_ADD_REVERSAL_OWN_CONT_DAYS";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.NumberFormat = "###,###,##0.00";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Postfix = "";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Prefix = "";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Size = new System.Drawing.Size(83, 20);
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.SkipValidation = false;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.TabIndex = 11;
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.Text = "0";
            this.PF_ADD_REVERSAL_OWN_CONT_DAYS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(31, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Personnel No";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Today";
            this.label13.Visible = false;
            // 
            // today
            // 
            this.today.CustomEnabled = true;
            this.today.CustomFormat = "dd/MM/yyyy";
            this.today.DataFieldMapping = "";
            this.today.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.today.HasChanges = true;
            this.today.Location = new System.Drawing.Point(3, 12);
            this.today.Name = "today";
            this.today.NullValue = " ";
            this.today.Size = new System.Drawing.Size(135, 20);
            this.today.TabIndex = 3;
            this.today.Value = new System.DateTime(2010, 12, 29, 0, 0, 0, 0);
            this.today.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(118, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter values for the parameters";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(148, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Report Parameters";
            // 
            // btnFinalSettlement
            // 
            this.btnFinalSettlement.Location = new System.Drawing.Point(263, 514);
            this.btnFinalSettlement.Margin = new System.Windows.Forms.Padding(2);
            this.btnFinalSettlement.Name = "btnFinalSettlement";
            this.btnFinalSettlement.Size = new System.Drawing.Size(104, 34);
            this.btnFinalSettlement.TabIndex = 1;
            this.btnFinalSettlement.Text = "Final Settlement";
            this.btnFinalSettlement.UseVisualStyleBackColor = true;
            this.btnFinalSettlement.Visible = false;
            this.btnFinalSettlement.Click += new System.EventHandler(this.btnFinalSettlement_Click);
            // 
            // CHRIS_Gratuity_GratuityReservesReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 564);
            this.Controls.Add(this.btnFinalSettlement);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "CHRIS_Gratuity_GratuityReservesReports";
            this.Text = "CHRIS_Gratuity_GratuityReservesReports";
            this.Load += new System.EventHandler(this.CHRIS_Gratuity_GratuityReservesReports_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.btnFinalSettlement, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLDatePicker today;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox PF_ADD_BAF_LOAN_SUBSIDY_PAY;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox PF_ADD_UNPAID_SALARY_DAYS;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox PF_ADD_LEAVE_ENCASHMENT_DAYS;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox PF_LESS_CITI_OUTSTANDING_LOAN;
        private CrplControlLibrary.SLTextBox PF_ADD_REVERSAL_OWN_CONT_DAYS;
        private CrplControlLibrary.SLButton slButton2;
        private CrplControlLibrary.SLButton slButton1;
        private CrplControlLibrary.SLTextBox txt_PR_NO_SETTLE;
        private CrplControlLibrary.SLTextBox txt_OUT_BAL_SETTLE;
        private CrplControlLibrary.SLTextBox PF_LESS_ADVANCE_STLMNT_PAID;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnFinalSettlement;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSaveFilePath;
        private System.Windows.Forms.Button btnSaveFilePath;
        private System.Windows.Forms.FolderBrowserDialog folderBrwDialog;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox PF_ADD_OTHER_PAYMENTS_2;
        private CrplControlLibrary.SLTextBox PF_ADD_OTHER_PAYMENTS_1;
        private CrplControlLibrary.SLTextBox PF_ADD_INCENTIVE_AWARD;
        private CrplControlLibrary.SLTextBox PF_ADD_DEFFERED_CASH;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox PF_ADD_ALLOWNCES_PAYMENT;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox PF_LESS_SALARY_REVERSAL_DAYS;
        private CrplControlLibrary.SLTextBox PF_LESS_ALLOWNCES_REVERSAL;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox PF_OUTSTANDING_VEHICLE_LOAN;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox PF_LESS_BAF_CAR_LOAN_DEDUCTION;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLTextBox PF_LESS_BAF_HOUSE_LOAN_DEDUCTION;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox PF_LESS_PF_REVERSAL_OWN_CONT_DAYS;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLTextBox PF_LESS_OTHER_DEDUCTIONS;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLTextBox PF_LESS_INCOME_TAX_DEDUCTION;
        private System.Windows.Forms.Label label30;
    }
}