using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Gratuity
{
    public partial class CHRIS_Gratuity_GratuityReservesReports : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        CHRIS.DATAOBJECTS.CmnDataManager CnmDM = new CHRIS.DATAOBJECTS.CmnDataManager();
        Result rslt;
        public CHRIS_Gratuity_GratuityReservesReports()
        {
            InitializeComponent();
        }
        public CHRIS_Gratuity_GratuityReservesReports(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        private void slButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            DataTable dt = new DataTable();

            dt.Columns.Add("ValueMember");
            dt.Columns.Add("DisplayMember");

            dt.Rows.Add("Screen", "Screen");
            dt.Rows.Add("File", "File");
            dt.Rows.Add("Printer", "Printer");
            dt.Rows.Add("Mail", "Mail");
            dt.Rows.Add("Preview", "Preview");


            //Dest_Type.DisplayMember = "DisplayMember";
            //Dest_Type.ValueMember = "ValueMember";
            //Dest_Type.DataSource = dt;
            //            this.Dest_Type.Items.RemoveAt(5);

            //this.PF_LESS_CITI_OUTSTANDING_LOAN.Text = "";
            //this.PF_ADD_LEAVE_ENCASHMENT_DAYS.Text = "";
            //this.txt_PR_NO_SETTLE.Text = "";
            //this.txt_OUT_BAL_SETTLE.Text = "";
            //this.PF_ADD_UNPAID_SALARY_DAYS.Text = "";
            //this.PF_LESS_ADVANCE_STLMNT_PAID.Text = "";
            //this.PF_ADD_BAF_LOAN_SUBSIDY_PAY.Text = "";

        }

        private void slButton1_Click(object sender, EventArgs e)
        {
            try
            {
                base.RptFileName = "PFENDReport";

                if (txt_PR_NO_SETTLE.Text != "")
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Clear();
                    param.Add("PF_PR_P_NO", txt_PR_NO_SETTLE.Text);
                    rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PP_VERIFY", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            MessageBox.Show("Invalid Personnel No. Entered");
                            txt_PR_NO_SETTLE.Focus();
                            return;

                        }
                    }
                    if (txtSaveFilePath.Text != "")
                    {
                        string DestinationDirectory = txtSaveFilePath.Text;
                        if (System.IO.Directory.Exists(DestinationDirectory))
                        {
                            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm basefrm = new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);
                            basefrm.RptFileName = RptFileName;
                            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

                            GroupBox pnl = new GroupBox();
                            pnl.Name = "DynamicGroupBox1";
                            CrplControlLibrary.SLTextBox txtPR_NO = new CrplControlLibrary.SLTextBox(comp);
                            txtPR_NO.DataFieldMapping = "PF_EN_PR_NO";
                            txtPR_NO.Name = "PF_EN_PR_NO";
                            txtPR_NO.Text = txt_PR_NO_SETTLE.Text;
                            pnl.Controls.Add(txtPR_NO);

                            //// OPTIONAL PARAMETERS (ADD SECTION)
                            if (PF_ADD_UNPAID_SALARY_DAYS.Text == "0" || PF_ADD_UNPAID_SALARY_DAYS.Text == "")
                                PF_ADD_UNPAID_SALARY_DAYS.Text = "0";
                            pnl.Controls.Add(PF_ADD_UNPAID_SALARY_DAYS);
                            if (PF_ADD_LEAVE_ENCASHMENT_DAYS.Text == "0" || PF_ADD_LEAVE_ENCASHMENT_DAYS.Text == "")
                                PF_ADD_LEAVE_ENCASHMENT_DAYS.Text = "0";
                            pnl.Controls.Add(PF_ADD_LEAVE_ENCASHMENT_DAYS);
                            if (PF_ADD_ALLOWNCES_PAYMENT.Text == "0" || PF_ADD_ALLOWNCES_PAYMENT.Text == "")
                                PF_ADD_ALLOWNCES_PAYMENT.Text = "0";
                            pnl.Controls.Add(PF_ADD_ALLOWNCES_PAYMENT);
                            if (PF_ADD_OTHER_PAYMENTS_1.Text == "0" || PF_ADD_OTHER_PAYMENTS_1.Text == "")
                                PF_ADD_OTHER_PAYMENTS_1.Text = "0";
                            pnl.Controls.Add(PF_ADD_OTHER_PAYMENTS_1);
                            if (PF_ADD_OTHER_PAYMENTS_2.Text == "0" || PF_ADD_OTHER_PAYMENTS_2.Text == "")
                                PF_ADD_OTHER_PAYMENTS_2.Text = "0";
                            pnl.Controls.Add(PF_ADD_OTHER_PAYMENTS_2);
                            if (PF_ADD_DEFFERED_CASH.Text == "0" || PF_ADD_DEFFERED_CASH.Text == "")
                                PF_ADD_DEFFERED_CASH.Text = "0";
                            pnl.Controls.Add(PF_ADD_DEFFERED_CASH);
                            if (PF_ADD_INCENTIVE_AWARD.Text == "0" || PF_ADD_INCENTIVE_AWARD.Text == "")
                                PF_ADD_INCENTIVE_AWARD.Text = "0";
                            pnl.Controls.Add(PF_ADD_INCENTIVE_AWARD);
                            if (PF_ADD_REVERSAL_OWN_CONT_DAYS.Text == "0" || PF_ADD_REVERSAL_OWN_CONT_DAYS.Text == "")
                                PF_ADD_REVERSAL_OWN_CONT_DAYS.Text = "0";
                            pnl.Controls.Add(PF_ADD_REVERSAL_OWN_CONT_DAYS);
                            if (PF_ADD_BAF_LOAN_SUBSIDY_PAY.Text == "0" || PF_ADD_BAF_LOAN_SUBSIDY_PAY.Text == "")
                                PF_ADD_BAF_LOAN_SUBSIDY_PAY.Text = "0";
                            pnl.Controls.Add(PF_ADD_BAF_LOAN_SUBSIDY_PAY);

                            //// OPTIONAL PARAMETERS (LESS SECTION)
                            if (PF_LESS_ALLOWNCES_REVERSAL.Text == "0" || PF_LESS_ALLOWNCES_REVERSAL.Text == "")
                                PF_LESS_ALLOWNCES_REVERSAL.Text = "0";
                            pnl.Controls.Add(PF_LESS_ALLOWNCES_REVERSAL);
                            if (PF_LESS_SALARY_REVERSAL_DAYS.Text == "0" || PF_LESS_SALARY_REVERSAL_DAYS.Text == "")
                                PF_LESS_SALARY_REVERSAL_DAYS.Text = "0";
                            pnl.Controls.Add(PF_LESS_SALARY_REVERSAL_DAYS);
                            if (PF_LESS_ADVANCE_STLMNT_PAID.Text == "0" || PF_LESS_ADVANCE_STLMNT_PAID.Text == "")
                                PF_LESS_ADVANCE_STLMNT_PAID.Text = "0";
                            pnl.Controls.Add(PF_LESS_ADVANCE_STLMNT_PAID);
                            if (PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Text == "0" || PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Text == "")
                                PF_LESS_PF_REVERSAL_OWN_CONT_DAYS.Text = "0";
                            pnl.Controls.Add(PF_LESS_PF_REVERSAL_OWN_CONT_DAYS);
                            if (PF_LESS_INCOME_TAX_DEDUCTION.Text == "0" || PF_LESS_INCOME_TAX_DEDUCTION.Text == "")
                                PF_LESS_INCOME_TAX_DEDUCTION.Text = "0";
                            pnl.Controls.Add(PF_LESS_INCOME_TAX_DEDUCTION);
                            if (PF_LESS_OTHER_DEDUCTIONS.Text == "0" || PF_LESS_OTHER_DEDUCTIONS.Text == "")
                                PF_LESS_OTHER_DEDUCTIONS.Text = "0";
                            pnl.Controls.Add(PF_LESS_OTHER_DEDUCTIONS);
                            if (PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Text == "0" || PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Text == "")
                                PF_LESS_BAF_HOUSE_LOAN_DEDUCTION.Text = "0";
                            pnl.Controls.Add(PF_LESS_BAF_HOUSE_LOAN_DEDUCTION);
                            if (PF_LESS_BAF_CAR_LOAN_DEDUCTION.Text == "0" || PF_LESS_BAF_CAR_LOAN_DEDUCTION.Text == "")
                                PF_LESS_BAF_CAR_LOAN_DEDUCTION.Text = "0";
                            pnl.Controls.Add(PF_LESS_BAF_CAR_LOAN_DEDUCTION);
                            if (PF_LESS_CITI_OUTSTANDING_LOAN.Text == "0" || PF_LESS_CITI_OUTSTANDING_LOAN.Text == "")
                                PF_LESS_CITI_OUTSTANDING_LOAN.Text = "0";
                            pnl.Controls.Add(PF_LESS_CITI_OUTSTANDING_LOAN );
                            if (PF_OUTSTANDING_VEHICLE_LOAN.Text == "0" || PF_OUTSTANDING_VEHICLE_LOAN.Text == "")
                                PF_OUTSTANDING_VEHICLE_LOAN.Text = "0";
                            pnl.Controls.Add(PF_OUTSTANDING_VEHICLE_LOAN);


                            basefrm.Controls.Add(pnl);

                            Dictionary<string, object> optionalparameters = new Dictionary<string, object>();


                            string DestinationFile = string.Concat(new string[] { DestinationDirectory, "\\", txt_PR_NO_SETTLE.Text + "_PF_GR_Report_" + DateTime.Now.ToString("ddMMyyyHHmmss") + ".pdf" });
                            basefrm.SaveAsPDFReport(DestinationFile, "pdf");
                            MessageBox.Show("Success, Please find the saved file @ " + DestinationFile);
                        }
                        else
                        {
                            MessageBox.Show("unable to find the specified directory : " + DestinationDirectory);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please provide directory to save file");
                        btnSaveFilePath.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(this.Name, "ExportCustomReport", ex);
                MessageBox.Show("Error in Generating File " + ex.Message, "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
            //private void slButton1_Click(object sender, EventArgs e)
            //{
            //    try
            //    {
            //        if (string.IsNullOrEmpty(txt_PR_NO_SETTLE.Text))
            //        {
            //            MessageBox.Show("Please Enter Personnel Number.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            return;
            //        }

            //        if (string.IsNullOrEmpty(txtSaveFilePath.Text))
            //        {
            //            MessageBox.Show("Please Select Path To Save File.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            return;
            //        }

            //        #region Interst calculation
            //        DataTable IntCal = new DataTable();
            //        IntCal.Columns.Add("IntYear", typeof(int));
            //        IntCal.Columns.Add("IntAmount", typeof(double));
            //        IntCal.Columns.Add("Balance", typeof(double));

            //        DataRow IntCal_Row;
            //        #endregion


            //        #region DataTable For Interst
            //        DataTable genInt_Detail = new DataTable();
            //        genInt_Detail.Columns.Add("IntYear", typeof(int));
            //        genInt_Detail.Columns.Add("IntMonth", typeof(int));
            //        genInt_Detail.Columns.Add("IntDays", typeof(int));
            //        genInt_Detail.Columns.Add("IntOfMonth", typeof(double));

            //        DataRow genInt_DetailRow;
            //        #endregion

            //        int PRNO = Convert.ToInt32(txt_PR_NO_SETTLE.Text.Trim());
            //        double Upto_Dec_Last_Two_Year = 0;
            //        double Upto_Dec_Last_One_Year = 0;
            //        DataTable dt_PF_YEAR_PFI_RATE = new DataTable();
            //        dt_PF_YEAR_PFI_RATE = SQLManager.CHRIS_SP_PF_YEAR_WISE_DETAIL_PFI_RATE().Tables[0];

            //        int noOfRows = dt_PF_YEAR_PFI_RATE.Rows.Count;
            //        if (noOfRows < 1)
            //        {
            //            MessageBox.Show("No Record Found Against The Personnel No : " + txt_PR_NO_SETTLE.Text, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            return;
            //        }

            //        DataTable dt_PF_YEAR_PFI_For_Detail = new DataTable();
            //        dt_PF_YEAR_PFI_For_Detail = SQLManager.CHRIS_SP_PF_YEAR_WISE_DETAIL_PFI_PF(PRNO).Tables[0];
            //        int PRnoFound = dt_PF_YEAR_PFI_For_Detail.Rows.Count;
            //        if (PRnoFound <= 0)
            //        {
            //            MessageBox.Show("No Record Found Against The Personnel No : " + txt_PR_NO_SETTLE.Text, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            return;
            //        }

            //        DataTable dt_PF_YEAR = new DataTable();
            //        dt_PF_YEAR = SQLManager.CHRIS_SP_PF_YEAR_WISE(PRNO).Tables[0];
            //        StringBuilder PF_YEAR = new StringBuilder();

            //        double d_EMP_CONT = 0;
            //        double d_BNK_CONT = 0;
            //        double d_PF_INT = 0;
            //        double d_PF_SUM = 0;

            //        double full_AMT = 0;

            //        bool istrue = true;
            //        bool isSecondTime = true;
            //        foreach (System.Data.DataRow drowx in dt_PF_YEAR.Rows)
            //        {
            //            string PR_YEAR = string.IsNullOrEmpty(drowx["YEAR"].ToString().Trim()) ? "" : drowx["YEAR"].ToString().Trim();
            //            string EMP_CONT = string.IsNullOrEmpty(drowx["EMP_CONT"].ToString().Trim()) ? "" : drowx["EMP_CONT"].ToString().Trim();
            //            string BNK_CONT = string.IsNullOrEmpty(drowx["BNK_CONT"].ToString().Trim()) ? "" : drowx["BNK_CONT"].ToString().Trim();
            //            string PF_INT = string.IsNullOrEmpty(drowx["PF_INT"].ToString().Trim()) ? "" : drowx["PF_INT"].ToString().Trim();

            //            double sum_PF_VALUE = Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(PF_INT);
            //            string disPRYEAR = string.Empty;

            //            if (istrue)
            //            {
            //                disPRYEAR = "<tr>"
            //                         + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                         + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                         + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                         + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                         + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                         + "</tr>"
            //                         + "<tr Style='border: 1px solid black;'>"
            //                         + "<td Style='border: 1px solid black;font-size:18px; text-align:center;'>UPTO December 31, " + PR_YEAR + "</td>"
            //                         + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(EMP_CONT)) + "</td>"
            //                         + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(BNK_CONT)) + "</td>"
            //                         + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(PF_INT)) + "</td>"
            //                         + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", (sum_PF_VALUE)) + "</td>"
            //                         + "</tr>"
            //                          + "<tr>"
            //                        + "<tr Style='border: 1px solid black;'>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;text-align:center;'></td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "</tr>";
            //                istrue = false;
            //                full_AMT = sum_PF_VALUE;
            //                Upto_Dec_Last_Two_Year = sum_PF_VALUE;
            //            }
            //            else
            //            {


            //                #region PPF Calculation

            //                double CN4 = 0;
            //                DataRow[] yearDBRows_PFDetail = dt_PF_YEAR_PFI_For_Detail.Select("YEAR = " + PR_YEAR + "", "PFD_DATE");
            //                bool isFirstStep = true;
            //                double CB4 = 0;
            //                double BC4 = 0;
            //                double CB4_SUM = 0;
            //                int PF_ROW_count = 0;
            //                string Mon_Name = string.Empty;
            //                int DaysInMonth = 0;
            //                foreach (System.Data.DataRow drowxPF in yearDBRows_PFDetail)
            //                {
            //                    genInt_DetailRow = genInt_Detail.NewRow();
            //                    string PR_YEAR_Date = string.IsNullOrEmpty(drowxPF["PFD_DATE"].ToString().Trim()) ? "" : drowxPF["PFD_DATE"].ToString().Trim();
            //                    string EMP_CONT_PF = string.IsNullOrEmpty(drowxPF["PFD_PFUND"].ToString().Trim()) ? "" : drowxPF["PFD_PFUND"].ToString().Trim();

            //                    DateTime dt_PF = Convert.ToDateTime(PR_YEAR_Date);
            //                    Mon_Name = dt_PF.ToString("MMMM");

            //                    int Month = dt_PF.Month;
            //                    DaysInMonth = Month;
            //                    int Year = dt_PF.Year;
            //                    int BO4 = DateTime.DaysInMonth(Year, Month);
            //                    double AP4 = (Convert.ToDouble(EMP_CONT_PF) * 2);
            //                    PF_ROW_count += 1;

            //                    genInt_DetailRow["IntYear"] = Year;
            //                    genInt_DetailRow["IntMonth"] = Month;
            //                    genInt_DetailRow["IntDays"] = BO4;

            //                    #region Step 1

            //                    if (isFirstStep)
            //                    {
            //                        double BB4 = full_AMT;
            //                        BC4 = BB4 + AP4;
            //                        full_AMT = 0;
            //                        full_AMT = BC4;
            //                        //JAN PR BAL
            //                        CB4 = BC4 * BO4;
            //                        CB4_SUM = CB4;
            //                        isFirstStep = false;

            //                    }
            //                    else
            //                    {
            //                        //FEB To DEC PR BAL
            //                        BC4 = full_AMT + AP4;
            //                        full_AMT = 0;
            //                        full_AMT = BC4;
            //                        CB4 = BC4 * BO4;
            //                        CB4_SUM = CB4;
            //                    }

            //                    DataRow[] yearPFIRateEachYear = dt_PF_YEAR_PFI_RATE.Select("YEAR = " + Year + "");

            //                    string yearRateEachYear = string.Empty;
            //                    string RateEachYear = string.Empty;
            //                    foreach (DataRow rowFile in yearPFIRateEachYear)
            //                    {
            //                        yearRateEachYear = string.IsNullOrEmpty(rowFile["YEAR"].ToString().Trim()) ? "" : rowFile["YEAR"].ToString().Trim();
            //                        RateEachYear = string.IsNullOrEmpty(rowFile["PFI_RATE"].ToString().Trim()) ? "" : rowFile["PFI_RATE"].ToString().Trim();

            //                    }

            //                    if (yearPFIRateEachYear.Length <= 0)
            //                    {
            //                        DataRow[] yearPFIRateLastYear = dt_PF_YEAR_PFI_RATE.Select("YEAR = " + (Year - 1) + "");

            //                        foreach (DataRow rowXFile in yearPFIRateLastYear)
            //                        {
            //                            yearRateEachYear = string.IsNullOrEmpty(rowXFile["YEAR"].ToString().Trim()) ? "" : rowXFile["YEAR"].ToString().Trim();
            //                            RateEachYear = string.IsNullOrEmpty(rowXFile["PFI_RATE"].ToString().Trim()) ? "" : rowXFile["PFI_RATE"].ToString().Trim();
            //                        }
            //                    }


            //                    #endregion
            //                    // PF_ROW_count += PF_ROW_count;
            //                    genInt_DetailRow["IntOfMonth"] = Math.Round((CB4_SUM / Math.Round(BO4 / Convert.ToDouble(RateEachYear) * 100, 2)), 2);  //CB4_SUM;
            //                    CN4 = CN4 + CB4_SUM;

            //                    genInt_Detail.Rows.Add(genInt_DetailRow);
            //                }

            //                DataRow[] yearPFIRate = dt_PF_YEAR_PFI_RATE.Select("YEAR = " + PR_YEAR + "");

            //                string yearRate = string.Empty;
            //                string Rate = string.Empty;
            //                foreach (DataRow rowFile in yearPFIRate)
            //                {
            //                    yearRate = string.IsNullOrEmpty(rowFile["YEAR"].ToString().Trim()) ? "" : rowFile["YEAR"].ToString().Trim();
            //                    Rate = string.IsNullOrEmpty(rowFile["PFI_RATE"].ToString().Trim()) ? "" : rowFile["PFI_RATE"].ToString().Trim();
            //                }

            //                if (yearPFIRate.Length <= 0)
            //                {
            //                    DataRow[] yearPFIRateX = dt_PF_YEAR_PFI_RATE.Select("YEAR = " + (Convert.ToInt32(PR_YEAR) - 1) + "");

            //                    foreach (DataRow rowXFile in yearPFIRateX)
            //                    {
            //                        yearRate = string.IsNullOrEmpty(rowXFile["YEAR"].ToString().Trim()) ? "" : rowXFile["YEAR"].ToString().Trim();
            //                        Rate = string.IsNullOrEmpty(rowXFile["PFI_RATE"].ToString().Trim()) ? "" : rowXFile["PFI_RATE"].ToString().Trim();
            //                    }
            //                }



            //                double Full_Int_Amt = Math.Round((CN4 / Math.Round(365 / Convert.ToDouble(Rate) * 100, 2)), 2);
            //                PF_INT = Convert.ToString(Full_Int_Amt);
            //                #endregion

            //                sum_PF_VALUE = double.Parse(EMP_CONT) + double.Parse(EMP_CONT) + Full_Int_Amt;
            //                disPRYEAR = "<tr>"
            //                          + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                          + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                          + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                          + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                          + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                          + "</tr>"
            //                          + "<tr Style='border: 1px solid black;'>"
            //                          + "<td Style='border: 1px solid black;font-size:18px; text-align:center;'>January 1, " + PR_YEAR + " to </td>"
            //                          + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(EMP_CONT)) + "</td>"
            //                          + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(BNK_CONT)) + "</td>"
            //                          + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", Full_Int_Amt) + "</td>"
            //                          + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", (sum_PF_VALUE)) + "</td>"
            //                          + "</tr>"
            //                          + "<tr>"
            //                        + "<tr Style='border: 1px solid black;'>"
            //                        + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + Mon_Name + " " + DaysInMonth + ", " + PR_YEAR + "</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "<td Style='border: 1px solid black;font-size:12px;'> &#160;</td>"
            //                        + "</tr>";


            //                IntCal_Row = IntCal.NewRow();

            //                IntCal_Row["IntYear"] = PR_YEAR;
            //                IntCal_Row["IntAmount"] = Full_Int_Amt;
            //                IntCal_Row["Balance"] = CN4;

            //                IntCal.Rows.Add(IntCal_Row);

            //                if (isSecondTime)
            //                {
            //                    Upto_Dec_Last_One_Year = sum_PF_VALUE;
            //                    full_AMT = full_AMT + Full_Int_Amt;
            //                    isSecondTime = false;
            //                }
            //            }




            //            d_EMP_CONT = d_EMP_CONT + Convert.ToDouble(EMP_CONT);
            //            d_BNK_CONT = d_BNK_CONT + Convert.ToDouble(BNK_CONT);
            //            d_PF_INT = d_PF_INT + Convert.ToDouble(PF_INT);
            //            d_PF_SUM = d_PF_SUM + Convert.ToDouble(sum_PF_VALUE);

            //            PF_YEAR.Append(disPRYEAR);
            //        }


            //        #region Defination of PROVIDENT FUND
            //        DataTable dt_PF_YEAR_DETAIL = new DataTable();

            //        dt_PF_YEAR_DETAIL = SQLManager.CHRIS_SP_PF_YEAR_WISE_DETAIL(PRNO).Tables[0];
            //        StringBuilder PF_YEAR_DETAIL = new StringBuilder();

            //        string PF_YEAR_DETAIL_STRING = string.Empty;
            //        double PR_BAL_AMT = 0;

            //        #region Count of Months

            //        DataView cnt_Years = new DataView(dt_PF_YEAR_DETAIL);
            //        DataTable dist_Year_Month = cnt_Years.ToTable(true, "YEAR", "Months");
            //        int cnt_year_months = dist_Year_Month.Rows.Count;

            //        #endregion
            //        int countRows = 1;
            //        int TotalDays = 0;
            //        double Sum_EMP_CONT = 0;
            //        double Emp_Sum = 0;
            //        double Bank_Sum = 0;
            //        int Days_Sum = 0;
            //        double Balance_Sum = 0;
            //        foreach (System.Data.DataRow drowx in dt_PF_YEAR_DETAIL.Rows)
            //        {
            //            PF_YEAR_DETAIL_STRING = string.Empty;
            //            string PR_YEAR = string.IsNullOrEmpty(drowx["YEAR"].ToString().Trim()) ? "" : drowx["YEAR"].ToString().Trim();
            //            string Months = string.IsNullOrEmpty(drowx["Months"].ToString().Trim()) ? "" : drowx["Months"].ToString().Trim();
            //            string NumMonth = string.IsNullOrEmpty(drowx["NumMonth"].ToString().Trim()) ? "" : drowx["NumMonth"].ToString().Trim();
            //            string Days = NumMonth;//string.IsNullOrEmpty(drowx["Days"].ToString().Trim()) ? "" : drowx["Days"].ToString().Trim();
            //            string EMP_CONT = string.IsNullOrEmpty(drowx["PF_EMP_CONT"].ToString().Trim()) ? "" : drowx["PF_EMP_CONT"].ToString().Trim();
            //            string BNK_CONT = string.IsNullOrEmpty(drowx["PF_BNK_CONT"].ToString().Trim()) ? "" : drowx["PF_BNK_CONT"].ToString().Trim();
            //            string Total_Cont = string.IsNullOrEmpty(drowx["Total_Cont"].ToString().Trim()) ? "" : drowx["Total_Cont"].ToString().Trim();
            //            string PF_INT = string.Empty;//string.IsNullOrEmpty(drowx["PF_INTEREST"].ToString().Trim()) ? "" : drowx["PF_INTEREST"].ToString().Trim();


            //            //genInt_Detail.Columns.Add("IntYear", typeof(int));
            //            //genInt_Detail.Columns.Add("IntMonth", typeof(int));
            //            //genInt_Detail.Columns.Add("IntDays", typeof(int));

            //            DataRow[] genIntDetailRows = genInt_Detail.Select("IntYear = " + PR_YEAR + " and IntMonth = " + NumMonth + " ");
            //            foreach (System.Data.DataRow drInt in genIntDetailRows)
            //            {
            //                //IntOfMonth
            //                PF_INT = string.IsNullOrEmpty(drInt["IntOfMonth"].ToString().Trim()) ? "" : drInt["IntOfMonth"].ToString().Trim();
            //                Days = string.IsNullOrEmpty(drInt["IntDays"].ToString().Trim()) ? "" : drInt["IntDays"].ToString().Trim();
            //            }


            //            int dt_PF_YEAR_COUNT = dt_PF_YEAR_DETAIL.Select("YEAR = " + PR_YEAR + "").Count();

            //            //double PR_TOTAL = (Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total));
            //            if (countRows == 1)
            //            {

            //                DataRow[] yearDBRows = dt_PF_YEAR.Select("YEAR = " + PR_YEAR + "");

            //                string last_year_total = "";
            //                foreach (DataRow rowFile in yearDBRows)
            //                {
            //                    string last_EMP_CONT = string.IsNullOrEmpty(rowFile["EMP_CONT"].ToString().Trim()) ? "" : rowFile["EMP_CONT"].ToString().Trim();
            //                    string last_BNK_CONT = string.IsNullOrEmpty(rowFile["BNK_CONT"].ToString().Trim()) ? "" : rowFile["BNK_CONT"].ToString().Trim();
            //                    string last_PF_INT = string.IsNullOrEmpty(rowFile["PF_INT"].ToString().Trim()) ? "" : rowFile["PF_INT"].ToString().Trim();

            //                    if (PR_BAL_AMT == 0)
            //                    {
            //                        last_year_total = Convert.ToString(Upto_Dec_Last_Two_Year);// Convert.ToString(Convert.ToDouble(last_EMP_CONT) + Convert.ToDouble(last_BNK_CONT) + Convert.ToDouble(last_PF_INT));
            //                    }
            //                    else
            //                    {
            //                        last_year_total = Convert.ToString(PR_BAL_AMT);
            //                    }




            //                    string txt_lastyear = "<table style='width: 90%;border: 1px solid black;margin-left:35px;border-collapse: collapse;margin-top: 45px;'>"
            //                                          + "<tr Style='border: 1px solid black;'>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'> &#160;</th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'>EMP CONT</th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'>BANK CONT</th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'>TOTAL </th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'> &#160;</th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'>PR BAL</th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'> </th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'> </th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'>BALANCE </th>"
            //                                          + "<th Style='border: 1px solid black;font-size:20px;'> </th>"
            //                                          + "</tr>" +
            //                                            "<tr>"
            //                                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                          + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(last_year_total)) + "</td>"
            //                                           + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                            + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                             + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                              + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "</tr>";
            //                    PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + txt_lastyear;
            //                }


            //                string d_last_year_total = "0";

            //                if (string.IsNullOrEmpty(last_year_total))
            //                {
            //                    last_year_total = d_last_year_total;
            //                }

            //                string disPRYEAR = "<tr Style='border: 1px solid black;'>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px; text-align:center;'>" + Months + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(EMP_CONT)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(BNK_CONT)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(Total_Cont)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(last_year_total)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(Convert.ToString(Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total)))) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>X</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + Days + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", (((Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total)) * 31))) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "</tr>";

            //                PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + disPRYEAR;
            //                PR_BAL_AMT = (Convert.ToDouble(Total_Cont) + Convert.ToDouble(last_year_total));
            //            }
            //            else
            //            {
            //                string disPRYEAR = "<tr Style='border: 1px solid black;'>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px; text-align:center;'>" + Months + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(EMP_CONT)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(BNK_CONT)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", double.Parse(Total_Cont)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", (PR_BAL_AMT)) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", ((Convert.ToDouble(Total_Cont) + PR_BAL_AMT))) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>X</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + Days + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", ((Convert.ToDouble(Total_Cont) + PR_BAL_AMT) * Convert.ToInt32(Days))) + "</td>"
            //                                    + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "</tr>";

            //                PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + disPRYEAR;
            //                PR_BAL_AMT = (Convert.ToDouble(Total_Cont) + PR_BAL_AMT);
            //            }

            //            //d_EMP_CONT = d_EMP_CONT + Convert.ToDouble(EMP_CONT);
            //            //d_BNK_CONT = d_BNK_CONT + Convert.ToDouble(BNK_CONT);
            //            //d_PF_INT = d_PF_INT + Convert.ToDouble(PF_INT);
            //            //d_PF_SUM = d_PF_SUM + Convert.ToDouble(sum_PF_VALUE);

            //            Emp_Sum = Emp_Sum + Convert.ToDouble(EMP_CONT);
            //            Bank_Sum = Bank_Sum + Convert.ToDouble(BNK_CONT);
            //            Days_Sum = Days_Sum + Convert.ToInt32(Days);
            //            Balance_Sum = Balance_Sum + PR_BAL_AMT;

            //            if (dt_PF_YEAR_COUNT == countRows)
            //            {

            //                #region Summary Interest 

            //                string disSummaryPRYEAR = "<tr>"
            //                                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                          + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                           + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                            + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                             + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                              + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                                         + "</tr>" +
            //                    "<tr Style='border: 1px solid black;'>"
            //                + "<td Style='border: 1px solid black;font-size:18px; text-align:center;'></td>"
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", Emp_Sum) + "</td>"
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", Bank_Sum) + "</td>"
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'></td>"  //" + string.Format("{0:#,##0.00}", double.Parse(Total_Cont)) + "
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'></td>"  // " + string.Format("{0:#,##0.00}", (PR_BAL_AMT)) + "
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'></td>"  //" + string.Format("{0:#,##0.00}", ((Convert.ToDouble(Total_Cont) + PR_BAL_AMT))) + "
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'></td>"
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + Days_Sum + "</td>"
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'>" + string.Format("{0:#,##0.00}", ((Convert.ToDouble(Total_Cont) + PR_BAL_AMT) * Convert.ToInt32(Days_Sum))) + "</td>"
            //                + "<td Style='border: 1px solid black;font-size:18px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                + "</tr>";

            //                PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + disSummaryPRYEAR;

            //                DataRow[] New_IntCal_Rows = IntCal.Select("IntYear = " + PR_YEAR + "");
            //                double IntAmountNew = 0;
            //                foreach (DataRow dx in New_IntCal_Rows)
            //                {
            //                    // yearRate = string.IsNullOrEmpty(dx["YEAR"].ToString().Trim()) ? "" : dx["YEAR"].ToString().Trim();
            //                    IntAmountNew = Convert.ToDouble(string.IsNullOrEmpty(dx["IntAmount"].ToString().Trim()) ? "" : dx["IntAmount"].ToString().Trim());
            //                }
            //                //add value for total amount
            //                PR_BAL_AMT = (Convert.ToDouble(IntAmountNew) + PR_BAL_AMT);

            //                #endregion



            //                DataRow[] yearPFIRate = dt_PF_YEAR_PFI_RATE.Select("YEAR = " + PR_YEAR + "");

            //                string yearRate = string.Empty;
            //                string Rate = string.Empty;
            //                foreach (DataRow rowFile in yearPFIRate)
            //                {
            //                    yearRate = string.IsNullOrEmpty(rowFile["YEAR"].ToString().Trim()) ? "" : rowFile["YEAR"].ToString().Trim();
            //                    Rate = string.IsNullOrEmpty(rowFile["PFI_RATE"].ToString().Trim()) ? "" : rowFile["PFI_RATE"].ToString().Trim();
            //                }

            //                DataRow[] IntCal_Rows = IntCal.Select("IntYear = " + PR_YEAR + "");
            //                double IntAmount = 0;
            //                foreach (DataRow dx in IntCal_Rows)
            //                {
            //                    // yearRate = string.IsNullOrEmpty(dx["YEAR"].ToString().Trim()) ? "" : dx["YEAR"].ToString().Trim();
            //                    IntAmount = Convert.ToDouble(string.IsNullOrEmpty(dx["IntAmount"].ToString().Trim()) ? "" : dx["IntAmount"].ToString().Trim());
            //                }

            //                string getDiscountValue = "</table> <div ><table style='width:90%; border: 0px;margin-left:35px; border-collapse: collapse;margin-top:10px;'>"
            //                                    + "<tr Style=''>"
            //                                    + "<td Style='font-size:20px;'>Interest rate used for " + PR_YEAR + ": " + Rate + " % </td>"
            //                                    + "<td Style='font-size:20px;'> = " + string.Format("{0:#,##0.00}", IntAmount) + "</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;text-align:center;'> &#160;&#160;&#160;</td>"
            //                                    + "<td Style='font-size:20px;'> <U></U> </td></table>";

            //                PF_YEAR_DETAIL_STRING = PF_YEAR_DETAIL_STRING + getDiscountValue;

            //                countRows = 1;
            //                Emp_Sum = 0;
            //                Bank_Sum = 0;
            //                Days_Sum = 0;
            //                Balance_Sum = 0;
            //            }
            //            else
            //            {
            //                countRows = countRows + 1;
            //            }

            //            TotalDays = TotalDays + Convert.ToInt32(Days);
            //            PF_YEAR_DETAIL.Append(PF_YEAR_DETAIL_STRING);
            //        }


            //        #endregion


            //        string Total_PF_VALUE = "<tr>"
            //                      + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                      + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                      + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                      + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                      + "<td Style='border: 1px solid black;font-size:5px;'> &#160;</td>"
            //                      + "</tr>"
            //                      + "<tr Style='border: 1px solid black;'>"
            //                      + "<td Style='border: 1px solid black;font-size:18px; text-align:center;font-weight:bold;'>TOTAL </td>"
            //                      + "<td Style='border: 1px solid black;font-size:18px;text-align:center;font-weight:bold;'>" + string.Format("{0:#,##0.00}", (d_EMP_CONT)) + "</td>"
            //                      + "<td Style='border: 1px solid black;font-size:18px;text-align:center;font-weight:bold;'>" + string.Format("{0:#,##0.00}", (d_BNK_CONT)) + " </td>"
            //                      + "<td Style='border: 1px solid black;font-size:18px;text-align:center;font-weight:bold;'></td>" //" + string.Format("{0:#,##0.00}", (d_PF_INT)) + "
            //                      + "<td Style='border: 1px solid black;font-size:18px;text-align:center;font-weight:bold;'>" + string.Format("{0:#,##0.00}", (d_PF_SUM)) + "</td>"
            //                      + "</tr>";

            //        PF_YEAR.Append(Total_PF_VALUE);
            //        string AmountInWords = ConvertAmountToWords(d_PF_SUM);

            //        #region FINAL DUES SETTLEMENT

            //        string disINFO_SETTLE = string.Empty;
            //        DataTable PR_INFO_FOR_SETTLEMENT = new DataTable();
            //        PR_INFO_FOR_SETTLEMENT = SQLManager.CHRIS_GET_PR_INFO_FOR_SETTLEMENT(PRNO).Tables[0];

            //        #region Variables

            //        //this.txt_Tax_Ex_Gratia_SETTLE.Text = "";
            //        //this.txt_Leave_Days_SETTLE.Text = "1";
            //        //this.txt_PR_NO_SETTLE.Text = "";
            //        //this.txt_OUT_BAL_SETTLE.Text = "";
            //        //this.txt_Notice_Per_SETTLE.Text = "";
            //        //this.txt_Adv_Paid_SETTLE.Text = "";
            //        //this.txt_BAF_Loan_SETTLE.Text = "";

            //        int Leave_Days = Convert.ToInt32(string.IsNullOrEmpty(txt_Leave_Days_SETTLE.Text) ? "0" : txt_Leave_Days_SETTLE.Text);
            //        double OUT_BAL_SETTLE = Convert.ToDouble(string.IsNullOrEmpty(txt_OUT_BAL_SETTLE.Text) ? "0" : txt_OUT_BAL_SETTLE.Text);
            //        int Notice_Per_SETTLE = Convert.ToInt32(string.IsNullOrEmpty(txt_Notice_Per_SETTLE.Text) ? "0" : txt_Notice_Per_SETTLE.Text);
            //        double Adv_Paid_SETTLE = Convert.ToDouble(string.IsNullOrEmpty(txt_Adv_Paid_SETTLE.Text) ? "0" : txt_Adv_Paid_SETTLE.Text);
            //        double BAF_Loan_SETTLE = Convert.ToDouble(string.IsNullOrEmpty(txt_BAF_Loan_SETTLE.Text) ? "0" : txt_BAF_Loan_SETTLE.Text);
            //        double Ex_GRATIA_SETTLE = Convert.ToDouble(string.IsNullOrEmpty(txt_Ex_GRATIA_SETTLE.Text) ? "0" : txt_Ex_GRATIA_SETTLE.Text);
            //        double Tax_Ex_Gratia_SETTLE = Convert.ToDouble(string.IsNullOrEmpty(txt_Tax_Ex_Gratia_SETTLE.Text) ? "0" : txt_Tax_Ex_Gratia_SETTLE.Text);

            //        //txt_Ex_GRATIA_SETTLE.Text
            //        #endregion

            //        foreach (System.Data.DataRow drowx in PR_INFO_FOR_SETTLEMENT.Rows)
            //        {
            //            string PR_P_NO = string.IsNullOrEmpty(drowx["PR_P_NO"].ToString().Trim()) ? "" : drowx["PR_P_NO"].ToString().Trim();
            //            string PR_BRANCH = string.IsNullOrEmpty(drowx["PR_BRANCH"].ToString().Trim()) ? "" : drowx["PR_BRANCH"].ToString().Trim();
            //            string PR_DEPT = string.IsNullOrEmpty(drowx["PR_DEPT"].ToString().Trim()) ? "" : drowx["PR_DEPT"].ToString().Trim();
            //            string PR_FUNC_TITTLE1 = string.IsNullOrEmpty(drowx["PR_FUNC_TITTLE1"].ToString().Trim()) ? "" : drowx["PR_FUNC_TITTLE1"].ToString().Trim();
            //            string PR_FUNC_TITTLE2 = string.IsNullOrEmpty(drowx["PR_FUNC_TITTLE2"].ToString().Trim()) ? "" : drowx["PR_FUNC_TITTLE2"].ToString().Trim();
            //            string PR_JOINING_DATE = string.IsNullOrEmpty(drowx["PR_JOINING_DATE"].ToString().Trim()) ? "" : drowx["PR_JOINING_DATE"].ToString().Trim();
            //            string PR_ANNUAL_PACK = string.IsNullOrEmpty(drowx["PR_ANNUAL_PACK"].ToString().Trim()) ? "" : drowx["PR_ANNUAL_PACK"].ToString().Trim();
            //            string PR_TERMIN_DATE = string.IsNullOrEmpty(drowx["PR_TERMIN_DATE"].ToString().Trim()) ? "" : drowx["PR_TERMIN_DATE"].ToString().Trim();
            //            string GR_LIAB = string.IsNullOrEmpty(drowx["GR_LIAB"].ToString().Trim()) ? "" : drowx["GR_LIAB"].ToString().Trim();

            //            //double sum_PF_VALUE = Convert.ToDouble(EMP_CONT) + Convert.ToDouble(BNK_CONT) + Convert.ToDouble(PF_INT);
            //            //DateTime.Now.ToString("MMMM dd")

            //            DateTime dtJoining = Convert.ToDateTime(PR_JOINING_DATE);
            //            string joiningDate = dtJoining.ToString("MMMM dd yyyy");
            //            string terminDate = string.Empty;
            //            DateTime dtTermin = new DateTime();
            //            double NoOfDays = 0;

            //            if (!string.IsNullOrEmpty(PR_TERMIN_DATE))
            //            {
            //                dtTermin = Convert.ToDateTime(PR_TERMIN_DATE);
            //                terminDate = dtTermin.ToString("MMMM dd yyyy");
            //                NoOfDays = ((dtTermin - dtJoining).Days);
            //            }

            //            string textShow = string.Empty;
            //            double AllowPay = Convert.ToDouble((string.IsNullOrEmpty(TxtBx_Allow_Pay.Text.Trim()) ? "0" : TxtBx_Allow_Pay.Text.Trim()));
            //            if (AllowPay > 0)
            //            {
            //                textShow = textShow + "<tr>"
            //                                + "<td> - Allowances Payment</td>"
            //                                + "<td>Rs.</td>"
            //                                + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", AllowPay) + "</td>"
            //                                + "</tr>";
            //            }

            //            double Deff_Cash = Convert.ToDouble((string.IsNullOrEmpty(TxtBx_Deff_Cash.Text.Trim()) ? "0" : TxtBx_Deff_Cash.Text.Trim()));
            //            if (Deff_Cash > 0)
            //            {
            //                textShow = textShow + "<tr>"
            //                            + "<td> - Deferred Cash</td>"
            //                            + "<td>Rs.</td>"
            //                            + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", Deff_Cash) + "</td>"
            //                            + "</tr>";
            //            }

            //            double Incen_Award = Convert.ToDouble((string.IsNullOrEmpty(TxtBx_Incen_Award.Text.Trim()) ? "0" : TxtBx_Incen_Award.Text.Trim()));
            //            if (Incen_Award > 0)
            //            {
            //                textShow = textShow + "<tr>"
            //                            + "<td> - Incentive Award</td>"
            //                            + "<td>Rs.</td>"
            //                            + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", Incen_Award) + "</td>"
            //                            + "</tr>";
            //            }

            //            double Reversal = Convert.ToDouble((string.IsNullOrEmpty(TxtBxPF_Reversal.Text.Trim()) ? "0" : TxtBxPF_Reversal.Text.Trim()));
            //            if (Reversal > 0)
            //            {
            //                textShow = textShow + "<tr>"
            //                            + "<td> - PF Reversal on own Contribution</td>"
            //                            + "<td>Rs.</td>"
            //                            + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", Reversal) + "</td>"
            //                            + "</tr>";
            //            }

            //            double OtherPay_1 = Convert.ToDouble((string.IsNullOrEmpty(TxtBx_Other_Pay_1.Text.Trim()) ? "0" : TxtBx_Other_Pay_1.Text.Trim()));
            //            if (OtherPay_1 > 0)
            //            {
            //                textShow = textShow + "<tr>"
            //                            + "<td> - Other Payments 1</td>"
            //                            + "<td>Rs.</td>"
            //                            + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", OtherPay_1) + "</td>"
            //                            + "</tr>";
            //            }

            //            double OtherPay_2 = Convert.ToDouble((string.IsNullOrEmpty(TxtBx_Other_Pay_2.Text.Trim()) ? "0" : TxtBx_Other_Pay_2.Text.Trim()));
            //            if (OtherPay_2 > 0)
            //            {
            //                textShow = textShow + "<tr>"
            //                            + "<td> - Other Payments 2</td>"
            //                            + "<td>Rs.</td>"
            //                            + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", OtherPay_2) + "</td>"
            //                            + "</tr>";
            //            }


            //            disINFO_SETTLE = "<div Style='page-break-before: always;margin-left:25px;width:80%;margin-left:50px;'>"
            //                                    + "<h2 Style='font-size:24px;text-align:left;margin-left:50px;margin-top:150px;padding-top:100px;padding-left:160px;'>FINAL DUES SETTLEMENT</h2>"
            //                                    + "<p Style='font-size:20px;;text-align:left;margin-left:50px;margin-top:20px;padding-left:160px;'> <B>Employee # " + PR_P_NO + " <br> Level: " + PR_FUNC_TITTLE1 + " " + PR_FUNC_TITTLE2 + " <br> " + PR_BRANCH + " - " + PR_DEPT + " </B>"
            //                                    + "</p>"
            //                                    + "<table style='width: 40%;margin-left: 207px;border-collapse: collapse;margin-top: 30px;font-size: 18px;text-align: left;'>"
            //                                    + "<tr>"
            //                                    + "<td Style='font-size:20px;;width:0px;margin-right:550px;'>Date of Joining <br> Date of Leaving  <br> Years of Service <br> Annual Package </td> "
            //                                    + "<td Style='font-size:20px;;width:0px;'> = <br> = <br> = <br> = </td>"
            //                                    + "<td Style='font-size:20px;;width:0px;'>" + joiningDate + " <br>" + terminDate + "  <br>" + Math.Round((NoOfDays / 365), 2) + " <br>" + string.Format("{0:#,##0.00}", (PR_ANNUAL_PACK)) + " </td>"
            //                                    + "</tr>"
            //                                    + "</table>"
            //                                    + "<table style='width: 105%;margin-left:215px;border-collapse: collapse;margin-top: 30px;font-size: 20px;text-align:left;'>"
            //                                    + "<tr>"
            //                                    + "<td>- Ex Gratia Payment </td>"
            //                                    + "<td> &#160;&#160;&#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td >Rs. </td>"
            //                                    + "<td Style='text-align:right;'>" + string.Format("{0:#,##0.00}", (Ex_GRATIA_SETTLE)) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"
            //                                    + "<td>(Less: Tax on Ex Gratia)</td>"
            //                                    + "<td> &#160;&#160;&#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td>Rs.</td>"
            //                                    + "<td Style='border-bottom:1px solid black;text-align:right;'> &#160;&#160;" + string.Format("{0:#,##0.00}", (Tax_Ex_Gratia_SETTLE)) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"
            //                                    + "<td><B>Net of Tax  </B></td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                     + "<td> &#160;&#160;&#160;</td>"
            //                                     + "<td> &#160;&#160;&#160;</td>"
            //                                     + "<td> &#160;&#160;&#160;</td>"
            //                                     + "<td> &#160;&#160;&#160;</td>"
            //                                     + "<td> &#160;&#160;&#160;</td>"
            //                                     + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> <B> Rs. </B></td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>";
            //                                    double TryParseDouble = (Convert.ToDouble(Ex_GRATIA_SETTLE) - Convert.ToDouble(Tax_Ex_Gratia_SETTLE));
            //                                    disINFO_SETTLE += "<td> <B>" + string.Format("{0:#,##0.00}", TryParseDouble) + " </B></td>"
            //                                    + "</tr>"
            //                                    + "</table>"
            //                                    + "</div>"
            //                                    + "<div Style='margin-left:25px;'>"
            //                                    + "<h1 Style='font-size:24px;;text-align:left;padding-left:160px;margin-left:55px;'>ADD: </h1>"
            //                                    + "<table style='width:67%; margin-left:215px; border-collapse: collapse;margin-top:10px; font-size:20px;;padding-left:150px;'>"
            //                                    + "<tr>"
            //                                    + "<td> - Provident Fund Settlement</td>"
            //                                    + "<td>Rs.</td>"
            //                                    + "<td Style='text-align:right;'>" + string.Format("{0:#,##0.00}", (d_PF_SUM)) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"
            //                                    + "<td> - Gratuity Fund Settlement</td>"
            //                                    + "<td>Rs.</td>";
            //                                    TryParseDouble = (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2));
            //                                    disINFO_SETTLE += "<td Style='text-align:right;'>" + string.Format("{0:#,##0.00}", TryParseDouble) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"

            //                                    + "<td> - Leave Encashment for " + txt_Leave_Days_SETTLE.Text + " Days</td>"
            //                                    + "<td>Rs.</td>";
            //                                    TryParseDouble = Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Leave_Days)), 2);
            //                                    disINFO_SETTLE += "<td Style='text-align:right;'>" + string.Format("{0:#,##0.00}", TryParseDouble) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"

            //                                    + "<td> - Notice Period Pay</td>"
            //                                    + "<td>Rs.</td>";
            //                                    TryParseDouble = (((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Notice_Per_SETTLE));
            //                                    disINFO_SETTLE += "<td Style='text-align:right;'>" + string.Format("{0:#,##0.00}", TryParseDouble) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"

            //                                    + "<td> - BAF loan subsidy</td>"
            //                                    + "<td>Rs.</td>"
            //                                    + "<td Style='border-bottom:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", (BAF_Loan_SETTLE)) + "</td>"
            //                                    + "</tr>" +

            //                                   textShow

            //                                    + "<tr>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>";
            //                                    TryParseDouble = Math.Round((d_PF_SUM) + (BAF_Loan_SETTLE) + (((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Notice_Per_SETTLE)) + Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Leave_Days)), 2) + (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2)), 2);
            //            disINFO_SETTLE += "<td Style='border-bottom:1px solid black;text-align:right;margin-left:50px;'>" + string.Format("{0:#,##0.00}", TryParseDouble) + "</td>"
            //            + "</tr>"
            //            + "</table>"
            //            + "</div>"
            //            + "<div Style='margin-left:25px;'>"
            //            + "<h1 Style='font-size:24px;;text-align:left;padding-left:160px;margin-left:50px;'>LESS:</h1>"
            //            + "<table style='width: 81%;margin-left:213px;border-collapse: collapse;margin-top:10px;font-size:20px;padding-left:150px;'>"
            //            + "<tr>"
            //            + "<td> - Advance Paid Relocation Deduction - Net</td>"
            //            + "<td>Rs.</td>"
            //            + "<td Style='text-align:right;' >" + Adv_Paid_SETTLE + "</td>"
            //            + "</tr>"
            //            + "<tr>"
            //            + "<td> - Outstanding Balance of Vehicle Loan</td>"
            //            + "<td>Rs.</td>"
            //            + "<td Style='border-bottom:1px solid black;text-align:right;'> &#160;&#160;" + string.Format("{0:#,##0.00}", OUT_BAL_SETTLE) + "</td>"
            //            + "</tr>"
            //            + "<tr>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td Style='border-bottom:1px solid black;text-align:right;'> &#160;&#160;" + string.Format("{0:#,##0.00}", (Adv_Paid_SETTLE + OUT_BAL_SETTLE)) + "</td>"
            //            + "</tr>"
            //            + "<tr>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td Style='border-bottom:1px solid black;text-align:right;'> &#160;&#160;&#160;&#160;&#160;&#160;0</td>" //Add Amount
            //            + "</tr>"
            //            + "<tr>"
            //            + "<td><B>Net amount due to from the bank</B></td>"

            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td> &#160;&#160;&#160;</td>"
            //            + "<td><B>Rs.</B></td>"
            //            + "<td Style='border-bottom:1.5px solid black;text-align:right;'><B> &#160;&#160;&#160;&#160;&#160;&#160;";
            //            TryParseDouble = (Math.Round((d_PF_SUM) + (BAF_Loan_SETTLE) + (((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Notice_Per_SETTLE)) + Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Leave_Days)), 2) + (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2)), 2)
            //                                    + (Convert.ToDouble(Ex_GRATIA_SETTLE) - Convert.ToDouble(Tax_Ex_Gratia_SETTLE)) - (Adv_Paid_SETTLE + OUT_BAL_SETTLE));
            //            disINFO_SETTLE+= string.Format("{0:#,##0.00}", TryParseDouble)
            //                                    + "</B></td>"
            //                                    + "</tr>"
            //                                    + "</table>"
            //                                    + "</div>"

            //                                     + "<div Style='width:100%;margin-top:10px;margin-bottom:-20px;'>"
            //                                     + "<h3 Style='font-size:24px;;text-align:center;'>UNDERTAKING</h3>"
            //                                     + "<p Style='font-size:20px;text-align:left;margin-left: 228px;'>I acknowledge receipt of <B>PKR " +
            //                                     string.Format("{0:#,##0.00}", (Math.Round((d_PF_SUM) + (BAF_Loan_SETTLE) + (((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Notice_Per_SETTLE)) + Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Leave_Days)), 2) + (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2)), 2)
            //                                    + (Convert.ToDouble(Ex_GRATIA_SETTLE) - Convert.ToDouble(Tax_Ex_Gratia_SETTLE)) - (Adv_Paid_SETTLE + OUT_BAL_SETTLE)))
            //                                     + " </B>� (Rupees: " +
            //                                     ConvertAmountToWords((Math.Round((d_PF_SUM) + (BAF_Loan_SETTLE) + (((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Notice_Per_SETTLE)) + Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK)) / 365) * Convert.ToInt32(Leave_Days)), 2) + (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2)), 2)
            //                                    + (Convert.ToDouble(Ex_GRATIA_SETTLE) - Convert.ToDouble(Tax_Ex_Gratia_SETTLE)) - (Adv_Paid_SETTLE + OUT_BAL_SETTLE)))
            //                                     + ""
            //                                     + " Only) in settlement of all my dues with the Citibank N.A.</p>"
            //                                     + "<p Style='font-size:20px;text-align:left;margin-left: 228px;'>I further confirm that no dues of any nature whatsoever are receivable from the bank </p>"
            //                                     + "</div>"
            //                                     + "<div Style='width:30%;margin-top:10px;margin-bottom:-20px; margin-left:670px;height:150px;'>"
            //                                     + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:100px; margin-right:30px;margin-top:50px;'> </div>"
            //                                     + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:50px;'> </div>"
            //                                     + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:12px;;text-align:left;'>Checker <B></B></p>"
            //                                     + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:20px; margin-right:30px;margin-top:-38px;'></div>"
            //                                     + "<p Style='width:150px;height=20px;margin-left:20px; margin-right:30px;margin-top:10px;font-size:12px;;text-align:left;'>Maker<B></B> </p>"
            //                                     + "</div>"


            //                                    + "<div Style='page-break-before: always;margin-left:80px;margin-top:60px;width:100%;padding-top:100px;'>"
            //                                    + "<h1 Style='font-size:24px;;text-align:left;margin-left:50px;margin-top:80px;'>TRUSTEES CITIBANK N.A.EMPLOYEE�S GRATUITY FUND <br>GRATUITY FUND</h1>"
            //                                    + "<p Style='font-size:20px;;text-align:left;margin-left:50px;margin-top:20px;'><B>Employee # " + PR_P_NO + "</B></p>"
            //                                    + "<table style='width:70%;margin-left:50px; border-collapse: collapse;margin-top:10px;margin-right:2px;'>"
            //                                    + "<tr>"
            //                                    + "<td Style='font-size:20px;;width:0px;'>Date of Joining <br>Date of Leaving<br>Annual Salary<br>Last Drawn Monthly Basic Salary<br>130% of Basic Monthly Salary (A)<br>Length of Service (B) <br> Gratuity (A*B)</td> "
            //                                    + "<td Style='font-size:20px;;width:0px;'>:<br>:<br>:<br>:<br>:<br>:<br>:</td>"
            //                                    + "<td Style='font-size:20px;;width:0px;text-align:right;'>" + joiningDate + "<br> " + terminDate + " <br>" + string.Format("{0:#,##0.00}", Convert.ToDouble(PR_ANNUAL_PACK)) + "<br>" + string.Format("{0:#,##0.00}", Math.Round(((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12), 2)) + "<br>" + string.Format("{0:#,##0.00}", Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2)) + "<br>" + Math.Round((NoOfDays / 365), 2) + "<br>" + string.Format("{0:#,##0.00}", (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2))) + "</td>"
            //                                    + "</tr>"
            //                                    + "<tr>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td> &#160;&#160;&#160;</td>"
            //                                    + "<td  Style='font-size:18px;;border-bottom:2px solid black;border-top:1px solid black;text-align:right;'>" + string.Format("{0:#,##0.00}", (Math.Round((NoOfDays / 365), 2) * Math.Round((((Convert.ToDouble(PR_ANNUAL_PACK) / 1.5) / 12) * 1.3), 2))) + "</td>"
            //                                    + "</tr>"
            //                                    + "</table>" + "</div>"
            //                                    + "<div Style='width:70%;margin-top:70px;margin-bottom:-20px; margin-left:70px;margin-right:70px;'>"
            //                                    + "<h3 Style='font-size:24px;;text-align:center;'>UNDERTAKING</h3>"
            //                                    + "<p Style='font-size:20px;;text-align:center;'>I acknowledge receipt of <B>PKR " + string.Format("{0:#,##0.00}", (Convert.ToDouble(GR_LIAB.Replace("-", "")))) + " � (Rupees: " + string.Format("{0:#,##0.00}", ConvertAmountToWords((Convert.ToDouble(GR_LIAB.Replace("-", ""))))) + " <br>"
            //                                    + " only) +</B>being Gratuity Fund Payment from Citibank N.A.Pakistan. </P>"
            //                                    + "</div>"
            //                                    + "<div Style='width:30%;margin-top:70px; margin-left:690px;height:150px;'>"
            //                                    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:10px; margin-right:30px;margin-top:20px;'></div>"
            //                                    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:0px;'></div>"
            //                                    + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:20px;;text-align:left;'>Trustee Approval</p>"
            //                                    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:200px; margin-right:30px;margin-top:50px;'></div>"
            //                                    + "<p Style='width:150px;height=20px;margin-left:200px; margin-right:30px;margin-top:10px;font-size:20px;;text-align:left;'>Checker<B></B></p>"
            //                                    + "<div Style='width:150px;height=20px;border-bottom:1px solid black;margin-left:20px; margin-right:30px;margin-top:-38px;'> </div>"
            //                                    + "<p Style='width:150px;height=20px;margin-left:20px; margin-right:30px;margin-top:10px;font-size:20px;;text-align:left;'>Maker<B></B></p>"
            //                                    + "</div>";
            //        }

            //        #endregion



            //        HtmlTemplateToString(PF_YEAR.ToString(), "FINAL_SETTLEMENT", PF_YEAR_DETAIL.ToString(), d_PF_SUM, AmountInWords, disINFO_SETTLE, PRNO, Upto_Dec_Last_Two_Year, Upto_Dec_Last_One_Year);

            //        MessageBox.Show("File Save Successfully");
            //    }
            //    catch (Exception ex)
            //    {
            //        LogError(this.Name, "ExportCustomReport", ex);
            //        MessageBox.Show("Error in Generating File " + ex.Message, "Note", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }

            //}

        public string ConvertAmountToWords(double amt)
        {
            string NumberWords = string.Empty;

            string withDecimal = Convert.ToString(amt);// string.IsNullOrEmpty(dsrow["Tax_Amount"].ToString()) ? "" : dsrow["Tax_Amount"].ToString().Trim();
            string[] cols = withDecimal.Split('.');

            int num = 0;
            int numDec = 0;
            string amtComaSplit = string.Empty;

            string amtNumber = string.Empty;
            string amtDecimal = string.Empty;
            if (cols.Length > 1)
            {
                amtNumber = cols[0];
                amtDecimal = cols[1];

                num = Convert.ToInt32(amtNumber);
                numDec = Convert.ToInt32(amtDecimal);
                NumberWords = ConvertToWords(Convert.ToString(num)) + " And Paisa " + ConvertToWords(Convert.ToString(numDec));//ConvertNumbertoWords(num);
                string.Format("{0:#,##0.00}", double.Parse(withDecimal));
            }
            else
            {
                amtNumber = cols[0];
                num = Convert.ToInt32(amtNumber);
                NumberWords = ConvertToWords(Convert.ToString(num));// ConvertNumbertoWords(num);
            }


            return NumberWords;
        }

        private static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents    
                        endStr = "Paisa " + endStr;//Cents    
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }

        private static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        private static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }

        private static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }


        public static string ConvertNumbertoWords(int number)
        {
            if (number == 0)
                return "ZERO";
            if (number < 0)
                return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";

            if ((number / 1000000000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000000000) + " Billion ";
                number %= 1000000000;
            }

            if ((number / 10000000) > 0)
            {
                words += ConvertNumbertoWords(number / 10000000) + " ";
                number %= 10000000;
            }

            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000000) + " MILLION ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "")
                    words += " "; //AND
                string[] unitsMap = { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
                string[] tensMap = { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }



        private void CHRIS_Gratuity_GratuityReservesReports_Load(object sender, EventArgs e)
        {


        }

        private void P_END_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnFinalSettlement_Click(object sender, EventArgs e)
        {
            CHRIS_FINAL_SETTLEMENT_REPORT frm = new CHRIS_FINAL_SETTLEMENT_REPORT();
            frm.ShowDialog();
        }

        private void btnSaveFilePath_Click(object sender, EventArgs e)
        {
            if (folderBrwDialog.ShowDialog() == DialogResult.OK)
            {
                txtSaveFilePath.Text = folderBrwDialog.SelectedPath;
            }
        }
    }
}