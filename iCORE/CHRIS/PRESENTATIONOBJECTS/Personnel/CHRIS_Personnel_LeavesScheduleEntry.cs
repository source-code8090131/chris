using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_LeavesScheduleEntry : ChrisSimpleForm
    {
        #region --Variable--
        CmnDataManager cmnDM            = new CmnDataManager();
        string CurrOption               = string.Empty;
        DataTable dt_lev_schedule_rep   = new DataTable();
        string delRec                   = string.Empty;
        #endregion

        #region --Constructor--

        public CHRIS_Personnel_LeavesScheduleEntry()
        {
            InitializeComponent();
        }

         public CHRIS_Personnel_LeavesScheduleEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region --Method--

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            pnlHead.SendToBack();
            tbtSave.Visible             = false;
            tbtDelete.Visible           = false;
            tbtAdd.Visible              = false;
            tbtEdit.Visible             = false;
            lblUserName.Text            = userID.ToString();
            tbtList.Visible             = false;
            this.FunctionConfig.F8      = Function.Query;
            this.txtDate.Text           = Now().ToString("dd/MM/yyyy");
            this.txt_W_PL_BAL.Enabled   = true;
            this.txt_LSH_PR_NO.Enabled  = true;
            this.txtDate.Enabled        = true;
            this.txt_W_OPTION_DIS.Enabled   = true;
            this.CurrentPanelBlock          = pnlPerLeave.Name;
            this.FunctionConfig.EnableF5    = false;
            this.FunctionConfig.EnableF2    = false;
        }

        protected override bool Query()
        {
            base.Query();
            //if (!txt_LSH_PR_NO.Focused)
            //{
            //    base.ShowList(pnlPerLeave, "LIST");

            //    txt_LSH_PR_NO.Select();
            //    txt_LSH_PR_NO.Focus();
            //    base.IterateFormToEnableControls(pnlPerLeave.Controls, true);

            //    ReadonlyControl(true);
            //    EnableControl(true);
            //    txt_LSH_JAN1.Enabled = true;
            //    txt_LSH_PR_NO.ReadOnly = false;
            //    txt_LSH_PR_NO.Enabled = true;
            //    txt_LSH_PR_NO.Select();
            //    txt_LSH_PR_NO.Focus();
            //}
            return false;
        }

        public void rep_update()
        {
            try
            {
                /*Delete Records First from LEV_SCHEDULE_REP Table, against the current PR_P_NO*/
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                Result rsltCode1;
                rsltCode1 = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "DELETE_LEV_SCHEDULE_REP", param1);




                /*Add Records in DB*/
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;

                if (txt_LSH_JAN1.Text != string.Empty || txt_LSH_JAN2.Text != string.Empty || txt_LSH_JAN3.Text != string.Empty || txt_LSH_JAN4.Text != string.Empty)
                {
                    param.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "1" + '|' + txt_LSH_JAN1.Text + '|' + txt_LSH_JAN2.Text + '|' + txt_LSH_JAN3.Text + '|' + txt_LSH_JAN4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }

                if (txt_LSH_FEB1.Text != string.Empty || txt_LSH_FEB2.Text != string.Empty || txt_LSH_FEB3.Text != string.Empty || txt_LSH_FEB4.Text != string.Empty)
                {
                    param.Clear();
                    //rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "2" + '|' + txt_LSH_FEB1.Text + '|' + txt_LSH_FEB2.Text + '|' + txt_LSH_FEB3.Text + '|' + txt_LSH_FEB4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }

                if (txt_LSH_MAR1.Text != string.Empty || txt_LSH_MAR2.Text != string.Empty || txt_LSH_MAR3.Text != string.Empty || txt_LSH_MAR4.Text != string.Empty)
                {
                    param.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "3" + '|' + txt_LSH_MAR1.Text + '|' + txt_LSH_MAR2.Text + '|' + txt_LSH_MAR3.Text + '|' + txt_LSH_MAR4.Text);
                    
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }

                if (txt_LSH_APR1.Text != string.Empty || txt_LSH_APR2.Text != string.Empty || txt_LSH_APR3.Text != string.Empty || txt_LSH_APR4.Text != string.Empty)
                {
                    param.Clear();
                    //rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "4" + '|' + txt_LSH_APR1.Text + '|' + txt_LSH_APR2.Text + '|' + txt_LSH_APR3.Text + '|' + txt_LSH_APR4.Text);
                    
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }


                if (txt_LSH_MAY1.Text != string.Empty || txt_LSH_MAY2.Text != string.Empty || txt_LSH_MAY3.Text != string.Empty || txt_LSH_MAY4.Text != string.Empty)
                {
                    param.Clear();
                    //rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "5" + '|' + txt_LSH_MAY1.Text + '|' + txt_LSH_MAY2.Text + '|' + txt_LSH_MAY3.Text + '|' + txt_LSH_MAY4.Text);
                    //param.Add("LSH_MAY1", txt_LSH_MAY1.Text);
                    //param.Add("LSH_MAY2", txt_LSH_MAY2.Text);
                    //param.Add("LSH_MAY3", txt_LSH_MAY3.Text);
                    //param.Add("LSH_MAY4", txt_LSH_MAY4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }


                if (txt_LSH_JUN1.Text != string.Empty || txt_LSH_JUN2.Text != string.Empty || txt_LSH_JUN3.Text != string.Empty || txt_LSH_JUN4.Text != string.Empty)
                {
                    param.Clear();
                   // rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "6" + '|' + txt_LSH_JUN1.Text + '|' + txt_LSH_JUN2.Text + '|' + txt_LSH_JUN3.Text + '|' + txt_LSH_JUN4.Text);
                    //param.Add("LSH_JUN1", txt_LSH_JUN1.Text);
                    //param.Add("LSH_JUN2", txt_LSH_JUN2.Text);
                    //param.Add("LSH_JUN3", txt_LSH_JUN3.Text);
                    //param.Add("LSH_JUN4", txt_LSH_JUN4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }


                if (txt_LSH_JUL1.Text != string.Empty || txt_LSH_JUL2.Text != string.Empty || txt_LSH_JUL3.Text != string.Empty || txt_LSH_JUL4.Text != string.Empty)
                {
                    param.Clear();
                   // rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "7" + '|' + txt_LSH_JUL1.Text + '|' + txt_LSH_JUL2.Text + '|' + txt_LSH_JUL3.Text + '|' + txt_LSH_JUL4.Text);
                    //param.Add("LSH_JUL1", txt_LSH_JUL1.Text);
                    //param.Add("LSH_JUL2", txt_LSH_JUL2.Text);
                    //param.Add("LSH_JUL3", txt_LSH_JUL3.Text);
                    //param.Add("LSH_JUL4", txt_LSH_JUL4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }


                if (txt_LSH_AUG1.Text != string.Empty || txt_LSH_AUG2.Text != string.Empty || txt_LSH_AUG3.Text != string.Empty || txt_LSH_AUG4.Text != string.Empty)
                {
                    param.Clear();
                   // rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "8" + '|' + txt_LSH_AUG1.Text + '|' + txt_LSH_AUG2.Text + '|' + txt_LSH_AUG3.Text + '|' + txt_LSH_AUG4.Text);
                    //param.Add("LSH_AUG1", txt_LSH_AUG1.Text);
                    //param.Add("LSH_AUG2", txt_LSH_AUG2.Text);
                    //param.Add("LSH_AUG3", txt_LSH_AUG3.Text);
                    //param.Add("LSH_AUG4", txt_LSH_AUG4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }



                if (txt_LSH_SEP1.Text != string.Empty || txt_LSH_SEP2.Text != string.Empty || txt_LSH_SEP3.Text != string.Empty || txt_LSH_SEP4.Text != string.Empty)
                {
                    param.Clear();
                   // rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "9" + '|' + txt_LSH_SEP1.Text + '|' + txt_LSH_SEP2.Text + '|' + txt_LSH_SEP3.Text + '|' + txt_LSH_SEP4.Text);
                    //param.Add("LSH_SEP1", txt_LSH_SEP1.Text);
                    //param.Add("LSH_SEP2", txt_LSH_SEP2.Text);
                    //param.Add("LSH_SEP3", txt_LSH_SEP3.Text);
                    //param.Add("LSH_SEP4", txt_LSH_SEP4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }



                if (txt_LSH_OCT1.Text != string.Empty || txt_LSH_OCT2.Text != string.Empty || txt_LSH_OCT3.Text != string.Empty || txt_LSH_OCT4.Text != string.Empty)
                {
                    param.Clear();
                   // rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "10" + '|' + txt_LSH_OCT1.Text + '|' + txt_LSH_OCT2.Text + '|' + txt_LSH_OCT3.Text + '|' + txt_LSH_OCT4.Text);
                    //param.Add("LSH_OCT1", txt_LSH_OCT1.Text);
                    //param.Add("LSH_OCT2", txt_LSH_OCT2.Text);
                    //param.Add("LSH_OCT3", txt_LSH_OCT3.Text);
                    //param.Add("LSH_OCT4", txt_LSH_OCT4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }


                if (txt_LSH_NOV1.Text != string.Empty || txt_LSH_NOV2.Text != string.Empty || txt_LSH_NOV3.Text != string.Empty || txt_LSH_NOV4.Text != string.Empty)
                {
                    param.Clear();
                    //rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "11" + '|' + txt_LSH_NOV1.Text + '|' + txt_LSH_NOV2.Text + '|' + txt_LSH_NOV3.Text + '|' + txt_LSH_NOV4.Text);
                    //param.Add("LSH_NOV1", txt_LSH_NOV1.Text);
                    //param.Add("LSH_NOV2", txt_LSH_NOV2.Text);
                    //param.Add("LSH_NOV3", txt_LSH_NOV3.Text);
                    //param.Add("LSH_NOV4", txt_LSH_NOV4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }


                if (txt_LSH_DEC1.Text != string.Empty || txt_LSH_DEC2.Text != string.Empty || txt_LSH_DEC3.Text != string.Empty || txt_LSH_DEC4.Text != string.Empty)
                {
                    param.Clear();
                    //rsltCode.dstResult.Clear();

                    param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                    param.Add("SEARCHFILTER", "12" + '|' + txt_LSH_DEC1.Text + '|' + txt_LSH_DEC2.Text + '|' + txt_LSH_DEC3.Text + '|' + txt_LSH_DEC4.Text);
                    //param.Add("LSH_DEC1", txt_LSH_DEC1.Text);
                    //param.Add("LSH_DEC2", txt_LSH_DEC2.Text);
                    //param.Add("LSH_DEC3", txt_LSH_DEC3.Text);
                    //param.Add("LSH_DEC4", txt_LSH_DEC4.Text);
                    rsltCode = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "SAVE_LEV_SCHEDULE_REP", param);
                }
            }
            catch (Exception exp)
            {
                //LogError(
            }
        }

        public void CallSAveLEV_SCHEDULE_REP()
        {
            //if (txt_LSH_JAN1.Text == string.Empty || txt_LSH_JAN2.Text == string.Empty || txt_LSH_JAN3.Text == string.Empty || txt_LSH_JAN4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_JAN1.Text, txt_LSH_JAN2.Text, txt_LSH_JAN3.Text, txt_LSH_JAN4.Text);
            //}

            //if (txt_LSH_FEB1.Text == string.Empty || txt_LSH_FEB2.Text == string.Empty || txt_LSH_FEB3.Text == string.Empty || txt_LSH_FEB4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_FEB1.Text, txt_LSH_FEB2.Text, txt_LSH_FEB3.Text, txt_LSH_FEB4.Text);
            //}

            //if (txt_LSH_MAR1.Text == string.Empty || txt_LSH_MAR2.Text == string.Empty || txt_LSH_MAR3.Text == string.Empty || txt_LSH_MAR4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_MAR1.Text, txt_LSH_MAR2.Text, txt_LSH_MAR3.Text, txt_LSH_MAR4.Text);
            //}

            //if (txt_LSH_APR1.Text == string.Empty || txt_LSH_APR2.Text == string.Empty || txt_LSH_APR3.Text == string.Empty || txt_LSH_APR4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_APR1.Text, txt_LSH_APR2.Text, txt_LSH_APR3.Text, txt_LSH_APR4.Text);
            //}

            //if (txt_LSH_MAY1.Text == string.Empty || txt_LSH_MAY2.Text == string.Empty || txt_LSH_MAY3.Text == string.Empty || txt_LSH_MAY4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_MAY1.Text, txt_LSH_MAY2.Text, txt_LSH_MAY3.Text, txt_LSH_MAY4.Text);
            //}

            //if (txt_LSH_JUN1.Text == string.Empty || txt_LSH_JUN2.Text == string.Empty || txt_LSH_JUN3.Text == string.Empty || txt_LSH_JUN4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_JUN1.Text, txt_LSH_JUN2.Text, txt_LSH_JUN3.Text, txt_LSH_JUN4.Text);
            //}

            //if (txt_LSH_AUG1.Text == string.Empty || txt_LSH_AUG2.Text == string.Empty || txt_LSH_AUG3.Text == string.Empty || txt_LSH_AUG4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_AUG1.Text, txt_LSH_AUG2.Text, txt_LSH_AUG3.Text, txt_LSH_AUG4.Text);
            //}

            //if (txt_LSH_SEP1.Text == string.Empty || txt_LSH_SEP2.Text == string.Empty || txt_LSH_SEP3.Text == string.Empty || txt_LSH_SEP4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_SEP1.Text, txt_LSH_SEP2.Text, txt_LSH_SEP3.Text, txt_LSH_SEP4.Text);
            //}

            //if (txt_LSH_OCT1.Text == string.Empty || txt_LSH_OCT2.Text == string.Empty || txt_LSH_OCT3.Text == string.Empty || txt_LSH_OCT4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_OCT1.Text, txt_LSH_OCT2.Text, txt_LSH_OCT3.Text, txt_LSH_OCT4.Text);
            //}

            //if (txt_LSH_NOV1.Text == string.Empty || txt_LSH_NOV2.Text == string.Empty || txt_LSH_NOV3.Text == string.Empty || txt_LSH_NOV4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_NOV1.Text, txt_LSH_NOV2.Text, txt_LSH_NOV3.Text, txt_LSH_NOV4.Text);
            //}

            //if (txt_LSH_DEC1.Text == string.Empty || txt_LSH_DEC2.Text == string.Empty || txt_LSH_DEC3.Text == string.Empty || txt_LSH_DEC4.Text == string.Empty)
            //{
            //    rep_update(txt_LSH_DEC1.Text, txt_LSH_DEC2.Text, txt_LSH_DEC3.Text, txt_LSH_DEC4.Text);
            //}
        }

        public void CallSave()
        {

            if (this.txtOption.Text == "A")
            {
                txtID.Text = string.Empty;
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            }
            else if (this.txtOption.Text == "M")
            {
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            }
            
            DialogResult dr = MessageBox.Show("Do You Want To Save The Record [Y]es [N]o", "From", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dr == DialogResult.Yes)
            {
                //CallSAveLEV_SCHEDULE_REP();

                CurrOption = txtOption.Text;
                rep_update();
                this.Save();
               
                MessageBox.Show(" Enter Personnel Number ...  Or Press [Enter] To Exit", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                base.Cancel();
                txtOption.Text          = CurrOption.ToString();
                if (this.txtOption.Text == "A")
                {
                    txt_W_OPTION_DIS.Text = "ADD";
                    FunctionConfig.CurrentOption = Function.Add;
                }
                else if (this.txtOption.Text == "M")
                {
                    txt_W_OPTION_DIS.Text = "MODIFY";
                    FunctionConfig.CurrentOption = Function.Modify;
                }

                //FunctionConfig.CurrentOption = Function.Add;
                base.IterateFormToEnableControls(pnlPerLeave.Controls, true);
                txtDate.Text            = this.Now().ToString("dd/MM/yyyy");
                txt_LSH_PR_NO.Select();
                txt_LSH_PR_NO.Focus();
            }
            else
            {
                base.Cancel();
                txt_W_OPTION_DIS.Text = string.Empty;
            }
            txtID.Text = string.Empty;
        }

        public void CallDelete()
        {
            DialogResult dr = MessageBox.Show("Do You Want To Delete The Record [Y]es [N]o", "From", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dr == DialogResult.Yes)
            {

                /*Delete Records First from LEV_SCHEDULE_REP Table, against the current PR_P_NO*/
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                Result rsltCode1;
                rsltCode1 = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "DELETE_LEV_SCHEDULE_REP", param1);


                base.Delete();
                MessageBox.Show(" Enter Personnel Number ...  Or Press [Enter] To Exit", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                base.Cancel();
                txt_W_OPTION_DIS.Text = string.Empty;
            }
            else
            {
                base.Cancel();
                txt_W_OPTION_DIS.Text = string.Empty;
            }
        }

        public void CallView()
        {
            DialogResult dr = MessageBox.Show("Do You Want To View The Record [Y]es [N]o", "From", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dr == DialogResult.Yes)
            {
                base.Cancel();
                MessageBox.Show(" Press [F9] To Display The List...  Or Press [Enter] To Exit", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                base.Cancel();
            }
        }

        public void DoAction()
        {
            switch (FunctionConfig.CurrentOption)
            {
                case Function.Add:
                    CallSave();
                    break;
                case Function.Modify:
                    CallSave();
                    break;
                case Function.View:
                    CallView();
                    break;
                case Function.Delete:
                    CallDelete();
                    break;
                case Function.Query:

                    break;

            }
        }

        public void PRO_RATE_LEAVES(DateTime JoiningDate)
        {
            int     pl = 0;
            double  w_pl_month = 0.0;
            double  w_pl_day = 0.0;
            int     w_months = 0;
            int     w_days = 0;

            DateTime LastDateOfCurrYear = new DateTime();
            LastDateOfCurrYear = Convert.ToDateTime("31-dec-" + Now().Year);

            w_months                = MonthsBetweenInOracle(LastDateOfCurrYear, JoiningDate);
            DateTime dtmonthadded   = JoiningDate.AddMonths(1);
            DateTime LastDayOfMonth = new DateTime(dtmonthadded.Year, dtmonthadded.Month, 1).AddDays(-1);
            TimeSpan ts             = LastDayOfMonth - JoiningDate;
            w_days                  = ts.Days;
            w_pl_month              = (30 / 12) * w_months;

            if (JoiningDate.Day > 1)
            {
                w_months    = MonthsBetweenInOracle(LastDateOfCurrYear, JoiningDate);
                w_pl_day    = (30 / 12 / 30) * w_days;
                w_pl_month  = (30/12) * w_months;
            }

            pl                  = Convert.ToInt32(Math.Round(w_pl_month + w_pl_day));
            txt_W_PL_BAL.Text   = pl.ToString();
        }

        public void ReadonlyControl(bool chk)
        {
            /*ReadOnly*/
            txt_LSH_JAN1.ReadOnly = chk;
            txt_LSH_JAN2.ReadOnly = chk;
            txt_LSH_JAN3.ReadOnly = chk;
            txt_LSH_JAN4.ReadOnly = chk;

            txt_LSH_FEB1.ReadOnly = chk;
            txt_LSH_FEB2.ReadOnly = chk;
            txt_LSH_FEB3.ReadOnly = chk;
            txt_LSH_FEB4.ReadOnly = chk;

            txt_LSH_MAR1.ReadOnly = chk;
            txt_LSH_MAR2.ReadOnly = chk;
            txt_LSH_MAR3.ReadOnly = chk;
            txt_LSH_MAR4.ReadOnly = chk;

            txt_LSH_APR1.ReadOnly = chk;
            txt_LSH_APR2.ReadOnly = chk;
            txt_LSH_APR3.ReadOnly = chk;
            txt_LSH_APR4.ReadOnly = chk;

            txt_LSH_MAY1.ReadOnly = chk;
            txt_LSH_MAY2.ReadOnly = chk;
            txt_LSH_MAY3.ReadOnly = chk;
            txt_LSH_MAY4.ReadOnly = chk;

            txt_LSH_JUN1.ReadOnly = chk;
            txt_LSH_JUN2.ReadOnly = chk;
            txt_LSH_JUN3.ReadOnly = chk;
            txt_LSH_JUN4.ReadOnly = chk;

            txt_LSH_JUL1.ReadOnly = chk;
            txt_LSH_JUL2.ReadOnly = chk;
            txt_LSH_JUL3.ReadOnly = chk;
            txt_LSH_JUL4.ReadOnly = chk;

            txt_LSH_AUG1.ReadOnly = chk;
            txt_LSH_AUG2.ReadOnly = chk;
            txt_LSH_AUG3.ReadOnly = chk;
            txt_LSH_AUG4.ReadOnly = chk;

            txt_LSH_SEP1.ReadOnly = chk;
            txt_LSH_SEP2.ReadOnly = chk;
            txt_LSH_SEP3.ReadOnly = chk;
            txt_LSH_SEP4.ReadOnly = chk;

            txt_LSH_OCT1.ReadOnly = chk;
            txt_LSH_OCT2.ReadOnly = chk;
            txt_LSH_OCT3.ReadOnly = chk;
            txt_LSH_OCT4.ReadOnly = chk;

            txt_LSH_NOV1.ReadOnly = chk;
            txt_LSH_NOV2.ReadOnly = chk;
            txt_LSH_NOV3.ReadOnly = chk;
            txt_LSH_NOV4.ReadOnly = chk;

            txt_LSH_DEC1.ReadOnly = chk;
            txt_LSH_DEC2.ReadOnly = chk;
            txt_LSH_DEC3.ReadOnly = chk;
            txt_LSH_DEC4.ReadOnly = chk;

        }

        public void EnableControl(bool chk)
        {
            txt_LSH_JAN1.Enabled = chk;
            txt_LSH_JAN2.Enabled = chk;
            txt_LSH_JAN3.Enabled = chk;
            txt_LSH_JAN4.Enabled = chk;

            txt_LSH_FEB1.Enabled = chk;
            txt_LSH_FEB2.Enabled = chk;
            txt_LSH_FEB3.Enabled = chk;
            txt_LSH_FEB4.Enabled = chk;

            txt_LSH_MAR1.Enabled = chk;
            txt_LSH_MAR2.Enabled = chk;
            txt_LSH_MAR3.Enabled = chk;
            txt_LSH_MAR4.Enabled = chk;

            txt_LSH_APR1.Enabled = chk;
            txt_LSH_APR2.Enabled = chk;
            txt_LSH_APR3.Enabled = chk;
            txt_LSH_APR4.Enabled = chk;

            txt_LSH_MAY1.Enabled = chk;
            txt_LSH_MAY2.Enabled = chk;
            txt_LSH_MAY3.Enabled = chk;
            txt_LSH_MAY4.Enabled = chk;

            txt_LSH_JUN1.Enabled = chk;
            txt_LSH_JUN2.Enabled = chk;
            txt_LSH_JUN3.Enabled = chk;
            txt_LSH_JUN4.Enabled = chk;

            txt_LSH_JUL1.Enabled = chk;
            txt_LSH_JUL2.Enabled = chk;
            txt_LSH_JUL3.Enabled = chk;
            txt_LSH_JUL4.Enabled = chk;

            txt_LSH_AUG1.Enabled = chk;
            txt_LSH_AUG2.Enabled = chk;
            txt_LSH_AUG3.Enabled = chk;
            txt_LSH_AUG4.Enabled = chk;

            txt_LSH_SEP1.Enabled = chk;
            txt_LSH_SEP2.Enabled = chk;
            txt_LSH_SEP3.Enabled = chk;
            txt_LSH_SEP4.Enabled = chk;

            txt_LSH_OCT1.Enabled = chk;
            txt_LSH_OCT2.Enabled = chk;
            txt_LSH_OCT3.Enabled = chk;
            txt_LSH_OCT4.Enabled = chk;

            txt_LSH_NOV1.Enabled = chk;
            txt_LSH_NOV2.Enabled = chk;
            txt_LSH_NOV3.Enabled = chk;
            txt_LSH_NOV4.Enabled = chk;

            txt_LSH_DEC1.Enabled = chk;
            txt_LSH_DEC2.Enabled = chk;
            txt_LSH_DEC3.Enabled = chk;
            txt_LSH_DEC4.Enabled = chk;


          
        }

        public void TabStopControl(bool chk)
        {
            /*TabStop*/

            txt_LSH_JAN1.TabStop = chk;
            txt_LSH_JAN2.TabStop = chk;
            txt_LSH_JAN3.TabStop = chk;
            txt_LSH_JAN4.TabStop = chk;

            txt_LSH_FEB1.TabStop = chk;
            txt_LSH_FEB2.TabStop = chk;
            txt_LSH_FEB3.TabStop = chk;
            txt_LSH_FEB4.TabStop = chk;

            txt_LSH_MAR1.TabStop = chk;
            txt_LSH_MAR2.TabStop = chk;
            txt_LSH_MAR3.TabStop = chk;
            txt_LSH_MAR4.TabStop = chk;

            txt_LSH_APR1.TabStop = chk;
            txt_LSH_APR2.TabStop = chk;
            txt_LSH_APR3.TabStop = chk;
            txt_LSH_APR4.TabStop = chk;

            txt_LSH_MAY1.TabStop = chk;
            txt_LSH_MAY2.TabStop = chk;
            txt_LSH_MAY3.TabStop = chk;
            txt_LSH_MAY4.TabStop = chk;

            txt_LSH_JUN1.TabStop = chk;
            txt_LSH_JUN2.TabStop = chk;
            txt_LSH_JUN3.TabStop = chk;
            txt_LSH_JUN4.TabStop = chk;

            txt_LSH_JUL1.TabStop = chk;
            txt_LSH_JUL2.TabStop = chk;
            txt_LSH_JUL3.TabStop = chk;
            txt_LSH_JUL4.TabStop = chk;

            txt_LSH_AUG1.TabStop = chk;
            txt_LSH_AUG2.TabStop = chk;
            txt_LSH_AUG3.TabStop = chk;
            txt_LSH_AUG4.TabStop = chk;

            txt_LSH_SEP1.TabStop = chk;
            txt_LSH_SEP2.TabStop = chk;
            txt_LSH_SEP3.TabStop = chk;
            txt_LSH_SEP4.TabStop = chk;

            txt_LSH_OCT1.TabStop = chk;
            txt_LSH_OCT2.TabStop = chk;
            txt_LSH_OCT3.TabStop = chk;
            txt_LSH_OCT4.TabStop = chk;

            txt_LSH_NOV1.TabStop = chk;
            txt_LSH_NOV2.TabStop = chk;
            txt_LSH_NOV3.TabStop = chk;
            txt_LSH_NOV4.TabStop = chk;

            txt_LSH_DEC1.TabStop = chk;
            txt_LSH_DEC2.TabStop = chk;
            txt_LSH_DEC3.TabStop = chk;
            txt_LSH_DEC4.TabStop = chk;
        }

        #endregion

        #region --Events--
        /// <summary>
        /// After Lov Selection Set the VALues in other textboxes.
        /// </summary>
        /// <param name="selectedRow"></param>
        /// <param name="actionType"></param>
        private void CHRIS_Personnel_LeavesScheduleEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {

            //DoAction();

            if (actionType == "PNo")
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txt_LSH_PR_NO.Text);
                Result rsltCode;
                rsltCode = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "LC_P_NAME", param);
                
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    txt_LC_P_NAME.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();


                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("LS_P_NO", txt_LSH_PR_NO.Text);
                rsltCode = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "W_PL_BAL", param1);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    txt_W_PL_BAL.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
            }
        }

        /// <summary>
        /// VALIDATE LSH_PR_NO TEXTBOX
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_LSH_PR_NO_Validating(object sender, CancelEventArgs e)
        {
            try
             {
                 if (!lbtnPNo.Focused)
                 {
                     if (txt_LSH_PR_NO.Text != string.Empty)
                     {
                         DateTime JoiningDate = new DateTime();

                         if (txtOption.Text == "M")
                         {
                             Dictionary<string, object> param3 = new Dictionary<string, object>();
                             param3.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                             Result rsltCode3;
                             rsltCode3 = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "CHECK_LEV_SCHEDULE", param3);

                             if (rsltCode3.isSuccessful && rsltCode3.dstResult.Tables[0].Rows.Count > 0)
                             {

                             }
                             else
                             {
                                 MessageBox.Show("Record Not Found... Press [Delete] Key", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                 e.Cancel = true;
                                 txt_LC_P_NAME.Text = string.Empty;
                                 return;
                             }
                         }


                         Dictionary<string, object> param2 = new Dictionary<string, object>();
                         param2.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                         Result rsltCode2;
                         rsltCode2 = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "LC_P_NAME", param2);

                         if (rsltCode2.isSuccessful && rsltCode2.dstResult.Tables[0].Rows.Count > 0)
                         {
                             txt_LC_P_NAME.Text = rsltCode2.dstResult.Tables[0].Rows[0]["PR_FIRST_NAME"].ToString();
                             JoiningDate = Convert.ToDateTime(rsltCode2.dstResult.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString());
                         }

                         Dictionary<string, object> param1 = new Dictionary<string, object>();
                         param1.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                         Result rsltCode1;
                         rsltCode1 = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "W_PL_BAL", param1);

                         if (rsltCode1.isSuccessful && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                         {
                             txt_W_PL_BAL.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                         }
                         else if (JoiningDate.Year == Now().Year)
                         {
                             PRO_RATE_LEAVES(JoiningDate);
                         }
                         else
                         {
                             txt_W_PL_BAL.Text = "30";
                         }



                         int No = 0;

                         if (txt_LSH_PR_NO.Text == string.Empty)
                         {
                             ClearForm(pnlPerLeave.Controls);
                             txtOption.Select();
                             txtOption.Focus();
                         }

                         if (txtOption.Text == "M" || txtOption.Text == "D")
                         {
                             //proc_mod_Del();
                         }

                         if (txtOption.Text == "V" || txtOption.Text == "A")
                         {
                             //ProcViewAdd();
                         }

                         if (txtOption.Text == "A")
                         {

                             Dictionary<string, object> param = new Dictionary<string, object>();
                             param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                             Result rsltCode;
                             rsltCode = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "W_PL_BAL_COUNT", param);

                             if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                                 No = Convert.ToInt32(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                             if (No > 0)
                             {
                                 MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                 ClearForm(pnlPerLeave.Controls);
                                 e.Cancel = true;
                             }
                         }

                         if (txtOption.Text == "V")
                         {
                             Dictionary<string, object> param = new Dictionary<string, object>();
                             param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                             Result rsltCode;
                             rsltCode = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "W_PL_BAL_COUNT", param);

                             if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                                 No = Convert.ToInt32(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                             if (No == 0)
                             {
                                 MessageBox.Show("Record Not Found", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                 ClearForm(pnlPerLeave.Controls);
                                 e.Cancel = true;
                                 return;
                             }

                             //Open PopUP --Do You Want to View More REcords
                             DialogResult dr = MessageBox.Show("Do You Want To View More Records [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                             if (dr == DialogResult.Yes)
                             {
                                 ClearForm(pnlPerLeave.Controls);
                                 txt_LSH_PR_NO.Select();
                                 txt_LSH_PR_NO.Focus();
                             }
                             else
                             {
                                 base.Cancel();
                                 txt_W_OPTION_DIS.Text = string.Empty;
                                 txtOption.Select();
                                 txtOption.Focus();
                             }
                         }

                         if (txtOption.Text == "D")
                         {
                             //open PopUP --Do You Want to Delete More REcords

                             Dictionary<string, object> param = new Dictionary<string, object>();
                             param.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                             Result rsltCode;
                             rsltCode = cmnDM.GetData("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "W_PL_BAL_COUNT", param);

                             if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                                 No = Convert.ToInt32(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                             if (No == 0)
                             {
                                 MessageBox.Show("Record Not Found", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                 ClearForm(pnlPerLeave.Controls);
                                 e.Cancel = true;
                                 return;
                             }

                             DialogResult drdel = MessageBox.Show("Do You Want To Delete Records [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                             if (drdel == DialogResult.Yes)
                             {
                                 /*Delete Records First from LEV_SCHEDULE_REP Table, against the current PR_P_NO*/
                                 Dictionary<string, object> paramD = new Dictionary<string, object>();
                                 //param1.Add("LSH_PR_NO", txt_LSH_PR_NO.Text);
                                 Result rsltCodeD;
                                 rsltCodeD = cmnDM.Execute("CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER", "DELETE_LEV_SCHEDULE_REP", param);

                                 base.Delete();
                                 base.m_intPKID = Convert.ToInt32(txtID.Text);
                                 base.Delete_Click(pnlPerLeave, "Delete");
                                 delRec = "RecDeleted";

                                 ClearForm(pnlPerLeave.Controls);
                                 txt_LSH_PR_NO.Select();
                                 txt_LSH_PR_NO.Focus();
                             }
                             else
                             {
                                 delRec = "NotDeleted";
                                 base.Cancel();
                                 txtOption.Select();
                                 txtOption.Focus();
                             }
                         }


                         if (txtOption.Text == "Q" || txtOption.Text == "V")
                         {
                             ReadonlyControl(true);
                             EnableControl(true);
                             TabStopControl(false);
                         }
                         else
                         {
                             base.IterateFormToEnableControls(pnlPerLeave.Controls, true);
                             ReadonlyControl(false);
                             EnableControl(true);
                             TabStopControl(true);
                             txt_LSH_JAN1.Select();
                             txt_LSH_JAN1.Focus();

                         }

                         if (delRec == "RecDeleted")
                         {
                             txt_LSH_PR_NO.Select();
                             txt_LSH_PR_NO.Focus();
                             delRec = string.Empty;
                         }
                         else if (delRec == "NotDeleted")
                         {
                             txtOption.Select();
                             txtOption.Focus();
                             delRec = string.Empty;
                         }
                     }
                     else
                     {
                         ClearForm(pnlPerLeave.Controls);
                         txtOption.Select();
                         txtOption.Focus();
                     }
                 }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_LSH_PR_NO_Validating", exp);
            }
        }
       
        #region --TextBox_Validating---
        /// <summary>
        /// VAlidate LSH_JAN1 Textbox: Only can input X or E
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_LSH_JAN1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JAN1.Text != string.Empty && txt_LSH_JAN1.Text != "X" && txt_LSH_JAN1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JAN1.Text == "E")
            {
                txt_LSH_JAN1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JAN2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JAN2.Text != string.Empty && txt_LSH_JAN2.Text != "X" && txt_LSH_JAN2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JAN2.Text == "E")
            {
                txt_LSH_JAN2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JAN3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JAN3.Text != string.Empty && txt_LSH_JAN3.Text != "X" && txt_LSH_JAN3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JAN3.Text == "E")
            {
                txt_LSH_JAN3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JAN4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JAN4.Text != string.Empty && txt_LSH_JAN4.Text != "X" && txt_LSH_JAN4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JAN4.Text == "E")
            {
                txt_LSH_JAN4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_FEB1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_FEB1.Text != string.Empty && txt_LSH_FEB1.Text != "X" && txt_LSH_FEB1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_FEB1.Text == "E")
            {
                txt_LSH_FEB1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_FEB2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_FEB2.Text != string.Empty && txt_LSH_FEB2.Text != "X" && txt_LSH_FEB2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_FEB2.Text == "E")
            {
                txt_LSH_FEB2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_FEB3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_FEB3.Text != string.Empty && txt_LSH_FEB3.Text != "X" && txt_LSH_FEB3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_FEB3.Text == "E")
            {
                txt_LSH_FEB3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_FEB4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_FEB4.Text != string.Empty && txt_LSH_FEB4.Text != "X" && txt_LSH_FEB4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_FEB4.Text == "E")
            {
                txt_LSH_FEB4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAR1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAR1.Text != string.Empty && txt_LSH_MAR1.Text != "X" && txt_LSH_MAR1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAR1.Text == "E")
            {
                txt_LSH_MAR1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAR2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAR2.Text != string.Empty && txt_LSH_MAR2.Text != "X" && txt_LSH_MAR2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAR2.Text == "E")
            {
                txt_LSH_MAR2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAR3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAR3.Text != string.Empty && txt_LSH_MAR3.Text != "X" && txt_LSH_MAR3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAR3.Text == "E")
            {
                txt_LSH_MAR3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAR4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAR4.Text != string.Empty && txt_LSH_MAR4.Text != "X" && txt_LSH_MAR4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAR4.Text == "E")
            {
                txt_LSH_MAR4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_APR1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_APR1.Text != string.Empty && txt_LSH_APR1.Text != "X" && txt_LSH_APR1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_APR1.Text == "E")
            {
                txt_LSH_APR1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_APR2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_APR2.Text != string.Empty && txt_LSH_APR2.Text != "X" && txt_LSH_APR2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_APR2.Text == "E")
            {
                txt_LSH_APR2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_APR3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_APR3.Text != string.Empty && txt_LSH_APR3.Text != "X" && txt_LSH_APR3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_APR3.Text == "E")
            {
                txt_LSH_APR3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_APR4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_APR4.Text != string.Empty && txt_LSH_APR4.Text != "X" && txt_LSH_APR4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_APR4.Text == "E")
            {
                txt_LSH_APR4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAY1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAY1.Text != string.Empty && txt_LSH_MAY1.Text != "X" && txt_LSH_MAY1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAY1.Text == "E")
            {
                txt_LSH_MAY1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAY2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAY2.Text != string.Empty && txt_LSH_MAY2.Text != "X" && txt_LSH_MAY2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAY2.Text == "E")
            {
                txt_LSH_MAY2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAY3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAY3.Text != string.Empty && txt_LSH_MAY3.Text != "X" && txt_LSH_MAY3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAY3.Text == "E")
            {
                txt_LSH_MAY3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_MAY4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_MAY4.Text != string.Empty && txt_LSH_MAY4.Text != "X" && txt_LSH_MAY4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_MAY4.Text == "E")
            {
                txt_LSH_MAY4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUN1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUN1.Text != string.Empty && txt_LSH_JUN1.Text != "X" && txt_LSH_JUN1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUN1.Text == "E")
            {
                txt_LSH_JUN1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUN2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUN2.Text != string.Empty && txt_LSH_JUN2.Text != "X" && txt_LSH_JUN2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUN2.Text == "E")
            {
                txt_LSH_JUN2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUN3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUN3.Text != string.Empty && txt_LSH_JUN3.Text != "X" && txt_LSH_JUN3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUN3.Text == "E")
            {
                txt_LSH_JUN3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUN4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUN4.Text != string.Empty && txt_LSH_JUN4.Text != "X" && txt_LSH_JUN4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUN4.Text == "E")
            {
                txt_LSH_JUN4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUL1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUL1.Text != string.Empty && txt_LSH_JUL1.Text != "X" && txt_LSH_JUL1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUL1.Text == "E")
            {
                txt_LSH_JUL1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUL2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUL2.Text != string.Empty && txt_LSH_JUL2.Text != "X" && txt_LSH_JUL2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUL2.Text == "E")
            {
                txt_LSH_JUL2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUL3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUL3.Text != string.Empty && txt_LSH_JUL3.Text != "X" && txt_LSH_JUL3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUL3.Text == "E")
            {
                txt_LSH_JUL3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_JUL4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_JUL4.Text != string.Empty && txt_LSH_JUL4.Text != "X" && txt_LSH_JUL4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_JUL4.Text == "E")
            {
                txt_LSH_JUL4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_AUG1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_AUG1.Text != string.Empty && txt_LSH_AUG1.Text != "X" && txt_LSH_AUG1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_AUG1.Text == "E")
            {
                txt_LSH_AUG1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_AUG2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_AUG2.Text != string.Empty && txt_LSH_AUG2.Text != "X" && txt_LSH_AUG2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_AUG2.Text == "E")
            {
                txt_LSH_AUG2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_AUG3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_AUG3.Text != string.Empty && txt_LSH_AUG3.Text != "X" && txt_LSH_AUG3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_AUG3.Text == "E")
            {
                txt_LSH_AUG3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_AUG4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_AUG4.Text != string.Empty && txt_LSH_AUG4.Text != "X" && txt_LSH_AUG4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_AUG4.Text == "E")
            {
                txt_LSH_AUG4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_SEP1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_SEP1.Text != string.Empty && txt_LSH_SEP1.Text != "X" && txt_LSH_SEP1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_SEP1.Text == "E")
            {
                txt_LSH_SEP1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_SEP2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_SEP2.Text != string.Empty && txt_LSH_SEP2.Text != "X" && txt_LSH_SEP2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_SEP2.Text == "E")
            {
                txt_LSH_SEP2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_SEP3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_SEP3.Text != string.Empty && txt_LSH_SEP3.Text != "X" && txt_LSH_SEP3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_SEP3.Text == "E")
            {
                txt_LSH_SEP3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_SEP4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_SEP4.Text != string.Empty && txt_LSH_SEP4.Text != "X" && txt_LSH_SEP4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_SEP4.Text == "E")
            {
                txt_LSH_SEP4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_OCT1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_OCT1.Text != string.Empty && txt_LSH_OCT1.Text != "X" && txt_LSH_OCT1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_OCT1.Text == "E")
            {
                txt_LSH_OCT1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_OCT2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_OCT2.Text != string.Empty && txt_LSH_OCT2.Text != "X" && txt_LSH_OCT2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_OCT2.Text == "E")
            {
                txt_LSH_OCT2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_OCT3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_OCT3.Text != string.Empty && txt_LSH_OCT3.Text != "X" && txt_LSH_OCT3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_OCT3.Text == "E")
            {
                txt_LSH_OCT3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_OCT4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_OCT4.Text != string.Empty && txt_LSH_OCT4.Text != "X" && txt_LSH_OCT4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_OCT4.Text == "E")
            {
                txt_LSH_OCT4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_NOV1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_NOV1.Text != string.Empty && txt_LSH_NOV1.Text != "X" && txt_LSH_NOV1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_NOV1.Text == "E")
            {
                txt_LSH_NOV1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_NOV2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_NOV2.Text != string.Empty && txt_LSH_NOV2.Text != "X" && txt_LSH_NOV2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_NOV2.Text == "E")
            {
                txt_LSH_NOV2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_NOV3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_NOV3.Text != string.Empty && txt_LSH_NOV3.Text != "X" && txt_LSH_NOV3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_NOV3.Text == "E")
            {
                txt_LSH_NOV3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_NOV4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_NOV4.Text != string.Empty && txt_LSH_NOV4.Text != "X" && txt_LSH_NOV4.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_NOV4.Text == "E")
            {
                txt_LSH_NOV4.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_DEC1_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_DEC1.Text != string.Empty && txt_LSH_DEC1.Text != "X" && txt_LSH_DEC1.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_DEC1.Text == "E")
            {
                txt_LSH_DEC1.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_DEC2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_DEC2.Text != string.Empty && txt_LSH_DEC2.Text != "X" && txt_LSH_DEC2.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_DEC2.Text == "E")
            {
                txt_LSH_DEC2.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_DEC3_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_DEC3.Text != string.Empty && txt_LSH_DEC3.Text != "X" && txt_LSH_DEC3.Text != "E")
            {
                MessageBox.Show("Field Must Be Enter [X or E]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_DEC3.Text == "E")
            {
                txt_LSH_DEC3.Text = null;
                DoAction();
            }
        }

        private void txt_LSH_DEC4_Validating(object sender, CancelEventArgs e)
        {
            if (txt_LSH_DEC4.Text != string.Empty && txt_LSH_DEC4.Text != "X" )
            {
                MessageBox.Show("Field Must Be Enter [X]....", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_LSH_DEC4.Text == "X" || txt_LSH_DEC4.Text == string.Empty)
            {
                //txt_LSH_DEC4.Text = null;
                DoAction();
            }
        }

        #endregion

        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
            if (txtOption.Focused)
            {
                if (txtOption.Text == "V")
                    txt_W_OPTION_DIS.Text = "VIEW";
                else if (txtOption.Text == "A")
                    txt_W_OPTION_DIS.Text = "ADD";
                else if (txtOption.Text == "D")
                    txt_W_OPTION_DIS.Text = "DELETE";
                else if (txtOption.Text == "M")
                    txt_W_OPTION_DIS.Text = "MODIFY";
                else if (txtOption.Text == "Q")
                {
                    txt_W_OPTION_DIS.Text = "QUERY";
                }
                else if (txtOption.Text == "E")
                    txt_W_OPTION_DIS.Text = "EXIT";

                txtDate.Text = this.Now().ToString("dd/MM/yyyy");


                if (txtOption.Text == "Q" || txtOption.Text == "V")
                {
                    ReadonlyControl(true);
                    EnableControl(true);
                    TabStopControl(false);
                }
                else
                {
                    ReadonlyControl(false);
                    EnableControl(false);
                    TabStopControl(true);
                }
            }
        }

        private void txtOption_TextChanged(object sender, EventArgs e)
        {
            if (txtOption.Text == "Q")
            {
                base.ShowList(pnlPerLeave, "LIST");

                txt_LSH_PR_NO.Select();
                txt_LSH_PR_NO.Focus();
                base.IterateFormToEnableControls(pnlPerLeave.Controls, true);

                ReadonlyControl(true);
                EnableControl(true);
                TabStopControl(false);
                txt_LSH_PR_NO.ReadOnly = false;
                txt_LSH_PR_NO.Enabled = true;
                txt_LSH_PR_NO.Select();
                txt_LSH_PR_NO.Focus();
            }

            if (txtOption.Text == "Q" || txtOption.Text == "V")
            {
                ReadonlyControl(true);
                EnableControl(true);
                TabStopControl(false);
            }
            else
            {
                ReadonlyControl(false);
                EnableControl(false);
                TabStopControl(true);
            }
        }

        #endregion
    }
}