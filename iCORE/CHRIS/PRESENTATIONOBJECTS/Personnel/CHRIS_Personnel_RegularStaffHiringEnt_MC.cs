﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_MC : Form
    {

        public CHRIS_Personnel_RegularStaffHiringEnt_MC()
        {
            InitializeComponent();
            // pnlPersonnelMain.Enabled = false;


            #region Personnel Panel Disable 

            this.txt_PR_FIRST_NAME.ReadOnly = true;
            this.txt_PR_LEVEL.ReadOnly = true;
            this.txt_F_RNAME.ReadOnly = true;
            this.txt_PR_LAST_NAME.ReadOnly = true;
            this.txt_PR_BRANCH.ReadOnly = true;
            this.txt_PR_DESIG.ReadOnly = true;
            this.txtReportTo.ReadOnly = true;
           // this.txt_PR_CATEGORY.ReadOnly = true;
            this.txt_PR_ANNUAL_PACK.ReadOnly = true;
            this.txt_PR_EMP_TYPE.ReadOnly = true;
            this.txt_F_GEID_NO.ReadOnly = true;
            this.txt_PR_RELIGION.ReadOnly = true;
            this.txt_W_RELIGION.ReadOnly = true;
            this.txt_PR_FUNC_Title2.ReadOnly = true;
            this.txt_PR_FUNC_Title1.ReadOnly = true;
            this.txt_PR_ACCOUNT_NO.ReadOnly = true;
            this.txt_PR_NATIONAL_TAX.ReadOnly = true;
            this.txt_PR_TAX_INC.ReadOnly = true;
            this.txt_PR_BANK_ID.ReadOnly = true;
            this.txt_PR_CLOSE_FLAG.ReadOnly = true;
            this.txt_PR_NEW_BRANCH.ReadOnly = true;
            this.txt_PR_CONF_FLAG.ReadOnly = true;
            this.txt_PR_NEW_ANNUAL_PACK.ReadOnly = true;
            this.txt_PR_P_NO.ReadOnly = true;
            //this.txt_PR_JOINING_DATE ;
            //this.txt_PR_CONFIRM;
            //private CrplControlLibrary.LookupButton lbtnRepNo;
            this.txt_PR_ADD1.ReadOnly = true;
            this.txt_PR_PHONE2.ReadOnly = true;
            this.txt_PR_PHONE1.ReadOnly = true;
            this.txt_PR_ADD2.ReadOnly = true;
            this.txt_PR_LANG2.ReadOnly = true;
            this.txt_PR_LANG1.ReadOnly = true;
            this.txt_w_date_2.ReadOnly = true;
            //this.txt_PR_D_BIRTH.ReadOnly = true;
            this.txt_PR_LANG6.ReadOnly = true;
            this.txt_PR_LANG5.ReadOnly = true;
            this.txt_PR_LANG4.ReadOnly = true;
            this.txt_PR_LANG3.ReadOnly = true;
            this.txt_PR_MARITAL.ReadOnly = true;
            this.txt_PR_OLD_ID_CARD_NO.ReadOnly = true;
            this.txt_PR_SEX.ReadOnly = true;
            this.txt_PR_ID_CARD_NO.ReadOnly = true;
            this.txt_PR_USER_IS_PRIME.ReadOnly = true;
            this.txt_PR_GROUP_HOSP.ReadOnly = true;
            this.txt_PR_OPD.ReadOnly = true;
            this.txt_PR_GROUP_LIFE.ReadOnly = true;
            this.txt_PR_NO_OF_CHILD.ReadOnly = true;
            this.txt_PR_SPOUSE.ReadOnly = true;
            //this.txt_PR_MARRIAGE.ReadOnly = true;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Name;
            //private System.Windows.Forms.DataGridViewTextBoxColumn prPNo;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add1;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add2;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add3;
            //private System.Windows.Forms.DataGridViewTextBoxColumn PrEYear;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Degree;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Grade;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Pr_College;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Pr_City;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Country;
            //private System.Windows.Forms.DataGridViewTextBoxColumn Pr_E_No;
            //public CrplControlLibrary.SLTextBox txt_PR_TAX_PAID;
            //public CrplControlLibrary.SLTextBox txt_w_date_2;
            //public CrplControlLibrary.SLTextBox txt_PR_BIRTH_SP;
            //private System.Windows.Forms.DataGridViewTextBoxColumn PrcNo;
            //private System.Windows.Forms.DataGridViewTextBoxColumn PRCHILDNAME;
            //private System.Windows.Forms.DataGridViewTextBoxColumn PRDATEBIRTH;
            //public CrplControlLibrary.SLDatePicker txt_PR_EXPIRY;
            //public CrplControlLibrary.SLDatePicker txt_PR_ID_ISSUE;
            //public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlView;
            //private System.Windows.Forms.Label label20;
            //private System.Windows.Forms.TextBox textBox1;
            //private System.Windows.Forms.Label label21;
            //public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlSave;
            //public CrplControlLibrary.SLTextBox txt_W_ADD_ANS;
            //private System.Windows.Forms.Label label19;
            //private System.Windows.Forms.TextBox txt_W_VIEW_ANS;
            //private System.Windows.Forms.Label label28;
            //public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDelete;
            //public CrplControlLibrary.SLTextBox txt_W_DEL_ANS;
            //private System.Windows.Forms.Label label22;
            //private CrplControlLibrary.SLTextBox txtLoc;
            //private System.Windows.Forms.Label label29;
            //public CrplControlLibrary.SLTextBox txt_W_OPTION_DIS;
            //private CrplControlLibrary.SLTextBox txtID;
            //private System.Windows.Forms.Label label30;
            //private System.Windows.Forms.Label lblUserName;
            //private CrplControlLibrary.SLTextBox slTxtbEmail;
            //private System.Windows.Forms.Label lbEmail;
            //private System.Windows.Forms.Button button1;
            //private CrplControlLibrary.LookupButton lkpBtnAuthorize;
            //private System.Windows.Forms.Label label31;
            //private CrplControlLibrary.SLTextBox slTxtBxUserID;

            #endregion

            #region Fill the Data 

            DataTable dt = new DataTable();
            dt = SQLManager.CHRIS_SP_GET_PersonnelInfo(CHRIS_Personnel_PersonalEntryChecker.SetValuePR_NO).Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dtrow in dt.Rows)
                {
                    this.txt_PR_P_NO.Text = string.IsNullOrEmpty(dtrow["PR_P_NO"].ToString().Trim()) ? "" : dtrow["PR_P_NO"].ToString().Trim();
                    this.txt_PR_BRANCH.Text = string.IsNullOrEmpty(dtrow["PR_BRANCH"].ToString().Trim()) ? "" : dtrow["PR_BRANCH"].ToString().Trim();
                    this.txt_PR_FIRST_NAME.Text = string.IsNullOrEmpty(dtrow["PR_FIRST_NAME"].ToString().Trim()) ? "" : dtrow["PR_FIRST_NAME"].ToString().Trim();
                    this.txt_PR_LAST_NAME.Text = string.IsNullOrEmpty(dtrow["PR_LAST_NAME"].ToString().Trim()) ? "" : dtrow["PR_LAST_NAME"].ToString().Trim();
                    this.txt_PR_DESIG.Text = string.IsNullOrEmpty(dtrow["PR_DESIG"].ToString().Trim()) ? "" : dtrow["PR_DESIG"].ToString().Trim();
                    this.txt_PR_LEVEL.Text = string.IsNullOrEmpty(dtrow["PR_LEVEL"].ToString().Trim()) ? "" : dtrow["PR_LEVEL"].ToString().Trim();
                  //  this.txt_PR_CATEGORY.Text = string.IsNullOrEmpty(dtrow["PR_CATEGORY"].ToString().Trim()) ? "" : dtrow["PR_CATEGORY"].ToString().Trim();
                    this.txt_PR_FUNC_Title1.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_TITTLE1"].ToString().Trim()) ? "" : dtrow["PR_FUNC_TITTLE1"].ToString().Trim();
                    this.txt_PR_FUNC_Title2.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_TITTLE2"].ToString().Trim()) ? "" : dtrow["PR_FUNC_TITTLE2"].ToString().Trim();
                    this.txt_PR_JOINING_DATE.Text = string.IsNullOrEmpty(dtrow["PR_JOINING_DATE"].ToString().Trim()) ? "" : dtrow["PR_JOINING_DATE"].ToString().Trim();
                    this.txt_PR_CONFIRM.Text = string.IsNullOrEmpty(dtrow["PR_CONFIRM"].ToString().Trim()) ? "" : dtrow["PR_CONFIRM"].ToString().Trim();
                    this.txt_PR_ANNUAL_PACK.Text = string.IsNullOrEmpty(dtrow["PR_ANNUAL_PACK"].ToString().Trim()) ? "" : dtrow["PR_ANNUAL_PACK"].ToString().Trim();
                    this.txt_PR_NATIONAL_TAX.Text = string.IsNullOrEmpty(dtrow["PR_NATIONAL_TAX"].ToString().Trim()) ? "" : dtrow["PR_NATIONAL_TAX"].ToString().Trim();
                    this.txt_PR_ACCOUNT_NO.Text = string.IsNullOrEmpty(dtrow["PR_ACCOUNT_NO"].ToString().Trim()) ? "" : dtrow["PR_ACCOUNT_NO"].ToString().Trim();
                    this.txt_PR_BANK_ID.Text = string.IsNullOrEmpty(dtrow["PR_BANK_ID"].ToString().Trim()) ? "" : dtrow["PR_BANK_ID"].ToString().Trim();
                    this.txt_PR_ID_ISSUE.Text = string.IsNullOrEmpty(dtrow["PR_ID_ISSUE"].ToString().Trim()) ? "" : dtrow["PR_ID_ISSUE"].ToString().Trim();
                    this.txt_PR_EXPIRY.Text = string.IsNullOrEmpty(dtrow["PR_EXPIRY"].ToString().Trim()) ? "" : dtrow["PR_EXPIRY"].ToString().Trim();
                    this.txt_PR_TAX_INC.Text = string.IsNullOrEmpty(dtrow["PR_TAX_INC"].ToString().Trim()) ? "" : dtrow["PR_TAX_INC"].ToString().Trim();
                    this.txt_PR_TAX_PAID.Text = string.IsNullOrEmpty(dtrow["PR_TAX_PAID"].ToString().Trim()) ? "" : dtrow["PR_TAX_PAID"].ToString().Trim();
                   // this.txt_PR_TAX_USED.Text = string.IsNullOrEmpty(dtrow["PR_TAX_USED"].ToString().Trim()) ? "" : dtrow["PR_TAX_USED"].ToString().Trim();
                    this.txt_PR_CLOSE_FLAG.Text = string.IsNullOrEmpty(dtrow["PR_CLOSE_FLAG"].ToString().Trim()) ? "" : dtrow["PR_CLOSE_FLAG"].ToString().Trim();
                    // this.txt_PR_CONFIRM_ON.Text = string.IsNullOrEmpty(dtrow["PR_CONFIRM_ON"].ToString().Trim()) ? "" : dtrow["PR_CONFIRM_ON"].ToString().Trim();
                    //this.txt_PR_EXPECTED.Text = string.IsNullOrEmpty(dtrow["PR_EXPECTED"].ToString().Trim()) ? "" : dtrow["PR_EXPECTED"].ToString().Trim();
                    //this.txt_PR_TERMIN_TYPE.Text = string.IsNullOrEmpty(dtrow["PR_TERMIN_TYPE"].ToString().Trim()) ? "" : dtrow["PR_TERMIN_TYPE"].ToString().Trim();
                    //this.txt_PR_TERMIN_DATE.Text = string.IsNullOrEmpty(dtrow["PR_TERMIN_DATE"].ToString().Trim()) ? "" : dtrow["PR_TERMIN_DATE"].ToString().Trim();
                    //this.txt_PR_REASONS.Text = string.IsNullOrEmpty(dtrow["PR_REASONS"].ToString().Trim()) ? "" : dtrow["PR_REASONS"].ToString().Trim();
                    //this.txt_PR_RESIG_RECV.Text = string.IsNullOrEmpty(dtrow["PR_RESIG_RECV"].ToString().Trim()) ? "" : dtrow["PR_RESIG_RECV"].ToString().Trim();
                    //this.txt_PR_APP_RESIG.Text = string.IsNullOrEmpty(dtrow["PR_APP_RESIG"].ToString().Trim()) ? "" : dtrow["PR_APP_RESIG"].ToString().Trim();
                    //this.txt_PR_EXIT_INTER.Text = string.IsNullOrEmpty(dtrow["PR_EXIT_INTER"].ToString().Trim()) ? "" : dtrow["PR_EXIT_INTER"].ToString().Trim();
                    //this.txt_PR_ID_RETURN.Text = string.IsNullOrEmpty(dtrow["PR_ID_RETURN"].ToString().Trim()) ? "" : dtrow["PR_ID_RETURN"].ToString().Trim();
                    //this.txt_PR_NOTICE.Text = string.IsNullOrEmpty(dtrow["PR_NOTICE"].ToString().Trim()) ? "" : dtrow["PR_NOTICE"].ToString().Trim();
                    //this.txt_PR_ARTONY.Text = string.IsNullOrEmpty(dtrow["PR_ARTONY"].ToString().Trim()) ? "" : dtrow["PR_ARTONY"].ToString().Trim();
                    //this.txt_PR_DELETION.Text = string.IsNullOrEmpty(dtrow["PR_DELETION"].ToString().Trim()) ? "" : dtrow["PR_DELETION"].ToString().Trim();
                    //this.txt_PR_SETTLE.Text = string.IsNullOrEmpty(dtrow["PR_SETTLE"].ToString().Trim()) ? "" : dtrow["PR_SETTLE"].ToString().Trim();
                    //this.txt_PR_CONF_FLAG.Text = string.IsNullOrEmpty(dtrow["PR_CONF_FLAG"].ToString().Trim()) ? "" : dtrow["PR_CONF_FLAG"].ToString().Trim();
                    //this.txt_PR_LAST_INCREMENT.Text = string.IsNullOrEmpty(dtrow["PR_LAST_INCREMENT"].ToString().Trim()) ? "" : dtrow["PR_LAST_INCREMENT"].ToString().Trim();
                    //this.txt_PR_NEXT_INCREMENT.Text = string.IsNullOrEmpty(dtrow["PR_NEXT_INCREMENT"].ToString().Trim()) ? "" : dtrow["PR_NEXT_INCREMENT"].ToString().Trim();
                    //this.txt_PR_TRANSFER_DATE.Text = string.IsNullOrEmpty(dtrow["PR_TRANSFER_DATE"].ToString().Trim()) ? "" : dtrow["PR_TRANSFER_DATE"].ToString().Trim();
                    //this.txt_PR_PROMOTION_DATE.Text = string.IsNullOrEmpty(dtrow["PR_PROMOTION_DATE"].ToString().Trim()) ? "" : dtrow["PR_PROMOTION_DATE"].ToString().Trim();
                    //this.txt_PR_NEW_BRANCH.Text = string.IsNullOrEmpty(dtrow["PR_NEW_BRANCH"].ToString().Trim()) ? "" : dtrow["PR_NEW_BRANCH"].ToString().Trim();
                    //this.txt_PR_NEW_ANNUAL_PACK.Text = string.IsNullOrEmpty(dtrow["PR_NEW_ANNUAL_PACK"].ToString().Trim()) ? "" : dtrow["PR_NEW_ANNUAL_PACK"].ToString().Trim();
                    //this.txt_PR_TRANSFER.Text = string.IsNullOrEmpty(dtrow["PR_TRANSFER"].ToString().Trim()) ? "" : dtrow["PR_TRANSFER"].ToString().Trim();
                    //this.txt_PR_MONTH_AWARD.Text = string.IsNullOrEmpty(dtrow["PR_MONTH_AWARD"].ToString().Trim()) ? "" : dtrow["PR_MONTH_AWARD"].ToString().Trim();
                    //this.txt_PR_ZAKAT_AMT.Text = string.IsNullOrEmpty(dtrow["PR_ZAKAT_AMT"].ToString().Trim()) ? "" : dtrow["PR_ZAKAT_AMT"].ToString().Trim();
                    //this.txt_PR_MED_FLAG.Text = string.IsNullOrEmpty(dtrow["PR_MED_FLAG"].ToString().Trim()) ? "" : dtrow["PR_MED_FLAG"].ToString().Trim();
                    //this.txt_PR_ATT_FLAG.Text = string.IsNullOrEmpty(dtrow["PR_ATT_FLAG"].ToString().Trim()) ? "" : dtrow["PR_ATT_FLAG"].ToString().Trim();
                    //this.txt_PR_PAY_FLAG.Text = string.IsNullOrEmpty(dtrow["PR_PAY_FLAG"].ToString().Trim()) ? "" : dtrow["PR_PAY_FLAG"].ToString().Trim();
                    //this.txt_PR_REP_NO.Text = string.IsNullOrEmpty(dtrow["PR_REP_NO"].ToString().Trim()) ? "" : dtrow["PR_REP_NO"].ToString().Trim();
                    //this.txt_PR_REFUND_AMT.Text = string.IsNullOrEmpty(dtrow["PR_REFUND_AMT"].ToString().Trim()) ? "" : dtrow["PR_REFUND_AMT"].ToString().Trim();
                    //this.txt_PR_REFUND_FOR.Text = string.IsNullOrEmpty(dtrow["PR_REFUND_FOR"].ToString().Trim()) ? "" : dtrow["PR_REFUND_FOR"].ToString().Trim();
                    //this.txt_PR_NTN_CERT_REC.Text = string.IsNullOrEmpty(dtrow["PR_NTN_CERT_REC"].ToString().Trim()) ? "" : dtrow["PR_NTN_CERT_REC"].ToString().Trim();
                    //this.txt_PR_NTN_CARD_REC.Text = string.IsNullOrEmpty(dtrow["PR_NTN_CARD_REC"].ToString().Trim()) ? "" : dtrow["PR_NTN_CARD_REC"].ToString().Trim();
                    this.txt_PR_EMP_TYPE.Text = string.IsNullOrEmpty(dtrow["PR_EMP_TYPE"].ToString().Trim()) ? "" : dtrow["PR_EMP_TYPE"].ToString().Trim();
                    this.txtLoc.Text = string.IsNullOrEmpty(dtrow["LOCATION"].ToString().Trim()) ? "" : dtrow["LOCATION"].ToString().Trim();
                    //this.txt_ID.Text = string.IsNullOrEmpty(dtrow["ID"].ToString().Trim()) ? "" : dtrow["ID"].ToString().Trim();
                    this.slTxtbEmail.Text = string.IsNullOrEmpty(dtrow["PR_EMAIL"].ToString().Trim()) ? "" : dtrow["PR_EMAIL"].ToString().Trim();
                    //this.txt_PR_UserID.Text = string.IsNullOrEmpty(dtrow["PR_UserID"].ToString().Trim()) ? "" : dtrow["PR_UserID"].ToString().Trim();
                    //this.txt_PR_UploadDate.Text = string.IsNullOrEmpty(dtrow["PR_UploadDate"].ToString().Trim()) ? "" : dtrow["PR_UploadDate"].ToString().Trim();
                    //this.txt_PR_User_Approved.Text = string.IsNullOrEmpty(dtrow["PR_User_Approved"].ToString().Trim()) ? "" : dtrow["PR_User_Approved"].ToString().Trim();
                    //this.txt_PR_SEGMENT.Text = string.IsNullOrEmpty(dtrow["PR_SEGMENT"].ToString().Trim()) ? "" : dtrow["PR_SEGMENT"].ToString().Trim();
                    //this.txt_PR_DEPT.Text = string.IsNullOrEmpty(dtrow["PR_DEPT"].ToString().Trim()) ? "" : dtrow["PR_DEPT"].ToString().Trim();
                    //this.txt_PR_CONTRIBUTION.Text = string.IsNullOrEmpty(dtrow["PR_CONTRIBUTION"].ToString().Trim()) ? "" : dtrow["PR_CONTRIBUTION"].ToString().Trim();
                    //this.txt_PR_COSTCODE.Text = string.IsNullOrEmpty(dtrow["PR_COSTCODE"].ToString().Trim()) ? "" : dtrow["PR_COSTCODE"].ToString().Trim();
                    //this.txt_PR_SOEID.Text = string.IsNullOrEmpty(dtrow["PR_SOEID"].ToString().Trim()) ? "" : dtrow["PR_SOEID"].ToString().Trim();

                    //Personel information
                    this.txt_PR_ADD1.Text = string.IsNullOrEmpty(dtrow["PR_ADD1"].ToString().Trim()) ? "" : dtrow["PR_ADD1"].ToString().Trim();
                    this.txt_PR_ADD2.Text = string.IsNullOrEmpty(dtrow["PR_ADD2"].ToString().Trim()) ? "" : dtrow["PR_ADD2"].ToString().Trim();
                    this.txt_PR_PHONE1.Text = string.IsNullOrEmpty(dtrow["PR_PHONE1"].ToString().Trim()) ? "" : dtrow["PR_PHONE1"].ToString().Trim();
                    this.txt_PR_PHONE2.Text = string.IsNullOrEmpty(dtrow["PR_PHONE2"].ToString().Trim()) ? "" : dtrow["PR_PHONE2"].ToString().Trim();
                    this.txt_PR_D_BIRTH.Text = string.IsNullOrEmpty(dtrow["PR_D_BIRTH"].ToString().Trim()) ? "" : dtrow["PR_D_BIRTH"].ToString().Trim();
                    this.txt_PR_SEX.Text = string.IsNullOrEmpty(dtrow["PR_SEX"].ToString().Trim()) ? "" : dtrow["PR_SEX"].ToString().Trim();
                    this.txt_PR_MARITAL.Text = string.IsNullOrEmpty(dtrow["PR_MARITAL"].ToString().Trim()) ? "" : dtrow["PR_MARITAL"].ToString().Trim();
                    this.txt_PR_SPOUSE.Text = string.IsNullOrEmpty(dtrow["PR_SPOUSE"].ToString().Trim()) ? "" : dtrow["PR_SPOUSE"].ToString().Trim();
                    this.txt_PR_BIRTH_SP.Text = string.IsNullOrEmpty(dtrow["PR_BIRTH_SP"].ToString().Trim()) ? "" : dtrow["PR_BIRTH_SP"].ToString().Trim();
                    this.txt_PR_NO_OF_CHILD.Text = string.IsNullOrEmpty(dtrow["PR_NO_OF_CHILD"].ToString().Trim()) ? "" : dtrow["PR_NO_OF_CHILD"].ToString().Trim();
                    this.txt_PR_ID_CARD_NO.Text = string.IsNullOrEmpty(dtrow["PR_ID_CARD_NO"].ToString().Trim()) ? "" : dtrow["PR_ID_CARD_NO"].ToString().Trim();
                    this.txt_PR_USER_IS_PRIME.Text = string.IsNullOrEmpty(dtrow["PR_USER_IS_PRIME"].ToString().Trim()) ? "" : dtrow["PR_USER_IS_PRIME"].ToString().Trim();
                    this.txt_PR_GROUP_LIFE.Text = string.IsNullOrEmpty(dtrow["PR_GROUP_LIFE"].ToString().Trim()) ? "" : dtrow["PR_GROUP_LIFE"].ToString().Trim();
                    this.txt_PR_GROUP_HOSP.Text = string.IsNullOrEmpty(dtrow["PR_GROUP_HOSP"].ToString().Trim()) ? "" : dtrow["PR_GROUP_HOSP"].ToString().Trim();
                    this.txt_PR_LANG1.Text = string.IsNullOrEmpty(dtrow["PR_LANG_1"].ToString().Trim()) ? "" : dtrow["PR_LANG_1"].ToString().Trim();
                    this.txt_PR_LANG2.Text = string.IsNullOrEmpty(dtrow["PR_LANG_2"].ToString().Trim()) ? "" : dtrow["PR_LANG_2"].ToString().Trim();
                    this.txt_PR_LANG3.Text = string.IsNullOrEmpty(dtrow["PR_LANG_3"].ToString().Trim()) ? "" : dtrow["PR_LANG_3"].ToString().Trim();
                    this.txt_PR_LANG4.Text = string.IsNullOrEmpty(dtrow["PR_LANG_4"].ToString().Trim()) ? "" : dtrow["PR_LANG_4"].ToString().Trim();
                    this.txt_PR_LANG5.Text = string.IsNullOrEmpty(dtrow["PR_LANG_5"].ToString().Trim()) ? "" : dtrow["PR_LANG_5"].ToString().Trim();
                    this.txt_PR_LANG6.Text = string.IsNullOrEmpty(dtrow["PR_LANG_6"].ToString().Trim()) ? "" : dtrow["PR_LANG_6"].ToString().Trim();
                    this.txt_PR_OPD.Text = string.IsNullOrEmpty(dtrow["PR_OPD"].ToString().Trim()) ? "" : dtrow["PR_OPD"].ToString().Trim();
                    this.txt_PR_MARRIAGE.Text = string.IsNullOrEmpty(dtrow["PR_MARRIAGE"].ToString().Trim()) ? "" : dtrow["PR_MARRIAGE"].ToString().Trim();
                   // this.txt_PR_NATIONALITY.Text = string.IsNullOrEmpty(dtrow["PR_NATIONALITY"].ToString().Trim()) ? "" : dtrow["PR_NATIONALITY"].ToString().Trim();
                  //  this.txt_PR_PER_ADDR.Text = string.IsNullOrEmpty(dtrow["PR_PER_ADDR"].ToString().Trim()) ? "" : dtrow["PR_PER_ADDR"].ToString().Trim();
                  //  this.txt_PR_MOBILE_NO.Text = string.IsNullOrEmpty(dtrow["PR_MOBILE_NO"].ToString().Trim()) ? "" : dtrow["PR_MOBILE_NO"].ToString().Trim();
                    //this.txt_PR_PLC_BIRTH.Text = string.IsNullOrEmpty(dtrow["PR_PLC_BIRTH"].ToString().Trim()) ? "" : dtrow["PR_PLC_BIRTH"].ToString().Trim();
                  //  this.txt_PR_CRNT_PHONE.Text = string.IsNullOrEmpty(dtrow["PR_CRNT_PHONE"].ToString().Trim()) ? "" : dtrow["PR_CRNT_PHONE"].ToString().Trim();
                  //  this.txt_PR_FATHER_NAME.Text = string.IsNullOrEmpty(dtrow["PR_FATHER_NAME"].ToString().Trim()) ? "" : dtrow["PR_FATHER_NAME"].ToString().Trim();
                  //  this.txt_PR_MOTHER_NAME.Text = string.IsNullOrEmpty(dtrow["PR_MOTHER_NAME"].ToString().Trim()) ? "" : dtrow["PR_MOTHER_NAME"].ToString().Trim();
                    this.txt_PR_OLD_ID_CARD_NO.Text = string.IsNullOrEmpty(dtrow["PR_OLD_ID_CARD_NO"].ToString().Trim()) ? "" : dtrow["PR_OLD_ID_CARD_NO"].ToString().Trim();
                   // this.txt_ID.Text = string.IsNullOrEmpty(dtrow["ID"].ToString().Trim()) ? "" : dtrow["ID"].ToString().Trim();


                }

                DataTable dtDept = new DataTable();
                dtDept = SQLManager.CHRIS_SP_GET_Dept_Cont(CHRIS_Personnel_PersonalEntryChecker.SetValuePR_NO).Tables[0];
                dtGVDept.DataSource = dtDept;
                dtGVDept.Update();
                dtGVDept.Visible = true;

                //CHRIS_SP_GET_Education
                DataTable dtEdu = new DataTable();
                dtEdu = SQLManager.CHRIS_SP_GET_Education(CHRIS_Personnel_PersonalEntryChecker.SetValuePR_NO).Tables[0];
                dtGVEdu.DataSource = dtEdu;
                dtGVEdu.Update();
                dtGVEdu.Visible = true;

                //dtGVExperience
                DataTable dtExp = new DataTable();
                dtExp = SQLManager.CHRIS_SP_GET_Experience(CHRIS_Personnel_PersonalEntryChecker.SetValuePR_NO).Tables[0];
                dtGVExp.DataSource = dtExp;
                dtGVExp.Update();
                dtGVExp.Visible = true;
            }
            else
            {

            }

            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            string PR_NO = this.txt_PR_P_NO.Text;
            string userLogin = CHRIS_Personnel_RegularStaffHiringEnt.SetValueUserLogin;
            if (!string.IsNullOrEmpty(PR_NO))
            {
                int id = Convert.ToInt32(PR_NO);
                SQLManager.ApprovedRegularHiring(id, userLogin);

                
                MessageBox.Show("Record Save Successfully");
                this.Close();
            }


        }

        private void btnReject_Click(object sender, EventArgs e)
        {

            string PR_NO = this.txt_PR_P_NO.Text;
            string userLogin = CHRIS_Personnel_RegularStaffHiringEnt.SetValueUserLogin;
            if (!string.IsNullOrEmpty(PR_NO))
            {
                int id = Convert.ToInt32(PR_NO);
                SQLManager.ApprovedRegularHiring(id, userLogin);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }
        }
    }
}
