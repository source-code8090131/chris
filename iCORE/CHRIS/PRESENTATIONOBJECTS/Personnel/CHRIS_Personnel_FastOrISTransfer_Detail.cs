﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_FastOrISTransfer_Detail : Form
    {
        public CHRIS_Personnel_FastOrISTransfer_Detail()
        {
            InitializeComponent();
        }

        private void CHRIS_Personnel_FastOrISTransfer_Detail_Load(object sender, EventArgs e)
        {
           // txtPrPNo.ReadOnly = false;
           // txtPRTransferType.ReadOnly = false;
          //  txtfurloughCity.ReadOnly = false;
            txtDesg.ReadOnly = false;
            txtLevel.ReadOnly = false;
            txtCountry.ReadOnly = false;
            txtCity.ReadOnly = false;
            txtPRDept.ReadOnly = false;
            txtCurrAnnual.ReadOnly = false;
          //  dtFastEndDate.ReadOnly = false;
         //   txtIsCordinate.ReadOnly = false;
            txtPR_REMARKS.ReadOnly = false;
         //   txtPR_EFFECTIVE.ReadOnly = false;
            txtCITI_FLAG1.ReadOnly = false;
            txtCITI_FLAG2.ReadOnly = false;
            txtNewAnnual.ReadOnly = false;
            txtCurrency.ReadOnly = false;
            txtID.ReadOnly = false;
            //   W_TypeOf.ReadOnly = false;
            //   dtJoiningDate.ReadOnly = false;
            //   dtTransferDate.ReadOnly = false;

            DataTable dtDept = new DataTable();
            dtDept = SQLManager.CHRIS_SP_GET_FastTransfer_PRNO(CHRIS_Personnel_General_View.SetValuePR_NO).Tables[0];

            foreach (System.Data.DataRow dtrow in dtDept.Rows)
            {
                txtPersNo.Text = string.IsNullOrEmpty(dtrow["PR_TR_NO"].ToString().Trim()) ? "" : dtrow["PR_TR_NO"].ToString().Trim();
                txtPRTransfer.Text = string.IsNullOrEmpty(dtrow["PR_TRANSFER_TYPE"].ToString().Trim()) ? "" : dtrow["PR_TRANSFER_TYPE"].ToString().Trim();
                txtPR_Func1.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_1"].ToString().Trim()) ? "" : dtrow["PR_FUNC_1"].ToString().Trim();
                txtPR_Func2.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_2"].ToString().Trim()) ? "" : dtrow["PR_FUNC_2"].ToString().Trim();
//.Text = string.IsNullOrEmpty(dtrow["PR_NEW_BRANCH"].ToString().Trim()) ? "" : dtrow["PR_NEW_BRANCH"].ToString().Trim();
//                txtfurloughCity.Text = string.IsNullOrEmpty(dtrow["PR_FURLOUGH"].ToString().Trim()) ? "" : dtrow["PR_FURLOUGH"].ToString().Trim();
                txtDesg.Text = string.IsNullOrEmpty(dtrow["PR_DESG"].ToString().Trim()) ? "" : dtrow["PR_DESG"].ToString().Trim();
                txtPR_DESG.Text = string.IsNullOrEmpty(dtrow["DESIG"].ToString().Trim()) ? "" : dtrow["DESIG"].ToString().Trim();
                txtLevel.Text = string.IsNullOrEmpty(dtrow["PR_LEVEL"].ToString().Trim()) ? "" : dtrow["PR_LEVEL"].ToString().Trim();
                txtCountry.Text = string.IsNullOrEmpty(dtrow["PR_COUNTRY"].ToString().Trim()) ? "" : dtrow["PR_COUNTRY"].ToString().Trim();
                txtCity.Text = string.IsNullOrEmpty(dtrow["PR_CITY"].ToString().Trim()) ? "" : dtrow["PR_CITY"].ToString().Trim();
                txtPRDept.Text = string.IsNullOrEmpty(dtrow["PR_DEPARTMENT_HC"].ToString().Trim()) ? "" : dtrow["PR_DEPARTMENT_HC"].ToString().Trim();
                txtCurrAnnual.Text = string.IsNullOrEmpty(dtrow["PR_ASR_DOL"].ToString().Trim()) ? "" : dtrow["PR_ASR_DOL"].ToString().Trim();
                //dtFastEndDate.Text = string.IsNullOrEmpty(dtrow["PR_FAST_END_DATE"].ToString().Trim()) ? "" : dtrow["PR_FAST_END_DATE"].ToString().Trim();
                //txtIsCordinate.Text = string.IsNullOrEmpty(dtrow["PR_IS_COORDINAT"].ToString().Trim()) ? "" : dtrow["PR_IS_COORDINAT"].ToString().Trim();
                txtPR_REMARKS.Text = string.IsNullOrEmpty(dtrow["PR_REMARKS"].ToString().Trim()) ? "" : dtrow["PR_REMARKS"].ToString().Trim();
                txtName.Text = string.IsNullOrEmpty(dtrow["PR_FIRST_NAME"].ToString().Trim()) ? "" : dtrow["PR_FIRST_NAME"].ToString().Trim();
                slTextBox1.Text = string.IsNullOrEmpty(dtrow["PR_LAST_NAME"].ToString().Trim()) ? "" : dtrow["PR_LAST_NAME"].ToString().Trim();
                txtPR_EFFECTIVE.Text = string.IsNullOrEmpty(dtrow["PR_EFFECTIVE"].ToString().Trim()) ? "" : dtrow["PR_EFFECTIVE"].ToString().Trim();
//.Text = string.IsNullOrEmpty(dtrow["PR_RENT"].ToString().Trim()) ? "" : dtrow["PR_RENT"].ToString().Trim();
//.Text = string.IsNullOrEmpty(dtrow["PR_UTILITIES"].ToString().Trim()) ? "" : dtrow["PR_UTILITIES"].ToString().Trim();
//.Text = string.IsNullOrEmpty(dtrow["PR_TAX_ON_TAX"].ToString().Trim()) ? "" : dtrow["PR_TAX_ON_TAX"].ToString().Trim();
//.Text = string.IsNullOrEmpty(dtrow["PR_OLD_BRANCH"].ToString().Trim()) ? "" : dtrow["PR_OLD_BRANCH"].ToString().Trim();
                txtCITI_FLAG1.Text = string.IsNullOrEmpty(dtrow["CITI_FLAG1"].ToString().Trim()) ? "" : dtrow["CITI_FLAG1"].ToString().Trim();
                txtCITI_FLAG2.Text = string.IsNullOrEmpty(dtrow["CITI_FLAG2"].ToString().Trim()) ? "" : dtrow["CITI_FLAG2"].ToString().Trim();
                txtNewAnnual.Text = string.IsNullOrEmpty(dtrow["PR_NEW_ASR"].ToString().Trim()) ? "" : dtrow["PR_NEW_ASR"].ToString().Trim();
                txtCurrency.Text = string.IsNullOrEmpty(dtrow["CURRENCY_TYPE"].ToString().Trim()) ? "" : dtrow["CURRENCY_TYPE"].ToString().Trim();
//                txtID.Text = string.IsNullOrEmpty(dtrow["ID"].ToString().Trim()) ? "" : dtrow["ID"].ToString().Trim();
//                W_TypeOf.Text = string.IsNullOrEmpty(dtrow["W_TypeOf"].ToString().Trim()) ? "" : dtrow["W_TypeOf"].ToString().Trim();
//                dtJoiningDate.Text = string.IsNullOrEmpty(dtrow["pr_joining_date"].ToString().Trim()) ? "" : dtrow["pr_joining_date"].ToString().Trim();
//.Text = string.IsNullOrEmpty(dtrow["PR_TERMIN_DATE"].ToString().Trim()) ? "" : dtrow["PR_TERMIN_DATE"].ToString().Trim();
//                dtTransferDate.Text = string.IsNullOrEmpty(dtrow["PR_TRANSFER_DATE"].ToString().Trim()) ? "" : dtrow["PR_TRANSFER_DATE"].ToString().Trim();


            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAproved_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersNo.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersNo.Text.Trim());
                SQLManager.ApprovedFastLocal(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }

        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersNo.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersNo.Text.Trim());
                SQLManager.ApprovedFastLocalReject(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }
        }
    }
}
