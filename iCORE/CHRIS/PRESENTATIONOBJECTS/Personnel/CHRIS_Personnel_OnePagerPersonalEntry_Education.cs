using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_OnePagerPersonalEntry_Education : ChrisTabularForm
    {

        #region Declarations

        private CHRIS_Personnel_OnePagerPersonalEntry _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Training frm_Training;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Education frm_Education;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory frm_EmpHistory;

        string pr_SegmentValue = string.Empty;
        private DataTable dtPreEmp;
        private DataTable dtEdu;
        private DataTable dtTraining;

        #endregion

        #region Constructor

        public CHRIS_Personnel_OnePagerPersonalEntry_Education()
        {
            InitializeComponent();
            this.ShowBottomBar.Equals(false);
        }

        public CHRIS_Personnel_OnePagerPersonalEntry_Education(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_OnePagerPersonalEntry mainForm, DataTable _dtPreEmp, DataTable _dtEdu, DataTable _dtTraining)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtPreEmp = _dtPreEmp;
            dtEdu = _dtEdu;
            dtTraining = _dtTraining;
            this._mainForm = mainForm;


            dgvEducation.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

        }
        
        #endregion

        #region Events
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            //Ctl+PageDown  "Open Training form"
                            frm_Training = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Training(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm,dtPreEmp, dtEdu, dtTraining);
                            this.Hide();
                            frm_Training.MdiParent = null;
                            frm_Training.Enabled = true;
                            frm_Training.Select();
                            frm_Training.Focus();
                            frm_Training.ShowDialog(this);

                            this.KeyPreview = true;
                            break;

                        case 33:
                            //Ctl+PageUp  "Hide this form and open History form"
                            this.Hide();
                            frm_EmpHistory = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, dtPreEmp, dtEdu, dtTraining);
                            //frm_EmpHistory = new CHRIS_Personnel_OnePagerPersonalEntry_EmpHistory();
                            frm_EmpHistory.ShowDialog(this);
                            this.KeyPreview = true;
                            break;

                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }
        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtCancel.Visible = false;
            this.tlbMain.Visible = false;
            this.tbtAdd.Visible = false;

            dgvEducation.GridSource = dtEdu;
            dgvEducation.DataSource = dgvEducation.GridSource;


            this.KeyPreview = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);

        }
        #endregion

        private void dgvEducation_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }

        }

    }
}