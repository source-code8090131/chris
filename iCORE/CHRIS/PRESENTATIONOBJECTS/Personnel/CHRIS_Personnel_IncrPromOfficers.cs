using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using CrplControlLibrary;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_IncrPromOfficers : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();

        #region Constructor
        public CHRIS_Personnel_IncrPromOfficers()
        {
            InitializeComponent();
        }
        public CHRIS_Personnel_IncrPromOfficers(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            txtOption.Focus();
            InitializeComponent();
            //List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(PnlIncPromotion);
            //this.PnlPersonnel.DependentPanels = lstDependentPanels;
            //this.IndependentPanels.Add(PnlPersonnel);
            //// this.DefaultDataBlock = pnlTblBenchmark;
            //this.CurrentPanelBlock = "PnlPersonnel";
        }
        #endregion

        #region Feilds
        int Dummy_FLd;
        DateTime J_date;
        double w_inc;
        double W_min;
        double W_max;
        double NUMB1;
        double NUMB;
        double NUMB2;
        char flg = 'N';
        char flg2 = 'N';
        int Remarkcount = 0;
        int _mViewCount = 0;
        int DUD = 0;
        #endregion

        #region Overidden Methods

        protected override void OnLoad(EventArgs e)
        {
            this.txtOption.Select();
            this.txtOption.Focus();
            base.OnLoad(e);
            this.tbtAdd.Visible = false;
            this.tbtDelete.Visible = false;
            this.tbtEdit.Visible = false;
            this.tbtCancel.Visible = false;
            this.tbtList.Visible = false;
            this.txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            this.txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            this.txtLocation.Text = this.CurrentLocation;
            this.txtUser.Text = this.userID;
            lblUserName.Text += "   " + this.UserName;
            dtLastApp.Value = null;
            dtNextApp.Value = null;
            DtPr_Effective.Value = null;
            dtPR_RANKING_DATE.Value = null;
            this.txtOption.Select();
            this.txtOption.Focus();

            this.FunctionConfig.F6 = Function.Quit;
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;

            this.CurrrentOptionTextBox = this.txtCurrOption;

        }

        protected override bool Save()
        {
            return base.Save();

        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {


            if (actionType == "Cancel")
            {
                //this.txtOption.Select();
                //this.txtOption.Focus(); 

                base.DoToolbarActions(ctrlsCollection, actionType);

                base.ClearForm(PnlIncPromotion.Controls);
                base.ClearForm(PnlPersonnel.Controls);
                this.ClearForm(this.PnlPersonnel.Controls);
                dtLastApp.Value = null;
                dtNextApp.Value = null;
                dtPR_RANKING_DATE.Value = null;
                DtPr_Effective.Value = null;

                this.txtPersonnel.CustomEnabled = true;
                this.lbPersonnel.CustomEnabled = true;
                this.txtPersonnel.Enabled = true;
                this.lbPersonnel.Enabled = true;
                RefreshValues();
                this.txtOption.Select();
                this.txtOption.Focus();

                return;
            }
            if (this.FunctionConfig.CurrentOption != Function.None)
            {
                if (actionType == "Save")
                {

                    if (MessageBox.Show("Do You Want to Save this records [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (txtIncremnttype.Text.ToUpper() == "P")
                        {
                            Proc_7();
                        }
                        Pre_Commit();
                        base.DoToolbarActions(this.Controls, "Save");
                        CallReport("AUP0", "NEW");
                        base.Cancel();
                        base.IterateFormToEnableControls(this.Controls, false);
                        //base.ClearForm(PnlPersonnel.Controls);
                        //base.ClearForm(PnlIncPromotion.Controls);
                        //dtLastApp.Value = null;
                        //dtNextApp.Value = null;
                        //DtPr_Effective.Value = null;
                        //dtPR_RANKING_DATE.Value = null;
                        //this.DoFunction(Function.Quit);
                        RefreshValues();
                        txtOption.Select();
                        txtOption.Focus();
                        return;
                    }
                }
            }
            else
                return;
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override void BindLOVLookupButton(LookupButton poCprlLookupButton)
        {
            if (this.FunctionConfig.CurrentOption != Function.None)
            {
                base.BindLOVLookupButton(poCprlLookupButton);
                if (poCprlLookupButton.Name == lbtnDate.Name)
                {
                    if (DtPr_Effective.Value == null)
                    {
                        if (this.FunctionConfig.CurrentOption != Function.None && txtPNo.Text != string.Empty && txtIncremnttype.Text != string.Empty && this.FunctionConfig.CurrentOption != Function.Add)
                        {
                            Result rslt;
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("SearchFilter", this.txtPersonnel.Text + "|" + this.txtIncremnttype.Text);
                            rslt = cmnDM.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "DateLOV", param);
                            if (rslt.isSuccessful)
                            {
                                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    this.BindLOVLookupButton(poCprlLookupButton);
                                }
                            }

                        }
                    }
                }

            }
        }

        protected override bool Edit()
        {
            this.ResetForm();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            base.Edit();
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Modify";
            this.txtOption.Text = "M";
            this.FunctionConfig.CurrentOption = Function.Modify;
            return false;
        }

        protected override bool Add()
        {
            this.ResetForm();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            base.Add();
            this.FunctionConfig.CurrentOption = Function.Add;
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Add";
            this.txtOption.Text = "A";
            return false;
        }

        protected override bool Delete()
        {
            this.ResetForm();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            return base.Delete();
        }

        protected override bool View()
        {
            this.ResetForm();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;

            return base.View();
        }

        protected override bool Quit()
        {
            //this.FunctionConfig.CurrentOption = Function.None;
            //this.txtOption.Select();
            //this.txtOption.Focus();
            bool flag = false;
            base.Quit();
            base.IterateFormToEnableControls(this.Controls, false);
            _mViewCount = 0;
            RefreshValues();
            this.txtOption.Select();
            this.txtOption.Focus();
            return flag;
        }

        #endregion

        #region Methods

        private void ResetSkipValidation()
        {

        }

        private void ResetForm()
        {

        }

        private void RefreshValues()
        {
            Dummy_FLd = 0;
            J_date = new DateTime();
            w_inc = 0;
            W_min = 0;
            W_max = 0;
            NUMB1 = 0;
            NUMB = 0;
            NUMB2 = 0;
            flg = 'N';
            flg2 = 'N';
            Remarkcount = 0;
            _mViewCount = 0;
            DUD = 0;// throw new Exception("The method or operation is not implemented.");
        }

        void Branch()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "Branch", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtnew_branch.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }
        }

        void count_Pr_p_no()
        {
            if (FunctionConfig.CurrentOption == Function.None)
                return;
            Result rslt;
            int count = 0;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "count", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out count);
                        //count = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    }
                }
                if (count == 0)
                {
                    //base.ClearForm(PnlPersonnel.Controls);
                    MessageBox.Show("NO DATA EXISTS FOR THE ENTERED PERSONNEL NO.");
                    base.DoFunction(Function.Quit);
                    dtLastApp.Value = null;
                    dtNextApp.Value = null;
                    DtPr_Effective.Value = null;
                    dtPR_RANKING_DATE.Value = null;
                    txtOption.Select();
                    txtOption.Focus();
                }
            }
        }

        void Proc_6()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            param.Add("PR_INC_TYPE", this.txtIncremnttype.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PROC_6", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out Dummy_FLd);
                    //if (Dummy_FLd == 0)
                    //{
                    //    base.DoFunction(Function.Quit);
                    //    dtLastApp.Value = null;
                    //    dtNextApp.Value = null;
                    //    DtPr_Effective.Value = null;
                    //    dtPR_RANKING_DATE.Value = null;
                    //    return;
                    //}

                    // Dummy_FLd = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                }
            }

        }

        void proc_1()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            param.Add("PR_INC_TYPE", this.txtIncremnttype.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PROC_1", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out Dummy_FLd);
                    //Dummy_FLd = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                }
            }

        }

        void PROC_8()
        {

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "PROC_8", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtAnnualPresnt.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_DESIG_PRESENT.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtLevel.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtWP_pre.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();

                }


            }


        }

        void PR_Joining_dt()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "PR_JOINING_DATE", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        DateTime.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out J_date);

                    //horrible logic 
                    //string chknull = string.Empty;
                    //if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                    //    chknull = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    //if (chknull != string.Empty)
                    //{
                    //    J_date = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    //}
                }
            }

        }

        void Pr_IncAmt_kyNxt1()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_IncAmt_kyNxt_1", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtPR_ANNUAL_PREVIOUS1.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_DESIG_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtPR_LEVEL_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtPR_FUNCT_1_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                        txtPR_FUNCT_2_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[5] != null)
                        txtPR_DESIG_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[6] != null)
                        txtPR_LEVEL_PRESENT1.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[7] != null)
                        txtPR_FUNCT_1_PRESENT.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[8] != null)
                        txtPR_FUNCT_2_PRESENT.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    // flg = 'Y';

                }
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                    Pr_IncAmt_knNxt2();
                    Pr_IncAmt_knNxt3();
                }
            }
        }

        void Pr_IncAmt_knNxt2()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_2", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtPR_ANNUAL_PREVIOUS1.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_DESIG_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtPR_LEVEL_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtPR_FUNCT_1_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                        txtPR_FUNCT_2_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                }
            }

        }

        void Pr_IncAmt_knNxt3()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_3", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtPR_DESIG_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_LEVEL_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtPR_FUNCT_1_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtPR_FUNCT_2_PREVIOUS.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                }
            }

        }

        void Pr_IncAmt_knNxt4()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            param.Add("PR_DESIG_PRESENT", this.txtPR_DESIG_PRESENT.Text);
            param.Add("PR_LEVEL_PRESENT", this.txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_4", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    W_min = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    W_max = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                }
            }

        }

        void Pr_IncAmt_knNxt5()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_5", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        Double.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out NUMB);
                    //NUMB = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                }
            }

        }

        void Pr_AnuualPresent_keyNxt()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_AnuualPresent_keyNxt", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out DUD);
                    //NUMB = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                }
            }

        }

        void nextincrement()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Next_Increment", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    flg2 = 'Y';
                }
            }
        }

        void Pr_effective()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_effective", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    dtLastApp.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    //dtNextApp.Value = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    //flg2 = 'Y';
                }
            }

        }

        void proc_3()
        {
            //DateTime D = new DateTime();
            int DD = 0;
            if (txtIncremnttype.Text.Trim().ToUpper() == "I")
            {
                #region ADD/MODIFY
                if ((FunctionConfig.CurrentOption == Function.Add) || (FunctionConfig.CurrentOption == Function.Modify))
                {
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_count", param);

                        if (rslt.isSuccessful)
                        {
                            if ((rslt.dstResult != null) && (rslt.dstResult.Tables[0].Rows.Count > 0))
                            {
                                if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                    DD = 1;
                            }

                        }
                    }

                    #region  ADD
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param.Add("PR_LAST_APP", dtLastApp.Value);
                        param.Add("PR_NEXT_APP", dtNextApp.Value);
                        param.Add("PR_ANNUAL_PRESENT", txtPR_ANNUAL_PRESENT.Text);
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_add", param);


                    }
                    #endregion

                    #region Modify
                    if (FunctionConfig.CurrentOption == Function.Modify)
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param.Add("PR_LAST_APP", dtLastApp.Value);
                        param.Add("PR_NEXT_APP", dtNextApp.Value);
                        //param.Add("PR_ANNUAL_PRESENT", txtPR_ANNUAL_PRESENT.Text);
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_modify", param);
                    }

                    #endregion

                    #region ADD
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        if (DD == 1)
                        {
                            Result rslt;
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_add_ext", param);
                        }
                        txtPR_CURRENT_PREVIOUS.Text = "C";
                    }
                    #endregion

                }
                #endregion

                #region Delete Case
                if (FunctionConfig.CurrentOption == Function.Delete)
                {
                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    param.Add("PR_EFFECTIVE", Convert.ToDateTime(DtPr_Effective.Value));
                    param.Add("PR_INC_TYPE", txtIncremnttype.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_delete_Promotion", param);

                    if (dtLastApp.Value == null)
                    {
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "update_personnel_increment", param);

                    }
                    else
                    {
                        Result rslt1;
                        Dictionary<string, object> param1 = new Dictionary<string, object>();
                        param1.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param1.Add("PR_EFFECTIVE", Convert.ToDateTime(DtPr_Effective.Value).ToShortDateString());
                        param1.Add("PR_NEXT_APP", Convert.ToDateTime(dtNextApp.Value).ToShortDateString());
                        param1.Add("PR_LAST_APP", Convert.ToDateTime(dtLastApp.Value).ToShortDateString());
                        rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3ifNotDelete", param1);
                    }



                    int w_value = 0;
                    Result rslt2;
                    Dictionary<string, object> param2 = new Dictionary<string, object>();
                    param2.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    rslt2 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "DeleteCOunt_proc3", param2);

                    if (rslt2.isSuccessful)
                    {
                        if (rslt2.dstResult != null && rslt2.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                Int32.TryParse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out w_value);
                        }
                        if (w_value == 0)
                            rslt2 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "update_personnel_Pr0c3", param2);
                    }

                    Result rslt11;
                    Dictionary<string, object> param11 = new Dictionary<string, object>();
                    param11.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    param11.Add("PR_LAST_APP", Convert.ToDateTime(dtLastApp.Value));
                    param11.Add("PR_INC_TYPE", txtIncremnttype.Text);

                    rslt11 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_last", param11);
                }
                #endregion
            }

        }

        void proc_4()
        {
            if (txtIncremnttype.Text == "A")
            {
                if ((FunctionConfig.CurrentOption == Function.Add) || (FunctionConfig.CurrentOption == Function.Modify))
                {
                    # region add
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param.Add("PR_ANNUAL_PRESENT", txtPR_ANNUAL_PRESENT.Text);
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "add_pr0c4", param);

                    }
                    #endregion

                    #region Modify
                    if (FunctionConfig.CurrentOption == Function.Modify)
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param.Add("PR_ANNUAL_PRESENT", txtPR_ANNUAL_PRESENT.Text);
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "proc_4_modify", param);

                    }
                    #endregion

                    #region Add
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param.Add("PR_EFFECTIVE", DtPr_Effective.Value);
                        rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc_4Addcurrnt", param);
                        txtPR_CURRENT_PREVIOUS.Text = "C";
                    }
                    #endregion
                }
                #region Delete
                if (FunctionConfig.CurrentOption == Function.Delete)
                {
                    int w_value = 0;

                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    param.Add("PR_INC_TYPE", txtIncremnttype.Text);
                    param.Add("PR_EFFECTIVE", DtPr_Effective.Value);
                    rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_delete_Promotion", param);
                    //rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr0c4_delete", param);

                    param = new Dictionary<string, object>();
                    param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    param.Add("PR_INC_TYPE", txtIncremnttype.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Delete_Proc4_upd", param);

                    param = new Dictionary<string, object>();
                    param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "count_value", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                w_value = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        }
                        if (w_value == 0)
                        {
                            param = new Dictionary<string, object>();
                            param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "countifZero", param);

                        }
                    }
                }
                #endregion
            }


        }

        void Proc_7()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", txtIncPr_p_no.Text);
            param.Add("PR_DESIG_PRESENT", txtPR_DESIG_PRESENT.Text);
            param.Add("PR_LEVEL_PRESENT", txtPR_LEVEL_PRESENT1.Text);

            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc_7", param);

        }

        void Pre_Commit()
        {
            DateTime da = new DateTime();
            double d3 = 0;
            CallReport("AUPR", "OLD");
            proc_4();
            proc_3();

            if (txtIncremnttype.Text.ToUpper() == "P")
            {
                #region ADD/MODIFY
                DateTime dummy_pr_eff_dt = CurrentDate;
                if (FunctionConfig.CurrentOption == Function.Add || FunctionConfig.CurrentOption == Function.Modify)
                {
                    Result rslt_modify;
                    Dictionary<string, object> param_modify = new Dictionary<string, object>();
                    {
                        //Result rslt_modify;
                        param_modify = new Dictionary<string, object>();
                        param_modify.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        rslt_modify = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_count", param_modify);

                        if (rslt_modify.isSuccessful)
                        {
                            if ((rslt_modify.dstResult != null) && (rslt_modify.dstResult.Tables[0].Rows.Count > 0))
                            {
                                if (rslt_modify.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                {
                                    DateTime.TryParse(rslt_modify.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out da);
                                    d3 = 1;
                                }
                            }
                        }
                    }

                    if (FunctionConfig.CurrentOption == Function.Modify)
                    {
                        param_modify = new Dictionary<string, object>();
                        param_modify.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param_modify.Add("PR_EFFECTIVE", Convert.ToDateTime(DtPr_Effective.Value));
                        rslt_modify = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_Commit_Modify", param_modify);
                    }
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        //Pre_commit_add
                        Result rslt_add;
                        Dictionary<string, object> param_add = new Dictionary<string, object>();
                        param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param_add.Add("PR_EFFECTIVE", Convert.ToDateTime(DtPr_Effective.Value));
                        param_add.Add("PR_ANNUAL_PRESENT", txtAnnualPresnt.Text);
                        rslt_add = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_add", param_add);
                    }
                }

                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    //**************Pre_commit_add*************
                    if (d3 == 1)
                    {
                        Result rslt_upd;
                        Dictionary<string, object> param_add = new Dictionary<string, object>();
                        param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param_add.Add("PR_EFFECTIVE", da.Date);
                        rslt_upd = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_add_Promotion", param_add);
                    }
                    txtPR_CURRENT_PREVIOUS.Text = "C";

                    {
                        Result rslt_Promotion;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_IncAmt_kyNxt_1", param);
                        if (rslt_Promotion.isSuccessful)
                        {
                            if (rslt_Promotion.dstResult != null && rslt_Promotion.dstResult.Tables[0].Rows.Count > 0)
                            {
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                    txtPR_ANNUAL_PREVIOUS1.Text = rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                                    txtPR_DESIG_PREVIOUS.Text = rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                                    txtPR_LEVEL_PREVIOUS.Text = rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                                    txtPR_FUNCT_1_PREVIOUS.Text = rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                                    txtPR_FUNCT_2_PREVIOUS.Text = rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

                            }
                            else
                            {
                                Result rslt_Promotion1;
                                param = new Dictionary<string, object>();
                                param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                                rslt_Promotion1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_2", param);
                                if (rslt_Promotion1.isSuccessful)
                                {
                                    if (rslt_Promotion1.dstResult != null && rslt_Promotion1.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        if (rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                            txtPR_ANNUAL_PREVIOUS1.Text = rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                        if (rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                                            txtPR_DESIG_PREVIOUS.Text = rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                                        if (rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                                            txtPR_LEVEL_PREVIOUS.Text = rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                                        if (rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                                            txtPR_FUNCT_1_PREVIOUS.Text = rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                                        if (rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                                            txtPR_FUNCT_2_PREVIOUS.Text = rslt_Promotion1.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                                    }
                                }

                            }
                        }
                    }
                    #region CommentedCode
                    //Result rslt;
                    //Dictionary<string, object> param = new Dictionary<string, object>();
                    //param.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    //param.Add("PR_EFFECTIVE", dummy_pr_eff_dt.ToShortDateString());
                    //param.Add("pr_annual_present", txtAnnualPresnt.Text);
                    //rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_add_Promotion", param);
                    //if (rslt.isSuccessful)
                    //{
                    //    if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                    //    {
                    //        string dummy = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    //        dummy_pr_eff_dt = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    //        if (dummy != string.Empty && dummy != "" && dummy != "")
                    //        {
                    //            d3 = 1;
                    //        }
                    //    }

                    //}
                    #endregion
                }
                #endregion

                #region Delete
                if (FunctionConfig.CurrentOption == Function.Delete)
                {
                    int w_value = 0;
                    string promoted = string.Empty;

                    Result rslt_Promotion;
                    Dictionary<string, object> param_add = new Dictionary<string, object>();
                    param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    param_add.Add("PR_EFFECTIVE", Convert.ToDateTime(DtPr_Effective.Value));
                    param_add.Add("PR_INC_TYPE", "P");
                    rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_isPromoted", param_add);
                    if (rslt_Promotion.isSuccessful)
                    {
                        if (rslt_Promotion.dstResult != null && rslt_Promotion.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt_Promotion.dstResult.Tables[0].Rows.Count == 1)
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                    promoted = rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        }
                    }
                    if (promoted.Equals("P"))
                    {
                        param_add = new Dictionary<string, object>();
                        param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_Upd_Category", param_add);

                    }
                    param_add = new Dictionary<string, object>();
                    param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                    param_add.Add("PR_EFFECTIVE", Convert.ToDateTime(DtPr_Effective.Value));
                    param_add.Add("PR_INC_TYPE", "P");
                    rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Proc3_delete_Promotion", param_add);

                    {
                        param_add = new Dictionary<string, object>();
                        param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "count_value", param_add);
                        if (rslt_Promotion.isSuccessful)
                        {
                            if (rslt_Promotion.dstResult != null && rslt_Promotion.dstResult.Tables[0].Rows.Count > 0)
                            {
                                if (rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                    Int32.TryParse(rslt_Promotion.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out w_value);
                            }
                        }
                        if (w_value > 0)
                        {
                            param_add = new Dictionary<string, object>();
                            param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                            rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_Upd_Personnel_Promotion", param_add);
                        }
                        else
                        {
                            param_add = new Dictionary<string, object>();
                            param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                            rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_Upd_Personnel_Promotion_ElseCon", param_add);
                        }

                        param_add = new Dictionary<string, object>();
                        param_add.Add("PR_IN_NO", txtIncPr_p_no.Text);
                        param_add.Add("PR_INC_TYPE", "P");
                        rslt_Promotion = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pre_commit_Upd_inc_promotion", param_add);
                    }
                    CallReport("AUP0", "NEW");
                }
                #endregion

            }


        }

        void pr_effective()
        {
            #region Add/Modify/Delete
            if ((FunctionConfig.CurrentOption == Function.View) || (FunctionConfig.CurrentOption == Function.Modify) || (FunctionConfig.CurrentOption == Function.Delete))
            {
                Result rslt1;
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("PR_IN_NO", this.txtPersonnel.Text);
                param1.Add("PR_INC_TYPE", this.txtIncremnttype.Text);
                param1.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
                rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext", param1);
                if (rslt1.isSuccessful)
                {
                    if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            txtPersonnel.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                            txtIncremnttype.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                            DtPr_Effective.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                            this.txtID.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    }

                }
                Result rslt2;
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                param2.Add("PR_IN_NO", this.txtPersonnel.Text);
                param2.Add("PR_INC_TYPE", this.txtIncremnttype.Text);
                param2.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
                rslt2 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Execute_query", param2);
                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateTime dt = CurrentDate;
                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            txtPR_RANK.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                            dtPR_RANKING_DATE.Value = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() == "" ? "01/01/1900" : rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                            txtIncAmonut.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                            txtIncrementPer.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[3].ToString().Substring(0, 4);

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                            txtPR_ANNUAL_PRESENT.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[5] != null && rslt2.dstResult.Tables[0].Rows[0].ItemArray[5].ToString() != string.Empty)
                        {
                            DateTime dtTemp = new DateTime();
                            DateTime.TryParse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[5].ToString(), out dtTemp);
                            if (dtTemp.Year != 1)
                                dtLastApp.Value = dtTemp;
                        }

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[6] != null && rslt2.dstResult.Tables[0].Rows[0].ItemArray[6].ToString() != string.Empty)
                            dtNextApp.Value = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[7] != null) //if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            txtPR_REMARKS.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    }

                    else
                    {
                        base.DoFunction(Function.Quit);
                        dtLastApp.Value = null;
                        dtNextApp.Value = null;
                        DtPr_Effective.Value = null;
                        dtPR_RANKING_DATE.Value = null;
                        return;
                    }

                }
                if (this.txtIncremnttype.Text.Trim().ToUpper() == "P")
                {
                    CHRIS_Personnel_IncrementOfficers_Designation officer_desig = new CHRIS_Personnel_IncrementOfficers_Designation();
                    officer_desig.desig = txtPR_DESIG_PRESENT.Text;
                    officer_desig.level = txtPR_LEVEL_PRESENT1.Text;
                    officer_desig.functit1 = txtPR_FUNCT_1_PRESENT.Text;
                    officer_desig.functit2 = txtPR_FUNCT_2_PRESENT.Text;
                    officer_desig.ShowDialog();
                    txtPR_DESIG_PRESENT.Text = officer_desig.desig;
                    txtPR_LEVEL_PRESENT1.Text = officer_desig.level;
                    txtPR_FUNCT_1_PRESENT.Text = officer_desig.functit1;
                    txtPR_FUNCT_2_PRESENT.Text = officer_desig.functit2;
                    officer_desig.Close();
                    Sal_Designation();
                }
                if (FunctionConfig.CurrentOption == Function.Modify)
                {
                    if (txtIncAmonut.Text != "" && txtIncAmonut.Text != null && txtIncAmonut.Text.ToString() != string.Empty)
                        double.TryParse(txtIncAmonut.Text, out w_inc);
                    txtPR_RANK.Focus();

                }
                if (FunctionConfig.CurrentOption == Function.Delete)
                {
                    if (txtIncremnttype.Text.ToString() == "P")
                    {
                        CHRIS_Personnel_IncrementOfficers_Designation officer_desig = new CHRIS_Personnel_IncrementOfficers_Designation();
                        officer_desig.ShowDialog();
                        txtPR_DESIG_PREVIOUS.Text = officer_desig.desig;
                        txtPR_LEVEL_PRESENT1.Text = officer_desig.level;
                        txtPR_FUNCT_1_PRESENT.Text = officer_desig.functit1;
                        txtPR_FUNCT_2_PRESENT.Text = officer_desig.functit2;
                        officer_desig.Close();
                    }
                    if (MessageBox.Show("Do You Want to Delete this records [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Pre_Commit();
                        base.DoFunction(Function.Quit);
                        dtLastApp.Value = null;
                        dtNextApp.Value = null;
                        DtPr_Effective.Value = null;
                        dtPR_RANKING_DATE.Value = null;
                        if (MessageBox.Show("Continue The Process For More Records [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            this.txtOption.Text = "D";
                            this.DoFunction(Function.Delete);
                            this.txtOption.Text = "D";
                            txtPersonnel.Select();
                            txtPersonnel.Focus();
                        }
                        return;

                    }
                }
                if (FunctionConfig.CurrentOption == Function.View)
                {
                    if (txtIncremnttype.Text.ToString() == "P")
                    {
                        CHRIS_Personnel_IncrementOfficers_Designation officer_desig = new CHRIS_Personnel_IncrementOfficers_Designation();
                        officer_desig.ShowDialog();
                        txtPR_DESIG_PRESENT.Text = officer_desig.desig;
                        txtPR_LEVEL_PRESENT1.Text = officer_desig.level;
                        txtPR_FUNCT_1_PRESENT.Text = officer_desig.functit1;
                        txtPR_FUNCT_2_PRESENT.Text = officer_desig.functit2;
                        officer_desig.Close();
                    }
                    if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        this.DoFunction(Function.Quit);
                        dtLastApp.Value = null;
                        dtNextApp.Value = null;
                        DtPr_Effective.Value = null;
                        dtPR_RANKING_DATE.Value = null;
                        base.DoFunction(Function.View);
                        this.txtOption.Text = "V";
                        txtPersonnel.Select();
                        txtPersonnel.Focus();

                    }
                    else
                    {
                        base.DoFunction(Function.Quit);
                        dtLastApp.Value = null;
                        dtNextApp.Value = null;
                        DtPr_Effective.Value = null;
                        dtPR_RANKING_DATE.Value = null;
                        return;
                    }
                }



            }
            #endregion

            //#region Add
            //if (FunctionConfig.CurrentOption == Function.Add)
            //{
            //    Result rslt1;
            //    Dictionary<string, object> param1 = new Dictionary<string, object>();
            //    param1.Add("PR_IN_NO", this.txtPersonnel.Text);
            //    param1.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
            //    rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add", param1);
            //    if (rslt1.isSuccessful)
            //    {
            //        if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            DateTime dummyDate = new DateTime();
            //            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
            //                if (DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dummyDate))
            //                {

            //                }
            //            //if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
            //            //    txtIncremnttype.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
            //            //if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
            //            //    DtPr_Effective.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
            //            //if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
            //            //    this.txtID.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
            //        }

            //    }

            //}

            //#endregion

            #region Commented Pervious Code
            //if ((FunctionConfig.CurrentOption == Function.View) || (FunctionConfig.CurrentOption == Function.Modify) || (FunctionConfig.CurrentOption == Function.Delete))
            //{
            //    Result rslt1;
            //    Dictionary<string, object> param1 = new Dictionary<string, object>();
            //    param1.Add("PR_IN_NO", this.txtPersonnel.Text);
            //    param1.Add("PR_INC_TYPE", this.txtIncremnttype.Text);
            //    param1.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
            //    rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext", param1);
            //    if (rslt1.isSuccessful)
            //    {
            //        if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
            //                txtPersonnel.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
            //            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
            //                txtIncremnttype.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
            //            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
            //                DtPr_Effective.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
            //            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
            //                this.txtID.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
            //        }

            //    }


            //    Result rslt2;
            //    Dictionary<string, object> param2 = new Dictionary<string, object>();
            //    param2.Add("PR_IN_NO", this.txtPersonnel.Text);
            //    param2.Add("PR_INC_TYPE", this.txtIncremnttype.Text);
            //    param2.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
            //    rslt2 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Execute_query", param2);
            //    {

            //        if (rslt2.isSuccessful)
            //        {
            //            if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
            //            {
            //                DateTime dt = CurrentDate;
            //                txtPR_RANK.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
            //                dtPR_RANKING_DATE.Value = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() == "" ? "01/01/1900" : rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
            //                txtIncAmonut.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
            //                txtIncrementPer.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[3].ToString().Substring(0, 4);
            //                txtPR_ANNUAL_PRESENT.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
            //                dtLastApp.Value = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[5].ToString());
            //                dtNextApp.Value = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
            //                txtPR_REMARKS.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
            //            }

            //            else
            //            {

            //                if (FunctionConfig.CurrentOption != Function.Add)
            //                {
            //                    base.ClearForm(PnlPersonnel.Controls);
            //                    base.ClearForm(PnlIncPromotion.Controls);
            //                    dtLastApp.Value = null;
            //                    dtNextApp.Value = null;
            //                    DtPr_Effective.Value = null;
            //                    dtPR_RANKING_DATE.Value = null;
            //                    txtOption.Select();
            //                    txtOption.Focus();
            //                }

            //            }

            //        }

            //    }
            //}

            //if (txtIncremnttype.Text.ToString() == "P")
            //{

            //    CHRIS_Personnel_IncrementOfficers_Designation officer_desig = new CHRIS_Personnel_IncrementOfficers_Designation();
            //    officer_desig.ShowDialog();
            //    txtPR_DESIG_PREVIOUS.Text = officer_desig.desig;
            //    txtPR_LEVEL_PRESENT1.Text = officer_desig.level;
            //    txtPR_FUNCT_1_PRESENT.Text = officer_desig.functit1;
            //    txtPR_FUNCT_2_PRESENT.Text = officer_desig.functit2;
            //    officer_desig.Close();

            //}

            //if (FunctionConfig.CurrentOption == Function.Modify)
            //{
            //    if (txtIncAmonut.Text != "" && txtIncAmonut.Text != null && txtIncAmonut.Text.ToString() != string.Empty)
            //        w_inc = double.Parse(txtIncAmonut.Text);

            //}




            //if (FunctionConfig.CurrentOption == Function.Delete)
            //{
            //    if (txtIncremnttype.Text.ToString() == "P")
            //    {
            //        CHRIS_Personnel_IncrementOfficers_Designation officer_desig = new CHRIS_Personnel_IncrementOfficers_Designation();
            //        officer_desig.ShowDialog();
            //        txtPR_DESIG_PREVIOUS.Text = officer_desig.desig;
            //        txtPR_LEVEL_PRESENT1.Text = officer_desig.level;
            //        txtPR_FUNCT_1_PRESENT.Text = officer_desig.functit1;
            //        txtPR_FUNCT_2_PRESENT.Text = officer_desig.functit2;
            //        officer_desig.Close();
            //    }
            //    if (MessageBox.Show("Do You Want to Delete this records [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //    {
            //        Pre_Comit();
            //        base.ClearForm(PnlPersonnel.Controls);
            //        base.ClearForm(PnlIncPromotion.Controls);
            //        dtLastApp.Value = null;
            //        dtNextApp.Value = null;
            //        DtPr_Effective.Value = null;
            //        dtPR_RANKING_DATE.Value = null;

            //    }


            //}
            #endregion



        }

        /// <summary>
        /// Gets Min Salary against specified Designation.this Value is then assinged to New Annual Package,when Increament Type is Promotion.
        /// </summary>
        private void Sal_Designation()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", this.txtPersonnel.Text);
            param.Add("PR_LEVEL_PRESENT", txtPR_LEVEL_PRESENT1.Text);
            param.Add("PR_DESIG_PRESENT", txtPR_DESIG_PRESENT1.Text);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "desig_Salary", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                    {
                        txtAnnualPresnt.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        // txtPR_ANNUAL_PRESENT.Text=
                    }


                }
            }
        }

        private void CallReport(string AuditPreOrAuditPo, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = this.GlobalReportPath;//@"C:\SPOOL\CHRIS\AUDIT\";
            string auditStatus = AuditPreOrAuditPo + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + auditStatus;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = DateTime.Now.ToString();//this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);


            CrplControlLibrary.SLTextBox txtPNo = new CrplControlLibrary.SLTextBox(comp);
            txtPNo.Name = "pno";
            txtPNo.Text = txtPersonnel.Text;
            pnl.Controls.Add(txtPNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT13A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }

        #endregion

        # region Events

        private void txtIncremnttype_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            if (txtIncremnttype.Text.Trim().ToUpper() == "I")
            {

                txtIncAmonut.IsRequired = true;
                txtIncAmonut.ReadOnly = false;
            }
            if (txtIncremnttype.Text.Trim().ToUpper() == "P")
            {
                txtIncAmonut.IsRequired = false;
                txtIncAmonut.ReadOnly = true;
            }
            if (txtIncremnttype.Text.Trim().ToUpper() == "A")
            {
            }

            if ((txtIncremnttype.Text.Trim().ToUpper() != "I") && (txtIncremnttype.Text.Trim().ToUpper() != "A") && (txtIncremnttype.Text.Trim().ToUpper() != "P"))
            {
                MessageBox.Show("YOU HAVE TO ENTER I , A or P");
                e.Cancel = true;
                return;
            }

            if ((FunctionConfig.CurrentOption == Function.Delete) || (FunctionConfig.CurrentOption == Function.Modify))
            {
                Proc_6();
                if (Dummy_FLd == 0)
                {
                    MessageBox.Show("NO DATA EXISTS FOR THIS OPTION.", "Error", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                    base.DoFunction(Function.Quit);
                    dtLastApp.Value = null;
                    dtNextApp.Value = null;
                    DtPr_Effective.Value = null;
                    dtPR_RANKING_DATE.Value = null;
                    return;
                }
                //if (Dummy_FLd > 1)
                //{
                //    MessageBox.Show("PRESS [F9] KEY TO VIEW THE LATEST DATE", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                //}
            }

            else if (FunctionConfig.CurrentOption == Function.View)
            {
                proc_1();
                if (Dummy_FLd == 0)
                {
                    MessageBox.Show("NO DATA EXISTS FOR THIS OPTION.", "Error", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                    base.DoFunction(Function.Quit);
                    dtLastApp.Value = null;
                    dtNextApp.Value = null;
                    DtPr_Effective.Value = null;
                    dtPR_RANKING_DATE.Value = null;
                    return;
                }
                //if (Dummy_FLd > 1)
                //{
                //    MessageBox.Show("PRESS [F9] KEY TO VIEW THE LATEST DATE", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                //}

            }



        }

        private void DtPr_Effective_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            PR_Joining_dt();
            if (DtPr_Effective.Value != null)
            {
                DateTime Effectv_dt = Convert.ToDateTime(DtPr_Effective.Value);

                TimeSpan ts = Effectv_dt.Subtract(J_date);
                int days = ts.Days;

                if (days < 0)
                {
                    MessageBox.Show(" DATE HAS TO BE GREATER THAN JOINING DATE  " + J_date.ToString("dd-MMM-yy"));
                    e.Cancel = true;

                }

                else
                {
                    pr_effective();

                    #region Add Mode Case
                    if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        Result rslt1;
                        Dictionary<string, object> param1 = new Dictionary<string, object>();
                        param1.Add("PR_IN_NO", this.txtPersonnel.Text);
                        param1.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
                        rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add", param1);
                        if (rslt1.isSuccessful)
                        {
                            #region Count == 1
                            if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count == 1)
                            {
                                DateTime dummyDate = new DateTime();
                                if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                    if (DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dummyDate))
                                    {
                                        MessageBox.Show(" DATE HAS TO BE GREATER THAN  " + dummyDate.ToString("dd/MM/yyyy"));
                                    }
                                //if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                                //    txtIncremnttype.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                                //if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                                //    DtPr_Effective.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                                //if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                                //    this.txtID.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                            }
                            #endregion

                            #region Count == 0
                            else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count == 0)
                            {
                                if (txtIncremnttype.Text.Trim() == "I")
                                {
                                    param1 = new Dictionary<string, object>();
                                    param1.Add("PR_IN_NO", this.txtPersonnel.Text);
                                    param1.Add("PR_EFFECTIVE", this.DtPr_Effective.Value);
                                    rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add2", param1);
                                    if (rslt1.isSuccessful)
                                    {
                                        if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count == 1)
                                        {
                                            DateTime dummyDate1 = new DateTime();
                                            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dummyDate1);

                                            if (dummyDate1.Year != 1)
                                                MessageBox.Show(" INCREMENT IS GIVEN AFTER ONE YEAR. LAST INCREMENT WAS GIVEN ON   " + dummyDate1.ToString("dd/MM/yyyy"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            else
                                                MessageBox.Show(" INCREMENT IS GIVEN AFTER ONE YEAR. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 1)
                                        {
                                            MessageBox.Show(" INVALID ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            this.DtPr_Effective.Value = null;

                                        }
                                        //else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count== 0)
                                        //NEXT_FIELD;
                                    }
                                }
                                //else next feild
                            }
                            #endregion

                            #region Count > 1
                            else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 1)
                            {
                                param1 = new Dictionary<string, object>();
                                param1.Add("PR_IN_NO", this.txtPersonnel.Text);
                                rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add3", param1);
                                if (rslt1.isSuccessful)
                                {
                                    if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        DateTime JJ = new DateTime();
                                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                            DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out JJ);
                                        MessageBox.Show(" DATE HAS TO BE GREATER THAN  " + JJ.ToString("dd/MM/YYY"));
                                    }
                                }

                            }

                            #endregion

                        }

                    }

                    #endregion
                }
            }
            else
            {

                if (_mViewCount == 0)
                {
                    if (this.FunctionConfig.CurrentOption != Function.Add)
                    {
                        this.BindLOVLookupButton(this.lbtnDate);
                        _mViewCount++;
                        this.DtPr_Effective_Validating(null, null);
                    }

                }

            }


        }

        private void dtPR_RANKING_DATE_Validating(object sender, CancelEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;

            if (dtPR_RANKING_DATE.Value != null)
            {
                DateTime ranking_dt = Convert.ToDateTime(dtPR_RANKING_DATE.Value);
                DateTime pr_effectiveDt = Convert.ToDateTime(DtPr_Effective.Value);

                TimeSpan ts = pr_effectiveDt.Subtract(ranking_dt);
                int days = ts.Days;
                if (days < 0)
                {
                    MessageBox.Show("VALUE HAS TO BE SAMALLER THAN EFFECTIVE DATE");
                    e.Cancel = true;

                }
                else
                {
                    PR_Joining_dt();
                    TimeSpan ts1 = ranking_dt.Subtract(J_date);
                    int days1 = ts1.Days;

                    if (days1 < 0)
                    {
                        MessageBox.Show("DATE HAS TO BE GREATER THAN JOINING DATE " + J_date.ToString("dd-MMM-yy"));
                        e.Cancel = true;
                        return;
                    }
                    if (txtIncremnttype.Text.Trim().ToUpper() != "P")
                        txtIncAmonut.Focus();


                }

            }
            if (txtIncremnttype.Text.Trim().ToUpper() == "P")
            {
                CHRIS_Personnel_IncrementOfficers_Designation officer_desig = new CHRIS_Personnel_IncrementOfficers_Designation();
                officer_desig.NewBranch = txtnew_branch.Text;
                officer_desig.ShowDialog();
                txtPR_DESIG_PRESENT1.Text = officer_desig.desig;//txtPR_DESIG_PRESENT.Text = officer_desig.desig;
                txtPR_LEVEL_PRESENT1.Text = officer_desig.level;
                txtPR_FUNCT_1_PRESENT.Text = officer_desig.functit1;
                txtPR_FUNCT_2_PRESENT.Text = officer_desig.functit2;
                officer_desig.Close();

            }
        }

        private void txtIncAmonut_Validating(object sender, CancelEventArgs e)
        {
            NUMB = 0;
            NUMB1 = 0;
            NUMB2 = 0;
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;

            #region Key Next of increment Amount

            double PR_INC_AMT = 0;
            if (FunctionConfig.CurrentOption == Function.Add)
            {
                w_inc = 0;
            }
            if (txtIncremnttype.Text.Trim().ToUpper() != "P")
            {
                Pr_IncAmt_kyNxt1();
                /**********************Pr_IncAmt_knNxt2();and  Pr_IncAmt_knNxt3(); were called in No data found condition
                of Pr_IncAmt_kyNxt1();Therefore its now integrated in tht condition.***********************************/

                /*if (flg == 'Y')
                //{
                //    Pr_IncAmt_knNxt2();
                //    Pr_IncAmt_knNxt3();
                }*/

                Pr_IncAmt_knNxt4();
                Pr_IncAmt_knNxt5();

                if ((txtIncAmonut.Text.ToString() != string.Empty) || (txtIncAmonut.Text.ToString() != "")/*&& (txtIncAmonut.Text.ToString() != null) */)
                    double.TryParse(txtIncAmonut.Text.ToString(), out PR_INC_AMT);
                //PR_INC_AMT = double.Parse(txtIncAmonut.Text.ToString()); 
                NUMB1 = NUMB + PR_INC_AMT - w_inc;
                NUMB2 = W_max - NUMB + w_inc;

                if (FunctionConfig.CurrentOption == Function.Modify)
                    txtPR_ANNUAL_PREVIOUS1.Text = Convert.ToString(NUMB - w_inc);

                if ((NUMB1 >= W_min) && (NUMB1 <= W_max))
                {
                    txtAnnualPresnt.Text = NUMB1.ToString();
                    if (NUMB == 0)
                    {
                        NUMB = 1;
                        txtIncrementPer.Text = Convert.ToString(Math.Round(((PR_INC_AMT / NUMB) * 100), 2));//.Substring(0, 4);//Convert.ToString(((PR_INC_AMT / NUMB) * 100)).Substring(0, 4);
                    }
                    else
                        txtIncrementPer.Text = Convert.ToString(Math.Round(((PR_INC_AMT / NUMB) * 100), 2));//.Substring(0, 4);//Convert.ToString(((PR_INC_AMT / NUMB) * 100)).Substring(0, 4);
                    txtPR_REMARKS.Focus();
                }
                else
                {
                    MessageBox.Show("INCREMENT SHOULD NOT BE >  " + NUMB2.ToString() + "  BECAUSE THE LIMIT IS  " + W_max.ToString()
                        //  + "   S EXISTING ANN.PACKAGE IS  "+ NUMB.ToString()
                                    );

                    txtPR_ANNUAL_PRESENT.Text = NUMB1.ToString();
                    if (NUMB != 0 && PR_INC_AMT != 0.0)
                        txtIncrementPer.Text = Convert.ToString(Math.Round(((PR_INC_AMT / NUMB) * 100), 2));
                    txtPR_REMARKS.Focus();
                }
            }
            #endregion

            #region Pre_text_item Of Pr_Next _app

            #region Next Appraisal
            if ((txtIncremnttype.Text.ToString() == "I") && (FunctionConfig.CurrentOption == Function.Add))
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_IN_NO", this.txtPersonnel.Text);
                rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Next_appraisal", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateTime dtTemp;
                        if (DateTime.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dtTemp))
                        { dtNextApp.Value = dtTemp; }
                        //dtNextApp.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    }
                }

            }
            #endregion

            #region Next Increment
            Result rslt1;
            Dictionary<string, object> param1 = new Dictionary<string, object>();
            param1.Add("PR_IN_NO", this.txtPersonnel.Text);
            rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Next_Increment", param1);
            if (rslt1.isSuccessful)
            {
                if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] == null)
                        dtLastApp.Value = null;
                    else
                        Pr_effective();

                }
                else
                    Pr_effective();
            }
            else
                Pr_effective();
            #endregion
            //nextincrement();
            //if (flg2 == 'Y')
            //{
            //    Pr_effective();

            //}

            #endregion
        }

        private void dtNextApp_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;

            DateTime nxtapp = new DateTime();
            DateTime effctvedt = new DateTime();
            if (dtNextApp.Value != null)
            {
                DateTime.TryParse(dtNextApp.Value.ToString(), out nxtapp);
            }
            if (DtPr_Effective.Value != null)
                DateTime.TryParse(DtPr_Effective.Value.ToString(), out effctvedt);
            //DateTime nxtapp = Convert.ToDateTime(dtNextApp.Value);
            //DateTime effctvedt = Convert.ToDateTime(DtPr_Effective.Value);

            //TimeSpan ts = nxtapp.Subtract(effctvedt);
            //int days = ts.Days;
            //if (days < 0)
            if (dtNextApp.Value == null || (nxtapp.Date <= effctvedt.Date))
            {
                MessageBox.Show("DATE HAS TO BE ENTERED & IT SHOULD BE GREATER THAN EFFECTIVE DATE ");
                e.Cancel = true;
            }

        }

        private void txtPR_REMARKS_Validating(object sender, CancelEventArgs e)
        {
            //    if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
            //        return;
            //    if (this.FunctionConfig.CurrentOption == Function.None)
            //        return;
            if (txtPR_REMARKS.Text.Trim() == string.Empty)
            {
                MessageBox.Show("VALUE HAS TO BE ENTERED ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
            {
                txtPersonnel.Focus();
            }


        }

        private void txtPR_RANK_Validating(object sender, CancelEventArgs e)
        {
            if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                return;
            if (this.FunctionConfig.CurrentOption == Function.None)
                return;

            if (txtIncremnttype.Text != "P")
            {
                txtIncAmonut.Focus();
            }
            else
            {
                txtPR_ANNUAL_PRESENT.Focus();
            }
        }

        private void txtPersonnel_TextChanged(object sender, EventArgs e)
        {
            this.txtIncPr_p_no.Text = this.txtPersonnel.Text;
            this.txtPNo.Text = this.txtPersonnel.Text;
        }

        private void txtPR_DESIG_PRESENT_TextChanged(object sender, EventArgs e)
        {
            txtPR_DESIG_PRESENT1.Text = txtPR_DESIG_PRESENT.Text;
        }

        private void CHRIS_Personnel_IncrementOfficers_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "PR_P_NO" || actionType == "PR_P_NO_exist")
            {
                PROC_8();
                Branch();
                if (FunctionConfig.CurrentOption != Function.Add)
                {
                    count_Pr_p_no();
                }

            }
            //if (actionType == "DateLOV" && selectedRow!=null)
            //{
            //    if (DtPr_Effective.Value != null)
            //    {
            //        this.DtPr_Effective_Validating(null, null);
            //    }
            //}
        }

        private void txtPR_ANNUAL_PRESENT_Validating(object sender, CancelEventArgs e)
        {
            Pr_AnuualPresent_keyNxt();
            if (DUD == 0)
            {
                MessageBox.Show("THE VALUE SHOULD BE IN THE RANGE " + W_min.ToString() + " AND " + W_max.ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (txtIncremnttype.Text.Trim().ToUpper() == "P")
            {
                txtPR_REMARKS.Focus();
            }
        }

        private void txtPR_REMARKS_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = false;
                }
            }
        }

        /* private void txtPersonnel_Validating(object sender, CancelEventArgs e)
        {
            txtIncPr_p_no.Text = txtPersonnel.Text;
            //PROC_8();
            //Branch();

            //if (FunctionConfig.CurrentOption != Function.Add)
            //{
            //    count_Pr_p_no();

            //}

        }*/

        #endregion


    }
}