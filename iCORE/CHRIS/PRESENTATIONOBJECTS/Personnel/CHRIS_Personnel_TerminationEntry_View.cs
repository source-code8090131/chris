﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_TerminationEntry_View : Form
    {
        public CHRIS_Personnel_TerminationEntry_View()
        {
            InitializeComponent();
        }

        private void CHRIS_Personnel_TerminationEntry_View_Load(object sender, EventArgs e)
        {
            string userID = CHRIS_Personnel_TerminationEntry.SetValueUserLogin;
            DataTable dt = new DataTable();
            dt = SQLManager.CHRIS_SP_TerminationCheck(userID).Tables[0];
            if (dt.Rows.Count > 0)
            {
                dtGVTermination.DataSource = dt;
                dtGVTermination.Update();
                dtGVTermination.Visible = true;
                // btnReject.Visible = true;
                // btnApproved.Visible = true;
            }
            else
            {
                dtGVTermination.Visible = false;
                // btnReject.Visible = false;
                // btnApproved.Visible = false;
            }
        }

        public static string SetValuePR_NO = "";
        private void btnShow_Click(object sender, EventArgs e)
        {
            string get_PR_No = string.Empty;
            int RowCount = dtGVTermination.SelectedRows.Count;

            if (RowCount >= 1)
            {
                foreach (DataGridViewRow row in dtGVTermination.SelectedRows)
                {
                    get_PR_No = row.Cells[0].Value.ToString();
                }

                SetValuePR_NO = get_PR_No;
                CHRIS_Personnel_TerminationEntry_Detail frm = new CHRIS_Personnel_TerminationEntry_Detail();
                frm.ShowDialog();

                string userID = CHRIS_Personnel_TerminationEntry.SetValueUserLogin;
                DataTable dt = new DataTable();
                dt = SQLManager.CHRIS_SP_TerminationCheck(userID).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dtGVTermination.DataSource = dt;
                    dtGVTermination.Update();
                    dtGVTermination.Visible = true;
                    // btnReject.Visible = true;
                    // btnApproved.Visible = true;
                }
                else
                {
                    dtGVTermination.Visible = false;
                    // btnReject.Visible = false;
                    // btnApproved.Visible = false;
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("You Must Select The Record");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
