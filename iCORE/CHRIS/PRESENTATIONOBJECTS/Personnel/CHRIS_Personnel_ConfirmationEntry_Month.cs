using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ConfirmationEntry_Month : Form
    {
        public CHRIS_Personnel_ConfirmationEntry_Month()
        {
            InitializeComponent();
        }

        private void txtinputMonth_Validating(object sender, CancelEventArgs e)
        {
            int month = Int32.Parse(txtinputMonth.Text);

            if (month > 12)
            {
                MessageBox.Show("Invalid Month Selection !!!!");
                e.Cancel = true;
            
            }
        }
    }
}