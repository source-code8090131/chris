namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class Chris_Personnel_ActualLeaveEntry_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chris_Personnel_ActualLeaveEntry_));
            this.PnlLeaveActual1 = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVLeaveStatus = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.LEV_LV_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LEV_START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LEV_END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W_DAYS_DIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LEV_REMARKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlLeaveStatus = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtCarryforward = new CrplControlLibrary.SLTextBox(this.components);
            this.blk4_lov = new CrplControlLibrary.LookupButton(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClEntitlement = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPlEntitlement = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMlEntitlement = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPlAdvance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtslhalfbal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlEntitlement = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLev_no = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPlAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtClAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMlAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPlBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtClBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMlBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlLeaveActual1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVLeaveStatus)).BeginInit();
            this.PnlLeaveStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(671, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(707, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 517);
            this.panel1.Size = new System.Drawing.Size(707, 60);
            // 
            // PnlLeaveActual1
            // 
            this.PnlLeaveActual1.ConcurrentPanels = null;
            this.PnlLeaveActual1.Controls.Add(this.DGVLeaveStatus);
            this.PnlLeaveActual1.DataManager = null;
            this.PnlLeaveActual1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlLeaveActual1.DependentPanels = null;
            this.PnlLeaveActual1.DisableDependentLoad = false;
            this.PnlLeaveActual1.EnableDelete = true;
            this.PnlLeaveActual1.EnableInsert = true;
            this.PnlLeaveActual1.EnableQuery = false;
            this.PnlLeaveActual1.EnableUpdate = true;
            this.PnlLeaveActual1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LeaveActualCommandGrid";
            this.PnlLeaveActual1.Location = new System.Drawing.Point(12, 255);
            this.PnlLeaveActual1.MasterPanel = null;
            this.PnlLeaveActual1.Name = "PnlLeaveActual1";
            this.PnlLeaveActual1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlLeaveActual1.Size = new System.Drawing.Size(683, 259);
            this.PnlLeaveActual1.SPName = "CHRIS_SP_LEAVE_ACTUAL_GRID_MANAGER";
            this.PnlLeaveActual1.TabIndex = 68;
            // 
            // DGVLeaveStatus
            // 
            this.DGVLeaveStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVLeaveStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LEV_LV_TYPE,
            this.LEV_START_DATE,
            this.LEV_END_DATE,
            this.W_DAYS_DIS,
            this.LEV_REMARKS});
            this.DGVLeaveStatus.ColumnToHide = null;
            this.DGVLeaveStatus.ColumnWidth = null;
            this.DGVLeaveStatus.CustomEnabled = true;
            this.DGVLeaveStatus.DisplayColumnWrapper = null;
            this.DGVLeaveStatus.GridDefaultRow = 0;
            this.DGVLeaveStatus.Location = new System.Drawing.Point(19, 5);
            this.DGVLeaveStatus.Name = "DGVLeaveStatus";
            this.DGVLeaveStatus.ReadOnlyColumns = null;
            this.DGVLeaveStatus.RequiredColumns = null;
            this.DGVLeaveStatus.Size = new System.Drawing.Size(637, 248);
            this.DGVLeaveStatus.SkippingColumns = null;
            this.DGVLeaveStatus.TabIndex = 1;
            // 
            // LEV_LV_TYPE
            // 
            this.LEV_LV_TYPE.DataPropertyName = "LEV_LV_TYPE";
            this.LEV_LV_TYPE.HeaderText = "Type";
            this.LEV_LV_TYPE.Name = "LEV_LV_TYPE";
            this.LEV_LV_TYPE.ReadOnly = true;
            this.LEV_LV_TYPE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LEV_START_DATE
            // 
            this.LEV_START_DATE.DataPropertyName = "LEV_START_DATE";
            this.LEV_START_DATE.HeaderText = "Started On";
            this.LEV_START_DATE.Name = "LEV_START_DATE";
            this.LEV_START_DATE.ReadOnly = true;
            this.LEV_START_DATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LEV_END_DATE
            // 
            this.LEV_END_DATE.DataPropertyName = "LEV_END_DATE";
            this.LEV_END_DATE.HeaderText = "Ended On";
            this.LEV_END_DATE.Name = "LEV_END_DATE";
            this.LEV_END_DATE.ReadOnly = true;
            this.LEV_END_DATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // W_DAYS_DIS
            // 
            this.W_DAYS_DIS.DataPropertyName = "W_DAYS_DIS";
            this.W_DAYS_DIS.HeaderText = "Days";
            this.W_DAYS_DIS.Name = "W_DAYS_DIS";
            this.W_DAYS_DIS.ReadOnly = true;
            this.W_DAYS_DIS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LEV_REMARKS
            // 
            this.LEV_REMARKS.DataPropertyName = "LEV_REMARKS";
            this.LEV_REMARKS.HeaderText = "Remarks";
            this.LEV_REMARKS.Name = "LEV_REMARKS";
            this.LEV_REMARKS.ReadOnly = true;
            this.LEV_REMARKS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LEV_REMARKS.Width = 193;
            // 
            // PnlLeaveStatus
            // 
            this.PnlLeaveStatus.ConcurrentPanels = null;
            this.PnlLeaveStatus.Controls.Add(this.txtCarryforward);
            this.PnlLeaveStatus.Controls.Add(this.blk4_lov);
            this.PnlLeaveStatus.Controls.Add(this.label9);
            this.PnlLeaveStatus.Controls.Add(this.label8);
            this.PnlLeaveStatus.Controls.Add(this.label7);
            this.PnlLeaveStatus.Controls.Add(this.label6);
            this.PnlLeaveStatus.Controls.Add(this.txtClEntitlement);
            this.PnlLeaveStatus.Controls.Add(this.txtPlEntitlement);
            this.PnlLeaveStatus.Controls.Add(this.label5);
            this.PnlLeaveStatus.Controls.Add(this.label4);
            this.PnlLeaveStatus.Controls.Add(this.label3);
            this.PnlLeaveStatus.Controls.Add(this.txtMlEntitlement);
            this.PnlLeaveStatus.Controls.Add(this.slTextBox3);
            this.PnlLeaveStatus.Controls.Add(this.txtPlAdvance);
            this.PnlLeaveStatus.Controls.Add(this.txtslhalfbal);
            this.PnlLeaveStatus.Controls.Add(this.txtSlEntitlement);
            this.PnlLeaveStatus.Controls.Add(this.label2);
            this.PnlLeaveStatus.Controls.Add(this.label1);
            this.PnlLeaveStatus.Controls.Add(this.slTextBox2);
            this.PnlLeaveStatus.Controls.Add(this.txtLev_no);
            this.PnlLeaveStatus.Controls.Add(this.label24);
            this.PnlLeaveStatus.Controls.Add(this.label23);
            this.PnlLeaveStatus.Controls.Add(this.label22);
            this.PnlLeaveStatus.Controls.Add(this.label21);
            this.PnlLeaveStatus.Controls.Add(this.label19);
            this.PnlLeaveStatus.Controls.Add(this.label13);
            this.PnlLeaveStatus.Controls.Add(this.label12);
            this.PnlLeaveStatus.Controls.Add(this.label11);
            this.PnlLeaveStatus.Controls.Add(this.txtPlAvailed);
            this.PnlLeaveStatus.Controls.Add(this.txtClAvailed);
            this.PnlLeaveStatus.Controls.Add(this.txtSlAvailed);
            this.PnlLeaveStatus.Controls.Add(this.txtMlAvailed);
            this.PnlLeaveStatus.Controls.Add(this.txtPlBalance);
            this.PnlLeaveStatus.Controls.Add(this.txtClBalance);
            this.PnlLeaveStatus.Controls.Add(this.txtSlBalance);
            this.PnlLeaveStatus.Controls.Add(this.txtMlBalance);
            this.PnlLeaveStatus.DataManager = null;
            this.PnlLeaveStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlLeaveStatus.DependentPanels = null;
            this.PnlLeaveStatus.DisableDependentLoad = false;
            this.PnlLeaveStatus.EnableDelete = true;
            this.PnlLeaveStatus.EnableInsert = true;
            this.PnlLeaveStatus.EnableQuery = false;
            this.PnlLeaveStatus.EnableUpdate = true;
            this.PnlLeaveStatus.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LeaveActualGridviewCommand";
            this.PnlLeaveStatus.Location = new System.Drawing.Point(12, 59);
            this.PnlLeaveStatus.MasterPanel = null;
            this.PnlLeaveStatus.Name = "PnlLeaveStatus";
            this.PnlLeaveStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlLeaveStatus.Size = new System.Drawing.Size(683, 190);
            this.PnlLeaveStatus.SPName = "CHRIS_SP_LEAVE_STATUS_gridview_MANAGER";
            this.PnlLeaveStatus.TabIndex = 69;
            // 
            // txtCarryforward
            // 
            this.txtCarryforward.AllowSpace = true;
            this.txtCarryforward.AssociatedLookUpName = "";
            this.txtCarryforward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCarryforward.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCarryforward.ContinuationTextBox = null;
            this.txtCarryforward.CustomEnabled = false;
            this.txtCarryforward.DataFieldMapping = "ls_c_forward";
            this.txtCarryforward.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCarryforward.GetRecordsOnUpDownKeys = false;
            this.txtCarryforward.IsDate = false;
            this.txtCarryforward.Location = new System.Drawing.Point(358, 21);
            this.txtCarryforward.Name = "txtCarryforward";
            this.txtCarryforward.NumberFormat = "###,###,##0.00";
            this.txtCarryforward.Postfix = "";
            this.txtCarryforward.Prefix = "";
            this.txtCarryforward.Size = new System.Drawing.Size(27, 20);
            this.txtCarryforward.SkipValidation = false;
            this.txtCarryforward.TabIndex = 110;
            this.txtCarryforward.TextType = CrplControlLibrary.TextType.String;
            this.txtCarryforward.Visible = false;
            // 
            // blk4_lov
            // 
            this.blk4_lov.ActionLOVExists = "Blk4_lovExists";
            this.blk4_lov.ActionType = "Blk4_lov";
            this.blk4_lov.ConditionalFields = "";
            this.blk4_lov.CustomEnabled = true;
            this.blk4_lov.DataFieldMapping = "";
            this.blk4_lov.DependentLovControls = "";
            this.blk4_lov.HiddenColumns = "";
            this.blk4_lov.Image = ((System.Drawing.Image)(resources.GetObject("blk4_lov.Image")));
            this.blk4_lov.LoadDependentEntities = true;
            this.blk4_lov.Location = new System.Drawing.Point(181, 48);
            this.blk4_lov.LookUpTitle = null;
            this.blk4_lov.Name = "blk4_lov";
            this.blk4_lov.Size = new System.Drawing.Size(26, 21);
            this.blk4_lov.SkipValidationOnLeave = false;
            this.blk4_lov.SPName = "CHRIS_SP_LEAVE_ACTUAL_GRID_MANAGER";
            this.blk4_lov.TabIndex = 109;
            this.blk4_lov.TabStop = false;
            this.blk4_lov.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(524, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 108;
            this.label9.Text = "ML Entitlement :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(526, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 107;
            this.label8.Text = "SL Entitlement :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(526, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 106;
            this.label7.Text = "CL Entitlement :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(526, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 105;
            this.label6.Text = "PL Entitlement :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtClEntitlement
            // 
            this.txtClEntitlement.AllowSpace = true;
            this.txtClEntitlement.AssociatedLookUpName = "";
            this.txtClEntitlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClEntitlement.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClEntitlement.ContinuationTextBox = null;
            this.txtClEntitlement.CustomEnabled = true;
            this.txtClEntitlement.DataFieldMapping = "W_CL_ENT";
            this.txtClEntitlement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClEntitlement.GetRecordsOnUpDownKeys = false;
            this.txtClEntitlement.IsDate = false;
            this.txtClEntitlement.Location = new System.Drawing.Point(629, 102);
            this.txtClEntitlement.Name = "txtClEntitlement";
            this.txtClEntitlement.NumberFormat = "###,###,##0.00";
            this.txtClEntitlement.Postfix = "";
            this.txtClEntitlement.Prefix = "";
            this.txtClEntitlement.ReadOnly = true;
            this.txtClEntitlement.Size = new System.Drawing.Size(27, 20);
            this.txtClEntitlement.SkipValidation = false;
            this.txtClEntitlement.TabIndex = 101;
            this.txtClEntitlement.TabStop = false;
            this.txtClEntitlement.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPlEntitlement
            // 
            this.txtPlEntitlement.AllowSpace = true;
            this.txtPlEntitlement.AssociatedLookUpName = "";
            this.txtPlEntitlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlEntitlement.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlEntitlement.ContinuationTextBox = null;
            this.txtPlEntitlement.CustomEnabled = true;
            this.txtPlEntitlement.DataFieldMapping = "W_PL_ENT";
            this.txtPlEntitlement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlEntitlement.GetRecordsOnUpDownKeys = false;
            this.txtPlEntitlement.IsDate = false;
            this.txtPlEntitlement.Location = new System.Drawing.Point(629, 76);
            this.txtPlEntitlement.Name = "txtPlEntitlement";
            this.txtPlEntitlement.NumberFormat = "###,###,##0.00";
            this.txtPlEntitlement.Postfix = "";
            this.txtPlEntitlement.Prefix = "";
            this.txtPlEntitlement.ReadOnly = true;
            this.txtPlEntitlement.Size = new System.Drawing.Size(27, 20);
            this.txtPlEntitlement.SkipValidation = false;
            this.txtPlEntitlement.TabIndex = 104;
            this.txtPlEntitlement.TabStop = false;
            this.txtPlEntitlement.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(376, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 103;
            this.label5.Text = "SL Half Pay  Bal. :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(404, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 102;
            this.label4.Text = "PL Advance :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(318, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 100;
            this.label3.Text = "Mandatory Yes/ No :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtMlEntitlement
            // 
            this.txtMlEntitlement.AllowSpace = true;
            this.txtMlEntitlement.AssociatedLookUpName = "";
            this.txtMlEntitlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMlEntitlement.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMlEntitlement.ContinuationTextBox = null;
            this.txtMlEntitlement.CustomEnabled = true;
            this.txtMlEntitlement.DataFieldMapping = "W_ML_ENT";
            this.txtMlEntitlement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMlEntitlement.GetRecordsOnUpDownKeys = false;
            this.txtMlEntitlement.IsDate = false;
            this.txtMlEntitlement.Location = new System.Drawing.Point(629, 155);
            this.txtMlEntitlement.Name = "txtMlEntitlement";
            this.txtMlEntitlement.NumberFormat = "###,###,##0.00";
            this.txtMlEntitlement.Postfix = "";
            this.txtMlEntitlement.Prefix = "";
            this.txtMlEntitlement.ReadOnly = true;
            this.txtMlEntitlement.Size = new System.Drawing.Size(27, 20);
            this.txtMlEntitlement.SkipValidation = false;
            this.txtMlEntitlement.TabIndex = 99;
            this.txtMlEntitlement.TabStop = false;
            this.txtMlEntitlement.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "LS_MANDATORY";
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.Location = new System.Drawing.Point(494, 76);
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.ReadOnly = true;
            this.slTextBox3.Size = new System.Drawing.Size(27, 20);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 98;
            this.slTextBox3.TabStop = false;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPlAdvance
            // 
            this.txtPlAdvance.AllowSpace = true;
            this.txtPlAdvance.AssociatedLookUpName = "";
            this.txtPlAdvance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlAdvance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlAdvance.ContinuationTextBox = null;
            this.txtPlAdvance.CustomEnabled = true;
            this.txtPlAdvance.DataFieldMapping = "LS_LEV_ADV";
            this.txtPlAdvance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlAdvance.GetRecordsOnUpDownKeys = false;
            this.txtPlAdvance.IsDate = false;
            this.txtPlAdvance.Location = new System.Drawing.Point(494, 102);
            this.txtPlAdvance.Name = "txtPlAdvance";
            this.txtPlAdvance.NumberFormat = "###,###,##0.00";
            this.txtPlAdvance.Postfix = "";
            this.txtPlAdvance.Prefix = "";
            this.txtPlAdvance.ReadOnly = true;
            this.txtPlAdvance.Size = new System.Drawing.Size(27, 20);
            this.txtPlAdvance.SkipValidation = false;
            this.txtPlAdvance.TabIndex = 97;
            this.txtPlAdvance.TabStop = false;
            this.txtPlAdvance.TextType = CrplControlLibrary.TextType.String;
            this.txtPlAdvance.TextChanged += new System.EventHandler(this.slTextBox4_TextChanged);
            // 
            // txtslhalfbal
            // 
            this.txtslhalfbal.AllowSpace = true;
            this.txtslhalfbal.AssociatedLookUpName = "";
            this.txtslhalfbal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtslhalfbal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtslhalfbal.ContinuationTextBox = null;
            this.txtslhalfbal.CustomEnabled = true;
            this.txtslhalfbal.DataFieldMapping = "LS_SL_HF_BAL";
            this.txtslhalfbal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtslhalfbal.GetRecordsOnUpDownKeys = false;
            this.txtslhalfbal.IsDate = false;
            this.txtslhalfbal.Location = new System.Drawing.Point(494, 129);
            this.txtslhalfbal.Name = "txtslhalfbal";
            this.txtslhalfbal.NumberFormat = "###,###,##0.00";
            this.txtslhalfbal.Postfix = "";
            this.txtslhalfbal.Prefix = "";
            this.txtslhalfbal.ReadOnly = true;
            this.txtslhalfbal.Size = new System.Drawing.Size(27, 20);
            this.txtslhalfbal.SkipValidation = false;
            this.txtslhalfbal.TabIndex = 96;
            this.txtslhalfbal.TabStop = false;
            this.txtslhalfbal.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSlEntitlement
            // 
            this.txtSlEntitlement.AllowSpace = true;
            this.txtSlEntitlement.AssociatedLookUpName = "";
            this.txtSlEntitlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlEntitlement.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlEntitlement.ContinuationTextBox = null;
            this.txtSlEntitlement.CustomEnabled = true;
            this.txtSlEntitlement.DataFieldMapping = "W_SL_ENT";
            this.txtSlEntitlement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlEntitlement.GetRecordsOnUpDownKeys = false;
            this.txtSlEntitlement.IsDate = false;
            this.txtSlEntitlement.Location = new System.Drawing.Point(629, 129);
            this.txtSlEntitlement.Name = "txtSlEntitlement";
            this.txtSlEntitlement.NumberFormat = "###,###,##0.00";
            this.txtSlEntitlement.Postfix = "";
            this.txtSlEntitlement.Prefix = "";
            this.txtSlEntitlement.ReadOnly = true;
            this.txtSlEntitlement.Size = new System.Drawing.Size(27, 20);
            this.txtSlEntitlement.SkipValidation = false;
            this.txtSlEntitlement.TabIndex = 95;
            this.txtSlEntitlement.TabStop = false;
            this.txtSlEntitlement.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(232, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 94;
            this.label2.Text = "Name :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 93;
            this.label1.Text = "P. no :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "W_NAME";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(285, 47);
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.ReadOnly = true;
            this.slTextBox2.Size = new System.Drawing.Size(371, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 92;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLev_no
            // 
            this.txtLev_no.AllowSpace = true;
            this.txtLev_no.AssociatedLookUpName = "blk4_lov";
            this.txtLev_no.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLev_no.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLev_no.ContinuationTextBox = null;
            this.txtLev_no.CustomEnabled = true;
            this.txtLev_no.DataFieldMapping = "LS_P_NO";
            this.txtLev_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLev_no.GetRecordsOnUpDownKeys = false;
            this.txtLev_no.IsDate = false;
            this.txtLev_no.Location = new System.Drawing.Point(107, 48);
            this.txtLev_no.MaxLength = 6;
            this.txtLev_no.Name = "txtLev_no";
            this.txtLev_no.NumberFormat = "###,###,##0.00";
            this.txtLev_no.Postfix = "";
            this.txtLev_no.Prefix = "";
            this.txtLev_no.Size = new System.Drawing.Size(68, 20);
            this.txtLev_no.SkipValidation = false;
            this.txtLev_no.TabIndex = 91;
            this.txtLev_no.TextType = CrplControlLibrary.TextType.Integer;
            this.txtLev_no.Validating += new System.ComponentModel.CancelEventHandler(this.txtLev_no_Validating);
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(197, 157);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 90;
            this.label24.Text = "ML Balance :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(199, 131);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 13);
            this.label23.TabIndex = 89;
            this.label23.Text = "SL Balance :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(199, 107);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 13);
            this.label22.TabIndex = 88;
            this.label22.Text = "CL Balance :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(199, 81);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 87;
            this.label21.Text = "PL Balance :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(19, 153);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 13);
            this.label19.TabIndex = 86;
            this.label19.Text = "ML Availed :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 85;
            this.label13.Text = "SL Availed :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 107);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 84;
            this.label12.Text = "CL Availed :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 81);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 83;
            this.label11.Text = "PL Availed :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPlAvailed
            // 
            this.txtPlAvailed.AllowSpace = true;
            this.txtPlAvailed.AssociatedLookUpName = "";
            this.txtPlAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlAvailed.ContinuationTextBox = null;
            this.txtPlAvailed.CustomEnabled = true;
            this.txtPlAvailed.DataFieldMapping = "LS_PL_AVAIL";
            this.txtPlAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlAvailed.GetRecordsOnUpDownKeys = false;
            this.txtPlAvailed.IsDate = false;
            this.txtPlAvailed.Location = new System.Drawing.Point(107, 76);
            this.txtPlAvailed.Name = "txtPlAvailed";
            this.txtPlAvailed.NumberFormat = "###,###,##0.00";
            this.txtPlAvailed.Postfix = "";
            this.txtPlAvailed.Prefix = "";
            this.txtPlAvailed.ReadOnly = true;
            this.txtPlAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtPlAvailed.SkipValidation = false;
            this.txtPlAvailed.TabIndex = 82;
            this.txtPlAvailed.TabStop = false;
            this.txtPlAvailed.TextType = CrplControlLibrary.TextType.String;
            this.txtPlAvailed.TextChanged += new System.EventHandler(this.txtPlAvailed_TextChanged);
            // 
            // txtClAvailed
            // 
            this.txtClAvailed.AllowSpace = true;
            this.txtClAvailed.AssociatedLookUpName = "";
            this.txtClAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClAvailed.ContinuationTextBox = null;
            this.txtClAvailed.CustomEnabled = true;
            this.txtClAvailed.DataFieldMapping = "LS_CL_AVAIL";
            this.txtClAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClAvailed.GetRecordsOnUpDownKeys = false;
            this.txtClAvailed.IsDate = false;
            this.txtClAvailed.Location = new System.Drawing.Point(107, 102);
            this.txtClAvailed.Name = "txtClAvailed";
            this.txtClAvailed.NumberFormat = "###,###,##0.00";
            this.txtClAvailed.Postfix = "";
            this.txtClAvailed.Prefix = "";
            this.txtClAvailed.ReadOnly = true;
            this.txtClAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtClAvailed.SkipValidation = false;
            this.txtClAvailed.TabIndex = 81;
            this.txtClAvailed.TabStop = false;
            this.txtClAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSlAvailed
            // 
            this.txtSlAvailed.AllowSpace = true;
            this.txtSlAvailed.AssociatedLookUpName = "";
            this.txtSlAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlAvailed.ContinuationTextBox = null;
            this.txtSlAvailed.CustomEnabled = true;
            this.txtSlAvailed.DataFieldMapping = "LS_SL_AVAIL";
            this.txtSlAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlAvailed.GetRecordsOnUpDownKeys = false;
            this.txtSlAvailed.IsDate = false;
            this.txtSlAvailed.Location = new System.Drawing.Point(107, 129);
            this.txtSlAvailed.Name = "txtSlAvailed";
            this.txtSlAvailed.NumberFormat = "###,###,##0.00";
            this.txtSlAvailed.Postfix = "";
            this.txtSlAvailed.Prefix = "";
            this.txtSlAvailed.ReadOnly = true;
            this.txtSlAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtSlAvailed.SkipValidation = false;
            this.txtSlAvailed.TabIndex = 80;
            this.txtSlAvailed.TabStop = false;
            this.txtSlAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtMlAvailed
            // 
            this.txtMlAvailed.AllowSpace = true;
            this.txtMlAvailed.AssociatedLookUpName = "";
            this.txtMlAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMlAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMlAvailed.ContinuationTextBox = null;
            this.txtMlAvailed.CustomEnabled = true;
            this.txtMlAvailed.DataFieldMapping = "LS_ML_AVAIL";
            this.txtMlAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMlAvailed.GetRecordsOnUpDownKeys = false;
            this.txtMlAvailed.IsDate = false;
            this.txtMlAvailed.Location = new System.Drawing.Point(107, 155);
            this.txtMlAvailed.Name = "txtMlAvailed";
            this.txtMlAvailed.NumberFormat = "###,###,##0.00";
            this.txtMlAvailed.Postfix = "";
            this.txtMlAvailed.Prefix = "";
            this.txtMlAvailed.ReadOnly = true;
            this.txtMlAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtMlAvailed.SkipValidation = false;
            this.txtMlAvailed.TabIndex = 79;
            this.txtMlAvailed.TabStop = false;
            this.txtMlAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPlBalance
            // 
            this.txtPlBalance.AllowSpace = true;
            this.txtPlBalance.AssociatedLookUpName = "";
            this.txtPlBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlBalance.ContinuationTextBox = null;
            this.txtPlBalance.CustomEnabled = true;
            this.txtPlBalance.DataFieldMapping = "LS_PL_BAL";
            this.txtPlBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlBalance.GetRecordsOnUpDownKeys = false;
            this.txtPlBalance.IsDate = false;
            this.txtPlBalance.Location = new System.Drawing.Point(285, 76);
            this.txtPlBalance.Name = "txtPlBalance";
            this.txtPlBalance.NumberFormat = "###,###,##0.00";
            this.txtPlBalance.Postfix = "";
            this.txtPlBalance.Prefix = "";
            this.txtPlBalance.ReadOnly = true;
            this.txtPlBalance.Size = new System.Drawing.Size(27, 20);
            this.txtPlBalance.SkipValidation = false;
            this.txtPlBalance.TabIndex = 78;
            this.txtPlBalance.TabStop = false;
            this.txtPlBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtClBalance
            // 
            this.txtClBalance.AllowSpace = true;
            this.txtClBalance.AssociatedLookUpName = "";
            this.txtClBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClBalance.ContinuationTextBox = null;
            this.txtClBalance.CustomEnabled = true;
            this.txtClBalance.DataFieldMapping = "LS_CL_BAL";
            this.txtClBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClBalance.GetRecordsOnUpDownKeys = false;
            this.txtClBalance.IsDate = false;
            this.txtClBalance.Location = new System.Drawing.Point(285, 102);
            this.txtClBalance.Name = "txtClBalance";
            this.txtClBalance.NumberFormat = "###,###,##0.00";
            this.txtClBalance.Postfix = "";
            this.txtClBalance.Prefix = "";
            this.txtClBalance.ReadOnly = true;
            this.txtClBalance.Size = new System.Drawing.Size(27, 20);
            this.txtClBalance.SkipValidation = false;
            this.txtClBalance.TabIndex = 77;
            this.txtClBalance.TabStop = false;
            this.txtClBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSlBalance
            // 
            this.txtSlBalance.AllowSpace = true;
            this.txtSlBalance.AssociatedLookUpName = "";
            this.txtSlBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlBalance.ContinuationTextBox = null;
            this.txtSlBalance.CustomEnabled = true;
            this.txtSlBalance.DataFieldMapping = "LS_SL_BAL";
            this.txtSlBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlBalance.GetRecordsOnUpDownKeys = false;
            this.txtSlBalance.IsDate = false;
            this.txtSlBalance.Location = new System.Drawing.Point(285, 129);
            this.txtSlBalance.Name = "txtSlBalance";
            this.txtSlBalance.NumberFormat = "###,###,##0.00";
            this.txtSlBalance.Postfix = "";
            this.txtSlBalance.Prefix = "";
            this.txtSlBalance.ReadOnly = true;
            this.txtSlBalance.Size = new System.Drawing.Size(27, 20);
            this.txtSlBalance.SkipValidation = false;
            this.txtSlBalance.TabIndex = 76;
            this.txtSlBalance.TabStop = false;
            this.txtSlBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtMlBalance
            // 
            this.txtMlBalance.AllowSpace = true;
            this.txtMlBalance.AssociatedLookUpName = "";
            this.txtMlBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMlBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMlBalance.ContinuationTextBox = null;
            this.txtMlBalance.CustomEnabled = true;
            this.txtMlBalance.DataFieldMapping = "LS_ML_BAL";
            this.txtMlBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMlBalance.GetRecordsOnUpDownKeys = false;
            this.txtMlBalance.IsDate = false;
            this.txtMlBalance.Location = new System.Drawing.Point(285, 155);
            this.txtMlBalance.Name = "txtMlBalance";
            this.txtMlBalance.NumberFormat = "###,###,##0.00";
            this.txtMlBalance.Postfix = "";
            this.txtMlBalance.Prefix = "";
            this.txtMlBalance.ReadOnly = true;
            this.txtMlBalance.Size = new System.Drawing.Size(27, 20);
            this.txtMlBalance.SkipValidation = false;
            this.txtMlBalance.TabIndex = 75;
            this.txtMlBalance.TabStop = false;
            this.txtMlBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // Chris_Personnel_ActualLeaveEntry_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 577);
            this.Controls.Add(this.PnlLeaveStatus);
            this.Controls.Add(this.PnlLeaveActual1);
            this.CurrentPanelBlock = "PnlLeaveStatus";
            this.Name = "Chris_Personnel_ActualLeaveEntry_";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "Chris_Personnel_ActualLeaveEntry";
            this.Shown += new System.EventHandler(this.Chris_Personnel_ActualLeaveEntry_gridview_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlLeaveActual1, 0);
            this.Controls.SetChildIndex(this.PnlLeaveStatus, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlLeaveActual1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVLeaveStatus)).EndInit();
            this.PnlLeaveStatus.ResumeLayout(false);
            this.PnlLeaveStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlLeaveActual1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVLeaveStatus;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlLeaveStatus;
        private CrplControlLibrary.LookupButton blk4_lov;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtClEntitlement;
        private CrplControlLibrary.SLTextBox txtPlEntitlement;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtMlEntitlement;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLTextBox txtPlAdvance;
        private CrplControlLibrary.SLTextBox txtslhalfbal;
        private CrplControlLibrary.SLTextBox txtSlEntitlement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox txtLev_no;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtPlAvailed;
        private CrplControlLibrary.SLTextBox txtClAvailed;
        private CrplControlLibrary.SLTextBox txtSlAvailed;
        private CrplControlLibrary.SLTextBox txtMlAvailed;
        private CrplControlLibrary.SLTextBox txtPlBalance;
        private CrplControlLibrary.SLTextBox txtClBalance;
        private CrplControlLibrary.SLTextBox txtSlBalance;
        private CrplControlLibrary.SLTextBox txtMlBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn LEV_LV_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LEV_START_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LEV_END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_DAYS_DIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn LEV_REMARKS;
        private CrplControlLibrary.SLTextBox txtCarryforward;
    }
}