namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_BrnReLocBrnMgrTrnsfr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_BrnReLocBrnMgrTrnsfr));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.IDHiddenPrsonl = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTempOption = new CrplControlLibrary.SLTextBox(this.components);
            this.lkbtnDesc = new CrplControlLibrary.LookupButton(this.components);
            this.txtTypeOf = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFunctional = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTitle = new CrplControlLibrary.SLTextBox(this.components);
            this.lbpersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNO = new CrplControlLibrary.SLTextBox(this.components);
            this.dgvSDC = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_SEGMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DEPT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CONTRIB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.lblSubHeader = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlSDC = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.pnlTSDC = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvTSDC = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colSeg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDept = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.PR_CONTRIB1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTransfer = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_PR_TAX_ON_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_UTILITIES = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_RENT = new CrplControlLibrary.SLTextBox(this.components);
            this.IDHidden = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPr_no_hidden = new CrplControlLibrary.SLTextBox(this.components);
            this.lkbtnTBrnch = new CrplControlLibrary.LookupButton(this.components);
            this.dtpTEffDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTRemarks = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTFLCity = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTISCord = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTFuncTitle = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTNewBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSDC)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlSDC.SuspendLayout();
            this.pnlTSDC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTSDC)).BeginInit();
            this.pnlTransfer.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(638, 0);
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            this.txtOption.Validated += new System.EventHandler(this.txtOption_Validated);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(674, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 595);
            this.panel1.Size = new System.Drawing.Size(674, 60);
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.IDHiddenPrsonl);
            this.PnlPersonnel.Controls.Add(this.txtTempOption);
            this.PnlPersonnel.Controls.Add(this.lkbtnDesc);
            this.PnlPersonnel.Controls.Add(this.txtTypeOf);
            this.PnlPersonnel.Controls.Add(this.txtBranch);
            this.PnlPersonnel.Controls.Add(this.label17);
            this.PnlPersonnel.Controls.Add(this.label14);
            this.PnlPersonnel.Controls.Add(this.txtFunctional);
            this.PnlPersonnel.Controls.Add(this.txtTitle);
            this.PnlPersonnel.Controls.Add(this.lbpersonnel);
            this.PnlPersonnel.Controls.Add(this.label7);
            this.PnlPersonnel.Controls.Add(this.label6);
            this.PnlPersonnel.Controls.Add(this.label5);
            this.PnlPersonnel.Controls.Add(this.label4);
            this.PnlPersonnel.Controls.Add(this.label2);
            this.PnlPersonnel.Controls.Add(this.label3);
            this.PnlPersonnel.Controls.Add(this.label1);
            this.PnlPersonnel.Controls.Add(this.txtTType);
            this.PnlPersonnel.Controls.Add(this.txtLevel);
            this.PnlPersonnel.Controls.Add(this.txtFirstName);
            this.PnlPersonnel.Controls.Add(this.txtLastName);
            this.PnlPersonnel.Controls.Add(this.txtDesig);
            this.PnlPersonnel.Controls.Add(this.txtPersonnelNO);
            this.PnlPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = false;
            this.PnlPersonnel.EnableInsert = false;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = false;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(6, 146);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(443, 246);
            this.PnlPersonnel.SPName = "CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER";
            this.PnlPersonnel.TabIndex = 54;
            // 
            // IDHiddenPrsonl
            // 
            this.IDHiddenPrsonl.AllowSpace = true;
            this.IDHiddenPrsonl.AssociatedLookUpName = "";
            this.IDHiddenPrsonl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDHiddenPrsonl.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.IDHiddenPrsonl.ContinuationTextBox = null;
            this.IDHiddenPrsonl.CustomEnabled = true;
            this.IDHiddenPrsonl.DataFieldMapping = "ID";
            this.IDHiddenPrsonl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDHiddenPrsonl.GetRecordsOnUpDownKeys = false;
            this.IDHiddenPrsonl.IsDate = false;
            this.IDHiddenPrsonl.Location = new System.Drawing.Point(286, 45);
            this.IDHiddenPrsonl.Name = "IDHiddenPrsonl";
            this.IDHiddenPrsonl.NumberFormat = "###,###,##0.00";
            this.IDHiddenPrsonl.Postfix = "";
            this.IDHiddenPrsonl.Prefix = "";
            this.IDHiddenPrsonl.Size = new System.Drawing.Size(100, 20);
            this.IDHiddenPrsonl.SkipValidation = false;
            this.IDHiddenPrsonl.TabIndex = 77;
            this.IDHiddenPrsonl.TextType = CrplControlLibrary.TextType.String;
            this.IDHiddenPrsonl.Visible = false;
            // 
            // txtTempOption
            // 
            this.txtTempOption.AllowSpace = true;
            this.txtTempOption.AssociatedLookUpName = "";
            this.txtTempOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTempOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTempOption.ContinuationTextBox = null;
            this.txtTempOption.CustomEnabled = true;
            this.txtTempOption.DataFieldMapping = "";
            this.txtTempOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTempOption.GetRecordsOnUpDownKeys = false;
            this.txtTempOption.IsDate = false;
            this.txtTempOption.Location = new System.Drawing.Point(244, 201);
            this.txtTempOption.Name = "txtTempOption";
            this.txtTempOption.NumberFormat = "###,###,##0.00";
            this.txtTempOption.Postfix = "";
            this.txtTempOption.Prefix = "";
            this.txtTempOption.ReadOnly = true;
            this.txtTempOption.Size = new System.Drawing.Size(34, 20);
            this.txtTempOption.SkipValidation = false;
            this.txtTempOption.TabIndex = 63;
            this.txtTempOption.TextType = CrplControlLibrary.TextType.String;
            this.txtTempOption.Visible = false;
            // 
            // lkbtnDesc
            // 
            this.lkbtnDesc.ActionLOVExists = "MAIN_LOV_EXIST";
            this.lkbtnDesc.ActionType = "MAIN_LOV";
            this.lkbtnDesc.ConditionalFields = "";
            this.lkbtnDesc.CustomEnabled = true;
            this.lkbtnDesc.DataFieldMapping = "";
            this.lkbtnDesc.DependentLovControls = "";
            this.lkbtnDesc.HiddenColumns = "";
            this.lkbtnDesc.Image = ((System.Drawing.Image)(resources.GetObject("lkbtnDesc.Image")));
            this.lkbtnDesc.LoadDependentEntities = false;
            this.lkbtnDesc.Location = new System.Drawing.Point(381, 11);
            this.lkbtnDesc.LookUpTitle = null;
            this.lkbtnDesc.Name = "lkbtnDesc";
            this.lkbtnDesc.Size = new System.Drawing.Size(26, 21);
            this.lkbtnDesc.SkipValidationOnLeave = false;
            this.lkbtnDesc.SPName = "CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER";
            this.lkbtnDesc.TabIndex = 62;
            this.lkbtnDesc.TabStop = false;
            this.lkbtnDesc.UseVisualStyleBackColor = true;
            // 
            // txtTypeOf
            // 
            this.txtTypeOf.AllowSpace = true;
            this.txtTypeOf.AssociatedLookUpName = "lkbtnDesc";
            this.txtTypeOf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTypeOf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTypeOf.ContinuationTextBox = null;
            this.txtTypeOf.CustomEnabled = true;
            this.txtTypeOf.DataFieldMapping = "OP_DESC";
            this.txtTypeOf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTypeOf.GetRecordsOnUpDownKeys = false;
            this.txtTypeOf.IsDate = false;
            this.txtTypeOf.IsRequired = true;
            this.txtTypeOf.Location = new System.Drawing.Point(230, 11);
            this.txtTypeOf.Name = "txtTypeOf";
            this.txtTypeOf.NumberFormat = "###,###,##0.00";
            this.txtTypeOf.Postfix = "";
            this.txtTypeOf.Prefix = "";
            this.txtTypeOf.Size = new System.Drawing.Size(145, 20);
            this.txtTypeOf.SkipValidation = false;
            this.txtTypeOf.TabIndex = 1;
            this.txtTypeOf.TabStop = false;
            this.txtTypeOf.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = false;
            this.txtBranch.DataFieldMapping = "PR_NEW_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(138, 175);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(100, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 17;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(65, 179);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 56;
            this.label17.Text = "6. Branch :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 226);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(448, 13);
            this.label14.TabIndex = 55;
            this.label14.Text = "_______________________________________________________________";
            // 
            // txtFunctional
            // 
            this.txtFunctional.AllowSpace = true;
            this.txtFunctional.AssociatedLookUpName = "";
            this.txtFunctional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFunctional.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFunctional.ContinuationTextBox = null;
            this.txtFunctional.CustomEnabled = false;
            this.txtFunctional.DataFieldMapping = "PR_FUNC_TITTLE1";
            this.txtFunctional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFunctional.GetRecordsOnUpDownKeys = false;
            this.txtFunctional.IsDate = false;
            this.txtFunctional.Location = new System.Drawing.Point(138, 123);
            this.txtFunctional.Name = "txtFunctional";
            this.txtFunctional.NumberFormat = "###,###,##0.00";
            this.txtFunctional.Postfix = "";
            this.txtFunctional.Prefix = "";
            this.txtFunctional.ReadOnly = true;
            this.txtFunctional.Size = new System.Drawing.Size(293, 20);
            this.txtFunctional.SkipValidation = false;
            this.txtFunctional.TabIndex = 15;
            this.txtFunctional.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTitle
            // 
            this.txtTitle.AllowSpace = true;
            this.txtTitle.AssociatedLookUpName = "";
            this.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTitle.ContinuationTextBox = null;
            this.txtTitle.CustomEnabled = false;
            this.txtTitle.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.GetRecordsOnUpDownKeys = false;
            this.txtTitle.IsDate = false;
            this.txtTitle.Location = new System.Drawing.Point(138, 149);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.NumberFormat = "###,###,##0.00";
            this.txtTitle.Postfix = "";
            this.txtTitle.Prefix = "";
            this.txtTitle.ReadOnly = true;
            this.txtTitle.Size = new System.Drawing.Size(293, 20);
            this.txtTitle.SkipValidation = false;
            this.txtTitle.TabIndex = 16;
            this.txtTitle.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbpersonnel
            // 
            this.lbpersonnel.ActionLOVExists = "PP_NO_EXISTS";
            this.lbpersonnel.ActionType = "PP_LOV";
            this.lbpersonnel.ConditionalFields = "txtTypeOf|txtTempOption";
            this.lbpersonnel.CustomEnabled = true;
            this.lbpersonnel.DataFieldMapping = "";
            this.lbpersonnel.DependentLovControls = "";
            this.lbpersonnel.HiddenColumns = "PR_DESIG|PR_LEVEL|PR_CATEGORY|PR_FUNC_TITTLE1|PR_FUNC_TITTLE2|PR_NEW_BRANCH|PR_TR" +
                "ANSFER";
            this.lbpersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbpersonnel.Image")));
            this.lbpersonnel.LoadDependentEntities = true;
            this.lbpersonnel.Location = new System.Drawing.Point(244, 45);
            this.lbpersonnel.LookUpTitle = null;
            this.lbpersonnel.Name = "lbpersonnel";
            this.lbpersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbpersonnel.SkipValidationOnLeave = false;
            this.lbpersonnel.SPName = "CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER";
            this.lbpersonnel.TabIndex = 1;
            this.lbpersonnel.TabStop = false;
            this.lbpersonnel.UseVisualStyleBackColor = true;
            this.lbpersonnel.Click += new System.EventHandler(this.lbpersonnel_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(26, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "7. Transfer Type :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(272, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Level :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(73, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Title :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(45, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "5. Functional :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "3. Designation :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(65, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "2. Name :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "1. Personnel No. :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTType
            // 
            this.txtTType.AllowSpace = true;
            this.txtTType.AssociatedLookUpName = "";
            this.txtTType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTType.ContinuationTextBox = null;
            this.txtTType.CustomEnabled = false;
            this.txtTType.DataFieldMapping = "PR_TRANSFER";
            this.txtTType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTType.GetRecordsOnUpDownKeys = false;
            this.txtTType.IsDate = false;
            this.txtTType.Location = new System.Drawing.Point(138, 201);
            this.txtTType.Name = "txtTType";
            this.txtTType.NumberFormat = "###,###,##0.00";
            this.txtTType.Postfix = "";
            this.txtTType.Prefix = "";
            this.txtTType.ReadOnly = true;
            this.txtTType.Size = new System.Drawing.Size(100, 20);
            this.txtTType.SkipValidation = false;
            this.txtTType.TabIndex = 18;
            this.txtTType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = false;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(331, 97);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(100, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 14;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = false;
            this.txtFirstName.DataFieldMapping = "FIRSTNAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(138, 71);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(145, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 11;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = false;
            this.txtLastName.DataFieldMapping = "LASTNAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(286, 71);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.ReadOnly = true;
            this.txtLastName.Size = new System.Drawing.Size(145, 20);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 12;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = false;
            this.txtDesig.DataFieldMapping = "PR_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(138, 97);
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(100, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 13;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNO
            // 
            this.txtPersonnelNO.AllowSpace = true;
            this.txtPersonnelNO.AssociatedLookUpName = "lbpersonnel";
            this.txtPersonnelNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNO.ContinuationTextBox = null;
            this.txtPersonnelNO.CustomEnabled = true;
            this.txtPersonnelNO.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNO.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNO.IsDate = false;
            this.txtPersonnelNO.IsRequired = true;
            this.txtPersonnelNO.Location = new System.Drawing.Point(138, 45);
            this.txtPersonnelNO.MaxLength = 6;
            this.txtPersonnelNO.Name = "txtPersonnelNO";
            this.txtPersonnelNO.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNO.Postfix = "";
            this.txtPersonnelNO.Prefix = "";
            this.txtPersonnelNO.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNO.SkipValidation = false;
            this.txtPersonnelNO.TabIndex = 0;
            this.txtPersonnelNO.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNO.TextChanged += new System.EventHandler(this.txtPersonnelNO_TextChanged);
            this.txtPersonnelNO.Leave += new System.EventHandler(this.txtPersonnelNO_Leave);
            this.txtPersonnelNO.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNO_Validating);
            // 
            // dgvSDC
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSDC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSDC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSDC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_SEGMENT,
            this.PR_DEPT,
            this.PR_CONTRIB});
            this.dgvSDC.ColumnToHide = null;
            this.dgvSDC.ColumnWidth = null;
            this.dgvSDC.CustomEnabled = true;
            this.dgvSDC.DisplayColumnWrapper = null;
            this.dgvSDC.GridDefaultRow = 0;
            this.dgvSDC.Location = new System.Drawing.Point(7, 10);
            this.dgvSDC.Name = "dgvSDC";
            this.dgvSDC.ReadOnlyColumns = null;
            this.dgvSDC.RequiredColumns = null;
            this.dgvSDC.Size = new System.Drawing.Size(200, 184);
            this.dgvSDC.SkippingColumns = null;
            this.dgvSDC.TabIndex = 100;
            // 
            // PR_SEGMENT
            // 
            this.PR_SEGMENT.DataPropertyName = "PR_SEGMENT";
            this.PR_SEGMENT.FillWeight = 20F;
            this.PR_SEGMENT.HeaderText = "Seg.";
            this.PR_SEGMENT.Name = "PR_SEGMENT";
            this.PR_SEGMENT.ReadOnly = true;
            this.PR_SEGMENT.Width = 40;
            // 
            // PR_DEPT
            // 
            this.PR_DEPT.DataPropertyName = "PR_DEPT";
            this.PR_DEPT.FillWeight = 20F;
            this.PR_DEPT.HeaderText = "Dept.";
            this.PR_DEPT.Name = "PR_DEPT";
            this.PR_DEPT.ReadOnly = true;
            this.PR_DEPT.Width = 60;
            // 
            // PR_CONTRIB
            // 
            this.PR_CONTRIB.DataPropertyName = "PR_CONTRIB";
            this.PR_CONTRIB.FillWeight = 20F;
            this.PR_CONTRIB.HeaderText = "%Contrib.";
            this.PR_CONTRIB.Name = "PR_CONTRIB";
            this.PR_CONTRIB.ReadOnly = true;
            this.PR_CONTRIB.Width = 60;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.lblSubHeader);
            this.pnlHead.Controls.Add(this.lblHeader);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Location = new System.Drawing.Point(55, 52);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(536, 88);
            this.pnlHead.TabIndex = 55;
            // 
            // lblSubHeader
            // 
            this.lblSubHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSubHeader.Location = new System.Drawing.Point(173, 44);
            this.lblSubHeader.Name = "lblSubHeader";
            this.lblSubHeader.Size = new System.Drawing.Size(153, 18);
            this.lblSubHeader.TabIndex = 20;
            this.lblSubHeader.Text = "BRANCH TO BRANCH";
            this.lblSubHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeader
            // 
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(141, 10);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(235, 20);
            this.lblHeader.TabIndex = 19;
            this.lblHeader.Text = "PERSONNEL SYSTEM";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(446, 56);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(446, 30);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(399, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(387, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(406, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 56;
            this.lblUserName.Text = "User Name  :";
            // 
            // pnlSDC
            // 
            this.pnlSDC.ConcurrentPanels = null;
            this.pnlSDC.Controls.Add(this.dgvSDC);
            this.pnlSDC.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlSDC.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSDC.DependentPanels = null;
            this.pnlSDC.DisableDependentLoad = false;
            this.pnlSDC.EnableDelete = false;
            this.pnlSDC.EnableInsert = false;
            this.pnlSDC.EnableQuery = false;
            this.pnlSDC.EnableUpdate = false;
            this.pnlSDC.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand_Personnel";
            this.pnlSDC.Location = new System.Drawing.Point(455, 147);
            this.pnlSDC.MasterPanel = this.PnlPersonnel;
            this.pnlSDC.Name = "pnlSDC";
            this.pnlSDC.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSDC.Size = new System.Drawing.Size(214, 217);
            this.pnlSDC.SPName = "CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER_ReadOnly";
            this.pnlSDC.TabIndex = 104;
            // 
            // pnlTSDC
            // 
            this.pnlTSDC.ConcurrentPanels = null;
            this.pnlTSDC.Controls.Add(this.dgvTSDC);
            this.pnlTSDC.DataManager = null;
            this.pnlTSDC.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTSDC.DependentPanels = null;
            this.pnlTSDC.DisableDependentLoad = false;
            this.pnlTSDC.EnableDelete = true;
            this.pnlTSDC.EnableInsert = true;
            this.pnlTSDC.EnableQuery = false;
            this.pnlTSDC.EnableUpdate = true;
            this.pnlTSDC.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDeptContCommand";
            this.pnlTSDC.Location = new System.Drawing.Point(455, 373);
            this.pnlTSDC.MasterPanel = this.pnlTransfer;
            this.pnlTSDC.Name = "pnlTSDC";
            this.pnlTSDC.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTSDC.Size = new System.Drawing.Size(214, 217);
            this.pnlTSDC.SPName = "CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER";
            this.pnlTSDC.TabIndex = 64;
            // 
            // dgvTSDC
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTSDC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTSDC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTSDC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSeg,
            this.colDept,
            this.PR_CONTRIB1,
            this.ID});
            this.dgvTSDC.ColumnToHide = null;
            this.dgvTSDC.ColumnWidth = null;
            this.dgvTSDC.CustomEnabled = true;
            this.dgvTSDC.DisplayColumnWrapper = null;
            this.dgvTSDC.GridDefaultRow = 0;
            this.dgvTSDC.Location = new System.Drawing.Point(7, 16);
            this.dgvTSDC.Name = "dgvTSDC";
            this.dgvTSDC.ReadOnlyColumns = null;
            this.dgvTSDC.RequiredColumns = null;
            this.dgvTSDC.Size = new System.Drawing.Size(200, 185);
            this.dgvTSDC.SkippingColumns = null;
            this.dgvTSDC.TabIndex = 8;
            this.dgvTSDC.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvTSDC_CellValidating);
            this.dgvTSDC.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTSDC_RowValidated);
            // 
            // colSeg
            // 
            this.colSeg.DataPropertyName = "PR_SEGMENT";
            this.colSeg.FillWeight = 20F;
            this.colSeg.HeaderText = "Seg.";
            this.colSeg.Name = "colSeg";
            this.colSeg.Width = 40;
            // 
            // colDept
            // 
            this.colDept.ActionLOV = "DEPT_LOV";
            this.colDept.ActionLOVExists = "DEPT_LOV_EXISTS";
            this.colDept.AttachParentEntity = false;
            this.colDept.DataPropertyName = "PR_DEPT";
            this.colDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDeptContCommand";
            this.colDept.FillWeight = 20F;
            this.colDept.HeaderText = "Dept.";
            this.colDept.LookUpTitle = null;
            this.colDept.LOVFieldMapping = "PR_DEPT";
            this.colDept.Name = "colDept";
            this.colDept.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDept.SearchColumn = "PR_SEGMENT";
            this.colDept.SkipValidationOnLeave = false;
            this.colDept.SpName = "CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER";
            this.colDept.Width = 60;
            // 
            // PR_CONTRIB1
            // 
            this.PR_CONTRIB1.DataPropertyName = "PR_CONTRIB";
            this.PR_CONTRIB1.FillWeight = 20F;
            this.PR_CONTRIB1.HeaderText = "%Contrib.";
            this.PR_CONTRIB1.Name = "PR_CONTRIB1";
            this.PR_CONTRIB1.ToolTipText = "OverAll SUM should >= 1 and <= 100";
            this.PR_CONTRIB1.Width = 60;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlTransfer
            // 
            this.pnlTransfer.ConcurrentPanels = null;
            this.pnlTransfer.Controls.Add(this.label12);
            this.pnlTransfer.Controls.Add(this.label11);
            this.pnlTransfer.Controls.Add(this.label10);
            this.pnlTransfer.Controls.Add(this.txt_PR_TAX_ON_TAX);
            this.pnlTransfer.Controls.Add(this.txt_PR_UTILITIES);
            this.pnlTransfer.Controls.Add(this.txt_PR_RENT);
            this.pnlTransfer.Controls.Add(this.IDHidden);
            this.pnlTransfer.Controls.Add(this.txtPr_no_hidden);
            this.pnlTransfer.Controls.Add(this.lkbtnTBrnch);
            this.pnlTransfer.Controls.Add(this.dtpTEffDate);
            this.pnlTransfer.Controls.Add(this.label27);
            this.pnlTransfer.Controls.Add(this.label20);
            this.pnlTransfer.Controls.Add(this.label19);
            this.pnlTransfer.Controls.Add(this.label13);
            this.pnlTransfer.Controls.Add(this.label9);
            this.pnlTransfer.Controls.Add(this.label8);
            this.pnlTransfer.Controls.Add(this.txtTRemarks);
            this.pnlTransfer.Controls.Add(this.txtTFLCity);
            this.pnlTransfer.Controls.Add(this.txtTISCord);
            this.pnlTransfer.Controls.Add(this.txtTFuncTitle);
            this.pnlTransfer.Controls.Add(this.txtTNewBranch);
            this.pnlTransfer.DataManager = null;
            this.pnlTransfer.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTransfer.DependentPanels = null;
            this.pnlTransfer.DisableDependentLoad = false;
            this.pnlTransfer.EnableDelete = true;
            this.pnlTransfer.EnableInsert = true;
            this.pnlTransfer.EnableQuery = false;
            this.pnlTransfer.EnableUpdate = true;
            this.pnlTransfer.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BranchManagerTransferCommand";
            this.pnlTransfer.Location = new System.Drawing.Point(6, 398);
            this.pnlTransfer.MasterPanel = null;
            this.pnlTransfer.Name = "pnlTransfer";
            this.pnlTransfer.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTransfer.Size = new System.Drawing.Size(443, 192);
            this.pnlTransfer.SPName = "CHRIS_TRANSFER_BRANCHMANAGER_MANAGER";
            this.pnlTransfer.TabIndex = 63;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(282, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 82;
            this.label12.Text = "Tax On Tax :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(306, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 81;
            this.label11.Text = "Utilities :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(318, 92);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "Rent :";
            // 
            // txt_PR_TAX_ON_TAX
            // 
            this.txt_PR_TAX_ON_TAX.AllowSpace = true;
            this.txt_PR_TAX_ON_TAX.AssociatedLookUpName = "";
            this.txt_PR_TAX_ON_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_TAX_ON_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_TAX_ON_TAX.ContinuationTextBox = null;
            this.txt_PR_TAX_ON_TAX.CustomEnabled = true;
            this.txt_PR_TAX_ON_TAX.DataFieldMapping = "PR_TAX_ON_TAX";
            this.txt_PR_TAX_ON_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_TAX_ON_TAX.GetRecordsOnUpDownKeys = false;
            this.txt_PR_TAX_ON_TAX.IsDate = false;
            this.txt_PR_TAX_ON_TAX.Location = new System.Drawing.Point(363, 138);
            this.txt_PR_TAX_ON_TAX.MaxLength = 7;
            this.txt_PR_TAX_ON_TAX.Name = "txt_PR_TAX_ON_TAX";
            this.txt_PR_TAX_ON_TAX.NumberFormat = "###,###,##0.00";
            this.txt_PR_TAX_ON_TAX.Postfix = "";
            this.txt_PR_TAX_ON_TAX.Prefix = "";
            this.txt_PR_TAX_ON_TAX.Size = new System.Drawing.Size(68, 20);
            this.txt_PR_TAX_ON_TAX.SkipValidation = false;
            this.txt_PR_TAX_ON_TAX.TabIndex = 79;
            this.txt_PR_TAX_ON_TAX.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_PR_TAX_ON_TAX.Validated += new System.EventHandler(this.txt_PR_TAX_ON_TAX_Validated);
            // 
            // txt_PR_UTILITIES
            // 
            this.txt_PR_UTILITIES.AllowSpace = true;
            this.txt_PR_UTILITIES.AssociatedLookUpName = "";
            this.txt_PR_UTILITIES.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_UTILITIES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_UTILITIES.ContinuationTextBox = null;
            this.txt_PR_UTILITIES.CustomEnabled = true;
            this.txt_PR_UTILITIES.DataFieldMapping = "PR_UTILITIES";
            this.txt_PR_UTILITIES.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_UTILITIES.GetRecordsOnUpDownKeys = false;
            this.txt_PR_UTILITIES.IsDate = false;
            this.txt_PR_UTILITIES.Location = new System.Drawing.Point(363, 112);
            this.txt_PR_UTILITIES.MaxLength = 7;
            this.txt_PR_UTILITIES.Name = "txt_PR_UTILITIES";
            this.txt_PR_UTILITIES.NumberFormat = "###,###,##0.00";
            this.txt_PR_UTILITIES.Postfix = "";
            this.txt_PR_UTILITIES.Prefix = "";
            this.txt_PR_UTILITIES.Size = new System.Drawing.Size(68, 20);
            this.txt_PR_UTILITIES.SkipValidation = false;
            this.txt_PR_UTILITIES.TabIndex = 78;
            this.txt_PR_UTILITIES.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_UTILITIES.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_PR_RENT
            // 
            this.txt_PR_RENT.AllowSpace = true;
            this.txt_PR_RENT.AssociatedLookUpName = "";
            this.txt_PR_RENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_RENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_RENT.ContinuationTextBox = null;
            this.txt_PR_RENT.CustomEnabled = true;
            this.txt_PR_RENT.DataFieldMapping = "PR_RENT";
            this.txt_PR_RENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_RENT.GetRecordsOnUpDownKeys = false;
            this.txt_PR_RENT.IsDate = false;
            this.txt_PR_RENT.Location = new System.Drawing.Point(363, 86);
            this.txt_PR_RENT.MaxLength = 7;
            this.txt_PR_RENT.Name = "txt_PR_RENT";
            this.txt_PR_RENT.NumberFormat = "###,###,##0.00";
            this.txt_PR_RENT.Postfix = "";
            this.txt_PR_RENT.Prefix = "";
            this.txt_PR_RENT.Size = new System.Drawing.Size(68, 20);
            this.txt_PR_RENT.SkipValidation = false;
            this.txt_PR_RENT.TabIndex = 77;
            this.txt_PR_RENT.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // IDHidden
            // 
            this.IDHidden.AllowSpace = true;
            this.IDHidden.AssociatedLookUpName = "";
            this.IDHidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDHidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.IDHidden.ContinuationTextBox = null;
            this.IDHidden.CustomEnabled = true;
            this.IDHidden.DataFieldMapping = "ID";
            this.IDHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDHidden.GetRecordsOnUpDownKeys = false;
            this.IDHidden.IsDate = false;
            this.IDHidden.Location = new System.Drawing.Point(307, 58);
            this.IDHidden.Name = "IDHidden";
            this.IDHidden.NumberFormat = "###,###,##0.00";
            this.IDHidden.Postfix = "";
            this.IDHidden.Prefix = "";
            this.IDHidden.Size = new System.Drawing.Size(100, 20);
            this.IDHidden.SkipValidation = false;
            this.IDHidden.TabIndex = 76;
            this.IDHidden.TextType = CrplControlLibrary.TextType.String;
            this.IDHidden.Visible = false;
            // 
            // txtPr_no_hidden
            // 
            this.txtPr_no_hidden.AllowSpace = true;
            this.txtPr_no_hidden.AssociatedLookUpName = "";
            this.txtPr_no_hidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPr_no_hidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPr_no_hidden.ContinuationTextBox = null;
            this.txtPr_no_hidden.CustomEnabled = true;
            this.txtPr_no_hidden.DataFieldMapping = "PR_P_NO";
            this.txtPr_no_hidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPr_no_hidden.GetRecordsOnUpDownKeys = false;
            this.txtPr_no_hidden.IsDate = false;
            this.txtPr_no_hidden.Location = new System.Drawing.Point(307, 29);
            this.txtPr_no_hidden.Name = "txtPr_no_hidden";
            this.txtPr_no_hidden.NumberFormat = "###,###,##0.00";
            this.txtPr_no_hidden.Postfix = "";
            this.txtPr_no_hidden.Prefix = "";
            this.txtPr_no_hidden.Size = new System.Drawing.Size(100, 20);
            this.txtPr_no_hidden.SkipValidation = false;
            this.txtPr_no_hidden.TabIndex = 64;
            this.txtPr_no_hidden.TextType = CrplControlLibrary.TextType.String;
            this.txtPr_no_hidden.Visible = false;
            // 
            // lkbtnTBrnch
            // 
            this.lkbtnTBrnch.ActionLOVExists = "BRN_LOVEXISTS";
            this.lkbtnTBrnch.ActionType = "BRN_LOV";
            this.lkbtnTBrnch.ConditionalFields = "";
            this.lkbtnTBrnch.CustomEnabled = true;
            this.lkbtnTBrnch.DataFieldMapping = "";
            this.lkbtnTBrnch.DependentLovControls = "";
            this.lkbtnTBrnch.HiddenColumns = "";
            this.lkbtnTBrnch.Image = ((System.Drawing.Image)(resources.GetObject("lkbtnTBrnch.Image")));
            this.lkbtnTBrnch.LoadDependentEntities = false;
            this.lkbtnTBrnch.Location = new System.Drawing.Point(242, 32);
            this.lkbtnTBrnch.LookUpTitle = null;
            this.lkbtnTBrnch.Name = "lkbtnTBrnch";
            this.lkbtnTBrnch.Size = new System.Drawing.Size(26, 21);
            this.lkbtnTBrnch.SkipValidationOnLeave = false;
            this.lkbtnTBrnch.SPName = "CHRIS_TRANSFER_BRANCHMANAGER_MANAGER";
            this.lkbtnTBrnch.TabIndex = 3;
            this.lkbtnTBrnch.TabStop = false;
            this.lkbtnTBrnch.UseVisualStyleBackColor = true;
            // 
            // dtpTEffDate
            // 
            this.dtpTEffDate.CustomEnabled = true;
            this.dtpTEffDate.CustomFormat = "dd/MM/yyyy";
            this.dtpTEffDate.DataFieldMapping = "PR_EFFECTIVE";
            this.dtpTEffDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTEffDate.HasChanges = true;
            this.dtpTEffDate.IsRequired = true;
            this.dtpTEffDate.Location = new System.Drawing.Point(134, 140);
            this.dtpTEffDate.Name = "dtpTEffDate";
            this.dtpTEffDate.NullValue = " ";
            this.dtpTEffDate.Size = new System.Drawing.Size(145, 20);
            this.dtpTEffDate.TabIndex = 7;
            this.dtpTEffDate.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            this.dtpTEffDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpTEffDate_Validating);
            this.dtpTEffDate.Leave += new System.EventHandler(this.dtpTEffDate_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(15, 92);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(118, 13);
            this.label27.TabIndex = 75;
            this.label27.Text = "2. Functional Title :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(53, 171);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 13);
            this.label20.TabIndex = 74;
            this.label20.Text = "4. Remarks :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(24, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 13);
            this.label19.TabIndex = 73;
            this.label19.Text = "ForLough City :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 144);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 72;
            this.label13.Text = "3. Effective Date :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(33, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 71;
            this.label9.Text = "1. New Branch :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(180, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Transfer      Information";
            // 
            // txtTRemarks
            // 
            this.txtTRemarks.AllowSpace = true;
            this.txtTRemarks.AssociatedLookUpName = "";
            this.txtTRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTRemarks.ContinuationTextBox = null;
            this.txtTRemarks.CustomEnabled = true;
            this.txtTRemarks.DataFieldMapping = "PR_REMARKS";
            this.txtTRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTRemarks.GetRecordsOnUpDownKeys = false;
            this.txtTRemarks.IsDate = false;
            this.txtTRemarks.Location = new System.Drawing.Point(134, 167);
            this.txtTRemarks.MaxLength = 30;
            this.txtTRemarks.Name = "txtTRemarks";
            this.txtTRemarks.NumberFormat = "###,###,##0.00";
            this.txtTRemarks.Postfix = "";
            this.txtTRemarks.Prefix = "";
            this.txtTRemarks.Size = new System.Drawing.Size(293, 20);
            this.txtTRemarks.SkipValidation = false;
            this.txtTRemarks.TabIndex = 66;
            this.txtTRemarks.TextType = CrplControlLibrary.TextType.String;
            this.txtTRemarks.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtTRemarks_PreviewKeyDown);
            this.txtTRemarks.Leave += new System.EventHandler(this.txtTRemarks_Leave);
            this.txtTRemarks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTRemarks_KeyPress);
            // 
            // txtTFLCity
            // 
            this.txtTFLCity.AllowSpace = true;
            this.txtTFLCity.AssociatedLookUpName = "";
            this.txtTFLCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTFLCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTFLCity.ContinuationTextBox = null;
            this.txtTFLCity.CustomEnabled = true;
            this.txtTFLCity.DataFieldMapping = "PR_FURLOUGH";
            this.txtTFLCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTFLCity.GetRecordsOnUpDownKeys = false;
            this.txtTFLCity.IsDate = false;
            this.txtTFLCity.Location = new System.Drawing.Point(134, 59);
            this.txtTFLCity.MaxLength = 15;
            this.txtTFLCity.Name = "txtTFLCity";
            this.txtTFLCity.NumberFormat = "###,###,##0.00";
            this.txtTFLCity.Postfix = "";
            this.txtTFLCity.Prefix = "";
            this.txtTFLCity.Size = new System.Drawing.Size(145, 20);
            this.txtTFLCity.SkipValidation = false;
            this.txtTFLCity.TabIndex = 4;
            this.txtTFLCity.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTISCord
            // 
            this.txtTISCord.AllowSpace = true;
            this.txtTISCord.AssociatedLookUpName = "";
            this.txtTISCord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTISCord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTISCord.ContinuationTextBox = null;
            this.txtTISCord.CustomEnabled = true;
            this.txtTISCord.DataFieldMapping = "PR_FUNC_2";
            this.txtTISCord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTISCord.GetRecordsOnUpDownKeys = false;
            this.txtTISCord.IsDate = false;
            this.txtTISCord.Location = new System.Drawing.Point(134, 113);
            this.txtTISCord.MaxLength = 30;
            this.txtTISCord.Name = "txtTISCord";
            this.txtTISCord.NumberFormat = "###,###,##0.00";
            this.txtTISCord.Postfix = "";
            this.txtTISCord.Prefix = "";
            this.txtTISCord.Size = new System.Drawing.Size(145, 20);
            this.txtTISCord.SkipValidation = false;
            this.txtTISCord.TabIndex = 6;
            this.txtTISCord.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTFuncTitle
            // 
            this.txtTFuncTitle.AllowSpace = true;
            this.txtTFuncTitle.AssociatedLookUpName = "";
            this.txtTFuncTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTFuncTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTFuncTitle.ContinuationTextBox = null;
            this.txtTFuncTitle.CustomEnabled = true;
            this.txtTFuncTitle.DataFieldMapping = "PR_FUNC_1";
            this.txtTFuncTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTFuncTitle.GetRecordsOnUpDownKeys = false;
            this.txtTFuncTitle.IsDate = false;
            this.txtTFuncTitle.Location = new System.Drawing.Point(134, 86);
            this.txtTFuncTitle.MaxLength = 30;
            this.txtTFuncTitle.Name = "txtTFuncTitle";
            this.txtTFuncTitle.NumberFormat = "###,###,##0.00";
            this.txtTFuncTitle.Postfix = "";
            this.txtTFuncTitle.Prefix = "";
            this.txtTFuncTitle.Size = new System.Drawing.Size(145, 20);
            this.txtTFuncTitle.SkipValidation = false;
            this.txtTFuncTitle.TabIndex = 5;
            this.txtTFuncTitle.TextType = CrplControlLibrary.TextType.String;
            this.txtTFuncTitle.Validated += new System.EventHandler(this.txtTFuncTitle_Validated);
            // 
            // txtTNewBranch
            // 
            this.txtTNewBranch.AllowSpace = true;
            this.txtTNewBranch.AssociatedLookUpName = "lkbtnTBrnch";
            this.txtTNewBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTNewBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTNewBranch.ContinuationTextBox = null;
            this.txtTNewBranch.CustomEnabled = true;
            this.txtTNewBranch.DataFieldMapping = "PR_NEW_BRANCH";
            this.txtTNewBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTNewBranch.GetRecordsOnUpDownKeys = false;
            this.txtTNewBranch.IsDate = false;
            this.txtTNewBranch.IsRequired = true;
            this.txtTNewBranch.Location = new System.Drawing.Point(134, 32);
            this.txtTNewBranch.Name = "txtTNewBranch";
            this.txtTNewBranch.NumberFormat = "###,###,##0.00";
            this.txtTNewBranch.Postfix = "";
            this.txtTNewBranch.Prefix = "";
            this.txtTNewBranch.Size = new System.Drawing.Size(100, 20);
            this.txtTNewBranch.SkipValidation = false;
            this.txtTNewBranch.TabIndex = 1;
            this.txtTNewBranch.TextType = CrplControlLibrary.TextType.String;
            this.txtTNewBranch.TextChanged += new System.EventHandler(this.txtTNewBranch_TextChanged);
            // 
            // CHRIS_Personnel_BranchManagerTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(674, 655);
            this.Controls.Add(this.pnlTransfer);
            this.Controls.Add(this.pnlTSDC);
            this.Controls.Add(this.pnlSDC);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPersonnel);
            this.CurrentPanelBlock = "pnlTransfer";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_BranchManagerTransfer";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Branch ManagerTransfer";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_BranchManagerTransfer_Shown);
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_BranchManagerTransfer_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.pnlSDC, 0);
            this.Controls.SetChildIndex(this.pnlTSDC, 0);
            this.Controls.SetChildIndex(this.pnlTransfer, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            this.PnlPersonnel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSDC)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlSDC.ResumeLayout(false);
            this.pnlTSDC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTSDC)).EndInit();
            this.pnlTransfer.ResumeLayout(false);
            this.pnlTransfer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPersonnel;
        private CrplControlLibrary.SLTextBox txtFunctional;
        private CrplControlLibrary.SLTextBox txtTitle;
        private CrplControlLibrary.LookupButton lbpersonnel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtTType;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtLastName;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtPersonnelNO;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label lblSubHeader;
        private System.Windows.Forms.Label lblHeader;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtBranch;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtTypeOf;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvSDC;
        private CrplControlLibrary.LookupButton lkbtnDesc;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlSDC;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTSDC;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlTransfer;
        private CrplControlLibrary.LookupButton lkbtnTBrnch;
        private CrplControlLibrary.SLDatePicker dtpTEffDate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtTRemarks;
        private CrplControlLibrary.SLTextBox txtTFLCity;
        private CrplControlLibrary.SLTextBox txtTISCord;
        private CrplControlLibrary.SLTextBox txtTFuncTitle;
        private CrplControlLibrary.SLTextBox txtTNewBranch;
        private CrplControlLibrary.SLTextBox txtTempOption;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvTSDC;
        private CrplControlLibrary.SLTextBox txtPr_no_hidden;
        private CrplControlLibrary.SLTextBox IDHiddenPrsonl;
        private CrplControlLibrary.SLTextBox IDHidden;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_SEGMENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DEPT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONTRIB;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeg;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn colDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONTRIB1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private CrplControlLibrary.SLTextBox txt_PR_TAX_ON_TAX;
        private CrplControlLibrary.SLTextBox txt_PR_UTILITIES;
        private CrplControlLibrary.SLTextBox txt_PR_RENT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
    }
}