using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS : MasterDetailForm
    {
        #region --Variable--
        int NoOfChild;
        private DataTable dtPers;
        private DataTable dtDept;
        private DataTable dtEmp;
        private DataTable dtRef;
        private DataTable dtChild;
        private DataTable dtEdu;
        private CHRIS_Personnel_RegularStaffHiringEnt _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU frm_Edu;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKREF frm_Ref;

        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP frm_Emp;
        bool FromValidate = true;
        #endregion

        #region --Constructor--
        public CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS()
        {
            InitializeComponent();
        }


        public CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_RegularStaffHiringEnt mainForm,
            CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU BLKEDU, CHRIS_Personnel_RegularStaffHiringEnt_BLKREF BLKREF, DataTable _dtDept, DataTable _dtEmp, DataTable _dtRef, DataTable _dtChild, DataTable _dtEdu)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtDept          = _dtDept;
            dtEmp           = _dtEmp;
            dtRef           = _dtRef;
            dtChild         = _dtChild;
            dtEdu           = _dtEdu;
            this._mainForm  = mainForm;
            frm_Edu = BLKEDU;
            frm_Ref = BLKREF;
        }
        #endregion

        #region --Method--

        /// <summary>
        /// Load the grid with the DataTable coming from MAIN From.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                UpdatePopUPForm();
                if (frm_Edu != null)
                    frm_Edu.Hide();
                else if (frm_Ref != null)
                    frm_Ref.Hide();

                dgvBlkChild.GridSource = dtChild;
                dgvBlkChild.DataSource = dgvBlkChild.GridSource;

                //if (dtChild.Rows.Count == 0)
                //{
                //    txt_PR_BIRTH_SP.Value = null;
                //    txt_PR_D_BIRTH.Value  = null;
                //    txt_PR_MARRIAGE.Value = null;
                //}


                if (_mainForm.txt_PR_D_BIRTH.Text == null)
                {
                    txt_PR_D_BIRTH.Value = null;
                }
                if (_mainForm.txt_PR_BIRTH_SP.Text == "")
                {
                    txt_PR_BIRTH_SP.Value = null;
                }
                if (_mainForm.txt_PR_MARRIAGE.Text == "")
                {
                    txt_PR_MARRIAGE.Value = null;
                }
                dgvBlkChild.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
                this.KeyPreview = true;
                this.KeyDown    += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        /// <summary>
        /// Update Main From with PopUP From Values
        /// </summary>
        private void UpdateMainForm()
        {
            try
            {
                _mainForm.txt_PR_ADD1.Text          = txt_PR_ADD1.Text;
                _mainForm.txt_PR_ADD2.Text          = txt_PR_ADD2.Text;
                _mainForm.txt_PR_PHONE1.Text        = txt_PR_PHONE1.Text;
                _mainForm.txt_PR_PHONE2.Text        = txt_PR_PHONE2.Text;
                
                if (txt_PR_D_BIRTH.Value != null)
                    _mainForm.txt_PR_D_BIRTH.Text   = txt_PR_D_BIRTH.Value.ToString();
                
                _mainForm.txt_PR_SEX.Text           = txt_PR_SEX.Text;
                _mainForm.txt_PR_ID_CARD_NO.Text    = txt_PR_ID_CARD_NO.Text;
                _mainForm.txt_PR_OLD_ID_CARD_NO.Text = txt_PR_OLD_ID_CARD_NO.Text;
                _mainForm.txt_PR_MARITAL.Text   = txt_PR_MARITAL.Text;

                if (txt_PR_MARRIAGE.Value != null)
                    _mainForm.txt_PR_MARRIAGE.Text  = txt_PR_MARRIAGE.Value.ToString();

                _mainForm.txt_PR_SPOUSE.Text        = txt_PR_SPOUSE.Text;

                if (txt_PR_BIRTH_SP.Value != null)
                    _mainForm.txt_PR_BIRTH_SP.Text  = (txt_PR_BIRTH_SP.Value.ToString());

                _mainForm.txt_PR_NO_OF_CHILD.Text   = txt_PR_NO_OF_CHILD.Text;
                _mainForm.txt_PR_LANG_1.Text        = txt_PR_LANG1.Text;
                _mainForm.txt_PR_LANG_2.Text        = txt_PR_LANG2.Text;
                _mainForm.txt_PR_LANG_3.Text        = txt_PR_LANG3.Text;
                _mainForm.txt_PR_LANG_4.Text        = txt_PR_LANG4.Text;
                _mainForm.txt_PR_LANG_5.Text        = txt_PR_LANG5.Text;
                _mainForm.txt_PR_LANG_6.Text        = txt_PR_LANG6.Text;
                _mainForm.txt_w_date_2.Text         = txt_w_date_2.Text;
                _mainForm.txt_PR_GROUP_LIFE.Text    = txt_PR_GROUP_LIFE.Text;
                _mainForm.txt_PR_OPD.Text           = txt_PR_OPD.Text;
                _mainForm.txt_PR_GROUP_HOSP.Text    = txt_PR_GROUP_HOSP.Text;
                _mainForm.txt_PR_USER_IS_PRIME.Text = txt_PR_USER_IS_PRIME.Text;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "UpdateMainForm", exp);
            }
        }

        /// <summary>
        /// Fill PopUP with Value from Main From
        /// </summary>
        private void UpdatePopUPForm()
        {
            try
            {
                txt_PR_ADD1.Text    = _mainForm.txt_PR_ADD1.Text;
                txt_PR_ADD2.Text    = _mainForm.txt_PR_ADD2.Text;
                txt_PR_PHONE1.Text  = _mainForm.txt_PR_PHONE1.Text;
                txt_PR_PHONE2.Text  = _mainForm.txt_PR_PHONE2.Text;
                
                if (_mainForm.txt_PR_D_BIRTH.Text.Trim() != string.Empty)
                    txt_PR_D_BIRTH.Value = Convert.ToDateTime(_mainForm.txt_PR_D_BIRTH.Text);

                txt_PR_SEX.Text         = _mainForm.txt_PR_SEX.Text;
                txt_PR_ID_CARD_NO.Text  = _mainForm.txt_PR_ID_CARD_NO.Text;
                txt_PR_OLD_ID_CARD_NO.Text = _mainForm.txt_PR_OLD_ID_CARD_NO.Text;
                txt_PR_MARITAL.Text     = _mainForm.txt_PR_MARITAL.Text;
                
                if (_mainForm.txt_PR_MARRIAGE.Text.Trim() != string.Empty)
                    txt_PR_MARRIAGE.Value= Convert.ToDateTime(_mainForm.txt_PR_MARRIAGE.Text);
                
                txt_PR_SPOUSE.Text      = _mainForm.txt_PR_SPOUSE.Text;
                
                if (_mainForm.txt_PR_BIRTH_SP.Text.Trim()  != string.Empty)
                    txt_PR_BIRTH_SP.Value = Convert.ToDateTime(_mainForm.txt_PR_BIRTH_SP.Text);
                
                txt_PR_NO_OF_CHILD.Text = _mainForm.txt_PR_NO_OF_CHILD.Text;
                txt_PR_LANG1.Text       = _mainForm.txt_PR_LANG_1.Text;
                txt_PR_LANG2.Text       = _mainForm.txt_PR_LANG_2.Text;
                txt_PR_LANG3.Text       = _mainForm.txt_PR_LANG_3.Text;
                txt_PR_LANG4.Text       = _mainForm.txt_PR_LANG_4.Text;
                txt_PR_LANG5.Text       = _mainForm.txt_PR_LANG_5.Text;
                txt_PR_LANG6.Text       = _mainForm.txt_PR_LANG_6.Text;
                txt_PR_GROUP_HOSP.Text  = _mainForm.txt_PR_GROUP_HOSP.Text;
                txt_PR_GROUP_LIFE.Text  = _mainForm.txt_PR_GROUP_LIFE.Text;
                txt_PR_USER_IS_PRIME.Text = _mainForm.txt_PR_USER_IS_PRIME.Text;
                txt_w_date_2.Text       = _mainForm.txt_w_date_2.Text;
                txt_PR_GROUP_LIFE.Text  = _mainForm.txt_PR_GROUP_LIFE.Text;
                txt_PR_OPD.Text         = _mainForm.txt_PR_OPD.Text;
                txt_PR_GROUP_HOSP.Text  = _mainForm.txt_PR_GROUP_HOSP.Text;
                txt_PR_USER_IS_PRIME.Text = _mainForm.txt_PR_USER_IS_PRIME.Text;


            }
            catch (Exception exp)
            {
                LogException(this.Name, "UpdatePopUPForm", exp);
            }
        }

        #endregion


        #region --EVENT--

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible     = false;
                this.tbtList.Visible    = false;
                this.tbtSave.Visible    = false;
                this.tbtCancel.Visible  = false;
                this.tlbMain.Visible    = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        /// <summary>
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (FromValidate)
                {
                    if (e.Modifiers == Keys.Control)
                    {
                        switch (e.KeyValue)
                        {
                            case 34:
                                UpdateMainForm();

                                frm_Edu             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this, null, dtEmp, null, dtChild, dtEdu); //dtDept  //dtRef
                                frm_Edu.MdiParent   = null;
                                frm_Edu.Enabled     = true; 
                                

                                dgvBlkChild.GridSource = dtChild;
                                //dgvBlkChild.DataSource = dgvBlkChild.GridSource;
                                frm_Edu.ShowDialog(this);
                                this.Hide();
                                //_mainForm.strCurrentPopUP = "BLKEMP";
                                //this.Close();

                                this.KeyPreview = true;
                                break;

                            case 33:
                                UpdateMainForm();

                            //frm_Ref             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKREF(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this ,null, null, dtEmp, null, dtChild, dtEdu); //dtDept //dtRef
                            //frm_Ref.MdiParent   = null;
                            //frm_Ref.Enabled     = true; 
                            //this.Hide();
                            //frm_Ref.ShowDialog(this);
                            ////this.Close();

                            //this.KeyPreview     = true;
                            //break;

                                frm_Emp = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, null, null, dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                frm_Emp.MdiParent = null;
                                frm_Emp.Enabled = true;
                                this.Hide();
                                frm_Emp.ShowDialog(this);
                                //this.Close();
                                this.KeyPreview = true;
                                break;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }

        /// <summary>
        /// Check if the Mode is Not View than:  Set the w_date_2 to current Datetime value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlSplBlkPersMain_Leave(object sender, EventArgs e)
        {
            if (_mainForm.txtOption.Text != "V")
            {
                txt_w_date_2.Text = DateTime.Now.ToShortDateString().ToString();
            }
        }

        /// <summary>
        /// Check if both phone numbers are same than: Show POPUP ...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_PHONE2_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;

            if (txt_PR_PHONE1.Text == txt_PR_PHONE2.Text && (txt_PR_PHONE1.Text != string.Empty && txt_PR_PHONE2.Text != string.Empty))
            {
                MessageBox.Show("Two Telephone numbers can not be same","Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FromValidate = false;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// DOB Validatioin
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_D_BIRTH_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                bool istrueBirthDate    = false;
                bool istrueJoiningDate  = false;
                FromValidate            = true;

                DateTime BirthDate;
                DateTime JoiningDate;
                int MonthBetween    = 0;
                if (txt_PR_D_BIRTH.Value != null)
                {
                    istrueBirthDate = IsValidExpression(txt_PR_D_BIRTH.Value.ToString(), InputValidator.DATETIME_REGEX);
                    istrueJoiningDate = IsValidExpression(_mainForm.txt_PR_JOINING_DATE.Value.ToString(), InputValidator.DATETIME_REGEX);

                    if (istrueBirthDate == true)
                    {
                        BirthDate       = DateTime.Parse(txt_PR_D_BIRTH.Value.ToString());
                        JoiningDate     = DateTime.Parse(_mainForm.txt_PR_JOINING_DATE.Value.ToString());
                        MonthBetween    = MonthsBetweenInOracle(JoiningDate, BirthDate);
                        if (DateTime.Compare(Convert.ToDateTime(txt_PR_D_BIRTH.Value), Convert.ToDateTime(_mainForm.txt_PR_JOINING_DATE.Value)) > 0)
                        {

                            MessageBox.Show("Birth Date Can Not Be Greater Then Joining Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FromValidate = false;
                            e.Cancel = true;
                            return;
                        }
                        else if (MonthBetween <= 180)
                        {
                            MessageBox.Show("Age is Less then 15 Years .......!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FromValidate = false;
                            e.Cancel = true;
                            return;
                        }

                        if (DateTime.Compare(Convert.ToDateTime(txt_PR_D_BIRTH.Value), Convert.ToDateTime(_mainForm.txtDate.Text)) >= 0)
                        {
                            MessageBox.Show("Date of Birth can not be greater then Today`s Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FromValidate = false;
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Date is Not in Correct Format", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FromValidate = false;
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    FromValidate = false;
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_D_BIRTH_Validating", exp);
            }
        }

        /// <summary>
        /// Check either Sex is M or F set the Focus on Current Textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_SEX_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;

            if (txt_PR_SEX.Text != "M" && txt_PR_SEX.Text != "F")
            {
                MessageBox.Show("Enter [M] or [F]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FromValidate = false;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Material Status Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_MARITAL_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                FromValidate = true;

                if (txt_PR_MARITAL.Text != "M" && txt_PR_MARITAL.Text != "S" && txt_PR_MARITAL.Text != "D" && txt_PR_MARITAL.Text != "W")
                {
                    MessageBox.Show("Enter [M],[S], [D] or [W]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FromValidate = false;
                    e.Cancel = true;
                }
                else if (txt_PR_MARITAL.Text == "M")
                {
                    txt_PR_MARRIAGE.Select();
                    txt_PR_MARRIAGE.Focus();
                    txt_PR_MARRIAGE.Enabled     = true;
                    txt_PR_SPOUSE.Enabled       = true;
                    txt_PR_BIRTH_SP.Enabled     = true;
                    txt_PR_NO_OF_CHILD.Enabled  = true;
                }
                else if (txt_PR_MARITAL.Text == "S")
                {
                    txt_PR_SPOUSE.Text      = null;
                    txt_PR_BIRTH_SP.Value   = null;
                    txt_PR_MARRIAGE.Value   = null;
                    txt_PR_NO_OF_CHILD.Text = null;

                    txt_PR_MARRIAGE.Enabled = false;
                    txt_PR_SPOUSE.Enabled   = false;
                    txt_PR_BIRTH_SP.Enabled = false;
                    txt_PR_NO_OF_CHILD.Enabled = false;


                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_P_NO", _mainForm.txt_PR_P_NO.Text);
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.Execute("CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER", "DeleteChild", param);

                    dgvBlkChild.DataSource  = null;
                    dgvBlkChild.Enabled     = false;

                    txt_PR_GROUP_LIFE.Select();
                    txt_PR_GROUP_LIFE.Focus();
                }
                else if (txt_PR_MARITAL.Text == "D" || txt_PR_MARITAL.Text == "W")
                {
                    txt_PR_SPOUSE.Text      = null;
                    txt_PR_BIRTH_SP.Value   = null;
                    txt_PR_MARRIAGE.Value   = null;

                    txt_PR_MARRIAGE.Enabled = false;
                    txt_PR_SPOUSE.Enabled   = false;
                    txt_PR_BIRTH_SP.Enabled = false;
                    txt_PR_NO_OF_CHILD.Enabled = false;
                    txt_PR_GROUP_LIFE.Select();
                    txt_PR_GROUP_LIFE.Focus();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_MARITAL_Validating", exp);
            }
        }

        /// <summary>
        /// Validate if pr_marital = "M" then Spouse can not be null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_SPOUSE_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;

            if (txt_PR_SPOUSE.Text == string.Empty && txt_PR_MARITAL.Text == "M")
            {
                FromValidate    = false;
                e.Cancel        = true;
            }
        }

        /// <summary>
        /// Check spouse Birth Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_BIRTH_SP_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                bool ValidSpDOB = true;
                int MonthsBetween;
                if (txt_PR_BIRTH_SP.Value == null)
                {
                    MessageBox.Show("Please Enter Data Of Birth .....!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }

                ValidSpDOB = IsValidExpression(txt_PR_BIRTH_SP.Value.ToString(), InputValidator.DATETIME_REGEX);

                if (ValidSpDOB)
                {
                    DateTime w_date = Convert.ToDateTime(_mainForm.txtDate.Text);

                    MonthsBetween = MonthsBetweenInOracle(w_date, Convert.ToDateTime(txt_PR_BIRTH_SP.Value));

                    //if ((12 * (Convert.ToDateTime(txt_w_date_2.Text).Year - Convert.ToDateTime(txt_PR_BIRTH_SP.Value).Year) + Convert.ToDateTime(txt_w_date_2.Text).Month - Convert.ToDateTime(txt_PR_BIRTH_SP.Value).Month) <= 108)
                    if (MonthsBetween <= 108)
                    {
                        MessageBox.Show("Age is Less then 10 Years .......!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }

                    if (DateTime.Compare(Convert.ToDateTime(txt_PR_BIRTH_SP.Value), Convert.ToDateTime(_mainForm.txtDate.Text)) >= 0)
                    {
                        MessageBox.Show("Date of Birth can not be greater then Today`s Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_PR_BIRTH_SP_Validating", exp);
            }
        }

        /// <summary>
        /// VAlidating if Group_life if Y/N else popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_GROUP_LIFE_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;
            if (txt_PR_GROUP_LIFE.Text != "Y" && txt_PR_GROUP_LIFE.Text != "N")
            {
                MessageBox.Show("Enter [Y] or [N]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FromValidate = false;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// VAlidating if PR_OPD if Y/N else popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_OPD_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;
            if (txt_PR_OPD.Text != "Y" && txt_PR_OPD.Text != "N")
            {
                MessageBox.Show("Enter [Y] or [N]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FromValidate = false;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// VAlidating if PR_GROUP_HOSP if Y/N else popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_GROUP_HOSP_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;
            if (txt_PR_GROUP_HOSP.Text != "Y" && txt_PR_GROUP_HOSP.Text != "N")
            {
                MessageBox.Show("Enter [Y] or [N]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FromValidate = false;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// PR_USER_IS_PRIME KeyPress Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_PR_USER_IS_PRIME_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txt_PR_MARITAL.Text == "M")
            {
                //dgvBlkChild.Select();
                //dgvBlkChild.Focus();
            }
            else
            {
                if (_mainForm.txtOption.Text == "M")
                {
                    if (e.KeyChar == '\t')
                    {
                        dgvBlkChild.Select();
                        dgvBlkChild.Focus();
                    }
                }
                else
                {
                    //go_block('blkedu');
                }
            }

        }

        /// <summary>
        /// dgvBlkChild Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkChild_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //dgvBlkChild.EndEdit();
                bool ChildDOB = true;
                FromValidate = true;
                if (txt_PR_MARITAL.Text != "S")
                {
                    if (dgvBlkChild.CurrentCell.OwningColumn.Name == "ChildName")
                    {
                        if (dgvBlkChild.CurrentCell.EditedFormattedValue == null)
                        {
                            FromValidate = false;
                            e.Cancel = true;
                        }
                        else
                        {
                            if (_mainForm.txtOption.Text == "A")
                            {
                                if (dgvBlkChild.CurrentCell.EditedFormattedValue.ToString() != "")
                                {
                                    if (NoOfChild == Convert.ToInt32(txt_PR_NO_OF_CHILD.Text == "" ? "0" : txt_PR_NO_OF_CHILD.Text))
                                    {
                                        NoOfChild = dgvBlkChild.Rows.Count - 1;
                                        txt_PR_NO_OF_CHILD.Text = NoOfChild.ToString();
                                    }
                                }
                            }
                        }
                    }
                    else if (dgvBlkChild.CurrentCell.OwningColumn.Name == "Date_of_Birth")
                    {
                        if (dgvBlkChild.CurrentCell.EditedFormattedValue.ToString() != "")
                        {
                            ChildDOB = IsValidExpression(dgvBlkChild.CurrentCell.EditedFormattedValue.ToString(), InputValidator.DATE_REGEX);
                            int MonthsBetween;
                            if (ChildDOB)
                            {
                                /*Find the age Difference between mother and Child: Should be less than 10 years*/
                                MonthsBetween = MonthsBetweenInOracle(Convert.ToDateTime(dgvBlkChild.CurrentCell.EditedFormattedValue.ToString()), Convert.ToDateTime(txt_PR_BIRTH_SP.Value.ToString()));

                                if (DateTime.Compare(Convert.ToDateTime(dgvBlkChild.CurrentCell.EditedFormattedValue.ToString()), Convert.ToDateTime(txt_PR_BIRTH_SP.Value)) <= 0)
                                {
                                    MessageBox.Show("Child Date Of Birth Is Less Then Mother`s Date Of Birth", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    FromValidate = false;
                                    e.Cancel = true;
                                    return;
                                }
                                else if (MonthsBetween <= 108)
                                {
                                    MessageBox.Show("Age Difference From Mother Age is Less then 10 Years ...!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    FromValidate = false;
                                    e.Cancel = true;
                                    return;
                                }

                                if (DateTime.Compare(Convert.ToDateTime(dgvBlkChild.CurrentCell.EditedFormattedValue.ToString()), Convert.ToDateTime(_mainForm.txtDate.Text)) >= 0)
                                {
                                    MessageBox.Show("Date Of Birth Can Not Be Greater Then Today`s Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    FromValidate = false;
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvBlkChild_CellValidating", exp);
            }
        }

        private void dgvBlkChild_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        private void txt_PR_ID_CARD_NO_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;

            if (txt_PR_ID_CARD_NO.Text != string.Empty)
            {

                if (txt_PR_ID_CARD_NO.Text.Length == 13)
                {
                    string Nic = txt_PR_ID_CARD_NO.Text.Insert(5, "-");
                    Nic = Nic.Insert(13, "-");
                    txt_PR_ID_CARD_NO.Text = Nic;
                }

                bool ValidCNIC = IsValidExpression(txt_PR_ID_CARD_NO.Text, InputValidator.NICNO_REGEX);

                if (!ValidCNIC)
                {
                    FromValidate = false;
                    e.Cancel = true;
                }
            }
        }

        private void txt_PR_OLD_ID_CARD_NO_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;

            if (txt_PR_OLD_ID_CARD_NO.Text != string.Empty)
            {

                if (txt_PR_OLD_ID_CARD_NO.Text.Length == 11)
                {
                    string OldNic = txt_PR_OLD_ID_CARD_NO.Text.Insert(3, "-");
                    OldNic = OldNic.Insert(6, "-");
                    txt_PR_OLD_ID_CARD_NO.Text = OldNic;
                }


                bool ValidOldNIC = IsValidExpression(txt_PR_OLD_ID_CARD_NO.Text, InputValidator.OldNICNO_REGEX);

                if (!ValidOldNIC)
                {
                    FromValidate = false;
                    e.Cancel = true;
                }
            }
        }

        private void txt_PR_ADD1_Validating(object sender, CancelEventArgs e)
        {
            FromValidate = true;

            if (txt_PR_ADD1.Text == string.Empty)
            {
                FromValidate = false;
                e.Cancel = true;
            }
        }

        private void txt_PR_USER_IS_PRIME_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (txt_PR_MARITAL.Text == "S")
            {
                if (e.KeyCode == Keys.Tab)
                {
                    if (FromValidate)
                    {
                        UpdateMainForm();
                        frm_Edu = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this, dtDept, dtEmp, dtRef, dtChild, dtEdu);
                        frm_Edu.MdiParent = null;
                        this.Hide();
                        dgvBlkChild.GridSource = dtChild;
                        frm_Edu.ShowDialog(this);
                        this.KeyPreview = true;
                    }
                }
            }
            else
            {
                if (e.KeyCode == Keys.Tab)
                {
                    dgvBlkChild.Focus();
                    dgvBlkChild.Select();
                    dgvBlkChild.BeginEdit(true);
                    dgvBlkChild.CurrentCell = dgvBlkChild[dgvBlkChild.FirstDisplayedCell.ColumnIndex, 0];
                    dgvBlkChild.BeginEdit(true);
                    dgvBlkChild.Focus();
                    dgvBlkChild.Select();

                }
            }
        }
        
        #endregion

        private void txt_PR_ID_CARD_NO_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txt_PR_ID_CARD_NO.Text.Contains("-"))
                {
                    string NIC = txt_PR_ID_CARD_NO.Text.Replace("-", "");
                    txt_PR_ID_CARD_NO.Text = NIC;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_PR_ID_CARD_NO_Enter", exp);
            }
        }

        private void txt_PR_OLD_ID_CARD_NO_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txt_PR_OLD_ID_CARD_NO.Text.Contains("-"))
                {
                    string OldNIC               = txt_PR_OLD_ID_CARD_NO.Text.Replace("-", "");
                    txt_PR_OLD_ID_CARD_NO.Text  = OldNIC;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_PR_OLD_ID_CARD_NO_Enter", exp);
            }
        }
    }
}