using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_SegmtOrDeptTrans : ChrisMasterDetailForm
    {
        
        #region Constructor 

        public CHRIS_Personnel_SegmtOrDeptTrans()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_SegmtOrDeptTrans(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlTblDept);
            this.pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);



        }

        #endregion

        #region Declaration
        int Sum1 = 0;
        int dCount = 0;
        bool flag = false;
        bool LastCellValidate = false;
        string pr_SegmentValue = string.Empty;
        #endregion

        #region Methods
        protected override bool Add()
        {
            base.Add();
            txtMode.Text = "A";
            return false;
        }

        protected override bool Edit()
        {
            base.Edit();
            txtMode.Text = "M";
            return false;
        }
        public void AddMoreRecords()
        {
           // this.Cancel();
            this.DoToolbarActions(this.pnlTblDept.Controls, "Cancel");
            this.DoToolbarActions(this.pnlTblDept2.Controls, "Cancel");
            this.Add();
            this.txtOption.Text = "A";
            EnableDisableControls(true);
            txtPersNo.Select();
            txtPersNo.Focus();
        }
        public void ModifyMoreRecords()
        {
            this.DoToolbarActions(this.pnlTblDept.Controls, "Cancel");
            this.DoToolbarActions(this.pnlTblDept2.Controls, "Cancel");
            this.Edit();
            this.txtOption.Text = "M";
            EnableDisableControls(true);
            txtPersNo.Select();
            txtPersNo.Focus();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
        }
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                this.txtUser.Text = this.userID;
                this.txtDate.Text = this.Now().ToString("MM/dd/yyyy");

                this.Dept.AttachParentEntity = true;
                this.Dept.SearchColumn = "Seg";

                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;            
                this.tbtSave.Visible = false;
                this.tbtDelete.Visible = false;
                txtUserName.Text = "User Name: " + this.UserName;
                this.DGVDept.Enabled = false;

                this.DGVDept2.Visible = false;               

                // DataGridViewHeader Font style
                Font newFontStyle = new Font(DGVDept2.Font, FontStyle.Bold);
                DGVDept2.ColumnHeadersDefaultCellStyle.Font = newFontStyle;

                Font newFontStyle1 = new Font(DGVDept.Font, FontStyle.Bold);
                DGVDept.ColumnHeadersDefaultCellStyle.Font = newFontStyle1;

                //MessageBox.Show(System.DateTime.Now.ToString("dd-MMM-yy"));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                base.LogException(this.Name, "CommonOnLoadMethods", ex);                
            }
        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }
        private void PROC_1()
        {
            DateTime NewDate = new DateTime();
            DataTable DtPROC_1;
            DateTime NewDate1 = new DateTime();
            DataTable DtPROC_2;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("PR_TR_NO", txtPersNo.Text);
            colsNVals.Add("PR_EFFECTIVE", dtpPR_EFFECTIVE.Value);

            DtPROC_1 = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "PROC1_Transfer", colsNVals);
            if (DtPROC_1 != null)
            {
                if (DtPROC_1.Rows.Count == 1)
                {
                    if (DtPROC_1.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        NewDate = Convert.ToDateTime(DtPROC_1.Rows[0].ItemArray[0]);
                    }
                    MessageBox.Show("INVALID. DATE HAS TO BE GREATER THAN  :" + NewDate.ToString("dd-MMM-yy"));
                    dtpPR_EFFECTIVE.Focus();
                    return;
                }

                if (DtPROC_1.Rows.Count > 1)
                {

                    Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
                    colsNVals1.Clear();
                    colsNVals1.Add("PR_TR_NO", txtPersNo.Text);


                    DtPROC_2 = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "PROC1_Max", colsNVals1);
                    if (DtPROC_2 != null)
                    {

                        if (DtPROC_2.Rows.Count > 0)
                        {
                            if (DtPROC_2.Rows[0].ItemArray[0].ToString() != string.Empty)
                            {
                                NewDate1 = Convert.ToDateTime(DtPROC_2.Rows[0].ItemArray[0]);
                            }
                            MessageBox.Show("INVALID. DATE HAS TO BE GREATER THAN  :" + NewDate1.ToString("dd-MMM-yy"));
                            dtpPR_EFFECTIVE.Focus();
                            return;

                        }
                    }

                }
            }
            else
            {
               
                    txtW_REP.Focus();
                    return;
                
            }


        }
        private void SetListValues(DataTable dtTransfer)
        {
            
            this.txtfunc1.Text = dtTransfer.Rows[0]["PR_FUNC_1"].ToString();
            this.txtfunc2.Text = dtTransfer.Rows[0]["PR_FUNC_2"].ToString();
            if (dtTransfer.Rows[0]["PR_EFFECTIVE"].ToString() != string.Empty)
            {
                dtpPR_EFFECTIVE.Value = Convert.ToDateTime(dtTransfer.Rows[0]["PR_EFFECTIVE"]);
            }
            //Block 2 post query
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("PR_TR_NO", txtPersNo.Text);

            DataTable dtBlock2PostQuery = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "Block2_PostQuery", colsNVals);
            if (dtBlock2PostQuery != null)
            {
                txtW_REP.Text = dtBlock2PostQuery.Rows[0]["w_rep"].ToString();
                txtW_Name.Text = dtBlock2PostQuery.Rows[0]["w_name"].ToString();
                txtW_GOAL.Text = dtBlock2PostQuery.Rows[0]["W_GOAL"].ToString();
                if (dtBlock2PostQuery.Rows[0]["W_GOAL_DATE"].ToString() != string.Empty)
                {
                    dtW_GOAL_DATE.Value = Convert.ToDateTime(dtBlock2PostQuery.Rows[0]["W_GOAL_DATE"]);
                }
            }
            
        }
        private void FillGridForExistingDeptCont()
        {
            try
            {
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", txtPersNo.Text);
                colsNVals.Add("PR_TRANSFER_DATE", dtPRTransferDate.Value);
                colsNVals.Add("PR_TRANSFER", txtPR_TRANSFER.Text);

                DataTable dtDeptCont2 = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "DeptCont_ExistingRecord", colsNVals);
                if (dtDeptCont2 != null)
                {
                    if (dtDeptCont2.Rows.Count > 0)
                    {
                        this.DGVDept2.DataSource = dtDeptCont2;                     
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ReadOnlyAllControls(bool isTrue)
        {           
            this.txtPR_NEW_BRANCH.ReadOnly = isTrue;
            this.txtName.ReadOnly = isTrue;
            this.slTextBox1.ReadOnly = isTrue;
            this.txtPR_DESG.ReadOnly = isTrue;
            this.txtPR_LEVEL.ReadOnly = isTrue;
            this.txtNoINCR.ReadOnly = isTrue;
            this.txtPR_Func1.ReadOnly = isTrue;
            this.txtPR_Func2.ReadOnly = isTrue;
            this.txtfunc1.ReadOnly = isTrue;
            this.txtfunc2.ReadOnly = isTrue;
            this.txtW_REP.ReadOnly = isTrue;
            this.txtW_Name.ReadOnly = isTrue;
            this.txtW_GOAL.ReadOnly = isTrue;
            this.DGVDept.ReadOnly = isTrue;
        }
        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Cancel()
        {
            base.Cancel();
            this.txtDate.Text = this.Now().ToString("MM/dd/yyyy");
            this.DGVDept2.Visible = false;           
            return false;
        }
        private void EnableDisableControls(bool isEnable)
        {
            lbtnPNo.Enabled = isEnable;
            txtfunc1.Enabled = isEnable;
            txtfunc2.Enabled = isEnable;
            dtpPR_EFFECTIVE.Enabled = isEnable;
            txtW_REP.Enabled = isEnable;
            lbtnRep.Enabled = isEnable;
            txtW_GOAL.Enabled = isEnable;
            dtW_GOAL_DATE.Enabled = isEnable;
           // DGVDept.Enabled = isEnable;
        }

        #endregion

        #region Events

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersNo.Text != string.Empty)
            {
                Sum1 = 0;

                DataTable DtPersonal;
                DataTable DtTransfer;
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                Dictionary<string, object> param = new Dictionary<string, object>();
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", txtPersNo.Text);


                DtPersonal = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "ExecuteQueryPersonal", colsNVals);

                if (DtPersonal != null)
                {
                    #region DataFound
                    if (DtPersonal.Rows.Count > 0)
                    {
                        if (DtPersonal.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            JoiningDate.Value = Convert.ToDateTime(DtPersonal.Rows[0]["pr_joining_date"].ToString());
                        }
                        else
                        {
                            JoiningDate.Value = null;

                        }
                        this.txtName.Text = DtPersonal.Rows[0]["pr_name_first"].ToString();
                        this.slTextBox1.Text = DtPersonal.Rows[0]["Pr_name_Last"].ToString();
                        txtPR_NEW_BRANCH.Text = DtPersonal.Rows[0]["PR_NEW_BRANCH"].ToString();
                        txtPR_CLOSE_FLAG.Text = DtPersonal.Rows[0]["PR_CLOSE_FLAG"].ToString();
                        this.txtNoINCR.Text = DtPersonal.Rows[0]["PR_TRANSFER"].ToString();
                        txtPR_TRANSFER.Text = DtPersonal.Rows[0]["PR_TRANSFER"].ToString();
                        if (DtPersonal.Rows[0]["PR_TERMIN_DATE"].ToString() != string.Empty)
                        {
                            dtPR_TERMIN_DATE.Value = Convert.ToDateTime(DtPersonal.Rows[0]["PR_TERMIN_DATE"].ToString());
                        }
                        else
                        {
                            dtPR_TERMIN_DATE.Value = null;

                        }
                        if (DtPersonal.Rows[0]["PR_TRANSFER_DATE"].ToString() != string.Empty)
                        {
                            dtPRTransferDate.Value = Convert.ToDateTime(DtPersonal.Rows[0]["PR_TRANSFER_DATE"].ToString());
                        }
                        else
                        {
                            dtPRTransferDate.Value = null;

                        }

                        txtPR_DESG.Text = DtPersonal.Rows[0]["PR_DESIG"].ToString();
                        txtPR_LEVEL.Text = DtPersonal.Rows[0]["PR_LEVEL"].ToString();
                        txtPR_Func1.Text = DtPersonal.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                        txtPR_Func2.Text = DtPersonal.Rows[0]["PR_FUNC_TITTLE2"].ToString();

                        if (DtPersonal.Rows[0]["PR_TERMIN_DATE"].ToString() == string.Empty && txtPR_CLOSE_FLAG.Text != "C")
                        {
                            if (this.FunctionConfig.CurrentOption == Function.Add)
                            {
                                #region Add
                                if (txtPR_TRANSFER.Text == string.Empty || Convert.ToInt32(txtPR_TRANSFER.Text) <= 5)
                                {
                                    this.DGVDept2.Visible = true;
                                    FillGridForExistingDeptCont();
                                }
                                else
                                {
                                    MessageBox.Show("THIS PERSONNEL ID. IS NOT VALID FOR THIS OPTION");

                                    this.Cancel();
                                    txtOption.Focus();
                                }
                                #endregion
                            }
                            if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.View)
                            {
                                #region Modify/Delete/View
                                this.DGVDept2.Visible = false;
                                if (txtPR_TRANSFER.Text == "1")
                                {

                                    param.Clear();
                                    param.Add("PR_TR_NO", txtPersNo.Text);
                                    if (dtPRTransferDate.Value != null)
                                    {
                                        param.Add("PR_TRANSFER_DATE", dtPRTransferDate.Value);
                                    }

                                    DtTransfer = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "List", param);
                                    if (DtTransfer != null)
                                    {
                                        if (DtTransfer.Rows.Count > 0)
                                        {
                                            SetListValues(DtTransfer);
                                        }
                                    }
                                    iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;
                                    if (ent != null)
                                    {
                                        ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
                                        if (txtPersNo.Text != string.Empty)
                                        {
                                            ent.PR_TR_NO = Double.Parse(txtPersNo.Text);
                                        }
                                        if (txtNoINCR.Text != string.Empty)
                                        {
                                            ent.PR_TRANSFER = int.Parse(txtNoINCR.Text);
                                        }
                                    }
                                    this.pnlDetail.LoadDependentPanels();

                                    if (this.txtOption.Text == "D")
                                    {
                                        #region Delete Record

                                        this.txtOption.Focus();
                                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                                        base.DoToolbarActions(this.Controls, "Delete");

                                        #region Continue The Process For More Records

                                        DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Yes/No]..", "Note"
                                                       , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                        if (dRes1 == DialogResult.Yes)
                                        {
                                            this.DoToolbarActions(this.Controls, "Cancel");
                                            //this.txtOption.Text = "D";
                                            //this.txtCurrOption.Text = "DELETE";
                                            //this.FunctionConfig.CurrentOption = Function.Delete;
                                            //EnableDisableControls(true);
                                            ////this.CanEnableDisableControls = false;
                                            //this.txtPersNo.Focus();
                                        }
                                        else if (dRes1 == DialogResult.No)
                                        {
                                            this.DoToolbarActions(this.Controls, "Cancel");
                                        }

                                        #endregion

                                        return;

                                        #endregion
                                    }
                                    else if (this.txtOption.Text == "V")
                                    {
                                        #region View Record

                                        DialogResult dRes1 = MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "Note"
                                                       , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                        if (dRes1 == DialogResult.Yes)
                                        {
                                            this.DoToolbarActions(this.Controls, "Cancel");
                                            this.txtOption.Text = "V";
                                            this.txtCurrOption.Text = "VIEW";
                                            this.FunctionConfig.CurrentOption = Function.View;
                                            //this.CanEnableDisableControls = false;
                                            EnableDisableControls(true);
                                            this.txtPersNo.Focus();
                                        }
                                        else if (dRes1 == DialogResult.No)
                                        {
                                            this.DoToolbarActions(this.Controls, "Cancel");
                                        }

                                        #endregion
                                    }


                                }
                                else
                                {
                                    MessageBox.Show("THIS PERSONNEL ID. IS NOT VALID FOR THIS OPTION");

                                    this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                                    txtOption.Focus();
                                }

                                #endregion
                            }

                        }

                    }
                    #endregion
                }
                else
                {
                    MessageBox.Show("Record Not Found");
                    this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
                    txtOption.Focus();
                    return;
                }
            }
            //else
            //{
            //    MessageBox.Show("Record Not Found");
               
            //    this.DoToolbarActions(this.pnlDetail.Controls, "Cancel");
              
            //    txtOption.Focus();
            //    return;
            //}
           
        }

        private void dtpPR_EFFECTIVE_Validating(object sender, CancelEventArgs e)
        {
            if (dtpPR_EFFECTIVE.Value == null)
            {
                MessageBox.Show("YOU HAVE NOT ENTERED THE DATE");
                dtpPR_EFFECTIVE.Focus();
                return;
            }
            else
            {

                DateTime date1 = Convert.ToDateTime(dtpPR_EFFECTIVE.Value);
                DateTime date2 = Convert.ToDateTime(JoiningDate.Value);
                int result = DateTime.Compare( date1,date2);
                if (result < 0)
                {
                    MessageBox.Show("THE DATE YOU ENTERED IS BEFORE THE JOINING DATE.WHICH IS " + date2.ToString("dd-MMM-yy"));
                    dtpPR_EFFECTIVE.Focus();
                    return;
                }
                if(this.FunctionConfig.CurrentOption== Function.Add)
                {
                    DateTime date3 = Convert.ToDateTime(dtPRTransferDate.Value);
                    int result1 = DateTime.Compare(date1, date3);
                    if(result1==0)
                    {
                        MessageBox.Show("RECORD FOR THIS DATE ALREADY EXISTS");
                        dtpPR_EFFECTIVE.Focus();
                        return;
                    }
                    else
                    {
                        PROC_1();
                    }

                }
                if(this.FunctionConfig.CurrentOption== Function.Modify)
                {
                    DateTime date3 = Convert.ToDateTime(dtPRTransferDate.Value);
                    int result2 = DateTime.Compare(date1.Date, date3.Date);
                    if(result2!=0)
                    {
                        PROC_1();
                    }

                    else
                    {
                        txtW_REP.Focus();
                    }

                }

                txtSum1.Text = "0";
                iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;
                if (ent != null)
                {
                    ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
                }
                //nidaaa
                //pnlDetail.LoadDependentPanels();
          
            }
        }

        private void txtW_GOAL_Validating(object sender, CancelEventArgs e)
        {
            if (txtW_GOAL.Text == string.Empty)
            {
                txtW_GOAL.Text = "N/A";
            }
            else if ((txtW_GOAL.Text.Substring(0, 1) != "Y") && (txtW_GOAL.Text.Substring(0, 1) != "N"))
            {
                MessageBox.Show("ENTER [YES] OR [NO] .........!");
                txtW_GOAL.Focus();
                return;
            }
            else if (txtW_GOAL.Text.Substring(0, 1) == "Y")
            {
                txtW_GOAL.Text = "YES";
            }
            else if (txtW_GOAL.Text.Length >= 3)
            {
                if ((txtW_GOAL.Text.Substring(0, 1) == "N") && (txtW_GOAL.Text.Substring(1, 2) != "/A"))
                {
                    txtW_GOAL.Text = "NO";
                }
            }                 

        }

        private void dtW_GOAL_DATE_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void DGVDept_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //column 1 is "PR_MENU_OPTION"

            if (!this.DGVDept.CurrentCell.IsInEditMode || this.DGVDept.Enabled==false)
            {

                return;
            }
            int WVal1=0;
            int contribValue=0;

            if (e.ColumnIndex == 1)
            {

                #region Column1
                if (DGVDept.Rows[e.RowIndex].Cells[0].IsInEditMode)
                {
                    //if (DGVDept.CurrentRow.Cells["Seg"].Value != null)
                    //{
                    //    pr_SegmentValue = DGVDept.CurrentRow.Cells["Seg"].Value.ToString();
                    //}
                    if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                    {

                        if ((DGVDept.Rows[e.RowIndex].Cells["Seg"].EditedFormattedValue.ToString() != "GF" && DGVDept.Rows[e.RowIndex].Cells["Seg"].EditedFormattedValue.ToString() != "GCB") || DGVDept.Rows[e.RowIndex].Cells["Seg"].EditedFormattedValue.ToString() == string.Empty)
                        {
                            MessageBox.Show("YOU HAVE ENTERED AN INVALID SEGMENT. ENTER GF OR GCB");

                            e.Cancel = true;

                        }
                        else
                        {
                            this.DGVDept[e.ColumnIndex, e.RowIndex].Value = DGVDept.Rows[e.RowIndex].Cells["Seg"].Value.ToString();
                        }
                    }

                    //}
                #endregion
                    if (txtNoINCR.Text != string.Empty)
                    {
                        DGVDept.Rows[e.RowIndex].Cells["PR_MENU_OPTION"].Value = txtNoINCR.Text;
                    }
                }
            }




            if (e.ColumnIndex == 2)
            {


                if (DGVDept.CurrentCell.OwningColumn.Name == "Dept" && DGVDept.CurrentCell.Value != null)
                {
                    string pr_dept = null;
                    string pr_deptValue = DGVDept.CurrentCell.Value.ToString();

                    if (DGVDept.CurrentRow.Cells["Seg"].Value != null)
                    {
                        pr_SegmentValue = DGVDept.CurrentRow.Cells["Seg"].Value.ToString();
                    }
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_SEGMENT", pr_SegmentValue);
                    param.Add("PR_DEPT", pr_deptValue);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER", "DEPT_LOV_EXISTS", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        pr_dept = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    }
                    else
                    {
                        e.Cancel = true;
                    }

                    //if (pr_dept == null && pr_dept == string.Empty)
                    //{
                    //    MessageBox.Show("Invalid Department Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    e.Cancel = true;
                    //}

                    iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand)this.pnlTblDept.CurrentBusinessEntity;
                    if (ent != null)
                    {
                        ent.PR_SEGMENT = pr_SegmentValue;
                    }

                }




                #region Column2
             
                if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                {

                    object obj = DGVDept.Rows[e.RowIndex].Cells[1].EditedFormattedValue;
                    object obj2 = DGVDept.Rows[e.RowIndex].Cells[2].EditedFormattedValue;
                    if (obj != null && obj.ToString() != string.Empty && obj2 != null && obj2.ToString() != string.Empty)
                    {
                        if (!this.DeptExists(obj.ToString(), obj2.ToString()))
                        {
                            MessageBox.Show("YOU ENTERED A DEPARTMENT WHICH DOES NOT BELONG TO THE SEGMENT ENTERED. PRESS [LIST] TO SEE VALID VALUE");
                            this.Message.ShowMessage();

                          
                            e.Cancel = true;
                            return;

                        }
                    }
                }

                
                #endregion
            }



            if (e.ColumnIndex == 3)
            {
                #region Column3
                if (txtSum1.Text != string.Empty)
                {
                    Sum1 = Convert.ToInt32(txtSum1.Text);
                }
                if (W_VAL1.Text != string.Empty)
                {
                    WVal1 = Convert.ToInt32(W_VAL1.Text);
                }

                //if (DGVDept.Rows[e.RowIndex].Cells["Contrib"].IsInEditMode)
                //{
                if (DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue.ToString() != string.Empty)
                {
                    if (int.TryParse(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue.ToString(), out contribValue))
                    {
                        if (contribValue != 0 && contribValue<=100)
                        
                        {

                            if (Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue) <= 100)
                            {
                                Sum1 = Sum1 + Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue) - WVal1;
                                txtSum1.Text = Sum1.ToString();
                                WVal1 = 0;
                            }

                            if (Sum1 == 100)
                            {
                                W_VAL1.Text = DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue.ToString();
                                txtSum1.Text = "0";

                                //if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                                //{
                                //    DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                //                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                //    if (dRes == DialogResult.Yes)
                                //    {

                                //        this.txtOption.Focus();
                                //        iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;
                                //        if (ent != null)
                                //        {
                                //            ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
                                //        }
                                //        //((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand)this.pnlTblDept.CurrentBusinessEntity).PR_TRANSFER = Decimal.Parse(this.txtNoINCR.Text);
                                //        //this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                                //        if (this.txtNoINCR.Text != string.Empty)
                                //        {
                                //            DGVDept[0, e.RowIndex].Value = Double.Parse(this.txtNoINCR.Text);
                                //        }
                                //        base.DoToolbarActions(this.Controls, "Save");
                                //        base.DoToolbarActions(this.Controls, "Cancel");

                                //        #region Continue The Process For More Records

                                //        DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Yes/No]..", "Note"
                                //                       , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                //        if (dRes1 == DialogResult.Yes)
                                //        {
                                //            base.DoToolbarActions(this.Controls, "Cancel");
                                //            this.txtOption.Text = "A";
                                //            this.txtCurrOption.Text = "ADD";
                                //            this.FunctionConfig.CurrentOption = Function.Add;
                                //            this.CanEnableDisableControls = false;
                                //            this.txtPersNo.Focus();
                                //        }
                                //        else if (dRes1 == DialogResult.No)
                                //        {
                                //            base.DoToolbarActions(this.Controls, "Cancel");
                                //        }

                                //        #endregion

                                //        return;

                                //    }
                                //    else if (dRes == DialogResult.No)
                                //    {
                                //        base.DoToolbarActions(this.Controls, "Cancel");
                                //        txtOption.Focus();

                                //        return;
                                //    }
                                //}

                            }

                            if (Sum1 > 100)/*** IF % > 100 ***/
                            {
                                MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100");
                                e.Cancel = true;
                                Sum1 = Sum1 - Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue);
                                txtSum1.Text = Sum1.ToString();
                                dCount = 0;
                                flag = false;


                            }


                        }
                        else
                        {
                            MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 AND GREATER THAN 0");
                            e.Cancel = true;
                        }

                    }
                    else
                    {
                        MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 AND GREATER THAN 0");
                        e.Cancel = true;
                    }
                }
                else
                {
                    MessageBox.Show("CONTRIBUTION MARGIN SHOULD BE LESS THAN OR EQUAL TO 100 AND GREATER THAN 0");
                    e.Cancel = true;
                }

                    //}
                #endregion
               
            }           

        }       
      
        private void DGVDept_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }             

        private void txtOption_Leave(object sender, EventArgs e)
        {
            if (this.txtOption.Text == "V")
            {
                //ReadOnlyAllControls(true);
            }
            else
            {
                //ReadOnlyAllControls(false);
            }
        }
       
        private void txtW_REP_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtW_REP.Text != string.Empty)
                {
                    Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

                    dicInputParameters.Add("W_REP", txtW_REP.Text);

                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rsltW_REP = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "ReportingTo", dicInputParameters);

                    if (rsltW_REP.isSuccessful)
                    {
                        if (rsltW_REP.dstResult != null)
                        {
                            if (rsltW_REP.dstResult.Tables.Count > 0)
                            {
                                if (rsltW_REP.dstResult.Tables[0].Rows.Count < 1)
                                {
                                    MessageBox.Show("Invalid Personnel No......");
                                    this.txtW_REP.Focus();
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Personnel No......");
                        this.txtW_REP.Focus();
                        return;
                    }

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }      

        private void dtW_GOAL_DATE_Leave(object sender, EventArgs e)
        {
            //this.DGVDept.Enabled = true;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            try
            {
                if (actionType == "Cancel")
                {
                    this.AutoValidate = AutoValidate.Disable;
                    this.txtPersNo.IsRequired = false;
                    this.txtPersNo.Text = "";
                }
                base.DoToolbarActions(ctrlsCollection, actionType);

                if (actionType == "Cancel")
                {

                    this.DGVDept2.Visible = false;
                    this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                    this.DGVDept.Enabled = false;
                    pnlTblDept.Enabled = false;

                    txtOption.Focus();
                    this.txtPersNo.IsRequired = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private bool DeptExists(string Segment,string Dept)
        {
            bool flag = false;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_SEGMENT", Segment);
            param.Add("PR_DEPT", Dept);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER", "DeptCheckOnSegment", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }

        #endregion

        private void dtW_GOAL_DATE_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                    if (dtW_GOAL_DATE.Value == null)
                    {
                        dtW_GOAL_DATE.Value = this.Now().ToString("MM/dd/yyyy");

                    }
                    this.pnlTblDept.Enabled = true;
                    this.DGVDept.Enabled = true;
                    DGVDept.Focus();
                    return;
                }

            }
        }

        private void DGVDept_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (tbtCancel.Pressed || tbtClose.Pressed || FunctionConfig.CurrentOption == Function.None || this.DGVDept.Enabled == false || LastCellValidate==false)
                return;
            if (!tlbMain.Items["tbtCancel"].Pressed && !tlbMain.Items["tbtClose"].Pressed)
            {
                if ((this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify) )
                {
                    if (((DGVDept.CurrentRow.Cells["Seg"].EditedFormattedValue.ToString() == string.Empty)
                          || (DGVDept.CurrentRow.Cells["Dept"].EditedFormattedValue.ToString() == string.Empty)
                          || (DGVDept.CurrentRow.Cells["Contrib"].EditedFormattedValue.ToString() == string.Empty))
                         )
                    {

                        MessageBox.Show("Please fill all mandatory columns.");
                        e.Cancel = true;
                        dCount = 0;
                        flag = false;
                        LastCellValidate = false;

                    }

                        double totalContribution = 0;
                    double.TryParse((DGVDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalContribution);
                    if (totalContribution > 100)
                    {
                        MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100");
                        e.Cancel = true;
                        Sum1 = Sum1 - Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].EditedFormattedValue);
                        txtSum1.Text = Sum1.ToString();
                      
                        dCount = 0;
                        flag = false;

                    }
                    //if (totalContribution == 100)
                    //{
                    //    flag = true;
                    //}
                   

                }
            }
                    
        }

        private void DGVDept_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (tbtCancel.Pressed || tbtClose.Pressed || FunctionConfig.CurrentOption == Function.None || this.DGVDept.Enabled == false)
                return;

            if (e.RowIndex >= 0)
            {
                if (DGVDept.DataSource != null)
                {
                    double totalContribution = 0;
                    double.TryParse((DGVDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalContribution);
                    if (totalContribution == 100)
                    {
                        if (flag)
                        {
                            dCount = dCount + 1;
                        }
                        if ((this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify) && dCount == 1)
                        {
                            DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                            , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dRes == DialogResult.Yes)
                            {
                                flag = false;
                                dCount = 0;
                                LastCellValidate = false;

                                if ((this.DGVDept.DataSource as DataTable) != null)
                                {
                                    if (DGVDept.Rows.Count > 1)
                                    {

                                        for (int i = 0; i < (this.DGVDept.DataSource as DataTable).Rows.Count; i++)
                                        {
                                            if (dtpPR_EFFECTIVE.Value != null)
                                                (this.DGVDept.DataSource as DataTable).Rows[i]["PR_EFFECTIVE"] = Convert.ToDateTime(dtpPR_EFFECTIVE.Value);
                                        }
                                    }
                                }
                                ///this.txtOption.Focus();
                                iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;
                                if (ent != null)
                                {
                                    ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
                                }
                                //((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand)this.pnlTblDept.CurrentBusinessEntity).PR_TRANSFER = Decimal.Parse(this.txtNoINCR.Text);
                                //this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;

                                //base.DoToolbarActions(this.Controls, "Save");


                                DataTable genLinkFile = new DataTable();
                                genLinkFile.Columns.Add("PR_TR_NO", typeof(int));
                                genLinkFile.Columns.Add("PR_TRANSFER_TYPE", typeof(int));
                                genLinkFile.Columns.Add("PR_FUNC_1", typeof(string));
                                genLinkFile.Columns.Add("PR_FUNC_2", typeof(string));
                                genLinkFile.Columns.Add("PR_EFFECTIVE", typeof(DateTime));
                                genLinkFile.Columns.Add("PR_REP_NO", typeof(int));
                                genLinkFile.Columns.Add("W_GOAL_DATE", typeof(DateTime));
                                genLinkFile.Columns.Add("W_GOAL", typeof(string));
                                
                                genLinkFile.Columns.Add("PR_UserID", typeof(string));
                                genLinkFile.Columns.Add("PR_UploadDate", typeof(string));
                                genLinkFile.Columns.Add("PR_User_Approved", typeof(bool));

                                DataRow genLinkRow;
                                //txtPersNo
                                //foreach (DataRow dtrow in dtUpdated.Rows)
                                {
                                    genLinkRow = genLinkFile.NewRow();

                                    string PRNO = string.IsNullOrEmpty(txtPersNo.Text.Trim()) ? "" : txtPersNo.Text.Trim();
                                    genLinkRow["PR_TR_NO"] = Convert.ToInt32(PRNO);

                                    genLinkRow["PR_FUNC_1"] = string.IsNullOrEmpty(txtfunc1.Text.Trim()) ? "" : txtfunc1.Text.Trim();
                                    genLinkRow["PR_EFFECTIVE"] = Convert.ToDateTime(string.IsNullOrEmpty(dtpPR_EFFECTIVE.Text.ToString().Trim()) ? "" : dtpPR_EFFECTIVE.Text.ToString().Trim());
                                    genLinkRow["PR_FUNC_2"] = string.IsNullOrEmpty(txtfunc2.Text.Trim()) ? "" : txtfunc2.Text.Trim();
                                    genLinkRow["PR_TRANSFER_TYPE"] = Convert.ToInt32(string.IsNullOrEmpty(txtNoINCR.Text.Trim()) ? "" : txtNoINCR.Text.Trim());
                                    genLinkRow["PR_REP_NO"] = string.IsNullOrEmpty(txtW_REP.Text.Trim()) ? "" : txtW_REP.Text.Trim();
                                    genLinkRow["W_GOAL_DATE"] = Convert.ToDateTime(string.IsNullOrEmpty(dtW_GOAL_DATE.Text.ToString().Trim()) ? "" : dtW_GOAL_DATE.Text.ToString().Trim());
                                    genLinkRow["W_GOAL"] = string.IsNullOrEmpty(txtW_GOAL.Text.Trim()) ? "" : txtW_GOAL.Text.Trim();
                                    //genLinkRow["PR_ID_RETURN"] = string.IsNullOrEmpty(txtIDCardRet.Text.Trim()) ? "" : txtIDCardRet.Text.Trim();
                                    //genLinkRow["PR_NOTICE"] = string.IsNullOrEmpty(txtNoticeOfStaffChanged.Text.Trim()) ? "" : txtNoticeOfStaffChanged.Text.Trim();
                                    //genLinkRow["PR_ARTONY"] = string.IsNullOrEmpty(txtPowerOfAttornyCancel.Text.Trim()) ? "" : txtPowerOfAttornyCancel.Text.Trim();
                                    //genLinkRow["PR_DELETION"] = string.IsNullOrEmpty(txtDelFromMic.Text.Trim()) ? "" : txtDelFromMic.Text.Trim();
                                    //genLinkRow["PR_SETTLE"] = string.IsNullOrEmpty(txtFinalSettlementReport.Text.Trim()) ? "" : txtFinalSettlementReport.Text.Trim();
                                    genLinkRow["PR_UserID"] = txtUser.Text;
                                    genLinkRow["PR_UploadDate"] = DateTime.Now.ToString();
                                    genLinkRow["PR_User_Approved"] = false;

                                    genLinkFile.Rows.Add(genLinkRow);

                                    //SQLManager.PR_TERMINATION(PR_P_NO, PR_FIRST_NAME, PR_TERMIN_TYPE, PR_TERMIN_DATE, PR_REASONS, PR_RESIG_RECV, PR_APP_RESIG, PR_EXIT_INTER, PR_ID_RETURN, PR_NOTICE,
                                    //                          PR_ARTONY, PR_DELETION, PR_SETTLE, PR_UserID);


                                }
                                uploadData(genLinkFile);


                                MessageBox.Show("Record Save Successfully");


                                #region Continue The Process For More Records

                                DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Yes/No]..", "Note"
                                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dRes1 == DialogResult.Yes)
                                {
                                   
                                    if (txtMode.Text == "A")
                                    {
                                        base.ClearForm(this.pnlDetail.Controls);
                                        base.ClearForm(this.pnlTblDept.Controls);
                                        base.ClearForm(this.pnlTblDept2.Controls);
                                        AddMoreRecords();
                                    }
                                    else
                                    {
                                        base.ClearForm(this.pnlDetail.Controls);
                                        base.ClearForm(this.pnlTblDept.Controls);
                                        base.ClearForm(this.pnlTblDept2.Controls);
                                        ModifyMoreRecords();
                                    }

                                }
                                else if (dRes1 == DialogResult.No)
                                {
                                    this.DoToolbarActions(this.Controls, "Cancel");
                                }

                                #endregion

                                return;

                            }
                            else if (dRes == DialogResult.No)
                            {
                                this.DoToolbarActions(this.Controls, "Cancel");
                                txtOption.Focus();

                                return;
                            }
                        }

                    }



                }

            }
        }

        public static void uploadData(DataTable dt)
        {
            try
            {
                SQLManager.ExecuteSegDetailUpload(dt);
            }
            catch (Exception ex)
            {

            }
        }

        private void DGVDept_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                LastCellValidate = true;

                if (this.FunctionConfig.CurrentOption == Function.Modify  && Sum1==0)
                {
                    int WVal1 = 0;
                    int SumVal = 0;
                    if (txtSum1.Text != string.Empty)
                    {
                        SumVal = Convert.ToInt32(txtSum1.Text);
                    }
                    if (W_VAL1.Text != string.Empty)
                    {
                        WVal1 = Convert.ToInt32(W_VAL1.Text);
                    }
                       if (Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].Value) <= 100)
                            {
                                SumVal = SumVal + Convert.ToInt32(DGVDept.Rows[e.RowIndex].Cells["Contrib"].Value) - WVal1;
                                txtSum1.Text = SumVal.ToString();
                                WVal1 = 0;
                            }

                            if (SumVal == 100)
                            {
                                W_VAL1.Text = DGVDept.Rows[e.RowIndex].Cells["Contrib"].Value.ToString();
                                txtSum1.Text = "0";
                                //Sum1 = 0;
                            }

                            if (SumVal == 100)/*** IF % > 100 ***/
                            {


                                flag = true;

                            }
                            else
                            {
                                dCount = 0;
                                flag = false;
                            }
                            return;

                }
                if (this.FunctionConfig.CurrentOption == Function.Add  || this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    if (Sum1 == 100)/*** IF % > 100 ***/
                    {


                        flag = true;

                    }
                    else
                    {
                        dCount = 0;
                        flag = false;
                    }
                }
            }

        }

        private void DGVDept_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Keys st = (Keys.Shift | Keys.Tab);
            if (e.KeyData == st)
                {
                   // e.IsInputKey = true;
               
                    this.DGVDept.Enabled = false;
                    dtpPR_EFFECTIVE.Focus();


                }

          

        }

        public static string SetValueUserLogin = "";
        private void btnAuth_Click(object sender, EventArgs e)
        {
            SetValueUserLogin = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            CHRIS_Personnel_SegmtOrDeptTrans_View frm = new CHRIS_Personnel_SegmtOrDeptTrans_View();
            frm.ShowDialog();
            this.Close();
        }
    }
}