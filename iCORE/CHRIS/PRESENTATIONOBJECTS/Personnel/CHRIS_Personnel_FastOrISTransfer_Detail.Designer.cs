﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_FastOrISTransfer_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtPR_Func2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtPR_Func1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPR_DESG = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtPRTransfer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBlkTransfer = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPR_REMARKS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountry = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCITI_FLAG2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCity = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCurrAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txtPRDept = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_EFFECTIVE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDesg = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txtNewAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCITI_FLAG1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrency = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.btnAproved = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlDetail.SuspendLayout();
            this.pnlBlkTransfer.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.btnClose);
            this.pnlDetail.Controls.Add(this.btnReject);
            this.pnlDetail.Controls.Add(this.btnAproved);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.slTextBox1);
            this.pnlDetail.Controls.Add(this.label6);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.txtPR_Func2);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.txtPR_Func1);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtPR_DESG);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtPRTransfer);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferISLOCALCommand";
            this.pnlDetail.Location = new System.Drawing.Point(13, 13);
            this.pnlDetail.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(800, 280);
            this.pnlDetail.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.pnlDetail.TabIndex = 14;
            this.pnlDetail.TabStop = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(39, 246);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(737, 17);
            this.label9.TabIndex = 146;
            this.label9.Text = "_________________________________________________________________________________" +
    "";
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(598, 66);
            this.slTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.slTextBox1.MaxLength = 20;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(142, 24);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 144;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(202, 18);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(359, 22);
            this.label6.TabIndex = 139;
            this.label6.Text = "PERSONNEL SYSTEM /FAST OR IS";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(440, 97);
            this.txtLevel.Margin = new System.Windows.Forms.Padding(4);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(115, 24);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 110;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(354, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 25);
            this.label5.TabIndex = 109;
            this.label5.Text = "4.Level:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func2
            // 
            this.txtPR_Func2.AllowSpace = true;
            this.txtPR_Func2.AssociatedLookUpName = "";
            this.txtPR_Func2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func2.ContinuationTextBox = null;
            this.txtPR_Func2.CustomEnabled = true;
            this.txtPR_Func2.DataFieldMapping = "";
            this.txtPR_Func2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func2.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func2.IsDate = false;
            this.txtPR_Func2.Location = new System.Drawing.Point(190, 165);
            this.txtPR_Func2.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_Func2.MaxLength = 30;
            this.txtPR_Func2.Name = "txtPR_Func2";
            this.txtPR_Func2.NumberFormat = "###,###,##0.00";
            this.txtPR_Func2.Postfix = "";
            this.txtPR_Func2.Prefix = "";
            this.txtPR_Func2.ReadOnly = true;
            this.txtPR_Func2.Size = new System.Drawing.Size(151, 24);
            this.txtPR_Func2.SkipValidation = false;
            this.txtPR_Func2.TabIndex = 108;
            this.txtPR_Func2.TabStop = false;
            this.txtPR_Func2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(5, 162);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 25);
            this.label7.TabIndex = 107;
            this.label7.Text = "Title :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func1
            // 
            this.txtPR_Func1.AllowSpace = true;
            this.txtPR_Func1.AssociatedLookUpName = "";
            this.txtPR_Func1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func1.ContinuationTextBox = null;
            this.txtPR_Func1.CustomEnabled = true;
            this.txtPR_Func1.DataFieldMapping = "";
            this.txtPR_Func1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func1.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func1.IsDate = false;
            this.txtPR_Func1.Location = new System.Drawing.Point(190, 133);
            this.txtPR_Func1.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_Func1.MaxLength = 30;
            this.txtPR_Func1.Name = "txtPR_Func1";
            this.txtPR_Func1.NumberFormat = "###,###,##0.00";
            this.txtPR_Func1.Postfix = "";
            this.txtPR_Func1.Prefix = "";
            this.txtPR_Func1.ReadOnly = true;
            this.txtPR_Func1.Size = new System.Drawing.Size(151, 24);
            this.txtPR_Func1.SkipValidation = false;
            this.txtPR_Func1.TabIndex = 88;
            this.txtPR_Func1.TabStop = false;
            this.txtPR_Func1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(2, 130);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(183, 25);
            this.label14.TabIndex = 87;
            this.label14.Text = "5.Functional :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(4, 197);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(183, 25);
            this.label12.TabIndex = 86;
            this.label12.Text = "6. Transfer Type :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_DESG
            // 
            this.txtPR_DESG.AllowSpace = true;
            this.txtPR_DESG.AssociatedLookUpName = "";
            this.txtPR_DESG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESG.ContinuationTextBox = null;
            this.txtPR_DESG.CustomEnabled = true;
            this.txtPR_DESG.DataFieldMapping = "";
            this.txtPR_DESG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESG.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESG.IsDate = false;
            this.txtPR_DESG.Location = new System.Drawing.Point(190, 97);
            this.txtPR_DESG.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_DESG.MaxLength = 10;
            this.txtPR_DESG.Name = "txtPR_DESG";
            this.txtPR_DESG.NumberFormat = "###,###,##0.00";
            this.txtPR_DESG.Postfix = "";
            this.txtPR_DESG.Prefix = "";
            this.txtPR_DESG.ReadOnly = true;
            this.txtPR_DESG.Size = new System.Drawing.Size(115, 24);
            this.txtPR_DESG.SkipValidation = false;
            this.txtPR_DESG.TabIndex = 80;
            this.txtPR_DESG.TabStop = false;
            this.txtPR_DESG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(26, 97);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(161, 25);
            this.label13.TabIndex = 82;
            this.label13.Text = "3. Designation :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRTransfer
            // 
            this.txtPRTransfer.AllowSpace = true;
            this.txtPRTransfer.AssociatedLookUpName = "";
            this.txtPRTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRTransfer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRTransfer.ContinuationTextBox = null;
            this.txtPRTransfer.CustomEnabled = true;
            this.txtPRTransfer.DataFieldMapping = "";
            this.txtPRTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRTransfer.GetRecordsOnUpDownKeys = false;
            this.txtPRTransfer.IsDate = false;
            this.txtPRTransfer.Location = new System.Drawing.Point(190, 197);
            this.txtPRTransfer.Margin = new System.Windows.Forms.Padding(4);
            this.txtPRTransfer.MaxLength = 1;
            this.txtPRTransfer.Name = "txtPRTransfer";
            this.txtPRTransfer.NumberFormat = "###,###,##0.00";
            this.txtPRTransfer.Postfix = "";
            this.txtPRTransfer.Prefix = "";
            this.txtPRTransfer.ReadOnly = true;
            this.txtPRTransfer.Size = new System.Drawing.Size(51, 24);
            this.txtPRTransfer.SkipValidation = false;
            this.txtPRTransfer.TabIndex = 68;
            this.txtPRTransfer.TabStop = false;
            this.txtPRTransfer.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(454, 66);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(142, 24);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label8.Location = new System.Drawing.Point(321, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 25);
            this.label8.TabIndex = 26;
            this.label8.Text = "2. First Name:";
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(190, 65);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(78, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(25, 65);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(161, 25);
            this.label11.TabIndex = 23;
            this.label11.Text = "1. Pr No. :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(190, 65);
            this.txtID.Margin = new System.Windows.Forms.Padding(4);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(47, 24);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // pnlBlkTransfer
            // 
            this.pnlBlkTransfer.ConcurrentPanels = null;
            this.pnlBlkTransfer.Controls.Add(this.label1);
            this.pnlBlkTransfer.Controls.Add(this.label2);
            this.pnlBlkTransfer.Controls.Add(this.label4);
            this.pnlBlkTransfer.Controls.Add(this.txtPR_REMARKS);
            this.pnlBlkTransfer.Controls.Add(this.txtCountry);
            this.pnlBlkTransfer.Controls.Add(this.label3);
            this.pnlBlkTransfer.Controls.Add(this.label10);
            this.pnlBlkTransfer.Controls.Add(this.txtCITI_FLAG2);
            this.pnlBlkTransfer.Controls.Add(this.txtCity);
            this.pnlBlkTransfer.Controls.Add(this.label15);
            this.pnlBlkTransfer.Controls.Add(this.label16);
            this.pnlBlkTransfer.Controls.Add(this.txtCurrAnnual);
            this.pnlBlkTransfer.Controls.Add(this.label17);
            this.pnlBlkTransfer.Controls.Add(this.txtPRDept);
            this.pnlBlkTransfer.Controls.Add(this.label19);
            this.pnlBlkTransfer.Controls.Add(this.slTextBox3);
            this.pnlBlkTransfer.Controls.Add(this.txtPR_EFFECTIVE);
            this.pnlBlkTransfer.Controls.Add(this.label18);
            this.pnlBlkTransfer.Controls.Add(this.label21);
            this.pnlBlkTransfer.Controls.Add(this.txtDesg);
            this.pnlBlkTransfer.Controls.Add(this.label22);
            this.pnlBlkTransfer.Controls.Add(this.txtNewAnnual);
            this.pnlBlkTransfer.Controls.Add(this.txtCITI_FLAG1);
            this.pnlBlkTransfer.Controls.Add(this.txtCurrency);
            this.pnlBlkTransfer.Controls.Add(this.label24);
            this.pnlBlkTransfer.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlBlkTransfer.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlBlkTransfer.DependentPanels = null;
            this.pnlBlkTransfer.DisableDependentLoad = false;
            this.pnlBlkTransfer.EnableDelete = true;
            this.pnlBlkTransfer.EnableInsert = true;
            this.pnlBlkTransfer.EnableQuery = false;
            this.pnlBlkTransfer.EnableUpdate = true;
            this.pnlBlkTransfer.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferISLOCALCommand";
            this.pnlBlkTransfer.Location = new System.Drawing.Point(20, 301);
            this.pnlBlkTransfer.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBlkTransfer.MasterPanel = null;
            this.pnlBlkTransfer.Name = "pnlBlkTransfer";
            this.pnlBlkTransfer.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlBlkTransfer.Size = new System.Drawing.Size(624, 399);
            this.pnlBlkTransfer.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.pnlBlkTransfer.TabIndex = 98;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(230, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 18);
            this.label1.TabIndex = 86;
            this.label1.Text = "Transfer Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(18, 364);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 18);
            this.label2.TabIndex = 96;
            this.label2.Text = "Remarks ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(18, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 18);
            this.label4.TabIndex = 70;
            this.label4.Text = "Transfer Country:";
            // 
            // txtPR_REMARKS
            // 
            this.txtPR_REMARKS.AllowSpace = true;
            this.txtPR_REMARKS.AssociatedLookUpName = "";
            this.txtPR_REMARKS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_REMARKS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_REMARKS.ContinuationTextBox = null;
            this.txtPR_REMARKS.CustomEnabled = true;
            this.txtPR_REMARKS.DataFieldMapping = "";
            this.txtPR_REMARKS.Enabled = false;
            this.txtPR_REMARKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_REMARKS.GetRecordsOnUpDownKeys = false;
            this.txtPR_REMARKS.IsDate = false;
            this.txtPR_REMARKS.Location = new System.Drawing.Point(250, 358);
            this.txtPR_REMARKS.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_REMARKS.MaxLength = 50;
            this.txtPR_REMARKS.Name = "txtPR_REMARKS";
            this.txtPR_REMARKS.NumberFormat = "###,###,##0.00";
            this.txtPR_REMARKS.Postfix = "";
            this.txtPR_REMARKS.Prefix = "";
            this.txtPR_REMARKS.Size = new System.Drawing.Size(358, 24);
            this.txtPR_REMARKS.SkipValidation = false;
            this.txtPR_REMARKS.TabIndex = 12;
            this.txtPR_REMARKS.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCountry
            // 
            this.txtCountry.AllowSpace = true;
            this.txtCountry.AssociatedLookUpName = "";
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountry.ContinuationTextBox = null;
            this.txtCountry.CustomEnabled = true;
            this.txtCountry.DataFieldMapping = "PR_COUNTRY";
            this.txtCountry.Enabled = false;
            this.txtCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountry.GetRecordsOnUpDownKeys = false;
            this.txtCountry.IsDate = false;
            this.txtCountry.Location = new System.Drawing.Point(250, 45);
            this.txtCountry.Margin = new System.Windows.Forms.Padding(4);
            this.txtCountry.MaxLength = 15;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.NumberFormat = "###,###,##0.00";
            this.txtCountry.Postfix = "";
            this.txtCountry.Prefix = "";
            this.txtCountry.Size = new System.Drawing.Size(133, 24);
            this.txtCountry.SkipValidation = false;
            this.txtCountry.TabIndex = 1;
            this.txtCountry.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(18, 330);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(194, 18);
            this.label3.TabIndex = 94;
            this.label3.Text = "Voluntary/Non Voluntary ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(18, 81);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 18);
            this.label10.TabIndex = 72;
            this.label10.Text = "Transfer City ";
            // 
            // txtCITI_FLAG2
            // 
            this.txtCITI_FLAG2.AllowSpace = true;
            this.txtCITI_FLAG2.AssociatedLookUpName = "";
            this.txtCITI_FLAG2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCITI_FLAG2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCITI_FLAG2.ContinuationTextBox = null;
            this.txtCITI_FLAG2.CustomEnabled = true;
            this.txtCITI_FLAG2.DataFieldMapping = "";
            this.txtCITI_FLAG2.Enabled = false;
            this.txtCITI_FLAG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCITI_FLAG2.GetRecordsOnUpDownKeys = false;
            this.txtCITI_FLAG2.IsDate = false;
            this.txtCITI_FLAG2.Location = new System.Drawing.Point(250, 326);
            this.txtCITI_FLAG2.Margin = new System.Windows.Forms.Padding(4);
            this.txtCITI_FLAG2.MaxLength = 15;
            this.txtCITI_FLAG2.Name = "txtCITI_FLAG2";
            this.txtCITI_FLAG2.NumberFormat = "###,###,##0.00";
            this.txtCITI_FLAG2.Postfix = "";
            this.txtCITI_FLAG2.Prefix = "";
            this.txtCITI_FLAG2.Size = new System.Drawing.Size(133, 24);
            this.txtCITI_FLAG2.SkipValidation = false;
            this.txtCITI_FLAG2.TabIndex = 11;
            this.txtCITI_FLAG2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCity
            // 
            this.txtCity.AllowSpace = true;
            this.txtCity.AssociatedLookUpName = "";
            this.txtCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.ContinuationTextBox = null;
            this.txtCity.CustomEnabled = true;
            this.txtCity.DataFieldMapping = "";
            this.txtCity.Enabled = false;
            this.txtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.GetRecordsOnUpDownKeys = false;
            this.txtCity.IsDate = false;
            this.txtCity.IsRequired = true;
            this.txtCity.Location = new System.Drawing.Point(250, 77);
            this.txtCity.Margin = new System.Windows.Forms.Padding(4);
            this.txtCity.MaxLength = 15;
            this.txtCity.Name = "txtCity";
            this.txtCity.NumberFormat = "###,###,##0.00";
            this.txtCity.Postfix = "";
            this.txtCity.Prefix = "";
            this.txtCity.Size = new System.Drawing.Size(133, 24);
            this.txtCity.SkipValidation = false;
            this.txtCity.TabIndex = 2;
            this.txtCity.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(18, 298);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(154, 18);
            this.label15.TabIndex = 92;
            this.label15.Text = "Regret/Non Regret ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(18, 113);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 18);
            this.label16.TabIndex = 74;
            this.label16.Text = "Designation ";
            // 
            // txtCurrAnnual
            // 
            this.txtCurrAnnual.AllowSpace = true;
            this.txtCurrAnnual.AssociatedLookUpName = "";
            this.txtCurrAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrAnnual.ContinuationTextBox = null;
            this.txtCurrAnnual.CustomEnabled = true;
            this.txtCurrAnnual.DataFieldMapping = "";
            this.txtCurrAnnual.Enabled = false;
            this.txtCurrAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrAnnual.GetRecordsOnUpDownKeys = false;
            this.txtCurrAnnual.IsDate = false;
            this.txtCurrAnnual.IsRequired = true;
            this.txtCurrAnnual.Location = new System.Drawing.Point(250, 172);
            this.txtCurrAnnual.Margin = new System.Windows.Forms.Padding(4);
            this.txtCurrAnnual.MaxLength = 7;
            this.txtCurrAnnual.Name = "txtCurrAnnual";
            this.txtCurrAnnual.NumberFormat = "###,###,##0.00";
            this.txtCurrAnnual.Postfix = "";
            this.txtCurrAnnual.Prefix = "";
            this.txtCurrAnnual.ReadOnly = true;
            this.txtCurrAnnual.Size = new System.Drawing.Size(133, 24);
            this.txtCurrAnnual.SkipValidation = false;
            this.txtCurrAnnual.TabIndex = 6;
            this.txtCurrAnnual.TabStop = false;
            this.txtCurrAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrAnnual.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(18, 145);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 18);
            this.label17.TabIndex = 76;
            this.label17.Text = "Department ";
            // 
            // txtPRDept
            // 
            this.txtPRDept.AllowSpace = true;
            this.txtPRDept.AssociatedLookUpName = "";
            this.txtPRDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRDept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRDept.ContinuationTextBox = null;
            this.txtPRDept.CustomEnabled = true;
            this.txtPRDept.DataFieldMapping = "";
            this.txtPRDept.Enabled = false;
            this.txtPRDept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRDept.GetRecordsOnUpDownKeys = false;
            this.txtPRDept.IsDate = false;
            this.txtPRDept.Location = new System.Drawing.Point(250, 141);
            this.txtPRDept.Margin = new System.Windows.Forms.Padding(4);
            this.txtPRDept.MaxLength = 15;
            this.txtPRDept.Name = "txtPRDept";
            this.txtPRDept.NumberFormat = "###,###,##0.00";
            this.txtPRDept.Postfix = "";
            this.txtPRDept.Prefix = "";
            this.txtPRDept.Size = new System.Drawing.Size(133, 24);
            this.txtPRDept.SkipValidation = false;
            this.txtPRDept.TabIndex = 5;
            this.txtPRDept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(18, 176);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(194, 18);
            this.label19.TabIndex = 78;
            this.label19.Text = "Current Annual Package ";
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "";
            this.slTextBox3.Enabled = false;
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.IsRequired = true;
            this.slTextBox3.Location = new System.Drawing.Point(397, 111);
            this.slTextBox3.Margin = new System.Windows.Forms.Padding(4);
            this.slTextBox3.MaxLength = 3;
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.Size = new System.Drawing.Size(81, 24);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 4;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPR_EFFECTIVE
            // 
            this.txtPR_EFFECTIVE.CustomEnabled = true;
            this.txtPR_EFFECTIVE.CustomFormat = "dd/MM/yyyy";
            this.txtPR_EFFECTIVE.DataFieldMapping = "";
            this.txtPR_EFFECTIVE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_EFFECTIVE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtPR_EFFECTIVE.HasChanges = true;
            this.txtPR_EFFECTIVE.IsRequired = true;
            this.txtPR_EFFECTIVE.Location = new System.Drawing.Point(250, 264);
            this.txtPR_EFFECTIVE.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_EFFECTIVE.Name = "txtPR_EFFECTIVE";
            this.txtPR_EFFECTIVE.NullValue = " ";
            this.txtPR_EFFECTIVE.Size = new System.Drawing.Size(132, 23);
            this.txtPR_EFFECTIVE.TabIndex = 9;
            this.txtPR_EFFECTIVE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(340, 113);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 18);
            this.label18.TabIndex = 88;
            this.label18.Text = "Level ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(18, 205);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(171, 18);
            this.label21.TabIndex = 80;
            this.label21.Text = "New Annual Package ";
            // 
            // txtDesg
            // 
            this.txtDesg.AllowSpace = true;
            this.txtDesg.AssociatedLookUpName = "";
            this.txtDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesg.ContinuationTextBox = null;
            this.txtDesg.CustomEnabled = true;
            this.txtDesg.DataFieldMapping = "";
            this.txtDesg.Enabled = false;
            this.txtDesg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesg.GetRecordsOnUpDownKeys = false;
            this.txtDesg.IsDate = false;
            this.txtDesg.IsRequired = true;
            this.txtDesg.Location = new System.Drawing.Point(250, 109);
            this.txtDesg.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesg.MaxLength = 3;
            this.txtDesg.Name = "txtDesg";
            this.txtDesg.NumberFormat = "###,###,##0.00";
            this.txtDesg.Postfix = "";
            this.txtDesg.Prefix = "";
            this.txtDesg.Size = new System.Drawing.Size(81, 24);
            this.txtDesg.SkipValidation = false;
            this.txtDesg.TabIndex = 3;
            this.txtDesg.TextType = CrplControlLibrary.TextType.String;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(18, 236);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(160, 18);
            this.label22.TabIndex = 81;
            this.label22.Text = "ASR Currency Type ";
            // 
            // txtNewAnnual
            // 
            this.txtNewAnnual.AllowSpace = true;
            this.txtNewAnnual.AssociatedLookUpName = "";
            this.txtNewAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNewAnnual.ContinuationTextBox = null;
            this.txtNewAnnual.CustomEnabled = true;
            this.txtNewAnnual.DataFieldMapping = "";
            this.txtNewAnnual.Enabled = false;
            this.txtNewAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewAnnual.GetRecordsOnUpDownKeys = false;
            this.txtNewAnnual.IsDate = false;
            this.txtNewAnnual.IsRequired = true;
            this.txtNewAnnual.Location = new System.Drawing.Point(250, 202);
            this.txtNewAnnual.Margin = new System.Windows.Forms.Padding(4);
            this.txtNewAnnual.MaxLength = 7;
            this.txtNewAnnual.Name = "txtNewAnnual";
            this.txtNewAnnual.NumberFormat = "###,###,##0.00";
            this.txtNewAnnual.Postfix = "";
            this.txtNewAnnual.Prefix = "";
            this.txtNewAnnual.Size = new System.Drawing.Size(133, 24);
            this.txtNewAnnual.SkipValidation = false;
            this.txtNewAnnual.TabIndex = 7;
            this.txtNewAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNewAnnual.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtCITI_FLAG1
            // 
            this.txtCITI_FLAG1.AllowSpace = true;
            this.txtCITI_FLAG1.AssociatedLookUpName = "";
            this.txtCITI_FLAG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCITI_FLAG1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCITI_FLAG1.ContinuationTextBox = null;
            this.txtCITI_FLAG1.CustomEnabled = true;
            this.txtCITI_FLAG1.DataFieldMapping = "";
            this.txtCITI_FLAG1.Enabled = false;
            this.txtCITI_FLAG1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCITI_FLAG1.GetRecordsOnUpDownKeys = false;
            this.txtCITI_FLAG1.IsDate = false;
            this.txtCITI_FLAG1.Location = new System.Drawing.Point(250, 294);
            this.txtCITI_FLAG1.Margin = new System.Windows.Forms.Padding(4);
            this.txtCITI_FLAG1.MaxLength = 10;
            this.txtCITI_FLAG1.Name = "txtCITI_FLAG1";
            this.txtCITI_FLAG1.NumberFormat = "###,###,##0.00";
            this.txtCITI_FLAG1.Postfix = "";
            this.txtCITI_FLAG1.Prefix = "";
            this.txtCITI_FLAG1.Size = new System.Drawing.Size(133, 24);
            this.txtCITI_FLAG1.SkipValidation = false;
            this.txtCITI_FLAG1.TabIndex = 10;
            this.txtCITI_FLAG1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrency
            // 
            this.txtCurrency.AllowSpace = true;
            this.txtCurrency.AssociatedLookUpName = "";
            this.txtCurrency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrency.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrency.ContinuationTextBox = null;
            this.txtCurrency.CustomEnabled = true;
            this.txtCurrency.DataFieldMapping = "";
            this.txtCurrency.Enabled = false;
            this.txtCurrency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrency.GetRecordsOnUpDownKeys = false;
            this.txtCurrency.IsDate = false;
            this.txtCurrency.IsRequired = true;
            this.txtCurrency.Location = new System.Drawing.Point(250, 232);
            this.txtCurrency.Margin = new System.Windows.Forms.Padding(4);
            this.txtCurrency.MaxLength = 10;
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.NumberFormat = "###,###,##0.00";
            this.txtCurrency.Postfix = "";
            this.txtCurrency.Prefix = "";
            this.txtCurrency.Size = new System.Drawing.Size(133, 24);
            this.txtCurrency.SkipValidation = false;
            this.txtCurrency.TabIndex = 8;
            this.txtCurrency.TextType = CrplControlLibrary.TextType.String;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(18, 268);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(118, 18);
            this.label24.TabIndex = 84;
            this.label24.Text = "Effective Date ";
            // 
            // btnAproved
            // 
            this.btnAproved.Location = new System.Drawing.Point(679, 213);
            this.btnAproved.Name = "btnAproved";
            this.btnAproved.Size = new System.Drawing.Size(97, 40);
            this.btnAproved.TabIndex = 101;
            this.btnAproved.Text = "Approve";
            this.btnAproved.UseVisualStyleBackColor = true;
            this.btnAproved.Click += new System.EventHandler(this.btnAproved_Click);
            // 
            // btnReject
            // 
            this.btnReject.Location = new System.Drawing.Point(566, 213);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(97, 40);
            this.btnReject.TabIndex = 100;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(454, 213);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(97, 40);
            this.btnClose.TabIndex = 99;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CHRIS_Personnel_FastOrISTransfer_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 707);
            this.Controls.Add(this.pnlBlkTransfer);
            this.Controls.Add(this.pnlDetail);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_FastOrISTransfer_Detail";
            this.Text = "CHRIS_Personnel_FastOrISTransfer_Detail";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_FastOrISTransfer_Detail_Load);
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlBlkTransfer.ResumeLayout(false);
            this.pnlBlkTransfer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label6;
        public CrplControlLibrary.SLTextBox txtLevel;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtPR_Func2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtPR_Func1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtPR_DESG;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtPRTransfer;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label8;
        public CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtID;
        private COMMON.SLCONTROLS.SLPanelSimple pnlBlkTransfer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtPR_REMARKS;
        private CrplControlLibrary.SLTextBox txtCountry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtCITI_FLAG2;
        private CrplControlLibrary.SLTextBox txtCity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtCurrAnnual;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtPRDept;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLDatePicker txtPR_EFFECTIVE;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtDesg;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txtNewAnnual;
        private CrplControlLibrary.SLTextBox txtCITI_FLAG1;
        private CrplControlLibrary.SLTextBox txtCurrency;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnAproved;
    }
}