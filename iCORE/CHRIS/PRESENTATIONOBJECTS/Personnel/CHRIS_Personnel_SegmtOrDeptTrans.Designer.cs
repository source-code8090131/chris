namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_SegmtOrDeptTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_SegmtOrDeptTrans));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtMode = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.W_VAL1 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtPRTransferDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtPR_TERMIN_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.JoiningDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtSum1 = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnRep = new CrplControlLibrary.LookupButton(this.components);
            this.txtPR_TRANSFER = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_CLOSE_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.txtW_GOAL = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.dtW_GOAL_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtW_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfunc1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPR_Func2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.dtpPR_EFFECTIVE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtW_REP = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txtPR_Func1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESG = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtPR_NEW_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txtfunc2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.txtNoINCR = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlTblDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_MENU_OPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Seg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dept = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.Contrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_TR_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_EFFECTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTblDept2 = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVDept2 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_SEGMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DEPT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CONTRIB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtUserName = new System.Windows.Forms.Label();
            this.btnAuth = new System.Windows.Forms.Button();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.pnlTblDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDept)).BeginInit();
            this.pnlTblDept2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDept2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtOption.Leave += new System.EventHandler(this.txtOption_Leave);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 748);
            this.panel1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtMode);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Location = new System.Drawing.Point(17, 102);
            this.pnlHead.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(859, 110);
            this.pnlHead.TabIndex = 11;
            // 
            // txtMode
            // 
            this.txtMode.AllowSpace = true;
            this.txtMode.AssociatedLookUpName = "";
            this.txtMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMode.ContinuationTextBox = null;
            this.txtMode.CustomEnabled = true;
            this.txtMode.DataFieldMapping = "";
            this.txtMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMode.GetRecordsOnUpDownKeys = false;
            this.txtMode.IsDate = false;
            this.txtMode.Location = new System.Drawing.Point(241, 4);
            this.txtMode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMode.MaxLength = 30;
            this.txtMode.Name = "txtMode";
            this.txtMode.NumberFormat = "###,###,##0.00";
            this.txtMode.Postfix = "";
            this.txtMode.Prefix = "";
            this.txtMode.ReadOnly = true;
            this.txtMode.Size = new System.Drawing.Size(47, 24);
            this.txtMode.SkipValidation = false;
            this.txtMode.TabIndex = 68;
            this.txtMode.TabStop = false;
            this.txtMode.TextType = CrplControlLibrary.TextType.String;
            this.txtMode.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(723, 52);
            this.txtDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(106, 24);
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(723, 23);
            this.txtCurrOption.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(106, 24);
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(660, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 18);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(644, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(112, 52);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(106, 24);
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(112, 23);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(106, 24);
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(16, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(15, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(335, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 25);
            this.label5.TabIndex = 19;
            this.label5.Text = "Personnel   System";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(249, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(403, 22);
            this.label6.TabIndex = 20;
            this.label6.Text = "Segment To Segment/ Dept To Dept Transfer";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.W_VAL1);
            this.pnlDetail.Controls.Add(this.dtPRTransferDate);
            this.pnlDetail.Controls.Add(this.dtPR_TERMIN_DATE);
            this.pnlDetail.Controls.Add(this.JoiningDate);
            this.pnlDetail.Controls.Add(this.txtSum1);
            this.pnlDetail.Controls.Add(this.lbtnRep);
            this.pnlDetail.Controls.Add(this.txtPR_TRANSFER);
            this.pnlDetail.Controls.Add(this.txtPR_CLOSE_FLAG);
            this.pnlDetail.Controls.Add(this.txtW_GOAL);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.dtW_GOAL_DATE);
            this.pnlDetail.Controls.Add(this.txtW_Name);
            this.pnlDetail.Controls.Add(this.txtfunc1);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtPR_Func2);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.slTextBox1);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.dtpPR_EFFECTIVE);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtW_REP);
            this.pnlDetail.Controls.Add(this.label22);
            this.pnlDetail.Controls.Add(this.txtPR_Func1);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtPR_LEVEL);
            this.pnlDetail.Controls.Add(this.txtPR_DESG);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtPR_NEW_BRANCH);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.txtfunc2);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtNoINCR);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand";
            this.pnlDetail.Location = new System.Drawing.Point(15, 219);
            this.pnlDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(572, 495);
            this.pnlDetail.SPName = "CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER";
            this.pnlDetail.TabIndex = 12;
            this.pnlDetail.TabStop = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 240);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(560, 17);
            this.label18.TabIndex = 128;
            this.label18.Text = "_____________________________________________________________________";
            // 
            // W_VAL1
            // 
            this.W_VAL1.AllowSpace = true;
            this.W_VAL1.AssociatedLookUpName = "";
            this.W_VAL1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_VAL1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_VAL1.ContinuationTextBox = null;
            this.W_VAL1.CustomEnabled = true;
            this.W_VAL1.DataFieldMapping = "";
            this.W_VAL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_VAL1.GetRecordsOnUpDownKeys = false;
            this.W_VAL1.IsDate = false;
            this.W_VAL1.Location = new System.Drawing.Point(409, 85);
            this.W_VAL1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.W_VAL1.MaxLength = 1;
            this.W_VAL1.Name = "W_VAL1";
            this.W_VAL1.NumberFormat = "###,###,##0.00";
            this.W_VAL1.Postfix = "";
            this.W_VAL1.Prefix = "";
            this.W_VAL1.Size = new System.Drawing.Size(51, 24);
            this.W_VAL1.SkipValidation = false;
            this.W_VAL1.TabIndex = 125;
            this.W_VAL1.TabStop = false;
            this.W_VAL1.TextType = CrplControlLibrary.TextType.String;
            this.W_VAL1.Visible = false;
            // 
            // dtPRTransferDate
            // 
            this.dtPRTransferDate.CustomEnabled = true;
            this.dtPRTransferDate.CustomFormat = "dd/MM/yyyy";
            this.dtPRTransferDate.DataFieldMapping = "PRTransferDate";
            this.dtPRTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPRTransferDate.HasChanges = true;
            this.dtPRTransferDate.Location = new System.Drawing.Point(496, 57);
            this.dtPRTransferDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtPRTransferDate.Name = "dtPRTransferDate";
            this.dtPRTransferDate.NullValue = " ";
            this.dtPRTransferDate.Size = new System.Drawing.Size(69, 22);
            this.dtPRTransferDate.TabIndex = 124;
            this.dtPRTransferDate.TabStop = false;
            this.dtPRTransferDate.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.dtPRTransferDate.Visible = false;
            // 
            // dtPR_TERMIN_DATE
            // 
            this.dtPR_TERMIN_DATE.CustomEnabled = true;
            this.dtPR_TERMIN_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtPR_TERMIN_DATE.DataFieldMapping = "PR_TERMIN_DATE";
            this.dtPR_TERMIN_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPR_TERMIN_DATE.HasChanges = true;
            this.dtPR_TERMIN_DATE.Location = new System.Drawing.Point(437, 28);
            this.dtPR_TERMIN_DATE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtPR_TERMIN_DATE.Name = "dtPR_TERMIN_DATE";
            this.dtPR_TERMIN_DATE.NullValue = " ";
            this.dtPR_TERMIN_DATE.Size = new System.Drawing.Size(128, 22);
            this.dtPR_TERMIN_DATE.TabIndex = 123;
            this.dtPR_TERMIN_DATE.TabStop = false;
            this.dtPR_TERMIN_DATE.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.dtPR_TERMIN_DATE.Visible = false;
            // 
            // JoiningDate
            // 
            this.JoiningDate.CustomEnabled = true;
            this.JoiningDate.CustomFormat = "dd/MM/yyyy";
            this.JoiningDate.DataFieldMapping = "pr_joining_date";
            this.JoiningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.JoiningDate.HasChanges = true;
            this.JoiningDate.Location = new System.Drawing.Point(465, 85);
            this.JoiningDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.JoiningDate.Name = "JoiningDate";
            this.JoiningDate.NullValue = " ";
            this.JoiningDate.Size = new System.Drawing.Size(96, 22);
            this.JoiningDate.TabIndex = 122;
            this.JoiningDate.TabStop = false;
            this.JoiningDate.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.JoiningDate.Visible = false;
            // 
            // txtSum1
            // 
            this.txtSum1.AllowSpace = true;
            this.txtSum1.AssociatedLookUpName = "";
            this.txtSum1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSum1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSum1.ContinuationTextBox = null;
            this.txtSum1.CustomEnabled = true;
            this.txtSum1.DataFieldMapping = "";
            this.txtSum1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSum1.GetRecordsOnUpDownKeys = false;
            this.txtSum1.IsDate = false;
            this.txtSum1.Location = new System.Drawing.Point(355, 85);
            this.txtSum1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSum1.MaxLength = 1;
            this.txtSum1.Name = "txtSum1";
            this.txtSum1.NumberFormat = "###,###,##0.00";
            this.txtSum1.Postfix = "";
            this.txtSum1.Prefix = "";
            this.txtSum1.Size = new System.Drawing.Size(51, 24);
            this.txtSum1.SkipValidation = false;
            this.txtSum1.TabIndex = 121;
            this.txtSum1.TabStop = false;
            this.txtSum1.TextType = CrplControlLibrary.TextType.String;
            this.txtSum1.Visible = false;
            // 
            // lbtnRep
            // 
            this.lbtnRep.ActionLOVExists = "W_REP_LOV_Exists";
            this.lbtnRep.ActionType = "W_REP_LOV";
            this.lbtnRep.ConditionalFields = "";
            this.lbtnRep.CustomEnabled = true;
            this.lbtnRep.DataFieldMapping = "";
            this.lbtnRep.DependentLovControls = "";
            this.lbtnRep.HiddenColumns = "";
            this.lbtnRep.Image = ((System.Drawing.Image)(resources.GetObject("lbtnRep.Image")));
            this.lbtnRep.LoadDependentEntities = true;
            this.lbtnRep.Location = new System.Drawing.Point(281, 390);
            this.lbtnRep.LookUpTitle = null;
            this.lbtnRep.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbtnRep.Name = "lbtnRep";
            this.lbtnRep.Size = new System.Drawing.Size(35, 26);
            this.lbtnRep.SkipValidationOnLeave = true;
            this.lbtnRep.SPName = "CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER";
            this.lbtnRep.TabIndex = 119;
            this.lbtnRep.TabStop = false;
            this.lbtnRep.UseVisualStyleBackColor = true;
            // 
            // txtPR_TRANSFER
            // 
            this.txtPR_TRANSFER.AllowSpace = true;
            this.txtPR_TRANSFER.AssociatedLookUpName = "";
            this.txtPR_TRANSFER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_TRANSFER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_TRANSFER.ContinuationTextBox = null;
            this.txtPR_TRANSFER.CustomEnabled = true;
            this.txtPR_TRANSFER.DataFieldMapping = "";
            this.txtPR_TRANSFER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_TRANSFER.GetRecordsOnUpDownKeys = false;
            this.txtPR_TRANSFER.IsDate = false;
            this.txtPR_TRANSFER.Location = new System.Drawing.Point(300, 85);
            this.txtPR_TRANSFER.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_TRANSFER.MaxLength = 1;
            this.txtPR_TRANSFER.Name = "txtPR_TRANSFER";
            this.txtPR_TRANSFER.NumberFormat = "###,###,##0.00";
            this.txtPR_TRANSFER.Postfix = "";
            this.txtPR_TRANSFER.Prefix = "";
            this.txtPR_TRANSFER.Size = new System.Drawing.Size(51, 24);
            this.txtPR_TRANSFER.SkipValidation = false;
            this.txtPR_TRANSFER.TabIndex = 118;
            this.txtPR_TRANSFER.TabStop = false;
            this.txtPR_TRANSFER.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_TRANSFER.Visible = false;
            // 
            // txtPR_CLOSE_FLAG
            // 
            this.txtPR_CLOSE_FLAG.AllowSpace = true;
            this.txtPR_CLOSE_FLAG.AssociatedLookUpName = "";
            this.txtPR_CLOSE_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_CLOSE_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_CLOSE_FLAG.ContinuationTextBox = null;
            this.txtPR_CLOSE_FLAG.CustomEnabled = true;
            this.txtPR_CLOSE_FLAG.DataFieldMapping = "";
            this.txtPR_CLOSE_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_CLOSE_FLAG.GetRecordsOnUpDownKeys = false;
            this.txtPR_CLOSE_FLAG.IsDate = false;
            this.txtPR_CLOSE_FLAG.Location = new System.Drawing.Point(244, 85);
            this.txtPR_CLOSE_FLAG.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_CLOSE_FLAG.MaxLength = 1;
            this.txtPR_CLOSE_FLAG.Name = "txtPR_CLOSE_FLAG";
            this.txtPR_CLOSE_FLAG.NumberFormat = "###,###,##0.00";
            this.txtPR_CLOSE_FLAG.Postfix = "";
            this.txtPR_CLOSE_FLAG.Prefix = "";
            this.txtPR_CLOSE_FLAG.Size = new System.Drawing.Size(51, 24);
            this.txtPR_CLOSE_FLAG.SkipValidation = false;
            this.txtPR_CLOSE_FLAG.TabIndex = 117;
            this.txtPR_CLOSE_FLAG.TabStop = false;
            this.txtPR_CLOSE_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_CLOSE_FLAG.Visible = false;
            // 
            // txtW_GOAL
            // 
            this.txtW_GOAL.AllowSpace = true;
            this.txtW_GOAL.AssociatedLookUpName = "lbtnDesg";
            this.txtW_GOAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_GOAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_GOAL.ContinuationTextBox = null;
            this.txtW_GOAL.CustomEnabled = true;
            this.txtW_GOAL.DataFieldMapping = "W_GOAL";
            this.txtW_GOAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_GOAL.GetRecordsOnUpDownKeys = false;
            this.txtW_GOAL.IsDate = false;
            this.txtW_GOAL.Location = new System.Drawing.Point(201, 420);
            this.txtW_GOAL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtW_GOAL.MaxLength = 3;
            this.txtW_GOAL.Name = "txtW_GOAL";
            this.txtW_GOAL.NumberFormat = "###,###,##0.00";
            this.txtW_GOAL.Postfix = "";
            this.txtW_GOAL.Prefix = "";
            this.txtW_GOAL.Size = new System.Drawing.Size(75, 24);
            this.txtW_GOAL.SkipValidation = false;
            this.txtW_GOAL.TabIndex = 7;
            this.txtW_GOAL.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtW_GOAL, "Enter Yes OR No");
            this.txtW_GOAL.Validating += new System.ComponentModel.CancelEventHandler(this.txtW_GOAL_Validating);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(281, 418);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 25);
            this.label17.TabIndex = 114;
            this.label17.Text = "Rcvd. Date :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtW_GOAL_DATE
            // 
            this.dtW_GOAL_DATE.CustomEnabled = true;
            this.dtW_GOAL_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtW_GOAL_DATE.DataFieldMapping = "W_GOAL_DATE";
            this.dtW_GOAL_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtW_GOAL_DATE.HasChanges = true;
            this.dtW_GOAL_DATE.IsRequired = true;
            this.dtW_GOAL_DATE.Location = new System.Drawing.Point(393, 420);
            this.dtW_GOAL_DATE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtW_GOAL_DATE.Name = "dtW_GOAL_DATE";
            this.dtW_GOAL_DATE.NullValue = " ";
            this.dtW_GOAL_DATE.Size = new System.Drawing.Size(117, 22);
            this.dtW_GOAL_DATE.TabIndex = 8;
            this.dtW_GOAL_DATE.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtW_GOAL_DATE.Leave += new System.EventHandler(this.dtW_GOAL_DATE_Leave);
            this.dtW_GOAL_DATE.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.dtW_GOAL_DATE_PreviewKeyDown);
            this.dtW_GOAL_DATE.Validating += new System.ComponentModel.CancelEventHandler(this.dtW_GOAL_DATE_Validating);
            // 
            // txtW_Name
            // 
            this.txtW_Name.AllowSpace = true;
            this.txtW_Name.AssociatedLookUpName = "lbtnRep";
            this.txtW_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_Name.ContinuationTextBox = null;
            this.txtW_Name.CustomEnabled = true;
            this.txtW_Name.DataFieldMapping = "W_REPName";
            this.txtW_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_Name.GetRecordsOnUpDownKeys = false;
            this.txtW_Name.IsDate = false;
            this.txtW_Name.Location = new System.Drawing.Point(323, 391);
            this.txtW_Name.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtW_Name.MaxLength = 30;
            this.txtW_Name.Name = "txtW_Name";
            this.txtW_Name.NumberFormat = "###,###,##0.00";
            this.txtW_Name.Postfix = "";
            this.txtW_Name.Prefix = "";
            this.txtW_Name.ReadOnly = true;
            this.txtW_Name.Size = new System.Drawing.Size(163, 24);
            this.txtW_Name.SkipValidation = false;
            this.txtW_Name.TabIndex = 112;
            this.txtW_Name.TabStop = false;
            this.txtW_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtfunc1
            // 
            this.txtfunc1.AllowSpace = true;
            this.txtfunc1.AssociatedLookUpName = "";
            this.txtfunc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunc1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunc1.ContinuationTextBox = null;
            this.txtfunc1.CustomEnabled = true;
            this.txtfunc1.DataFieldMapping = "PR_FUNC_1";
            this.txtfunc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunc1.GetRecordsOnUpDownKeys = false;
            this.txtfunc1.IsDate = false;
            this.txtfunc1.Location = new System.Drawing.Point(201, 305);
            this.txtfunc1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtfunc1.MaxLength = 30;
            this.txtfunc1.Name = "txtfunc1";
            this.txtfunc1.NumberFormat = "###,###,##0.00";
            this.txtfunc1.Postfix = "";
            this.txtfunc1.Prefix = "";
            this.txtfunc1.Size = new System.Drawing.Size(285, 24);
            this.txtfunc1.SkipValidation = true;
            this.txtfunc1.TabIndex = 3;
            this.txtfunc1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtfunc1, "Enter New Functional Title");
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(20, 303);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(183, 25);
            this.label10.TabIndex = 110;
            this.label10.Text = "1. Functional Title :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(220, 277);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(289, 25);
            this.label9.TabIndex = 109;
            this.label9.Text = "Transfer Information";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtPR_Func2
            // 
            this.txtPR_Func2.AllowSpace = true;
            this.txtPR_Func2.AssociatedLookUpName = "";
            this.txtPR_Func2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func2.ContinuationTextBox = null;
            this.txtPR_Func2.CustomEnabled = true;
            this.txtPR_Func2.DataFieldMapping = "";
            this.txtPR_Func2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func2.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func2.IsDate = false;
            this.txtPR_Func2.Location = new System.Drawing.Point(168, 170);
            this.txtPR_Func2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_Func2.MaxLength = 30;
            this.txtPR_Func2.Name = "txtPR_Func2";
            this.txtPR_Func2.NumberFormat = "###,###,##0.00";
            this.txtPR_Func2.Postfix = "";
            this.txtPR_Func2.Prefix = "";
            this.txtPR_Func2.ReadOnly = true;
            this.txtPR_Func2.Size = new System.Drawing.Size(146, 24);
            this.txtPR_Func2.SkipValidation = false;
            this.txtPR_Func2.TabIndex = 108;
            this.txtPR_Func2.TabStop = false;
            this.txtPR_Func2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_Func2, "[M]arried or [S]ingle");
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(85, 166);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 25);
            this.label7.TabIndex = 107;
            this.label7.Text = "Title :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "Pr_name_Last";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(364, 57);
            this.slTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.slTextBox1.MaxLength = 20;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(131, 24);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 106;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(49, 417);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(149, 25);
            this.label27.TabIndex = 104;
            this.label27.Text = "4. Goals Rcvd. :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpPR_EFFECTIVE
            // 
            this.dtpPR_EFFECTIVE.CustomEnabled = true;
            this.dtpPR_EFFECTIVE.CustomFormat = "dd/MM/yyyy";
            this.dtpPR_EFFECTIVE.DataFieldMapping = "PR_EFFECTIVE";
            this.dtpPR_EFFECTIVE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPR_EFFECTIVE.HasChanges = true;
            this.dtpPR_EFFECTIVE.IsRequired = true;
            this.dtpPR_EFFECTIVE.Location = new System.Drawing.Point(201, 362);
            this.dtpPR_EFFECTIVE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpPR_EFFECTIVE.Name = "dtpPR_EFFECTIVE";
            this.dtpPR_EFFECTIVE.NullValue = " ";
            this.dtpPR_EFFECTIVE.Size = new System.Drawing.Size(163, 22);
            this.dtpPR_EFFECTIVE.TabIndex = 5;
            this.dtpPR_EFFECTIVE.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpPR_EFFECTIVE.Validating += new System.ComponentModel.CancelEventHandler(this.dtpPR_EFFECTIVE_Validating);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(45, 389);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(153, 25);
            this.label23.TabIndex = 95;
            this.label23.Text = "3. Reporting To :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtW_REP
            // 
            this.txtW_REP.AllowSpace = true;
            this.txtW_REP.AssociatedLookUpName = "lbtnRep";
            this.txtW_REP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_REP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_REP.ContinuationTextBox = null;
            this.txtW_REP.CustomEnabled = true;
            this.txtW_REP.DataFieldMapping = "W_REP";
            this.txtW_REP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_REP.GetRecordsOnUpDownKeys = false;
            this.txtW_REP.IsDate = false;
            this.txtW_REP.IsRequired = true;
            this.txtW_REP.Location = new System.Drawing.Point(201, 390);
            this.txtW_REP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtW_REP.MaxLength = 6;
            this.txtW_REP.Name = "txtW_REP";
            this.txtW_REP.NumberFormat = "###,###,##0.00";
            this.txtW_REP.Postfix = "";
            this.txtW_REP.Prefix = "";
            this.txtW_REP.Size = new System.Drawing.Size(75, 24);
            this.txtW_REP.SkipValidation = false;
            this.txtW_REP.TabIndex = 6;
            this.txtW_REP.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtW_REP, "Press <F9> Key to Display The List");
            this.txtW_REP.Leave += new System.EventHandler(this.txtW_REP_Leave);
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(16, 359);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(183, 25);
            this.label22.TabIndex = 93;
            this.label22.Text = "2. Effective Date :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func1
            // 
            this.txtPR_Func1.AllowSpace = true;
            this.txtPR_Func1.AssociatedLookUpName = "";
            this.txtPR_Func1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func1.ContinuationTextBox = null;
            this.txtPR_Func1.CustomEnabled = true;
            this.txtPR_Func1.DataFieldMapping = "";
            this.txtPR_Func1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func1.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func1.IsDate = false;
            this.txtPR_Func1.Location = new System.Drawing.Point(168, 142);
            this.txtPR_Func1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_Func1.MaxLength = 30;
            this.txtPR_Func1.Name = "txtPR_Func1";
            this.txtPR_Func1.NumberFormat = "###,###,##0.00";
            this.txtPR_Func1.Postfix = "";
            this.txtPR_Func1.Prefix = "";
            this.txtPR_Func1.ReadOnly = true;
            this.txtPR_Func1.Size = new System.Drawing.Size(146, 24);
            this.txtPR_Func1.SkipValidation = false;
            this.txtPR_Func1.TabIndex = 88;
            this.txtPR_Func1.TabStop = false;
            this.txtPR_Func1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_Func1, "[M]arried or [S]ingle");
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(32, 140);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(132, 25);
            this.label14.TabIndex = 87;
            this.label14.Text = "5. Functional :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(320, 113);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 25);
            this.label12.TabIndex = 86;
            this.label12.Text = "Transfer Type :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_LEVEL
            // 
            this.txtPR_LEVEL.AllowSpace = true;
            this.txtPR_LEVEL.AssociatedLookUpName = "";
            this.txtPR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL.ContinuationTextBox = null;
            this.txtPR_LEVEL.CustomEnabled = true;
            this.txtPR_LEVEL.DataFieldMapping = "";
            this.txtPR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL.IsDate = false;
            this.txtPR_LEVEL.Location = new System.Drawing.Point(168, 113);
            this.txtPR_LEVEL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_LEVEL.MaxLength = 3;
            this.txtPR_LEVEL.Name = "txtPR_LEVEL";
            this.txtPR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL.Postfix = "";
            this.txtPR_LEVEL.Prefix = "";
            this.txtPR_LEVEL.ReadOnly = true;
            this.txtPR_LEVEL.Size = new System.Drawing.Size(110, 24);
            this.txtPR_LEVEL.SkipValidation = false;
            this.txtPR_LEVEL.TabIndex = 84;
            this.txtPR_LEVEL.TabStop = false;
            this.txtPR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPR_DESG
            // 
            this.txtPR_DESG.AllowSpace = true;
            this.txtPR_DESG.AssociatedLookUpName = "lbtnPNo";
            this.txtPR_DESG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESG.ContinuationTextBox = null;
            this.txtPR_DESG.CustomEnabled = true;
            this.txtPR_DESG.DataFieldMapping = "";
            this.txtPR_DESG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESG.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESG.IsDate = false;
            this.txtPR_DESG.Location = new System.Drawing.Point(168, 85);
            this.txtPR_DESG.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_DESG.MaxLength = 10;
            this.txtPR_DESG.Name = "txtPR_DESG";
            this.txtPR_DESG.NumberFormat = "###,###,##0.00";
            this.txtPR_DESG.Postfix = "";
            this.txtPR_DESG.Prefix = "";
            this.txtPR_DESG.ReadOnly = true;
            this.txtPR_DESG.Size = new System.Drawing.Size(73, 24);
            this.txtPR_DESG.SkipValidation = false;
            this.txtPR_DESG.TabIndex = 80;
            this.txtPR_DESG.TabStop = false;
            this.txtPR_DESG.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_DESG, "Press <F9> Key to Display The List");
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(13, 82);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 25);
            this.label13.TabIndex = 82;
            this.label13.Text = "3. Designation :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_NEW_BRANCH
            // 
            this.txtPR_NEW_BRANCH.AllowSpace = true;
            this.txtPR_NEW_BRANCH.AssociatedLookUpName = "";
            this.txtPR_NEW_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_NEW_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_NEW_BRANCH.ContinuationTextBox = null;
            this.txtPR_NEW_BRANCH.CustomEnabled = true;
            this.txtPR_NEW_BRANCH.DataFieldMapping = "PR_NEW_BRANCH";
            this.txtPR_NEW_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_NEW_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txtPR_NEW_BRANCH.IsDate = false;
            this.txtPR_NEW_BRANCH.Location = new System.Drawing.Point(372, 28);
            this.txtPR_NEW_BRANCH.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPR_NEW_BRANCH.MaxLength = 3;
            this.txtPR_NEW_BRANCH.Name = "txtPR_NEW_BRANCH";
            this.txtPR_NEW_BRANCH.NumberFormat = "###,###,##0.00";
            this.txtPR_NEW_BRANCH.Postfix = "";
            this.txtPR_NEW_BRANCH.Prefix = "";
            this.txtPR_NEW_BRANCH.ReadOnly = true;
            this.txtPR_NEW_BRANCH.Size = new System.Drawing.Size(62, 24);
            this.txtPR_NEW_BRANCH.SkipValidation = false;
            this.txtPR_NEW_BRANCH.TabIndex = 74;
            this.txtPR_NEW_BRANCH.TabStop = false;
            this.txtPR_NEW_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(283, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 25);
            this.label15.TabIndex = 75;
            this.label15.Text = "Branch:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtfunc2
            // 
            this.txtfunc2.AllowSpace = true;
            this.txtfunc2.AssociatedLookUpName = "";
            this.txtfunc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunc2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunc2.ContinuationTextBox = null;
            this.txtfunc2.CustomEnabled = true;
            this.txtfunc2.DataFieldMapping = "PR_FUNC_2";
            this.txtfunc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunc2.GetRecordsOnUpDownKeys = false;
            this.txtfunc2.IsDate = false;
            this.txtfunc2.Location = new System.Drawing.Point(201, 334);
            this.txtfunc2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtfunc2.MaxLength = 30;
            this.txtfunc2.Name = "txtfunc2";
            this.txtfunc2.NumberFormat = "###,###,##0.00";
            this.txtfunc2.Postfix = "";
            this.txtfunc2.Prefix = "";
            this.txtfunc2.Size = new System.Drawing.Size(285, 24);
            this.txtfunc2.SkipValidation = true;
            this.txtfunc2.TabIndex = 4;
            this.txtfunc2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(55, 111);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 25);
            this.label16.TabIndex = 69;
            this.label16.Text = "4. Level :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNoINCR
            // 
            this.txtNoINCR.AllowSpace = true;
            this.txtNoINCR.AssociatedLookUpName = "";
            this.txtNoINCR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoINCR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoINCR.ContinuationTextBox = null;
            this.txtNoINCR.CustomEnabled = false;
            this.txtNoINCR.DataFieldMapping = "PR_TRANSFER_TYPE";
            this.txtNoINCR.Enabled = false;
            this.txtNoINCR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoINCR.GetRecordsOnUpDownKeys = false;
            this.txtNoINCR.IsDate = false;
            this.txtNoINCR.Location = new System.Drawing.Point(467, 113);
            this.txtNoINCR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNoINCR.MaxLength = 1;
            this.txtNoINCR.Name = "txtNoINCR";
            this.txtNoINCR.NumberFormat = "###,###,##0.00";
            this.txtNoINCR.Postfix = "";
            this.txtNoINCR.Prefix = "";
            this.txtNoINCR.Size = new System.Drawing.Size(51, 24);
            this.txtNoINCR.SkipValidation = false;
            this.txtNoINCR.TabIndex = 68;
            this.txtNoINCR.TabStop = false;
            this.txtNoINCR.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BackColor = System.Drawing.SystemColors.Control;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "pr_name_first";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(168, 57);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(193, 24);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(72, 57);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 25);
            this.label8.TabIndex = 26;
            this.label8.Text = "2. Name :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "Pr_P_No_LOV_Exists";
            this.lbtnPNo.ActionType = "Pr_P_No_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(244, 28);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(35, 26);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_TR_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(168, 28);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(73, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(3, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(163, 25);
            this.label11.TabIndex = 23;
            this.label11.Text = "1. Personnel No. :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(471, 145);
            this.txtID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(47, 24);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // pnlTblDept
            // 
            this.pnlTblDept.ConcurrentPanels = null;
            this.pnlTblDept.Controls.Add(this.DGVDept);
            this.pnlTblDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept.DependentPanels = null;
            this.pnlTblDept.DisableDependentLoad = false;
            this.pnlTblDept.EnableDelete = true;
            this.pnlTblDept.EnableInsert = true;
            this.pnlTblDept.EnableQuery = false;
            this.pnlTblDept.EnableUpdate = true;
            this.pnlTblDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand";
            this.pnlTblDept.Location = new System.Drawing.Point(595, 495);
            this.pnlTblDept.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlTblDept.MasterPanel = this.pnlDetail;
            this.pnlTblDept.Name = "pnlTblDept";
            this.pnlTblDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept.Size = new System.Drawing.Size(281, 219);
            this.pnlTblDept.SPName = "CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER";
            this.pnlTblDept.TabIndex = 13;
            this.pnlTblDept.TabStop = true;
            // 
            // DGVDept
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_MENU_OPTION,
            this.Seg,
            this.Dept,
            this.Contrib,
            this.PR_TR_NO,
            this.PR_EFFECTIVE,
            this.ID});
            this.DGVDept.ColumnToHide = null;
            this.DGVDept.ColumnWidth = null;
            this.DGVDept.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVDept.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGVDept.DisplayColumnWrapper = null;
            this.DGVDept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVDept.GridDefaultRow = 0;
            this.DGVDept.Location = new System.Drawing.Point(0, 0);
            this.DGVDept.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DGVDept.Name = "DGVDept";
            this.DGVDept.ReadOnlyColumns = null;
            this.DGVDept.RequiredColumns = null;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGVDept.Size = new System.Drawing.Size(281, 219);
            this.DGVDept.SkippingColumns = null;
            this.DGVDept.TabIndex = 0;
            this.DGVDept.TabStop = false;
            this.DGVDept.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVDept_CellValidated);
            this.DGVDept.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVDept_CellValidating);
            this.DGVDept.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGVDept_EditingControlShowing);
            this.DGVDept.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVDept_RowValidated);
            this.DGVDept.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGVDept_RowValidating);
            this.DGVDept.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.DGVDept_PreviewKeyDown);
            // 
            // PR_MENU_OPTION
            // 
            this.PR_MENU_OPTION.DataPropertyName = "PR_MENU_OPTION";
            this.PR_MENU_OPTION.HeaderText = "PR_MENU_OPTION";
            this.PR_MENU_OPTION.Name = "PR_MENU_OPTION";
            this.PR_MENU_OPTION.ReadOnly = true;
            this.PR_MENU_OPTION.Visible = false;
            // 
            // Seg
            // 
            this.Seg.DataPropertyName = "PR_SEGMENT";
            this.Seg.HeaderText = "Seg.";
            this.Seg.MaxInputLength = 3;
            this.Seg.Name = "Seg";
            this.Seg.Width = 50;
            // 
            // Dept
            // 
            this.Dept.ActionLOV = "DEPT_LOV";
            this.Dept.ActionLOVExists = "DEPT_LOV_EXISTS";
            this.Dept.AttachParentEntity = false;
            this.Dept.DataPropertyName = "PR_DEPT";
            this.Dept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand";
            this.Dept.HeaderText = "Dept.";
            this.Dept.LookUpTitle = null;
            this.Dept.LOVFieldMapping = "PR_DEPT";
            this.Dept.MaxInputLength = 5;
            this.Dept.MinimumWidth = 10;
            this.Dept.Name = "Dept";
            this.Dept.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Dept.SearchColumn = "PR_DEPT";
            this.Dept.SkipValidationOnLeave = false;
            this.Dept.SpName = "CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER";
            this.Dept.Width = 70;
            // 
            // Contrib
            // 
            this.Contrib.DataPropertyName = "PR_CONTRIB";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.Contrib.DefaultCellStyle = dataGridViewCellStyle2;
            this.Contrib.HeaderText = "%Contrib.";
            this.Contrib.MaxInputLength = 5;
            this.Contrib.Name = "Contrib";
            this.Contrib.Width = 80;
            // 
            // PR_TR_NO
            // 
            this.PR_TR_NO.HeaderText = "PR_TR_NO";
            this.PR_TR_NO.MinimumWidth = 6;
            this.PR_TR_NO.Name = "PR_TR_NO";
            this.PR_TR_NO.Visible = false;
            // 
            // PR_EFFECTIVE
            // 
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.PR_EFFECTIVE.DefaultCellStyle = dataGridViewCellStyle3;
            this.PR_EFFECTIVE.HeaderText = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.MinimumWidth = 10;
            this.PR_EFFECTIVE.Name = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.Visible = false;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlTblDept2
            // 
            this.pnlTblDept2.ConcurrentPanels = null;
            this.pnlTblDept2.Controls.Add(this.DGVDept2);
            this.pnlTblDept2.DataManager = null;
            this.pnlTblDept2.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept2.DependentPanels = null;
            this.pnlTblDept2.DisableDependentLoad = false;
            this.pnlTblDept2.EnableDelete = true;
            this.pnlTblDept2.EnableInsert = true;
            this.pnlTblDept2.EnableQuery = false;
            this.pnlTblDept2.EnableUpdate = true;
            this.pnlTblDept2.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand";
            this.pnlTblDept2.Location = new System.Drawing.Point(595, 219);
            this.pnlTblDept2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlTblDept2.MasterPanel = null;
            this.pnlTblDept2.Name = "pnlTblDept2";
            this.pnlTblDept2.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept2.Size = new System.Drawing.Size(281, 233);
            this.pnlTblDept2.SPName = "CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER";
            this.pnlTblDept2.TabIndex = 14;
            // 
            // DGVDept2
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDept2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DGVDept2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVDept2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_SEGMENT,
            this.PR_DEPT,
            this.PR_CONTRIB});
            this.DGVDept2.ColumnToHide = null;
            this.DGVDept2.ColumnWidth = null;
            this.DGVDept2.CustomEnabled = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVDept2.DefaultCellStyle = dataGridViewCellStyle7;
            this.DGVDept2.DisplayColumnWrapper = null;
            this.DGVDept2.GridDefaultRow = 0;
            this.DGVDept2.Location = new System.Drawing.Point(4, 4);
            this.DGVDept2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DGVDept2.Name = "DGVDept2";
            this.DGVDept2.ReadOnlyColumns = null;
            this.DGVDept2.RequiredColumns = null;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDept2.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DGVDept2.Size = new System.Drawing.Size(273, 217);
            this.DGVDept2.SkippingColumns = null;
            this.DGVDept2.TabIndex = 0;
            this.DGVDept2.TabStop = false;
            // 
            // PR_SEGMENT
            // 
            this.PR_SEGMENT.DataPropertyName = "PR_SEGMENT";
            this.PR_SEGMENT.HeaderText = "Seg";
            this.PR_SEGMENT.Name = "PR_SEGMENT";
            this.PR_SEGMENT.ReadOnly = true;
            this.PR_SEGMENT.Width = 50;
            // 
            // PR_DEPT
            // 
            this.PR_DEPT.DataPropertyName = "PR_DEPT";
            this.PR_DEPT.HeaderText = "Dept";
            this.PR_DEPT.Name = "PR_DEPT";
            this.PR_DEPT.ReadOnly = true;
            this.PR_DEPT.Width = 60;
            // 
            // PR_CONTRIB
            // 
            this.PR_CONTRIB.DataPropertyName = "PR_CONTRIB";
            this.PR_CONTRIB.HeaderText = "%Contrib";
            this.PR_CONTRIB.Name = "PR_CONTRIB";
            this.PR_CONTRIB.ReadOnly = true;
            this.PR_CONTRIB.Width = 70;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(481, 11);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(173, 17);
            this.txtUserName.TabIndex = 121;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // btnAuth
            // 
            this.btnAuth.Location = new System.Drawing.Point(129, 67);
            this.btnAuth.Name = "btnAuth";
            this.btnAuth.Size = new System.Drawing.Size(106, 28);
            this.btnAuth.TabIndex = 122;
            this.btnAuth.Text = "Authorize";
            this.btnAuth.UseVisualStyleBackColor = true;
            this.btnAuth.Click += new System.EventHandler(this.btnAuth_Click);
            // 
            // CHRIS_Personnel_SegmtOrDeptTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(892, 822);
            this.Controls.Add(this.btnAuth);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlTblDept2);
            this.Controls.Add(this.pnlTblDept);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlDetail);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.Name = "CHRIS_Personnel_SegmtOrDeptTrans";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_SegmtOrDeptTrans";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlTblDept, 0);
            this.Controls.SetChildIndex(this.pnlTblDept2, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.Controls.SetChildIndex(this.btnAuth, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlTblDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVDept)).EndInit();
            this.pnlTblDept2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVDept2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLDatePicker dtpPR_EFFECTIVE;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtW_REP;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txtPR_Func1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtPR_LEVEL;
        private CrplControlLibrary.SLTextBox txtPR_DESG;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtPR_NEW_BRANCH;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtfunc2;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtNoINCR;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtID;
        private CrplControlLibrary.SLTextBox txtPR_Func2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtfunc1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtW_Name;
        private CrplControlLibrary.SLTextBox txtW_GOAL;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLDatePicker dtW_GOAL_DATE;
        private CrplControlLibrary.SLTextBox txtPR_TRANSFER;
        private CrplControlLibrary.SLTextBox txtPR_CLOSE_FLAG;
        private CrplControlLibrary.LookupButton lbtnRep;
        private CrplControlLibrary.SLTextBox txtSum1;
        private CrplControlLibrary.SLDatePicker dtPRTransferDate;
        private CrplControlLibrary.SLDatePicker dtPR_TERMIN_DATE;
        private CrplControlLibrary.SLDatePicker JoiningDate;
        private CrplControlLibrary.SLTextBox W_VAL1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDept;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVDept;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDept2;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVDept2;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_SEGMENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DEPT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONTRIB;
        private System.Windows.Forms.Label txtUserName;
        private CrplControlLibrary.SLTextBox txtMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_MENU_OPTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn Seg;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_TR_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_EFFECTIVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.Button btnAuth;
    }
}