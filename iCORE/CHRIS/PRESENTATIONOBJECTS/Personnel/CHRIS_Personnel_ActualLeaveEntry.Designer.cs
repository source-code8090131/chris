namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_ActualLeaveEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_ActualLeaveEntry));
            this.PnlLeaveActual = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtLA_ID = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtlc_bal_cf = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_MAX_ADV = new CrplControlLibrary.SLTextBox(this.components);
            this.txtw_sex = new CrplControlLibrary.SLTextBox(this.components);
            this.LBStartDt = new CrplControlLibrary.LookupButton(this.components);
            this.lbnLeavetype = new CrplControlLibrary.LookupButton(this.components);
            this.lbpersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.dtJoiningdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtEndDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtstartdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPlAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtClAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMlAvailed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCarryforward = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPlBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtClBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMlBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMandatoryAvialed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPlAdvance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlHalf = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRemarks = new CrplControlLibrary.SLTextBox(this.components);
            this.txtApprovedHead = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocalCertificate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLC_CF_APPROVED = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotalDays = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLeavetype = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNO = new CrplControlLibrary.SLTextBox(this.components);
            this.dtlc_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtlc_s_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlLeaveActual.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(590, 0);
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            this.txtOption.Validating += new System.ComponentModel.CancelEventHandler(this.txtOption_Validating);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(626, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 604);
            this.panel1.Size = new System.Drawing.Size(626, 60);
            // 
            // PnlLeaveActual
            // 
            this.PnlLeaveActual.ConcurrentPanels = null;
            this.PnlLeaveActual.Controls.Add(this.txtLA_ID);
            this.PnlLeaveActual.Controls.Add(this.label33);
            this.PnlLeaveActual.Controls.Add(this.label32);
            this.PnlLeaveActual.Controls.Add(this.label31);
            this.PnlLeaveActual.Controls.Add(this.label30);
            this.PnlLeaveActual.Controls.Add(this.label29);
            this.PnlLeaveActual.Controls.Add(this.label28);
            this.PnlLeaveActual.Controls.Add(this.txtlc_bal_cf);
            this.PnlLeaveActual.Controls.Add(this.txt_W_MAX_ADV);
            this.PnlLeaveActual.Controls.Add(this.txtw_sex);
            this.PnlLeaveActual.Controls.Add(this.LBStartDt);
            this.PnlLeaveActual.Controls.Add(this.lbnLeavetype);
            this.PnlLeaveActual.Controls.Add(this.lbpersonnel);
            this.PnlLeaveActual.Controls.Add(this.dtJoiningdate);
            this.PnlLeaveActual.Controls.Add(this.dtEndDate);
            this.PnlLeaveActual.Controls.Add(this.dtstartdate);
            this.PnlLeaveActual.Controls.Add(this.label27);
            this.PnlLeaveActual.Controls.Add(this.label26);
            this.PnlLeaveActual.Controls.Add(this.label25);
            this.PnlLeaveActual.Controls.Add(this.label24);
            this.PnlLeaveActual.Controls.Add(this.label23);
            this.PnlLeaveActual.Controls.Add(this.label22);
            this.PnlLeaveActual.Controls.Add(this.label21);
            this.PnlLeaveActual.Controls.Add(this.label20);
            this.PnlLeaveActual.Controls.Add(this.label19);
            this.PnlLeaveActual.Controls.Add(this.label13);
            this.PnlLeaveActual.Controls.Add(this.label12);
            this.PnlLeaveActual.Controls.Add(this.label11);
            this.PnlLeaveActual.Controls.Add(this.txtPlAvailed);
            this.PnlLeaveActual.Controls.Add(this.txtClAvailed);
            this.PnlLeaveActual.Controls.Add(this.txtSlAvailed);
            this.PnlLeaveActual.Controls.Add(this.txtMlAvailed);
            this.PnlLeaveActual.Controls.Add(this.txtCarryforward);
            this.PnlLeaveActual.Controls.Add(this.txtPlBalance);
            this.PnlLeaveActual.Controls.Add(this.txtClBalance);
            this.PnlLeaveActual.Controls.Add(this.txtSlBalance);
            this.PnlLeaveActual.Controls.Add(this.txtMlBalance);
            this.PnlLeaveActual.Controls.Add(this.txtMandatoryAvialed);
            this.PnlLeaveActual.Controls.Add(this.txtPlAdvance);
            this.PnlLeaveActual.Controls.Add(this.txtSlHalf);
            this.PnlLeaveActual.Controls.Add(this.label10);
            this.PnlLeaveActual.Controls.Add(this.label9);
            this.PnlLeaveActual.Controls.Add(this.label8);
            this.PnlLeaveActual.Controls.Add(this.label7);
            this.PnlLeaveActual.Controls.Add(this.label6);
            this.PnlLeaveActual.Controls.Add(this.label5);
            this.PnlLeaveActual.Controls.Add(this.label4);
            this.PnlLeaveActual.Controls.Add(this.label2);
            this.PnlLeaveActual.Controls.Add(this.label1);
            this.PnlLeaveActual.Controls.Add(this.txtRemarks);
            this.PnlLeaveActual.Controls.Add(this.txtApprovedHead);
            this.PnlLeaveActual.Controls.Add(this.txtLocalCertificate);
            this.PnlLeaveActual.Controls.Add(this.txtLC_CF_APPROVED);
            this.PnlLeaveActual.Controls.Add(this.txtTotalDays);
            this.PnlLeaveActual.Controls.Add(this.txtName);
            this.PnlLeaveActual.Controls.Add(this.txtLeavetype);
            this.PnlLeaveActual.Controls.Add(this.txtPersonnelNO);
            this.PnlLeaveActual.DataManager = null;
            this.PnlLeaveActual.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlLeaveActual.DependentPanels = null;
            this.PnlLeaveActual.DisableDependentLoad = false;
            this.PnlLeaveActual.EnableDelete = true;
            this.PnlLeaveActual.EnableInsert = true;
            this.PnlLeaveActual.EnableQuery = false;
            this.PnlLeaveActual.EnableUpdate = true;
            this.PnlLeaveActual.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LEAVE_ACTUALCommand";
            this.PnlLeaveActual.Location = new System.Drawing.Point(12, 172);
            this.PnlLeaveActual.MasterPanel = null;
            this.PnlLeaveActual.Name = "PnlLeaveActual";
            this.PnlLeaveActual.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlLeaveActual.Size = new System.Drawing.Size(598, 430);
            this.PnlLeaveActual.SPName = "CHRIS_SP_LEAVE_ACTUAL_MANAGER";
            this.PnlLeaveActual.TabIndex = 0;
            // 
            // txtLA_ID
            // 
            this.txtLA_ID.AllowDrop = true;
            this.txtLA_ID.AllowSpace = true;
            this.txtLA_ID.AssociatedLookUpName = "";
            this.txtLA_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLA_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLA_ID.ContinuationTextBox = null;
            this.txtLA_ID.CustomEnabled = false;
            this.txtLA_ID.DataFieldMapping = "LA_ID";
            this.txtLA_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLA_ID.GetRecordsOnUpDownKeys = false;
            this.txtLA_ID.IsDate = false;
            this.txtLA_ID.Location = new System.Drawing.Point(313, 53);
            this.txtLA_ID.Name = "txtLA_ID";
            this.txtLA_ID.NumberFormat = "###,###,##0.00";
            this.txtLA_ID.Postfix = "";
            this.txtLA_ID.Prefix = "";
            this.txtLA_ID.Size = new System.Drawing.Size(44, 20);
            this.txtLA_ID.SkipValidation = false;
            this.txtLA_ID.TabIndex = 62;
            this.txtLA_ID.TextType = CrplControlLibrary.TextType.String;
            this.txtLA_ID.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, -12);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(583, 13);
            this.label33.TabIndex = 61;
            this.label33.Text = "_________________________________________________________________________________" +
                "_______________";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(8, 224);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(577, 13);
            this.label32.TabIndex = 60;
            this.label32.Text = "_________________________________________________________________________________" +
                "______________";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(245, 239);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(167, 13);
            this.label31.TabIndex = 59;
            this.label31.Text = "L E A V E S      S T A T U S";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(431, 378);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(38, 13);
            this.label30.TabIndex = 58;
            this.label30.Text = "[Y/N]";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(217, 180);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(38, 13);
            this.label29.TabIndex = 57;
            this.label29.Text = "[Y/N]";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(217, 158);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(38, 13);
            this.label28.TabIndex = 56;
            this.label28.Text = "[Y/N]";
            // 
            // txtlc_bal_cf
            // 
            this.txtlc_bal_cf.AllowDrop = true;
            this.txtlc_bal_cf.AllowSpace = true;
            this.txtlc_bal_cf.AssociatedLookUpName = "";
            this.txtlc_bal_cf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlc_bal_cf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlc_bal_cf.ContinuationTextBox = null;
            this.txtlc_bal_cf.CustomEnabled = false;
            this.txtlc_bal_cf.DataFieldMapping = "";
            this.txtlc_bal_cf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlc_bal_cf.GetRecordsOnUpDownKeys = false;
            this.txtlc_bal_cf.IsDate = false;
            this.txtlc_bal_cf.Location = new System.Drawing.Point(416, 150);
            this.txtlc_bal_cf.Name = "txtlc_bal_cf";
            this.txtlc_bal_cf.NumberFormat = "###,###,##0.00";
            this.txtlc_bal_cf.Postfix = "";
            this.txtlc_bal_cf.Prefix = "";
            this.txtlc_bal_cf.Size = new System.Drawing.Size(44, 20);
            this.txtlc_bal_cf.SkipValidation = false;
            this.txtlc_bal_cf.TabIndex = 55;
            this.txtlc_bal_cf.TextType = CrplControlLibrary.TextType.String;
            this.txtlc_bal_cf.Visible = false;
            // 
            // txt_W_MAX_ADV
            // 
            this.txt_W_MAX_ADV.AllowSpace = true;
            this.txt_W_MAX_ADV.AssociatedLookUpName = "";
            this.txt_W_MAX_ADV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_MAX_ADV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_MAX_ADV.ContinuationTextBox = null;
            this.txt_W_MAX_ADV.CustomEnabled = false;
            this.txt_W_MAX_ADV.DataFieldMapping = "";
            this.txt_W_MAX_ADV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_MAX_ADV.GetRecordsOnUpDownKeys = false;
            this.txt_W_MAX_ADV.IsDate = false;
            this.txt_W_MAX_ADV.Location = new System.Drawing.Point(416, 178);
            this.txt_W_MAX_ADV.Name = "txt_W_MAX_ADV";
            this.txt_W_MAX_ADV.NumberFormat = "###,###,##0.00";
            this.txt_W_MAX_ADV.Postfix = "";
            this.txt_W_MAX_ADV.Prefix = "";
            this.txt_W_MAX_ADV.Size = new System.Drawing.Size(44, 20);
            this.txt_W_MAX_ADV.SkipValidation = false;
            this.txt_W_MAX_ADV.TabIndex = 54;
            this.txt_W_MAX_ADV.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_MAX_ADV.Visible = false;
            // 
            // txtw_sex
            // 
            this.txtw_sex.AllowSpace = true;
            this.txtw_sex.AssociatedLookUpName = "";
            this.txtw_sex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtw_sex.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtw_sex.ContinuationTextBox = null;
            this.txtw_sex.CustomEnabled = false;
            this.txtw_sex.DataFieldMapping = "LEV_MED_CERTIF";
            this.txtw_sex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtw_sex.GetRecordsOnUpDownKeys = false;
            this.txtw_sex.IsDate = false;
            this.txtw_sex.Location = new System.Drawing.Point(466, 176);
            this.txtw_sex.Name = "txtw_sex";
            this.txtw_sex.NumberFormat = "###,###,##0.00";
            this.txtw_sex.Postfix = "";
            this.txtw_sex.Prefix = "";
            this.txtw_sex.Size = new System.Drawing.Size(44, 20);
            this.txtw_sex.SkipValidation = false;
            this.txtw_sex.TabIndex = 51;
            this.txtw_sex.TextType = CrplControlLibrary.TextType.String;
            this.txtw_sex.Visible = false;
            // 
            // LBStartDt
            // 
            this.LBStartDt.ActionLOVExists = "";
            this.LBStartDt.ActionType = "StartDate";
            this.LBStartDt.ConditionalFields = "txtPersonnelNO|txtLeavetype";
            this.LBStartDt.CustomEnabled = true;
            this.LBStartDt.DataFieldMapping = "";
            this.LBStartDt.DependentLovControls = "";
            this.LBStartDt.HiddenColumns = "";
            this.LBStartDt.Image = ((System.Drawing.Image)(resources.GetObject("LBStartDt.Image")));
            this.LBStartDt.LoadDependentEntities = false;
            this.LBStartDt.Location = new System.Drawing.Point(267, 77);
            this.LBStartDt.LookUpTitle = null;
            this.LBStartDt.Name = "LBStartDt";
            this.LBStartDt.Size = new System.Drawing.Size(26, 21);
            this.LBStartDt.SkipValidationOnLeave = false;
            this.LBStartDt.SPName = "CHRIS_SP_LEAVE_ACTUAL_MANAGER";
            this.LBStartDt.TabIndex = 50;
            this.LBStartDt.TabStop = false;
            this.LBStartDt.UseVisualStyleBackColor = true;
            this.LBStartDt.Visible = false;
            // 
            // lbnLeavetype
            // 
            this.lbnLeavetype.ActionLOVExists = "type_date_exists";
            this.lbnLeavetype.ActionType = "type_date";
            this.lbnLeavetype.ConditionalFields = "txtPersonnelNO|txtLA_ID";
            this.lbnLeavetype.CustomEnabled = true;
            this.lbnLeavetype.DataFieldMapping = "";
            this.lbnLeavetype.DependentLovControls = "";
            this.lbnLeavetype.HiddenColumns = "LA_ID";
            this.lbnLeavetype.Image = ((System.Drawing.Image)(resources.GetObject("lbnLeavetype.Image")));
            this.lbnLeavetype.LoadDependentEntities = false;
            this.lbnLeavetype.Location = new System.Drawing.Point(266, 51);
            this.lbnLeavetype.LookUpTitle = null;
            this.lbnLeavetype.Name = "lbnLeavetype";
            this.lbnLeavetype.Size = new System.Drawing.Size(26, 21);
            this.lbnLeavetype.SkipValidationOnLeave = false;
            this.lbnLeavetype.SPName = "CHRIS_SP_LEAVE_ACTUAL_MANAGER";
            this.lbnLeavetype.TabIndex = 49;
            this.lbnLeavetype.TabStop = false;
            this.lbnLeavetype.UseVisualStyleBackColor = true;
            // 
            // lbpersonnel
            // 
            this.lbpersonnel.ActionLOVExists = "Perssonnel_no_exists";
            this.lbpersonnel.ActionType = "Perssonnel_no";
            this.lbpersonnel.ConditionalFields = "";
            this.lbpersonnel.CustomEnabled = true;
            this.lbpersonnel.DataFieldMapping = "";
            this.lbpersonnel.DependentLovControls = "";
            this.lbpersonnel.HiddenColumns = "W_JOINING";
            this.lbpersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbpersonnel.Image")));
            this.lbpersonnel.LoadDependentEntities = false;
            this.lbpersonnel.Location = new System.Drawing.Point(265, 25);
            this.lbpersonnel.LookUpTitle = null;
            this.lbpersonnel.Name = "lbpersonnel";
            this.lbpersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbpersonnel.SkipValidationOnLeave = false;
            this.lbpersonnel.SPName = "CHRIS_SP_LEAVE_ACTUAL_MANAGER";
            this.lbpersonnel.TabIndex = 48;
            this.lbpersonnel.TabStop = false;
            this.lbpersonnel.UseVisualStyleBackColor = true;
            // 
            // dtJoiningdate
            // 
            this.dtJoiningdate.CustomEnabled = false;
            this.dtJoiningdate.CustomFormat = "dd/MM/yyyy";
            this.dtJoiningdate.DataFieldMapping = "W_JOINING";
            this.dtJoiningdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtJoiningdate.HasChanges = false;
            this.dtJoiningdate.Location = new System.Drawing.Point(481, 77);
            this.dtJoiningdate.Name = "dtJoiningdate";
            this.dtJoiningdate.NullValue = " ";
            this.dtJoiningdate.Size = new System.Drawing.Size(100, 20);
            this.dtJoiningdate.TabIndex = 47;
            this.dtJoiningdate.TabStop = false;
            this.dtJoiningdate.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            // 
            // dtEndDate
            // 
            this.dtEndDate.CustomEnabled = true;
            this.dtEndDate.CustomFormat = "dd/MM/yyyy";
            this.dtEndDate.DataFieldMapping = "LEV_END_DATE";
            this.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEndDate.HasChanges = true;
            this.dtEndDate.IsRequired = true;
            this.dtEndDate.Location = new System.Drawing.Point(162, 103);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.NullValue = " ";
            this.dtEndDate.Size = new System.Drawing.Size(100, 20);
            this.dtEndDate.TabIndex = 4;
            this.dtEndDate.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            this.dtEndDate.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.dtEndDate_PreviewKeyDown);
            this.dtEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtEndDate_Validating);
            // 
            // dtstartdate
            // 
            this.dtstartdate.CustomEnabled = true;
            this.dtstartdate.CustomFormat = "dd/MM/yyyy";
            this.dtstartdate.DataFieldMapping = "LEV_START_DATE";
            this.dtstartdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtstartdate.HasChanges = true;
            this.dtstartdate.IsRequired = true;
            this.dtstartdate.Location = new System.Drawing.Point(162, 77);
            this.dtstartdate.Name = "dtstartdate";
            this.dtstartdate.NullValue = " ";
            this.dtstartdate.Size = new System.Drawing.Size(100, 20);
            this.dtstartdate.TabIndex = 3;
            this.dtstartdate.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            this.dtstartdate.Validating += new System.ComponentModel.CancelEventHandler(this.dtstartdate_Validating);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(463, 275);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 13);
            this.label27.TabIndex = 44;
            this.label27.Text = "PL Advance :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(490, 327);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 43;
            this.label26.Text = "SL Half :";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(273, 379);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 13);
            this.label25.TabIndex = 42;
            this.label25.Text = "Mandatory Availed :";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(310, 354);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 41;
            this.label24.Text = "ML Balance :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(313, 325);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 13);
            this.label23.TabIndex = 40;
            this.label23.Text = "SL Balance :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(313, 301);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 13);
            this.label22.TabIndex = 39;
            this.label22.Text = "CL Balance :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(312, 280);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "PL Balance :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(38, 380);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "Carry Forward :";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(52, 355);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "ML Availed :";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(54, 328);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "SL Availed :";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(54, 302);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "CL Availed :";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(54, 280);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "PL Availed :";
            // 
            // txtPlAvailed
            // 
            this.txtPlAvailed.AllowSpace = true;
            this.txtPlAvailed.AssociatedLookUpName = "";
            this.txtPlAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlAvailed.ContinuationTextBox = null;
            this.txtPlAvailed.CustomEnabled = true;
            this.txtPlAvailed.DataFieldMapping = "lc_pl_avail";
            this.txtPlAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlAvailed.GetRecordsOnUpDownKeys = false;
            this.txtPlAvailed.IsDate = false;
            this.txtPlAvailed.Location = new System.Drawing.Point(140, 273);
            this.txtPlAvailed.Name = "txtPlAvailed";
            this.txtPlAvailed.NumberFormat = "###,###,##0.00";
            this.txtPlAvailed.Postfix = "";
            this.txtPlAvailed.Prefix = "";
            this.txtPlAvailed.ReadOnly = true;
            this.txtPlAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtPlAvailed.SkipValidation = false;
            this.txtPlAvailed.TabIndex = 32;
            this.txtPlAvailed.TabStop = false;
            this.txtPlAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtClAvailed
            // 
            this.txtClAvailed.AllowSpace = true;
            this.txtClAvailed.AssociatedLookUpName = "";
            this.txtClAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClAvailed.ContinuationTextBox = null;
            this.txtClAvailed.CustomEnabled = true;
            this.txtClAvailed.DataFieldMapping = "lc_cl_avail";
            this.txtClAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClAvailed.GetRecordsOnUpDownKeys = false;
            this.txtClAvailed.IsDate = false;
            this.txtClAvailed.Location = new System.Drawing.Point(140, 299);
            this.txtClAvailed.Name = "txtClAvailed";
            this.txtClAvailed.NumberFormat = "###,###,##0.00";
            this.txtClAvailed.Postfix = "";
            this.txtClAvailed.Prefix = "";
            this.txtClAvailed.ReadOnly = true;
            this.txtClAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtClAvailed.SkipValidation = false;
            this.txtClAvailed.TabIndex = 31;
            this.txtClAvailed.TabStop = false;
            this.txtClAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSlAvailed
            // 
            this.txtSlAvailed.AllowSpace = true;
            this.txtSlAvailed.AssociatedLookUpName = "";
            this.txtSlAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlAvailed.ContinuationTextBox = null;
            this.txtSlAvailed.CustomEnabled = true;
            this.txtSlAvailed.DataFieldMapping = "lc_sl_avail";
            this.txtSlAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlAvailed.GetRecordsOnUpDownKeys = false;
            this.txtSlAvailed.IsDate = false;
            this.txtSlAvailed.Location = new System.Drawing.Point(140, 326);
            this.txtSlAvailed.Name = "txtSlAvailed";
            this.txtSlAvailed.NumberFormat = "###,###,##0.00";
            this.txtSlAvailed.Postfix = "";
            this.txtSlAvailed.Prefix = "";
            this.txtSlAvailed.ReadOnly = true;
            this.txtSlAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtSlAvailed.SkipValidation = false;
            this.txtSlAvailed.TabIndex = 30;
            this.txtSlAvailed.TabStop = false;
            this.txtSlAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtMlAvailed
            // 
            this.txtMlAvailed.AllowSpace = true;
            this.txtMlAvailed.AssociatedLookUpName = "";
            this.txtMlAvailed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMlAvailed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMlAvailed.ContinuationTextBox = null;
            this.txtMlAvailed.CustomEnabled = true;
            this.txtMlAvailed.DataFieldMapping = "lc_ml_avail";
            this.txtMlAvailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMlAvailed.GetRecordsOnUpDownKeys = false;
            this.txtMlAvailed.IsDate = false;
            this.txtMlAvailed.Location = new System.Drawing.Point(140, 352);
            this.txtMlAvailed.Name = "txtMlAvailed";
            this.txtMlAvailed.NumberFormat = "###,###,##0.00";
            this.txtMlAvailed.Postfix = "";
            this.txtMlAvailed.Prefix = "";
            this.txtMlAvailed.ReadOnly = true;
            this.txtMlAvailed.Size = new System.Drawing.Size(27, 20);
            this.txtMlAvailed.SkipValidation = false;
            this.txtMlAvailed.TabIndex = 29;
            this.txtMlAvailed.TabStop = false;
            this.txtMlAvailed.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCarryforward
            // 
            this.txtCarryforward.AllowSpace = true;
            this.txtCarryforward.AssociatedLookUpName = "";
            this.txtCarryforward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCarryforward.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCarryforward.ContinuationTextBox = null;
            this.txtCarryforward.CustomEnabled = true;
            this.txtCarryforward.DataFieldMapping = "LC_C_FORWARD";
            this.txtCarryforward.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCarryforward.GetRecordsOnUpDownKeys = false;
            this.txtCarryforward.IsDate = false;
            this.txtCarryforward.Location = new System.Drawing.Point(140, 378);
            this.txtCarryforward.Name = "txtCarryforward";
            this.txtCarryforward.NumberFormat = "###,###,##0.00";
            this.txtCarryforward.Postfix = "";
            this.txtCarryforward.Prefix = "";
            this.txtCarryforward.ReadOnly = true;
            this.txtCarryforward.Size = new System.Drawing.Size(27, 20);
            this.txtCarryforward.SkipValidation = false;
            this.txtCarryforward.TabIndex = 28;
            this.txtCarryforward.TabStop = false;
            this.txtCarryforward.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPlBalance
            // 
            this.txtPlBalance.AllowSpace = true;
            this.txtPlBalance.AssociatedLookUpName = "";
            this.txtPlBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlBalance.ContinuationTextBox = null;
            this.txtPlBalance.CustomEnabled = true;
            this.txtPlBalance.DataFieldMapping = "lc_pl_bal";
            this.txtPlBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlBalance.GetRecordsOnUpDownKeys = false;
            this.txtPlBalance.IsDate = false;
            this.txtPlBalance.Location = new System.Drawing.Point(398, 273);
            this.txtPlBalance.Name = "txtPlBalance";
            this.txtPlBalance.NumberFormat = "###,###,##0.00";
            this.txtPlBalance.Postfix = "";
            this.txtPlBalance.Prefix = "";
            this.txtPlBalance.ReadOnly = true;
            this.txtPlBalance.Size = new System.Drawing.Size(27, 20);
            this.txtPlBalance.SkipValidation = false;
            this.txtPlBalance.TabIndex = 27;
            this.txtPlBalance.TabStop = false;
            this.txtPlBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtClBalance
            // 
            this.txtClBalance.AllowSpace = true;
            this.txtClBalance.AssociatedLookUpName = "";
            this.txtClBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtClBalance.ContinuationTextBox = null;
            this.txtClBalance.CustomEnabled = true;
            this.txtClBalance.DataFieldMapping = "lc_cl_bal";
            this.txtClBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClBalance.GetRecordsOnUpDownKeys = false;
            this.txtClBalance.IsDate = false;
            this.txtClBalance.Location = new System.Drawing.Point(398, 299);
            this.txtClBalance.Name = "txtClBalance";
            this.txtClBalance.NumberFormat = "###,###,##0.00";
            this.txtClBalance.Postfix = "";
            this.txtClBalance.Prefix = "";
            this.txtClBalance.ReadOnly = true;
            this.txtClBalance.Size = new System.Drawing.Size(27, 20);
            this.txtClBalance.SkipValidation = false;
            this.txtClBalance.TabIndex = 26;
            this.txtClBalance.TabStop = false;
            this.txtClBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSlBalance
            // 
            this.txtSlBalance.AllowSpace = true;
            this.txtSlBalance.AssociatedLookUpName = "";
            this.txtSlBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlBalance.ContinuationTextBox = null;
            this.txtSlBalance.CustomEnabled = true;
            this.txtSlBalance.DataFieldMapping = "lc_sl_bal";
            this.txtSlBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlBalance.GetRecordsOnUpDownKeys = false;
            this.txtSlBalance.IsDate = false;
            this.txtSlBalance.Location = new System.Drawing.Point(398, 325);
            this.txtSlBalance.Name = "txtSlBalance";
            this.txtSlBalance.NumberFormat = "###,###,##0.00";
            this.txtSlBalance.Postfix = "";
            this.txtSlBalance.Prefix = "";
            this.txtSlBalance.ReadOnly = true;
            this.txtSlBalance.Size = new System.Drawing.Size(27, 20);
            this.txtSlBalance.SkipValidation = false;
            this.txtSlBalance.TabIndex = 25;
            this.txtSlBalance.TabStop = false;
            this.txtSlBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtMlBalance
            // 
            this.txtMlBalance.AllowSpace = true;
            this.txtMlBalance.AssociatedLookUpName = "";
            this.txtMlBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMlBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMlBalance.ContinuationTextBox = null;
            this.txtMlBalance.CustomEnabled = true;
            this.txtMlBalance.DataFieldMapping = "lc_ml_bal";
            this.txtMlBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMlBalance.GetRecordsOnUpDownKeys = false;
            this.txtMlBalance.IsDate = false;
            this.txtMlBalance.Location = new System.Drawing.Point(398, 351);
            this.txtMlBalance.Name = "txtMlBalance";
            this.txtMlBalance.NumberFormat = "###,###,##0.00";
            this.txtMlBalance.Postfix = "";
            this.txtMlBalance.Prefix = "";
            this.txtMlBalance.ReadOnly = true;
            this.txtMlBalance.Size = new System.Drawing.Size(27, 20);
            this.txtMlBalance.SkipValidation = false;
            this.txtMlBalance.TabIndex = 24;
            this.txtMlBalance.TabStop = false;
            this.txtMlBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtMandatoryAvialed
            // 
            this.txtMandatoryAvialed.AllowSpace = true;
            this.txtMandatoryAvialed.AssociatedLookUpName = "";
            this.txtMandatoryAvialed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMandatoryAvialed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMandatoryAvialed.ContinuationTextBox = null;
            this.txtMandatoryAvialed.CustomEnabled = true;
            this.txtMandatoryAvialed.DataFieldMapping = "lc_mandatory";
            this.txtMandatoryAvialed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMandatoryAvialed.GetRecordsOnUpDownKeys = false;
            this.txtMandatoryAvialed.IsDate = false;
            this.txtMandatoryAvialed.Location = new System.Drawing.Point(398, 377);
            this.txtMandatoryAvialed.MaxLength = 1;
            this.txtMandatoryAvialed.Name = "txtMandatoryAvialed";
            this.txtMandatoryAvialed.NumberFormat = "###,###,##0.00";
            this.txtMandatoryAvialed.Postfix = "";
            this.txtMandatoryAvialed.Prefix = "";
            this.txtMandatoryAvialed.Size = new System.Drawing.Size(27, 20);
            this.txtMandatoryAvialed.SkipValidation = false;
            this.txtMandatoryAvialed.TabIndex = 8;
            this.txtMandatoryAvialed.TextType = CrplControlLibrary.TextType.String;
            this.txtMandatoryAvialed.Validating += new System.ComponentModel.CancelEventHandler(this.txtMandatoryAvialed_Validating);
            // 
            // txtPlAdvance
            // 
            this.txtPlAdvance.AllowSpace = true;
            this.txtPlAdvance.AssociatedLookUpName = "";
            this.txtPlAdvance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlAdvance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlAdvance.ContinuationTextBox = null;
            this.txtPlAdvance.CustomEnabled = true;
            this.txtPlAdvance.DataFieldMapping = "w_pl_adv";
            this.txtPlAdvance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlAdvance.ForeColor = System.Drawing.Color.Black;
            this.txtPlAdvance.GetRecordsOnUpDownKeys = false;
            this.txtPlAdvance.IsDate = false;
            this.txtPlAdvance.Location = new System.Drawing.Point(553, 273);
            this.txtPlAdvance.Name = "txtPlAdvance";
            this.txtPlAdvance.NumberFormat = "###,###,##0.00";
            this.txtPlAdvance.Postfix = "";
            this.txtPlAdvance.Prefix = "";
            this.txtPlAdvance.ReadOnly = true;
            this.txtPlAdvance.Size = new System.Drawing.Size(27, 20);
            this.txtPlAdvance.SkipValidation = false;
            this.txtPlAdvance.TabIndex = 22;
            this.txtPlAdvance.TabStop = false;
            this.txtPlAdvance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSlHalf
            // 
            this.txtSlHalf.AllowSpace = true;
            this.txtSlHalf.AssociatedLookUpName = "";
            this.txtSlHalf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlHalf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlHalf.ContinuationTextBox = null;
            this.txtSlHalf.CustomEnabled = true;
            this.txtSlHalf.DataFieldMapping = "m_slh";
            this.txtSlHalf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlHalf.ForeColor = System.Drawing.Color.Black;
            this.txtSlHalf.GetRecordsOnUpDownKeys = false;
            this.txtSlHalf.IsDate = false;
            this.txtSlHalf.Location = new System.Drawing.Point(553, 323);
            this.txtSlHalf.Name = "txtSlHalf";
            this.txtSlHalf.NumberFormat = "###,###,##0.00";
            this.txtSlHalf.Postfix = "";
            this.txtSlHalf.Prefix = "";
            this.txtSlHalf.ReadOnly = true;
            this.txtSlHalf.Size = new System.Drawing.Size(27, 20);
            this.txtSlHalf.SkipValidation = false;
            this.txtSlHalf.TabIndex = 21;
            this.txtSlHalf.TabStop = false;
            this.txtSlHalf.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(90, 206);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Remarks :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(35, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Approved By Head :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Medical Certificate :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(397, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Total Days :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(389, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Joining Date :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(86, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "End Date :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(81, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Start Date :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(72, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Leave Type :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Personnel No. / Name   :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemarks
            // 
            this.txtRemarks.AllowSpace = true;
            this.txtRemarks.AssociatedLookUpName = "";
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRemarks.ContinuationTextBox = null;
            this.txtRemarks.CustomEnabled = true;
            this.txtRemarks.DataFieldMapping = "LEV_REMARKS";
            this.txtRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.GetRecordsOnUpDownKeys = false;
            this.txtRemarks.IsDate = false;
            this.txtRemarks.Location = new System.Drawing.Point(166, 204);
            this.txtRemarks.MaxLength = 50;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.NumberFormat = "###,###,##0.00";
            this.txtRemarks.Postfix = "";
            this.txtRemarks.Prefix = "";
            this.txtRemarks.Size = new System.Drawing.Size(419, 20);
            this.txtRemarks.SkipValidation = false;
            this.txtRemarks.TabIndex = 7;
            this.txtRemarks.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtApprovedHead
            // 
            this.txtApprovedHead.AllowSpace = true;
            this.txtApprovedHead.AssociatedLookUpName = "";
            this.txtApprovedHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApprovedHead.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApprovedHead.ContinuationTextBox = null;
            this.txtApprovedHead.CustomEnabled = true;
            this.txtApprovedHead.DataFieldMapping = "LEV_APPROVED";
            this.txtApprovedHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApprovedHead.GetRecordsOnUpDownKeys = false;
            this.txtApprovedHead.IsDate = false;
            this.txtApprovedHead.Location = new System.Drawing.Point(166, 178);
            this.txtApprovedHead.MaxLength = 1;
            this.txtApprovedHead.Name = "txtApprovedHead";
            this.txtApprovedHead.NumberFormat = "###,###,##0.00";
            this.txtApprovedHead.Postfix = "";
            this.txtApprovedHead.Prefix = "";
            this.txtApprovedHead.Size = new System.Drawing.Size(44, 20);
            this.txtApprovedHead.SkipValidation = false;
            this.txtApprovedHead.TabIndex = 6;
            this.txtApprovedHead.TextType = CrplControlLibrary.TextType.String;
            this.txtApprovedHead.Validating += new System.ComponentModel.CancelEventHandler(this.txtApprovedHead_Validating);
            // 
            // txtLocalCertificate
            // 
            this.txtLocalCertificate.AllowSpace = true;
            this.txtLocalCertificate.AssociatedLookUpName = "";
            this.txtLocalCertificate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalCertificate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocalCertificate.ContinuationTextBox = null;
            this.txtLocalCertificate.CustomEnabled = true;
            this.txtLocalCertificate.DataFieldMapping = "LEV_MED_CERTIF";
            this.txtLocalCertificate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalCertificate.GetRecordsOnUpDownKeys = false;
            this.txtLocalCertificate.IsDate = false;
            this.txtLocalCertificate.Location = new System.Drawing.Point(166, 152);
            this.txtLocalCertificate.MaxLength = 1;
            this.txtLocalCertificate.Name = "txtLocalCertificate";
            this.txtLocalCertificate.NumberFormat = "###,###,##0.00";
            this.txtLocalCertificate.Postfix = "";
            this.txtLocalCertificate.Prefix = "";
            this.txtLocalCertificate.Size = new System.Drawing.Size(44, 20);
            this.txtLocalCertificate.SkipValidation = false;
            this.txtLocalCertificate.TabIndex = 5;
            this.txtLocalCertificate.TextType = CrplControlLibrary.TextType.String;
            this.txtLocalCertificate.Validating += new System.ComponentModel.CancelEventHandler(this.txtLocalCertificate_Validating);
            // 
            // txtLC_CF_APPROVED
            // 
            this.txtLC_CF_APPROVED.AllowDrop = true;
            this.txtLC_CF_APPROVED.AllowSpace = true;
            this.txtLC_CF_APPROVED.AssociatedLookUpName = "";
            this.txtLC_CF_APPROVED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLC_CF_APPROVED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLC_CF_APPROVED.ContinuationTextBox = null;
            this.txtLC_CF_APPROVED.CustomEnabled = false;
            this.txtLC_CF_APPROVED.DataFieldMapping = "";
            this.txtLC_CF_APPROVED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLC_CF_APPROVED.GetRecordsOnUpDownKeys = false;
            this.txtLC_CF_APPROVED.IsDate = false;
            this.txtLC_CF_APPROVED.Location = new System.Drawing.Point(480, 150);
            this.txtLC_CF_APPROVED.Name = "txtLC_CF_APPROVED";
            this.txtLC_CF_APPROVED.NumberFormat = "###,###,##0.00";
            this.txtLC_CF_APPROVED.Postfix = "";
            this.txtLC_CF_APPROVED.Prefix = "";
            this.txtLC_CF_APPROVED.Size = new System.Drawing.Size(100, 20);
            this.txtLC_CF_APPROVED.SkipValidation = false;
            this.txtLC_CF_APPROVED.TabIndex = 7;
            this.txtLC_CF_APPROVED.TextType = CrplControlLibrary.TextType.String;
            this.txtLC_CF_APPROVED.Visible = false;
            // 
            // txtTotalDays
            // 
            this.txtTotalDays.AllowSpace = true;
            this.txtTotalDays.AssociatedLookUpName = "";
            this.txtTotalDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalDays.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalDays.ContinuationTextBox = null;
            this.txtTotalDays.CustomEnabled = true;
            this.txtTotalDays.DataFieldMapping = "W_DAYS";
            this.txtTotalDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDays.GetRecordsOnUpDownKeys = false;
            this.txtTotalDays.IsDate = false;
            this.txtTotalDays.Location = new System.Drawing.Point(481, 105);
            this.txtTotalDays.Name = "txtTotalDays";
            this.txtTotalDays.NumberFormat = "###,###,##0.00";
            this.txtTotalDays.Postfix = "";
            this.txtTotalDays.Prefix = "";
            this.txtTotalDays.ReadOnly = true;
            this.txtTotalDays.Size = new System.Drawing.Size(100, 20);
            this.txtTotalDays.SkipValidation = false;
            this.txtTotalDays.TabIndex = 6;
            this.txtTotalDays.TabStop = false;
            this.txtTotalDays.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "LC_PR_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.ForeColor = System.Drawing.Color.Black;
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(297, 27);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(283, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 4;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLeavetype
            // 
            this.txtLeavetype.AllowSpace = true;
            this.txtLeavetype.AssociatedLookUpName = "lbnLeavetype";
            this.txtLeavetype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLeavetype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLeavetype.ContinuationTextBox = null;
            this.txtLeavetype.CustomEnabled = true;
            this.txtLeavetype.DataFieldMapping = "LEV_LV_TYPE";
            this.txtLeavetype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeavetype.GetRecordsOnUpDownKeys = false;
            this.txtLeavetype.IsDate = false;
            this.txtLeavetype.IsRequired = true;
            this.txtLeavetype.Location = new System.Drawing.Point(162, 53);
            this.txtLeavetype.Name = "txtLeavetype";
            this.txtLeavetype.NumberFormat = "###,###,##0.00";
            this.txtLeavetype.Postfix = "";
            this.txtLeavetype.Prefix = "";
            this.txtLeavetype.Size = new System.Drawing.Size(100, 20);
            this.txtLeavetype.SkipValidation = true;
            this.txtLeavetype.TabIndex = 2;
            this.txtLeavetype.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtLeavetype, "Enter value for : PL/CL/SL/ML/LL/OT");
            this.txtLeavetype.Enter += new System.EventHandler(this.txtLeavetype_Enter);
            this.txtLeavetype.Validating += new System.ComponentModel.CancelEventHandler(this.txtLeavetype_Validating);
            // 
            // txtPersonnelNO
            // 
            this.txtPersonnelNO.AllowSpace = true;
            this.txtPersonnelNO.AssociatedLookUpName = "lbpersonnel";
            this.txtPersonnelNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNO.ContinuationTextBox = null;
            this.txtPersonnelNO.CustomEnabled = true;
            this.txtPersonnelNO.DataFieldMapping = "LEV_PNO";
            this.txtPersonnelNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNO.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNO.IsDate = false;
            this.txtPersonnelNO.IsRequired = true;
            this.txtPersonnelNO.Location = new System.Drawing.Point(162, 27);
            this.txtPersonnelNO.MaxLength = 6;
            this.txtPersonnelNO.Name = "txtPersonnelNO";
            this.txtPersonnelNO.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNO.Postfix = "";
            this.txtPersonnelNO.Prefix = "";
            this.txtPersonnelNO.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNO.SkipValidation = false;
            this.txtPersonnelNO.TabIndex = 1;
            this.txtPersonnelNO.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersonnelNO, "[F9]=Help [F10]=Save Leave Balances [F6]=Exit W/O Save.");
            this.txtPersonnelNO.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNO_Validating);
            // 
            // dtlc_date
            // 
            this.dtlc_date.CustomEnabled = false;
            this.dtlc_date.CustomFormat = "dd/MM/yyyy";
            this.dtlc_date.DataFieldMapping = "";
            this.dtlc_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtlc_date.HasChanges = false;
            this.dtlc_date.Location = new System.Drawing.Point(288, 56);
            this.dtlc_date.Name = "dtlc_date";
            this.dtlc_date.NullValue = " ";
            this.dtlc_date.Size = new System.Drawing.Size(92, 20);
            this.dtlc_date.TabIndex = 52;
            this.dtlc_date.Value = new System.DateTime(2011, 1, 13, 0, 0, 0, 0);
            this.dtlc_date.Visible = false;
            // 
            // dtlc_s_date
            // 
            this.dtlc_s_date.CustomEnabled = false;
            this.dtlc_s_date.CustomFormat = "dd/MM/yyyy";
            this.dtlc_s_date.DataFieldMapping = "";
            this.dtlc_s_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtlc_s_date.HasChanges = false;
            this.dtlc_s_date.Location = new System.Drawing.Point(400, 56);
            this.dtlc_s_date.Name = "dtlc_s_date";
            this.dtlc_s_date.NullValue = " ";
            this.dtlc_s_date.Size = new System.Drawing.Size(92, 20);
            this.dtlc_s_date.TabIndex = 53;
            this.dtlc_s_date.Value = new System.DateTime(2011, 1, 13, 0, 0, 0, 0);
            this.dtlc_s_date.Visible = false;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 82);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(598, 84);
            this.pnlHead.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(169, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "L E A V E S";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(188, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(235, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Personnel System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(500, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(500, 36);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(457, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(449, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(401, 9);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 54;
            this.label34.Text = "User Name :";
            // 
            // CHRIS_Personnel_ActualLeaveEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(626, 664);
            this.Controls.Add(this.PnlLeaveActual);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.dtlc_date);
            this.Controls.Add(this.dtlc_s_date);
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_ActualLeaveEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.dtlc_s_date, 0);
            this.Controls.SetChildIndex(this.dtlc_date, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label34, 0);
            this.Controls.SetChildIndex(this.PnlLeaveActual, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlLeaveActual.ResumeLayout(false);
            this.PnlLeaveActual.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlLeaveActual;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtRemarks;
        private CrplControlLibrary.SLTextBox txtApprovedHead;
        private CrplControlLibrary.SLTextBox txtLocalCertificate;
        private CrplControlLibrary.SLTextBox txtLC_CF_APPROVED;
        private CrplControlLibrary.SLTextBox txtTotalDays;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtLeavetype;
        private CrplControlLibrary.SLTextBox txtPersonnelNO;
        private CrplControlLibrary.SLTextBox txtPlAvailed;
        private CrplControlLibrary.SLTextBox txtClAvailed;
        private CrplControlLibrary.SLTextBox txtSlAvailed;
        private CrplControlLibrary.SLTextBox txtMlAvailed;
        private CrplControlLibrary.SLTextBox txtCarryforward;
        private CrplControlLibrary.SLTextBox txtPlBalance;
        private CrplControlLibrary.SLTextBox txtClBalance;
        private CrplControlLibrary.SLTextBox txtSlBalance;
        private CrplControlLibrary.SLTextBox txtMlBalance;
        private CrplControlLibrary.SLTextBox txtMandatoryAvialed;
        private CrplControlLibrary.SLTextBox txtPlAdvance;
        private CrplControlLibrary.SLTextBox txtSlHalf;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLDatePicker dtJoiningdate;
        private CrplControlLibrary.SLDatePicker dtEndDate;
        private CrplControlLibrary.SLDatePicker dtstartdate;
        private CrplControlLibrary.LookupButton lbpersonnel;
        private CrplControlLibrary.LookupButton lbnLeavetype;
        private CrplControlLibrary.LookupButton LBStartDt;
        private CrplControlLibrary.SLTextBox txtw_sex;
        private CrplControlLibrary.SLDatePicker dtlc_s_date;
        private CrplControlLibrary.SLDatePicker dtlc_date;
        private CrplControlLibrary.SLTextBox txt_W_MAX_ADV;
        private CrplControlLibrary.SLTextBox txtlc_bal_cf;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txtLA_ID;
    }
}