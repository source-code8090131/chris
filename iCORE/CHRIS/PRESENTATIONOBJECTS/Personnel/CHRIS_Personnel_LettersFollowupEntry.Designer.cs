namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_LettersFollowupEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_LettersFollowupEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtPR_MED_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slPanelDetailReferences = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvReferenceDetails = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.REF_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REF_LET_SNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REF_ANS_REC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REF_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.slPanelDetailEmployment = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvEmployment = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_JOB_FROM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_JOB_TO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_ORGANIZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DESIG_PR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_LET_SNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_ANS_REC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreEmpID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmpPR_P_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.slPanelDetailReferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReferenceDetails)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.slPanelDetailEmployment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployment)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Location = new System.Drawing.Point(12, 87);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(644, 89);
            this.pnlHead.TabIndex = 13;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(534, 45);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(534, 19);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(488, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(483, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(75, 45);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(75, 16);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(2, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(3, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(638, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "PERSONNEL SYSTEM";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(149, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(347, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "LETTER\'S FOLLOWUP ENTRY";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.txtPR_MED_FLAG);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlDetail.Location = new System.Drawing.Point(11, 182);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(644, 77);
            this.pnlDetail.SPName = "CHRIS_SP_LETTERS_FOLLOWUP_Personnel_MANAGER";
            this.pnlDetail.TabIndex = 14;
            this.pnlDetail.TabStop = true;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(223, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 20);
            this.label8.TabIndex = 145;
            this.label8.Text = "(Y/N)";
            // 
            // txtPR_MED_FLAG
            // 
            this.txtPR_MED_FLAG.AllowSpace = true;
            this.txtPR_MED_FLAG.AssociatedLookUpName = "lbtnPNo";
            this.txtPR_MED_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_MED_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_MED_FLAG.ContinuationTextBox = null;
            this.txtPR_MED_FLAG.CustomEnabled = true;
            this.txtPR_MED_FLAG.DataFieldMapping = "PR_MED_FLAG";
            this.txtPR_MED_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_MED_FLAG.GetRecordsOnUpDownKeys = false;
            this.txtPR_MED_FLAG.IsDate = false;
            this.txtPR_MED_FLAG.Location = new System.Drawing.Point(151, 50);
            this.txtPR_MED_FLAG.MaxLength = 10;
            this.txtPR_MED_FLAG.Name = "txtPR_MED_FLAG";
            this.txtPR_MED_FLAG.NumberFormat = "###,###,##0.00";
            this.txtPR_MED_FLAG.Postfix = "";
            this.txtPR_MED_FLAG.Prefix = "";
            this.txtPR_MED_FLAG.Size = new System.Drawing.Size(59, 20);
            this.txtPR_MED_FLAG.SkipValidation = false;
            this.txtPR_MED_FLAG.TabIndex = 3;
            this.txtPR_MED_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_MED_FLAG, "Press <F9> Key to Display The List");
            this.txtPR_MED_FLAG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPR_MED_FLAG_KeyPress);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(17, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 20);
            this.label13.TabIndex = 82;
            this.label13.Text = "Medical Received:";
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = false;
            this.txtName.DataFieldMapping = "PRName";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(247, 21);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.Size = new System.Drawing.Size(249, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PrPNo_LOV_Exists";
            this.lbtnPNo.ActionType = "PrPNo_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(215, 21);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = true;
            this.lbtnPNo.SPName = "CHRIS_SP_LETTERS_FOLLOWUP_Personnel_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(151, 21);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(59, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(17, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "Personnel No. :";
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(151, 21);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(36, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slPanelDetailReferences);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(11, 269);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(645, 142);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "<REFERENCES>";
            // 
            // slPanelDetailReferences
            // 
            this.slPanelDetailReferences.ConcurrentPanels = null;
            this.slPanelDetailReferences.Controls.Add(this.dgvReferenceDetails);
            this.slPanelDetailReferences.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelDetailReferences.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelDetailReferences.DependentPanels = null;
            this.slPanelDetailReferences.DisableDependentLoad = false;
            this.slPanelDetailReferences.EnableDelete = true;
            this.slPanelDetailReferences.EnableInsert = true;
            this.slPanelDetailReferences.EnableQuery = false;
            this.slPanelDetailReferences.EnableUpdate = true;
            this.slPanelDetailReferences.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LetterFollowUpReferencesCommand";
            this.slPanelDetailReferences.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelDetailReferences.ForeColor = System.Drawing.SystemColors.ControlText;
            this.slPanelDetailReferences.Location = new System.Drawing.Point(7, 19);
            this.slPanelDetailReferences.MasterPanel = this.pnlDetail;
            this.slPanelDetailReferences.Name = "slPanelDetailReferences";
            this.slPanelDetailReferences.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelDetailReferences.Size = new System.Drawing.Size(622, 114);
            this.slPanelDetailReferences.SPName = "CHRIS_SP_LETTERS_FOLLOWUP_EMP_REFERENCE_MANAGER";
            this.slPanelDetailReferences.TabIndex = 3;
            // 
            // dgvReferenceDetails
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReferenceDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReferenceDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReferenceDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.REF_NAME,
            this.REF_LET_SNT,
            this.REF_ANS_REC,
            this.REF_CODE,
            this.ID,
            this.Ref_PR_P_NO});
            this.dgvReferenceDetails.ColumnToHide = null;
            this.dgvReferenceDetails.ColumnWidth = null;
            this.dgvReferenceDetails.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReferenceDetails.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvReferenceDetails.DisplayColumnWrapper = null;
            this.dgvReferenceDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReferenceDetails.GridDefaultRow = 0;
            this.dgvReferenceDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvReferenceDetails.Name = "dgvReferenceDetails";
            this.dgvReferenceDetails.ReadOnlyColumns = null;
            this.dgvReferenceDetails.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReferenceDetails.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvReferenceDetails.Size = new System.Drawing.Size(622, 114);
            this.dgvReferenceDetails.SkippingColumns = null;
            this.dgvReferenceDetails.TabIndex = 0;
            this.dgvReferenceDetails.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvReferenceDetails_CellValidating);
            // 
            // REF_NAME
            // 
            this.REF_NAME.DataPropertyName = "REF_NAME";
            this.REF_NAME.HeaderText = "Name";
            this.REF_NAME.Name = "REF_NAME";
            this.REF_NAME.ReadOnly = true;
            // 
            // REF_LET_SNT
            // 
            this.REF_LET_SNT.DataPropertyName = "REF_LET_SNT";
            this.REF_LET_SNT.HeaderText = "Letter Sent";
            this.REF_LET_SNT.Name = "REF_LET_SNT";
            // 
            // REF_ANS_REC
            // 
            this.REF_ANS_REC.DataPropertyName = "REF_ANS_REC";
            this.REF_ANS_REC.HeaderText = "Received";
            this.REF_ANS_REC.Name = "REF_ANS_REC";
            // 
            // REF_CODE
            // 
            this.REF_CODE.DataPropertyName = "REF_CODE";
            this.REF_CODE.HeaderText = "REF_CODE";
            this.REF_CODE.Name = "REF_CODE";
            this.REF_CODE.Visible = false;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // Ref_PR_P_NO
            // 
            this.Ref_PR_P_NO.DataPropertyName = "PR_P_NO";
            this.Ref_PR_P_NO.HeaderText = "PR_P_NO";
            this.Ref_PR_P_NO.Name = "Ref_PR_P_NO";
            this.Ref_PR_P_NO.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.slPanelDetailEmployment);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(12, 417);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(645, 135);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "< PREVIOUS EMPLOYEMENT >";
            // 
            // slPanelDetailEmployment
            // 
            this.slPanelDetailEmployment.ConcurrentPanels = null;
            this.slPanelDetailEmployment.Controls.Add(this.dgvEmployment);
            this.slPanelDetailEmployment.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelDetailEmployment.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelDetailEmployment.DependentPanels = null;
            this.slPanelDetailEmployment.DisableDependentLoad = false;
            this.slPanelDetailEmployment.EnableDelete = true;
            this.slPanelDetailEmployment.EnableInsert = true;
            this.slPanelDetailEmployment.EnableQuery = false;
            this.slPanelDetailEmployment.EnableUpdate = true;
            this.slPanelDetailEmployment.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.EmploymentLetterFollowUpCommand";
            this.slPanelDetailEmployment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelDetailEmployment.ForeColor = System.Drawing.SystemColors.ControlText;
            this.slPanelDetailEmployment.Location = new System.Drawing.Point(7, 19);
            this.slPanelDetailEmployment.MasterPanel = this.pnlDetail;
            this.slPanelDetailEmployment.Name = "slPanelDetailEmployment";
            this.slPanelDetailEmployment.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelDetailEmployment.Size = new System.Drawing.Size(622, 107);
            this.slPanelDetailEmployment.SPName = "CHRIS_SP_LETTERS_FOLLOWUP_PRE_EMP_MANAGER";
            this.slPanelDetailEmployment.TabIndex = 3;
            // 
            // dgvEmployment
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmployment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEmployment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_JOB_FROM,
            this.PR_JOB_TO,
            this.PR_ORGANIZ,
            this.PR_DESIG_PR,
            this.PR_LET_SNT,
            this.PR_ANS_REC,
            this.PreEmpID,
            this.EmpPR_P_No});
            this.dgvEmployment.ColumnToHide = null;
            this.dgvEmployment.ColumnWidth = null;
            this.dgvEmployment.CustomEnabled = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmployment.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEmployment.DisplayColumnWrapper = null;
            this.dgvEmployment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEmployment.GridDefaultRow = 0;
            this.dgvEmployment.Location = new System.Drawing.Point(0, 0);
            this.dgvEmployment.Name = "dgvEmployment";
            this.dgvEmployment.ReadOnlyColumns = null;
            this.dgvEmployment.RequiredColumns = null;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmployment.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvEmployment.Size = new System.Drawing.Size(622, 107);
            this.dgvEmployment.SkippingColumns = null;
            this.dgvEmployment.TabIndex = 0;
            // 
            // PR_JOB_FROM
            // 
            this.PR_JOB_FROM.DataPropertyName = "PR_JOB_FROM";
            this.PR_JOB_FROM.HeaderText = "From";
            this.PR_JOB_FROM.Name = "PR_JOB_FROM";
            this.PR_JOB_FROM.ReadOnly = true;
            // 
            // PR_JOB_TO
            // 
            this.PR_JOB_TO.DataPropertyName = "PR_JOB_TO";
            this.PR_JOB_TO.HeaderText = "To";
            this.PR_JOB_TO.Name = "PR_JOB_TO";
            this.PR_JOB_TO.ReadOnly = true;
            // 
            // PR_ORGANIZ
            // 
            this.PR_ORGANIZ.DataPropertyName = "PR_ORGANIZ";
            this.PR_ORGANIZ.HeaderText = "Organization";
            this.PR_ORGANIZ.Name = "PR_ORGANIZ";
            this.PR_ORGANIZ.ReadOnly = true;
            // 
            // PR_DESIG_PR
            // 
            this.PR_DESIG_PR.DataPropertyName = "PR_DESIG_PR";
            this.PR_DESIG_PR.HeaderText = "Designation";
            this.PR_DESIG_PR.Name = "PR_DESIG_PR";
            this.PR_DESIG_PR.ReadOnly = true;
            // 
            // PR_LET_SNT
            // 
            this.PR_LET_SNT.DataPropertyName = "PR_LET_SNT";
            this.PR_LET_SNT.HeaderText = "Letter Sent";
            this.PR_LET_SNT.Name = "PR_LET_SNT";
            // 
            // PR_ANS_REC
            // 
            this.PR_ANS_REC.DataPropertyName = "PR_ANS_REC";
            this.PR_ANS_REC.HeaderText = "Received";
            this.PR_ANS_REC.Name = "PR_ANS_REC";
            // 
            // PreEmpID
            // 
            this.PreEmpID.DataPropertyName = "ID";
            this.PreEmpID.HeaderText = "ID";
            this.PreEmpID.Name = "PreEmpID";
            this.PreEmpID.Visible = false;
            // 
            // EmpPR_P_No
            // 
            this.EmpPR_P_No.DataPropertyName = "PR_P_NO";
            this.EmpPR_P_No.HeaderText = "PR_P_NO";
            this.EmpPR_P_No.Name = "EmpPR_P_No";
            this.EmpPR_P_No.Visible = false;
            // 
            // CHRIS_Personnel_LettersFollowupEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_LettersFollowupEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_LettersFollowupEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.slPanelDetailReferences.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReferenceDetails)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.slPanelDetailEmployment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtPR_MED_FLAG;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelDetailReferences;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvReferenceDetails;
        private System.Windows.Forms.GroupBox groupBox2;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelDetailEmployment;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvEmployment;
        private System.Windows.Forms.DataGridViewTextBoxColumn REF_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn REF_LET_SNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn REF_ANS_REC;
        private System.Windows.Forms.DataGridViewTextBoxColumn REF_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_JOB_FROM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_JOB_TO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_ORGANIZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DESIG_PR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_LET_SNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_ANS_REC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreEmpID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmpPR_P_No;
    }
}