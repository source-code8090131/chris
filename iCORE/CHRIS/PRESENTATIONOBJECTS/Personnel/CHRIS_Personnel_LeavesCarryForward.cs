using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_LeavesCarryForward : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();


        # region Constructor
        public CHRIS_Personnel_LeavesCarryForward()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_LeavesCarryForward(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //this.CurrentPanelBlock = this.PnlPersonnel.Name;
        }

        # endregion

        # region Methods
        bool isvalidating = false;
        private void proc_delete()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            {
                param.Add("ls_p_no", Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() == "" ? "0" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString()));
                cmnDM.GetData("CHRIS_SP_LEAVE_STATUS_MANAGER", "proc_delete", param);

            }


        }
        private void proc_modify()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            {
                Result rsltr;
                param.Add("ls_p_no", Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() == "" ? "0" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString()));
                param.Add("ls_bal_cf", (DGVLEAVE_STATUS.CurrentRow.Cells["ls_bal_cf"].EditedFormattedValue.ToString() == "" ? "" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_bal_cf"].EditedFormattedValue.ToString()));
                param.Add("ls_c_forward", (DGVLEAVE_STATUS.CurrentRow.Cells["ls_c_forward"].EditedFormattedValue.ToString() == "" ? "" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_c_forward"].EditedFormattedValue.ToString()));
                param.Add("ls_cf_approved", (DGVLEAVE_STATUS.CurrentRow.Cells["ls_cf_approved"].EditedFormattedValue.ToString() == "" ? "" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_cf_approved"].EditedFormattedValue.ToString()));
                rsltr = cmnDM.GetData("CHRIS_SP_LEAVE_STATUS_MANAGER", "proc_modify", param);

                if(rsltr.isSuccessful)
                {
                    MessageBox.Show("Record Updated Sucessfully");                
                }

                

            }


        }
        private void proc_mod_Del()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            {
                Result rslt;
                param.Add("ls_p_no", Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() == "" ? "0" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString()));

                rslt = cmnDM.GetData("CHRIS_SP_LEAVE_STATUS_MANAGER", "proc_mod_Del", param);

                if (rslt.isSuccessful)
                {
                    if ((rslt.dstResult != null) && (rslt.dstResult.Tables[0].Rows.Count > 0))
                    {
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "" ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["lc_pl_bal"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() == "" ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_bal_cf"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString() == "" ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_c_forward"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString() == "" ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_cf_approved"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString() == "" ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString());
                    }
                }


            }



        }
        private void proc_view_add()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            {
                param.Add("ls_p_no", Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() == "" ? "0" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString()));
                rslt = cmnDM.GetData("CHRIS_SP_LEAVE_STATUS_MANAGER", "proc_view_add", param);

                if (rslt.isSuccessful)
                {
                    if ((rslt.dstResult != null) && (rslt.dstResult.Tables[0].Rows.Count > 0))
                    {
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "" ? "" : rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["lc_pl_bal"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() == "" ? "" : rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_bal_cf"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString() == "" ? "" : rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_c_forward"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString() == "" ? "0" : rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString());
                        DGVLEAVE_STATUS.CurrentRow.Cells["ls_cf_approved"].Value = (rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString() == "" ? "" : rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString());
                    }
                }


            }




        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {

                return;
            }

            if (actionType == "Delete")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                return;
            }


            if (actionType == "Search")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                return;

            }
            base.DoToolbarActions(ctrlsCollection, actionType);

        }
        protected override bool Add()
        {
            base.Cancel();
            return base.Add();
        }
        protected override bool View()
        {
            base.Cancel();
            return base.View();
        }
        protected override bool Edit()
        {
            base.Cancel();
            return base.Edit();
        }
        //protected override bool Delete()
        //{
        //    //base.Cancel();
        //    //return base.Delete();
        //}

        protected override void OnLoad(EventArgs e)
        {

            //LS_P_NO.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //FirstName.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //LC_PL_BAL.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //LS_BAL_CF.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //LS_C_FORWARD.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            //LS_CF_APPROVED.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);

            DGVLEAVE_STATUS.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);


            //    DGVLEAVE_STATUS.Columns["LS_P_NO"].ValueType = typeof(double);

            txtOption.Visible = false;
            base.OnLoad(e);
            label1.Text += "" + this.UserName;
            txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.tbtSave.Visible = false;

        }

        # endregion

        # region Events
        private void DGVLEAVE_STATUS_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
            {

            if (DGVLEAVE_STATUS.CurrentCell.IsInEditMode)
            {
                #region  case 1
                if (e.ColumnIndex == 0)
                {
                    proc_view_add();
                    if (DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() != null && DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() != string.Empty && DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() != "")
                    {
                        if ((FunctionConfig.CurrentOption == Function.Modify) || (FunctionConfig.CurrentOption == Function.Delete))
                        {
                            proc_mod_Del();
                        }
                        if ((FunctionConfig.CurrentOption == Function.View) || (FunctionConfig.CurrentOption == Function.Add))
                        {


                        }

                        if (FunctionConfig.CurrentOption == Function.Add)
                        {
                            Result rslt;
                            int count;
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("ls_p_no", Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() == "" ? "0" : DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString()));
                            rslt = cmnDM.GetData("CHRIS_SP_LEAVE_STATUS_MANAGER", "count_chk", param);

                            count = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                            if (count > 0)
                            {
                                MessageBox.Show("Record Already Entered.........");
                                DataTable dt = (DataTable)(DGVLEAVE_STATUS.DataSource);
                                dt.Rows.Clear();
                            }
                        }

                        if (FunctionConfig.CurrentOption == Function.View)
                        {
                            if (MessageBox.Show("Do You Want View   [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                // MessageBox.Show("Press [F9] To Display The List...  Or Press [Enter] To Exit");

                            }
                            else
                            {
                                DataTable dt = (DataTable)(DGVLEAVE_STATUS.DataSource);
                                dt.Rows.Clear();

                            }
                        }
                        if (FunctionConfig.CurrentOption == Function.Delete)
                        {
                            if (MessageBox.Show("Do You Want Delete   [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                proc_delete();
                                //('Press [Home] To Display The List .. Or Press [Enter] To Exit');

                            }
                            else
                            {
                                DataTable dt = (DataTable)(DGVLEAVE_STATUS.DataSource);
                                dt.Rows.Clear();

                            }
                        }
                    }
                }
                #endregion

                #region case 2
                if (e.ColumnIndex == 3)
                {

                    if (DGVLEAVE_STATUS.Rows[e.RowIndex].Cells["LS_BAL_CF"].IsInEditMode)
                    {

                        string Bal_cf = DGVLEAVE_STATUS.CurrentCell.EditedFormattedValue.ToString();
                        if (Bal_cf.ToUpper() != "Y" && Bal_cf.ToUpper() != "N")
                        {
                            MessageBox.Show("Field Must Be Enter (Y/N) ........");
                            e.Cancel = true;
                        }
                        else if (Bal_cf.ToUpper() != "Y" 
                            //&& isvalidating == false
                            )
                        {
                            DGVLEAVE_STATUS.CurrentRow.Cells["LS_BAL_CF"].Value = "N";
                            DGVLEAVE_STATUS.CurrentRow.Cells["ls_c_forward"].Value = 0;
                            DGVLEAVE_STATUS.CurrentRow.Cells["ls_cf_approved"].Value = 'N';
                            proc_modify();
                           // isvalidating = true;
                            DGVLEAVE_STATUS.CurrentRow.Cells["LS_C_FORWARD"].ReadOnly = true;
                            DGVLEAVE_STATUS.CurrentRow.Cells["LS_CF_APPROVED"].ReadOnly = true;
                        }
                        else
                        {
                            DGVLEAVE_STATUS.CurrentRow.Cells["LS_C_FORWARD"].ReadOnly = false;
                            DGVLEAVE_STATUS.CurrentRow.Cells["LS_CF_APPROVED"].ReadOnly = false;
                           // isvalidating = false;
                        }

                    }


                }
                #endregion

                #region case 3
                if (e.ColumnIndex == 4)
                {


                    int counter1 = 0;
                    int counter2 = 0;

                    if (DGVLEAVE_STATUS.CurrentRow.Cells[4].EditedFormattedValue.ToString() == string.Empty || DGVLEAVE_STATUS.CurrentRow.Cells[4].EditedFormattedValue.ToString() == "")
                    {
                        counter1 = 0;
                        counter2 = 0;
                    }

                    else
                    {

                        if (Int32.TryParse(DGVLEAVE_STATUS.CurrentRow.Cells[2].EditedFormattedValue.ToString(), out counter1) && Int32.TryParse(DGVLEAVE_STATUS.CurrentRow.Cells[4].EditedFormattedValue.ToString(), out counter2))
                        {
                            counter1 = Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells[2].EditedFormattedValue.ToString());
                            counter2 = Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells[4].EditedFormattedValue.ToString());



                            if (counter2 > counter1)
                            {
                                MessageBox.Show("Carry Forward Leaves Must be less then or Equal to PL Balance.......");
                                e.Cancel = true;

                            }
                        }
                    }
                }
                #endregion

                # region case 4
                if (e.ColumnIndex == 5)
                {

                    if (DGVLEAVE_STATUS.Rows[e.RowIndex].Cells["LS_CF_APPROVED"].IsInEditMode)
                    {

                        string cf_Approved = DGVLEAVE_STATUS.CurrentCell.EditedFormattedValue.ToString();
                        if (cf_Approved.ToUpper() != "Y" && cf_Approved.ToUpper() != "N")
                        {
                            MessageBox.Show("Field Must Be Enter (Y/N) ........");
                            e.Cancel = true;

                        }

                        proc_modify();

                    }


                    //if (DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() != null && DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() != string.Empty && DGVLEAVE_STATUS.CurrentRow.Cells["ls_p_no"].EditedFormattedValue.ToString() != "")
                    //{

                    //    string check;
                    //    check = DGVLEAVE_STATUS.CurrentRow.Cells[5].EditedFormattedValue.ToString();

                    //    if (check.ToUpper() != "Y" && check.ToUpper() != "N")
                    //    {
                    //        MessageBox.Show("Field Must Be Enter (Y/N) ........");
                    //        e.Cancel = true;

                    //    }
                    //    else
                    //    {
                    //        if (FunctionConfig.CurrentOption == Function.Modify)
                    //        {
                    //            proc_modify();

                    //        }
                    //        else
                    //        {
                    //            if (FunctionConfig.CurrentOption == Function.Add)
                    //            {
                    //                if (MessageBox.Show("Do You Want TO save This Record ", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    //                {

                    //                    DGVLEAVE_STATUS.CurrentRow.Cells["LS_PL_BAL"].Value = 30 + Int32.Parse(DGVLEAVE_STATUS.CurrentRow.Cells["ls_c_forward"].EditedFormattedValue.ToString());
                    //                    DGVLEAVE_STATUS.CurrentRow.Cells["LS_SL_BAL"].Value = 60;
                    //                    DGVLEAVE_STATUS.CurrentRow.Cells["LS_ML_BAL"].Value = 30;
                    //                    DGVLEAVE_STATUS.CurrentRow.Cells["LS_CL_BAL"].Value = 12;
                    //                    //       :LS_PL_BAL := 30+:ls_c_forward;
                    //                    //       :LS_SL_BAL := 60;
                    //                    //       :LS_ML_BAL := 30;
                    //                    //       :LS_CL_BAL := 12;

                    //                    base.DoToolbarActions(this.Controls, "Save");

                    //                }
                    //                else
                    //                {

                    //                    DataTable dt = (DataTable)(DGVLEAVE_STATUS.DataSource);
                    //                    dt.Rows.Clear();

                    //                }


                    //            }
                    //        }

                    //    }

                    //}

                }
                #endregion
            }
        }


        private void DGVLEAVE_STATUS_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


            }
        }

        # endregion

        private void DGVLEAVE_STATUS_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (DGVLEAVE_STATUS.CurrentCell.IsInEditMode)
            {
                if (DGVLEAVE_STATUS.CurrentCell.OwningColumn.Name == "LS_P_NO")
                {
                    bool chkPr_P_NO = IsValidExpression(DGVLEAVE_STATUS.CurrentCell.EditedFormattedValue.ToString(), InputValidator.NUMBER_REGEX);

                    if (!chkPr_P_NO)
                    {
                        return;
                    }


                }
            }
        }

        private void DGVLEAVE_STATUS_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((DGVLEAVE_STATUS.CurrentCell.OwningColumn.Name == "LS_C_FORWARD" ||
                DGVLEAVE_STATUS.CurrentCell.OwningColumn.Name == "LS_BAL_CF" ||
                DGVLEAVE_STATUS.CurrentCell.OwningColumn.Name == "LS_CF_APPROVED") 
                && (this.operationMode == Mode.Edit))
            {
                DGVLEAVE_STATUS.BeginEdit(true);
            }
        }

        private void DGVLEAVE_STATUS_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        //private void txtOption_TextChanged(object sender, EventArgs e)
        //{
        //    DGVLEAVE_STATUS.DataSource = null;
        //}

    }

}