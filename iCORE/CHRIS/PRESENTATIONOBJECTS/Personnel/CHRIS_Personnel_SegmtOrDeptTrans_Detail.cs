﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_SegmtOrDeptTrans_Detail : Form
    {
        public CHRIS_Personnel_SegmtOrDeptTrans_Detail()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CHRIS_Personnel_SegmtOrDeptTrans_Detail_Load(object sender, EventArgs e)
        {
            txtW_GOAL.ReadOnly = false;
           // dtW_GOAL_DATE.ReadOnly = false;
            txtW_Name.ReadOnly = false;
            txtfunc1.ReadOnly = false;
            txtPR_Func2.ReadOnly = false;
            slTextBox1.ReadOnly = false;
           // dtpPR_EFFECTIVE.ReadOnly = false;
            txtW_REP.ReadOnly = false;
            txtPR_Func1.ReadOnly = false;
            txtPR_LEVEL.ReadOnly = false;
            txtPR_DESG.ReadOnly = false;
            txtPR_NEW_BRANCH.ReadOnly = false;
            txtfunc2.ReadOnly = false;
            txtNoINCR.ReadOnly = false;
            txtName.ReadOnly = false;
            txtPersNo.ReadOnly = false;


            DataTable dtDept = new DataTable();
            dtDept = SQLManager.CHRIS_SP_GET_SegmtOrDept_PRNO(CHRIS_Personnel_SegmtOrDeptTrans_View.SetValuePR_NO).Tables[0];
            string PRNO = string.Empty;
            foreach (System.Data.DataRow dtrow in dtDept.Rows)
            {
                txtW_GOAL.Text = string.IsNullOrEmpty(dtrow["W_GOAL"].ToString().Trim()) ? "" : dtrow["W_GOAL"].ToString().Trim();
                dtW_GOAL_DATE.Text = string.IsNullOrEmpty(dtrow["W_GOAL_DATE"].ToString().Trim()) ? "" : dtrow["W_GOAL_DATE"].ToString().Trim();
                txtfunc1.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_1"].ToString().Trim()) ? "" : dtrow["PR_FUNC_1"].ToString().Trim();
                txtPR_Func2.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_TITTLE2"].ToString().Trim()) ? "" : dtrow["PR_FUNC_TITTLE2"].ToString().Trim();
                slTextBox1.Text = string.IsNullOrEmpty(dtrow["PR_LAST_NAME"].ToString().Trim()) ? "" : dtrow["PR_LAST_NAME"].ToString().Trim();
                dtpPR_EFFECTIVE.Text = string.IsNullOrEmpty(dtrow["PR_EFFECTIVE"].ToString().Trim()) ? "" : dtrow["PR_EFFECTIVE"].ToString().Trim();
                txtW_REP.Text = string.IsNullOrEmpty(dtrow["PR_REP_NO"].ToString().Trim()) ? "" : dtrow["PR_REP_NO"].ToString().Trim();
                txtPR_Func1.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_TITTLE1"].ToString().Trim()) ? "" : dtrow["PR_FUNC_TITTLE1"].ToString().Trim();
                txtPR_LEVEL.Text = string.IsNullOrEmpty(dtrow["PR_LEVEL"].ToString().Trim()) ? "" : dtrow["PR_LEVEL"].ToString().Trim();
                txtPR_DESG.Text = string.IsNullOrEmpty(dtrow["PR_DESIG"].ToString().Trim()) ? "" : dtrow["PR_DESIG"].ToString().Trim();
                txtPR_NEW_BRANCH.Text = string.IsNullOrEmpty(dtrow["PR_NEW_BRANCH"].ToString().Trim()) ? "" : dtrow["PR_NEW_BRANCH"].ToString().Trim();
                txtfunc2.Text = string.IsNullOrEmpty(dtrow["PR_FUNC_2"].ToString().Trim()) ? "" : dtrow["PR_FUNC_2"].ToString().Trim();
                txtNoINCR.Text = string.IsNullOrEmpty(dtrow["PR_TRANSFER_TYPE"].ToString().Trim()) ? "" : dtrow["PR_TRANSFER_TYPE"].ToString().Trim();
                txtName.Text = string.IsNullOrEmpty(dtrow["PR_FIRST_NAME"].ToString().Trim()) ? "" : dtrow["PR_FIRST_NAME"].ToString().Trim();
                txtPersNo.Text = string.IsNullOrEmpty(dtrow["PR_TR_NO"].ToString().Trim()) ? "" : dtrow["PR_TR_NO"].ToString().Trim();
                PRNO = (string.IsNullOrEmpty(dtrow["PR_TR_NO"].ToString().Trim()) ? "" : dtrow["PR_TR_NO"].ToString().Trim());
            }

            //CHRIS_SP_GET_Dept_PRNO
            DataTable dtDP = new DataTable();
            dtDP = SQLManager.CHRIS_SP_GET_Dept_PRNO(PRNO).Tables[0];
            if (dtDP.Rows.Count > 0)
            {
                DGVDepts.DataSource = dtDP;
                DGVDepts.Update();
                DGVDepts.Visible = true;
                // btnReject.Visible = true;
                // btnApproved.Visible = true;
            }

        }

        private void btnAproved_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersNo.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersNo.Text.Trim());
                SQLManager.ApprovedSegDept(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersNo.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersNo.Text.Trim());
                SQLManager.ApprovedSegDeptReject(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }
        }
    }
}
