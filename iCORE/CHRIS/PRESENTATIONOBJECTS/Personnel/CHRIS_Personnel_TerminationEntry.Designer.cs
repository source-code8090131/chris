namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_TerminationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_TerminationEntry));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTxtBxUserID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.txtFinalSettlementReport = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDelFromMic = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPowerOfAttornyCancel = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtNoticeOfStaffChanged = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIDCardRet = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtExitIntrvDone = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtResgAppr = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtResgRecieved = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtReason = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.dtpTerminDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtTerminType = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtConfDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCloseFlag = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConfOnDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJoiningDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.btnAuth = new System.Windows.Forms.Button();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(788, 0);
            this.txtOption.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.pnlBottom.Size = new System.Drawing.Size(835, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 658);
            this.panel1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.panel1.Size = new System.Drawing.Size(835, 74);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(16, 70);
            this.pnlHead.Margin = new System.Windows.Forms.Padding(4);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(798, 102);
            this.pnlHead.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(220, 78);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(368, 22);
            this.label6.TabIndex = 20;
            this.label6.Text = "T E R M I N A T I O N S";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(220, 43);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(368, 25);
            this.label5.TabIndex = 19;
            this.label5.Text = "Personnel System";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(664, 75);
            this.txtDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(106, 24);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(664, 43);
            this.txtCurrOption.Margin = new System.Windows.Forms.Padding(4);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(106, 24);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(607, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 18);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(596, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(105, 75);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(106, 24);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(105, 43);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(106, 24);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(4, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.slTxtBxUserID);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.Controls.Add(this.label20);
            this.pnlDetail.Controls.Add(this.txtFinalSettlementReport);
            this.pnlDetail.Controls.Add(this.label21);
            this.pnlDetail.Controls.Add(this.label22);
            this.pnlDetail.Controls.Add(this.txtDelFromMic);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.label24);
            this.pnlDetail.Controls.Add(this.txtPowerOfAttornyCancel);
            this.pnlDetail.Controls.Add(this.label25);
            this.pnlDetail.Controls.Add(this.label26);
            this.pnlDetail.Controls.Add(this.txtNoticeOfStaffChanged);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtIDCardRet);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.txtExitIntrvDone);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.txtResgAppr);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtResgRecieved);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtReason);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.dtpTerminDate);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtTerminType);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtFirstName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.txtConfDate);
            this.pnlDetail.Controls.Add(this.txtCloseFlag);
            this.pnlDetail.Controls.Add(this.txtConfOnDate);
            this.pnlDetail.Controls.Add(this.txtJoiningDate);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlDetail.Location = new System.Drawing.Point(16, 180);
            this.pnlDetail.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(803, 464);
            this.pnlDetail.SPName = "CHRIS_SP_TERMINATION_MANAGER";
            this.pnlDetail.TabIndex = 1;
            this.pnlDetail.TabStop = true;
            // 
            // slTxtBxUserID
            // 
            this.slTxtBxUserID.AllowSpace = true;
            this.slTxtBxUserID.AssociatedLookUpName = "txt_W_USER";
            this.slTxtBxUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBxUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBxUserID.ContinuationTextBox = null;
            this.slTxtBxUserID.CustomEnabled = true;
            this.slTxtBxUserID.DataFieldMapping = "PR_UserID";
            this.slTxtBxUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBxUserID.GetRecordsOnUpDownKeys = false;
            this.slTxtBxUserID.IsDate = false;
            this.slTxtBxUserID.Location = new System.Drawing.Point(7, 15);
            this.slTxtBxUserID.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtBxUserID.MaxLength = 20;
            this.slTxtBxUserID.Name = "slTxtBxUserID";
            this.slTxtBxUserID.NumberFormat = "###,###,##0.00";
            this.slTxtBxUserID.Postfix = "";
            this.slTxtBxUserID.Prefix = "";
            this.slTxtBxUserID.Size = new System.Drawing.Size(45, 24);
            this.slTxtBxUserID.SkipValidation = false;
            this.slTxtBxUserID.TabIndex = 94;
            this.slTxtBxUserID.TextType = CrplControlLibrary.TextType.String;
            this.slTxtBxUserID.Visible = false;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(692, 134);
            this.txtID.Margin = new System.Windows.Forms.Padding(4);
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(106, 24);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 58;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(449, 420);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 25);
            this.label20.TabIndex = 57;
            this.label20.Text = "Y/N";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFinalSettlementReport
            // 
            this.txtFinalSettlementReport.AllowSpace = true;
            this.txtFinalSettlementReport.AssociatedLookUpName = "";
            this.txtFinalSettlementReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinalSettlementReport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinalSettlementReport.ContinuationTextBox = null;
            this.txtFinalSettlementReport.CustomEnabled = true;
            this.txtFinalSettlementReport.DataFieldMapping = "PR_SETTLE";
            this.txtFinalSettlementReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinalSettlementReport.GetRecordsOnUpDownKeys = false;
            this.txtFinalSettlementReport.IsDate = false;
            this.txtFinalSettlementReport.IsRequired = true;
            this.txtFinalSettlementReport.Location = new System.Drawing.Point(415, 420);
            this.txtFinalSettlementReport.Margin = new System.Windows.Forms.Padding(4);
            this.txtFinalSettlementReport.MaxLength = 1;
            this.txtFinalSettlementReport.Name = "txtFinalSettlementReport";
            this.txtFinalSettlementReport.NumberFormat = "###,###,##0.00";
            this.txtFinalSettlementReport.Postfix = "";
            this.txtFinalSettlementReport.Prefix = "";
            this.txtFinalSettlementReport.Size = new System.Drawing.Size(26, 24);
            this.txtFinalSettlementReport.SkipValidation = false;
            this.txtFinalSettlementReport.TabIndex = 13;
            this.txtFinalSettlementReport.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtFinalSettlementReport, "Enter [Y/N]");
            this.txtFinalSettlementReport.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            this.txtFinalSettlementReport.Validating += new System.ComponentModel.CancelEventHandler(this.txtFinalSettlementReport_Validating);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(172, 420);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(235, 25);
            this.label21.TabIndex = 55;
            this.label21.Text = "Final Settlement Report:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(449, 388);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 25);
            this.label22.TabIndex = 54;
            this.label22.Text = "Y/N";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDelFromMic
            // 
            this.txtDelFromMic.AllowSpace = true;
            this.txtDelFromMic.AssociatedLookUpName = "";
            this.txtDelFromMic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDelFromMic.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDelFromMic.ContinuationTextBox = null;
            this.txtDelFromMic.CustomEnabled = true;
            this.txtDelFromMic.DataFieldMapping = "PR_DELETION";
            this.txtDelFromMic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDelFromMic.GetRecordsOnUpDownKeys = false;
            this.txtDelFromMic.IsDate = false;
            this.txtDelFromMic.IsRequired = true;
            this.txtDelFromMic.Location = new System.Drawing.Point(415, 388);
            this.txtDelFromMic.Margin = new System.Windows.Forms.Padding(4);
            this.txtDelFromMic.MaxLength = 1;
            this.txtDelFromMic.Name = "txtDelFromMic";
            this.txtDelFromMic.NumberFormat = "###,###,##0.00";
            this.txtDelFromMic.Postfix = "";
            this.txtDelFromMic.Prefix = "";
            this.txtDelFromMic.Size = new System.Drawing.Size(26, 24);
            this.txtDelFromMic.SkipValidation = false;
            this.txtDelFromMic.TabIndex = 12;
            this.txtDelFromMic.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtDelFromMic, "Enter [Y/N]");
            this.txtDelFromMic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(176, 388);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(231, 25);
            this.label23.TabIndex = 52;
            this.label23.Text = "Delete from Micro/Prime:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(449, 356);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 25);
            this.label24.TabIndex = 51;
            this.label24.Text = "Y/N";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPowerOfAttornyCancel
            // 
            this.txtPowerOfAttornyCancel.AllowSpace = true;
            this.txtPowerOfAttornyCancel.AssociatedLookUpName = "";
            this.txtPowerOfAttornyCancel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPowerOfAttornyCancel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPowerOfAttornyCancel.ContinuationTextBox = null;
            this.txtPowerOfAttornyCancel.CustomEnabled = true;
            this.txtPowerOfAttornyCancel.DataFieldMapping = "PR_ARTONY";
            this.txtPowerOfAttornyCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPowerOfAttornyCancel.GetRecordsOnUpDownKeys = false;
            this.txtPowerOfAttornyCancel.IsDate = false;
            this.txtPowerOfAttornyCancel.IsRequired = true;
            this.txtPowerOfAttornyCancel.Location = new System.Drawing.Point(415, 356);
            this.txtPowerOfAttornyCancel.Margin = new System.Windows.Forms.Padding(4);
            this.txtPowerOfAttornyCancel.MaxLength = 1;
            this.txtPowerOfAttornyCancel.Name = "txtPowerOfAttornyCancel";
            this.txtPowerOfAttornyCancel.NumberFormat = "###,###,##0.00";
            this.txtPowerOfAttornyCancel.Postfix = "";
            this.txtPowerOfAttornyCancel.Prefix = "";
            this.txtPowerOfAttornyCancel.Size = new System.Drawing.Size(26, 24);
            this.txtPowerOfAttornyCancel.SkipValidation = false;
            this.txtPowerOfAttornyCancel.TabIndex = 11;
            this.txtPowerOfAttornyCancel.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPowerOfAttornyCancel, "Enter [Y/N]");
            this.txtPowerOfAttornyCancel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(172, 356);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(235, 25);
            this.label25.TabIndex = 49;
            this.label25.Text = "Power Of Attorney Cancel:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(449, 324);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 25);
            this.label26.TabIndex = 48;
            this.label26.Text = "Y/N";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNoticeOfStaffChanged
            // 
            this.txtNoticeOfStaffChanged.AllowSpace = true;
            this.txtNoticeOfStaffChanged.AssociatedLookUpName = "";
            this.txtNoticeOfStaffChanged.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoticeOfStaffChanged.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoticeOfStaffChanged.ContinuationTextBox = null;
            this.txtNoticeOfStaffChanged.CustomEnabled = true;
            this.txtNoticeOfStaffChanged.DataFieldMapping = "PR_NOTICE";
            this.txtNoticeOfStaffChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoticeOfStaffChanged.GetRecordsOnUpDownKeys = false;
            this.txtNoticeOfStaffChanged.IsDate = false;
            this.txtNoticeOfStaffChanged.IsRequired = true;
            this.txtNoticeOfStaffChanged.Location = new System.Drawing.Point(415, 324);
            this.txtNoticeOfStaffChanged.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoticeOfStaffChanged.MaxLength = 1;
            this.txtNoticeOfStaffChanged.Name = "txtNoticeOfStaffChanged";
            this.txtNoticeOfStaffChanged.NumberFormat = "###,###,##0.00";
            this.txtNoticeOfStaffChanged.Postfix = "";
            this.txtNoticeOfStaffChanged.Prefix = "";
            this.txtNoticeOfStaffChanged.Size = new System.Drawing.Size(26, 24);
            this.txtNoticeOfStaffChanged.SkipValidation = false;
            this.txtNoticeOfStaffChanged.TabIndex = 10;
            this.txtNoticeOfStaffChanged.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtNoticeOfStaffChanged, "Enter [Y/N]");
            this.txtNoticeOfStaffChanged.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(172, 324);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(235, 25);
            this.label27.TabIndex = 46;
            this.label27.Text = "Notice Of Staff Change:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(449, 292);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 25);
            this.label16.TabIndex = 45;
            this.label16.Text = "Y/N";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtIDCardRet
            // 
            this.txtIDCardRet.AllowSpace = true;
            this.txtIDCardRet.AssociatedLookUpName = "";
            this.txtIDCardRet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIDCardRet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIDCardRet.ContinuationTextBox = null;
            this.txtIDCardRet.CustomEnabled = true;
            this.txtIDCardRet.DataFieldMapping = "PR_ID_RETURN";
            this.txtIDCardRet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDCardRet.GetRecordsOnUpDownKeys = false;
            this.txtIDCardRet.IsDate = false;
            this.txtIDCardRet.IsRequired = true;
            this.txtIDCardRet.Location = new System.Drawing.Point(415, 292);
            this.txtIDCardRet.Margin = new System.Windows.Forms.Padding(4);
            this.txtIDCardRet.MaxLength = 1;
            this.txtIDCardRet.Name = "txtIDCardRet";
            this.txtIDCardRet.NumberFormat = "###,###,##0.00";
            this.txtIDCardRet.Postfix = "";
            this.txtIDCardRet.Prefix = "";
            this.txtIDCardRet.Size = new System.Drawing.Size(26, 24);
            this.txtIDCardRet.SkipValidation = false;
            this.txtIDCardRet.TabIndex = 9;
            this.txtIDCardRet.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtIDCardRet, "Enter [Y/N]");
            this.txtIDCardRet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(220, 292);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(187, 25);
            this.label17.TabIndex = 43;
            this.label17.Text = "ID Card Returned:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(449, 260);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 25);
            this.label18.TabIndex = 42;
            this.label18.Text = "Y/N";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExitIntrvDone
            // 
            this.txtExitIntrvDone.AllowSpace = true;
            this.txtExitIntrvDone.AssociatedLookUpName = "";
            this.txtExitIntrvDone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExitIntrvDone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtExitIntrvDone.ContinuationTextBox = null;
            this.txtExitIntrvDone.CustomEnabled = true;
            this.txtExitIntrvDone.DataFieldMapping = "PR_EXIT_INTER";
            this.txtExitIntrvDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExitIntrvDone.GetRecordsOnUpDownKeys = false;
            this.txtExitIntrvDone.IsDate = false;
            this.txtExitIntrvDone.IsRequired = true;
            this.txtExitIntrvDone.Location = new System.Drawing.Point(415, 260);
            this.txtExitIntrvDone.Margin = new System.Windows.Forms.Padding(4);
            this.txtExitIntrvDone.MaxLength = 1;
            this.txtExitIntrvDone.Name = "txtExitIntrvDone";
            this.txtExitIntrvDone.NumberFormat = "###,###,##0.00";
            this.txtExitIntrvDone.Postfix = "";
            this.txtExitIntrvDone.Prefix = "";
            this.txtExitIntrvDone.Size = new System.Drawing.Size(26, 24);
            this.txtExitIntrvDone.SkipValidation = false;
            this.txtExitIntrvDone.TabIndex = 8;
            this.txtExitIntrvDone.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtExitIntrvDone, "Enter [Y/N]");
            this.txtExitIntrvDone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(220, 260);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(187, 25);
            this.label19.TabIndex = 40;
            this.label19.Text = "Exit Interview Done:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(449, 228);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 25);
            this.label14.TabIndex = 39;
            this.label14.Text = "Y/N";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtResgAppr
            // 
            this.txtResgAppr.AllowSpace = true;
            this.txtResgAppr.AssociatedLookUpName = "";
            this.txtResgAppr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtResgAppr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtResgAppr.ContinuationTextBox = null;
            this.txtResgAppr.CustomEnabled = true;
            this.txtResgAppr.DataFieldMapping = "PR_APP_RESIG";
            this.txtResgAppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResgAppr.GetRecordsOnUpDownKeys = false;
            this.txtResgAppr.IsDate = false;
            this.txtResgAppr.IsRequired = true;
            this.txtResgAppr.Location = new System.Drawing.Point(415, 228);
            this.txtResgAppr.Margin = new System.Windows.Forms.Padding(4);
            this.txtResgAppr.MaxLength = 1;
            this.txtResgAppr.Name = "txtResgAppr";
            this.txtResgAppr.NumberFormat = "###,###,##0.00";
            this.txtResgAppr.Postfix = "";
            this.txtResgAppr.Prefix = "";
            this.txtResgAppr.Size = new System.Drawing.Size(26, 24);
            this.txtResgAppr.SkipValidation = false;
            this.txtResgAppr.TabIndex = 7;
            this.txtResgAppr.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtResgAppr, "Enter [Y/N]");
            this.txtResgAppr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(168, 228);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(239, 25);
            this.label15.TabIndex = 37;
            this.label15.Text = "Approval Of Resignation:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(449, 196);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 25);
            this.label13.TabIndex = 36;
            this.label13.Text = "Y/N";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtResgRecieved
            // 
            this.txtResgRecieved.AllowSpace = true;
            this.txtResgRecieved.AssociatedLookUpName = "";
            this.txtResgRecieved.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtResgRecieved.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtResgRecieved.ContinuationTextBox = null;
            this.txtResgRecieved.CustomEnabled = true;
            this.txtResgRecieved.DataFieldMapping = "PR_RESIG_RECV";
            this.txtResgRecieved.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResgRecieved.GetRecordsOnUpDownKeys = false;
            this.txtResgRecieved.IsDate = false;
            this.txtResgRecieved.IsRequired = true;
            this.txtResgRecieved.Location = new System.Drawing.Point(415, 196);
            this.txtResgRecieved.Margin = new System.Windows.Forms.Padding(4);
            this.txtResgRecieved.MaxLength = 1;
            this.txtResgRecieved.Name = "txtResgRecieved";
            this.txtResgRecieved.NumberFormat = "###,###,##0.00";
            this.txtResgRecieved.Postfix = "";
            this.txtResgRecieved.Prefix = "";
            this.txtResgRecieved.Size = new System.Drawing.Size(26, 24);
            this.txtResgRecieved.SkipValidation = false;
            this.txtResgRecieved.TabIndex = 6;
            this.txtResgRecieved.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtResgRecieved, "Enter [Y/N]");
            this.txtResgRecieved.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYN_KeyPress);
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(199, 196);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(208, 25);
            this.label12.TabIndex = 34;
            this.label12.Text = "Resignation Received:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtReason
            // 
            this.txtReason.AllowSpace = true;
            this.txtReason.AssociatedLookUpName = "";
            this.txtReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReason.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReason.ContinuationTextBox = null;
            this.txtReason.CustomEnabled = true;
            this.txtReason.DataFieldMapping = "PR_REASONS";
            this.txtReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReason.GetRecordsOnUpDownKeys = false;
            this.txtReason.IsDate = false;
            this.txtReason.IsRequired = true;
            this.txtReason.Location = new System.Drawing.Point(415, 164);
            this.txtReason.Margin = new System.Windows.Forms.Padding(4);
            this.txtReason.MaxLength = 30;
            this.txtReason.Name = "txtReason";
            this.txtReason.NumberFormat = "###,###,##0.00";
            this.txtReason.Postfix = "";
            this.txtReason.Prefix = "";
            this.txtReason.Size = new System.Drawing.Size(198, 24);
            this.txtReason.SkipValidation = false;
            this.txtReason.TabIndex = 5;
            this.txtReason.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(220, 164);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(187, 25);
            this.label11.TabIndex = 32;
            this.label11.Text = "Reasons Of Leaving:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpTerminDate
            // 
            this.dtpTerminDate.CustomEnabled = true;
            this.dtpTerminDate.CustomFormat = "dd/MM/yyyy";
            this.dtpTerminDate.DataFieldMapping = "PR_TERMIN_DATE";
            this.dtpTerminDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTerminDate.HasChanges = true;
            this.dtpTerminDate.Location = new System.Drawing.Point(415, 132);
            this.dtpTerminDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpTerminDate.Name = "dtpTerminDate";
            this.dtpTerminDate.NullValue = " ";
            this.dtpTerminDate.Size = new System.Drawing.Size(132, 22);
            this.dtpTerminDate.TabIndex = 4;
            this.dtpTerminDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpTerminDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpTerminDate_Validating);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(220, 132);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(187, 25);
            this.label10.TabIndex = 30;
            this.label10.Text = "Termination Date:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTerminType
            // 
            this.txtTerminType.AllowSpace = true;
            this.txtTerminType.AssociatedLookUpName = "";
            this.txtTerminType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTerminType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTerminType.ContinuationTextBox = null;
            this.txtTerminType.CustomEnabled = true;
            this.txtTerminType.DataFieldMapping = "PR_TERMIN_TYPE";
            this.txtTerminType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerminType.GetRecordsOnUpDownKeys = false;
            this.txtTerminType.IsDate = false;
            this.txtTerminType.IsRequired = true;
            this.txtTerminType.Location = new System.Drawing.Point(415, 100);
            this.txtTerminType.Margin = new System.Windows.Forms.Padding(4);
            this.txtTerminType.MaxLength = 1;
            this.txtTerminType.Name = "txtTerminType";
            this.txtTerminType.NumberFormat = "###,###,##0.00";
            this.txtTerminType.Postfix = "";
            this.txtTerminType.Prefix = "";
            this.txtTerminType.Size = new System.Drawing.Size(39, 24);
            this.txtTerminType.SkipValidation = false;
            this.txtTerminType.TabIndex = 3;
            this.txtTerminType.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtTerminType, "Press [T]ermination or [R]esignation or [S]eparation or [M]is Conduct");
            this.txtTerminType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTerminType_KeyPress);
            this.txtTerminType.Validating += new System.ComponentModel.CancelEventHandler(this.txtTerminType_Validating);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(220, 100);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(187, 25);
            this.label9.TabIndex = 28;
            this.label9.Text = "Termination Type:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(415, 65);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 30;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(198, 24);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(220, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(187, 25);
            this.label8.TabIndex = 26;
            this.label8.Text = "Name:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "P_NO_EXISTS";
            this.lbtnPNo.ActionType = "P_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = false;
            this.lbtnPNo.Location = new System.Drawing.Point(579, 31);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Margin = new System.Windows.Forms.Padding(4);
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_TERMINATION_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            this.lbtnPNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnPNo_MouseDown);
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsLookUpField = true;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(415, 31);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(155, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(220, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(187, 25);
            this.label7.TabIndex = 23;
            this.label7.Text = "Personnel No.:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtConfDate
            // 
            this.txtConfDate.AllowSpace = true;
            this.txtConfDate.AssociatedLookUpName = "";
            this.txtConfDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfDate.ContinuationTextBox = null;
            this.txtConfDate.CustomEnabled = true;
            this.txtConfDate.DataFieldMapping = "PR_CONFIRM";
            this.txtConfDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfDate.GetRecordsOnUpDownKeys = false;
            this.txtConfDate.IsDate = false;
            this.txtConfDate.Location = new System.Drawing.Point(692, 100);
            this.txtConfDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfDate.Name = "txtConfDate";
            this.txtConfDate.NumberFormat = "###,###,##0.00";
            this.txtConfDate.Postfix = "";
            this.txtConfDate.Prefix = "";
            this.txtConfDate.ReadOnly = true;
            this.txtConfDate.Size = new System.Drawing.Size(106, 24);
            this.txtConfDate.SkipValidation = false;
            this.txtConfDate.TabIndex = 22;
            this.txtConfDate.TextType = CrplControlLibrary.TextType.String;
            this.txtConfDate.Visible = false;
            // 
            // txtCloseFlag
            // 
            this.txtCloseFlag.AllowSpace = true;
            this.txtCloseFlag.AssociatedLookUpName = "";
            this.txtCloseFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCloseFlag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCloseFlag.ContinuationTextBox = null;
            this.txtCloseFlag.CustomEnabled = true;
            this.txtCloseFlag.DataFieldMapping = "PR_CLOSE_FLAG";
            this.txtCloseFlag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCloseFlag.GetRecordsOnUpDownKeys = false;
            this.txtCloseFlag.IsDate = false;
            this.txtCloseFlag.Location = new System.Drawing.Point(692, 68);
            this.txtCloseFlag.Margin = new System.Windows.Forms.Padding(4);
            this.txtCloseFlag.Name = "txtCloseFlag";
            this.txtCloseFlag.NumberFormat = "###,###,##0.00";
            this.txtCloseFlag.Postfix = "";
            this.txtCloseFlag.Prefix = "";
            this.txtCloseFlag.ReadOnly = true;
            this.txtCloseFlag.Size = new System.Drawing.Size(106, 24);
            this.txtCloseFlag.SkipValidation = false;
            this.txtCloseFlag.TabIndex = 21;
            this.txtCloseFlag.TextType = CrplControlLibrary.TextType.String;
            this.txtCloseFlag.Visible = false;
            // 
            // txtConfOnDate
            // 
            this.txtConfOnDate.AllowSpace = true;
            this.txtConfOnDate.AssociatedLookUpName = "";
            this.txtConfOnDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfOnDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfOnDate.ContinuationTextBox = null;
            this.txtConfOnDate.CustomEnabled = true;
            this.txtConfOnDate.DataFieldMapping = "PR_CONFIRM_ON";
            this.txtConfOnDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfOnDate.GetRecordsOnUpDownKeys = false;
            this.txtConfOnDate.IsDate = false;
            this.txtConfOnDate.Location = new System.Drawing.Point(692, 36);
            this.txtConfOnDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfOnDate.Name = "txtConfOnDate";
            this.txtConfOnDate.NumberFormat = "###,###,##0.00";
            this.txtConfOnDate.Postfix = "";
            this.txtConfOnDate.Prefix = "";
            this.txtConfOnDate.ReadOnly = true;
            this.txtConfOnDate.Size = new System.Drawing.Size(106, 24);
            this.txtConfOnDate.SkipValidation = false;
            this.txtConfOnDate.TabIndex = 20;
            this.txtConfOnDate.TextType = CrplControlLibrary.TextType.String;
            this.txtConfOnDate.Visible = false;
            // 
            // txtJoiningDate
            // 
            this.txtJoiningDate.AllowSpace = true;
            this.txtJoiningDate.AssociatedLookUpName = "";
            this.txtJoiningDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJoiningDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJoiningDate.ContinuationTextBox = null;
            this.txtJoiningDate.CustomEnabled = true;
            this.txtJoiningDate.DataFieldMapping = "PR_JOINING_DATE";
            this.txtJoiningDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJoiningDate.GetRecordsOnUpDownKeys = false;
            this.txtJoiningDate.IsDate = false;
            this.txtJoiningDate.Location = new System.Drawing.Point(692, 4);
            this.txtJoiningDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtJoiningDate.Name = "txtJoiningDate";
            this.txtJoiningDate.NumberFormat = "###,###,##0.00";
            this.txtJoiningDate.Postfix = "";
            this.txtJoiningDate.Prefix = "";
            this.txtJoiningDate.ReadOnly = true;
            this.txtJoiningDate.Size = new System.Drawing.Size(106, 24);
            this.txtJoiningDate.SkipValidation = false;
            this.txtJoiningDate.TabIndex = 19;
            this.txtJoiningDate.TextType = CrplControlLibrary.TextType.String;
            this.txtJoiningDate.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(508, 16);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 17);
            this.label28.TabIndex = 10;
            this.label28.Text = "User Name: ";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(595, 16);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 17);
            this.lblUserName.TabIndex = 11;
            // 
            // btnAuth
            // 
            this.btnAuth.Location = new System.Drawing.Point(120, 42);
            this.btnAuth.Name = "btnAuth";
            this.btnAuth.Size = new System.Drawing.Size(106, 28);
            this.btnAuth.TabIndex = 58;
            this.btnAuth.Text = "Authorize";
            this.btnAuth.UseVisualStyleBackColor = true;
            this.btnAuth.Click += new System.EventHandler(this.btnAuth_Click);
            // 
            // CHRIS_Personnel_TerminationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 732);
            this.Controls.Add(this.btnAuth);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.Name = "CHRIS_Personnel_TerminationEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Termination Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.label28, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.btnAuth, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtConfOnDate;
        private CrplControlLibrary.SLTextBox txtJoiningDate;
        private CrplControlLibrary.SLTextBox txtConfDate;
        private CrplControlLibrary.SLTextBox txtCloseFlag;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtTerminType;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLDatePicker dtpTerminDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtReason;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtResgRecieved;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtResgAppr;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtFinalSettlementReport;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txtDelFromMic;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtPowerOfAttornyCancel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLTextBox txtNoticeOfStaffChanged;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtIDCardRet;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtExitIntrvDone;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox slTxtBxUserID;
        private System.Windows.Forms.Button btnAuth;
    }
}