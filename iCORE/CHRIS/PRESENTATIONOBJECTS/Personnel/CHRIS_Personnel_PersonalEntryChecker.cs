﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_PersonalEntryChecker : Form
    {
        public CHRIS_Personnel_PersonalEntryChecker()
        {
            InitializeComponent();
        }

        private void CHRIS_Personnel_PersonalEntryChecker_Load(object sender, EventArgs e)
        {
            string userID = CHRIS_Personnel_RegularStaffHiringEnt.SetValueUserLogin;
            DataTable dt = new DataTable();
            dt = SQLManager.CHRIS_SP_PersonnelCheck(userID).Tables[0];
            if (dt.Rows.Count > 0)
            {
                dGVMaker.DataSource = dt;
                dGVMaker.Update();
                dGVMaker.Visible = true;
               // btnReject.Visible = true;
               // btnApproved.Visible = true;
            }
            else
            {
                dGVMaker.Visible = false;
               // btnReject.Visible = false;
               // btnApproved.Visible = false;
            }
        }

        public static string SetValuePR_NO = "";
        private void button1_Click(object sender, EventArgs e)
        {
            string get_PR_No = string.Empty;
            int RowCount = dGVMaker.SelectedRows.Count;

            if (RowCount >= 1 )
            {
                foreach (DataGridViewRow row in dGVMaker.SelectedRows)
                {
                    get_PR_No = row.Cells[0].Value.ToString();
                }

                SetValuePR_NO = get_PR_No;
                CHRIS_Personnel_RegularStaffHiringEnt_MC frm = new CHRIS_Personnel_RegularStaffHiringEnt_MC();
                frm.ShowDialog();

                string userID = CHRIS_Personnel_RegularStaffHiringEnt.SetValueUserLogin;
                DataTable dt = new DataTable();
                dt = SQLManager.CHRIS_SP_PersonnelCheck(userID).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dGVMaker.DataSource = dt;
                    dGVMaker.Update();
                    dGVMaker.Visible = true;
                    // btnReject.Visible = true;
                    // btnApproved.Visible = true;
                }
                else
                {
                    dGVMaker.Visible = false;
                    // btnReject.Visible = false;
                    // btnApproved.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("You Must Select The Record");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
