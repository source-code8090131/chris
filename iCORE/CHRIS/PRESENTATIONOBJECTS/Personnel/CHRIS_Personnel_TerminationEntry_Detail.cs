﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_TerminationEntry_Detail : Form
    {
        public CHRIS_Personnel_TerminationEntry_Detail()
        {
            InitializeComponent();
        }

        private void CHRIS_Personnel_TerminationEntry_Detail_Load(object sender, EventArgs e)
        {

            this.txtFirstName.ReadOnly = false;
            this.txtTerminType.ReadOnly = false;
            this.txtReason.ReadOnly = false;
            this.txtResgRecieved.ReadOnly = false;
            this.txtResgAppr.ReadOnly = false;
            this.txtFinalSettlementReport.ReadOnly = false;
            this.txtDelFromMic.ReadOnly = false;
            this.txtPowerOfAttornyCancel.ReadOnly = false;
            this.txtNoticeOfStaffChanged.ReadOnly = false;
            this.txtIDCardRet.ReadOnly = false;
            this.txtExitIntrvDone.ReadOnly = false;


            DataTable dtDept = new DataTable();
            dtDept = SQLManager.CHRIS_SP_GET_Termin_PRNO(CHRIS_Personnel_TerminationEntry_View.SetValuePR_NO).Tables[0];

            foreach (System.Data.DataRow dtrow in dtDept.Rows)
            {
                // genLinkRow["CustomerNo"] = customerNo = string.IsNullOrEmpty(dtrow["CustomerNo"].ToString().Trim()) ? "" : dtrow["CustomerNo"].ToString().Trim();

                txtPersNo.Text = string.IsNullOrEmpty(dtrow["PR_P_NO"].ToString().Trim()) ? "" : dtrow["PR_P_NO"].ToString().Trim();

                txtFirstName.Text = string.IsNullOrEmpty(dtrow["PR_FIRST_NAME"].ToString().Trim()) ? "" : dtrow["PR_FIRST_NAME"].ToString().Trim();

                dtpTerminDate.Text = string.IsNullOrEmpty(dtrow["PR_TERMIN_DATE"].ToString().Trim()) ? "" : dtrow["PR_TERMIN_DATE"].ToString().Trim();

                txtTerminType.Text = string.IsNullOrEmpty(dtrow["PR_TERMIN_TYPE"].ToString().Trim()) ? "" : dtrow["PR_TERMIN_TYPE"].ToString().Trim();

                txtReason.Text = string.IsNullOrEmpty(dtrow["PR_REASONS"].ToString().Trim()) ? "" : dtrow["PR_REASONS"].ToString().Trim();

                txtResgRecieved.Text = string.IsNullOrEmpty(dtrow["PR_RESIG_RECV"].ToString().Trim()) ? "" : dtrow["PR_RESIG_RECV"].ToString().Trim();

                txtResgAppr.Text = string.IsNullOrEmpty(dtrow["PR_APP_RESIG"].ToString().Trim()) ? "" : dtrow["PR_APP_RESIG"].ToString().Trim();

                txtExitIntrvDone.Text = string.IsNullOrEmpty(dtrow["PR_EXIT_INTER"].ToString().Trim()) ? "" : dtrow["PR_EXIT_INTER"].ToString().Trim();

                txtIDCardRet.Text = string.IsNullOrEmpty(dtrow["PR_ID_RETURN"].ToString().Trim()) ? "" : dtrow["PR_ID_RETURN"].ToString().Trim();

                txtNoticeOfStaffChanged.Text = string.IsNullOrEmpty(dtrow["PR_NOTICE"].ToString().Trim()) ? "" : dtrow["PR_NOTICE"].ToString().Trim();

                txtPowerOfAttornyCancel.Text = string.IsNullOrEmpty(dtrow["PR_ARTONY"].ToString().Trim()) ? "" : dtrow["PR_ARTONY"].ToString().Trim();

                txtDelFromMic.Text = string.IsNullOrEmpty(dtrow["PR_DELETION"].ToString().Trim()) ? "" : dtrow["PR_DELETION"].ToString().Trim();

                txtFinalSettlementReport.Text = string.IsNullOrEmpty(dtrow["PR_SETTLE"].ToString().Trim()) ? "" : dtrow["PR_SETTLE"].ToString().Trim();

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAproved_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersNo.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersNo.Text.Trim());
                SQLManager.ApprovedTermination(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }


        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersNo.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersNo.Text.Trim());
                SQLManager.ApprovedTerminReject(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }

        }
    }
}
