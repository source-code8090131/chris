using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_FastOrISTransfer_TransferPage :ChrisSimpleForm
    {

        #region Variables Declaration
        private CHRIS_Personnel_FastOrISTransfer _mainForm = null;
        string Pr_P_No;
        string Tr_Type;
        string frmOption;
        string typeOf;
        string userid;
        DateTime joiningDate;
        string globalTransferDate = "";
        bool Isquit = false;
        #endregion

        #region Constructor
        public CHRIS_Personnel_FastOrISTransfer_TransferPage()
        {
            InitializeComponent();       
        }
      
        public CHRIS_Personnel_FastOrISTransfer_TransferPage(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_FastOrISTransfer mainForm)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this._mainForm = mainForm;

            Pr_P_No = this._mainForm.txtPersNo.Text;
            Tr_Type = this._mainForm.txtPRTransferTypeGlobal.Text;
            frmOption = this._mainForm.txtOption.Text;
            typeOf = this._mainForm.txtTransferType.Text;
            joiningDate = Convert.ToDateTime(this._mainForm.txtJoiningDate.Text);
            userid = this._mainForm.UserName;

        }
        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtPR_EFFECTIVE.Text = null;
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = false;
            tbtDelete.Enabled = false;
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtCancel.Visible = false;
            tbtDelete.Visible = false;
            tbtAdd.Visible=false;
            tbtClose.Visible=false;
            ShowOptionTextBox = false;
            txtPrPNo.Text = Pr_P_No;

            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.CurrentOption = this._mainForm.FunctionConfig.CurrentOption;
            txtPRTransferType.Text = Tr_Type;
            dtJoiningDate.Value = joiningDate;
            WOption.Text = frmOption;
            W_TypeOf.Text = typeOf;
            txtOption.Text = WOption.Text;
            ShowStatusBar = false;
            ShowBottomBar = false;
            txtPR_EFFECTIVE.Value = null;
            dtFastEndDate.Value = null;
            this.FunctionConfig.EnableF10 = true;
            DataTable DtPersonal;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("PR_TR_NO", txtPrPNo.Text);
            DtPersonal = GetData("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "DtTransferDate", colsNVals);
            if (DtPersonal != null)
            {
                if (DtPersonal.Rows.Count > 0)
                {
                    if (DtPersonal.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        dtTransferDate.Value = Convert.ToDateTime(DtPersonal.Rows[0].ItemArray[0]);
                    }
                    else
                    {
                        //dtTransferDate.Value = null;
                        globalTransferDate = "1";
                    }

                }
            }
            else
            {
                //dtTransferDate.Value = null;
                globalTransferDate = "1";
            }


            if (_mainForm.DtTransfer != null)
            {
                SetExecuteQuery();
            }

            txtCountry.Select();
            txtCountry.Focus();

        }

       
        private bool IsValidated()
        {
            bool validated = true;

          
            if (FunctionConfig.CurrentOption != Function.Delete)
            {
                //if (this.txtCountry.Text == "")
                //{
                //    validated = false;
                //    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
                //}
                if (this.txtCity.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
                }
                if (this.txtDesg.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
                }
                if (this.txtLevel.Text == "")
                {
                    validated = false;
                    //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
                }

                if (this.txtCurrAnnual.Text == "")
                {
                    validated = false;
                }

                if (this.txtNewAnnual.Text == "")
                {
                    validated = false;
                }

                if (this.txtCurrency.Text == "")
                {
                    validated = false;
                }

                if (this.txtPR_EFFECTIVE.Value ==null)
                {
                    validated = false;
                }
              
             
            }


            return validated;
        }
        protected override bool Save()
        {
            if (!this.IsValidated())
                return false;
            this.Hide();

            DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {

                //SetEntityValues();
                //this._mainForm.DoToolbarActions(this._mainForm.Controls, "Save");

                #region ISFASTLOCAL Table 

                DataTable genLinkFile = new DataTable();
                
                genLinkFile.Columns.Add("PR_TR_NO", typeof(int));
                genLinkFile.Columns.Add("PR_TRANSFER_TYPE", typeof(int));
                genLinkFile.Columns.Add("PR_FUNC_1", typeof(string));
                genLinkFile.Columns.Add("PR_FUNC_2", typeof(string));
                genLinkFile.Columns.Add("PR_NEW_BRANCH", typeof(string));
                genLinkFile.Columns.Add("PR_FURLOUGH", typeof(string));
                genLinkFile.Columns.Add("PR_DESG", typeof(string));
                genLinkFile.Columns.Add("PR_LEVEL", typeof(string));
                genLinkFile.Columns.Add("PR_COUNTRY", typeof(string));
                genLinkFile.Columns.Add("PR_CITY", typeof(string));
                genLinkFile.Columns.Add("PR_DEPARTMENT_HC", typeof(string));
                genLinkFile.Columns.Add("PR_ASR_DOL", typeof(double));
                genLinkFile.Columns.Add("PR_FAST_END_DATE", typeof(DateTime));
                genLinkFile.Columns.Add("PR_IS_COORDINAT", typeof(string));
                genLinkFile.Columns.Add("PR_REMARKS", typeof(string));
                genLinkFile.Columns.Add("PR_FAST_CONVER", typeof(DateTime));
                genLinkFile.Columns.Add("PR_FLAG", typeof(string));
                genLinkFile.Columns.Add("PR_EFFECTIVE", typeof(DateTime));
                genLinkFile.Columns.Add("PR_RENT", typeof(double));
                genLinkFile.Columns.Add("PR_UTILITIES", typeof(double));
                genLinkFile.Columns.Add("PR_TAX_ON_TAX", typeof(double));
                genLinkFile.Columns.Add("PR_OLD_BRANCH", typeof(string));
                genLinkFile.Columns.Add("CITI_FLAG1", typeof(string));
                genLinkFile.Columns.Add("CITI_FLAG2", typeof(string));
                genLinkFile.Columns.Add("PR_NEW_ASR", typeof(double));
                genLinkFile.Columns.Add("CURRENCY_TYPE", typeof(string));
              //  genLinkFile.Columns.Add("ID", typeof(string));
                genLinkFile.Columns.Add("W_TypeOf", typeof(string));
                genLinkFile.Columns.Add("pr_joining_date", typeof(DateTime));
                genLinkFile.Columns.Add("PR_TERMIN_DATE", typeof(DateTime));
                genLinkFile.Columns.Add("PR_TRANSFER_DATE", typeof(DateTime));
                genLinkFile.Columns.Add("PR_UserID", typeof(string));
                genLinkFile.Columns.Add("PR_UploadDate", typeof(string));
                genLinkFile.Columns.Add("PR_User_Approved", typeof(bool));



                DataRow genLinkRow;
                
                {
                    genLinkRow = genLinkFile.NewRow();

                    string PRNO = string.IsNullOrEmpty(txtPrPNo.Text.Trim()) ? "" : txtPrPNo.Text.Trim();
                    genLinkRow["PR_TR_NO"] = Convert.ToInt32(PRNO);

                    int getTypeID = Convert.ToInt32(string.IsNullOrEmpty(txtPRTransferType.Text.Trim()) ? "" : txtPRTransferType.Text.Trim());
                    genLinkRow["PR_TRANSFER_TYPE"] = getTypeID;

                    genLinkRow["PR_FUNC_1"] = "";
                    genLinkRow["PR_FUNC_2"] = "";
                    genLinkRow["PR_NEW_BRANCH"] = "";
                    genLinkRow["PR_FURLOUGH"] = string.IsNullOrEmpty(txtfurloughCity.Text.Trim()) ? "" : txtfurloughCity.Text.Trim();
                    genLinkRow["PR_DESG"] = string.IsNullOrEmpty(txtDesg.Text.Trim()) ? "" : txtDesg.Text.Trim();
                    genLinkRow["PR_LEVEL"] = string.IsNullOrEmpty(txtLevel.Text.Trim()) ? "" : txtLevel.Text.Trim();
                    genLinkRow["PR_COUNTRY"] = string.IsNullOrEmpty(txtCountry.Text.Trim()) ? "" : txtCountry.Text.Trim();
                    genLinkRow["PR_CITY"] = string.IsNullOrEmpty(txtCity.Text.Trim()) ? "" : txtCity.Text.Trim();
                    genLinkRow["PR_DEPARTMENT_HC"] = string.IsNullOrEmpty(txtPRDept.Text.Trim()) ? "" : txtPRDept.Text.Trim();
                    genLinkRow["PR_ASR_DOL"] = Convert.ToDouble(string.IsNullOrEmpty(txtCurrAnnual.Text.Trim()) ? "" : txtCurrAnnual.Text.Trim());
                    string dateFastEndDate = string.IsNullOrEmpty(dtFastEndDate.Text.Trim()) ? "" : dtFastEndDate.Text.Trim();

                    if (string.IsNullOrEmpty(dateFastEndDate))
                    {
                        genLinkRow["PR_FAST_END_DATE"] = DateTime.Now;
                    }
                    else
                    {
                        genLinkRow["PR_FAST_END_DATE"] = Convert.ToDateTime(dateFastEndDate);
                    }

                    genLinkRow["PR_IS_COORDINAT"] = string.IsNullOrEmpty(txtIsCordinate.Text.Trim()) ? "" : txtIsCordinate.Text.Trim();
                    genLinkRow["PR_REMARKS"] = string.IsNullOrEmpty(txtPR_REMARKS.Text.Trim()) ? "" : txtPR_REMARKS.Text.Trim();
                    genLinkRow["PR_FAST_CONVER"] = DateTime.Now;
                    genLinkRow["PR_FLAG"] = "";
                    genLinkRow["PR_EFFECTIVE"] = Convert.ToDateTime(string.IsNullOrEmpty(txtPR_EFFECTIVE.Text.Trim()) ? "" : txtPR_EFFECTIVE.Text.Trim());
                    genLinkRow["PR_RENT"] = Convert.ToDouble("0");
                    genLinkRow["PR_UTILITIES"] = Convert.ToDouble("0"); ;
                    genLinkRow["PR_TAX_ON_TAX"] = Convert.ToDouble("0"); ;
                    genLinkRow["PR_OLD_BRANCH"] = "";
                    genLinkRow["CITI_FLAG1"] = string.IsNullOrEmpty(txtCITI_FLAG1.Text.Trim()) ? "" : txtCITI_FLAG1.Text.Trim();
                    genLinkRow["CITI_FLAG2"] = string.IsNullOrEmpty(txtCITI_FLAG2.Text.Trim()) ? "" : txtCITI_FLAG2.Text.Trim();
                    genLinkRow["PR_NEW_ASR"] = Convert.ToDouble(string.IsNullOrEmpty(txtNewAnnual.Text.Trim()) ? "" : txtNewAnnual.Text.Trim());
                    genLinkRow["CURRENCY_TYPE"] = string.IsNullOrEmpty(txtCurrency.Text.Trim()) ? "" : txtCurrency.Text.Trim();
                    genLinkRow["W_TypeOf"] = string.IsNullOrEmpty(W_TypeOf.Text.Trim()) ? "" : W_TypeOf.Text.Trim();
                    string dtJoinDate = string.IsNullOrEmpty(dtJoiningDate.Text.Trim()) ? "" : dtJoiningDate.Text.Trim();

                    if (string.IsNullOrEmpty(dtJoinDate))
                    {
                       genLinkRow["pr_joining_date"] = DateTime.Now;
                    }
                    else
                    {
                        genLinkRow["pr_joining_date"] = Convert.ToDateTime(dtJoinDate);
                    }


                    genLinkRow["PR_TERMIN_DATE"] = DateTime.Now;
                    string dtTransDate = string.IsNullOrEmpty(dtTransferDate.Text.Trim()) ? "" : dtTransferDate.Text.Trim();

                    if (string.IsNullOrEmpty(dtTransDate))
                    {
                      genLinkRow["PR_TRANSFER_DATE"] = DateTime.Now;
                    }
                    else
                    {
                        genLinkRow["PR_TRANSFER_DATE"] = Convert.ToDateTime(dtTransDate);
                    }

                    genLinkRow["PR_UserID"] = userid; //(this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId(); 
                    genLinkRow["PR_UploadDate"] = DateTime.Now.ToString();
                    genLinkRow["PR_User_Approved"] = false;

                    genLinkFile.Rows.Add(genLinkRow);

                    //SQLManager.PR_TERMINATION(PR_P_NO, PR_FIRST_NAME, PR_TERMIN_TYPE, PR_TERMIN_DATE, PR_REASONS, PR_RESIG_RECV, PR_APP_RESIG, PR_EXIT_INTER, PR_ID_RETURN, PR_NOTICE,
                    //                          PR_ARTONY, PR_DELETION, PR_SETTLE, PR_UserID);


                }
                uploadData(genLinkFile);

                MessageBox.Show("Record Save Successfully");



                #endregion



                #region Continue The Process For More Records

                DialogResult dRes1 = MessageBox.Show("Continue The Process For More Records [Yes/No]..", "Note"
                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes1 == DialogResult.Yes)
                {
                    this.Hide();           
                    _mainForm.AddMoreRecords();
                    this.Close();
                   
                         
                }
                else if (dRes1 == DialogResult.No)
                {
                    this.Hide();

                    this._mainForm.DoToolbarActions(this._mainForm.Controls, "Cancel");
                    this.Close();

                   
                }

                #endregion
               
                return false;

            }
            else if (dRes == DialogResult.No)
            {
                //ClearForm(pnlBlkTransfer);
                this.Hide();    
               
                this._mainForm.DoToolbarActions(this._mainForm.Controls, "Cancel");
                this.Close();

                return false;
            }

            this.Close();
            return false;
        }

        public static void uploadData(DataTable dt)
        {
            try
            {
                SQLManager.ExecuteISFASTLOCALDetailUpload(dt);
            }
            catch (Exception ex)
            {

            }
        }



        protected override bool Quit()
        {
          
            if (this._mainForm.txtTransferType.Text != string.Empty)
            {
                Isquit = true;
           
            }
            else
            {
                this._mainForm.QuitNew();
            }
            if (Isquit)
            {
                this._mainForm.DoToolbarActions(this._mainForm.Controls, "Cancel");
            }
            this.Close();
            return false;
        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }
        private void PROC_1()
        {
            DateTime dtDate = new DateTime();
            DateTime dtDate1 = new DateTime();
            DataTable DtTransfer;
            DataTable DtTransfer2;
            DateTime date1 = new DateTime();
            date1 = Convert.ToDateTime(txtPR_EFFECTIVE.Value);
            Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
            colsNVals1.Clear();
            colsNVals1.Add("PR_TR_NO", txtPrPNo.Text);
            colsNVals1.Add("PR_EFFECTIVE", txtPR_EFFECTIVE.Value);

            Dictionary<string, object> colsNVals2 = new Dictionary<string, object>();
            colsNVals2.Clear();
            colsNVals2.Add("PR_TR_NO", txtPrPNo.Text);

            DtTransfer = GetData("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "TransferEffectiveProc1", colsNVals1);
            DtTransfer2 = GetData("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "TransferProc1", colsNVals2);
            if (DtTransfer != null)
            {
                if (DtTransfer.Rows.Count == 1)
                {
                    #region Single row exists
                    if (DtTransfer.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        dtDate = Convert.ToDateTime(DtTransfer.Rows[0].ItemArray[0]);
                    }
                    this.txtPR_EFFECTIVE.Value = null;
                    MessageBox.Show("INVALID DATE.DATE HAS TO BE > THAN " + dtDate.ToString("MM/dd/yyyy"));
                    txtPR_EFFECTIVE.Focus();
                    return;
                    #endregion
                }
                else if (DtTransfer.Rows.Count == 0)
                {
                    #region No rows in DataTransfer
                    if (W_TypeOf.Text == "LOCAL")
                    {
                        //No rows in DataTransfer
                        CHRIS_Personnel_FastOrISTransfer_FastEndDatePage process = new CHRIS_Personnel_FastOrISTransfer_FastEndDatePage(date1, date1, txtOption.Text,this);
                        process.OperationMode = this._mainForm.txtOption.Text;
                        if (dtFastEndDate.Value != null)
                        {
                            process.DateValue = Convert.ToDateTime(dtFastEndDate.Value);
                        }
                        else
                        {
                            process.DateValue = null;
                        }
                        process.ShowDialog();
                        dtFastEndDate.Value = process.DateValue;
                        process.Close();

                        this.txtCITI_FLAG1.Focus();

                    }
                    else if (W_TypeOf.Text == "IS")
                    {
                        // No rows in DataTransfer 
                        CHRIS_Personnel_FastOrISTransfer_CityPage process1 = new CHRIS_Personnel_FastOrISTransfer_CityPage(this);
                        process1.OperationMode = this._mainForm.txtOption.Text;
                        process1.furloughCity = txtfurloughCity.Text;
                        process1.IsCordinate = txtIsCordinate.Text;
                        process1.ShowDialog();
                        txtfurloughCity.Text = process1.furloughCity;
                        txtIsCordinate.Text = process1.IsCordinate;
                        process1.Close();

                        this.txtCITI_FLAG1.Focus();
                    }
                    #endregion
                }
                else if (DtTransfer.Rows.Count > 1)
                {
                    #region More Than One row exists
                    if (DtTransfer2 != null)
                    {
                        if (DtTransfer2.Rows.Count > 0)
                        {

                            if (DtTransfer2.Rows[0].ItemArray[0].ToString() != string.Empty)
                            {
                                dtDate1 = Convert.ToDateTime(DtTransfer2.Rows[0].ItemArray[0]);
                            }
                            this.txtPR_EFFECTIVE.Value = null;
                            MessageBox.Show("INVALID DATE.DATE HAS TO BE GREATER THAN  " + dtDate1.ToString("MM/dd/yyyy"));
                            txtPR_EFFECTIVE.Focus();
                            return;

                        }

                    }
                    #endregion
                }

            }
            else
            {
                if (W_TypeOf.Text == "LOCAL")
                {
                    //  go_field('BLKTWO.PR_FAST_END_DATE');
                    CHRIS_Personnel_FastOrISTransfer_FastEndDatePage process = new CHRIS_Personnel_FastOrISTransfer_FastEndDatePage(date1, date1, txtOption.Text,this);
                    process.OperationMode = this._mainForm.txtOption.Text;
                    if (dtFastEndDate.Value != null)
                    {
                        process.DateValue = Convert.ToDateTime(dtFastEndDate.Value);
                    }
                    else
                    {
                        process.DateValue = null;
                    }
                    process.ShowDialog();
                    dtFastEndDate.Value = process.DateValue;
                    process.Close();

                    this.txtCITI_FLAG1.Focus();

                }
                else if (W_TypeOf.Text == "IS")
                {
                    // go_field('BLKTWO.PR_IS_COORDINAT');
                    CHRIS_Personnel_FastOrISTransfer_CityPage process1 = new CHRIS_Personnel_FastOrISTransfer_CityPage(this);
                    process1.OperationMode = this._mainForm.txtOption.Text;
                    process1.furloughCity = txtfurloughCity.Text;
                    process1.IsCordinate = txtIsCordinate.Text;
                    process1.ShowDialog();
                    txtfurloughCity.Text = process1.furloughCity;
                    txtIsCordinate.Text = process1.IsCordinate;
                    process1.Close();

                    this.txtCITI_FLAG1.Focus();
                }
            }


        }
        public void SetEntityValues()
        {
            int IDM=0;

            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferISLOCALCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferISLOCALCommand)this._mainForm.pnlDetail.CurrentBusinessEntity;
            ent.CITI_FLAG1 = this.txtCITI_FLAG1.Text;
            ent.CITI_FLAG2 = this.txtCITI_FLAG2.Text;
            ent.CURRENCY_TYPE = this.txtCurrency.Text;
            if (this.txtCurrAnnual.Text != string.Empty)
            {
                ent.PR_ASR_DOL = Convert.ToDecimal(this.txtCurrAnnual.Text);
            }
            if (this.txtNewAnnual.Text != string.Empty)
            {
                ent.PR_NEW_ASR = Convert.ToDecimal(this.txtNewAnnual.Text);
            }
            ent.PR_CITY = this.txtCity.Text;
            ent.PR_COUNTRY = this.txtCountry.Text;
            ent.PR_DEPARTMENT_HC = this.txtPRDept.Text;
            ent.PR_DESG = this.txtDesg.Text;
            if (this.txtPR_EFFECTIVE.Value != null)
            {
                ent.PR_EFFECTIVE = (DateTime)this.txtPR_EFFECTIVE.Value;
            }
            if (this.dtFastEndDate.Value != null)
            {
                ent.PR_FAST_END_DATE = (DateTime)this.dtFastEndDate.Value;
            }
            ent.PR_FURLOUGH = this.txtfurloughCity.Text;
            ent.PR_IS_COORDINAT = this.txtIsCordinate.Text;
            ent.PR_LEVEL = this.txtLevel.Text;
            ent.PR_REMARKS = this.txtPR_REMARKS.Text;
            ent.W_TypeOf = this.W_TypeOf.Text;
            if (this.txtPRTransferType.Text != string.Empty)
            {
                ent.PR_TRANSFER_TYPE = Convert.ToDecimal(this.txtPRTransferType.Text);
            }
            //ent.PR_TRANSFER_DATE = (DateTime)this.dtTransferDate.Value;
            if (this.dtJoiningDate.Value != null)
            {
                ent.pr_joining_date = (DateTime)this.dtJoiningDate.Value;
            }
            //ent.PR_TERMIN_DATE = (DateTime)this.dtPR_TERMIN_DATE.Value;
            if(txtID.Text !=string.Empty)
            {
                if(int.TryParse(txtID.Text,out IDM))
                {
                    ent.ID = IDM;
                   _mainForm.SetID(IDM);
                }
            }

        }
        public void SetExecuteQuery()
        {
            if (_mainForm.DtTransfer != null)
            {
                if (_mainForm.DtTransfer.Rows.Count > 0)
                {
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[0] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[0].ToString())))
                    {
                        txtCountry.Text = _mainForm.DtTransfer.Rows[0].ItemArray[0].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[1] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[1].ToString())))
                    {
                        txtCity.Text = _mainForm.DtTransfer.Rows[0].ItemArray[1].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[2] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[2].ToString())))
                    {
                        txtDesg.Text = _mainForm.DtTransfer.Rows[0].ItemArray[2].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[3] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[3].ToString())))
                    {
                        txtLevel.Text = _mainForm.DtTransfer.Rows[0].ItemArray[3].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[4] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[4].ToString())))
                    {
                        txtPRDept.Text = _mainForm.DtTransfer.Rows[0].ItemArray[4].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[5] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[5].ToString())))
                    {
                        txtCurrAnnual.Text = _mainForm.DtTransfer.Rows[0].ItemArray[5].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[6] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[6].ToString())))
                    {
                        txtNewAnnual.Text = _mainForm.DtTransfer.Rows[0].ItemArray[6].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0].ItemArray[7] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0].ItemArray[7].ToString())))
                    {
                        txtCurrency.Text = _mainForm.DtTransfer.Rows[0].ItemArray[7].ToString();
                    }
                    if (_mainForm.DtTransfer.Rows[0].ItemArray[8].ToString() != string.Empty)
                    {
                        txtPR_EFFECTIVE.Value = Convert.ToDateTime(_mainForm.DtTransfer.Rows[0].ItemArray[8]);
                    }
                    txtCITI_FLAG1.Text = _mainForm.DtTransfer.Rows[0].ItemArray[9].ToString();
                    txtCITI_FLAG2.Text = _mainForm.DtTransfer.Rows[0].ItemArray[10].ToString();
                    txtPR_REMARKS.Text = _mainForm.DtTransfer.Rows[0].ItemArray[11].ToString();

                    if ((_mainForm.DtTransfer.Rows[0]["PR_FURLOUGH"] != DBNull.Value) && (_mainForm.DtTransfer.Rows[0]["PR_FURLOUGH"] != string.Empty))
                    {
                        txtfurloughCity.Text = _mainForm.DtTransfer.Rows[0]["PR_FURLOUGH"].ToString();
                    }
                    if ((_mainForm.DtTransfer.Rows[0]["PR_IS_COORDINAT"] != DBNull.Value) && (_mainForm.DtTransfer.Rows[0]["PR_FURLOUGH"] != string.Empty))
                    {
                        txtIsCordinate.Text = _mainForm.DtTransfer.Rows[0]["PR_IS_COORDINAT"].ToString();
                    }
                    if (_mainForm.DtTransfer.Rows[0].ItemArray[12].ToString() != string.Empty)
                    {
                        dtFastEndDate.Value = Convert.ToDateTime(_mainForm.DtTransfer.Rows[0].ItemArray[12]);
                    }
                      if ((_mainForm.DtTransfer.Rows[0]["ID"] != System.DBNull.Value) && (!String.IsNullOrEmpty(_mainForm.DtTransfer.Rows[0]["ID"].ToString())))
                        {
                            txtID.Text = _mainForm.DtTransfer.Rows[0]["ID"].ToString();
                      }
                    //if (WOption.Text == "V")
                    //{
                    //    DisableControls();                      
                    //}
                }
            }
        }
        private void DisableControls()
        {
            txtCountry.Enabled = false;
            txtCity.Enabled = false;
            txtDesg.Enabled = false;
            txtLevel.Enabled = false;
            txtPRDept.Enabled = false;
            txtCurrAnnual.Enabled = false;
            txtNewAnnual.Enabled = false;
            txtCurrency.Enabled = false;
            txtPR_EFFECTIVE.Enabled = false;
            txtCITI_FLAG1.Enabled = false;
            txtCITI_FLAG2.Enabled = false;
            txtPR_REMARKS.Enabled = false;
        }
       
        #endregion

        #region Events

        private void txtPRDept_Validating(object sender, CancelEventArgs e)
        {
             DataTable DtAnnualPack;
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", txtPrPNo.Text);

                DtAnnualPack = GetData("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "PersonalAnnual", colsNVals);

                 if (DtAnnualPack != null)
                 {
                     if (DtAnnualPack.Rows.Count > 0)
                     {
                         if(DtAnnualPack.Rows[0].ItemArray[0] !=null)
                         {
                             if(DtAnnualPack.Rows[0].ItemArray[0].ToString() != string.Empty)
                             {
                                 txtCurrAnnual.Text = DtAnnualPack.Rows[0].ItemArray[0].ToString();

                             }
                         }
                     }


                 }
                 
        }

        private void txtNewAnnual_Validating(object sender, CancelEventArgs e)
        {
            if (txtCurrency.Text == string.Empty)
            {
                txtCurrency.Text = "PAK RS.";
            }
            txtCurrency.Focus();
        }

        private void txtPR_EFFECTIVE_Validating(object sender, CancelEventArgs e)
        {
            
            DateTime date2 = new DateTime();
            date2 = Convert.ToDateTime(dtJoiningDate.Value);
            if (txtPR_EFFECTIVE.Value != null)
            {
                DateTime date1 = new DateTime();
                
                DateTime date3 = new DateTime();

                date1 = Convert.ToDateTime(txtPR_EFFECTIVE.Value);
              
                if (globalTransferDate != "1")
                {
                    date3 = Convert.ToDateTime(dtTransferDate.Value);
                }
                if (DateTime.Compare(date1.Date, date2.Date) < 0)
                {
                    
                    MessageBox.Show("DATE HAS TO BE GREATER THAN " + date2.ToString("dd-MMM-yy") + " (JOINING DATE)");
                    txtPR_EFFECTIVE.Focus();
                    e.Cancel = true;
                    return;
                }
                if (WOption.Text == "A")
                {
                    if (DateTime.Compare(date1.Date, date3.Date) == 0)
                    {
                       
                        MessageBox.Show("RECORD FOR THIS DATE ALREADY EXISTS");
                        txtPR_EFFECTIVE.Focus();
                        e.Cancel = true;
                        return;
                    }

                    else
                    {
                        //pROC 1
                        PROC_1();
                    }
                }
                if (WOption.Text == "M")
                {
                    if (DateTime.Compare(date1.Date,date3.Date) !=0)
                    {
                        // proc_1;
                        PROC_1();
                    }

                    else
                    {
                        if (W_TypeOf.Text == "LOCAL")
                        {
                            //  go_field('BLKTWO.PR_FAST_END_DATE');
                            if (this.txtPR_EFFECTIVE.Value != null)
                            {
                                CHRIS_Personnel_FastOrISTransfer_FastEndDatePage process = new CHRIS_Personnel_FastOrISTransfer_FastEndDatePage(date1, date1,txtOption.Text,this);
                                process.OperationMode = this._mainForm.txtOption.Text;
                                if (dtFastEndDate.Value != null)
                                {
                                    process.DateValue = Convert.ToDateTime(dtFastEndDate.Value);
                                }
                                else
                                {
                                    process.DateValue = null;
                                }
                                process.ShowDialog();
                                dtFastEndDate.Value = process.DateValue;
                                process.Close();

                                this.txtCITI_FLAG1.Focus();

                            }
                        }
                        else if (W_TypeOf.Text == "IS")
                        {
                            // go_field('BLKTWO.PR_IS_COORDINAT');

                            //Added by Anila on 22nd march 2011
                            //Reason: To read "furloughCity" and "IsCordinate" from another form
                            CHRIS_Personnel_FastOrISTransfer_CityPage process1 = new CHRIS_Personnel_FastOrISTransfer_CityPage(this);                          

                            process1.OperationMode = this._mainForm.txtOption.Text;
                            process1.furloughCity = txtfurloughCity.Text;
                            process1.IsCordinate = txtIsCordinate.Text;
                            process1.ShowDialog();

                            //To Save "furloughCity" and "IsCordinate"
                            txtfurloughCity.Text = process1.furloughCity;
                            txtIsCordinate.Text = process1.IsCordinate;
                            process1.Close();

                            this.txtCITI_FLAG1.Focus();
                        }
                    }
                }
            }
            else
            {

                this.txtPR_EFFECTIVE.Value = null;
                MessageBox.Show("YOU HAVE NOT ENTERED THE DATE");
                txtPR_EFFECTIVE.Focus();
                return;
            }

        }
   
        private void txtPR_REMARKS_Validating(object sender, CancelEventArgs e)
        {
          
              
        }

        private void txtCITI_FLAG2_KeyUp(object sender, KeyEventArgs e)
        {

          
               
            
        }

        private void CHRIS_Personnel_FastOrISTransfer_TransferPage_Shown(object sender, EventArgs e)
        {
            if (WOption.Text == "V" || WOption.Text == "D")
            //if (WOption.Text == "M" || WOption.Text == "V" || WOption.Text == "D")
                {
                    if (_mainForm.DtTransfer != null)
                    {
                        if (_mainForm.DtTransfer.Rows.Count > 0)
                        {
                            if (_mainForm.DtTransfer.Rows[0].ItemArray[12].ToString() != string.Empty)
                            {
                                DateTime dtDate1 = new DateTime();
                                dtDate1 = Convert.ToDateTime(txtPR_EFFECTIVE.Value);

                                if (W_TypeOf.Text == "LOCAL")
                                {
                                    //show_page(9);
                                    CHRIS_Personnel_FastOrISTransfer_FastEndDatePage process = new CHRIS_Personnel_FastOrISTransfer_FastEndDatePage(dtDate1, Convert.ToDateTime(_mainForm.DtTransfer.Rows[0].ItemArray[12]),txtOption.Text,this);
                                    process.OperationMode = this._mainForm.txtOption.Text;
                                    if (dtFastEndDate.Value != null)
                                    {
                                        process.DateValue = Convert.ToDateTime(dtFastEndDate.Value);
                                    }
                                    else
                                    {
                                        process.DateValue = null;
                                    }
                                    process.ShowDialog();
                                }
                            }
                            if (W_TypeOf.Text == "IS")
                            {
                                // show_page(8);
                                CHRIS_Personnel_FastOrISTransfer_CityPage process1 = new CHRIS_Personnel_FastOrISTransfer_CityPage(this);
                                process1.OperationMode = this._mainForm.txtOption.Text;
                                process1.IsCordinate = this.txtIsCordinate.Text;
                                process1.furloughCity = this.txtfurloughCity.Text;
                                process1.ShowDialog();

                            }
                        }
                        //if (WOption.Text == "D")
                        //{
                        //    this._mainForm.DoToolbarActions(this._mainForm.Controls, "Delete");
                        //}
                    }

            }

        }
                     
       
        #endregion

        private void txtPR_REMARKS_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                CHRIS_Personnel_FastOrISTransfer_CityPage process1 = new CHRIS_Personnel_FastOrISTransfer_CityPage(this);
                process1.OperationMode = this._mainForm.txtOption.Text;
                process1.furloughCity = txtfurloughCity.Text;
                process1.IsCordinate = txtIsCordinate.Text;

                process1.ShowDialog();

                txtfurloughCity.Text = process1.furloughCity;
                txtIsCordinate.Text = process1.IsCordinate;
                process1.Close();

                this.txtCITI_FLAG1.Focus();
                return;
            }
        }

        private void txtPR_REMARKS_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    MessageBox.Show(" Press <F10> TO Save   <F6> Exit W/O Save");
                    e.IsInputKey = true;

                    txtPR_REMARKS.Focus();
                    return;
            

                }

            }
                    
        }

        public void showDeleteMsg(CHRIS_Personnel_FastOrISTransfer_FastEndDatePage frm)
        {
            DialogResult dRes = MessageBox.Show("Do You Want To Delete The Record [Y]es or [N]o", "Note"
                                                 , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {

                //base.DoToolbarActions(this.Controls, "Delete");
                CustomDelete();

                frm.Hide();
                frm.Close();
                base.ClearForm(this.Controls);
                this.Hide();

                this._mainForm.DoToolbarActions(_mainForm.Controls, "Cancel");
                this.Close();
                return;

            }
            else if (dRes == DialogResult.No)
            {
                frm.Hide();
                frm.Close();
                base.ClearForm(this.Controls);
                this.Hide();

               
                this._mainForm.DoToolbarActions(_mainForm.Controls, "Cancel");

                this.Close();
                //txtOption.Select();
                //txtOption.Focus();

                return;
            }
        }

        public void ShowViewMsg(CHRIS_Personnel_FastOrISTransfer_FastEndDatePage frm)
        {

            if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {



                frm.Hide();
                frm.Close();
                this.Hide();
                
                this._mainForm.ViewMoreRecords();
                this.Close();
                return;
             
              
            }
            else
            {
                frm.Hide();
                frm.Close();
                this.Hide();
                _mainForm.DoToolbarActions(_mainForm.Controls, "Cancel");
                this.Close();
                return;

                //txtOption.Select();
                //txtOption.Focus();
               // return;

            }

        }



        public void ShowViewMsgCity(CHRIS_Personnel_FastOrISTransfer_CityPage frm)
        {

            if (MessageBox.Show("Do You Want To View More Record [Y]es or [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {

                frm.Hide();
                frm.Close();
                this.Hide();
                this._mainForm.ViewMoreRecords();
                this.Close();
                return;
               
               


            }
            else
            {
                frm.Hide();
                frm.Close();
                this.Hide();
                _mainForm.DoToolbarActions(_mainForm.Controls, "Cancel");
                this.Close();
                return;

            }

        }


        public void showDeleteMsgCity(CHRIS_Personnel_FastOrISTransfer_CityPage frm)
        {
            DialogResult dRes = MessageBox.Show("Do You Want To Delete The Record [Y]es or [N]o", "Note"
                                                 , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {

                //base.DoToolbarActions(this.Controls, "Delete");
                CustomDelete();

                frm.Hide();
                frm.Close();
                base.ClearForm(this.Controls);
                this.Hide();
                this._mainForm.DoToolbarActions(_mainForm.Controls, "Cancel");
                this.Close();

                //call save
                return;

            }
            else if (dRes == DialogResult.No)
            {
                frm.Hide();
                frm.Close();
                base.ClearForm(this.Controls);
                this.Hide();

                this._mainForm.DoToolbarActions(_mainForm.Controls, "Cancel");
                this.Close();
                //txtOption.Select();
                //txtOption.Focus();

                 return;
            }
        }


        private void CustomDelete()
        {
            int IDM = 0;
            int ID = 0;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            if (txtID.Text != string.Empty)
            {
                if (int.TryParse(txtID.Text, out IDM))
                {
                    ID = IDM;
                }
            }
            if (ID > 0)
            {
                //ID = ID;
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", txtPrPNo.Text);
                colsNVals.Add("PR_EFFECTIVE", txtPR_EFFECTIVE.Value);

                rsltCode = cmnDM.Execute("CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER", "Delete", colsNVals);

                if (rsltCode.isSuccessful)
                {


                    MessageBox.Show("Record deleted successfully");
                    return;
                }
            }


        }

    }
}