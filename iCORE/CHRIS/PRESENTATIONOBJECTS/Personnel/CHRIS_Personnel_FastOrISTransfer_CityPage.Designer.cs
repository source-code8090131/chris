namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_FastOrISTransfer_CityPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtISCordinate = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtFurLoughCity = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.SuspendLayout();
            // 
            // txtISCordinate
            // 
            this.txtISCordinate.AllowSpace = true;
            this.txtISCordinate.AssociatedLookUpName = "";
            this.txtISCordinate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtISCordinate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtISCordinate.ContinuationTextBox = null;
            this.txtISCordinate.CustomEnabled = true;
            this.txtISCordinate.DataFieldMapping = "";
            this.txtISCordinate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtISCordinate.GetRecordsOnUpDownKeys = false;
            this.txtISCordinate.IsDate = false;
            this.txtISCordinate.Location = new System.Drawing.Point(73, 69);
            this.txtISCordinate.MaxLength = 10;
            this.txtISCordinate.Name = "txtISCordinate";
            this.txtISCordinate.NumberFormat = "###,###,##0.00";
            this.txtISCordinate.Postfix = "";
            this.txtISCordinate.Prefix = "";
            this.txtISCordinate.Size = new System.Drawing.Size(109, 20);
            this.txtISCordinate.SkipValidation = false;
            this.txtISCordinate.TabIndex = 0;
            this.txtISCordinate.TextType = CrplControlLibrary.TextType.String;
            this.txtISCordinate.Validating += new System.ComponentModel.CancelEventHandler(this.txtISCordinate_Validating);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(73, 46);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 20);
            this.label13.TabIndex = 84;
            this.label13.Text = "IS Co-ordinate :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFurLoughCity
            // 
            this.txtFurLoughCity.AllowSpace = true;
            this.txtFurLoughCity.AssociatedLookUpName = "";
            this.txtFurLoughCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFurLoughCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFurLoughCity.ContinuationTextBox = null;
            this.txtFurLoughCity.CustomEnabled = true;
            this.txtFurLoughCity.DataFieldMapping = "";
            this.txtFurLoughCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFurLoughCity.GetRecordsOnUpDownKeys = false;
            this.txtFurLoughCity.IsDate = false;
            this.txtFurLoughCity.IsRequired = true;
            this.txtFurLoughCity.Location = new System.Drawing.Point(73, 143);
            this.txtFurLoughCity.MaxLength = 10;
            this.txtFurLoughCity.Name = "txtFurLoughCity";
            this.txtFurLoughCity.NumberFormat = "###,###,##0.00";
            this.txtFurLoughCity.Postfix = "";
            this.txtFurLoughCity.Prefix = "";
            this.txtFurLoughCity.Size = new System.Drawing.Size(109, 20);
            this.txtFurLoughCity.SkipValidation = false;
            this.txtFurLoughCity.TabIndex = 1;
            this.txtFurLoughCity.TextType = CrplControlLibrary.TextType.String;
            this.txtFurLoughCity.Validating += new System.ComponentModel.CancelEventHandler(this.txtFurLoughCity_Validating);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(73, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 86;
            this.label1.Text = "Furlough City :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(268, 203);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(1, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 2;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Personnel_FastOrISTransfer_CityPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(269, 223);
            this.Controls.Add(this.slTextBox1);
            this.Controls.Add(this.txtFurLoughCity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtISCordinate);
            this.Controls.Add(this.label13);
            this.Name = "CHRIS_Personnel_FastOrISTransfer_CityPage";
            this.Text = "CHRIS_Personnel_FastOrISTransfer_CityPage";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_FastOrISTransfer_CityPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtISCordinate;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtFurLoughCity;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTextBox1;
    }
}