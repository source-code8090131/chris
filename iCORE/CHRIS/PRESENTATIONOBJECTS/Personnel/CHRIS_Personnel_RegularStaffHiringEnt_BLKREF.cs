using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKREF : TabularForm
    {
        #region --Variable--
        private DataTable dtDept;
        private DataTable dtEmp;
        private DataTable dtRef;
        private DataTable dtChild;
        private DataTable dtEdu;
        bool ValidationGrid = true;
        private CHRIS_Personnel_RegularStaffHiringEnt _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS frm_Pers;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP frm_Emp;
        string pr_SegmentValue = string.Empty;
        int refCode = 0;
        #endregion

        #region --Constructor--
        public CHRIS_Personnel_RegularStaffHiringEnt_BLKREF()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_RegularStaffHiringEnt_BLKREF(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_RegularStaffHiringEnt mainForm,
            CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS BLKPERS, CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP BLKEMP, DataTable _dtDept, DataTable _dtEmp, DataTable _dtRef, DataTable _dtChild, DataTable _dtEdu)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtDept  = _dtDept;
            dtEmp   = _dtEmp;
            dtRef   = _dtRef;
            dtChild = _dtChild;
            dtEdu   = _dtEdu;
            frm_Emp = BLKEMP;
            frm_Pers = BLKPERS;
            this._mainForm = mainForm;
            
        }
        #endregion

        #region --Events--
        /// <summary>
        /// Remove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        /// <summary>
        /// On FromLoad Set the GridView Data Source
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                if (frm_Pers != null)
                    frm_Pers.Hide();
                else if (frm_Emp != null)
                    frm_Emp.Hide();

                dgvBlkRef.GridSource    = dtRef;
                dgvBlkRef.DataSource    = dgvBlkRef.GridSource;
                dgvBlkRef.Focus();
                dgvBlkRef.CurrentCell   = dgvBlkRef[dgvBlkRef.FirstDisplayedCell.ColumnIndex, 0];

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("REF_P_NO", _mainForm.txt_PR_P_NO.Text);
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_EMP_REFERENCE_MANAGER", "REF_CODE", param);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    refCode = Convert.ToInt32(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                }

                this.KeyPreview         = true;
                this.KeyDown            += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
                dgvBlkRef.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        /// <summary>
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (_mainForm.txtOption.Text == "A")
                {
                    if (ValidationGrid && !dgvBlkRef.CurrentCell.IsInEditMode)
                    {
                        if (e.Modifiers == Keys.Control)
                        {
                            switch (e.KeyValue)
                            {
                                case 34:
                                    frm_Pers            = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, null,this , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Pers.MdiParent  = null;
                                    frm_Pers.Enabled    = true;
                                    this.Hide();
                                    frm_Pers.ShowDialog(this);
                                    //this.Close();
                                    this.KeyPreview     = true;
                                    break;

                                case 33:
                                    frm_Emp             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this ,null , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Emp.MdiParent   = null;
                                    frm_Emp.Enabled     = true;
                                    this.Hide();
                                    frm_Emp.ShowDialog(this);
                                    //this.Close();
                                    this.KeyPreview     = true;
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    if (e.Modifiers == Keys.Control)
                    {
                        switch (e.KeyValue)
                        {
                            case 34:
                                frm_Pers            = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm ,null ,this, dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                frm_Pers.MdiParent  = null;
                                frm_Pers.Enabled    = true;
                                this.Hide();
                                frm_Pers.ShowDialog(this);
                                //this.Close();
                                this.KeyPreview     = true;
                                break;

                            case 33:
                                frm_Emp             = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKEMP(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm,  this ,null , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                frm_Emp.MdiParent   = null;
                                frm_Emp.Enabled     = true;
                                this.Hide();
                                frm_Emp.ShowDialog(this);
                                //this.Close();
                                this.KeyPreview     = true;
                                break;
                        }
                    }
                }

            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }

        /// <summary>
        /// BlkRef GridView Cell Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkRef_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                ValidationGrid = true;
                dgvBlkRef.EndEdit();

                if (dgvBlkRef.Rows.Count > 0 && dgvBlkRef != null)
                {
                    if (dgvBlkRef.CurrentCell.OwningColumn.Name == "Ref_Name" && dgvBlkRef.CurrentCell.Value != null)
                    {
                        if (dgvBlkRef.CurrentCell.EditedFormattedValue.ToString() == "")
                        {
                            ValidationGrid = false;
                            e.Cancel = true;
                        }

                        if (_mainForm.txtOption.Text == "A")
                        {
                            dgvBlkRef.CurrentRow.Cells["ref_Code"].Value = refCode.ToString();
                        }

                        refCode = refCode + 1;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvBlkRef_CellValidating", exp);
            }
        }

        /// <summary>
        /// SET CharacterCasing UPPER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkRef_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }
       
        #endregion

        private void dgvBlkRef_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvBlkRef.BeginEdit(true);
        }
    }
}