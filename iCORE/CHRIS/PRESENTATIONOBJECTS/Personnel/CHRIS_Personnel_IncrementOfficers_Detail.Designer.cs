﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_IncrementOfficers_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txt_PR_EFFECTIVE = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.dtLastApp = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtNextApp = new CrplControlLibrary.SLDatePicker(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtPR_RANKING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPR_RANK = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_REMARKS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncrementPer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_ANNUAL_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncAmonut = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncremnttype = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAnnualPresnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESIG_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnel = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtWP_pre = new CrplControlLibrary.SLTextBox(this.components);
            this.btnAproved = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_PR_EFFECTIVE
            // 
            this.txt_PR_EFFECTIVE.AllowSpace = true;
            this.txt_PR_EFFECTIVE.AssociatedLookUpName = "lbtnDate";
            this.txt_PR_EFFECTIVE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_EFFECTIVE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_EFFECTIVE.ContinuationTextBox = null;
            this.txt_PR_EFFECTIVE.CustomEnabled = true;
            this.txt_PR_EFFECTIVE.DataFieldMapping = "";
            this.txt_PR_EFFECTIVE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_EFFECTIVE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_EFFECTIVE.IsDate = false;
            this.txt_PR_EFFECTIVE.Location = new System.Drawing.Point(275, 213);
            this.txt_PR_EFFECTIVE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_EFFECTIVE.Name = "txt_PR_EFFECTIVE";
            this.txt_PR_EFFECTIVE.NumberFormat = "dd/MM/yyyy";
            this.txt_PR_EFFECTIVE.Postfix = "";
            this.txt_PR_EFFECTIVE.Prefix = "";
            this.txt_PR_EFFECTIVE.Size = new System.Drawing.Size(133, 24);
            this.txt_PR_EFFECTIVE.SkipValidation = false;
            this.txt_PR_EFFECTIVE.TabIndex = 45;
            this.txt_PR_EFFECTIVE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(472, 248);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(136, 17);
            this.label24.TabIndex = 75;
            this.label24.Text = "(Before Effective)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(396, 188);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(277, 17);
            this.label23.TabIndex = 71;
            this.label23.Text = "[I]ncrement [A]djustment [P]romotion ";
            // 
            // dtLastApp
            // 
            this.dtLastApp.CustomEnabled = true;
            this.dtLastApp.CustomFormat = "dd/MM/yyyy";
            this.dtLastApp.DataFieldMapping = "";
            this.dtLastApp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtLastApp.HasChanges = true;
            this.dtLastApp.Location = new System.Drawing.Point(275, 373);
            this.dtLastApp.Margin = new System.Windows.Forms.Padding(4);
            this.dtLastApp.Name = "dtLastApp";
            this.dtLastApp.NullValue = " ";
            this.dtLastApp.Size = new System.Drawing.Size(132, 22);
            this.dtLastApp.TabIndex = 70;
            this.dtLastApp.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            // 
            // dtNextApp
            // 
            this.dtNextApp.CustomEnabled = true;
            this.dtNextApp.CustomFormat = "dd/MM/yyyy";
            this.dtNextApp.DataFieldMapping = "";
            this.dtNextApp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNextApp.HasChanges = true;
            this.dtNextApp.Location = new System.Drawing.Point(275, 407);
            this.dtNextApp.Margin = new System.Windows.Forms.Padding(4);
            this.dtNextApp.Name = "dtNextApp";
            this.dtNextApp.NullValue = " ";
            this.dtNextApp.Size = new System.Drawing.Size(132, 22);
            this.dtNextApp.TabIndex = 69;
            this.dtNextApp.Value = new System.DateTime(2011, 1, 15, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(29, 415);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(212, 17);
            this.label10.TabIndex = 62;
            this.label10.Text = "10) Next Appraisal             :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(29, 447);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(210, 17);
            this.label12.TabIndex = 61;
            this.label12.Text = "11) Remarks                     :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(496, 351);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(158, 17);
            this.label11.TabIndex = 59;
            this.label11.Text = "Previous Package   :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(29, 383);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(212, 17);
            this.label9.TabIndex = 58;
            this.label9.Text = "9) Last Appraisal               :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(29, 349);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(210, 17);
            this.label8.TabIndex = 57;
            this.label8.Text = "8) New Annual Package     :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(29, 317);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(212, 17);
            this.label7.TabIndex = 56;
            this.label7.Text = "7) Increment %                  :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(29, 282);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(213, 17);
            this.label6.TabIndex = 55;
            this.label6.Text = "6) Increment Amount          :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(29, 248);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(215, 17);
            this.label5.TabIndex = 54;
            this.label5.Text = "5) Ranking && Ranking Date :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 220);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 17);
            this.label4.TabIndex = 53;
            this.label4.Text = "4) Effective from               :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 191);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 17);
            this.label3.TabIndex = 49;
            this.label3.Text = "3) Increment Type             :";
            // 
            // dtPR_RANKING_DATE
            // 
            this.dtPR_RANKING_DATE.CustomEnabled = true;
            this.dtPR_RANKING_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtPR_RANKING_DATE.DataFieldMapping = "";
            this.dtPR_RANKING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPR_RANKING_DATE.HasChanges = true;
            this.dtPR_RANKING_DATE.Location = new System.Drawing.Point(275, 243);
            this.dtPR_RANKING_DATE.Margin = new System.Windows.Forms.Padding(4);
            this.dtPR_RANKING_DATE.Name = "dtPR_RANKING_DATE";
            this.dtPR_RANKING_DATE.NullValue = " ";
            this.dtPR_RANKING_DATE.Size = new System.Drawing.Size(132, 22);
            this.dtPR_RANKING_DATE.TabIndex = 46;
            this.dtPR_RANKING_DATE.Value = new System.DateTime(2011, 1, 14, 0, 0, 0, 0);
            // 
            // txtPR_RANK
            // 
            this.txtPR_RANK.AllowSpace = true;
            this.txtPR_RANK.AssociatedLookUpName = "";
            this.txtPR_RANK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_RANK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_RANK.ContinuationTextBox = null;
            this.txtPR_RANK.CustomEnabled = true;
            this.txtPR_RANK.DataFieldMapping = "PR_RANK";
            this.txtPR_RANK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_RANK.GetRecordsOnUpDownKeys = false;
            this.txtPR_RANK.IsDate = false;
            this.txtPR_RANK.Location = new System.Drawing.Point(416, 243);
            this.txtPR_RANK.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_RANK.MaxLength = 1;
            this.txtPR_RANK.Name = "txtPR_RANK";
            this.txtPR_RANK.NumberFormat = "###,###,##0.00";
            this.txtPR_RANK.Postfix = "";
            this.txtPR_RANK.Prefix = "";
            this.txtPR_RANK.Size = new System.Drawing.Size(34, 24);
            this.txtPR_RANK.SkipValidation = false;
            this.txtPR_RANK.TabIndex = 47;
            this.txtPR_RANK.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPR_REMARKS
            // 
            this.txtPR_REMARKS.AllowSpace = true;
            this.txtPR_REMARKS.AssociatedLookUpName = "";
            this.txtPR_REMARKS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_REMARKS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_REMARKS.ContinuationTextBox = null;
            this.txtPR_REMARKS.CustomEnabled = true;
            this.txtPR_REMARKS.DataFieldMapping = "";
            this.txtPR_REMARKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_REMARKS.GetRecordsOnUpDownKeys = false;
            this.txtPR_REMARKS.IsDate = false;
            this.txtPR_REMARKS.IsRequired = true;
            this.txtPR_REMARKS.Location = new System.Drawing.Point(275, 439);
            this.txtPR_REMARKS.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_REMARKS.MaxLength = 50;
            this.txtPR_REMARKS.Name = "txtPR_REMARKS";
            this.txtPR_REMARKS.NumberFormat = "###,###,##0.00";
            this.txtPR_REMARKS.Postfix = "";
            this.txtPR_REMARKS.Prefix = "";
            this.txtPR_REMARKS.Size = new System.Drawing.Size(530, 24);
            this.txtPR_REMARKS.SkipValidation = false;
            this.txtPR_REMARKS.TabIndex = 72;
            this.txtPR_REMARKS.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtIncrementPer
            // 
            this.txtIncrementPer.AllowSpace = true;
            this.txtIncrementPer.AssociatedLookUpName = "";
            this.txtIncrementPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncrementPer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncrementPer.ContinuationTextBox = null;
            this.txtIncrementPer.CustomEnabled = true;
            this.txtIncrementPer.DataFieldMapping = "";
            this.txtIncrementPer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncrementPer.GetRecordsOnUpDownKeys = false;
            this.txtIncrementPer.IsDate = false;
            this.txtIncrementPer.Location = new System.Drawing.Point(275, 311);
            this.txtIncrementPer.Margin = new System.Windows.Forms.Padding(4);
            this.txtIncrementPer.MaxLength = 7;
            this.txtIncrementPer.Name = "txtIncrementPer";
            this.txtIncrementPer.NumberFormat = "0.00";
            this.txtIncrementPer.Postfix = "";
            this.txtIncrementPer.Prefix = "";
            this.txtIncrementPer.Size = new System.Drawing.Size(133, 24);
            this.txtIncrementPer.SkipValidation = false;
            this.txtIncrementPer.TabIndex = 50;
            this.txtIncrementPer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncrementPer.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPR_ANNUAL_PRESENT
            // 
            this.txtPR_ANNUAL_PRESENT.AllowSpace = true;
            this.txtPR_ANNUAL_PRESENT.AssociatedLookUpName = "";
            this.txtPR_ANNUAL_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_ANNUAL_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_ANNUAL_PRESENT.ContinuationTextBox = null;
            this.txtPR_ANNUAL_PRESENT.CustomEnabled = true;
            this.txtPR_ANNUAL_PRESENT.DataFieldMapping = "";
            this.txtPR_ANNUAL_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_ANNUAL_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_ANNUAL_PRESENT.IsDate = false;
            this.txtPR_ANNUAL_PRESENT.Location = new System.Drawing.Point(275, 343);
            this.txtPR_ANNUAL_PRESENT.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_ANNUAL_PRESENT.MaxLength = 11;
            this.txtPR_ANNUAL_PRESENT.Name = "txtPR_ANNUAL_PRESENT";
            this.txtPR_ANNUAL_PRESENT.NumberFormat = "###,###,###";
            this.txtPR_ANNUAL_PRESENT.Postfix = "";
            this.txtPR_ANNUAL_PRESENT.Prefix = "";
            this.txtPR_ANNUAL_PRESENT.Size = new System.Drawing.Size(133, 24);
            this.txtPR_ANNUAL_PRESENT.SkipValidation = false;
            this.txtPR_ANNUAL_PRESENT.TabIndex = 51;
            this.txtPR_ANNUAL_PRESENT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPR_ANNUAL_PRESENT.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtIncAmonut
            // 
            this.txtIncAmonut.AllowSpace = true;
            this.txtIncAmonut.AssociatedLookUpName = "";
            this.txtIncAmonut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncAmonut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncAmonut.ContinuationTextBox = null;
            this.txtIncAmonut.CustomEnabled = true;
            this.txtIncAmonut.DataFieldMapping = "";
            this.txtIncAmonut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncAmonut.GetRecordsOnUpDownKeys = false;
            this.txtIncAmonut.IsDate = false;
            this.txtIncAmonut.IsRequired = true;
            this.txtIncAmonut.Location = new System.Drawing.Point(275, 279);
            this.txtIncAmonut.Margin = new System.Windows.Forms.Padding(4);
            this.txtIncAmonut.MaxLength = 7;
            this.txtIncAmonut.Name = "txtIncAmonut";
            this.txtIncAmonut.NumberFormat = "###,###,##0.00";
            this.txtIncAmonut.Postfix = "";
            this.txtIncAmonut.Prefix = "";
            this.txtIncAmonut.Size = new System.Drawing.Size(133, 24);
            this.txtIncAmonut.SkipValidation = false;
            this.txtIncAmonut.TabIndex = 48;
            this.txtIncAmonut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncAmonut.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtIncremnttype
            // 
            this.txtIncremnttype.AllowSpace = true;
            this.txtIncremnttype.AssociatedLookUpName = "lbtnIncType";
            this.txtIncremnttype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncremnttype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncremnttype.ContinuationTextBox = null;
            this.txtIncremnttype.CustomEnabled = true;
            this.txtIncremnttype.DataFieldMapping = "";
            this.txtIncremnttype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncremnttype.GetRecordsOnUpDownKeys = false;
            this.txtIncremnttype.IsDate = false;
            this.txtIncremnttype.IsRequired = true;
            this.txtIncremnttype.Location = new System.Drawing.Point(275, 183);
            this.txtIncremnttype.Margin = new System.Windows.Forms.Padding(4);
            this.txtIncremnttype.MaxLength = 1;
            this.txtIncremnttype.Name = "txtIncremnttype";
            this.txtIncremnttype.NumberFormat = "###,###,##0.00";
            this.txtIncremnttype.Postfix = "";
            this.txtIncremnttype.Prefix = "";
            this.txtIncremnttype.Size = new System.Drawing.Size(61, 24);
            this.txtIncremnttype.SkipValidation = false;
            this.txtIncremnttype.TabIndex = 44;
            this.txtIncremnttype.TextType = CrplControlLibrary.TextType.String;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(740, 118);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 17);
            this.label22.TabIndex = 83;
            this.label22.Text = "Level :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(588, 118);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 17);
            this.label21.TabIndex = 82;
            this.label21.Text = "Desig:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(432, 118);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 17);
            this.label20.TabIndex = 81;
            this.label20.Text = "ASR:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 149);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 17);
            this.label2.TabIndex = 80;
            this.label2.Text = "2)  Name                          :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 123);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 17);
            this.label1.TabIndex = 79;
            this.label1.Text = "1)  Personnel No.              :";
            // 
            // txtAnnualPresnt
            // 
            this.txtAnnualPresnt.AllowSpace = true;
            this.txtAnnualPresnt.AssociatedLookUpName = "";
            this.txtAnnualPresnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualPresnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualPresnt.ContinuationTextBox = null;
            this.txtAnnualPresnt.CustomEnabled = true;
            this.txtAnnualPresnt.DataFieldMapping = "";
            this.txtAnnualPresnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualPresnt.GetRecordsOnUpDownKeys = false;
            this.txtAnnualPresnt.IsDate = false;
            this.txtAnnualPresnt.Location = new System.Drawing.Point(488, 114);
            this.txtAnnualPresnt.Margin = new System.Windows.Forms.Padding(4);
            this.txtAnnualPresnt.MaxLength = 11;
            this.txtAnnualPresnt.Name = "txtAnnualPresnt";
            this.txtAnnualPresnt.NumberFormat = "###,###,##0.00";
            this.txtAnnualPresnt.Postfix = "";
            this.txtAnnualPresnt.Prefix = "";
            this.txtAnnualPresnt.ReadOnly = true;
            this.txtAnnualPresnt.Size = new System.Drawing.Size(86, 24);
            this.txtAnnualPresnt.SkipValidation = false;
            this.txtAnnualPresnt.TabIndex = 88;
            this.txtAnnualPresnt.TabStop = false;
            this.txtAnnualPresnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnnualPresnt.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPR_DESIG_PRESENT
            // 
            this.txtPR_DESIG_PRESENT.AllowSpace = true;
            this.txtPR_DESIG_PRESENT.AssociatedLookUpName = "";
            this.txtPR_DESIG_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESIG_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESIG_PRESENT.ContinuationTextBox = null;
            this.txtPR_DESIG_PRESENT.CustomEnabled = true;
            this.txtPR_DESIG_PRESENT.DataFieldMapping = "";
            this.txtPR_DESIG_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESIG_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESIG_PRESENT.IsDate = false;
            this.txtPR_DESIG_PRESENT.Location = new System.Drawing.Point(653, 114);
            this.txtPR_DESIG_PRESENT.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_DESIG_PRESENT.Name = "txtPR_DESIG_PRESENT";
            this.txtPR_DESIG_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_DESIG_PRESENT.Postfix = "";
            this.txtPR_DESIG_PRESENT.Prefix = "";
            this.txtPR_DESIG_PRESENT.ReadOnly = true;
            this.txtPR_DESIG_PRESENT.Size = new System.Drawing.Size(74, 24);
            this.txtPR_DESIG_PRESENT.SkipValidation = false;
            this.txtPR_DESIG_PRESENT.TabIndex = 87;
            this.txtPR_DESIG_PRESENT.TabStop = false;
            this.txtPR_DESIG_PRESENT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(809, 114);
            this.txtLevel.Margin = new System.Windows.Forms.Padding(4);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(31, 24);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 85;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(276, 146);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(378, 24);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 86;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnel
            // 
            this.txtPersonnel.AllowSpace = true;
            this.txtPersonnel.AssociatedLookUpName = "";
            this.txtPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnel.ContinuationTextBox = null;
            this.txtPersonnel.CustomEnabled = true;
            this.txtPersonnel.DataFieldMapping = "";
            this.txtPersonnel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnel.GetRecordsOnUpDownKeys = false;
            this.txtPersonnel.IsDate = false;
            this.txtPersonnel.IsRequired = true;
            this.txtPersonnel.Location = new System.Drawing.Point(276, 114);
            this.txtPersonnel.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersonnel.MaxLength = 6;
            this.txtPersonnel.Name = "txtPersonnel";
            this.txtPersonnel.NumberFormat = "###,###,##0.00";
            this.txtPersonnel.Postfix = "";
            this.txtPersonnel.Prefix = "";
            this.txtPersonnel.Size = new System.Drawing.Size(109, 24);
            this.txtPersonnel.SkipValidation = false;
            this.txtPersonnel.TabIndex = 78;
            this.txtPersonnel.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(257, 73);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(313, 25);
            this.label19.TabIndex = 91;
            this.label19.Text = "(OFFICERS     STAFF)";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(231, 44);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(364, 22);
            this.label13.TabIndex = 90;
            this.label13.Text = "INCREMENTS / PROMOTIONS";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(257, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(313, 25);
            this.label14.TabIndex = 89;
            this.label14.Text = "PERSONNEL SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWP_pre
            // 
            this.txtWP_pre.AllowSpace = true;
            this.txtWP_pre.AssociatedLookUpName = "";
            this.txtWP_pre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWP_pre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWP_pre.ContinuationTextBox = null;
            this.txtWP_pre.CustomEnabled = true;
            this.txtWP_pre.DataFieldMapping = "";
            this.txtWP_pre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWP_pre.GetRecordsOnUpDownKeys = false;
            this.txtWP_pre.IsDate = false;
            this.txtWP_pre.Location = new System.Drawing.Point(672, 347);
            this.txtWP_pre.Margin = new System.Windows.Forms.Padding(4);
            this.txtWP_pre.MaxLength = 11;
            this.txtWP_pre.Name = "txtWP_pre";
            this.txtWP_pre.NumberFormat = "###,###,###";
            this.txtWP_pre.Postfix = "";
            this.txtWP_pre.Prefix = "";
            this.txtWP_pre.ReadOnly = true;
            this.txtWP_pre.Size = new System.Drawing.Size(133, 24);
            this.txtWP_pre.SkipValidation = false;
            this.txtWP_pre.TabIndex = 92;
            this.txtWP_pre.TabStop = false;
            this.txtWP_pre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWP_pre.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // btnAproved
            // 
            this.btnAproved.Location = new System.Drawing.Point(708, 487);
            this.btnAproved.Name = "btnAproved";
            this.btnAproved.Size = new System.Drawing.Size(97, 40);
            this.btnAproved.TabIndex = 100;
            this.btnAproved.Text = "Approve";
            this.btnAproved.UseVisualStyleBackColor = true;
            this.btnAproved.Click += new System.EventHandler(this.btnAproved_Click);
            // 
            // btnReject
            // 
            this.btnReject.Location = new System.Drawing.Point(592, 489);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(97, 40);
            this.btnReject.TabIndex = 99;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(489, 487);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(97, 40);
            this.btnClose.TabIndex = 98;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CHRIS_Personnel_IncrementOfficers_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 551);
            this.Controls.Add(this.btnAproved);
            this.Controls.Add(this.btnReject);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtWP_pre);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAnnualPresnt);
            this.Controls.Add(this.txtPR_DESIG_PRESENT);
            this.Controls.Add(this.txtLevel);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtPersonnel);
            this.Controls.Add(this.txt_PR_EFFECTIVE);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.dtLastApp);
            this.Controls.Add(this.dtNextApp);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtPR_RANKING_DATE);
            this.Controls.Add(this.txtPR_RANK);
            this.Controls.Add(this.txtPR_REMARKS);
            this.Controls.Add(this.txtIncrementPer);
            this.Controls.Add(this.txtPR_ANNUAL_PRESENT);
            this.Controls.Add(this.txtIncAmonut);
            this.Controls.Add(this.txtIncremnttype);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_IncrementOfficers_Detail";
            this.Text = "CHRIS_Personnel_IncrementOfficers_Detail";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_IncrementOfficers_Detail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txt_PR_EFFECTIVE;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLDatePicker dtLastApp;
        private CrplControlLibrary.SLDatePicker dtNextApp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLDatePicker dtPR_RANKING_DATE;
        private CrplControlLibrary.SLTextBox txtPR_RANK;
        private CrplControlLibrary.SLTextBox txtPR_REMARKS;
        private CrplControlLibrary.SLTextBox txtIncrementPer;
        private CrplControlLibrary.SLTextBox txtPR_ANNUAL_PRESENT;
        private CrplControlLibrary.SLTextBox txtIncAmonut;
        private CrplControlLibrary.SLTextBox txtIncremnttype;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtAnnualPresnt;
        private CrplControlLibrary.SLTextBox txtPR_DESIG_PRESENT;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtPersonnel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtWP_pre;
        private System.Windows.Forms.Button btnAproved;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnClose;
    }
}