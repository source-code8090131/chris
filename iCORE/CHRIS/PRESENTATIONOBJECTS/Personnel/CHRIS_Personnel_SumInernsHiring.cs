using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_SumInernsHiring : ChrisMasterDetailForm
    {
        #region Variable
        int CountributionCount = 0;
        string w_cont = null;
        string AUDIT_PATH = "G:\\SPOOL\\";
        string PATH_REPORT = "G:\\";
        bool validateFrom = true;
        #endregion

        #region Constructor

        public CHRIS_Personnel_SumInernsHiring()
        {
            InitializeComponent();
        }
        public CHRIS_Personnel_SumInernsHiring(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlDept);
            pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);
            this.CurrentPanelBlock = pnlDetail.Name;
            this.tbtDelete.Available = false;
            this.tbtSave.Available = false;
            this.txtOption.Enabled = true;
            this.txtOption.Select();
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            tbtList.Visible = false;
            this.txtUser.Text = this.userID;
            this.slTxtBxUserID.Text = this.userID;
            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.CurrrentOptionTextBox = this.txtCurrOption;
            dgvDept.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF2 = false;
            this.txtUserName.Text = "User Name:   " + this.UserName;
            this.lbtnPNo.Visible = true;
            this.lbtnBranch.Visible = true;
            this.lkpBtnAuthorize.Visible = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
        }

        public void AddModifyFunc()
        {
            bool Validate = true;
            string CurrOption = txtOption.Text;
            dgvDept.EndEdit();

            if (txtOption.Text == "A" || txtOption.Text == "M")
            {
                CountributionCount = 0;

                foreach (DataGridViewRow row in dgvDept.Rows)
                {
                    if (row.Cells["colContribution"].Value != null && row.Cells["colContribution"].Value.ToString() != "")
                    {
                        CountributionCount = CountributionCount + Convert.ToInt32(row.Cells["colContribution"].Value.ToString());
                    }
                }
                w_cont = CountributionCount.ToString();
                if (w_cont != "100")
                {
                    MessageBox.Show("Total Contribution Should be 100 ....!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Validate = false;
                    dgvDept.Rows[0].Cells[0].DataGridView.Focus();
                    dgvDept.Rows[0].Cells[0].DataGridView.Select();
                }
                else if (w_cont == null)
                {
                    MessageBox.Show("Please Enter Department In Which Employee Is Hired", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Validate = false;
                }
                else
                {
                    if ((txtOption.Text == "A" || txtOption.Text == "M") && Validate)
                    {
                        DialogResult dr = MessageBox.Show("Do You Want To Save The Record [Y]es [N]o", "From", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                        if (dr == DialogResult.Yes)
                        {
                            CallReportOLD();
                            base.Save();
                            if (validateFrom)
                            {
                                CallReportNEW();
                                base.Cancel();
                                this.txtUser.Text = this.userID;
                                this.slTxtBxUserID.Text = this.userID;
                                this.txtLocation.Text = this.CurrentLocation;
                                this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
                                this.CurrrentOptionTextBox = this.txtCurrOption;
                                txtOption.Text = CurrOption;
                                txtOption.Select();
                                txtOption.Focus();
                            }
                            else
                            {
                                validateFrom = true;
                                return;
                            }
                        }
                        else
                        {
                            base.Cancel();
                            this.txtUser.Text = this.userID;
                            this.slTxtBxUserID.Text = this.userID;
                            this.txtLocation.Text = this.CurrentLocation;
                            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
                            txtOption.Select();
                            txtOption.Focus();
                        }
                    }
                }
            }
        }


        /// <summary>
        /// First Test the Validation
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            AddModifyFunc();
                            this.KeyPreview = true;
                            break;
                    }

                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
        }

        /// <summary>
        /// Report before Commiting Record
        /// </summary>
        private void CallReportOLD()
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            string FN1 = "AUPR" + (DateTime.Now.ToString("yyyyMMddhhmiss")) + ".LIS";

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = frm.m_ReportPath + FN1;
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "INO";
            txtPNO.Text = txtPersNo.Text;
            //txtPNO.Text = g_Pno.ToString();
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "ST";
            txtST.Text = "OLD";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString("dd/MM/yyyy");
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit10A";
            frm.Owner = this;
            frm.ExportCustomReportToTXT(frm.m_ReportPath + FN1, "txt");
            //frm.RunReport();
        }

        /// <summary>
        /// Report After Commiting Record
        /// </summary>
        private void CallReportNEW()
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            string FN1 = "AUPO" + (DateTime.Now.ToString("yyyyMMddhhmiss")) + ".LIS";

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = frm.m_ReportPath + FN1;
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "INO";
            txtPNO.Text = txtPersNo.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox sltxtBxuser = new CrplControlLibrary.SLTextBox(comp);
            sltxtBxuser.Name = "slTxtBxUserID";
            sltxtBxuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(sltxtBxuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "ST";
            txtST.Text = "NEW";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString("dd/MM/yyyy");
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit10A";
            frm.Owner = this;
            frm.ExportCustomReportToTXT(frm.m_ReportPath + FN1, "txt");
            //frm.RunReport();
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                if (IsValidated() && this.FindForm().Validate())
                {
                    base.DoToolbarActions(ctrlsCollection, actionType);
                }
                else
                {
                    validateFrom = false;
                }
            }
            else
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
            }
        }

        private void ResetForm()
        {
            this.txtPersNo.Text = "";
            this.txtFirstName.Text = "";
            this.txtLastName.Text = "";
            this.txtBranch.Text = "";

            this.txtUni.Text = "";
            this.txtClass.Text = "";
            //this.dtpFromDate.Value  = null;
            //this.dtpToDate.Value    = null;

            this.txtAddress.Text = "";
            this.txtPh1.Text = "";
            this.txtPh2.Text = "";
            //this.dtpDOB.Value       = null;
            this.txtMarital.Text = "";
            this.txtSex.Text = "";
            this.txtNID.Text = "";

            this.txtPersNo.IsRequired = false;
            this.txtFirstName.IsRequired = false;
            this.txtBranch.IsRequired = false;

            this.txtUni.IsRequired = false;
            this.txtClass.IsRequired = false;
            this.dtpFromDate.IsRequired = false;
            this.dtpToDate.IsRequired = false;

            this.dtpDOB.IsRequired = false;
            this.txtMarital.IsRequired = false;
            this.txtSex.IsRequired = false;
            this.txtNID.IsRequired = false;

            this.Cancel();

            this.txtUser.Text = this.userID;
            this.slTxtBxUserID.Text = this.userID;
            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.CurrrentOptionTextBox = this.txtCurrOption;

            this.txtPersNo.IsRequired = true;
            this.txtFirstName.IsRequired = true;
            this.txtBranch.IsRequired = true;

            //this.txtUni.IsRequired    = true;
            //this.txtClass.IsRequired  = true;
            this.dtpFromDate.IsRequired = true;
            this.dtpToDate.IsRequired = true;
            this.dtpDOB.IsRequired = true;
            this.txtMarital.IsRequired = true;
            this.txtSex.IsRequired = true;


            //DataTable dt = (DataTable)this.dgvDept.DataSource;
            //if (dt != null)
            //{
            //    dt.Rows.Clear();
            //}
        }
        private bool IsValidated()
        {
            bool validated = true;

            if (this.txtPersNo.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
            }
            if (this.txtFirstName.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            }
            if (this.txtBranch.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
            }
            if (this.txtUni.Text == "")
            {
                //validated = false;
                //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
            }
            if (this.txtClass.Text == "")
            {
                //validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
            }
            if (this.dtpFromDate.Value == null)
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            }
            if (this.dtpToDate.Value == null)
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            }
            if (this.dtpDOB.Value == null)
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            }
            if (this.txtMarital.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
            }
            if (this.txtSex.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
            }
            if (this.txtNID.Text == "")
            {
                //validated = false;
                //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
            }

            return validated;
        }

        protected override bool Add()
        {
            this.ResetForm();
            this.lbtnPNo.ActionType = "PR_P_NO_LOV";
            this.lbtnPNo.ActionLOVExists = "PR_P_NO_LOV_EXISTS";
            this.lbtnPNo.SkipValidationOnLeave = true;
            this.lkpBtnAuthorize.SkipValidationOnLeave = true;
            base.Add();
            this.txtOption.Text = "A";
            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "SER_NO");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.txtPersNo.Text = rsltCode.dstResult.Tables[0].Rows[0][0] == null ? "" : rsltCode.dstResult.Tables[0].Rows[0][0].ToString();
                }
            }

            this.txtBranch.Select();
            this.txtBranch.Focus();

            this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF1;

            return false;
        }
        protected override bool Edit()
        {
            this.ResetForm();
            this.lbtnPNo.ActionType = "List";
            this.lkpBtnAuthorize.ActionType = "_SumInernsHiring";

            base.Edit();

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lkpBtnAuthorize.SkipValidationOnLeave = false;
            this.txtPersNo.Select();
            this.txtPersNo.Focus();
            this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF7;

            return false;

        }
        protected override bool View()
        {
            this.ResetForm(); ;
            this.lbtnPNo.ActionType = "List";
            this.lkpBtnAuthorize.ActionType = "_SumInernsHiring";
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lkpBtnAuthorize.SkipValidationOnLeave = false;

            base.View();

            this.txtOption.Text = "V";
            this.txtPersNo.Select();
            this.txtPersNo.Focus();
            this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF3;

            return false;
        }
        protected override bool Delete()
        {
            //this.Cancel();
            this.ResetForm();
            this.lbtnPNo.ActionType = "List";

            base.Delete();

            this.txtPersNo.Select();
            this.txtPersNo.Select();
            this.txtPersNo.Focus();

            this.FunctionConfig.CurrentOption = Function.Delete;
            this.FunctionConfig.OptionLetterF10 = this.FunctionConfig.OptionLetterF4;

            return false;
        }
        protected override bool Save()
        {
            if (!this.IsValidated())
                return false;

            bool flag = false;
            if (this.dgvDept.Rows.Count > 1)
            {
                if (!this.IsValidated())
                    return false;

                this.dgvDept.EndEdit();
                double totalCon;
                double.TryParse((dgvDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);

                if (totalCon == 100)
                {
                    flag = base.Save();

                    if (validateFrom)
                    {
                        this.ResetForm();
                        this.txtOption.Select();
                    }
                }
                else
                {
                    MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                this.FunctionConfig.CurrentOption = this.FunctionConfig.CurrentOption;
                this.ActiveControl.Select();
                this.ActiveControl.Focus();
            }

            return flag;
        }

        #endregion

        #region Event Handlers

        private void dgvDept_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //if (!this.dgvDept.ContainsFocus)
            //    return;

            try
            {
                if (dgvDept.CurrentRow.Index < dgvDept.Rows.Count)
                {
                    if (dgvDept.CurrentCell.OwningColumn.Name == "colSeg" && (dgvDept.CurrentCell.EditedFormattedValue.ToString() == ""))
                    {
                        MessageBox.Show("Enter 'GF' or 'GCB'", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else if (dgvDept.CurrentCell.OwningColumn.Name == "colDept" && (dgvDept.CurrentCell.EditedFormattedValue.ToString() == ""))
                    {
                        MessageBox.Show("Invalid Department Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else if (dgvDept.CurrentCell.OwningColumn.Name == "colContribution" && (dgvDept.CurrentCell.EditedFormattedValue.ToString() == "" || dgvDept.CurrentCell.EditedFormattedValue.ToString() == "0"))
                    {
                        e.Cancel = true;
                        return;
                    }

                }


                if (e.ColumnIndex == 0 && dgvDept.CurrentCell.IsInEditMode)
                {

                    string txt = dgvDept.CurrentCell.EditedFormattedValue == null ? null : dgvDept.CurrentCell.EditedFormattedValue.ToString();
                    if (txt.ToUpper() == "GF" || txt.ToUpper() == "GCB")
                    {
                        dgvDept.CurrentCell.Value = txt.ToUpper();
                    }
                    else
                    {
                        MessageBox.Show("Enter 'GF' or 'GCB'", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }

                }

                if (dgvDept.CurrentCell.OwningColumn.Name == "colDept")
                {
                    string txt1 = dgvDept.CurrentCell.EditedFormattedValue == null ? null : dgvDept.CurrentCell.EditedFormattedValue.ToString();
                    string txt2 = dgvDept.CurrentRow.Cells["colSeg"].Value.ToString();
                    Dictionary<string, object> paramDept = new Dictionary<string, object>();
                    paramDept.Add("PR_SEGMENT", txt2);
                    paramDept.Add("PR_DEPT", txt1);


                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER", "DEPT_VALID", paramDept);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {

                    }
                    else
                    {
                        MessageBox.Show("Invalid Department Entered Press [F9] Key For Help", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }

                }
                else if (dgvDept.CurrentCell.OwningColumn.Name == "colContribution")
                {
                    if (dgvDept.CurrentCell.IsInEditMode)
                    {
                        if (dgvDept.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                        {
                            bool entrValue = true;
                            Double dContribution = 0;
                            Double PreviousValue = 0;
                            if (dgvDept[e.ColumnIndex, e.RowIndex].Value != null)
                                double.TryParse(dgvDept[e.ColumnIndex, e.RowIndex].Value.ToString(), out PreviousValue);

                            Double.TryParse(dgvDept[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString(), out dContribution);
                            dContribution = Math.Round(dContribution, 0, MidpointRounding.AwayFromZero);

                            double totalCon = 0;


                            int Contr = 0;//Convert.ToInt32(dgvDept.CurrentCell.EditedFormattedValue.ToString());

                            if (dgvDept[e.ColumnIndex, e.RowIndex].Value != null)
                                entrValue = int.TryParse(dgvDept[e.ColumnIndex, e.RowIndex].Value.ToString(), out Contr);


                            if (Contr == 0)
                            {
                                e.Cancel = true;
                            }
                            if (Contr > 100)
                            {
                                MessageBox.Show("If The Contribution Is Equal To 100 Then Press [ESC ESC F2] To Save The Record", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                e.Cancel = true;
                            }

                            if (dgvDept.DataSource as DataTable != null)
                                double.TryParse((dgvDept.DataSource as DataTable).Compute("SUM(PR_CONTRIB)", "").ToString(), out totalCon);
                            totalCon = totalCon + dContribution - PreviousValue;

                            if (totalCon < 100)
                            {
                                dgvDept.CurrentCell.Value = dContribution;
                            }
                            if (totalCon == 100)
                            {
                                dgvDept.CurrentCell.Value = dContribution;
                            }
                            if (totalCon > 100)
                            {
                                MessageBox.Show("YOUR CONTRIBUTION % SUM FOR THE CURRENT ID IS EXCEEDING 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                totalCon = totalCon - dContribution;
                                e.Cancel = true;
                            }


                            if (dgvDept.CurrentRow.Index == (dgvDept.Rows.Count - 2))
                            {
                                if (totalCon == 100)
                                {
                                    dgvDept.CurrentCell.Value = dContribution;
                                    AddModifyFunc();
                                    return;
                                }
                            }
                            totalCon = 0;
                        }
                    }
                }
                dgvDept.EndEdit();
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvDept_CellValidating", exp);
            }

        }
        private void CHRIS_Personnel_ContractStafHiringEnt_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            //if (actionType != "List")
            //    return;

            if (actionType == "BRANCH_LOV" || actionType == "BRANCH_LOV_EXISTS")
                return;

            switch (this.FunctionConfig.CurrentOption)
            {
                case Function.Delete:
                    //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));


                    DialogResult dr = MessageBox.Show("Do You Want To Delete The Record [Y]es [N]o", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    if (dr == DialogResult.Yes)
                    {
                        CallReportOLD();
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", txtPersNo.Text);
                        Result rsltCode1;
                        CmnDataManager cmnDM1 = new CmnDataManager();
                        rsltCode1 = cmnDM1.Execute("CHRIS_SP_SUMMER_INTERNS_MANAGER", "Delete", param);

                        CallReportNEW();
                        this.ResetForm();
                        txtOption.Text = "D";
                        txtCurrOption.Text = "Delete";
                        txtPersNo.Select();
                        txtPersNo.Focus();
                    }
                    else
                    {
                        this.ResetForm();
                    }
                    break;

                case Function.Modify:
                    break;

                case Function.View:
                    break;
            }
        }
        private void txtPersNo_Enter(object sender, EventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                this.txtBranch.Select();
                this.txtBranch.Focus();
            }
        }
        private void txtPh2_Validating(object sender, CancelEventArgs e)
        {
            if (txtPh2.Text != string.Empty)
            {
                if (txtPh1.Text == txtPh2.Text)
                {

                    MessageBox.Show("Both Telephone No. Cannot Be Same");
                    txtPh2.Focus();
                    return;
                }

            }
        }
        private void txtSex_Validating(object sender, CancelEventArgs e)
        {
            if (txtSex.Text != string.Empty)
            {
                if (txtSex.Text != "M" && txtSex.Text != "F")
                {
                    MessageBox.Show("Press [M]ale or [F]emale");
                    txtSex.Focus();
                    txtSex.Text = "";
                    return;
                }
            }
            else
            {
                MessageBox.Show("Press [M]ale or [F]emale");
                txtSex.Focus();
                txtSex.Text = "";
                return;
            }
        }
        private void txtMarital_Validating(object sender, CancelEventArgs e)
        {
            if (txtMarital.Text != string.Empty)
            {
                if (txtMarital.Text != "M" && txtMarital.Text != "S")
                {
                    MessageBox.Show("Please [M]arried or [S]ingle");
                    txtMarital.Focus();
                    txtMarital.Text = "";
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please [M]arried or [S]ingle");
                txtMarital.Focus();
                txtMarital.Text = "";
                return;
            }
        }
        private void txtNID_Validating(object sender, CancelEventArgs e)
        {
            bool ValidNIC = true;
            if (txtNID.Text != string.Empty)
            {
                ValidNIC = IsValidExpression(txtNID.Text, InputValidator.OldNICNO_REGEX);

                if (!ValidNIC)
                {
                    MessageBox.Show("Field must be of the from 999'-'99'-'999999", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = false;
                }
            }
        }
        private void dtpDOB_Validating(object sender, CancelEventArgs e)
        {
            if (dtpDOB.Value != null)
            {
                DateTime DateDob = Convert.ToDateTime(dtpDOB.Value);
                DateTime DateFrom = Convert.ToDateTime(dtpFromDate.Value);
                int MonthsBetween = base.MonthsBetweenInOracle(Convert.ToDateTime(dtpFromDate.Value), Convert.ToDateTime(dtpDOB.Value));
                if (DateTime.Compare(Convert.ToDateTime(dtpDOB.Value), Convert.ToDateTime(dtpFromDate.Value)) > 0)
                {
                    MessageBox.Show("Birth Date can not be Greater Then From Date");
                    dtpDOB.Focus();
                    e.Cancel = true;
                    return;
                }
                else if (MonthsBetween <= 120)
                {
                    MessageBox.Show("Age Should be 10 years Less then From Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

        }
        private void lbtn_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                Button btn = (Button)sender;
                if (btn != null)
                {
                    btn.Enabled = false;
                    btn.Enabled = true;
                }
            }
        }
        private void dtpToDate_Validating(object sender, CancelEventArgs e)
        {
            DateTime DateTo = Convert.ToDateTime(dtpToDate.Value);
            DateTime DateFrom = Convert.ToDateTime(dtpFromDate.Value);

            if (DateTime.Compare(DateFrom, DateTo) >= 0)
            {
                MessageBox.Show("To Date Must Be Greater Than From Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }
        private void dgvDept_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }
        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtOption.Text == "V")
            {
                DialogResult dr = MessageBox.Show("Do You Want To View More Record [Y]es [N]o", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    base.Cancel();
                    this.txtUser.Text = this.userID;
                    this.slTxtBxUserID.Text = this.userID;
                    this.txtLocation.Text = this.CurrentLocation;
                    this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
                    this.CurrrentOptionTextBox = this.txtCurrOption;
                    this.txtOption.Text = "V";
                    this.txtPersNo.Select();
                    this.txtPersNo.Focus();
                    this.View();
                    base.IterateFormToEnableControls(this.Controls, true);
                }
                else
                {
                    base.Cancel();
                    this.txtUser.Text = this.userID;
                    this.slTxtBxUserID.Text = this.userID;
                    this.txtLocation.Text = this.CurrentLocation;
                    this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
                    this.txtOption.Text = "V";
                    this.txtOption.Select();
                    this.txtOption.Focus();

                }
            }
        }

        private void dgvDept_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!this.dgvDept.ContainsFocus)
                return;
            dgvDept.BeginEdit(true);
        }


        #endregion

        public static string SetUserValue = "";



        private void btnAuthorize_Click(object sender, EventArgs e)
        {
            //Edit();
            //this.slTextBox2.Text = "457";
            //this.lbtnPNo.ActionType = "List";
            //base.BindLOVShortCut("CHRIS_get_FrmCall_Users", this.slTextBox2);

            //SetUserValue = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            //CHRIS_Personnel_MakerChecker frm = new CHRIS_Personnel_MakerChecker();
            //frm.ShowDialog();

            //Edit();
            //DataRow isDataRow = CHRIS_Personnel_MakerChecker.SetdtRow;
           
            //{
            //    this.txtPersNo.Text = string.IsNullOrEmpty(isDataRow["PR_I_NO"].ToString().Trim()) ? "" : isDataRow["PR_I_NO"].ToString().Trim();
            //    this.txtFirstName.Text = string.IsNullOrEmpty(isDataRow["PR_FIRST_NAME"].ToString().Trim()) ? "" : isDataRow["PR_FIRST_NAME"].ToString().Trim();
            //    this.txtLastName.Text = string.IsNullOrEmpty(isDataRow["PR_LAST_NAME"].ToString().Trim()) ? "" : isDataRow["PR_LAST_NAME"].ToString().Trim();
            //    this.txtBranch.Text = string.IsNullOrEmpty(isDataRow["PR_BRANCH"].ToString().Trim()) ? "" : isDataRow["PR_BRANCH"].ToString().Trim();
            //    this.txtUni.Text = string.IsNullOrEmpty(isDataRow["PR_UNIVERSITY"].ToString().Trim()) ? "" : isDataRow["PR_UNIVERSITY"].ToString().Trim();
            //    this.txtClass.Text = string.IsNullOrEmpty(isDataRow["PR_CLASS"].ToString().Trim()) ? "" : isDataRow["PR_CLASS"].ToString().Trim();
            //    this.dtpFromDate.Value  = Convert.ToDateTime(string.IsNullOrEmpty(isDataRow["PR_CON_FROM"].ToString().Trim()) ? "" : isDataRow["PR_CON_FROM"].ToString().Trim());
            //    this.dtpToDate.Value    = Convert.ToDateTime(string.IsNullOrEmpty(isDataRow["PR_CON_TO"].ToString().Trim()) ? "" : isDataRow["PR_CON_TO"].ToString().Trim());

            //    this.txtAddress.Text = string.IsNullOrEmpty(isDataRow["PR_ADDRESS"].ToString().Trim()) ? "" : isDataRow["PR_ADDRESS"].ToString().Trim();
            //    this.txtPh1.Text = string.IsNullOrEmpty(isDataRow["PR_PHONE1"].ToString().Trim()) ? "" : isDataRow["PR_PHONE1"].ToString().Trim();
            //    this.txtPh2.Text = string.IsNullOrEmpty(isDataRow["PR_PHONE2"].ToString().Trim()) ? "" : isDataRow["PR_PHONE2"].ToString().Trim();
            //    this.dtpDOB.Value       = Convert.ToDateTime(string.IsNullOrEmpty(isDataRow["PR_DATE_BIRTH"].ToString().Trim()) ? "" : isDataRow["PR_DATE_BIRTH"].ToString().Trim());
            //    this.txtMarital.Text = string.IsNullOrEmpty(isDataRow["PR_MAR_STATUS"].ToString().Trim()) ? "" : isDataRow["PR_MAR_STATUS"].ToString().Trim();
            //    this.txtSex.Text = string.IsNullOrEmpty(isDataRow["PR_SEX"].ToString().Trim()) ? "" : isDataRow["PR_SEX"].ToString().Trim();
            //    this.txtNID.Text = string.IsNullOrEmpty(isDataRow["PR_NID_CARD"].ToString().Trim()) ? "" : isDataRow["PR_NID_CARD"].ToString().Trim();
            //    this.txtPrevIntern.Text = string.IsNullOrEmpty(isDataRow["PR_INTERNSHIP"].ToString().Trim()) ? "" : isDataRow["PR_INTERNSHIP"].ToString().Trim();
            //    this.txtStipend.Text = string.IsNullOrEmpty(isDataRow["PR_STIPENED"].ToString().Trim()) ? "" : isDataRow["PR_STIPENED"].ToString().Trim();
            //    this.slTextBox2.Text = string.IsNullOrEmpty(isDataRow["PR_RECOM"].ToString().Trim()) ? "" : isDataRow["PR_RECOM"].ToString().Trim();
            //}

        }

        //private void lkpBtnAuthorize_Click(object sender, EventArgs e)
        //{
        //   //base.BindLOVShortCut("CHRIS_SP_FrmCall_Users", this.slTextBox2);
        //}
       
    }
}