namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_IncremPromotCleric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_IncremPromotCleric));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtpersonal = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txttextMode = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.viewoption = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_PROMOTED = new CrplControlLibrary.SLTextBox(this.components);
            this.txtprCurrentPrevious = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESIG_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtwBasic = new CrplControlLibrary.SLTextBox(this.components);
            this.txtprBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtWNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_2_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_1_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_LEVEL_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnualPrevious = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_1_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESIG_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_LEVEL_PREVIOUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_FUNCT_2_PRESENT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnsW5 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtjoinDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtnEffectiveDate = new CrplControlLibrary.LookupButton(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.txtPR_REMARKS = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpNextAppraisalDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtWP_PRE = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpPR_LAST_APP = new CrplControlLibrary.SLDatePicker(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtPRAnnualPresent = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtStepAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.txtPR_INC_AMT = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRank = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.lbtnIncrementType = new CrplControlLibrary.LookupButton(this.components);
            this.txtType = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtWP_LEV = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txtWP_DES = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txtASR = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txtNoOfMonths = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.txtNoINCR = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpToDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpEffectiveDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUserName = new System.Windows.Forms.Label();
            this.PnlDetails = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVCourse = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.CTT_YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_INSTITUTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_GRADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_IN_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_PNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_EFFECTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCourseDtl = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvCourseDtl = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPR_IN_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPR_EFFECTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCTT_YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCTT_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCTT_INSTITUTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCTT_GRADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.PnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVCourse)).BeginInit();
            this.pnlCourseDtl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourseDtl)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(584, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(620, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(620, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtpersonal);
            this.pnlHead.Controls.Add(this.label7);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Location = new System.Drawing.Point(12, 94);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(603, 83);
            this.pnlHead.TabIndex = 10;
            // 
            // txtpersonal
            // 
            this.txtpersonal.AllowSpace = true;
            this.txtpersonal.AssociatedLookUpName = "";
            this.txtpersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpersonal.ContinuationTextBox = null;
            this.txtpersonal.CustomEnabled = true;
            this.txtpersonal.DataFieldMapping = "";
            this.txtpersonal.Enabled = false;
            this.txtpersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpersonal.GetRecordsOnUpDownKeys = false;
            this.txtpersonal.IsDate = false;
            this.txtpersonal.Location = new System.Drawing.Point(173, 55);
            this.txtpersonal.MaxLength = 10;
            this.txtpersonal.Name = "txtpersonal";
            this.txtpersonal.NumberFormat = "###,###,##0.00";
            this.txtpersonal.Postfix = "";
            this.txtpersonal.Prefix = "";
            this.txtpersonal.Size = new System.Drawing.Size(52, 20);
            this.txtpersonal.SkipValidation = false;
            this.txtpersonal.TabIndex = 125;
            this.txtpersonal.TabStop = false;
            this.txtpersonal.TextType = CrplControlLibrary.TextType.String;
            this.txtpersonal.Visible = false;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(136, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(347, 18);
            this.label7.TabIndex = 21;
            this.label7.Text = "(CLERCAL STAFF)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(517, 32);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(517, 6);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(471, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(457, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "System:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(75, 32);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(75, 6);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(136, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(347, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "PERSONNEL SYSTEM";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(136, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(347, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "INCREMENTS / PROMOTIONS";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txttextMode
            // 
            this.txttextMode.AllowSpace = true;
            this.txttextMode.AssociatedLookUpName = "";
            this.txttextMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttextMode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttextMode.ContinuationTextBox = null;
            this.txttextMode.CustomEnabled = true;
            this.txttextMode.DataFieldMapping = "";
            this.txttextMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttextMode.GetRecordsOnUpDownKeys = false;
            this.txttextMode.IsDate = false;
            this.txttextMode.Location = new System.Drawing.Point(216, 68);
            this.txttextMode.MaxLength = 3;
            this.txttextMode.Name = "txttextMode";
            this.txttextMode.NumberFormat = "###,###,##0.00";
            this.txttextMode.Postfix = "";
            this.txttextMode.Prefix = "";
            this.txttextMode.Size = new System.Drawing.Size(61, 20);
            this.txttextMode.SkipValidation = false;
            this.txttextMode.TabIndex = 124;
            this.txttextMode.TextType = CrplControlLibrary.TextType.String;
            this.txttextMode.Visible = false;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.viewoption);
            this.pnlDetail.Controls.Add(this.txtPR_PROMOTED);
            this.pnlDetail.Controls.Add(this.txtprCurrentPrevious);
            this.pnlDetail.Controls.Add(this.txtPR_DESIG_PRESENT);
            this.pnlDetail.Controls.Add(this.txtwBasic);
            this.pnlDetail.Controls.Add(this.txtprBranch);
            this.pnlDetail.Controls.Add(this.txtWNo);
            this.pnlDetail.Controls.Add(this.txtPR_FUNCT_2_PREVIOUS);
            this.pnlDetail.Controls.Add(this.txtPR_FUNCT_1_PREVIOUS);
            this.pnlDetail.Controls.Add(this.txtPR_LEVEL_PRESENT);
            this.pnlDetail.Controls.Add(this.txtAnnualPrevious);
            this.pnlDetail.Controls.Add(this.txtPR_FUNCT_1_PRESENT);
            this.pnlDetail.Controls.Add(this.txtPR_DESIG_PREVIOUS);
            this.pnlDetail.Controls.Add(this.txtPR_LEVEL_PREVIOUS);
            this.pnlDetail.Controls.Add(this.txtPR_FUNCT_2_PRESENT);
            this.pnlDetail.Controls.Add(this.txtAnsW5);
            this.pnlDetail.Controls.Add(this.dtjoinDate);
            this.pnlDetail.Controls.Add(this.lbtnEffectiveDate);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.txtPR_REMARKS);
            this.pnlDetail.Controls.Add(this.dtpNextAppraisalDate);
            this.pnlDetail.Controls.Add(this.label26);
            this.pnlDetail.Controls.Add(this.label25);
            this.pnlDetail.Controls.Add(this.label24);
            this.pnlDetail.Controls.Add(this.txtWP_PRE);
            this.pnlDetail.Controls.Add(this.dtpPR_LAST_APP);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtPRAnnualPresent);
            this.pnlDetail.Controls.Add(this.label22);
            this.pnlDetail.Controls.Add(this.label20);
            this.pnlDetail.Controls.Add(this.txtStepAmount);
            this.pnlDetail.Controls.Add(this.label21);
            this.pnlDetail.Controls.Add(this.txtPR_INC_AMT);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtRank);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.Controls.Add(this.lbtnIncrementType);
            this.pnlDetail.Controls.Add(this.txtType);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtWP_LEV);
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.txtWP_DES);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.txtASR);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.txtNoOfMonths);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtNoINCR);
            this.pnlDetail.Controls.Add(this.dtpToDate);
            this.pnlDetail.Controls.Add(this.dtpEffectiveDate);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementPromotionClericalCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 183);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(603, 420);
            this.pnlDetail.SPName = "CHRIS_SP_INC_PROMOTION_MANAGER";
            this.pnlDetail.TabIndex = 11;
            this.pnlDetail.TabStop = true;
            // 
            // viewoption
            // 
            this.viewoption.AllowSpace = true;
            this.viewoption.AssociatedLookUpName = "";
            this.viewoption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewoption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.viewoption.ContinuationTextBox = null;
            this.viewoption.CustomEnabled = true;
            this.viewoption.DataFieldMapping = "";
            this.viewoption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewoption.GetRecordsOnUpDownKeys = false;
            this.viewoption.IsDate = false;
            this.viewoption.Location = new System.Drawing.Point(439, 366);
            this.viewoption.MaxLength = 3;
            this.viewoption.Name = "viewoption";
            this.viewoption.NumberFormat = "###,###,##0.00";
            this.viewoption.Postfix = "";
            this.viewoption.Prefix = "";
            this.viewoption.Size = new System.Drawing.Size(61, 20);
            this.viewoption.SkipValidation = false;
            this.viewoption.TabIndex = 123;
            this.viewoption.TextType = CrplControlLibrary.TextType.String;
            this.viewoption.Visible = false;
            // 
            // txtPR_PROMOTED
            // 
            this.txtPR_PROMOTED.AllowSpace = true;
            this.txtPR_PROMOTED.AssociatedLookUpName = "";
            this.txtPR_PROMOTED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_PROMOTED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_PROMOTED.ContinuationTextBox = null;
            this.txtPR_PROMOTED.CustomEnabled = true;
            this.txtPR_PROMOTED.DataFieldMapping = "PR_PROMOTED";
            this.txtPR_PROMOTED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_PROMOTED.GetRecordsOnUpDownKeys = false;
            this.txtPR_PROMOTED.IsDate = false;
            this.txtPR_PROMOTED.Location = new System.Drawing.Point(372, 366);
            this.txtPR_PROMOTED.MaxLength = 3;
            this.txtPR_PROMOTED.Name = "txtPR_PROMOTED";
            this.txtPR_PROMOTED.NumberFormat = "###,###,##0.00";
            this.txtPR_PROMOTED.Postfix = "";
            this.txtPR_PROMOTED.Prefix = "";
            this.txtPR_PROMOTED.Size = new System.Drawing.Size(61, 20);
            this.txtPR_PROMOTED.SkipValidation = false;
            this.txtPR_PROMOTED.TabIndex = 122;
            this.txtPR_PROMOTED.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_PROMOTED.Visible = false;
            // 
            // txtprCurrentPrevious
            // 
            this.txtprCurrentPrevious.AllowSpace = true;
            this.txtprCurrentPrevious.AssociatedLookUpName = "";
            this.txtprCurrentPrevious.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprCurrentPrevious.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtprCurrentPrevious.ContinuationTextBox = null;
            this.txtprCurrentPrevious.CustomEnabled = true;
            this.txtprCurrentPrevious.DataFieldMapping = "PR_CURRENT_PREVIOUS";
            this.txtprCurrentPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprCurrentPrevious.GetRecordsOnUpDownKeys = false;
            this.txtprCurrentPrevious.IsDate = false;
            this.txtprCurrentPrevious.Location = new System.Drawing.Point(379, 392);
            this.txtprCurrentPrevious.Name = "txtprCurrentPrevious";
            this.txtprCurrentPrevious.NumberFormat = "###,###,##0.00";
            this.txtprCurrentPrevious.Postfix = "";
            this.txtprCurrentPrevious.Prefix = "";
            this.txtprCurrentPrevious.Size = new System.Drawing.Size(104, 20);
            this.txtprCurrentPrevious.SkipValidation = false;
            this.txtprCurrentPrevious.TabIndex = 121;
            this.txtprCurrentPrevious.TextType = CrplControlLibrary.TextType.String;
            this.txtprCurrentPrevious.Visible = false;
            // 
            // txtPR_DESIG_PRESENT
            // 
            this.txtPR_DESIG_PRESENT.AllowSpace = true;
            this.txtPR_DESIG_PRESENT.AssociatedLookUpName = "";
            this.txtPR_DESIG_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESIG_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESIG_PRESENT.ContinuationTextBox = null;
            this.txtPR_DESIG_PRESENT.CustomEnabled = false;
            this.txtPR_DESIG_PRESENT.DataFieldMapping = "PR_DESIG_PRESENT";
            this.txtPR_DESIG_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESIG_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESIG_PRESENT.IsDate = false;
            this.txtPR_DESIG_PRESENT.Location = new System.Drawing.Point(216, 366);
            this.txtPR_DESIG_PRESENT.Name = "txtPR_DESIG_PRESENT";
            this.txtPR_DESIG_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_DESIG_PRESENT.Postfix = "";
            this.txtPR_DESIG_PRESENT.Prefix = "";
            this.txtPR_DESIG_PRESENT.Size = new System.Drawing.Size(72, 20);
            this.txtPR_DESIG_PRESENT.SkipValidation = false;
            this.txtPR_DESIG_PRESENT.TabIndex = 120;
            this.txtPR_DESIG_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_DESIG_PRESENT.Visible = false;
            // 
            // txtwBasic
            // 
            this.txtwBasic.AllowSpace = true;
            this.txtwBasic.AssociatedLookUpName = "";
            this.txtwBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwBasic.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtwBasic.ContinuationTextBox = null;
            this.txtwBasic.CustomEnabled = true;
            this.txtwBasic.DataFieldMapping = "";
            this.txtwBasic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwBasic.GetRecordsOnUpDownKeys = false;
            this.txtwBasic.IsDate = false;
            this.txtwBasic.Location = new System.Drawing.Point(386, 293);
            this.txtwBasic.MaxLength = 7;
            this.txtwBasic.Name = "txtwBasic";
            this.txtwBasic.NumberFormat = "###,###,##0.00";
            this.txtwBasic.Postfix = "";
            this.txtwBasic.Prefix = "";
            this.txtwBasic.Size = new System.Drawing.Size(61, 20);
            this.txtwBasic.SkipValidation = false;
            this.txtwBasic.TabIndex = 119;
            this.txtwBasic.TabStop = false;
            this.txtwBasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtwBasic.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtprBranch
            // 
            this.txtprBranch.AllowSpace = true;
            this.txtprBranch.AssociatedLookUpName = "";
            this.txtprBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtprBranch.ContinuationTextBox = null;
            this.txtprBranch.CustomEnabled = true;
            this.txtprBranch.DataFieldMapping = "pr_new_branch";
            this.txtprBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprBranch.GetRecordsOnUpDownKeys = false;
            this.txtprBranch.IsDate = false;
            this.txtprBranch.Location = new System.Drawing.Point(283, 104);
            this.txtprBranch.Name = "txtprBranch";
            this.txtprBranch.NumberFormat = "###,###,##0.00";
            this.txtprBranch.Postfix = "";
            this.txtprBranch.Prefix = "";
            this.txtprBranch.Size = new System.Drawing.Size(33, 20);
            this.txtprBranch.SkipValidation = false;
            this.txtprBranch.TabIndex = 118;
            this.txtprBranch.TextType = CrplControlLibrary.TextType.String;
            this.txtprBranch.Visible = false;
            // 
            // txtWNo
            // 
            this.txtWNo.AllowSpace = true;
            this.txtWNo.AssociatedLookUpName = "";
            this.txtWNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWNo.ContinuationTextBox = null;
            this.txtWNo.CustomEnabled = true;
            this.txtWNo.DataFieldMapping = "";
            this.txtWNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWNo.GetRecordsOnUpDownKeys = false;
            this.txtWNo.IsDate = false;
            this.txtWNo.Location = new System.Drawing.Point(352, 104);
            this.txtWNo.Name = "txtWNo";
            this.txtWNo.NumberFormat = "###,###,##0.00";
            this.txtWNo.Postfix = "";
            this.txtWNo.Prefix = "";
            this.txtWNo.Size = new System.Drawing.Size(33, 20);
            this.txtWNo.SkipValidation = false;
            this.txtWNo.TabIndex = 117;
            this.txtWNo.TextType = CrplControlLibrary.TextType.String;
            this.txtWNo.Visible = false;
            // 
            // txtPR_FUNCT_2_PREVIOUS
            // 
            this.txtPR_FUNCT_2_PREVIOUS.AllowSpace = true;
            this.txtPR_FUNCT_2_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_FUNCT_2_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_2_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_2_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_FUNCT_2_PREVIOUS.CustomEnabled = false;
            this.txtPR_FUNCT_2_PREVIOUS.DataFieldMapping = "PR_FUNCT_2_PREVIOUS";
            this.txtPR_FUNCT_2_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_2_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_2_PREVIOUS.IsDate = false;
            this.txtPR_FUNCT_2_PREVIOUS.Location = new System.Drawing.Point(229, 392);
            this.txtPR_FUNCT_2_PREVIOUS.Name = "txtPR_FUNCT_2_PREVIOUS";
            this.txtPR_FUNCT_2_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_2_PREVIOUS.Postfix = "";
            this.txtPR_FUNCT_2_PREVIOUS.Prefix = "";
            this.txtPR_FUNCT_2_PREVIOUS.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_2_PREVIOUS.SkipValidation = false;
            this.txtPR_FUNCT_2_PREVIOUS.TabIndex = 116;
            this.txtPR_FUNCT_2_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_2_PREVIOUS.Visible = false;
            // 
            // txtPR_FUNCT_1_PREVIOUS
            // 
            this.txtPR_FUNCT_1_PREVIOUS.AllowSpace = true;
            this.txtPR_FUNCT_1_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_FUNCT_1_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_1_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_1_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_FUNCT_1_PREVIOUS.CustomEnabled = false;
            this.txtPR_FUNCT_1_PREVIOUS.DataFieldMapping = "PR_FUNCT_1_PREVIOUS";
            this.txtPR_FUNCT_1_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_1_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_1_PREVIOUS.IsDate = false;
            this.txtPR_FUNCT_1_PREVIOUS.Location = new System.Drawing.Point(161, 392);
            this.txtPR_FUNCT_1_PREVIOUS.Name = "txtPR_FUNCT_1_PREVIOUS";
            this.txtPR_FUNCT_1_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_1_PREVIOUS.Postfix = "";
            this.txtPR_FUNCT_1_PREVIOUS.Prefix = "";
            this.txtPR_FUNCT_1_PREVIOUS.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_1_PREVIOUS.SkipValidation = false;
            this.txtPR_FUNCT_1_PREVIOUS.TabIndex = 115;
            this.txtPR_FUNCT_1_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_1_PREVIOUS.Visible = false;
            // 
            // txtPR_LEVEL_PRESENT
            // 
            this.txtPR_LEVEL_PRESENT.AllowSpace = true;
            this.txtPR_LEVEL_PRESENT.AssociatedLookUpName = "";
            this.txtPR_LEVEL_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL_PRESENT.ContinuationTextBox = null;
            this.txtPR_LEVEL_PRESENT.CustomEnabled = false;
            this.txtPR_LEVEL_PRESENT.DataFieldMapping = "PR_LEVEL_PRESENT";
            this.txtPR_LEVEL_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL_PRESENT.IsDate = false;
            this.txtPR_LEVEL_PRESENT.Location = new System.Drawing.Point(294, 366);
            this.txtPR_LEVEL_PRESENT.Name = "txtPR_LEVEL_PRESENT";
            this.txtPR_LEVEL_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL_PRESENT.Postfix = "";
            this.txtPR_LEVEL_PRESENT.Prefix = "";
            this.txtPR_LEVEL_PRESENT.Size = new System.Drawing.Size(72, 20);
            this.txtPR_LEVEL_PRESENT.SkipValidation = false;
            this.txtPR_LEVEL_PRESENT.TabIndex = 114;
            this.txtPR_LEVEL_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_LEVEL_PRESENT.Visible = false;
            // 
            // txtAnnualPrevious
            // 
            this.txtAnnualPrevious.AllowSpace = true;
            this.txtAnnualPrevious.AssociatedLookUpName = "lbtnDesg";
            this.txtAnnualPrevious.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualPrevious.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualPrevious.ContinuationTextBox = null;
            this.txtAnnualPrevious.CustomEnabled = true;
            this.txtAnnualPrevious.DataFieldMapping = "PR_ANNUAL_PREVIOUS";
            this.txtAnnualPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualPrevious.GetRecordsOnUpDownKeys = false;
            this.txtAnnualPrevious.IsDate = false;
            this.txtAnnualPrevious.IsRequired = true;
            this.txtAnnualPrevious.Location = new System.Drawing.Point(299, 392);
            this.txtAnnualPrevious.MaxLength = 3;
            this.txtAnnualPrevious.Name = "txtAnnualPrevious";
            this.txtAnnualPrevious.NumberFormat = "###,###,##0.00";
            this.txtAnnualPrevious.Postfix = "";
            this.txtAnnualPrevious.Prefix = "";
            this.txtAnnualPrevious.Size = new System.Drawing.Size(75, 20);
            this.txtAnnualPrevious.SkipValidation = false;
            this.txtAnnualPrevious.TabIndex = 113;
            this.txtAnnualPrevious.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtAnnualPrevious, "Press <F9> Key to Display The List");
            this.txtAnnualPrevious.Visible = false;
            // 
            // txtPR_FUNCT_1_PRESENT
            // 
            this.txtPR_FUNCT_1_PRESENT.AllowSpace = true;
            this.txtPR_FUNCT_1_PRESENT.AssociatedLookUpName = "";
            this.txtPR_FUNCT_1_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_1_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_1_PRESENT.ContinuationTextBox = null;
            this.txtPR_FUNCT_1_PRESENT.CustomEnabled = false;
            this.txtPR_FUNCT_1_PRESENT.DataFieldMapping = "PR_FUNCT_1_PRESENT";
            this.txtPR_FUNCT_1_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_1_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_1_PRESENT.IsDate = false;
            this.txtPR_FUNCT_1_PRESENT.Location = new System.Drawing.Point(9, 366);
            this.txtPR_FUNCT_1_PRESENT.Name = "txtPR_FUNCT_1_PRESENT";
            this.txtPR_FUNCT_1_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_1_PRESENT.Postfix = "";
            this.txtPR_FUNCT_1_PRESENT.Prefix = "";
            this.txtPR_FUNCT_1_PRESENT.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_1_PRESENT.SkipValidation = false;
            this.txtPR_FUNCT_1_PRESENT.TabIndex = 112;
            this.txtPR_FUNCT_1_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_1_PRESENT.Visible = false;
            // 
            // txtPR_DESIG_PREVIOUS
            // 
            this.txtPR_DESIG_PREVIOUS.AllowSpace = true;
            this.txtPR_DESIG_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_DESIG_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESIG_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESIG_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_DESIG_PREVIOUS.CustomEnabled = false;
            this.txtPR_DESIG_PREVIOUS.DataFieldMapping = "PR_DESIG_PREVIOUS";
            this.txtPR_DESIG_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESIG_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESIG_PREVIOUS.IsDate = false;
            this.txtPR_DESIG_PREVIOUS.Location = new System.Drawing.Point(83, 392);
            this.txtPR_DESIG_PREVIOUS.Name = "txtPR_DESIG_PREVIOUS";
            this.txtPR_DESIG_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_DESIG_PREVIOUS.Postfix = "";
            this.txtPR_DESIG_PREVIOUS.Prefix = "";
            this.txtPR_DESIG_PREVIOUS.Size = new System.Drawing.Size(72, 20);
            this.txtPR_DESIG_PREVIOUS.SkipValidation = false;
            this.txtPR_DESIG_PREVIOUS.TabIndex = 110;
            this.txtPR_DESIG_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_DESIG_PREVIOUS.Visible = false;
            // 
            // txtPR_LEVEL_PREVIOUS
            // 
            this.txtPR_LEVEL_PREVIOUS.AllowSpace = true;
            this.txtPR_LEVEL_PREVIOUS.AssociatedLookUpName = "";
            this.txtPR_LEVEL_PREVIOUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL_PREVIOUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL_PREVIOUS.ContinuationTextBox = null;
            this.txtPR_LEVEL_PREVIOUS.CustomEnabled = false;
            this.txtPR_LEVEL_PREVIOUS.DataFieldMapping = "PR_LEVEL_PREVIOUS";
            this.txtPR_LEVEL_PREVIOUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL_PREVIOUS.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL_PREVIOUS.IsDate = false;
            this.txtPR_LEVEL_PREVIOUS.Location = new System.Drawing.Point(6, 397);
            this.txtPR_LEVEL_PREVIOUS.Name = "txtPR_LEVEL_PREVIOUS";
            this.txtPR_LEVEL_PREVIOUS.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL_PREVIOUS.Postfix = "";
            this.txtPR_LEVEL_PREVIOUS.Prefix = "";
            this.txtPR_LEVEL_PREVIOUS.Size = new System.Drawing.Size(72, 20);
            this.txtPR_LEVEL_PREVIOUS.SkipValidation = false;
            this.txtPR_LEVEL_PREVIOUS.TabIndex = 109;
            this.txtPR_LEVEL_PREVIOUS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_LEVEL_PREVIOUS.Visible = false;
            // 
            // txtPR_FUNCT_2_PRESENT
            // 
            this.txtPR_FUNCT_2_PRESENT.AllowSpace = true;
            this.txtPR_FUNCT_2_PRESENT.AssociatedLookUpName = "";
            this.txtPR_FUNCT_2_PRESENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_FUNCT_2_PRESENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_FUNCT_2_PRESENT.ContinuationTextBox = null;
            this.txtPR_FUNCT_2_PRESENT.CustomEnabled = false;
            this.txtPR_FUNCT_2_PRESENT.DataFieldMapping = "PR_FUNCT_2_PRESENT";
            this.txtPR_FUNCT_2_PRESENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_FUNCT_2_PRESENT.GetRecordsOnUpDownKeys = false;
            this.txtPR_FUNCT_2_PRESENT.IsDate = false;
            this.txtPR_FUNCT_2_PRESENT.Location = new System.Drawing.Point(75, 366);
            this.txtPR_FUNCT_2_PRESENT.Name = "txtPR_FUNCT_2_PRESENT";
            this.txtPR_FUNCT_2_PRESENT.NumberFormat = "###,###,##0.00";
            this.txtPR_FUNCT_2_PRESENT.Postfix = "";
            this.txtPR_FUNCT_2_PRESENT.Prefix = "";
            this.txtPR_FUNCT_2_PRESENT.Size = new System.Drawing.Size(64, 20);
            this.txtPR_FUNCT_2_PRESENT.SkipValidation = false;
            this.txtPR_FUNCT_2_PRESENT.TabIndex = 108;
            this.txtPR_FUNCT_2_PRESENT.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_FUNCT_2_PRESENT.Visible = false;
            // 
            // txtAnsW5
            // 
            this.txtAnsW5.AllowSpace = true;
            this.txtAnsW5.AssociatedLookUpName = "";
            this.txtAnsW5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnsW5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnsW5.ContinuationTextBox = null;
            this.txtAnsW5.CustomEnabled = true;
            this.txtAnsW5.DataFieldMapping = "";
            this.txtAnsW5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnsW5.GetRecordsOnUpDownKeys = false;
            this.txtAnsW5.IsDate = false;
            this.txtAnsW5.Location = new System.Drawing.Point(322, 104);
            this.txtAnsW5.Name = "txtAnsW5";
            this.txtAnsW5.NumberFormat = "###,###,##0.00";
            this.txtAnsW5.Postfix = "";
            this.txtAnsW5.Prefix = "";
            this.txtAnsW5.Size = new System.Drawing.Size(29, 20);
            this.txtAnsW5.SkipValidation = false;
            this.txtAnsW5.TabIndex = 107;
            this.txtAnsW5.TextType = CrplControlLibrary.TextType.String;
            this.txtAnsW5.Visible = false;
            // 
            // dtjoinDate
            // 
            this.dtjoinDate.CustomEnabled = true;
            this.dtjoinDate.CustomFormat = "dd/MM/yyyy";
            this.dtjoinDate.DataFieldMapping = "pr_joining_date";
            this.dtjoinDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtjoinDate.HasChanges = true;
            this.dtjoinDate.Location = new System.Drawing.Point(391, 103);
            this.dtjoinDate.Name = "dtjoinDate";
            this.dtjoinDate.NullValue = " ";
            this.dtjoinDate.Size = new System.Drawing.Size(85, 20);
            this.dtjoinDate.TabIndex = 106;
            this.dtjoinDate.Value = new System.DateTime(2011, 1, 15, 0, 0, 0, 0);
            this.dtjoinDate.Visible = false;
            // 
            // lbtnEffectiveDate
            // 
            this.lbtnEffectiveDate.ActionLOVExists = "Effective_Date_LOV_Exists";
            this.lbtnEffectiveDate.ActionType = "Effective_Date_LOV";
            this.lbtnEffectiveDate.ConditionalFields = "txtPersNo|txtType";
            this.lbtnEffectiveDate.CustomEnabled = true;
            this.lbtnEffectiveDate.DataFieldMapping = "";
            this.lbtnEffectiveDate.DependentLovControls = "";
            this.lbtnEffectiveDate.HiddenColumns = "";
            this.lbtnEffectiveDate.Image = ((System.Drawing.Image)(resources.GetObject("lbtnEffectiveDate.Image")));
            this.lbtnEffectiveDate.LoadDependentEntities = false;
            this.lbtnEffectiveDate.Location = new System.Drawing.Point(247, 102);
            this.lbtnEffectiveDate.LookUpTitle = null;
            this.lbtnEffectiveDate.Name = "lbtnEffectiveDate";
            this.lbtnEffectiveDate.Size = new System.Drawing.Size(26, 21);
            this.lbtnEffectiveDate.SkipValidationOnLeave = false;
            this.lbtnEffectiveDate.SPName = "CHRIS_SP_INC_PROMOTION_MANAGER";
            this.lbtnEffectiveDate.TabIndex = 105;
            this.lbtnEffectiveDate.TabStop = false;
            this.lbtnEffectiveDate.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 343);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(141, 20);
            this.label27.TabIndex = 104;
            this.label27.Text = "Remarks :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_REMARKS
            // 
            this.txtPR_REMARKS.AllowSpace = true;
            this.txtPR_REMARKS.AssociatedLookUpName = "lbtnDesg";
            this.txtPR_REMARKS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_REMARKS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_REMARKS.ContinuationTextBox = null;
            this.txtPR_REMARKS.CustomEnabled = true;
            this.txtPR_REMARKS.DataFieldMapping = "PR_REMARKS";
            this.txtPR_REMARKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_REMARKS.GetRecordsOnUpDownKeys = false;
            this.txtPR_REMARKS.IsDate = false;
            this.txtPR_REMARKS.IsRequired = true;
            this.txtPR_REMARKS.Location = new System.Drawing.Point(153, 343);
            this.txtPR_REMARKS.MaxLength = 50;
            this.txtPR_REMARKS.Name = "txtPR_REMARKS";
            this.txtPR_REMARKS.NumberFormat = "###,###,##0.00";
            this.txtPR_REMARKS.Postfix = "";
            this.txtPR_REMARKS.Prefix = "";
            this.txtPR_REMARKS.Size = new System.Drawing.Size(421, 20);
            this.txtPR_REMARKS.SkipValidation = false;
            this.txtPR_REMARKS.TabIndex = 13;
            this.txtPR_REMARKS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_REMARKS.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPR_REMARKS_PreviewKeyDown);
            // 
            // dtpNextAppraisalDate
            // 
            this.dtpNextAppraisalDate.CustomEnabled = true;
            this.dtpNextAppraisalDate.CustomFormat = "dd/MM/yyyy";
            this.dtpNextAppraisalDate.DataFieldMapping = "PR_NEXT_APP";
            this.dtpNextAppraisalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNextAppraisalDate.HasChanges = true;
            this.dtpNextAppraisalDate.Location = new System.Drawing.Point(153, 317);
            this.dtpNextAppraisalDate.Name = "dtpNextAppraisalDate";
            this.dtpNextAppraisalDate.NullValue = " ";
            this.dtpNextAppraisalDate.Size = new System.Drawing.Size(102, 20);
            this.dtpNextAppraisalDate.TabIndex = 12;
            this.dtpNextAppraisalDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpNextAppraisalDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpNextAppraisalDate_Validating);
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(21, 317);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(126, 20);
            this.label26.TabIndex = 101;
            this.label26.Text = "Next Appraisal :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(277, 291);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(108, 20);
            this.label25.TabIndex = 100;
            this.label25.Text = "Monthly Basic :";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(277, 265);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(108, 20);
            this.label24.TabIndex = 98;
            this.label24.Text = "Previous ASR :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWP_PRE
            // 
            this.txtWP_PRE.AllowSpace = true;
            this.txtWP_PRE.AssociatedLookUpName = "";
            this.txtWP_PRE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWP_PRE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWP_PRE.ContinuationTextBox = null;
            this.txtWP_PRE.CustomEnabled = false;
            this.txtWP_PRE.DataFieldMapping = "WP_PRE";
            this.txtWP_PRE.Enabled = false;
            this.txtWP_PRE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWP_PRE.GetRecordsOnUpDownKeys = false;
            this.txtWP_PRE.IsDate = false;
            this.txtWP_PRE.IsRequired = true;
            this.txtWP_PRE.Location = new System.Drawing.Point(386, 265);
            this.txtWP_PRE.MaxLength = 3;
            this.txtWP_PRE.Name = "txtWP_PRE";
            this.txtWP_PRE.NumberFormat = "###,###,##0.00";
            this.txtWP_PRE.Postfix = "";
            this.txtWP_PRE.Prefix = "";
            this.txtWP_PRE.Size = new System.Drawing.Size(61, 20);
            this.txtWP_PRE.SkipValidation = false;
            this.txtWP_PRE.TabIndex = 14;
            this.txtWP_PRE.TabStop = false;
            this.txtWP_PRE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWP_PRE.TextType = CrplControlLibrary.TextType.Double;
            // 
            // dtpPR_LAST_APP
            // 
            this.dtpPR_LAST_APP.CustomEnabled = true;
            this.dtpPR_LAST_APP.CustomFormat = "dd/MM/yyyy";
            this.dtpPR_LAST_APP.DataFieldMapping = "PR_LAST_APP";
            this.dtpPR_LAST_APP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPR_LAST_APP.HasChanges = true;
            this.dtpPR_LAST_APP.Location = new System.Drawing.Point(153, 265);
            this.dtpPR_LAST_APP.Name = "dtpPR_LAST_APP";
            this.dtpPR_LAST_APP.NullValue = " ";
            this.dtpPR_LAST_APP.Size = new System.Drawing.Size(102, 20);
            this.dtpPR_LAST_APP.TabIndex = 10;
            this.dtpPR_LAST_APP.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 291);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(141, 20);
            this.label23.TabIndex = 95;
            this.label23.Text = "New Annual Package :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRAnnualPresent
            // 
            this.txtPRAnnualPresent.AllowSpace = true;
            this.txtPRAnnualPresent.AssociatedLookUpName = "lbtnDesg";
            this.txtPRAnnualPresent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRAnnualPresent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRAnnualPresent.ContinuationTextBox = null;
            this.txtPRAnnualPresent.CustomEnabled = true;
            this.txtPRAnnualPresent.DataFieldMapping = "PR_ANNUAL_PRESENT";
            this.txtPRAnnualPresent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRAnnualPresent.GetRecordsOnUpDownKeys = false;
            this.txtPRAnnualPresent.IsDate = false;
            this.txtPRAnnualPresent.Location = new System.Drawing.Point(153, 291);
            this.txtPRAnnualPresent.MaxLength = 11;
            this.txtPRAnnualPresent.Name = "txtPRAnnualPresent";
            this.txtPRAnnualPresent.NumberFormat = "###,###,##0.00";
            this.txtPRAnnualPresent.Postfix = "";
            this.txtPRAnnualPresent.Prefix = "";
            this.txtPRAnnualPresent.Size = new System.Drawing.Size(102, 20);
            this.txtPRAnnualPresent.SkipValidation = false;
            this.txtPRAnnualPresent.TabIndex = 11;
            this.txtPRAnnualPresent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPRAnnualPresent.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(21, 265);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(126, 20);
            this.label22.TabIndex = 93;
            this.label22.Text = "Last Appraisal :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(21, 239);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(126, 20);
            this.label20.TabIndex = 91;
            this.label20.Text = "No. Of Months :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtStepAmount
            // 
            this.txtStepAmount.AllowSpace = true;
            this.txtStepAmount.AssociatedLookUpName = "";
            this.txtStepAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStepAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStepAmount.ContinuationTextBox = null;
            this.txtStepAmount.CustomEnabled = true;
            this.txtStepAmount.DataFieldMapping = "PR_STEP_AMT";
            this.txtStepAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStepAmount.GetRecordsOnUpDownKeys = false;
            this.txtStepAmount.IsDate = false;
            this.txtStepAmount.Location = new System.Drawing.Point(153, 213);
            this.txtStepAmount.MaxLength = 7;
            this.txtStepAmount.Name = "txtStepAmount";
            this.txtStepAmount.NumberFormat = "###,###,##0.00";
            this.txtStepAmount.Postfix = "";
            this.txtStepAmount.Prefix = "";
            this.txtStepAmount.Size = new System.Drawing.Size(102, 20);
            this.txtStepAmount.SkipValidation = false;
            this.txtStepAmount.TabIndex = 8;
            this.txtStepAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStepAmount.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(21, 211);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(126, 20);
            this.label21.TabIndex = 89;
            this.label21.Text = "Stepping Amount :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_INC_AMT
            // 
            this.txtPR_INC_AMT.AllowSpace = true;
            this.txtPR_INC_AMT.AssociatedLookUpName = "";
            this.txtPR_INC_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_INC_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_INC_AMT.ContinuationTextBox = null;
            this.txtPR_INC_AMT.CustomEnabled = true;
            this.txtPR_INC_AMT.DataFieldMapping = "PR_INC_AMT";
            this.txtPR_INC_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_INC_AMT.GetRecordsOnUpDownKeys = false;
            this.txtPR_INC_AMT.IsDate = false;
            this.txtPR_INC_AMT.Location = new System.Drawing.Point(153, 187);
            this.txtPR_INC_AMT.MaxLength = 7;
            this.txtPR_INC_AMT.Name = "txtPR_INC_AMT";
            this.txtPR_INC_AMT.NumberFormat = "###,###,##0.00";
            this.txtPR_INC_AMT.Postfix = "";
            this.txtPR_INC_AMT.Prefix = "";
            this.txtPR_INC_AMT.Size = new System.Drawing.Size(102, 20);
            this.txtPR_INC_AMT.SkipValidation = false;
            this.txtPR_INC_AMT.TabIndex = 7;
            this.txtPR_INC_AMT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPR_INC_AMT.TextType = CrplControlLibrary.TextType.Double;
            this.txtPR_INC_AMT.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_INC_AMT_Validating);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 185);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 20);
            this.label14.TabIndex = 87;
            this.label14.Text = "Increment Amount :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 156);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 20);
            this.label12.TabIndex = 86;
            this.label12.Text = "No. Of Increments :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(269, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 20);
            this.label9.TabIndex = 85;
            this.label9.Text = "(smaller than Effective Date)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRank
            // 
            this.txtRank.AllowSpace = true;
            this.txtRank.AssociatedLookUpName = "";
            this.txtRank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRank.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRank.ContinuationTextBox = null;
            this.txtRank.CustomEnabled = true;
            this.txtRank.DataFieldMapping = "PR_RANK";
            this.txtRank.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRank.GetRecordsOnUpDownKeys = false;
            this.txtRank.IsDate = false;
            this.txtRank.Location = new System.Drawing.Point(153, 129);
            this.txtRank.MaxLength = 1;
            this.txtRank.Name = "txtRank";
            this.txtRank.NumberFormat = "###,###,##0.00";
            this.txtRank.Postfix = "";
            this.txtRank.Prefix = "";
            this.txtRank.Size = new System.Drawing.Size(20, 20);
            this.txtRank.SkipValidation = false;
            this.txtRank.TabIndex = 4;
            this.txtRank.TextType = CrplControlLibrary.TextType.String;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(243, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(238, 20);
            this.label19.TabIndex = 83;
            this.label19.Text = "[C]ourse in[T]erim [Y]early [P]romotion";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbtnIncrementType
            // 
            this.lbtnIncrementType.ActionLOVExists = "INC_TYPES_LOV_Exists";
            this.lbtnIncrementType.ActionType = "INC_TYPES_LOV";
            this.lbtnIncrementType.ConditionalFields = "txtPersNo";
            this.lbtnIncrementType.CustomEnabled = true;
            this.lbtnIncrementType.DataFieldMapping = "";
            this.lbtnIncrementType.DependentLovControls = "";
            this.lbtnIncrementType.HiddenColumns = "";
            this.lbtnIncrementType.Image = ((System.Drawing.Image)(resources.GetObject("lbtnIncrementType.Image")));
            this.lbtnIncrementType.LoadDependentEntities = false;
            this.lbtnIncrementType.Location = new System.Drawing.Point(216, 75);
            this.lbtnIncrementType.LookUpTitle = null;
            this.lbtnIncrementType.Name = "lbtnIncrementType";
            this.lbtnIncrementType.Size = new System.Drawing.Size(26, 21);
            this.lbtnIncrementType.SkipValidationOnLeave = false;
            this.lbtnIncrementType.SPName = "CHRIS_SP_INC_PROMOTION_MANAGER";
            this.lbtnIncrementType.TabIndex = 81;
            this.lbtnIncrementType.TabStop = false;
            this.lbtnIncrementType.UseVisualStyleBackColor = true;
            // 
            // txtType
            // 
            this.txtType.AllowSpace = true;
            this.txtType.AssociatedLookUpName = "lbtnIncrementType";
            this.txtType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtType.ContinuationTextBox = null;
            this.txtType.CustomEnabled = true;
            this.txtType.DataFieldMapping = "PR_INC_TYPE";
            this.txtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.GetRecordsOnUpDownKeys = false;
            this.txtType.IsDate = false;
            this.txtType.IsRequired = true;
            this.txtType.Location = new System.Drawing.Point(153, 75);
            this.txtType.MaxLength = 6;
            this.txtType.Name = "txtType";
            this.txtType.NumberFormat = "###,###,##0.00";
            this.txtType.Postfix = "";
            this.txtType.Prefix = "";
            this.txtType.Size = new System.Drawing.Size(59, 20);
            this.txtType.SkipValidation = false;
            this.txtType.TabIndex = 2;
            this.txtType.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtType, "Press <F9> Key to Display The List");
            this.txtType.Validating += new System.ComponentModel.CancelEventHandler(this.txtType_Validating);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(37, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 20);
            this.label13.TabIndex = 82;
            this.label13.Text = "Increment Type :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWP_LEV
            // 
            this.txtWP_LEV.AllowSpace = true;
            this.txtWP_LEV.AssociatedLookUpName = "";
            this.txtWP_LEV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWP_LEV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWP_LEV.ContinuationTextBox = null;
            this.txtWP_LEV.CustomEnabled = false;
            this.txtWP_LEV.DataFieldMapping = "WP_LEV";
            this.txtWP_LEV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWP_LEV.GetRecordsOnUpDownKeys = false;
            this.txtWP_LEV.IsDate = false;
            this.txtWP_LEV.Location = new System.Drawing.Point(516, 23);
            this.txtWP_LEV.MaxLength = 20;
            this.txtWP_LEV.Name = "txtWP_LEV";
            this.txtWP_LEV.NumberFormat = "###,###,##0.00";
            this.txtWP_LEV.Postfix = "";
            this.txtWP_LEV.Prefix = "";
            this.txtWP_LEV.ReadOnly = true;
            this.txtWP_LEV.Size = new System.Drawing.Size(61, 20);
            this.txtWP_LEV.SkipValidation = false;
            this.txtWP_LEV.TabIndex = 78;
            this.txtWP_LEV.TabStop = false;
            this.txtWP_LEV.TextType = CrplControlLibrary.TextType.String;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(465, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 20);
            this.label18.TabIndex = 79;
            this.label18.Text = "Level:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWP_DES
            // 
            this.txtWP_DES.AllowSpace = true;
            this.txtWP_DES.AssociatedLookUpName = "";
            this.txtWP_DES.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWP_DES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWP_DES.ContinuationTextBox = null;
            this.txtWP_DES.CustomEnabled = false;
            this.txtWP_DES.DataFieldMapping = "WP_DES";
            this.txtWP_DES.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWP_DES.GetRecordsOnUpDownKeys = false;
            this.txtWP_DES.IsDate = false;
            this.txtWP_DES.Location = new System.Drawing.Point(396, 23);
            this.txtWP_DES.MaxLength = 20;
            this.txtWP_DES.Name = "txtWP_DES";
            this.txtWP_DES.NumberFormat = "###,###,##0.00";
            this.txtWP_DES.Postfix = "";
            this.txtWP_DES.Prefix = "";
            this.txtWP_DES.ReadOnly = true;
            this.txtWP_DES.Size = new System.Drawing.Size(62, 20);
            this.txtWP_DES.SkipValidation = false;
            this.txtWP_DES.TabIndex = 76;
            this.txtWP_DES.TabStop = false;
            this.txtWP_DES.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(346, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 20);
            this.label17.TabIndex = 77;
            this.label17.Text = "Desig.:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtASR
            // 
            this.txtASR.AllowSpace = true;
            this.txtASR.AssociatedLookUpName = "";
            this.txtASR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtASR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtASR.ContinuationTextBox = null;
            this.txtASR.CustomEnabled = false;
            this.txtASR.DataFieldMapping = "WP_ASR";
            this.txtASR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtASR.GetRecordsOnUpDownKeys = false;
            this.txtASR.IsDate = false;
            this.txtASR.Location = new System.Drawing.Point(283, 23);
            this.txtASR.MaxLength = 20;
            this.txtASR.Name = "txtASR";
            this.txtASR.NumberFormat = "###,###,##0.00";
            this.txtASR.Postfix = "";
            this.txtASR.Prefix = "";
            this.txtASR.ReadOnly = true;
            this.txtASR.Size = new System.Drawing.Size(57, 20);
            this.txtASR.SkipValidation = false;
            this.txtASR.TabIndex = 74;
            this.txtASR.TabStop = false;
            this.txtASR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtASR.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(242, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 20);
            this.label15.TabIndex = 75;
            this.label15.Text = "ASR:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNoOfMonths
            // 
            this.txtNoOfMonths.AllowSpace = true;
            this.txtNoOfMonths.AssociatedLookUpName = "lbtnDesg";
            this.txtNoOfMonths.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoOfMonths.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoOfMonths.ContinuationTextBox = null;
            this.txtNoOfMonths.CustomEnabled = true;
            this.txtNoOfMonths.DataFieldMapping = "PR_NO_MONTHS";
            this.txtNoOfMonths.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoOfMonths.GetRecordsOnUpDownKeys = false;
            this.txtNoOfMonths.IsDate = false;
            this.txtNoOfMonths.Location = new System.Drawing.Point(153, 239);
            this.txtNoOfMonths.MaxLength = 2;
            this.txtNoOfMonths.Name = "txtNoOfMonths";
            this.txtNoOfMonths.NumberFormat = "###,###,##0.00";
            this.txtNoOfMonths.Postfix = "";
            this.txtNoOfMonths.Prefix = "";
            this.txtNoOfMonths.Size = new System.Drawing.Size(102, 20);
            this.txtNoOfMonths.SkipValidation = false;
            this.txtNoOfMonths.TabIndex = 9;
            this.txtNoOfMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoOfMonths.TextType = CrplControlLibrary.TextType.Double;
            this.txtNoOfMonths.Validating += new System.ComponentModel.CancelEventHandler(this.txtNoOfMonths_Validating);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(9, 129);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(138, 20);
            this.label16.TabIndex = 69;
            this.label16.Text = "Rate && Rating Date :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNoINCR
            // 
            this.txtNoINCR.AllowSpace = true;
            this.txtNoINCR.AssociatedLookUpName = "";
            this.txtNoINCR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoINCR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoINCR.ContinuationTextBox = null;
            this.txtNoINCR.CustomEnabled = true;
            this.txtNoINCR.DataFieldMapping = "PR_NO_INCR";
            this.txtNoINCR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoINCR.GetRecordsOnUpDownKeys = false;
            this.txtNoINCR.IsDate = false;
            this.txtNoINCR.Location = new System.Drawing.Point(153, 156);
            this.txtNoINCR.MaxLength = 2;
            this.txtNoINCR.Name = "txtNoINCR";
            this.txtNoINCR.NumberFormat = "###,###,##0.00";
            this.txtNoINCR.Postfix = "";
            this.txtNoINCR.Prefix = "";
            this.txtNoINCR.Size = new System.Drawing.Size(39, 20);
            this.txtNoINCR.SkipValidation = false;
            this.txtNoINCR.TabIndex = 6;
            this.txtNoINCR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoINCR.TextType = CrplControlLibrary.TextType.Double;
            this.txtNoINCR.Validating += new System.ComponentModel.CancelEventHandler(this.txtNoINCR_Validating);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomEnabled = true;
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.DataFieldMapping = "PR_RANKING_DATE";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.HasChanges = true;
            this.dtpToDate.Location = new System.Drawing.Point(180, 129);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.NullValue = " ";
            this.dtpToDate.Size = new System.Drawing.Size(88, 20);
            this.dtpToDate.TabIndex = 5;
            this.dtpToDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpToDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpToDate_Validating);
            // 
            // dtpEffectiveDate
            // 
            this.dtpEffectiveDate.CustomEnabled = true;
            this.dtpEffectiveDate.CustomFormat = "dd/MM/yyyy";
            this.dtpEffectiveDate.DataFieldMapping = "PR_EFFECTIVE";
            this.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEffectiveDate.HasChanges = true;
            this.dtpEffectiveDate.Location = new System.Drawing.Point(153, 102);
            this.dtpEffectiveDate.Name = "dtpEffectiveDate";
            this.dtpEffectiveDate.NullValue = " ";
            this.dtpEffectiveDate.Size = new System.Drawing.Size(88, 20);
            this.dtpEffectiveDate.TabIndex = 3;
            this.dtpEffectiveDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpEffectiveDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpEffectiveDate_Validating);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 20);
            this.label10.TabIndex = 30;
            this.label10.Text = "Effective from :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = false;
            this.txtName.DataFieldMapping = "PrName";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.IsRequired = true;
            this.txtName.Location = new System.Drawing.Point(153, 49);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(331, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "Name :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PrPNoLOV_Exists";
            this.lbtnPNo.ActionType = "PrPNoLOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "pr_new_branch|WP_ASR|WP_DES|WP_LEV|WP_PRE";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(215, 23);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_INC_PROMOTION_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_IN_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(153, 23);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(59, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Double;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(37, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "Personnel No. :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(153, 23);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(36, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(374, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 118;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // PnlDetails
            // 
            this.PnlDetails.ConcurrentPanels = null;
            this.PnlDetails.Controls.Add(this.DGVCourse);
            this.PnlDetails.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlDetails.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetails.DependentPanels = null;
            this.PnlDetails.DisableDependentLoad = false;
            this.PnlDetails.EnableDelete = true;
            this.PnlDetails.EnableInsert = true;
            this.PnlDetails.EnableQuery = false;
            this.PnlDetails.EnableUpdate = true;
            this.PnlDetails.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementCourse_Command";
            this.PnlDetails.Location = new System.Drawing.Point(643, 183);
            this.PnlDetails.MasterPanel = this.pnlDetail;
            this.PnlDetails.Name = "PnlDetails";
            this.PnlDetails.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetails.Size = new System.Drawing.Size(267, 136);
            this.PnlDetails.SPName = "CHRIS_SP_INCREMENT_PROMOTION_CTT_COURSE_MANAGER";
            this.PnlDetails.TabIndex = 119;
            // 
            // DGVCourse
            // 
            this.DGVCourse.AllowUserToAddRows = false;
            this.DGVCourse.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCourse.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVCourse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVCourse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CTT_YEAR,
            this.CTT_TITLE,
            this.CTT_INSTITUTE,
            this.CTT_GRADE,
            this.Column1,
            this.PR_IN_NO,
            this.CTT_PNO,
            this.PR_EFFECTIVE});
            this.DGVCourse.ColumnToHide = null;
            this.DGVCourse.ColumnWidth = null;
            this.DGVCourse.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVCourse.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGVCourse.DisplayColumnWrapper = null;
            this.DGVCourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVCourse.GridDefaultRow = 0;
            this.DGVCourse.Location = new System.Drawing.Point(0, 0);
            this.DGVCourse.Name = "DGVCourse";
            this.DGVCourse.ReadOnlyColumns = null;
            this.DGVCourse.RequiredColumns = null;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCourse.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGVCourse.Size = new System.Drawing.Size(267, 136);
            this.DGVCourse.SkippingColumns = null;
            this.DGVCourse.TabIndex = 0;
            // 
            // CTT_YEAR
            // 
            this.CTT_YEAR.DataPropertyName = "CTT_YEAR";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.CTT_YEAR.DefaultCellStyle = dataGridViewCellStyle2;
            this.CTT_YEAR.HeaderText = "CTT_YEAR";
            this.CTT_YEAR.MaxInputLength = 4;
            this.CTT_YEAR.Name = "CTT_YEAR";
            // 
            // CTT_TITLE
            // 
            this.CTT_TITLE.DataPropertyName = "CTT_TITLE";
            this.CTT_TITLE.HeaderText = "CTT_TITLE";
            this.CTT_TITLE.MaxInputLength = 15;
            this.CTT_TITLE.Name = "CTT_TITLE";
            // 
            // CTT_INSTITUTE
            // 
            this.CTT_INSTITUTE.DataPropertyName = "CTT_INSTITUTE";
            this.CTT_INSTITUTE.HeaderText = "CTT_INSTITUTE";
            this.CTT_INSTITUTE.MaxInputLength = 20;
            this.CTT_INSTITUTE.Name = "CTT_INSTITUTE";
            // 
            // CTT_GRADE
            // 
            this.CTT_GRADE.DataPropertyName = "CTT_GRADE";
            this.CTT_GRADE.HeaderText = "CTT_GRADE";
            this.CTT_GRADE.MaxInputLength = 10;
            this.CTT_GRADE.Name = "CTT_GRADE";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // PR_IN_NO
            // 
            this.PR_IN_NO.DataPropertyName = "PR_IN_NO";
            this.PR_IN_NO.HeaderText = "PR_IN_NO";
            this.PR_IN_NO.Name = "PR_IN_NO";
            this.PR_IN_NO.ReadOnly = true;
            this.PR_IN_NO.Visible = false;
            // 
            // CTT_PNO
            // 
            this.CTT_PNO.DataPropertyName = "CTT_PNO";
            this.CTT_PNO.HeaderText = "CTT_PNO";
            this.CTT_PNO.Name = "CTT_PNO";
            // 
            // PR_EFFECTIVE
            // 
            this.PR_EFFECTIVE.DataPropertyName = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.HeaderText = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.Name = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.Visible = false;
            // 
            // pnlCourseDtl
            // 
            this.pnlCourseDtl.ConcurrentPanels = null;
            this.pnlCourseDtl.Controls.Add(this.dgvCourseDtl);
            this.pnlCourseDtl.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlCourseDtl.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlCourseDtl.DependentPanels = null;
            this.pnlCourseDtl.DisableDependentLoad = false;
            this.pnlCourseDtl.EnableDelete = true;
            this.pnlCourseDtl.EnableInsert = true;
            this.pnlCourseDtl.EnableQuery = false;
            this.pnlCourseDtl.EnableUpdate = true;
            this.pnlCourseDtl.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementCourse_Command";
            this.pnlCourseDtl.Location = new System.Drawing.Point(675, 339);
            this.pnlCourseDtl.MasterPanel = null;
            this.pnlCourseDtl.Name = "pnlCourseDtl";
            this.pnlCourseDtl.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlCourseDtl.Size = new System.Drawing.Size(200, 166);
            this.pnlCourseDtl.SPName = "CHRIS_SP_INCREMENT_PROMOTION_CTT_COURSE_MANAGER";
            this.pnlCourseDtl.TabIndex = 125;
            // 
            // dgvCourseDtl
            // 
            this.dgvCourseDtl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCourseDtl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPR_IN_NO,
            this.colPR_EFFECTIVE,
            this.colCTT_YEAR,
            this.colCTT_TITLE,
            this.colCTT_INSTITUTE,
            this.colCTT_GRADE,
            this.colID});
            this.dgvCourseDtl.ColumnToHide = null;
            this.dgvCourseDtl.ColumnWidth = null;
            this.dgvCourseDtl.CustomEnabled = true;
            this.dgvCourseDtl.DisplayColumnWrapper = null;
            this.dgvCourseDtl.GridDefaultRow = 0;
            this.dgvCourseDtl.Location = new System.Drawing.Point(3, 4);
            this.dgvCourseDtl.Name = "dgvCourseDtl";
            this.dgvCourseDtl.ReadOnlyColumns = null;
            this.dgvCourseDtl.RequiredColumns = null;
            this.dgvCourseDtl.Size = new System.Drawing.Size(179, 150);
            this.dgvCourseDtl.SkippingColumns = null;
            this.dgvCourseDtl.TabIndex = 0;
            // 
            // colPR_IN_NO
            // 
            this.colPR_IN_NO.DataPropertyName = "lPR_IN_NO";
            this.colPR_IN_NO.HeaderText = "PR_IN_NO";
            this.colPR_IN_NO.Name = "colPR_IN_NO";
            // 
            // colPR_EFFECTIVE
            // 
            this.colPR_EFFECTIVE.DataPropertyName = "PR_EFFECTIVE";
            this.colPR_EFFECTIVE.HeaderText = "PR_EFFECTIVE";
            this.colPR_EFFECTIVE.Name = "colPR_EFFECTIVE";
            // 
            // colCTT_YEAR
            // 
            this.colCTT_YEAR.DataPropertyName = "CTT_YEAR";
            this.colCTT_YEAR.HeaderText = "CTT_YEAR";
            this.colCTT_YEAR.Name = "colCTT_YEAR";
            // 
            // colCTT_TITLE
            // 
            this.colCTT_TITLE.DataPropertyName = "CTT_TITLE";
            this.colCTT_TITLE.HeaderText = "CTT_TITLE";
            this.colCTT_TITLE.Name = "colCTT_TITLE";
            // 
            // colCTT_INSTITUTE
            // 
            this.colCTT_INSTITUTE.DataPropertyName = "CTT_INSTITUTE";
            this.colCTT_INSTITUTE.HeaderText = "CTT_INSTITUTE";
            this.colCTT_INSTITUTE.Name = "colCTT_INSTITUTE";
            // 
            // colCTT_GRADE
            // 
            this.colCTT_GRADE.DataPropertyName = "CTT_GRADE";
            this.colCTT_GRADE.HeaderText = "CTT_GRADE";
            this.colCTT_GRADE.Name = "colCTT_GRADE";
            // 
            // colID
            // 
            this.colID.DataPropertyName = "ID";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            // 
            // CHRIS_Personnel_IncremPromotCleric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(620, 668);
            this.Controls.Add(this.pnlCourseDtl);
            this.Controls.Add(this.txttextMode);
            this.Controls.Add(this.PnlDetails);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlDetail);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_IncremPromotCleric";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_IncremPromotCleric";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_IncremPromotCleric_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.Controls.SetChildIndex(this.PnlDetails, 0);
            this.Controls.SetChildIndex(this.txttextMode, 0);
            this.Controls.SetChildIndex(this.pnlCourseDtl, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.PnlDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVCourse)).EndInit();
            this.pnlCourseDtl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourseDtl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtNoOfMonths;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtNoINCR;
        private CrplControlLibrary.SLDatePicker dtpToDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtID;
        private CrplControlLibrary.SLTextBox txtWP_LEV;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtWP_DES;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtASR;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtRank;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.LookupButton lbtnIncrementType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtWP_PRE;
        private CrplControlLibrary.SLDatePicker dtpPR_LAST_APP;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtPRAnnualPresent;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtStepAmount;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtPR_INC_AMT;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox txtPR_REMARKS;
        private CrplControlLibrary.SLDatePicker dtpNextAppraisalDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private CrplControlLibrary.LookupButton lbtnEffectiveDate;
        private CrplControlLibrary.SLDatePicker dtjoinDate;
        private CrplControlLibrary.SLTextBox txtAnsW5;
        private CrplControlLibrary.SLTextBox txtPR_DESIG_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtPR_LEVEL_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtAnnualPrevious;
        private CrplControlLibrary.SLTextBox txtPR_FUNCT_2_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtPR_FUNCT_1_PREVIOUS;
        private CrplControlLibrary.SLTextBox txtWNo;
        private CrplControlLibrary.SLTextBox txtprBranch;
        private CrplControlLibrary.SLTextBox txtwBasic;
        private CrplControlLibrary.SLTextBox txtPR_DESIG_PRESENT;
        private CrplControlLibrary.SLTextBox txtprCurrentPrevious;
        private CrplControlLibrary.SLTextBox txtPR_PROMOTED;
        private System.Windows.Forms.Label txtUserName;
        public CrplControlLibrary.SLTextBox viewoption;
        public CrplControlLibrary.SLTextBox txtPR_FUNCT_2_PRESENT;
        public CrplControlLibrary.SLTextBox txtPR_FUNCT_1_PRESENT;
        public CrplControlLibrary.SLTextBox txtPR_LEVEL_PRESENT;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetails;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVCourse;
        public CrplControlLibrary.SLTextBox txtPersNo;
        public CrplControlLibrary.SLTextBox txttextMode;
        public CrplControlLibrary.SLDatePicker dtpEffectiveDate;
        public CrplControlLibrary.SLTextBox txtType;
        private CrplControlLibrary.SLTextBox txtpersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_TITLE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_INSTITUTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_GRADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_IN_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_PNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_EFFECTIVE;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlCourseDtl;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvCourseDtl;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPR_IN_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPR_EFFECTIVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCTT_YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCTT_TITLE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCTT_INSTITUTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCTT_GRADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
    }
}