using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class Chris_Personnel_ActualLeaveEntry_ : ChrisMasterDetailForm
    {
        public Chris_Personnel_ActualLeaveEntry_()
        {
            InitializeComponent();
        }
        public Chris_Personnel_ActualLeaveEntry_(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(PnlLeaveActual);
            //PnlLeaveStatus.DependentPanels = lstDependentPanels;
            //this.IndependentPanels.Add(PnlLeaveStatus);
            
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtOption.Visible = false;
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlLeaveActual1);
            this.PnlLeaveStatus.DependentPanels = lstDependentPanels;
            this.IndependentPanels = new List<SLPanel>();
            this.IndependentPanels.Add(PnlLeaveStatus);
            this.tbtSave.Visible = false;
            this.tbtList.Visible = false;
            this.tbtDelete.Visible = false;
            this.tbtAdd.Visible = false;
            txtLev_no.Select();
            txtLev_no.Focus();


            LEV_LV_TYPE.HeaderCell.Style.Font = new Font("Microsoft sens serif", 8.25f, FontStyle.Bold); 
            LEV_START_DATE.HeaderCell.Style.Font = new Font("Microsoft Sens Serif" , 8.25f,FontStyle.Bold);
            LEV_END_DATE.HeaderCell.Style.Font = new Font("Microsoft Sens Serif",8.25f,FontStyle.Bold);
            W_DAYS_DIS.HeaderCell.Style.Font = new Font("Microsoft sans serif",8.25f,FontStyle.Bold);
            LEV_REMARKS.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            
        }



        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                
                base.ClearForm(PnlLeaveStatus.Controls);
                base.ClearForm(PnlLeaveActual1.Controls);
                this.FunctionConfig.CurrentOption = Function.None;
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtLev_no.Select();
                txtLev_no.Focus();
                return;
            }

            if (actionType == "Close")
            {                        
            base.DoToolbarActions(ctrlsCollection, actionType);
            }


            
        }
        private void Chris_Personnel_ActualLeaveEntry_gridview_Shown(object sender, EventArgs e)
        {
            //InitializeComponent();
            //txtOption.Visible = false;
            //List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(PnlLeaveActual1);
            //this.PnlLeaveStatus.DependentPanels = lstDependentPanels;
            //this.IndependentPanels = new List<SLPanel>();
            //this.IndependentPanels.Add(PnlLeaveStatus);
            //// this.IndependentPanels.Add(PnlLeaveStatus);
        }

        private void txtPlAvailed_TextChanged(object sender, EventArgs e)
        {
           

        }

        private void txtLev_no_Validating(object sender, CancelEventArgs e)
        {
            int plavail = 0;
            int clavail = 0;
            int slavail = 0;
            int mlavail = 0;

            int plBalance = 0;
            int clBalance = 0;
            int slBalance = 0;
            int mlBalance = 0;
            int ls_sl_hf_bal = 0;
            int ls_c_forward = 0;



            if (txtPlAvailed.Text != string.Empty)
            {
                plavail = int.Parse(txtPlAvailed.Text);
            }
            if (txtClAvailed.Text != string.Empty)
            {
                clavail = int.Parse(txtClAvailed.Text);
            }
            if (txtSlAvailed.Text != string.Empty)
            {
                slavail = int.Parse(txtSlAvailed.Text);
            }
            if (txtMlAvailed.Text != string.Empty)
            {
                mlavail = int.Parse(txtMlAvailed.Text);
            }


            if (txtPlBalance.Text != string.Empty)
            {
                plBalance = int.Parse(txtPlBalance.Text);

            }
            if (txtSlBalance.Text != string.Empty)
            {
                slBalance = int.Parse(txtSlBalance.Text);

            }
            if (txtMlBalance.Text != string.Empty)
            {
                mlBalance = int.Parse(txtMlBalance.Text);

            }
            if (txtClBalance.Text != string.Empty)
            {
                clBalance = int.Parse(txtClBalance.Text);

            }


            if (txtslhalfbal.Text != string.Empty)
            {
                ls_sl_hf_bal = int.Parse(txtslhalfbal.Text);

            }

            if (txtCarryforward.Text != string.Empty)
            {
                ls_c_forward = int.Parse(txtCarryforward.Text);

            }




            int plEntilte = plBalance;
            int slEntilte = slBalance;
            int clEntilte = clBalance;
            int mlEntilte = mlBalance;

            txtPlEntitlement.Text = Convert.ToString(plEntilte);
            txtClEntitlement.Text = Convert.ToString(clEntilte);
            txtSlEntitlement.Text = Convert.ToString(slEntilte);
            txtMlEntitlement.Text = Convert.ToString(mlEntilte);


            if (txtPlAdvance.Text == "0")
            {
                txtPlBalance.Text = Convert.ToString(((plBalance) + (ls_c_forward)) - (plavail));
            }
            else
            {
                txtPlBalance.Text = "0";
            }



            txtClBalance.Text = Convert.ToString((clBalance) - (clavail));
            if ((slavail) > (slBalance)) 
                    {
                        txtslhalfbal.Text = Convert.ToString((slBalance + ls_sl_hf_bal) - (slavail));
                    txtSlBalance.Text = "0";
                    }
        else
        {
            txtSlBalance.Text = Convert.ToString(slBalance - slavail);
        }
        txtMlBalance.Text = Convert.ToString((mlBalance) - (mlavail));




        }

        private void slTextBox4_TextChanged(object sender, EventArgs e)
        {

        }

          protected override bool  Quit()
            {
                if (this.txtLev_no.Text != string.Empty)
                {
                    this.FunctionConfig.CurrentOption = Function.Add;
                    base.Quit();
                    return false;
                }
                else
                {
                    base.Quit();
                    return false;
                }
            
            } 

      

        
    }
}