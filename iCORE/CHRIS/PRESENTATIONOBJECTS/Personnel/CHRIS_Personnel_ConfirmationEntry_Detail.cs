﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_ConfirmationEntry_Detail : Form
    {
        public CHRIS_Personnel_ConfirmationEntry_Detail()
        {
            InitializeComponent();
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            string get_PR_No = string.Empty;
            int RowCount = dtGVConfirmation.Rows.Count;

            if (RowCount >= 1)
            {
                foreach (DataGridViewRow row in dtGVConfirmation.Rows)
                {
                    get_PR_No = row.Cells[0].Value.ToString();
                    break;
                }

                if (!string.IsNullOrEmpty(get_PR_No))
                {
                    int id = Convert.ToInt32(get_PR_No);
                    SQLManager.ApprovedConfirmation(id);


                    MessageBox.Show("Record Save Successfully");
                    this.Close();
                }

            }



        }
    

    private void CHRIS_Personnel_ConfirmationEntry_Detail_Load(object sender, EventArgs e)
    {
        DataTable dtDept = new DataTable();
        dtDept = SQLManager.CHRIS_SP_GET_Confir_PRNO(CHRIS_Personnel_ConfirmationEntry_View.SetValuePR_NO).Tables[0];
        dtGVConfirmation.DataSource = dtDept;
        dtGVConfirmation.Update();
        dtGVConfirmation.Visible = true;
    }

    private void btnReject_Click(object sender, EventArgs e)
    {
            string get_PR_No = string.Empty;
            int RowCount = dtGVConfirmation.Rows.Count;

            if (RowCount >= 1)
            {
                foreach (DataGridViewRow row in dtGVConfirmation.Rows)
                {
                    get_PR_No = row.Cells[0].Value.ToString();
                }

                if (!string.IsNullOrEmpty(get_PR_No))
                {
                    int id = Convert.ToInt32(get_PR_No);
                    SQLManager.ApprovedConfirmation(id);


                    MessageBox.Show("Record Save Successfully");
                    this.Close();
                }

            }
        }
}
}
