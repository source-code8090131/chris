﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_MakerChecker : Form
    {
        public CHRIS_Personnel_MakerChecker()
        {
            InitializeComponent();
        }

        public static DataRow SetdtRow;
        private void button2_Click(object sender, EventArgs e)
        {
            if(dtGVMakerChecker.SelectedRows.Count == 1 )
            {
                SetdtRow  = ((DataRowView)dtGVMakerChecker.SelectedRows[0].DataBoundItem).Row;
                Close();
            }
            else
            {
                MessageBox.Show("Please Select One Row At A Time.");
            }
           
        }

        private void CHRIS_Personnel_MakerChecker_Load(object sender, EventArgs e)
        {
            try
            {
                string userID = CHRIS_Personnel_SumInernsHiring.SetUserValue;
                DataTable dt = new DataTable();
                dt = SQLManager.sp_MakerChecker("CHRIS_get_FrmCall_Users", "CHRIS_Personnel_SumInernsHiring", userID).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dtGVMakerChecker.DataSource = dt;
                    dtGVMakerChecker.Update();
                    dtGVMakerChecker.Visible = true;
                    //btnReject.Visible = true;
                    //btnApproved.Visible = true;
                }
                else
                {
                    dtGVMakerChecker.Visible = false;
                    //btnReject.Visible = false;
                    //btnApproved.Visible = false;
                }

            }
            catch (Exception)
            {
                
            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtGVMakerChecker.SelectedRows)
                {
                    string id = row.Cells[0].Value.ToString();
                    int RowID = Convert.ToInt32(id);
                    SQLManager.sp_MC_DeleteRowData("CHRIS_Update_SUMMER_INTERNS_CHK", RowID);
                }
            }
            catch (Exception)
            {

            }

        }

        private void btnClosed_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
