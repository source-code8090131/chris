using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_TerminationHold : Form
    {

        #region Property
        private String _FlagValue;
        public String FlagValue
        {
            get
            {
                return _FlagValue;
            }

            set
            {
                _FlagValue = value;
            }
        }
        #endregion
        CHRIS_Personnel_TerminationSettHold _mainForm;

        #region Constructor
        public CHRIS_Personnel_TerminationHold(CHRIS_Personnel_TerminationSettHold mainForm)
        {
            InitializeComponent();
            _mainForm = mainForm;

        }
        #endregion

        #region Events
        private void txtflag_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.txtflag.Text == string.Empty)
                {
                    return;
                }
                bool returnValue = true;
                if (txtflag.Text == "R")
                {
                    returnValue = _mainForm.CheckforTransferType();
                    if (returnValue == false)
                    {

                        txtflag.Focus();
                        return;


                    }
                }
              
                this.FlagValue = txtflag.Text;
                this.Close();
            }

            if (e.KeyCode == Keys.F6)
            {
                this.Hide();
                _mainForm.mainFormQuit();
                this.Close();
                return;
            }
        }
        private void txtflag_TextChanged(object sender, EventArgs e)
        {
            if (this.txtflag.Text.Length > 0)
            {
                if (this.txtflag.Text != "H" && this.txtflag.Text != "R")
                {
                    MessageBox.Show("Enter a valid Selection.");

                }
               
            }
        }
        #endregion

        private void CHRIS_Personnel_TerminationHold_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.txtflag.Text.Length == 0)
            {
                    e.Cancel = true;
            }
        }

        private void CHRIS_Personnel_TerminationHold_KeyDown(object sender, KeyEventArgs e)
        {
           


        }

   }
}