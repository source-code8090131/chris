namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_LeavesScheduleEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_LeavesScheduleEntry));
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txt_W_PL_BAL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LC_P_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_PR_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_LSH_JAN1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OPTION_DIS = new CrplControlLibrary.SLTextBox(this.components);
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.pnlPerLeave = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.txt_LSH_DEC4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_DEC3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_DEC2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txt_LSH_DEC1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label73 = new System.Windows.Forms.Label();
            this.txt_LSH_NOV4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_NOV3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_NOV2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.txt_LSH_NOV1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label77 = new System.Windows.Forms.Label();
            this.txt_LSH_OCT4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_OCT3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_OCT2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.txt_LSH_OCT1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label81 = new System.Windows.Forms.Label();
            this.txt_LSH_SEP4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_SEP3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_SEP2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.txt_LSH_SEP1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label85 = new System.Windows.Forms.Label();
            this.txt_LSH_AUG4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_AUG3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_AUG2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.txt_LSH_AUG1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label89 = new System.Windows.Forms.Label();
            this.txt_LSH_JUL4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_JUL3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_JUL2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.txt_LSH_JUL1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label93 = new System.Windows.Forms.Label();
            this.txt_LSH_JUN4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_JUN3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_JUN2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txt_LSH_JUN1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txt_LSH_MAY4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_MAY3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_MAY2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txt_LSH_MAY1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.txt_LSH_APR4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_APR3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_APR2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_LSH_APR1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txt_LSH_MAR4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_MAR3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_MAR2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_LSH_MAR1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txt_LSH_FEB4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_FEB3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_FEB2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_LSH_FEB1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txt_LSH_JAN4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_JAN3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_LSH_JAN2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label35 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlPerLeave.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(629, 0);
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            this.txtOption.Validating += new System.ComponentModel.CancelEventHandler(this.txtOption_Validating);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(665, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 316);
            this.panel1.Size = new System.Drawing.Size(665, 60);
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PNo_Exist";
            this.lbtnPNo.ActionType = "PNo";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = resources.GetString("lbtnPNo.HiddenColumns");
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = false;
            this.lbtnPNo.Location = new System.Drawing.Point(110, 57);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER";
            this.lbtnPNo.TabIndex = 7;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txt_W_PL_BAL
            // 
            this.txt_W_PL_BAL.AllowSpace = true;
            this.txt_W_PL_BAL.AssociatedLookUpName = "";
            this.txt_W_PL_BAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_PL_BAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_PL_BAL.ContinuationTextBox = null;
            this.txt_W_PL_BAL.CustomEnabled = true;
            this.txt_W_PL_BAL.DataFieldMapping = "W_PL_BAL";
            this.txt_W_PL_BAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_PL_BAL.GetRecordsOnUpDownKeys = false;
            this.txt_W_PL_BAL.IsDate = false;
            this.txt_W_PL_BAL.Location = new System.Drawing.Point(58, 108);
            this.txt_W_PL_BAL.Name = "txt_W_PL_BAL";
            this.txt_W_PL_BAL.NumberFormat = "###,###,##0.00";
            this.txt_W_PL_BAL.Postfix = "";
            this.txt_W_PL_BAL.Prefix = "";
            this.txt_W_PL_BAL.ReadOnly = true;
            this.txt_W_PL_BAL.Size = new System.Drawing.Size(47, 20);
            this.txt_W_PL_BAL.SkipValidation = false;
            this.txt_W_PL_BAL.TabIndex = 6;
            this.txt_W_PL_BAL.TabStop = false;
            this.txt_W_PL_BAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_LC_P_NAME
            // 
            this.txt_LC_P_NAME.AllowSpace = true;
            this.txt_LC_P_NAME.AssociatedLookUpName = "";
            this.txt_LC_P_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LC_P_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LC_P_NAME.ContinuationTextBox = null;
            this.txt_LC_P_NAME.CustomEnabled = true;
            this.txt_LC_P_NAME.DataFieldMapping = "LC_P_NAME";
            this.txt_LC_P_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LC_P_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_LC_P_NAME.IsDate = false;
            this.txt_LC_P_NAME.Location = new System.Drawing.Point(58, 82);
            this.txt_LC_P_NAME.Name = "txt_LC_P_NAME";
            this.txt_LC_P_NAME.NumberFormat = "###,###,##0.00";
            this.txt_LC_P_NAME.Postfix = "";
            this.txt_LC_P_NAME.Prefix = "";
            this.txt_LC_P_NAME.ReadOnly = true;
            this.txt_LC_P_NAME.Size = new System.Drawing.Size(123, 20);
            this.txt_LC_P_NAME.SkipValidation = false;
            this.txt_LC_P_NAME.TabIndex = 5;
            this.txt_LC_P_NAME.TabStop = false;
            this.txt_LC_P_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_LSH_PR_NO
            // 
            this.txt_LSH_PR_NO.AllowSpace = true;
            this.txt_LSH_PR_NO.AssociatedLookUpName = "lbtnPNo";
            this.txt_LSH_PR_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_PR_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_PR_NO.ContinuationTextBox = null;
            this.txt_LSH_PR_NO.CustomEnabled = true;
            this.txt_LSH_PR_NO.DataFieldMapping = "LSH_PR_NO";
            this.txt_LSH_PR_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_PR_NO.GetRecordsOnUpDownKeys = true;
            this.txt_LSH_PR_NO.IsDate = false;
            this.txt_LSH_PR_NO.Location = new System.Drawing.Point(58, 57);
            this.txt_LSH_PR_NO.MaxLength = 6;
            this.txt_LSH_PR_NO.Name = "txt_LSH_PR_NO";
            this.txt_LSH_PR_NO.NumberFormat = "###,###,##0.00";
            this.txt_LSH_PR_NO.Postfix = "";
            this.txt_LSH_PR_NO.Prefix = "";
            this.txt_LSH_PR_NO.Size = new System.Drawing.Size(47, 20);
            this.txt_LSH_PR_NO.SkipValidation = false;
            this.txt_LSH_PR_NO.TabIndex = 0;
            this.txt_LSH_PR_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_LSH_PR_NO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_PR_NO_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pl. Bal. :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "P. No :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Personnel Information";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(597, 89);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(32, 13);
            this.label39.TabIndex = 101;
            this.label39.Text = "DEC";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(520, 89);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(33, 13);
            this.label44.TabIndex = 92;
            this.label44.Text = "NOV";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(445, 89);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(32, 13);
            this.label49.TabIndex = 83;
            this.label49.Text = "OCT";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(359, 89);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 74;
            this.label54.Text = "SEP";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(283, 89);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(33, 13);
            this.label59.TabIndex = 65;
            this.label59.Text = "AUG";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(213, 89);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(29, 13);
            this.label64.TabIndex = 56;
            this.label64.Text = "JUL";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(596, 21);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 13);
            this.label34.TabIndex = 47;
            this.label34.Text = "JUN";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(519, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(33, 13);
            this.label29.TabIndex = 38;
            this.label29.Text = "MAY";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(444, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 13);
            this.label24.TabIndex = 29;
            this.label24.Text = "APR";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(359, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "MAR";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(283, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "FEB";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(242, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "4";
            // 
            // txt_LSH_JAN1
            // 
            this.txt_LSH_JAN1.AllowSpace = true;
            this.txt_LSH_JAN1.AssociatedLookUpName = "";
            this.txt_LSH_JAN1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JAN1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JAN1.ContinuationTextBox = null;
            this.txt_LSH_JAN1.CustomEnabled = true;
            this.txt_LSH_JAN1.DataFieldMapping = "LSH_JAN1";
            this.txt_LSH_JAN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JAN1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JAN1.IsDate = false;
            this.txt_LSH_JAN1.Location = new System.Drawing.Point(190, 58);
            this.txt_LSH_JAN1.MaxLength = 1;
            this.txt_LSH_JAN1.Name = "txt_LSH_JAN1";
            this.txt_LSH_JAN1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JAN1.Postfix = "";
            this.txt_LSH_JAN1.Prefix = "";
            this.txt_LSH_JAN1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JAN1.SkipValidation = false;
            this.txt_LSH_JAN1.TabIndex = 1;
            this.txt_LSH_JAN1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JAN1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JAN1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JAN1_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(225, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(208, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(191, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(212, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "JAN";
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.panel9);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txt_W_OPTION_DIS);
            this.pnlHead.Controls.Add(this.label65);
            this.pnlHead.Controls.Add(this.label66);
            this.pnlHead.Controls.Add(this.label67);
            this.pnlHead.Controls.Add(this.label68);
            this.pnlHead.Location = new System.Drawing.Point(4, 80);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(658, 54);
            this.pnlHead.TabIndex = 12;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Location = new System.Drawing.Point(500, 54);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1, 168);
            this.panel9.TabIndex = 206;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Location = new System.Drawing.Point(0, -1);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1, 168);
            this.panel10.TabIndex = 205;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = false;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(564, 30);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(89, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OPTION_DIS
            // 
            this.txt_W_OPTION_DIS.AllowSpace = true;
            this.txt_W_OPTION_DIS.AssociatedLookUpName = "";
            this.txt_W_OPTION_DIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OPTION_DIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OPTION_DIS.ContinuationTextBox = null;
            this.txt_W_OPTION_DIS.CustomEnabled = false;
            this.txt_W_OPTION_DIS.DataFieldMapping = "";
            this.txt_W_OPTION_DIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OPTION_DIS.GetRecordsOnUpDownKeys = false;
            this.txt_W_OPTION_DIS.IsDate = false;
            this.txt_W_OPTION_DIS.Location = new System.Drawing.Point(564, 8);
            this.txt_W_OPTION_DIS.Name = "txt_W_OPTION_DIS";
            this.txt_W_OPTION_DIS.NumberFormat = "###,###,##0.00";
            this.txt_W_OPTION_DIS.Postfix = "";
            this.txt_W_OPTION_DIS.Prefix = "";
            this.txt_W_OPTION_DIS.ReadOnly = true;
            this.txt_W_OPTION_DIS.Size = new System.Drawing.Size(57, 20);
            this.txt_W_OPTION_DIS.SkipValidation = false;
            this.txt_W_OPTION_DIS.TabIndex = 8;
            this.txt_W_OPTION_DIS.TabStop = false;
            this.txt_W_OPTION_DIS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(522, 31);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(42, 13);
            this.label65.TabIndex = 5;
            this.label65.Text = "Date :";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(514, 8);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(52, 13);
            this.label66.TabIndex = 4;
            this.label66.Text = "Option :";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(213, 31);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(180, 13);
            this.label67.TabIndex = 3;
            this.label67.Text = "L E A V E S   S C H E D U L E";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(252, 8);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(107, 13);
            this.label68.TabIndex = 2;
            this.label68.Text = "Personnel System";
            // 
            // pnlPerLeave
            // 
            this.pnlPerLeave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPerLeave.ConcurrentPanels = null;
            this.pnlPerLeave.Controls.Add(this.panel13);
            this.pnlPerLeave.Controls.Add(this.panel11);
            this.pnlPerLeave.Controls.Add(this.panel7);
            this.pnlPerLeave.Controls.Add(this.panel6);
            this.pnlPerLeave.Controls.Add(this.panel5);
            this.pnlPerLeave.Controls.Add(this.panel2);
            this.pnlPerLeave.Controls.Add(this.txtID);
            this.pnlPerLeave.Controls.Add(this.panel4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_DEC4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_DEC3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_DEC2);
            this.pnlPerLeave.Controls.Add(this.label70);
            this.pnlPerLeave.Controls.Add(this.label71);
            this.pnlPerLeave.Controls.Add(this.label72);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_DEC1);
            this.pnlPerLeave.Controls.Add(this.label73);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_NOV4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_NOV3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_NOV2);
            this.pnlPerLeave.Controls.Add(this.label74);
            this.pnlPerLeave.Controls.Add(this.label75);
            this.pnlPerLeave.Controls.Add(this.label76);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_NOV1);
            this.pnlPerLeave.Controls.Add(this.label77);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_OCT4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_OCT3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_OCT2);
            this.pnlPerLeave.Controls.Add(this.label78);
            this.pnlPerLeave.Controls.Add(this.label79);
            this.pnlPerLeave.Controls.Add(this.label80);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_OCT1);
            this.pnlPerLeave.Controls.Add(this.label81);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_SEP4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_SEP3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_SEP2);
            this.pnlPerLeave.Controls.Add(this.label82);
            this.pnlPerLeave.Controls.Add(this.label83);
            this.pnlPerLeave.Controls.Add(this.label84);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_SEP1);
            this.pnlPerLeave.Controls.Add(this.label85);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_AUG4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_AUG3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_AUG2);
            this.pnlPerLeave.Controls.Add(this.label86);
            this.pnlPerLeave.Controls.Add(this.label87);
            this.pnlPerLeave.Controls.Add(this.label88);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_AUG1);
            this.pnlPerLeave.Controls.Add(this.label89);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUL4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUL3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUL2);
            this.pnlPerLeave.Controls.Add(this.label90);
            this.pnlPerLeave.Controls.Add(this.label91);
            this.pnlPerLeave.Controls.Add(this.label92);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUL1);
            this.pnlPerLeave.Controls.Add(this.label93);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUN4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUN3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUN2);
            this.pnlPerLeave.Controls.Add(this.label30);
            this.pnlPerLeave.Controls.Add(this.label31);
            this.pnlPerLeave.Controls.Add(this.label32);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JUN1);
            this.pnlPerLeave.Controls.Add(this.label33);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAY4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAY3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAY2);
            this.pnlPerLeave.Controls.Add(this.label25);
            this.pnlPerLeave.Controls.Add(this.label26);
            this.pnlPerLeave.Controls.Add(this.label27);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAY1);
            this.pnlPerLeave.Controls.Add(this.label28);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_APR4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_APR3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_APR2);
            this.pnlPerLeave.Controls.Add(this.label20);
            this.pnlPerLeave.Controls.Add(this.label21);
            this.pnlPerLeave.Controls.Add(this.label22);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_APR1);
            this.pnlPerLeave.Controls.Add(this.label23);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAR4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAR3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAR2);
            this.pnlPerLeave.Controls.Add(this.label15);
            this.pnlPerLeave.Controls.Add(this.label16);
            this.pnlPerLeave.Controls.Add(this.label17);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_MAR1);
            this.pnlPerLeave.Controls.Add(this.label18);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_FEB4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_FEB3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_FEB2);
            this.pnlPerLeave.Controls.Add(this.label10);
            this.pnlPerLeave.Controls.Add(this.label11);
            this.pnlPerLeave.Controls.Add(this.label12);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_FEB1);
            this.pnlPerLeave.Controls.Add(this.label13);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JAN4);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JAN3);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JAN2);
            this.pnlPerLeave.Controls.Add(this.lbtnPNo);
            this.pnlPerLeave.Controls.Add(this.label8);
            this.pnlPerLeave.Controls.Add(this.txt_W_PL_BAL);
            this.pnlPerLeave.Controls.Add(this.label7);
            this.pnlPerLeave.Controls.Add(this.label6);
            this.pnlPerLeave.Controls.Add(this.txt_LC_P_NAME);
            this.pnlPerLeave.Controls.Add(this.label5);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_JAN1);
            this.pnlPerLeave.Controls.Add(this.txt_LSH_PR_NO);
            this.pnlPerLeave.Controls.Add(this.label9);
            this.pnlPerLeave.Controls.Add(this.label4);
            this.pnlPerLeave.Controls.Add(this.label3);
            this.pnlPerLeave.Controls.Add(this.label14);
            this.pnlPerLeave.Controls.Add(this.label2);
            this.pnlPerLeave.Controls.Add(this.label1);
            this.pnlPerLeave.Controls.Add(this.label39);
            this.pnlPerLeave.Controls.Add(this.label19);
            this.pnlPerLeave.Controls.Add(this.label44);
            this.pnlPerLeave.Controls.Add(this.label24);
            this.pnlPerLeave.Controls.Add(this.label49);
            this.pnlPerLeave.Controls.Add(this.label29);
            this.pnlPerLeave.Controls.Add(this.label54);
            this.pnlPerLeave.Controls.Add(this.label34);
            this.pnlPerLeave.Controls.Add(this.label59);
            this.pnlPerLeave.Controls.Add(this.label64);
            this.pnlPerLeave.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPerLeave.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPerLeave.DependentPanels = null;
            this.pnlPerLeave.DisableDependentLoad = false;
            this.pnlPerLeave.EnableDelete = true;
            this.pnlPerLeave.EnableInsert = true;
            this.pnlPerLeave.EnableQuery = false;
            this.pnlPerLeave.EnableUpdate = true;
            this.pnlPerLeave.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LeaSchEntCommand";
            this.pnlPerLeave.Location = new System.Drawing.Point(4, 134);
            this.pnlPerLeave.MasterPanel = null;
            this.pnlPerLeave.Name = "pnlPerLeave";
            this.pnlPerLeave.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPerLeave.Size = new System.Drawing.Size(658, 163);
            this.pnlPerLeave.SPName = "CHRIS_SP_LeaSchEnt_LEAVE_SCHEDULE_MANAGER";
            this.pnlPerLeave.TabIndex = 13;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Location = new System.Drawing.Point(579, -2);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1, 168);
            this.panel13.TabIndex = 207;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Location = new System.Drawing.Point(0, -1);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1, 168);
            this.panel14.TabIndex = 205;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Location = new System.Drawing.Point(501, -2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1, 168);
            this.panel11.TabIndex = 206;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Location = new System.Drawing.Point(0, -1);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1, 168);
            this.panel12.TabIndex = 205;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Location = new System.Drawing.Point(421, -3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1, 168);
            this.panel7.TabIndex = 204;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Location = new System.Drawing.Point(0, -1);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1, 168);
            this.panel8.TabIndex = 205;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Location = new System.Drawing.Point(343, -1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1, 168);
            this.panel6.TabIndex = 204;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Location = new System.Drawing.Point(264, -3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1, 168);
            this.panel5.TabIndex = 204;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(-1, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(186, 1);
            this.panel2.TabIndex = 203;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(-1, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(176, 1);
            this.panel3.TabIndex = 204;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(58, 136);
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(47, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 200;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(185, -4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1, 168);
            this.panel4.TabIndex = 203;
            // 
            // txt_LSH_DEC4
            // 
            this.txt_LSH_DEC4.AllowSpace = true;
            this.txt_LSH_DEC4.AssociatedLookUpName = "";
            this.txt_LSH_DEC4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_DEC4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_DEC4.ContinuationTextBox = null;
            this.txt_LSH_DEC4.CustomEnabled = true;
            this.txt_LSH_DEC4.DataFieldMapping = "LSH_DEC4";
            this.txt_LSH_DEC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_DEC4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_DEC4.IsDate = false;
            this.txt_LSH_DEC4.Location = new System.Drawing.Point(637, 123);
            this.txt_LSH_DEC4.MaxLength = 1;
            this.txt_LSH_DEC4.Name = "txt_LSH_DEC4";
            this.txt_LSH_DEC4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_DEC4.Postfix = "";
            this.txt_LSH_DEC4.Prefix = "";
            this.txt_LSH_DEC4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_DEC4.SkipValidation = false;
            this.txt_LSH_DEC4.TabIndex = 48;
            this.txt_LSH_DEC4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_DEC4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_DEC4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_DEC4_Validating);
            // 
            // txt_LSH_DEC3
            // 
            this.txt_LSH_DEC3.AllowSpace = true;
            this.txt_LSH_DEC3.AssociatedLookUpName = "";
            this.txt_LSH_DEC3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_DEC3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_DEC3.ContinuationTextBox = null;
            this.txt_LSH_DEC3.CustomEnabled = true;
            this.txt_LSH_DEC3.DataFieldMapping = "LSH_DEC3";
            this.txt_LSH_DEC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_DEC3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_DEC3.IsDate = false;
            this.txt_LSH_DEC3.Location = new System.Drawing.Point(619, 123);
            this.txt_LSH_DEC3.MaxLength = 1;
            this.txt_LSH_DEC3.Name = "txt_LSH_DEC3";
            this.txt_LSH_DEC3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_DEC3.Postfix = "";
            this.txt_LSH_DEC3.Prefix = "";
            this.txt_LSH_DEC3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_DEC3.SkipValidation = false;
            this.txt_LSH_DEC3.TabIndex = 47;
            this.txt_LSH_DEC3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_DEC3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_DEC3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_DEC3_Validating);
            // 
            // txt_LSH_DEC2
            // 
            this.txt_LSH_DEC2.AllowSpace = true;
            this.txt_LSH_DEC2.AssociatedLookUpName = "";
            this.txt_LSH_DEC2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_DEC2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_DEC2.ContinuationTextBox = null;
            this.txt_LSH_DEC2.CustomEnabled = true;
            this.txt_LSH_DEC2.DataFieldMapping = "LSH_DEC2";
            this.txt_LSH_DEC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_DEC2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_DEC2.IsDate = false;
            this.txt_LSH_DEC2.Location = new System.Drawing.Point(601, 123);
            this.txt_LSH_DEC2.MaxLength = 1;
            this.txt_LSH_DEC2.Name = "txt_LSH_DEC2";
            this.txt_LSH_DEC2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_DEC2.Postfix = "";
            this.txt_LSH_DEC2.Prefix = "";
            this.txt_LSH_DEC2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_DEC2.SkipValidation = false;
            this.txt_LSH_DEC2.TabIndex = 46;
            this.txt_LSH_DEC2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_DEC2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_DEC2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_DEC2_Validating);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(584, 107);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(14, 13);
            this.label70.TabIndex = 196;
            this.label70.Text = "1";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(601, 107);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(14, 13);
            this.label71.TabIndex = 197;
            this.label71.Text = "2";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(618, 107);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(14, 13);
            this.label72.TabIndex = 198;
            this.label72.Text = "3";
            // 
            // txt_LSH_DEC1
            // 
            this.txt_LSH_DEC1.AllowSpace = true;
            this.txt_LSH_DEC1.AssociatedLookUpName = "";
            this.txt_LSH_DEC1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_DEC1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_DEC1.ContinuationTextBox = null;
            this.txt_LSH_DEC1.CustomEnabled = true;
            this.txt_LSH_DEC1.DataFieldMapping = "LSH_DEC1";
            this.txt_LSH_DEC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_DEC1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_DEC1.IsDate = false;
            this.txt_LSH_DEC1.Location = new System.Drawing.Point(583, 123);
            this.txt_LSH_DEC1.MaxLength = 1;
            this.txt_LSH_DEC1.Name = "txt_LSH_DEC1";
            this.txt_LSH_DEC1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_DEC1.Postfix = "";
            this.txt_LSH_DEC1.Prefix = "";
            this.txt_LSH_DEC1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_DEC1.SkipValidation = false;
            this.txt_LSH_DEC1.TabIndex = 45;
            this.txt_LSH_DEC1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_DEC1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_DEC1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_DEC1_Validating);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(635, 107);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(14, 13);
            this.label73.TabIndex = 199;
            this.label73.Text = "4";
            // 
            // txt_LSH_NOV4
            // 
            this.txt_LSH_NOV4.AllowSpace = true;
            this.txt_LSH_NOV4.AssociatedLookUpName = "";
            this.txt_LSH_NOV4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_NOV4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_NOV4.ContinuationTextBox = null;
            this.txt_LSH_NOV4.CustomEnabled = true;
            this.txt_LSH_NOV4.DataFieldMapping = "LSH_NOV4";
            this.txt_LSH_NOV4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_NOV4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_NOV4.IsDate = false;
            this.txt_LSH_NOV4.Location = new System.Drawing.Point(559, 123);
            this.txt_LSH_NOV4.MaxLength = 1;
            this.txt_LSH_NOV4.Name = "txt_LSH_NOV4";
            this.txt_LSH_NOV4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_NOV4.Postfix = "";
            this.txt_LSH_NOV4.Prefix = "";
            this.txt_LSH_NOV4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_NOV4.SkipValidation = false;
            this.txt_LSH_NOV4.TabIndex = 44;
            this.txt_LSH_NOV4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_NOV4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_NOV4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_NOV4_Validating);
            // 
            // txt_LSH_NOV3
            // 
            this.txt_LSH_NOV3.AllowSpace = true;
            this.txt_LSH_NOV3.AssociatedLookUpName = "";
            this.txt_LSH_NOV3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_NOV3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_NOV3.ContinuationTextBox = null;
            this.txt_LSH_NOV3.CustomEnabled = true;
            this.txt_LSH_NOV3.DataFieldMapping = "LSH_NOV3";
            this.txt_LSH_NOV3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_NOV3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_NOV3.IsDate = false;
            this.txt_LSH_NOV3.Location = new System.Drawing.Point(541, 123);
            this.txt_LSH_NOV3.MaxLength = 1;
            this.txt_LSH_NOV3.Name = "txt_LSH_NOV3";
            this.txt_LSH_NOV3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_NOV3.Postfix = "";
            this.txt_LSH_NOV3.Prefix = "";
            this.txt_LSH_NOV3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_NOV3.SkipValidation = false;
            this.txt_LSH_NOV3.TabIndex = 43;
            this.txt_LSH_NOV3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_NOV3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_NOV3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_NOV3_Validating);
            // 
            // txt_LSH_NOV2
            // 
            this.txt_LSH_NOV2.AllowSpace = true;
            this.txt_LSH_NOV2.AssociatedLookUpName = "";
            this.txt_LSH_NOV2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_NOV2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_NOV2.ContinuationTextBox = null;
            this.txt_LSH_NOV2.CustomEnabled = true;
            this.txt_LSH_NOV2.DataFieldMapping = "LSH_NOV2";
            this.txt_LSH_NOV2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_NOV2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_NOV2.IsDate = false;
            this.txt_LSH_NOV2.Location = new System.Drawing.Point(523, 123);
            this.txt_LSH_NOV2.MaxLength = 1;
            this.txt_LSH_NOV2.Name = "txt_LSH_NOV2";
            this.txt_LSH_NOV2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_NOV2.Postfix = "";
            this.txt_LSH_NOV2.Prefix = "";
            this.txt_LSH_NOV2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_NOV2.SkipValidation = false;
            this.txt_LSH_NOV2.TabIndex = 42;
            this.txt_LSH_NOV2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_NOV2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_NOV2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_NOV2_Validating);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(506, 107);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(14, 13);
            this.label74.TabIndex = 188;
            this.label74.Text = "1";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(523, 107);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(14, 13);
            this.label75.TabIndex = 189;
            this.label75.Text = "2";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(540, 107);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(14, 13);
            this.label76.TabIndex = 190;
            this.label76.Text = "3";
            // 
            // txt_LSH_NOV1
            // 
            this.txt_LSH_NOV1.AllowSpace = true;
            this.txt_LSH_NOV1.AssociatedLookUpName = "";
            this.txt_LSH_NOV1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_NOV1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_NOV1.ContinuationTextBox = null;
            this.txt_LSH_NOV1.CustomEnabled = true;
            this.txt_LSH_NOV1.DataFieldMapping = "LSH_NOV1";
            this.txt_LSH_NOV1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_NOV1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_NOV1.IsDate = false;
            this.txt_LSH_NOV1.Location = new System.Drawing.Point(505, 123);
            this.txt_LSH_NOV1.MaxLength = 1;
            this.txt_LSH_NOV1.Name = "txt_LSH_NOV1";
            this.txt_LSH_NOV1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_NOV1.Postfix = "";
            this.txt_LSH_NOV1.Prefix = "";
            this.txt_LSH_NOV1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_NOV1.SkipValidation = false;
            this.txt_LSH_NOV1.TabIndex = 41;
            this.txt_LSH_NOV1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_NOV1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_NOV1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_NOV1_Validating);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(557, 107);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(14, 13);
            this.label77.TabIndex = 191;
            this.label77.Text = "4";
            // 
            // txt_LSH_OCT4
            // 
            this.txt_LSH_OCT4.AllowSpace = true;
            this.txt_LSH_OCT4.AssociatedLookUpName = "";
            this.txt_LSH_OCT4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_OCT4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_OCT4.ContinuationTextBox = null;
            this.txt_LSH_OCT4.CustomEnabled = true;
            this.txt_LSH_OCT4.DataFieldMapping = "LSH_OCT4";
            this.txt_LSH_OCT4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_OCT4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_OCT4.IsDate = false;
            this.txt_LSH_OCT4.Location = new System.Drawing.Point(481, 123);
            this.txt_LSH_OCT4.MaxLength = 1;
            this.txt_LSH_OCT4.Name = "txt_LSH_OCT4";
            this.txt_LSH_OCT4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_OCT4.Postfix = "";
            this.txt_LSH_OCT4.Prefix = "";
            this.txt_LSH_OCT4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_OCT4.SkipValidation = false;
            this.txt_LSH_OCT4.TabIndex = 40;
            this.txt_LSH_OCT4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_OCT4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_OCT4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_OCT4_Validating);
            // 
            // txt_LSH_OCT3
            // 
            this.txt_LSH_OCT3.AllowSpace = true;
            this.txt_LSH_OCT3.AssociatedLookUpName = "";
            this.txt_LSH_OCT3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_OCT3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_OCT3.ContinuationTextBox = null;
            this.txt_LSH_OCT3.CustomEnabled = true;
            this.txt_LSH_OCT3.DataFieldMapping = "LSH_OCT3";
            this.txt_LSH_OCT3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_OCT3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_OCT3.IsDate = false;
            this.txt_LSH_OCT3.Location = new System.Drawing.Point(463, 123);
            this.txt_LSH_OCT3.MaxLength = 1;
            this.txt_LSH_OCT3.Name = "txt_LSH_OCT3";
            this.txt_LSH_OCT3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_OCT3.Postfix = "";
            this.txt_LSH_OCT3.Prefix = "";
            this.txt_LSH_OCT3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_OCT3.SkipValidation = false;
            this.txt_LSH_OCT3.TabIndex = 39;
            this.txt_LSH_OCT3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_OCT3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_OCT3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_OCT3_Validating);
            // 
            // txt_LSH_OCT2
            // 
            this.txt_LSH_OCT2.AllowSpace = true;
            this.txt_LSH_OCT2.AssociatedLookUpName = "";
            this.txt_LSH_OCT2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_OCT2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_OCT2.ContinuationTextBox = null;
            this.txt_LSH_OCT2.CustomEnabled = true;
            this.txt_LSH_OCT2.DataFieldMapping = "LSH_OCT2";
            this.txt_LSH_OCT2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_OCT2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_OCT2.IsDate = false;
            this.txt_LSH_OCT2.Location = new System.Drawing.Point(445, 123);
            this.txt_LSH_OCT2.MaxLength = 1;
            this.txt_LSH_OCT2.Name = "txt_LSH_OCT2";
            this.txt_LSH_OCT2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_OCT2.Postfix = "";
            this.txt_LSH_OCT2.Prefix = "";
            this.txt_LSH_OCT2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_OCT2.SkipValidation = false;
            this.txt_LSH_OCT2.TabIndex = 38;
            this.txt_LSH_OCT2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_OCT2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_OCT2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_OCT2_Validating);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(428, 107);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(14, 13);
            this.label78.TabIndex = 180;
            this.label78.Text = "1";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(445, 107);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(14, 13);
            this.label79.TabIndex = 181;
            this.label79.Text = "2";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(462, 107);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(14, 13);
            this.label80.TabIndex = 182;
            this.label80.Text = "3";
            // 
            // txt_LSH_OCT1
            // 
            this.txt_LSH_OCT1.AllowSpace = true;
            this.txt_LSH_OCT1.AssociatedLookUpName = "";
            this.txt_LSH_OCT1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_OCT1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_OCT1.ContinuationTextBox = null;
            this.txt_LSH_OCT1.CustomEnabled = true;
            this.txt_LSH_OCT1.DataFieldMapping = "LSH_OCT1";
            this.txt_LSH_OCT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_OCT1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_OCT1.IsDate = false;
            this.txt_LSH_OCT1.Location = new System.Drawing.Point(427, 123);
            this.txt_LSH_OCT1.MaxLength = 1;
            this.txt_LSH_OCT1.Name = "txt_LSH_OCT1";
            this.txt_LSH_OCT1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_OCT1.Postfix = "";
            this.txt_LSH_OCT1.Prefix = "";
            this.txt_LSH_OCT1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_OCT1.SkipValidation = false;
            this.txt_LSH_OCT1.TabIndex = 37;
            this.txt_LSH_OCT1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_OCT1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_OCT1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_OCT1_Validating);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(479, 107);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(14, 13);
            this.label81.TabIndex = 183;
            this.label81.Text = "4";
            // 
            // txt_LSH_SEP4
            // 
            this.txt_LSH_SEP4.AllowSpace = true;
            this.txt_LSH_SEP4.AssociatedLookUpName = "";
            this.txt_LSH_SEP4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_SEP4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_SEP4.ContinuationTextBox = null;
            this.txt_LSH_SEP4.CustomEnabled = true;
            this.txt_LSH_SEP4.DataFieldMapping = "LSH_SEP4";
            this.txt_LSH_SEP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_SEP4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_SEP4.IsDate = false;
            this.txt_LSH_SEP4.Location = new System.Drawing.Point(402, 123);
            this.txt_LSH_SEP4.MaxLength = 1;
            this.txt_LSH_SEP4.Name = "txt_LSH_SEP4";
            this.txt_LSH_SEP4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_SEP4.Postfix = "";
            this.txt_LSH_SEP4.Prefix = "";
            this.txt_LSH_SEP4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_SEP4.SkipValidation = false;
            this.txt_LSH_SEP4.TabIndex = 36;
            this.txt_LSH_SEP4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_SEP4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_SEP4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_SEP4_Validating);
            // 
            // txt_LSH_SEP3
            // 
            this.txt_LSH_SEP3.AllowSpace = true;
            this.txt_LSH_SEP3.AssociatedLookUpName = "";
            this.txt_LSH_SEP3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_SEP3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_SEP3.ContinuationTextBox = null;
            this.txt_LSH_SEP3.CustomEnabled = true;
            this.txt_LSH_SEP3.DataFieldMapping = "LSH_SEP3";
            this.txt_LSH_SEP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_SEP3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_SEP3.IsDate = false;
            this.txt_LSH_SEP3.Location = new System.Drawing.Point(384, 123);
            this.txt_LSH_SEP3.MaxLength = 1;
            this.txt_LSH_SEP3.Name = "txt_LSH_SEP3";
            this.txt_LSH_SEP3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_SEP3.Postfix = "";
            this.txt_LSH_SEP3.Prefix = "";
            this.txt_LSH_SEP3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_SEP3.SkipValidation = false;
            this.txt_LSH_SEP3.TabIndex = 35;
            this.txt_LSH_SEP3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_SEP3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_SEP3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_SEP3_Validating);
            // 
            // txt_LSH_SEP2
            // 
            this.txt_LSH_SEP2.AllowSpace = true;
            this.txt_LSH_SEP2.AssociatedLookUpName = "";
            this.txt_LSH_SEP2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_SEP2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_SEP2.ContinuationTextBox = null;
            this.txt_LSH_SEP2.CustomEnabled = true;
            this.txt_LSH_SEP2.DataFieldMapping = "LSH_SEP2";
            this.txt_LSH_SEP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_SEP2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_SEP2.IsDate = false;
            this.txt_LSH_SEP2.Location = new System.Drawing.Point(366, 123);
            this.txt_LSH_SEP2.MaxLength = 1;
            this.txt_LSH_SEP2.Name = "txt_LSH_SEP2";
            this.txt_LSH_SEP2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_SEP2.Postfix = "";
            this.txt_LSH_SEP2.Prefix = "";
            this.txt_LSH_SEP2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_SEP2.SkipValidation = false;
            this.txt_LSH_SEP2.TabIndex = 34;
            this.txt_LSH_SEP2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_SEP2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_SEP2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_SEP2_Validating);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(349, 107);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(14, 13);
            this.label82.TabIndex = 172;
            this.label82.Text = "1";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(366, 107);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(14, 13);
            this.label83.TabIndex = 173;
            this.label83.Text = "2";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(383, 107);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(14, 13);
            this.label84.TabIndex = 174;
            this.label84.Text = "3";
            // 
            // txt_LSH_SEP1
            // 
            this.txt_LSH_SEP1.AllowSpace = true;
            this.txt_LSH_SEP1.AssociatedLookUpName = "";
            this.txt_LSH_SEP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_SEP1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_SEP1.ContinuationTextBox = null;
            this.txt_LSH_SEP1.CustomEnabled = true;
            this.txt_LSH_SEP1.DataFieldMapping = "LSH_SEP1";
            this.txt_LSH_SEP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_SEP1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_SEP1.IsDate = false;
            this.txt_LSH_SEP1.Location = new System.Drawing.Point(348, 123);
            this.txt_LSH_SEP1.MaxLength = 1;
            this.txt_LSH_SEP1.Name = "txt_LSH_SEP1";
            this.txt_LSH_SEP1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_SEP1.Postfix = "";
            this.txt_LSH_SEP1.Prefix = "";
            this.txt_LSH_SEP1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_SEP1.SkipValidation = false;
            this.txt_LSH_SEP1.TabIndex = 33;
            this.txt_LSH_SEP1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_SEP1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_SEP1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_SEP1_Validating);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(400, 107);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(14, 13);
            this.label85.TabIndex = 175;
            this.label85.Text = "4";
            // 
            // txt_LSH_AUG4
            // 
            this.txt_LSH_AUG4.AllowSpace = true;
            this.txt_LSH_AUG4.AssociatedLookUpName = "";
            this.txt_LSH_AUG4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_AUG4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_AUG4.ContinuationTextBox = null;
            this.txt_LSH_AUG4.CustomEnabled = true;
            this.txt_LSH_AUG4.DataFieldMapping = "LSH_AUG4";
            this.txt_LSH_AUG4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_AUG4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_AUG4.IsDate = false;
            this.txt_LSH_AUG4.Location = new System.Drawing.Point(323, 123);
            this.txt_LSH_AUG4.MaxLength = 1;
            this.txt_LSH_AUG4.Name = "txt_LSH_AUG4";
            this.txt_LSH_AUG4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_AUG4.Postfix = "";
            this.txt_LSH_AUG4.Prefix = "";
            this.txt_LSH_AUG4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_AUG4.SkipValidation = false;
            this.txt_LSH_AUG4.TabIndex = 32;
            this.txt_LSH_AUG4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_AUG4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_AUG4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_AUG4_Validating);
            // 
            // txt_LSH_AUG3
            // 
            this.txt_LSH_AUG3.AllowSpace = true;
            this.txt_LSH_AUG3.AssociatedLookUpName = "";
            this.txt_LSH_AUG3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_AUG3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_AUG3.ContinuationTextBox = null;
            this.txt_LSH_AUG3.CustomEnabled = true;
            this.txt_LSH_AUG3.DataFieldMapping = "LSH_AUG3";
            this.txt_LSH_AUG3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_AUG3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_AUG3.IsDate = false;
            this.txt_LSH_AUG3.Location = new System.Drawing.Point(305, 123);
            this.txt_LSH_AUG3.MaxLength = 1;
            this.txt_LSH_AUG3.Name = "txt_LSH_AUG3";
            this.txt_LSH_AUG3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_AUG3.Postfix = "";
            this.txt_LSH_AUG3.Prefix = "";
            this.txt_LSH_AUG3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_AUG3.SkipValidation = false;
            this.txt_LSH_AUG3.TabIndex = 31;
            this.txt_LSH_AUG3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_AUG3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_AUG3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_AUG3_Validating);
            // 
            // txt_LSH_AUG2
            // 
            this.txt_LSH_AUG2.AllowSpace = true;
            this.txt_LSH_AUG2.AssociatedLookUpName = "";
            this.txt_LSH_AUG2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_AUG2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_AUG2.ContinuationTextBox = null;
            this.txt_LSH_AUG2.CustomEnabled = true;
            this.txt_LSH_AUG2.DataFieldMapping = "LSH_AUG2";
            this.txt_LSH_AUG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_AUG2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_AUG2.IsDate = false;
            this.txt_LSH_AUG2.Location = new System.Drawing.Point(287, 123);
            this.txt_LSH_AUG2.MaxLength = 1;
            this.txt_LSH_AUG2.Name = "txt_LSH_AUG2";
            this.txt_LSH_AUG2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_AUG2.Postfix = "";
            this.txt_LSH_AUG2.Prefix = "";
            this.txt_LSH_AUG2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_AUG2.SkipValidation = false;
            this.txt_LSH_AUG2.TabIndex = 30;
            this.txt_LSH_AUG2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_AUG2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_AUG2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_AUG2_Validating);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(270, 107);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(14, 13);
            this.label86.TabIndex = 164;
            this.label86.Text = "1";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(287, 107);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(14, 13);
            this.label87.TabIndex = 165;
            this.label87.Text = "2";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(304, 107);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(14, 13);
            this.label88.TabIndex = 166;
            this.label88.Text = "3";
            // 
            // txt_LSH_AUG1
            // 
            this.txt_LSH_AUG1.AllowSpace = true;
            this.txt_LSH_AUG1.AssociatedLookUpName = "";
            this.txt_LSH_AUG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_AUG1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_AUG1.ContinuationTextBox = null;
            this.txt_LSH_AUG1.CustomEnabled = true;
            this.txt_LSH_AUG1.DataFieldMapping = "LSH_AUG1";
            this.txt_LSH_AUG1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_AUG1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_AUG1.IsDate = false;
            this.txt_LSH_AUG1.Location = new System.Drawing.Point(269, 123);
            this.txt_LSH_AUG1.MaxLength = 1;
            this.txt_LSH_AUG1.Name = "txt_LSH_AUG1";
            this.txt_LSH_AUG1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_AUG1.Postfix = "";
            this.txt_LSH_AUG1.Prefix = "";
            this.txt_LSH_AUG1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_AUG1.SkipValidation = false;
            this.txt_LSH_AUG1.TabIndex = 29;
            this.txt_LSH_AUG1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_AUG1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_AUG1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_AUG1_Validating);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(321, 107);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(14, 13);
            this.label89.TabIndex = 167;
            this.label89.Text = "4";
            // 
            // txt_LSH_JUL4
            // 
            this.txt_LSH_JUL4.AllowSpace = true;
            this.txt_LSH_JUL4.AssociatedLookUpName = "";
            this.txt_LSH_JUL4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUL4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUL4.ContinuationTextBox = null;
            this.txt_LSH_JUL4.CustomEnabled = true;
            this.txt_LSH_JUL4.DataFieldMapping = "LSH_JUL4";
            this.txt_LSH_JUL4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUL4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUL4.IsDate = false;
            this.txt_LSH_JUL4.Location = new System.Drawing.Point(244, 123);
            this.txt_LSH_JUL4.MaxLength = 1;
            this.txt_LSH_JUL4.Name = "txt_LSH_JUL4";
            this.txt_LSH_JUL4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUL4.Postfix = "";
            this.txt_LSH_JUL4.Prefix = "";
            this.txt_LSH_JUL4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUL4.SkipValidation = false;
            this.txt_LSH_JUL4.TabIndex = 28;
            this.txt_LSH_JUL4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUL4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUL4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUL4_Validating);
            // 
            // txt_LSH_JUL3
            // 
            this.txt_LSH_JUL3.AllowSpace = true;
            this.txt_LSH_JUL3.AssociatedLookUpName = "";
            this.txt_LSH_JUL3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUL3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUL3.ContinuationTextBox = null;
            this.txt_LSH_JUL3.CustomEnabled = true;
            this.txt_LSH_JUL3.DataFieldMapping = "LSH_JUL3";
            this.txt_LSH_JUL3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUL3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUL3.IsDate = false;
            this.txt_LSH_JUL3.Location = new System.Drawing.Point(226, 123);
            this.txt_LSH_JUL3.MaxLength = 1;
            this.txt_LSH_JUL3.Name = "txt_LSH_JUL3";
            this.txt_LSH_JUL3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUL3.Postfix = "";
            this.txt_LSH_JUL3.Prefix = "";
            this.txt_LSH_JUL3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUL3.SkipValidation = false;
            this.txt_LSH_JUL3.TabIndex = 27;
            this.txt_LSH_JUL3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUL3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUL3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUL3_Validating);
            // 
            // txt_LSH_JUL2
            // 
            this.txt_LSH_JUL2.AllowSpace = true;
            this.txt_LSH_JUL2.AssociatedLookUpName = "";
            this.txt_LSH_JUL2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUL2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUL2.ContinuationTextBox = null;
            this.txt_LSH_JUL2.CustomEnabled = true;
            this.txt_LSH_JUL2.DataFieldMapping = "LSH_JUL2";
            this.txt_LSH_JUL2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUL2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUL2.IsDate = false;
            this.txt_LSH_JUL2.Location = new System.Drawing.Point(208, 123);
            this.txt_LSH_JUL2.MaxLength = 1;
            this.txt_LSH_JUL2.Name = "txt_LSH_JUL2";
            this.txt_LSH_JUL2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUL2.Postfix = "";
            this.txt_LSH_JUL2.Prefix = "";
            this.txt_LSH_JUL2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUL2.SkipValidation = false;
            this.txt_LSH_JUL2.TabIndex = 26;
            this.txt_LSH_JUL2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUL2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUL2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUL2_Validating);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(191, 107);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(14, 13);
            this.label90.TabIndex = 155;
            this.label90.Text = "1";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(208, 107);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(14, 13);
            this.label91.TabIndex = 157;
            this.label91.Text = "2";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(225, 107);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(14, 13);
            this.label92.TabIndex = 158;
            this.label92.Text = "3";
            // 
            // txt_LSH_JUL1
            // 
            this.txt_LSH_JUL1.AllowSpace = true;
            this.txt_LSH_JUL1.AssociatedLookUpName = "";
            this.txt_LSH_JUL1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUL1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUL1.ContinuationTextBox = null;
            this.txt_LSH_JUL1.CustomEnabled = true;
            this.txt_LSH_JUL1.DataFieldMapping = "LSH_JUL1";
            this.txt_LSH_JUL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUL1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUL1.IsDate = false;
            this.txt_LSH_JUL1.Location = new System.Drawing.Point(190, 123);
            this.txt_LSH_JUL1.MaxLength = 1;
            this.txt_LSH_JUL1.Name = "txt_LSH_JUL1";
            this.txt_LSH_JUL1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUL1.Postfix = "";
            this.txt_LSH_JUL1.Prefix = "";
            this.txt_LSH_JUL1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUL1.SkipValidation = false;
            this.txt_LSH_JUL1.TabIndex = 25;
            this.txt_LSH_JUL1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUL1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUL1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUL1_Validating);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(242, 107);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(14, 13);
            this.label93.TabIndex = 159;
            this.label93.Text = "4";
            // 
            // txt_LSH_JUN4
            // 
            this.txt_LSH_JUN4.AllowSpace = true;
            this.txt_LSH_JUN4.AssociatedLookUpName = "";
            this.txt_LSH_JUN4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUN4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUN4.ContinuationTextBox = null;
            this.txt_LSH_JUN4.CustomEnabled = true;
            this.txt_LSH_JUN4.DataFieldMapping = "LSH_JUN4";
            this.txt_LSH_JUN4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUN4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUN4.IsDate = false;
            this.txt_LSH_JUN4.Location = new System.Drawing.Point(637, 58);
            this.txt_LSH_JUN4.MaxLength = 1;
            this.txt_LSH_JUN4.Name = "txt_LSH_JUN4";
            this.txt_LSH_JUN4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUN4.Postfix = "";
            this.txt_LSH_JUN4.Prefix = "";
            this.txt_LSH_JUN4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUN4.SkipValidation = false;
            this.txt_LSH_JUN4.TabIndex = 24;
            this.txt_LSH_JUN4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUN4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUN4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUN4_Validating);
            // 
            // txt_LSH_JUN3
            // 
            this.txt_LSH_JUN3.AllowSpace = true;
            this.txt_LSH_JUN3.AssociatedLookUpName = "";
            this.txt_LSH_JUN3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUN3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUN3.ContinuationTextBox = null;
            this.txt_LSH_JUN3.CustomEnabled = true;
            this.txt_LSH_JUN3.DataFieldMapping = "LSH_JUN3";
            this.txt_LSH_JUN3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUN3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUN3.IsDate = false;
            this.txt_LSH_JUN3.Location = new System.Drawing.Point(619, 58);
            this.txt_LSH_JUN3.MaxLength = 1;
            this.txt_LSH_JUN3.Name = "txt_LSH_JUN3";
            this.txt_LSH_JUN3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUN3.Postfix = "";
            this.txt_LSH_JUN3.Prefix = "";
            this.txt_LSH_JUN3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUN3.SkipValidation = false;
            this.txt_LSH_JUN3.TabIndex = 23;
            this.txt_LSH_JUN3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUN3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUN3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUN3_Validating);
            // 
            // txt_LSH_JUN2
            // 
            this.txt_LSH_JUN2.AllowSpace = true;
            this.txt_LSH_JUN2.AssociatedLookUpName = "";
            this.txt_LSH_JUN2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUN2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUN2.ContinuationTextBox = null;
            this.txt_LSH_JUN2.CustomEnabled = true;
            this.txt_LSH_JUN2.DataFieldMapping = "LSH_JUN2";
            this.txt_LSH_JUN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUN2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUN2.IsDate = false;
            this.txt_LSH_JUN2.Location = new System.Drawing.Point(601, 58);
            this.txt_LSH_JUN2.MaxLength = 1;
            this.txt_LSH_JUN2.Name = "txt_LSH_JUN2";
            this.txt_LSH_JUN2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUN2.Postfix = "";
            this.txt_LSH_JUN2.Prefix = "";
            this.txt_LSH_JUN2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUN2.SkipValidation = false;
            this.txt_LSH_JUN2.TabIndex = 22;
            this.txt_LSH_JUN2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUN2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUN2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUN2_Validating);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(584, 42);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 13);
            this.label30.TabIndex = 148;
            this.label30.Text = "1";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(601, 42);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 13);
            this.label31.TabIndex = 149;
            this.label31.Text = "2";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(618, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 150;
            this.label32.Text = "3";
            // 
            // txt_LSH_JUN1
            // 
            this.txt_LSH_JUN1.AllowSpace = true;
            this.txt_LSH_JUN1.AssociatedLookUpName = "";
            this.txt_LSH_JUN1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JUN1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JUN1.ContinuationTextBox = null;
            this.txt_LSH_JUN1.CustomEnabled = true;
            this.txt_LSH_JUN1.DataFieldMapping = "LSH_JUN1";
            this.txt_LSH_JUN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JUN1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JUN1.IsDate = false;
            this.txt_LSH_JUN1.Location = new System.Drawing.Point(583, 58);
            this.txt_LSH_JUN1.MaxLength = 1;
            this.txt_LSH_JUN1.Name = "txt_LSH_JUN1";
            this.txt_LSH_JUN1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JUN1.Postfix = "";
            this.txt_LSH_JUN1.Prefix = "";
            this.txt_LSH_JUN1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JUN1.SkipValidation = false;
            this.txt_LSH_JUN1.TabIndex = 21;
            this.txt_LSH_JUN1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JUN1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JUN1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JUN1_Validating);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(635, 42);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(14, 13);
            this.label33.TabIndex = 151;
            this.label33.Text = "4";
            // 
            // txt_LSH_MAY4
            // 
            this.txt_LSH_MAY4.AllowSpace = true;
            this.txt_LSH_MAY4.AssociatedLookUpName = "";
            this.txt_LSH_MAY4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAY4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAY4.ContinuationTextBox = null;
            this.txt_LSH_MAY4.CustomEnabled = true;
            this.txt_LSH_MAY4.DataFieldMapping = "LSH_MAY4";
            this.txt_LSH_MAY4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAY4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAY4.IsDate = false;
            this.txt_LSH_MAY4.Location = new System.Drawing.Point(559, 58);
            this.txt_LSH_MAY4.MaxLength = 1;
            this.txt_LSH_MAY4.Name = "txt_LSH_MAY4";
            this.txt_LSH_MAY4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAY4.Postfix = "";
            this.txt_LSH_MAY4.Prefix = "";
            this.txt_LSH_MAY4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAY4.SkipValidation = false;
            this.txt_LSH_MAY4.TabIndex = 20;
            this.txt_LSH_MAY4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAY4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAY4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAY4_Validating);
            // 
            // txt_LSH_MAY3
            // 
            this.txt_LSH_MAY3.AllowSpace = true;
            this.txt_LSH_MAY3.AssociatedLookUpName = "";
            this.txt_LSH_MAY3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAY3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAY3.ContinuationTextBox = null;
            this.txt_LSH_MAY3.CustomEnabled = true;
            this.txt_LSH_MAY3.DataFieldMapping = "LSH_MAY3";
            this.txt_LSH_MAY3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAY3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAY3.IsDate = false;
            this.txt_LSH_MAY3.Location = new System.Drawing.Point(541, 58);
            this.txt_LSH_MAY3.MaxLength = 1;
            this.txt_LSH_MAY3.Name = "txt_LSH_MAY3";
            this.txt_LSH_MAY3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAY3.Postfix = "";
            this.txt_LSH_MAY3.Prefix = "";
            this.txt_LSH_MAY3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAY3.SkipValidation = false;
            this.txt_LSH_MAY3.TabIndex = 19;
            this.txt_LSH_MAY3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAY3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAY3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAY3_Validating);
            // 
            // txt_LSH_MAY2
            // 
            this.txt_LSH_MAY2.AllowSpace = true;
            this.txt_LSH_MAY2.AssociatedLookUpName = "";
            this.txt_LSH_MAY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAY2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAY2.ContinuationTextBox = null;
            this.txt_LSH_MAY2.CustomEnabled = true;
            this.txt_LSH_MAY2.DataFieldMapping = "LSH_MAY2";
            this.txt_LSH_MAY2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAY2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAY2.IsDate = false;
            this.txt_LSH_MAY2.Location = new System.Drawing.Point(523, 58);
            this.txt_LSH_MAY2.MaxLength = 1;
            this.txt_LSH_MAY2.Name = "txt_LSH_MAY2";
            this.txt_LSH_MAY2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAY2.Postfix = "";
            this.txt_LSH_MAY2.Prefix = "";
            this.txt_LSH_MAY2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAY2.SkipValidation = false;
            this.txt_LSH_MAY2.TabIndex = 18;
            this.txt_LSH_MAY2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAY2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAY2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAY2_Validating);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(506, 42);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 13);
            this.label25.TabIndex = 140;
            this.label25.Text = "1";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(523, 42);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 13);
            this.label26.TabIndex = 141;
            this.label26.Text = "2";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(540, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 13);
            this.label27.TabIndex = 142;
            this.label27.Text = "3";
            // 
            // txt_LSH_MAY1
            // 
            this.txt_LSH_MAY1.AllowSpace = true;
            this.txt_LSH_MAY1.AssociatedLookUpName = "";
            this.txt_LSH_MAY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAY1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAY1.ContinuationTextBox = null;
            this.txt_LSH_MAY1.CustomEnabled = true;
            this.txt_LSH_MAY1.DataFieldMapping = "LSH_MAY1";
            this.txt_LSH_MAY1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAY1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAY1.IsDate = false;
            this.txt_LSH_MAY1.Location = new System.Drawing.Point(505, 58);
            this.txt_LSH_MAY1.MaxLength = 1;
            this.txt_LSH_MAY1.Name = "txt_LSH_MAY1";
            this.txt_LSH_MAY1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAY1.Postfix = "";
            this.txt_LSH_MAY1.Prefix = "";
            this.txt_LSH_MAY1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAY1.SkipValidation = false;
            this.txt_LSH_MAY1.TabIndex = 17;
            this.txt_LSH_MAY1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAY1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAY1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAY1_Validating);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(557, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 13);
            this.label28.TabIndex = 143;
            this.label28.Text = "4";
            // 
            // txt_LSH_APR4
            // 
            this.txt_LSH_APR4.AllowSpace = true;
            this.txt_LSH_APR4.AssociatedLookUpName = "";
            this.txt_LSH_APR4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_APR4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_APR4.ContinuationTextBox = null;
            this.txt_LSH_APR4.CustomEnabled = true;
            this.txt_LSH_APR4.DataFieldMapping = "LSH_APR4";
            this.txt_LSH_APR4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_APR4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_APR4.IsDate = false;
            this.txt_LSH_APR4.Location = new System.Drawing.Point(481, 58);
            this.txt_LSH_APR4.MaxLength = 1;
            this.txt_LSH_APR4.Name = "txt_LSH_APR4";
            this.txt_LSH_APR4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_APR4.Postfix = "";
            this.txt_LSH_APR4.Prefix = "";
            this.txt_LSH_APR4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_APR4.SkipValidation = false;
            this.txt_LSH_APR4.TabIndex = 16;
            this.txt_LSH_APR4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_APR4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_APR4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_APR4_Validating);
            // 
            // txt_LSH_APR3
            // 
            this.txt_LSH_APR3.AllowSpace = true;
            this.txt_LSH_APR3.AssociatedLookUpName = "";
            this.txt_LSH_APR3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_APR3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_APR3.ContinuationTextBox = null;
            this.txt_LSH_APR3.CustomEnabled = true;
            this.txt_LSH_APR3.DataFieldMapping = "LSH_APR3";
            this.txt_LSH_APR3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_APR3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_APR3.IsDate = false;
            this.txt_LSH_APR3.Location = new System.Drawing.Point(463, 58);
            this.txt_LSH_APR3.MaxLength = 1;
            this.txt_LSH_APR3.Name = "txt_LSH_APR3";
            this.txt_LSH_APR3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_APR3.Postfix = "";
            this.txt_LSH_APR3.Prefix = "";
            this.txt_LSH_APR3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_APR3.SkipValidation = false;
            this.txt_LSH_APR3.TabIndex = 15;
            this.txt_LSH_APR3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_APR3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_APR3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_APR3_Validating);
            // 
            // txt_LSH_APR2
            // 
            this.txt_LSH_APR2.AllowSpace = true;
            this.txt_LSH_APR2.AssociatedLookUpName = "";
            this.txt_LSH_APR2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_APR2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_APR2.ContinuationTextBox = null;
            this.txt_LSH_APR2.CustomEnabled = true;
            this.txt_LSH_APR2.DataFieldMapping = "LSH_APR2";
            this.txt_LSH_APR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_APR2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_APR2.IsDate = false;
            this.txt_LSH_APR2.Location = new System.Drawing.Point(445, 58);
            this.txt_LSH_APR2.MaxLength = 1;
            this.txt_LSH_APR2.Name = "txt_LSH_APR2";
            this.txt_LSH_APR2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_APR2.Postfix = "";
            this.txt_LSH_APR2.Prefix = "";
            this.txt_LSH_APR2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_APR2.SkipValidation = false;
            this.txt_LSH_APR2.TabIndex = 14;
            this.txt_LSH_APR2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_APR2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_APR2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_APR2_Validating);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(428, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 13);
            this.label20.TabIndex = 132;
            this.label20.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(445, 42);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 13);
            this.label21.TabIndex = 133;
            this.label21.Text = "2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(462, 42);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 134;
            this.label22.Text = "3";
            // 
            // txt_LSH_APR1
            // 
            this.txt_LSH_APR1.AllowSpace = true;
            this.txt_LSH_APR1.AssociatedLookUpName = "";
            this.txt_LSH_APR1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_APR1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_APR1.ContinuationTextBox = null;
            this.txt_LSH_APR1.CustomEnabled = true;
            this.txt_LSH_APR1.DataFieldMapping = "LSH_APR1";
            this.txt_LSH_APR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_APR1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_APR1.IsDate = false;
            this.txt_LSH_APR1.Location = new System.Drawing.Point(427, 58);
            this.txt_LSH_APR1.MaxLength = 1;
            this.txt_LSH_APR1.Name = "txt_LSH_APR1";
            this.txt_LSH_APR1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_APR1.Postfix = "";
            this.txt_LSH_APR1.Prefix = "";
            this.txt_LSH_APR1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_APR1.SkipValidation = false;
            this.txt_LSH_APR1.TabIndex = 13;
            this.txt_LSH_APR1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_APR1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_APR1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_APR1_Validating);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(479, 42);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 13);
            this.label23.TabIndex = 135;
            this.label23.Text = "4";
            // 
            // txt_LSH_MAR4
            // 
            this.txt_LSH_MAR4.AllowSpace = true;
            this.txt_LSH_MAR4.AssociatedLookUpName = "";
            this.txt_LSH_MAR4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAR4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAR4.ContinuationTextBox = null;
            this.txt_LSH_MAR4.CustomEnabled = true;
            this.txt_LSH_MAR4.DataFieldMapping = "LSH_MAR4";
            this.txt_LSH_MAR4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAR4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAR4.IsDate = false;
            this.txt_LSH_MAR4.Location = new System.Drawing.Point(402, 58);
            this.txt_LSH_MAR4.MaxLength = 1;
            this.txt_LSH_MAR4.Name = "txt_LSH_MAR4";
            this.txt_LSH_MAR4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAR4.Postfix = "";
            this.txt_LSH_MAR4.Prefix = "";
            this.txt_LSH_MAR4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAR4.SkipValidation = false;
            this.txt_LSH_MAR4.TabIndex = 12;
            this.txt_LSH_MAR4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAR4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAR4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAR4_Validating);
            // 
            // txt_LSH_MAR3
            // 
            this.txt_LSH_MAR3.AllowSpace = true;
            this.txt_LSH_MAR3.AssociatedLookUpName = "";
            this.txt_LSH_MAR3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAR3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAR3.ContinuationTextBox = null;
            this.txt_LSH_MAR3.CustomEnabled = true;
            this.txt_LSH_MAR3.DataFieldMapping = "LSH_MAR3";
            this.txt_LSH_MAR3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAR3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAR3.IsDate = false;
            this.txt_LSH_MAR3.Location = new System.Drawing.Point(384, 58);
            this.txt_LSH_MAR3.MaxLength = 1;
            this.txt_LSH_MAR3.Name = "txt_LSH_MAR3";
            this.txt_LSH_MAR3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAR3.Postfix = "";
            this.txt_LSH_MAR3.Prefix = "";
            this.txt_LSH_MAR3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAR3.SkipValidation = false;
            this.txt_LSH_MAR3.TabIndex = 11;
            this.txt_LSH_MAR3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAR3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAR3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAR3_Validating);
            // 
            // txt_LSH_MAR2
            // 
            this.txt_LSH_MAR2.AllowSpace = true;
            this.txt_LSH_MAR2.AssociatedLookUpName = "";
            this.txt_LSH_MAR2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAR2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAR2.ContinuationTextBox = null;
            this.txt_LSH_MAR2.CustomEnabled = true;
            this.txt_LSH_MAR2.DataFieldMapping = "LSH_MAR2";
            this.txt_LSH_MAR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAR2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAR2.IsDate = false;
            this.txt_LSH_MAR2.Location = new System.Drawing.Point(366, 58);
            this.txt_LSH_MAR2.MaxLength = 1;
            this.txt_LSH_MAR2.Name = "txt_LSH_MAR2";
            this.txt_LSH_MAR2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAR2.Postfix = "";
            this.txt_LSH_MAR2.Prefix = "";
            this.txt_LSH_MAR2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAR2.SkipValidation = false;
            this.txt_LSH_MAR2.TabIndex = 10;
            this.txt_LSH_MAR2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAR2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAR2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAR2_Validating);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(349, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 124;
            this.label15.Text = "1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(366, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 125;
            this.label16.Text = "2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(383, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 13);
            this.label17.TabIndex = 126;
            this.label17.Text = "3";
            // 
            // txt_LSH_MAR1
            // 
            this.txt_LSH_MAR1.AllowSpace = true;
            this.txt_LSH_MAR1.AssociatedLookUpName = "";
            this.txt_LSH_MAR1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_MAR1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_MAR1.ContinuationTextBox = null;
            this.txt_LSH_MAR1.CustomEnabled = true;
            this.txt_LSH_MAR1.DataFieldMapping = "LSH_MAR1";
            this.txt_LSH_MAR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_MAR1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_MAR1.IsDate = false;
            this.txt_LSH_MAR1.Location = new System.Drawing.Point(348, 58);
            this.txt_LSH_MAR1.MaxLength = 1;
            this.txt_LSH_MAR1.Name = "txt_LSH_MAR1";
            this.txt_LSH_MAR1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_MAR1.Postfix = "";
            this.txt_LSH_MAR1.Prefix = "";
            this.txt_LSH_MAR1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_MAR1.SkipValidation = false;
            this.txt_LSH_MAR1.TabIndex = 9;
            this.txt_LSH_MAR1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_MAR1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_MAR1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_MAR1_Validating);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(400, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 13);
            this.label18.TabIndex = 127;
            this.label18.Text = "4";
            // 
            // txt_LSH_FEB4
            // 
            this.txt_LSH_FEB4.AllowSpace = true;
            this.txt_LSH_FEB4.AssociatedLookUpName = "";
            this.txt_LSH_FEB4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_FEB4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_FEB4.ContinuationTextBox = null;
            this.txt_LSH_FEB4.CustomEnabled = true;
            this.txt_LSH_FEB4.DataFieldMapping = "LSH_FEB4";
            this.txt_LSH_FEB4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_FEB4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_FEB4.IsDate = false;
            this.txt_LSH_FEB4.Location = new System.Drawing.Point(323, 58);
            this.txt_LSH_FEB4.MaxLength = 1;
            this.txt_LSH_FEB4.Name = "txt_LSH_FEB4";
            this.txt_LSH_FEB4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_FEB4.Postfix = "";
            this.txt_LSH_FEB4.Prefix = "";
            this.txt_LSH_FEB4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_FEB4.SkipValidation = false;
            this.txt_LSH_FEB4.TabIndex = 8;
            this.txt_LSH_FEB4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_FEB4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_FEB4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_FEB4_Validating);
            // 
            // txt_LSH_FEB3
            // 
            this.txt_LSH_FEB3.AllowSpace = true;
            this.txt_LSH_FEB3.AssociatedLookUpName = "";
            this.txt_LSH_FEB3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_FEB3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_FEB3.ContinuationTextBox = null;
            this.txt_LSH_FEB3.CustomEnabled = true;
            this.txt_LSH_FEB3.DataFieldMapping = "LSH_FEB3";
            this.txt_LSH_FEB3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_FEB3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_FEB3.IsDate = false;
            this.txt_LSH_FEB3.Location = new System.Drawing.Point(305, 58);
            this.txt_LSH_FEB3.MaxLength = 1;
            this.txt_LSH_FEB3.Name = "txt_LSH_FEB3";
            this.txt_LSH_FEB3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_FEB3.Postfix = "";
            this.txt_LSH_FEB3.Prefix = "";
            this.txt_LSH_FEB3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_FEB3.SkipValidation = false;
            this.txt_LSH_FEB3.TabIndex = 7;
            this.txt_LSH_FEB3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_FEB3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_FEB3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_FEB3_Validating);
            // 
            // txt_LSH_FEB2
            // 
            this.txt_LSH_FEB2.AllowSpace = true;
            this.txt_LSH_FEB2.AssociatedLookUpName = "";
            this.txt_LSH_FEB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_FEB2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_FEB2.ContinuationTextBox = null;
            this.txt_LSH_FEB2.CustomEnabled = true;
            this.txt_LSH_FEB2.DataFieldMapping = "LSH_FEB2";
            this.txt_LSH_FEB2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_FEB2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_FEB2.IsDate = false;
            this.txt_LSH_FEB2.Location = new System.Drawing.Point(287, 58);
            this.txt_LSH_FEB2.MaxLength = 1;
            this.txt_LSH_FEB2.Name = "txt_LSH_FEB2";
            this.txt_LSH_FEB2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_FEB2.Postfix = "";
            this.txt_LSH_FEB2.Prefix = "";
            this.txt_LSH_FEB2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_FEB2.SkipValidation = false;
            this.txt_LSH_FEB2.TabIndex = 6;
            this.txt_LSH_FEB2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_FEB2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_FEB2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_FEB2_Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(270, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 13);
            this.label10.TabIndex = 116;
            this.label10.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(287, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 13);
            this.label11.TabIndex = 117;
            this.label11.Text = "2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(304, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 118;
            this.label12.Text = "3";
            // 
            // txt_LSH_FEB1
            // 
            this.txt_LSH_FEB1.AllowSpace = true;
            this.txt_LSH_FEB1.AssociatedLookUpName = "";
            this.txt_LSH_FEB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_FEB1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_FEB1.ContinuationTextBox = null;
            this.txt_LSH_FEB1.CustomEnabled = true;
            this.txt_LSH_FEB1.DataFieldMapping = "LSH_FEB1";
            this.txt_LSH_FEB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_FEB1.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_FEB1.IsDate = false;
            this.txt_LSH_FEB1.Location = new System.Drawing.Point(269, 58);
            this.txt_LSH_FEB1.MaxLength = 1;
            this.txt_LSH_FEB1.Name = "txt_LSH_FEB1";
            this.txt_LSH_FEB1.NumberFormat = "###,###,##0.00";
            this.txt_LSH_FEB1.Postfix = "";
            this.txt_LSH_FEB1.Prefix = "";
            this.txt_LSH_FEB1.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_FEB1.SkipValidation = false;
            this.txt_LSH_FEB1.TabIndex = 5;
            this.txt_LSH_FEB1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_FEB1, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_FEB1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_FEB1_Validating);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(321, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 119;
            this.label13.Text = "4";
            // 
            // txt_LSH_JAN4
            // 
            this.txt_LSH_JAN4.AllowSpace = true;
            this.txt_LSH_JAN4.AssociatedLookUpName = "";
            this.txt_LSH_JAN4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JAN4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JAN4.ContinuationTextBox = null;
            this.txt_LSH_JAN4.CustomEnabled = true;
            this.txt_LSH_JAN4.DataFieldMapping = "LSH_JAN4";
            this.txt_LSH_JAN4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JAN4.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JAN4.IsDate = false;
            this.txt_LSH_JAN4.Location = new System.Drawing.Point(244, 58);
            this.txt_LSH_JAN4.MaxLength = 1;
            this.txt_LSH_JAN4.Name = "txt_LSH_JAN4";
            this.txt_LSH_JAN4.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JAN4.Postfix = "";
            this.txt_LSH_JAN4.Prefix = "";
            this.txt_LSH_JAN4.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JAN4.SkipValidation = false;
            this.txt_LSH_JAN4.TabIndex = 4;
            this.txt_LSH_JAN4.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JAN4, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JAN4.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JAN4_Validating);
            // 
            // txt_LSH_JAN3
            // 
            this.txt_LSH_JAN3.AllowSpace = true;
            this.txt_LSH_JAN3.AssociatedLookUpName = "";
            this.txt_LSH_JAN3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JAN3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JAN3.ContinuationTextBox = null;
            this.txt_LSH_JAN3.CustomEnabled = true;
            this.txt_LSH_JAN3.DataFieldMapping = "LSH_JAN3";
            this.txt_LSH_JAN3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JAN3.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JAN3.IsDate = false;
            this.txt_LSH_JAN3.Location = new System.Drawing.Point(226, 58);
            this.txt_LSH_JAN3.MaxLength = 1;
            this.txt_LSH_JAN3.Name = "txt_LSH_JAN3";
            this.txt_LSH_JAN3.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JAN3.Postfix = "";
            this.txt_LSH_JAN3.Prefix = "";
            this.txt_LSH_JAN3.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JAN3.SkipValidation = false;
            this.txt_LSH_JAN3.TabIndex = 3;
            this.txt_LSH_JAN3.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JAN3, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JAN3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JAN3_Validating);
            // 
            // txt_LSH_JAN2
            // 
            this.txt_LSH_JAN2.AllowSpace = true;
            this.txt_LSH_JAN2.AssociatedLookUpName = "";
            this.txt_LSH_JAN2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_LSH_JAN2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_LSH_JAN2.ContinuationTextBox = null;
            this.txt_LSH_JAN2.CustomEnabled = true;
            this.txt_LSH_JAN2.DataFieldMapping = "LSH_JAN2";
            this.txt_LSH_JAN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LSH_JAN2.GetRecordsOnUpDownKeys = false;
            this.txt_LSH_JAN2.IsDate = false;
            this.txt_LSH_JAN2.Location = new System.Drawing.Point(208, 58);
            this.txt_LSH_JAN2.MaxLength = 1;
            this.txt_LSH_JAN2.Name = "txt_LSH_JAN2";
            this.txt_LSH_JAN2.NumberFormat = "###,###,##0.00";
            this.txt_LSH_JAN2.Postfix = "";
            this.txt_LSH_JAN2.Prefix = "";
            this.txt_LSH_JAN2.Size = new System.Drawing.Size(16, 20);
            this.txt_LSH_JAN2.SkipValidation = false;
            this.txt_LSH_JAN2.TabIndex = 2;
            this.txt_LSH_JAN2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_LSH_JAN2, "Enter \"X\" To Mark Leaves OR \'E\' To Exit...");
            this.txt_LSH_JAN2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_LSH_JAN2_Validating);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(454, 9);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 13);
            this.label35.TabIndex = 201;
            this.label35.Text = "User Name :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(518, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(40, 13);
            this.lblUserName.TabIndex = 202;
            this.lblUserName.Text = "P. No :";
            // 
            // CHRIS_Personnel_LeavesScheduleEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(665, 376);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.pnlPerLeave);
            this.Controls.Add(this.pnlHead);
            this.Name = "CHRIS_Personnel_LeavesScheduleEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_LeavesScheduleEntry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_LeavesScheduleEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlPerLeave, 0);
            this.Controls.SetChildIndex(this.label35, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.pnlPerLeave.ResumeLayout(false);
            this.pnlPerLeave.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txt_W_PL_BAL;
        private CrplControlLibrary.SLTextBox txt_LC_P_NAME;
        private CrplControlLibrary.SLTextBox txt_LSH_PR_NO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txt_LSH_JAN1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label64;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Panel pnlHead;
        public CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txt_W_OPTION_DIS;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPerLeave;
        private CrplControlLibrary.SLTextBox txt_LSH_JAN4;
        private CrplControlLibrary.SLTextBox txt_LSH_JAN3;
        private CrplControlLibrary.SLTextBox txt_LSH_JAN2;
        private CrplControlLibrary.SLTextBox txt_LSH_JUN4;
        private CrplControlLibrary.SLTextBox txt_LSH_JUN3;
        private CrplControlLibrary.SLTextBox txt_LSH_JUN2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private CrplControlLibrary.SLTextBox txt_LSH_JUN1;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txt_LSH_MAY4;
        private CrplControlLibrary.SLTextBox txt_LSH_MAY3;
        private CrplControlLibrary.SLTextBox txt_LSH_MAY2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox txt_LSH_MAY1;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLTextBox txt_LSH_APR4;
        private CrplControlLibrary.SLTextBox txt_LSH_APR3;
        private CrplControlLibrary.SLTextBox txt_LSH_APR2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txt_LSH_APR1;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txt_LSH_MAR4;
        private CrplControlLibrary.SLTextBox txt_LSH_MAR3;
        private CrplControlLibrary.SLTextBox txt_LSH_MAR2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txt_LSH_MAR1;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txt_LSH_FEB4;
        private CrplControlLibrary.SLTextBox txt_LSH_FEB3;
        private CrplControlLibrary.SLTextBox txt_LSH_FEB2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txt_LSH_FEB1;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txt_LSH_DEC4;
        private CrplControlLibrary.SLTextBox txt_LSH_DEC3;
        private CrplControlLibrary.SLTextBox txt_LSH_DEC2;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private CrplControlLibrary.SLTextBox txt_LSH_DEC1;
        private System.Windows.Forms.Label label73;
        private CrplControlLibrary.SLTextBox txt_LSH_NOV4;
        private CrplControlLibrary.SLTextBox txt_LSH_NOV3;
        private CrplControlLibrary.SLTextBox txt_LSH_NOV2;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private CrplControlLibrary.SLTextBox txt_LSH_NOV1;
        private System.Windows.Forms.Label label77;
        private CrplControlLibrary.SLTextBox txt_LSH_OCT4;
        private CrplControlLibrary.SLTextBox txt_LSH_OCT3;
        private CrplControlLibrary.SLTextBox txt_LSH_OCT2;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private CrplControlLibrary.SLTextBox txt_LSH_OCT1;
        private System.Windows.Forms.Label label81;
        private CrplControlLibrary.SLTextBox txt_LSH_SEP4;
        private CrplControlLibrary.SLTextBox txt_LSH_SEP3;
        private CrplControlLibrary.SLTextBox txt_LSH_SEP2;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private CrplControlLibrary.SLTextBox txt_LSH_SEP1;
        private System.Windows.Forms.Label label85;
        private CrplControlLibrary.SLTextBox txt_LSH_AUG4;
        private CrplControlLibrary.SLTextBox txt_LSH_AUG3;
        private CrplControlLibrary.SLTextBox txt_LSH_AUG2;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private CrplControlLibrary.SLTextBox txt_LSH_AUG1;
        private System.Windows.Forms.Label label89;
        private CrplControlLibrary.SLTextBox txt_LSH_JUL4;
        private CrplControlLibrary.SLTextBox txt_LSH_JUL3;
        private CrplControlLibrary.SLTextBox txt_LSH_JUL2;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private CrplControlLibrary.SLTextBox txt_LSH_JUL1;
        private System.Windows.Forms.Label label93;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;

    }
}