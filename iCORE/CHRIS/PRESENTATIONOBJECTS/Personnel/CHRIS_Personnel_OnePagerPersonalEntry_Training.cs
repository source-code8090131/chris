using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_OnePagerPersonalEntry_Training : ChrisTabularForm
    {

        #region Declarations

        private CHRIS_Personnel_OnePagerPersonalEntry _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Training frm_Training;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Education frm_Education;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_CitiRef frm_CitiRef;

        string pr_SegmentValue = string.Empty;
        private DataTable dtPreEmp;
        private DataTable dtEdu;
        private DataTable dtTraining;

        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> objVals = new Dictionary<string, object>();

        #endregion

        #region Constructor
        public CHRIS_Personnel_OnePagerPersonalEntry_Training()
        {
            InitializeComponent();
            this.ShowBottomBar.Equals(false);
         
        }

        public CHRIS_Personnel_OnePagerPersonalEntry_Training(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_OnePagerPersonalEntry mainForm, DataTable _dtPreEmp, DataTable _dtEdu, DataTable _dtTraining)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtPreEmp = _dtPreEmp;
            dtEdu = _dtEdu;
            dtTraining = _dtTraining;
            this._mainForm = mainForm;
            dgvTraining.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

        }
        #endregion

        #region Methods
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            this.txtOption.Visible = false;
            this.tbtAdd.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtCancel.Visible = false;
            this.tlbMain.Visible = false;

            dgvTraining.GridSource = dtTraining;
            dgvTraining.DataSource = dgvTraining.GridSource;


            this.KeyPreview = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);

        }
        #endregion

        #region Events

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Modifiers == Keys.Control)
                {
                    switch (e.KeyValue)
                    {
                        case 34:
                            //Ctl+PageDown  "Open CitiRef form"
                            frm_CitiRef = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_CitiRef(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, dtPreEmp,dtEdu,dtTraining);
                            this.Hide();
                            frm_CitiRef.MdiParent = null;
                            frm_CitiRef.Enabled = true;
                            frm_CitiRef.Select();
                            frm_CitiRef.Focus();
                            frm_CitiRef.ShowDialog(this);
                            this.KeyPreview = true;
                            break;

                        case 33:
                            //Ctl+PageUp  "Hide this form and open Education form"
                            this.Hide();
                            frm_Education = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_OnePagerPersonalEntry_Education(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this._mainForm.MdiParent)), this.connbean, this._mainForm, dtPreEmp, dtEdu, dtTraining);
                            //frm_Education = new CHRIS_Personnel_OnePagerPersonalEntry_Education();
                            frm_Education.ShowDialog(this);
                            this.KeyPreview = true;
                            break;

                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "ShortCutKey_Press", exp);
            }
        }

        private void dgvTraining_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (e.ColumnIndex == 0)
            {
                if ( dgvTraining.CurrentRow.Cells[1].EditedFormattedValue.ToString() != null || dgvTraining.CurrentRow.Cells[2].EditedFormattedValue.ToString() != null  )
                {

                    if (dgvTraining.CurrentRow.Cells[0].EditedFormattedValue.ToString() == "" || dgvTraining.CurrentRow.Cells[0].EditedFormattedValue.ToString() == null)
                    {
                        MessageBox.Show("VALUE MUST BE ENETRED");
                        e.Cancel = true;
                    }
                }
            }





        }

        #endregion

    }
}