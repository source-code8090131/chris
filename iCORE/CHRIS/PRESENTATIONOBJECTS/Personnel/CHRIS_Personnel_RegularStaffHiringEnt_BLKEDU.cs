using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU : TabularForm
    {
        #region --Variable--
        private DataTable dtDept;
        private DataTable dtEmp;
        private DataTable dtRef;
        private DataTable dtChild;
        private DataTable dtEdu;
        bool ValidateGrid       = true;
        TextBox txt             = new TextBox();
        string lblViewPopUP     = "[N]ext P. No.     Next [P]age         [O]ptions";
        string lblSavePopUP     = "Do You Want To Save The Above Information [Y/N]";
        string lblDeletePopUP   = "Do You Want To Delete The Above Information [Y/N]";
        private CHRIS_Personnel_RegularStaffHiringEnt _mainForm = null;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS frm_Pers;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frm_Page2;
        #endregion

        #region --Constructor--
        public CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU()
        {
            InitializeComponent();
        }


        public CHRIS_Personnel_RegularStaffHiringEnt_BLKEDU(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Personnel_RegularStaffHiringEnt mainForm,
            CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS BLKPERS, DataTable _dtDept, DataTable _dtEmp, DataTable _dtRef, DataTable _dtChild, DataTable _dtEdu)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtDept  = _dtDept;
            dtEmp   = _dtEmp;
            dtRef   = _dtRef;
            dtChild = _dtChild;
            dtEdu   = _dtEdu;
            frm_Pers = BLKPERS;
            this._mainForm  = mainForm;
            
        }
        #endregion

        #region --Method--

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible     = false;
                this.tbtList.Visible    = false;
                this.tbtSave.Visible    = false;
                this.tbtCancel.Visible  = false;
                this.tlbMain.Visible    = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        /// <summary>
        /// Load the grid with the DataTable coming from MAIN From.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                if(frm_Pers != null)
                    frm_Pers.Hide();
                

                dgvBlkEdu.GridSource    = dtEdu;
                dgvBlkEdu.DataSource    = dgvBlkEdu.GridSource;
                dgvBlkEdu.CurrentCell   = dgvBlkEdu[0, 0];
                this.KeyPreview         = true;
                this.KeyDown            += new System.Windows.Forms.KeyEventHandler(this.ShortCutKey_Press);
                dgvBlkEdu.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

     
    #endregion

        #region --Events--
        /// <summary>
        /// On Ctrl+Down/Ctrl+UP open new POPUPs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
            try
            {
                if (_mainForm.txtOption.Text == "A")
                {
                    if (ValidateGrid && !dgvBlkEdu.CurrentCell.IsInEditMode)
                    {
                        if (e.Modifiers == Keys.Control)
                        {
                            switch (e.KeyValue)
                            {
                                case 34:
                                    if (_mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "M")
                                    {
                                        #region Save Record
                                        this.Hide();

                                        DialogResult dr = MessageBox.Show("Do You Want To Save The Above Information [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                        if (dr == DialogResult.Yes)
                                        {
                                            _mainForm.CallSave("Y");
                                        }
                                        else
                                        {
                                            _mainForm.CallSave("N");
                                        }
                                        #endregion
                                    }
                                    else if (_mainForm.txtOption.Text == "V")
                                    {
                                        #region View Record
                                        this.Hide();
                                        frm_Page2 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblViewPopUP);
                                        frm_Page2.TextBox.MaxLength = 1;
                                        frm_Page2.TextBox.Validating += new CancelEventHandler(TextBox_Validating);
                                        frm_Page2.TextBox.Validated += new EventHandler(TextBox_Validated);
                                        frm_Page2.MdiParent = null;
                                        frm_Page2.ShowDialog(this);
                                        txt.Text = frm_Page2.Value;

                                        if (txt.Text == "N")
                                        {
                                            _mainForm.txt_PR_P_NO.Select();
                                            _mainForm.txt_PR_P_NO.Focus();
                                            
                                        }
                                        else if (txt.Text == "O")
                                        {
                                            _mainForm.txtOption.Select();
                                            _mainForm.txtOption.Focus();
                                            
                                        }
                                        #endregion
                                    }
                                    else if (_mainForm.txtOption.Text == "D")
                                    {
                                        #region Delete Record
                                        this.Hide();

                                        DialogResult dr = MessageBox.Show("Do You Want To Delete The Above Information [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                        if (dr == DialogResult.Yes)
                                        {
                                            _mainForm.CallDelete("Y");
                                        }
                                        else
                                        {
                                            _mainForm.CallDelete("N");
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        this.Hide();
                                    }

                                    this.KeyPreview = true;
                                    break;

                                case 33:

                                    if (_mainForm.txtOption.Text == "V")
                                    {
                                        frm_Pers            = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this, null, dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                        frm_Pers.MdiParent  = null;
                                        frm_Pers.Enabled    = true;
                                        this.Hide();
                                        frm_Pers.ShowDialog(this);
                                    }
                                    else if (_mainForm.txtOption.Text == "M" || _mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "D")
                                    {
                                        //frm_Pers.txt_PR_MARITAL.Text = "S";

                                        frm_Pers = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this, null,dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                        frm_Pers.MdiParent = null;
                                        frm_Pers.txt_PR_USER_IS_PRIME.Select();
                                        frm_Pers.txt_PR_USER_IS_PRIME.Focus();
                                        
                                        this.Hide();
                                        frm_Pers.ShowDialog(this);

                                    }
                                    //this.Close();

                                    this.KeyPreview = true;
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    if (e.Modifiers == Keys.Control)
                    {
                        switch (e.KeyValue)
                        {
                            case 34:
                                if (_mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "M")
                                {
                                    #region Save Record
                                    this.Hide();
                                    DialogResult dr = MessageBox.Show("Do You Want To Save The Above Information [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                    if (dr == DialogResult.Yes)
                                        _mainForm.CallSave("Y");
                                    //if (_mainForm.txtOption.Text == "M")
                                    //{
                                    //    _mainForm.txtOption.Select();
                                    //    _mainForm.txtOption.Focus();
                                    //    _mainForm.NewTxtOption = "N";
                                    //}
                                    else
                                        _mainForm.CallSave("N");
                                    #endregion
                                }
                                else if (_mainForm.txtOption.Text == "V")
                                {
                                    #region View Record
                                    this.Hide();
                                    frm_Page2                   = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblViewPopUP);
                                    frm_Page2.TextBox.MaxLength = 1;
                                    frm_Page2.TextBox.Validating+= new CancelEventHandler(TextBox_Validating);
                                    frm_Page2.TextBox.Validated += new EventHandler(TextBox_Validated);
                                    frm_Page2.MdiParent         = null;
                                    frm_Page2.ShowDialog(this);
                                    frm_Page2.Enabled           = true;
                                    txt.Text                    = frm_Page2.Value;
                                    _mainForm.option            = txt.Text;

                                    if (txt.Text == "N")
                                    {
                                        _mainForm.txt_PR_P_NO.Select();
                                        _mainForm.txt_PR_P_NO.Focus();
                                        _mainForm.NewTxtOption = "N";
                                    }
                                    else if (txt.Text == "O")
                                    {
                                        _mainForm.txtOption.Select();
                                        _mainForm.txtOption.Focus();
                                        _mainForm.NewTxtOption = "O";
                                    }
                                    #endregion
                                }
                                else if (_mainForm.txtOption.Text == "D")
                                {
                                    #region Delete Record
                                    this.Hide();

                                    DialogResult dr = MessageBox.Show("Do You Want To Delete The Above Information [Y/N]", "Form", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                    if (dr == DialogResult.Yes)
                                    {
                                        _mainForm.CallDelete("Y");
                                    }
                                    else
                                    {
                                        _mainForm.CallDelete("N");
                                    }
                                    #endregion
                                }
                                else
                                    this.Hide();

                                this.KeyPreview = true;
                                break;

                            case 33:

                                if (_mainForm.txtOption.Text == "V")
                                {
                                    frm_Pers = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm ,this ,null , dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Pers.MdiParent = null;
                                    this.Hide();
                                    frm_Pers.ShowDialog(this);
                                }
                                else if (_mainForm.txtOption.Text == "M" || _mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "D")
                                {
                                    frm_Pers = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_BLKPERS(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, _mainForm, this, null, dtDept, dtEmp, dtRef, dtChild, dtEdu);
                                    frm_Pers.MdiParent = null;
                                    frm_Pers.txt_PR_USER_IS_PRIME.Select();
                                    frm_Pers.txt_PR_USER_IS_PRIME.Focus();
                                    this.Hide();
                                    frm_Pers.ShowDialog(this);
                                }
                                this.KeyPreview = true;
                                break;
                        }
                    }
                }
                
            }
            catch (Exception exp)
            {
                LogException(this.Name, "ShortCutKey_Press", exp);
            }
            }

        /// <summary>
        /// PopUP TextBox Text Validating Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)sender;
                if (_mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "M")
                {
                    if (txt.Text != "Y" && txt.Text != "N")
                    {
                        e.Cancel = true;
                    }
                }
                else if (_mainForm.txtOption.Text == "D")
                {
                    if (txt.Text != "Y" && txt.Text != "N")
                    {
                        e.Cancel = true;
                    }
                }
                else if (_mainForm.txtOption.Text == "V")
                {
                    if (txt.Text != "N" && txt.Text != "P" && txt.Text != "O")
                    {
                        e.Cancel = true;
                    }
                }

                //frm_Page2.Hide();

            }
            catch (Exception exp)
            {
                LogError(this.Name, "TextBox_Leave", exp);
            }
        }

        ///<summary>
        ///Handler: which call the relative method in MAin Form.
        ///</summary>
        ///<param name="sender"></param>
        ///<param name="e"></param>
        void TextBox_Validated(object sender, EventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)sender;
                //if (_mainForm.txtOption.Text == "A" || _mainForm.txtOption.Text == "M")
                //{
                //    frm_Page2.Hide();
                //    _mainForm.CallSave(txt.Text);
                //}
                //else if(_mainForm.txtOption.Text == "D" )
                //{
                //    frm_Page2.Hide();
                //    _mainForm.CallDelete(txt.Text);
                //}
                //else
                if (_mainForm.txtOption.Text == "V")
                {
                    frm_Page2.Hide();
                    _mainForm.CallView(txt.Text);

                    if (txt.Text == "O")
                    {
                        _mainForm.txtOption.Select();
                        _mainForm.txtOption.Focus();
                    }
                    else if (txt.Text == "P")
                    {
                        _mainForm.txt_PR_P_NO.Select();
                        _mainForm.txt_PR_P_NO.Focus();
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "TextBox_Leave", exp);
            }
        }

        /// <summary>
        /// Cell Validation of DataGridView Cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkEdu_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                dgvBlkEdu.EndEdit();
                ValidateGrid = true;

                if (dgvBlkEdu.Rows.Count > 0 && dgvBlkEdu != null)
                {
                    //if (dgvBlkEdu.CurrentCell.IsInEditMode)
                    {
                        if (dgvBlkEdu.CurrentCell.OwningColumn.Name == "PrEYear")
                        {
                            if (dgvBlkEdu.CurrentCell.EditedFormattedValue != string.Empty)
                            {
                                bool NumberOnly = IsValidExpression(dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString(), InputValidator.NUMBER_REGEX);

                                if (!NumberOnly)
                                {
                                    ValidateGrid = false;
                                    e.Cancel = true;
                                }
                            }
                            //if (dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                            //{
                            //    e.Cancel = true;
                            //}
                        }
                        else if (dgvBlkEdu.CurrentCell.OwningColumn.Name == "Pr_Degree")//&& dgvBlkEdu.CurrentCell.EditedFormattedValue != null)
                        {
                            if (dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }
                        }
                        else if (dgvBlkEdu.CurrentCell.OwningColumn.Name == "Pr_Grade")//&& dgvBlkEdu.CurrentCell.EditedFormattedValue != null)
                        {
                            if (dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }
                        }
                        else if (dgvBlkEdu.CurrentCell.OwningColumn.Name == "Pr_College")//&& dgvBlkEdu.CurrentCell.EditedFormattedValue != null)
                        {
                            if (dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }
                        }
                        else if (dgvBlkEdu.CurrentCell.OwningColumn.Name == "Pr_City")//&& dgvBlkEdu.CurrentCell.EditedFormattedValue != null)
                        {
                            if (dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }
                        }
                        else if (dgvBlkEdu.CurrentCell.OwningColumn.Name == "Pr_Country")//&& dgvBlkEdu.CurrentCell.EditedFormattedValue != null)
                        {
                            if (dgvBlkEdu.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                            {
                                ValidateGrid = false;
                                e.Cancel = true;
                            }
                        }
                    }
                }
                else
                {
                    ValidateGrid = false;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "dgvBlkEdu_CellValidating", exp);
            }
        }

        /// <summary>
        /// Set CharacterCasing Upper
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkEdu_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        #endregion
        /// <summary>
        /// Set the Grid Edit Mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBlkEdu_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvBlkEdu.BeginEdit(true);
        }
    }
}