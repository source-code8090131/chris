namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_ShadowSalary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_ShadowSalary));
            this.PnlShadowSalary = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbRank = new CrplControlLibrary.LookupButton(this.components);
            this.dtPromotion = new System.Windows.Forms.DateTimePicker();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.LbLastDt = new CrplControlLibrary.LookupButton(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lbPr_p_no = new CrplControlLibrary.LookupButton(this.components);
            this.DtLastInc = new CrplControlLibrary.SLDatePicker(this.components);
            this.DtTransferDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtRankingDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.DtIncrement = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtRemarks = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncrementPerRs = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRanking = new CrplControlLibrary.SLTextBox(this.components);
            this.txtReviseShadowSal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocalTIRPercent = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnualShadowSal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRevisePackgeDolar = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncrementPercent = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncremntType = new CrplControlLibrary.SLTextBox(this.components);
            this.TxtRankinginHC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnualPackage = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIs_TirPercent = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtcountry = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCity = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstname = new CrplControlLibrary.SLTextBox(this.components);
            this.txtShs_pr_No = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlShadowSalary.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(543, 0);
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Location = new System.Drawing.Point(19, 0);
            this.pnlBottom.Size = new System.Drawing.Size(579, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 580);
            this.panel1.Size = new System.Drawing.Size(620, 73);
            // 
            // PnlShadowSalary
            // 
            this.PnlShadowSalary.ConcurrentPanels = null;
            this.PnlShadowSalary.Controls.Add(this.lbRank);
            this.PnlShadowSalary.Controls.Add(this.dtPromotion);
            this.PnlShadowSalary.Controls.Add(this.label51);
            this.PnlShadowSalary.Controls.Add(this.label50);
            this.PnlShadowSalary.Controls.Add(this.label49);
            this.PnlShadowSalary.Controls.Add(this.label45);
            this.PnlShadowSalary.Controls.Add(this.label44);
            this.PnlShadowSalary.Controls.Add(this.label43);
            this.PnlShadowSalary.Controls.Add(this.label42);
            this.PnlShadowSalary.Controls.Add(this.label48);
            this.PnlShadowSalary.Controls.Add(this.label47);
            this.PnlShadowSalary.Controls.Add(this.label46);
            this.PnlShadowSalary.Controls.Add(this.label41);
            this.PnlShadowSalary.Controls.Add(this.label40);
            this.PnlShadowSalary.Controls.Add(this.label39);
            this.PnlShadowSalary.Controls.Add(this.label38);
            this.PnlShadowSalary.Controls.Add(this.label37);
            this.PnlShadowSalary.Controls.Add(this.label36);
            this.PnlShadowSalary.Controls.Add(this.label35);
            this.PnlShadowSalary.Controls.Add(this.LbLastDt);
            this.PnlShadowSalary.Controls.Add(this.label34);
            this.PnlShadowSalary.Controls.Add(this.label32);
            this.PnlShadowSalary.Controls.Add(this.label31);
            this.PnlShadowSalary.Controls.Add(this.lbPr_p_no);
            this.PnlShadowSalary.Controls.Add(this.DtLastInc);
            this.PnlShadowSalary.Controls.Add(this.DtTransferDate);
            this.PnlShadowSalary.Controls.Add(this.label23);
            this.PnlShadowSalary.Controls.Add(this.label22);
            this.PnlShadowSalary.Controls.Add(this.label21);
            this.PnlShadowSalary.Controls.Add(this.label20);
            this.PnlShadowSalary.Controls.Add(this.label19);
            this.PnlShadowSalary.Controls.Add(this.label18);
            this.PnlShadowSalary.Controls.Add(this.label17);
            this.PnlShadowSalary.Controls.Add(this.label16);
            this.PnlShadowSalary.Controls.Add(this.label15);
            this.PnlShadowSalary.Controls.Add(this.label14);
            this.PnlShadowSalary.Controls.Add(this.label13);
            this.PnlShadowSalary.Controls.Add(this.label12);
            this.PnlShadowSalary.Controls.Add(this.label11);
            this.PnlShadowSalary.Controls.Add(this.label10);
            this.PnlShadowSalary.Controls.Add(this.label9);
            this.PnlShadowSalary.Controls.Add(this.label8);
            this.PnlShadowSalary.Controls.Add(this.label7);
            this.PnlShadowSalary.Controls.Add(this.label6);
            this.PnlShadowSalary.Controls.Add(this.label5);
            this.PnlShadowSalary.Controls.Add(this.label4);
            this.PnlShadowSalary.Controls.Add(this.label3);
            this.PnlShadowSalary.Controls.Add(this.label2);
            this.PnlShadowSalary.Controls.Add(this.label1);
            this.PnlShadowSalary.Controls.Add(this.dtRankingDate);
            this.PnlShadowSalary.Controls.Add(this.DtIncrement);
            this.PnlShadowSalary.Controls.Add(this.txtRemarks);
            this.PnlShadowSalary.Controls.Add(this.txtIncrementPerRs);
            this.PnlShadowSalary.Controls.Add(this.txtRanking);
            this.PnlShadowSalary.Controls.Add(this.txtReviseShadowSal);
            this.PnlShadowSalary.Controls.Add(this.txtLocalTIRPercent);
            this.PnlShadowSalary.Controls.Add(this.txtAnnualShadowSal);
            this.PnlShadowSalary.Controls.Add(this.txtRevisePackgeDolar);
            this.PnlShadowSalary.Controls.Add(this.txtIncrementPercent);
            this.PnlShadowSalary.Controls.Add(this.txtIncremntType);
            this.PnlShadowSalary.Controls.Add(this.TxtRankinginHC);
            this.PnlShadowSalary.Controls.Add(this.txtAnnualPackage);
            this.PnlShadowSalary.Controls.Add(this.txtIs_TirPercent);
            this.PnlShadowSalary.Controls.Add(this.txtLevel);
            this.PnlShadowSalary.Controls.Add(this.txtcountry);
            this.PnlShadowSalary.Controls.Add(this.txtDesignation);
            this.PnlShadowSalary.Controls.Add(this.txtCity);
            this.PnlShadowSalary.Controls.Add(this.txtFirstname);
            this.PnlShadowSalary.Controls.Add(this.txtShs_pr_No);
            this.PnlShadowSalary.Controls.Add(this.label33);
            this.PnlShadowSalary.DataManager = null;
            this.PnlShadowSalary.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlShadowSalary.DependentPanels = null;
            this.PnlShadowSalary.DisableDependentLoad = false;
            this.PnlShadowSalary.EnableDelete = true;
            this.PnlShadowSalary.EnableInsert = true;
            this.PnlShadowSalary.EnableQuery = false;
            this.PnlShadowSalary.EnableUpdate = true;
            this.PnlShadowSalary.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ShadowsalaryCommand";
            this.PnlShadowSalary.Location = new System.Drawing.Point(19, 151);
            this.PnlShadowSalary.MasterPanel = null;
            this.PnlShadowSalary.Name = "PnlShadowSalary";
            this.PnlShadowSalary.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlShadowSalary.Size = new System.Drawing.Size(579, 422);
            this.PnlShadowSalary.SPName = "CHRIS_SP_SHADOW_SALARY_MANAGER";
            this.PnlShadowSalary.TabIndex = 10;
            // 
            // lbRank
            // 
            this.lbRank.ActionLOVExists = "Rank_LovExists";
            this.lbRank.ActionType = "Rank_Lov";
            this.lbRank.ConditionalFields = "";
            this.lbRank.CustomEnabled = true;
            this.lbRank.DataFieldMapping = "";
            this.lbRank.DependentLovControls = "";
            this.lbRank.HiddenColumns = "";
            this.lbRank.Image = ((System.Drawing.Image)(resources.GetObject("lbRank.Image")));
            this.lbRank.LoadDependentEntities = false;
            this.lbRank.Location = new System.Drawing.Point(548, 339);
            this.lbRank.LookUpTitle = null;
            this.lbRank.Name = "lbRank";
            this.lbRank.Size = new System.Drawing.Size(26, 21);
            this.lbRank.SkipValidationOnLeave = false;
            this.lbRank.SPName = "CHRIS_SP_SHADOW_SALARY_MANAGER";
            this.lbRank.TabIndex = 73;
            this.lbRank.TabStop = false;
            this.lbRank.UseVisualStyleBackColor = true;
            // 
            // dtPromotion
            // 
            this.dtPromotion.Location = new System.Drawing.Point(429, 101);
            this.dtPromotion.Name = "dtPromotion";
            this.dtPromotion.Size = new System.Drawing.Size(101, 20);
            this.dtPromotion.TabIndex = 72;
            this.dtPromotion.Visible = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(413, 368);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(11, 13);
            this.label51.TabIndex = 71;
            this.label51.Text = ":";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(413, 344);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(11, 13);
            this.label50.TabIndex = 70;
            this.label50.Text = ":";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(155, 367);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(11, 13);
            this.label49.TabIndex = 69;
            this.label49.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(126, 259);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(11, 13);
            this.label45.TabIndex = 65;
            this.label45.Text = ":";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(126, 233);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(11, 13);
            this.label44.TabIndex = 64;
            this.label44.Text = ":";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(126, 207);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(11, 13);
            this.label43.TabIndex = 63;
            this.label43.Text = ":";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(126, 181);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(11, 13);
            this.label42.TabIndex = 62;
            this.label42.Text = ":";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(437, 228);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(14, 13);
            this.label48.TabIndex = 68;
            this.label48.Text = "$";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(437, 201);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(14, 13);
            this.label47.TabIndex = 67;
            this.label47.Text = "$";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(414, 179);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(11, 13);
            this.label46.TabIndex = 66;
            this.label46.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(409, 83);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(11, 13);
            this.label41.TabIndex = 61;
            this.label41.Text = ":";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(409, 57);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(11, 13);
            this.label40.TabIndex = 60;
            this.label40.Text = ":";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(409, 31);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(11, 13);
            this.label39.TabIndex = 59;
            this.label39.Text = ":";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(109, 109);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(11, 13);
            this.label38.TabIndex = 58;
            this.label38.Text = ":";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(109, 83);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(11, 13);
            this.label37.TabIndex = 57;
            this.label37.Text = ":";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(109, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(11, 13);
            this.label36.TabIndex = 56;
            this.label36.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(109, 31);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(11, 13);
            this.label35.TabIndex = 55;
            this.label35.Text = ":";
            // 
            // LbLastDt
            // 
            this.LbLastDt.ActionLOVExists = "LastDtINC_exists";
            this.LbLastDt.ActionType = "LastDtINC";
            this.LbLastDt.ConditionalFields = "txtShs_pr_No";
            this.LbLastDt.CustomEnabled = true;
            this.LbLastDt.DataFieldMapping = "";
            this.LbLastDt.DependentLovControls = "";
            this.LbLastDt.HiddenColumns = "";
            this.LbLastDt.Image = ((System.Drawing.Image)(resources.GetObject("LbLastDt.Image")));
            this.LbLastDt.LoadDependentEntities = false;
            this.LbLastDt.Location = new System.Drawing.Point(252, 174);
            this.LbLastDt.LookUpTitle = null;
            this.LbLastDt.Name = "LbLastDt";
            this.LbLastDt.Size = new System.Drawing.Size(26, 21);
            this.LbLastDt.SkipValidationOnLeave = false;
            this.LbLastDt.SPName = "CHRIS_SP_SHADOW_SALARY_MANAGER";
            this.LbLastDt.TabIndex = 54;
            this.LbLastDt.TabStop = false;
            this.LbLastDt.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label34.Location = new System.Drawing.Point(3, 3);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(579, 13);
            this.label34.TabIndex = 53;
            this.label34.Text = "_________________________________________________________________________________" +
                "____________________";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(5, 275);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(613, 13);
            this.label32.TabIndex = 51;
            this.label32.Text = "_________________________________________________________________________________" +
                "____________________";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 125);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(613, 13);
            this.label31.TabIndex = 50;
            this.label31.Text = "_________________________________________________________________________________" +
                "____________________";
            // 
            // lbPr_p_no
            // 
            this.lbPr_p_no.ActionLOVExists = "Lov_PR_p_no_exists";
            this.lbPr_p_no.ActionType = "Lov_PR_p_no";
            this.lbPr_p_no.ConditionalFields = "";
            this.lbPr_p_no.CustomEnabled = true;
            this.lbPr_p_no.DataFieldMapping = "";
            this.lbPr_p_no.DependentLovControls = "";
            this.lbPr_p_no.HiddenColumns = "";
            this.lbPr_p_no.Image = ((System.Drawing.Image)(resources.GetObject("lbPr_p_no.Image")));
            this.lbPr_p_no.LoadDependentEntities = false;
            this.lbPr_p_no.Location = new System.Drawing.Point(238, 26);
            this.lbPr_p_no.LookUpTitle = null;
            this.lbPr_p_no.Name = "lbPr_p_no";
            this.lbPr_p_no.Size = new System.Drawing.Size(26, 21);
            this.lbPr_p_no.SkipValidationOnLeave = false;
            this.lbPr_p_no.SPName = "CHRIS_SP_SHADOW_SALARY_MANAGER";
            this.lbPr_p_no.TabIndex = 49;
            this.lbPr_p_no.TabStop = false;
            this.lbPr_p_no.UseVisualStyleBackColor = true;
            // 
            // DtLastInc
            // 
            this.DtLastInc.CustomEnabled = true;
            this.DtLastInc.CustomFormat = "dd/MM/yyyy";
            this.DtLastInc.DataFieldMapping = "SHS_INC_LAST_DATE";
            this.DtLastInc.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtLastInc.HasChanges = true;
            this.DtLastInc.Location = new System.Drawing.Point(146, 177);
            this.DtLastInc.Name = "DtLastInc";
            this.DtLastInc.NullValue = " ";
            this.DtLastInc.Size = new System.Drawing.Size(100, 20);
            this.DtLastInc.TabIndex = 2;
            this.DtLastInc.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            this.DtLastInc.Validating += new System.ComponentModel.CancelEventHandler(this.DtLastInc_Validating);
            // 
            // DtTransferDate
            // 
            this.DtTransferDate.CustomEnabled = false;
            this.DtTransferDate.CustomFormat = "dd/MM/yyyy";
            this.DtTransferDate.DataFieldMapping = "SHS_TRANSFER_DATE";
            this.DtTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtTransferDate.HasChanges = false;
            this.DtTransferDate.Location = new System.Drawing.Point(429, 27);
            this.DtTransferDate.Name = "DtTransferDate";
            this.DtTransferDate.NullValue = " ";
            this.DtTransferDate.Size = new System.Drawing.Size(100, 20);
            this.DtTransferDate.TabIndex = 47;
            this.DtTransferDate.TabStop = false;
            this.DtTransferDate.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(198, 305);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(249, 17);
            this.label23.TabIndex = 46;
            this.label23.Text = "LOCAL COUNTRY INFROMATION";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(205, 148);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(242, 17);
            this.label22.TabIndex = 45;
            this.label22.Text = "HOST COUNTRY INFROMATION";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(295, 393);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 13);
            this.label21.TabIndex = 44;
            this.label21.Text = "6) Remarks";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(295, 371);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(116, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "4) Increment % RS.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(295, 343);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(106, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "2) Ranking /Date";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 393);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(154, 13);
            this.label18.TabIndex = 41;
            this.label18.Text = "5) Revise Schedule sal . :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 367);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "3) Local TIR %";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(10, 343);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(134, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "1) Annual Shadow sal.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(291, 257);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(138, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "8) Revise Package  $ :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(291, 228);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "6) Increment % ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(291, 202);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "4) Annual Package";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(291, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "2) Increment Date ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 259);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "7) Increment Type";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 233);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "5) Ranking In HC";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 207);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "3) IS-TIR %";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "1) Date of Last Inc.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(306, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "7) Country";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(306, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "6) City";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(306, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "5) Transfer Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "4) Level";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "3) Designation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "2) First Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "1) Personnel No.";
            // 
            // dtRankingDate
            // 
            this.dtRankingDate.CustomEnabled = true;
            this.dtRankingDate.CustomFormat = "dd/MM/yyyy";
            this.dtRankingDate.DataFieldMapping = "SHS_LOC_DATE";
            this.dtRankingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRankingDate.HasChanges = true;
            this.dtRankingDate.Location = new System.Drawing.Point(460, 340);
            this.dtRankingDate.Name = "dtRankingDate";
            this.dtRankingDate.NullValue = " ";
            this.dtRankingDate.Size = new System.Drawing.Size(87, 20);
            this.dtRankingDate.TabIndex = 16;
            this.dtRankingDate.Value = new System.DateTime(2011, 1, 19, 0, 0, 0, 0);
            this.dtRankingDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtRankingDate_Validating);
            // 
            // DtIncrement
            // 
            this.DtIncrement.CustomEnabled = true;
            this.DtIncrement.CustomFormat = "dd/MM/yyyy";
            this.DtIncrement.DataFieldMapping = "SHS_INC_CRNT_DATE";
            this.DtIncrement.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtIncrement.HasChanges = true;
            this.DtIncrement.Location = new System.Drawing.Point(455, 172);
            this.DtIncrement.Name = "DtIncrement";
            this.DtIncrement.NullValue = " ";
            this.DtIncrement.Size = new System.Drawing.Size(100, 20);
            this.DtIncrement.TabIndex = 3;
            this.DtIncrement.Value = new System.DateTime(2011, 1, 19, 0, 0, 0, 0);
            this.DtIncrement.Validating += new System.ComponentModel.CancelEventHandler(this.DtIncrement_Validating);
            // 
            // txtRemarks
            // 
            this.txtRemarks.AllowSpace = true;
            this.txtRemarks.AssociatedLookUpName = "";
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRemarks.ContinuationTextBox = null;
            this.txtRemarks.CustomEnabled = true;
            this.txtRemarks.DataFieldMapping = "SHS_REMARKS";
            this.txtRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.GetRecordsOnUpDownKeys = false;
            this.txtRemarks.IsDate = false;
            this.txtRemarks.Location = new System.Drawing.Point(430, 391);
            this.txtRemarks.MaxLength = 40;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.NumberFormat = "###,###,##0.00";
            this.txtRemarks.Postfix = "";
            this.txtRemarks.Prefix = "";
            this.txtRemarks.Size = new System.Drawing.Size(100, 20);
            this.txtRemarks.SkipValidation = false;
            this.txtRemarks.TabIndex = 17;
            this.txtRemarks.TextType = CrplControlLibrary.TextType.String;
            this.txtRemarks.Validating += new System.ComponentModel.CancelEventHandler(this.txtRemarks_Validating);
            // 
            // txtIncrementPerRs
            // 
            this.txtIncrementPerRs.AllowSpace = true;
            this.txtIncrementPerRs.AssociatedLookUpName = "";
            this.txtIncrementPerRs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncrementPerRs.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncrementPerRs.ContinuationTextBox = null;
            this.txtIncrementPerRs.CustomEnabled = false;
            this.txtIncrementPerRs.DataFieldMapping = "LC_INC_RS";
            this.txtIncrementPerRs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncrementPerRs.GetRecordsOnUpDownKeys = false;
            this.txtIncrementPerRs.IsDate = false;
            this.txtIncrementPerRs.Location = new System.Drawing.Point(430, 365);
            this.txtIncrementPerRs.MaxLength = 2;
            this.txtIncrementPerRs.Name = "txtIncrementPerRs";
            this.txtIncrementPerRs.NumberFormat = "###,###,##0.00";
            this.txtIncrementPerRs.Postfix = "";
            this.txtIncrementPerRs.Prefix = "";
            this.txtIncrementPerRs.Size = new System.Drawing.Size(44, 20);
            this.txtIncrementPerRs.SkipValidation = false;
            this.txtIncrementPerRs.TabIndex = 20;
            this.txtIncrementPerRs.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRanking
            // 
            this.txtRanking.AllowSpace = true;
            this.txtRanking.AssociatedLookUpName = "lbRank";
            this.txtRanking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRanking.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRanking.ContinuationTextBox = null;
            this.txtRanking.CustomEnabled = true;
            this.txtRanking.DataFieldMapping = "SHS_LOC_RANK";
            this.txtRanking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRanking.GetRecordsOnUpDownKeys = false;
            this.txtRanking.IsDate = false;
            this.txtRanking.IsRequired = true;
            this.txtRanking.Location = new System.Drawing.Point(430, 340);
            this.txtRanking.MaxLength = 1;
            this.txtRanking.Name = "txtRanking";
            this.txtRanking.NumberFormat = "###,###,##0.00";
            this.txtRanking.Postfix = "";
            this.txtRanking.Prefix = "";
            this.txtRanking.Size = new System.Drawing.Size(27, 20);
            this.txtRanking.SkipValidation = false;
            this.txtRanking.TabIndex = 15;
            this.txtRanking.Text = " ";
            this.txtRanking.TextType = CrplControlLibrary.TextType.String;
            this.txtRanking.Validating += new System.ComponentModel.CancelEventHandler(this.txtRanking_Validating);
            // 
            // txtReviseShadowSal
            // 
            this.txtReviseShadowSal.AllowSpace = true;
            this.txtReviseShadowSal.AssociatedLookUpName = "";
            this.txtReviseShadowSal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReviseShadowSal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReviseShadowSal.ContinuationTextBox = null;
            this.txtReviseShadowSal.CustomEnabled = true;
            this.txtReviseShadowSal.DataFieldMapping = "SHS_LOC_SALARY";
            this.txtReviseShadowSal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReviseShadowSal.GetRecordsOnUpDownKeys = false;
            this.txtReviseShadowSal.IsDate = false;
            this.txtReviseShadowSal.Location = new System.Drawing.Point(175, 391);
            this.txtReviseShadowSal.MaxLength = 11;
            this.txtReviseShadowSal.Name = "txtReviseShadowSal";
            this.txtReviseShadowSal.NumberFormat = "###,###,##0.00";
            this.txtReviseShadowSal.Postfix = "";
            this.txtReviseShadowSal.Prefix = "";
            this.txtReviseShadowSal.ReadOnly = true;
            this.txtReviseShadowSal.Size = new System.Drawing.Size(100, 20);
            this.txtReviseShadowSal.SkipValidation = false;
            this.txtReviseShadowSal.TabIndex = 17;
            this.txtReviseShadowSal.TabStop = false;
            this.txtReviseShadowSal.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocalTIRPercent
            // 
            this.txtLocalTIRPercent.AllowSpace = true;
            this.txtLocalTIRPercent.AssociatedLookUpName = "";
            this.txtLocalTIRPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocalTIRPercent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocalTIRPercent.ContinuationTextBox = null;
            this.txtLocalTIRPercent.CustomEnabled = true;
            this.txtLocalTIRPercent.DataFieldMapping = "LC_LOCAL_TIR";
            this.txtLocalTIRPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalTIRPercent.GetRecordsOnUpDownKeys = false;
            this.txtLocalTIRPercent.IsDate = false;
            this.txtLocalTIRPercent.Location = new System.Drawing.Point(175, 365);
            this.txtLocalTIRPercent.MaxLength = 5;
            this.txtLocalTIRPercent.Name = "txtLocalTIRPercent";
            this.txtLocalTIRPercent.NumberFormat = "###,###,##0.00";
            this.txtLocalTIRPercent.Postfix = "";
            this.txtLocalTIRPercent.Prefix = "";
            this.txtLocalTIRPercent.ReadOnly = true;
            this.txtLocalTIRPercent.Size = new System.Drawing.Size(60, 20);
            this.txtLocalTIRPercent.SkipValidation = false;
            this.txtLocalTIRPercent.TabIndex = 16;
            this.txtLocalTIRPercent.TabStop = false;
            this.txtLocalTIRPercent.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAnnualShadowSal
            // 
            this.txtAnnualShadowSal.AllowSpace = true;
            this.txtAnnualShadowSal.AssociatedLookUpName = "";
            this.txtAnnualShadowSal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualShadowSal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualShadowSal.ContinuationTextBox = null;
            this.txtAnnualShadowSal.CustomEnabled = true;
            this.txtAnnualShadowSal.DataFieldMapping = "SHS_ANL_SHD_SAL";
            this.txtAnnualShadowSal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualShadowSal.GetRecordsOnUpDownKeys = false;
            this.txtAnnualShadowSal.IsDate = false;
            this.txtAnnualShadowSal.Location = new System.Drawing.Point(175, 339);
            this.txtAnnualShadowSal.Name = "txtAnnualShadowSal";
            this.txtAnnualShadowSal.NumberFormat = "###,###,##0.00";
            this.txtAnnualShadowSal.Postfix = "";
            this.txtAnnualShadowSal.Prefix = "";
            this.txtAnnualShadowSal.ReadOnly = true;
            this.txtAnnualShadowSal.Size = new System.Drawing.Size(100, 20);
            this.txtAnnualShadowSal.SkipValidation = false;
            this.txtAnnualShadowSal.TabIndex = 15;
            this.txtAnnualShadowSal.TabStop = false;
            this.txtAnnualShadowSal.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRevisePackgeDolar
            // 
            this.txtRevisePackgeDolar.AllowSpace = true;
            this.txtRevisePackgeDolar.AssociatedLookUpName = "";
            this.txtRevisePackgeDolar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRevisePackgeDolar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRevisePackgeDolar.ContinuationTextBox = null;
            this.txtRevisePackgeDolar.CustomEnabled = true;
            this.txtRevisePackgeDolar.DataFieldMapping = "SHS_REV_PKG";
            this.txtRevisePackgeDolar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRevisePackgeDolar.GetRecordsOnUpDownKeys = false;
            this.txtRevisePackgeDolar.IsDate = false;
            this.txtRevisePackgeDolar.Location = new System.Drawing.Point(455, 250);
            this.txtRevisePackgeDolar.MaxLength = 8;
            this.txtRevisePackgeDolar.Name = "txtRevisePackgeDolar";
            this.txtRevisePackgeDolar.NumberFormat = "###,###,##0.00";
            this.txtRevisePackgeDolar.Postfix = "";
            this.txtRevisePackgeDolar.Prefix = "";
            this.txtRevisePackgeDolar.Size = new System.Drawing.Size(100, 20);
            this.txtRevisePackgeDolar.SkipValidation = false;
            this.txtRevisePackgeDolar.TabIndex = 14;
            this.txtRevisePackgeDolar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRevisePackgeDolar.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtIncrementPercent
            // 
            this.txtIncrementPercent.AllowSpace = true;
            this.txtIncrementPercent.AssociatedLookUpName = "";
            this.txtIncrementPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncrementPercent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncrementPercent.ContinuationTextBox = null;
            this.txtIncrementPercent.CustomEnabled = true;
            this.txtIncrementPercent.DataFieldMapping = "SHS_INC_PERCENT";
            this.txtIncrementPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncrementPercent.GetRecordsOnUpDownKeys = false;
            this.txtIncrementPercent.IsDate = false;
            this.txtIncrementPercent.Location = new System.Drawing.Point(455, 224);
            this.txtIncrementPercent.MaxLength = 5;
            this.txtIncrementPercent.Name = "txtIncrementPercent";
            this.txtIncrementPercent.NumberFormat = "###,###,##0.00";
            this.txtIncrementPercent.Postfix = "";
            this.txtIncrementPercent.Prefix = "";
            this.txtIncrementPercent.Size = new System.Drawing.Size(52, 20);
            this.txtIncrementPercent.SkipValidation = false;
            this.txtIncrementPercent.TabIndex = 7;
            this.txtIncrementPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncrementPercent.TextType = CrplControlLibrary.TextType.Double;
            this.txtIncrementPercent.Validating += new System.ComponentModel.CancelEventHandler(this.txtIncrementPercent_Validating);
            // 
            // txtIncremntType
            // 
            this.txtIncremntType.AllowSpace = true;
            this.txtIncremntType.AssociatedLookUpName = "";
            this.txtIncremntType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncremntType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncremntType.ContinuationTextBox = null;
            this.txtIncremntType.CustomEnabled = true;
            this.txtIncremntType.DataFieldMapping = "SHS_INC_TYPE";
            this.txtIncremntType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncremntType.GetRecordsOnUpDownKeys = false;
            this.txtIncremntType.IsDate = false;
            this.txtIncremntType.Location = new System.Drawing.Point(146, 254);
            this.txtIncremntType.MaxLength = 1;
            this.txtIncremntType.Name = "txtIncremntType";
            this.txtIncremntType.NumberFormat = "###,###,##0.00";
            this.txtIncremntType.Postfix = "";
            this.txtIncremntType.Prefix = "";
            this.txtIncremntType.Size = new System.Drawing.Size(40, 20);
            this.txtIncremntType.SkipValidation = false;
            this.txtIncremntType.TabIndex = 8;
            this.txtIncremntType.TextType = CrplControlLibrary.TextType.String;
            // 
            // TxtRankinginHC
            // 
            this.TxtRankinginHC.AllowSpace = true;
            this.TxtRankinginHC.AssociatedLookUpName = "";
            this.TxtRankinginHC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRankinginHC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtRankinginHC.ContinuationTextBox = null;
            this.TxtRankinginHC.CustomEnabled = true;
            this.TxtRankinginHC.DataFieldMapping = "SHS_RANK_HC";
            this.TxtRankinginHC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRankinginHC.GetRecordsOnUpDownKeys = false;
            this.TxtRankinginHC.IsDate = false;
            this.TxtRankinginHC.Location = new System.Drawing.Point(146, 228);
            this.TxtRankinginHC.MaxLength = 1;
            this.TxtRankinginHC.Name = "TxtRankinginHC";
            this.TxtRankinginHC.NumberFormat = "###,###,##0.00";
            this.TxtRankinginHC.Postfix = "";
            this.TxtRankinginHC.Prefix = "";
            this.TxtRankinginHC.Size = new System.Drawing.Size(40, 20);
            this.TxtRankinginHC.SkipValidation = false;
            this.TxtRankinginHC.TabIndex = 6;
            this.TxtRankinginHC.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAnnualPackage
            // 
            this.txtAnnualPackage.AllowSpace = true;
            this.txtAnnualPackage.AssociatedLookUpName = "";
            this.txtAnnualPackage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualPackage.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualPackage.ContinuationTextBox = null;
            this.txtAnnualPackage.CustomEnabled = true;
            this.txtAnnualPackage.DataFieldMapping = "SHS_ANL_PKG";
            this.txtAnnualPackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualPackage.GetRecordsOnUpDownKeys = false;
            this.txtAnnualPackage.IsDate = false;
            this.txtAnnualPackage.Location = new System.Drawing.Point(455, 198);
            this.txtAnnualPackage.MaxLength = 8;
            this.txtAnnualPackage.Name = "txtAnnualPackage";
            this.txtAnnualPackage.NumberFormat = "###,###,##0.00";
            this.txtAnnualPackage.Postfix = "";
            this.txtAnnualPackage.Prefix = "";
            this.txtAnnualPackage.Size = new System.Drawing.Size(100, 20);
            this.txtAnnualPackage.SkipValidation = false;
            this.txtAnnualPackage.TabIndex = 5;
            this.txtAnnualPackage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnnualPackage.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtIs_TirPercent
            // 
            this.txtIs_TirPercent.AllowSpace = true;
            this.txtIs_TirPercent.AssociatedLookUpName = "";
            this.txtIs_TirPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIs_TirPercent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIs_TirPercent.ContinuationTextBox = null;
            this.txtIs_TirPercent.CustomEnabled = true;
            this.txtIs_TirPercent.DataFieldMapping = "SHS_IS_TIR";
            this.txtIs_TirPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIs_TirPercent.GetRecordsOnUpDownKeys = false;
            this.txtIs_TirPercent.IsDate = false;
            this.txtIs_TirPercent.Location = new System.Drawing.Point(146, 202);
            this.txtIs_TirPercent.MaxLength = 5;
            this.txtIs_TirPercent.Name = "txtIs_TirPercent";
            this.txtIs_TirPercent.NumberFormat = "###,###,##0.00";
            this.txtIs_TirPercent.Postfix = "";
            this.txtIs_TirPercent.Prefix = "";
            this.txtIs_TirPercent.Size = new System.Drawing.Size(60, 20);
            this.txtIs_TirPercent.SkipValidation = false;
            this.txtIs_TirPercent.TabIndex = 4;
            this.txtIs_TirPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIs_TirPercent.TextType = CrplControlLibrary.TextType.Double;
            this.txtIs_TirPercent.Validating += new System.ComponentModel.CancelEventHandler(this.txtIs_TirPercent_Validating);
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "SHS_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(131, 105);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(40, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 6;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtcountry
            // 
            this.txtcountry.AllowSpace = true;
            this.txtcountry.AssociatedLookUpName = "";
            this.txtcountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcountry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcountry.ContinuationTextBox = null;
            this.txtcountry.CustomEnabled = true;
            this.txtcountry.DataFieldMapping = "SHS_COUNTRY";
            this.txtcountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcountry.GetRecordsOnUpDownKeys = false;
            this.txtcountry.IsDate = false;
            this.txtcountry.Location = new System.Drawing.Point(429, 79);
            this.txtcountry.Name = "txtcountry";
            this.txtcountry.NumberFormat = "###,###,##0.00";
            this.txtcountry.Postfix = "";
            this.txtcountry.Prefix = "";
            this.txtcountry.ReadOnly = true;
            this.txtcountry.Size = new System.Drawing.Size(100, 20);
            this.txtcountry.SkipValidation = false;
            this.txtcountry.TabIndex = 5;
            this.txtcountry.TabStop = false;
            this.txtcountry.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesignation
            // 
            this.txtDesignation.AllowSpace = true;
            this.txtDesignation.AssociatedLookUpName = "";
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesignation.ContinuationTextBox = null;
            this.txtDesignation.CustomEnabled = true;
            this.txtDesignation.DataFieldMapping = "SHS_DESIG";
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.GetRecordsOnUpDownKeys = false;
            this.txtDesignation.IsDate = false;
            this.txtDesignation.Location = new System.Drawing.Point(131, 79);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.NumberFormat = "###,###,##0.00";
            this.txtDesignation.Postfix = "";
            this.txtDesignation.Prefix = "";
            this.txtDesignation.ReadOnly = true;
            this.txtDesignation.Size = new System.Drawing.Size(40, 20);
            this.txtDesignation.SkipValidation = false;
            this.txtDesignation.TabIndex = 4;
            this.txtDesignation.TabStop = false;
            this.txtDesignation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCity
            // 
            this.txtCity.AllowSpace = true;
            this.txtCity.AssociatedLookUpName = "";
            this.txtCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.ContinuationTextBox = null;
            this.txtCity.CustomEnabled = true;
            this.txtCity.DataFieldMapping = "SHS_CITY";
            this.txtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.GetRecordsOnUpDownKeys = false;
            this.txtCity.IsDate = false;
            this.txtCity.Location = new System.Drawing.Point(429, 53);
            this.txtCity.Name = "txtCity";
            this.txtCity.NumberFormat = "###,###,##0.00";
            this.txtCity.Postfix = "";
            this.txtCity.Prefix = "";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(100, 20);
            this.txtCity.SkipValidation = false;
            this.txtCity.TabIndex = 3;
            this.txtCity.TabStop = false;
            this.txtCity.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstname
            // 
            this.txtFirstname.AllowSpace = true;
            this.txtFirstname.AssociatedLookUpName = "";
            this.txtFirstname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstname.ContinuationTextBox = null;
            this.txtFirstname.CustomEnabled = true;
            this.txtFirstname.DataFieldMapping = "LC_PR_NAME";
            this.txtFirstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstname.GetRecordsOnUpDownKeys = false;
            this.txtFirstname.IsDate = false;
            this.txtFirstname.Location = new System.Drawing.Point(131, 53);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.NumberFormat = "###,###,##0.00";
            this.txtFirstname.Postfix = "";
            this.txtFirstname.Prefix = "";
            this.txtFirstname.ReadOnly = true;
            this.txtFirstname.Size = new System.Drawing.Size(133, 20);
            this.txtFirstname.SkipValidation = false;
            this.txtFirstname.TabIndex = 2;
            this.txtFirstname.TabStop = false;
            this.txtFirstname.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtShs_pr_No
            // 
            this.txtShs_pr_No.AllowSpace = true;
            this.txtShs_pr_No.AssociatedLookUpName = "lbPr_p_no";
            this.txtShs_pr_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtShs_pr_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtShs_pr_No.ContinuationTextBox = null;
            this.txtShs_pr_No.CustomEnabled = true;
            this.txtShs_pr_No.DataFieldMapping = "SHS_PR_NO";
            this.txtShs_pr_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShs_pr_No.GetRecordsOnUpDownKeys = false;
            this.txtShs_pr_No.IsDate = false;
            this.txtShs_pr_No.Location = new System.Drawing.Point(131, 27);
            this.txtShs_pr_No.MaxLength = 6;
            this.txtShs_pr_No.Name = "txtShs_pr_No";
            this.txtShs_pr_No.NumberFormat = "###,###,##0.00";
            this.txtShs_pr_No.Postfix = "";
            this.txtShs_pr_No.Prefix = "";
            this.txtShs_pr_No.Size = new System.Drawing.Size(100, 20);
            this.txtShs_pr_No.SkipValidation = false;
            this.txtShs_pr_No.TabIndex = 1;
            this.txtShs_pr_No.TextType = CrplControlLibrary.TextType.Integer;
            this.txtShs_pr_No.Validated += new System.EventHandler(this.txtShs_pr_No_Validated);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(-3, 407);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(613, 13);
            this.label33.TabIndex = 52;
            this.label33.Text = "_________________________________________________________________________________" +
                "____________________";
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label24);
            this.pnlHead.Controls.Add(this.label25);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label26);
            this.pnlHead.Controls.Add(this.label27);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label28);
            this.pnlHead.Controls.Add(this.label29);
            this.pnlHead.Location = new System.Drawing.Point(19, 82);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(582, 70);
            this.pnlHead.TabIndex = 54;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(177, 42);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(251, 18);
            this.label24.TabIndex = 20;
            this.label24.Text = "S H A D O W      S A L A R Y";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(190, 14);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(235, 20);
            this.label25.TabIndex = 19;
            this.label25.Text = "Personnel System";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(486, 41);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(486, 16);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(443, 43);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 15);
            this.label26.TabIndex = 16;
            this.label26.Text = "Date:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(435, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 15);
            this.label27.TabIndex = 15;
            this.label27.Text = "Option:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(88, 42);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(88, 16);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(12, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(70, 20);
            this.label28.TabIndex = 2;
            this.label28.Text = "Loc:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(24, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(58, 20);
            this.label29.TabIndex = 1;
            this.label29.Text = "User:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(402, 13);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(63, 13);
            this.label30.TabIndex = 55;
            this.label30.Text = "UserName :";
            // 
            // CHRIS_Personnel_ShadowSalary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(620, 653);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlShadowSalary);
            this.CurrentPanelBlock = "PnlShadowSalary";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.MaximumSize = new System.Drawing.Size(626, 685);
            this.Name = "CHRIS_Personnel_ShadowSalary";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_ShadowSalary";
            this.Controls.SetChildIndex(this.PnlShadowSalary, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label30, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlShadowSalary.ResumeLayout(false);
            this.PnlShadowSalary.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlShadowSalary;
        private CrplControlLibrary.SLTextBox txtRemarks;
        private CrplControlLibrary.SLTextBox txtIncrementPerRs;
        private CrplControlLibrary.SLTextBox txtRanking;
        private CrplControlLibrary.SLTextBox txtReviseShadowSal;
        private CrplControlLibrary.SLTextBox txtLocalTIRPercent;
        private CrplControlLibrary.SLTextBox txtAnnualShadowSal;
        private CrplControlLibrary.SLTextBox txtRevisePackgeDolar;
        private CrplControlLibrary.SLTextBox txtIncrementPercent;
        private CrplControlLibrary.SLTextBox txtIncremntType;
        private CrplControlLibrary.SLTextBox TxtRankinginHC;
        private CrplControlLibrary.SLTextBox txtAnnualPackage;
        private CrplControlLibrary.SLTextBox txtIs_TirPercent;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtcountry;
        private CrplControlLibrary.SLTextBox txtDesignation;
        private CrplControlLibrary.SLTextBox txtCity;
        private CrplControlLibrary.SLTextBox txtFirstname;
        private CrplControlLibrary.SLTextBox txtShs_pr_No;
        private CrplControlLibrary.SLDatePicker DtIncrement;
        private CrplControlLibrary.SLDatePicker dtRankingDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLDatePicker DtTransferDate;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLDatePicker DtLastInc;
        private CrplControlLibrary.LookupButton lbPr_p_no;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.LookupButton LbLastDt;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.DateTimePicker dtPromotion;
        private CrplControlLibrary.LookupButton lbRank;
    }
}