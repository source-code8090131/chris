namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_ContractStaffHiringEnt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_ContractStaffHiringEnt));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.lkpBtnAuthorize = new CrplControlLibrary.LookupButton(this.components);
            this.slTxtBxUserID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtContractor1 = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnContractor = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnDesg = new CrplControlLibrary.LookupButton(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.dtpDOB = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPh2 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpToDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtContractorName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbtnBranch = new CrplControlLibrary.LookupButton(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSalary = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.txtNID = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtSex = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMarital = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPh1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAddress = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.dtpFromDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtDesg = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colSeg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDept = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.colContribution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.pnlDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(812, 0);
            this.txtOption.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Margin = new System.Windows.Forms.Padding(5);
            this.pnlBottom.Size = new System.Drawing.Size(859, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 693);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(859, 74);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(16, 48);
            this.pnlHead.Margin = new System.Windows.Forms.Padding(4);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(827, 112);
            this.pnlHead.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(220, 84);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(412, 22);
            this.label6.TabIndex = 20;
            this.label6.Text = "C O N T R A C T U A L S    E N T R Y";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(220, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(412, 25);
            this.label5.TabIndex = 19;
            this.label5.Text = "Personnel System";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(697, 81);
            this.txtDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(106, 24);
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(697, 49);
            this.txtCurrOption.Margin = new System.Windows.Forms.Padding(4);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(106, 24);
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(640, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 18);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(629, 52);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(105, 81);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(106, 24);
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(105, 49);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(106, 24);
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(4, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.lkpBtnAuthorize);
            this.pnlDetail.Controls.Add(this.slTxtBxUserID);
            this.pnlDetail.Controls.Add(this.txtContractor1);
            this.pnlDetail.Controls.Add(this.lbtnContractor);
            this.pnlDetail.Controls.Add(this.lbtnDesg);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.dtpDOB);
            this.pnlDetail.Controls.Add(this.txtPh2);
            this.pnlDetail.Controls.Add(this.dtpToDate);
            this.pnlDetail.Controls.Add(this.txtContractorName);
            this.pnlDetail.Controls.Add(this.txtLastName);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.lbtnBranch);
            this.pnlDetail.Controls.Add(this.txtBranch);
            this.pnlDetail.Controls.Add(this.txtSalary);
            this.pnlDetail.Controls.Add(this.label21);
            this.pnlDetail.Controls.Add(this.txtNID);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtSex);
            this.pnlDetail.Controls.Add(this.txtMarital);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.Controls.Add(this.txtPh1);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtAddress);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.dtpFromDate);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtDesg);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtFirstName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ContractCommand";
            this.pnlDetail.Location = new System.Drawing.Point(16, 164);
            this.pnlDetail.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(475, 503);
            this.pnlDetail.SPName = "CHRIS_SP_CONTRACT_MANAGER";
            this.pnlDetail.TabIndex = 1;
            this.pnlDetail.TabStop = true;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(59, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 24);
            this.label18.TabIndex = 91;
            this.label18.Text = "Authorize :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lkpBtnAuthorize
            // 
            this.lkpBtnAuthorize.ActionLOVExists = "MakerChecker";
            this.lkpBtnAuthorize.ActionType = "_ContractualsHiring";
            this.lkpBtnAuthorize.ConditionalFields = "";
            this.lkpBtnAuthorize.CustomEnabled = true;
            this.lkpBtnAuthorize.DataFieldMapping = "";
            this.lkpBtnAuthorize.DependentLovControls = "";
            this.lkpBtnAuthorize.HiddenColumns = "PR_LAST_NAME|PR_BRANCH|PR_DESIG|PR_CONTRACTOR|PR_FROM_DATE|PR_TO_DATE|PR_ADDRESS|" +
    "PR_PHONE_1|PR_PHONE_2|PR_DATE_BIRTH|PR_MARITAL|PR_SEX|PR_NID|PR_MONTHLY_SALARY|P" +
    "R_FLAG";
            this.lkpBtnAuthorize.Image = ((System.Drawing.Image)(resources.GetObject("lkpBtnAuthorize.Image")));
            this.lkpBtnAuthorize.LoadDependentEntities = true;
            this.lkpBtnAuthorize.Location = new System.Drawing.Point(204, 5);
            this.lkpBtnAuthorize.LookUpTitle = null;
            this.lkpBtnAuthorize.Margin = new System.Windows.Forms.Padding(4);
            this.lkpBtnAuthorize.Name = "lkpBtnAuthorize";
            this.lkpBtnAuthorize.Size = new System.Drawing.Size(26, 21);
            this.lkpBtnAuthorize.SkipValidationOnLeave = false;
            this.lkpBtnAuthorize.SPName = "CHRIS_SP_FrmCall_Users";
            this.lkpBtnAuthorize.TabIndex = 68;
            this.lkpBtnAuthorize.TabStop = false;
            this.lkpBtnAuthorize.UseVisualStyleBackColor = true;
            // 
            // slTxtBxUserID
            // 
            this.slTxtBxUserID.AllowSpace = true;
            this.slTxtBxUserID.AssociatedLookUpName = "lbtnUserID";
            this.slTxtBxUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBxUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBxUserID.ContinuationTextBox = null;
            this.slTxtBxUserID.CustomEnabled = true;
            this.slTxtBxUserID.DataFieldMapping = "PR_UserID";
            this.slTxtBxUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBxUserID.GetRecordsOnUpDownKeys = false;
            this.slTxtBxUserID.IsDate = false;
            this.slTxtBxUserID.Location = new System.Drawing.Point(403, 4);
            this.slTxtBxUserID.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtBxUserID.MaxLength = 20;
            this.slTxtBxUserID.Name = "slTxtBxUserID";
            this.slTxtBxUserID.NumberFormat = "###,###,##0.00";
            this.slTxtBxUserID.Postfix = "";
            this.slTxtBxUserID.Prefix = "";
            this.slTxtBxUserID.Size = new System.Drawing.Size(45, 24);
            this.slTxtBxUserID.SkipValidation = false;
            this.slTxtBxUserID.TabIndex = 89;
            this.slTxtBxUserID.TextType = CrplControlLibrary.TextType.String;
            this.slTxtBxUserID.Visible = false;
            // 
            // txtContractor1
            // 
            this.txtContractor1.AllowSpace = true;
            this.txtContractor1.AssociatedLookUpName = "lbtnContractor";
            this.txtContractor1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContractor1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContractor1.ContinuationTextBox = null;
            this.txtContractor1.CustomEnabled = true;
            this.txtContractor1.DataFieldMapping = "PR_CONTRACTOR";
            this.txtContractor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContractor1.GetRecordsOnUpDownKeys = false;
            this.txtContractor1.IsDate = false;
            this.txtContractor1.IsLookUpField = true;
            this.txtContractor1.IsRequired = true;
            this.txtContractor1.Location = new System.Drawing.Point(204, 191);
            this.txtContractor1.Margin = new System.Windows.Forms.Padding(4);
            this.txtContractor1.MaxLength = 3;
            this.txtContractor1.Name = "txtContractor1";
            this.txtContractor1.NumberFormat = "###,###,##0.00";
            this.txtContractor1.Postfix = "";
            this.txtContractor1.Prefix = "";
            this.txtContractor1.Size = new System.Drawing.Size(65, 24);
            this.txtContractor1.SkipValidation = false;
            this.txtContractor1.TabIndex = 5;
            this.txtContractor1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtContractor1, "Enter Valid Contractor Code Or Press [F9] Key For Help");
            // 
            // lbtnContractor
            // 
            this.lbtnContractor.ActionLOVExists = "CONTRACTOR_LOV_EXISTS";
            this.lbtnContractor.ActionType = "CONTRACTOR_LOV";
            this.lbtnContractor.ConditionalFields = "";
            this.lbtnContractor.CustomEnabled = true;
            this.lbtnContractor.DataFieldMapping = "";
            this.lbtnContractor.DependentLovControls = "";
            this.lbtnContractor.HiddenColumns = "";
            this.lbtnContractor.Image = ((System.Drawing.Image)(resources.GetObject("lbtnContractor.Image")));
            this.lbtnContractor.LoadDependentEntities = false;
            this.lbtnContractor.Location = new System.Drawing.Point(277, 191);
            this.lbtnContractor.LookUpTitle = null;
            this.lbtnContractor.Margin = new System.Windows.Forms.Padding(4);
            this.lbtnContractor.Name = "lbtnContractor";
            this.lbtnContractor.Size = new System.Drawing.Size(26, 21);
            this.lbtnContractor.SkipValidationOnLeave = false;
            this.lbtnContractor.SPName = "CHRIS_SP_CONTRACT_MANAGER";
            this.lbtnContractor.TabIndex = 5;
            this.lbtnContractor.TabStop = false;
            this.lbtnContractor.UseVisualStyleBackColor = true;
            this.lbtnContractor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // lbtnDesg
            // 
            this.lbtnDesg.ActionLOVExists = "DESG_LOV_EXISTS";
            this.lbtnDesg.ActionType = "DESG_LOV";
            this.lbtnDesg.ConditionalFields = "";
            this.lbtnDesg.CustomEnabled = true;
            this.lbtnDesg.DataFieldMapping = "";
            this.lbtnDesg.DependentLovControls = "";
            this.lbtnDesg.HiddenColumns = "";
            this.lbtnDesg.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDesg.Image")));
            this.lbtnDesg.LoadDependentEntities = false;
            this.lbtnDesg.Location = new System.Drawing.Point(277, 159);
            this.lbtnDesg.LookUpTitle = null;
            this.lbtnDesg.Margin = new System.Windows.Forms.Padding(4);
            this.lbtnDesg.Name = "lbtnDesg";
            this.lbtnDesg.Size = new System.Drawing.Size(26, 21);
            this.lbtnDesg.SkipValidationOnLeave = false;
            this.lbtnDesg.SPName = "CHRIS_SP_CONTRACT_MANAGER";
            this.lbtnDesg.TabIndex = 4;
            this.lbtnDesg.TabStop = false;
            this.lbtnDesg.UseVisualStyleBackColor = true;
            this.lbtnDesg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(59, 383);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 25);
            this.label27.TabIndex = 46;
            this.label27.Text = "Sex";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomEnabled = true;
            this.dtpDOB.CustomFormat = "dd/MM/yyyy";
            this.dtpDOB.DataFieldMapping = "PR_DATE_BIRTH";
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.HasChanges = true;
            this.dtpDOB.IsRequired = true;
            this.dtpDOB.Location = new System.Drawing.Point(204, 319);
            this.dtpDOB.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.NullValue = " ";
            this.dtpDOB.Size = new System.Drawing.Size(116, 22);
            this.dtpDOB.TabIndex = 11;
            this.dtpDOB.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpDOB.Validating += new System.ComponentModel.CancelEventHandler(this.dtpDOB_Validating);
            // 
            // txtPh2
            // 
            this.txtPh2.AllowSpace = true;
            this.txtPh2.AssociatedLookUpName = "";
            this.txtPh2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPh2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPh2.ContinuationTextBox = null;
            this.txtPh2.CustomEnabled = true;
            this.txtPh2.DataFieldMapping = "PR_PHONE_2";
            this.txtPh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPh2.GetRecordsOnUpDownKeys = false;
            this.txtPh2.IsDate = false;
            this.txtPh2.Location = new System.Drawing.Point(313, 287);
            this.txtPh2.Margin = new System.Windows.Forms.Padding(4);
            this.txtPh2.MaxLength = 8;
            this.txtPh2.Name = "txtPh2";
            this.txtPh2.NumberFormat = "###,###,##0.00";
            this.txtPh2.Postfix = "";
            this.txtPh2.Prefix = "";
            this.txtPh2.Size = new System.Drawing.Size(101, 24);
            this.txtPh2.SkipValidation = false;
            this.txtPh2.TabIndex = 10;
            this.txtPh2.TextType = CrplControlLibrary.TextType.String;
            this.txtPh2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPh2_Validating);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomEnabled = true;
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.DataFieldMapping = "PR_TO_DATE";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.HasChanges = true;
            this.dtpToDate.Location = new System.Drawing.Point(329, 223);
            this.dtpToDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.NullValue = " ";
            this.dtpToDate.Size = new System.Drawing.Size(116, 22);
            this.dtpToDate.TabIndex = 7;
            this.dtpToDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpToDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpToDate_Validating);
            // 
            // txtContractorName
            // 
            this.txtContractorName.AllowSpace = true;
            this.txtContractorName.AssociatedLookUpName = "";
            this.txtContractorName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContractorName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContractorName.ContinuationTextBox = null;
            this.txtContractorName.CustomEnabled = true;
            this.txtContractorName.DataFieldMapping = "W_CONTRA_NAME";
            this.txtContractorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContractorName.GetRecordsOnUpDownKeys = false;
            this.txtContractorName.IsDate = false;
            this.txtContractorName.Location = new System.Drawing.Point(320, 191);
            this.txtContractorName.Margin = new System.Windows.Forms.Padding(4);
            this.txtContractorName.MaxLength = 30;
            this.txtContractorName.Name = "txtContractorName";
            this.txtContractorName.NumberFormat = "###,###,##0.00";
            this.txtContractorName.Postfix = "";
            this.txtContractorName.Prefix = "";
            this.txtContractorName.ReadOnly = true;
            this.txtContractorName.Size = new System.Drawing.Size(126, 24);
            this.txtContractorName.SkipValidation = false;
            this.txtContractorName.TabIndex = 61;
            this.txtContractorName.TabStop = false;
            this.txtContractorName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "PR_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(204, 127);
            this.txtLastName.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastName.MaxLength = 20;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.Size = new System.Drawing.Size(198, 24);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 3;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(59, 127);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 25);
            this.label14.TabIndex = 60;
            this.label14.Text = "Last Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(59, 63);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 25);
            this.label13.TabIndex = 58;
            this.label13.Text = "Branch";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbtnBranch
            // 
            this.lbtnBranch.ActionLOVExists = "BRANCH_LOV_EXISTS";
            this.lbtnBranch.ActionType = "BRANCH_LOV";
            this.lbtnBranch.ConditionalFields = "";
            this.lbtnBranch.CustomEnabled = true;
            this.lbtnBranch.DataFieldMapping = "";
            this.lbtnBranch.DependentLovControls = "";
            this.lbtnBranch.HiddenColumns = "";
            this.lbtnBranch.Image = ((System.Drawing.Image)(resources.GetObject("lbtnBranch.Image")));
            this.lbtnBranch.LoadDependentEntities = false;
            this.lbtnBranch.Location = new System.Drawing.Point(313, 63);
            this.lbtnBranch.LookUpTitle = null;
            this.lbtnBranch.Margin = new System.Windows.Forms.Padding(4);
            this.lbtnBranch.Name = "lbtnBranch";
            this.lbtnBranch.Size = new System.Drawing.Size(26, 21);
            this.lbtnBranch.SkipValidationOnLeave = false;
            this.lbtnBranch.SPName = "CHRIS_SP_CONTRACT_MANAGER";
            this.lbtnBranch.TabIndex = 1;
            this.lbtnBranch.TabStop = false;
            this.lbtnBranch.UseVisualStyleBackColor = true;
            this.lbtnBranch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "lbtnBranch";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PR_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.IsRequired = true;
            this.txtBranch.Location = new System.Drawing.Point(204, 63);
            this.txtBranch.Margin = new System.Windows.Forms.Padding(4);
            this.txtBranch.MaxLength = 3;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.Size = new System.Drawing.Size(101, 24);
            this.txtBranch.SkipValidation = true;
            this.txtBranch.TabIndex = 1;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtBranch, "Enter Valid Branch Or Press [Spaces] To Exit");
            this.txtBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBranch_KeyPress);
            this.txtBranch.Validating += new System.ComponentModel.CancelEventHandler(this.txtBranch_Validating);
            // 
            // txtSalary
            // 
            this.txtSalary.AllowSpace = true;
            this.txtSalary.AssociatedLookUpName = "";
            this.txtSalary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSalary.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSalary.ContinuationTextBox = null;
            this.txtSalary.CustomEnabled = true;
            this.txtSalary.DataFieldMapping = "PR_MONTHLY_SALARY";
            this.txtSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalary.GetRecordsOnUpDownKeys = false;
            this.txtSalary.IsDate = false;
            this.txtSalary.IsRequired = true;
            this.txtSalary.Location = new System.Drawing.Point(204, 447);
            this.txtSalary.Margin = new System.Windows.Forms.Padding(4);
            this.txtSalary.MaxLength = 8;
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.NumberFormat = "###,###,##";
            this.txtSalary.Postfix = "";
            this.txtSalary.Prefix = "";
            this.txtSalary.Size = new System.Drawing.Size(181, 24);
            this.txtSalary.SkipValidation = false;
            this.txtSalary.TabIndex = 15;
            this.txtSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSalary.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(59, 447);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(139, 25);
            this.label21.TabIndex = 55;
            this.label21.Text = "Monthly Salary";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNID
            // 
            this.txtNID.AllowSpace = true;
            this.txtNID.AssociatedLookUpName = "";
            this.txtNID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNID.ContinuationTextBox = null;
            this.txtNID.CustomEnabled = true;
            this.txtNID.DataFieldMapping = "PR_NID";
            this.txtNID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNID.GetRecordsOnUpDownKeys = false;
            this.txtNID.IsDate = false;
            this.txtNID.IsRequired = true;
            this.txtNID.Location = new System.Drawing.Point(204, 415);
            this.txtNID.Margin = new System.Windows.Forms.Padding(4);
            this.txtNID.MaxLength = 13;
            this.txtNID.Name = "txtNID";
            this.txtNID.NumberFormat = "999-99-999999";
            this.txtNID.Postfix = "";
            this.txtNID.Prefix = "";
            this.txtNID.Size = new System.Drawing.Size(181, 24);
            this.txtNID.SkipValidation = false;
            this.txtNID.TabIndex = 14;
            this.txtNID.TextType = CrplControlLibrary.TextType.DigitAndHyphen;
            this.toolTip1.SetToolTip(this.txtNID, "Formate of NID Card is 999-99-999999");
            this.txtNID.Validating += new System.ComponentModel.CancelEventHandler(this.txtNID_Validating);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(59, 415);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(139, 25);
            this.label23.TabIndex = 52;
            this.label23.Text = "N.I.D Card No.";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSex
            // 
            this.txtSex.AllowSpace = true;
            this.txtSex.AssociatedLookUpName = "";
            this.txtSex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSex.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSex.ContinuationTextBox = null;
            this.txtSex.CustomEnabled = true;
            this.txtSex.DataFieldMapping = "PR_SEX";
            this.txtSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSex.GetRecordsOnUpDownKeys = false;
            this.txtSex.IsDate = false;
            this.txtSex.IsRequired = true;
            this.txtSex.Location = new System.Drawing.Point(204, 383);
            this.txtSex.Margin = new System.Windows.Forms.Padding(4);
            this.txtSex.MaxLength = 1;
            this.txtSex.Name = "txtSex";
            this.txtSex.NumberFormat = "###,###,##0.00";
            this.txtSex.Postfix = "";
            this.txtSex.Prefix = "";
            this.txtSex.Size = new System.Drawing.Size(26, 24);
            this.txtSex.SkipValidation = false;
            this.txtSex.TabIndex = 13;
            this.txtSex.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSex, "[M]ale or [F]emale");
            this.txtSex.Validating += new System.ComponentModel.CancelEventHandler(this.txtSex_Validating);
            // 
            // txtMarital
            // 
            this.txtMarital.AllowSpace = true;
            this.txtMarital.AssociatedLookUpName = "";
            this.txtMarital.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarital.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarital.ContinuationTextBox = null;
            this.txtMarital.CustomEnabled = true;
            this.txtMarital.DataFieldMapping = "PR_MARITAL";
            this.txtMarital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarital.GetRecordsOnUpDownKeys = false;
            this.txtMarital.IsDate = false;
            this.txtMarital.IsRequired = true;
            this.txtMarital.Location = new System.Drawing.Point(204, 351);
            this.txtMarital.Margin = new System.Windows.Forms.Padding(4);
            this.txtMarital.MaxLength = 1;
            this.txtMarital.Name = "txtMarital";
            this.txtMarital.NumberFormat = "###,###,##0.00";
            this.txtMarital.Postfix = "";
            this.txtMarital.Prefix = "";
            this.txtMarital.Size = new System.Drawing.Size(26, 24);
            this.txtMarital.SkipValidation = false;
            this.txtMarital.TabIndex = 12;
            this.txtMarital.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtMarital, "[M]arried or [S]ingle");
            this.txtMarital.Validating += new System.ComponentModel.CancelEventHandler(this.txtMarital_Validating);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(59, 351);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(139, 25);
            this.label17.TabIndex = 43;
            this.label17.Text = "Marital";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(59, 319);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(139, 25);
            this.label19.TabIndex = 40;
            this.label19.Text = "Date Of Birth";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPh1
            // 
            this.txtPh1.AllowSpace = true;
            this.txtPh1.AssociatedLookUpName = "";
            this.txtPh1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPh1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPh1.ContinuationTextBox = null;
            this.txtPh1.CustomEnabled = true;
            this.txtPh1.DataFieldMapping = "PR_PHONE_1";
            this.txtPh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPh1.GetRecordsOnUpDownKeys = false;
            this.txtPh1.IsDate = false;
            this.txtPh1.Location = new System.Drawing.Point(204, 287);
            this.txtPh1.Margin = new System.Windows.Forms.Padding(4);
            this.txtPh1.MaxLength = 8;
            this.txtPh1.Name = "txtPh1";
            this.txtPh1.NumberFormat = "###,###,##0.00";
            this.txtPh1.Postfix = "";
            this.txtPh1.Prefix = "";
            this.txtPh1.Size = new System.Drawing.Size(101, 24);
            this.txtPh1.SkipValidation = false;
            this.txtPh1.TabIndex = 9;
            this.txtPh1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(59, 287);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(139, 25);
            this.label15.TabIndex = 37;
            this.label15.Text = "Phone";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(59, 191);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 25);
            this.label12.TabIndex = 34;
            this.label12.Text = "Contractor";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAddress
            // 
            this.txtAddress.AllowSpace = true;
            this.txtAddress.AssociatedLookUpName = "";
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAddress.ContinuationTextBox = null;
            this.txtAddress.CustomEnabled = true;
            this.txtAddress.DataFieldMapping = "PR_ADDRESS";
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.GetRecordsOnUpDownKeys = false;
            this.txtAddress.IsDate = false;
            this.txtAddress.Location = new System.Drawing.Point(204, 255);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddress.MaxLength = 50;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.NumberFormat = "###,###,##0.00";
            this.txtAddress.Postfix = "";
            this.txtAddress.Prefix = "";
            this.txtAddress.Size = new System.Drawing.Size(198, 24);
            this.txtAddress.SkipValidation = false;
            this.txtAddress.TabIndex = 8;
            this.txtAddress.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(59, 255);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(139, 25);
            this.label11.TabIndex = 32;
            this.label11.Text = "Address";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomEnabled = true;
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.DataFieldMapping = "PR_FROM_DATE";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.HasChanges = true;
            this.dtpFromDate.IsRequired = true;
            this.dtpFromDate.Location = new System.Drawing.Point(204, 223);
            this.dtpFromDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.NullValue = " ";
            this.dtpFromDate.Size = new System.Drawing.Size(116, 22);
            this.dtpFromDate.TabIndex = 6;
            this.dtpFromDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(59, 223);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 25);
            this.label10.TabIndex = 30;
            this.label10.Text = "From / To";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDesg
            // 
            this.txtDesg.AllowSpace = true;
            this.txtDesg.AssociatedLookUpName = "lbtnDesg";
            this.txtDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesg.ContinuationTextBox = null;
            this.txtDesg.CustomEnabled = true;
            this.txtDesg.DataFieldMapping = "PR_DESIG";
            this.txtDesg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesg.GetRecordsOnUpDownKeys = false;
            this.txtDesg.IsDate = false;
            this.txtDesg.IsRequired = true;
            this.txtDesg.Location = new System.Drawing.Point(204, 159);
            this.txtDesg.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesg.MaxLength = 3;
            this.txtDesg.Name = "txtDesg";
            this.txtDesg.NumberFormat = "###,###,##0.00";
            this.txtDesg.Postfix = "";
            this.txtDesg.Prefix = "";
            this.txtDesg.Size = new System.Drawing.Size(65, 24);
            this.txtDesg.SkipValidation = false;
            this.txtDesg.TabIndex = 4;
            this.txtDesg.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtDesg, "Enter Valid Designation Or Press [F9] Key For Help");
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(59, 159);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 25);
            this.label9.TabIndex = 28;
            this.label9.Text = "Designation";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.IsRequired = true;
            this.txtFirstName.Location = new System.Drawing.Point(204, 95);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 20;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(198, 24);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(59, 95);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 25);
            this.label8.TabIndex = 26;
            this.label8.Text = "First Name";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PR_P_NO_LOV_EXISTS";
            this.lbtnPNo.ActionType = "PR_P_NO_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "PR_LAST_NAME|PR_BRANCH|PR_DESIG|PR_CONTRACTOR|PR_FROM_DATE|PR_TO_DATE|PR_ADDRESS|" +
    "PR_PHONE_1|PR_PHONE_2|PR_DATE_BIRTH|PR_MARITAL|PR_SEX|PR_NID|PR_MONTHLY_SALARY|P" +
    "R_FLAG";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(369, 31);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Margin = new System.Windows.Forms.Padding(4);
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_CONTRACT_MANAGER";
            this.lbtnPNo.TabIndex = 0;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            this.lbtnPNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(204, 31);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(155, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Enter Personnel No.  [F9]= For Help");
            this.txtPersNo.Enter += new System.EventHandler(this.txtPersNo_Enter);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(59, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 25);
            this.label7.TabIndex = 23;
            this.label7.Text = "Personnel No.";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(204, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(4);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(47, 24);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // pnlDept
            // 
            this.pnlDept.ConcurrentPanels = null;
            this.pnlDept.Controls.Add(this.dgvDept);
            this.pnlDept.DataManager = null;
            this.pnlDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.NonIsolated;
            this.pnlDept.DependentPanels = null;
            this.pnlDept.DisableDependentLoad = false;
            this.pnlDept.EnableDelete = true;
            this.pnlDept.EnableInsert = true;
            this.pnlDept.EnableQuery = false;
            this.pnlDept.EnableUpdate = true;
            this.pnlDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.pnlDept.Location = new System.Drawing.Point(499, 226);
            this.pnlDept.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDept.MasterPanel = null;
            this.pnlDept.Name = "pnlDept";
            this.pnlDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDept.Size = new System.Drawing.Size(321, 217);
            this.pnlDept.SPName = "CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER";
            this.pnlDept.TabIndex = 2;
            this.pnlDept.TabStop = true;
            // 
            // dgvDept
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDept.ColumnHeadersHeight = 28;
            this.dgvDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSeg,
            this.colDept,
            this.colContribution,
            this.colType,
            this.colDNo});
            this.dgvDept.ColumnToHide = null;
            this.dgvDept.ColumnWidth = null;
            this.dgvDept.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDept.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDept.DisplayColumnWrapper = null;
            this.dgvDept.GridDefaultRow = 0;
            this.dgvDept.Location = new System.Drawing.Point(5, 0);
            this.dgvDept.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDept.Name = "dgvDept";
            this.dgvDept.ReadOnlyColumns = null;
            this.dgvDept.RequiredColumns = "COLDEPT,COLCONTRIBUTION,COLSEG";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDept.Size = new System.Drawing.Size(312, 217);
            this.dgvDept.SkippingColumns = null;
            this.dgvDept.TabIndex = 0;
            this.dgvDept.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDept_CellEnter);
            this.dgvDept.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDept_CellValidating);
            this.dgvDept.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvDept_EditingControlShowing);
            this.dgvDept.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.dgvDept_PreviewKeyDown);
            // 
            // colSeg
            // 
            this.colSeg.DataPropertyName = "PR_SEGMENT";
            this.colSeg.HeaderText = "Segment";
            this.colSeg.MaxInputLength = 3;
            this.colSeg.Name = "colSeg";
            this.colSeg.ToolTipText = "Valid Segments are GF or GCB";
            this.colSeg.Width = 60;
            // 
            // colDept
            // 
            this.colDept.ActionLOV = "DEPT_LOV";
            this.colDept.ActionLOVExists = "DEPT_LOV_EXISTS";
            this.colDept.AttachParentEntity = false;
            this.colDept.DataPropertyName = "PR_DEPT";
            this.colDept.EntityName = null;
            this.colDept.HeaderText = "Dept.";
            this.colDept.LookUpTitle = null;
            this.colDept.LOVFieldMapping = "PR_DEPT";
            this.colDept.Name = "colDept";
            this.colDept.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDept.SearchColumn = "PR_DEPT";
            this.colDept.SkipValidationOnLeave = false;
            this.colDept.SpName = "CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER";
            this.colDept.ToolTipText = "Press [F9] to Display The List";
            this.colDept.Width = 80;
            // 
            // colContribution
            // 
            this.colContribution.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colContribution.DataPropertyName = "PR_CONTRIB";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.colContribution.DefaultCellStyle = dataGridViewCellStyle2;
            this.colContribution.HeaderText = "Contri. %";
            this.colContribution.MaxInputLength = 3;
            this.colContribution.Name = "colContribution";
            this.colContribution.ToolTipText = "If The Contribution Is Equal To 100 Then Press [F10] To Save The Record";
            // 
            // colType
            // 
            this.colType.DataPropertyName = "PR_TYPE";
            this.colType.HeaderText = "Type";
            this.colType.Name = "colType";
            this.colType.Visible = false;
            // 
            // colDNo
            // 
            this.colDNo.DataPropertyName = "PR_P_NO";
            this.colDNo.HeaderText = "D. No.";
            this.colDNo.Name = "colDNo";
            this.colDNo.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(444, 16);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 17);
            this.label16.TabIndex = 10;
            this.label16.Text = "User Name: ";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(543, 16);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 17);
            this.lblUserName.TabIndex = 11;
            // 
            // CHRIS_Personnel_ContractStaffHiringEnt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 767);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.pnlDept);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlDetail";
            this.currentQueryMode = iCORE.Common.Enumerations.eQueryMode.ExecuteQuery;
            this.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.Name = "CHRIS_Personnel_ContractStaffHiringEnt";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRISS - Contractuals Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_ContractStafHiringEnt_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlDept, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtSalary;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtNID;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtSex;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox txtMarital;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtPh1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtAddress;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLDatePicker dtpFromDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtDesg;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label7;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlDept;
        private CrplControlLibrary.LookupButton lbtnBranch;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtContractorName;
        private CrplControlLibrary.SLTextBox txtLastName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLDatePicker dtpToDate;
        private CrplControlLibrary.SLTextBox txtPh2;
        private CrplControlLibrary.SLDatePicker dtpDOB;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDept;
        private CrplControlLibrary.LookupButton lbtnDesg;
        private CrplControlLibrary.LookupButton lbtnContractor;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox txtContractor1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeg;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn colDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContribution;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDNo;
        private CrplControlLibrary.LookupButton lkpBtnAuthorize;
        private CrplControlLibrary.SLTextBox slTxtBxUserID;
        private System.Windows.Forms.Label label18;
    }
}