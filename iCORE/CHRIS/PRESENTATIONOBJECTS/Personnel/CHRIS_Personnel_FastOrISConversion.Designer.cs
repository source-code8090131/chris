namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_FastOrISConversion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_FastOrISConversion));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtMode = new System.Windows.Forms.TextBox();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtCurrOption = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.terminDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.transferDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.joinDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtprcloseFlag = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtPR_REMARKS = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txtFurLoughCity = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txtISCordinate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_EFFECTIVE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.txtCurrAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPRDept = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.txtDesg = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txtCity = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtCountry = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtPR_Func2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtPR_Func1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESG = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPrTransfer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 588);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtMode);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Location = new System.Drawing.Point(12, 85);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(625, 74);
            this.pnlHead.TabIndex = 12;
            // 
            // txtMode
            // 
            this.txtMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMode.Location = new System.Drawing.Point(158, 44);
            this.txtMode.MaxLength = 10;
            this.txtMode.Name = "txtMode";
            this.txtMode.ReadOnly = true;
            this.txtMode.Size = new System.Drawing.Size(39, 20);
            this.txtMode.TabIndex = 21;
            this.txtMode.TabStop = false;
            this.txtMode.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(534, 46);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.Location = new System.Drawing.Point(534, 19);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(488, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(483, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(75, 45);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(75, 16);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(2, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(142, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(347, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "PERSONNEL SYSTEM";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(155, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(347, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "FAST TO IS";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.terminDate);
            this.pnlDetail.Controls.Add(this.transferDate);
            this.pnlDetail.Controls.Add(this.joinDate);
            this.pnlDetail.Controls.Add(this.label25);
            this.pnlDetail.Controls.Add(this.slTextBox1);
            this.pnlDetail.Controls.Add(this.slTextBox2);
            this.pnlDetail.Controls.Add(this.dtDate);
            this.pnlDetail.Controls.Add(this.txtprcloseFlag);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtPR_REMARKS);
            this.pnlDetail.Controls.Add(this.label22);
            this.pnlDetail.Controls.Add(this.txtFurLoughCity);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.txtISCordinate);
            this.pnlDetail.Controls.Add(this.txtPR_EFFECTIVE);
            this.pnlDetail.Controls.Add(this.label24);
            this.pnlDetail.Controls.Add(this.txtCurrAnnual);
            this.pnlDetail.Controls.Add(this.label21);
            this.pnlDetail.Controls.Add(this.label20);
            this.pnlDetail.Controls.Add(this.txtPRDept);
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.Controls.Add(this.txtDesg);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.txtCity);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtCountry);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtPR_Func2);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.txtPR_Func1);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtPR_LEVEL);
            this.pnlDetail.Controls.Add(this.txtPR_DESG);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtPrTransfer);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 163);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(625, 414);
            this.pnlDetail.SPName = "CHRIS_SP_CONVERSION_TRANSFER_MANAGER";
            this.pnlDetail.TabIndex = 13;
            this.pnlDetail.TabStop = true;
            // 
            // terminDate
            // 
            this.terminDate.CustomEnabled = true;
            this.terminDate.CustomFormat = "dd/MM/yyyy";
            this.terminDate.DataFieldMapping = "PR_TERMIN_DATE";
            this.terminDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.terminDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.terminDate.HasChanges = true;
            this.terminDate.Location = new System.Drawing.Point(178, 132);
            this.terminDate.Name = "terminDate";
            this.terminDate.NullValue = " ";
            this.terminDate.Size = new System.Drawing.Size(100, 20);
            this.terminDate.TabIndex = 149;
            this.terminDate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.terminDate.Visible = false;
            // 
            // transferDate
            // 
            this.transferDate.CustomEnabled = true;
            this.transferDate.CustomFormat = "dd/MM/yyyy";
            this.transferDate.DataFieldMapping = "PR_TRANSFER_DATE";
            this.transferDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.transferDate.HasChanges = true;
            this.transferDate.Location = new System.Drawing.Point(390, 131);
            this.transferDate.Name = "transferDate";
            this.transferDate.NullValue = " ";
            this.transferDate.Size = new System.Drawing.Size(100, 20);
            this.transferDate.TabIndex = 148;
            this.transferDate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.transferDate.Visible = false;
            // 
            // joinDate
            // 
            this.joinDate.CustomEnabled = true;
            this.joinDate.CustomFormat = "dd/MM/yyyy";
            this.joinDate.DataFieldMapping = "PR_JOINING_DATE";
            this.joinDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.joinDate.HasChanges = true;
            this.joinDate.Location = new System.Drawing.Point(284, 131);
            this.joinDate.Name = "joinDate";
            this.joinDate.NullValue = " ";
            this.joinDate.Size = new System.Drawing.Size(100, 20);
            this.joinDate.TabIndex = 147;
            this.joinDate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.joinDate.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 146);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(602, 13);
            this.label25.TabIndex = 146;
            this.label25.Text = "_________________________________________________________________________________" +
                "____";
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "PRName1";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(440, 21);
            this.slTextBox1.MaxLength = 20;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(116, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 145;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(534, 103);
            this.slTextBox2.MaxLength = 10;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(62, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 144;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.DateTime;
            this.slTextBox2.Visible = false;
            // 
            // dtDate
            // 
            this.dtDate.AllowSpace = true;
            this.dtDate.AssociatedLookUpName = "";
            this.dtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dtDate.ContinuationTextBox = null;
            this.dtDate.CustomEnabled = true;
            this.dtDate.DataFieldMapping = "";
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.GetRecordsOnUpDownKeys = false;
            this.dtDate.IsDate = false;
            this.dtDate.Location = new System.Drawing.Point(528, 80);
            this.dtDate.MaxLength = 10;
            this.dtDate.Name = "dtDate";
            this.dtDate.NumberFormat = "###,###,##0.00";
            this.dtDate.Postfix = "";
            this.dtDate.Prefix = "";
            this.dtDate.Size = new System.Drawing.Size(68, 20);
            this.dtDate.SkipValidation = false;
            this.dtDate.TabIndex = 143;
            this.dtDate.TabStop = false;
            this.dtDate.TextType = CrplControlLibrary.TextType.DateTime;
            this.dtDate.Visible = false;
            // 
            // txtprcloseFlag
            // 
            this.txtprcloseFlag.AllowSpace = true;
            this.txtprcloseFlag.AssociatedLookUpName = "";
            this.txtprcloseFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprcloseFlag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtprcloseFlag.ContinuationTextBox = null;
            this.txtprcloseFlag.CustomEnabled = true;
            this.txtprcloseFlag.DataFieldMapping = "PR_CLOSE_FLAG";
            this.txtprcloseFlag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprcloseFlag.GetRecordsOnUpDownKeys = false;
            this.txtprcloseFlag.IsDate = false;
            this.txtprcloseFlag.Location = new System.Drawing.Point(473, 103);
            this.txtprcloseFlag.MaxLength = 1;
            this.txtprcloseFlag.Name = "txtprcloseFlag";
            this.txtprcloseFlag.NumberFormat = "###,###,##0.00";
            this.txtprcloseFlag.Postfix = "";
            this.txtprcloseFlag.Prefix = "";
            this.txtprcloseFlag.Size = new System.Drawing.Size(39, 20);
            this.txtprcloseFlag.SkipValidation = false;
            this.txtprcloseFlag.TabIndex = 142;
            this.txtprcloseFlag.TabStop = false;
            this.txtprcloseFlag.TextType = CrplControlLibrary.TextType.String;
            this.txtprcloseFlag.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(89, 353);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 15);
            this.label23.TabIndex = 140;
            this.label23.Text = "Remarks :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_REMARKS
            // 
            this.txtPR_REMARKS.AllowSpace = true;
            this.txtPR_REMARKS.AssociatedLookUpName = "";
            this.txtPR_REMARKS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_REMARKS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_REMARKS.ContinuationTextBox = null;
            this.txtPR_REMARKS.CustomEnabled = true;
            this.txtPR_REMARKS.DataFieldMapping = "PR_REMARKS";
            this.txtPR_REMARKS.Enabled = false;
            this.txtPR_REMARKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_REMARKS.GetRecordsOnUpDownKeys = false;
            this.txtPR_REMARKS.IsDate = false;
            this.txtPR_REMARKS.Location = new System.Drawing.Point(153, 348);
            this.txtPR_REMARKS.MaxLength = 30;
            this.txtPR_REMARKS.Name = "txtPR_REMARKS";
            this.txtPR_REMARKS.NumberFormat = "###,###,##0.00";
            this.txtPR_REMARKS.Postfix = "";
            this.txtPR_REMARKS.Prefix = "";
            this.txtPR_REMARKS.Size = new System.Drawing.Size(269, 20);
            this.txtPR_REMARKS.SkipValidation = false;
            this.txtPR_REMARKS.TabIndex = 11;
            this.txtPR_REMARKS.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_REMARKS, "<F6> Exit W/O Save");
            this.txtPR_REMARKS.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPR_REMARKS_PreviewKeyDown);
            this.txtPR_REMARKS.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_REMARKS_Validating);
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(50, 322);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 20);
            this.label22.TabIndex = 138;
            this.label22.Text = "Furlough City :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFurLoughCity
            // 
            this.txtFurLoughCity.AllowSpace = true;
            this.txtFurLoughCity.AssociatedLookUpName = "";
            this.txtFurLoughCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFurLoughCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFurLoughCity.ContinuationTextBox = null;
            this.txtFurLoughCity.CustomEnabled = true;
            this.txtFurLoughCity.DataFieldMapping = "PR_FURLOUGH";
            this.txtFurLoughCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFurLoughCity.GetRecordsOnUpDownKeys = false;
            this.txtFurLoughCity.IsDate = false;
            this.txtFurLoughCity.Location = new System.Drawing.Point(153, 322);
            this.txtFurLoughCity.MaxLength = 15;
            this.txtFurLoughCity.Name = "txtFurLoughCity";
            this.txtFurLoughCity.NumberFormat = "###,###,##0.00";
            this.txtFurLoughCity.Postfix = "";
            this.txtFurLoughCity.Prefix = "";
            this.txtFurLoughCity.Size = new System.Drawing.Size(109, 20);
            this.txtFurLoughCity.SkipValidation = false;
            this.txtFurLoughCity.TabIndex = 10;
            this.txtFurLoughCity.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtFurLoughCity, "<F6> Exit W/O Save");
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(334, 296);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(109, 20);
            this.label17.TabIndex = 136;
            this.label17.Text = "IS Co-ordinate :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtISCordinate
            // 
            this.txtISCordinate.AllowSpace = true;
            this.txtISCordinate.AssociatedLookUpName = "";
            this.txtISCordinate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtISCordinate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtISCordinate.ContinuationTextBox = null;
            this.txtISCordinate.CustomEnabled = true;
            this.txtISCordinate.DataFieldMapping = "PR_IS_COORDINAT";
            this.txtISCordinate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtISCordinate.GetRecordsOnUpDownKeys = false;
            this.txtISCordinate.IsDate = false;
            this.txtISCordinate.Location = new System.Drawing.Point(447, 296);
            this.txtISCordinate.MaxLength = 15;
            this.txtISCordinate.Name = "txtISCordinate";
            this.txtISCordinate.NumberFormat = "###,###,##0.00";
            this.txtISCordinate.Postfix = "";
            this.txtISCordinate.Prefix = "";
            this.txtISCordinate.Size = new System.Drawing.Size(109, 20);
            this.txtISCordinate.SkipValidation = false;
            this.txtISCordinate.TabIndex = 9;
            this.txtISCordinate.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtISCordinate, "<F6> Exit W/O Save");
            // 
            // txtPR_EFFECTIVE
            // 
            this.txtPR_EFFECTIVE.CustomEnabled = true;
            this.txtPR_EFFECTIVE.CustomFormat = "dd/MM/yyyy";
            this.txtPR_EFFECTIVE.DataFieldMapping = "PR_FAST_CONVER";
            this.txtPR_EFFECTIVE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_EFFECTIVE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtPR_EFFECTIVE.HasChanges = true;
            this.txtPR_EFFECTIVE.Location = new System.Drawing.Point(153, 296);
            this.txtPR_EFFECTIVE.Name = "txtPR_EFFECTIVE";
            this.txtPR_EFFECTIVE.NullValue = " ";
            this.txtPR_EFFECTIVE.Size = new System.Drawing.Size(100, 20);
            this.txtPR_EFFECTIVE.TabIndex = 8;
            this.toolTip1.SetToolTip(this.txtPR_EFFECTIVE, "<F6> Exit W/O Save");
            this.txtPR_EFFECTIVE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.txtPR_EFFECTIVE.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_EFFECTIVE_Validating);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(46, 298);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(120, 15);
            this.label24.TabIndex = 134;
            this.label24.Text = "Conversion Date :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCurrAnnual
            // 
            this.txtCurrAnnual.AllowSpace = true;
            this.txtCurrAnnual.AssociatedLookUpName = "";
            this.txtCurrAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrAnnual.ContinuationTextBox = null;
            this.txtCurrAnnual.CustomEnabled = true;
            this.txtCurrAnnual.DataFieldMapping = "PR_ASR_DOL";
            this.txtCurrAnnual.Enabled = false;
            this.txtCurrAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrAnnual.GetRecordsOnUpDownKeys = false;
            this.txtCurrAnnual.IsDate = false;
            this.txtCurrAnnual.Location = new System.Drawing.Point(447, 270);
            this.txtCurrAnnual.MaxLength = 7;
            this.txtCurrAnnual.Name = "txtCurrAnnual";
            this.txtCurrAnnual.NumberFormat = "###,###,##0.00";
            this.txtCurrAnnual.Postfix = "";
            this.txtCurrAnnual.Prefix = "";
            this.txtCurrAnnual.Size = new System.Drawing.Size(100, 20);
            this.txtCurrAnnual.SkipValidation = false;
            this.txtCurrAnnual.TabIndex = 7;
            this.txtCurrAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrAnnual.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtCurrAnnual, "ASR Can Be Zero   <F6> Exit W/O Save");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(296, 273);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(169, 15);
            this.label21.TabIndex = 132;
            this.label21.Text = "Current Annual Package :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(72, 272);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 15);
            this.label20.TabIndex = 130;
            this.label20.Text = "Department :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRDept
            // 
            this.txtPRDept.AllowSpace = true;
            this.txtPRDept.AssociatedLookUpName = "";
            this.txtPRDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRDept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRDept.ContinuationTextBox = null;
            this.txtPRDept.CustomEnabled = true;
            this.txtPRDept.DataFieldMapping = "PR_DEPARTMENT_HC";
            this.txtPRDept.Enabled = false;
            this.txtPRDept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRDept.GetRecordsOnUpDownKeys = false;
            this.txtPRDept.IsDate = false;
            this.txtPRDept.Location = new System.Drawing.Point(153, 270);
            this.txtPRDept.MaxLength = 30;
            this.txtPRDept.Name = "txtPRDept";
            this.txtPRDept.NumberFormat = "###,###,##0.00";
            this.txtPRDept.Postfix = "";
            this.txtPRDept.Prefix = "";
            this.txtPRDept.Size = new System.Drawing.Size(100, 20);
            this.txtPRDept.SkipValidation = false;
            this.txtPRDept.TabIndex = 6;
            this.txtPRDept.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPRDept, "Department Can Be Blank <F6> Exit W/O Save");
            this.txtPRDept.Validating += new System.ComponentModel.CancelEventHandler(this.txtPRDept_Validating);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(70, 247);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 15);
            this.label18.TabIndex = 127;
            this.label18.Text = "Designation :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Enabled = false;
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.IsRequired = true;
            this.txtLevel.Location = new System.Drawing.Point(447, 242);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(61, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 5;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtLevel, "Enter Any Value  <F6> Exit W/O Save");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(394, 244);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 15);
            this.label19.TabIndex = 128;
            this.label19.Text = "Level :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDesg
            // 
            this.txtDesg.AllowSpace = true;
            this.txtDesg.AssociatedLookUpName = "";
            this.txtDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesg.ContinuationTextBox = null;
            this.txtDesg.CustomEnabled = true;
            this.txtDesg.DataFieldMapping = "PR_DESG";
            this.txtDesg.Enabled = false;
            this.txtDesg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesg.GetRecordsOnUpDownKeys = false;
            this.txtDesg.IsDate = false;
            this.txtDesg.IsRequired = true;
            this.txtDesg.Location = new System.Drawing.Point(154, 244);
            this.txtDesg.MaxLength = 3;
            this.txtDesg.Name = "txtDesg";
            this.txtDesg.NumberFormat = "###,###,##0.00";
            this.txtDesg.Postfix = "";
            this.txtDesg.Prefix = "";
            this.txtDesg.Size = new System.Drawing.Size(100, 20);
            this.txtDesg.SkipValidation = false;
            this.txtDesg.TabIndex = 4;
            this.txtDesg.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtDesg, "Enter Any Value  <F6> Exit W/O Save");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(117, 220);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 15);
            this.label15.TabIndex = 124;
            this.label15.Text = "City :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCity
            // 
            this.txtCity.AllowSpace = true;
            this.txtCity.AssociatedLookUpName = "";
            this.txtCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.ContinuationTextBox = null;
            this.txtCity.CustomEnabled = true;
            this.txtCity.DataFieldMapping = "PR_CITY";
            this.txtCity.Enabled = false;
            this.txtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.GetRecordsOnUpDownKeys = false;
            this.txtCity.IsDate = false;
            this.txtCity.IsRequired = true;
            this.txtCity.Location = new System.Drawing.Point(154, 218);
            this.txtCity.MaxLength = 20;
            this.txtCity.Name = "txtCity";
            this.txtCity.NumberFormat = "###,###,##0.00";
            this.txtCity.Postfix = "";
            this.txtCity.Prefix = "";
            this.txtCity.Size = new System.Drawing.Size(139, 20);
            this.txtCity.SkipValidation = false;
            this.txtCity.TabIndex = 3;
            this.txtCity.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtCity, "<F6> Exit W/O Save");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(95, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 15);
            this.label10.TabIndex = 121;
            this.label10.Text = "Country :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCountry
            // 
            this.txtCountry.AllowSpace = true;
            this.txtCountry.AssociatedLookUpName = "";
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountry.ContinuationTextBox = null;
            this.txtCountry.CustomEnabled = true;
            this.txtCountry.DataFieldMapping = "PR_COUNTRY";
            this.txtCountry.Enabled = false;
            this.txtCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountry.GetRecordsOnUpDownKeys = false;
            this.txtCountry.IsDate = false;
            this.txtCountry.IsRequired = true;
            this.txtCountry.Location = new System.Drawing.Point(155, 192);
            this.txtCountry.MaxLength = 20;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.NumberFormat = "###,###,##0.00";
            this.txtCountry.Postfix = "";
            this.txtCountry.Prefix = "";
            this.txtCountry.Size = new System.Drawing.Size(138, 20);
            this.txtCountry.SkipValidation = false;
            this.txtCountry.TabIndex = 2;
            this.txtCountry.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtCountry, "<F6> Exit W/O Save");
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(6, 169);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(614, 20);
            this.label9.TabIndex = 109;
            this.label9.Text = "Transfer Information";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtPR_Func2
            // 
            this.txtPR_Func2.AllowSpace = true;
            this.txtPR_Func2.AssociatedLookUpName = "";
            this.txtPR_Func2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func2.ContinuationTextBox = null;
            this.txtPR_Func2.CustomEnabled = true;
            this.txtPR_Func2.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txtPR_Func2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func2.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func2.IsDate = false;
            this.txtPR_Func2.Location = new System.Drawing.Point(151, 105);
            this.txtPR_Func2.MaxLength = 30;
            this.txtPR_Func2.Name = "txtPR_Func2";
            this.txtPR_Func2.NumberFormat = "###,###,##0.00";
            this.txtPR_Func2.Postfix = "";
            this.txtPR_Func2.Prefix = "";
            this.txtPR_Func2.ReadOnly = true;
            this.txtPR_Func2.Size = new System.Drawing.Size(206, 20);
            this.txtPR_Func2.SkipValidation = false;
            this.txtPR_Func2.TabIndex = 108;
            this.txtPR_Func2.TabStop = false;
            this.txtPR_Func2.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_Func2, "[M]arried or [S]ingle");
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(14, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 20);
            this.label7.TabIndex = 107;
            this.label7.Text = "Title :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func1
            // 
            this.txtPR_Func1.AllowSpace = true;
            this.txtPR_Func1.AssociatedLookUpName = "";
            this.txtPR_Func1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func1.ContinuationTextBox = null;
            this.txtPR_Func1.CustomEnabled = true;
            this.txtPR_Func1.DataFieldMapping = "PR_FUNC_TITTLE1";
            this.txtPR_Func1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func1.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func1.IsDate = false;
            this.txtPR_Func1.Location = new System.Drawing.Point(151, 78);
            this.txtPR_Func1.MaxLength = 30;
            this.txtPR_Func1.Name = "txtPR_Func1";
            this.txtPR_Func1.NumberFormat = "###,###,##0.00";
            this.txtPR_Func1.Postfix = "";
            this.txtPR_Func1.Prefix = "";
            this.txtPR_Func1.ReadOnly = true;
            this.txtPR_Func1.Size = new System.Drawing.Size(206, 20);
            this.txtPR_Func1.SkipValidation = false;
            this.txtPR_Func1.TabIndex = 88;
            this.txtPR_Func1.TabStop = false;
            this.txtPR_Func1.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_Func1, "[M]arried or [S]ingle");
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(14, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(137, 20);
            this.label14.TabIndex = 87;
            this.label14.Text = "5.Functional :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(360, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 20);
            this.label12.TabIndex = 86;
            this.label12.Text = "6.Transfer Type :";
            // 
            // txtPR_LEVEL
            // 
            this.txtPR_LEVEL.AllowSpace = true;
            this.txtPR_LEVEL.AssociatedLookUpName = "";
            this.txtPR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL.ContinuationTextBox = null;
            this.txtPR_LEVEL.CustomEnabled = true;
            this.txtPR_LEVEL.DataFieldMapping = "P_PR_LEVEL";
            this.txtPR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL.IsDate = false;
            this.txtPR_LEVEL.Location = new System.Drawing.Point(477, 50);
            this.txtPR_LEVEL.MaxLength = 3;
            this.txtPR_LEVEL.Name = "txtPR_LEVEL";
            this.txtPR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL.Postfix = "";
            this.txtPR_LEVEL.Prefix = "";
            this.txtPR_LEVEL.ReadOnly = true;
            this.txtPR_LEVEL.Size = new System.Drawing.Size(39, 20);
            this.txtPR_LEVEL.SkipValidation = false;
            this.txtPR_LEVEL.TabIndex = 84;
            this.txtPR_LEVEL.TabStop = false;
            this.txtPR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPR_DESG
            // 
            this.txtPR_DESG.AllowSpace = true;
            this.txtPR_DESG.AssociatedLookUpName = "lbtnPNo";
            this.txtPR_DESG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESG.ContinuationTextBox = null;
            this.txtPR_DESG.CustomEnabled = true;
            this.txtPR_DESG.DataFieldMapping = "P_PR_DESIG";
            this.txtPR_DESG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESG.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESG.IsDate = false;
            this.txtPR_DESG.Location = new System.Drawing.Point(151, 50);
            this.txtPR_DESG.MaxLength = 10;
            this.txtPR_DESG.Name = "txtPR_DESG";
            this.txtPR_DESG.NumberFormat = "###,###,##0.00";
            this.txtPR_DESG.Postfix = "";
            this.txtPR_DESG.Prefix = "";
            this.txtPR_DESG.ReadOnly = true;
            this.txtPR_DESG.Size = new System.Drawing.Size(59, 20);
            this.txtPR_DESG.SkipValidation = false;
            this.txtPR_DESG.TabIndex = 80;
            this.txtPR_DESG.TabStop = false;
            this.txtPR_DESG.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPR_DESG, "Press <F9> Key to Display The List");
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(30, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 20);
            this.label13.TabIndex = 82;
            this.label13.Text = "3.Designation :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(360, 50);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 20);
            this.label16.TabIndex = 69;
            this.label16.Text = "4.Level:";
            // 
            // txtPrTransfer
            // 
            this.txtPrTransfer.AllowSpace = true;
            this.txtPrTransfer.AssociatedLookUpName = "";
            this.txtPrTransfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrTransfer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrTransfer.ContinuationTextBox = null;
            this.txtPrTransfer.CustomEnabled = true;
            this.txtPrTransfer.DataFieldMapping = "PR_TRANSFER";
            this.txtPrTransfer.Enabled = false;
            this.txtPrTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrTransfer.GetRecordsOnUpDownKeys = false;
            this.txtPrTransfer.IsDate = false;
            this.txtPrTransfer.Location = new System.Drawing.Point(477, 78);
            this.txtPrTransfer.MaxLength = 1;
            this.txtPrTransfer.Name = "txtPrTransfer";
            this.txtPrTransfer.NumberFormat = "###,###,##0.00";
            this.txtPrTransfer.Postfix = "";
            this.txtPrTransfer.Prefix = "";
            this.txtPrTransfer.ReadOnly = true;
            this.txtPrTransfer.Size = new System.Drawing.Size(39, 20);
            this.txtPrTransfer.SkipValidation = false;
            this.txtPrTransfer.TabIndex = 68;
            this.txtPrTransfer.TabStop = false;
            this.txtPrTransfer.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PRName";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(305, 21);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(131, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(247, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "2.Name:";
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "Pr_P_NO_LOV_Edit_Exists";
            this.lbtnPNo.ActionType = "Pr_P_NO_LOV_Edit";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = false;
            this.lbtnPNo.Location = new System.Drawing.Point(215, 23);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_CONVERSION_TRANSFER_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_TR_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(151, 23);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(59, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Double;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(30, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "1.Pr No. :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(151, 23);
            this.txtID.MaxLength = 30;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(36, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 67;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(385, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 120;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Personnel_FastOrISConversion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(644, 648);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Personnel_FastOrISConversion";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Personnel_FastToISConversion";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_FastOrISConversion_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtPR_Func2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtPR_Func1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtPR_LEVEL;
        private CrplControlLibrary.SLTextBox txtPR_DESG;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtPrTransfer;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtID;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtCountry;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtCity;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtLevel;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtDesg;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtPRDept;
        private CrplControlLibrary.SLTextBox txtCurrAnnual;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLDatePicker txtPR_EFFECTIVE;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtISCordinate;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txtFurLoughCity;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtPR_REMARKS;
        private CrplControlLibrary.SLTextBox txtprcloseFlag;
        private CrplControlLibrary.SLTextBox dtDate;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMode;
        private CrplControlLibrary.SLDatePicker joinDate;
        private CrplControlLibrary.SLDatePicker transferDate;
        private CrplControlLibrary.SLDatePicker terminDate;
    }
}