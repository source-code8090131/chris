namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_IncrementOfficersClerical_Course
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_IncrementOfficersClerical_Course));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlDetails = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVCourse = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.CTT_YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_INSTITUTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTT_GRADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_IN_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_EFFECTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVCourse)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 358);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // PnlDetails
            // 
            this.PnlDetails.ConcurrentPanels = null;
            this.PnlDetails.Controls.Add(this.DGVCourse);
            this.PnlDetails.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlDetails.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetails.DependentPanels = null;
            this.PnlDetails.DisableDependentLoad = false;
            this.PnlDetails.EnableDelete = true;
            this.PnlDetails.EnableInsert = true;
            this.PnlDetails.EnableQuery = false;
            this.PnlDetails.EnableUpdate = true;
            this.PnlDetails.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncrementCourse_Command";
            this.PnlDetails.Location = new System.Drawing.Point(12, 87);
            this.PnlDetails.MasterPanel = null;
            this.PnlDetails.Name = "PnlDetails";
            this.PnlDetails.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetails.Size = new System.Drawing.Size(620, 240);
            this.PnlDetails.SPName = "CHRIS_SP_INCREMENT_PROMOTION_CTT_COURSE_MANAGER";
            this.PnlDetails.TabIndex = 12;
            // 
            // DGVCourse
            // 
            this.DGVCourse.AllowUserToAddRows = false;
            this.DGVCourse.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCourse.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVCourse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVCourse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CTT_YEAR,
            this.CTT_TITLE,
            this.CTT_INSTITUTE,
            this.CTT_GRADE,
            this.Column1,
            this.PR_IN_NO,
            this.PR_EFFECTIVE});
            this.DGVCourse.ColumnToHide = null;
            this.DGVCourse.ColumnWidth = null;
            this.DGVCourse.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVCourse.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGVCourse.DisplayColumnWrapper = null;
            this.DGVCourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVCourse.GridDefaultRow = 0;
            this.DGVCourse.Location = new System.Drawing.Point(0, 0);
            this.DGVCourse.Name = "DGVCourse";
            this.DGVCourse.ReadOnlyColumns = null;
            this.DGVCourse.RequiredColumns = null;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCourse.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGVCourse.Size = new System.Drawing.Size(620, 240);
            this.DGVCourse.SkippingColumns = null;
            this.DGVCourse.TabIndex = 0;
            this.DGVCourse.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVCourse_CellValidating);
            this.DGVCourse.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGVCourse_EditingControlShowing);
            // 
            // CTT_YEAR
            // 
            this.CTT_YEAR.DataPropertyName = "CTT_YEAR";
            dataGridViewCellStyle2.NullValue = null;
            this.CTT_YEAR.DefaultCellStyle = dataGridViewCellStyle2;
            this.CTT_YEAR.HeaderText = "YEAR";
            this.CTT_YEAR.MaxInputLength = 4;
            this.CTT_YEAR.Name = "CTT_YEAR";
            // 
            // CTT_TITLE
            // 
            this.CTT_TITLE.DataPropertyName = "CTT_TITLE";
            this.CTT_TITLE.HeaderText = "TITLE";
            this.CTT_TITLE.MaxInputLength = 15;
            this.CTT_TITLE.Name = "CTT_TITLE";
            // 
            // CTT_INSTITUTE
            // 
            this.CTT_INSTITUTE.DataPropertyName = "CTT_INSTITUTE";
            this.CTT_INSTITUTE.HeaderText = "INSTITUTION";
            this.CTT_INSTITUTE.MaxInputLength = 20;
            this.CTT_INSTITUTE.Name = "CTT_INSTITUTE";
            // 
            // CTT_GRADE
            // 
            this.CTT_GRADE.DataPropertyName = "CTT_GRADE";
            this.CTT_GRADE.HeaderText = "GRADE";
            this.CTT_GRADE.MaxInputLength = 10;
            this.CTT_GRADE.Name = "CTT_GRADE";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // PR_IN_NO
            // 
            this.PR_IN_NO.DataPropertyName = "PR_IN_NO";
            this.PR_IN_NO.HeaderText = "PR_IN_NO";
            this.PR_IN_NO.Name = "PR_IN_NO";
            this.PR_IN_NO.ReadOnly = true;
            this.PR_IN_NO.Visible = false;
            // 
            // PR_EFFECTIVE
            // 
            this.PR_EFFECTIVE.DataPropertyName = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.HeaderText = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.Name = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(136, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(347, 20);
            this.label5.TabIndex = 20;
            this.label5.Text = "COURSE(S) TAKEN && THEIR DESCRIPTION";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(12, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(620, 31);
            this.panel2.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 20);
            this.label11.TabIndex = 24;
            this.label11.Text = "<F8> To Save";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(222, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.TabIndex = 25;
            this.label1.Text = "<Enter> For Next Record";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(474, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "<F6> Exit W/O Save";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(12, 324);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(620, 34);
            this.panel3.TabIndex = 27;
            // 
            // CHRIS_Personnel_IncrementOfficersClerical_Course
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 418);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.PnlDetails);
            this.Name = "CHRIS_Personnel_IncrementOfficersClerical_Course";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_IncrementOfficersClerical_Course";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_IncrementOfficersClerical_Course_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDetails, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVCourse)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetails;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVCourse;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_TITLE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_INSTITUTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTT_GRADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_IN_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_EFFECTIVE;

    }
}