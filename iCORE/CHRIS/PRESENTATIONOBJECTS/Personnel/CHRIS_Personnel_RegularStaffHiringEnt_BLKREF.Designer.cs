namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKREF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt_BLKREF));
            this.pnlTblBlkRef = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkRef = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref_Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Add1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Add2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ref_Add3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblBlkRef.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkRef)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTblBlkRef
            // 
            this.pnlTblBlkRef.ConcurrentPanels = null;
            this.pnlTblBlkRef.Controls.Add(this.dgvBlkRef);
            this.pnlTblBlkRef.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBlkRef.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkRef.DependentPanels = null;
            this.pnlTblBlkRef.DisableDependentLoad = false;
            this.pnlTblBlkRef.EnableDelete = true;
            this.pnlTblBlkRef.EnableInsert = true;
            this.pnlTblBlkRef.EnableQuery = false;
            this.pnlTblBlkRef.EnableUpdate = true;
            this.pnlTblBlkRef.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntReferenceCommand";
            this.pnlTblBlkRef.Location = new System.Drawing.Point(12, 55);
            this.pnlTblBlkRef.MasterPanel = null;
            this.pnlTblBlkRef.Name = "pnlTblBlkRef";
            this.pnlTblBlkRef.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkRef.Size = new System.Drawing.Size(502, 207);
            this.pnlTblBlkRef.SPName = "CHRIS_SP_RegStHiEnt_EMP_REFERENCE_MANAGER";
            this.pnlTblBlkRef.TabIndex = 105;
            // 
            // dgvBlkRef
            // 
            this.dgvBlkRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Ref_Name,
            this.ref_Code,
            this.Ref_Add1,
            this.Ref_Add2,
            this.Ref_Add3,
            this.PR_P_NO});
            this.dgvBlkRef.ColumnToHide = null;
            this.dgvBlkRef.ColumnWidth = null;
            this.dgvBlkRef.CustomEnabled = true;
            this.dgvBlkRef.DisplayColumnWrapper = null;
            this.dgvBlkRef.GridDefaultRow = 0;
            this.dgvBlkRef.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkRef.Name = "dgvBlkRef";
            this.dgvBlkRef.ReadOnlyColumns = null;
            this.dgvBlkRef.RequiredColumns = null;
            this.dgvBlkRef.Size = new System.Drawing.Size(495, 202);
            this.dgvBlkRef.SkippingColumns = null;
            this.dgvBlkRef.TabIndex = 0;
            this.dgvBlkRef.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvBlkRef_CellValidating);
            this.dgvBlkRef.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvBlkRef_EditingControlShowing);
            this.dgvBlkRef.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBlkRef_CellEnter);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.MaxInputLength = 0;
            this.Column1.MinimumWidth = 2;
            this.Column1.Name = "Column1";
            this.Column1.Width = 2;
            // 
            // Ref_Name
            // 
            this.Ref_Name.DataPropertyName = "REF_NAME";
            this.Ref_Name.HeaderText = "Name";
            this.Ref_Name.MaxInputLength = 40;
            this.Ref_Name.Name = "Ref_Name";
            this.Ref_Name.Width = 150;
            // 
            // ref_Code
            // 
            this.ref_Code.DataPropertyName = "REF_CODE";
            this.ref_Code.HeaderText = "ref_Code";
            this.ref_Code.Name = "ref_Code";
            this.ref_Code.Visible = false;
            // 
            // Ref_Add1
            // 
            this.Ref_Add1.DataPropertyName = "Ref_Add1";
            this.Ref_Add1.HeaderText = "Address1";
            this.Ref_Add1.MaxInputLength = 40;
            this.Ref_Add1.Name = "Ref_Add1";
            // 
            // Ref_Add2
            // 
            this.Ref_Add2.DataPropertyName = "Ref_Add2";
            this.Ref_Add2.HeaderText = "Address2";
            this.Ref_Add2.MaxInputLength = 40;
            this.Ref_Add2.Name = "Ref_Add2";
            // 
            // Ref_Add3
            // 
            this.Ref_Add3.DataPropertyName = "Ref_Add3";
            this.Ref_Add3.HeaderText = "Address3";
            this.Ref_Add3.MaxInputLength = 40;
            this.Ref_Add3.Name = "Ref_Add3";
            // 
            // PR_P_NO
            // 
            this.PR_P_NO.DataPropertyName = "PR_P_NO";
            this.PR_P_NO.HeaderText = "PR_P_NO";
            this.PR_P_NO.Name = "PR_P_NO";
            this.PR_P_NO.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(171, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 17);
            this.label1.TabIndex = 106;
            this.label1.Text = "Reference Information";
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_BLKREF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 272);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlTblBlkRef);
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_BLKREF";
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt_BLKREF";
            this.Controls.SetChildIndex(this.pnlTblBlkRef, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblBlkRef.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkRef)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkRef;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkRef;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref_Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ref_Add3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_P_NO;

    }
}