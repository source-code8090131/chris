﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_MakerChecker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMakerChecker = new System.Windows.Forms.Panel();
            this.dtGVMakerChecker = new System.Windows.Forms.DataGridView();
            this.btnClosed = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.pnlMakerChecker.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVMakerChecker)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMakerChecker
            // 
            this.pnlMakerChecker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMakerChecker.Controls.Add(this.dtGVMakerChecker);
            this.pnlMakerChecker.Location = new System.Drawing.Point(12, 12);
            this.pnlMakerChecker.Name = "pnlMakerChecker";
            this.pnlMakerChecker.Size = new System.Drawing.Size(1126, 339);
            this.pnlMakerChecker.TabIndex = 0;
            // 
            // dtGVMakerChecker
            // 
            this.dtGVMakerChecker.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGVMakerChecker.Location = new System.Drawing.Point(12, 14);
            this.dtGVMakerChecker.Name = "dtGVMakerChecker";
            this.dtGVMakerChecker.RowTemplate.Height = 24;
            this.dtGVMakerChecker.Size = new System.Drawing.Size(1100, 308);
            this.dtGVMakerChecker.TabIndex = 0;
            // 
            // btnClosed
            // 
            this.btnClosed.Location = new System.Drawing.Point(1011, 367);
            this.btnClosed.Name = "btnClosed";
            this.btnClosed.Size = new System.Drawing.Size(114, 40);
            this.btnClosed.TabIndex = 3;
            this.btnClosed.Text = "Close";
            this.btnClosed.UseVisualStyleBackColor = true;
            this.btnClosed.Click += new System.EventHandler(this.btnClosed_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(875, 367);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(114, 40);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(739, 367);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(114, 40);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // CHRIS_Personnel_MakerChecker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 416);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnClosed);
            this.Controls.Add(this.pnlMakerChecker);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_MakerChecker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_Personnel_MakerChecker";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_MakerChecker_Load);
            this.pnlMakerChecker.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtGVMakerChecker)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMakerChecker;
        private System.Windows.Forms.Button btnClosed;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.DataGridView dtGVMakerChecker;
    }
}