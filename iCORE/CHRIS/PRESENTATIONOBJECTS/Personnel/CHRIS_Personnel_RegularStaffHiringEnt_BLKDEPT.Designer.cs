namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT));
            this.pnlBLKDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.colSegment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dept = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.prContrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contribution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlBLKDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkDept)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBLKDept
            // 
            this.pnlBLKDept.ConcurrentPanels = null;
            this.pnlBLKDept.Controls.Add(this.dgvBlkDept);
            this.pnlBLKDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlBLKDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlBLKDept.DependentPanels = null;
            this.pnlBLKDept.DisableDependentLoad = false;
            this.pnlBLKDept.EnableDelete = true;
            this.pnlBLKDept.EnableInsert = true;
            this.pnlBLKDept.EnableQuery = false;
            this.pnlBLKDept.EnableUpdate = true;
            this.pnlBLKDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.pnlBLKDept.Location = new System.Drawing.Point(3, 58);
            this.pnlBLKDept.MasterPanel = null;
            this.pnlBLKDept.Name = "pnlBLKDept";
            this.pnlBLKDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlBLKDept.Size = new System.Drawing.Size(373, 229);
            this.pnlBLKDept.SPName = "CHRIS_SP_RegStHiEnt_DEPT_CONT_MANAGER";
            this.pnlBLKDept.TabIndex = 11;
            // 
            // dgvBlkDept
            // 
            this.dgvBlkDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSegment,
            this.Dept,
            this.prContrib,
            this.Contribution,
            this.Pr_Type});
            this.dgvBlkDept.ColumnToHide = null;
            this.dgvBlkDept.ColumnWidth = null;
            this.dgvBlkDept.CustomEnabled = true;
            this.dgvBlkDept.DisplayColumnWrapper = null;
            this.dgvBlkDept.GridDefaultRow = 0;
            this.dgvBlkDept.Location = new System.Drawing.Point(3, 3);
            this.dgvBlkDept.Name = "dgvBlkDept";
            this.dgvBlkDept.ReadOnlyColumns = null;
            this.dgvBlkDept.RequiredColumns = null;
            this.dgvBlkDept.Size = new System.Drawing.Size(365, 223);
            this.dgvBlkDept.SkippingColumns = null;
            this.dgvBlkDept.TabIndex = 0;
            this.dgvBlkDept.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBlkDept_CellLeave);
            this.dgvBlkDept.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvBlkDept_CellValidating);
            this.dgvBlkDept.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvBlkDept_EditingControlShowing);
            this.dgvBlkDept.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBlkDept_CellEnter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(103, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Department Contribution";
            // 
            // colSegment
            // 
            this.colSegment.DataPropertyName = "PR_SEGMENT";
            this.colSegment.HeaderText = "Segment";
            this.colSegment.MaxInputLength = 3;
            this.colSegment.Name = "colSegment";
            // 
            // Dept
            // 
            this.Dept.ActionLOV = "DEPT";
            this.Dept.ActionLOVExists = "DEPT_EXIST";
            this.Dept.AttachParentEntity = false;
            this.Dept.DataPropertyName = "PR_DEPT";
            this.Dept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.Dept.HeaderText = "Dept.";
            this.Dept.LookUpTitle = "Department Codes";
            this.Dept.LOVFieldMapping = "PR_DEPT";
            this.Dept.MaxInputLength = 5;
            this.Dept.Name = "Dept";
            this.Dept.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Dept.SearchColumn = "PR_DEPT";
            this.Dept.SkipValidationOnLeave = false;
            this.Dept.SpName = "CHRIS_SP_RegStHiEnt_DEPT_CONT_MANAGER";
            // 
            // prContrib
            // 
            this.prContrib.DataPropertyName = "PR_P_NO";
            this.prContrib.HeaderText = "Pr_Contrib";
            this.prContrib.Name = "prContrib";
            this.prContrib.Visible = false;
            // 
            // Contribution
            // 
            this.Contribution.DataPropertyName = "PR_CONTRIB";
            this.Contribution.HeaderText = "Contribution %";
            this.Contribution.MaxInputLength = 3;
            this.Contribution.Name = "Contribution";
            this.Contribution.Width = 120;
            // 
            // Pr_Type
            // 
            this.Pr_Type.DataPropertyName = "PR_TYPE";
            this.Pr_Type.HeaderText = "Pr_Type";
            this.Pr_Type.Name = "Pr_Type";
            this.Pr_Type.Visible = false;
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 299);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlBLKDept);
            this.Enabled = false;
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT";
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_RegularStaffHiringEnt_BLKDEPT_Shown);
            this.Controls.SetChildIndex(this.pnlBLKDept, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlBLKDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkDept)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlBLKDept;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBlkDept;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSegment;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn prContrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contribution;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Type;

    }
}