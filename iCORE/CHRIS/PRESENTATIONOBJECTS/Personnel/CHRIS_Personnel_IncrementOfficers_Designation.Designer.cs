namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_IncrementOfficers_Designation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_IncrementOfficers_Designation));
            this.PnlIncPromotion = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnLevel = new CrplControlLibrary.LookupButton(this.components);
            this.LBDesig = new CrplControlLibrary.LookupButton(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtfunctitl2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfunctitle1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlIncPromotion.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(334, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(370, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 176);
            this.panel1.Size = new System.Drawing.Size(370, 60);
            // 
            // PnlIncPromotion
            // 
            this.PnlIncPromotion.ConcurrentPanels = null;
            this.PnlIncPromotion.Controls.Add(this.lbtnLevel);
            this.PnlIncPromotion.Controls.Add(this.LBDesig);
            this.PnlIncPromotion.Controls.Add(this.label3);
            this.PnlIncPromotion.Controls.Add(this.label2);
            this.PnlIncPromotion.Controls.Add(this.label1);
            this.PnlIncPromotion.Controls.Add(this.txtfunctitl2);
            this.PnlIncPromotion.Controls.Add(this.txtfunctitle1);
            this.PnlIncPromotion.Controls.Add(this.txtLevel);
            this.PnlIncPromotion.Controls.Add(this.txtDesignation);
            this.PnlIncPromotion.DataManager = null;
            this.PnlIncPromotion.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlIncPromotion.DependentPanels = null;
            this.PnlIncPromotion.DisableDependentLoad = false;
            this.PnlIncPromotion.EnableDelete = true;
            this.PnlIncPromotion.EnableInsert = true;
            this.PnlIncPromotion.EnableQuery = false;
            this.PnlIncPromotion.EnableUpdate = true;
            this.PnlIncPromotion.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.Inc_Promotion_officerCommand";
            this.PnlIncPromotion.Location = new System.Drawing.Point(12, 39);
            this.PnlIncPromotion.MasterPanel = null;
            this.PnlIncPromotion.Name = "PnlIncPromotion";
            this.PnlIncPromotion.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlIncPromotion.Size = new System.Drawing.Size(346, 134);
            this.PnlIncPromotion.SPName = "CHRIS_SP_INC_PROMOTION_Increment_MANAGER";
            this.PnlIncPromotion.TabIndex = 13;
            // 
            // lbtnLevel
            // 
            this.lbtnLevel.ActionLOVExists = "LOV_LEVEL_Exists";
            this.lbtnLevel.ActionType = "LOV_LEVEL";
            this.lbtnLevel.ConditionalFields = "txtDesignation";
            this.lbtnLevel.CustomEnabled = true;
            this.lbtnLevel.DataFieldMapping = "";
            this.lbtnLevel.DependentLovControls = "";
            this.lbtnLevel.HiddenColumns = "";
            this.lbtnLevel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnLevel.Image")));
            this.lbtnLevel.LoadDependentEntities = false;
            this.lbtnLevel.Location = new System.Drawing.Point(303, 28);
            this.lbtnLevel.LookUpTitle = null;
            this.lbtnLevel.Name = "lbtnLevel";
            this.lbtnLevel.Size = new System.Drawing.Size(26, 21);
            this.lbtnLevel.SkipValidationOnLeave = false;
            this.lbtnLevel.SPName = "CHRIS_SP_INC_PROMOTION_Increment_MANAGER";
            this.lbtnLevel.TabIndex = 29;
            this.lbtnLevel.TabStop = false;
            this.lbtnLevel.UseVisualStyleBackColor = true;
            // 
            // LBDesig
            // 
            this.LBDesig.ActionLOVExists = "Lov_desigExists";
            this.LBDesig.ActionType = "Lov_desig";
            this.LBDesig.ConditionalFields = "";
            this.LBDesig.CustomEnabled = true;
            this.LBDesig.DataFieldMapping = "";
            this.LBDesig.DependentLovControls = "";
            this.LBDesig.HiddenColumns = "PR_LEVEL_PRESENT";
            this.LBDesig.Image = ((System.Drawing.Image)(resources.GetObject("LBDesig.Image")));
            this.LBDesig.LoadDependentEntities = false;
            this.LBDesig.Location = new System.Drawing.Point(167, 28);
            this.LBDesig.LookUpTitle = null;
            this.LBDesig.Name = "LBDesig";
            this.LBDesig.Size = new System.Drawing.Size(26, 21);
            this.LBDesig.SkipValidationOnLeave = false;
            this.LBDesig.SPName = "CHRIS_SP_INC_PROMOTION_Increment_MANAGER";
            this.LBDesig.TabIndex = 28;
            this.LBDesig.TabStop = false;
            this.LBDesig.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Function Title :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(208, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Level ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Designation :";
            // 
            // txtfunctitl2
            // 
            this.txtfunctitl2.AllowSpace = true;
            this.txtfunctitl2.AssociatedLookUpName = "";
            this.txtfunctitl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunctitl2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunctitl2.ContinuationTextBox = null;
            this.txtfunctitl2.CustomEnabled = true;
            this.txtfunctitl2.DataFieldMapping = "";
            this.txtfunctitl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunctitl2.GetRecordsOnUpDownKeys = false;
            this.txtfunctitl2.IsDate = false;
            this.txtfunctitl2.Location = new System.Drawing.Point(32, 105);
            this.txtfunctitl2.Name = "txtfunctitl2";
            this.txtfunctitl2.NumberFormat = "###,###,##0.00";
            this.txtfunctitl2.Postfix = "";
            this.txtfunctitl2.Prefix = "";
            this.txtfunctitl2.Size = new System.Drawing.Size(265, 20);
            this.txtfunctitl2.SkipValidation = false;
            this.txtfunctitl2.TabIndex = 24;
            this.txtfunctitl2.TextType = CrplControlLibrary.TextType.String;
            this.txtfunctitl2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtfunctitl2_PreviewKeyDown);
            this.txtfunctitl2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtfunctitl2_KeyPress);
            this.txtfunctitl2.Validating += new System.ComponentModel.CancelEventHandler(this.txtfunctitl2_Validating);
            // 
            // txtfunctitle1
            // 
            this.txtfunctitle1.AllowSpace = true;
            this.txtfunctitle1.AssociatedLookUpName = "";
            this.txtfunctitle1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunctitle1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunctitle1.ContinuationTextBox = null;
            this.txtfunctitle1.CustomEnabled = true;
            this.txtfunctitle1.DataFieldMapping = "";
            this.txtfunctitle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunctitle1.GetRecordsOnUpDownKeys = false;
            this.txtfunctitle1.IsDate = false;
            this.txtfunctitle1.Location = new System.Drawing.Point(32, 79);
            this.txtfunctitle1.Name = "txtfunctitle1";
            this.txtfunctitle1.NumberFormat = "###,###,##0.00";
            this.txtfunctitle1.Postfix = "";
            this.txtfunctitle1.Prefix = "";
            this.txtfunctitle1.Size = new System.Drawing.Size(265, 20);
            this.txtfunctitle1.SkipValidation = false;
            this.txtfunctitle1.TabIndex = 23;
            this.txtfunctitle1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "lbtnLevel";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL_PRESENT";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(253, 28);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(44, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 22;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesignation
            // 
            this.txtDesignation.AllowSpace = true;
            this.txtDesignation.AssociatedLookUpName = "LBDesig";
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesignation.ContinuationTextBox = null;
            this.txtDesignation.CustomEnabled = true;
            this.txtDesignation.DataFieldMapping = "PR_DESIG_PRESENT";
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.GetRecordsOnUpDownKeys = false;
            this.txtDesignation.IsDate = false;
            this.txtDesignation.Location = new System.Drawing.Point(117, 28);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.NumberFormat = "###,###,##0.00";
            this.txtDesignation.Postfix = "";
            this.txtDesignation.Prefix = "";
            this.txtDesignation.Size = new System.Drawing.Size(44, 20);
            this.txtDesignation.SkipValidation = false;
            this.txtDesignation.TabIndex = 21;
            this.txtDesignation.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Personnel_IncrementOfficers_Designation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(370, 236);
            this.Controls.Add(this.PnlIncPromotion);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "CHRIS_Personnel_IncrementOfficers_Designation";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_IncrementOfficers_Designation";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlIncPromotion, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlIncPromotion.ResumeLayout(false);
            this.PnlIncPromotion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlIncPromotion;
        private CrplControlLibrary.LookupButton LBDesig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtfunctitl2;
        private CrplControlLibrary.SLTextBox txtfunctitle1;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtDesignation;
        private CrplControlLibrary.LookupButton lbtnLevel;


    }
}