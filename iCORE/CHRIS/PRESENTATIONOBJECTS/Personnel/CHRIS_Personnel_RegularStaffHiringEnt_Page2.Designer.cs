namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_Page2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txt_W_VIEW_ANS = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblPopUP = new System.Windows.Forms.Label();
            this.pnlSavePopUP = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.pnlSavePopUP.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_W_VIEW_ANS
            // 
            this.txt_W_VIEW_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_VIEW_ANS.Location = new System.Drawing.Point(419, 4);
            this.txt_W_VIEW_ANS.Name = "txt_W_VIEW_ANS";
            this.txt_W_VIEW_ANS.Size = new System.Drawing.Size(31, 20);
            this.txt_W_VIEW_ANS.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(498, 1);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1, 20);
            this.textBox1.TabIndex = 4;
            // 
            // lblPopUP
            // 
            this.lblPopUP.AutoSize = true;
            this.lblPopUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPopUP.Location = new System.Drawing.Point(31, 16);
            this.lblPopUP.Name = "lblPopUP";
            this.lblPopUP.Size = new System.Drawing.Size(0, 13);
            this.lblPopUP.TabIndex = 0;
            // 
            // pnlSavePopUP
            // 
            this.pnlSavePopUP.ConcurrentPanels = null;
            this.pnlSavePopUP.Controls.Add(this.txt_W_VIEW_ANS);
            this.pnlSavePopUP.Controls.Add(this.textBox1);
            this.pnlSavePopUP.DataManager = null;
            this.pnlSavePopUP.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSavePopUP.DependentPanels = null;
            this.pnlSavePopUP.DisableDependentLoad = false;
            this.pnlSavePopUP.EnableDelete = true;
            this.pnlSavePopUP.EnableInsert = true;
            this.pnlSavePopUP.EnableUpdate = true;
            this.pnlSavePopUP.EntityName = null;
            this.pnlSavePopUP.Location = new System.Drawing.Point(18, 7);
            this.pnlSavePopUP.MasterPanel = null;
            this.pnlSavePopUP.Name = "pnlSavePopUP";
            this.pnlSavePopUP.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSavePopUP.Size = new System.Drawing.Size(511, 29);
            this.pnlSavePopUP.SPName = null;
            this.pnlSavePopUP.TabIndex = 13;
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_Page2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(541, 40);
            this.Controls.Add(this.lblPopUP);
            this.Controls.Add(this.pnlSavePopUP);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_Page2";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CHRIS_Personnel_RegularStaffHiringEnt_Page2_Load);
            this.pnlSavePopUP.ResumeLayout(false);
            this.pnlSavePopUP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_W_VIEW_ANS;
        private System.Windows.Forms.TextBox textBox1;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlSavePopUP;
        private System.Windows.Forms.Label lblPopUP;

    }
}