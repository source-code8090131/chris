namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_FastOrISTransfer_FastEndDatePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label4 = new System.Windows.Forms.Label();
            this.dtPrEffective = new System.Windows.Forms.DateTimePicker();
            this.dtEndDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(43, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 71;
            this.label4.Text = "FAST END DATE :";
            // 
            // dtPrEffective
            // 
            this.dtPrEffective.CustomFormat = "dd/MM/yyyy";
            this.dtPrEffective.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPrEffective.Location = new System.Drawing.Point(57, 113);
            this.dtPrEffective.Name = "dtPrEffective";
            this.dtPrEffective.Size = new System.Drawing.Size(86, 20);
            this.dtPrEffective.TabIndex = 135;
            this.dtPrEffective.Visible = false;
            // 
            // dtEndDate
            // 
            this.dtEndDate.CustomEnabled = true;
            this.dtEndDate.CustomFormat = "dd/MM/yyyy";
            this.dtEndDate.DataFieldMapping = "dtFastEndDate";
            this.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEndDate.HasChanges = true;
            this.dtEndDate.Location = new System.Drawing.Point(57, 72);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.NullValue = " ";
            this.dtEndDate.Size = new System.Drawing.Size(86, 20);
            this.dtEndDate.TabIndex = 1;
            this.dtEndDate.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.dtEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtEndDate_Validating);
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(46, 134);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(1, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 136;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Personnel_FastOrISTransfer_FastEndDatePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(192, 166);
            this.Controls.Add(this.slTextBox1);
            this.Controls.Add(this.dtEndDate);
            this.Controls.Add(this.dtPrEffective);
            this.Controls.Add(this.label4);
            this.Name = "CHRIS_Personnel_FastOrISTransfer_FastEndDatePage";
            this.Text = "CHRIS_Personnel_FastOrISTransfer_FastEndDatePage";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_FastOrISTransfer_FastEndDatePage_Load);
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_FastOrISTransfer_FastEndDatePage_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtPrEffective;
        private CrplControlLibrary.SLDatePicker dtEndDate;
        private CrplControlLibrary.SLTextBox slTextBox1;
    }
}