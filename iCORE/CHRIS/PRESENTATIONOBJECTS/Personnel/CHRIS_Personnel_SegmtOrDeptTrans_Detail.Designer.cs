﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_SegmtOrDeptTrans_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_SegmtOrDeptTrans_Detail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txtW_GOAL = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.dtW_GOAL_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtW_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfunc1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPR_Func2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.dtpPR_EFFECTIVE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtW_REP = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txtPR_Func1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_DESG = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtPR_NEW_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txtfunc2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.txtNoINCR = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlTblDept2 = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVDepts = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.btnAproved = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlDetail.SuspendLayout();
            this.pnlTblDept2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDepts)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.txtW_GOAL);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.dtW_GOAL_DATE);
            this.pnlDetail.Controls.Add(this.txtW_Name);
            this.pnlDetail.Controls.Add(this.txtfunc1);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtPR_Func2);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.slTextBox1);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.dtpPR_EFFECTIVE);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtW_REP);
            this.pnlDetail.Controls.Add(this.label22);
            this.pnlDetail.Controls.Add(this.txtPR_Func1);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtPR_LEVEL);
            this.pnlDetail.Controls.Add(this.txtPR_DESG);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtPR_NEW_BRANCH);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.txtfunc2);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtNoINCR);
            this.pnlDetail.Controls.Add(this.txtName);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand";
            this.pnlDetail.Location = new System.Drawing.Point(13, 105);
            this.pnlDetail.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(572, 495);
            this.pnlDetail.SPName = "CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER";
            this.pnlDetail.TabIndex = 13;
            this.pnlDetail.TabStop = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 240);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(560, 17);
            this.label18.TabIndex = 128;
            this.label18.Text = "_____________________________________________________________________";
            // 
            // txtW_GOAL
            // 
            this.txtW_GOAL.AllowSpace = true;
            this.txtW_GOAL.AssociatedLookUpName = "";
            this.txtW_GOAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_GOAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_GOAL.ContinuationTextBox = null;
            this.txtW_GOAL.CustomEnabled = true;
            this.txtW_GOAL.DataFieldMapping = "";
            this.txtW_GOAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_GOAL.GetRecordsOnUpDownKeys = false;
            this.txtW_GOAL.IsDate = false;
            this.txtW_GOAL.Location = new System.Drawing.Point(201, 420);
            this.txtW_GOAL.Margin = new System.Windows.Forms.Padding(4);
            this.txtW_GOAL.MaxLength = 3;
            this.txtW_GOAL.Name = "txtW_GOAL";
            this.txtW_GOAL.NumberFormat = "###,###,##0.00";
            this.txtW_GOAL.Postfix = "";
            this.txtW_GOAL.Prefix = "";
            this.txtW_GOAL.Size = new System.Drawing.Size(75, 24);
            this.txtW_GOAL.SkipValidation = false;
            this.txtW_GOAL.TabIndex = 7;
            this.txtW_GOAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(281, 418);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 25);
            this.label17.TabIndex = 114;
            this.label17.Text = "Rcvd. Date :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtW_GOAL_DATE
            // 
            this.dtW_GOAL_DATE.CustomEnabled = true;
            this.dtW_GOAL_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtW_GOAL_DATE.DataFieldMapping = "";
            this.dtW_GOAL_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtW_GOAL_DATE.HasChanges = true;
            this.dtW_GOAL_DATE.IsRequired = true;
            this.dtW_GOAL_DATE.Location = new System.Drawing.Point(393, 420);
            this.dtW_GOAL_DATE.Margin = new System.Windows.Forms.Padding(4);
            this.dtW_GOAL_DATE.Name = "dtW_GOAL_DATE";
            this.dtW_GOAL_DATE.NullValue = " ";
            this.dtW_GOAL_DATE.Size = new System.Drawing.Size(117, 22);
            this.dtW_GOAL_DATE.TabIndex = 8;
            this.dtW_GOAL_DATE.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // txtW_Name
            // 
            this.txtW_Name.AllowSpace = true;
            this.txtW_Name.AssociatedLookUpName = "";
            this.txtW_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_Name.ContinuationTextBox = null;
            this.txtW_Name.CustomEnabled = true;
            this.txtW_Name.DataFieldMapping = "";
            this.txtW_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_Name.GetRecordsOnUpDownKeys = false;
            this.txtW_Name.IsDate = false;
            this.txtW_Name.Location = new System.Drawing.Point(323, 391);
            this.txtW_Name.Margin = new System.Windows.Forms.Padding(4);
            this.txtW_Name.MaxLength = 30;
            this.txtW_Name.Name = "txtW_Name";
            this.txtW_Name.NumberFormat = "###,###,##0.00";
            this.txtW_Name.Postfix = "";
            this.txtW_Name.Prefix = "";
            this.txtW_Name.ReadOnly = true;
            this.txtW_Name.Size = new System.Drawing.Size(163, 24);
            this.txtW_Name.SkipValidation = false;
            this.txtW_Name.TabIndex = 112;
            this.txtW_Name.TabStop = false;
            this.txtW_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtfunc1
            // 
            this.txtfunc1.AllowSpace = true;
            this.txtfunc1.AssociatedLookUpName = "";
            this.txtfunc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunc1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunc1.ContinuationTextBox = null;
            this.txtfunc1.CustomEnabled = true;
            this.txtfunc1.DataFieldMapping = "";
            this.txtfunc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunc1.GetRecordsOnUpDownKeys = false;
            this.txtfunc1.IsDate = false;
            this.txtfunc1.Location = new System.Drawing.Point(201, 305);
            this.txtfunc1.Margin = new System.Windows.Forms.Padding(4);
            this.txtfunc1.MaxLength = 30;
            this.txtfunc1.Name = "txtfunc1";
            this.txtfunc1.NumberFormat = "###,###,##0.00";
            this.txtfunc1.Postfix = "";
            this.txtfunc1.Prefix = "";
            this.txtfunc1.Size = new System.Drawing.Size(285, 24);
            this.txtfunc1.SkipValidation = true;
            this.txtfunc1.TabIndex = 3;
            this.txtfunc1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(20, 303);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(183, 25);
            this.label10.TabIndex = 110;
            this.label10.Text = "1. Functional Title :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(220, 277);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(289, 25);
            this.label9.TabIndex = 109;
            this.label9.Text = "Transfer Information";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtPR_Func2
            // 
            this.txtPR_Func2.AllowSpace = true;
            this.txtPR_Func2.AssociatedLookUpName = "";
            this.txtPR_Func2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func2.ContinuationTextBox = null;
            this.txtPR_Func2.CustomEnabled = true;
            this.txtPR_Func2.DataFieldMapping = "";
            this.txtPR_Func2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func2.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func2.IsDate = false;
            this.txtPR_Func2.Location = new System.Drawing.Point(168, 170);
            this.txtPR_Func2.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_Func2.MaxLength = 30;
            this.txtPR_Func2.Name = "txtPR_Func2";
            this.txtPR_Func2.NumberFormat = "###,###,##0.00";
            this.txtPR_Func2.Postfix = "";
            this.txtPR_Func2.Prefix = "";
            this.txtPR_Func2.ReadOnly = true;
            this.txtPR_Func2.Size = new System.Drawing.Size(146, 24);
            this.txtPR_Func2.SkipValidation = false;
            this.txtPR_Func2.TabIndex = 108;
            this.txtPR_Func2.TabStop = false;
            this.txtPR_Func2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(85, 166);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 25);
            this.label7.TabIndex = 107;
            this.label7.Text = "Title :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "Pr_name_Last";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(364, 57);
            this.slTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.slTextBox1.MaxLength = 20;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(131, 24);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 106;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(49, 417);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(149, 25);
            this.label27.TabIndex = 104;
            this.label27.Text = "4. Goals Rcvd. :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpPR_EFFECTIVE
            // 
            this.dtpPR_EFFECTIVE.CustomEnabled = true;
            this.dtpPR_EFFECTIVE.CustomFormat = "dd/MM/yyyy";
            this.dtpPR_EFFECTIVE.DataFieldMapping = "";
            this.dtpPR_EFFECTIVE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPR_EFFECTIVE.HasChanges = true;
            this.dtpPR_EFFECTIVE.IsRequired = true;
            this.dtpPR_EFFECTIVE.Location = new System.Drawing.Point(201, 362);
            this.dtpPR_EFFECTIVE.Margin = new System.Windows.Forms.Padding(4);
            this.dtpPR_EFFECTIVE.Name = "dtpPR_EFFECTIVE";
            this.dtpPR_EFFECTIVE.NullValue = " ";
            this.dtpPR_EFFECTIVE.Size = new System.Drawing.Size(163, 22);
            this.dtpPR_EFFECTIVE.TabIndex = 5;
            this.dtpPR_EFFECTIVE.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(45, 389);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(153, 25);
            this.label23.TabIndex = 95;
            this.label23.Text = "3. Reporting To :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtW_REP
            // 
            this.txtW_REP.AllowSpace = true;
            this.txtW_REP.AssociatedLookUpName = "";
            this.txtW_REP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_REP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_REP.ContinuationTextBox = null;
            this.txtW_REP.CustomEnabled = true;
            this.txtW_REP.DataFieldMapping = "";
            this.txtW_REP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_REP.GetRecordsOnUpDownKeys = false;
            this.txtW_REP.IsDate = false;
            this.txtW_REP.IsRequired = true;
            this.txtW_REP.Location = new System.Drawing.Point(201, 390);
            this.txtW_REP.Margin = new System.Windows.Forms.Padding(4);
            this.txtW_REP.MaxLength = 6;
            this.txtW_REP.Name = "txtW_REP";
            this.txtW_REP.NumberFormat = "###,###,##0.00";
            this.txtW_REP.Postfix = "";
            this.txtW_REP.Prefix = "";
            this.txtW_REP.Size = new System.Drawing.Size(75, 24);
            this.txtW_REP.SkipValidation = false;
            this.txtW_REP.TabIndex = 6;
            this.txtW_REP.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(16, 359);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(183, 25);
            this.label22.TabIndex = 93;
            this.label22.Text = "2. Effective Date :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_Func1
            // 
            this.txtPR_Func1.AllowSpace = true;
            this.txtPR_Func1.AssociatedLookUpName = "";
            this.txtPR_Func1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_Func1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_Func1.ContinuationTextBox = null;
            this.txtPR_Func1.CustomEnabled = true;
            this.txtPR_Func1.DataFieldMapping = "";
            this.txtPR_Func1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_Func1.GetRecordsOnUpDownKeys = false;
            this.txtPR_Func1.IsDate = false;
            this.txtPR_Func1.Location = new System.Drawing.Point(168, 142);
            this.txtPR_Func1.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_Func1.MaxLength = 30;
            this.txtPR_Func1.Name = "txtPR_Func1";
            this.txtPR_Func1.NumberFormat = "###,###,##0.00";
            this.txtPR_Func1.Postfix = "";
            this.txtPR_Func1.Prefix = "";
            this.txtPR_Func1.ReadOnly = true;
            this.txtPR_Func1.Size = new System.Drawing.Size(146, 24);
            this.txtPR_Func1.SkipValidation = false;
            this.txtPR_Func1.TabIndex = 88;
            this.txtPR_Func1.TabStop = false;
            this.txtPR_Func1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(32, 140);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(132, 25);
            this.label14.TabIndex = 87;
            this.label14.Text = "5. Functional :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(320, 113);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 25);
            this.label12.TabIndex = 86;
            this.label12.Text = "Transfer Type :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_LEVEL
            // 
            this.txtPR_LEVEL.AllowSpace = true;
            this.txtPR_LEVEL.AssociatedLookUpName = "";
            this.txtPR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_LEVEL.ContinuationTextBox = null;
            this.txtPR_LEVEL.CustomEnabled = true;
            this.txtPR_LEVEL.DataFieldMapping = "";
            this.txtPR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txtPR_LEVEL.IsDate = false;
            this.txtPR_LEVEL.Location = new System.Drawing.Point(168, 113);
            this.txtPR_LEVEL.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_LEVEL.MaxLength = 3;
            this.txtPR_LEVEL.Name = "txtPR_LEVEL";
            this.txtPR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txtPR_LEVEL.Postfix = "";
            this.txtPR_LEVEL.Prefix = "";
            this.txtPR_LEVEL.ReadOnly = true;
            this.txtPR_LEVEL.Size = new System.Drawing.Size(110, 24);
            this.txtPR_LEVEL.SkipValidation = false;
            this.txtPR_LEVEL.TabIndex = 84;
            this.txtPR_LEVEL.TabStop = false;
            this.txtPR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPR_DESG
            // 
            this.txtPR_DESG.AllowSpace = true;
            this.txtPR_DESG.AssociatedLookUpName = "";
            this.txtPR_DESG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_DESG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_DESG.ContinuationTextBox = null;
            this.txtPR_DESG.CustomEnabled = true;
            this.txtPR_DESG.DataFieldMapping = "";
            this.txtPR_DESG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_DESG.GetRecordsOnUpDownKeys = false;
            this.txtPR_DESG.IsDate = false;
            this.txtPR_DESG.Location = new System.Drawing.Point(168, 85);
            this.txtPR_DESG.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_DESG.MaxLength = 10;
            this.txtPR_DESG.Name = "txtPR_DESG";
            this.txtPR_DESG.NumberFormat = "###,###,##0.00";
            this.txtPR_DESG.Postfix = "";
            this.txtPR_DESG.Prefix = "";
            this.txtPR_DESG.ReadOnly = true;
            this.txtPR_DESG.Size = new System.Drawing.Size(73, 24);
            this.txtPR_DESG.SkipValidation = false;
            this.txtPR_DESG.TabIndex = 80;
            this.txtPR_DESG.TabStop = false;
            this.txtPR_DESG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(13, 82);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 25);
            this.label13.TabIndex = 82;
            this.label13.Text = "3. Designation :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPR_NEW_BRANCH
            // 
            this.txtPR_NEW_BRANCH.AllowSpace = true;
            this.txtPR_NEW_BRANCH.AssociatedLookUpName = "";
            this.txtPR_NEW_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_NEW_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_NEW_BRANCH.ContinuationTextBox = null;
            this.txtPR_NEW_BRANCH.CustomEnabled = true;
            this.txtPR_NEW_BRANCH.DataFieldMapping = "";
            this.txtPR_NEW_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_NEW_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txtPR_NEW_BRANCH.IsDate = false;
            this.txtPR_NEW_BRANCH.Location = new System.Drawing.Point(372, 28);
            this.txtPR_NEW_BRANCH.Margin = new System.Windows.Forms.Padding(4);
            this.txtPR_NEW_BRANCH.MaxLength = 3;
            this.txtPR_NEW_BRANCH.Name = "txtPR_NEW_BRANCH";
            this.txtPR_NEW_BRANCH.NumberFormat = "###,###,##0.00";
            this.txtPR_NEW_BRANCH.Postfix = "";
            this.txtPR_NEW_BRANCH.Prefix = "";
            this.txtPR_NEW_BRANCH.ReadOnly = true;
            this.txtPR_NEW_BRANCH.Size = new System.Drawing.Size(62, 24);
            this.txtPR_NEW_BRANCH.SkipValidation = false;
            this.txtPR_NEW_BRANCH.TabIndex = 74;
            this.txtPR_NEW_BRANCH.TabStop = false;
            this.txtPR_NEW_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(283, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 25);
            this.label15.TabIndex = 75;
            this.label15.Text = "Branch:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtfunc2
            // 
            this.txtfunc2.AllowSpace = true;
            this.txtfunc2.AssociatedLookUpName = "";
            this.txtfunc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfunc2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfunc2.ContinuationTextBox = null;
            this.txtfunc2.CustomEnabled = true;
            this.txtfunc2.DataFieldMapping = "";
            this.txtfunc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfunc2.GetRecordsOnUpDownKeys = false;
            this.txtfunc2.IsDate = false;
            this.txtfunc2.Location = new System.Drawing.Point(201, 334);
            this.txtfunc2.Margin = new System.Windows.Forms.Padding(4);
            this.txtfunc2.MaxLength = 30;
            this.txtfunc2.Name = "txtfunc2";
            this.txtfunc2.NumberFormat = "###,###,##0.00";
            this.txtfunc2.Postfix = "";
            this.txtfunc2.Prefix = "";
            this.txtfunc2.Size = new System.Drawing.Size(285, 24);
            this.txtfunc2.SkipValidation = true;
            this.txtfunc2.TabIndex = 4;
            this.txtfunc2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(55, 111);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 25);
            this.label16.TabIndex = 69;
            this.label16.Text = "4. Level :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNoINCR
            // 
            this.txtNoINCR.AllowSpace = true;
            this.txtNoINCR.AssociatedLookUpName = "";
            this.txtNoINCR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoINCR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoINCR.ContinuationTextBox = null;
            this.txtNoINCR.CustomEnabled = false;
            this.txtNoINCR.DataFieldMapping = "";
            this.txtNoINCR.Enabled = false;
            this.txtNoINCR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoINCR.GetRecordsOnUpDownKeys = false;
            this.txtNoINCR.IsDate = false;
            this.txtNoINCR.Location = new System.Drawing.Point(467, 113);
            this.txtNoINCR.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoINCR.MaxLength = 1;
            this.txtNoINCR.Name = "txtNoINCR";
            this.txtNoINCR.NumberFormat = "###,###,##0.00";
            this.txtNoINCR.Postfix = "";
            this.txtNoINCR.Prefix = "";
            this.txtNoINCR.Size = new System.Drawing.Size(51, 24);
            this.txtNoINCR.SkipValidation = false;
            this.txtNoINCR.TabIndex = 68;
            this.txtNoINCR.TabStop = false;
            this.txtNoINCR.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BackColor = System.Drawing.SystemColors.Control;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(168, 57);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.MaxLength = 20;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(193, 24);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(72, 57);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 25);
            this.label8.TabIndex = 26;
            this.label8.Text = "2. Name :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(168, 28);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(73, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(3, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(163, 25);
            this.label11.TabIndex = 23;
            this.label11.Text = "1. Personnel No. :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(205, 22);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 25);
            this.label5.TabIndex = 21;
            this.label5.Text = "Personnel   System";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(119, 48);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(403, 22);
            this.label6.TabIndex = 22;
            this.label6.Text = "Segment To Segment/ Dept To Dept Transfer";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlTblDept2
            // 
            this.pnlTblDept2.ConcurrentPanels = null;
            this.pnlTblDept2.Controls.Add(this.DGVDepts);
            this.pnlTblDept2.DataManager = null;
            this.pnlTblDept2.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept2.DependentPanels = null;
            this.pnlTblDept2.DisableDependentLoad = false;
            this.pnlTblDept2.EnableDelete = true;
            this.pnlTblDept2.EnableInsert = true;
            this.pnlTblDept2.EnableQuery = false;
            this.pnlTblDept2.EnableUpdate = true;
            this.pnlTblDept2.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContTransferCommand";
            this.pnlTblDept2.Location = new System.Drawing.Point(593, 367);
            this.pnlTblDept2.Margin = new System.Windows.Forms.Padding(4);
            this.pnlTblDept2.MasterPanel = null;
            this.pnlTblDept2.Name = "pnlTblDept2";
            this.pnlTblDept2.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept2.Size = new System.Drawing.Size(281, 233);
            this.pnlTblDept2.SPName = "CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER";
            this.pnlTblDept2.TabIndex = 23;
            // 
            // DGVDepts
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDepts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVDepts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVDepts.ColumnToHide = null;
            this.DGVDepts.ColumnWidth = null;
            this.DGVDepts.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVDepts.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVDepts.DisplayColumnWrapper = null;
            this.DGVDepts.GridDefaultRow = 0;
            this.DGVDepts.Location = new System.Drawing.Point(4, 4);
            this.DGVDepts.Margin = new System.Windows.Forms.Padding(4);
            this.DGVDepts.Name = "DGVDepts";
            this.DGVDepts.ReadOnlyColumns = null;
            this.DGVDepts.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDepts.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVDepts.Size = new System.Drawing.Size(273, 217);
            this.DGVDepts.SkippingColumns = null;
            this.DGVDepts.TabIndex = 0;
            this.DGVDepts.TabStop = false;
            // 
            // btnAproved
            // 
            this.btnAproved.Location = new System.Drawing.Point(777, 40);
            this.btnAproved.Name = "btnAproved";
            this.btnAproved.Size = new System.Drawing.Size(97, 40);
            this.btnAproved.TabIndex = 100;
            this.btnAproved.Text = "Approve";
            this.btnAproved.UseVisualStyleBackColor = true;
            this.btnAproved.Click += new System.EventHandler(this.btnAproved_Click);
            // 
            // btnReject
            // 
            this.btnReject.Location = new System.Drawing.Point(669, 40);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(97, 40);
            this.btnReject.TabIndex = 99;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(558, 40);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(97, 40);
            this.btnClose.TabIndex = 98;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CHRIS_Personnel_SegmtOrDeptTrans_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 617);
            this.Controls.Add(this.btnAproved);
            this.Controls.Add(this.btnReject);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pnlTblDept2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlDetail);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_SegmtOrDeptTrans_Detail";
            this.Text = "CHRIS_Personnel_SegmtOrDeptTrans_Detail";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_SegmtOrDeptTrans_Detail_Load);
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlTblDept2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVDepts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtW_GOAL;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLDatePicker dtW_GOAL_DATE;
        private CrplControlLibrary.SLTextBox txtW_Name;
        private CrplControlLibrary.SLTextBox txtfunc1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtPR_Func2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLDatePicker dtpPR_EFFECTIVE;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtW_REP;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txtPR_Func1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtPR_LEVEL;
        private CrplControlLibrary.SLTextBox txtPR_DESG;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtPR_NEW_BRANCH;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtfunc2;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtNoINCR;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private COMMON.SLCONTROLS.SLPanelTabular pnlTblDept2;
        private COMMON.SLCONTROLS.SLDataGridView DGVDepts;
        private System.Windows.Forms.Button btnAproved;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnClose;
    }
}