namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_FastOrISTransfer_TransferPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_FastOrISTransfer_TransferPage));
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlBlkTransfer = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.dtJoiningDate = new System.Windows.Forms.DateTimePicker();
            this.dtTransferDate = new System.Windows.Forms.DateTimePicker();
            this.txtfurloughCity = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIsCordinate = new CrplControlLibrary.SLTextBox(this.components);
            this.dtFastEndDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.W_TypeOf = new CrplControlLibrary.SLTextBox(this.components);
            this.WOption = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPRTransferType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrPNo = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnflag2 = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnflag1 = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnCountry = new CrplControlLibrary.LookupButton(this.components);
            this.txtPR_REMARKS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountry = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCITI_FLAG2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCity = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPRDept = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPR_EFFECTIVE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtDesg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNewAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCITI_FLAG1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrency = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlBlkTransfer.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(456, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(492, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 406);
            this.panel1.Size = new System.Drawing.Size(492, 60);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(13, 265);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 15);
            this.label24.TabIndex = 84;
            this.label24.Text = "Effective Date ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(13, 239);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(132, 15);
            this.label22.TabIndex = 81;
            this.label22.Text = "ASR Currency Type ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(13, 214);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(146, 15);
            this.label21.TabIndex = 80;
            this.label21.Text = "New Annual Package ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(13, 190);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(165, 15);
            this.label19.TabIndex = 78;
            this.label19.Text = "Current Annual Package ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(13, 165);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 15);
            this.label13.TabIndex = 76;
            this.label13.Text = "Department ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(13, 139);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 15);
            this.label12.TabIndex = 74;
            this.label12.Text = "Designation ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(13, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 15);
            this.label8.TabIndex = 72;
            this.label8.Text = "Transfer City ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(13, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 15);
            this.label4.TabIndex = 70;
            this.label4.Text = "Transfer Country:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(166, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 15);
            this.label1.TabIndex = 86;
            this.label1.Text = "Transfer Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(254, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 15);
            this.label2.TabIndex = 88;
            this.label2.Text = "Level ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(13, 289);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 15);
            this.label3.TabIndex = 92;
            this.label3.Text = "Regret/Non Regret ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(13, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 15);
            this.label5.TabIndex = 94;
            this.label5.Text = "Voluntary/Non Voluntary ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(13, 343);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 15);
            this.label6.TabIndex = 96;
            this.label6.Text = "Remarks ";
            // 
            // pnlBlkTransfer
            // 
            this.pnlBlkTransfer.ConcurrentPanels = null;
            this.pnlBlkTransfer.Controls.Add(this.txtID);
            this.pnlBlkTransfer.Controls.Add(this.dtJoiningDate);
            this.pnlBlkTransfer.Controls.Add(this.dtTransferDate);
            this.pnlBlkTransfer.Controls.Add(this.txtfurloughCity);
            this.pnlBlkTransfer.Controls.Add(this.txtIsCordinate);
            this.pnlBlkTransfer.Controls.Add(this.dtFastEndDate);
            this.pnlBlkTransfer.Controls.Add(this.W_TypeOf);
            this.pnlBlkTransfer.Controls.Add(this.WOption);
            this.pnlBlkTransfer.Controls.Add(this.txtPRTransferType);
            this.pnlBlkTransfer.Controls.Add(this.txtPrPNo);
            this.pnlBlkTransfer.Controls.Add(this.lbtnflag2);
            this.pnlBlkTransfer.Controls.Add(this.lbtnflag1);
            this.pnlBlkTransfer.Controls.Add(this.lbtnCountry);
            this.pnlBlkTransfer.Controls.Add(this.label1);
            this.pnlBlkTransfer.Controls.Add(this.label6);
            this.pnlBlkTransfer.Controls.Add(this.label4);
            this.pnlBlkTransfer.Controls.Add(this.txtPR_REMARKS);
            this.pnlBlkTransfer.Controls.Add(this.txtCountry);
            this.pnlBlkTransfer.Controls.Add(this.label5);
            this.pnlBlkTransfer.Controls.Add(this.label8);
            this.pnlBlkTransfer.Controls.Add(this.txtCITI_FLAG2);
            this.pnlBlkTransfer.Controls.Add(this.txtCity);
            this.pnlBlkTransfer.Controls.Add(this.label3);
            this.pnlBlkTransfer.Controls.Add(this.label12);
            this.pnlBlkTransfer.Controls.Add(this.txtCurrAnnual);
            this.pnlBlkTransfer.Controls.Add(this.label13);
            this.pnlBlkTransfer.Controls.Add(this.txtPRDept);
            this.pnlBlkTransfer.Controls.Add(this.label19);
            this.pnlBlkTransfer.Controls.Add(this.txtLevel);
            this.pnlBlkTransfer.Controls.Add(this.txtPR_EFFECTIVE);
            this.pnlBlkTransfer.Controls.Add(this.label2);
            this.pnlBlkTransfer.Controls.Add(this.label21);
            this.pnlBlkTransfer.Controls.Add(this.txtDesg);
            this.pnlBlkTransfer.Controls.Add(this.label22);
            this.pnlBlkTransfer.Controls.Add(this.txtNewAnnual);
            this.pnlBlkTransfer.Controls.Add(this.txtCITI_FLAG1);
            this.pnlBlkTransfer.Controls.Add(this.txtCurrency);
            this.pnlBlkTransfer.Controls.Add(this.label24);
            this.pnlBlkTransfer.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlBlkTransfer.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlBlkTransfer.DependentPanels = null;
            this.pnlBlkTransfer.DisableDependentLoad = false;
            this.pnlBlkTransfer.EnableDelete = true;
            this.pnlBlkTransfer.EnableInsert = true;
            this.pnlBlkTransfer.EnableQuery = false;
            this.pnlBlkTransfer.EnableUpdate = true;
            this.pnlBlkTransfer.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TransferISLOCALCommand";
            this.pnlBlkTransfer.Location = new System.Drawing.Point(12, 39);
            this.pnlBlkTransfer.MasterPanel = null;
            this.pnlBlkTransfer.Name = "pnlBlkTransfer";
            this.pnlBlkTransfer.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlBlkTransfer.Size = new System.Drawing.Size(468, 364);
            this.pnlBlkTransfer.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.pnlBlkTransfer.TabIndex = 97;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "";
            this.txtID.Enabled = false;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(366, 248);
            this.txtID.MaxLength = 20;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(78, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 135;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // dtJoiningDate
            // 
            this.dtJoiningDate.CustomFormat = "dd/MM/yyyy";
            this.dtJoiningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtJoiningDate.Location = new System.Drawing.Point(366, 118);
            this.dtJoiningDate.Name = "dtJoiningDate";
            this.dtJoiningDate.Size = new System.Drawing.Size(97, 20);
            this.dtJoiningDate.TabIndex = 134;
            this.dtJoiningDate.Visible = false;
            // 
            // dtTransferDate
            // 
            this.dtTransferDate.CustomFormat = "dd/MM/yyyy";
            this.dtTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTransferDate.Location = new System.Drawing.Point(366, 144);
            this.dtTransferDate.Name = "dtTransferDate";
            this.dtTransferDate.Size = new System.Drawing.Size(97, 20);
            this.dtTransferDate.TabIndex = 133;
            this.dtTransferDate.Visible = false;
            // 
            // txtfurloughCity
            // 
            this.txtfurloughCity.AllowSpace = true;
            this.txtfurloughCity.AssociatedLookUpName = "";
            this.txtfurloughCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfurloughCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfurloughCity.ContinuationTextBox = null;
            this.txtfurloughCity.CustomEnabled = true;
            this.txtfurloughCity.DataFieldMapping = "PR_FURLOUGH";
            this.txtfurloughCity.Enabled = false;
            this.txtfurloughCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfurloughCity.GetRecordsOnUpDownKeys = false;
            this.txtfurloughCity.IsDate = false;
            this.txtfurloughCity.Location = new System.Drawing.Point(366, 196);
            this.txtfurloughCity.MaxLength = 20;
            this.txtfurloughCity.Name = "txtfurloughCity";
            this.txtfurloughCity.NumberFormat = "###,###,##0.00";
            this.txtfurloughCity.Postfix = "";
            this.txtfurloughCity.Prefix = "";
            this.txtfurloughCity.Size = new System.Drawing.Size(78, 20);
            this.txtfurloughCity.SkipValidation = false;
            this.txtfurloughCity.TabIndex = 132;
            this.txtfurloughCity.TextType = CrplControlLibrary.TextType.String;
            this.txtfurloughCity.Visible = false;
            // 
            // txtIsCordinate
            // 
            this.txtIsCordinate.AllowSpace = true;
            this.txtIsCordinate.AssociatedLookUpName = "";
            this.txtIsCordinate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIsCordinate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIsCordinate.ContinuationTextBox = null;
            this.txtIsCordinate.CustomEnabled = true;
            this.txtIsCordinate.DataFieldMapping = "PR_IS_COORDINAT";
            this.txtIsCordinate.Enabled = false;
            this.txtIsCordinate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIsCordinate.GetRecordsOnUpDownKeys = false;
            this.txtIsCordinate.IsDate = false;
            this.txtIsCordinate.Location = new System.Drawing.Point(366, 222);
            this.txtIsCordinate.MaxLength = 20;
            this.txtIsCordinate.Name = "txtIsCordinate";
            this.txtIsCordinate.NumberFormat = "###,###,##0.00";
            this.txtIsCordinate.Postfix = "";
            this.txtIsCordinate.Prefix = "";
            this.txtIsCordinate.Size = new System.Drawing.Size(78, 20);
            this.txtIsCordinate.SkipValidation = false;
            this.txtIsCordinate.TabIndex = 131;
            this.txtIsCordinate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIsCordinate.TextType = CrplControlLibrary.TextType.String;
            this.txtIsCordinate.Visible = false;
            // 
            // dtFastEndDate
            // 
            this.dtFastEndDate.CustomEnabled = true;
            this.dtFastEndDate.CustomFormat = "dd/MM/yyyy";
            this.dtFastEndDate.DataFieldMapping = "PR_FAST_END_DATE";
            this.dtFastEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFastEndDate.HasChanges = true;
            this.dtFastEndDate.Location = new System.Drawing.Point(366, 170);
            this.dtFastEndDate.Name = "dtFastEndDate";
            this.dtFastEndDate.NullValue = " ";
            this.dtFastEndDate.Size = new System.Drawing.Size(97, 20);
            this.dtFastEndDate.TabIndex = 130;
            this.dtFastEndDate.TabStop = false;
            this.dtFastEndDate.Value = new System.DateTime(2011, 1, 17, 0, 0, 0, 0);
            this.dtFastEndDate.Visible = false;
            // 
            // W_TypeOf
            // 
            this.W_TypeOf.AllowSpace = true;
            this.W_TypeOf.AssociatedLookUpName = "";
            this.W_TypeOf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_TypeOf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_TypeOf.ContinuationTextBox = null;
            this.W_TypeOf.CustomEnabled = true;
            this.W_TypeOf.DataFieldMapping = "W_TypeOf";
            this.W_TypeOf.Enabled = false;
            this.W_TypeOf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_TypeOf.GetRecordsOnUpDownKeys = false;
            this.W_TypeOf.IsDate = false;
            this.W_TypeOf.Location = new System.Drawing.Point(366, 92);
            this.W_TypeOf.MaxLength = 20;
            this.W_TypeOf.Name = "W_TypeOf";
            this.W_TypeOf.NumberFormat = "###,###,##0.00";
            this.W_TypeOf.Postfix = "";
            this.W_TypeOf.Prefix = "";
            this.W_TypeOf.Size = new System.Drawing.Size(78, 20);
            this.W_TypeOf.SkipValidation = false;
            this.W_TypeOf.TabIndex = 128;
            this.W_TypeOf.TextType = CrplControlLibrary.TextType.String;
            this.W_TypeOf.Visible = false;
            // 
            // WOption
            // 
            this.WOption.AllowSpace = true;
            this.WOption.AssociatedLookUpName = "";
            this.WOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.WOption.ContinuationTextBox = null;
            this.WOption.CustomEnabled = true;
            this.WOption.DataFieldMapping = "";
            this.WOption.Enabled = false;
            this.WOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WOption.GetRecordsOnUpDownKeys = false;
            this.WOption.IsDate = false;
            this.WOption.Location = new System.Drawing.Point(366, 14);
            this.WOption.MaxLength = 20;
            this.WOption.Name = "WOption";
            this.WOption.NumberFormat = "###,###,##0.00";
            this.WOption.Postfix = "";
            this.WOption.Prefix = "";
            this.WOption.Size = new System.Drawing.Size(78, 20);
            this.WOption.SkipValidation = false;
            this.WOption.TabIndex = 127;
            this.WOption.TextType = CrplControlLibrary.TextType.String;
            this.WOption.Visible = false;
            // 
            // txtPRTransferType
            // 
            this.txtPRTransferType.AllowSpace = true;
            this.txtPRTransferType.AssociatedLookUpName = "";
            this.txtPRTransferType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRTransferType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRTransferType.ContinuationTextBox = null;
            this.txtPRTransferType.CustomEnabled = true;
            this.txtPRTransferType.DataFieldMapping = "PR_TRANSFER_TYPE";
            this.txtPRTransferType.Enabled = false;
            this.txtPRTransferType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRTransferType.GetRecordsOnUpDownKeys = false;
            this.txtPRTransferType.IsDate = false;
            this.txtPRTransferType.Location = new System.Drawing.Point(366, 66);
            this.txtPRTransferType.MaxLength = 20;
            this.txtPRTransferType.Name = "txtPRTransferType";
            this.txtPRTransferType.NumberFormat = "###,###,##0.00";
            this.txtPRTransferType.Postfix = "";
            this.txtPRTransferType.Prefix = "";
            this.txtPRTransferType.Size = new System.Drawing.Size(78, 20);
            this.txtPRTransferType.SkipValidation = false;
            this.txtPRTransferType.TabIndex = 101;
            this.txtPRTransferType.TextType = CrplControlLibrary.TextType.String;
            this.txtPRTransferType.Visible = false;
            // 
            // txtPrPNo
            // 
            this.txtPrPNo.AllowSpace = true;
            this.txtPrPNo.AssociatedLookUpName = "";
            this.txtPrPNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrPNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrPNo.ContinuationTextBox = null;
            this.txtPrPNo.CustomEnabled = true;
            this.txtPrPNo.DataFieldMapping = "PR_TR_NO";
            this.txtPrPNo.Enabled = false;
            this.txtPrPNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrPNo.GetRecordsOnUpDownKeys = false;
            this.txtPrPNo.IsDate = false;
            this.txtPrPNo.Location = new System.Drawing.Point(366, 40);
            this.txtPrPNo.MaxLength = 20;
            this.txtPrPNo.Name = "txtPrPNo";
            this.txtPrPNo.NumberFormat = "###,###,##0.00";
            this.txtPrPNo.Postfix = "";
            this.txtPrPNo.Prefix = "";
            this.txtPrPNo.Size = new System.Drawing.Size(78, 20);
            this.txtPrPNo.SkipValidation = false;
            this.txtPrPNo.TabIndex = 100;
            this.txtPrPNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrPNo.TextType = CrplControlLibrary.TextType.Double;
            this.txtPrPNo.Visible = false;
            // 
            // lbtnflag2
            // 
            this.lbtnflag2.ActionLOVExists = "FLag2_LOVExists";
            this.lbtnflag2.ActionType = "FLag2_LOV";
            this.lbtnflag2.ConditionalFields = "";
            this.lbtnflag2.CustomEnabled = true;
            this.lbtnflag2.DataFieldMapping = "";
            this.lbtnflag2.DependentLovControls = "";
            this.lbtnflag2.HiddenColumns = "";
            this.lbtnflag2.Image = ((System.Drawing.Image)(resources.GetObject("lbtnflag2.Image")));
            this.lbtnflag2.LoadDependentEntities = true;
            this.lbtnflag2.Location = new System.Drawing.Point(293, 309);
            this.lbtnflag2.LookUpTitle = null;
            this.lbtnflag2.Name = "lbtnflag2";
            this.lbtnflag2.Size = new System.Drawing.Size(26, 21);
            this.lbtnflag2.SkipValidationOnLeave = false;
            this.lbtnflag2.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.lbtnflag2.TabIndex = 99;
            this.lbtnflag2.TabStop = false;
            this.lbtnflag2.UseVisualStyleBackColor = true;
            // 
            // lbtnflag1
            // 
            this.lbtnflag1.ActionLOVExists = "FLag1_LOVExists";
            this.lbtnflag1.ActionType = "FLag1_LOV";
            this.lbtnflag1.ConditionalFields = "";
            this.lbtnflag1.CustomEnabled = true;
            this.lbtnflag1.DataFieldMapping = "";
            this.lbtnflag1.DependentLovControls = "";
            this.lbtnflag1.HiddenColumns = "";
            this.lbtnflag1.Image = ((System.Drawing.Image)(resources.GetObject("lbtnflag1.Image")));
            this.lbtnflag1.LoadDependentEntities = true;
            this.lbtnflag1.Location = new System.Drawing.Point(293, 286);
            this.lbtnflag1.LookUpTitle = null;
            this.lbtnflag1.Name = "lbtnflag1";
            this.lbtnflag1.Size = new System.Drawing.Size(26, 21);
            this.lbtnflag1.SkipValidationOnLeave = false;
            this.lbtnflag1.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.lbtnflag1.TabIndex = 98;
            this.lbtnflag1.TabStop = false;
            this.lbtnflag1.UseVisualStyleBackColor = true;
            // 
            // lbtnCountry
            // 
            this.lbtnCountry.ActionLOVExists = "Country_LOVExists";
            this.lbtnCountry.ActionType = "Country_LOV";
            this.lbtnCountry.ConditionalFields = "";
            this.lbtnCountry.CustomEnabled = true;
            this.lbtnCountry.DataFieldMapping = "";
            this.lbtnCountry.DependentLovControls = "";
            this.lbtnCountry.HiddenColumns = "";
            this.lbtnCountry.Image = ((System.Drawing.Image)(resources.GetObject("lbtnCountry.Image")));
            this.lbtnCountry.LoadDependentEntities = true;
            this.lbtnCountry.Location = new System.Drawing.Point(293, 83);
            this.lbtnCountry.LookUpTitle = null;
            this.lbtnCountry.Name = "lbtnCountry";
            this.lbtnCountry.Size = new System.Drawing.Size(26, 21);
            this.lbtnCountry.SkipValidationOnLeave = false;
            this.lbtnCountry.SPName = "CHRIS_SP_ISFASTLOCAL_TRANSFER_MANAGER";
            this.lbtnCountry.TabIndex = 97;
            this.lbtnCountry.TabStop = false;
            this.lbtnCountry.UseVisualStyleBackColor = true;
            // 
            // txtPR_REMARKS
            // 
            this.txtPR_REMARKS.AllowSpace = true;
            this.txtPR_REMARKS.AssociatedLookUpName = "";
            this.txtPR_REMARKS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPR_REMARKS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPR_REMARKS.ContinuationTextBox = null;
            this.txtPR_REMARKS.CustomEnabled = true;
            this.txtPR_REMARKS.DataFieldMapping = "PR_REMARKS";
            this.txtPR_REMARKS.Enabled = false;
            this.txtPR_REMARKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_REMARKS.GetRecordsOnUpDownKeys = false;
            this.txtPR_REMARKS.IsDate = false;
            this.txtPR_REMARKS.Location = new System.Drawing.Point(187, 338);
            this.txtPR_REMARKS.MaxLength = 50;
            this.txtPR_REMARKS.Name = "txtPR_REMARKS";
            this.txtPR_REMARKS.NumberFormat = "###,###,##0.00";
            this.txtPR_REMARKS.Postfix = "";
            this.txtPR_REMARKS.Prefix = "";
            this.txtPR_REMARKS.Size = new System.Drawing.Size(269, 20);
            this.txtPR_REMARKS.SkipValidation = false;
            this.txtPR_REMARKS.TabIndex = 12;
            this.txtPR_REMARKS.TextType = CrplControlLibrary.TextType.String;
            this.txtPR_REMARKS.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPR_REMARKS_PreviewKeyDown);
            this.txtPR_REMARKS.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPR_REMARKS_KeyUp);
            this.txtPR_REMARKS.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_REMARKS_Validating);
            // 
            // txtCountry
            // 
            this.txtCountry.AllowSpace = true;
            this.txtCountry.AssociatedLookUpName = "lbtnCountry";
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountry.ContinuationTextBox = null;
            this.txtCountry.CustomEnabled = true;
            this.txtCountry.DataFieldMapping = "PR_COUNTRY";
            this.txtCountry.Enabled = false;
            this.txtCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountry.GetRecordsOnUpDownKeys = false;
            this.txtCountry.IsDate = false;
            this.txtCountry.Location = new System.Drawing.Point(187, 84);
            this.txtCountry.MaxLength = 15;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.NumberFormat = "###,###,##0.00";
            this.txtCountry.Postfix = "";
            this.txtCountry.Prefix = "";
            this.txtCountry.Size = new System.Drawing.Size(100, 20);
            this.txtCountry.SkipValidation = false;
            this.txtCountry.TabIndex = 1;
            this.txtCountry.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCITI_FLAG2
            // 
            this.txtCITI_FLAG2.AllowSpace = true;
            this.txtCITI_FLAG2.AssociatedLookUpName = "lbtnflag2";
            this.txtCITI_FLAG2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCITI_FLAG2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCITI_FLAG2.ContinuationTextBox = null;
            this.txtCITI_FLAG2.CustomEnabled = true;
            this.txtCITI_FLAG2.DataFieldMapping = "CITI_FLAG2";
            this.txtCITI_FLAG2.Enabled = false;
            this.txtCITI_FLAG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCITI_FLAG2.GetRecordsOnUpDownKeys = false;
            this.txtCITI_FLAG2.IsDate = false;
            this.txtCITI_FLAG2.Location = new System.Drawing.Point(187, 312);
            this.txtCITI_FLAG2.MaxLength = 15;
            this.txtCITI_FLAG2.Name = "txtCITI_FLAG2";
            this.txtCITI_FLAG2.NumberFormat = "###,###,##0.00";
            this.txtCITI_FLAG2.Postfix = "";
            this.txtCITI_FLAG2.Prefix = "";
            this.txtCITI_FLAG2.Size = new System.Drawing.Size(100, 20);
            this.txtCITI_FLAG2.SkipValidation = false;
            this.txtCITI_FLAG2.TabIndex = 11;
            this.txtCITI_FLAG2.TextType = CrplControlLibrary.TextType.String;
            this.txtCITI_FLAG2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCITI_FLAG2_KeyUp);
            // 
            // txtCity
            // 
            this.txtCity.AllowSpace = true;
            this.txtCity.AssociatedLookUpName = "";
            this.txtCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.ContinuationTextBox = null;
            this.txtCity.CustomEnabled = true;
            this.txtCity.DataFieldMapping = "PR_CITY";
            this.txtCity.Enabled = false;
            this.txtCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.GetRecordsOnUpDownKeys = false;
            this.txtCity.IsDate = false;
            this.txtCity.IsRequired = true;
            this.txtCity.Location = new System.Drawing.Point(187, 110);
            this.txtCity.MaxLength = 15;
            this.txtCity.Name = "txtCity";
            this.txtCity.NumberFormat = "###,###,##0.00";
            this.txtCity.Postfix = "";
            this.txtCity.Prefix = "";
            this.txtCity.Size = new System.Drawing.Size(100, 20);
            this.txtCity.SkipValidation = false;
            this.txtCity.TabIndex = 2;
            this.txtCity.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrAnnual
            // 
            this.txtCurrAnnual.AllowSpace = true;
            this.txtCurrAnnual.AssociatedLookUpName = "";
            this.txtCurrAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrAnnual.ContinuationTextBox = null;
            this.txtCurrAnnual.CustomEnabled = true;
            this.txtCurrAnnual.DataFieldMapping = "PR_ASR_DOL";
            this.txtCurrAnnual.Enabled = false;
            this.txtCurrAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrAnnual.GetRecordsOnUpDownKeys = false;
            this.txtCurrAnnual.IsDate = false;
            this.txtCurrAnnual.IsRequired = true;
            this.txtCurrAnnual.Location = new System.Drawing.Point(187, 187);
            this.txtCurrAnnual.MaxLength = 7;
            this.txtCurrAnnual.Name = "txtCurrAnnual";
            this.txtCurrAnnual.NumberFormat = "###,###,##0.00";
            this.txtCurrAnnual.Postfix = "";
            this.txtCurrAnnual.Prefix = "";
            this.txtCurrAnnual.ReadOnly = true;
            this.txtCurrAnnual.Size = new System.Drawing.Size(100, 20);
            this.txtCurrAnnual.SkipValidation = false;
            this.txtCurrAnnual.TabIndex = 6;
            this.txtCurrAnnual.TabStop = false;
            this.txtCurrAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrAnnual.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtPRDept
            // 
            this.txtPRDept.AllowSpace = true;
            this.txtPRDept.AssociatedLookUpName = "";
            this.txtPRDept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRDept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRDept.ContinuationTextBox = null;
            this.txtPRDept.CustomEnabled = true;
            this.txtPRDept.DataFieldMapping = "PR_DEPARTMENT_HC";
            this.txtPRDept.Enabled = false;
            this.txtPRDept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRDept.GetRecordsOnUpDownKeys = false;
            this.txtPRDept.IsDate = false;
            this.txtPRDept.Location = new System.Drawing.Point(187, 162);
            this.txtPRDept.MaxLength = 15;
            this.txtPRDept.Name = "txtPRDept";
            this.txtPRDept.NumberFormat = "###,###,##0.00";
            this.txtPRDept.Postfix = "";
            this.txtPRDept.Prefix = "";
            this.txtPRDept.Size = new System.Drawing.Size(100, 20);
            this.txtPRDept.SkipValidation = false;
            this.txtPRDept.TabIndex = 5;
            this.txtPRDept.TextType = CrplControlLibrary.TextType.String;
            this.txtPRDept.Validating += new System.ComponentModel.CancelEventHandler(this.txtPRDept_Validating);
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Enabled = false;
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.IsRequired = true;
            this.txtLevel.Location = new System.Drawing.Point(297, 137);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(61, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 4;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPR_EFFECTIVE
            // 
            this.txtPR_EFFECTIVE.CustomEnabled = true;
            this.txtPR_EFFECTIVE.CustomFormat = "dd/MM/yyyy";
            this.txtPR_EFFECTIVE.DataFieldMapping = "PR_EFFECTIVE";
            this.txtPR_EFFECTIVE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPR_EFFECTIVE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtPR_EFFECTIVE.HasChanges = true;
            this.txtPR_EFFECTIVE.IsRequired = true;
            this.txtPR_EFFECTIVE.Location = new System.Drawing.Point(187, 262);
            this.txtPR_EFFECTIVE.Name = "txtPR_EFFECTIVE";
            this.txtPR_EFFECTIVE.NullValue = " ";
            this.txtPR_EFFECTIVE.Size = new System.Drawing.Size(100, 20);
            this.txtPR_EFFECTIVE.TabIndex = 9;
            this.txtPR_EFFECTIVE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.txtPR_EFFECTIVE.Validating += new System.ComponentModel.CancelEventHandler(this.txtPR_EFFECTIVE_Validating);
            // 
            // txtDesg
            // 
            this.txtDesg.AllowSpace = true;
            this.txtDesg.AssociatedLookUpName = "";
            this.txtDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesg.ContinuationTextBox = null;
            this.txtDesg.CustomEnabled = true;
            this.txtDesg.DataFieldMapping = "PR_DESG";
            this.txtDesg.Enabled = false;
            this.txtDesg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesg.GetRecordsOnUpDownKeys = false;
            this.txtDesg.IsDate = false;
            this.txtDesg.IsRequired = true;
            this.txtDesg.Location = new System.Drawing.Point(187, 136);
            this.txtDesg.MaxLength = 3;
            this.txtDesg.Name = "txtDesg";
            this.txtDesg.NumberFormat = "###,###,##0.00";
            this.txtDesg.Postfix = "";
            this.txtDesg.Prefix = "";
            this.txtDesg.Size = new System.Drawing.Size(61, 20);
            this.txtDesg.SkipValidation = false;
            this.txtDesg.TabIndex = 3;
            this.txtDesg.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNewAnnual
            // 
            this.txtNewAnnual.AllowSpace = true;
            this.txtNewAnnual.AssociatedLookUpName = "";
            this.txtNewAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNewAnnual.ContinuationTextBox = null;
            this.txtNewAnnual.CustomEnabled = true;
            this.txtNewAnnual.DataFieldMapping = "PR_NEW_ASR";
            this.txtNewAnnual.Enabled = false;
            this.txtNewAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewAnnual.GetRecordsOnUpDownKeys = false;
            this.txtNewAnnual.IsDate = false;
            this.txtNewAnnual.IsRequired = true;
            this.txtNewAnnual.Location = new System.Drawing.Point(187, 211);
            this.txtNewAnnual.MaxLength = 7;
            this.txtNewAnnual.Name = "txtNewAnnual";
            this.txtNewAnnual.NumberFormat = "###,###,##0.00";
            this.txtNewAnnual.Postfix = "";
            this.txtNewAnnual.Prefix = "";
            this.txtNewAnnual.Size = new System.Drawing.Size(100, 20);
            this.txtNewAnnual.SkipValidation = false;
            this.txtNewAnnual.TabIndex = 7;
            this.txtNewAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNewAnnual.TextType = CrplControlLibrary.TextType.Double;
            this.txtNewAnnual.Validating += new System.ComponentModel.CancelEventHandler(this.txtNewAnnual_Validating);
            // 
            // txtCITI_FLAG1
            // 
            this.txtCITI_FLAG1.AllowSpace = true;
            this.txtCITI_FLAG1.AssociatedLookUpName = "lbtnflag1";
            this.txtCITI_FLAG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCITI_FLAG1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCITI_FLAG1.ContinuationTextBox = null;
            this.txtCITI_FLAG1.CustomEnabled = true;
            this.txtCITI_FLAG1.DataFieldMapping = "CITI_FLAG1";
            this.txtCITI_FLAG1.Enabled = false;
            this.txtCITI_FLAG1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCITI_FLAG1.GetRecordsOnUpDownKeys = false;
            this.txtCITI_FLAG1.IsDate = false;
            this.txtCITI_FLAG1.Location = new System.Drawing.Point(187, 286);
            this.txtCITI_FLAG1.MaxLength = 10;
            this.txtCITI_FLAG1.Name = "txtCITI_FLAG1";
            this.txtCITI_FLAG1.NumberFormat = "###,###,##0.00";
            this.txtCITI_FLAG1.Postfix = "";
            this.txtCITI_FLAG1.Prefix = "";
            this.txtCITI_FLAG1.Size = new System.Drawing.Size(100, 20);
            this.txtCITI_FLAG1.SkipValidation = false;
            this.txtCITI_FLAG1.TabIndex = 10;
            this.txtCITI_FLAG1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrency
            // 
            this.txtCurrency.AllowSpace = true;
            this.txtCurrency.AssociatedLookUpName = "";
            this.txtCurrency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrency.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrency.ContinuationTextBox = null;
            this.txtCurrency.CustomEnabled = true;
            this.txtCurrency.DataFieldMapping = "CURRENCY_TYPE";
            this.txtCurrency.Enabled = false;
            this.txtCurrency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrency.GetRecordsOnUpDownKeys = false;
            this.txtCurrency.IsDate = false;
            this.txtCurrency.IsRequired = true;
            this.txtCurrency.Location = new System.Drawing.Point(187, 236);
            this.txtCurrency.MaxLength = 10;
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.NumberFormat = "###,###,##0.00";
            this.txtCurrency.Postfix = "";
            this.txtCurrency.Prefix = "";
            this.txtCurrency.Size = new System.Drawing.Size(100, 20);
            this.txtCurrency.SkipValidation = false;
            this.txtCurrency.TabIndex = 8;
            this.txtCurrency.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Personnel_FastOrISTransfer_TransferPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(492, 466);
            this.Controls.Add(this.pnlBlkTransfer);
            this.CurrentPanelBlock = "pnlBlkTransfer";
            this.Name = "CHRIS_Personnel_FastOrISTransfer_TransferPage";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Personnel_FastOrISTransfer_TransferPage";
            this.Shown += new System.EventHandler(this.CHRIS_Personnel_FastOrISTransfer_TransferPage_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlBlkTransfer, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlBlkTransfer.ResumeLayout(false);
            this.pnlBlkTransfer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtCITI_FLAG1;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtCurrency;
        private CrplControlLibrary.SLTextBox txtNewAnnual;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLDatePicker txtPR_EFFECTIVE;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtCity;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtCountry;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtDesg;
        private CrplControlLibrary.SLTextBox txtLevel;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtPRDept;
        private CrplControlLibrary.SLTextBox txtCurrAnnual;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtCITI_FLAG2;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtPR_REMARKS;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlBlkTransfer;
        private CrplControlLibrary.LookupButton lbtnflag2;
        private CrplControlLibrary.LookupButton lbtnflag1;
        private CrplControlLibrary.LookupButton lbtnCountry;
        private CrplControlLibrary.SLTextBox txtPrPNo;
        private CrplControlLibrary.SLTextBox WOption;
        private CrplControlLibrary.SLTextBox W_TypeOf;
        private CrplControlLibrary.SLDatePicker dtFastEndDate;
        private CrplControlLibrary.SLTextBox txtfurloughCity;
        private CrplControlLibrary.SLTextBox txtIsCordinate;
        public CrplControlLibrary.SLTextBox txtPRTransferType;
        private System.Windows.Forms.DateTimePicker dtTransferDate;
        private System.Windows.Forms.DateTimePicker dtJoiningDate;
        private CrplControlLibrary.SLTextBox txtID;
    }
}