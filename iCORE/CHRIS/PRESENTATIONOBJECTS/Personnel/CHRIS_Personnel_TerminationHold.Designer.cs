namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_TerminationHold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelInProgress = new System.Windows.Forms.Panel();
            this.txtflag = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panelInProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelInProgress
            // 
            this.panelInProgress.Controls.Add(this.txtflag);
            this.panelInProgress.Controls.Add(this.label1);
            this.panelInProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelInProgress.Location = new System.Drawing.Point(55, 3);
            this.panelInProgress.Name = "panelInProgress";
            this.panelInProgress.Size = new System.Drawing.Size(434, 33);
            this.panelInProgress.TabIndex = 11;
            // 
            // txtflag
            // 
            this.txtflag.AllowSpace = true;
            this.txtflag.AssociatedLookUpName = "";
            this.txtflag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtflag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtflag.ContinuationTextBox = null;
            this.txtflag.CustomEnabled = true;
            this.txtflag.DataFieldMapping = "";
            this.txtflag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtflag.GetRecordsOnUpDownKeys = false;
            this.txtflag.IsDate = false;
            this.txtflag.Location = new System.Drawing.Point(285, 5);
            this.txtflag.MaxLength = 1;
            this.txtflag.Name = "txtflag";
            this.txtflag.NumberFormat = "###,###,##0.00";
            this.txtflag.Postfix = "";
            this.txtflag.Prefix = "";
            this.txtflag.Size = new System.Drawing.Size(51, 20);
            this.txtflag.SkipValidation = false;
            this.txtflag.TabIndex = 8;
            this.txtflag.TextType = CrplControlLibrary.TextType.String;
            this.txtflag.TextChanged += new System.EventHandler(this.txtflag_TextChanged);
            this.txtflag.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtflag_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter [H]old / [R]egularlize :";
            // 
            // CHRIS_Personnel_TerminationHold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(544, 40);
            this.Controls.Add(this.panelInProgress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_TerminationHold";
            this.ShowIcon = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CHRIS_Personnel_TerminationHold_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CHRIS_Personnel_TerminationHold_KeyDown);
            this.panelInProgress.ResumeLayout(false);
            this.panelInProgress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelInProgress;
        private CrplControlLibrary.SLTextBox txtflag;
        private System.Windows.Forms.Label label1;
    }
}