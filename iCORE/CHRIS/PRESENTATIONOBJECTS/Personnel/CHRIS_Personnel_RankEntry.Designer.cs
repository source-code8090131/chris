namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RankEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RankEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlPersonal = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtJoinDt = new CrplControlLibrary.SLDatePicker(this.components);
            this.lkbtnRank = new CrplControlLibrary.LookupButton(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtGradeSince = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrNo = new CrplControlLibrary.SLTextBox(this.components);
            this.DGVRnak = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Pr_P_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qtr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlRank = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlPersonal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVRnak)).BeginInit();
            this.pnlRank.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(67, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Size = new System.Drawing.Size(103, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 408);
            this.panel1.Size = new System.Drawing.Size(636, 60);
            // 
            // pnlPersonal
            // 
            this.pnlPersonal.ConcurrentPanels = null;
            this.pnlPersonal.Controls.Add(this.txtJoinDt);
            this.pnlPersonal.Controls.Add(this.lkbtnRank);
            this.pnlPersonal.Controls.Add(this.label7);
            this.pnlPersonal.Controls.Add(this.label6);
            this.pnlPersonal.Controls.Add(this.label5);
            this.pnlPersonal.Controls.Add(this.label4);
            this.pnlPersonal.Controls.Add(this.label3);
            this.pnlPersonal.Controls.Add(this.label2);
            this.pnlPersonal.Controls.Add(this.label1);
            this.pnlPersonal.Controls.Add(this.txtName);
            this.pnlPersonal.Controls.Add(this.txtBranch);
            this.pnlPersonal.Controls.Add(this.txtGradeSince);
            this.pnlPersonal.Controls.Add(this.txtLevel);
            this.pnlPersonal.Controls.Add(this.txtDesig);
            this.pnlPersonal.Controls.Add(this.txtPrNo);
            this.pnlPersonal.DataManager = null;
            this.pnlPersonal.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonal.DependentPanels = null;
            this.pnlPersonal.DisableDependentLoad = false;
            this.pnlPersonal.EnableDelete = false;
            this.pnlPersonal.EnableInsert = false;
            this.pnlPersonal.EnableQuery = false;
            this.pnlPersonal.EnableUpdate = false;
            this.pnlPersonal.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlPersonal.Location = new System.Drawing.Point(13, 95);
            this.pnlPersonal.MasterPanel = null;
            this.pnlPersonal.Name = "pnlPersonal";
            this.pnlPersonal.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonal.Size = new System.Drawing.Size(611, 76);
            this.pnlPersonal.SPName = "CHRIS_SP_RANK_PERSONNEL_MANAGER";
            this.pnlPersonal.TabIndex = 0;
            // 
            // txtJoinDt
            // 
            this.txtJoinDt.CustomEnabled = false;
            this.txtJoinDt.CustomFormat = "dd-MMM-yyyy";
            this.txtJoinDt.DataFieldMapping = "PR_JOINING_DATE";
            this.txtJoinDt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtJoinDt.HasChanges = false;
            this.txtJoinDt.Location = new System.Drawing.Point(95, 52);
            this.txtJoinDt.Name = "txtJoinDt";
            this.txtJoinDt.NullValue = " ";
            this.txtJoinDt.Size = new System.Drawing.Size(100, 20);
            this.txtJoinDt.TabIndex = 15;
            this.txtJoinDt.Value = new System.DateTime(2010, 12, 13, 0, 0, 0, 0);
            // 
            // lkbtnRank
            // 
            this.lkbtnRank.ActionLOVExists = "Personal_LovExists";
            this.lkbtnRank.ActionType = "Personal_Lov";
            this.lkbtnRank.ConditionalFields = "";
            this.lkbtnRank.CustomEnabled = true;
            this.lkbtnRank.DataFieldMapping = "";
            this.lkbtnRank.DependentLovControls = "";
            this.lkbtnRank.HiddenColumns = "PR_DESIG|PR_JOINING_DATE|PR_LEVEL|PR_BRANCH|PR_FIRST_NAME|GRD";
            this.lkbtnRank.Image = ((System.Drawing.Image)(resources.GetObject("lkbtnRank.Image")));
            this.lkbtnRank.LoadDependentEntities = true;
            this.lkbtnRank.Location = new System.Drawing.Point(201, 1);
            this.lkbtnRank.LookUpTitle = null;
            this.lkbtnRank.Name = "lkbtnRank";
            this.lkbtnRank.Size = new System.Drawing.Size(26, 21);
            this.lkbtnRank.SkipValidationOnLeave = false;
            this.lkbtnRank.SPName = "CHRIS_SP_RANK_PERSONNEL_MANAGER";
            this.lkbtnRank.TabIndex = 14;
            this.lkbtnRank.TabStop = false;
            this.lkbtnRank.Tag = "";
            this.lkbtnRank.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Joining Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(234, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Grade In Since";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(446, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Branch";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(446, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Level";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(235, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Desig";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Pr No";
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(280, 3);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(321, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 1;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PR_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(501, 52);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(100, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 6;
            this.txtBranch.TabStop = false;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtGradeSince
            // 
            this.txtGradeSince.AllowSpace = true;
            this.txtGradeSince.AssociatedLookUpName = "";
            this.txtGradeSince.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGradeSince.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGradeSince.ContinuationTextBox = null;
            this.txtGradeSince.CustomEnabled = true;
            this.txtGradeSince.DataFieldMapping = "GRD";
            this.txtGradeSince.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGradeSince.GetRecordsOnUpDownKeys = false;
            this.txtGradeSince.IsDate = false;
            this.txtGradeSince.Location = new System.Drawing.Point(332, 52);
            this.txtGradeSince.Name = "txtGradeSince";
            this.txtGradeSince.NumberFormat = "###,###,##0.00";
            this.txtGradeSince.Postfix = "";
            this.txtGradeSince.Prefix = "";
            this.txtGradeSince.ReadOnly = true;
            this.txtGradeSince.Size = new System.Drawing.Size(100, 20);
            this.txtGradeSince.SkipValidation = false;
            this.txtGradeSince.TabIndex = 4;
            this.txtGradeSince.TabStop = false;
            this.txtGradeSince.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(501, 26);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(100, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 5;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "PR_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(95, 26);
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(169, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 2;
            this.txtDesig.TabStop = false;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPrNo
            // 
            this.txtPrNo.AllowSpace = true;
            this.txtPrNo.AssociatedLookUpName = "lkbtnRank";
            this.txtPrNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrNo.ContinuationTextBox = null;
            this.txtPrNo.CustomEnabled = true;
            this.txtPrNo.DataFieldMapping = "PR_P_NO";
            this.txtPrNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrNo.GetRecordsOnUpDownKeys = false;
            this.txtPrNo.IsDate = false;
            this.txtPrNo.IsLookUpField = true;
            this.txtPrNo.IsRequired = true;
            this.txtPrNo.Location = new System.Drawing.Point(95, 3);
            this.txtPrNo.MaxLength = 8;
            this.txtPrNo.Name = "txtPrNo";
            this.txtPrNo.NumberFormat = "###,###,##0.00";
            this.txtPrNo.Postfix = "";
            this.txtPrNo.Prefix = "";
            this.txtPrNo.Size = new System.Drawing.Size(100, 20);
            this.txtPrNo.SkipValidation = false;
            this.txtPrNo.TabIndex = 0;
            this.txtPrNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrNo.TextType = CrplControlLibrary.TextType.Double;
            // 
            // DGVRnak
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVRnak.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVRnak.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Pr_P_No,
            this.Year,
            this.Qtr,
            this.Rank,
            this.ID});
            this.DGVRnak.ColumnToHide = null;
            this.DGVRnak.ColumnWidth = null;
            this.DGVRnak.CustomEnabled = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVRnak.DefaultCellStyle = dataGridViewCellStyle5;
            this.DGVRnak.DisplayColumnWrapper = null;
            this.DGVRnak.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVRnak.GridDefaultRow = 0;
            this.DGVRnak.Location = new System.Drawing.Point(0, 0);
            this.DGVRnak.Name = "DGVRnak";
            this.DGVRnak.ReadOnlyColumns = null;
            this.DGVRnak.RequiredColumns = null;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVRnak.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DGVRnak.RowHeadersWidth = 20;
            this.DGVRnak.Size = new System.Drawing.Size(326, 214);
            this.DGVRnak.SkippingColumns = null;
            this.DGVRnak.TabIndex = 7;
            this.DGVRnak.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVRnak_CellValidating);
            // 
            // Pr_P_No
            // 
            this.Pr_P_No.DataPropertyName = "PR_P_NO";
            this.Pr_P_No.HeaderText = "P_No.";
            this.Pr_P_No.Name = "Pr_P_No";
            this.Pr_P_No.Visible = false;
            // 
            // Year
            // 
            this.Year.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Year.DataPropertyName = "Year";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Year.DefaultCellStyle = dataGridViewCellStyle2;
            this.Year.HeaderText = "Year";
            this.Year.MaxInputLength = 4;
            this.Year.Name = "Year";
            // 
            // Qtr
            // 
            this.Qtr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Qtr.DataPropertyName = "R_QUARTER";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Qtr.DefaultCellStyle = dataGridViewCellStyle3;
            this.Qtr.HeaderText = "Qtr";
            this.Qtr.MaxInputLength = 1;
            this.Qtr.MinimumWidth = 3;
            this.Qtr.Name = "Qtr";
            // 
            // Rank
            // 
            this.Rank.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Rank.DataPropertyName = "R_RANK";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rank.DefaultCellStyle = dataGridViewCellStyle4;
            this.Rank.HeaderText = "Rank";
            this.Rank.MaxInputLength = 3;
            this.Rank.MinimumWidth = 3;
            this.Rank.Name = "Rank";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlRank
            // 
            this.pnlRank.ConcurrentPanels = null;
            this.pnlRank.Controls.Add(this.DGVRnak);
            this.pnlRank.DataManager = null;
            this.pnlRank.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlRank.DependentPanels = null;
            this.pnlRank.DisableDependentLoad = true;
            this.pnlRank.EnableDelete = true;
            this.pnlRank.EnableInsert = true;
            this.pnlRank.EnableQuery = false;
            this.pnlRank.EnableUpdate = true;
            this.pnlRank.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RANKCommand";
            this.pnlRank.Location = new System.Drawing.Point(151, 191);
            this.pnlRank.MasterPanel = null;
            this.pnlRank.Name = "pnlRank";
            this.pnlRank.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlRank.Size = new System.Drawing.Size(326, 214);
            this.pnlRank.SPName = "CHRIS_RANK_MANAGER";
            this.pnlRank.TabIndex = 12;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(357, 13);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(75, 13);
            this.lblUserName.TabIndex = 13;
            this.lblUserName.Text = "User Name :   ";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(83, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(465, 32);
            this.label9.TabIndex = 14;
            this.label9.Text = "Rank Entry";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CHRIS_Personnel_RankEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 468);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlRank);
            this.Controls.Add(this.pnlPersonal);
            this.Name = "CHRIS_Personnel_RankEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Rank Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Personnel_RankEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlPersonal, 0);
            this.Controls.SetChildIndex(this.pnlRank, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlPersonal.ResumeLayout(false);
            this.pnlPersonal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVRnak)).EndInit();
            this.pnlRank.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonal;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVRnak;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtGradeSince;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtPrNo;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlRank;
        private CrplControlLibrary.LookupButton lkbtnRank;
        private CrplControlLibrary.SLDatePicker txtJoinDt;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_P_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qtr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rank;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}