using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_RankEntry : ChrisMasterDetailForm
    {
        CmnDataManager cmnDM = new CmnDataManager();

        # region constructor

        public CHRIS_Personnel_RankEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Personnel_RankEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlRank);
            pnlPersonal.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlPersonal);
            this.CurrentPanelBlock = pnlPersonal.Name;

            this.Year.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.Qtr.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.Rank.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;

        }

        #endregion

        #region Methods

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {

                this.DGVRnak.CancelEdit();
                this.DGVRnak.EndEdit();
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtJoinDt.Value = null;
                this.txtPrNo.Focus();

            }
            else if (actionType == "Delete")
            {
                SLPanel currPanel = base.GetCurrentPanelBlock;
                if (currPanel != null && currPanel is SLPanelTabular)
                {
                    if (this.DGVRnak.SelectedRows != null && this.DGVRnak.SelectedRows.Count > 0)
                        base.DoToolbarActions(ctrlsCollection, actionType);
                }
            }
            else if (actionType == "Save")
            {
                DataTable dt = (this.DGVRnak.DataSource as DataTable);
                if (dt != null)
                {
                    base.AllGridsEndEdit(this);
                    DataTable dtAdd = dt.GetChanges(DataRowState.Added);
                    DataTable dtMod = dt.GetChanges(DataRowState.Modified);
                    if (dtAdd != null || dtMod != null)
                        base.DoToolbarActions(ctrlsCollection, actionType);
                }
            }
            else
                base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override bool Add()
        {
            // this.Category_Code.SkipValidationOnLeave = true;
            this.txtPrNo.Select();
            this.txtPrNo.Focus();
            //return base.Add();
            return false;
        }
        protected override bool Quit()
        {
            return base.Quit();
        }
        protected override bool Save()
        {
            if (this.txtPrNo.Text == "" || !(this.DGVRnak.Rows.Count > 1))
                return false;

            return base.Save();
        }

        #endregion

        #region Events

        private void DGVRnak_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (!this.DGVRnak.CurrentCell.IsInEditMode)
                return;

            if (this.DGVRnak.CurrentCell.OwningColumn.Name == "Year" || this.DGVRnak.CurrentCell.OwningColumn.Name == "Qtr")
            {
                if (this.DGVRnak.CurrentCell.EditedFormattedValue != null && this.DGVRnak.CurrentCell.EditedFormattedValue.ToString() != "")
                {
                    try
                    {
                        int i = int.Parse(this.DGVRnak.CurrentCell.EditedFormattedValue.ToString());
                    }
                    catch (FormatException ex)
                    {
                        MessageBox.Show("Enter Valid Number For 0-9 ", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    //e.Cancel = true;
                    //return;
                }
            }


            Result rslt;
            object obj = DGVRnak.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue;

            if (obj != null && obj.ToString() != string.Empty)
            {
                if (((e.ColumnIndex == 3) || (e.ColumnIndex == 1) || (e.ColumnIndex == 2)) && "" + DGVRnak.CurrentCell.EditedFormattedValue != "" + DGVRnak.CurrentCell.Value)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_P_NO", this.txtPrNo.Text);
                    param.Add("YEAR", DGVRnak.Rows[e.RowIndex].Cells[1].EditedFormattedValue);
                    param.Add("R_QUARTER", DGVRnak.Rows[e.RowIndex].Cells[2].EditedFormattedValue);
                    param.Add("R_RANK", DGVRnak.Rows[e.RowIndex].Cells[3].EditedFormattedValue);



                    rslt = cmnDM.GetData("CHRIS_SP_RANK_MANAGER", "RankCheck", param);


                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show("rank already exist");
                            e.Cancel = true;
                        }
                        else
                        {
                        }

                    }
                }

                if (this.FunctionConfig.CurrentOption == Function.Add)
                {

                    txtPrNo.SkipValidation = true;
                }
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtOption.Enabled = false;
            txtJoinDt.Value = null;
            tbtList.Visible = false;
            //this.FunctionConfig.F1 = Function.Add;
            //this.FunctionConfig.F6 = Function.Quit;
            //this.FunctionConfig.F10 = Function.Save;

            //this.ShowF1Option = true;
            //this.ShowF6Option = true;
            //this.ShowF10Option = true;

            //this.F1OptionText = " New Criteria";
            //this.F10OptionText = "F10 = Save";
            //this.F6OptionText = "F6 = Exit";

            this.ShowOptionKeys = false;
            this.ShowStatusBar = false;
            this.lblUserName.Text = "User Name :   " + this.UserName;

        }
        private void CHRIS_Personnel_RankEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (this.DGVRnak.Rows.Count < 2)
            {
                MessageBox.Show("Rank Records not Found", "Forms");
            }
            if (this.DGVRnak.Rows.Count > 0)
            {
                this.DGVRnak.FirstDisplayedScrollingRowIndex = 0;
                //this.DGVRnak.FirstDisplayedScrollingColumnIndex = 0;
                this.DGVRnak.Select();
                this.DGVRnak.Focus();
            }
        }


        #endregion


    }
}