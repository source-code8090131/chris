﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_IncrementOfficers_Detail : Form
    {
        public CHRIS_Personnel_IncrementOfficers_Detail()
        {
            InitializeComponent();
        }

        private void CHRIS_Personnel_IncrementOfficers_Detail_Load(object sender, EventArgs e)
        {
            this.txtPersonnel.ReadOnly = false;
            this.txtIncremnttype.ReadOnly = false;
            this.txtPR_RANK.ReadOnly = false;
            this.txtAnnualPresnt.ReadOnly = false;
            this.txt_PR_EFFECTIVE.ReadOnly = false;
            this.txtIncAmonut.ReadOnly = false;
            this.txtIncrementPer.ReadOnly = false;
            this.txtWP_pre.ReadOnly = false;
            // this.dtNextApp.ReadOnly = false;
            this.txtPR_REMARKS.ReadOnly = false;
            this.txtPR_ANNUAL_PRESENT.ReadOnly = false;
            this.txtPR_DESIG_PRESENT.ReadOnly = false;
            this.txtLevel.ReadOnly = false;
            this.txtName.ReadOnly = false;
            //this.txtPR_FUNCT_2_PRESENT.ReadOnly = false;
            //this.txtPR_ANNUAL_PREVIOUS1.ReadOnly = false;
            //this.txtPR_DESIG_PREVIOUS.ReadOnly = false;
            //this.txtPR_LEVEL_PREVIOUS.ReadOnly = false;
            //this.txtPR_FUNCT_1_PREVIOUS.ReadOnly = false;
            //this.txtPR_FUNCT_2_PREVIOUS.ReadOnly = false;
            //this.txtPR_CURRENT_PREVIOUS.ReadOnly = false;

            //txtAnnualPresnt

            DataTable dtDept = new DataTable();
            dtDept = SQLManager.CHRIS_SP_GET_INC_PRO_PRNO(CHRIS_Personnel_IncrementOfficers_View.SetValuePR_NO).Tables[0];

            foreach (System.Data.DataRow dtrow in dtDept.Rows)
            {
                txtPersonnel.Text = string.IsNullOrEmpty(dtrow["PR_IN_NO"].ToString().Trim()) ? "" : dtrow["PR_IN_NO"].ToString().Trim();
                txtName.Text = string.IsNullOrEmpty(dtrow["FirstName"].ToString().Trim()) ? "" : dtrow["FirstName"].ToString().Trim();
                txtIncremnttype.Text = string.IsNullOrEmpty(dtrow["PR_INC_TYPE"].ToString().Trim()) ? "" : dtrow["PR_INC_TYPE"].ToString().Trim();
                txtPR_RANK.Text = string.IsNullOrEmpty(dtrow["PR_RANK"].ToString().Trim()) ? "" : dtrow["PR_RANK"].ToString().Trim();
                dtPR_RANKING_DATE.Text = string.IsNullOrEmpty(dtrow["PR_RANKING_DATE"].ToString().Trim()) ? "" : dtrow["PR_RANKING_DATE"].ToString().Trim();
                txt_PR_EFFECTIVE.Text = string.IsNullOrEmpty(dtrow["PR_EFFECTIVE"].ToString().Trim()) ? "" : dtrow["PR_EFFECTIVE"].ToString().Trim();
                txtIncAmonut.Text = string.IsNullOrEmpty(dtrow["PR_INC_AMT"].ToString().Trim()) ? "" : dtrow["PR_INC_AMT"].ToString().Trim();
                txtIncrementPer.Text = string.IsNullOrEmpty(dtrow["PR_INC_PER"].ToString().Trim()) ? "" : dtrow["PR_INC_PER"].ToString().Trim();
                dtLastApp.Text = string.IsNullOrEmpty(dtrow["PR_LAST_APP"].ToString().Trim()) ? "" : dtrow["PR_LAST_APP"].ToString().Trim();
                dtNextApp.Text = string.IsNullOrEmpty(dtrow["PR_NEXT_APP"].ToString().Trim()) ? "" : dtrow["PR_NEXT_APP"].ToString().Trim();
                txtPR_REMARKS.Text = string.IsNullOrEmpty(dtrow["PR_REMARKS"].ToString().Trim()) ? "" : dtrow["PR_REMARKS"].ToString().Trim();
                txtPR_ANNUAL_PRESENT.Text = string.IsNullOrEmpty(dtrow["PR_ANNUAL_PRESENT"].ToString().Trim()) ? "" : dtrow["PR_ANNUAL_PRESENT"].ToString().Trim();

                txtPR_DESIG_PRESENT.Text = string.IsNullOrEmpty(dtrow["PR_DESIG_PRESENT"].ToString().Trim()) ? "" : dtrow["PR_DESIG_PRESENT"].ToString().Trim();
                txtLevel.Text = string.IsNullOrEmpty(dtrow["PR_LEVEL_PRESENT"].ToString().Trim()) ? "" : dtrow["PR_LEVEL_PRESENT"].ToString().Trim();
             //   txtPR_FUNCT_1_PRESENT.Text = string.IsNullOrEmpty(dtrow["PR_FUNCT_1_PRESENT"].ToString().Trim()) ? "" : dtrow["PR_FUNCT_1_PRESENT"].ToString().Trim();
             //   txtPR_FUNCT_2_PRESENT.Text = string.IsNullOrEmpty(dtrow["PR_FUNCT_2_PRESENT"].ToString().Trim()) ? "" : dtrow["PR_FUNCT_2_PRESENT"].ToString().Trim();
                txtWP_pre.Text = string.IsNullOrEmpty(dtrow["PR_ANNUAL_PREVIOUS"].ToString().Trim()) ? "" : dtrow["PR_ANNUAL_PREVIOUS"].ToString().Trim();
                //txtPR_DESIG_PREVIOUS.Text = string.IsNullOrEmpty(dtrow["PR_DESIG_PREVIOUS"].ToString().Trim()) ? "" : dtrow["PR_DESIG_PREVIOUS"].ToString().Trim();
                //txtPR_LEVEL_PREVIOUS.Text = string.IsNullOrEmpty(dtrow["PR_LEVEL_PREVIOUS"].ToString().Trim()) ? "" : dtrow["PR_LEVEL_PREVIOUS"].ToString().Trim();
                //txtPR_FUNCT_1_PREVIOUS.Text = string.IsNullOrEmpty(dtrow["PR_FUNCT_1_PREVIOUS"].ToString().Trim()) ? "" : dtrow["PR_FUNCT_1_PREVIOUS"].ToString().Trim();
                //txtPR_FUNCT_2_PREVIOUS.Text = string.IsNullOrEmpty(dtrow["PR_FUNCT_2_PREVIOUS"].ToString().Trim()) ? "" : dtrow["PR_FUNCT_2_PREVIOUS"].ToString().Trim();
                //txtPR_CURRENT_PREVIOUS.Text = string.IsNullOrEmpty(dtrow["PR_CURRENT_PREVIOUS"].ToString().Trim()) ? "" : dtrow["PR_CURRENT_PREVIOUS"].ToString().Trim();
                //txtPR_FINAL.Text = string.IsNullOrEmpty(dtrow["PR_FINAL"].ToString().Trim()) ? "" : dtrow["PR_FINAL"].ToString().Trim();


            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersonnel.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersonnel.Text.Trim());
                SQLManager.ApprovedTerminReject(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }
        }

        private void btnAproved_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPersonnel.Text.Trim()))
            {
                int id = Convert.ToInt32(txtPersonnel.Text.Trim());
                SQLManager.ApprovedPROMOTION(id);


                MessageBox.Show("Record Save Successfully");
                this.Close();
            }

        }
    }
}
