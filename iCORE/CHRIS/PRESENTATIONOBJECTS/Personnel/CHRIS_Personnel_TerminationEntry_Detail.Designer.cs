﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_TerminationEntry_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnAproved = new System.Windows.Forms.Button();
            this.txtFinalSettlementReport = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDelFromMic = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPowerOfAttornyCancel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNoticeOfStaffChanged = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIDCardRet = new CrplControlLibrary.SLTextBox(this.components);
            this.txtExitIntrvDone = new CrplControlLibrary.SLTextBox(this.components);
            this.txtResgAppr = new CrplControlLibrary.SLTextBox(this.components);
            this.txtResgRecieved = new CrplControlLibrary.SLTextBox(this.components);
            this.txtReason = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpTerminDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtTerminType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAproved);
            this.panel1.Controls.Add(this.btnReject);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txtFinalSettlementReport);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.txtDelFromMic);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.txtPowerOfAttornyCancel);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.txtNoticeOfStaffChanged);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txtIDCardRet);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtExitIntrvDone);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtResgAppr);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtResgRecieved);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtReason);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.dtpTerminDate);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtTerminType);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtFirstName);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtPersNo);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(714, 552);
            this.panel1.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(303, 507);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 25);
            this.label20.TabIndex = 92;
            this.label20.Text = "Y/N";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(26, 507);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(235, 25);
            this.label21.TabIndex = 91;
            this.label21.Text = "Final Settlement Report:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(303, 475);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 25);
            this.label22.TabIndex = 90;
            this.label22.Text = "Y/N";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(30, 475);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(231, 25);
            this.label23.TabIndex = 89;
            this.label23.Text = "Delete from Micro/Prime:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(303, 443);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 25);
            this.label24.TabIndex = 88;
            this.label24.Text = "Y/N";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(26, 443);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(235, 25);
            this.label25.TabIndex = 87;
            this.label25.Text = "Power Of Attorney Cancel:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(303, 411);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 25);
            this.label26.TabIndex = 86;
            this.label26.Text = "Y/N";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(26, 411);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(235, 25);
            this.label27.TabIndex = 85;
            this.label27.Text = "Notice Of Staff Change:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(303, 379);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 25);
            this.label16.TabIndex = 84;
            this.label16.Text = "Y/N";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(74, 379);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(187, 25);
            this.label17.TabIndex = 83;
            this.label17.Text = "ID Card Returned:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(303, 347);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 25);
            this.label18.TabIndex = 82;
            this.label18.Text = "Y/N";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(74, 347);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(187, 25);
            this.label19.TabIndex = 81;
            this.label19.Text = "Exit Interview Done:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(303, 315);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 25);
            this.label14.TabIndex = 80;
            this.label14.Text = "Y/N";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(22, 315);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(239, 25);
            this.label15.TabIndex = 79;
            this.label15.Text = "Approval Of Resignation:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(303, 283);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 25);
            this.label13.TabIndex = 78;
            this.label13.Text = "Y/N";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(53, 283);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(208, 25);
            this.label12.TabIndex = 77;
            this.label12.Text = "Resignation Received:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(74, 251);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(187, 25);
            this.label11.TabIndex = 76;
            this.label11.Text = "Reasons Of Leaving:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(74, 219);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(187, 25);
            this.label10.TabIndex = 75;
            this.label10.Text = "Termination Date:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(74, 187);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(187, 25);
            this.label9.TabIndex = 74;
            this.label9.Text = "Termination Type:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(74, 152);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(187, 25);
            this.label8.TabIndex = 73;
            this.label8.Text = "Name:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(74, 118);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(187, 25);
            this.label7.TabIndex = 72;
            this.label7.Text = "Personnel No.:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(151, 59);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(368, 22);
            this.label6.TabIndex = 94;
            this.label6.Text = "T E R M I N A T I O N S";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(151, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(368, 25);
            this.label5.TabIndex = 93;
            this.label5.Text = "Personnel System";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(370, 492);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(97, 40);
            this.btnClose.TabIndex = 95;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReject
            // 
            this.btnReject.Location = new System.Drawing.Point(473, 494);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(97, 40);
            this.btnReject.TabIndex = 96;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnAproved
            // 
            this.btnAproved.Location = new System.Drawing.Point(589, 492);
            this.btnAproved.Name = "btnAproved";
            this.btnAproved.Size = new System.Drawing.Size(97, 40);
            this.btnAproved.TabIndex = 97;
            this.btnAproved.Text = "Approve";
            this.btnAproved.UseVisualStyleBackColor = true;
            this.btnAproved.Click += new System.EventHandler(this.btnAproved_Click);
            // 
            // txtFinalSettlementReport
            // 
            this.txtFinalSettlementReport.AllowSpace = true;
            this.txtFinalSettlementReport.AssociatedLookUpName = "";
            this.txtFinalSettlementReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinalSettlementReport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinalSettlementReport.ContinuationTextBox = null;
            this.txtFinalSettlementReport.CustomEnabled = true;
            this.txtFinalSettlementReport.DataFieldMapping = "";
            this.txtFinalSettlementReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinalSettlementReport.GetRecordsOnUpDownKeys = false;
            this.txtFinalSettlementReport.IsDate = false;
            this.txtFinalSettlementReport.IsRequired = true;
            this.txtFinalSettlementReport.Location = new System.Drawing.Point(269, 507);
            this.txtFinalSettlementReport.Margin = new System.Windows.Forms.Padding(4);
            this.txtFinalSettlementReport.MaxLength = 1;
            this.txtFinalSettlementReport.Name = "txtFinalSettlementReport";
            this.txtFinalSettlementReport.NumberFormat = "###,###,##0.00";
            this.txtFinalSettlementReport.Postfix = "";
            this.txtFinalSettlementReport.Prefix = "";
            this.txtFinalSettlementReport.Size = new System.Drawing.Size(26, 24);
            this.txtFinalSettlementReport.SkipValidation = false;
            this.txtFinalSettlementReport.TabIndex = 71;
            this.txtFinalSettlementReport.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDelFromMic
            // 
            this.txtDelFromMic.AllowSpace = true;
            this.txtDelFromMic.AssociatedLookUpName = "";
            this.txtDelFromMic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDelFromMic.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDelFromMic.ContinuationTextBox = null;
            this.txtDelFromMic.CustomEnabled = true;
            this.txtDelFromMic.DataFieldMapping = "";
            this.txtDelFromMic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDelFromMic.GetRecordsOnUpDownKeys = false;
            this.txtDelFromMic.IsDate = false;
            this.txtDelFromMic.IsRequired = true;
            this.txtDelFromMic.Location = new System.Drawing.Point(269, 475);
            this.txtDelFromMic.Margin = new System.Windows.Forms.Padding(4);
            this.txtDelFromMic.MaxLength = 1;
            this.txtDelFromMic.Name = "txtDelFromMic";
            this.txtDelFromMic.NumberFormat = "###,###,##0.00";
            this.txtDelFromMic.Postfix = "";
            this.txtDelFromMic.Prefix = "";
            this.txtDelFromMic.Size = new System.Drawing.Size(26, 24);
            this.txtDelFromMic.SkipValidation = false;
            this.txtDelFromMic.TabIndex = 70;
            this.txtDelFromMic.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPowerOfAttornyCancel
            // 
            this.txtPowerOfAttornyCancel.AllowSpace = true;
            this.txtPowerOfAttornyCancel.AssociatedLookUpName = "";
            this.txtPowerOfAttornyCancel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPowerOfAttornyCancel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPowerOfAttornyCancel.ContinuationTextBox = null;
            this.txtPowerOfAttornyCancel.CustomEnabled = true;
            this.txtPowerOfAttornyCancel.DataFieldMapping = "";
            this.txtPowerOfAttornyCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPowerOfAttornyCancel.GetRecordsOnUpDownKeys = false;
            this.txtPowerOfAttornyCancel.IsDate = false;
            this.txtPowerOfAttornyCancel.IsRequired = true;
            this.txtPowerOfAttornyCancel.Location = new System.Drawing.Point(269, 443);
            this.txtPowerOfAttornyCancel.Margin = new System.Windows.Forms.Padding(4);
            this.txtPowerOfAttornyCancel.MaxLength = 1;
            this.txtPowerOfAttornyCancel.Name = "txtPowerOfAttornyCancel";
            this.txtPowerOfAttornyCancel.NumberFormat = "###,###,##0.00";
            this.txtPowerOfAttornyCancel.Postfix = "";
            this.txtPowerOfAttornyCancel.Prefix = "";
            this.txtPowerOfAttornyCancel.Size = new System.Drawing.Size(26, 24);
            this.txtPowerOfAttornyCancel.SkipValidation = false;
            this.txtPowerOfAttornyCancel.TabIndex = 69;
            this.txtPowerOfAttornyCancel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNoticeOfStaffChanged
            // 
            this.txtNoticeOfStaffChanged.AllowSpace = true;
            this.txtNoticeOfStaffChanged.AssociatedLookUpName = "";
            this.txtNoticeOfStaffChanged.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoticeOfStaffChanged.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoticeOfStaffChanged.ContinuationTextBox = null;
            this.txtNoticeOfStaffChanged.CustomEnabled = true;
            this.txtNoticeOfStaffChanged.DataFieldMapping = "";
            this.txtNoticeOfStaffChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoticeOfStaffChanged.GetRecordsOnUpDownKeys = false;
            this.txtNoticeOfStaffChanged.IsDate = false;
            this.txtNoticeOfStaffChanged.IsRequired = true;
            this.txtNoticeOfStaffChanged.Location = new System.Drawing.Point(269, 411);
            this.txtNoticeOfStaffChanged.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoticeOfStaffChanged.MaxLength = 1;
            this.txtNoticeOfStaffChanged.Name = "txtNoticeOfStaffChanged";
            this.txtNoticeOfStaffChanged.NumberFormat = "###,###,##0.00";
            this.txtNoticeOfStaffChanged.Postfix = "";
            this.txtNoticeOfStaffChanged.Prefix = "";
            this.txtNoticeOfStaffChanged.Size = new System.Drawing.Size(26, 24);
            this.txtNoticeOfStaffChanged.SkipValidation = false;
            this.txtNoticeOfStaffChanged.TabIndex = 68;
            this.txtNoticeOfStaffChanged.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtIDCardRet
            // 
            this.txtIDCardRet.AllowSpace = true;
            this.txtIDCardRet.AssociatedLookUpName = "";
            this.txtIDCardRet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIDCardRet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIDCardRet.ContinuationTextBox = null;
            this.txtIDCardRet.CustomEnabled = true;
            this.txtIDCardRet.DataFieldMapping = "";
            this.txtIDCardRet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDCardRet.GetRecordsOnUpDownKeys = false;
            this.txtIDCardRet.IsDate = false;
            this.txtIDCardRet.IsRequired = true;
            this.txtIDCardRet.Location = new System.Drawing.Point(269, 379);
            this.txtIDCardRet.Margin = new System.Windows.Forms.Padding(4);
            this.txtIDCardRet.MaxLength = 1;
            this.txtIDCardRet.Name = "txtIDCardRet";
            this.txtIDCardRet.NumberFormat = "###,###,##0.00";
            this.txtIDCardRet.Postfix = "";
            this.txtIDCardRet.Prefix = "";
            this.txtIDCardRet.Size = new System.Drawing.Size(26, 24);
            this.txtIDCardRet.SkipValidation = false;
            this.txtIDCardRet.TabIndex = 67;
            this.txtIDCardRet.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtExitIntrvDone
            // 
            this.txtExitIntrvDone.AllowSpace = true;
            this.txtExitIntrvDone.AssociatedLookUpName = "";
            this.txtExitIntrvDone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExitIntrvDone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtExitIntrvDone.ContinuationTextBox = null;
            this.txtExitIntrvDone.CustomEnabled = true;
            this.txtExitIntrvDone.DataFieldMapping = "";
            this.txtExitIntrvDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExitIntrvDone.GetRecordsOnUpDownKeys = false;
            this.txtExitIntrvDone.IsDate = false;
            this.txtExitIntrvDone.IsRequired = true;
            this.txtExitIntrvDone.Location = new System.Drawing.Point(269, 347);
            this.txtExitIntrvDone.Margin = new System.Windows.Forms.Padding(4);
            this.txtExitIntrvDone.MaxLength = 1;
            this.txtExitIntrvDone.Name = "txtExitIntrvDone";
            this.txtExitIntrvDone.NumberFormat = "###,###,##0.00";
            this.txtExitIntrvDone.Postfix = "";
            this.txtExitIntrvDone.Prefix = "";
            this.txtExitIntrvDone.Size = new System.Drawing.Size(26, 24);
            this.txtExitIntrvDone.SkipValidation = false;
            this.txtExitIntrvDone.TabIndex = 66;
            this.txtExitIntrvDone.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtResgAppr
            // 
            this.txtResgAppr.AllowSpace = true;
            this.txtResgAppr.AssociatedLookUpName = "";
            this.txtResgAppr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtResgAppr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtResgAppr.ContinuationTextBox = null;
            this.txtResgAppr.CustomEnabled = true;
            this.txtResgAppr.DataFieldMapping = "";
            this.txtResgAppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResgAppr.GetRecordsOnUpDownKeys = false;
            this.txtResgAppr.IsDate = false;
            this.txtResgAppr.IsRequired = true;
            this.txtResgAppr.Location = new System.Drawing.Point(269, 315);
            this.txtResgAppr.Margin = new System.Windows.Forms.Padding(4);
            this.txtResgAppr.MaxLength = 1;
            this.txtResgAppr.Name = "txtResgAppr";
            this.txtResgAppr.NumberFormat = "###,###,##0.00";
            this.txtResgAppr.Postfix = "";
            this.txtResgAppr.Prefix = "";
            this.txtResgAppr.Size = new System.Drawing.Size(26, 24);
            this.txtResgAppr.SkipValidation = false;
            this.txtResgAppr.TabIndex = 65;
            this.txtResgAppr.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtResgRecieved
            // 
            this.txtResgRecieved.AllowSpace = true;
            this.txtResgRecieved.AssociatedLookUpName = "";
            this.txtResgRecieved.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtResgRecieved.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtResgRecieved.ContinuationTextBox = null;
            this.txtResgRecieved.CustomEnabled = true;
            this.txtResgRecieved.DataFieldMapping = "";
            this.txtResgRecieved.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResgRecieved.GetRecordsOnUpDownKeys = false;
            this.txtResgRecieved.IsDate = false;
            this.txtResgRecieved.IsRequired = true;
            this.txtResgRecieved.Location = new System.Drawing.Point(269, 283);
            this.txtResgRecieved.Margin = new System.Windows.Forms.Padding(4);
            this.txtResgRecieved.MaxLength = 1;
            this.txtResgRecieved.Name = "txtResgRecieved";
            this.txtResgRecieved.NumberFormat = "###,###,##0.00";
            this.txtResgRecieved.Postfix = "";
            this.txtResgRecieved.Prefix = "";
            this.txtResgRecieved.Size = new System.Drawing.Size(26, 24);
            this.txtResgRecieved.SkipValidation = false;
            this.txtResgRecieved.TabIndex = 64;
            this.txtResgRecieved.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtReason
            // 
            this.txtReason.AllowSpace = true;
            this.txtReason.AssociatedLookUpName = "";
            this.txtReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReason.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReason.ContinuationTextBox = null;
            this.txtReason.CustomEnabled = true;
            this.txtReason.DataFieldMapping = "";
            this.txtReason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReason.GetRecordsOnUpDownKeys = false;
            this.txtReason.IsDate = false;
            this.txtReason.IsRequired = true;
            this.txtReason.Location = new System.Drawing.Point(269, 251);
            this.txtReason.Margin = new System.Windows.Forms.Padding(4);
            this.txtReason.MaxLength = 30;
            this.txtReason.Name = "txtReason";
            this.txtReason.NumberFormat = "###,###,##0.00";
            this.txtReason.Postfix = "";
            this.txtReason.Prefix = "";
            this.txtReason.Size = new System.Drawing.Size(198, 24);
            this.txtReason.SkipValidation = false;
            this.txtReason.TabIndex = 63;
            this.txtReason.TextType = CrplControlLibrary.TextType.String;
            // 
            // dtpTerminDate
            // 
            this.dtpTerminDate.CustomEnabled = true;
            this.dtpTerminDate.CustomFormat = "dd/MM/yyyy";
            this.dtpTerminDate.DataFieldMapping = "";
            this.dtpTerminDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTerminDate.HasChanges = true;
            this.dtpTerminDate.Location = new System.Drawing.Point(269, 219);
            this.dtpTerminDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpTerminDate.Name = "dtpTerminDate";
            this.dtpTerminDate.NullValue = " ";
            this.dtpTerminDate.Size = new System.Drawing.Size(132, 22);
            this.dtpTerminDate.TabIndex = 62;
            this.dtpTerminDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // txtTerminType
            // 
            this.txtTerminType.AllowSpace = true;
            this.txtTerminType.AssociatedLookUpName = "";
            this.txtTerminType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTerminType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTerminType.ContinuationTextBox = null;
            this.txtTerminType.CustomEnabled = true;
            this.txtTerminType.DataFieldMapping = "";
            this.txtTerminType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerminType.GetRecordsOnUpDownKeys = false;
            this.txtTerminType.IsDate = false;
            this.txtTerminType.IsRequired = true;
            this.txtTerminType.Location = new System.Drawing.Point(269, 187);
            this.txtTerminType.Margin = new System.Windows.Forms.Padding(4);
            this.txtTerminType.MaxLength = 1;
            this.txtTerminType.Name = "txtTerminType";
            this.txtTerminType.NumberFormat = "###,###,##0.00";
            this.txtTerminType.Postfix = "";
            this.txtTerminType.Prefix = "";
            this.txtTerminType.Size = new System.Drawing.Size(39, 24);
            this.txtTerminType.SkipValidation = false;
            this.txtTerminType.TabIndex = 61;
            this.txtTerminType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = false;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(269, 152);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 30;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(198, 24);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 60;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsLookUpField = true;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(269, 118);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(155, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 58;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // CHRIS_Personnel_TerminationEntry_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 572);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_TerminationEntry_Detail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_Personnel_TerminationEntry_Detail";
            this.Load += new System.EventHandler(this.CHRIS_Personnel_TerminationEntry_Detail_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtFinalSettlementReport;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txtDelFromMic;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtPowerOfAttornyCancel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLTextBox txtNoticeOfStaffChanged;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtIDCardRet;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtExitIntrvDone;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtResgAppr;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtResgRecieved;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtReason;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLDatePicker dtpTerminDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtTerminType;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAproved;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnClose;
    }
}