﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_RegularStaffHiringEnt_MC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Personnel_RegularStaffHiringEnt_MC));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnApprove = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dtGVDept = new System.Windows.Forms.DataGridView();
            this.dtGVEdu = new System.Windows.Forms.DataGridView();
            this.dtGVExp = new System.Windows.Forms.DataGridView();
            this.pnlTblBlkChild = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBlkChild = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChildName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date_of_Birth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_PR_PA_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_USER_IS_PRIME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_GROUP_HOSP = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_OPD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_GROUP_LIFE = new CrplControlLibrary.SLTextBox(this.components);
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.pnlSplBlkPersMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.txt_w_date_2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_NO_OF_CHILD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_BIRTH_SP = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_MARRIAGE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txt_PR_D_BIRTH = new CrplControlLibrary.SLDatePicker(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.txt_PR_ADD2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label53 = new System.Windows.Forms.Label();
            this.txt_PR_PHONE1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label52 = new System.Windows.Forms.Label();
            this.txt_PR_PHONE2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txt_PR_ID_CARD_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label49 = new System.Windows.Forms.Label();
            this.txt_PR_SEX = new CrplControlLibrary.SLTextBox(this.components);
            this.label48 = new System.Windows.Forms.Label();
            this.txt_PR_OLD_ID_CARD_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label47 = new System.Windows.Forms.Label();
            this.txt_PR_MARITAL = new CrplControlLibrary.SLTextBox(this.components);
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txt_PR_LANG1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label37 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txt_PR_LANG2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txt_PR_LANG3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ADD1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label42 = new System.Windows.Forms.Label();
            this.txt_PR_LANG4 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LANG6 = new CrplControlLibrary.SLTextBox(this.components);
            this.label41 = new System.Windows.Forms.Label();
            this.txt_PR_LANG5 = new CrplControlLibrary.SLTextBox(this.components);
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txt_PR_SPOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlPersonnelMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTxtBxUserID = new CrplControlLibrary.SLTextBox(this.components);
            this.slTxtbEmail = new CrplControlLibrary.SLTextBox(this.components);
            this.lbEmail = new System.Windows.Forms.Label();
            this.txtLoc = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.pnlView = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlSave = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_ADD_ANS = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.txt_W_VIEW_ANS = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.pnlDelete = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_DEL_ANS = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txt_PR_EXPIRY = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_ID_ISSUE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_NEW_ANNUAL_PACK = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CONFIRM = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_JOINING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_CONF_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_NEW_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CLOSE_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_TAX_PAID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_TAX_INC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_BANK_ID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ACCOUNT_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txt_PR_NATIONAL_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_ANNUAL_PACK = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_EMP_TYPE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_F_GEID_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_RELIGION = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_RELIGION = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FUNC_Title2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FUNC_Title1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGeid = new System.Windows.Forms.Label();
            this.lblFunctionalTitle = new System.Windows.Forms.Label();
            this.txt_PR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_F_RNAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LAST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.txtReportTo = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FIRST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblReportTo = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblPersonnelsNo = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSegment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dept = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.prContrib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contribution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrEYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Degree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_College = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_City = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pr_E_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prPeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Organization_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVEdu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVExp)).BeginInit();
            this.pnlTblBlkChild.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkChild)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.pnlSplBlkPersMain.SuspendLayout();
            this.pnlPersonnelMain.SuspendLayout();
            this.pnlView.SuspendLayout();
            this.pnlSave.SuspendLayout();
            this.pnlDelete.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Location = new System.Drawing.Point(471, 13);
            this.pnlHead.Margin = new System.Windows.Forms.Padding(4);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(854, 65);
            this.pnlHead.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(289, 38);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(313, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "P E R S O N N E L   I N F O R M A T I O N";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(365, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Personnel System";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1046, 86);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(215, 20);
            this.label23.TabIndex = 13;
            this.label23.Text = "Department Contribution";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(309, 612);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(192, 20);
            this.label24.TabIndex = 14;
            this.label24.Text = "Education Information";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1046, 226);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(257, 20);
            this.label25.TabIndex = 16;
            this.label25.Text = "Previous Employment History";
            // 
            // btnApprove
            // 
            this.btnApprove.Location = new System.Drawing.Point(1497, 42);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Size = new System.Drawing.Size(118, 26);
            this.btnApprove.TabIndex = 107;
            this.btnApprove.Text = "Approve";
            this.btnApprove.UseVisualStyleBackColor = true;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnReject
            // 
            this.btnReject.Location = new System.Drawing.Point(1373, 42);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(118, 26);
            this.btnReject.TabIndex = 108;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(1621, 42);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(118, 26);
            this.btnClose.TabIndex = 109;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dtGVDept
            // 
            this.dtGVDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGVDept.Location = new System.Drawing.Point(893, 110);
            this.dtGVDept.Name = "dtGVDept";
            this.dtGVDept.RowTemplate.Height = 24;
            this.dtGVDept.Size = new System.Drawing.Size(520, 114);
            this.dtGVDept.TabIndex = 110;
            // 
            // dtGVEdu
            // 
            this.dtGVEdu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGVEdu.Location = new System.Drawing.Point(13, 636);
            this.dtGVEdu.Name = "dtGVEdu";
            this.dtGVEdu.RowTemplate.Height = 24;
            this.dtGVEdu.Size = new System.Drawing.Size(872, 207);
            this.dtGVEdu.TabIndex = 111;
            // 
            // dtGVExp
            // 
            this.dtGVExp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGVExp.Location = new System.Drawing.Point(893, 249);
            this.dtGVExp.Name = "dtGVExp";
            this.dtGVExp.RowTemplate.Height = 24;
            this.dtGVExp.Size = new System.Drawing.Size(812, 114);
            this.dtGVExp.TabIndex = 112;
            // 
            // pnlTblBlkChild
            // 
            this.pnlTblBlkChild.ConcurrentPanels = null;
            this.pnlTblBlkChild.Controls.Add(this.dgvBlkChild);
            this.pnlTblBlkChild.DataManager = null;
            this.pnlTblBlkChild.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkChild.DependentPanels = null;
            this.pnlTblBlkChild.DisableDependentLoad = false;
            this.pnlTblBlkChild.EnableDelete = true;
            this.pnlTblBlkChild.EnableInsert = true;
            this.pnlTblBlkChild.EnableQuery = false;
            this.pnlTblBlkChild.EnableUpdate = true;
            this.pnlTblBlkChild.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntCHILDERNCommand";
            this.pnlTblBlkChild.Location = new System.Drawing.Point(1248, 695);
            this.pnlTblBlkChild.Margin = new System.Windows.Forms.Padding(4);
            this.pnlTblBlkChild.MasterPanel = null;
            this.pnlTblBlkChild.Name = "pnlTblBlkChild";
            this.pnlTblBlkChild.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkChild.Size = new System.Drawing.Size(432, 148);
            this.pnlTblBlkChild.SPName = "CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER";
            this.pnlTblBlkChild.TabIndex = 20;
            // 
            // dgvBlkChild
            // 
            this.dgvBlkChild.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBlkChild.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_P_NO,
            this.ChildName,
            this.Date_of_Birth});
            this.dgvBlkChild.ColumnToHide = null;
            this.dgvBlkChild.ColumnWidth = null;
            this.dgvBlkChild.CustomEnabled = true;
            this.dgvBlkChild.DisplayColumnWrapper = null;
            this.dgvBlkChild.GridDefaultRow = 0;
            this.dgvBlkChild.Location = new System.Drawing.Point(8, 4);
            this.dgvBlkChild.Margin = new System.Windows.Forms.Padding(4);
            this.dgvBlkChild.Name = "dgvBlkChild";
            this.dgvBlkChild.ReadOnlyColumns = null;
            this.dgvBlkChild.RequiredColumns = null;
            this.dgvBlkChild.Size = new System.Drawing.Size(392, 139);
            this.dgvBlkChild.SkippingColumns = null;
            this.dgvBlkChild.TabIndex = 24;
            // 
            // PR_P_NO
            // 
            this.PR_P_NO.DataPropertyName = "PR_P_NO";
            this.PR_P_NO.HeaderText = "PR_P_NO";
            this.PR_P_NO.Name = "PR_P_NO";
            this.PR_P_NO.Visible = false;
            // 
            // ChildName
            // 
            this.ChildName.DataPropertyName = "PR_CHILD_NAME";
            this.ChildName.HeaderText = "Child Name";
            this.ChildName.MaxInputLength = 30;
            this.ChildName.Name = "ChildName";
            this.ChildName.Width = 130;
            // 
            // Date_of_Birth
            // 
            this.Date_of_Birth.DataPropertyName = "PR_DATE_BIRTH";
            dataGridViewCellStyle1.Format = "dd/MM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.Date_of_Birth.DefaultCellStyle = dataGridViewCellStyle1;
            this.Date_of_Birth.HeaderText = "Date of Birth";
            this.Date_of_Birth.MaxInputLength = 10;
            this.Date_of_Birth.Name = "Date_of_Birth";
            this.Date_of_Birth.Width = 120;
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.txt_PR_PA_NO);
            this.slPanelSimple1.Controls.Add(this.txt_PR_USER_IS_PRIME);
            this.slPanelSimple1.Controls.Add(this.txt_PR_GROUP_HOSP);
            this.slPanelSimple1.Controls.Add(this.txt_PR_OPD);
            this.slPanelSimple1.Controls.Add(this.txt_PR_GROUP_LIFE);
            this.slPanelSimple1.Controls.Add(this.label32);
            this.slPanelSimple1.Controls.Add(this.label33);
            this.slPanelSimple1.Controls.Add(this.label36);
            this.slPanelSimple1.Controls.Add(this.label54);
            this.slPanelSimple1.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand";
            this.slPanelSimple1.Location = new System.Drawing.Point(893, 695);
            this.slPanelSimple1.Margin = new System.Windows.Forms.Padding(4);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(347, 150);
            this.slPanelSimple1.SPName = "CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER";
            this.slPanelSimple1.TabIndex = 19;
            // 
            // txt_PR_PA_NO
            // 
            this.txt_PR_PA_NO.AllowSpace = true;
            this.txt_PR_PA_NO.AssociatedLookUpName = "";
            this.txt_PR_PA_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PA_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PA_NO.ContinuationTextBox = null;
            this.txt_PR_PA_NO.CustomEnabled = true;
            this.txt_PR_PA_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PR_PA_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PA_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PA_NO.IsDate = false;
            this.txt_PR_PA_NO.Location = new System.Drawing.Point(205, 118);
            this.txt_PR_PA_NO.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_PA_NO.Name = "txt_PR_PA_NO";
            this.txt_PR_PA_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_PA_NO.Postfix = "";
            this.txt_PR_PA_NO.Prefix = "";
            this.txt_PR_PA_NO.Size = new System.Drawing.Size(93, 24);
            this.txt_PR_PA_NO.SkipValidation = false;
            this.txt_PR_PA_NO.TabIndex = 137;
            this.txt_PR_PA_NO.TabStop = false;
            this.txt_PR_PA_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_PA_NO.Visible = false;
            // 
            // txt_PR_USER_IS_PRIME
            // 
            this.txt_PR_USER_IS_PRIME.AllowSpace = true;
            this.txt_PR_USER_IS_PRIME.AssociatedLookUpName = "";
            this.txt_PR_USER_IS_PRIME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_USER_IS_PRIME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_USER_IS_PRIME.ContinuationTextBox = null;
            this.txt_PR_USER_IS_PRIME.CustomEnabled = true;
            this.txt_PR_USER_IS_PRIME.DataFieldMapping = "PR_USER_IS_PRIME";
            this.txt_PR_USER_IS_PRIME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_USER_IS_PRIME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_USER_IS_PRIME.IsDate = false;
            this.txt_PR_USER_IS_PRIME.Location = new System.Drawing.Point(205, 86);
            this.txt_PR_USER_IS_PRIME.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_USER_IS_PRIME.MaxLength = 10;
            this.txt_PR_USER_IS_PRIME.Name = "txt_PR_USER_IS_PRIME";
            this.txt_PR_USER_IS_PRIME.NumberFormat = "###,###,##0.00";
            this.txt_PR_USER_IS_PRIME.Postfix = "";
            this.txt_PR_USER_IS_PRIME.Prefix = "";
            this.txt_PR_USER_IS_PRIME.Size = new System.Drawing.Size(93, 24);
            this.txt_PR_USER_IS_PRIME.SkipValidation = false;
            this.txt_PR_USER_IS_PRIME.TabIndex = 23;
            this.txt_PR_USER_IS_PRIME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_GROUP_HOSP
            // 
            this.txt_PR_GROUP_HOSP.AllowSpace = true;
            this.txt_PR_GROUP_HOSP.AssociatedLookUpName = "";
            this.txt_PR_GROUP_HOSP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_GROUP_HOSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_GROUP_HOSP.ContinuationTextBox = null;
            this.txt_PR_GROUP_HOSP.CustomEnabled = true;
            this.txt_PR_GROUP_HOSP.DataFieldMapping = "PR_GROUP_HOSP";
            this.txt_PR_GROUP_HOSP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_GROUP_HOSP.GetRecordsOnUpDownKeys = false;
            this.txt_PR_GROUP_HOSP.IsDate = false;
            this.txt_PR_GROUP_HOSP.Location = new System.Drawing.Point(205, 58);
            this.txt_PR_GROUP_HOSP.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_GROUP_HOSP.MaxLength = 1;
            this.txt_PR_GROUP_HOSP.Name = "txt_PR_GROUP_HOSP";
            this.txt_PR_GROUP_HOSP.NumberFormat = "###,###,##0.00";
            this.txt_PR_GROUP_HOSP.Postfix = "";
            this.txt_PR_GROUP_HOSP.Prefix = "";
            this.txt_PR_GROUP_HOSP.Size = new System.Drawing.Size(45, 24);
            this.txt_PR_GROUP_HOSP.SkipValidation = false;
            this.txt_PR_GROUP_HOSP.TabIndex = 22;
            this.txt_PR_GROUP_HOSP.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_OPD
            // 
            this.txt_PR_OPD.AllowSpace = true;
            this.txt_PR_OPD.AssociatedLookUpName = "";
            this.txt_PR_OPD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_OPD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_OPD.ContinuationTextBox = null;
            this.txt_PR_OPD.CustomEnabled = true;
            this.txt_PR_OPD.DataFieldMapping = "PR_OPD";
            this.txt_PR_OPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_OPD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_OPD.IsDate = false;
            this.txt_PR_OPD.Location = new System.Drawing.Point(205, 31);
            this.txt_PR_OPD.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_OPD.MaxLength = 1;
            this.txt_PR_OPD.Name = "txt_PR_OPD";
            this.txt_PR_OPD.NumberFormat = "###,###,##0.00";
            this.txt_PR_OPD.Postfix = "";
            this.txt_PR_OPD.Prefix = "";
            this.txt_PR_OPD.Size = new System.Drawing.Size(45, 24);
            this.txt_PR_OPD.SkipValidation = false;
            this.txt_PR_OPD.TabIndex = 21;
            this.txt_PR_OPD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_GROUP_LIFE
            // 
            this.txt_PR_GROUP_LIFE.AllowSpace = true;
            this.txt_PR_GROUP_LIFE.AssociatedLookUpName = "";
            this.txt_PR_GROUP_LIFE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_GROUP_LIFE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_GROUP_LIFE.ContinuationTextBox = null;
            this.txt_PR_GROUP_LIFE.CustomEnabled = true;
            this.txt_PR_GROUP_LIFE.DataFieldMapping = "PR_GROUP_LIFE";
            this.txt_PR_GROUP_LIFE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_GROUP_LIFE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_GROUP_LIFE.IsDate = false;
            this.txt_PR_GROUP_LIFE.Location = new System.Drawing.Point(205, 4);
            this.txt_PR_GROUP_LIFE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_GROUP_LIFE.MaxLength = 1;
            this.txt_PR_GROUP_LIFE.Name = "txt_PR_GROUP_LIFE";
            this.txt_PR_GROUP_LIFE.NumberFormat = "###,###,##0.00";
            this.txt_PR_GROUP_LIFE.Postfix = "";
            this.txt_PR_GROUP_LIFE.Prefix = "";
            this.txt_PR_GROUP_LIFE.Size = new System.Drawing.Size(45, 24);
            this.txt_PR_GROUP_LIFE.SkipValidation = false;
            this.txt_PR_GROUP_LIFE.TabIndex = 20;
            this.txt_PR_GROUP_LIFE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(45, 86);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(147, 17);
            this.label32.TabIndex = 133;
            this.label32.Text = "User ID on PRIME :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(4, 58);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(193, 17);
            this.label33.TabIndex = 132;
            this.label33.Text = "Enrolled in Group Hosp. :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(13, 31);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(183, 17);
            this.label36.TabIndex = 131;
            this.label36.Text = "Enrolled in Out Patient :";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(20, 4);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(178, 17);
            this.label54.TabIndex = 130;
            this.label54.Text = "Enrolled in Group Life :";
            // 
            // pnlSplBlkPersMain
            // 
            this.pnlSplBlkPersMain.ConcurrentPanels = null;
            this.pnlSplBlkPersMain.Controls.Add(this.label26);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_w_date_2);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_NO_OF_CHILD);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_BIRTH_SP);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_MARRIAGE);
            this.pnlSplBlkPersMain.Controls.Add(this.label27);
            this.pnlSplBlkPersMain.Controls.Add(this.label30);
            this.pnlSplBlkPersMain.Controls.Add(this.label31);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_D_BIRTH);
            this.pnlSplBlkPersMain.Controls.Add(this.label34);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ADD2);
            this.pnlSplBlkPersMain.Controls.Add(this.label53);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_PHONE1);
            this.pnlSplBlkPersMain.Controls.Add(this.label52);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_PHONE2);
            this.pnlSplBlkPersMain.Controls.Add(this.label51);
            this.pnlSplBlkPersMain.Controls.Add(this.label50);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ID_CARD_NO);
            this.pnlSplBlkPersMain.Controls.Add(this.label49);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_SEX);
            this.pnlSplBlkPersMain.Controls.Add(this.label48);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_OLD_ID_CARD_NO);
            this.pnlSplBlkPersMain.Controls.Add(this.label47);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_MARITAL);
            this.pnlSplBlkPersMain.Controls.Add(this.label46);
            this.pnlSplBlkPersMain.Controls.Add(this.label45);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG1);
            this.pnlSplBlkPersMain.Controls.Add(this.label37);
            this.pnlSplBlkPersMain.Controls.Add(this.label44);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG2);
            this.pnlSplBlkPersMain.Controls.Add(this.label38);
            this.pnlSplBlkPersMain.Controls.Add(this.label43);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG3);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_ADD1);
            this.pnlSplBlkPersMain.Controls.Add(this.label42);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG4);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG6);
            this.pnlSplBlkPersMain.Controls.Add(this.label41);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_LANG5);
            this.pnlSplBlkPersMain.Controls.Add(this.label39);
            this.pnlSplBlkPersMain.Controls.Add(this.label40);
            this.pnlSplBlkPersMain.Controls.Add(this.txt_PR_SPOUSE);
            this.pnlSplBlkPersMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlSplBlkPersMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSplBlkPersMain.DependentPanels = null;
            this.pnlSplBlkPersMain.DisableDependentLoad = false;
            this.pnlSplBlkPersMain.EnableDelete = true;
            this.pnlSplBlkPersMain.EnableInsert = true;
            this.pnlSplBlkPersMain.EnableQuery = false;
            this.pnlSplBlkPersMain.EnableUpdate = true;
            this.pnlSplBlkPersMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand";
            this.pnlSplBlkPersMain.Location = new System.Drawing.Point(892, 384);
            this.pnlSplBlkPersMain.Margin = new System.Windows.Forms.Padding(4);
            this.pnlSplBlkPersMain.MasterPanel = null;
            this.pnlSplBlkPersMain.Name = "pnlSplBlkPersMain";
            this.pnlSplBlkPersMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSplBlkPersMain.Size = new System.Drawing.Size(813, 303);
            this.pnlSplBlkPersMain.SPName = " ";
            this.pnlSplBlkPersMain.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(581, 16);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 17);
            this.label26.TabIndex = 131;
            this.label26.Text = "Date :";
            this.label26.Visible = false;
            // 
            // txt_w_date_2
            // 
            this.txt_w_date_2.AllowSpace = true;
            this.txt_w_date_2.AssociatedLookUpName = "";
            this.txt_w_date_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_w_date_2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_w_date_2.ContinuationTextBox = null;
            this.txt_w_date_2.CustomEnabled = true;
            this.txt_w_date_2.DataFieldMapping = "";
            this.txt_w_date_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_w_date_2.GetRecordsOnUpDownKeys = false;
            this.txt_w_date_2.IsDate = false;
            this.txt_w_date_2.Location = new System.Drawing.Point(643, 14);
            this.txt_w_date_2.Margin = new System.Windows.Forms.Padding(4);
            this.txt_w_date_2.Name = "txt_w_date_2";
            this.txt_w_date_2.NumberFormat = "###,###,##0.00";
            this.txt_w_date_2.Postfix = "";
            this.txt_w_date_2.Prefix = "";
            this.txt_w_date_2.Size = new System.Drawing.Size(127, 24);
            this.txt_w_date_2.SkipValidation = false;
            this.txt_w_date_2.TabIndex = 130;
            this.txt_w_date_2.TextType = CrplControlLibrary.TextType.String;
            this.txt_w_date_2.Visible = false;
            // 
            // txt_PR_NO_OF_CHILD
            // 
            this.txt_PR_NO_OF_CHILD.AllowSpace = true;
            this.txt_PR_NO_OF_CHILD.AssociatedLookUpName = "";
            this.txt_PR_NO_OF_CHILD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NO_OF_CHILD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NO_OF_CHILD.ContinuationTextBox = null;
            this.txt_PR_NO_OF_CHILD.CustomEnabled = true;
            this.txt_PR_NO_OF_CHILD.DataFieldMapping = "PR_NO_OF_CHILD";
            this.txt_PR_NO_OF_CHILD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NO_OF_CHILD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NO_OF_CHILD.IsDate = false;
            this.txt_PR_NO_OF_CHILD.Location = new System.Drawing.Point(515, 274);
            this.txt_PR_NO_OF_CHILD.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_NO_OF_CHILD.Name = "txt_PR_NO_OF_CHILD";
            this.txt_PR_NO_OF_CHILD.NumberFormat = "###,###,##0.00";
            this.txt_PR_NO_OF_CHILD.Postfix = "";
            this.txt_PR_NO_OF_CHILD.Prefix = "";
            this.txt_PR_NO_OF_CHILD.Size = new System.Drawing.Size(69, 24);
            this.txt_PR_NO_OF_CHILD.SkipValidation = false;
            this.txt_PR_NO_OF_CHILD.TabIndex = 19;
            this.txt_PR_NO_OF_CHILD.TabStop = false;
            this.txt_PR_NO_OF_CHILD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_BIRTH_SP
            // 
            this.txt_PR_BIRTH_SP.CustomEnabled = true;
            this.txt_PR_BIRTH_SP.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_BIRTH_SP.DataFieldMapping = "PR_BIRTH_SP";
            this.txt_PR_BIRTH_SP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_BIRTH_SP.HasChanges = true;
            this.txt_PR_BIRTH_SP.Location = new System.Drawing.Point(515, 242);
            this.txt_PR_BIRTH_SP.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_BIRTH_SP.Name = "txt_PR_BIRTH_SP";
            this.txt_PR_BIRTH_SP.NullValue = " ";
            this.txt_PR_BIRTH_SP.Size = new System.Drawing.Size(160, 22);
            this.txt_PR_BIRTH_SP.TabIndex = 18;
            this.txt_PR_BIRTH_SP.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            // 
            // txt_PR_MARRIAGE
            // 
            this.txt_PR_MARRIAGE.CustomEnabled = true;
            this.txt_PR_MARRIAGE.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_MARRIAGE.DataFieldMapping = "PR_MARRIAGE";
            this.txt_PR_MARRIAGE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_MARRIAGE.HasChanges = true;
            this.txt_PR_MARRIAGE.Location = new System.Drawing.Point(167, 274);
            this.txt_PR_MARRIAGE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_MARRIAGE.Name = "txt_PR_MARRIAGE";
            this.txt_PR_MARRIAGE.NullValue = " ";
            this.txt_PR_MARRIAGE.Size = new System.Drawing.Size(127, 22);
            this.txt_PR_MARRIAGE.TabIndex = 16;
            this.txt_PR_MARRIAGE.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(355, 277);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(131, 17);
            this.label27.TabIndex = 126;
            this.label27.Text = "No. of Childern`s";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(383, 246);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 17);
            this.label30.TabIndex = 125;
            this.label30.Text = "Date Of Birth";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(377, 219);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(108, 17);
            this.label31.TabIndex = 124;
            this.label31.Text = "Spouse Name";
            // 
            // txt_PR_D_BIRTH
            // 
            this.txt_PR_D_BIRTH.CustomEnabled = true;
            this.txt_PR_D_BIRTH.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_D_BIRTH.DataFieldMapping = "";
            this.txt_PR_D_BIRTH.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_D_BIRTH.HasChanges = true;
            this.txt_PR_D_BIRTH.Location = new System.Drawing.Point(123, 142);
            this.txt_PR_D_BIRTH.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_D_BIRTH.Name = "txt_PR_D_BIRTH";
            this.txt_PR_D_BIRTH.NullValue = " ";
            this.txt_PR_D_BIRTH.Size = new System.Drawing.Size(127, 22);
            this.txt_PR_D_BIRTH.TabIndex = 5;
            this.txt_PR_D_BIRTH.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(13, 16);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 17);
            this.label34.TabIndex = 86;
            this.label34.Text = "Page 2";
            this.label34.Visible = false;
            // 
            // txt_PR_ADD2
            // 
            this.txt_PR_ADD2.AllowSpace = true;
            this.txt_PR_ADD2.AssociatedLookUpName = "";
            this.txt_PR_ADD2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ADD2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ADD2.ContinuationTextBox = null;
            this.txt_PR_ADD2.CustomEnabled = true;
            this.txt_PR_ADD2.DataFieldMapping = "";
            this.txt_PR_ADD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ADD2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ADD2.IsDate = false;
            this.txt_PR_ADD2.Location = new System.Drawing.Point(103, 66);
            this.txt_PR_ADD2.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_ADD2.MaxLength = 30;
            this.txt_PR_ADD2.Name = "txt_PR_ADD2";
            this.txt_PR_ADD2.NumberFormat = "###,###,##0.00";
            this.txt_PR_ADD2.Postfix = "";
            this.txt_PR_ADD2.Prefix = "";
            this.txt_PR_ADD2.Size = new System.Drawing.Size(350, 24);
            this.txt_PR_ADD2.SkipValidation = false;
            this.txt_PR_ADD2.TabIndex = 2;
            this.txt_PR_ADD2.TextType = CrplControlLibrary.TextType.AllCharacters;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(16, 277);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(140, 17);
            this.label53.TabIndex = 106;
            this.label53.Text = "Date of Marriage :";
            // 
            // txt_PR_PHONE1
            // 
            this.txt_PR_PHONE1.AllowSpace = true;
            this.txt_PR_PHONE1.AssociatedLookUpName = "";
            this.txt_PR_PHONE1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PHONE1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PHONE1.ContinuationTextBox = null;
            this.txt_PR_PHONE1.CustomEnabled = true;
            this.txt_PR_PHONE1.DataFieldMapping = "";
            this.txt_PR_PHONE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PHONE1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PHONE1.IsDate = false;
            this.txt_PR_PHONE1.Location = new System.Drawing.Point(103, 95);
            this.txt_PR_PHONE1.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_PHONE1.MaxLength = 15;
            this.txt_PR_PHONE1.Name = "txt_PR_PHONE1";
            this.txt_PR_PHONE1.NumberFormat = "###,###,##0.00";
            this.txt_PR_PHONE1.Postfix = "";
            this.txt_PR_PHONE1.Prefix = "";
            this.txt_PR_PHONE1.Size = new System.Drawing.Size(147, 24);
            this.txt_PR_PHONE1.SkipValidation = false;
            this.txt_PR_PHONE1.TabIndex = 3;
            this.txt_PR_PHONE1.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(17, 246);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(118, 17);
            this.label52.TabIndex = 105;
            this.label52.Text = "Marital Status :";
            // 
            // txt_PR_PHONE2
            // 
            this.txt_PR_PHONE2.AllowSpace = true;
            this.txt_PR_PHONE2.AssociatedLookUpName = "";
            this.txt_PR_PHONE2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_PHONE2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_PHONE2.ContinuationTextBox = null;
            this.txt_PR_PHONE2.CustomEnabled = true;
            this.txt_PR_PHONE2.DataFieldMapping = "";
            this.txt_PR_PHONE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_PHONE2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_PHONE2.IsDate = false;
            this.txt_PR_PHONE2.Location = new System.Drawing.Point(292, 95);
            this.txt_PR_PHONE2.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_PHONE2.MaxLength = 15;
            this.txt_PR_PHONE2.Name = "txt_PR_PHONE2";
            this.txt_PR_PHONE2.NumberFormat = "###,###,##0.00";
            this.txt_PR_PHONE2.Postfix = "";
            this.txt_PR_PHONE2.Prefix = "";
            this.txt_PR_PHONE2.Size = new System.Drawing.Size(161, 24);
            this.txt_PR_PHONE2.SkipValidation = false;
            this.txt_PR_PHONE2.TabIndex = 4;
            this.txt_PR_PHONE2.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(17, 207);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(97, 17);
            this.label51.TabIndex = 104;
            this.label51.Text = "Old ID Card.";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(17, 177);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(92, 17);
            this.label50.TabIndex = 103;
            this.label50.Text = "ID Card No.";
            // 
            // txt_PR_ID_CARD_NO
            // 
            this.txt_PR_ID_CARD_NO.AllowSpace = true;
            this.txt_PR_ID_CARD_NO.AssociatedLookUpName = "";
            this.txt_PR_ID_CARD_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ID_CARD_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ID_CARD_NO.ContinuationTextBox = null;
            this.txt_PR_ID_CARD_NO.CustomEnabled = true;
            this.txt_PR_ID_CARD_NO.DataFieldMapping = "PR_ID_CARD_NO";
            this.txt_PR_ID_CARD_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ID_CARD_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ID_CARD_NO.IsDate = false;
            this.txt_PR_ID_CARD_NO.Location = new System.Drawing.Point(123, 174);
            this.txt_PR_ID_CARD_NO.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_ID_CARD_NO.MaxLength = 15;
            this.txt_PR_ID_CARD_NO.Name = "txt_PR_ID_CARD_NO";
            this.txt_PR_ID_CARD_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_ID_CARD_NO.Postfix = "";
            this.txt_PR_ID_CARD_NO.Prefix = "";
            this.txt_PR_ID_CARD_NO.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_ID_CARD_NO.SkipValidation = false;
            this.txt_PR_ID_CARD_NO.TabIndex = 7;
            this.txt_PR_ID_CARD_NO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(261, 149);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 17);
            this.label49.TabIndex = 102;
            this.label49.Text = "Sex";
            // 
            // txt_PR_SEX
            // 
            this.txt_PR_SEX.AllowSpace = true;
            this.txt_PR_SEX.AssociatedLookUpName = "";
            this.txt_PR_SEX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_SEX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_SEX.ContinuationTextBox = null;
            this.txt_PR_SEX.CustomEnabled = true;
            this.txt_PR_SEX.DataFieldMapping = "";
            this.txt_PR_SEX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_SEX.GetRecordsOnUpDownKeys = false;
            this.txt_PR_SEX.IsDate = false;
            this.txt_PR_SEX.Location = new System.Drawing.Point(307, 142);
            this.txt_PR_SEX.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_SEX.MaxLength = 1;
            this.txt_PR_SEX.Name = "txt_PR_SEX";
            this.txt_PR_SEX.NumberFormat = "###,###,##0.00";
            this.txt_PR_SEX.Postfix = "";
            this.txt_PR_SEX.Prefix = "";
            this.txt_PR_SEX.Size = new System.Drawing.Size(39, 24);
            this.txt_PR_SEX.SkipValidation = false;
            this.txt_PR_SEX.TabIndex = 6;
            this.txt_PR_SEX.TextType = CrplControlLibrary.TextType.String;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(17, 144);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 17);
            this.label48.TabIndex = 101;
            this.label48.Text = "Date of Birth";
            // 
            // txt_PR_OLD_ID_CARD_NO
            // 
            this.txt_PR_OLD_ID_CARD_NO.AllowSpace = true;
            this.txt_PR_OLD_ID_CARD_NO.AssociatedLookUpName = "";
            this.txt_PR_OLD_ID_CARD_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_OLD_ID_CARD_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_OLD_ID_CARD_NO.ContinuationTextBox = null;
            this.txt_PR_OLD_ID_CARD_NO.CustomEnabled = true;
            this.txt_PR_OLD_ID_CARD_NO.DataFieldMapping = "PR_OLD_ID_CARD_NO";
            this.txt_PR_OLD_ID_CARD_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_OLD_ID_CARD_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_OLD_ID_CARD_NO.IsDate = false;
            this.txt_PR_OLD_ID_CARD_NO.Location = new System.Drawing.Point(123, 203);
            this.txt_PR_OLD_ID_CARD_NO.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_OLD_ID_CARD_NO.MaxLength = 14;
            this.txt_PR_OLD_ID_CARD_NO.Name = "txt_PR_OLD_ID_CARD_NO";
            this.txt_PR_OLD_ID_CARD_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_OLD_ID_CARD_NO.Postfix = "";
            this.txt_PR_OLD_ID_CARD_NO.Prefix = "";
            this.txt_PR_OLD_ID_CARD_NO.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_OLD_ID_CARD_NO.SkipValidation = false;
            this.txt_PR_OLD_ID_CARD_NO.TabIndex = 8;
            this.txt_PR_OLD_ID_CARD_NO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(268, 100);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(23, 17);
            this.label47.TabIndex = 100;
            this.label47.Text = "2)";
            // 
            // txt_PR_MARITAL
            // 
            this.txt_PR_MARITAL.AllowSpace = true;
            this.txt_PR_MARITAL.AssociatedLookUpName = "";
            this.txt_PR_MARITAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_MARITAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_MARITAL.ContinuationTextBox = null;
            this.txt_PR_MARITAL.CustomEnabled = true;
            this.txt_PR_MARITAL.DataFieldMapping = "";
            this.txt_PR_MARITAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_MARITAL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_MARITAL.IsDate = false;
            this.txt_PR_MARITAL.Location = new System.Drawing.Point(167, 245);
            this.txt_PR_MARITAL.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_MARITAL.MaxLength = 1;
            this.txt_PR_MARITAL.Name = "txt_PR_MARITAL";
            this.txt_PR_MARITAL.NumberFormat = "###,###,##0.00";
            this.txt_PR_MARITAL.Postfix = "";
            this.txt_PR_MARITAL.Prefix = "";
            this.txt_PR_MARITAL.Size = new System.Drawing.Size(45, 24);
            this.txt_PR_MARITAL.SkipValidation = false;
            this.txt_PR_MARITAL.TabIndex = 15;
            this.txt_PR_MARITAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(17, 100);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(74, 17);
            this.label46.TabIndex = 99;
            this.label46.Text = "Phone 1)";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(611, 180);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(23, 17);
            this.label45.TabIndex = 98;
            this.label45.Text = "6)";
            // 
            // txt_PR_LANG1
            // 
            this.txt_PR_LANG1.AllowSpace = true;
            this.txt_PR_LANG1.AssociatedLookUpName = "";
            this.txt_PR_LANG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG1.ContinuationTextBox = null;
            this.txt_PR_LANG1.CustomEnabled = true;
            this.txt_PR_LANG1.DataFieldMapping = "PR_LANG1";
            this.txt_PR_LANG1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG1.IsDate = false;
            this.txt_PR_LANG1.Location = new System.Drawing.Point(643, 42);
            this.txt_PR_LANG1.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LANG1.MaxLength = 10;
            this.txt_PR_LANG1.Name = "txt_PR_LANG1";
            this.txt_PR_LANG1.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG1.Postfix = "";
            this.txt_PR_LANG1.Prefix = "";
            this.txt_PR_LANG1.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_LANG1.SkipValidation = false;
            this.txt_PR_LANG1.TabIndex = 9;
            this.txt_PR_LANG1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(275, 4);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(223, 17);
            this.label37.TabIndex = 90;
            this.label37.Text = "PERSONNELS INFORMATION";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(611, 153);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(23, 17);
            this.label44.TabIndex = 97;
            this.label44.Text = "5)";
            // 
            // txt_PR_LANG2
            // 
            this.txt_PR_LANG2.AllowSpace = true;
            this.txt_PR_LANG2.AssociatedLookUpName = "";
            this.txt_PR_LANG2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG2.ContinuationTextBox = null;
            this.txt_PR_LANG2.CustomEnabled = true;
            this.txt_PR_LANG2.DataFieldMapping = "PR_LANG2";
            this.txt_PR_LANG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG2.IsDate = false;
            this.txt_PR_LANG2.Location = new System.Drawing.Point(643, 69);
            this.txt_PR_LANG2.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LANG2.MaxLength = 10;
            this.txt_PR_LANG2.Name = "txt_PR_LANG2";
            this.txt_PR_LANG2.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG2.Postfix = "";
            this.txt_PR_LANG2.Prefix = "";
            this.txt_PR_LANG2.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_LANG2.SkipValidation = false;
            this.txt_PR_LANG2.TabIndex = 10;
            this.txt_PR_LANG2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(17, 44);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 17);
            this.label38.TabIndex = 91;
            this.label38.Text = "Address :";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(611, 127);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(23, 17);
            this.label43.TabIndex = 96;
            this.label43.Text = "4)";
            // 
            // txt_PR_LANG3
            // 
            this.txt_PR_LANG3.AllowSpace = true;
            this.txt_PR_LANG3.AssociatedLookUpName = "";
            this.txt_PR_LANG3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG3.ContinuationTextBox = null;
            this.txt_PR_LANG3.CustomEnabled = true;
            this.txt_PR_LANG3.DataFieldMapping = "PR_LANG3";
            this.txt_PR_LANG3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG3.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG3.IsDate = false;
            this.txt_PR_LANG3.Location = new System.Drawing.Point(643, 96);
            this.txt_PR_LANG3.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LANG3.MaxLength = 10;
            this.txt_PR_LANG3.Name = "txt_PR_LANG3";
            this.txt_PR_LANG3.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG3.Postfix = "";
            this.txt_PR_LANG3.Prefix = "";
            this.txt_PR_LANG3.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_LANG3.SkipValidation = false;
            this.txt_PR_LANG3.TabIndex = 11;
            this.txt_PR_LANG3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_ADD1
            // 
            this.txt_PR_ADD1.AllowSpace = true;
            this.txt_PR_ADD1.AssociatedLookUpName = "";
            this.txt_PR_ADD1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ADD1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ADD1.ContinuationTextBox = null;
            this.txt_PR_ADD1.CustomEnabled = true;
            this.txt_PR_ADD1.DataFieldMapping = "";
            this.txt_PR_ADD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ADD1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ADD1.IsDate = false;
            this.txt_PR_ADD1.IsRequired = true;
            this.txt_PR_ADD1.Location = new System.Drawing.Point(103, 38);
            this.txt_PR_ADD1.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_ADD1.MaxLength = 30;
            this.txt_PR_ADD1.Name = "txt_PR_ADD1";
            this.txt_PR_ADD1.NumberFormat = "###,###,##0.00";
            this.txt_PR_ADD1.Postfix = "";
            this.txt_PR_ADD1.Prefix = "";
            this.txt_PR_ADD1.Size = new System.Drawing.Size(350, 24);
            this.txt_PR_ADD1.SkipValidation = false;
            this.txt_PR_ADD1.TabIndex = 1;
            this.txt_PR_ADD1.TextType = CrplControlLibrary.TextType.AllCharacters;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(611, 101);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(23, 17);
            this.label42.TabIndex = 95;
            this.label42.Text = "3)";
            // 
            // txt_PR_LANG4
            // 
            this.txt_PR_LANG4.AllowSpace = true;
            this.txt_PR_LANG4.AssociatedLookUpName = "";
            this.txt_PR_LANG4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG4.ContinuationTextBox = null;
            this.txt_PR_LANG4.CustomEnabled = true;
            this.txt_PR_LANG4.DataFieldMapping = "PR_LANG4";
            this.txt_PR_LANG4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG4.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG4.IsDate = false;
            this.txt_PR_LANG4.Location = new System.Drawing.Point(643, 123);
            this.txt_PR_LANG4.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LANG4.MaxLength = 10;
            this.txt_PR_LANG4.Name = "txt_PR_LANG4";
            this.txt_PR_LANG4.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG4.Postfix = "";
            this.txt_PR_LANG4.Prefix = "";
            this.txt_PR_LANG4.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_LANG4.SkipValidation = false;
            this.txt_PR_LANG4.TabIndex = 12;
            this.txt_PR_LANG4.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LANG6
            // 
            this.txt_PR_LANG6.AllowSpace = true;
            this.txt_PR_LANG6.AssociatedLookUpName = "";
            this.txt_PR_LANG6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG6.ContinuationTextBox = null;
            this.txt_PR_LANG6.CustomEnabled = true;
            this.txt_PR_LANG6.DataFieldMapping = "PR_LANG6";
            this.txt_PR_LANG6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG6.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG6.IsDate = false;
            this.txt_PR_LANG6.Location = new System.Drawing.Point(643, 177);
            this.txt_PR_LANG6.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LANG6.MaxLength = 10;
            this.txt_PR_LANG6.Name = "txt_PR_LANG6";
            this.txt_PR_LANG6.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG6.Postfix = "";
            this.txt_PR_LANG6.Prefix = "";
            this.txt_PR_LANG6.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_LANG6.SkipValidation = false;
            this.txt_PR_LANG6.TabIndex = 14;
            this.txt_PR_LANG6.TextType = CrplControlLibrary.TextType.String;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(611, 74);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(23, 17);
            this.label41.TabIndex = 94;
            this.label41.Text = "2)";
            // 
            // txt_PR_LANG5
            // 
            this.txt_PR_LANG5.AllowSpace = true;
            this.txt_PR_LANG5.AssociatedLookUpName = "";
            this.txt_PR_LANG5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LANG5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LANG5.ContinuationTextBox = null;
            this.txt_PR_LANG5.CustomEnabled = true;
            this.txt_PR_LANG5.DataFieldMapping = "PR_LANG5";
            this.txt_PR_LANG5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LANG5.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LANG5.IsDate = false;
            this.txt_PR_LANG5.Location = new System.Drawing.Point(643, 150);
            this.txt_PR_LANG5.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LANG5.MaxLength = 10;
            this.txt_PR_LANG5.Name = "txt_PR_LANG5";
            this.txt_PR_LANG5.NumberFormat = "###,###,##0.00";
            this.txt_PR_LANG5.Postfix = "";
            this.txt_PR_LANG5.Prefix = "";
            this.txt_PR_LANG5.Size = new System.Drawing.Size(127, 24);
            this.txt_PR_LANG5.SkipValidation = false;
            this.txt_PR_LANG5.TabIndex = 13;
            this.txt_PR_LANG5.TextType = CrplControlLibrary.TextType.String;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(511, 47);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(88, 17);
            this.label39.TabIndex = 92;
            this.label39.Text = "Languages";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(611, 47);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(23, 17);
            this.label40.TabIndex = 93;
            this.label40.Text = "1)";
            // 
            // txt_PR_SPOUSE
            // 
            this.txt_PR_SPOUSE.AllowSpace = true;
            this.txt_PR_SPOUSE.AssociatedLookUpName = "";
            this.txt_PR_SPOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_SPOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_SPOUSE.ContinuationTextBox = null;
            this.txt_PR_SPOUSE.CustomEnabled = true;
            this.txt_PR_SPOUSE.DataFieldMapping = "PR_SPOUSE";
            this.txt_PR_SPOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_SPOUSE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_SPOUSE.IsDate = false;
            this.txt_PR_SPOUSE.Location = new System.Drawing.Point(515, 210);
            this.txt_PR_SPOUSE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_SPOUSE.MaxLength = 45;
            this.txt_PR_SPOUSE.Name = "txt_PR_SPOUSE";
            this.txt_PR_SPOUSE.NumberFormat = "###,###,##0.00";
            this.txt_PR_SPOUSE.Postfix = "";
            this.txt_PR_SPOUSE.Prefix = "";
            this.txt_PR_SPOUSE.Size = new System.Drawing.Size(255, 24);
            this.txt_PR_SPOUSE.SkipValidation = false;
            this.txt_PR_SPOUSE.TabIndex = 17;
            this.txt_PR_SPOUSE.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlPersonnelMain
            // 
            this.pnlPersonnelMain.ConcurrentPanels = null;
            this.pnlPersonnelMain.Controls.Add(this.slTxtBxUserID);
            this.pnlPersonnelMain.Controls.Add(this.slTxtbEmail);
            this.pnlPersonnelMain.Controls.Add(this.lbEmail);
            this.pnlPersonnelMain.Controls.Add(this.txtLoc);
            this.pnlPersonnelMain.Controls.Add(this.label29);
            this.pnlPersonnelMain.Controls.Add(this.pnlView);
            this.pnlPersonnelMain.Controls.Add(this.pnlDelete);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_EXPIRY);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_ID_ISSUE);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_NEW_ANNUAL_PACK);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CONFIRM);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_JOINING_DATE);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CONF_FLAG);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_NEW_BRANCH);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_CLOSE_FLAG);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_TAX_PAID);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_TAX_INC);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_BANK_ID);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_ACCOUNT_NO);
            this.pnlPersonnelMain.Controls.Add(this.label7);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_NATIONAL_TAX);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_ANNUAL_PACK);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_EMP_TYPE);
            this.pnlPersonnelMain.Controls.Add(this.txt_F_GEID_NO);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_RELIGION);
            this.pnlPersonnelMain.Controls.Add(this.txt_W_RELIGION);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_FUNC_Title2);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_FUNC_Title1);
            this.pnlPersonnelMain.Controls.Add(this.label18);
            this.pnlPersonnelMain.Controls.Add(this.label17);
            this.pnlPersonnelMain.Controls.Add(this.label16);
            this.pnlPersonnelMain.Controls.Add(this.label15);
            this.pnlPersonnelMain.Controls.Add(this.label14);
            this.pnlPersonnelMain.Controls.Add(this.label13);
            this.pnlPersonnelMain.Controls.Add(this.label12);
            this.pnlPersonnelMain.Controls.Add(this.label11);
            this.pnlPersonnelMain.Controls.Add(this.label10);
            this.pnlPersonnelMain.Controls.Add(this.label9);
            this.pnlPersonnelMain.Controls.Add(this.label8);
            this.pnlPersonnelMain.Controls.Add(this.label2);
            this.pnlPersonnelMain.Controls.Add(this.lblGeid);
            this.pnlPersonnelMain.Controls.Add(this.lblFunctionalTitle);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_LEVEL);
            this.pnlPersonnelMain.Controls.Add(this.txt_F_RNAME);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_LAST_NAME);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_BRANCH);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_DESIG);
            this.pnlPersonnelMain.Controls.Add(this.txtReportTo);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_FIRST_NAME);
            this.pnlPersonnelMain.Controls.Add(this.txt_PR_P_NO);
            this.pnlPersonnelMain.Controls.Add(this.lblLevel);
            this.pnlPersonnelMain.Controls.Add(this.lblName);
            this.pnlPersonnelMain.Controls.Add(this.lblLastName);
            this.pnlPersonnelMain.Controls.Add(this.lblBranch);
            this.pnlPersonnelMain.Controls.Add(this.label1);
            this.pnlPersonnelMain.Controls.Add(this.lblReportTo);
            this.pnlPersonnelMain.Controls.Add(this.lblFirstName);
            this.pnlPersonnelMain.Controls.Add(this.lblPersonnelsNo);
            this.pnlPersonnelMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPersonnelMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonnelMain.DependentPanels = null;
            this.pnlPersonnelMain.DisableDependentLoad = false;
            this.pnlPersonnelMain.EnableDelete = true;
            this.pnlPersonnelMain.EnableInsert = true;
            this.pnlPersonnelMain.EnableQuery = false;
            this.pnlPersonnelMain.EnableUpdate = true;
            this.pnlPersonnelMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntCommand";
            this.pnlPersonnelMain.Location = new System.Drawing.Point(13, 86);
            this.pnlPersonnelMain.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPersonnelMain.MasterPanel = null;
            this.pnlPersonnelMain.Name = "pnlPersonnelMain";
            this.pnlPersonnelMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonnelMain.Size = new System.Drawing.Size(872, 517);
            this.pnlPersonnelMain.SPName = "";
            this.pnlPersonnelMain.TabIndex = 6;
            // 
            // slTxtBxUserID
            // 
            this.slTxtBxUserID.AllowSpace = true;
            this.slTxtBxUserID.AssociatedLookUpName = "txt_W_USER";
            this.slTxtBxUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBxUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBxUserID.ContinuationTextBox = null;
            this.slTxtBxUserID.CustomEnabled = true;
            this.slTxtBxUserID.DataFieldMapping = "PR_UserID";
            this.slTxtBxUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBxUserID.GetRecordsOnUpDownKeys = false;
            this.slTxtBxUserID.IsDate = false;
            this.slTxtBxUserID.Location = new System.Drawing.Point(799, 481);
            this.slTxtBxUserID.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtBxUserID.MaxLength = 20;
            this.slTxtBxUserID.Name = "slTxtBxUserID";
            this.slTxtBxUserID.NumberFormat = "###,###,##0.00";
            this.slTxtBxUserID.Postfix = "";
            this.slTxtBxUserID.Prefix = "";
            this.slTxtBxUserID.Size = new System.Drawing.Size(45, 24);
            this.slTxtBxUserID.SkipValidation = false;
            this.slTxtBxUserID.TabIndex = 93;
            this.slTxtBxUserID.TextType = CrplControlLibrary.TextType.String;
            this.slTxtBxUserID.Visible = false;
            // 
            // slTxtbEmail
            // 
            this.slTxtbEmail.AllowSpace = true;
            this.slTxtbEmail.AssociatedLookUpName = "";
            this.slTxtbEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtbEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtbEmail.ContinuationTextBox = null;
            this.slTxtbEmail.CustomEnabled = true;
            this.slTxtbEmail.DataFieldMapping = "PR_EMAIL";
            this.slTxtbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtbEmail.GetRecordsOnUpDownKeys = false;
            this.slTxtbEmail.IsDate = false;
            this.slTxtbEmail.Location = new System.Drawing.Point(184, 491);
            this.slTxtbEmail.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtbEmail.MaxLength = 255;
            this.slTxtbEmail.Name = "slTxtbEmail";
            this.slTxtbEmail.NumberFormat = "###,###,##0.00";
            this.slTxtbEmail.Postfix = "";
            this.slTxtbEmail.Prefix = "";
            this.slTxtbEmail.Size = new System.Drawing.Size(179, 24);
            this.slTxtbEmail.SkipValidation = false;
            this.slTxtbEmail.TabIndex = 53;
            this.slTxtbEmail.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbEmail
            // 
            this.lbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEmail.Location = new System.Drawing.Point(47, 496);
            this.lbEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(72, 16);
            this.lbEmail.TabIndex = 52;
            this.lbEmail.Text = "Email :";
            this.lbEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLoc
            // 
            this.txtLoc.AllowSpace = true;
            this.txtLoc.AssociatedLookUpName = "";
            this.txtLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLoc.ContinuationTextBox = null;
            this.txtLoc.CustomEnabled = true;
            this.txtLoc.DataFieldMapping = "LOCATION";
            this.txtLoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoc.GetRecordsOnUpDownKeys = false;
            this.txtLoc.IsDate = false;
            this.txtLoc.Location = new System.Drawing.Point(184, 49);
            this.txtLoc.Margin = new System.Windows.Forms.Padding(4);
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.NumberFormat = "###,###,##0.00";
            this.txtLoc.Postfix = "";
            this.txtLoc.Prefix = "";
            this.txtLoc.Size = new System.Drawing.Size(433, 24);
            this.txtLoc.SkipValidation = false;
            this.txtLoc.TabIndex = 8;
            this.txtLoc.TabStop = false;
            this.txtLoc.TextType = CrplControlLibrary.TextType.String;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(17, 54);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(159, 16);
            this.label29.TabIndex = 7;
            this.label29.Text = "Location :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlView
            // 
            this.pnlView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlView.ConcurrentPanels = null;
            this.pnlView.Controls.Add(this.label20);
            this.pnlView.Controls.Add(this.textBox1);
            this.pnlView.Controls.Add(this.label21);
            this.pnlView.Controls.Add(this.pnlSave);
            this.pnlView.Controls.Add(this.txt_W_VIEW_ANS);
            this.pnlView.Controls.Add(this.label28);
            this.pnlView.DataManager = null;
            this.pnlView.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlView.DependentPanels = null;
            this.pnlView.DisableDependentLoad = false;
            this.pnlView.EnableDelete = true;
            this.pnlView.EnableInsert = true;
            this.pnlView.EnableQuery = false;
            this.pnlView.EnableUpdate = true;
            this.pnlView.EntityName = null;
            this.pnlView.Location = new System.Drawing.Point(724, 460);
            this.pnlView.Margin = new System.Windows.Forms.Padding(4);
            this.pnlView.MasterPanel = null;
            this.pnlView.Name = "pnlView";
            this.pnlView.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlView.Size = new System.Drawing.Size(42, 25);
            this.pnlView.SPName = null;
            this.pnlView.TabIndex = 62;
            this.pnlView.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(73, 12);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 17);
            this.label20.TabIndex = 0;
            this.label20.Text = "[N]ext P. No.";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(605, 6);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(0, 22);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(227, 12);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 17);
            this.label21.TabIndex = 1;
            this.label21.Text = "Next [P]age";
            // 
            // pnlSave
            // 
            this.pnlSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSave.ConcurrentPanels = null;
            this.pnlSave.Controls.Add(this.txt_W_ADD_ANS);
            this.pnlSave.Controls.Add(this.label19);
            this.pnlSave.DataManager = null;
            this.pnlSave.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlSave.DependentPanels = null;
            this.pnlSave.DisableDependentLoad = false;
            this.pnlSave.EnableDelete = true;
            this.pnlSave.EnableInsert = true;
            this.pnlSave.EnableQuery = false;
            this.pnlSave.EnableUpdate = true;
            this.pnlSave.EntityName = null;
            this.pnlSave.Location = new System.Drawing.Point(-1, 38);
            this.pnlSave.Margin = new System.Windows.Forms.Padding(4);
            this.pnlSave.MasterPanel = null;
            this.pnlSave.Name = "pnlSave";
            this.pnlSave.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlSave.Size = new System.Drawing.Size(693, 35);
            this.pnlSave.SPName = null;
            this.pnlSave.TabIndex = 1;
            this.pnlSave.Visible = false;
            // 
            // txt_W_ADD_ANS
            // 
            this.txt_W_ADD_ANS.AllowSpace = true;
            this.txt_W_ADD_ANS.AssociatedLookUpName = "";
            this.txt_W_ADD_ANS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_ADD_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_ADD_ANS.ContinuationTextBox = null;
            this.txt_W_ADD_ANS.CustomEnabled = true;
            this.txt_W_ADD_ANS.DataFieldMapping = "";
            this.txt_W_ADD_ANS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_ADD_ANS.GetRecordsOnUpDownKeys = false;
            this.txt_W_ADD_ANS.IsDate = false;
            this.txt_W_ADD_ANS.Location = new System.Drawing.Point(519, 4);
            this.txt_W_ADD_ANS.Margin = new System.Windows.Forms.Padding(4);
            this.txt_W_ADD_ANS.MaxLength = 1;
            this.txt_W_ADD_ANS.Name = "txt_W_ADD_ANS";
            this.txt_W_ADD_ANS.NumberFormat = "###,###,##0.00";
            this.txt_W_ADD_ANS.Postfix = "";
            this.txt_W_ADD_ANS.Prefix = "";
            this.txt_W_ADD_ANS.Size = new System.Drawing.Size(39, 24);
            this.txt_W_ADD_ANS.SkipValidation = false;
            this.txt_W_ADD_ANS.TabIndex = 3;
            this.txt_W_ADD_ANS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(83, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(378, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "Do You Want To Save The Above Information [Y/N]";
            // 
            // txt_W_VIEW_ANS
            // 
            this.txt_W_VIEW_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_VIEW_ANS.Location = new System.Drawing.Point(521, 6);
            this.txt_W_VIEW_ANS.Margin = new System.Windows.Forms.Padding(4);
            this.txt_W_VIEW_ANS.Name = "txt_W_VIEW_ANS";
            this.txt_W_VIEW_ANS.Size = new System.Drawing.Size(41, 22);
            this.txt_W_VIEW_ANS.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(377, 12);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(74, 17);
            this.label28.TabIndex = 2;
            this.label28.Text = "[O]ptions";
            // 
            // pnlDelete
            // 
            this.pnlDelete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDelete.ConcurrentPanels = null;
            this.pnlDelete.Controls.Add(this.txt_W_DEL_ANS);
            this.pnlDelete.Controls.Add(this.label22);
            this.pnlDelete.DataManager = null;
            this.pnlDelete.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDelete.DependentPanels = null;
            this.pnlDelete.DisableDependentLoad = false;
            this.pnlDelete.EnableDelete = true;
            this.pnlDelete.EnableInsert = true;
            this.pnlDelete.EnableQuery = false;
            this.pnlDelete.EnableUpdate = true;
            this.pnlDelete.EntityName = null;
            this.pnlDelete.Location = new System.Drawing.Point(739, 463);
            this.pnlDelete.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDelete.MasterPanel = null;
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDelete.Size = new System.Drawing.Size(27, 20);
            this.pnlDelete.SPName = null;
            this.pnlDelete.TabIndex = 63;
            this.pnlDelete.Visible = false;
            // 
            // txt_W_DEL_ANS
            // 
            this.txt_W_DEL_ANS.AllowSpace = true;
            this.txt_W_DEL_ANS.AssociatedLookUpName = "";
            this.txt_W_DEL_ANS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_DEL_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_DEL_ANS.ContinuationTextBox = null;
            this.txt_W_DEL_ANS.CustomEnabled = true;
            this.txt_W_DEL_ANS.DataFieldMapping = "";
            this.txt_W_DEL_ANS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_DEL_ANS.GetRecordsOnUpDownKeys = false;
            this.txt_W_DEL_ANS.IsDate = false;
            this.txt_W_DEL_ANS.Location = new System.Drawing.Point(524, 4);
            this.txt_W_DEL_ANS.Margin = new System.Windows.Forms.Padding(4);
            this.txt_W_DEL_ANS.MaxLength = 1;
            this.txt_W_DEL_ANS.Name = "txt_W_DEL_ANS";
            this.txt_W_DEL_ANS.NumberFormat = "###,###,##0.00";
            this.txt_W_DEL_ANS.Postfix = "";
            this.txt_W_DEL_ANS.Prefix = "";
            this.txt_W_DEL_ANS.Size = new System.Drawing.Size(39, 24);
            this.txt_W_DEL_ANS.SkipValidation = false;
            this.txt_W_DEL_ANS.TabIndex = 4;
            this.txt_W_DEL_ANS.TabStop = false;
            this.txt_W_DEL_ANS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(69, 6);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(394, 17);
            this.label22.TabIndex = 0;
            this.label22.Text = "Do You Want To Delete The Above Information [Y/N] ";
            // 
            // txt_PR_EXPIRY
            // 
            this.txt_PR_EXPIRY.CustomEnabled = true;
            this.txt_PR_EXPIRY.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_EXPIRY.DataFieldMapping = "PR_EXPIRY";
            this.txt_PR_EXPIRY.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_EXPIRY.HasChanges = true;
            this.txt_PR_EXPIRY.Location = new System.Drawing.Point(229, 462);
            this.txt_PR_EXPIRY.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_EXPIRY.Name = "txt_PR_EXPIRY";
            this.txt_PR_EXPIRY.NullValue = " ";
            this.txt_PR_EXPIRY.Size = new System.Drawing.Size(123, 22);
            this.txt_PR_EXPIRY.TabIndex = 51;
            this.txt_PR_EXPIRY.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            // 
            // txt_PR_ID_ISSUE
            // 
            this.txt_PR_ID_ISSUE.CustomEnabled = true;
            this.txt_PR_ID_ISSUE.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_ID_ISSUE.DataFieldMapping = "PR_ID_ISSUE";
            this.txt_PR_ID_ISSUE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_ID_ISSUE.HasChanges = true;
            this.txt_PR_ID_ISSUE.Location = new System.Drawing.Point(229, 433);
            this.txt_PR_ID_ISSUE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_ID_ISSUE.Name = "txt_PR_ID_ISSUE";
            this.txt_PR_ID_ISSUE.NullValue = " ";
            this.txt_PR_ID_ISSUE.Size = new System.Drawing.Size(123, 22);
            this.txt_PR_ID_ISSUE.TabIndex = 49;
            this.txt_PR_ID_ISSUE.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            // 
            // txt_PR_NEW_ANNUAL_PACK
            // 
            this.txt_PR_NEW_ANNUAL_PACK.AllowSpace = true;
            this.txt_PR_NEW_ANNUAL_PACK.AssociatedLookUpName = "";
            this.txt_PR_NEW_ANNUAL_PACK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NEW_ANNUAL_PACK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NEW_ANNUAL_PACK.ContinuationTextBox = null;
            this.txt_PR_NEW_ANNUAL_PACK.CustomEnabled = true;
            this.txt_PR_NEW_ANNUAL_PACK.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txt_PR_NEW_ANNUAL_PACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NEW_ANNUAL_PACK.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NEW_ANNUAL_PACK.IsDate = false;
            this.txt_PR_NEW_ANNUAL_PACK.Location = new System.Drawing.Point(436, 384);
            this.txt_PR_NEW_ANNUAL_PACK.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_NEW_ANNUAL_PACK.Name = "txt_PR_NEW_ANNUAL_PACK";
            this.txt_PR_NEW_ANNUAL_PACK.NumberFormat = "###,###,##0.00";
            this.txt_PR_NEW_ANNUAL_PACK.Postfix = "";
            this.txt_PR_NEW_ANNUAL_PACK.Prefix = "";
            this.txt_PR_NEW_ANNUAL_PACK.Size = new System.Drawing.Size(201, 24);
            this.txt_PR_NEW_ANNUAL_PACK.SkipValidation = false;
            this.txt_PR_NEW_ANNUAL_PACK.TabIndex = 45;
            this.txt_PR_NEW_ANNUAL_PACK.TabStop = false;
            this.txt_PR_NEW_ANNUAL_PACK.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_NEW_ANNUAL_PACK.Visible = false;
            // 
            // txt_PR_CONFIRM
            // 
            this.txt_PR_CONFIRM.CustomEnabled = true;
            this.txt_PR_CONFIRM.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_CONFIRM.DataFieldMapping = "PR_CONFIRM";
            this.txt_PR_CONFIRM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_CONFIRM.HasChanges = true;
            this.txt_PR_CONFIRM.IsRequired = true;
            this.txt_PR_CONFIRM.Location = new System.Drawing.Point(580, 298);
            this.txt_PR_CONFIRM.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_CONFIRM.Name = "txt_PR_CONFIRM";
            this.txt_PR_CONFIRM.NullValue = " ";
            this.txt_PR_CONFIRM.Size = new System.Drawing.Size(123, 22);
            this.txt_PR_CONFIRM.TabIndex = 38;
            this.txt_PR_CONFIRM.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            // 
            // txt_PR_JOINING_DATE
            // 
            this.txt_PR_JOINING_DATE.CustomEnabled = true;
            this.txt_PR_JOINING_DATE.CustomFormat = "dd/MM/yyyy";
            this.txt_PR_JOINING_DATE.DataFieldMapping = "PR_JOINING_DATE";
            this.txt_PR_JOINING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_PR_JOINING_DATE.HasChanges = true;
            this.txt_PR_JOINING_DATE.IsRequired = true;
            this.txt_PR_JOINING_DATE.Location = new System.Drawing.Point(184, 298);
            this.txt_PR_JOINING_DATE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_JOINING_DATE.Name = "txt_PR_JOINING_DATE";
            this.txt_PR_JOINING_DATE.NullValue = " ";
            this.txt_PR_JOINING_DATE.Size = new System.Drawing.Size(123, 22);
            this.txt_PR_JOINING_DATE.TabIndex = 36;
            this.txt_PR_JOINING_DATE.Value = new System.DateTime(2011, 1, 18, 0, 0, 0, 0);
            // 
            // txt_PR_CONF_FLAG
            // 
            this.txt_PR_CONF_FLAG.AllowSpace = true;
            this.txt_PR_CONF_FLAG.AssociatedLookUpName = "";
            this.txt_PR_CONF_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CONF_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CONF_FLAG.ContinuationTextBox = null;
            this.txt_PR_CONF_FLAG.CustomEnabled = true;
            this.txt_PR_CONF_FLAG.DataFieldMapping = "PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CONF_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CONF_FLAG.IsDate = false;
            this.txt_PR_CONF_FLAG.Location = new System.Drawing.Point(803, 351);
            this.txt_PR_CONF_FLAG.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_CONF_FLAG.Name = "txt_PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PR_CONF_FLAG.Postfix = "";
            this.txt_PR_CONF_FLAG.Prefix = "";
            this.txt_PR_CONF_FLAG.Size = new System.Drawing.Size(42, 24);
            this.txt_PR_CONF_FLAG.SkipValidation = false;
            this.txt_PR_CONF_FLAG.TabIndex = 59;
            this.txt_PR_CONF_FLAG.TabStop = false;
            this.txt_PR_CONF_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_CONF_FLAG.Visible = false;
            // 
            // txt_PR_NEW_BRANCH
            // 
            this.txt_PR_NEW_BRANCH.AllowSpace = true;
            this.txt_PR_NEW_BRANCH.AssociatedLookUpName = "";
            this.txt_PR_NEW_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NEW_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NEW_BRANCH.ContinuationTextBox = null;
            this.txt_PR_NEW_BRANCH.CustomEnabled = true;
            this.txt_PR_NEW_BRANCH.DataFieldMapping = "PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NEW_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NEW_BRANCH.IsDate = false;
            this.txt_PR_NEW_BRANCH.Location = new System.Drawing.Point(803, 411);
            this.txt_PR_NEW_BRANCH.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_NEW_BRANCH.Name = "txt_PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PR_NEW_BRANCH.Postfix = "";
            this.txt_PR_NEW_BRANCH.Prefix = "";
            this.txt_PR_NEW_BRANCH.Size = new System.Drawing.Size(42, 24);
            this.txt_PR_NEW_BRANCH.SkipValidation = false;
            this.txt_PR_NEW_BRANCH.TabIndex = 61;
            this.txt_PR_NEW_BRANCH.TabStop = false;
            this.txt_PR_NEW_BRANCH.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_NEW_BRANCH.Visible = false;
            // 
            // txt_PR_CLOSE_FLAG
            // 
            this.txt_PR_CLOSE_FLAG.AllowSpace = true;
            this.txt_PR_CLOSE_FLAG.AssociatedLookUpName = "";
            this.txt_PR_CLOSE_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CLOSE_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CLOSE_FLAG.ContinuationTextBox = null;
            this.txt_PR_CLOSE_FLAG.CustomEnabled = true;
            this.txt_PR_CLOSE_FLAG.DataFieldMapping = "PR_CLOSE_FLAG";
            this.txt_PR_CLOSE_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CLOSE_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CLOSE_FLAG.IsDate = false;
            this.txt_PR_CLOSE_FLAG.Location = new System.Drawing.Point(803, 383);
            this.txt_PR_CLOSE_FLAG.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_CLOSE_FLAG.Name = "txt_PR_CLOSE_FLAG";
            this.txt_PR_CLOSE_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PR_CLOSE_FLAG.Postfix = "";
            this.txt_PR_CLOSE_FLAG.Prefix = "";
            this.txt_PR_CLOSE_FLAG.Size = new System.Drawing.Size(42, 24);
            this.txt_PR_CLOSE_FLAG.SkipValidation = false;
            this.txt_PR_CLOSE_FLAG.TabIndex = 60;
            this.txt_PR_CLOSE_FLAG.TabStop = false;
            this.txt_PR_CLOSE_FLAG.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_CLOSE_FLAG.Visible = false;
            // 
            // txt_PR_TAX_PAID
            // 
            this.txt_PR_TAX_PAID.AllowSpace = true;
            this.txt_PR_TAX_PAID.AssociatedLookUpName = "";
            this.txt_PR_TAX_PAID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_TAX_PAID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_TAX_PAID.ContinuationTextBox = null;
            this.txt_PR_TAX_PAID.CustomEnabled = true;
            this.txt_PR_TAX_PAID.DataFieldMapping = "PR_TAX_PAID";
            this.txt_PR_TAX_PAID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_TAX_PAID.GetRecordsOnUpDownKeys = false;
            this.txt_PR_TAX_PAID.IsDate = false;
            this.txt_PR_TAX_PAID.Location = new System.Drawing.Point(563, 462);
            this.txt_PR_TAX_PAID.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_TAX_PAID.MaxLength = 9;
            this.txt_PR_TAX_PAID.Name = "txt_PR_TAX_PAID";
            this.txt_PR_TAX_PAID.NumberFormat = "###,###,##0.00";
            this.txt_PR_TAX_PAID.Postfix = "";
            this.txt_PR_TAX_PAID.Prefix = "";
            this.txt_PR_TAX_PAID.Size = new System.Drawing.Size(138, 24);
            this.txt_PR_TAX_PAID.SkipValidation = false;
            this.txt_PR_TAX_PAID.TabIndex = 58;
            this.txt_PR_TAX_PAID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_TAX_PAID.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_TAX_INC
            // 
            this.txt_PR_TAX_INC.AllowSpace = true;
            this.txt_PR_TAX_INC.AssociatedLookUpName = "";
            this.txt_PR_TAX_INC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_TAX_INC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_TAX_INC.ContinuationTextBox = null;
            this.txt_PR_TAX_INC.CustomEnabled = true;
            this.txt_PR_TAX_INC.DataFieldMapping = "PR_TAX_INC";
            this.txt_PR_TAX_INC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_TAX_INC.GetRecordsOnUpDownKeys = false;
            this.txt_PR_TAX_INC.IsDate = false;
            this.txt_PR_TAX_INC.Location = new System.Drawing.Point(563, 433);
            this.txt_PR_TAX_INC.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_TAX_INC.MaxLength = 9;
            this.txt_PR_TAX_INC.Name = "txt_PR_TAX_INC";
            this.txt_PR_TAX_INC.NumberFormat = "###,###,##0.00";
            this.txt_PR_TAX_INC.Postfix = "";
            this.txt_PR_TAX_INC.Prefix = "";
            this.txt_PR_TAX_INC.Size = new System.Drawing.Size(138, 24);
            this.txt_PR_TAX_INC.SkipValidation = false;
            this.txt_PR_TAX_INC.TabIndex = 56;
            this.txt_PR_TAX_INC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_TAX_INC.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_BANK_ID
            // 
            this.txt_PR_BANK_ID.AllowSpace = true;
            this.txt_PR_BANK_ID.AssociatedLookUpName = "";
            this.txt_PR_BANK_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_BANK_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_BANK_ID.ContinuationTextBox = null;
            this.txt_PR_BANK_ID.CustomEnabled = true;
            this.txt_PR_BANK_ID.DataFieldMapping = "PR_BANK_ID";
            this.txt_PR_BANK_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_BANK_ID.GetRecordsOnUpDownKeys = false;
            this.txt_PR_BANK_ID.IsDate = false;
            this.txt_PR_BANK_ID.Location = new System.Drawing.Point(235, 404);
            this.txt_PR_BANK_ID.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_BANK_ID.MaxLength = 1;
            this.txt_PR_BANK_ID.Name = "txt_PR_BANK_ID";
            this.txt_PR_BANK_ID.NumberFormat = "###,###,##0.00";
            this.txt_PR_BANK_ID.Postfix = "";
            this.txt_PR_BANK_ID.Prefix = "";
            this.txt_PR_BANK_ID.Size = new System.Drawing.Size(42, 24);
            this.txt_PR_BANK_ID.SkipValidation = false;
            this.txt_PR_BANK_ID.TabIndex = 47;
            this.txt_PR_BANK_ID.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_ACCOUNT_NO
            // 
            this.txt_PR_ACCOUNT_NO.AllowSpace = true;
            this.txt_PR_ACCOUNT_NO.AssociatedLookUpName = "";
            this.txt_PR_ACCOUNT_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ACCOUNT_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ACCOUNT_NO.ContinuationTextBox = null;
            this.txt_PR_ACCOUNT_NO.CustomEnabled = true;
            this.txt_PR_ACCOUNT_NO.DataFieldMapping = "PR_ACCOUNT_NO";
            this.txt_PR_ACCOUNT_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ACCOUNT_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ACCOUNT_NO.IsDate = false;
            this.txt_PR_ACCOUNT_NO.Location = new System.Drawing.Point(184, 359);
            this.txt_PR_ACCOUNT_NO.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_ACCOUNT_NO.MaxLength = 24;
            this.txt_PR_ACCOUNT_NO.Name = "txt_PR_ACCOUNT_NO";
            this.txt_PR_ACCOUNT_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_ACCOUNT_NO.Postfix = "";
            this.txt_PR_ACCOUNT_NO.Prefix = "";
            this.txt_PR_ACCOUNT_NO.Size = new System.Drawing.Size(179, 24);
            this.txt_PR_ACCOUNT_NO.SkipValidation = false;
            this.txt_PR_ACCOUNT_NO.TabIndex = 44;
            this.txt_PR_ACCOUNT_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(69, 364);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 16);
            this.label7.TabIndex = 42;
            this.label7.Text = "Account No :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_PR_NATIONAL_TAX
            // 
            this.txt_PR_NATIONAL_TAX.AllowSpace = true;
            this.txt_PR_NATIONAL_TAX.AssociatedLookUpName = "";
            this.txt_PR_NATIONAL_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NATIONAL_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NATIONAL_TAX.ContinuationTextBox = null;
            this.txt_PR_NATIONAL_TAX.CustomEnabled = true;
            this.txt_PR_NATIONAL_TAX.DataFieldMapping = "PR_NATIONAL_TAX";
            this.txt_PR_NATIONAL_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NATIONAL_TAX.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NATIONAL_TAX.IsDate = false;
            this.txt_PR_NATIONAL_TAX.Location = new System.Drawing.Point(579, 329);
            this.txt_PR_NATIONAL_TAX.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_NATIONAL_TAX.MaxLength = 18;
            this.txt_PR_NATIONAL_TAX.Name = "txt_PR_NATIONAL_TAX";
            this.txt_PR_NATIONAL_TAX.NumberFormat = "###,###,##0.00";
            this.txt_PR_NATIONAL_TAX.Postfix = "";
            this.txt_PR_NATIONAL_TAX.Prefix = "";
            this.txt_PR_NATIONAL_TAX.Size = new System.Drawing.Size(187, 24);
            this.txt_PR_NATIONAL_TAX.SkipValidation = false;
            this.txt_PR_NATIONAL_TAX.TabIndex = 42;
            this.txt_PR_NATIONAL_TAX.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_ANNUAL_PACK
            // 
            this.txt_PR_ANNUAL_PACK.AllowSpace = true;
            this.txt_PR_ANNUAL_PACK.AssociatedLookUpName = "";
            this.txt_PR_ANNUAL_PACK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ANNUAL_PACK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ANNUAL_PACK.ContinuationTextBox = null;
            this.txt_PR_ANNUAL_PACK.CustomEnabled = true;
            this.txt_PR_ANNUAL_PACK.DataFieldMapping = "PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ANNUAL_PACK.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ANNUAL_PACK.IsDate = false;
            this.txt_PR_ANNUAL_PACK.IsRequired = true;
            this.txt_PR_ANNUAL_PACK.Location = new System.Drawing.Point(184, 329);
            this.txt_PR_ANNUAL_PACK.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_ANNUAL_PACK.MaxLength = 11;
            this.txt_PR_ANNUAL_PACK.Name = "txt_PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.NumberFormat = "###,###,##0";
            this.txt_PR_ANNUAL_PACK.Postfix = "";
            this.txt_PR_ANNUAL_PACK.Prefix = "";
            this.txt_PR_ANNUAL_PACK.Size = new System.Drawing.Size(123, 24);
            this.txt_PR_ANNUAL_PACK.SkipValidation = false;
            this.txt_PR_ANNUAL_PACK.TabIndex = 40;
            this.txt_PR_ANNUAL_PACK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_ANNUAL_PACK.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_EMP_TYPE
            // 
            this.txt_PR_EMP_TYPE.AllowSpace = true;
            this.txt_PR_EMP_TYPE.AssociatedLookUpName = "";
            this.txt_PR_EMP_TYPE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_EMP_TYPE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_EMP_TYPE.ContinuationTextBox = null;
            this.txt_PR_EMP_TYPE.CustomEnabled = true;
            this.txt_PR_EMP_TYPE.DataFieldMapping = "PR_EMP_TYPE";
            this.txt_PR_EMP_TYPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_EMP_TYPE.GetRecordsOnUpDownKeys = false;
            this.txt_PR_EMP_TYPE.IsDate = false;
            this.txt_PR_EMP_TYPE.Location = new System.Drawing.Point(580, 234);
            this.txt_PR_EMP_TYPE.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_EMP_TYPE.MaxLength = 2;
            this.txt_PR_EMP_TYPE.Name = "txt_PR_EMP_TYPE";
            this.txt_PR_EMP_TYPE.NumberFormat = "###,###,##0.00";
            this.txt_PR_EMP_TYPE.Postfix = "";
            this.txt_PR_EMP_TYPE.Prefix = "";
            this.txt_PR_EMP_TYPE.Size = new System.Drawing.Size(87, 24);
            this.txt_PR_EMP_TYPE.SkipValidation = false;
            this.txt_PR_EMP_TYPE.TabIndex = 31;
            this.txt_PR_EMP_TYPE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_F_GEID_NO
            // 
            this.txt_F_GEID_NO.AllowSpace = true;
            this.txt_F_GEID_NO.AssociatedLookUpName = "";
            this.txt_F_GEID_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_F_GEID_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_F_GEID_NO.ContinuationTextBox = null;
            this.txt_F_GEID_NO.CustomEnabled = true;
            this.txt_F_GEID_NO.DataFieldMapping = "F_GEID_NO";
            this.txt_F_GEID_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_F_GEID_NO.GetRecordsOnUpDownKeys = false;
            this.txt_F_GEID_NO.IsDate = false;
            this.txt_F_GEID_NO.Location = new System.Drawing.Point(580, 202);
            this.txt_F_GEID_NO.Margin = new System.Windows.Forms.Padding(4);
            this.txt_F_GEID_NO.Name = "txt_F_GEID_NO";
            this.txt_F_GEID_NO.NumberFormat = "###,###,##0.00";
            this.txt_F_GEID_NO.Postfix = "";
            this.txt_F_GEID_NO.Prefix = "";
            this.txt_F_GEID_NO.Size = new System.Drawing.Size(121, 24);
            this.txt_F_GEID_NO.SkipValidation = false;
            this.txt_F_GEID_NO.TabIndex = 28;
            this.txt_F_GEID_NO.TabStop = false;
            this.txt_F_GEID_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_RELIGION
            // 
            this.txt_PR_RELIGION.AllowSpace = true;
            this.txt_PR_RELIGION.AssociatedLookUpName = "";
            this.txt_PR_RELIGION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_RELIGION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_RELIGION.ContinuationTextBox = null;
            this.txt_PR_RELIGION.CustomEnabled = false;
            this.txt_PR_RELIGION.DataFieldMapping = "PR_RELIGION";
            this.txt_PR_RELIGION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_RELIGION.GetRecordsOnUpDownKeys = false;
            this.txt_PR_RELIGION.IsDate = false;
            this.txt_PR_RELIGION.Location = new System.Drawing.Point(229, 266);
            this.txt_PR_RELIGION.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_RELIGION.Name = "txt_PR_RELIGION";
            this.txt_PR_RELIGION.NumberFormat = "###,###,##0.00";
            this.txt_PR_RELIGION.Postfix = "";
            this.txt_PR_RELIGION.Prefix = "";
            this.txt_PR_RELIGION.ReadOnly = true;
            this.txt_PR_RELIGION.Size = new System.Drawing.Size(134, 24);
            this.txt_PR_RELIGION.SkipValidation = false;
            this.txt_PR_RELIGION.TabIndex = 34;
            this.txt_PR_RELIGION.TabStop = false;
            this.txt_PR_RELIGION.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_RELIGION.Visible = false;
            // 
            // txt_W_RELIGION
            // 
            this.txt_W_RELIGION.AllowSpace = true;
            this.txt_W_RELIGION.AssociatedLookUpName = "";
            this.txt_W_RELIGION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_RELIGION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_RELIGION.ContinuationTextBox = null;
            this.txt_W_RELIGION.CustomEnabled = true;
            this.txt_W_RELIGION.DataFieldMapping = "W_RELIGION";
            this.txt_W_RELIGION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_RELIGION.GetRecordsOnUpDownKeys = false;
            this.txt_W_RELIGION.IsDate = false;
            this.txt_W_RELIGION.Location = new System.Drawing.Point(184, 266);
            this.txt_W_RELIGION.Margin = new System.Windows.Forms.Padding(4);
            this.txt_W_RELIGION.MaxLength = 1;
            this.txt_W_RELIGION.Name = "txt_W_RELIGION";
            this.txt_W_RELIGION.NumberFormat = "###,###,##0.00";
            this.txt_W_RELIGION.Postfix = "";
            this.txt_W_RELIGION.Prefix = "";
            this.txt_W_RELIGION.Size = new System.Drawing.Size(37, 24);
            this.txt_W_RELIGION.SkipValidation = false;
            this.txt_W_RELIGION.TabIndex = 33;
            this.txt_W_RELIGION.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_RELIGION.Visible = false;
            // 
            // txt_PR_FUNC_Title2
            // 
            this.txt_PR_FUNC_Title2.AllowSpace = true;
            this.txt_PR_FUNC_Title2.AssociatedLookUpName = "";
            this.txt_PR_FUNC_Title2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FUNC_Title2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FUNC_Title2.ContinuationTextBox = null;
            this.txt_PR_FUNC_Title2.CustomEnabled = true;
            this.txt_PR_FUNC_Title2.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txt_PR_FUNC_Title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FUNC_Title2.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FUNC_Title2.IsDate = false;
            this.txt_PR_FUNC_Title2.Location = new System.Drawing.Point(184, 234);
            this.txt_PR_FUNC_Title2.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_FUNC_Title2.MaxLength = 30;
            this.txt_PR_FUNC_Title2.Name = "txt_PR_FUNC_Title2";
            this.txt_PR_FUNC_Title2.NumberFormat = "###,###,##0.00";
            this.txt_PR_FUNC_Title2.Postfix = "";
            this.txt_PR_FUNC_Title2.Prefix = "";
            this.txt_PR_FUNC_Title2.Size = new System.Drawing.Size(294, 24);
            this.txt_PR_FUNC_Title2.SkipValidation = false;
            this.txt_PR_FUNC_Title2.TabIndex = 29;
            this.txt_PR_FUNC_Title2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_FUNC_Title1
            // 
            this.txt_PR_FUNC_Title1.AllowSpace = true;
            this.txt_PR_FUNC_Title1.AssociatedLookUpName = "";
            this.txt_PR_FUNC_Title1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FUNC_Title1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FUNC_Title1.ContinuationTextBox = null;
            this.txt_PR_FUNC_Title1.CustomEnabled = true;
            this.txt_PR_FUNC_Title1.DataFieldMapping = "PR_FUNC_TITTLE1";
            this.txt_PR_FUNC_Title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FUNC_Title1.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FUNC_Title1.IsDate = false;
            this.txt_PR_FUNC_Title1.Location = new System.Drawing.Point(184, 202);
            this.txt_PR_FUNC_Title1.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_FUNC_Title1.MaxLength = 30;
            this.txt_PR_FUNC_Title1.Name = "txt_PR_FUNC_Title1";
            this.txt_PR_FUNC_Title1.NumberFormat = "###,###,##0.00";
            this.txt_PR_FUNC_Title1.Postfix = "";
            this.txt_PR_FUNC_Title1.Prefix = "";
            this.txt_PR_FUNC_Title1.Size = new System.Drawing.Size(294, 24);
            this.txt_PR_FUNC_Title1.SkipValidation = false;
            this.txt_PR_FUNC_Title1.TabIndex = 26;
            this.txt_PR_FUNC_Title1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(455, 466);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 16);
            this.label18.TabIndex = 57;
            this.label18.Text = "Tax Paid";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(409, 438);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(135, 16);
            this.label17.TabIndex = 55;
            this.label17.Text = "Taxable Income";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(448, 409);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(287, 22);
            this.label16.TabIndex = 54;
            this.label16.Text = "Previous Employment Tax Info";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(43, 466);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(185, 16);
            this.label15.TabIndex = 50;
            this.label15.Text = "ID Card Expiry Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(27, 438);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(201, 16);
            this.label14.TabIndex = 48;
            this.label14.Text = "ID Card Issue Date :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 409);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(225, 16);
            this.label13.TabIndex = 46;
            this.label13.Text = "Bank ID Card Issued [Y/N] :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(405, 334);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(156, 16);
            this.label12.TabIndex = 41;
            this.label12.Text = "National Tax No. :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1, 334);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 16);
            this.label11.TabIndex = 39;
            this.label11.Text = "Annual Package(Rs) :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(448, 303);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 16);
            this.label10.TabIndex = 37;
            this.label10.Text = "Confirmation :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 303);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(167, 16);
            this.label9.TabIndex = 35;
            this.label9.Text = "Joining Date :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(493, 239);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 16);
            this.label8.TabIndex = 30;
            this.label8.Text = "FT/PT";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 271);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 16);
            this.label2.TabIndex = 32;
            this.label2.Text = "Religion :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // lblGeid
            // 
            this.lblGeid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGeid.Location = new System.Drawing.Point(477, 207);
            this.lblGeid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGeid.Name = "lblGeid";
            this.lblGeid.Size = new System.Drawing.Size(91, 16);
            this.lblGeid.TabIndex = 27;
            this.lblGeid.Text = "Geid No.";
            this.lblGeid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFunctionalTitle
            // 
            this.lblFunctionalTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunctionalTitle.Location = new System.Drawing.Point(17, 207);
            this.lblFunctionalTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunctionalTitle.Name = "lblFunctionalTitle";
            this.lblFunctionalTitle.Size = new System.Drawing.Size(163, 16);
            this.lblFunctionalTitle.TabIndex = 25;
            this.lblFunctionalTitle.Text = "Functional Title :";
            this.lblFunctionalTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_PR_LEVEL
            // 
            this.txt_PR_LEVEL.AllowSpace = true;
            this.txt_PR_LEVEL.AssociatedLookUpName = "";
            this.txt_PR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LEVEL.ContinuationTextBox = null;
            this.txt_PR_LEVEL.CustomEnabled = true;
            this.txt_PR_LEVEL.DataFieldMapping = "";
            this.txt_PR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LEVEL.IsDate = false;
            this.txt_PR_LEVEL.IsRequired = true;
            this.txt_PR_LEVEL.Location = new System.Drawing.Point(563, 148);
            this.txt_PR_LEVEL.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LEVEL.Name = "txt_PR_LEVEL";
            this.txt_PR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txt_PR_LEVEL.Postfix = "";
            this.txt_PR_LEVEL.Prefix = "";
            this.txt_PR_LEVEL.Size = new System.Drawing.Size(54, 24);
            this.txt_PR_LEVEL.SkipValidation = true;
            this.txt_PR_LEVEL.TabIndex = 23;
            this.txt_PR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_F_RNAME
            // 
            this.txt_F_RNAME.AllowSpace = true;
            this.txt_F_RNAME.AssociatedLookUpName = "";
            this.txt_F_RNAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_F_RNAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_F_RNAME.ContinuationTextBox = null;
            this.txt_F_RNAME.CustomEnabled = true;
            this.txt_F_RNAME.DataFieldMapping = "NAME";
            this.txt_F_RNAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_F_RNAME.GetRecordsOnUpDownKeys = false;
            this.txt_F_RNAME.IsDate = false;
            this.txt_F_RNAME.Location = new System.Drawing.Point(405, 114);
            this.txt_F_RNAME.Margin = new System.Windows.Forms.Padding(4);
            this.txt_F_RNAME.MaxLength = 20;
            this.txt_F_RNAME.Name = "txt_F_RNAME";
            this.txt_F_RNAME.NumberFormat = "###,###,##0.00";
            this.txt_F_RNAME.Postfix = "";
            this.txt_F_RNAME.Prefix = "";
            this.txt_F_RNAME.ReadOnly = true;
            this.txt_F_RNAME.Size = new System.Drawing.Size(395, 24);
            this.txt_F_RNAME.SkipValidation = false;
            this.txt_F_RNAME.TabIndex = 18;
            this.txt_F_RNAME.TabStop = false;
            this.txt_F_RNAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_LAST_NAME
            // 
            this.txt_PR_LAST_NAME.AllowSpace = true;
            this.txt_PR_LAST_NAME.AssociatedLookUpName = "";
            this.txt_PR_LAST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LAST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LAST_NAME.ContinuationTextBox = null;
            this.txt_PR_LAST_NAME.CustomEnabled = true;
            this.txt_PR_LAST_NAME.DataFieldMapping = "";
            this.txt_PR_LAST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LAST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LAST_NAME.IsDate = false;
            this.txt_PR_LAST_NAME.Location = new System.Drawing.Point(563, 82);
            this.txt_PR_LAST_NAME.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_LAST_NAME.MaxLength = 20;
            this.txt_PR_LAST_NAME.Name = "txt_PR_LAST_NAME";
            this.txt_PR_LAST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_LAST_NAME.Postfix = "";
            this.txt_PR_LAST_NAME.Prefix = "";
            this.txt_PR_LAST_NAME.Size = new System.Drawing.Size(239, 24);
            this.txt_PR_LAST_NAME.SkipValidation = true;
            this.txt_PR_LAST_NAME.TabIndex = 13;
            this.txt_PR_LAST_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_BRANCH
            // 
            this.txt_PR_BRANCH.AllowSpace = true;
            this.txt_PR_BRANCH.AssociatedLookUpName = "";
            this.txt_PR_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_BRANCH.ContinuationTextBox = null;
            this.txt_PR_BRANCH.CustomEnabled = true;
            this.txt_PR_BRANCH.DataFieldMapping = "";
            this.txt_PR_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_BRANCH.IsDate = false;
            this.txt_PR_BRANCH.Location = new System.Drawing.Point(563, 17);
            this.txt_PR_BRANCH.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_BRANCH.MaxLength = 3;
            this.txt_PR_BRANCH.Name = "txt_PR_BRANCH";
            this.txt_PR_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PR_BRANCH.Postfix = "";
            this.txt_PR_BRANCH.Prefix = "";
            this.txt_PR_BRANCH.Size = new System.Drawing.Size(54, 24);
            this.txt_PR_BRANCH.SkipValidation = true;
            this.txt_PR_BRANCH.TabIndex = 4;
            this.txt_PR_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_DESIG
            // 
            this.txt_PR_DESIG.AllowSpace = true;
            this.txt_PR_DESIG.AssociatedLookUpName = "";
            this.txt_PR_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_DESIG.ContinuationTextBox = null;
            this.txt_PR_DESIG.CustomEnabled = true;
            this.txt_PR_DESIG.DataFieldMapping = "";
            this.txt_PR_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_DESIG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_DESIG.IsDate = false;
            this.txt_PR_DESIG.IsRequired = true;
            this.txt_PR_DESIG.Location = new System.Drawing.Point(184, 148);
            this.txt_PR_DESIG.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_DESIG.MaxLength = 3;
            this.txt_PR_DESIG.Name = "txt_PR_DESIG";
            this.txt_PR_DESIG.NumberFormat = "###,###,##0.00";
            this.txt_PR_DESIG.Postfix = "";
            this.txt_PR_DESIG.Prefix = "";
            this.txt_PR_DESIG.Size = new System.Drawing.Size(65, 24);
            this.txt_PR_DESIG.SkipValidation = true;
            this.txt_PR_DESIG.TabIndex = 20;
            this.txt_PR_DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtReportTo
            // 
            this.txtReportTo.AllowSpace = true;
            this.txtReportTo.AssociatedLookUpName = "";
            this.txtReportTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReportTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReportTo.ContinuationTextBox = null;
            this.txtReportTo.CustomEnabled = true;
            this.txtReportTo.DataFieldMapping = "";
            this.txtReportTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportTo.GetRecordsOnUpDownKeys = false;
            this.txtReportTo.IsDate = false;
            this.txtReportTo.Location = new System.Drawing.Point(184, 114);
            this.txtReportTo.Margin = new System.Windows.Forms.Padding(4);
            this.txtReportTo.MaxLength = 6;
            this.txtReportTo.Name = "txtReportTo";
            this.txtReportTo.NumberFormat = "###,###,##0.00";
            this.txtReportTo.Postfix = "";
            this.txtReportTo.Prefix = "";
            this.txtReportTo.Size = new System.Drawing.Size(101, 24);
            this.txtReportTo.SkipValidation = true;
            this.txtReportTo.TabIndex = 15;
            this.txtReportTo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_FIRST_NAME
            // 
            this.txt_PR_FIRST_NAME.AllowSpace = true;
            this.txt_PR_FIRST_NAME.AssociatedLookUpName = "";
            this.txt_PR_FIRST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FIRST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FIRST_NAME.ContinuationTextBox = null;
            this.txt_PR_FIRST_NAME.CustomEnabled = true;
            this.txt_PR_FIRST_NAME.DataFieldMapping = "";
            this.txt_PR_FIRST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FIRST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FIRST_NAME.IsDate = false;
            this.txt_PR_FIRST_NAME.IsRequired = true;
            this.txt_PR_FIRST_NAME.Location = new System.Drawing.Point(184, 82);
            this.txt_PR_FIRST_NAME.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_FIRST_NAME.MaxLength = 20;
            this.txt_PR_FIRST_NAME.Name = "txt_PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_FIRST_NAME.Postfix = "";
            this.txt_PR_FIRST_NAME.Prefix = "";
            this.txt_PR_FIRST_NAME.Size = new System.Drawing.Size(247, 24);
            this.txt_PR_FIRST_NAME.SkipValidation = true;
            this.txt_PR_FIRST_NAME.TabIndex = 11;
            this.txt_PR_FIRST_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(184, 17);
            this.txt_PR_P_NO.Margin = new System.Windows.Forms.Padding(4);
            this.txt_PR_P_NO.MaxLength = 6;
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.Size = new System.Drawing.Size(101, 24);
            this.txt_PR_P_NO.SkipValidation = true;
            this.txt_PR_P_NO.TabIndex = 1;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblLevel
            // 
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(495, 153);
            this.lblLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(61, 16);
            this.lblLevel.TabIndex = 22;
            this.lblLevel.Text = "Level";
            this.lblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(345, 119);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(55, 16);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastName
            // 
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(464, 87);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(92, 16);
            this.lblLastName.TabIndex = 12;
            this.lblLastName.Text = "Last Name :";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBranch
            // 
            this.lblBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(488, 22);
            this.lblBranch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(68, 16);
            this.lblBranch.TabIndex = 3;
            this.lblBranch.Text = "Branch";
            this.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 153);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Designation Code :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblReportTo
            // 
            this.lblReportTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReportTo.Location = new System.Drawing.Point(36, 119);
            this.lblReportTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReportTo.Name = "lblReportTo";
            this.lblReportTo.Size = new System.Drawing.Size(144, 16);
            this.lblReportTo.TabIndex = 14;
            this.lblReportTo.Text = "Report To :";
            this.lblReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFirstName
            // 
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.Location = new System.Drawing.Point(25, 87);
            this.lblFirstName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(155, 16);
            this.lblFirstName.TabIndex = 10;
            this.lblFirstName.Text = "First Name :";
            this.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPersonnelsNo
            // 
            this.lblPersonnelsNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonnelsNo.Location = new System.Drawing.Point(21, 22);
            this.lblPersonnelsNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonnelsNo.Name = "lblPersonnelsNo";
            this.lblPersonnelsNo.Size = new System.Drawing.Size(159, 16);
            this.lblPersonnelsNo.TabIndex = 0;
            this.lblPersonnelsNo.Text = "Personnel No :";
            this.lblPersonnelsNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Category";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Dept";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Contribution";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // colSegment
            // 
            this.colSegment.DataPropertyName = "PR_SEGMENT";
            this.colSegment.HeaderText = "Segment";
            this.colSegment.MaxInputLength = 3;
            this.colSegment.Name = "colSegment";
            // 
            // Dept
            // 
            this.Dept.ActionLOV = "DEPT";
            this.Dept.ActionLOVExists = "DEPT_EXIST";
            this.Dept.AttachParentEntity = false;
            this.Dept.DataPropertyName = "PR_DEPT";
            this.Dept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.Dept.HeaderText = "Dept.";
            this.Dept.LookUpTitle = "Department Codes";
            this.Dept.LOVFieldMapping = "PR_DEPT";
            this.Dept.MaxInputLength = 5;
            this.Dept.Name = "Dept";
            this.Dept.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Dept.SearchColumn = "PR_DEPT";
            this.Dept.SkipValidationOnLeave = false;
            this.Dept.SpName = "CHRIS_SP_RegStHiEnt_DEPT_CONT_MANAGER";
            // 
            // prContrib
            // 
            this.prContrib.DataPropertyName = "PR_P_NO";
            this.prContrib.HeaderText = "Pr_Contrib";
            this.prContrib.Name = "prContrib";
            this.prContrib.Visible = false;
            // 
            // Contribution
            // 
            this.Contribution.DataPropertyName = "PR_CONTRIB";
            this.Contribution.HeaderText = "Contribution %";
            this.Contribution.MaxInputLength = 3;
            this.Contribution.Name = "Contribution";
            this.Contribution.Width = 120;
            // 
            // Pr_Type
            // 
            this.Pr_Type.DataPropertyName = "PR_TYPE";
            this.Pr_Type.HeaderText = "Pr_Type";
            this.Pr_Type.Name = "Pr_Type";
            this.Pr_Type.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.MaxInputLength = 0;
            this.Column1.MinimumWidth = 2;
            this.Column1.Name = "Column1";
            this.Column1.Width = 2;
            // 
            // PrEYear
            // 
            this.PrEYear.DataPropertyName = "PR_E_YEAR";
            this.PrEYear.HeaderText = "Year";
            this.PrEYear.MaxInputLength = 4;
            this.PrEYear.Name = "PrEYear";
            this.PrEYear.Width = 50;
            // 
            // Pr_Degree
            // 
            this.Pr_Degree.DataPropertyName = "PR_DEGREE";
            this.Pr_Degree.HeaderText = "Certificate/Degree";
            this.Pr_Degree.MaxInputLength = 10;
            this.Pr_Degree.Name = "Pr_Degree";
            this.Pr_Degree.Width = 120;
            // 
            // Pr_Grade
            // 
            this.Pr_Grade.DataPropertyName = "PR_GRADE";
            this.Pr_Grade.HeaderText = "Division/Grade";
            this.Pr_Grade.MaxInputLength = 7;
            this.Pr_Grade.Name = "Pr_Grade";
            // 
            // Pr_College
            // 
            this.Pr_College.DataPropertyName = "PR_COLLAGE";
            this.Pr_College.HeaderText = "School/College/Unv";
            this.Pr_College.MaxInputLength = 50;
            this.Pr_College.Name = "Pr_College";
            this.Pr_College.Width = 130;
            // 
            // Pr_City
            // 
            this.Pr_City.DataPropertyName = "PR_CITY";
            this.Pr_City.HeaderText = "City";
            this.Pr_City.MaxInputLength = 10;
            this.Pr_City.Name = "Pr_City";
            // 
            // Pr_Country
            // 
            this.Pr_Country.DataPropertyName = "PR_COUNTRY";
            this.Pr_Country.HeaderText = "Country";
            this.Pr_Country.MaxInputLength = 10;
            this.Pr_Country.Name = "Pr_Country";
            // 
            // Pr_E_No
            // 
            this.Pr_E_No.DataPropertyName = "PR_P_NO";
            this.Pr_E_No.HeaderText = "Pr_E_No";
            this.Pr_E_No.Name = "Pr_E_No";
            this.Pr_E_No.Visible = false;
            // 
            // prPeNo
            // 
            this.prPeNo.DataPropertyName = "PR_P_NO";
            this.prPeNo.HeaderText = "prPeNo";
            this.prPeNo.Name = "prPeNo";
            this.prPeNo.Visible = false;
            // 
            // From
            // 
            this.From.DataPropertyName = "PR_JOB_FROM";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.From.DefaultCellStyle = dataGridViewCellStyle2;
            this.From.HeaderText = "From";
            this.From.MaxInputLength = 10;
            this.From.Name = "From";
            // 
            // To
            // 
            this.To.DataPropertyName = "PR_JOB_TO";
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.To.DefaultCellStyle = dataGridViewCellStyle3;
            this.To.HeaderText = "To";
            this.To.MaxInputLength = 10;
            this.To.Name = "To";
            // 
            // Organization_Address
            // 
            this.Organization_Address.DataPropertyName = "PR_ORGANIZ";
            this.Organization_Address.HeaderText = "Organization Address";
            this.Organization_Address.MaxInputLength = 40;
            this.Organization_Address.Name = "Organization_Address";
            this.Organization_Address.Width = 150;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "PR_DESIG_PR";
            this.Designation.HeaderText = "Designation";
            this.Designation.MaxInputLength = 40;
            this.Designation.Name = "Designation";
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "PR_REMARKS_PR";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 40;
            this.Remarks.Name = "Remarks";
            // 
            // Address1
            // 
            this.Address1.DataPropertyName = "PR_ADD1";
            this.Address1.HeaderText = "Address1";
            this.Address1.Name = "Address1";
            this.Address1.Visible = false;
            // 
            // Address2
            // 
            this.Address2.DataPropertyName = "PR_ADD2";
            this.Address2.HeaderText = "Address2";
            this.Address2.Name = "Address2";
            this.Address2.Visible = false;
            // 
            // Address3
            // 
            this.Address3.DataPropertyName = "PR_ADD3";
            this.Address3.HeaderText = "Address3";
            this.Address3.Name = "Address3";
            this.Address3.Visible = false;
            // 
            // Address4
            // 
            this.Address4.DataPropertyName = "PR_ADD4";
            this.Address4.HeaderText = "Address4";
            this.Address4.Name = "Address4";
            this.Address4.Visible = false;
            // 
            // CHRIS_Personnel_RegularStaffHiringEnt_MC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1748, 852);
            this.Controls.Add(this.dtGVExp);
            this.Controls.Add(this.dtGVEdu);
            this.Controls.Add(this.dtGVDept);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReject);
            this.Controls.Add(this.btnApprove);
            this.Controls.Add(this.pnlTblBlkChild);
            this.Controls.Add(this.slPanelSimple1);
            this.Controls.Add(this.pnlSplBlkPersMain);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlPersonnelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Personnel_RegularStaffHiringEnt_MC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_Personnel_RegularStaffHiringEnt";
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVEdu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGVExp)).EndInit();
            this.pnlTblBlkChild.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBlkChild)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.pnlSplBlkPersMain.ResumeLayout(false);
            this.pnlSplBlkPersMain.PerformLayout();
            this.pnlPersonnelMain.ResumeLayout(false);
            this.pnlPersonnelMain.PerformLayout();
            this.pnlView.ResumeLayout(false);
            this.pnlView.PerformLayout();
            this.pnlSave.ResumeLayout(false);
            this.pnlSave.PerformLayout();
            this.pnlDelete.ResumeLayout(false);
            this.pnlDelete.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private COMMON.SLCONTROLS.SLPanelSimple pnlPersonnelMain;
        private CrplControlLibrary.SLTextBox slTxtBxUserID;
        private CrplControlLibrary.SLTextBox slTxtbEmail;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label label29;
        public COMMON.SLCONTROLS.SLPanelSimple pnlView;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label21;
        public COMMON.SLCONTROLS.SLPanelSimple pnlSave;
        public CrplControlLibrary.SLTextBox txt_W_ADD_ANS;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_W_VIEW_ANS;
        private System.Windows.Forms.Label label28;
        public COMMON.SLCONTROLS.SLPanelSimple pnlDelete;
        public CrplControlLibrary.SLTextBox txt_W_DEL_ANS;
        private System.Windows.Forms.Label label22;
        public CrplControlLibrary.SLDatePicker txt_PR_EXPIRY;
        public CrplControlLibrary.SLDatePicker txt_PR_ID_ISSUE;
        private CrplControlLibrary.SLTextBox txt_PR_NEW_ANNUAL_PACK;
        public CrplControlLibrary.SLDatePicker txt_PR_CONFIRM;
        public CrplControlLibrary.SLDatePicker txt_PR_JOINING_DATE;
        private CrplControlLibrary.SLTextBox txt_PR_CONF_FLAG;
        private CrplControlLibrary.SLTextBox txt_PR_NEW_BRANCH;
        private CrplControlLibrary.SLTextBox txt_PR_CLOSE_FLAG;
        public CrplControlLibrary.SLTextBox txt_PR_TAX_PAID;
        private CrplControlLibrary.SLTextBox txt_PR_TAX_INC;
        private CrplControlLibrary.SLTextBox txt_PR_BANK_ID;
        private CrplControlLibrary.SLTextBox txt_PR_ACCOUNT_NO;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txt_PR_NATIONAL_TAX;
        private CrplControlLibrary.SLTextBox txt_PR_ANNUAL_PACK;
        private CrplControlLibrary.SLTextBox txt_PR_EMP_TYPE;
        private CrplControlLibrary.SLTextBox txt_F_GEID_NO;
        private CrplControlLibrary.SLTextBox txt_PR_RELIGION;
        private CrplControlLibrary.SLTextBox txt_W_RELIGION;
        private CrplControlLibrary.SLTextBox txt_PR_FUNC_Title2;
        private CrplControlLibrary.SLTextBox txt_PR_FUNC_Title1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGeid;
        private System.Windows.Forms.Label lblFunctionalTitle;
        private CrplControlLibrary.SLTextBox txt_PR_LEVEL;
        private CrplControlLibrary.SLTextBox txt_F_RNAME;
        private CrplControlLibrary.SLTextBox txt_PR_LAST_NAME;
        private CrplControlLibrary.SLTextBox txt_PR_BRANCH;
        private CrplControlLibrary.SLTextBox txt_PR_DESIG;
        private CrplControlLibrary.SLTextBox txtReportTo;
        private CrplControlLibrary.SLTextBox txt_PR_FIRST_NAME;
        public CrplControlLibrary.SLTextBox txt_PR_P_NO;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblBranch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblReportTo;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblPersonnelsNo;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSegment;
        private COMMON.SLCONTROLS.DataGridViewLOVColumn Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn prContrib;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contribution;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Type;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrEYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Degree;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_College;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_City;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pr_E_No;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridViewTextBoxColumn prPeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn Organization_Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address4;
        private COMMON.SLCONTROLS.SLPanelSimple pnlSplBlkPersMain;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLTextBox txt_w_date_2;
        private CrplControlLibrary.SLTextBox txt_PR_NO_OF_CHILD;
        private CrplControlLibrary.SLDatePicker txt_PR_BIRTH_SP;
        private CrplControlLibrary.SLDatePicker txt_PR_MARRIAGE;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLDatePicker txt_PR_D_BIRTH;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txt_PR_ADD2;
        private System.Windows.Forms.Label label53;
        private CrplControlLibrary.SLTextBox txt_PR_PHONE1;
        private System.Windows.Forms.Label label52;
        private CrplControlLibrary.SLTextBox txt_PR_PHONE2;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private CrplControlLibrary.SLTextBox txt_PR_ID_CARD_NO;
        private System.Windows.Forms.Label label49;
        private CrplControlLibrary.SLTextBox txt_PR_SEX;
        private System.Windows.Forms.Label label48;
        private CrplControlLibrary.SLTextBox txt_PR_OLD_ID_CARD_NO;
        private System.Windows.Forms.Label label47;
        public CrplControlLibrary.SLTextBox txt_PR_MARITAL;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private CrplControlLibrary.SLTextBox txt_PR_LANG1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label44;
        private CrplControlLibrary.SLTextBox txt_PR_LANG2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label43;
        private CrplControlLibrary.SLTextBox txt_PR_LANG3;
        private CrplControlLibrary.SLTextBox txt_PR_ADD1;
        private System.Windows.Forms.Label label42;
        private CrplControlLibrary.SLTextBox txt_PR_LANG4;
        private CrplControlLibrary.SLTextBox txt_PR_LANG6;
        private System.Windows.Forms.Label label41;
        private CrplControlLibrary.SLTextBox txt_PR_LANG5;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private CrplControlLibrary.SLTextBox txt_PR_SPOUSE;
        private COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private CrplControlLibrary.SLTextBox txt_PR_PA_NO;
        public CrplControlLibrary.SLTextBox txt_PR_USER_IS_PRIME;
        private CrplControlLibrary.SLTextBox txt_PR_GROUP_HOSP;
        private CrplControlLibrary.SLTextBox txt_PR_OPD;
        private CrplControlLibrary.SLTextBox txt_PR_GROUP_LIFE;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label54;
        private COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkChild;
        private COMMON.SLCONTROLS.SLDataGridView dgvBlkChild;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChildName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date_of_Birth;
        private System.Windows.Forms.Button btnApprove;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnClose;
        private CrplControlLibrary.SLTextBox txtLoc;
        private System.Windows.Forms.DataGridView dtGVDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dtGVEdu;
        private System.Windows.Forms.DataGridView dtGVExp;
    }
}