namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    partial class CHRIS_Personnel_ConfirmationEntry_Month
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtinputMonth = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtinputMonth
            // 
            this.txtinputMonth.AllowSpace = true;
            this.txtinputMonth.AssociatedLookUpName = "";
            this.txtinputMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtinputMonth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtinputMonth.ContinuationTextBox = null;
            this.txtinputMonth.CustomEnabled = true;
            this.txtinputMonth.DataFieldMapping = "";
            this.txtinputMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtinputMonth.GetRecordsOnUpDownKeys = false;
            this.txtinputMonth.IsDate = false;
            this.txtinputMonth.Location = new System.Drawing.Point(192, 25);
            this.txtinputMonth.Name = "txtinputMonth";
            this.txtinputMonth.NumberFormat = "###,###,##0.00";
            this.txtinputMonth.Postfix = "";
            this.txtinputMonth.Prefix = "";
            this.txtinputMonth.Size = new System.Drawing.Size(43, 20);
            this.txtinputMonth.SkipValidation = false;
            this.txtinputMonth.TabIndex = 0;
            this.txtinputMonth.TextType = CrplControlLibrary.TextType.String;
            this.txtinputMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtinputMonth_Validating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(136, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Month :";
            // 
            // CHRIS_Personnel_ConfirmationEntry_Month
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(373, 78);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtinputMonth);
            this.Name = "CHRIS_Personnel_ConfirmationEntry_Month";
            this.Text = "CHRIS_Personnel_ActualLeaveEntry_Month";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtinputMonth;
        private System.Windows.Forms.Label label1;
    }
}