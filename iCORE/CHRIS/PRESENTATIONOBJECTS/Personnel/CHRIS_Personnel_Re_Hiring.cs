using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_Re_Hiring : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();

        # region Constructor
        public CHRIS_Personnel_Re_Hiring()
        {
            InitializeComponent();
        }

        public CHRIS_Personnel_Re_Hiring(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.CurrentPanelBlock = this.PnlPersonnel.Name;
        }
        #endregion

        # region Methods
        private DataTable proc_mod_Del(int Reh_no)
        {
          Result rsltCode;
          DataTable dt  = null;
            
            Dictionary<string, object> param = new Dictionary<string, object>();
            

            param.Add("REH_PR_NO", Reh_no);
            rsltCode = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "Proc_mod_del", param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    DataTable dtable = (DataTable)DGVPersonnel.DataSource;
                  //  DGVPersonnel.DataSource = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private void proc_view_add(int Reh_no)
        {

            Result rsltCode;
            string sp_Desc = string.Empty;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("REH_PR_NO", Reh_no);

            rsltCode = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "proc_view_add", param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                    dtf.ShortDatePattern = "dd/MM/yyyy";
                    DataTable dtable = (DataTable)DGVPersonnel.DataSource;
                    //dtable.Rows[]["reh_pr_no"] = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    //dtable.Rows[0]["reh_old_pr_no"] = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    //dtable.Rows[0]["reh_date"] = DateTime.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString(), dtf);
                    DGVPersonnel.CurrentRow.Cells["reh_pr_no"].Value = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    DGVPersonnel.CurrentRow.Cells["reh_old_pr_no"].Value = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    DGVPersonnel.CurrentRow.Cells["reh_date"].Value = DateTime.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString(),dtf);
               
                  
                }
            }
           // return sp_Desc;
        }
        private void Post_name(int Reh_no)
        {
            Result rsltCode;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("REH_PR_NO", Reh_no);

            rsltCode = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "Post_Name", param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    DataTable dtable = (DataTable)DGVPersonnel.DataSource;
                    DGVPersonnel.CurrentRow.Cells["LC_NAME"].Value = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                   // dtable.Rows[0]["LC_NAME"] = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    DGVPersonnel.CurrentRow.Cells["LC_DATE"].Value = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                }
            }
        
        
        
        
        }
        private void Proc_Modify() 
        { }
        protected override void  OnLoad(EventArgs e)
            {
 	             base.OnLoad(e);
                 txtOption.Select();
                 txtOption.Focus();


                 this.txtUser.Text = this.userID;
                 this.txtDate.Text = this.Now().ToString("MM/dd/yyyy");

            }
        //public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        //{
        //    if (actionType == "Save")
        //    {
        //        //Result rslt;
        //        //Dictionary<string, object> param = new Dictionary<string, object>();

                
        //        //param.Add("reh_old_pr_no", DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].EditedFormattedValue);
        //        // rslt =cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "update_person", param);

        //        // if (rslt.isSuccessful)
        //        // {
        //        //     MessageBox.Show("Record saved successfully");
        //        //     DataTable dt = (DataTable)(DGVPersonnel.DataSource);
        //        //     dt.Rows.Clear();
        //        // }

        //        return;
        //    }


        //    if (actionType == "Delete")
        //    {
        //        Result rslt;
        //        Dictionary<string, object> param = new Dictionary<string, object>();


        //        param.Add("reh_old_pr_no", DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].EditedFormattedValue);
        //        rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "Proc_delete", param);

        //        if (rslt.isSuccessful)
        //        {
        //            MessageBox.Show("Record Deleted successfully");
        //            DataTable dt = (DataTable)(DGVPersonnel.DataSource);
        //            dt.Rows.Clear();
        //        }

        //        return;
        //    }




        //    if (actionType == "Search")
        //    {
        //        return;
            
        //    }


        //}
        public void Add_New_Pr_p_no()
        {

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "GetPr_p_no");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    DataTable dtable = (DataTable)DGVPersonnel.DataSource;
                    if (dtable.Rows.Count < 1)
                    {
                        dtable.Rows.Add();
                    }
                    dtable.Rows[0]["REH_PR_NO"] = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    DGVPersonnel.CurrentCell = DGVPersonnel.Rows[0].Cells[0];
                  //  int a = DGVPersonnel.CurrentCellAddress.X;
                    DGVPersonnel.Focus();
                    DGVPersonnel.Rows[0].Cells[0].ReadOnly = true;
                }

            }
        
        
        }




        #endregion

        # region Events
        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
            if (txtOption.Text == "A")
            {

                Add_New_Pr_p_no();
            
            }
        }
        private void DGVPersonnel_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            # region REH_PR_NO (cell 0)
            if (e.ColumnIndex == 0)
            {
                if (DGVPersonnel.CurrentRow.Cells[0].IsInEditMode)
                {
                    #region Modify or Del
                    if (FunctionConfig.CurrentOption == Function.Modify || FunctionConfig.CurrentOption == Function.Add)
                    {

                        proc_mod_Del(Int32.Parse(DGVPersonnel.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString()));

                    }
                    #endregion

                    #region add
                   if (FunctionConfig.CurrentOption == Function.Add)
                    {
                        Result rslt;
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("REH_PR_NO", Int32.Parse(DGVPersonnel.CurrentRow.Cells[0].EditedFormattedValue.ToString()));
                        rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "PrP_count", param);
                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                int count;
                                count = Int32.Parse(rslt.dstResult.Tables[0].Rows[e.RowIndex].ItemArray[0].ToString());
                                if (count > 0)
                                {
                                    MessageBox.Show("Record Already Entered");
                                   // base.ClearForm(PnlPersonnel.Controls);


                                }
                            }
                        }

                    }
                    #endregion

                    #region view or add
                    if (FunctionConfig.CurrentOption == Function.View || FunctionConfig.CurrentOption == Function.Add)
                    {
                        proc_view_add(Int32.Parse(DGVPersonnel.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString()));
                    }

                    #endregion

                    #region Modify

                    if (FunctionConfig.CurrentOption == Function.Modify)
                    {
                        Post_name(Int32.Parse(DGVPersonnel.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString()));

                    }
                    #endregion

                    # region view

                    if (FunctionConfig.CurrentOption == Function.View)
                    {
                    //    if (MessageBox.Show("Do You Want to view more records   [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    //    {
                    //    }
                    //    else
                    //    { //base.ClearForm(PnlPersonnel.Controls); 
                    //    }
 
                    }
                    #endregion

                    #region Delete
                    if (FunctionConfig.CurrentOption == Function.Delete)
                    { 
                      if(MessageBox.Show("Do You Want to Delete this records [Y/N]","",MessageBoxButtons.YesNo) == DialogResult.Yes)
                      {
                          base.DoToolbarActions(this.Controls,"Delete");
                      }
                 
                    }

                    #endregion
                }

            }
            #endregion

            # region REH_OLD_PR_NO (cell 1)

            if (e.ColumnIndex == 1)
            {

                if ((DGVPersonnel.CurrentCell.IsInEditMode) && (DGVPersonnel.CurrentRow.Cells["REH_OLD_PR_NO"].EditedFormattedValue == null || DGVPersonnel.CurrentRow.Cells["REH_OLD_PR_NO"].EditedFormattedValue.ToString() == string.Empty || DGVPersonnel.CurrentRow.Cells["REH_OLD_PR_NO"].EditedFormattedValue.ToString() == ""))
                {
                    //base.DoToolbarActions("cancel");

                    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["Cancel"]));
                    ////return base.Edit();
                }
                else if (DGVPersonnel.CurrentCell.IsInEditMode)
                
                {
                    string Pr_p_no;
                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("REH_OLD_PR_NO", Int32.Parse(DGVPersonnel.Rows[0].Cells["REH_OLD_PR_NO"].EditedFormattedValue.ToString()));
                    rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "onOldP_no_NextItem", param);
                    if (rslt.isSuccessful && rslt.dstResult.Tables != null)
                    {
                        if (rslt.dstResult.Tables.Count>0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                Pr_p_no = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                                MessageBox.Show("Employee Has Already Been Rehired Can Not Hire It Again" +
                                    " Until Resigned Again With P.No." +' '+ Pr_p_no);
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            #endregion

            # region REH_DATE (cell 3)

            if (e.ColumnIndex == 3)
            {
                if (DGVPersonnel.CurrentRow.Cells["REH_DATE"].IsInEditMode)
                {
            
                    if (DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue != null && DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString()  != string.Empty && DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString() != "")
                        {
                             //this.CurrentDate
                                DateTime dTime = new DateTime(1900, 1, 1);
                                DateTime dt_Maxdt = this.CurrentDate;
                                int days;

                        try
                        {
                            System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                            dtf.ShortDatePattern = "dd/MM/yyyy";
                            dTime = DateTime.Parse((DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString()), dtf);
                          TimeSpan ts = dTime.Subtract(dt_Maxdt);

                            days = ts.Days;
                            int years= days/365;

                            if (days < 0 )
                             {
                                 if (years < 0)
                                 {
                                     MessageBox.Show("Re-hiring Year Can Not Be Less Than Current Year");
                                     e.Cancel = true;
                                 }
                            }
                            else
                            { }

                        }
                        catch (Exception ex)
                        {

                            if (dTime == new DateTime(1900, 1, 1))
                            {
                                DGVPersonnel.CurrentCell.ErrorText = "Incorrect date format";
                                e.Cancel = true;
                                return;
                            }
                        }
                            
                }

                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    int count;
                    Result rslt;
                    System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                    dtf.ShortDatePattern = "dd/MM/yyyy";
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("REH_OLD_PR_NO",Int32.Parse(DGVPersonnel.CurrentRow.Cells["REH_OLD_PR_NO"].EditedFormattedValue.ToString()));
                    param.Add("REH_DATE",DateTime.Parse(DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString(),dtf) );

                    rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "Rec_Already_Entered", param);

                    if (rslt.isSuccessful)
                    {
                        if ((rslt.dstResult.Tables[0].Rows.Count > 0) && (rslt.dstResult != null))
                        {

                            count = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                            if (count > 0)
                            {

                                MessageBox.Show("Record Already Entered");
                                e.Cancel = true;
                            
                            
                            }
                        }
                                       
                    }
                }
       

            
            }
            
            }
        
#endregion

            # region LC_DATE (cell 4)

                if (e.ColumnIndex == 4)
            {

                if (DGVPersonnel.CurrentRow.Cells["LC_Date"].IsInEditMode)
                {

                    if (DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue != null && DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue.ToString() != string.Empty && DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue.ToString().ToString() != "")
                    {
                        System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                        dtf.ShortDatePattern = "dd/MM/yyyy";
                        //this.CurrentDate
                        DateTime Lc_date = new DateTime(1900, 1, 1);
                        DateTime Reh_date = DateTime.Parse(DGVPersonnel.CurrentRow.Cells["REH_DATE"].EditedFormattedValue.ToString(),dtf);
                        int days;

                        try
                        {
                            
                            Lc_date = DateTime.Parse((DGVPersonnel.CurrentRow.Cells["LC_Date"].EditedFormattedValue.ToString()), dtf);
                            TimeSpan ts = Lc_date.Subtract(Reh_date);

                            days = ts.Days;

                            if (days < 0)
                            {
                                MessageBox.Show("Confirmation Date Should be Greater Than Re-Hiring Date");
                                e.Cancel = true;

                            }
                            else
                            { }

                        }
                        catch (Exception ex)
                        {

                            if (Lc_date == new DateTime(1900, 1, 1))
                            {
                                DGVPersonnel.CurrentCell.ErrorText = "Incorrect date format";
                                e.Cancel = true;
                                return;
                            }
                        }

                    }

                    else {

                        e.Cancel = true;
                    }
                }


                if (FunctionConfig.CurrentOption == Function.Modify)
                {

                    Proc_Modify();
                }
            
            
            }
        #endregion

        }
        private void DGVPersonnel_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        bool isSaved = false;
        private void DGVPersonnel_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                if (FunctionConfig.CurrentOption == Function.Add)
                {
                    if (!isSaved)
                    {
                        Result rslt;
                        if (MessageBox.Show("Do You Want Save   [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            isSaved = true;
                            Dictionary<string, object> param = new Dictionary<string, object>();

                            param.Add("reh_old_pr_no", DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].EditedFormattedValue);
                            rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "update_person", param);
                            //base.DoToolbarActions(this.Controls, "Save");
                            DGVPersonnel.EndEdit();
                            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
                            DataTable dt = (DataTable)(DGVPersonnel.DataSource);
                            dt.Rows.Clear();
                            //base.DoToolbarActions(this.PnlPersonnel.Controls, "Cancel");
                            Add_New_Pr_p_no();
                        }
                    }
                    else
                    {
                        DataTable dt = (DataTable)(DGVPersonnel.DataSource);
                        dt.Rows.Clear();
                        isSaved = false;
                    }
                }






                if (FunctionConfig.CurrentOption == Function.Delete)
                {

                    Result rslt;
                    if (MessageBox.Show("Do You Want Delete   [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        Dictionary<string, object> param = new Dictionary<string, object>();

                        param.Add("reh_old_pr_no", DGVPersonnel.CurrentRow.Cells["REH_PR_NO"].EditedFormattedValue);
                        rslt = cmnDM.GetData("CHRIS_SP_REHAIR_MANAGER", "Proc_delete", param);
                        DGVPersonnel.EndEdit();
                        DataTable dt = (DataTable)(DGVPersonnel.DataSource);
                        dt.Rows.Clear();
                        //base.DoToolbarActions(this.PnlPersonnel.Controls, "Cancel");
                       // Add_New_Pr_p_no();

                    }
                    else
                    {
                        DataTable dt = (DataTable)(DGVPersonnel.DataSource);
                        dt.Rows.Clear();

                    }
                }





                if (FunctionConfig.CurrentOption == Function.View)
                {

                    Result rslt;
                    if (MessageBox.Show("Do You Want View More Records  [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                      
                    }
                    else
                    {
                        DataTable dt = (DataTable)(DGVPersonnel.DataSource);
                        dt.Rows.Clear();

                    }
                }






            }
        }

        #endregion

        private void DGVPersonnel_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}