using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel
{
    public partial class CHRIS_Personnel_IncrementOfficers_Level : ChrisSimpleForm
    {

        public String desig;
        public String levelClerical;
        public String functit1Clerical;
        public String functit2Clerical;
        CHRIS_Personnel_IncremPromotCleric _mainForm;
        CHRIS_Personnel_IncrementOfficersClerical_Course _courseFrm;
        public CHRIS_Personnel_IncrementOfficers_Level(CHRIS_Personnel_IncremPromotCleric mainForm)
        {
            txtOption.Visible = false;
            InitializeComponent();
            _mainForm = mainForm;
           
        }

        public CHRIS_Personnel_IncrementOfficers_Level(CHRIS_Personnel_IncremPromotCleric mainForm,CHRIS_Personnel_IncrementOfficersClerical_Course courseFrm)
        {
            txtOption.Visible = false;
            InitializeComponent();
            _mainForm = mainForm;
            _courseFrm = courseFrm;

        }
        private void SetValues()
        {
            txtLevel.Text       = _mainForm.txtPR_LEVEL_PRESENT.Text;
            txtfunctitle1.Text  = _mainForm.txtPR_FUNCT_1_PRESENT.Text;
            txtfunctitl2.Text   = _mainForm.txtPR_FUNCT_2_PRESENT.Text;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible  = false;
            this.tbtAdd.Visible     = false;
            this.tbtSave.Visible    = false;
            this.tbtDelete.Visible  = false;
            this.tbtList.Visible    = false;
            this.tbtCancel.Visible  = false;
            this.tbtClose.Visible   = false;
            this.txtLevel.Select();
            this.txtLevel.Focus();
            this.stsOptions.Visible         = false;
            this.txtLevel.CustomEnabled     = true;
            this.txtfunctitle1.CustomEnabled= true;
            this.txtfunctitl2.CustomEnabled = true;
            this.txtLevel.Enabled           = true;
            this.txtfunctitle1.Enabled      = true;
            this.txtfunctitl2.Enabled       = true;
            this.txtLevel.ReadOnly          = false;
            this.txtfunctitle1.ReadOnly     = false;
            this.txtfunctitl2.ReadOnly      = false;
            this.ShowStatusBar              = false;

            //if (_mainForm.viewoption.Text == "Y")
            {
                SetValues();
            }

        }

        private void txtLevel_Validating(object sender, CancelEventArgs e)
        {
            levelClerical = txtLevel.Text;

        }

        private void txtfunctitle1_Validating(object sender, CancelEventArgs e)
        {
            functit1Clerical = txtfunctitle1.Text;
        }

        private void txtfunctitl2_Validating(object sender, CancelEventArgs e)
        {
            functit2Clerical = txtfunctitl2.Text;
        }

        private void CHRIS_Personnel_IncrementOfficers_Level_Shown(object sender, EventArgs e)
        {
            if (_mainForm.txtOption.Text == "V" && _mainForm.txtType.Text !="C")
            {
                _mainForm.ShowViewMsg(this);
            }
            if (_mainForm.txtOption.Text == "V" && _mainForm.txtType.Text == "C")
            {
                _mainForm.ShowViewMsgCourse(this, _courseFrm);
            }
            if (_mainForm.txtOption.Text == "D")
            {
                _mainForm.ShowDeleteMsg(this);
            }
        }
    }
}