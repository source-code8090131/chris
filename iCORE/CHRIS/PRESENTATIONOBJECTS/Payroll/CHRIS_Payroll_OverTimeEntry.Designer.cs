namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_OverTimeEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slTBOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.slPanelMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slPanelDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.slTBOption);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.slTextBox1);
            this.pnlHead.Controls.Add(this.slTextBox2);
            this.pnlHead.Controls.Add(this.label33);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label34);
            this.pnlHead.Controls.Add(this.label35);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label36);
            this.pnlHead.Controls.Add(this.label37);
            this.pnlHead.Location = new System.Drawing.Point(0, 85);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(645, 52);
            this.pnlHead.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(497, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 15);
            this.label3.TabIndex = 56;
            this.label3.Text = "Option:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(29, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "User :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTBOption
            // 
            this.slTBOption.AllowSpace = true;
            this.slTBOption.AssociatedLookUpName = "";
            this.slTBOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTBOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTBOption.ContinuationTextBox = null;
            this.slTBOption.CustomEnabled = true;
            this.slTBOption.DataFieldMapping = "";
            this.slTBOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTBOption.GetRecordsOnUpDownKeys = false;
            this.slTBOption.IsDate = false;
            this.slTBOption.Location = new System.Drawing.Point(556, 3);
            this.slTBOption.MaxLength = 10;
            this.slTBOption.Name = "slTBOption";
            this.slTBOption.NumberFormat = "###,###,##0.00";
            this.slTBOption.Postfix = "";
            this.slTBOption.Prefix = "";
            this.slTBOption.ReadOnly = true;
            this.slTBOption.Size = new System.Drawing.Size(80, 20);
            this.slTBOption.SkipValidation = false;
            this.slTBOption.TabIndex = 54;
            this.slTBOption.TabStop = false;
            this.slTBOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 53;
            this.label2.Text = "Location :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = false;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(89, 26);
            this.slTextBox1.MaxLength = 10;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(87, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 21;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = false;
            this.slTextBox2.DataFieldMapping = "";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(89, 3);
            this.slTextBox2.MaxLength = 10;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.ReadOnly = true;
            this.slTextBox2.Size = new System.Drawing.Size(87, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 13;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(206, 8);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 30);
            this.label33.TabIndex = 19;
            this.label33.Text = "PAYROLL SYSTEM \n TAX ASSESMENT STATUS ENTRY";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(556, 27);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(501, 77);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(509, 31);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 15);
            this.label34.TabIndex = 16;
            this.label34.Text = "Date:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(450, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 15);
            this.label35.TabIndex = 15;
            this.label35.Text = "Option:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label35.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(83, 87);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(83, 61);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(7, 87);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 20);
            this.label36.TabIndex = 2;
            this.label36.Text = "Location:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(16, 61);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 20);
            this.label37.TabIndex = 1;
            this.label37.Text = "User:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label37.Visible = false;
            // 
            // slPanelMaster
            // 
            this.slPanelMaster.ConcurrentPanels = null;
            this.slPanelMaster.DataManager = null;
            this.slPanelMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelMaster.DependentPanels = null;
            this.slPanelMaster.DisableDependentLoad = false;
            this.slPanelMaster.EnableDelete = true;
            this.slPanelMaster.EnableInsert = true;
            this.slPanelMaster.EnableUpdate = true;
            this.slPanelMaster.EntityName = null;
            this.slPanelMaster.Location = new System.Drawing.Point(9, 144);
            this.slPanelMaster.MasterPanel = null;
            this.slPanelMaster.Name = "slPanelMaster";
            this.slPanelMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.slPanelMaster.Size = new System.Drawing.Size(654, 61);
            this.slPanelMaster.SPName = null;
            this.slPanelMaster.TabIndex = 11;
            // 
            // slPanelDetail
            // 
            this.slPanelDetail.ConcurrentPanels = null;
            this.slPanelDetail.DataManager = null;
            this.slPanelDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelDetail.DependentPanels = null;
            this.slPanelDetail.DisableDependentLoad = false;
            this.slPanelDetail.EnableDelete = true;
            this.slPanelDetail.EnableInsert = true;
            this.slPanelDetail.EnableUpdate = true;
            this.slPanelDetail.EntityName = null;
            this.slPanelDetail.Location = new System.Drawing.Point(9, 212);
            this.slPanelDetail.MasterPanel = null;
            this.slPanelDetail.Name = "slPanelDetail";
            this.slPanelDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelDetail.Size = new System.Drawing.Size(654, 198);
            this.slPanelDetail.SPName = null;
            this.slPanelDetail.TabIndex = 12;
            // 
            // CHRIS_Payroll_OverTimeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 476);
            this.Controls.Add(this.slPanelDetail);
            this.Controls.Add(this.slPanelMaster);
            this.Controls.Add(this.pnlHead);
            this.Name = "CHRIS_Payroll_OverTimeEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Payroll_OverTimeEntry";
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.slPanelMaster, 0);
            this.Controls.SetChildIndex(this.slPanelDetail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTBOption;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelMaster;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelDetail;
    }
}