namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_SalaryAdvanceEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_SalaryAdvanceEntry));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.CurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtHiddenField = new CrplControlLibrary.SLTextBox(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.DtSdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtn = new CrplControlLibrary.LookupButton(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMonth2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMonth1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnel = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.slPanelSimple1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(384, 0);
            this.txtOption.Size = new System.Drawing.Size(43, 20);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Location = new System.Drawing.Point(61, 3);
            this.pnlBottom.Size = new System.Drawing.Size(427, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 409);
            this.panel1.Size = new System.Drawing.Size(619, 79);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(200, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "SALARY ADVANCE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(200, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PAYROLL SYSTEM";
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label12);
            this.pnlHead.Controls.Add(this.CurrOption);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label8);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(4, 39);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(615, 88);
            this.pnlHead.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(390, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Option";
            // 
            // CurrOption
            // 
            this.CurrOption.AllowSpace = true;
            this.CurrOption.AssociatedLookUpName = "";
            this.CurrOption.BackColor = System.Drawing.SystemColors.Control;
            this.CurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CurrOption.ContinuationTextBox = null;
            this.CurrOption.CustomEnabled = false;
            this.CurrOption.DataFieldMapping = "";
            this.CurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrOption.GetRecordsOnUpDownKeys = false;
            this.CurrOption.IsDate = false;
            this.CurrOption.Location = new System.Drawing.Point(435, 34);
            this.CurrOption.Name = "CurrOption";
            this.CurrOption.NumberFormat = "###,###,##0.00";
            this.CurrOption.Postfix = "";
            this.CurrOption.Prefix = "";
            this.CurrOption.Size = new System.Drawing.Size(100, 20);
            this.CurrOption.SkipValidation = false;
            this.CurrOption.TabIndex = 21;
            this.CurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = false;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(435, 59);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(100, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 20;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(390, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "Date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.txtHiddenField);
            this.slPanelSimple1.Controls.Add(this.txtID);
            this.slPanelSimple1.Controls.Add(this.DtSdate);
            this.slPanelSimple1.Controls.Add(this.lbtn);
            this.slPanelSimple1.Controls.Add(this.label9);
            this.slPanelSimple1.Controls.Add(this.label7);
            this.slPanelSimple1.Controls.Add(this.label15);
            this.slPanelSimple1.Controls.Add(this.txtAmount);
            this.slPanelSimple1.Controls.Add(this.label6);
            this.slPanelSimple1.Controls.Add(this.label5);
            this.slPanelSimple1.Controls.Add(this.label4);
            this.slPanelSimple1.Controls.Add(this.label3);
            this.slPanelSimple1.Controls.Add(this.txtMonth2);
            this.slPanelSimple1.Controls.Add(this.txtMonth1);
            this.slPanelSimple1.Controls.Add(this.txtName);
            this.slPanelSimple1.Controls.Add(this.txtPersonnel);
            this.slPanelSimple1.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SalaryAdvanceCommand";
            this.slPanelSimple1.Location = new System.Drawing.Point(3, 136);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(616, 267);
            this.slPanelSimple1.SPName = "CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER";
            this.slPanelSimple1.TabIndex = 2;
            // 
            // txtHiddenField
            // 
            this.txtHiddenField.AllowSpace = true;
            this.txtHiddenField.AssociatedLookUpName = "";
            this.txtHiddenField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHiddenField.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHiddenField.ContinuationTextBox = null;
            this.txtHiddenField.CustomEnabled = true;
            this.txtHiddenField.DataFieldMapping = "ID";
            this.txtHiddenField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHiddenField.GetRecordsOnUpDownKeys = false;
            this.txtHiddenField.IsDate = false;
            this.txtHiddenField.Location = new System.Drawing.Point(9, 235);
            this.txtHiddenField.Name = "txtHiddenField";
            this.txtHiddenField.NumberFormat = "###,###,##0.00";
            this.txtHiddenField.Postfix = "";
            this.txtHiddenField.Prefix = "";
            this.txtHiddenField.Size = new System.Drawing.Size(28, 20);
            this.txtHiddenField.SkipValidation = false;
            this.txtHiddenField.TabIndex = 17;
            this.txtHiddenField.TextType = CrplControlLibrary.TextType.String;
            this.txtHiddenField.Visible = false;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "lbtn";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(22, 105);
            this.txtID.MaxLength = 6;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(28, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 16;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // DtSdate
            // 
            this.DtSdate.CustomEnabled = false;
            this.DtSdate.CustomFormat = "dd/MM/yyyy";
            this.DtSdate.DataFieldMapping = "SA_DATE";
            this.DtSdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtSdate.HasChanges = false;
            this.DtSdate.Location = new System.Drawing.Point(342, 192);
            this.DtSdate.Name = "DtSdate";
            this.DtSdate.NullValue = " ";
            this.DtSdate.Size = new System.Drawing.Size(109, 20);
            this.DtSdate.TabIndex = 14;
            this.DtSdate.Value = new System.DateTime(2011, 1, 20, 0, 0, 0, 0);
            // 
            // lbtn
            // 
            this.lbtn.ActionLOVExists = "PR_P_NO_Exists";
            this.lbtn.ActionType = "BLK_P_PR_P_NO_LOV0";
            this.lbtn.ConditionalFields = "";
            this.lbtn.CustomEnabled = true;
            this.lbtn.DataFieldMapping = "";
            this.lbtn.DependentLovControls = "";
            this.lbtn.HiddenColumns = "";
            this.lbtn.Image = ((System.Drawing.Image)(resources.GetObject("lbtn.Image")));
            this.lbtn.LoadDependentEntities = true;
            this.lbtn.Location = new System.Drawing.Point(79, 49);
            this.lbtn.LookUpTitle = null;
            this.lbtn.Name = "lbtn";
            this.lbtn.Size = new System.Drawing.Size(26, 21);
            this.lbtn.SkipValidationOnLeave = false;
            this.lbtn.SPName = "CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER";
            this.lbtn.TabIndex = 13;
            this.lbtn.TabStop = false;
            this.lbtn.UseVisualStyleBackColor = true;
            this.lbtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtn_MouseDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(370, 176);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Was Taken";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(339, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(148, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Date On Which Advance";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(389, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Month 2";
            // 
            // txtAmount
            // 
            this.txtAmount.AllowSpace = true;
            this.txtAmount.AssociatedLookUpName = "";
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAmount.ContinuationTextBox = null;
            this.txtAmount.CustomEnabled = true;
            this.txtAmount.DataFieldMapping = "SA_ADV_AMOUNT";
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.GetRecordsOnUpDownKeys = false;
            this.txtAmount.IsDate = false;
            this.txtAmount.Location = new System.Drawing.Point(459, 51);
            this.txtAmount.MaxLength = 6;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.NumberFormat = "###,###,##0.00";
            this.txtAmount.Postfix = "";
            this.txtAmount.Prefix = "";
            this.txtAmount.Size = new System.Drawing.Size(90, 20);
            this.txtAmount.SkipValidation = false;
            this.txtAmount.TabIndex = 8;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextType = CrplControlLibrary.TextType.Amount;
            this.txtAmount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtAmount_PreviewKeyDown);
            this.txtAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAmount_KeyDown);
            this.txtAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            this.txtAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAmount_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(456, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 26);
            this.label6.TabIndex = 7;
            this.label6.Text = "Advance\r\n Amount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(319, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Month 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(113, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Personnel \r\nNo";
            // 
            // txtMonth2
            // 
            this.txtMonth2.AllowSpace = true;
            this.txtMonth2.AssociatedLookUpName = "";
            this.txtMonth2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMonth2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonth2.ContinuationTextBox = null;
            this.txtMonth2.CustomEnabled = true;
            this.txtMonth2.DataFieldMapping = "SA_FOR_THE_MONTH2";
            this.txtMonth2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonth2.GetRecordsOnUpDownKeys = false;
            this.txtMonth2.IsDate = false;
            this.txtMonth2.Location = new System.Drawing.Point(384, 51);
            this.txtMonth2.MaxLength = 2;
            this.txtMonth2.Name = "txtMonth2";
            this.txtMonth2.NumberFormat = "###,###,##0.00";
            this.txtMonth2.Postfix = "";
            this.txtMonth2.Prefix = "";
            this.txtMonth2.Size = new System.Drawing.Size(58, 20);
            this.txtMonth2.SkipValidation = false;
            this.txtMonth2.TabIndex = 7;
            this.txtMonth2.TextType = CrplControlLibrary.TextType.Integer;
            this.txtMonth2.Leave += new System.EventHandler(this.txtMonth2_Leave);
            this.txtMonth2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth2_Validating);
            // 
            // txtMonth1
            // 
            this.txtMonth1.AllowSpace = true;
            this.txtMonth1.AssociatedLookUpName = "";
            this.txtMonth1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMonth1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonth1.ContinuationTextBox = null;
            this.txtMonth1.CustomEnabled = true;
            this.txtMonth1.DataFieldMapping = "SA_FOR_THE_MONTH1";
            this.txtMonth1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonth1.GetRecordsOnUpDownKeys = false;
            this.txtMonth1.IsDate = false;
            this.txtMonth1.Location = new System.Drawing.Point(317, 51);
            this.txtMonth1.MaxLength = 2;
            this.txtMonth1.Name = "txtMonth1";
            this.txtMonth1.NumberFormat = "###,###,##0.00";
            this.txtMonth1.Postfix = "";
            this.txtMonth1.Prefix = "";
            this.txtMonth1.Size = new System.Drawing.Size(58, 20);
            this.txtMonth1.SkipValidation = false;
            this.txtMonth1.TabIndex = 6;
            this.txtMonth1.TextType = CrplControlLibrary.TextType.Integer;
            this.txtMonth1.Leave += new System.EventHandler(this.txtMonth1_Leave);
            this.txtMonth1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth1_Validating);
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(111, 51);
            this.txtName.MaxLength = 35;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(200, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 5;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnel
            // 
            this.txtPersonnel.AllowSpace = true;
            this.txtPersonnel.AssociatedLookUpName = "lbtn";
            this.txtPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnel.ContinuationTextBox = null;
            this.txtPersonnel.CustomEnabled = true;
            this.txtPersonnel.DataFieldMapping = "SA_P_NO";
            this.txtPersonnel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnel.GetRecordsOnUpDownKeys = false;
            this.txtPersonnel.IsDate = false;
            this.txtPersonnel.Location = new System.Drawing.Point(19, 51);
            this.txtPersonnel.MaxLength = 6;
            this.txtPersonnel.Name = "txtPersonnel";
            this.txtPersonnel.NumberFormat = "###,###,##0.00";
            this.txtPersonnel.Postfix = "";
            this.txtPersonnel.Prefix = "";
            this.txtPersonnel.Size = new System.Drawing.Size(54, 20);
            this.txtPersonnel.SkipValidation = false;
            this.txtPersonnel.TabIndex = 4;
            this.txtPersonnel.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnel.Leave += new System.EventHandler(this.txtPersonnel_Leave);
            this.txtPersonnel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersonnel_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(376, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "User Name:";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.Location = new System.Drawing.Point(459, 12);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 13;
            // 
            // CHRIS_Payroll_SalaryAdvanceEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(619, 488);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.slPanelSimple1);
            this.CurrentPanelBlock = "slPanelSimple1";
            this.CurrrentOptionTextBox = this.CurrOption;
            this.Name = "CHRIS_Payroll_SalaryAdvanceEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = " ";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Payroll_SalaryAdvanceEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlHead;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtMonth2;
        private CrplControlLibrary.SLTextBox txtMonth1;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtPersonnel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtAmount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.LookupButton lbtn;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox CurrOption;
        private CrplControlLibrary.SLDatePicker DtSdate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox txtID;
        private CrplControlLibrary.SLTextBox txtHiddenField;

    }
}