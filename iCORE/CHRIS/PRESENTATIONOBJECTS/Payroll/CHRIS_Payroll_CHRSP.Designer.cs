namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_CHRSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_Chrsp = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.slButton_Exit = new CrplControlLibrary.SLButton();
            this.slButton_Print = new CrplControlLibrary.SLButton();
            this.slButton_Generate = new CrplControlLibrary.SLButton();
            this.slButton_View = new CrplControlLibrary.SLButton();
            this.dgvView = new System.Windows.Forms.DataGridView();
            this.col_pr_new_branch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_HC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AMOUNT1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label_amt_tot = new System.Windows.Forms.Label();
            this.label_Amt1_tot = new System.Windows.Forms.Label();
            this.label_l1 = new System.Windows.Forms.Label();
            this.panel_generateFile = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.slButton_ExitFile = new CrplControlLibrary.SLButton();
            this.slButton_StartGeneration = new CrplControlLibrary.SLButton();
            this.txt_FilePath = new System.Windows.Forms.TextBox();
            this.slDatePicker_validate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel_Chrsp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel_generateFile.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(600, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(636, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 557);
            this.panel1.Size = new System.Drawing.Size(636, 60);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(251, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 39);
            this.label1.TabIndex = 8;
            this.label1.Text = "CHRSP";
            // 
            // panel_Chrsp
            // 
            this.panel_Chrsp.Controls.Add(this.dataGridView1);
            this.panel_Chrsp.Controls.Add(this.slButton_Exit);
            this.panel_Chrsp.Controls.Add(this.slButton_Print);
            this.panel_Chrsp.Controls.Add(this.slButton_Generate);
            this.panel_Chrsp.Controls.Add(this.slButton_View);
            this.panel_Chrsp.Controls.Add(this.dgvView);
            this.panel_Chrsp.Controls.Add(this.groupBox1);
            this.panel_Chrsp.Location = new System.Drawing.Point(24, 124);
            this.panel_Chrsp.Name = "panel_Chrsp";
            this.panel_Chrsp.Size = new System.Drawing.Size(588, 323);
            this.panel_Chrsp.TabIndex = 10;
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(17, 242);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(564, 13);
            this.dataGridView1.TabIndex = 18;
            this.dataGridView1.Visible = false;
            // 
            // slButton_Exit
            // 
            this.slButton_Exit.ActionType = "";
            this.slButton_Exit.Location = new System.Drawing.Point(361, 297);
            this.slButton_Exit.Name = "slButton_Exit";
            this.slButton_Exit.Size = new System.Drawing.Size(75, 23);
            this.slButton_Exit.TabIndex = 15;
            this.slButton_Exit.Text = "Exit";
            this.slButton_Exit.UseVisualStyleBackColor = true;
            this.slButton_Exit.Click += new System.EventHandler(this.slButton_Exit_Click);
            // 
            // slButton_Print
            // 
            this.slButton_Print.ActionType = "";
            this.slButton_Print.Location = new System.Drawing.Point(286, 297);
            this.slButton_Print.Name = "slButton_Print";
            this.slButton_Print.Size = new System.Drawing.Size(75, 23);
            this.slButton_Print.TabIndex = 14;
            this.slButton_Print.Text = "Print";
            this.slButton_Print.UseVisualStyleBackColor = true;
            this.slButton_Print.Click += new System.EventHandler(this.slButton_Print_Click);
            // 
            // slButton_Generate
            // 
            this.slButton_Generate.ActionType = "";
            this.slButton_Generate.Location = new System.Drawing.Point(212, 297);
            this.slButton_Generate.Name = "slButton_Generate";
            this.slButton_Generate.Size = new System.Drawing.Size(75, 23);
            this.slButton_Generate.TabIndex = 13;
            this.slButton_Generate.Text = "Generate";
            this.slButton_Generate.UseVisualStyleBackColor = true;
            this.slButton_Generate.Click += new System.EventHandler(this.slButton_Generate_Click);
            // 
            // slButton_View
            // 
            this.slButton_View.ActionType = "";
            this.slButton_View.Location = new System.Drawing.Point(138, 297);
            this.slButton_View.Name = "slButton_View";
            this.slButton_View.Size = new System.Drawing.Size(75, 23);
            this.slButton_View.TabIndex = 12;
            this.slButton_View.Text = "View";
            this.slButton_View.UseVisualStyleBackColor = true;
            this.slButton_View.Click += new System.EventHandler(this.slButton_View_Click);
            // 
            // dgvView
            // 
            this.dgvView.AllowUserToAddRows = false;
            this.dgvView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_pr_new_branch,
            this.col_HC,
            this.col_AMT,
            this.col_AMOUNT1});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvView.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvView.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvView.Location = new System.Drawing.Point(0, 0);
            this.dgvView.Name = "dgvView";
            this.dgvView.ReadOnly = true;
            this.dgvView.Size = new System.Drawing.Size(588, 238);
            this.dgvView.TabIndex = 11;
            // 
            // col_pr_new_branch
            // 
            this.col_pr_new_branch.DataPropertyName = "pr_new_branch";
            this.col_pr_new_branch.HeaderText = "Branch";
            this.col_pr_new_branch.Name = "col_pr_new_branch";
            this.col_pr_new_branch.ReadOnly = true;
            this.col_pr_new_branch.Width = 130;
            // 
            // col_HC
            // 
            this.col_HC.DataPropertyName = "HC";
            this.col_HC.HeaderText = "Head Count";
            this.col_HC.Name = "col_HC";
            this.col_HC.ReadOnly = true;
            this.col_HC.Width = 130;
            // 
            // col_AMT
            // 
            this.col_AMT.DataPropertyName = "AMT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_AMT.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_AMT.HeaderText = "Amount";
            this.col_AMT.Name = "col_AMT";
            this.col_AMT.ReadOnly = true;
            this.col_AMT.Width = 130;
            // 
            // col_AMOUNT1
            // 
            this.col_AMOUNT1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_AMOUNT1.DataPropertyName = "AMOUNT1";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_AMOUNT1.DefaultCellStyle = dataGridViewCellStyle5;
            this.col_AMOUNT1.HeaderText = "Amount in CHRSP";
            this.col_AMOUNT1.Name = "col_AMOUNT1";
            this.col_AMOUNT1.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label_amt_tot);
            this.groupBox1.Controls.Add(this.label_Amt1_tot);
            this.groupBox1.Location = new System.Drawing.Point(0, 253);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(588, 38);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(6, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Total    :";
            // 
            // label_amt_tot
            // 
            this.label_amt_tot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_amt_tot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_amt_tot.Location = new System.Drawing.Point(308, 14);
            this.label_amt_tot.Name = "label_amt_tot";
            this.label_amt_tot.Size = new System.Drawing.Size(112, 13);
            this.label_amt_tot.TabIndex = 6;
            this.label_amt_tot.Text = "000000000000000";
            this.label_amt_tot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_Amt1_tot
            // 
            this.label_Amt1_tot.AutoSize = true;
            this.label_Amt1_tot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Amt1_tot.Location = new System.Drawing.Point(448, 14);
            this.label_Amt1_tot.Name = "label_Amt1_tot";
            this.label_Amt1_tot.Size = new System.Drawing.Size(133, 13);
            this.label_Amt1_tot.TabIndex = 7;
            this.label_Amt1_tot.Text = "000000000000000000";
            // 
            // label_l1
            // 
            this.label_l1.AutoSize = true;
            this.label_l1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_l1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label_l1.Location = new System.Drawing.Point(12, 106);
            this.label_l1.Name = "label_l1";
            this.label_l1.Size = new System.Drawing.Size(78, 15);
            this.label_l1.TabIndex = 17;
            this.label_l1.Text = "HPAYROLL";
            // 
            // panel_generateFile
            // 
            this.panel_generateFile.Controls.Add(this.groupBox2);
            this.panel_generateFile.Location = new System.Drawing.Point(64, 465);
            this.panel_generateFile.Name = "panel_generateFile";
            this.panel_generateFile.Size = new System.Drawing.Size(508, 117);
            this.panel_generateFile.TabIndex = 11;
            this.panel_generateFile.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.slButton_ExitFile);
            this.groupBox2.Controls.Add(this.slButton_StartGeneration);
            this.groupBox2.Controls.Add(this.txt_FilePath);
            this.groupBox2.Controls.Add(this.slDatePicker_validate);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(20, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(478, 100);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // slButton_ExitFile
            // 
            this.slButton_ExitFile.ActionType = "";
            this.slButton_ExitFile.Location = new System.Drawing.Point(322, 71);
            this.slButton_ExitFile.Name = "slButton_ExitFile";
            this.slButton_ExitFile.Size = new System.Drawing.Size(90, 23);
            this.slButton_ExitFile.TabIndex = 17;
            this.slButton_ExitFile.Text = "Exit";
            this.slButton_ExitFile.UseVisualStyleBackColor = true;
            this.slButton_ExitFile.Click += new System.EventHandler(this.slButton_ExitFile_Click);
            // 
            // slButton_StartGeneration
            // 
            this.slButton_StartGeneration.ActionType = "";
            this.slButton_StartGeneration.Location = new System.Drawing.Point(226, 71);
            this.slButton_StartGeneration.Name = "slButton_StartGeneration";
            this.slButton_StartGeneration.Size = new System.Drawing.Size(90, 23);
            this.slButton_StartGeneration.TabIndex = 16;
            this.slButton_StartGeneration.Text = "Start";
            this.slButton_StartGeneration.UseVisualStyleBackColor = true;
            this.slButton_StartGeneration.Click += new System.EventHandler(this.slButton_StartGeneration_Click);
            // 
            // txt_FilePath
            // 
            this.txt_FilePath.Location = new System.Drawing.Point(226, 47);
            this.txt_FilePath.Name = "txt_FilePath";
            this.txt_FilePath.Size = new System.Drawing.Size(224, 20);
            this.txt_FilePath.TabIndex = 9;
            // 
            // slDatePicker_validate
            // 
            this.slDatePicker_validate.CustomEnabled = true;
            this.slDatePicker_validate.CustomFormat = "yyyy/MM/dd";
            this.slDatePicker_validate.DataFieldMapping = "";
            this.slDatePicker_validate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_validate.HasChanges = true;
            this.slDatePicker_validate.Location = new System.Drawing.Point(226, 20);
            this.slDatePicker_validate.Name = "slDatePicker_validate";
            this.slDatePicker_validate.NullValue = " ";
            this.slDatePicker_validate.Size = new System.Drawing.Size(90, 20);
            this.slDatePicker_validate.TabIndex = 8;
            this.slDatePicker_validate.Value = new System.DateTime(2011, 1, 28, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(13, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Enter File Name With Path";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(13, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Enter Value Date (YYYYMMDD)";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(403, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 28;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Payroll_CHRSP
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(636, 617);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.panel_generateFile);
            this.Controls.Add(this.label_l1);
            this.Controls.Add(this.panel_Chrsp);
            this.Controls.Add(this.label1);
            this.CurrentPanelBlock = "";
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CHRIS_Payroll_CHRSP";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Payroll_CHRSP";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.panel_Chrsp, 0);
            this.Controls.SetChildIndex(this.label_l1, 0);
            this.Controls.SetChildIndex(this.panel_generateFile, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel_Chrsp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel_generateFile.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel_Chrsp;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label_l1;
        private CrplControlLibrary.SLButton slButton_Exit;
        private CrplControlLibrary.SLButton slButton_Print;
        private CrplControlLibrary.SLButton slButton_Generate;
        private CrplControlLibrary.SLButton slButton_View;
        private System.Windows.Forms.DataGridView dgvView;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_pr_new_branch;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_HC;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AMOUNT1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_amt_tot;
        private System.Windows.Forms.Label label_Amt1_tot;
        private System.Windows.Forms.Panel panel_generateFile;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLDatePicker slDatePicker_validate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_FilePath;
        private CrplControlLibrary.SLButton slButton_ExitFile;
        private CrplControlLibrary.SLButton slButton_StartGeneration;
        private System.Windows.Forms.Label lblUserName;
        //private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvView;
    }
}