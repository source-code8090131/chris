using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_ArearUpdate : TabularForm
    {
        CHRIS_Payroll_PR111 ObjBasecopy = null;

        #region Constructor

        public CHRIS_Payroll_ArearUpdate()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_ArearUpdate(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Payroll_PR111 menuBaseForm)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            ObjBasecopy = menuBaseForm;
            ObjBasecopy.Hide();

        }

        #endregion

        #region Events

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
           // txtOption.Visible = false;
            this.dgvSalArear.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            dgvSalArear.Columns["col_SA_DATE"].DefaultCellStyle.Format = "dd/MM/yyyy";
            dgvSalArear.Columns["col_SA_DATE"].DefaultCellStyle.NullValue = "";

        }

        protected override void TabularForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
            {
                if (dgvSalArear.CurrentCell != null)
                    if (dgvSalArear.CurrentCell.IsInEditMode)
                    {
                        dgvSalArear.CancelEdit();
                    }
                this.Close();
                //this.DoToolbarActions(this.Controls, "Close");
            }
            else
                base.TabularForm_KeyUp(sender, e);

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            ObjBasecopy.Show();
            base.OnFormClosing(e);
        }


        #endregion

        #region Commented
        //private void CHRIS_Payroll_ArearUpdate_KeyUp(object sender, KeyEventArgs e)
        //{
            
        //    if (e.KeyCode == Keys.F6)
        //    {
        //        if (dgvSalArear.CurrentCell != null)
        //            if (dgvSalArear.CurrentCell.IsInEditMode)
        //            {
        //                dgvSalArear.CancelEdit();
        //            }
        //        this.DoToolbarActions(this.Controls, "Close");
        //    }
        //    else
        //        base.OnFormKeyup(sender, e);

        //}

        //private void dgvSalArear_KeyUp(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.F6)
        //    {
        //        if (dgvSalArear.CurrentCell != null)
        //            if (dgvSalArear.CurrentCell.IsInEditMode)
        //            {
        //                dgvSalArear.CancelEdit();
        //            }
        //        this.DoToolbarActions(this.Controls, "Close");
        //    }
        //    else
        //        base.OnFormKeyup(sender, e);

        //}
        #endregion


    }
}