namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_ShiftEntry_GridForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_ShiftEntry_GridForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.slPanelShift1 = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvShiftOT1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.F_DATE1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_MON1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_TIME_IN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_TIME_OUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_FRI_HOL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F_SHIFT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SH_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_SINGLE_HR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_DOUBLE_HR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_MEAL_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_MEAL_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_CONV_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OT_CONV_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slPanelPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtp_Ot_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_Pr_P_No = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelShift1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShiftOT1)).BeginInit();
            this.slPanelPersonnel.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanelShift1
            // 
            this.slPanelShift1.ConcurrentPanels = null;
            this.slPanelShift1.Controls.Add(this.dgvShiftOT1);
            this.slPanelShift1.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelShift1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelShift1.DependentPanels = null;
            this.slPanelShift1.DisableDependentLoad = false;
            this.slPanelShift1.EnableDelete = false;
            this.slPanelShift1.EnableInsert = true;
            this.slPanelShift1.EnableQuery = false;
            this.slPanelShift1.EnableUpdate = true;
            this.slPanelShift1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OverTimeCommand";
            this.slPanelShift1.Location = new System.Drawing.Point(45, 37);
            this.slPanelShift1.MasterPanel = null;
            this.slPanelShift1.Name = "slPanelShift1";
            this.slPanelShift1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelShift1.Size = new System.Drawing.Size(510, 592);
            this.slPanelShift1.SPName = "CHRIS_SP_SHIFT_OVERTIME_MANAGER";
            this.slPanelShift1.TabIndex = 10;
            // 
            // dgvShiftOT1
            // 
            this.dgvShiftOT1.AllowUserToAddRows = false;
            this.dgvShiftOT1.AllowUserToDeleteRows = false;
            this.dgvShiftOT1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShiftOT1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShiftOT1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShiftOT1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F_DATE1,
            this.F_MON1,
            this.OT_TIME_IN,
            this.OT_TIME_OUT,
            this.OT_FRI_HOL,
            this.F_SHIFT,
            this.SH_AMOUNT,
            this.OT_SINGLE_HR,
            this.OT_DOUBLE_HR,
            this.OT_MEAL_CODE,
            this.OT_MEAL_AMT,
            this.OT_CONV_CODE,
            this.OT_CONV_AMT});
            this.dgvShiftOT1.ColumnToHide = null;
            this.dgvShiftOT1.ColumnWidth = null;
            this.dgvShiftOT1.CustomEnabled = true;
            this.dgvShiftOT1.DisplayColumnWrapper = null;
            this.dgvShiftOT1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvShiftOT1.GridDefaultRow = 0;
            this.dgvShiftOT1.Location = new System.Drawing.Point(0, 0);
            this.dgvShiftOT1.Name = "dgvShiftOT1";
            this.dgvShiftOT1.ReadOnlyColumns = "F_DATE1,F_MON1";
            this.dgvShiftOT1.RequiredColumns = null;
            this.dgvShiftOT1.RowHeadersVisible = false;
            this.dgvShiftOT1.Size = new System.Drawing.Size(510, 592);
            this.dgvShiftOT1.SkippingColumns = "F_DATE1,F_MON1";
            this.dgvShiftOT1.TabIndex = 0;
            this.dgvShiftOT1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvShiftOT1_CellValidating);
            this.dgvShiftOT1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvShiftOT1_EditingControlShowing);
            // 
            // F_DATE1
            // 
            this.F_DATE1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.F_DATE1.DataPropertyName = "OT_DATE";
            this.F_DATE1.HeaderText = "Date";
            this.F_DATE1.Name = "F_DATE1";
            this.F_DATE1.ReadOnly = true;
            // 
            // F_MON1
            // 
            this.F_MON1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.F_MON1.DataPropertyName = "DAYFIELD";
            this.F_MON1.HeaderText = "Day";
            this.F_MON1.Name = "F_MON1";
            this.F_MON1.ReadOnly = true;
            // 
            // OT_TIME_IN
            // 
            this.OT_TIME_IN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_TIME_IN.DataPropertyName = "OT_TIME_IN";
            this.OT_TIME_IN.HeaderText = "Time In";
            this.OT_TIME_IN.MaxInputLength = 5;
            this.OT_TIME_IN.Name = "OT_TIME_IN";
            // 
            // OT_TIME_OUT
            // 
            this.OT_TIME_OUT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_TIME_OUT.DataPropertyName = "OT_TIME_OUT";
            this.OT_TIME_OUT.HeaderText = "Time Out";
            this.OT_TIME_OUT.MaxInputLength = 5;
            this.OT_TIME_OUT.Name = "OT_TIME_OUT";
            this.OT_TIME_OUT.ToolTipText = "Enter time out.";
            // 
            // OT_FRI_HOL
            // 
            this.OT_FRI_HOL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_FRI_HOL.DataPropertyName = "OT_FRI_HOL";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "N";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OT_FRI_HOL.DefaultCellStyle = dataGridViewCellStyle2;
            this.OT_FRI_HOL.HeaderText = "Holiday";
            this.OT_FRI_HOL.MaxInputLength = 1;
            this.OT_FRI_HOL.Name = "OT_FRI_HOL";
            this.OT_FRI_HOL.ToolTipText = "ENTER [H] FOR HOLIDAY [N] FOR WORKING DAY";
            // 
            // F_SHIFT
            // 
            this.F_SHIFT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.F_SHIFT.DataPropertyName = "SH_SHIFT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.F_SHIFT.DefaultCellStyle = dataGridViewCellStyle3;
            this.F_SHIFT.HeaderText = "Shift";
            this.F_SHIFT.MaxInputLength = 1;
            this.F_SHIFT.Name = "F_SHIFT";
            this.F_SHIFT.ToolTipText = "Valid Shifts Are [A],[B],[C],[D] or Blank For No Shift";
            // 
            // SH_AMOUNT
            // 
            this.SH_AMOUNT.DataPropertyName = "SH_AMOUNT";
            this.SH_AMOUNT.HeaderText = "SH AMOUNT";
            this.SH_AMOUNT.Name = "SH_AMOUNT";
            this.SH_AMOUNT.Visible = false;
            // 
            // OT_SINGLE_HR
            // 
            this.OT_SINGLE_HR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_SINGLE_HR.DataPropertyName = "OT_SINGLE_HR";
            this.OT_SINGLE_HR.HeaderText = "OT_SINGLE_HR";
            this.OT_SINGLE_HR.Name = "OT_SINGLE_HR";
            this.OT_SINGLE_HR.Visible = false;
            // 
            // OT_DOUBLE_HR
            // 
            this.OT_DOUBLE_HR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_DOUBLE_HR.DataPropertyName = "OT_DOUBLE_HR";
            this.OT_DOUBLE_HR.HeaderText = "OT_DOUBLE_HR";
            this.OT_DOUBLE_HR.Name = "OT_DOUBLE_HR";
            this.OT_DOUBLE_HR.Visible = false;
            // 
            // OT_MEAL_CODE
            // 
            this.OT_MEAL_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_MEAL_CODE.DataPropertyName = "OT_MEAL_CODE";
            this.OT_MEAL_CODE.HeaderText = "OT_MEAL_CODE";
            this.OT_MEAL_CODE.Name = "OT_MEAL_CODE";
            this.OT_MEAL_CODE.Visible = false;
            // 
            // OT_MEAL_AMT
            // 
            this.OT_MEAL_AMT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_MEAL_AMT.DataPropertyName = "OT_MEAL_AMT";
            this.OT_MEAL_AMT.HeaderText = "OT_MEAL_AMT";
            this.OT_MEAL_AMT.Name = "OT_MEAL_AMT";
            this.OT_MEAL_AMT.Visible = false;
            // 
            // OT_CONV_CODE
            // 
            this.OT_CONV_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_CONV_CODE.DataPropertyName = "OT_CONV_CODE";
            this.OT_CONV_CODE.HeaderText = "OT_CONV_CODE";
            this.OT_CONV_CODE.Name = "OT_CONV_CODE";
            this.OT_CONV_CODE.Visible = false;
            // 
            // OT_CONV_AMT
            // 
            this.OT_CONV_AMT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OT_CONV_AMT.DataPropertyName = "OT_CONV_AMT";
            this.OT_CONV_AMT.HeaderText = "OT_CONV_AMT";
            this.OT_CONV_AMT.Name = "OT_CONV_AMT";
            this.OT_CONV_AMT.Visible = false;
            // 
            // slPanelPersonnel
            // 
            this.slPanelPersonnel.ConcurrentPanels = null;
            this.slPanelPersonnel.Controls.Add(this.dtp_Ot_date);
            this.slPanelPersonnel.Controls.Add(this.txt_Pr_P_No);
            this.slPanelPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelPersonnel.DependentPanels = null;
            this.slPanelPersonnel.DisableDependentLoad = false;
            this.slPanelPersonnel.EnableDelete = false;
            this.slPanelPersonnel.EnableInsert = true;
            this.slPanelPersonnel.EnableQuery = false;
            this.slPanelPersonnel.EnableUpdate = true;
            this.slPanelPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand_ShiftEntry";
            this.slPanelPersonnel.Location = new System.Drawing.Point(44, 636);
            this.slPanelPersonnel.MasterPanel = null;
            this.slPanelPersonnel.Name = "slPanelPersonnel";
            this.slPanelPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelPersonnel.Size = new System.Drawing.Size(509, 39);
            this.slPanelPersonnel.SPName = "CHRIS_SP_PERSONNEL_MANAGER_SHIFTENTRY";
            this.slPanelPersonnel.TabIndex = 11;
            // 
            // dtp_Ot_date
            // 
            this.dtp_Ot_date.CustomEnabled = true;
            this.dtp_Ot_date.DataFieldMapping = "OT_DATE_FILTER";
            this.dtp_Ot_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_Ot_date.HasChanges = true;
            this.dtp_Ot_date.Location = new System.Drawing.Point(84, 3);
            this.dtp_Ot_date.Name = "dtp_Ot_date";
            this.dtp_Ot_date.NullValue = " ";
            this.dtp_Ot_date.Size = new System.Drawing.Size(200, 20);
            this.dtp_Ot_date.TabIndex = 5;
            this.dtp_Ot_date.Value = new System.DateTime(2011, 5, 6, 0, 0, 0, 0);
            // 
            // txt_Pr_P_No
            // 
            this.txt_Pr_P_No.AllowSpace = true;
            this.txt_Pr_P_No.AssociatedLookUpName = "";
            this.txt_Pr_P_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pr_P_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pr_P_No.ContinuationTextBox = null;
            this.txt_Pr_P_No.CustomEnabled = true;
            this.txt_Pr_P_No.DataFieldMapping = "OT_P_NO";
            this.txt_Pr_P_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pr_P_No.GetRecordsOnUpDownKeys = false;
            this.txt_Pr_P_No.IsDate = false;
            this.txt_Pr_P_No.IsLookUpField = true;
            this.txt_Pr_P_No.IsRequired = true;
            this.txt_Pr_P_No.Location = new System.Drawing.Point(3, 3);
            this.txt_Pr_P_No.MaxLength = 6;
            this.txt_Pr_P_No.Name = "txt_Pr_P_No";
            this.txt_Pr_P_No.NumberFormat = "###,###,##0.00";
            this.txt_Pr_P_No.Postfix = "";
            this.txt_Pr_P_No.Prefix = "";
            this.txt_Pr_P_No.Size = new System.Drawing.Size(75, 20);
            this.txt_Pr_P_No.SkipValidation = false;
            this.txt_Pr_P_No.TabIndex = 4;
            this.txt_Pr_P_No.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // CHRIS_Payroll_ShiftEntry_GridForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 633);
            this.Controls.Add(this.slPanelPersonnel);
            this.Controls.Add(this.slPanelShift1);
            this.CurrentPanelBlock = "slPanelPersonnel";
            this.Name = "CHRIS_Payroll_ShiftEntry_GridForm";
            this.Text = "iCORE CHRIS :  Shift Entry - OverTime";
            this.Controls.SetChildIndex(this.slPanelShift1, 0);
            this.Controls.SetChildIndex(this.slPanelPersonnel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelShift1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShiftOT1)).EndInit();
            this.slPanelPersonnel.ResumeLayout(false);
            this.slPanelPersonnel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelShift1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvShiftOT1;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelPersonnel;
        private CrplControlLibrary.SLDatePicker dtp_Ot_date;
        private CrplControlLibrary.SLTextBox txt_Pr_P_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_DATE1;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_MON1;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_TIME_IN;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_TIME_OUT;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_FRI_HOL;
        private System.Windows.Forms.DataGridViewTextBoxColumn F_SHIFT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SH_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_SINGLE_HR;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_DOUBLE_HR;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_MEAL_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_MEAL_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_CONV_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OT_CONV_AMT;
    }
}