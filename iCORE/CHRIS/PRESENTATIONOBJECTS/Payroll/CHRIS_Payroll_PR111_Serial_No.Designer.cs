namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_PR111_Serial_No
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slPanelSerial = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_SerialNum = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.slPanelSerial.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slPanelSerial);
            this.groupBox1.Location = new System.Drawing.Point(12, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 98);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee Serial Number";
            // 
            // slPanelSerial
            // 
            this.slPanelSerial.ConcurrentPanels = null;
            this.slPanelSerial.Controls.Add(this.txt_SerialNum);
            this.slPanelSerial.Controls.Add(this.label1);
            this.slPanelSerial.Controls.Add(this.label2);
            this.slPanelSerial.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSerial.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSerial.DependentPanels = null;
            this.slPanelSerial.DisableDependentLoad = false;
            this.slPanelSerial.EnableDelete = true;
            this.slPanelSerial.EnableInsert = true;
            this.slPanelSerial.EnableUpdate = true;
            this.slPanelSerial.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SerialNumCommand";
            this.slPanelSerial.Location = new System.Drawing.Point(7, 19);
            this.slPanelSerial.MasterPanel = null;
            this.slPanelSerial.Name = "slPanelSerial";
            this.slPanelSerial.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSerial.Size = new System.Drawing.Size(369, 72);
            this.slPanelSerial.SPName = "CHRIS_SP_SERIAL_NO_MANAGER";
            this.slPanelSerial.TabIndex = 2;
            // 
            // txt_SerialNum
            // 
            this.txt_SerialNum.AllowSpace = true;
            this.txt_SerialNum.AssociatedLookUpName = "";
            this.txt_SerialNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SerialNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SerialNum.ContinuationTextBox = null;
            this.txt_SerialNum.CustomEnabled = true;
            this.txt_SerialNum.DataFieldMapping = "PR_SNO";
            this.txt_SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SerialNum.GetRecordsOnUpDownKeys = false;
            this.txt_SerialNum.IsDate = false;
            this.txt_SerialNum.Location = new System.Drawing.Point(209, 5);
            this.txt_SerialNum.MaxLength = 6;
            this.txt_SerialNum.Name = "txt_SerialNum";
            this.txt_SerialNum.NumberFormat = "###,###,##0.00";
            this.txt_SerialNum.Postfix = "";
            this.txt_SerialNum.Prefix = "";
            this.txt_SerialNum.Size = new System.Drawing.Size(57, 20);
            this.txt_SerialNum.SkipValidation = false;
            this.txt_SerialNum.TabIndex = 2;
            this.txt_SerialNum.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Last Generated Employee Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(257, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Press F10 to save";
            // 
            // CHRIS_Payroll_PR111_Serial_No
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 184);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_Payroll_PR111_Serial_No";
            this.Text = "CHRIS_Payroll_PR111_Serial_No";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.slPanelSerial.ResumeLayout(false);
            this.slPanelSerial.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSerial;
        private CrplControlLibrary.SLTextBox txt_SerialNum;
    }
}