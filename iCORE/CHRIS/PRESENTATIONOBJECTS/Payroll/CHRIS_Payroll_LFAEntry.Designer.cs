namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_LFAEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_LFAEntry));
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slPnlLeaveFairAssistance = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtLeaveBalanceHidden = new CrplControlLibrary.SLTextBox(this.components);
            this.lkbPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.txtOptionView = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDateofGeneration = new CrplControlLibrary.SLTextBox(this.components);
            this.txtApproved = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLFAamount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLeaveBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.btnView = new CrplControlLibrary.SLButton();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlLeaveFairAssistance.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(598, 0);
            this.txtOption.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(634, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 491);
            this.panel1.Size = new System.Drawing.Size(634, 60);
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Enabled = false;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(80, 60);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(87, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 17;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Enabled = false;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(80, 37);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(87, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 100;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(467, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(240, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "LEAVE FAIR ASSISTANCE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(258, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "PAYROLL SYSTEM";
            // 
            // slPnlLeaveFairAssistance
            // 
            this.slPnlLeaveFairAssistance.ConcurrentPanels = null;
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtLeaveBalanceHidden);
            this.slPnlLeaveFairAssistance.Controls.Add(this.lkbPersonnel);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtOptionView);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label17);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label16);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtDateofGeneration);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtApproved);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtLFAamount);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtLeaveBalance);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtPersonnelName);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtPersonnelNo);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label15);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label14);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label13);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label12);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label11);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label10);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label9);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label8);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label6);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label5);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label4);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label3);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtDate);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label1);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtLocation);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label2);
            this.slPnlLeaveFairAssistance.Controls.Add(this.txtUser);
            this.slPnlLeaveFairAssistance.Controls.Add(this.label7);
            this.slPnlLeaveFairAssistance.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPnlLeaveFairAssistance.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlLeaveFairAssistance.DependentPanels = null;
            this.slPnlLeaveFairAssistance.DisableDependentLoad = false;
            this.slPnlLeaveFairAssistance.EnableDelete = true;
            this.slPnlLeaveFairAssistance.EnableInsert = true;
            this.slPnlLeaveFairAssistance.EnableQuery = false;
            this.slPnlLeaveFairAssistance.EnableUpdate = true;
            this.slPnlLeaveFairAssistance.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LFA_TABCommand";
            this.slPnlLeaveFairAssistance.Location = new System.Drawing.Point(14, 101);
            this.slPnlLeaveFairAssistance.MasterPanel = null;
            this.slPnlLeaveFairAssistance.Name = "slPnlLeaveFairAssistance";
            this.slPnlLeaveFairAssistance.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlLeaveFairAssistance.Size = new System.Drawing.Size(610, 354);
            this.slPnlLeaveFairAssistance.SPName = "CHRIS_SP_LFA_TAB_MANAGER";
            this.slPnlLeaveFairAssistance.TabIndex = 18;
            // 
            // txtLeaveBalanceHidden
            // 
            this.txtLeaveBalanceHidden.AllowSpace = true;
            this.txtLeaveBalanceHidden.AssociatedLookUpName = "";
            this.txtLeaveBalanceHidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLeaveBalanceHidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLeaveBalanceHidden.ContinuationTextBox = null;
            this.txtLeaveBalanceHidden.CustomEnabled = true;
            this.txtLeaveBalanceHidden.DataFieldMapping = "LF_LEAV_BAL";
            this.txtLeaveBalanceHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeaveBalanceHidden.GetRecordsOnUpDownKeys = false;
            this.txtLeaveBalanceHidden.IsDate = false;
            this.txtLeaveBalanceHidden.Location = new System.Drawing.Point(367, 228);
            this.txtLeaveBalanceHidden.MaxLength = 4;
            this.txtLeaveBalanceHidden.Name = "txtLeaveBalanceHidden";
            this.txtLeaveBalanceHidden.NumberFormat = "###,###,##0.00";
            this.txtLeaveBalanceHidden.Postfix = "";
            this.txtLeaveBalanceHidden.Prefix = "";
            this.txtLeaveBalanceHidden.Size = new System.Drawing.Size(70, 20);
            this.txtLeaveBalanceHidden.SkipValidation = false;
            this.txtLeaveBalanceHidden.TabIndex = 102;
            this.txtLeaveBalanceHidden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeaveBalanceHidden.TextType = CrplControlLibrary.TextType.Integer;
            this.txtLeaveBalanceHidden.Visible = false;
            // 
            // lkbPersonnel
            // 
            this.lkbPersonnel.ActionLOVExists = "PersonnelLovExist";
            this.lkbPersonnel.ActionType = "PersonnelLov";
            this.lkbPersonnel.ConditionalFields = "";
            this.lkbPersonnel.CustomEnabled = true;
            this.lkbPersonnel.DataFieldMapping = "";
            this.lkbPersonnel.DependentLovControls = "";
            this.lkbPersonnel.HiddenColumns = "";
            this.lkbPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lkbPersonnel.Image")));
            this.lkbPersonnel.LoadDependentEntities = false;
            this.lkbPersonnel.Location = new System.Drawing.Point(72, 202);
            this.lkbPersonnel.LookUpTitle = null;
            this.lkbPersonnel.Name = "lkbPersonnel";
            this.lkbPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lkbPersonnel.SkipValidationOnLeave = false;
            this.lkbPersonnel.SPName = "CHRIS_SP_LFA_TAB_MANAGER";
            this.lkbPersonnel.TabIndex = 101;
            this.lkbPersonnel.TabStop = false;
            this.lkbPersonnel.UseVisualStyleBackColor = true;
            // 
            // txtOptionView
            // 
            this.txtOptionView.AllowSpace = true;
            this.txtOptionView.AssociatedLookUpName = "";
            this.txtOptionView.BackColor = System.Drawing.SystemColors.Window;
            this.txtOptionView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOptionView.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOptionView.ContinuationTextBox = null;
            this.txtOptionView.CustomEnabled = true;
            this.txtOptionView.DataFieldMapping = "";
            this.txtOptionView.Enabled = false;
            this.txtOptionView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOptionView.GetRecordsOnUpDownKeys = false;
            this.txtOptionView.IsDate = false;
            this.txtOptionView.Location = new System.Drawing.Point(512, 29);
            this.txtOptionView.MaxLength = 10;
            this.txtOptionView.Name = "txtOptionView";
            this.txtOptionView.NumberFormat = "###,###,##0.00";
            this.txtOptionView.Postfix = "";
            this.txtOptionView.Prefix = "";
            this.txtOptionView.ReadOnly = true;
            this.txtOptionView.Size = new System.Drawing.Size(46, 20);
            this.txtOptionView.SkipValidation = false;
            this.txtOptionView.TabIndex = 42;
            this.txtOptionView.TabStop = false;
            this.txtOptionView.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(459, 313);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Generation";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(465, 300);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 38;
            this.label16.Text = "Date of";
            // 
            // txtDateofGeneration
            // 
            this.txtDateofGeneration.AllowSpace = true;
            this.txtDateofGeneration.AssociatedLookUpName = "";
            this.txtDateofGeneration.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateofGeneration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDateofGeneration.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateofGeneration.ContinuationTextBox = null;
            this.txtDateofGeneration.CustomEnabled = true;
            this.txtDateofGeneration.DataFieldMapping = "LF_DATE";
            this.txtDateofGeneration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateofGeneration.GetRecordsOnUpDownKeys = false;
            this.txtDateofGeneration.IsDate = false;
            this.txtDateofGeneration.Location = new System.Drawing.Point(457, 329);
            this.txtDateofGeneration.MaxLength = 10;
            this.txtDateofGeneration.Name = "txtDateofGeneration";
            this.txtDateofGeneration.NumberFormat = "###,###,##0.00";
            this.txtDateofGeneration.Postfix = "";
            this.txtDateofGeneration.Prefix = "";
            this.txtDateofGeneration.ReadOnly = true;
            this.txtDateofGeneration.Size = new System.Drawing.Size(71, 20);
            this.txtDateofGeneration.SkipValidation = false;
            this.txtDateofGeneration.TabIndex = 36;
            this.txtDateofGeneration.TabStop = false;
            this.txtDateofGeneration.TextType = CrplControlLibrary.TextType.String;
            this.txtDateofGeneration.TextChanged += new System.EventHandler(this.txtDateofGeneration_TextChanged);
            // 
            // txtApproved
            // 
            this.txtApproved.AllowSpace = true;
            this.txtApproved.AssociatedLookUpName = "";
            this.txtApproved.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApproved.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApproved.ContinuationTextBox = null;
            this.txtApproved.CustomEnabled = true;
            this.txtApproved.DataFieldMapping = "LF_APPROVED";
            this.txtApproved.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApproved.GetRecordsOnUpDownKeys = false;
            this.txtApproved.IsDate = false;
            this.txtApproved.Location = new System.Drawing.Point(540, 202);
            this.txtApproved.MaxLength = 1;
            this.txtApproved.Name = "txtApproved";
            this.txtApproved.NumberFormat = "###,###,##0.00";
            this.txtApproved.Postfix = "";
            this.txtApproved.Prefix = "";
            this.txtApproved.Size = new System.Drawing.Size(31, 20);
            this.txtApproved.SkipValidation = false;
            this.txtApproved.TabIndex = 5;
            this.txtApproved.TextType = CrplControlLibrary.TextType.String;
            this.txtApproved.TextChanged += new System.EventHandler(this.txtApproved_TextChanged);
            this.txtApproved.Leave += new System.EventHandler(this.txtApproved_Leave);
            // 
            // txtLFAamount
            // 
            this.txtLFAamount.AllowSpace = true;
            this.txtLFAamount.AssociatedLookUpName = "";
            this.txtLFAamount.BackColor = System.Drawing.SystemColors.Window;
            this.txtLFAamount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLFAamount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLFAamount.ContinuationTextBox = null;
            this.txtLFAamount.CustomEnabled = true;
            this.txtLFAamount.DataFieldMapping = "LF_LFA_AMOUNT";
            this.txtLFAamount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLFAamount.GetRecordsOnUpDownKeys = false;
            this.txtLFAamount.IsDate = false;
            this.txtLFAamount.Location = new System.Drawing.Point(443, 202);
            this.txtLFAamount.MaxLength = 6;
            this.txtLFAamount.Name = "txtLFAamount";
            this.txtLFAamount.NumberFormat = "###,###,##";
            this.txtLFAamount.Postfix = "";
            this.txtLFAamount.Prefix = "";
            this.txtLFAamount.Size = new System.Drawing.Size(91, 20);
            this.txtLFAamount.SkipValidation = false;
            this.txtLFAamount.TabIndex = 4;
            this.txtLFAamount.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLeaveBalance
            // 
            this.txtLeaveBalance.AllowSpace = true;
            this.txtLeaveBalance.AssociatedLookUpName = "";
            this.txtLeaveBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLeaveBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLeaveBalance.ContinuationTextBox = null;
            this.txtLeaveBalance.CustomEnabled = true;
            this.txtLeaveBalance.DataFieldMapping = "LF_LEAV_BAL";
            this.txtLeaveBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeaveBalance.GetRecordsOnUpDownKeys = false;
            this.txtLeaveBalance.IsDate = false;
            this.txtLeaveBalance.Location = new System.Drawing.Point(367, 202);
            this.txtLeaveBalance.MaxLength = 4;
            this.txtLeaveBalance.Name = "txtLeaveBalance";
            this.txtLeaveBalance.NumberFormat = "###,###,##0.00";
            this.txtLeaveBalance.Postfix = "";
            this.txtLeaveBalance.Prefix = "";
            this.txtLeaveBalance.Size = new System.Drawing.Size(70, 20);
            this.txtLeaveBalance.SkipValidation = false;
            this.txtLeaveBalance.TabIndex = 3;
            this.txtLeaveBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeaveBalance.TextType = CrplControlLibrary.TextType.Double;
            this.txtLeaveBalance.TextChanged += new System.EventHandler(this.txtLeaveBalance_TextChanged);
            this.txtLeaveBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLeaveBalance_KeyDown);
            this.txtLeaveBalance.Leave += new System.EventHandler(this.txtLeaveBalance_Leave);
            this.txtLeaveBalance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLeaveBalance_KeyPress);
            this.txtLeaveBalance.Validating += new System.ComponentModel.CancelEventHandler(this.txtLeaveBalance_Validating);
            // 
            // txtPersonnelName
            // 
            this.txtPersonnelName.AllowSpace = true;
            this.txtPersonnelName.AssociatedLookUpName = "";
            this.txtPersonnelName.BackColor = System.Drawing.SystemColors.Window;
            this.txtPersonnelName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelName.ContinuationTextBox = null;
            this.txtPersonnelName.CustomEnabled = true;
            this.txtPersonnelName.DataFieldMapping = "PR_NAME";
            this.txtPersonnelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelName.IsDate = false;
            this.txtPersonnelName.Location = new System.Drawing.Point(104, 202);
            this.txtPersonnelName.MaxLength = 40;
            this.txtPersonnelName.Name = "txtPersonnelName";
            this.txtPersonnelName.NumberFormat = "###,###,##0.00";
            this.txtPersonnelName.Postfix = "";
            this.txtPersonnelName.Prefix = "";
            this.txtPersonnelName.ReadOnly = true;
            this.txtPersonnelName.Size = new System.Drawing.Size(257, 20);
            this.txtPersonnelName.SkipValidation = false;
            this.txtPersonnelName.TabIndex = 2;
            this.txtPersonnelName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "lkbPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "LF_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.Location = new System.Drawing.Point(20, 202);
            this.txtPersonnelNo.MaxLength = 6;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(49, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 1;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPersonnelNo_KeyDown);
            this.txtPersonnelNo.Leave += new System.EventHandler(this.txtPersonnelNo_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(379, 156);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Leave";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(114, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Name";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(30, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "No";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 156);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Personnel ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(468, 156);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "LFA";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(535, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "[Y/N]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(526, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Approved";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(373, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Balance";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(459, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Amount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(457, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Option :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "User :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Location :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(512, 55);
            this.txtDate.MaxLength = 11;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(83, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // btnView
            // 
            this.btnView.ActionType = "";
            this.btnView.Location = new System.Drawing.Point(473, 461);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 41;
            this.btnView.Text = "VIEW";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(416, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 111;
            this.lblUserName.Text = "User Name :";
            // 
            // CHRIS_Payroll_LFAEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(634, 551);
            this.Controls.Add(this.slPnlLeaveFairAssistance);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.btnView);
            this.CurrentPanelBlock = "slPnlLeaveFairAssistance";
            this.Name = "CHRIS_Payroll_LFAEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRIS :  LFA Entry";
            this.Shown += new System.EventHandler(this.CHRIS_Payroll_LFAEntry_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.btnView, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.slPnlLeaveFairAssistance, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlLeaveFairAssistance.ResumeLayout(false);
            this.slPnlLeaveFairAssistance.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLButton btnView;
        private CrplControlLibrary.SLTextBox txtOptionView;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.LookupButton lkbPersonnel;
        private CrplControlLibrary.SLTextBox txtLeaveBalanceHidden;
        public CrplControlLibrary.SLTextBox txtDateofGeneration;
        public CrplControlLibrary.SLTextBox txtApproved;
        public CrplControlLibrary.SLTextBox txtLFAamount;
        public CrplControlLibrary.SLTextBox txtLeaveBalance;
        public CrplControlLibrary.SLTextBox txtPersonnelName;
        public CrplControlLibrary.SLTextBox txtPersonnelNo;
        public iCORE.COMMON.SLCONTROLS.SLPanelSimple slPnlLeaveFairAssistance;
    }
}