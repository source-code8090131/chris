namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_FTEEntry_ProcessingDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmployeeNo = new CrplControlLibrary.SLTextBox(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Processing Employee No.";
            // 
            // txtEmployeeNo
            // 
            this.txtEmployeeNo.AllowSpace = true;
            this.txtEmployeeNo.AssociatedLookUpName = "";
            this.txtEmployeeNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmployeeNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEmployeeNo.ContinuationTextBox = null;
            this.txtEmployeeNo.CustomEnabled = true;
            this.txtEmployeeNo.DataFieldMapping = "";
            this.txtEmployeeNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmployeeNo.GetRecordsOnUpDownKeys = false;
            this.txtEmployeeNo.IsDate = false;
            this.txtEmployeeNo.Location = new System.Drawing.Point(217, 25);
            this.txtEmployeeNo.Name = "txtEmployeeNo";
            this.txtEmployeeNo.NumberFormat = "###,###,##0.00";
            this.txtEmployeeNo.Postfix = "";
            this.txtEmployeeNo.Prefix = "";
            this.txtEmployeeNo.ReadOnly = true;
            this.txtEmployeeNo.Size = new System.Drawing.Size(52, 20);
            this.txtEmployeeNo.SkipValidation = false;
            this.txtEmployeeNo.TabIndex = 10;
            this.txtEmployeeNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Payroll_FTEEntry_ProcessingDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 66);
            this.ControlBox = false;
            this.Controls.Add(this.txtEmployeeNo);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(350, 100);
            this.Name = "CHRIS_Payroll_FTEEntry_ProcessingDialog";
            this.Text = "CHRIS_Payroll_FTEEntry_ProcessingDialog";
            this.Load += new System.EventHandler(this.CHRIS_Payroll_FTEEntry_ProcessingDialog_Load);
            this.Shown += new System.EventHandler(this.CHRIS_Payroll_FTEEntry_ProcessingDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtEmployeeNo;
    }
}