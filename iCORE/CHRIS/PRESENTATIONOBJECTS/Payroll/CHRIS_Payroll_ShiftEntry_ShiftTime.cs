using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_ShiftEntry_ShiftTime : SimpleForm
    {
        #region Feilds
        public const string TIMEFORMAT_REGEX = "^([0-1][0-9]|[2][0-3]):([0-5][0-9])|(99:99)$";
        public string WIOne = string.Empty;
        public string WOOne = string.Empty;
        public string WITwo = string.Empty;
        public string WOTwo = string.Empty;
        public string WIThree = string.Empty;
        public string WOThree = string.Empty;
        #endregion

        #region Constructors

        public CHRIS_Payroll_ShiftEntry_ShiftTime()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_ShiftEntry_ShiftTime(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region Overriden Region.
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            //this.tbtAdd.Visible = false;
            //this.tbtCancel.Visible = false;
            //this.tbtClose.Visible = false;
            //this.tbtDelete.Visible = false;
            //this.tbtEdit.Visible = false;
            //this.tbtList.Visible = false;
            this.tlbMain.Visible = false;
            txt_WI1.Text=WIOne;
            txt_WO1.Text=WOOne ;
            txt_WI2.Text = WITwo;
            txt_WO2.Text = WOTwo;
            txt_WI3.Text = WIThree;
            txt_WO3.Text = WOThree;

        }

        protected override void SimpleForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
            {
                this.Close();
                return;
            }
            //else
            //    base.OnFormKeyup(sender, e);
        }

        #endregion

        #region Events

        private void txt_WO3_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (!(e.Shift))
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                }
            }
        }

        private void txt_WO3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\t' || e.KeyChar == 13)
            {
                if (txt_WO3.Text == string.Empty)
                    txt_WO3.Text = "07:00";
                WIOne = txt_WI1.Text;
                WOOne = txt_WO1.Text;
                WITwo = txt_WI2.Text;
                WOTwo = txt_WO2.Text;
                WIThree = txt_WI3.Text;
                WOThree = txt_WO3.Text;
                this.Close();

            }

        }

        private void txt_WI1_Validating(object sender, CancelEventArgs e)
        {
            string text = txt_WI1.Text;
            if (string.IsNullOrEmpty(text))
            {
                txt_WI1.Text = "08:00";
            }
            else
            {
                if (!IsValidExpression(text, TIMEFORMAT_REGEX))
                {
                    MessageBox.Show("Feild must be of form 99\":\"99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }

        }

        private void txt_WO1_Validating(object sender, CancelEventArgs e)
        {
            string text = txt_WO1.Text;
            if (string.IsNullOrEmpty(text))
            {
                txt_WO1.Text = "16:00";
            }
            else
            {
                if (!IsValidExpression(text, TIMEFORMAT_REGEX))
                {
                    MessageBox.Show("Feild must be of form 99\":\"99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }
        }

        private void txt_WI2_Validating(object sender, CancelEventArgs e)
        {
            string text = txt_WI2.Text;
            if (string.IsNullOrEmpty(text))
            {
                txt_WI2.Text = "15:30";
            }
            else
            {
                if (!IsValidExpression(text, TIMEFORMAT_REGEX))
                {
                    MessageBox.Show("Feild must be of form 99\":\"99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }

        }

        private void txt_WO2_Validating(object sender, CancelEventArgs e)
        {
            string text = txt_WO2.Text;
            if (string.IsNullOrEmpty(text))
            {
                txt_WO2.Text = "23:30";
            }
            else
            {
                if (!IsValidExpression(text, TIMEFORMAT_REGEX))
                {
                    MessageBox.Show("Feild must be of form 99\":\"99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }

        }

        private void txt_WI3_Validating(object sender, CancelEventArgs e)
        {
            string text = txt_WI3.Text;
            if (string.IsNullOrEmpty(text))
            {
                txt_WI3.Text = "23:00";
            }
            else
            {
                if (!IsValidExpression(text, TIMEFORMAT_REGEX))
                {
                    MessageBox.Show("Feild must be of form 99\":\"99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }

        }

        private void txt_WO3_Validating(object sender, CancelEventArgs e)
        {
            string text = txt_WO3.Text;
            if (string.IsNullOrEmpty(text))
            {
                txt_WO3.Text = "07:00";
            }
            else
            {
                if (!IsValidExpression(text, TIMEFORMAT_REGEX))
                {
                    MessageBox.Show("Feild must be of form 99\":\"99", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

            }

        }

        #endregion
    }
}