using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_IndividualsDeductionEntry : ChrisMasterDetailForm
    {

        #region Constructor
        public CHRIS_Payroll_IndividualsDeductionEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_IndividualsDeductionEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            //Dependent panel
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPnlDetail);
            this.slPnlMaster.DependentPanels = lstDependentPanels;

            //Independent panel
            this.IndependentPanels.Add(slPnlMaster);

        }
        #endregion

        #region Methods

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                //this.txtOption.Focus();               

                this.txtOption.Visible = true;
                this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                lblUserName.Text = "User Name : " + this.UserName;
                this.txtUser.Text = this.userID;
                this.sldgvDetail.ReadOnly = true;
                EnableToolBarOptions(false);

                //View 
                this.FunctionConfig.F1 = Function.Add;
                this.FunctionConfig.EnableF1 = true;
                this.ShowF1Option = true;
                this.F1OptionText = " [S]TART ENTRY";
                this.FunctionConfig.OptionLetterF1 = "S";

                //Save
                this.FunctionConfig.F8 = Function.Save;
                this.FunctionConfig.EnableF8 = true;
                this.ShowF8Option = true;
                this.F8OptionText = "[F8] To Save";

                //Delete
                //this.FunctionConfig.F4 = Function.Delete;
                //this.FunctionConfig.EnableF4 = true;
                //this.ShowF4Option = true;
                //this.F4OptionText = "[F4] Delete";
                this.CanEnableDisableControls = true;


                //sldgvDetail.Columns["SP_P_NO"].ValueType = typeof(double);

                ((DataGridViewLOVColumn)this.sldgvDetail.Columns["SP_P_NO"]).LovDataType = typeof(decimal);
                this.sldgvDetail.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
                this.sldgvDetail.Columns["SP_DED_AMT"].DefaultCellStyle.NullValue = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Override it for Entry mode
        /// </summary>
        /// <returns></returns>
        //protected override bool View()
        //{
        //    this.txtOption.Text = "S";
        //    this.txtOptionView.Text = "ENTRY";
        //    this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");

        //    return false;
        //}

        /// <summary>
        /// Override it Delete
        /// </summary>
        /// <returns></returns>
        protected override bool Delete()
        {
            if ((sldgvDetail.ReadOnly == false) && (this.sldgvDetail.SelectedRows.Count > 0))
            {
                base.Delete();
                base.DoToolbarActions(this.Controls, "Delete");
                txtDeductionCode.Focus();
            }
            return false;
        }

        /// <summary>
        /// Override it Save
        /// </summary>
        /// <returns></returns>
        protected override bool Save()
        {
            if (sldgvDetail.ReadOnly == false)
            {
                //base.Save();  
                base.DoToolbarActions(this.Controls, "Save");
            }
            return false;
        }

        protected override bool Add()
        {
            base.Add();
            this.txtOption.Text = "S";
            return false;
        }

        protected override bool Quit()
        {
            bool flag = false;
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            }
            this.txtOption.Select();
            this.txtOption.Focus();
            return flag;
        }

        void GetDeductionDetail()
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("SP_DED_CODE", (object)this.txtDeductionCode.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_DETAILS_MANAGER", "List", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            this.sldgvDetail.DataSource = rslt.dstResult.Tables[0];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void EnableFormControls(bool isEnable)
        {
            foreach (Control cl in this.Controls)
            {
                if (cl is TextBox)
                {
                    ((TextBox)cl).ReadOnly = isEnable;
                }
                else if (cl is Button)
                {
                    ((Button)cl).Enabled = isEnable;
                }
                else if (cl is DataGridView)
                {
                    ((DataGridView)cl).ReadOnly = isEnable;
                }
            }
        }

        void EnableToolBarOptions(bool isEnable)
        {
            this.tbtList.Visible = false;
            this.tbtAdd.Visible = false;
            //this.tbtSave.Visible = false;
            this.tbtEdit.Visible = false;
            //this.tbtDelete.Visible = false;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            try
            {
                if (actionType == "Save")
                {
                    if (sldgvDetail.ReadOnly == false)
                    {
                        base.DoToolbarActions(ctrlsCollection, "Save");
                    }
                }
                else
                    base.DoToolbarActions(ctrlsCollection, actionType);

                if (actionType == "Cancel")
                {
                    this.sldgvDetail.ReadOnly = true;
                    this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                    lblUserName.Text = this.UserName;
                    this.txtUser.Text = this.userID;
                }
                if (actionType == "Delete")
                {
                    if (txtDeductionDescription.Focused)
                        txtDeductionDescription.SkipValidation = true;
                    txtDeductionCode.Focus();
                    txtDeductionDescription.SkipValidation = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Check whether particular deduction code exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDeductionCode_Leave(object sender, EventArgs e)
        {

            //if (this.txtDeductionCode.Text == string.Empty)
            //{
            //    MessageBox.Show("Please Enter Deduction Code <F5>=Save  <F6>=Exit W/O Save");
            //    this.txtDeductionCode.Focus();
            //}
            //else
            //{
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("SP_DED_CODE", (object)this.txtDeductionCode.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "ValidDeductionCodeLovExist", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count == 0)
                    {
                        //No record Exist for same deduction code
                        lkpValidDeductionCode.SkipValidationOnLeave = true;
                        this.operationMode = Mode.Add;
                        this.slPnlMaster.LoadDependentPanels();
                        sldgvDetail.ReadOnly = false;
                    }
                    else
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            this.txtDeductionDescription.Text = rslt.dstResult.Tables[0].Rows[0]["SP_DED_DESC"].ToString();
                            this.sldpValidFrom.Text = rslt.dstResult.Tables[0].Rows[0]["SP_VALID_FROM_D"].ToString();
                            this.sldpValidTo.Text = rslt.dstResult.Tables[0].Rows[0]["SP_VALID_TO_D"].ToString();
                            this.txtDeductionAmount.Text = rslt.dstResult.Tables[0].Rows[0]["SP_DED_AMOUNT"].ToString();
                            this.txtAccountNo.Text = rslt.dstResult.Tables[0].Rows[0]["SP_ACOUNT_NO_D"].ToString();
                            this.txtAccountDescription.Text = rslt.dstResult.Tables[0].Rows[0]["SP_ACC_DESC"].ToString();
                            this.operationMode = Mode.Edit;
                            this.slPnlMaster.SetEntityObject(rslt.dstResult.Tables[0].Rows[0]);
                            this.slPnlMaster.LoadDependentPanels();
                            //GetDeductionDetail();
                            sldgvDetail.ReadOnly = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                base.LogException(this.Name, "txtDeductionCode_Leave", ex);
            }
            //}
        }

        /// <summary>
        /// Check whether "Valid From Date" is greater than "Valid To Date"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sldpValidTo_Leave(object sender, EventArgs e)
        {
            if (sldpValidFrom.Value != null && sldpValidTo.Value != null)
            {
                if (DateTime.Parse(sldpValidFrom.Value.ToString()) > DateTime.Parse(sldpValidTo.Value.ToString()))
                {
                    MessageBox.Show("To Date Must Be Greater Than From Date");
                    this.sldpValidFrom.Focus();
                }
            }

        }

        /// <summary>
        /// Check whether Deduction amount is zero or less than zero
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDeductionAmount_Leave(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Check whether particular account exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAccountNo_Leave(object sender, EventArgs e)
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("SP_ACOUNT_NO_D", (object)this.txtAccountNo.Text);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "ValidAccountNumberLovExist", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count == 0)
                    {
                        //No record Exist for same Account No
                        MessageBox.Show("Enter A Valid Account No");
                        this.lkpValidAccount.SkipValidationOnLeave = true;
                        this.txtAccountNo.Focus();

                    }
                    else
                    {
                        //If record Exist
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows[0]["SP_ACC_DESC"] != null && rslt.dstResult.Tables[0].Rows[0]["SP_ACC_DESC"].ToString() != "")
                            {
                                //If record Exist for same Account No then get its description
                                this.txtAccountDescription.Text = rslt.dstResult.Tables[0].Rows[0]["SP_ACC_DESC"].ToString();
                                if (this.FunctionConfig.CurrentOption == Function.Add)
                                {
                                    this.sldgvDetail.Focus();
                                }
                            }
                        }
                        this.lkpValidAccount.SkipValidationOnLeave = true;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CHRIS_Payroll_IndividualsDeductionEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if ((actionType == "ValidDeductionCodeLov") || (actionType == "ValidDeductionCodeLovExist"))
            {
                this.sldgvDetail.ReadOnly = false;
            }
        }

        private void sldgvDetail_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0 && (!this.sldgvDetail.CurrentRow.IsNewRow))
            {
                if (this.sldgvDetail.CurrentRow.Cells[0].Value != DBNull.Value)
                {
                    double amountVal = 0.00;
                    double.TryParse(txtDeductionAmount.Text, out amountVal);
                    this.sldgvDetail.CurrentRow.Cells["SP_DED_AMT"].Value = amountVal;
                }

            }
        }

        private void txtDeductionAmount_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtDeductionAmount.Text != "")
            {
                if (Convert.ToInt32(this.txtDeductionAmount.Text) <= 0)
                {
                    MessageBox.Show("Amount Can Not Be Zero Or Less Then Zero");
                    e.Cancel = true;
                }
            }

        }
        #endregion

        private void sldgvDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
            {
                if (sldgvDetail.CurrentRow != null 
                    && sldgvDetail.CurrentRow.Index < sldgvDetail.Rows.Count
                    && sldgvDetail.CurrentRow.Index != sldgvDetail.Rows.Count - 1)
                {
                    sldgvDetail.EndEdit();
                    this.Refresh();
                    sldgvDetail.Rows.RemoveAt(sldgvDetail.CurrentRow.Index);
                    this.Refresh();
                }
            }
        }
        #region Commented Code

        //private void CHRIS_Payroll_IndividualsDeductionEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        //{
        //    if ((actionType == "ValidDeductionCodeLov") || (actionType == "ValidDeductionCodeLovExist"))
        //    {
        //        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
        //    }
        //}
        /// <summary>
        /// For DateTime Conversion
        /// </summary>
        /// <param name="pStrDateTime"></param>
        /// <returns> string </returns>
        //private string DateTimeConversion(string pStrDateTime)
        //{
        //    string[] DateTimeArray = pStrDateTime.Split('/');

        //    string strDay = DateTimeArray[0];
        //    string strMonth = DateTimeArray[1];
        //    string strYear = DateTimeArray[2];

        //    pStrDateTime = strMonth + "/" + strDay + "/" + strYear;

        //    return pStrDateTime;
        //}

        #endregion

    }
}