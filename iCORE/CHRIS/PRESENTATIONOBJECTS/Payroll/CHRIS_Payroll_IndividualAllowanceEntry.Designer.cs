namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_IndividualAllowanceEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_IndividualAllowanceEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpBase = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slPanelMasterAllowance = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lookupAccounts = new CrplControlLibrary.LookupButton(this.components);
            this.txt_SP_ASR_BASIC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ALL_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DESC = new CrplControlLibrary.SLTextBox(this.components);
            this.lookupSpCodes = new CrplControlLibrary.LookupButton(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.slDatePicker_SP_VALID_TO = new CrplControlLibrary.SLDatePicker(this.components);
            this.slDatePicker_SP_VALID_FROM = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_FORECAST_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ACOUNT_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Acc_Desc = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ALL_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slPanelDetailAllowance = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvAllowanceDetails = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.col_SP_P_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.col_P_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SP_ALL_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SP_REMARKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SP_ALL_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grpBase.SuspendLayout();
            this.slPanelMasterAllowance.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.slPanelDetailAllowance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllowanceDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(402, 0);
            this.txtOption.Size = new System.Drawing.Size(36, 26);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlBottom.Location = new System.Drawing.Point(213, 0);
            this.pnlBottom.Size = new System.Drawing.Size(438, 22);
            this.pnlBottom.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 545);
            this.panel1.Size = new System.Drawing.Size(651, 73);
            this.panel1.TabIndex = 1;
            // 
            // grpBase
            // 
            this.grpBase.Controls.Add(this.label10);
            this.grpBase.Controls.Add(this.label1);
            this.grpBase.Controls.Add(this.slPanelMasterAllowance);
            this.grpBase.Controls.Add(this.groupBox1);
            this.grpBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBase.ForeColor = System.Drawing.Color.Purple;
            this.grpBase.Location = new System.Drawing.Point(4, 73);
            this.grpBase.Name = "grpBase";
            this.grpBase.Size = new System.Drawing.Size(643, 466);
            this.grpBase.TabIndex = 2;
            this.grpBase.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(243, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(157, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Individual Allowance Entry";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Allowance";
            // 
            // slPanelMasterAllowance
            // 
            this.slPanelMasterAllowance.ConcurrentPanels = null;
            this.slPanelMasterAllowance.Controls.Add(this.lookupAccounts);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_ASR_BASIC);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_ALL_AMOUNT);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_DESC);
            this.slPanelMasterAllowance.Controls.Add(this.lookupSpCodes);
            this.slPanelMasterAllowance.Controls.Add(this.label6);
            this.slPanelMasterAllowance.Controls.Add(this.slDatePicker_SP_VALID_TO);
            this.slPanelMasterAllowance.Controls.Add(this.slDatePicker_SP_VALID_FROM);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_FORECAST_TAX);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_TAX);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_ACOUNT_NO);
            this.slPanelMasterAllowance.Controls.Add(this.txt_Acc_Desc);
            this.slPanelMasterAllowance.Controls.Add(this.txt_SP_ALL_CODE);
            this.slPanelMasterAllowance.Controls.Add(this.label9);
            this.slPanelMasterAllowance.Controls.Add(this.label8);
            this.slPanelMasterAllowance.Controls.Add(this.label7);
            this.slPanelMasterAllowance.Controls.Add(this.label5);
            this.slPanelMasterAllowance.Controls.Add(this.label4);
            this.slPanelMasterAllowance.Controls.Add(this.label3);
            this.slPanelMasterAllowance.Controls.Add(this.label2);
            this.slPanelMasterAllowance.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPanelMasterAllowance.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Cascading;
            this.slPanelMasterAllowance.DependentPanels = null;
            this.slPanelMasterAllowance.DisableDependentLoad = false;
            this.slPanelMasterAllowance.EnableDelete = false;
            this.slPanelMasterAllowance.EnableInsert = true;
            this.slPanelMasterAllowance.EnableQuery = false;
            this.slPanelMasterAllowance.EnableUpdate = true;
            this.slPanelMasterAllowance.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceCommand";
            this.slPanelMasterAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelMasterAllowance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.slPanelMasterAllowance.Location = new System.Drawing.Point(2, 54);
            this.slPanelMasterAllowance.MasterPanel = null;
            this.slPanelMasterAllowance.Name = "slPanelMasterAllowance";
            this.slPanelMasterAllowance.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelMasterAllowance.Size = new System.Drawing.Size(633, 95);
            this.slPanelMasterAllowance.SPName = "CHRIS_Sp_ALLOWANCE_MANAGER";
            this.slPanelMasterAllowance.TabIndex = 1;
            // 
            // lookupAccounts
            // 
            this.lookupAccounts.ActionLOVExists = "GetAccountLovExists";
            this.lookupAccounts.ActionType = "GetAccountLov";
            this.lookupAccounts.ConditionalFields = "";
            this.lookupAccounts.CustomEnabled = true;
            this.lookupAccounts.DataFieldMapping = "";
            this.lookupAccounts.DependentLovControls = "";
            this.lookupAccounts.HiddenColumns = "";
            this.lookupAccounts.Image = ((System.Drawing.Image)(resources.GetObject("lookupAccounts.Image")));
            this.lookupAccounts.LoadDependentEntities = false;
            this.lookupAccounts.Location = new System.Drawing.Point(164, 67);
            this.lookupAccounts.LookUpTitle = null;
            this.lookupAccounts.Name = "lookupAccounts";
            this.lookupAccounts.Size = new System.Drawing.Size(26, 21);
            this.lookupAccounts.SkipValidationOnLeave = false;
            this.lookupAccounts.SPName = "CHRIS_Sp_ALLOWANCE_MANAGER";
            this.lookupAccounts.TabIndex = 91;
            this.lookupAccounts.TabStop = false;
            this.lookupAccounts.UseVisualStyleBackColor = true;
            // 
            // txt_SP_ASR_BASIC
            // 
            this.txt_SP_ASR_BASIC.AllowSpace = true;
            this.txt_SP_ASR_BASIC.AssociatedLookUpName = "";
            this.txt_SP_ASR_BASIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ASR_BASIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ASR_BASIC.ContinuationTextBox = null;
            this.txt_SP_ASR_BASIC.CustomEnabled = true;
            this.txt_SP_ASR_BASIC.DataFieldMapping = "SP_ASR_BASIC";
            this.txt_SP_ASR_BASIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ASR_BASIC.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ASR_BASIC.IsDate = false;
            this.txt_SP_ASR_BASIC.Location = new System.Drawing.Point(528, 37);
            this.txt_SP_ASR_BASIC.Name = "txt_SP_ASR_BASIC";
            this.txt_SP_ASR_BASIC.NumberFormat = "###,###,##0.00";
            this.txt_SP_ASR_BASIC.Postfix = "";
            this.txt_SP_ASR_BASIC.Prefix = "";
            this.txt_SP_ASR_BASIC.Size = new System.Drawing.Size(18, 20);
            this.txt_SP_ASR_BASIC.SkipValidation = false;
            this.txt_SP_ASR_BASIC.TabIndex = 7;
            this.txt_SP_ASR_BASIC.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_SP_ASR_BASIC, "Use [A]ASR or [B]Basic");
            this.txt_SP_ASR_BASIC.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_ASR_BASIC_Validating);
            // 
            // txt_SP_ALL_AMOUNT
            // 
            this.txt_SP_ALL_AMOUNT.AllowSpace = true;
            this.txt_SP_ALL_AMOUNT.AssociatedLookUpName = "";
            this.txt_SP_ALL_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ALL_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ALL_AMOUNT.ContinuationTextBox = null;
            this.txt_SP_ALL_AMOUNT.CustomEnabled = true;
            this.txt_SP_ALL_AMOUNT.DataFieldMapping = "SP_ALL_AMOUNT";
            this.txt_SP_ALL_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ALL_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ALL_AMOUNT.IsDate = false;
            this.txt_SP_ALL_AMOUNT.Location = new System.Drawing.Point(368, 37);
            this.txt_SP_ALL_AMOUNT.MaxLength = 6;
            this.txt_SP_ALL_AMOUNT.Name = "txt_SP_ALL_AMOUNT";
            this.txt_SP_ALL_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_SP_ALL_AMOUNT.Postfix = "";
            this.txt_SP_ALL_AMOUNT.Prefix = "";
            this.txt_SP_ALL_AMOUNT.Size = new System.Drawing.Size(90, 20);
            this.txt_SP_ALL_AMOUNT.SkipValidation = false;
            this.txt_SP_ALL_AMOUNT.TabIndex = 6;
            this.txt_SP_ALL_AMOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_SP_ALL_AMOUNT.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_SP_DESC
            // 
            this.txt_SP_DESC.AllowSpace = true;
            this.txt_SP_DESC.AssociatedLookUpName = "";
            this.txt_SP_DESC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DESC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DESC.ContinuationTextBox = null;
            this.txt_SP_DESC.CustomEnabled = true;
            this.txt_SP_DESC.DataFieldMapping = "SP_DESC";
            this.txt_SP_DESC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DESC.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DESC.IsDate = false;
            this.txt_SP_DESC.Location = new System.Drawing.Point(207, 8);
            this.txt_SP_DESC.MaxLength = 30;
            this.txt_SP_DESC.Name = "txt_SP_DESC";
            this.txt_SP_DESC.NumberFormat = "###,###,##0.00";
            this.txt_SP_DESC.Postfix = "";
            this.txt_SP_DESC.Prefix = "";
            this.txt_SP_DESC.Size = new System.Drawing.Size(251, 20);
            this.txt_SP_DESC.SkipValidation = false;
            this.txt_SP_DESC.TabIndex = 1;
            this.txt_SP_DESC.TextType = CrplControlLibrary.TextType.String;
            // 
            // lookupSpCodes
            // 
            this.lookupSpCodes.ActionLOVExists = "ListExist";
            this.lookupSpCodes.ActionType = "List";
            this.lookupSpCodes.ConditionalFields = "";
            this.lookupSpCodes.CustomEnabled = true;
            this.lookupSpCodes.DataFieldMapping = "";
            this.lookupSpCodes.DependentLovControls = "";
            this.lookupSpCodes.HiddenColumns = "";
            this.lookupSpCodes.Image = ((System.Drawing.Image)(resources.GetObject("lookupSpCodes.Image")));
            this.lookupSpCodes.LoadDependentEntities = true;
            this.lookupSpCodes.Location = new System.Drawing.Point(163, 8);
            this.lookupSpCodes.LookUpTitle = "Allowance Description";
            this.lookupSpCodes.Name = "lookupSpCodes";
            this.lookupSpCodes.Size = new System.Drawing.Size(26, 21);
            this.lookupSpCodes.SkipValidationOnLeave = true;
            this.lookupSpCodes.SPName = "CHRIS_Sp_ALLOWANCE_MANAGER";
            this.lookupSpCodes.TabIndex = 16;
            this.lookupSpCodes.TabStop = false;
            this.lookupSpCodes.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "/";
            // 
            // slDatePicker_SP_VALID_TO
            // 
            this.slDatePicker_SP_VALID_TO.CustomEnabled = true;
            this.slDatePicker_SP_VALID_TO.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_SP_VALID_TO.DataFieldMapping = "SP_VALID_TO";
            this.slDatePicker_SP_VALID_TO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slDatePicker_SP_VALID_TO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_SP_VALID_TO.HasChanges = true;
            this.slDatePicker_SP_VALID_TO.IsRequired = true;
            this.slDatePicker_SP_VALID_TO.Location = new System.Drawing.Point(207, 37);
            this.slDatePicker_SP_VALID_TO.Name = "slDatePicker_SP_VALID_TO";
            this.slDatePicker_SP_VALID_TO.NullValue = " ";
            this.slDatePicker_SP_VALID_TO.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_SP_VALID_TO.TabIndex = 5;
            this.slDatePicker_SP_VALID_TO.Value = new System.DateTime(2011, 1, 24, 0, 0, 0, 0);
            this.slDatePicker_SP_VALID_TO.Validating += new System.ComponentModel.CancelEventHandler(this.slDatePicker_SP_VALID_TO_Validating);
            // 
            // slDatePicker_SP_VALID_FROM
            // 
            this.slDatePicker_SP_VALID_FROM.CustomEnabled = true;
            this.slDatePicker_SP_VALID_FROM.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_SP_VALID_FROM.DataFieldMapping = "SP_VALID_FROM";
            this.slDatePicker_SP_VALID_FROM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slDatePicker_SP_VALID_FROM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_SP_VALID_FROM.HasChanges = true;
            this.slDatePicker_SP_VALID_FROM.IsRequired = true;
            this.slDatePicker_SP_VALID_FROM.Location = new System.Drawing.Point(89, 37);
            this.slDatePicker_SP_VALID_FROM.Name = "slDatePicker_SP_VALID_FROM";
            this.slDatePicker_SP_VALID_FROM.NullValue = " ";
            this.slDatePicker_SP_VALID_FROM.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_SP_VALID_FROM.TabIndex = 4;
            this.slDatePicker_SP_VALID_FROM.Value = new System.DateTime(2011, 1, 24, 0, 0, 0, 0);
            // 
            // txt_SP_FORECAST_TAX
            // 
            this.txt_SP_FORECAST_TAX.AllowSpace = true;
            this.txt_SP_FORECAST_TAX.AssociatedLookUpName = "";
            this.txt_SP_FORECAST_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_FORECAST_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_FORECAST_TAX.ContinuationTextBox = null;
            this.txt_SP_FORECAST_TAX.CustomEnabled = true;
            this.txt_SP_FORECAST_TAX.DataFieldMapping = "SP_FORECAST_TAX";
            this.txt_SP_FORECAST_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_FORECAST_TAX.GetRecordsOnUpDownKeys = false;
            this.txt_SP_FORECAST_TAX.IsDate = false;
            this.txt_SP_FORECAST_TAX.IsRequired = true;
            this.txt_SP_FORECAST_TAX.Location = new System.Drawing.Point(606, 8);
            this.txt_SP_FORECAST_TAX.Name = "txt_SP_FORECAST_TAX";
            this.txt_SP_FORECAST_TAX.NumberFormat = "###,###,##0.00";
            this.txt_SP_FORECAST_TAX.Postfix = "";
            this.txt_SP_FORECAST_TAX.Prefix = "";
            this.txt_SP_FORECAST_TAX.Size = new System.Drawing.Size(18, 20);
            this.txt_SP_FORECAST_TAX.SkipValidation = false;
            this.txt_SP_FORECAST_TAX.TabIndex = 3;
            this.txt_SP_FORECAST_TAX.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_SP_FORECAST_TAX, "Use [Y]Yes or [N]No");
            this.txt_SP_FORECAST_TAX.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_FORECAST_TAX_Validating);
            // 
            // txt_SP_TAX
            // 
            this.txt_SP_TAX.AllowSpace = true;
            this.txt_SP_TAX.AssociatedLookUpName = "";
            this.txt_SP_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_TAX.ContinuationTextBox = null;
            this.txt_SP_TAX.CustomEnabled = true;
            this.txt_SP_TAX.DataFieldMapping = "SP_TAX";
            this.txt_SP_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_TAX.GetRecordsOnUpDownKeys = false;
            this.txt_SP_TAX.IsDate = false;
            this.txt_SP_TAX.IsRequired = true;
            this.txt_SP_TAX.Location = new System.Drawing.Point(528, 8);
            this.txt_SP_TAX.MaxLength = 1;
            this.txt_SP_TAX.Name = "txt_SP_TAX";
            this.txt_SP_TAX.NumberFormat = "###,###,##0.00";
            this.txt_SP_TAX.Postfix = "";
            this.txt_SP_TAX.Prefix = "";
            this.txt_SP_TAX.Size = new System.Drawing.Size(18, 20);
            this.txt_SP_TAX.SkipValidation = false;
            this.txt_SP_TAX.TabIndex = 2;
            this.txt_SP_TAX.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_SP_TAX, "Use [Y]Yes or [N]No");
            this.txt_SP_TAX.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_TAX_Validating);
            // 
            // txt_SP_ACOUNT_NO
            // 
            this.txt_SP_ACOUNT_NO.AllowSpace = true;
            this.txt_SP_ACOUNT_NO.AssociatedLookUpName = "lookupAccounts";
            this.txt_SP_ACOUNT_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ACOUNT_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ACOUNT_NO.ContinuationTextBox = null;
            this.txt_SP_ACOUNT_NO.CustomEnabled = true;
            this.txt_SP_ACOUNT_NO.DataFieldMapping = "SP_ACOUNT_NO";
            this.txt_SP_ACOUNT_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ACOUNT_NO.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ACOUNT_NO.IsDate = false;
            this.txt_SP_ACOUNT_NO.IsLookUpField = true;
            this.txt_SP_ACOUNT_NO.IsRequired = true;
            this.txt_SP_ACOUNT_NO.Location = new System.Drawing.Point(89, 67);
            this.txt_SP_ACOUNT_NO.MaxLength = 11;
            this.txt_SP_ACOUNT_NO.Name = "txt_SP_ACOUNT_NO";
            this.txt_SP_ACOUNT_NO.NumberFormat = "###,###,##0.00";
            this.txt_SP_ACOUNT_NO.Postfix = "";
            this.txt_SP_ACOUNT_NO.Prefix = "";
            this.txt_SP_ACOUNT_NO.Size = new System.Drawing.Size(69, 20);
            this.txt_SP_ACOUNT_NO.SkipValidation = false;
            this.txt_SP_ACOUNT_NO.TabIndex = 8;
            this.txt_SP_ACOUNT_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Acc_Desc
            // 
            this.txt_Acc_Desc.AllowSpace = true;
            this.txt_Acc_Desc.AssociatedLookUpName = "";
            this.txt_Acc_Desc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Acc_Desc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Acc_Desc.ContinuationTextBox = null;
            this.txt_Acc_Desc.CustomEnabled = true;
            this.txt_Acc_Desc.DataFieldMapping = "SP_ACC_DESC";
            this.txt_Acc_Desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Acc_Desc.GetRecordsOnUpDownKeys = false;
            this.txt_Acc_Desc.IsDate = false;
            this.txt_Acc_Desc.Location = new System.Drawing.Point(207, 67);
            this.txt_Acc_Desc.MaxLength = 30;
            this.txt_Acc_Desc.Name = "txt_Acc_Desc";
            this.txt_Acc_Desc.NumberFormat = "###,###,##0.00";
            this.txt_Acc_Desc.Postfix = "";
            this.txt_Acc_Desc.Prefix = "";
            this.txt_Acc_Desc.ReadOnly = true;
            this.txt_Acc_Desc.Size = new System.Drawing.Size(417, 20);
            this.txt_Acc_Desc.SkipValidation = false;
            this.txt_Acc_Desc.TabIndex = 90;
            this.txt_Acc_Desc.TabStop = false;
            this.txt_Acc_Desc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_ALL_CODE
            // 
            this.txt_SP_ALL_CODE.AllowSpace = true;
            this.txt_SP_ALL_CODE.AssociatedLookUpName = "lookupSpCodes";
            this.txt_SP_ALL_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ALL_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ALL_CODE.ContinuationTextBox = null;
            this.txt_SP_ALL_CODE.CustomEnabled = true;
            this.txt_SP_ALL_CODE.DataFieldMapping = "SP_ALL_CODE";
            this.txt_SP_ALL_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ALL_CODE.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ALL_CODE.IsDate = false;
            this.txt_SP_ALL_CODE.IsLookUpField = true;
            this.txt_SP_ALL_CODE.IsRequired = true;
            this.txt_SP_ALL_CODE.Location = new System.Drawing.Point(89, 8);
            this.txt_SP_ALL_CODE.MaxLength = 3;
            this.txt_SP_ALL_CODE.Name = "txt_SP_ALL_CODE";
            this.txt_SP_ALL_CODE.NumberFormat = "###,###,##0.00";
            this.txt_SP_ALL_CODE.Postfix = "";
            this.txt_SP_ALL_CODE.Prefix = "";
            this.txt_SP_ALL_CODE.Size = new System.Drawing.Size(45, 20);
            this.txt_SP_ALL_CODE.SkipValidation = true;
            this.txt_SP_ALL_CODE.TabIndex = 0;
            this.txt_SP_ALL_CODE.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_ALL_CODE.Validated += new System.EventHandler(this.txt_SP_ALL_CODE_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Sp Acount No";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(313, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Amount";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "From / To";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(464, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Taxable";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(464, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Asr/Basic";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(548, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Forecast";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sp All Code";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slPanelDetailAllowance);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Purple;
            this.groupBox1.Location = new System.Drawing.Point(6, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(629, 303);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Allowance Details";
            // 
            // slPanelDetailAllowance
            // 
            this.slPanelDetailAllowance.ConcurrentPanels = null;
            this.slPanelDetailAllowance.Controls.Add(this.dgvAllowanceDetails);
            this.slPanelDetailAllowance.DataManager = null;
            this.slPanelDetailAllowance.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelDetailAllowance.DependentPanels = null;
            this.slPanelDetailAllowance.DisableDependentLoad = false;
            this.slPanelDetailAllowance.EnableDelete = true;
            this.slPanelDetailAllowance.EnableInsert = true;
            this.slPanelDetailAllowance.EnableQuery = false;
            this.slPanelDetailAllowance.EnableUpdate = true;
            this.slPanelDetailAllowance.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.slPanelDetailAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelDetailAllowance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.slPanelDetailAllowance.Location = new System.Drawing.Point(5, 21);
            this.slPanelDetailAllowance.MasterPanel = null;
            this.slPanelDetailAllowance.Name = "slPanelDetailAllowance";
            this.slPanelDetailAllowance.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelDetailAllowance.Size = new System.Drawing.Size(618, 274);
            this.slPanelDetailAllowance.SPName = "CHRIS_Sp_ALLOWANCE_DETAILS_MANAGER";
            this.slPanelDetailAllowance.TabIndex = 3;
            // 
            // dgvAllowanceDetails
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAllowanceDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAllowanceDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllowanceDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_SP_P_NO,
            this.col_P_NAME,
            this.col_SP_ALL_AMT,
            this.col_SP_REMARKS,
            this.col_SP_ALL_CODE});
            this.dgvAllowanceDetails.ColumnToHide = null;
            this.dgvAllowanceDetails.ColumnWidth = null;
            this.dgvAllowanceDetails.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAllowanceDetails.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAllowanceDetails.DisplayColumnWrapper = null;
            this.dgvAllowanceDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAllowanceDetails.GridDefaultRow = 0;
            this.dgvAllowanceDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvAllowanceDetails.Name = "dgvAllowanceDetails";
            this.dgvAllowanceDetails.ReadOnlyColumns = "col_P_NAME";
            this.dgvAllowanceDetails.RequiredColumns = "COL_SP_P_NO";
            this.dgvAllowanceDetails.Size = new System.Drawing.Size(618, 274);
            this.dgvAllowanceDetails.SkippingColumns = null;
            this.dgvAllowanceDetails.TabIndex = 0;
            this.dgvAllowanceDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAllowanceDetails_CellValueChanged);
            this.dgvAllowanceDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAllowanceDetails_KeyDown);
            this.dgvAllowanceDetails.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.dgvAllowanceDetails_PreviewKeyDown);
            // 
            // col_SP_P_NO
            // 
            this.col_SP_P_NO.ActionLOV = "GetPr_P_NoLov";
            this.col_SP_P_NO.ActionLOVExists = "GetPr_P_NoLovExist";
            this.col_SP_P_NO.AttachParentEntity = false;
            this.col_SP_P_NO.DataPropertyName = "SP_P_NO";
            this.col_SP_P_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.col_SP_P_NO.HeaderText = "Personnel No. (Press F9)";
            this.col_SP_P_NO.LookUpTitle = "Personnel Info";
            this.col_SP_P_NO.LOVFieldMapping = "SP_P_NO";
            this.col_SP_P_NO.MaxInputLength = 6;
            this.col_SP_P_NO.Name = "col_SP_P_NO";
            this.col_SP_P_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_SP_P_NO.SearchColumn = "SP_P_NO";
            this.col_SP_P_NO.SkipValidationOnLeave = false;
            this.col_SP_P_NO.SpName = "CHRIS_Sp_ALLOWANCE_DETAILS_MANAGER";
            // 
            // col_P_NAME
            // 
            this.col_P_NAME.DataPropertyName = "P_NAME";
            this.col_P_NAME.HeaderText = "Personnel Name";
            this.col_P_NAME.Name = "col_P_NAME";
            this.col_P_NAME.ReadOnly = true;
            this.col_P_NAME.Width = 180;
            // 
            // col_SP_ALL_AMT
            // 
            this.col_SP_ALL_AMT.DataPropertyName = "SP_ALL_AMT";
            dataGridViewCellStyle2.NullValue = null;
            this.col_SP_ALL_AMT.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_SP_ALL_AMT.HeaderText = "Allowance Amount";
            this.col_SP_ALL_AMT.MaxInputLength = 14;
            this.col_SP_ALL_AMT.Name = "col_SP_ALL_AMT";
            // 
            // col_SP_REMARKS
            // 
            this.col_SP_REMARKS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_SP_REMARKS.DataPropertyName = "SP_REMARKS";
            this.col_SP_REMARKS.HeaderText = "Remarks";
            this.col_SP_REMARKS.MaxInputLength = 30;
            this.col_SP_REMARKS.Name = "col_SP_REMARKS";
            // 
            // col_SP_ALL_CODE
            // 
            this.col_SP_ALL_CODE.DataPropertyName = "SP_ALL_CODE";
            this.col_SP_ALL_CODE.HeaderText = "Column1";
            this.col_SP_ALL_CODE.Name = "col_SP_ALL_CODE";
            this.col_SP_ALL_CODE.Visible = false;
            // 
            // CHRIS_Payroll_IndividualAllowanceEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 618);
            this.Controls.Add(this.grpBase);
            this.CurrentPanelBlock = "slPanelMasterAllowance";
            this.Name = "CHRIS_Payroll_IndividualAllowanceEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Payroll_IndividualAllowanceEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.grpBase, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grpBase.ResumeLayout(false);
            this.grpBase.PerformLayout();
            this.slPanelMasterAllowance.ResumeLayout(false);
            this.slPanelMasterAllowance.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.slPanelDetailAllowance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllowanceDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBase;
        private System.Windows.Forms.GroupBox groupBox1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelDetailAllowance;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvAllowanceDetails;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txt_SP_ALL_CODE;
        private CrplControlLibrary.SLTextBox txt_Acc_Desc;
        private CrplControlLibrary.SLTextBox txt_SP_ACOUNT_NO;
        private CrplControlLibrary.SLTextBox txt_SP_TAX;
        private CrplControlLibrary.SLTextBox txt_SP_FORECAST_TAX;
        private CrplControlLibrary.SLDatePicker slDatePicker_SP_VALID_FROM;
        private CrplControlLibrary.SLDatePicker slDatePicker_SP_VALID_TO;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.LookupButton lookupSpCodes;
        private CrplControlLibrary.SLTextBox txt_SP_DESC;
        private CrplControlLibrary.SLTextBox txt_SP_ALL_AMOUNT;
        private CrplControlLibrary.SLTextBox txt_SP_ASR_BASIC;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelMasterAllowance;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.LookupButton lookupAccounts;
        private System.Windows.Forms.Label label10;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn col_SP_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_P_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SP_ALL_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SP_REMARKS;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SP_ALL_CODE;
    }
}