using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_IncentiveBonusEntry : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> BonusValues = new Dictionary<string, object>();
        Result rslt;
        #region Constructors

        public CHRIS_Payroll_IncentiveBonusEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_IncentiveBonusEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

        #region Overridden /Methods

        protected override void OnLoad(EventArgs e)
        {
            txtOption.Select();
            txtOption.Focus();
            base.OnLoad(e);
            this.txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            //this.txt_Year.Text = System.DateTime.Now.Year.ToString();
            tbtAdd.Visible = false;
            //tbtCancel.Visible = false;
            //tbtClose.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
            
            this.txtLocation1.Text = this.CurrentLocation;
            this.txtUser1.Text = this.userID;
            lblUserName.Text += "   " + this.UserName;
            txtOption.Select();
            txtOption.Focus();
            //this.txt_BO_INC_AMOUNT.Text = "50000";
            this.lookup_Pr_p_No.Enabled = false;
            this.lookup_Pr_p_No.Click += new EventHandler(lookup_Pr_p_No_Click);
            //this.ShowF10Option = true;
            //this.F10OptionText = "F10=Save";
            //this.FunctionConfig.EnableF10 = true;
            //this.FunctionConfig.F10 = Function.Save;

            

        }

        void lookup_Pr_p_No_Click(object sender, EventArgs e)
        {
            this.txt_Pr_P_No.Select();
            this.txt_Pr_P_No.Focus();
            this.txt_Pr_P_No_Validated(null, null);
        }

        private void InitializeDictionary()
        {
            BonusValues.Clear();
            BonusValues.Add("PR_P_NO", 0);
            BonusValues.Add("W_YY", 0);
            BonusValues.Add("W_AS_CTT", 0);
            BonusValues.Add("PR_PROMOTED", null);
            BonusValues.Add("W_GOVT", 0);
            BonusValues.Add("W_OT_AREAR", 0);
            BonusValues.Add("W_AVG_OT", 0);
            BonusValues.Add("W_TOT_OT", 0);
            BonusValues.Add("W_NO_OT", 0);
            BonusValues.Add("W_LAST_BASIC", 0);
            BonusValues.Add("BO_INC_AMOUNT", 0);
            BonusValues.Add("BO_HL_MARKUP", 0);
            BonusValues.Add("BO_Donation", 0);
            BonusValues.Add("BO_Investment", 0);
            BonusValues.Add("BO_Utilities", 0);

        }

        protected override bool Delete()
        {
            bool flag = false;
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Delete";
            this.txtOption.Text = "D";
            this.FunctionConfig.CurrentOption = Function.Delete;
            this.txt_Year.Text = System.DateTime.Now.Year.ToString();
            //this.tbtDelete.PerformClick();
            this.lookup_Pr_p_No.Enabled = true;
            return flag;
            //return base.Delete();
        }

        protected override bool Add()
        {
            bool flag = false;
            this.lookup_Pr_p_No.Enabled = true;
            //this.tbtAdd.PerformClick();
            //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtAdd"]));
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Add";
            this.operationMode = Mode.Add;
            this.FunctionConfig.CurrentOption = Function.Add;
            this.txt_Year.Text = System.DateTime.Now.Year.ToString();
            return flag;
        }

        protected override bool Edit()
        {
            bool flag = false;
            this.lookup_Pr_p_No.Enabled = true;
            //this.tbtEdit.PerformClick();
            //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtEdit"]));
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Modify";
            this.txtOption.Text = "M";
            this.FunctionConfig.CurrentOption = Function.Modify;
            this.operationMode = Mode.Edit;
            this.txt_Year.Text = System.DateTime.Now.Year.ToString();
            return flag;
        }

        protected override bool View()
        {
            CHRIS_Payroll_IncentiveBonusEntry_Dialog viewDialogObject = new CHRIS_Payroll_IncentiveBonusEntry_Dialog();
            viewDialogObject.ShowDialog();
            base.ClearForm(slPanelBonusApproval.Controls);//slPanelBonusApproval.Controls
            this.FunctionConfig.CurrentOption = Function.None;
            this.txtOption.Text = string.Empty;
            this.txtOption.Focus();
            return false;

            //return base.View();
        }

        protected override bool Quit()
        {
            bool flag = false;
            this.lookup_Pr_p_No.Enabled = false;
            this.txt_BO_P_NO.IsRequired = false;
            base.Quit();
            this.txtOption.Focus();
            this.lookup_Pr_p_No.Enabled = false;
            this.txt_BO_P_NO.IsRequired =true;
            return flag;
        }
        protected override bool Cancel()
        {
            base.Cancel();
            this.lookup_Pr_p_No.Enabled = false;
            return false;
        }

        #endregion

        #region Events

        private void txt_Pr_P_No_Validated(object sender, EventArgs e)
        {
            if(txt_Pr_P_No.Text.Trim()!=string.Empty)
            {
                if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                {
                    //IF MODE IS Function.Add AND id ALREADY EXIST FOR THT PR_P_NO in Bonus.then Show failure and Clear form. 
                    if (this.FunctionConfig.CurrentOption == Function.Add && this.txt_ID.Text != string.Empty)
                    {
                        MessageBox.Show("RECORD ALREADY EXISTS FOR THIS P.NO.ENTER OTHER P.NO OR [F6] TO EXIT", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        this.FunctionConfig.CurrentOption = Function.None;
                        this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                        //base.ClearForm(slPanelBonusApproval.Controls);//
                        this.FunctionConfig.CurrentOption = Function.None;
                        this.BonusValues.Clear();
                        this.txt_BO_P_NO.IsRequired = false;
                        this.lookup_Pr_p_No.Enabled = false;
                        this.CurrrentOptionTextBox.Text = string.Empty;
                        this.txtOption.Text = string.Empty;
                        this.txtOption.Select();
                        this.txtOption.Focus();
                        this.txt_BO_P_NO.IsRequired = true;
                        return;
                    }
                    //IF MODE IS Function.Modify or Delete AND id ALREADY doesnot EXIST FOR THT PR_P_NO in Bonus.then Show failure and Clear form. 
                    if ((this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete) && this.txt_ID.Text == string.Empty)
                    {
                        MessageBox.Show("RECORD DOES NOT EXIST FOR THIS P.NO.ENTER OTHER P.NO OR [F6] TO EXIT", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        //base.ClearForm(slPanelBonusApproval.Controls);//slPanelBonusApproval.Controls
                        this.FunctionConfig.CurrentOption = Function.None;
                        this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));

                        this.BonusValues.Clear();
                        this.txt_BO_P_NO.IsRequired = false;
                        this.lookup_Pr_p_No.Enabled = false;
                        this.CurrrentOptionTextBox.Text = string.Empty;
                        this.txtOption.Text = string.Empty;
                        this.txtOption.Select();
                        this.txtOption.Focus();
                        this.txt_BO_P_NO.IsRequired = true;
                        return;

                    }
                    if (this.FunctionConfig.CurrentOption == Function.Delete && this.txt_ID.Text != string.Empty)
                    {
                        //base.Delete();
                        this.FunctionConfig.CurrentOption = Function.None;
                        base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
                        // base.ClearForm(slPanelBonusApproval.Controls);
                        this.txt_BO_P_NO.IsRequired = false;
                        this.ClearForm(slPanelBonusApproval.Controls);
                        this.BonusValues.Clear();
                        this.FunctionConfig.CurrentOption = Function.None;
                        this.lookup_Pr_p_No.Enabled = false;
                        this.CurrrentOptionTextBox.Text = string.Empty;
                        this.txtOption.Text = string.Empty;
                        this.txtOption.Select();
                        this.txtOption.Focus();
                        this.txt_BO_P_NO.IsRequired = true;
                        return;
                    }
                    if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        //**************************Proc 2 and Proc 3********************************
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Clear();
                        param.Add("PR_P_NO", txt_Pr_P_No.Text);
                        param.Add("W_YY", txt_Year.Text);
                        param.Add("W_JOIN", txt_JoiningDate.Text);

                        rslt = cmnDM.GetData("CHRIS_SP_BONUS_INCENTIVE_PROCS", "", param);
                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                txt_W_AS_CTT.Text = rslt.dstResult.Tables[0].Rows[0]["W_AS_CTT"].ToString();
                                BonusValues["W_AS_CTT"] = rslt.dstResult.Tables[0].Rows[0]["W_AS_CTT"];

                                if (rslt.dstResult.Tables[0].Rows[0]["W_GOVT"] != null && rslt.dstResult.Tables[0].Rows[0]["W_GOVT"].ToString() != string.Empty)
                                {
                                    BonusValues["W_GOVT"] = rslt.dstResult.Tables[0].Rows[0]["W_GOVT"];
                                    txt_W_GOVT.Text = rslt.dstResult.Tables[0].Rows[0]["W_GOVT"].ToString();
                                }
                                if (rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTED"] != null && rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTED"].ToString() != string.Empty)
                                {
                                    BonusValues["PR_PROMOTED"] = rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTED"];
                                    txt_Promoted1.Text = rslt.dstResult.Tables[0].Rows[0]["PR_PROMOTED"].ToString();
                                }
                            }

                        }
                        //****************Proc 4***************************************************
                        /** If The Employee Is A clerk **/
                        if (txt_Category.Text.ToUpper() == "C" || Convert.ToInt32(BonusValues["W_AS_CTT"]) == 12 || BonusValues["PR_PROMOTED"].ToString() == "P")
                        {
                            param.Clear();
                            param.Add("PR_P_NO", txt_Pr_P_No.Text);
                            param.Add("W_BONUS_TAB_ID", txt_ID.Text);
                            param.Add("W_YY", txt_Year.Text);
                            param.Add("W_AS_CTT", BonusValues["W_AS_CTT"]);//W_GOVT
                            param.Add("W_GOVT", BonusValues["W_GOVT"]);
                            rslt = cmnDM.GetData("CHRIS_SP_BONUS_INCENTIVE_PROC4", "", param);
                            if (rslt.isSuccessful)
                            {
                                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    if (rslt.dstResult.Tables[0].Rows[0]["W_OT_AREAR"] != null && rslt.dstResult.Tables[0].Rows[0]["W_OT_AREAR"].ToString() != string.Empty)
                                        txt_W_OT_AREAR.Text = rslt.dstResult.Tables[0].Rows[0]["W_OT_AREAR"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["W_AVG_OT"] != null && rslt.dstResult.Tables[0].Rows[0]["W_AVG_OT"].ToString() != string.Empty)
                                        txt_W_AVG_OT.Text = rslt.dstResult.Tables[0].Rows[0]["W_AVG_OT"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["W_TOT_OT"] != null && rslt.dstResult.Tables[0].Rows[0]["W_TOT_OT"].ToString() != string.Empty)
                                        txt_W_TOT_OT.Text = rslt.dstResult.Tables[0].Rows[0]["W_TOT_OT"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["W_NO_OT"] != null && rslt.dstResult.Tables[0].Rows[0]["W_NO_OT"].ToString() != string.Empty)
                                        txt_W_NO_OT.Text = rslt.dstResult.Tables[0].Rows[0]["W_NO_OT"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["W_LAST_BASIC"] != null && rslt.dstResult.Tables[0].Rows[0]["W_LAST_BASIC"].ToString() != string.Empty)
                                        txt_W_LAST_BASIC.Text = rslt.dstResult.Tables[0].Rows[0]["W_LAST_BASIC"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["BO_INC_AMOUNT"] != null && rslt.dstResult.Tables[0].Rows[0]["BO_INC_AMOUNT"].ToString() != string.Empty && rslt.dstResult.Tables[0].Rows[0]["BO_INC_AMOUNT"].ToString() != "0.00")
                                        txt_BO_INC_AMOUNT.Text = rslt.dstResult.Tables[0].Rows[0]["BO_INC_AMOUNT"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["BO_HL_MARKUP"] != null && rslt.dstResult.Tables[0].Rows[0]["BO_HL_MARKUP"].ToString() != string.Empty && rslt.dstResult.Tables[0].Rows[0]["BO_HL_MARKUP"].ToString() != "0.00")
                                        txtHLMARKUP.Text = rslt.dstResult.Tables[0].Rows[0]["BO_HL_MARKUP"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["BO_Investment"] != null && rslt.dstResult.Tables[0].Rows[0]["BO_Investment"].ToString() != string.Empty && rslt.dstResult.Tables[0].Rows[0]["BO_Investment"].ToString() != "0.00")
                                        txtInvestments.Text = rslt.dstResult.Tables[0].Rows[0]["BO_Investment"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["BO_Donation"] != null && rslt.dstResult.Tables[0].Rows[0]["BO_Donation"].ToString() != string.Empty && rslt.dstResult.Tables[0].Rows[0]["BO_Donation"].ToString() != "0.00")
                                        txtDonation.Text = rslt.dstResult.Tables[0].Rows[0]["BO_Donation"].ToString();
                                    if (rslt.dstResult.Tables[0].Rows[0]["BO_Utilities"] != null && rslt.dstResult.Tables[0].Rows[0]["BO_Utilities"].ToString() != string.Empty && rslt.dstResult.Tables[0].Rows[0]["BO_Utilities"].ToString() != "0.00")
                                        txtUtilities.Text = rslt.dstResult.Tables[0].Rows[0]["BO_Utilities"].ToString();
                                }
                            }

                        }
                    }


                }
            }

        }

        private void txt_Year_Validating(object sender, CancelEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption != Function.None)
            {
                if (txt_Year.Text.Length > 0)
                {
                    int yearValue;
                    if (int.TryParse(txt_Year.Text, out yearValue))
                    {
                        if (!(yearValue >= 1753 && yearValue <= DateTime.Now.Year))
                            e.Cancel = true;
                    }
                    else
                        e.Cancel = true;
                }
                else
                    e.Cancel = true;
            }
        }

        private void txt_BO_INC_APP_Validating(object sender, CancelEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
            {
                if (txt_BO_INC_APP.Text.Length > 0)
                {
                    if (txt_BO_INC_APP.Text.ToUpper() != "Y" && txt_BO_INC_APP.Text.ToUpper() != "N")
                        e.Cancel = true;

                }
                else
                    e.Cancel = true;
            }
        }

        private void txt_BO_INC_APP_Validated(object sender, EventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
            {
                if (txt_BO_INC_APP.Text.Length > 0)
                {
                    if (txt_BO_INC_APP.Text.ToUpper() == "Y" || txt_BO_INC_APP.Text.ToUpper() == "N")
                    {
                       if (DialogResult.Yes == MessageBox.Show("Do You Want to Save  The Changes?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                            {
                                base.Save();   
                            }
                        this.ClearForm(slPanelBonusApproval.Controls);//this.Controls
                        this.BonusValues.Clear();
                        this.lookup_Pr_p_No.Enabled = false;
                        this.FunctionConfig.CurrentOption = Function.None;
                        this.CurrrentOptionTextBox.Text = string.Empty;
                        this.txtOption.Text = string.Empty;
                        txtOption.Focus();
                        return;
                    }
                    //if (txt_BO_INC_APP.Text.ToUpper() == "N")
                    //{
                    //    this.ClearForm(slPanelBonusApproval.Controls);//this.Controls
                    //    this.FunctionConfig.CurrentOption = Function.None;
                    //    txtOption.Focus();
                    //    return;
                    //}


                }
            }
        }

        private void txt_BO_INC_AMOUNT_KeyDown(object sender, KeyEventArgs e)
        {
            

            //e.Handled = IsInvalidDouble((int)e.KeyCode);
            
        }
        private void txt_BO_INC_AMOUNT_Validating(object sender, CancelEventArgs e)
        {
            //if (txt_BO_INC_AMOUNT.Text != string.Empty)
            //{
            //    double amnt = 0;
            //    //this.Text = Convert.ToDouble(this.Text == "" ? "0" : this.Text).ToString(this._NumberFormat);
            //    double.TryParse(txt_BO_INC_AMOUNT.Text, out amnt);

            //    if (!double.TryParse(this.Text, out amnt))
            //    {
            //        txt_BO_INC_AMOUNT.Text = "";
            //        e.Cancel = true;
            //    }
            //    //if (amnt == 0)
            //    //{
            //    //    //this.Text = "";
            //    //}
            //}

        }
        private bool IsInvalidDouble(int keyCode)
        {
            bool isInvalid = true;
            Keys key = (Keys)keyCode;
            if (IsNumber(keyCode) || IsBackSpace(keyCode))
                isInvalid = false;
            else if (IsDot(keyCode))
            {
                int dotOccurrance = 0;

                foreach (char ch in txt_BO_INC_AMOUNT.Text) //this.Text)
                {
                    if (IsDot(ch))
                        dotOccurrance++;
                }

                isInvalid = dotOccurrance > 0;
            }

            return isInvalid;
        }
        protected bool IsNumber(int KeyCode)
        {
            if (KeyCode > 47 && KeyCode < 58)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// it validates the char type if text type is BackSpace
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsBackSpace(int KeyCode)
        {
            if (KeyCode == 8)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// it validates the char type if text type is DotOnly
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsDot(int KeyCode)
        {
            if (KeyCode == 46)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// it validates the char type if text type is IsSpecialChar
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsSpecialChar(int KeyCode)
        {
            if (KeyCode > 32 && KeyCode < 39 || KeyCode > 39 && KeyCode < 44 || KeyCode > 44 && KeyCode < 48 || KeyCode > 57 && KeyCode < 65 || KeyCode > 90 && KeyCode < 96 || KeyCode > 122 && KeyCode < 127)
                return true;
            else
                return false;
        }

        private void txt_BO_INC_AMOUNT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
            {
                Keys key = (Keys)e.KeyChar;
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }
                if (!(key == Keys.Back || key == Keys.Delete))
                {
                    e.Handled = true;
                }
            }
        }


        //private void CHRIS_Payroll_IncentiveBonusEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        //{
        //    if (actionType == "Pr_P_No_Lov")
        //    {
        //        this.txt_Pr_P_No.Select();
        //        this.txt_Pr_P_No.Focus();
        //        //this.txt_Pr_P_No_Validated(txt_Pr_P_No, null);
        //    }
        //}
        #endregion

        private void txt_BO_INC_AMOUNT_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHLMARKUP_KeyPress(object sender, KeyPressEventArgs e)
            {
            if (!(char.IsDigit(e.KeyChar)))
            {
                Keys key = (Keys)e.KeyChar;
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }
                if (!(key == Keys.Back || key == Keys.Delete))
                {
                    e.Handled = true;
                }
            }
        }

        private void slTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
            {
                Keys key = (Keys)e.KeyChar;
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }
                if (!(key == Keys.Back || key == Keys.Delete))
                {
                    e.Handled = true;
                }
            }
        }

        private void txtInvestments_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
            {
                Keys key = (Keys)e.KeyChar;
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }
                if (!(key == Keys.Back || key == Keys.Delete))
                {
                    e.Handled = true;
                }
            }
        }

        private void txtDonation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
            {
                Keys key = (Keys)e.KeyChar;
                if (e.KeyChar == '.')
                {
                    e.Handled = true;
                }
                if (!(key == Keys.Back || key == Keys.Delete))
                {
                    e.Handled = true;
                }
            }
        }

        private void txt_BO_INC_AMOUNT_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_BO_INC_AMOUNT.Text))
            {
                txt_BO_INC_AMOUNT.Text = "0";
            }
        }

        private void txtHLMARKUP_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtHLMARKUP.Text))
            {
                txtHLMARKUP.Text = "0";
            }
        }

        private void txtUtilities_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtUtilities.Text))
            {
                txtUtilities.Text = "0";
            }
        }

        private void txtInvestments_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtInvestments.Text))
            {
                txtInvestments.Text = "0";
            }
        }

        private void txtDonation_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDonation.Text))
            {
                txtDonation.Text = "0";
            }
        }
    }

}