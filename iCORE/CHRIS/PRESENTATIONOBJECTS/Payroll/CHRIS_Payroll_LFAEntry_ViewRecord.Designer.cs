namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_LFAEntry_ViewRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_LFAEntry_ViewRecord));
            this.gboLFA_TAB = new System.Windows.Forms.GroupBox();
            this.slPnlLFA_TAB = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.sldgvLFA_TAB = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.LF_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LF_LEAV_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LF_LFA_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LF_APPROVED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlLFA_TAB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldgvLFA_TAB)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Enabled = false;
            this.txtOption.Location = new System.Drawing.Point(588, 0);
            this.txtOption.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(624, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 395);
            this.panel1.Size = new System.Drawing.Size(624, 60);
            // 
            // gboLFA_TAB
            // 
            this.gboLFA_TAB.Location = new System.Drawing.Point(24, 39);
            this.gboLFA_TAB.Name = "gboLFA_TAB";
            this.gboLFA_TAB.Size = new System.Drawing.Size(600, 12);
            this.gboLFA_TAB.TabIndex = 10;
            this.gboLFA_TAB.TabStop = false;
            this.gboLFA_TAB.Visible = false;
            // 
            // slPnlLFA_TAB
            // 
            this.slPnlLFA_TAB.ConcurrentPanels = null;
            this.slPnlLFA_TAB.Controls.Add(this.sldgvLFA_TAB);
            this.slPnlLFA_TAB.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPnlLFA_TAB.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlLFA_TAB.DependentPanels = null;
            this.slPnlLFA_TAB.DisableDependentLoad = false;
            this.slPnlLFA_TAB.EnableDelete = true;
            this.slPnlLFA_TAB.EnableInsert = true;
            this.slPnlLFA_TAB.EnableQuery = true;
            this.slPnlLFA_TAB.EnableUpdate = true;
            this.slPnlLFA_TAB.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LFA_TABCommand";
            this.slPnlLFA_TAB.Location = new System.Drawing.Point(17, 76);
            this.slPnlLFA_TAB.MasterPanel = null;
            this.slPnlLFA_TAB.Name = "slPnlLFA_TAB";
            this.slPnlLFA_TAB.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlLFA_TAB.Size = new System.Drawing.Size(588, 295);
            this.slPnlLFA_TAB.SPName = "CHRIS_SP_LFA_TAB_MANAGER";
            this.slPnlLFA_TAB.TabIndex = 11;
            // 
            // sldgvLFA_TAB
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sldgvLFA_TAB.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.sldgvLFA_TAB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sldgvLFA_TAB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LF_P_NO,
            this.W_NAME,
            this.LF_LEAV_BAL,
            this.LF_LFA_AMOUNT,
            this.LF_APPROVED,
            this.ID});
            this.sldgvLFA_TAB.ColumnToHide = null;
            this.sldgvLFA_TAB.ColumnWidth = null;
            this.sldgvLFA_TAB.CustomEnabled = true;
            this.sldgvLFA_TAB.DisplayColumnWrapper = null;
            this.sldgvLFA_TAB.GridDefaultRow = 0;
            this.sldgvLFA_TAB.Location = new System.Drawing.Point(14, 8);
            this.sldgvLFA_TAB.Name = "sldgvLFA_TAB";
            this.sldgvLFA_TAB.ReadOnlyColumns = null;
            this.sldgvLFA_TAB.RequiredColumns = null;
            this.sldgvLFA_TAB.Size = new System.Drawing.Size(556, 274);
            this.sldgvLFA_TAB.SkippingColumns = null;
            this.sldgvLFA_TAB.TabIndex = 0;
            // 
            // LF_P_NO
            // 
            this.LF_P_NO.DataPropertyName = "LF_P_NO";
            this.LF_P_NO.HeaderText = "Personnel No";
            this.LF_P_NO.MaxInputLength = 6;
            this.LF_P_NO.Name = "LF_P_NO";
            this.LF_P_NO.ReadOnly = true;
            this.LF_P_NO.Width = 80;
            // 
            // W_NAME
            // 
            this.W_NAME.DataPropertyName = "PR_NAME";
            this.W_NAME.HeaderText = "Name";
            this.W_NAME.MaxInputLength = 25;
            this.W_NAME.Name = "W_NAME";
            this.W_NAME.ReadOnly = true;
            this.W_NAME.Width = 210;
            // 
            // LF_LEAV_BAL
            // 
            this.LF_LEAV_BAL.DataPropertyName = "LF_LEAV_BAL";
            this.LF_LEAV_BAL.HeaderText = "Leave Balance";
            this.LF_LEAV_BAL.MaxInputLength = 20;
            this.LF_LEAV_BAL.Name = "LF_LEAV_BAL";
            this.LF_LEAV_BAL.ReadOnly = true;
            this.LF_LEAV_BAL.Width = 80;
            // 
            // LF_LFA_AMOUNT
            // 
            this.LF_LFA_AMOUNT.DataPropertyName = "LF_LFA_AMOUNT";
            this.LF_LFA_AMOUNT.HeaderText = "LFA Amount";
            this.LF_LFA_AMOUNT.MaxInputLength = 10;
            this.LF_LFA_AMOUNT.Name = "LF_LFA_AMOUNT";
            this.LF_LFA_AMOUNT.ReadOnly = true;
            this.LF_LFA_AMOUNT.Width = 80;
            // 
            // LF_APPROVED
            // 
            this.LF_APPROVED.DataPropertyName = "LF_APPROVED";
            this.LF_APPROVED.HeaderText = "Approved [Y/N]";
            this.LF_APPROVED.MaxInputLength = 1;
            this.LF_APPROVED.Name = "LF_APPROVED";
            this.LF_APPROVED.ReadOnly = true;
            this.LF_APPROVED.Width = 65;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // CHRIS_Payroll_LFAEntry_ViewRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 455);
            this.Controls.Add(this.slPnlLFA_TAB);
            this.Controls.Add(this.gboLFA_TAB);
            this.CurrentPanelBlock = "slPnlLFA_TAB";
            this.F8OptionText = "";
            this.Name = "CHRIS_Payroll_LFAEntry_ViewRecord";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "Leave Fair Assistance";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CHRIS_Payroll_LFAEntry_ViewRecord_KeyPress);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CHRIS_Payroll_LFAEntry_ViewRecord_KeyDown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.gboLFA_TAB, 0);
            this.Controls.SetChildIndex(this.slPnlLFA_TAB, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlLFA_TAB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sldgvLFA_TAB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboLFA_TAB;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPnlLFA_TAB;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView sldgvLFA_TAB;
        private System.Windows.Forms.DataGridViewTextBoxColumn LF_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn LF_LEAV_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LF_LFA_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn LF_APPROVED;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}