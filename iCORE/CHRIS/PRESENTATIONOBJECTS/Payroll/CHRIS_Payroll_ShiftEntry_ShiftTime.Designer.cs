namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_ShiftEntry_ShiftTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.slPanelSimple2 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_WO3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_WI3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_WO2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_WI2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_WO1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_WI1 = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.slPanelSimple2.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.label1);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = false;
            this.slPanelSimple1.EnableInsert = false;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = false;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(52, 39);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(223, 50);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Shift Standard Timing\'s";
            // 
            // slPanelSimple2
            // 
            this.slPanelSimple2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelSimple2.ConcurrentPanels = null;
            this.slPanelSimple2.Controls.Add(this.label7);
            this.slPanelSimple2.Controls.Add(this.label6);
            this.slPanelSimple2.Controls.Add(this.label5);
            this.slPanelSimple2.Controls.Add(this.label4);
            this.slPanelSimple2.Controls.Add(this.label3);
            this.slPanelSimple2.Controls.Add(this.label2);
            this.slPanelSimple2.Controls.Add(this.txt_WO3);
            this.slPanelSimple2.Controls.Add(this.txt_WI3);
            this.slPanelSimple2.Controls.Add(this.txt_WO2);
            this.slPanelSimple2.Controls.Add(this.txt_WI2);
            this.slPanelSimple2.Controls.Add(this.txt_WO1);
            this.slPanelSimple2.Controls.Add(this.txt_WI1);
            this.slPanelSimple2.DataManager = null;
            this.slPanelSimple2.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple2.DependentPanels = null;
            this.slPanelSimple2.DisableDependentLoad = false;
            this.slPanelSimple2.EnableDelete = false;
            this.slPanelSimple2.EnableInsert = false;
            this.slPanelSimple2.EnableQuery = false;
            this.slPanelSimple2.EnableUpdate = false;
            this.slPanelSimple2.EntityName = null;
            this.slPanelSimple2.Location = new System.Drawing.Point(31, 95);
            this.slPanelSimple2.MasterPanel = null;
            this.slPanelSimple2.Name = "slPanelSimple2";
            this.slPanelSimple2.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple2.Size = new System.Drawing.Size(264, 139);
            this.slPanelSimple2.SPName = null;
            this.slPanelSimple2.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "[C]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "[B]";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "[A]";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(161, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Time Out";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(87, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Time In";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Shift";
            // 
            // txt_WO3
            // 
            this.txt_WO3.AllowSpace = true;
            this.txt_WO3.AssociatedLookUpName = "";
            this.txt_WO3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_WO3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_WO3.ContinuationTextBox = null;
            this.txt_WO3.CustomEnabled = true;
            this.txt_WO3.DataFieldMapping = "";
            this.txt_WO3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WO3.GetRecordsOnUpDownKeys = false;
            this.txt_WO3.IsDate = false;
            this.txt_WO3.Location = new System.Drawing.Point(160, 88);
            this.txt_WO3.MaxLength = 5;
            this.txt_WO3.Name = "txt_WO3";
            this.txt_WO3.NumberFormat = "###,###,##0.00";
            this.txt_WO3.Postfix = "";
            this.txt_WO3.Prefix = "";
            this.txt_WO3.Size = new System.Drawing.Size(61, 20);
            this.txt_WO3.SkipValidation = false;
            this.txt_WO3.TabIndex = 16;
            this.txt_WO3.TextType = CrplControlLibrary.TextType.String;
            this.txt_WO3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_WO3_PreviewKeyDown);
            this.txt_WO3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_WO3_KeyPress);
            this.txt_WO3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_WO3_Validating);
            // 
            // txt_WI3
            // 
            this.txt_WI3.AllowSpace = true;
            this.txt_WI3.AssociatedLookUpName = "";
            this.txt_WI3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_WI3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_WI3.ContinuationTextBox = null;
            this.txt_WI3.CustomEnabled = true;
            this.txt_WI3.DataFieldMapping = "";
            this.txt_WI3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WI3.GetRecordsOnUpDownKeys = false;
            this.txt_WI3.IsDate = false;
            this.txt_WI3.Location = new System.Drawing.Point(81, 88);
            this.txt_WI3.MaxLength = 5;
            this.txt_WI3.Name = "txt_WI3";
            this.txt_WI3.NumberFormat = "###,###,##0.00";
            this.txt_WI3.Postfix = "";
            this.txt_WI3.Prefix = "";
            this.txt_WI3.Size = new System.Drawing.Size(61, 20);
            this.txt_WI3.SkipValidation = false;
            this.txt_WI3.TabIndex = 15;
            this.txt_WI3.TextType = CrplControlLibrary.TextType.String;
            this.txt_WI3.Validating += new System.ComponentModel.CancelEventHandler(this.txt_WI3_Validating);
            // 
            // txt_WO2
            // 
            this.txt_WO2.AllowSpace = true;
            this.txt_WO2.AssociatedLookUpName = "";
            this.txt_WO2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_WO2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_WO2.ContinuationTextBox = null;
            this.txt_WO2.CustomEnabled = true;
            this.txt_WO2.DataFieldMapping = "";
            this.txt_WO2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WO2.GetRecordsOnUpDownKeys = false;
            this.txt_WO2.IsDate = false;
            this.txt_WO2.Location = new System.Drawing.Point(160, 62);
            this.txt_WO2.MaxLength = 5;
            this.txt_WO2.Name = "txt_WO2";
            this.txt_WO2.NumberFormat = "###,###,##0.00";
            this.txt_WO2.Postfix = "";
            this.txt_WO2.Prefix = "";
            this.txt_WO2.Size = new System.Drawing.Size(61, 20);
            this.txt_WO2.SkipValidation = false;
            this.txt_WO2.TabIndex = 14;
            this.txt_WO2.TextType = CrplControlLibrary.TextType.String;
            this.txt_WO2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_WO2_Validating);
            // 
            // txt_WI2
            // 
            this.txt_WI2.AllowSpace = true;
            this.txt_WI2.AssociatedLookUpName = "";
            this.txt_WI2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_WI2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_WI2.ContinuationTextBox = null;
            this.txt_WI2.CustomEnabled = true;
            this.txt_WI2.DataFieldMapping = "";
            this.txt_WI2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WI2.GetRecordsOnUpDownKeys = false;
            this.txt_WI2.IsDate = false;
            this.txt_WI2.Location = new System.Drawing.Point(81, 62);
            this.txt_WI2.MaxLength = 5;
            this.txt_WI2.Name = "txt_WI2";
            this.txt_WI2.NumberFormat = "###,###,##0.00";
            this.txt_WI2.Postfix = "";
            this.txt_WI2.Prefix = "";
            this.txt_WI2.Size = new System.Drawing.Size(61, 20);
            this.txt_WI2.SkipValidation = false;
            this.txt_WI2.TabIndex = 13;
            this.txt_WI2.TextType = CrplControlLibrary.TextType.String;
            this.txt_WI2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_WI2_Validating);
            // 
            // txt_WO1
            // 
            this.txt_WO1.AllowSpace = true;
            this.txt_WO1.AssociatedLookUpName = "";
            this.txt_WO1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_WO1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_WO1.ContinuationTextBox = null;
            this.txt_WO1.CustomEnabled = true;
            this.txt_WO1.DataFieldMapping = "";
            this.txt_WO1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WO1.GetRecordsOnUpDownKeys = false;
            this.txt_WO1.IsDate = false;
            this.txt_WO1.Location = new System.Drawing.Point(160, 34);
            this.txt_WO1.MaxLength = 5;
            this.txt_WO1.Name = "txt_WO1";
            this.txt_WO1.NumberFormat = "###,###,##0.00";
            this.txt_WO1.Postfix = "";
            this.txt_WO1.Prefix = "";
            this.txt_WO1.Size = new System.Drawing.Size(61, 20);
            this.txt_WO1.SkipValidation = false;
            this.txt_WO1.TabIndex = 12;
            this.txt_WO1.TextType = CrplControlLibrary.TextType.String;
            this.txt_WO1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_WO1_Validating);
            // 
            // txt_WI1
            // 
            this.txt_WI1.AllowSpace = true;
            this.txt_WI1.AssociatedLookUpName = "";
            this.txt_WI1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_WI1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_WI1.ContinuationTextBox = null;
            this.txt_WI1.CustomEnabled = true;
            this.txt_WI1.DataFieldMapping = "";
            this.txt_WI1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WI1.GetRecordsOnUpDownKeys = false;
            this.txt_WI1.IsDate = false;
            this.txt_WI1.Location = new System.Drawing.Point(81, 34);
            this.txt_WI1.MaxLength = 5;
            this.txt_WI1.Name = "txt_WI1";
            this.txt_WI1.NumberFormat = "###,###,##0.00";
            this.txt_WI1.Postfix = "";
            this.txt_WI1.Prefix = "";
            this.txt_WI1.Size = new System.Drawing.Size(61, 20);
            this.txt_WI1.SkipValidation = false;
            this.txt_WI1.TabIndex = 11;
            this.txt_WI1.TextType = CrplControlLibrary.TextType.String;
            this.txt_WI1.Validating += new System.ComponentModel.CancelEventHandler(this.txt_WI1_Validating);
            // 
            // CHRIS_Payroll_ShiftEntry_ShiftTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 248);
            this.Controls.Add(this.slPanelSimple2);
            this.Controls.Add(this.slPanelSimple1);
            this.Name = "CHRIS_Payroll_ShiftEntry_ShiftTime";
            this.Text = "CHRIS_Payroll_ShiftEntry_ShiftTime";
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.slPanelSimple2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.slPanelSimple2.ResumeLayout(false);
            this.slPanelSimple2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public CrplControlLibrary.SLTextBox txt_WI1;
        public CrplControlLibrary.SLTextBox txt_WO3;
        public CrplControlLibrary.SLTextBox txt_WI3;
        public CrplControlLibrary.SLTextBox txt_WO2;
        public CrplControlLibrary.SLTextBox txt_WI2;
        public CrplControlLibrary.SLTextBox txt_WO1;
    }
}