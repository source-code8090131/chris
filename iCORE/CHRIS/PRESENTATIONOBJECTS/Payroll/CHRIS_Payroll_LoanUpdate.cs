using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_LoanUpdate : MasterDetailForm
    {
        DataTable temp;
        // private CHRIS_Payroll_SalaryUpdate 
        public SLPanelSimpleList slPnlCopy;
        CHRIS_Payroll_PR111 ObjBasecopy = null;
        public string userNameText = string.Empty;

        #region Constructors

        public CHRIS_Payroll_LoanUpdate()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_LoanUpdate(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Payroll_PR111 menuBaseForm)//SLPanelSimpleList DataTable slPanelList_Deduction_Source
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            ObjBasecopy = menuBaseForm;
            ObjBasecopy.Hide();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelDetail);
            slPanel_Master.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(slPanel_Master);
        }

        #endregion

        #region Methods

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            slDatePicker_FN_END_DATE.Value = null;
            slDatePicker_FN_EXTRA_TIME.Value = null;
            slDatePicker_FN_START_DATE.Value = null;
            // dataGridView1.Columns[2].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvDetail.Columns["col_FN_MDATE"].DefaultCellStyle.Format = "dd/MM/yyyy";
            dgvDetail.Columns["col_FN_MDATE"].DefaultCellStyle.NullValue = "";
            this.lblUserName.Text = this.userNameText;
            this.tbtList.Visible = false;

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Search")
            {
                if (!string.IsNullOrEmpty(this.txt_FN_P_NO.Text))
                    (this.slPanel_Master.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand_PR111).PR_P_NO = double.Parse(this.txt_FN_P_NO.Text);
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Cancel")
                this.txt_FN_P_NO.Focus(); ;

        }



        #endregion

        #region Events

        private void dgvDetail_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            //{
            //    if (e.ColumnIndex == dgvDetail.Columns["col_FN_M_BRANCH"].Index)
            //    {
            //        if (dgvDetail["col_ID", e.RowIndex].Value != null && dgvDetail["col_ID", e.RowIndex].Value.ToString() != string.Empty)
            //        {
            //            dgvDetail[e.ColumnIndex, e.RowIndex].ReadOnly = true;
            //            //if(dgvDetail[e.ColumnIndex,e.RowIndex].IsInEditMode)
            //            //MessageBox.Show("Field is protected against update","Information",MessageBoxButtons.OK,MessageBoxIcon.Information,MessageBoxDefaultButton.Button1);
            //        }
            //    }
            //}
        }

        private void txt_FN_P_NO_Validated(object sender, EventArgs e)
        {

            //if ((slPanel_Master.CurrentBusinessEntity as FinBookingCommand_PR111).ID == 0)
            //{
            //    txt_FN_FINANCE_NO.ReadOnly = false;
            //    txt_FN_P_NO.ReadOnly = false;
            //    txt_FN_TYPE.ReadOnly = false;
            //    slDatePicker_FN_END_DATE.TabStop = true;
            //    slDatePicker_FN_START_DATE.TabStop = true;

            //    slDatePicker_FN_END_DATE.Enabled = true;
            //    slDatePicker_FN_START_DATE.Enabled = true;

            //}
            //else
            //{
            //    txt_FN_P_NO.ReadOnly = true;
            //    txt_FN_FINANCE_NO.ReadOnly = true;
            //    txt_FN_TYPE.ReadOnly = true;
            //    slDatePicker_FN_END_DATE.Enabled = false;
            //    slDatePicker_FN_START_DATE.Enabled = false;
            //    slDatePicker_FN_END_DATE.TabStop = false;
            //    slDatePicker_FN_START_DATE.TabStop = false;
            //}

        }

        private void dgvDetail_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (e.ColumnIndex == dgvDetail.Columns["col_FN_MDATE"].Index)
                {
                    //if (this.dgvDetail.CurrentCell.IsInEditMode)
                    //{
                    //    if (!(dgvDetail["col_FN_MDATE", e.RowIndex].EditedFormattedValue != null && dgvDetail["col_FN_MDATE", e.RowIndex].EditedFormattedValue.ToString() != string.Empty))
                    //    {
                    //        dgvDetail["col_FN_MDATE", e.RowIndex].Value = DBNull.Value;

                    //    }
                    //}
                }
                //if (e.ColumnIndex == dgvDetail.Columns["col_FN_M_BRANCH"].Index)
                //{
                //    if (dgvDetail["col_ID", e.RowIndex].Value != DBNull.Value && dgvDetail["col_ID", e.RowIndex].Value.ToString() != string.Empty)
                //    {
                //        this.dgvDetail.CurrentCell.Value = this.dgvDetail.CurrentCell.Value;
                //        //this.dgvDetail.CurrentCell.ReadOnly = true;//if (dgvDetail[e.ColumnIndex, e.RowIndex].ReadOnly)
                //        //MessageBox.Show("Field is protected against update", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                //    }

                //}
            }
        }

        private void txt_FN_FINANCE_NO_Validated(object sender, EventArgs e)
        {
            if ((slPanel_Master.CurrentBusinessEntity as FinBookingCommand_PR111).ID == 0)
            {
                slDatePicker_FN_END_DATE.Select();
                slDatePicker_FN_START_DATE.Focus();
                slDatePicker_FN_START_DATE.Value = DateTime.Now;
            }
            else
            {
                slDatePicker_FN_EXTRA_TIME.Focus();
            }

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            ObjBasecopy.Show();
            base.OnFormClosing(e);
        }

        protected override void MasterDetailForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
                this.Close();
            else
                base.MasterDetailForm_KeyUp(sender, e);
        }

        #endregion






    }
}