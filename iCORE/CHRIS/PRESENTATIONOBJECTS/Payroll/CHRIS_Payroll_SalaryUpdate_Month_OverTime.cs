using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using CrplControlLibrary;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_SalaryUpdate_Month_OverTime : SimpleForm
    {
        DataTable temp;
        // private CHRIS_Payroll_SalaryUpdate 
        public SLPanelSimpleList slPnlCopy;
        public string tempPrPNum = string.Empty;
        public CHRIS_Payroll_SalaryUpdate ObjSalBaseForm = new CHRIS_Payroll_SalaryUpdate();

        #region Constructors

        public CHRIS_Payroll_SalaryUpdate_Month_OverTime()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_SalaryUpdate_Month_OverTime(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, ref SLPanelSimpleList SourcePanel)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            slPnlCopy = SourcePanel;

        }

        #endregion

        #region Methods

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtAdd.Visible = false;
            tbtCancel.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtSave.Visible = false;
            tbtList.Visible = false;
            slPanelSimpleList_MonthOverTime.GridSource = slPnlCopy.GridSource;
            slPanelSimpleList_MonthOverTime.operationMode = slPnlCopy.operationMode;
            txt_tempPr_p_no.Text = tempPrPNum;
            //InitializeFormObject(slPanelSimpleList_MonthOverTime);
            //IterateFormControls(slPanelSimpleList_MonthOverTime.Controls, false, false, false);
            if (slPnlCopy.GridSource != null && slPnlCopy.GridSource.Rows.Count > 0)
            {
                this.SetPanelControlWithListItem(this.slPanelSimpleList_MonthOverTime.Controls, (this.slPanelSimpleList_MonthOverTime.GridSource as DataTable).Rows[0]);
            }
            if (slPnlCopy.GridSource != null && slPnlCopy.GridSource.Rows.Count == 0)
            {
                DataRow drow = slPanelSimpleList_MonthOverTime.GridSource.NewRow();
                slPanelSimpleList_MonthOverTime.GridSource.Rows.Add(drow);
                this.SetPanelControlWithListItem(this.slPanelSimpleList_MonthOverTime.Controls, (this.slPanelSimpleList_MonthOverTime.GridSource as DataTable).Rows[0]);
            }

            //slPanelList_Deduction1.GetValuesFromPanel(this.slPanelSimpleList_MonthOverTime.Controls);
            //this.IterateFormControls(this.slPanelSimpleList_MonthOverTime.Controls, false, false, false);
            txt_tempPr_p_no.Text = tempPrPNum;
        }

        #region Commented Code
        //protected override void BindLOVLookupButton(LookupButton poCprlLookupButton)
        //{
        //    if (poCprlLookupButton.Name == lookup_MO_SEGMENT_LOV4.Name)
        //    {
        //        try
        //        {
        //            Cmn_ListOfRecords sectionList = new Cmn_ListOfRecords(poCprlLookupButton.SPName, poCprlLookupButton.ActionType);

        //            BusinessEntity entity = RuntimeClassLoader.GetBusinessEntity("iCORE.Common.BusinessEntity");
        //            entity.SearchFilter = tempPrPNum;//(slPanelSimpleList_MonthOverTime.CurrentBusinessEntity as MonthOverTimeCommand).PR_P_NO.ToString();
        //            sectionList.ColumnToHide = poCprlLookupButton.HiddenColumns;
        //            sectionList.Entity = entity;
        //            sectionList.Id = this.m_intPKID;
        //            sectionList.Text = ((String.IsNullOrEmpty(poCprlLookupButton.LookUpTitle)) ? this.Text + " List" : poCprlLookupButton.LookUpTitle);
        //            sectionList.ListType = ListType.SetupList;
        //            //this.BeforeLOVShowing(sectionList, poCprlLookupButton.ActionType);
        //            if (sectionList.ShowDialog() == DialogResult.OK)
        //            {
        //                DataRow drw = sectionList.SelectedRecord;
        //                if (drw != null)
        //                {
        //                    this.SetPanelControlWithListItem(poCprlLookupButton.Parent.Controls, drw);
        //                    if (poCprlLookupButton.LoadDependentEntities)
        //                    {
        //                        slPanelSimpleList_MonthOverTime.SetEntityObject(drw);
        //                        //base.LoadEntityAndDependentPanels(drw);
        //                        //base.operationMode = Mode.Edit;
        //                    }
        //                    slPanelSimpleList_MonthOverTime.SetEntityObject(drw);
        //                }
        //                // base.LOVRecordSelected(drw, poCprlLookupButton.ActionType);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            this.LogException(this.GetType().Name, "ShowList", ex);
        //        }

        //    }
        //    else
        //        base.BindLOVLookupButton(poCprlLookupButton);


        //}

        //protected override void BindLOVShortCut(string pstrSPName, SLTextBox poSLTextBox)
        //{
        //    if (poSLTextBox.Name == txt_MO_SEGMENT.Name)
        //    {
        //        try
        //        {
        //            LookupButton lpButton = (poSLTextBox.Parent.Controls[poSLTextBox.AssociatedLookUpName] as LookupButton);
        //            Cmn_ListOfRecords sectionList = new Cmn_ListOfRecords(pstrSPName, lpButton.ActionType);

        //            BusinessEntity entity = RuntimeClassLoader.GetBusinessEntity("iCORE.Common.BusinessEntity");
        //            entity.SearchFilter = tempPrPNum;
        //            sectionList.ColumnToHide = lpButton.HiddenColumns;
        //            sectionList.Entity = entity;
        //            sectionList.Id = this.m_intPKID;
        //            sectionList.Text = ((String.IsNullOrEmpty(lpButton.LookUpTitle)) ? this.Text + " List" : lpButton.LookUpTitle);
        //            sectionList.ListType = ListType.SetupList;
        //            sectionList.SearchText = poSLTextBox.Text.Trim();
        //            sectionList.SearchColumn = poSLTextBox.DataFieldMapping.Trim();
        //            //this.BeforeLOVShowing(sectionList, lpButton.ActionType);
        //            if (sectionList.ShowDialog() == DialogResult.OK)
        //            {
        //                DataRow drw = sectionList.SelectedRecord;
        //                if (drw != null)
        //                {
        //                    this.SetPanelControlWithListItem(poSLTextBox.Parent.Controls, drw);
        //                    slPanelSimpleList_MonthOverTime.SetEntityObject(drw);
        //                }
        //                //this.LOVRecordSelected(drw, lpButton.ActionType);
        //            }
        //            else if (sectionList.DialogResult != DialogResult.OK)
        //            {
        //                base.ShowErrorMessage("Error", "Invalid value for field");
        //                poSLTextBox.Focus();
        //                poSLTextBox.SelectAll();
        //                //this.m_IsValidPage = false;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            this.LogException("BaseForm", "ShowList", ex);
        //        }
        //    }
        //    else
        //        base.BindLOVShortCut(pstrSPName, poSLTextBox);
        //}
        #endregion

        #endregion

        #region Event Handlers

        private void txt_MO_CONV_COST_Validated(object sender, EventArgs e)
        {
            //slPanelSimpleList_MonthOverTime.GetValuesFromPanel(slPanelSimpleList_MonthOverTime.Controls);
            
            ////ObjSalBaseForm.slPanelSimpleList_MonthOverTime.GridSource = this.slPanelSimpleList_MonthOverTime.GridSource;
            ////((iCORE.COMMON.SLCONTROLS.SLPanelSimpleList)(((System.Windows.Forms.Layout.ArrangedElementCollection)(((System.Windows.Forms.Control)(ObjSalBaseForm.grp_MonthOvertime)).Controls))[0])).GridSource = this.slPanelSimpleList_MonthOverTime.GridSource;
            ////((SLPanelSimpleList)(ObjSalBaseForm.Controls["slPanelSimpleList_MonthOverTime"])).GridSource = this.slPanelSimpleList_MonthOverTime.GridSource;
            //if (DialogResult.Yes == MessageBox.Show("Do You Want To Save The Above Record", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
            //{
            //    base.SetPanelControlWithListItem(this.ObjSalBaseForm.slPanelSimpleList_MonthOverTime.Controls, this.slPanelSimpleList_MonthOverTime.GridSource.Rows[this.ObjSalBaseForm.slPanelSimpleList_MonthOverTime.CurrentRowIndex]);
            //    this.ObjSalBaseForm.DoToolbarActions(ObjSalBaseForm.slPanelPersonnel.Controls, "Save");
            //}
            
            //this.Close();


            //this.slPanelSimpleList_MonthOverTime.GridSource;


        }
                
        private void txt_MO_CONV_COST_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (!(e.Shift))
            {
                if (e.KeyCode == Keys.Tab)
                {
                    e.IsInputKey = true;
                }
            }

        }

        private void txt_MO_CONV_COST_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\t' || e.KeyChar == 13)
            {
                slPanelSimpleList_MonthOverTime.GetValuesFromPanel(slPanelSimpleList_MonthOverTime.Controls);

                
                if (DialogResult.Yes == MessageBox.Show("Do You Want To Save The Above Record", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                {
                    base.SetPanelControlWithListItem(this.ObjSalBaseForm.slPanelSimpleList_MonthOverTime.Controls, this.slPanelSimpleList_MonthOverTime.GridSource.Rows[this.ObjSalBaseForm.slPanelSimpleList_MonthOverTime.CurrentRowIndex]);
                    this.ObjSalBaseForm.DoToolbarActions(ObjSalBaseForm.slPanelPersonnel.Controls, "Save");
                }

                this.Close();
 
            }
        }

        protected override void SimpleForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6)
            {
                this.Close();
                return;
            }
            else
                base.OnFormKeyup(sender, e);
        }

        #endregion
    }
}