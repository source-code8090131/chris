using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using CrplControlLibrary;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_PR111 : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> BonusValues = new Dictionary<string, object>();
        Result rslt;

        CHRIS_Payroll_PR111_Serial_No ObjSerialNum = null;
        CHRIS_Payroll_ArearUpdate ObjArearUpd = null;
        CHRIS_Payroll_LoanUpdate ObjLoanUpd = null;
        CHRIS_Payroll_SalaryUpdate ObjSalUpd = null;
        XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mMenu;

        bool _isErrorHandled = false;

        #region Constructors

        public CHRIS_Payroll_PR111()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_PR111(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            mMenu = mainmenu;
        }

        #endregion

        #region Methods

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtAdd.Visible = false;
            tbtCancel.Visible = false;
            //tbtClose.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
            txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
            this.txt_Option.Select();
            this.txt_Option.Focus();
        }

        #endregion

        #region Events

        private void txt_Option_KeyUp(object sender, KeyEventArgs e)
        {
            //if (!_isErrorHandled)
            //if( is SLTextBox)
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                {
                    if (txt_Option.Text.Length > 0)
                    {
                        if (txt_Option.Text == "1")
                        {
                            //**********************1 ...... SalaryUpdate (block1)*********************
                            _isErrorHandled = false;
                            ObjSalUpd = new CHRIS_Payroll_SalaryUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
                            ObjSalUpd.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
                            ObjSalUpd.Show();
                        }
                        else if (txt_Option.Text == "2")
                        {
                            //*******************2 ...... Arear (blkfour)******************************
                            _isErrorHandled = false;
                            ObjArearUpd = new CHRIS_Payroll_ArearUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
                            ObjArearUpd.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
                            ObjArearUpd.Show();
                        }
                        else if (txt_Option.Text == "3")
                        {
                            //************************3 ...... Loan (block8)***************************
                            _isErrorHandled = false;
                            ObjLoanUpd = new CHRIS_Payroll_LoanUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
                            ObjLoanUpd.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
                            ObjLoanUpd.userNameText = this.lblUserName.Text;
                            ObjLoanUpd.Show();

                        }
                        else if (txt_Option.Text == "4")
                        {
                            //************4 ...... Employee Number Update (SERIAL_NO)******************
                            _isErrorHandled = false;
                            //this.Hide();
                            ObjSerialNum = new CHRIS_Payroll_PR111_Serial_No(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
                            ObjSerialNum.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)); //(CHRIS_Payroll_PR111)this;//mMenu;//null;
                            ObjSerialNum.Show();

                        }
                        else if (txt_Option.Text == "5")
                        {
                            //************4 ...... Employee Tax View (View)******************
                            _isErrorHandled = false;
                            //this.Hide();
                            CHRIS_Payroll_View_Info objView_Info = new CHRIS_Payroll_View_Info(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
                            objView_Info.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)); //(CHRIS_Payroll_PR111)this;//mMenu;//null;
                            objView_Info.Show();

                        }
                        else if (txt_Option.Text == "0")
                        {
                            _isErrorHandled = false;
                            this.Close();
                        }
                        else
                        {
                            txt_Option.Text = "";
                            MessageBox.Show("Select Option In [1,2,3,4] or 0 To  Exit ....", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            _isErrorHandled = true;
                        }
                    }
                }

            }
            //else
            //{
            //    if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            //    {
            //        if (txt_Option.Text.Length > 0)
            //        {
            //            if (txt_Option.Text == "1")
            //            {
            //                //**********************1 ...... SalaryUpdate (block1)*********************
            //                _isErrorHandled = false;
            //                ObjSalUpd = new CHRIS_Payroll_SalaryUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
            //                ObjSalUpd.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
            //                ObjSalUpd.Show();
            //            }
            //            else if (txt_Option.Text == "2")
            //            {
            //                //*******************2 ...... Arear (blkfour)******************************
            //                _isErrorHandled = false;
            //                ObjArearUpd = new CHRIS_Payroll_ArearUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
            //                ObjArearUpd.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
            //                ObjArearUpd.Show();
            //            }
            //            else if (txt_Option.Text == "3")
            //            {
            //                //************************3 ...... Loan (block8)***************************
            //                _isErrorHandled = false;
            //                ObjLoanUpd = new CHRIS_Payroll_LoanUpdate(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
            //                ObjLoanUpd.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
            //                ObjLoanUpd.userNameText = this.lblUserName.Text;
            //                ObjLoanUpd.Show();

            //            }
            //            else if (txt_Option.Text == "4")
            //            {
            //                //************4 ...... Employee Number Update (SERIAL_NO)******************
            //                _isErrorHandled = false;
            //                //this.Hide();
            //                ObjSerialNum = new CHRIS_Payroll_PR111_Serial_No(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_PR111)this);
            //                ObjSerialNum.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)); //(CHRIS_Payroll_PR111)this;//mMenu;//null;
            //                ObjSerialNum.Show();

            //            }
            //            else if (txt_Option.Text == "0")
            //            {
            //                _isErrorHandled = false;
            //                this.Close();
            //            }
                        

            //        }

            //    }
            //}
        }





        #endregion
    }
}