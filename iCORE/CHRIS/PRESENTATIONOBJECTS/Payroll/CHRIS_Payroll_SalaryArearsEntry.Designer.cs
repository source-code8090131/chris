namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_SalaryArearsEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_SalaryArearsEntry));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lbl_UserName = new System.Windows.Forms.Label();
            this.slPanelSalaryProcess = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lookup_Branch = new CrplControlLibrary.LookupButton(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_ANS = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_NEW_MED = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_NEW_LFA = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_MED = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_LFA = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_CONV_AMT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_MEAL_AMT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_COLD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_MOLD = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker_W_FROM = new CrplControlLibrary.SLDatePicker(this.components);
            this.slDatePicker_W_TO = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.slPanelSalaryProcess.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 436);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.txtUser1);
            this.pnlHead.Controls.Add(this.txtLocation1);
            this.pnlHead.Controls.Add(this.label33);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label34);
            this.pnlHead.Controls.Add(this.label35);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label36);
            this.pnlHead.Controls.Add(this.label37);
            this.pnlHead.Location = new System.Drawing.Point(12, 79);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(645, 52);
            this.pnlHead.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(29, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "User :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 53;
            this.label2.Text = "Location :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // txtUser1
            // 
            this.txtUser1.AllowSpace = true;
            this.txtUser1.AssociatedLookUpName = "";
            this.txtUser1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser1.ContinuationTextBox = null;
            this.txtUser1.CustomEnabled = false;
            this.txtUser1.DataFieldMapping = "";
            this.txtUser1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser1.GetRecordsOnUpDownKeys = false;
            this.txtUser1.IsDate = false;
            this.txtUser1.Location = new System.Drawing.Point(89, 7);
            this.txtUser1.MaxLength = 10;
            this.txtUser1.Name = "txtUser1";
            this.txtUser1.NumberFormat = "###,###,##0.00";
            this.txtUser1.Postfix = "";
            this.txtUser1.Prefix = "";
            this.txtUser1.ReadOnly = true;
            this.txtUser1.Size = new System.Drawing.Size(87, 20);
            this.txtUser1.SkipValidation = false;
            this.txtUser1.TabIndex = 21;
            this.txtUser1.TabStop = false;
            this.txtUser1.TextType = CrplControlLibrary.TextType.String;
            this.txtUser1.Visible = false;
            // 
            // txtLocation1
            // 
            this.txtLocation1.AllowSpace = true;
            this.txtLocation1.AssociatedLookUpName = "";
            this.txtLocation1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation1.ContinuationTextBox = null;
            this.txtLocation1.CustomEnabled = false;
            this.txtLocation1.DataFieldMapping = "";
            this.txtLocation1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation1.GetRecordsOnUpDownKeys = false;
            this.txtLocation1.IsDate = false;
            this.txtLocation1.Location = new System.Drawing.Point(89, 29);
            this.txtLocation1.MaxLength = 10;
            this.txtLocation1.Name = "txtLocation1";
            this.txtLocation1.NumberFormat = "###,###,##0.00";
            this.txtLocation1.Postfix = "";
            this.txtLocation1.Prefix = "";
            this.txtLocation1.ReadOnly = true;
            this.txtLocation1.Size = new System.Drawing.Size(87, 20);
            this.txtLocation1.SkipValidation = false;
            this.txtLocation1.TabIndex = 13;
            this.txtLocation1.TabStop = false;
            this.txtLocation1.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation1.Visible = false;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(184, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 30);
            this.label33.TabIndex = 19;
            this.label33.Text = "Union Arrears Processing ";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(556, 27);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(501, 77);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(498, 28);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 15);
            this.label34.TabIndex = 16;
            this.label34.Text = "Date:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(450, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 15);
            this.label35.TabIndex = 15;
            this.label35.Text = "Option:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label35.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(83, 87);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(83, 61);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(7, 87);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 20);
            this.label36.TabIndex = 2;
            this.label36.Text = "Location:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(16, 61);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 20);
            this.label37.TabIndex = 1;
            this.label37.Text = "User:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label37.Visible = false;
            // 
            // lbl_UserName
            // 
            this.lbl_UserName.AutoSize = true;
            this.lbl_UserName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_UserName.Location = new System.Drawing.Point(421, 4);
            this.lbl_UserName.Name = "lbl_UserName";
            this.lbl_UserName.Size = new System.Drawing.Size(35, 13);
            this.lbl_UserName.TabIndex = 12;
            this.lbl_UserName.Text = "label4";
            // 
            // slPanelSalaryProcess
            // 
            this.slPanelSalaryProcess.ConcurrentPanels = null;
            this.slPanelSalaryProcess.Controls.Add(this.lookup_Branch);
            this.slPanelSalaryProcess.Controls.Add(this.txt_PR_P_NO);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_ANS);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_NEW_MED);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_NEW_LFA);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_MED);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_LFA);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_CONV_AMT);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_MEAL_AMT);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_COLD);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_MOLD);
            this.slPanelSalaryProcess.Controls.Add(this.slDatePicker_W_FROM);
            this.slPanelSalaryProcess.Controls.Add(this.slDatePicker_W_TO);
            this.slPanelSalaryProcess.Controls.Add(this.txt_W_BRN);
            this.slPanelSalaryProcess.Controls.Add(this.label16);
            this.slPanelSalaryProcess.Controls.Add(this.label14);
            this.slPanelSalaryProcess.Controls.Add(this.label6);
            this.slPanelSalaryProcess.Controls.Add(this.label7);
            this.slPanelSalaryProcess.Controls.Add(this.label8);
            this.slPanelSalaryProcess.Controls.Add(this.label13);
            this.slPanelSalaryProcess.Controls.Add(this.label15);
            this.slPanelSalaryProcess.Controls.Add(this.label12);
            this.slPanelSalaryProcess.Controls.Add(this.label11);
            this.slPanelSalaryProcess.Controls.Add(this.label10);
            this.slPanelSalaryProcess.Controls.Add(this.label9);
            this.slPanelSalaryProcess.Controls.Add(this.label5);
            this.slPanelSalaryProcess.Controls.Add(this.label4);
            this.slPanelSalaryProcess.DataManager = null;
            this.slPanelSalaryProcess.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSalaryProcess.DependentPanels = null;
            this.slPanelSalaryProcess.DisableDependentLoad = false;
            this.slPanelSalaryProcess.EnableDelete = true;
            this.slPanelSalaryProcess.EnableInsert = true;
            this.slPanelSalaryProcess.EnableQuery = false;
            this.slPanelSalaryProcess.EnableUpdate = true;
            this.slPanelSalaryProcess.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BranchCommand";
            this.slPanelSalaryProcess.Location = new System.Drawing.Point(12, 140);
            this.slPanelSalaryProcess.MasterPanel = null;
            this.slPanelSalaryProcess.Name = "slPanelSalaryProcess";
            this.slPanelSalaryProcess.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSalaryProcess.Size = new System.Drawing.Size(645, 293);
            this.slPanelSalaryProcess.SPName = null;
            this.slPanelSalaryProcess.TabIndex = 13;
            // 
            // lookup_Branch
            // 
            this.lookup_Branch.ActionLOVExists = "GetBranchLovExist";
            this.lookup_Branch.ActionType = "GetBranchLov";
            this.lookup_Branch.ConditionalFields = "";
            this.lookup_Branch.CustomEnabled = true;
            this.lookup_Branch.DataFieldMapping = "";
            this.lookup_Branch.DependentLovControls = "";
            this.lookup_Branch.HiddenColumns = "";
            this.lookup_Branch.Image = ((System.Drawing.Image)(resources.GetObject("lookup_Branch.Image")));
            this.lookup_Branch.LoadDependentEntities = false;
            this.lookup_Branch.Location = new System.Drawing.Point(249, 70);
            this.lookup_Branch.LookUpTitle = "Valid Branch Codes";
            this.lookup_Branch.Name = "lookup_Branch";
            this.lookup_Branch.Size = new System.Drawing.Size(26, 21);
            this.lookup_Branch.SkipValidationOnLeave = false;
            this.lookup_Branch.SPName = "CHRIS_SP_BRANCH_MANAGER";
            this.lookup_Branch.TabIndex = 34;
            this.lookup_Branch.TabStop = false;
            this.lookup_Branch.UseVisualStyleBackColor = true;
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(396, 263);
            this.txt_PR_P_NO.MaxLength = 6;
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.ReadOnly = true;
            this.txt_PR_P_NO.Size = new System.Drawing.Size(45, 20);
            this.txt_PR_P_NO.SkipValidation = false;
            this.txt_PR_P_NO.TabIndex = 12;
            this.txt_PR_P_NO.TabStop = false;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_ANS
            // 
            this.txt_W_ANS.AllowSpace = true;
            this.txt_W_ANS.AssociatedLookUpName = "";
            this.txt_W_ANS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_ANS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_ANS.ContinuationTextBox = null;
            this.txt_W_ANS.CustomEnabled = true;
            this.txt_W_ANS.DataFieldMapping = "";
            this.txt_W_ANS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_ANS.GetRecordsOnUpDownKeys = false;
            this.txt_W_ANS.IsDate = false;
            this.txt_W_ANS.IsRequired = true;
            this.txt_W_ANS.Location = new System.Drawing.Point(418, 233);
            this.txt_W_ANS.MaxLength = 1;
            this.txt_W_ANS.Name = "txt_W_ANS";
            this.txt_W_ANS.NumberFormat = "###,###,##0.00";
            this.txt_W_ANS.Postfix = "";
            this.txt_W_ANS.Prefix = "";
            this.txt_W_ANS.Size = new System.Drawing.Size(23, 20);
            this.txt_W_ANS.SkipValidation = false;
            this.txt_W_ANS.TabIndex = 11;
            this.txt_W_ANS.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_ANS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_W_ANS_KeyPress);
            this.txt_W_ANS.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_ANS_Validating);
            // 
            // txt_W_NEW_MED
            // 
            this.txt_W_NEW_MED.AllowSpace = true;
            this.txt_W_NEW_MED.AssociatedLookUpName = "";
            this.txt_W_NEW_MED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_NEW_MED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_NEW_MED.ContinuationTextBox = null;
            this.txt_W_NEW_MED.CustomEnabled = true;
            this.txt_W_NEW_MED.DataFieldMapping = "";
            this.txt_W_NEW_MED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_NEW_MED.GetRecordsOnUpDownKeys = false;
            this.txt_W_NEW_MED.IsDate = false;
            this.txt_W_NEW_MED.Location = new System.Drawing.Point(452, 171);
            this.txt_W_NEW_MED.MaxLength = 6;
            this.txt_W_NEW_MED.Name = "txt_W_NEW_MED";
            this.txt_W_NEW_MED.NumberFormat = "###,###,##0.00";
            this.txt_W_NEW_MED.Postfix = "";
            this.txt_W_NEW_MED.Prefix = "";
            this.txt_W_NEW_MED.Size = new System.Drawing.Size(87, 20);
            this.txt_W_NEW_MED.SkipValidation = false;
            this.txt_W_NEW_MED.TabIndex = 10;
            this.txt_W_NEW_MED.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_W_NEW_LFA
            // 
            this.txt_W_NEW_LFA.AllowSpace = true;
            this.txt_W_NEW_LFA.AssociatedLookUpName = "";
            this.txt_W_NEW_LFA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_NEW_LFA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_NEW_LFA.ContinuationTextBox = null;
            this.txt_W_NEW_LFA.CustomEnabled = true;
            this.txt_W_NEW_LFA.DataFieldMapping = "";
            this.txt_W_NEW_LFA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_NEW_LFA.GetRecordsOnUpDownKeys = false;
            this.txt_W_NEW_LFA.IsDate = false;
            this.txt_W_NEW_LFA.Location = new System.Drawing.Point(452, 148);
            this.txt_W_NEW_LFA.MaxLength = 6;
            this.txt_W_NEW_LFA.Name = "txt_W_NEW_LFA";
            this.txt_W_NEW_LFA.NumberFormat = "###,###,##0.00";
            this.txt_W_NEW_LFA.Postfix = "";
            this.txt_W_NEW_LFA.Prefix = "";
            this.txt_W_NEW_LFA.Size = new System.Drawing.Size(87, 20);
            this.txt_W_NEW_LFA.SkipValidation = false;
            this.txt_W_NEW_LFA.TabIndex = 8;
            this.txt_W_NEW_LFA.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_W_MED
            // 
            this.txt_W_MED.AllowSpace = true;
            this.txt_W_MED.AssociatedLookUpName = "";
            this.txt_W_MED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_MED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_MED.ContinuationTextBox = null;
            this.txt_W_MED.CustomEnabled = true;
            this.txt_W_MED.DataFieldMapping = "";
            this.txt_W_MED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_MED.GetRecordsOnUpDownKeys = false;
            this.txt_W_MED.IsDate = false;
            this.txt_W_MED.Location = new System.Drawing.Point(197, 174);
            this.txt_W_MED.MaxLength = 6;
            this.txt_W_MED.Name = "txt_W_MED";
            this.txt_W_MED.NumberFormat = "###,###,##0.00";
            this.txt_W_MED.Postfix = "";
            this.txt_W_MED.Prefix = "";
            this.txt_W_MED.Size = new System.Drawing.Size(85, 20);
            this.txt_W_MED.SkipValidation = false;
            this.txt_W_MED.TabIndex = 9;
            this.txt_W_MED.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_W_LFA
            // 
            this.txt_W_LFA.AllowSpace = true;
            this.txt_W_LFA.AssociatedLookUpName = "";
            this.txt_W_LFA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_LFA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_LFA.ContinuationTextBox = null;
            this.txt_W_LFA.CustomEnabled = true;
            this.txt_W_LFA.DataFieldMapping = "";
            this.txt_W_LFA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_LFA.GetRecordsOnUpDownKeys = false;
            this.txt_W_LFA.IsDate = false;
            this.txt_W_LFA.Location = new System.Drawing.Point(197, 150);
            this.txt_W_LFA.MaxLength = 6;
            this.txt_W_LFA.Name = "txt_W_LFA";
            this.txt_W_LFA.NumberFormat = "###,###,##0.00";
            this.txt_W_LFA.Postfix = "";
            this.txt_W_LFA.Prefix = "";
            this.txt_W_LFA.Size = new System.Drawing.Size(85, 20);
            this.txt_W_LFA.SkipValidation = false;
            this.txt_W_LFA.TabIndex = 7;
            this.txt_W_LFA.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_W_CONV_AMT
            // 
            this.txt_W_CONV_AMT.AllowSpace = true;
            this.txt_W_CONV_AMT.AssociatedLookUpName = "";
            this.txt_W_CONV_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_CONV_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_CONV_AMT.ContinuationTextBox = null;
            this.txt_W_CONV_AMT.CustomEnabled = true;
            this.txt_W_CONV_AMT.DataFieldMapping = "";
            this.txt_W_CONV_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_CONV_AMT.GetRecordsOnUpDownKeys = false;
            this.txt_W_CONV_AMT.IsDate = false;
            this.txt_W_CONV_AMT.IsRequired = true;
            this.txt_W_CONV_AMT.Location = new System.Drawing.Point(452, 125);
            this.txt_W_CONV_AMT.MaxLength = 3;
            this.txt_W_CONV_AMT.Name = "txt_W_CONV_AMT";
            this.txt_W_CONV_AMT.NumberFormat = "###,###,##0.00";
            this.txt_W_CONV_AMT.Postfix = "";
            this.txt_W_CONV_AMT.Prefix = "";
            this.txt_W_CONV_AMT.Size = new System.Drawing.Size(46, 20);
            this.txt_W_CONV_AMT.SkipValidation = false;
            this.txt_W_CONV_AMT.TabIndex = 6;
            this.txt_W_CONV_AMT.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_W_CONV_AMT.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_CONV_AMT_Validating);
            // 
            // txt_W_MEAL_AMT
            // 
            this.txt_W_MEAL_AMT.AllowSpace = true;
            this.txt_W_MEAL_AMT.AssociatedLookUpName = "";
            this.txt_W_MEAL_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_MEAL_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_MEAL_AMT.ContinuationTextBox = null;
            this.txt_W_MEAL_AMT.CustomEnabled = true;
            this.txt_W_MEAL_AMT.DataFieldMapping = "";
            this.txt_W_MEAL_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_MEAL_AMT.GetRecordsOnUpDownKeys = false;
            this.txt_W_MEAL_AMT.IsDate = false;
            this.txt_W_MEAL_AMT.IsRequired = true;
            this.txt_W_MEAL_AMT.Location = new System.Drawing.Point(452, 102);
            this.txt_W_MEAL_AMT.MaxLength = 3;
            this.txt_W_MEAL_AMT.Name = "txt_W_MEAL_AMT";
            this.txt_W_MEAL_AMT.NumberFormat = "###,###,##0.00";
            this.txt_W_MEAL_AMT.Postfix = "";
            this.txt_W_MEAL_AMT.Prefix = "";
            this.txt_W_MEAL_AMT.Size = new System.Drawing.Size(46, 20);
            this.txt_W_MEAL_AMT.SkipValidation = false;
            this.txt_W_MEAL_AMT.TabIndex = 5;
            this.txt_W_MEAL_AMT.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_W_MEAL_AMT.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_MEAL_AMT_Validating);
            // 
            // txt_W_COLD
            // 
            this.txt_W_COLD.AllowSpace = true;
            this.txt_W_COLD.AssociatedLookUpName = "";
            this.txt_W_COLD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_COLD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_COLD.ContinuationTextBox = null;
            this.txt_W_COLD.CustomEnabled = true;
            this.txt_W_COLD.DataFieldMapping = "";
            this.txt_W_COLD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_COLD.GetRecordsOnUpDownKeys = false;
            this.txt_W_COLD.IsDate = false;
            this.txt_W_COLD.IsRequired = true;
            this.txt_W_COLD.Location = new System.Drawing.Point(197, 126);
            this.txt_W_COLD.MaxLength = 3;
            this.txt_W_COLD.Name = "txt_W_COLD";
            this.txt_W_COLD.NumberFormat = "###,###,##0.00";
            this.txt_W_COLD.Postfix = "";
            this.txt_W_COLD.Prefix = "";
            this.txt_W_COLD.Size = new System.Drawing.Size(46, 20);
            this.txt_W_COLD.SkipValidation = false;
            this.txt_W_COLD.TabIndex = 4;
            this.txt_W_COLD.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_W_COLD.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_COLD_Validating);
            // 
            // txt_W_MOLD
            // 
            this.txt_W_MOLD.AllowSpace = true;
            this.txt_W_MOLD.AssociatedLookUpName = "";
            this.txt_W_MOLD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_MOLD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_MOLD.ContinuationTextBox = null;
            this.txt_W_MOLD.CustomEnabled = true;
            this.txt_W_MOLD.DataFieldMapping = "";
            this.txt_W_MOLD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_MOLD.GetRecordsOnUpDownKeys = false;
            this.txt_W_MOLD.IsDate = false;
            this.txt_W_MOLD.IsRequired = true;
            this.txt_W_MOLD.Location = new System.Drawing.Point(197, 102);
            this.txt_W_MOLD.MaxLength = 3;
            this.txt_W_MOLD.Name = "txt_W_MOLD";
            this.txt_W_MOLD.NumberFormat = "###,###,##0.00";
            this.txt_W_MOLD.Postfix = "";
            this.txt_W_MOLD.Prefix = "";
            this.txt_W_MOLD.Size = new System.Drawing.Size(46, 20);
            this.txt_W_MOLD.SkipValidation = false;
            this.txt_W_MOLD.TabIndex = 3;
            this.txt_W_MOLD.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_W_MOLD.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_MOLD_Validating);
            // 
            // slDatePicker_W_FROM
            // 
            this.slDatePicker_W_FROM.CustomEnabled = true;
            this.slDatePicker_W_FROM.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_W_FROM.DataFieldMapping = "";
            this.slDatePicker_W_FROM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_W_FROM.HasChanges = true;
            this.slDatePicker_W_FROM.Location = new System.Drawing.Point(197, 40);
            this.slDatePicker_W_FROM.Name = "slDatePicker_W_FROM";
            this.slDatePicker_W_FROM.NullValue = " ";
            this.slDatePicker_W_FROM.Size = new System.Drawing.Size(85, 20);
            this.slDatePicker_W_FROM.TabIndex = 0;
            this.slDatePicker_W_FROM.Value = new System.DateTime(2011, 1, 22, 0, 0, 0, 0);
            // 
            // slDatePicker_W_TO
            // 
            this.slDatePicker_W_TO.CustomEnabled = true;
            this.slDatePicker_W_TO.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_W_TO.DataFieldMapping = "";
            this.slDatePicker_W_TO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_W_TO.HasChanges = true;
            this.slDatePicker_W_TO.Location = new System.Drawing.Point(452, 40);
            this.slDatePicker_W_TO.Name = "slDatePicker_W_TO";
            this.slDatePicker_W_TO.NullValue = " ";
            this.slDatePicker_W_TO.Size = new System.Drawing.Size(87, 20);
            this.slDatePicker_W_TO.TabIndex = 1;
            this.slDatePicker_W_TO.Value = new System.DateTime(2011, 1, 22, 0, 0, 0, 0);
            // 
            // txt_W_BRN
            // 
            this.txt_W_BRN.AllowSpace = true;
            this.txt_W_BRN.AssociatedLookUpName = "lookup_Branch";
            this.txt_W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_BRN.ContinuationTextBox = null;
            this.txt_W_BRN.CustomEnabled = true;
            this.txt_W_BRN.DataFieldMapping = "BRN_CODE";
            this.txt_W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_BRN.GetRecordsOnUpDownKeys = false;
            this.txt_W_BRN.IsDate = false;
            this.txt_W_BRN.IsLookUpField = true;
            this.txt_W_BRN.IsRequired = true;
            this.txt_W_BRN.Location = new System.Drawing.Point(198, 72);
            this.txt_W_BRN.MaxLength = 3;
            this.txt_W_BRN.Name = "txt_W_BRN";
            this.txt_W_BRN.NumberFormat = "###,###,##0.00";
            this.txt_W_BRN.Postfix = "";
            this.txt_W_BRN.Prefix = "";
            this.txt_W_BRN.Size = new System.Drawing.Size(45, 20);
            this.txt_W_BRN.SkipValidation = false;
            this.txt_W_BRN.TabIndex = 2;
            this.txt_W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(178, 268);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(179, 15);
            this.label16.TabIndex = 33;
            this.label16.Text = "Processing  Employee No. ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(178, 233);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(182, 15);
            this.label14.TabIndex = 32;
            this.label14.Text = "Start Processing          [Y/N]";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(362, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "New Meal :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(363, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 15);
            this.label7.TabIndex = 30;
            this.label7.Text = "New Conv :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(370, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 29;
            this.label8.Text = "New LFA :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(346, 177);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 15);
            this.label13.TabIndex = 28;
            this.label13.Text = "New Medical :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(367, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 15);
            this.label15.TabIndex = 26;
            this.label15.Text = "End Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(109, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 15);
            this.label12.TabIndex = 25;
            this.label12.Text = "Old Meal :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(110, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 24;
            this.label11.Text = "Old Conv :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(117, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 15);
            this.label10.TabIndex = 23;
            this.label10.Text = "Old LFA :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(90, 176);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 15);
            this.label9.TabIndex = 22;
            this.label9.Text = "Old Medical :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(122, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "Branch :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(100, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = "Start From :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CHRIS_Payroll_SalaryArearsEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 496);
            this.Controls.Add(this.slPanelSalaryProcess);
            this.Controls.Add(this.lbl_UserName);
            this.Controls.Add(this.pnlHead);
            this.Name = "CHRIS_Payroll_SalaryArearsEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Payroll_SalaryEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lbl_UserName, 0);
            this.Controls.SetChildIndex(this.slPanelSalaryProcess, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.slPanelSalaryProcess.ResumeLayout(false);
            this.slPanelSalaryProcess.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtUser1;
        private CrplControlLibrary.SLTextBox txtLocation1;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lbl_UserName;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSalaryProcess;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLDatePicker slDatePicker_W_FROM;
        private CrplControlLibrary.SLDatePicker slDatePicker_W_TO;
        private CrplControlLibrary.SLTextBox txt_W_BRN;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txt_W_CONV_AMT;
        private CrplControlLibrary.SLTextBox txt_W_MEAL_AMT;
        private CrplControlLibrary.SLTextBox txt_W_COLD;
        private CrplControlLibrary.SLTextBox txt_W_MOLD;
        private CrplControlLibrary.SLTextBox txt_W_NEW_MED;
        private CrplControlLibrary.SLTextBox txt_W_NEW_LFA;
        private CrplControlLibrary.SLTextBox txt_W_MED;
        private CrplControlLibrary.SLTextBox txt_W_LFA;
        private CrplControlLibrary.SLTextBox txt_PR_P_NO;
        private CrplControlLibrary.SLTextBox txt_W_ANS;
        private CrplControlLibrary.LookupButton lookup_Branch;
    }
}