namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_ShiftEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_ShiftEntry));
            this.slPanelMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_category = new CrplControlLibrary.SLTextBox(this.components);
            this.lookup_Pr_p_No = new CrplControlLibrary.LookupButton(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txt_PName = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Pr_P_No = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Month = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Year = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUser1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation1 = new CrplControlLibrary.SLTextBox(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 201);
            // 
            // slPanelMain
            // 
            this.slPanelMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanelMain.ConcurrentPanels = null;
            this.slPanelMain.Controls.Add(this.txt_category);
            this.slPanelMain.Controls.Add(this.lookup_Pr_p_No);
            this.slPanelMain.Controls.Add(this.label3);
            this.slPanelMain.Controls.Add(this.txt_PName);
            this.slPanelMain.Controls.Add(this.label5);
            this.slPanelMain.Controls.Add(this.txt_Pr_P_No);
            this.slPanelMain.Controls.Add(this.label2);
            this.slPanelMain.Controls.Add(this.txt_Month);
            this.slPanelMain.Controls.Add(this.txt_Year);
            this.slPanelMain.Controls.Add(this.label4);
            this.slPanelMain.Controls.Add(this.label1);
            this.slPanelMain.Controls.Add(this.txtUser1);
            this.slPanelMain.Controls.Add(this.txtLocation1);
            this.slPanelMain.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPanelMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelMain.DependentPanels = null;
            this.slPanelMain.DisableDependentLoad = false;
            this.slPanelMain.EnableDelete = false;
            this.slPanelMain.EnableInsert = false;
            this.slPanelMain.EnableQuery = false;
            this.slPanelMain.EnableUpdate = false;
            this.slPanelMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.slPanelMain.Location = new System.Drawing.Point(12, 105);
            this.slPanelMain.MasterPanel = null;
            this.slPanelMain.Name = "slPanelMain";
            this.slPanelMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelMain.Size = new System.Drawing.Size(620, 75);
            this.slPanelMain.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.slPanelMain.TabIndex = 0;
            this.slPanelMain.TabStop = true;
            // 
            // txt_category
            // 
            this.txt_category.AllowSpace = true;
            this.txt_category.AssociatedLookUpName = "";
            this.txt_category.BackColor = System.Drawing.SystemColors.Window;
            this.txt_category.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_category.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_category.ContinuationTextBox = null;
            this.txt_category.CustomEnabled = true;
            this.txt_category.DataFieldMapping = "pr_category";
            this.txt_category.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_category.GetRecordsOnUpDownKeys = false;
            this.txt_category.IsDate = false;
            this.txt_category.Location = new System.Drawing.Point(1, 50);
            this.txt_category.MaxLength = 10;
            this.txt_category.Name = "txt_category";
            this.txt_category.NumberFormat = "###,###,##0.00";
            this.txt_category.Postfix = "";
            this.txt_category.Prefix = "";
            this.txt_category.ReadOnly = true;
            this.txt_category.Size = new System.Drawing.Size(87, 20);
            this.txt_category.SkipValidation = false;
            this.txt_category.TabIndex = 33;
            this.txt_category.TabStop = false;
            this.txt_category.TextType = CrplControlLibrary.TextType.String;
            this.txt_category.Visible = false;
            // 
            // lookup_Pr_p_No
            // 
            this.lookup_Pr_p_No.ActionLOVExists = "Pr_P_No_Lov_Shift_Exist";
            this.lookup_Pr_p_No.ActionType = "Pr_P_No_Lov_Shift";
            this.lookup_Pr_p_No.ConditionalFields = "";
            this.lookup_Pr_p_No.CustomEnabled = true;
            this.lookup_Pr_p_No.DataFieldMapping = "";
            this.lookup_Pr_p_No.DependentLovControls = "";
            this.lookup_Pr_p_No.HiddenColumns = "pr_category";
            this.lookup_Pr_p_No.Image = ((System.Drawing.Image)(resources.GetObject("lookup_Pr_p_No.Image")));
            this.lookup_Pr_p_No.LoadDependentEntities = false;
            this.lookup_Pr_p_No.Location = new System.Drawing.Point(175, 35);
            this.lookup_Pr_p_No.LookUpTitle = null;
            this.lookup_Pr_p_No.Name = "lookup_Pr_p_No";
            this.lookup_Pr_p_No.Size = new System.Drawing.Size(26, 21);
            this.lookup_Pr_p_No.SkipValidationOnLeave = false;
            this.lookup_Pr_p_No.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.lookup_Pr_p_No.TabIndex = 4;
            this.lookup_Pr_p_No.TabStop = false;
            this.lookup_Pr_p_No.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(220, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Name";
            // 
            // txt_PName
            // 
            this.txt_PName.AllowSpace = true;
            this.txt_PName.AssociatedLookUpName = "";
            this.txt_PName.BackColor = System.Drawing.SystemColors.Window;
            this.txt_PName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PName.ContinuationTextBox = null;
            this.txt_PName.CustomEnabled = true;
            this.txt_PName.DataFieldMapping = "PNAME";
            this.txt_PName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PName.GetRecordsOnUpDownKeys = false;
            this.txt_PName.IsDate = false;
            this.txt_PName.Location = new System.Drawing.Point(263, 37);
            this.txt_PName.Name = "txt_PName";
            this.txt_PName.NumberFormat = "###,###,##0.00";
            this.txt_PName.Postfix = "";
            this.txt_PName.Prefix = "";
            this.txt_PName.ReadOnly = true;
            this.txt_PName.Size = new System.Drawing.Size(338, 20);
            this.txt_PName.SkipValidation = false;
            this.txt_PName.TabIndex = 31;
            this.txt_PName.TabStop = false;
            this.txt_PName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "P. No :";
            // 
            // txt_Pr_P_No
            // 
            this.txt_Pr_P_No.AllowSpace = true;
            this.txt_Pr_P_No.AssociatedLookUpName = "lookup_Pr_p_No";
            this.txt_Pr_P_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pr_P_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pr_P_No.ContinuationTextBox = null;
            this.txt_Pr_P_No.CustomEnabled = true;
            this.txt_Pr_P_No.DataFieldMapping = "PR_P_NO";
            this.txt_Pr_P_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pr_P_No.GetRecordsOnUpDownKeys = false;
            this.txt_Pr_P_No.IsDate = false;
            this.txt_Pr_P_No.IsLookUpField = true;
            this.txt_Pr_P_No.IsRequired = true;
            this.txt_Pr_P_No.Location = new System.Drawing.Point(94, 37);
            this.txt_Pr_P_No.MaxLength = 6;
            this.txt_Pr_P_No.Name = "txt_Pr_P_No";
            this.txt_Pr_P_No.NumberFormat = "###,###,##0.00";
            this.txt_Pr_P_No.Postfix = "";
            this.txt_Pr_P_No.Prefix = "";
            this.txt_Pr_P_No.Size = new System.Drawing.Size(75, 20);
            this.txt_Pr_P_No.SkipValidation = false;
            this.txt_Pr_P_No.TabIndex = 3;
            this.txt_Pr_P_No.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_Pr_P_No, "Press [F9] Key To See Valid Personnel Numbers [F6] To Exit");
            this.txt_Pr_P_No.Validated += new System.EventHandler(this.txt_Pr_P_No_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Enter Month";
            // 
            // txt_Month
            // 
            this.txt_Month.AllowSpace = true;
            this.txt_Month.AssociatedLookUpName = "";
            this.txt_Month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Month.ContinuationTextBox = null;
            this.txt_Month.CustomEnabled = true;
            this.txt_Month.DataFieldMapping = "";
            this.txt_Month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Month.GetRecordsOnUpDownKeys = false;
            this.txt_Month.IsDate = false;
            this.txt_Month.IsRequired = true;
            this.txt_Month.Location = new System.Drawing.Point(94, 15);
            this.txt_Month.MaxLength = 2;
            this.txt_Month.Name = "txt_Month";
            this.txt_Month.NumberFormat = "###,###,##0.00";
            this.txt_Month.Postfix = "";
            this.txt_Month.Prefix = "";
            this.txt_Month.Size = new System.Drawing.Size(51, 20);
            this.txt_Month.SkipValidation = false;
            this.txt_Month.TabIndex = 1;
            this.txt_Month.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_Month, "Enter month as MM.");
            this.txt_Month.Leave += new System.EventHandler(this.txt_Month_Leave);
            // 
            // txt_Year
            // 
            this.txt_Year.AllowSpace = true;
            this.txt_Year.AssociatedLookUpName = "";
            this.txt_Year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Year.ContinuationTextBox = null;
            this.txt_Year.CustomEnabled = true;
            this.txt_Year.DataFieldMapping = "";
            this.txt_Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Year.GetRecordsOnUpDownKeys = false;
            this.txt_Year.IsDate = false;
            this.txt_Year.IsRequired = true;
            this.txt_Year.Location = new System.Drawing.Point(263, 15);
            this.txt_Year.MaxLength = 4;
            this.txt_Year.Name = "txt_Year";
            this.txt_Year.NumberFormat = "###,###,##0.00";
            this.txt_Year.Postfix = "";
            this.txt_Year.Prefix = "";
            this.txt_Year.Size = new System.Drawing.Size(51, 20);
            this.txt_Year.SkipValidation = false;
            this.txt_Year.TabIndex = 2;
            this.txt_Year.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_Year, "Enter year as YYYY.");
            this.txt_Year.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Year_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(220, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Year";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(344, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 24;
            this.label1.Text = "User / Location :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUser1
            // 
            this.txtUser1.AllowSpace = true;
            this.txtUser1.AssociatedLookUpName = "";
            this.txtUser1.BackColor = System.Drawing.SystemColors.Window;
            this.txtUser1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser1.ContinuationTextBox = null;
            this.txtUser1.CustomEnabled = true;
            this.txtUser1.DataFieldMapping = "";
            this.txtUser1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser1.GetRecordsOnUpDownKeys = false;
            this.txtUser1.IsDate = false;
            this.txtUser1.Location = new System.Drawing.Point(471, 15);
            this.txtUser1.MaxLength = 10;
            this.txtUser1.Name = "txtUser1";
            this.txtUser1.NumberFormat = "###,###,##0.00";
            this.txtUser1.Postfix = "";
            this.txtUser1.Prefix = "";
            this.txtUser1.ReadOnly = true;
            this.txtUser1.Size = new System.Drawing.Size(87, 20);
            this.txtUser1.SkipValidation = false;
            this.txtUser1.TabIndex = 23;
            this.txtUser1.TabStop = false;
            this.txtUser1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation1
            // 
            this.txtLocation1.AllowSpace = true;
            this.txtLocation1.AssociatedLookUpName = "";
            this.txtLocation1.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation1.ContinuationTextBox = null;
            this.txtLocation1.CustomEnabled = true;
            this.txtLocation1.DataFieldMapping = "";
            this.txtLocation1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation1.GetRecordsOnUpDownKeys = false;
            this.txtLocation1.IsDate = false;
            this.txtLocation1.Location = new System.Drawing.Point(564, 15);
            this.txtLocation1.MaxLength = 10;
            this.txtLocation1.Name = "txtLocation1";
            this.txtLocation1.NumberFormat = "###,###,##0.00";
            this.txtLocation1.Postfix = "";
            this.txtLocation1.Prefix = "";
            this.txtLocation1.ReadOnly = true;
            this.txtLocation1.Size = new System.Drawing.Size(37, 20);
            this.txtLocation1.SkipValidation = false;
            this.txtLocation1.TabIndex = 22;
            this.txtLocation1.TabStop = false;
            this.txtLocation1.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(403, 11);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 28;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Payroll_ShiftEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 261);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.slPanelMain);
            this.Name = "CHRIS_Payroll_ShiftEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :   Shift Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPanelMain, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelMain.ResumeLayout(false);
            this.slPanelMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelMain;
        private CrplControlLibrary.SLTextBox txtUser1;
        private CrplControlLibrary.SLTextBox txtLocation1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txt_Month;
        private CrplControlLibrary.SLTextBox txt_Year;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txt_PName;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txt_Pr_P_No;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.LookupButton lookup_Pr_p_No;
        private CrplControlLibrary.SLTextBox txt_category;
    }
}