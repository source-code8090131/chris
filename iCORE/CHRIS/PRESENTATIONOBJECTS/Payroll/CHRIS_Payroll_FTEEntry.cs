using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_FTEEntry : ChrisMasterDetailForm
    {

        #region Constructor
        public CHRIS_Payroll_FTEEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_FTEEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            //Dependent panel
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slpnlDetail);
            this.slpnlMaster.DependentPanels = lstDependentPanels;

            //Independent panel
            this.IndependentPanels.Add(slpnlMaster);

            sldgvDept_FTE.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);



        }
        #endregion

        #region Overloaded Methods

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");

                ((DataGridViewLOVColumn)this.sldgvDept_FTE.Columns["DP_DEPT"]).AttachParentEntity = true;

                //Save
                //this.FunctionConfig.F3 = Function.Save;
                //this.ShowF3Option = true;
                //this.F3OptionText = "[F3] = Save";

                //Modify Mode
                //this.FunctionConfig.F10 = Function.Modify;
                //this.ShowF10Option = true;
                //this.F10OptionText = "[F10] = Modify";

                //Save
                this.FunctionConfig.F10 = Function.Save;
                this.FunctionConfig.EnableF10 = true;
                this.ShowF10Option = true;
                this.F10OptionText = "[F10] = Save";

                //Quit
                this.FunctionConfig.F3 = Function.Quit;
                this.FunctionConfig.EnableF3 = true;

                //View 
                this.FunctionConfig.F1 = Function.View;
                this.ShowF1Option = true;
                this.F1OptionText = " FTE Updation";

                //Delete
                this.FunctionConfig.F4 = Function.Delete;
                this.FunctionConfig.EnableF4 = true;
                this.ShowF4Option = true;
                this.F4OptionText = "[F4] Delete";

                this.sldgvDept_FTE.Enabled = false;
                //this.tbtDelete.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtList.Visible = false;
                slDatePicker1.Visible = false;
                this.txtOption.Visible = false;

                this.txtUser.Text = this.userID;
                lblUserName.Text = lblUserName.Text + " " + this.UserName;
                this.sldgvDept_FTE.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
                lkpPersonnel.Click += new EventHandler(lkpPersonnel_Click);

                //this.txtPersonnelNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.txtPersonnelNo.Select();
            this.txtPersonnelNo.Focus();
            base.OnLoad(e);
            this.txtPersonnelNo.Select();
            this.txtPersonnelNo.Focus();
        }

        /// <summary>
        /// Override it to start FTE Updation Process
        /// </summary>
        /// <returns></returns>
        protected override bool View()
        {
            StartProcessing();
            txtPersonnelNo.IsRequired = false;
            return false;

            
        }

        /// <summary>
        /// Override it to set focus on next row on grid
        /// </summary>
        /// <returns></returns>
        protected override bool Quit()
        {
            if (txtPersonnelNo.Text != string.Empty || this.sldgvDept_FTE.Rows.Count > 1)
            {
                DoToolbarActions(this.Controls, "Cancel");
            }
            else
            {
                base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
            }
            //int count = this.sldgvDept_FTE.Rows.Count;

            //sldgvDept_FTE.Focus();
            //sldgvDept_FTE.CurrentCell = sldgvDept_FTE[0, count - 1];

            return false;
        }

        /// <summary>
        /// Override it Delete
        /// </summary>
        /// <returns></returns>
        protected override bool Delete()
        {
            if ((sldgvDept_FTE.Enabled == true) && (this.sldgvDept_FTE.SelectedRows.Count > 0))
            {
                base.Delete();
                base.DoToolbarActions(this.Controls, "Delete");
            }
            return false;
        }

        /// <summary>
        /// Override it Save
        /// </summary>
        /// <returns></returns>
        protected override bool Save()
        {
            if (sldgvDept_FTE.Enabled == true)
            {
                this.DoToolbarActions(this.Controls, "Save");
            }
            return false;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            try
            {
                if (actionType == "Save")
                {
                    if (!this.sldgvDept_FTE.EndEdit())
                        return;
                    else
                    {
                        for (int i = 0; i < this.sldgvDept_FTE.Rows.Count - 1; i++)
                        {
                            if (!this.sldgvDept_FTE.Rows[i].IsNewRow)
                            {
                                if (string.IsNullOrEmpty(this.sldgvDept_FTE.Rows[i].Cells["DP_SEG"].Value.ToString()) || string.IsNullOrEmpty(this.sldgvDept_FTE.Rows[i].Cells["DP_HC"].Value.ToString()) || string.IsNullOrEmpty(this.sldgvDept_FTE.Rows[i].Cells["DP_HC"].Value.ToString()) || string.IsNullOrEmpty(this.sldgvDept_FTE.Rows[i].Cells["DP_FTE"].Value.ToString()))
                                {
                                    return;
                                }
                            }
                        }

                        this.sldgvDept_FTE.BeginEdit(false);
                        if ((this.sldgvDept_FTE.DataSource as DataTable) != null)
                        {
                            if ((this.sldgvDept_FTE.DataSource as DataTable).Rows.Count > 1)
                            {
                                for (int i = 0; i < this.sldgvDept_FTE.Rows.Count - 1; i++)
                                {
                                    if (!this.sldgvDept_FTE.Rows[i].IsNewRow)
                                    {
                                        this.sldgvDept_FTE.Rows[i].Cells["DP_HC"].Value = this.sldgvDept_FTE.Rows[i].Cells["DP_HC"].Value;

                                    }
                                }
                                if (!this.sldgvDept_FTE.EndEdit())
                                    return;
                            }

                        }
                    }
                }
                base.DoToolbarActions(ctrlsCollection, actionType);

                if (actionType == "Cancel")
                {
                    this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                    this.txtUser.Text = this.userID;
                    lblUserName.Text = lblUserName.Text + " " + this.UserName;
                    this.sldgvDept_FTE.Enabled = false;
                    this.txtPersonnelNo.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                base.LogError("DoToolbarActions:actionType= " + actionType, ex);
            }
        }
        #endregion

        #region Methods
        void StartProcessing()
        {

            CHRIS_Payroll_FTEEntry_MonthYearDialog objCHRIS_Payroll_FTEEntry_MonthYearDialog = new CHRIS_Payroll_FTEEntry_MonthYearDialog();

            //Open Month Year Dialog
            objCHRIS_Payroll_FTEEntry_MonthYearDialog.ShowDialog();

            if (objCHRIS_Payroll_FTEEntry_MonthYearDialog.IsCompleted)
            {
                CHRIS_Payroll_FTEEntry_UpdationDialog objCHRIS_Payroll_FTEEntry_UpdationDialog = new CHRIS_Payroll_FTEEntry_UpdationDialog();
                objCHRIS_Payroll_FTEEntry_UpdationDialog.W_MY = objCHRIS_Payroll_FTEEntry_MonthYearDialog.W_MY;

                //Close Month Year Dialog
                objCHRIS_Payroll_FTEEntry_MonthYearDialog.Close();
                if (objCHRIS_Payroll_FTEEntry_UpdationDialog.W_MY == string.Empty)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                }
                else if (objCHRIS_Payroll_FTEEntry_UpdationDialog.W_MY != string.Empty)
                {
                    //Open Updation Dialog
                    objCHRIS_Payroll_FTEEntry_UpdationDialog.ShowDialog();

                    if (objCHRIS_Payroll_FTEEntry_UpdationDialog.IsCompleted)
                    {
                        CHRIS_Payroll_FTEEntry_ProcessingDialog objCHRIS_Payroll_FTEEntry_ProcessingDialog = new CHRIS_Payroll_FTEEntry_ProcessingDialog();
                        objCHRIS_Payroll_FTEEntry_ProcessingDialog.W_MY = objCHRIS_Payroll_FTEEntry_UpdationDialog.W_MY;
                        objCHRIS_Payroll_FTEEntry_ProcessingDialog.W_OPT = objCHRIS_Payroll_FTEEntry_UpdationDialog.W_OPT;

                        //Close Updation Dialog
                        objCHRIS_Payroll_FTEEntry_UpdationDialog.Close();
                       // if (objCHRIS_Payroll_FTEEntry_ProcessingDialog.W_OPT != 0)
                        {//Open Processing Dialog ---------------------------------   
                            objCHRIS_Payroll_FTEEntry_ProcessingDialog.ShowDialog();

                            if (objCHRIS_Payroll_FTEEntry_ProcessingDialog.IsCompleted)
                            {
                                //Close Processing Dialog
                                objCHRIS_Payroll_FTEEntry_ProcessingDialog.Close();
                            }
                        }
                      
                    }
                    else if (!objCHRIS_Payroll_FTEEntry_UpdationDialog.IsCompleted)
                    {
                        base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    }
                }
            }
        }
        #endregion

        #region Events

        private void sldgvDept_FTE_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (sldgvDept_FTE.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode)
                {
                    if ((this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_SEG"].EditedFormattedValue.ToString() != "GCB") && (this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_SEG"].EditedFormattedValue.ToString() != "GF"))
                    {
                        MessageBox.Show("ENTER  [GF] OR [GCB] ........!");

                        sldgvDept_FTE.Rows[e.RowIndex].Cells[0].Value = "";

                        e.Cancel = true;
                        return;

                    }
                    else if (this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_SEG"].EditedFormattedValue.ToString() == "GCB" || this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_SEG"].EditedFormattedValue.ToString() == "GF")
                    {
                        sldgvDept_FTE.Rows[e.RowIndex].Cells[0].Value = sldgvDept_FTE.Rows[e.RowIndex].Cells[0].EditedFormattedValue;
                        (this.slpnlDetail.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptFTECommand).DP_SEG = sldgvDept_FTE.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString();
                    }
                }
                else
                {
                    if (!this.sldgvDept_FTE.CurrentRow.IsNewRow)
                        if (string.IsNullOrEmpty(this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_SEG"].Value.ToString()))
                        {
                            MessageBox.Show("ENTER  [GF] OR [GCB] ........!");
                            this.sldgvDept_FTE.BeginEdit(true);
                            e.Cancel = true;
                        }

                }
            }
            else if (e.ColumnIndex == 2)
            {
                if (sldgvDept_FTE.CurrentCell.IsInEditMode)
                {
                    if (this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_HC"].EditedFormattedValue.ToString() == "")
                    {
                        MessageBox.Show("Please Enter Contribution For The Given Dept.");
                        e.Cancel = true;
                        return;

                    }
                }

            }
            else if (e.ColumnIndex == 3)
            {

                if (this.sldgvDept_FTE.Rows[e.RowIndex].Cells["DP_FTE"].EditedFormattedValue.ToString() == "")
                {
                    MessageBox.Show("Please Enter Contribution For The Given Dept.");

                    e.Cancel = true;
                    return;

                }

            }
        }

        private void sldgvDept_FTE_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        void lkpPersonnel_Click(object sender, EventArgs e)
        {
            if (slDatePicker1.Value != null)
            {
                ((DeptFTECommand)(this.slpnlDetail.CurrentBusinessEntity)).PR_TRANSFER_DATE = Convert.ToDateTime(slDatePicker1.Value.ToString());
                //this.slpnlMaster.LoadDependentPanels();
            }
        }

        private void txtPersonnelNo_Leave(object sender, EventArgs e)
        {
            if (this.txtPersonnelNo.Text != "")
            {
                ///*------To store input parameters of StoredProcedure------*/
                //Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                //dicInputParameters.Clear();
                //dicInputParameters.Add("PR_P_NO", this.txtPersonnelNo.Text);

                //CmnDataManager objCmnDataManager = new CmnDataManager();
                //rslt = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_GETALL", "", dicInputParameters);

                //if (rslt.isSuccessful)
                //{
                //    if (rslt.dstResult != null)
                //    {
                //        //if (rslt.dstResult.Tables.Count > 0)
                //        //{
                //        //    if (rslt.dstResult.Tables[0].Rows.Count > 0)
                //        //    {
                //        //        this.slpnlMaster.LoadDependentPanels();
                //        //    }
                //        //}
                //        this.slpnlMaster.LoadDependentPanels();
                //    }
                //}
            }
        }

        //protected override void OnKeyDown(KeyEventArgs e)
        //{
        //    base.OnKeyDown(e);
        //}

        private void CHRIS_Payroll_FTEEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if ((actionType == "FTEPersonnelLov") || (actionType == "FTEPersonnelLovExist"))
            {
                if (selectedRow != null)
                {
                    this.sldgvDept_FTE.Enabled = true;

                    //Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                    //dicInputParameters.Clear();
                    //dicInputParameters.Add("PR_P_NO", this.txtPersonnelNo1.Text);
                    //dicInputParameters.Add("PR_TRANSFER_DATE", slDatePicker1.Value);
                    //CmnDataManager objCmnDataManager = new CmnDataManager();
                    //rslt = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_GETALL", "", dicInputParameters);

                    //if (rslt.isSuccessful)
                    //{
                    //    if (rslt.dstResult != null)
                    //    {
                    //        this.slpnlMaster.LoadDependentPanels();
                    //    }
                    //}
                }
            }
        }

        private void txtPersonnelNo_Validated(object sender, EventArgs e)
        {
            if (this.txtPersonnelNo.Text != "")
            {
                this.sldgvDept_FTE.Select();
                this.sldgvDept_FTE.Focus();
                txtPersonnelNo.IsRequired = true;
            }
        }
        #endregion

        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersonnelNo.Text == "")
            {
                txtPersonnelNo.IsRequired = true;
                e.Cancel = true;
            }
        }
    }
}