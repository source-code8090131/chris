using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using System.Reflection;
using System.Data.SqlClient;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_View_Info : MasterDetailForm
    {

        CHRIS_Payroll_PR111 ObjBasecopy = null;
        public const string Fin_regex = @"^(\-(?=\d))?\d{0,6}(\.\d{1,2})?$";
        Object Mdi;
        Dictionary<string, object> paramList = new Dictionary<string, object>();
        Result rslt;
        Result flowRslt;
        CmnDataManager cmnDM = new CmnDataManager();
        SqlTransaction processTrans = null;          
        
        #region Constructors

        public CHRIS_Payroll_View_Info()
        {
            InitializeComponent();           
        }

        public CHRIS_Payroll_View_Info(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Payroll_PR111 menuBaseForm)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.MdiParent = mainmenu;
            ObjBasecopy = menuBaseForm;
            ObjBasecopy.Hide();
            tbtSave.Visible = false;
            tbtDelete.Visible = false;

        }

        #endregion

        #region Methods
       
       

        #endregion

        #region Events

       

        protected override void OnFormClosing(FormClosingEventArgs e)
        {

            base.OnFormClosing(e);
            ObjBasecopy.Show();
            //base.DoToolbarActions(this.Controls, "Close");
        }
        
        #endregion
        
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                paramList.Clear();
                paramList.Add("PR_P_NO", txt_PR_P_NO.Text);
                paramList.Add("W_DATE", dateTimePicker1.Value);
                paramList.Add("W_JOINING_DATE", slDatePicker_PR_JOINING_DATE.Value);
                paramList.Add("W_AN_PACK", txt_PR_ANNUAL_PACK.Text);
                paramList.Add("W_BRANCH", txt_PR_NEW_BRANCH.Text);
                paramList.Add("W_DESIG", txt_PR_DESIG.Text);
                paramList.Add("W_LEVEL", txt_PR_LEVEL.Text);
                paramList.Add("W_CATE", txt_PR_CATEGORY.Text);
                paramList.Add("W_ZAKAT", 0);
                paramList.Add("W_PRV_INC", 0);
                paramList.Add("W_TAX_USED", 0);
                paramList.Add("W_REFUND", 0);
                paramList.Add("W_PRV_TAX_PD", 0);
                paramList.Add("W_ITAX", 0);
                paramList.Add("W_PF_EXEMPT", 150000);
                paramList.Add("W_SYS", slDatePicker_PR_JOINING_DATE.Value);
                paramList.Add("GLOBAL_W_10_AMT", 0);
                paramList.Add("GLOBAL_W_INC_AMT", 0);
                paramList.Add("W_VP", 0);
                paramList.Add("SCREEN_TYPE", 0);

                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_GENERATION_Z_TAX", "", paramList, ref processTrans, 60);
                if (flowRslt.isSuccessful)
                {

                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        TXT_SUBS_LOAN_TAX_AMT.Text = flowRslt.dstResult.Tables[0].Rows[0]["SUBS_LOAN_TAX_AMT"].ToString();
                        txt_ANNUAL_SALARY.Text = flowRslt.dstResult.Tables[0].Rows[0]["ANNUAL_SALARY"].ToString();
                        //txt_INCREASE_IN_SALARY.Text = flowRslt.dstResult.Tables[0].Rows[0]["INCREASE_IN_SALARY"].ToString();
                        //txt_DATE_OF_INCREMENT.Text = flowRslt.dstResult.Tables[0].Rows[0]["DATE_OF_INCREMENT"].ToString();
                        txt_GROSS_SALARY.Text = flowRslt.dstResult.Tables[0].Rows[0]["GROSS_SALARY"].ToString();
                        txt_ANNUAL_PF.Text = flowRslt.dstResult.Tables[0].Rows[0]["ANNUAL_PF"].ToString();
                        txt_PF_EXEMPT.Text = flowRslt.dstResult.Tables[0].Rows[0]["PF_EXEMPT"].ToString();
                        txt_CAR_ALLOWNCE.Text = flowRslt.dstResult.Tables[0].Rows[0]["CAR_ALLOWNCE"].ToString();
                        txt_GHA_ALLOWNCE.Text = flowRslt.dstResult.Tables[0].Rows[0]["GHA_ALLOWNCE"].ToString();
                        txt_OPD_ALLOWNCE.Text = flowRslt.dstResult.Tables[0].Rows[0]["OPD_ALLOWNCE"].ToString();
                        txt_FUEL_ALLONCE.Text = flowRslt.dstResult.Tables[0].Rows[0]["FUEL_ALLONCE"].ToString();
                        txt_OTHER_ALLOWNCE.Text = flowRslt.dstResult.Tables[0].Rows[0]["OTHER_ALLOWNCE"].ToString();
                        txt_ZAKAT_DONATION.Text = flowRslt.dstResult.Tables[0].Rows[0]["ZAKAT_DONATION"].ToString();
                        txt_BONUS_PAYMENT.Text = flowRslt.dstResult.Tables[0].Rows[0]["BONUS_PAYMENT"].ToString();
                        txt_DIVIDENT_STOCK.Text = flowRslt.dstResult.Tables[0].Rows[0]["DIVIDENT_STOCK"].ToString();
                        txt_HL_MARKUP.Text = flowRslt.dstResult.Tables[0].Rows[0]["HL_MARKUP"].ToString();
                        txtInvestment.Text = flowRslt.dstResult.Tables[0].Rows[0]["Investments"].ToString();
                        txtInvrebate.Text = flowRslt.dstResult.Tables[0].Rows[0]["INV_REBATE"].ToString();
                        txtDonation.Text = flowRslt.dstResult.Tables[0].Rows[0]["Donations"].ToString();
                        txtDonationRebate.Text = flowRslt.dstResult.Tables[0].Rows[0]["DON_REBATE"].ToString();
                        txtUtilities.Text = flowRslt.dstResult.Tables[0].Rows[0]["Utilities_or_Other"].ToString();
                        txt_GLOBAL_W_10_AMT.Text = flowRslt.dstResult.Tables[0].Rows[0]["GLOBAL_W_10_AMT"].ToString();
                        txt_GLOBAL_W_INC_AMT.Text = flowRslt.dstResult.Tables[0].Rows[0]["GLOBAL_W_INC_AMT"].ToString();
                        txt_TOTAL_TAXABLE_INCOME.Text = flowRslt.dstResult.Tables[0].Rows[0]["TOTAL_TAXABLE_INCOME"].ToString();
                        txt_FIXED_TAX.Text = flowRslt.dstResult.Tables[0].Rows[0]["FIXED_TAX"].ToString();
                        txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Text = flowRslt.dstResult.Tables[0].Rows[0]["AMOUNT_EXCEEDING_EXCEMPTION_LIMIT"].ToString();
                        txt_VAR_TAX.Text = flowRslt.dstResult.Tables[0].Rows[0]["VAR_TAX"].ToString();
                        txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Text = flowRslt.dstResult.Tables[0].Rows[0]["TAX_EXEMPT_LIMIT_FROM_SLAB"].ToString();
                        txt_TAX_ALREADY_PAID.Text = flowRslt.dstResult.Tables[0].Rows[0]["TAX_ALREADY_PAID"].ToString();
                        txt_TAX_RATE_FROM_SLAB.Text = flowRslt.dstResult.Tables[0].Rows[0]["TAX_RATE_FROM_SLAB"].ToString();
                        txt_FINAL_ANNUAL_TAX.Text = flowRslt.dstResult.Tables[0].Rows[0]["FINAL_ANNUAL_TAX"].ToString();
                        txt_REMAINING_MONTHS.Text = flowRslt.dstResult.Tables[0].Rows[0]["REMAINING_MONTHS"].ToString();
                        txt_W_ITAX.Text = flowRslt.dstResult.Tables[0].Rows[0]["W_ITAX"].ToString();
                        txt_W_INST_L.Text = flowRslt.dstResult.Tables[0].Rows[0]["W_INST_L"].ToString();
                        txt_START_DATE_OF_CAL.Text = flowRslt.dstResult.Tables[0].Rows[0]["START_DATE_OF_CAL"].ToString();
                        txt_END_DATE_OF_CALC.Text = flowRslt.dstResult.Tables[0].Rows[0]["END_DATE_OF_CALC"].ToString();
                    }
                }
                else
                {
                    Exception expr = flowRslt.exp;
                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PAYROLL_GENERATION_Z_TAX", expr);
                    MessageBox.Show(expr.Message);
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "InitiateProcess--CHRIS_SP_PAYROLL_GENERATION_Z_TAX", exp);
                // msg = exp.ToString();
            }
            finally
            {
                // Logging(this.Name, "Z_TAX PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
            }
        }
    }
}

