namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_FTEEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_FTEEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPersonnelNo1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPersonnelName = new CrplControlLibrary.SLTextBox(this.components);
            this.slpnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker1 = new CrplControlLibrary.SLDatePicker(this.components);
            this.lkpPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.slpnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.sldgvDept_FTE = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.DP_SEG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DP_DEPT = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.DP_HC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DP_FTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gboPersonnel = new System.Windows.Forms.GroupBox();
            this.gboFTE = new System.Windows.Forms.GroupBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slpnlMaster.SuspendLayout();
            this.slpnlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldgvDept_FTE)).BeginInit();
            this.gboPersonnel.SuspendLayout();
            this.gboFTE.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(598, 0);
            this.txtOption.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(634, 22);
            this.pnlBottom.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 575);
            this.panel1.Size = new System.Drawing.Size(634, 60);
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(539, 88);
            this.txtDate.MaxLength = 11;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(83, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 55;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(491, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 54;
            this.label7.Text = "Date :";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(94, 105);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(88, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 53;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(94, 83);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(88, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 47;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(256, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "FTE AND HEAD COUNT ENTRY";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "Location :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "User :";
            // 
            // txtPersonnelNo1
            // 
            this.txtPersonnelNo1.AllowSpace = true;
            this.txtPersonnelNo1.AssociatedLookUpName = "lkpPersonnel";
            this.txtPersonnelNo1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo1.ContinuationTextBox = null;
            this.txtPersonnelNo1.CustomEnabled = true;
            this.txtPersonnelNo1.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo1.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo1.IsDate = false;
            this.txtPersonnelNo1.IsLookUpField = true;
            this.txtPersonnelNo1.IsRequired = true;
            this.txtPersonnelNo1.Location = new System.Drawing.Point(72, 50);
            this.txtPersonnelNo1.MaxLength = 6;
            this.txtPersonnelNo1.Name = "txtPersonnelNo1";
            this.txtPersonnelNo1.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo1.Postfix = "";
            this.txtPersonnelNo1.Prefix = "";
            this.txtPersonnelNo1.Size = new System.Drawing.Size(65, 20);
            this.txtPersonnelNo1.SkipValidation = false;
            this.txtPersonnelNo1.TabIndex = 101;
            this.txtPersonnelNo1.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNo1.Visible = false;
            this.txtPersonnelNo1.Leave += new System.EventHandler(this.txtPersonnelNo_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "P.No.  :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(198, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 58;
            this.label8.Text = "Name :";
            // 
            // txtPersonnelName
            // 
            this.txtPersonnelName.AllowSpace = true;
            this.txtPersonnelName.AssociatedLookUpName = "";
            this.txtPersonnelName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelName.ContinuationTextBox = null;
            this.txtPersonnelName.CustomEnabled = true;
            this.txtPersonnelName.DataFieldMapping = "PR_NAME";
            this.txtPersonnelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelName.IsDate = false;
            this.txtPersonnelName.Location = new System.Drawing.Point(251, 22);
            this.txtPersonnelName.Name = "txtPersonnelName";
            this.txtPersonnelName.NumberFormat = "###,###,##0.00";
            this.txtPersonnelName.Postfix = "";
            this.txtPersonnelName.Prefix = "";
            this.txtPersonnelName.ReadOnly = true;
            this.txtPersonnelName.Size = new System.Drawing.Size(238, 20);
            this.txtPersonnelName.SkipValidation = false;
            this.txtPersonnelName.TabIndex = 2;
            this.txtPersonnelName.TextType = CrplControlLibrary.TextType.String;
            // 
            // slpnlMaster
            // 
            this.slpnlMaster.ConcurrentPanels = null;
            this.slpnlMaster.Controls.Add(this.txtPersonnelNo);
            this.slpnlMaster.Controls.Add(this.slDatePicker1);
            this.slpnlMaster.Controls.Add(this.lkpPersonnel);
            this.slpnlMaster.Controls.Add(this.label8);
            this.slpnlMaster.Controls.Add(this.txtPersonnelNo1);
            this.slpnlMaster.Controls.Add(this.txtPersonnelName);
            this.slpnlMaster.Controls.Add(this.label6);
            this.slpnlMaster.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slpnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slpnlMaster.DependentPanels = null;
            this.slpnlMaster.DisableDependentLoad = false;
            this.slpnlMaster.EnableDelete = false;
            this.slpnlMaster.EnableInsert = true;
            this.slpnlMaster.EnableQuery = false;
            this.slpnlMaster.EnableUpdate = false;
            this.slpnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand_Zakat";
            this.slpnlMaster.Location = new System.Drawing.Point(18, 19);
            this.slpnlMaster.MasterPanel = null;
            this.slpnlMaster.Name = "slpnlMaster";
            this.slpnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slpnlMaster.Size = new System.Drawing.Size(570, 74);
            this.slpnlMaster.SPName = "CHRIS_SP_PERSONNEL_ZAKAT_PERSONNEL_MANAGER";
            this.slpnlMaster.TabIndex = 0;
            this.slpnlMaster.TabStop = true;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "lkpPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsLookUpField = true;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(72, 22);
            this.txtPersonnelNo.MaxLength = 6;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(65, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 0;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNo.Validated += new System.EventHandler(this.txtPersonnelNo_Validated);
            this.txtPersonnelNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNo_Validating);
            // 
            // slDatePicker1
            // 
            this.slDatePicker1.CustomEnabled = true;
            this.slDatePicker1.DataFieldMapping = "PR_TRANSFER_DATE";
            this.slDatePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.slDatePicker1.HasChanges = true;
            this.slDatePicker1.Location = new System.Drawing.Point(251, 48);
            this.slDatePicker1.Name = "slDatePicker1";
            this.slDatePicker1.NullValue = " ";
            this.slDatePicker1.Size = new System.Drawing.Size(200, 20);
            this.slDatePicker1.TabIndex = 65;
            this.slDatePicker1.TabStop = false;
            this.slDatePicker1.Value = new System.DateTime(2011, 2, 1, 0, 0, 0, 0);
            // 
            // lkpPersonnel
            // 
            this.lkpPersonnel.ActionLOVExists = "FTEPersonnelLovExist";
            this.lkpPersonnel.ActionType = "FTEPersonnelLov";
            this.lkpPersonnel.ConditionalFields = "";
            this.lkpPersonnel.CustomEnabled = true;
            this.lkpPersonnel.DataFieldMapping = "";
            this.lkpPersonnel.DependentLovControls = "";
            this.lkpPersonnel.HiddenColumns = "";
            this.lkpPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lkpPersonnel.Image")));
            this.lkpPersonnel.LoadDependentEntities = true;
            this.lkpPersonnel.Location = new System.Drawing.Point(143, 22);
            this.lkpPersonnel.LookUpTitle = null;
            this.lkpPersonnel.Name = "lkpPersonnel";
            this.lkpPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lkpPersonnel.SkipValidationOnLeave = false;
            this.lkpPersonnel.SPName = "CHRIS_SP_PERSONNEL_ZAKAT_PERSONNEL_MANAGER";
            this.lkpPersonnel.TabIndex = 64;
            this.lkpPersonnel.TabStop = false;
            this.lkpPersonnel.UseVisualStyleBackColor = true;
            // 
            // slpnlDetail
            // 
            this.slpnlDetail.ConcurrentPanels = null;
            this.slpnlDetail.Controls.Add(this.sldgvDept_FTE);
            this.slpnlDetail.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slpnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slpnlDetail.DependentPanels = null;
            this.slpnlDetail.DisableDependentLoad = false;
            this.slpnlDetail.EnableDelete = true;
            this.slpnlDetail.EnableInsert = true;
            this.slpnlDetail.EnableQuery = false;
            this.slpnlDetail.EnableUpdate = true;
            this.slpnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptFTECommand";
            this.slpnlDetail.Location = new System.Drawing.Point(32, 19);
            this.slpnlDetail.MasterPanel = null;
            this.slpnlDetail.Name = "slpnlDetail";
            this.slpnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slpnlDetail.Size = new System.Drawing.Size(551, 189);
            this.slpnlDetail.SPName = "CHRIS_SP_DEPT_FTE_MANAGER";
            this.slpnlDetail.TabIndex = 4;
            // 
            // sldgvDept_FTE
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sldgvDept_FTE.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.sldgvDept_FTE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sldgvDept_FTE.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DP_SEG,
            this.DP_DEPT,
            this.DP_HC,
            this.DP_FTE});
            this.sldgvDept_FTE.ColumnToHide = null;
            this.sldgvDept_FTE.ColumnWidth = null;
            this.sldgvDept_FTE.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sldgvDept_FTE.DefaultCellStyle = dataGridViewCellStyle4;
            this.sldgvDept_FTE.DisplayColumnWrapper = null;
            this.sldgvDept_FTE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sldgvDept_FTE.GridDefaultRow = 0;
            this.sldgvDept_FTE.Location = new System.Drawing.Point(0, 0);
            this.sldgvDept_FTE.Name = "sldgvDept_FTE";
            this.sldgvDept_FTE.ReadOnlyColumns = "";
            this.sldgvDept_FTE.RequiredColumns = "DP_SEG|DP_HC|DP_DEPT|SEGMENT|DEPT|HEAD COUNT|DP_FTE|FTE %";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sldgvDept_FTE.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.sldgvDept_FTE.Size = new System.Drawing.Size(551, 189);
            this.sldgvDept_FTE.SkippingColumns = null;
            this.sldgvDept_FTE.TabIndex = 0;
            this.sldgvDept_FTE.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.sldgvDept_FTE_CellValidating);
            this.sldgvDept_FTE.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.sldgvDept_FTE_EditingControlShowing);
            // 
            // DP_SEG
            // 
            this.DP_SEG.DataPropertyName = "DP_SEG";
            this.DP_SEG.HeaderText = "Segment";
            this.DP_SEG.MaxInputLength = 3;
            this.DP_SEG.Name = "DP_SEG";
            this.DP_SEG.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DP_SEG.Width = 110;
            // 
            // DP_DEPT
            // 
            this.DP_DEPT.ActionLOV = "DeptLOV";
            this.DP_DEPT.ActionLOVExists = "DeptLOVExist";
            this.DP_DEPT.AttachParentEntity = false;
            this.DP_DEPT.DataPropertyName = "DP_DEPT";
            this.DP_DEPT.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptFTECommand";
            this.DP_DEPT.HeaderText = "Dept";
            this.DP_DEPT.LookUpTitle = "DEPARTMENT LIST";
            this.DP_DEPT.LOVFieldMapping = "DP_DEPT";
            this.DP_DEPT.Name = "DP_DEPT";
            this.DP_DEPT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DP_DEPT.SearchColumn = "DP_SEG";
            this.DP_DEPT.SkipValidationOnLeave = false;
            this.DP_DEPT.SpName = "CHRIS_SP_DEPT_FTE_MANAGER";
            this.DP_DEPT.Width = 230;
            // 
            // DP_HC
            // 
            this.DP_HC.DataPropertyName = "DP_HC";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.DP_HC.DefaultCellStyle = dataGridViewCellStyle2;
            this.DP_HC.HeaderText = "Head Count";
            this.DP_HC.MaxInputLength = 3;
            this.DP_HC.Name = "DP_HC";
            this.DP_HC.Width = 130;
            // 
            // DP_FTE
            // 
            this.DP_FTE.DataPropertyName = "DP_FTE";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.DP_FTE.DefaultCellStyle = dataGridViewCellStyle3;
            this.DP_FTE.HeaderText = "FTE %";
            this.DP_FTE.MaxInputLength = 3;
            this.DP_FTE.Name = "DP_FTE";
            this.DP_FTE.Width = 130;
            // 
            // gboPersonnel
            // 
            this.gboPersonnel.Controls.Add(this.slpnlMaster);
            this.gboPersonnel.Location = new System.Drawing.Point(8, 158);
            this.gboPersonnel.Name = "gboPersonnel";
            this.gboPersonnel.Size = new System.Drawing.Size(615, 109);
            this.gboPersonnel.TabIndex = 62;
            this.gboPersonnel.TabStop = false;
            // 
            // gboFTE
            // 
            this.gboFTE.Controls.Add(this.slpnlDetail);
            this.gboFTE.Location = new System.Drawing.Point(8, 273);
            this.gboFTE.Name = "gboFTE";
            this.gboFTE.Size = new System.Drawing.Size(615, 248);
            this.gboFTE.TabIndex = 63;
            this.gboFTE.TabStop = false;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(435, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 110;
            this.lblUserName.Text = "User Name :";
            // 
            // CHRIS_Payroll_FTEEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 635);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.gboFTE);
            this.Controls.Add(this.gboPersonnel);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.CurrentPanelBlock = "slpnlMaster";
            this.F10OptionText = "F[10]=Add";
            this.F1OptionText = resources.GetString("$this.F1OptionText");
            this.Name = "CHRIS_Payroll_FTEEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS : Payroll FTE Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Payroll_FTEEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.gboPersonnel, 0);
            this.Controls.SetChildIndex(this.gboFTE, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slpnlMaster.ResumeLayout(false);
            this.slpnlMaster.PerformLayout();
            this.slpnlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sldgvDept_FTE)).EndInit();
            this.gboPersonnel.ResumeLayout(false);
            this.gboFTE.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtPersonnelNo1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtPersonnelName;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slpnlMaster;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slpnlDetail;
        private System.Windows.Forms.GroupBox gboPersonnel;
        private System.Windows.Forms.GroupBox gboFTE;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView sldgvDept_FTE;
        private CrplControlLibrary.LookupButton lkpPersonnel;
        private CrplControlLibrary.SLDatePicker slDatePicker1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DP_SEG;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn DP_DEPT;
        private System.Windows.Forms.DataGridViewTextBoxColumn DP_HC;
        private System.Windows.Forms.DataGridViewTextBoxColumn DP_FTE;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
    }
}