using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_ZakatEntry : ChrisSimpleForm
    {

        #region Constructor

        public CHRIS_Payroll_ZakatEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Payroll_ZakatEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            lblUserName.Text = this.UserName;
            this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
        }
        
        #endregion

        #region Overloaded Methods

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Add()
        {
            base.Add();
            lkpPersonnel.Enabled = true;
            this.txtPersonnelNo.Focus();
         
            this.txtOption.Text = "A";
            this.txtOptionView.Text = "ADD";
            this.txtPersonnelNo.Focus();

            return false;

        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Edit()
        {
            base.Edit();
            lkpPersonnel.Enabled = true;
            this.txtOption.Text = "M";
            this.txtOptionView.Text = "MODIFY";
            this.txtPersonnelNo.Focus();

            return false;

        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool View()
        {
            base.View();
            lkpPersonnel.Enabled = true;
            this.txtOption.Text = "V";
            this.txtOptionView.Text = "VIEW";
            this.txtPersonnelNo.Focus();        

            return false;

        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Delete()
        {
            base.Delete();
            lkpPersonnel.Enabled = true;
            this.txtOption.Text = "D";
            this.txtOptionView.Text = "DELETE";
            this.txtPersonnelNo.Focus();

            return false;

        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        /// <returns></returns>
        protected override bool Quit()
        {
            bool flag = false;
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
                lkpPersonnel.Enabled = false;
            }
            this.txtOption.Select();
            this.txtOption.Focus();
            return flag;
            //base.Quit();
            //this.Close();

            return false;
        }

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods(); 
            this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");         
        }      

        #endregion

        #region Methods

        void Reset()
        {
            this.txtPersonnelNo.Text = "";
            this.txtFirstName.Text = "";
            this.txtZakatAmount.Text = "";
            this.txtRefundAmount.Text = "";
            this.txtRefundYear1.Text = "";
            this.txtRefundYear2.Text = "";
            lkpPersonnel.Enabled = false;
            this.FunctionConfig.CurrentOption = Function.None;
            this.txtOption.Text = "";
            this.txtOption.Focus();
        }

        void ShowSaveRecordDialog()
        {
            if ((this.FunctionConfig.CurrentOption == Function.Add) || (this.FunctionConfig.CurrentOption == Function.Modify))
            {

                DialogResult dRes = MessageBox.Show("DO YOU WANT TO SAVE [Y]es/[N]o..", "Note"
              , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes == DialogResult.Yes)
                {
                    base.DoToolbarActions(this.Controls, "Save");
                    Reset();
                    return;

                }
                else if (dRes == DialogResult.No)
                {
                    Reset();
                    //txtOption.Focus();
                    return;
                }
            }

            //else
            //{
            //    base.DoToolbarActions(this.Controls, "List");
            //}
        }
        void ShowViewRecordDialog()
        {
            DialogResult dRes = MessageBox.Show("DO YOU WANT TO VIEW MORE RECORDS", "Note",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {                              
                Reset();
                this.txtPersonnelNo.Focus();
                return;

            }
            else if (dRes == DialogResult.No)
            {
                //this.txtOption.Text = "";
                //this.txtOption.Focus();
                Reset();
                return;
            }
        }
        void ShowDeleteRecordDialog()
        {
            DialogResult dRes = MessageBox.Show("DO YOU WANT TO DELETE THIS RECORD [Y]es/[N]o", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                base.DoToolbarActions(this.Controls, "Delete");
                //this.txtOption.Text = "";
                //this.txtOption.Focus();
                Reset();
                return;

            }
            else if (dRes == DialogResult.No)
            {                   
                Reset();
                //this.txtOption.Text = "";
                //this.txtOption.Focus();
                return;
            }
        }

        void CheckPersonnel()
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("PR_P_NO", (object)this.txtPersonnelNo.Text);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "CheckZakatEntry", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            //-------------------------------- Personnel Exist -------------------------------
                            this.lkpPersonnel.SkipValidationOnLeave = true;
                            String [] refundYears = rslt.dstResult.Tables[0].Rows[0]["PR_REFUND_FOR1"].ToString().Split('/');

                            this.txtPersonnelNo.Text = rslt.dstResult.Tables[0].Rows[0]["PR_P_NO"].ToString();
                            this.txtFirstName.Text = rslt.dstResult.Tables[0].Rows[0]["PR_NAME"].ToString();
                            this.txtZakatAmount.Text = rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"].ToString();
                            this.txtRefundAmount.Text = rslt.dstResult.Tables[0].Rows[0]["PR_REFUND_AMT"].ToString();
                            this.txtRefundYear1.Text = (refundYears.Length > 0) ? refundYears[0] : String.Empty;
                            this.txtRefundYear2.Text = (refundYears.Length > 1) ? refundYears[1] : String.Empty;

                            #region Add Case zakat Exist

                            if (((Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"]) != 0) || (Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["PR_REFUND_AMT"]) != 0)) && (this.FunctionConfig.CurrentOption == Function.Add))
                            {                           
                                 MessageBox.Show("THE VALUE IS ALREADY THERE YOU CAN MODIFY IT");
                                 Reset();
                                 this.txtOption.Text = "";
                                 this.txtOption.Focus();
                                 return;
                             }

                            #endregion

                            #region Add case Zakat Not Exist
                            else if ((Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"]) == 0) && (this.FunctionConfig.CurrentOption == Function.Add))
                            {
                                this.txtZakatAmount.Focus();
                            }
                             #endregion

                            #region Modify/Delete/View Zakat Not Exist
                            else if (((this.FunctionConfig.CurrentOption == Function.Modify) || (this.FunctionConfig.CurrentOption == Function.Delete) || (this.FunctionConfig.CurrentOption == Function.View)) && (Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"]) == 0))
                            {
                                this.txtZakatAmount.Text = rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"].ToString();
                                MessageBox.Show("THE VALUE IS NOT THERE . NOTHING TO MODIFY,DELETE OR VIEW");
                                Reset();
                                this.txtOption.Text = "";
                                this.txtOption.Focus();
                                return;
                            }
                            #endregion

                            #region Modify/Delete/View Zakat Exist
                            else if (((this.FunctionConfig.CurrentOption == Function.Modify) || (this.FunctionConfig.CurrentOption == Function.Delete) || (this.FunctionConfig.CurrentOption == Function.View)) && (Convert.ToInt32(rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"]) != 0))
                            {
                                #region Fill Records in textboxes
                                this.txtZakatAmount.Text = rslt.dstResult.Tables[0].Rows[0]["PR_ZAKAT_AMT"].ToString();
                                this.txtRefundAmount.Text = rslt.dstResult.Tables[0].Rows[0]["PR_REFUND_AMT"].ToString();

                                if (rslt.dstResult.Tables[0].Rows[0]["PR_REFUND_FOR1"] != null)
                                {
                                    string strPR_REFUND_FOR = rslt.dstResult.Tables[0].Rows[0]["PR_REFUND_FOR1"].ToString();

                                    if (strPR_REFUND_FOR.Contains("/"))
                                    {
                                        string[] array = strPR_REFUND_FOR.Split('/');
                                        if (array.Length == 2)
                                        {
                                            this.txtRefundYear1.Text = array[0];
                                            this.txtRefundYear2.Text = array[1];
                                        }

                                    }
                                }
                                #endregion

                                if (this.FunctionConfig.CurrentOption == Function.Modify)
                                {
                                    this.txtZakatAmount.Focus();
                                    this.tbtAdd.Visible = false;
                                }
                                else if (this.FunctionConfig.CurrentOption == Function.View)
                                {                                   
                                   ShowViewRecordDialog();
                                }
                                else if (this.FunctionConfig.CurrentOption == Function.Delete)
                                {
                                    //ShowDeleteRecordDialog();
                                    base.DoToolbarActions(this.Controls, "Delete");
                                    Reset();                                 
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            //-------------------------------- Personnel Not Exist -------------------------------
                            MessageBox.Show("PRESS [F9] KEY FOR HELP/ [Enter] TO EXIT");
                            //this.txtOption.Text = "";                           
                            //this.txtOption.Focus();
                            Reset();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Events
        private void txtPersonnelNo_Leave(object sender, EventArgs e)
        {
            try
            {
                if ((txtPersonnelNo.Text != "") && (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.View))
                {
                    CheckPersonnel();
                }
                else
                {
                    this.txtOption.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                base.LogException(this.Name, "txtPersonnelNo_Leave", ex);
            }
            
        }
        private void txtRefundYear1_Leave(object sender, EventArgs e)
        {
            string strRefundYear1, strRefundYear2;
            try
            {
                if (((this.FunctionConfig.CurrentOption == Function.Add) || (this.FunctionConfig.CurrentOption == Function.Modify)) && (this.txtRefundYear1.Text != ""))
                {
                    this.txtRefundYear1.Text = this.txtRefundYear1.Text.PadLeft(4, '0');
                    int uRefundYearTo = Convert.ToInt32(this.txtRefundYear1.Text);                 
                    uRefundYearTo = uRefundYearTo + 1;
                    strRefundYear2 = uRefundYearTo.ToString();
                    this.txtRefundYear2.Text = strRefundYear2.PadLeft(4, '0');
                    ShowSaveRecordDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                base.LogException(this.Name, "txtRefundYear1_Leave", ex);
            }
        }
        private void txtRefundAmount_TextChanged(object sender, EventArgs e)
        {
            if (this.txtRefundAmount.Text.Length == 9)
            {
                MessageBox.Show("Field must be of form 9,999,999.99");
            }
        }
        #endregion      
        
    }
}