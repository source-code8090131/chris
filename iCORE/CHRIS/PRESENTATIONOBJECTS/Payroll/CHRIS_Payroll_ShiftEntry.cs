using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_ShiftEntry : ChrisSimpleForm
    {
        CHRIS_Payroll_ShiftEntry_GridForm ObjShiftEntry = null;

        #region Constructors

        public CHRIS_Payroll_ShiftEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Payroll_ShiftEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region Overidden Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            this.txtLocation1.Text = this.CurrentLocation;
            this.txtUser1.Text = this.userID;
            lblUserName.Text += "   " + this.UserName;
            this.tbtAdd.Visible = false;
            this.tbtDelete.Visible = false;
            this.tbtEdit.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtList.Visible = false;
            this.tbtCancel.Visible = false;
            this.txt_Month.Focus();
            this.txt_Pr_P_No.KeyDown += new KeyEventHandler(txt_Pr_P_No_KeyDown);
            txt_Month.Text = Convert.ToString(DateTime.Now.Month);
            if (txt_Month.Text.Length == 1)
            {
                txt_Month.Text = "0" + txt_Month.Text;
            }


        }

        void txt_Pr_P_No_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {

            }
        }


        #endregion

        #region Methods

        private DateTime MakeDate()
        {
            int month = DateTime.Now.Date.Month;
            int year = DateTime.Now.Date.Year;
            DateTime dt;
            month = int.Parse(this.txt_Month.Text);
            int tempyear=int.Parse(this.txt_Year.Text);
            if (tempyear > 1700/*DateTime.MinValue.Year*/ && tempyear < 3000 /*DateTime.MaxValue.Year*/)
                year = int.Parse(this.txt_Year.Text);
            dt = new DateTime(year, month, 1);
            return dt;

        }

        #endregion

        #region Events

        //added by Ahad Zubair//
        private void txt_Month_Leave(object sender, EventArgs e)
        {
            if (txt_Month.Text != "")
            {
                if (Convert.ToDecimal(txt_Month.Text) < 1 || Convert.ToDecimal(txt_Month.Text) > 12)
                {
                    MessageBox.Show("Invalid Month");
                    txt_Year.Text = "";
                    txt_Month.Focus();
                    return;
                }
                else
                {
                    txt_Year.Text = Convert.ToString(DateTime.Now.Year);
                }
            }

        }

        private void txt_Pr_P_No_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txt_Pr_P_No.Text.Trim()))
            {
                if (ObjShiftEntry == null)
                {
                    ObjShiftEntry = new CHRIS_Payroll_ShiftEntry_GridForm(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, (CHRIS_Payroll_ShiftEntry)this);
                    ObjShiftEntry.MdiParent = ((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent));
                    ObjShiftEntry.pr_p_no = this.txt_Pr_P_No.Text;
                    ObjShiftEntry.Ot_Date = MakeDate();
                    ObjShiftEntry.month = int.Parse(this.txt_Month.Text);
                    ObjShiftEntry.year = int.Parse(this.txt_Year.Text);
                    ObjShiftEntry.w_cate = this.txt_category.Text;
                    ObjShiftEntry.Show();
                    //ObjShiftEntry.Close();
                    ObjShiftEntry = null;
                    DoToolbarActions(this.Controls, "Cancel");
                    txt_Month.Text = Convert.ToString(DateTime.Now.Month);
                    if (txt_Month.Text.Length == 1)
                    {
                        txt_Month.Text = "0" + txt_Month.Text;
                    }
                    this.txtUser1.Text = this.userID;
                }
            }
        }

        private void txt_Year_Validating(object sender, CancelEventArgs e)
        {
            //if (txt_Year.Text.Length > 0)
            //{
            //    if(txt_Year.Text.Length!=4)
            //    {

            //    }

            //}
        }

        #endregion
    }
}