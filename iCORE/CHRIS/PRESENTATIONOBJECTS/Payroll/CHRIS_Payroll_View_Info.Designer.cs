namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_View_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_View_Info));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.grpPersonnel = new System.Windows.Forms.GroupBox();
            this.slPanelPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lookup_PR_CATEGORY = new CrplControlLibrary.LookupButton(this.components);
            this.lookup_Pr_P_No = new CrplControlLibrary.LookupButton(this.components);
            this.slDatePicker_PR_CONFIRM_ON = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_ANNUAL_PACK = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CONF_FLAG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_MONTH_AWARD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_FIRST_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_NEW_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_PR_CATEGORY = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slDatePicker_PR_JOINING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txt_ANNUAL_SALARY = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_GROSS_SALARY = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_ANNUAL_PF = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_PF_EXEMPT = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_CAR_ALLOWNCE = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_GHA_ALLOWNCE = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_GLOBAL_W_INC_AMT = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_GLOBAL_W_10_AMT = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_HL_MARKUP = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_DIVIDENT_STOCK = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_BONUS_PAYMENT = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_ZAKAT_DONATION = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_OTHER_ALLOWNCE = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_FUEL_ALLONCE = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_OPD_ALLOWNCE = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_TOTAL_TAXABLE_INCOME = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_FIXED_TAX = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_VAR_TAX = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_TAX_ALREADY_PAID = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_TAX_RATE_FROM_SLAB = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_FINAL_ANNUAL_TAX = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_REMAINING_MONTHS = new CrplControlLibrary.CrplTextBox(this.components);
            this.txt_W_ITAX = new CrplControlLibrary.CrplTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.TXT_SUBS_LOAN_TAX_AMT = new CrplControlLibrary.CrplTextBox(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.txt_W_INST_L = new CrplControlLibrary.CrplTextBox(this.components);
            this.label43 = new System.Windows.Forms.Label();
            this.txt_START_DATE_OF_CAL = new CrplControlLibrary.CrplTextBox(this.components);
            this.label44 = new System.Windows.Forms.Label();
            this.txt_END_DATE_OF_CALC = new CrplControlLibrary.CrplTextBox(this.components);
            this.label45 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label46 = new System.Windows.Forms.Label();
            this.txtInvestment = new CrplControlLibrary.CrplTextBox(this.components);
            this.txtInvrebate = new CrplControlLibrary.CrplTextBox(this.components);
            this.label47 = new System.Windows.Forms.Label();
            this.txtDonationRebate = new CrplControlLibrary.CrplTextBox(this.components);
            this.label48 = new System.Windows.Forms.Label();
            this.txtDonation = new CrplControlLibrary.CrplTextBox(this.components);
            this.label49 = new System.Windows.Forms.Label();
            this.txtUtilities = new CrplControlLibrary.CrplTextBox(this.components);
            this.label50 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grpPersonnel.SuspendLayout();
            this.slPanelPersonnel.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(6, 43);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(627, 25);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(12, 44);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(620, 33);
            this.label33.TabIndex = 22;
            this.label33.Text = "Select Employe For Tax Details";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpPersonnel
            // 
            this.grpPersonnel.Controls.Add(this.slPanelPersonnel);
            this.grpPersonnel.Location = new System.Drawing.Point(6, 101);
            this.grpPersonnel.Name = "grpPersonnel";
            this.grpPersonnel.Size = new System.Drawing.Size(626, 161);
            this.grpPersonnel.TabIndex = 18;
            this.grpPersonnel.TabStop = false;
            // 
            // slPanelPersonnel
            // 
            this.slPanelPersonnel.ConcurrentPanels = null;
            this.slPanelPersonnel.Controls.Add(this.lookup_PR_CATEGORY);
            this.slPanelPersonnel.Controls.Add(this.lookup_Pr_P_No);
            this.slPanelPersonnel.Controls.Add(this.slDatePicker_PR_CONFIRM_ON);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_ANNUAL_PACK);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_CONF_FLAG);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_MONTH_AWARD);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_FIRST_NAME);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_NEW_BRANCH);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_LEVEL);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_DESIG);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_CATEGORY);
            this.slPanelPersonnel.Controls.Add(this.label11);
            this.slPanelPersonnel.Controls.Add(this.label10);
            this.slPanelPersonnel.Controls.Add(this.label9);
            this.slPanelPersonnel.Controls.Add(this.label8);
            this.slPanelPersonnel.Controls.Add(this.label7);
            this.slPanelPersonnel.Controls.Add(this.label6);
            this.slPanelPersonnel.Controls.Add(this.label5);
            this.slPanelPersonnel.Controls.Add(this.label4);
            this.slPanelPersonnel.Controls.Add(this.label2);
            this.slPanelPersonnel.Controls.Add(this.label3);
            this.slPanelPersonnel.Controls.Add(this.label1);
            this.slPanelPersonnel.Controls.Add(this.slDatePicker_PR_JOINING_DATE);
            this.slPanelPersonnel.Controls.Add(this.txt_PR_P_NO);
            this.slPanelPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelPersonnel.DependentPanels = null;
            this.slPanelPersonnel.DisableDependentLoad = false;
            this.slPanelPersonnel.EnableDelete = false;
            this.slPanelPersonnel.EnableInsert = false;
            this.slPanelPersonnel.EnableQuery = false;
            this.slPanelPersonnel.EnableUpdate = false;
            this.slPanelPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.slPanelPersonnel.Location = new System.Drawing.Point(9, -10);
            this.slPanelPersonnel.MasterPanel = null;
            this.slPanelPersonnel.Name = "slPanelPersonnel";
            this.slPanelPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelPersonnel.Size = new System.Drawing.Size(617, 148);
            this.slPanelPersonnel.SPName = "CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE";
            this.slPanelPersonnel.TabIndex = 9;
            // 
            // lookup_PR_CATEGORY
            // 
            this.lookup_PR_CATEGORY.ActionLOVExists = "PR_CATEGORY_LOV1_EXISTS";
            this.lookup_PR_CATEGORY.ActionType = "PR_CATEGORY_LOV1";
            this.lookup_PR_CATEGORY.ConditionalFields = "";
            this.lookup_PR_CATEGORY.CustomEnabled = true;
            this.lookup_PR_CATEGORY.DataFieldMapping = "";
            this.lookup_PR_CATEGORY.DependentLovControls = "";
            this.lookup_PR_CATEGORY.HiddenColumns = "";
            this.lookup_PR_CATEGORY.Image = ((System.Drawing.Image)(resources.GetObject("lookup_PR_CATEGORY.Image")));
            this.lookup_PR_CATEGORY.LoadDependentEntities = false;
            this.lookup_PR_CATEGORY.Location = new System.Drawing.Point(176, 77);
            this.lookup_PR_CATEGORY.LookUpTitle = null;
            this.lookup_PR_CATEGORY.Name = "lookup_PR_CATEGORY";
            this.lookup_PR_CATEGORY.Size = new System.Drawing.Size(26, 21);
            this.lookup_PR_CATEGORY.SkipValidationOnLeave = false;
            this.lookup_PR_CATEGORY.SPName = "CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE";
            this.lookup_PR_CATEGORY.TabIndex = 26;
            this.lookup_PR_CATEGORY.TabStop = false;
            this.lookup_PR_CATEGORY.UseVisualStyleBackColor = true;
            // 
            // lookup_Pr_P_No
            // 
            this.lookup_Pr_P_No.ActionLOVExists = "ListExist";
            this.lookup_Pr_P_No.ActionType = "List";
            this.lookup_Pr_P_No.ConditionalFields = "";
            this.lookup_Pr_P_No.CustomEnabled = true;
            this.lookup_Pr_P_No.DataFieldMapping = "";
            this.lookup_Pr_P_No.DependentLovControls = "";
            this.lookup_Pr_P_No.HiddenColumns = "PR_DESIG|PR_LEVEL|PR_CATEGORY|PR_JOINING_DATE|PR_ANNUAL_PACK|PR_CONFIRM_ON|PR_CON" +
    "F_FLAG|PR_NEW_BRANCH|PR_MONTH_AWARD";
            this.lookup_Pr_P_No.Image = ((System.Drawing.Image)(resources.GetObject("lookup_Pr_P_No.Image")));
            this.lookup_Pr_P_No.LoadDependentEntities = true;
            this.lookup_Pr_P_No.Location = new System.Drawing.Point(233, 9);
            this.lookup_Pr_P_No.LookUpTitle = null;
            this.lookup_Pr_P_No.Name = "lookup_Pr_P_No";
            this.lookup_Pr_P_No.Size = new System.Drawing.Size(26, 21);
            this.lookup_Pr_P_No.SkipValidationOnLeave = false;
            this.lookup_Pr_P_No.SPName = "CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE";
            this.lookup_Pr_P_No.TabIndex = 24;
            this.lookup_Pr_P_No.TabStop = false;
            this.lookup_Pr_P_No.UseVisualStyleBackColor = true;
            // 
            // slDatePicker_PR_CONFIRM_ON
            // 
            this.slDatePicker_PR_CONFIRM_ON.CustomEnabled = true;
            this.slDatePicker_PR_CONFIRM_ON.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PR_CONFIRM_ON.DataFieldMapping = "PR_CONFIRM_ON";
            this.slDatePicker_PR_CONFIRM_ON.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PR_CONFIRM_ON.HasChanges = true;
            this.slDatePicker_PR_CONFIRM_ON.Location = new System.Drawing.Point(127, 124);
            this.slDatePicker_PR_CONFIRM_ON.Name = "slDatePicker_PR_CONFIRM_ON";
            this.slDatePicker_PR_CONFIRM_ON.NullValue = " ";
            this.slDatePicker_PR_CONFIRM_ON.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PR_CONFIRM_ON.TabIndex = 1;
            this.slDatePicker_PR_CONFIRM_ON.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            // 
            // txt_PR_ANNUAL_PACK
            // 
            this.txt_PR_ANNUAL_PACK.AllowSpace = true;
            this.txt_PR_ANNUAL_PACK.AssociatedLookUpName = "";
            this.txt_PR_ANNUAL_PACK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_ANNUAL_PACK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_ANNUAL_PACK.ContinuationTextBox = null;
            this.txt_PR_ANNUAL_PACK.CustomEnabled = true;
            this.txt_PR_ANNUAL_PACK.DataFieldMapping = "PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_ANNUAL_PACK.GetRecordsOnUpDownKeys = false;
            this.txt_PR_ANNUAL_PACK.IsDate = false;
            this.txt_PR_ANNUAL_PACK.Location = new System.Drawing.Point(127, 100);
            this.txt_PR_ANNUAL_PACK.MaxLength = 7;
            this.txt_PR_ANNUAL_PACK.Name = "txt_PR_ANNUAL_PACK";
            this.txt_PR_ANNUAL_PACK.NumberFormat = "###,###,##0.00";
            this.txt_PR_ANNUAL_PACK.Postfix = "";
            this.txt_PR_ANNUAL_PACK.Prefix = "";
            this.txt_PR_ANNUAL_PACK.ReadOnly = true;
            this.txt_PR_ANNUAL_PACK.Size = new System.Drawing.Size(100, 20);
            this.txt_PR_ANNUAL_PACK.SkipValidation = false;
            this.txt_PR_ANNUAL_PACK.TabIndex = 22;
            this.txt_PR_ANNUAL_PACK.TabStop = false;
            this.txt_PR_ANNUAL_PACK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PR_ANNUAL_PACK.TextType = CrplControlLibrary.TextType.Double;
            this.toolTip1.SetToolTip(this.txt_PR_ANNUAL_PACK, "Enter value for : PR_ANNUAL_PACK");
            // 
            // txt_PR_CONF_FLAG
            // 
            this.txt_PR_CONF_FLAG.AllowSpace = true;
            this.txt_PR_CONF_FLAG.AssociatedLookUpName = "";
            this.txt_PR_CONF_FLAG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CONF_FLAG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CONF_FLAG.ContinuationTextBox = null;
            this.txt_PR_CONF_FLAG.CustomEnabled = true;
            this.txt_PR_CONF_FLAG.DataFieldMapping = "PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CONF_FLAG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CONF_FLAG.IsDate = false;
            this.txt_PR_CONF_FLAG.IsRequired = true;
            this.txt_PR_CONF_FLAG.Location = new System.Drawing.Point(448, 122);
            this.txt_PR_CONF_FLAG.MaxLength = 1;
            this.txt_PR_CONF_FLAG.Name = "txt_PR_CONF_FLAG";
            this.txt_PR_CONF_FLAG.NumberFormat = "###,###,##0.00";
            this.txt_PR_CONF_FLAG.Postfix = "";
            this.txt_PR_CONF_FLAG.Prefix = "";
            this.txt_PR_CONF_FLAG.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_CONF_FLAG.SkipValidation = false;
            this.txt_PR_CONF_FLAG.TabIndex = 5;
            this.txt_PR_CONF_FLAG.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PR_MONTH_AWARD
            // 
            this.txt_PR_MONTH_AWARD.AllowSpace = true;
            this.txt_PR_MONTH_AWARD.AssociatedLookUpName = "";
            this.txt_PR_MONTH_AWARD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_MONTH_AWARD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_MONTH_AWARD.ContinuationTextBox = null;
            this.txt_PR_MONTH_AWARD.CustomEnabled = true;
            this.txt_PR_MONTH_AWARD.DataFieldMapping = "PR_MONTH_AWARD";
            this.txt_PR_MONTH_AWARD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_MONTH_AWARD.GetRecordsOnUpDownKeys = false;
            this.txt_PR_MONTH_AWARD.IsDate = false;
            this.txt_PR_MONTH_AWARD.Location = new System.Drawing.Point(448, 99);
            this.txt_PR_MONTH_AWARD.MaxLength = 2;
            this.txt_PR_MONTH_AWARD.Name = "txt_PR_MONTH_AWARD";
            this.txt_PR_MONTH_AWARD.NumberFormat = "###,###,##0.00";
            this.txt_PR_MONTH_AWARD.Postfix = "";
            this.txt_PR_MONTH_AWARD.Prefix = "";
            this.txt_PR_MONTH_AWARD.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_MONTH_AWARD.SkipValidation = false;
            this.txt_PR_MONTH_AWARD.TabIndex = 3;
            this.txt_PR_MONTH_AWARD.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_PR_MONTH_AWARD, "Enter value for : PR_MONTH_AWARD");
            // 
            // txt_PR_FIRST_NAME
            // 
            this.txt_PR_FIRST_NAME.AllowSpace = true;
            this.txt_PR_FIRST_NAME.AssociatedLookUpName = "";
            this.txt_PR_FIRST_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_FIRST_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_FIRST_NAME.ContinuationTextBox = null;
            this.txt_PR_FIRST_NAME.CustomEnabled = true;
            this.txt_PR_FIRST_NAME.DataFieldMapping = "PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_FIRST_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_PR_FIRST_NAME.IsDate = false;
            this.txt_PR_FIRST_NAME.Location = new System.Drawing.Point(127, 34);
            this.txt_PR_FIRST_NAME.Name = "txt_PR_FIRST_NAME";
            this.txt_PR_FIRST_NAME.NumberFormat = "###,###,##0.00";
            this.txt_PR_FIRST_NAME.Postfix = "";
            this.txt_PR_FIRST_NAME.Prefix = "";
            this.txt_PR_FIRST_NAME.ReadOnly = true;
            this.txt_PR_FIRST_NAME.Size = new System.Drawing.Size(200, 20);
            this.txt_PR_FIRST_NAME.SkipValidation = false;
            this.txt_PR_FIRST_NAME.TabIndex = 18;
            this.txt_PR_FIRST_NAME.TabStop = false;
            this.txt_PR_FIRST_NAME.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_FIRST_NAME, "Enter value for : PR_FIRST_NAME");
            // 
            // txt_PR_NEW_BRANCH
            // 
            this.txt_PR_NEW_BRANCH.AllowSpace = true;
            this.txt_PR_NEW_BRANCH.AssociatedLookUpName = "";
            this.txt_PR_NEW_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_NEW_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_NEW_BRANCH.ContinuationTextBox = null;
            this.txt_PR_NEW_BRANCH.CustomEnabled = true;
            this.txt_PR_NEW_BRANCH.DataFieldMapping = "PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_NEW_BRANCH.GetRecordsOnUpDownKeys = false;
            this.txt_PR_NEW_BRANCH.IsDate = false;
            this.txt_PR_NEW_BRANCH.Location = new System.Drawing.Point(448, 12);
            this.txt_PR_NEW_BRANCH.MaxLength = 3;
            this.txt_PR_NEW_BRANCH.Name = "txt_PR_NEW_BRANCH";
            this.txt_PR_NEW_BRANCH.NumberFormat = "###,###,##0.00";
            this.txt_PR_NEW_BRANCH.Postfix = "";
            this.txt_PR_NEW_BRANCH.Prefix = "";
            this.txt_PR_NEW_BRANCH.ReadOnly = true;
            this.txt_PR_NEW_BRANCH.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_NEW_BRANCH.SkipValidation = false;
            this.txt_PR_NEW_BRANCH.TabIndex = 17;
            this.txt_PR_NEW_BRANCH.TabStop = false;
            this.txt_PR_NEW_BRANCH.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_NEW_BRANCH, "Enter value for : PR_BRANCH");
            // 
            // txt_PR_LEVEL
            // 
            this.txt_PR_LEVEL.AllowSpace = true;
            this.txt_PR_LEVEL.AssociatedLookUpName = "";
            this.txt_PR_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_LEVEL.ContinuationTextBox = null;
            this.txt_PR_LEVEL.CustomEnabled = true;
            this.txt_PR_LEVEL.DataFieldMapping = "PR_LEVEL";
            this.txt_PR_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_LEVEL.GetRecordsOnUpDownKeys = false;
            this.txt_PR_LEVEL.IsDate = false;
            this.txt_PR_LEVEL.Location = new System.Drawing.Point(448, 53);
            this.txt_PR_LEVEL.Name = "txt_PR_LEVEL";
            this.txt_PR_LEVEL.NumberFormat = "###,###,##0.00";
            this.txt_PR_LEVEL.Postfix = "";
            this.txt_PR_LEVEL.Prefix = "";
            this.txt_PR_LEVEL.ReadOnly = true;
            this.txt_PR_LEVEL.Size = new System.Drawing.Size(34, 20);
            this.txt_PR_LEVEL.SkipValidation = false;
            this.txt_PR_LEVEL.TabIndex = 16;
            this.txt_PR_LEVEL.TabStop = false;
            this.txt_PR_LEVEL.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_LEVEL, "Enter value for : PR_LEVEL");
            // 
            // txt_PR_DESIG
            // 
            this.txt_PR_DESIG.AllowSpace = true;
            this.txt_PR_DESIG.AssociatedLookUpName = "";
            this.txt_PR_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_DESIG.ContinuationTextBox = null;
            this.txt_PR_DESIG.CustomEnabled = true;
            this.txt_PR_DESIG.DataFieldMapping = "PR_DESIG";
            this.txt_PR_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_DESIG.GetRecordsOnUpDownKeys = false;
            this.txt_PR_DESIG.IsDate = false;
            this.txt_PR_DESIG.Location = new System.Drawing.Point(127, 56);
            this.txt_PR_DESIG.MaxLength = 3;
            this.txt_PR_DESIG.Name = "txt_PR_DESIG";
            this.txt_PR_DESIG.NumberFormat = "###,###,##0.00";
            this.txt_PR_DESIG.Postfix = "";
            this.txt_PR_DESIG.Prefix = "";
            this.txt_PR_DESIG.ReadOnly = true;
            this.txt_PR_DESIG.Size = new System.Drawing.Size(43, 20);
            this.txt_PR_DESIG.SkipValidation = false;
            this.txt_PR_DESIG.TabIndex = 15;
            this.txt_PR_DESIG.TabStop = false;
            this.txt_PR_DESIG.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_DESIG, "Enter value for : PR_DESIG");
            // 
            // txt_PR_CATEGORY
            // 
            this.txt_PR_CATEGORY.AllowSpace = true;
            this.txt_PR_CATEGORY.AssociatedLookUpName = "lookup_PR_CATEGORY";
            this.txt_PR_CATEGORY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_CATEGORY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_CATEGORY.ContinuationTextBox = null;
            this.txt_PR_CATEGORY.CustomEnabled = true;
            this.txt_PR_CATEGORY.DataFieldMapping = "PR_CATEGORY";
            this.txt_PR_CATEGORY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_CATEGORY.GetRecordsOnUpDownKeys = false;
            this.txt_PR_CATEGORY.IsDate = false;
            this.txt_PR_CATEGORY.IsLookUpField = true;
            this.txt_PR_CATEGORY.IsRequired = true;
            this.txt_PR_CATEGORY.Location = new System.Drawing.Point(127, 78);
            this.txt_PR_CATEGORY.MaxLength = 1;
            this.txt_PR_CATEGORY.Name = "txt_PR_CATEGORY";
            this.txt_PR_CATEGORY.NumberFormat = "###,###,##0.00";
            this.txt_PR_CATEGORY.Postfix = "";
            this.txt_PR_CATEGORY.Prefix = "";
            this.txt_PR_CATEGORY.Size = new System.Drawing.Size(43, 20);
            this.txt_PR_CATEGORY.SkipValidation = false;
            this.txt_PR_CATEGORY.TabIndex = 2;
            this.txt_PR_CATEGORY.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txt_PR_CATEGORY, "Enter value for : PR_CATEGORY");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(376, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Branch   :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(354, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Joining Date  :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(29, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Designation   :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Category   :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Annual Package   :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(339, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Monthly Award   :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(385, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Level   :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Conf. Date   :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(339, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Confirmed Y/N   :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(61, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Name   :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Personnel No.  :";
            // 
            // slDatePicker_PR_JOINING_DATE
            // 
            this.slDatePicker_PR_JOINING_DATE.CustomEnabled = true;
            this.slDatePicker_PR_JOINING_DATE.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_PR_JOINING_DATE.DataFieldMapping = "PR_JOINING_DATE";
            this.slDatePicker_PR_JOINING_DATE.Enabled = false;
            this.slDatePicker_PR_JOINING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_PR_JOINING_DATE.HasChanges = true;
            this.slDatePicker_PR_JOINING_DATE.Location = new System.Drawing.Point(448, 76);
            this.slDatePicker_PR_JOINING_DATE.Name = "slDatePicker_PR_JOINING_DATE";
            this.slDatePicker_PR_JOINING_DATE.NullValue = " ";
            this.slDatePicker_PR_JOINING_DATE.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker_PR_JOINING_DATE.TabIndex = 1;
            this.slDatePicker_PR_JOINING_DATE.TabStop = false;
            this.slDatePicker_PR_JOINING_DATE.Value = new System.DateTime(2011, 1, 29, 0, 0, 0, 0);
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "lookup_Pr_P_No";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "PR_P_NO";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.IsLookUpField = true;
            this.txt_PR_P_NO.IsRequired = true;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(127, 12);
            this.txt_PR_P_NO.MaxLength = 6;
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_PR_P_NO.SkipValidation = true;
            this.txt_PR_P_NO.TabIndex = 0;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txt_PR_P_NO, "Enter value for : PR_P_NO");
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(14, 242);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(616, 21);
            this.label12.TabIndex = 23;
            this.label12.Text = "View Tax Details";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_ANNUAL_SALARY
            // 
            this.txt_ANNUAL_SALARY.AllowSpace = true;
            this.txt_ANNUAL_SALARY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ANNUAL_SALARY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ANNUAL_SALARY.ContinuationTextBox = null;
            this.txt_ANNUAL_SALARY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ANNUAL_SALARY.Location = new System.Drawing.Point(139, 363);
            this.txt_ANNUAL_SALARY.Name = "txt_ANNUAL_SALARY";
            this.txt_ANNUAL_SALARY.NumberFormat = "###,###,##0.00";
            this.txt_ANNUAL_SALARY.Postfix = "";
            this.txt_ANNUAL_SALARY.Prefix = "";
            this.txt_ANNUAL_SALARY.ReadOnly = true;
            this.txt_ANNUAL_SALARY.Size = new System.Drawing.Size(100, 20);
            this.txt_ANNUAL_SALARY.TabIndex = 24;
            this.txt_ANNUAL_SALARY.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_GROSS_SALARY
            // 
            this.txt_GROSS_SALARY.AllowSpace = true;
            this.txt_GROSS_SALARY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_GROSS_SALARY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_GROSS_SALARY.ContinuationTextBox = null;
            this.txt_GROSS_SALARY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GROSS_SALARY.Location = new System.Drawing.Point(139, 389);
            this.txt_GROSS_SALARY.Name = "txt_GROSS_SALARY";
            this.txt_GROSS_SALARY.NumberFormat = "###,###,##0.00";
            this.txt_GROSS_SALARY.Postfix = "";
            this.txt_GROSS_SALARY.Prefix = "";
            this.txt_GROSS_SALARY.ReadOnly = true;
            this.txt_GROSS_SALARY.Size = new System.Drawing.Size(100, 20);
            this.txt_GROSS_SALARY.TabIndex = 28;
            this.txt_GROSS_SALARY.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_ANNUAL_PF
            // 
            this.txt_ANNUAL_PF.AllowSpace = true;
            this.txt_ANNUAL_PF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ANNUAL_PF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ANNUAL_PF.ContinuationTextBox = null;
            this.txt_ANNUAL_PF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ANNUAL_PF.Location = new System.Drawing.Point(139, 412);
            this.txt_ANNUAL_PF.Name = "txt_ANNUAL_PF";
            this.txt_ANNUAL_PF.NumberFormat = "###,###,##0.00";
            this.txt_ANNUAL_PF.Postfix = "";
            this.txt_ANNUAL_PF.Prefix = "";
            this.txt_ANNUAL_PF.ReadOnly = true;
            this.txt_ANNUAL_PF.Size = new System.Drawing.Size(100, 20);
            this.txt_ANNUAL_PF.TabIndex = 29;
            this.txt_ANNUAL_PF.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_PF_EXEMPT
            // 
            this.txt_PF_EXEMPT.AllowSpace = true;
            this.txt_PF_EXEMPT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PF_EXEMPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PF_EXEMPT.ContinuationTextBox = null;
            this.txt_PF_EXEMPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PF_EXEMPT.Location = new System.Drawing.Point(139, 435);
            this.txt_PF_EXEMPT.Name = "txt_PF_EXEMPT";
            this.txt_PF_EXEMPT.NumberFormat = "###,###,##0.00";
            this.txt_PF_EXEMPT.Postfix = "";
            this.txt_PF_EXEMPT.Prefix = "";
            this.txt_PF_EXEMPT.ReadOnly = true;
            this.txt_PF_EXEMPT.Size = new System.Drawing.Size(100, 20);
            this.txt_PF_EXEMPT.TabIndex = 30;
            this.txt_PF_EXEMPT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_CAR_ALLOWNCE
            // 
            this.txt_CAR_ALLOWNCE.AllowSpace = true;
            this.txt_CAR_ALLOWNCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_CAR_ALLOWNCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_CAR_ALLOWNCE.ContinuationTextBox = null;
            this.txt_CAR_ALLOWNCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_CAR_ALLOWNCE.Location = new System.Drawing.Point(139, 457);
            this.txt_CAR_ALLOWNCE.Name = "txt_CAR_ALLOWNCE";
            this.txt_CAR_ALLOWNCE.NumberFormat = "###,###,##0.00";
            this.txt_CAR_ALLOWNCE.Postfix = "";
            this.txt_CAR_ALLOWNCE.Prefix = "";
            this.txt_CAR_ALLOWNCE.ReadOnly = true;
            this.txt_CAR_ALLOWNCE.Size = new System.Drawing.Size(100, 20);
            this.txt_CAR_ALLOWNCE.TabIndex = 31;
            this.txt_CAR_ALLOWNCE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_GHA_ALLOWNCE
            // 
            this.txt_GHA_ALLOWNCE.AllowSpace = true;
            this.txt_GHA_ALLOWNCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_GHA_ALLOWNCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_GHA_ALLOWNCE.ContinuationTextBox = null;
            this.txt_GHA_ALLOWNCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GHA_ALLOWNCE.Location = new System.Drawing.Point(139, 479);
            this.txt_GHA_ALLOWNCE.Name = "txt_GHA_ALLOWNCE";
            this.txt_GHA_ALLOWNCE.NumberFormat = "###,###,##0.00";
            this.txt_GHA_ALLOWNCE.Postfix = "";
            this.txt_GHA_ALLOWNCE.Prefix = "";
            this.txt_GHA_ALLOWNCE.ReadOnly = true;
            this.txt_GHA_ALLOWNCE.Size = new System.Drawing.Size(100, 20);
            this.txt_GHA_ALLOWNCE.TabIndex = 32;
            this.txt_GHA_ALLOWNCE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_GLOBAL_W_INC_AMT
            // 
            this.txt_GLOBAL_W_INC_AMT.AllowSpace = true;
            this.txt_GLOBAL_W_INC_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_GLOBAL_W_INC_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_GLOBAL_W_INC_AMT.ContinuationTextBox = null;
            this.txt_GLOBAL_W_INC_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GLOBAL_W_INC_AMT.Location = new System.Drawing.Point(473, 639);
            this.txt_GLOBAL_W_INC_AMT.Name = "txt_GLOBAL_W_INC_AMT";
            this.txt_GLOBAL_W_INC_AMT.NumberFormat = "###,###,##0.00";
            this.txt_GLOBAL_W_INC_AMT.Postfix = "";
            this.txt_GLOBAL_W_INC_AMT.Prefix = "";
            this.txt_GLOBAL_W_INC_AMT.ReadOnly = true;
            this.txt_GLOBAL_W_INC_AMT.Size = new System.Drawing.Size(100, 20);
            this.txt_GLOBAL_W_INC_AMT.TabIndex = 41;
            this.txt_GLOBAL_W_INC_AMT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_GLOBAL_W_10_AMT
            // 
            this.txt_GLOBAL_W_10_AMT.AllowSpace = true;
            this.txt_GLOBAL_W_10_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_GLOBAL_W_10_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_GLOBAL_W_10_AMT.ContinuationTextBox = null;
            this.txt_GLOBAL_W_10_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GLOBAL_W_10_AMT.Location = new System.Drawing.Point(142, 663);
            this.txt_GLOBAL_W_10_AMT.Name = "txt_GLOBAL_W_10_AMT";
            this.txt_GLOBAL_W_10_AMT.NumberFormat = "###,###,##0.00";
            this.txt_GLOBAL_W_10_AMT.Postfix = "";
            this.txt_GLOBAL_W_10_AMT.Prefix = "";
            this.txt_GLOBAL_W_10_AMT.ReadOnly = true;
            this.txt_GLOBAL_W_10_AMT.Size = new System.Drawing.Size(100, 20);
            this.txt_GLOBAL_W_10_AMT.TabIndex = 40;
            this.txt_GLOBAL_W_10_AMT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_HL_MARKUP
            // 
            this.txt_HL_MARKUP.AllowSpace = true;
            this.txt_HL_MARKUP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_HL_MARKUP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_HL_MARKUP.ContinuationTextBox = null;
            this.txt_HL_MARKUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_HL_MARKUP.Location = new System.Drawing.Point(141, 639);
            this.txt_HL_MARKUP.Name = "txt_HL_MARKUP";
            this.txt_HL_MARKUP.NumberFormat = "###,###,##0.00";
            this.txt_HL_MARKUP.Postfix = "";
            this.txt_HL_MARKUP.Prefix = "";
            this.txt_HL_MARKUP.ReadOnly = true;
            this.txt_HL_MARKUP.Size = new System.Drawing.Size(100, 20);
            this.txt_HL_MARKUP.TabIndex = 39;
            this.txt_HL_MARKUP.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_DIVIDENT_STOCK
            // 
            this.txt_DIVIDENT_STOCK.AllowSpace = true;
            this.txt_DIVIDENT_STOCK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_DIVIDENT_STOCK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_DIVIDENT_STOCK.ContinuationTextBox = null;
            this.txt_DIVIDENT_STOCK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_DIVIDENT_STOCK.Location = new System.Drawing.Point(140, 616);
            this.txt_DIVIDENT_STOCK.Name = "txt_DIVIDENT_STOCK";
            this.txt_DIVIDENT_STOCK.NumberFormat = "###,###,##0.00";
            this.txt_DIVIDENT_STOCK.Postfix = "";
            this.txt_DIVIDENT_STOCK.Prefix = "";
            this.txt_DIVIDENT_STOCK.ReadOnly = true;
            this.txt_DIVIDENT_STOCK.Size = new System.Drawing.Size(100, 20);
            this.txt_DIVIDENT_STOCK.TabIndex = 38;
            this.txt_DIVIDENT_STOCK.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_BONUS_PAYMENT
            // 
            this.txt_BONUS_PAYMENT.AllowSpace = true;
            this.txt_BONUS_PAYMENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_BONUS_PAYMENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_BONUS_PAYMENT.ContinuationTextBox = null;
            this.txt_BONUS_PAYMENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_BONUS_PAYMENT.Location = new System.Drawing.Point(140, 593);
            this.txt_BONUS_PAYMENT.Name = "txt_BONUS_PAYMENT";
            this.txt_BONUS_PAYMENT.NumberFormat = "###,###,##0.00";
            this.txt_BONUS_PAYMENT.Postfix = "";
            this.txt_BONUS_PAYMENT.Prefix = "";
            this.txt_BONUS_PAYMENT.ReadOnly = true;
            this.txt_BONUS_PAYMENT.Size = new System.Drawing.Size(100, 20);
            this.txt_BONUS_PAYMENT.TabIndex = 37;
            this.txt_BONUS_PAYMENT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_ZAKAT_DONATION
            // 
            this.txt_ZAKAT_DONATION.AllowSpace = true;
            this.txt_ZAKAT_DONATION.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ZAKAT_DONATION.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ZAKAT_DONATION.ContinuationTextBox = null;
            this.txt_ZAKAT_DONATION.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ZAKAT_DONATION.Location = new System.Drawing.Point(140, 570);
            this.txt_ZAKAT_DONATION.Name = "txt_ZAKAT_DONATION";
            this.txt_ZAKAT_DONATION.NumberFormat = "###,###,##0.00";
            this.txt_ZAKAT_DONATION.Postfix = "";
            this.txt_ZAKAT_DONATION.Prefix = "";
            this.txt_ZAKAT_DONATION.ReadOnly = true;
            this.txt_ZAKAT_DONATION.Size = new System.Drawing.Size(100, 20);
            this.txt_ZAKAT_DONATION.TabIndex = 36;
            this.txt_ZAKAT_DONATION.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OTHER_ALLOWNCE
            // 
            this.txt_OTHER_ALLOWNCE.AllowSpace = true;
            this.txt_OTHER_ALLOWNCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OTHER_ALLOWNCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OTHER_ALLOWNCE.ContinuationTextBox = null;
            this.txt_OTHER_ALLOWNCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OTHER_ALLOWNCE.Location = new System.Drawing.Point(140, 547);
            this.txt_OTHER_ALLOWNCE.Name = "txt_OTHER_ALLOWNCE";
            this.txt_OTHER_ALLOWNCE.NumberFormat = "###,###,##0.00";
            this.txt_OTHER_ALLOWNCE.Postfix = "";
            this.txt_OTHER_ALLOWNCE.Prefix = "";
            this.txt_OTHER_ALLOWNCE.ReadOnly = true;
            this.txt_OTHER_ALLOWNCE.Size = new System.Drawing.Size(100, 20);
            this.txt_OTHER_ALLOWNCE.TabIndex = 35;
            this.txt_OTHER_ALLOWNCE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_FUEL_ALLONCE
            // 
            this.txt_FUEL_ALLONCE.AllowSpace = true;
            this.txt_FUEL_ALLONCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FUEL_ALLONCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FUEL_ALLONCE.ContinuationTextBox = null;
            this.txt_FUEL_ALLONCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FUEL_ALLONCE.Location = new System.Drawing.Point(139, 524);
            this.txt_FUEL_ALLONCE.Name = "txt_FUEL_ALLONCE";
            this.txt_FUEL_ALLONCE.NumberFormat = "###,###,##0.00";
            this.txt_FUEL_ALLONCE.Postfix = "";
            this.txt_FUEL_ALLONCE.Prefix = "";
            this.txt_FUEL_ALLONCE.ReadOnly = true;
            this.txt_FUEL_ALLONCE.Size = new System.Drawing.Size(100, 20);
            this.txt_FUEL_ALLONCE.TabIndex = 34;
            this.txt_FUEL_ALLONCE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OPD_ALLOWNCE
            // 
            this.txt_OPD_ALLOWNCE.AllowSpace = true;
            this.txt_OPD_ALLOWNCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OPD_ALLOWNCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OPD_ALLOWNCE.ContinuationTextBox = null;
            this.txt_OPD_ALLOWNCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OPD_ALLOWNCE.Location = new System.Drawing.Point(139, 501);
            this.txt_OPD_ALLOWNCE.Name = "txt_OPD_ALLOWNCE";
            this.txt_OPD_ALLOWNCE.NumberFormat = "###,###,##0.00";
            this.txt_OPD_ALLOWNCE.Postfix = "";
            this.txt_OPD_ALLOWNCE.Prefix = "";
            this.txt_OPD_ALLOWNCE.ReadOnly = true;
            this.txt_OPD_ALLOWNCE.Size = new System.Drawing.Size(100, 20);
            this.txt_OPD_ALLOWNCE.TabIndex = 33;
            this.txt_OPD_ALLOWNCE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_TOTAL_TAXABLE_INCOME
            // 
            this.txt_TOTAL_TAXABLE_INCOME.AllowSpace = true;
            this.txt_TOTAL_TAXABLE_INCOME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_TOTAL_TAXABLE_INCOME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_TOTAL_TAXABLE_INCOME.ContinuationTextBox = null;
            this.txt_TOTAL_TAXABLE_INCOME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TOTAL_TAXABLE_INCOME.Location = new System.Drawing.Point(473, 388);
            this.txt_TOTAL_TAXABLE_INCOME.Name = "txt_TOTAL_TAXABLE_INCOME";
            this.txt_TOTAL_TAXABLE_INCOME.NumberFormat = "###,###,##0.00";
            this.txt_TOTAL_TAXABLE_INCOME.Postfix = "";
            this.txt_TOTAL_TAXABLE_INCOME.Prefix = "";
            this.txt_TOTAL_TAXABLE_INCOME.ReadOnly = true;
            this.txt_TOTAL_TAXABLE_INCOME.Size = new System.Drawing.Size(100, 20);
            this.txt_TOTAL_TAXABLE_INCOME.TabIndex = 42;
            this.txt_TOTAL_TAXABLE_INCOME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT
            // 
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.AllowSpace = true;
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.ContinuationTextBox = null;
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Location = new System.Drawing.Point(473, 411);
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Name = "txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT";
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.NumberFormat = "###,###,##0.00";
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Postfix = "";
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Prefix = "";
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.ReadOnly = true;
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.Size = new System.Drawing.Size(100, 20);
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.TabIndex = 43;
            this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_FIXED_TAX
            // 
            this.txt_FIXED_TAX.AllowSpace = true;
            this.txt_FIXED_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FIXED_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FIXED_TAX.ContinuationTextBox = null;
            this.txt_FIXED_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FIXED_TAX.Location = new System.Drawing.Point(473, 435);
            this.txt_FIXED_TAX.Name = "txt_FIXED_TAX";
            this.txt_FIXED_TAX.NumberFormat = "###,###,##0.00";
            this.txt_FIXED_TAX.Postfix = "";
            this.txt_FIXED_TAX.Prefix = "";
            this.txt_FIXED_TAX.ReadOnly = true;
            this.txt_FIXED_TAX.Size = new System.Drawing.Size(100, 20);
            this.txt_FIXED_TAX.TabIndex = 44;
            this.txt_FIXED_TAX.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_VAR_TAX
            // 
            this.txt_VAR_TAX.AllowSpace = true;
            this.txt_VAR_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_VAR_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_VAR_TAX.ContinuationTextBox = null;
            this.txt_VAR_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_VAR_TAX.Location = new System.Drawing.Point(473, 458);
            this.txt_VAR_TAX.Name = "txt_VAR_TAX";
            this.txt_VAR_TAX.NumberFormat = "###,###,##0.00";
            this.txt_VAR_TAX.Postfix = "";
            this.txt_VAR_TAX.Prefix = "";
            this.txt_VAR_TAX.ReadOnly = true;
            this.txt_VAR_TAX.Size = new System.Drawing.Size(100, 20);
            this.txt_VAR_TAX.TabIndex = 45;
            this.txt_VAR_TAX.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_TAX_EXEMPT_LIMIT_FROM_SLAB
            // 
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.AllowSpace = true;
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.ContinuationTextBox = null;
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Location = new System.Drawing.Point(473, 480);
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Name = "txt_TAX_EXEMPT_LIMIT_FROM_SLAB";
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.NumberFormat = "###,###,##0.00";
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Postfix = "";
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Prefix = "";
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.ReadOnly = true;
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.Size = new System.Drawing.Size(100, 20);
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.TabIndex = 46;
            this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_TAX_ALREADY_PAID
            // 
            this.txt_TAX_ALREADY_PAID.AllowSpace = true;
            this.txt_TAX_ALREADY_PAID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_TAX_ALREADY_PAID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_TAX_ALREADY_PAID.ContinuationTextBox = null;
            this.txt_TAX_ALREADY_PAID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TAX_ALREADY_PAID.Location = new System.Drawing.Point(473, 502);
            this.txt_TAX_ALREADY_PAID.Name = "txt_TAX_ALREADY_PAID";
            this.txt_TAX_ALREADY_PAID.NumberFormat = "###,###,##0.00";
            this.txt_TAX_ALREADY_PAID.Postfix = "";
            this.txt_TAX_ALREADY_PAID.Prefix = "";
            this.txt_TAX_ALREADY_PAID.ReadOnly = true;
            this.txt_TAX_ALREADY_PAID.Size = new System.Drawing.Size(100, 20);
            this.txt_TAX_ALREADY_PAID.TabIndex = 47;
            this.txt_TAX_ALREADY_PAID.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_TAX_RATE_FROM_SLAB
            // 
            this.txt_TAX_RATE_FROM_SLAB.AllowSpace = true;
            this.txt_TAX_RATE_FROM_SLAB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_TAX_RATE_FROM_SLAB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_TAX_RATE_FROM_SLAB.ContinuationTextBox = null;
            this.txt_TAX_RATE_FROM_SLAB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TAX_RATE_FROM_SLAB.Location = new System.Drawing.Point(473, 524);
            this.txt_TAX_RATE_FROM_SLAB.Name = "txt_TAX_RATE_FROM_SLAB";
            this.txt_TAX_RATE_FROM_SLAB.NumberFormat = "###,###,##0.00";
            this.txt_TAX_RATE_FROM_SLAB.Postfix = "";
            this.txt_TAX_RATE_FROM_SLAB.Prefix = "";
            this.txt_TAX_RATE_FROM_SLAB.ReadOnly = true;
            this.txt_TAX_RATE_FROM_SLAB.Size = new System.Drawing.Size(100, 20);
            this.txt_TAX_RATE_FROM_SLAB.TabIndex = 48;
            this.txt_TAX_RATE_FROM_SLAB.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_FINAL_ANNUAL_TAX
            // 
            this.txt_FINAL_ANNUAL_TAX.AllowSpace = true;
            this.txt_FINAL_ANNUAL_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_FINAL_ANNUAL_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_FINAL_ANNUAL_TAX.ContinuationTextBox = null;
            this.txt_FINAL_ANNUAL_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FINAL_ANNUAL_TAX.Location = new System.Drawing.Point(473, 547);
            this.txt_FINAL_ANNUAL_TAX.Name = "txt_FINAL_ANNUAL_TAX";
            this.txt_FINAL_ANNUAL_TAX.NumberFormat = "###,###,##0.00";
            this.txt_FINAL_ANNUAL_TAX.Postfix = "";
            this.txt_FINAL_ANNUAL_TAX.Prefix = "";
            this.txt_FINAL_ANNUAL_TAX.ReadOnly = true;
            this.txt_FINAL_ANNUAL_TAX.Size = new System.Drawing.Size(100, 20);
            this.txt_FINAL_ANNUAL_TAX.TabIndex = 49;
            this.txt_FINAL_ANNUAL_TAX.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_REMAINING_MONTHS
            // 
            this.txt_REMAINING_MONTHS.AllowSpace = true;
            this.txt_REMAINING_MONTHS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_REMAINING_MONTHS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_REMAINING_MONTHS.ContinuationTextBox = null;
            this.txt_REMAINING_MONTHS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_REMAINING_MONTHS.Location = new System.Drawing.Point(473, 570);
            this.txt_REMAINING_MONTHS.Name = "txt_REMAINING_MONTHS";
            this.txt_REMAINING_MONTHS.NumberFormat = "###,###,##0.00";
            this.txt_REMAINING_MONTHS.Postfix = "";
            this.txt_REMAINING_MONTHS.Prefix = "";
            this.txt_REMAINING_MONTHS.ReadOnly = true;
            this.txt_REMAINING_MONTHS.Size = new System.Drawing.Size(100, 20);
            this.txt_REMAINING_MONTHS.TabIndex = 50;
            this.txt_REMAINING_MONTHS.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_ITAX
            // 
            this.txt_W_ITAX.AllowSpace = true;
            this.txt_W_ITAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_ITAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_ITAX.ContinuationTextBox = null;
            this.txt_W_ITAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_ITAX.Location = new System.Drawing.Point(473, 593);
            this.txt_W_ITAX.Name = "txt_W_ITAX";
            this.txt_W_ITAX.NumberFormat = "###,###,##0.00";
            this.txt_W_ITAX.Postfix = "";
            this.txt_W_ITAX.Prefix = "";
            this.txt_W_ITAX.ReadOnly = true;
            this.txt_W_ITAX.Size = new System.Drawing.Size(100, 20);
            this.txt_W_ITAX.TabIndex = 51;
            this.txt_W_ITAX.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(47, 367);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 13);
            this.label13.TabIndex = 52;
            this.label13.Text = "Annual Salary:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(42, 618);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 53;
            this.label14.Text = "Divident Stock:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 596);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 13);
            this.label15.TabIndex = 54;
            this.label15.Text = "Bounce Payments:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(37, 573);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 13);
            this.label16.TabIndex = 55;
            this.label16.Text = "Zakat Allownce:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(39, 550);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 56;
            this.label17.Text = "Other Allownce:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(45, 527);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 57;
            this.label18.Text = "Fuel Allownce:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(43, 504);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 13);
            this.label19.TabIndex = 58;
            this.label19.Text = "OPD Allownce:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(43, 482);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 13);
            this.label20.TabIndex = 59;
            this.label20.Text = "GHA Allownce:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(50, 460);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 13);
            this.label21.TabIndex = 60;
            this.label21.Text = "Car Allownce:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(65, 438);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 61;
            this.label22.Text = "PF Exempt:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(68, 415);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 62;
            this.label23.Text = "Annual PF:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(54, 392);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 63;
            this.label24.Text = "Gross Salary:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(362, 505);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(107, 13);
            this.label28.TabIndex = 67;
            this.label28.Text = "Tax Already Paid:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(305, 483);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(164, 13);
            this.label29.TabIndex = 68;
            this.label29.Text = "Tax Exempt Limit from Slab:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(414, 461);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 13);
            this.label30.TabIndex = 69;
            this.label30.Text = "Var Tax:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(404, 438);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(66, 13);
            this.label31.TabIndex = 70;
            this.label31.Text = "Fixed Tax:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(255, 414);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(215, 13);
            this.label32.TabIndex = 71;
            this.label32.Text = "Amount Exceeding Excemption Limit:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(336, 392);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(134, 13);
            this.label34.TabIndex = 72;
            this.label34.Text = "Total Taxable Income:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(361, 642);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(110, 13);
            this.label35.TabIndex = 73;
            this.label35.Text = "Global W Inc Amt:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(34, 666);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 13);
            this.label36.TabIndex = 74;
            this.label36.Text = "Global W 10 amt:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(64, 642);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(73, 13);
            this.label37.TabIndex = 75;
            this.label37.Text = "HL Markup:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(416, 596);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(52, 13);
            this.label38.TabIndex = 76;
            this.label38.Text = "W ITax:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(354, 573);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(115, 13);
            this.label39.TabIndex = 77;
            this.label39.Text = "Ramaining Months:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(362, 550);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 13);
            this.label40.TabIndex = 78;
            this.label40.Text = "Final Annual Tax:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(346, 527);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(123, 13);
            this.label41.TabIndex = 79;
            this.label41.Text = "Tax Rate From Slab:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(13, 343);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(121, 13);
            this.label42.TabIndex = 81;
            this.label42.Text = "Subs Loan Tax Amt:\r\n";
            // 
            // TXT_SUBS_LOAN_TAX_AMT
            // 
            this.TXT_SUBS_LOAN_TAX_AMT.AllowSpace = true;
            this.TXT_SUBS_LOAN_TAX_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXT_SUBS_LOAN_TAX_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TXT_SUBS_LOAN_TAX_AMT.ContinuationTextBox = null;
            this.TXT_SUBS_LOAN_TAX_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_SUBS_LOAN_TAX_AMT.Location = new System.Drawing.Point(139, 340);
            this.TXT_SUBS_LOAN_TAX_AMT.Name = "TXT_SUBS_LOAN_TAX_AMT";
            this.TXT_SUBS_LOAN_TAX_AMT.NumberFormat = "###,###,##0.00";
            this.TXT_SUBS_LOAN_TAX_AMT.Postfix = "";
            this.TXT_SUBS_LOAN_TAX_AMT.Prefix = "";
            this.TXT_SUBS_LOAN_TAX_AMT.ReadOnly = true;
            this.TXT_SUBS_LOAN_TAX_AMT.Size = new System.Drawing.Size(100, 20);
            this.TXT_SUBS_LOAN_TAX_AMT.TabIndex = 80;
            this.TXT_SUBS_LOAN_TAX_AMT.TextType = CrplControlLibrary.TextType.String;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(410, 619);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 13);
            this.label26.TabIndex = 83;
            this.label26.Text = "W Inst L:";
            // 
            // txt_W_INST_L
            // 
            this.txt_W_INST_L.AllowSpace = true;
            this.txt_W_INST_L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_INST_L.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_INST_L.ContinuationTextBox = null;
            this.txt_W_INST_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_INST_L.Location = new System.Drawing.Point(473, 616);
            this.txt_W_INST_L.Name = "txt_W_INST_L";
            this.txt_W_INST_L.NumberFormat = "###,###,##0.00";
            this.txt_W_INST_L.Postfix = "";
            this.txt_W_INST_L.Prefix = "";
            this.txt_W_INST_L.ReadOnly = true;
            this.txt_W_INST_L.Size = new System.Drawing.Size(100, 20);
            this.txt_W_INST_L.TabIndex = 82;
            this.txt_W_INST_L.TextType = CrplControlLibrary.TextType.String;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(28, 299);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(108, 13);
            this.label43.TabIndex = 85;
            this.label43.Text = "Start Date Of Cal:";
            // 
            // txt_START_DATE_OF_CAL
            // 
            this.txt_START_DATE_OF_CAL.AllowSpace = true;
            this.txt_START_DATE_OF_CAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_START_DATE_OF_CAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_START_DATE_OF_CAL.ContinuationTextBox = null;
            this.txt_START_DATE_OF_CAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_START_DATE_OF_CAL.Location = new System.Drawing.Point(139, 296);
            this.txt_START_DATE_OF_CAL.Name = "txt_START_DATE_OF_CAL";
            this.txt_START_DATE_OF_CAL.NumberFormat = "###,###,##0.00";
            this.txt_START_DATE_OF_CAL.Postfix = "";
            this.txt_START_DATE_OF_CAL.Prefix = "";
            this.txt_START_DATE_OF_CAL.ReadOnly = true;
            this.txt_START_DATE_OF_CAL.Size = new System.Drawing.Size(100, 20);
            this.txt_START_DATE_OF_CAL.TabIndex = 84;
            this.txt_START_DATE_OF_CAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(32, 321);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(103, 13);
            this.label44.TabIndex = 87;
            this.label44.Text = "End Date Of Cal:";
            // 
            // txt_END_DATE_OF_CALC
            // 
            this.txt_END_DATE_OF_CALC.AllowSpace = true;
            this.txt_END_DATE_OF_CALC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_END_DATE_OF_CALC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_END_DATE_OF_CALC.ContinuationTextBox = null;
            this.txt_END_DATE_OF_CALC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_END_DATE_OF_CALC.Location = new System.Drawing.Point(139, 318);
            this.txt_END_DATE_OF_CALC.Name = "txt_END_DATE_OF_CALC";
            this.txt_END_DATE_OF_CALC.NumberFormat = "###,###,##0.00";
            this.txt_END_DATE_OF_CALC.Postfix = "";
            this.txt_END_DATE_OF_CALC.Prefix = "";
            this.txt_END_DATE_OF_CALC.ReadOnly = true;
            this.txt_END_DATE_OF_CALC.Size = new System.Drawing.Size(100, 20);
            this.txt_END_DATE_OF_CALC.TabIndex = 86;
            this.txt_END_DATE_OF_CALC.TextType = CrplControlLibrary.TextType.String;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(59, 276);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(78, 13);
            this.label45.TabIndex = 89;
            this.label45.Text = "Select Date:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(139, 273);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(395, 276);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(73, 13);
            this.label46.TabIndex = 91;
            this.label46.Text = "Investment:";
            // 
            // txtInvestment
            // 
            this.txtInvestment.AllowSpace = true;
            this.txtInvestment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvestment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInvestment.ContinuationTextBox = null;
            this.txtInvestment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvestment.Location = new System.Drawing.Point(472, 273);
            this.txtInvestment.Name = "txtInvestment";
            this.txtInvestment.NumberFormat = "###,###,##0.00";
            this.txtInvestment.Postfix = "";
            this.txtInvestment.Prefix = "";
            this.txtInvestment.ReadOnly = true;
            this.txtInvestment.Size = new System.Drawing.Size(100, 20);
            this.txtInvestment.TabIndex = 92;
            this.txtInvestment.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtInvrebate
            // 
            this.txtInvrebate.AllowSpace = true;
            this.txtInvrebate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvrebate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInvrebate.ContinuationTextBox = null;
            this.txtInvrebate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvrebate.Location = new System.Drawing.Point(472, 296);
            this.txtInvrebate.Name = "txtInvrebate";
            this.txtInvrebate.NumberFormat = "###,###,##0.00";
            this.txtInvrebate.Postfix = "";
            this.txtInvrebate.Prefix = "";
            this.txtInvrebate.ReadOnly = true;
            this.txtInvrebate.Size = new System.Drawing.Size(100, 20);
            this.txtInvrebate.TabIndex = 94;
            this.txtInvrebate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(412, 299);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(56, 13);
            this.label47.TabIndex = 93;
            this.label47.Text = "Rebate :";
            // 
            // txtDonationRebate
            // 
            this.txtDonationRebate.AllowSpace = true;
            this.txtDonationRebate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDonationRebate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonationRebate.ContinuationTextBox = null;
            this.txtDonationRebate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonationRebate.Location = new System.Drawing.Point(473, 341);
            this.txtDonationRebate.Name = "txtDonationRebate";
            this.txtDonationRebate.NumberFormat = "###,###,##0.00";
            this.txtDonationRebate.Postfix = "";
            this.txtDonationRebate.Prefix = "";
            this.txtDonationRebate.ReadOnly = true;
            this.txtDonationRebate.Size = new System.Drawing.Size(100, 20);
            this.txtDonationRebate.TabIndex = 98;
            this.txtDonationRebate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(414, 344);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(56, 13);
            this.label48.TabIndex = 97;
            this.label48.Text = "Rebate :";
            // 
            // txtDonation
            // 
            this.txtDonation.AllowSpace = true;
            this.txtDonation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDonation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonation.ContinuationTextBox = null;
            this.txtDonation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonation.Location = new System.Drawing.Point(473, 318);
            this.txtDonation.Name = "txtDonation";
            this.txtDonation.NumberFormat = "###,###,##0.00";
            this.txtDonation.Postfix = "";
            this.txtDonation.Prefix = "";
            this.txtDonation.ReadOnly = true;
            this.txtDonation.Size = new System.Drawing.Size(100, 20);
            this.txtDonation.TabIndex = 96;
            this.txtDonation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(408, 321);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(62, 13);
            this.label49.TabIndex = 95;
            this.label49.Text = "Donation:";
            // 
            // txtUtilities
            // 
            this.txtUtilities.AllowSpace = true;
            this.txtUtilities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUtilities.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUtilities.ContinuationTextBox = null;
            this.txtUtilities.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUtilities.Location = new System.Drawing.Point(473, 364);
            this.txtUtilities.Name = "txtUtilities";
            this.txtUtilities.NumberFormat = "###,###,##0.00";
            this.txtUtilities.Postfix = "";
            this.txtUtilities.Prefix = "";
            this.txtUtilities.ReadOnly = true;
            this.txtUtilities.Size = new System.Drawing.Size(100, 20);
            this.txtUtilities.TabIndex = 100;
            this.txtUtilities.TextType = CrplControlLibrary.TextType.String;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(282, 367);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(187, 13);
            this.label50.TabIndex = 99;
            this.label50.Text = "V.Tax / Utilities / Other Taxes :";
            // 
            // CHRIS_Payroll_View_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 728);
            this.Controls.Add(this.txtUtilities);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.txtDonationRebate);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.txtDonation);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.txtInvrebate);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.txtInvestment);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.txt_END_DATE_OF_CALC);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.txt_START_DATE_OF_CAL);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.txt_W_INST_L);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.TXT_SUBS_LOAN_TAX_AMT);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txt_W_ITAX);
            this.Controls.Add(this.txt_REMAINING_MONTHS);
            this.Controls.Add(this.txt_FINAL_ANNUAL_TAX);
            this.Controls.Add(this.txt_TAX_RATE_FROM_SLAB);
            this.Controls.Add(this.txt_TAX_ALREADY_PAID);
            this.Controls.Add(this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB);
            this.Controls.Add(this.txt_VAR_TAX);
            this.Controls.Add(this.txt_FIXED_TAX);
            this.Controls.Add(this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT);
            this.Controls.Add(this.txt_TOTAL_TAXABLE_INCOME);
            this.Controls.Add(this.txt_GLOBAL_W_INC_AMT);
            this.Controls.Add(this.txt_GLOBAL_W_10_AMT);
            this.Controls.Add(this.txt_HL_MARKUP);
            this.Controls.Add(this.txt_DIVIDENT_STOCK);
            this.Controls.Add(this.txt_BONUS_PAYMENT);
            this.Controls.Add(this.txt_ZAKAT_DONATION);
            this.Controls.Add(this.txt_OTHER_ALLOWNCE);
            this.Controls.Add(this.txt_FUEL_ALLONCE);
            this.Controls.Add(this.txt_OPD_ALLOWNCE);
            this.Controls.Add(this.txt_GHA_ALLOWNCE);
            this.Controls.Add(this.txt_CAR_ALLOWNCE);
            this.Controls.Add(this.txt_PF_EXEMPT);
            this.Controls.Add(this.txt_ANNUAL_PF);
            this.Controls.Add(this.txt_GROSS_SALARY);
            this.Controls.Add(this.txt_ANNUAL_SALARY);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.grpPersonnel);
            this.Controls.Add(this.groupBox5);
            this.CurrentPanelBlock = "slPanelPersonnel";
            this.Name = "CHRIS_Payroll_View_Info";
            this.Text = "CHRIS_View_Tax_Details";
            this.toolTip1.SetToolTip(this, "ANNUAL_SALARY\r\n");
            this.Controls.SetChildIndex(this.groupBox5, 0);
            this.Controls.SetChildIndex(this.grpPersonnel, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.txt_ANNUAL_SALARY, 0);
            this.Controls.SetChildIndex(this.txt_GROSS_SALARY, 0);
            this.Controls.SetChildIndex(this.txt_ANNUAL_PF, 0);
            this.Controls.SetChildIndex(this.txt_PF_EXEMPT, 0);
            this.Controls.SetChildIndex(this.txt_CAR_ALLOWNCE, 0);
            this.Controls.SetChildIndex(this.txt_GHA_ALLOWNCE, 0);
            this.Controls.SetChildIndex(this.txt_OPD_ALLOWNCE, 0);
            this.Controls.SetChildIndex(this.txt_FUEL_ALLONCE, 0);
            this.Controls.SetChildIndex(this.txt_OTHER_ALLOWNCE, 0);
            this.Controls.SetChildIndex(this.txt_ZAKAT_DONATION, 0);
            this.Controls.SetChildIndex(this.txt_BONUS_PAYMENT, 0);
            this.Controls.SetChildIndex(this.txt_DIVIDENT_STOCK, 0);
            this.Controls.SetChildIndex(this.txt_HL_MARKUP, 0);
            this.Controls.SetChildIndex(this.txt_GLOBAL_W_10_AMT, 0);
            this.Controls.SetChildIndex(this.txt_GLOBAL_W_INC_AMT, 0);
            this.Controls.SetChildIndex(this.txt_TOTAL_TAXABLE_INCOME, 0);
            this.Controls.SetChildIndex(this.txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT, 0);
            this.Controls.SetChildIndex(this.txt_FIXED_TAX, 0);
            this.Controls.SetChildIndex(this.txt_VAR_TAX, 0);
            this.Controls.SetChildIndex(this.txt_TAX_EXEMPT_LIMIT_FROM_SLAB, 0);
            this.Controls.SetChildIndex(this.txt_TAX_ALREADY_PAID, 0);
            this.Controls.SetChildIndex(this.txt_TAX_RATE_FROM_SLAB, 0);
            this.Controls.SetChildIndex(this.txt_FINAL_ANNUAL_TAX, 0);
            this.Controls.SetChildIndex(this.txt_REMAINING_MONTHS, 0);
            this.Controls.SetChildIndex(this.txt_W_ITAX, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.label18, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.label20, 0);
            this.Controls.SetChildIndex(this.label21, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            this.Controls.SetChildIndex(this.label23, 0);
            this.Controls.SetChildIndex(this.label24, 0);
            this.Controls.SetChildIndex(this.label28, 0);
            this.Controls.SetChildIndex(this.label29, 0);
            this.Controls.SetChildIndex(this.label30, 0);
            this.Controls.SetChildIndex(this.label31, 0);
            this.Controls.SetChildIndex(this.label32, 0);
            this.Controls.SetChildIndex(this.label34, 0);
            this.Controls.SetChildIndex(this.label35, 0);
            this.Controls.SetChildIndex(this.label36, 0);
            this.Controls.SetChildIndex(this.label37, 0);
            this.Controls.SetChildIndex(this.label38, 0);
            this.Controls.SetChildIndex(this.label39, 0);
            this.Controls.SetChildIndex(this.label40, 0);
            this.Controls.SetChildIndex(this.label41, 0);
            this.Controls.SetChildIndex(this.label33, 0);
            this.Controls.SetChildIndex(this.TXT_SUBS_LOAN_TAX_AMT, 0);
            this.Controls.SetChildIndex(this.label42, 0);
            this.Controls.SetChildIndex(this.txt_W_INST_L, 0);
            this.Controls.SetChildIndex(this.label26, 0);
            this.Controls.SetChildIndex(this.txt_START_DATE_OF_CAL, 0);
            this.Controls.SetChildIndex(this.label43, 0);
            this.Controls.SetChildIndex(this.txt_END_DATE_OF_CALC, 0);
            this.Controls.SetChildIndex(this.label44, 0);
            this.Controls.SetChildIndex(this.label45, 0);
            this.Controls.SetChildIndex(this.dateTimePicker1, 0);
            this.Controls.SetChildIndex(this.label46, 0);
            this.Controls.SetChildIndex(this.txtInvestment, 0);
            this.Controls.SetChildIndex(this.label47, 0);
            this.Controls.SetChildIndex(this.txtInvrebate, 0);
            this.Controls.SetChildIndex(this.label49, 0);
            this.Controls.SetChildIndex(this.txtDonation, 0);
            this.Controls.SetChildIndex(this.label48, 0);
            this.Controls.SetChildIndex(this.txtDonationRebate, 0);
            this.Controls.SetChildIndex(this.label50, 0);
            this.Controls.SetChildIndex(this.txtUtilities, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grpPersonnel.ResumeLayout(false);
            this.slPanelPersonnel.ResumeLayout(false);
            this.slPanelPersonnel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox grpPersonnel;
        public COMMON.SLCONTROLS.SLPanelSimple slPanelPersonnel;
        private CrplControlLibrary.LookupButton lookup_PR_CATEGORY;
        private CrplControlLibrary.LookupButton lookup_Pr_P_No;
        private CrplControlLibrary.SLDatePicker slDatePicker_PR_CONFIRM_ON;
        private CrplControlLibrary.SLTextBox txt_PR_ANNUAL_PACK;
        private CrplControlLibrary.SLTextBox txt_PR_CONF_FLAG;
        private CrplControlLibrary.SLTextBox txt_PR_MONTH_AWARD;
        private CrplControlLibrary.SLTextBox txt_PR_FIRST_NAME;
        private CrplControlLibrary.SLTextBox txt_PR_NEW_BRANCH;
        private CrplControlLibrary.SLTextBox txt_PR_LEVEL;
        private CrplControlLibrary.SLTextBox txt_PR_DESIG;
        private CrplControlLibrary.SLTextBox txt_PR_CATEGORY;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker slDatePicker_PR_JOINING_DATE;
        private CrplControlLibrary.SLTextBox txt_PR_P_NO;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.CrplTextBox txt_ANNUAL_SALARY;
        private CrplControlLibrary.CrplTextBox txt_GROSS_SALARY;
        private CrplControlLibrary.CrplTextBox txt_ANNUAL_PF;
        private CrplControlLibrary.CrplTextBox txt_PF_EXEMPT;
        private CrplControlLibrary.CrplTextBox txt_CAR_ALLOWNCE;
        private CrplControlLibrary.CrplTextBox txt_GHA_ALLOWNCE;
        private CrplControlLibrary.CrplTextBox txt_GLOBAL_W_INC_AMT;
        private CrplControlLibrary.CrplTextBox txt_GLOBAL_W_10_AMT;
        private CrplControlLibrary.CrplTextBox txt_HL_MARKUP;
        private CrplControlLibrary.CrplTextBox txt_DIVIDENT_STOCK;
        private CrplControlLibrary.CrplTextBox txt_BONUS_PAYMENT;
        private CrplControlLibrary.CrplTextBox txt_ZAKAT_DONATION;
        private CrplControlLibrary.CrplTextBox txt_OTHER_ALLOWNCE;
        private CrplControlLibrary.CrplTextBox txt_FUEL_ALLONCE;
        private CrplControlLibrary.CrplTextBox txt_OPD_ALLOWNCE;
        private CrplControlLibrary.CrplTextBox txt_TOTAL_TAXABLE_INCOME;
        private CrplControlLibrary.CrplTextBox txt_AMOUNT_EXCEEDING_EXCEMPTION_LIMIT;
        private CrplControlLibrary.CrplTextBox txt_FIXED_TAX;
        private CrplControlLibrary.CrplTextBox txt_VAR_TAX;
        private CrplControlLibrary.CrplTextBox txt_TAX_EXEMPT_LIMIT_FROM_SLAB;
        private CrplControlLibrary.CrplTextBox txt_TAX_ALREADY_PAID;
        private CrplControlLibrary.CrplTextBox txt_TAX_RATE_FROM_SLAB;
        private CrplControlLibrary.CrplTextBox txt_FINAL_ANNUAL_TAX;
        private CrplControlLibrary.CrplTextBox txt_REMAINING_MONTHS;
        private CrplControlLibrary.CrplTextBox txt_W_ITAX;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private CrplControlLibrary.CrplTextBox TXT_SUBS_LOAN_TAX_AMT;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.CrplTextBox txt_W_INST_L;
        private System.Windows.Forms.Label label43;
        private CrplControlLibrary.CrplTextBox txt_START_DATE_OF_CAL;
        private System.Windows.Forms.Label label44;
        private CrplControlLibrary.CrplTextBox txt_END_DATE_OF_CALC;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label46;
        private CrplControlLibrary.CrplTextBox txtInvestment;
        private CrplControlLibrary.CrplTextBox txtInvrebate;
        private System.Windows.Forms.Label label47;
        private CrplControlLibrary.CrplTextBox txtDonationRebate;
        private System.Windows.Forms.Label label48;
        private CrplControlLibrary.CrplTextBox txtDonation;
        private System.Windows.Forms.Label label49;
        private CrplControlLibrary.CrplTextBox txtUtilities;
        private System.Windows.Forms.Label label50;
    }
}