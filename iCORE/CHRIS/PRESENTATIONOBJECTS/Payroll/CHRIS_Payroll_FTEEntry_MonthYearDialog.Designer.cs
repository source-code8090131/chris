namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_FTEEntry_MonthYearDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtMonthYear = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtMonthYear
            // 
            this.txtMonthYear.AllowSpace = true;
            this.txtMonthYear.AssociatedLookUpName = "";
            this.txtMonthYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMonthYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMonthYear.ContinuationTextBox = null;
            this.txtMonthYear.CustomEnabled = true;
            this.txtMonthYear.DataFieldMapping = "";
            this.txtMonthYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonthYear.GetRecordsOnUpDownKeys = false;
            this.txtMonthYear.IsDate = false;
            this.txtMonthYear.Location = new System.Drawing.Point(168, 24);
            this.txtMonthYear.MaxLength = 6;
            this.txtMonthYear.Name = "txtMonthYear";
            this.txtMonthYear.NumberFormat = "###,###,##0.00";
            this.txtMonthYear.Postfix = "";
            this.txtMonthYear.Prefix = "";
            this.txtMonthYear.Size = new System.Drawing.Size(52, 20);
            this.txtMonthYear.SkipValidation = false;
            this.txtMonthYear.TabIndex = 9;
            this.txtMonthYear.TextType = CrplControlLibrary.TextType.Integer;
            this.txtMonthYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMonthYear_KeyDown);
            this.txtMonthYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonthYear_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Enter Month for FTE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(226, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "(YYYYMM)";
            // 
            // CHRIS_Payroll_FTEEntry_MonthYearDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 66);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMonthYear);
            this.MaximumSize = new System.Drawing.Size(350, 100);
            this.Name = "CHRIS_Payroll_FTEEntry_MonthYearDialog";
            this.Text = "CHRIS_Payroll_FTEEntry_MonthYearDialog";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CHRIS_Payroll_FTEEntry_MonthYearDialog_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtMonthYear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}