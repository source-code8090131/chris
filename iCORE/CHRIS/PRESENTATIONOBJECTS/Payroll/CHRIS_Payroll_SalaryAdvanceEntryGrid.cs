using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_SalaryAdvanceEntryGrid : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();

        public CHRIS_Payroll_SalaryAdvanceEntryGrid()
        {
            InitializeComponent();

        }
        public CHRIS_Payroll_SalaryAdvanceEntryGrid(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //if (FunctionConfig.CurrentOption == Function.Modify)
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            tlbMain.Visible = false;
            this.txtOption.Visible = false;
            tbtAdd.Visible = false;
            tbtCancel.Visible = false;
            tbtClose.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
            tlbMain.Visible = false;

            this.ShowStatusBar = false;
            this.ShowOptionKeys = false;

            //this.ShowF6Option = true;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;

            this.colAmnt.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.colM1.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.colM2.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.colName.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.colPNo.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);

            GetAdvance();

            this.slPanelTabular1.BringToFront();
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.F8)
                base.OnKeyDown(e);
            if (e.KeyValue == 13 || e.KeyValue == 9)
            {
                this.Cancel();
                this.slDataGridView1.GridSource.Rows.Clear();//.CurrentCell = this.slDataGridView1.Rows[0].Cells[0]; ;
                this.slDataGridView1.FirstDisplayedCell = this.slDataGridView1.Rows[0].Cells[0];

                this.Dispose();
                this.Close();

            }
        }

        protected void GetAdvance()
        {
            Result rslt;

            Dictionary<string, object> param = new Dictionary<string, object>();

            //param.Add("", );
            rslt = cmnDM.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "advance", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    slDataGridView1.AutoGenerateColumns = false;
                    slDataGridView1.DataSource = rslt.dstResult.Tables[0];



                }




            }

        }
    }
}