using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    public partial class CHRIS_Payroll_FTEEntry_ProcessingDialog : Form
    {

        #region Variables
        Double GLOBAL_PNO;      
        string BLK_HEAD_W_DESIG;
        DateTime BLK_HEAD_W_TRANSFER;
        string GLOBAL_BRN;
        string GLOBAL_CAT;
        DateTime PR_PROMOTION_DATE;
        #endregion

        #region Constructor
        public CHRIS_Payroll_FTEEntry_ProcessingDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties

        bool isCompleted = false;
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        string m_W_MY;
        public string W_MY
        {
            get { return m_W_MY; }
            set { m_W_MY = value; }
        }

        int m_W_OPT;
        public int W_OPT
        {
            get { return m_W_OPT; }
            set { m_W_OPT = value; }
        }

        #endregion

        #region Methods

        void StartProcessing()
        {
            //To store input parameters of StoredProcedure
            Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
            dicInputParameters.Add("W_MY1", W_MY);
            dicInputParameters.Add("W_OPT",W_OPT);

            try
            {
                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_MONTH_SUMMARY_UPDATION", "UpdateMonthSummaryUpdation", dicInputParameters);

                if (W_OPT == 1)
                {
                    Update_AllEmployees();
                    DeleteTempTab();
                }
                else if (W_OPT == 2)
                {
                    Update_RegularStaffOnly();
                    DeleteTempTab();
                } 
                else if (W_OPT == 3)
                {
                    Update_InternsOnly();
                    DeleteTempTab();
                }
                else if (W_OPT == 4)
                {
                    Update_ContractualStaffOnly();
                    DeleteTempTab();
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void DeleteTempTab()
        {
            //To store input parameters of StoredProcedure
            Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
            dicInputParameters.Add("W_MY1", W_MY);
            dicInputParameters.Add("W_OPT", W_OPT);

            try
            {
                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.Execute("CHRIS_SP_FTE_MONTH_SUMMARY_UPDATION", "DeleteTempTab", dicInputParameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Update_AllEmployees()
        {
            try
            {
                PROC_3();
                PROC_8();
                PROC_9();
                PROC_7();

                isCompleted = true;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void Update_RegularStaffOnly()
        {
            try
            {
                PROC_3();
                PROC_7();

                isCompleted = true;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void Update_InternsOnly()
        {
            try
            {
                PROC_8();
                PROC_7();

                isCompleted = true;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void Update_ContractualStaffOnly()
        {
            try
            {
                PROC_9();
                PROC_7();

                isCompleted = true;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void PROC_3()
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", (object)W_MY);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_3", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < rslt.dstResult.Tables[0].Rows.Count; i++)
                            {
                                #region Get data
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_P_NO"] != null)
                                {
                                    GLOBAL_PNO = Double.Parse(rslt.dstResult.Tables[0].Rows[i]["PR_P_NO"].ToString());
                                    txtEmployeeNo.Text = rslt.dstResult.Tables[0].Rows[i]["PR_P_NO"].ToString();
                                    Application.DoEvents();
                                }
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_DESIG"] != null)
                                {
                                    BLK_HEAD_W_DESIG = rslt.dstResult.Tables[0].Rows[i]["PR_DESIG"].ToString();
                                }
                                if ((rslt.dstResult.Tables[0].Rows[i]["PR_TRANSFER_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[i]["PR_TRANSFER_DATE"].ToString())))
                                {
                                    BLK_HEAD_W_TRANSFER = DateTime.Parse(rslt.dstResult.Tables[0].Rows[i]["PR_TRANSFER_DATE"].ToString());
                                }
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_NEW_BRANCH"] != null)
                                {
                                    GLOBAL_BRN = rslt.dstResult.Tables[0].Rows[i]["PR_NEW_BRANCH"].ToString();
                                }
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_CATEGORY"] != null)
                                {
                                    GLOBAL_CAT = rslt.dstResult.Tables[0].Rows[i]["PR_CATEGORY"].ToString();
                                }
                                if ((rslt.dstResult.Tables[0].Rows[i]["PR_PROMOTION_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[i]["PR_PROMOTION_DATE"].ToString())))
                                {
                                    PR_PROMOTION_DATE = DateTime.Parse(rslt.dstResult.Tables[0].Rows[i]["PR_PROMOTION_DATE"].ToString());
                                    PROC_4();
                                }
                                PROC_5();

                                #endregion
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PROC_4()
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", (object)W_MY);
                dicInputParameters.Add("GLOBAL_PNO", (object)GLOBAL_PNO);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_4", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows[0]["BLK_HEAD_W_DESIG"] != null)
                            {
                                BLK_HEAD_W_DESIG = rslt.dstResult.Tables[0].Rows[0]["BLK_HEAD_W_DESIG"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PROC_5()
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", (object)W_MY);
                dicInputParameters.Add("GLOBAL_PNO", (object)GLOBAL_PNO);
                dicInputParameters.Add("GLOBAL_BRN", (object)GLOBAL_BRN);
                dicInputParameters.Add("W_DESIG", (object)BLK_HEAD_W_DESIG);
                dicInputParameters.Add("GLOBAL_CAT", (object)GLOBAL_CAT);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_5", " ", dicInputParameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
       
        }
        void PROC_8()
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", (object)W_MY);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_8", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < rslt.dstResult.Tables[0].Rows.Count; i++)
                            {
                                if (rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"] != null)
                                {
                                    this.txtEmployeeNo.Text = rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"].ToString();
                                    Application.DoEvents();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PROC_9()
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", (object)W_MY);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_9", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < rslt.dstResult.Tables[0].Rows.Count; i++)
                            {
                                if (rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"] != null)
                                {
                                    this.txtEmployeeNo.Text = rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"].ToString();
                                    Application.DoEvents();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void PROC_7()
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", (object)W_MY);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_7", " ", dicInputParameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        #endregion

        #region Events
        private void CHRIS_Payroll_FTEEntry_ProcessingDialog_Load(object sender, EventArgs e)
        {
            //StartProcessing();
        }
        #endregion

        private void CHRIS_Payroll_FTEEntry_ProcessingDialog_Shown(object sender, EventArgs e)
        {
            StartProcessing();
        }

    }
}