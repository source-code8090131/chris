namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Payroll
{
    partial class CHRIS_Payroll_ArearUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Payroll_ArearUpdate));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slPanelTabular_SalaryArear = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvSalArear = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.col_SA_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SA_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SA_SAL_AREARS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SA_PF_AREARS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SA_OT_AREARS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SA_AREARS_APPROV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.slPanelTabular_SalaryArear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalArear)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Location = new System.Drawing.Point(12, 73);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(585, 36);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(175, 11);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 15);
            this.label33.TabIndex = 22;
            this.label33.Text = "Salary Arear Updation";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slPanelTabular_SalaryArear);
            this.groupBox1.Location = new System.Drawing.Point(12, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(585, 341);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // slPanelTabular_SalaryArear
            // 
            this.slPanelTabular_SalaryArear.ConcurrentPanels = null;
            this.slPanelTabular_SalaryArear.Controls.Add(this.dgvSalArear);
            this.slPanelTabular_SalaryArear.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelTabular_SalaryArear.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelTabular_SalaryArear.DependentPanels = null;
            this.slPanelTabular_SalaryArear.DisableDependentLoad = false;
            this.slPanelTabular_SalaryArear.EnableDelete = true;
            this.slPanelTabular_SalaryArear.EnableInsert = true;
            this.slPanelTabular_SalaryArear.EnableQuery = false;
            this.slPanelTabular_SalaryArear.EnableUpdate = true;
            this.slPanelTabular_SalaryArear.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SalArearCommand";
            this.slPanelTabular_SalaryArear.Location = new System.Drawing.Point(11, 16);
            this.slPanelTabular_SalaryArear.MasterPanel = null;
            this.slPanelTabular_SalaryArear.Name = "slPanelTabular_SalaryArear";
            this.slPanelTabular_SalaryArear.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelTabular_SalaryArear.Size = new System.Drawing.Size(563, 322);
            this.slPanelTabular_SalaryArear.SPName = "CHRIS_SP_SAL_AREAR_MANAGER";
            this.slPanelTabular_SalaryArear.TabIndex = 0;
            // 
            // dgvSalArear
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalArear.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSalArear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSalArear.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_SA_P_NO,
            this.col_SA_DATE,
            this.col_SA_SAL_AREARS,
            this.col_SA_PF_AREARS,
            this.col_SA_OT_AREARS,
            this.col_SA_AREARS_APPROV});
            this.dgvSalArear.ColumnToHide = null;
            this.dgvSalArear.ColumnWidth = null;
            this.dgvSalArear.CustomEnabled = true;
            this.dgvSalArear.DisplayColumnWrapper = null;
            this.dgvSalArear.GridDefaultRow = 0;
            this.dgvSalArear.Location = new System.Drawing.Point(0, 0);
            this.dgvSalArear.Name = "dgvSalArear";
            this.dgvSalArear.ReadOnlyColumns = null;
            this.dgvSalArear.RequiredColumns = "COL_SA_P_NO";
            this.dgvSalArear.Size = new System.Drawing.Size(560, 322);
            this.dgvSalArear.SkippingColumns = null;
            this.dgvSalArear.TabIndex = 0;
            // 
            // col_SA_P_NO
            // 
            this.col_SA_P_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_SA_P_NO.DataPropertyName = "SA_P_NO";
            this.col_SA_P_NO.HeaderText = "P. No.";
            this.col_SA_P_NO.MaxInputLength = 6;
            this.col_SA_P_NO.Name = "col_SA_P_NO";
            // 
            // col_SA_DATE
            // 
            this.col_SA_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_SA_DATE.DataPropertyName = "SA_DATE";
            this.col_SA_DATE.HeaderText = "Entry Date";
            this.col_SA_DATE.Name = "col_SA_DATE";
            // 
            // col_SA_SAL_AREARS
            // 
            this.col_SA_SAL_AREARS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_SA_SAL_AREARS.DataPropertyName = "SA_SAL_AREARS";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.col_SA_SAL_AREARS.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_SA_SAL_AREARS.HeaderText = "Salary Arear";
            this.col_SA_SAL_AREARS.MaxInputLength = 10;
            this.col_SA_SAL_AREARS.Name = "col_SA_SAL_AREARS";
            // 
            // col_SA_PF_AREARS
            // 
            this.col_SA_PF_AREARS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_SA_PF_AREARS.DataPropertyName = "SA_PF_AREARS";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.col_SA_PF_AREARS.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_SA_PF_AREARS.HeaderText = "PF Arear";
            this.col_SA_PF_AREARS.MaxInputLength = 9;
            this.col_SA_PF_AREARS.Name = "col_SA_PF_AREARS";
            // 
            // col_SA_OT_AREARS
            // 
            this.col_SA_OT_AREARS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_SA_OT_AREARS.DataPropertyName = "SA_OT_AREARS";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.col_SA_OT_AREARS.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_SA_OT_AREARS.HeaderText = "OT Arear";
            this.col_SA_OT_AREARS.MaxInputLength = 9;
            this.col_SA_OT_AREARS.Name = "col_SA_OT_AREARS";
            // 
            // col_SA_AREARS_APPROV
            // 
            this.col_SA_AREARS_APPROV.DataPropertyName = "SA_AREARS_APPROV";
            this.col_SA_AREARS_APPROV.HeaderText = "Approve [Y/N]";
            this.col_SA_AREARS_APPROV.MaxInputLength = 1;
            this.col_SA_AREARS_APPROV.Name = "col_SA_AREARS_APPROV";
            this.col_SA_AREARS_APPROV.Width = 60;
            // 
            // CHRIS_Payroll_ArearUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 491);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.CurrentPanelBlock = "slPanelTabular_SalaryArear";
            this.Name = "CHRIS_Payroll_ArearUpdate";
            this.SetFormTitle = "";
            this.Text = "CHRIS_Payroll_ArearUpdate";
            this.Controls.SetChildIndex(this.groupBox5, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.slPanelTabular_SalaryArear.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalArear)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelTabular_SalaryArear;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvSalArear;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SA_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SA_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SA_SAL_AREARS;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SA_PF_AREARS;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SA_OT_AREARS;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SA_AREARS_APPROV;
    }
}