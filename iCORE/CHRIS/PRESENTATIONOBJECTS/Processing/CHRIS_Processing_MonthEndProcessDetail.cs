using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_MonthEndProcessDetail : ChrisSimpleForm
    {

        bool isCompleted = false;
        public CHRIS_Processing_MonthEndProcessDetail()
        {
            InitializeComponent();
            this.ShowStatusBar = false;
            txtOption.Visible = false;
            txtAns.Focus();
            lblUserName.Text += "   " + base.userID;



            //Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            //DataTable dtMEP = new DataTable();
            //Result rsltMain,rsltMonth,rsltMnthSumm;
            //CmnDataManager cmnDM = new CmnDataManager();

            ////CALLING THE MONTHEND INITIAL//
            //rsltMain = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_MAIN", "Initial", colsNVals);
            //if (rsltMain.isSuccessful)
            //{
            //    if (rsltMain.dstResult.ExtendedProperties.Count > 0)
            //    {
            //        MessageBox.Show("Processing Is In Progress On Other Terminal .......!");
            //    }
            //    else
            //    {
            //        rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlag", colsNVals);
            //    }
            //}
            ////CALLING THE P_MONTH PROCEDURES [CONTAIN 5 SPs]//
            //rsltMonth = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_P_MONTH", "", colsNVals);

            //if (rsltMonth.isSuccessful)
            //{
            //    //MessageBox.Show("count of Month =   " + rsltMonth.dstResult.Tables.Count); 
            //    rsltMnthSumm = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_P_MONTH_SUMMARY", "", colsNVals);
            //    if (rsltMnthSumm.isSuccessful)
            //    {
            //        if (rsltMnthSumm.dstResult.Tables.Count > 0)
            //        {
            //                dtMEP = rsltMnthSumm.dstResult.Tables[0];
            //            if (dtMEP.Rows.Count > 0)
            //            {

            //                //for (int j = 0; j < dtMEP.Rows.Count; j++)
            //                foreach (DataRow drw in dtMEP.Rows)
            //                {

            //                    TE_YEAR.Text = drw.ItemArray[0].ToString();
            //                    //TE_YEAR.Text = Convert.ToString(dtMEP.Rows[j]["TE_YEAR"]);
            //                    //TE_P_NO.Text = Convert.ToString(dtMEP.Rows[j]["TE_P_NO"]);
            //                    //TE_MONTH.Text = Convert.ToString(dtMEP.Rows[j]["TE_MONTH"]);
            //                    //TE_BRANCH.Text = Convert.ToString(dtMEP.Rows[j]["TE_BRANCH"]);
            //                    //TE_DESIG.Text = Convert.ToString(dtMEP.Rows[j]["TE_DESIG"]);
            //                    //TE_CATEGORY.Text = Convert.ToString(dtMEP.Rows[j]["TE_CATEGORY"]);
            //                    //TE_EMPLOYEE_TYPE.Text = Convert.ToString(dtMEP.Rows[j]["TE_EMPLOYEE_TYPE"]);
            //                    //TE_BASIC.Text = Convert.ToString(dtMEP.Rows[j]["TE_BASIC"]);
            //                    //TE_HOUSE.Text = Convert.ToString(dtMEP.Rows[j]["TE_HOUSE"]);
            //                    //TE_CONV.Text = Convert.ToString(dtMEP.Rows[j]["TE_CONV"]);
            //                    //TE_PFUND.Text = Convert.ToString(dtMEP.Rows[j]["TE_PFUND"]);
            //                    //TE_OT_HRS.Text = Convert.ToString(dtMEP.Rows[j]["TE_OT_HRS"]);
            //                    //TE_SEGMENT.Text = Convert.ToString(dtMEP.Rows[j]["TE_SEGMENT"]);
            //                    //TE_GROUP.Text = Convert.ToString(dtMEP.Rows[j]["TE_GROUP"]);
            //                    //TE_DEPT.Text = Convert.ToString(dtMEP.Rows[j]["TE_DEPT"]);
            //                    //TE_CONTRIB_TOTAL.Text = Convert.ToString(dtMEP.Rows[j]["TE_CONTRIB_TOTAL"]);
            //                    //TE_OT_COST.Text = Convert.ToString(dtMEP.Rows[j]["TE_OT_COST"]);
            //                    //TE_ATT_AWD.Text = Convert.ToString(dtMEP.Rows[j]["TE_ATT_AWD"]);
            //                    //TE_MEAL.Text = Convert.ToString(dtMEP.Rows[j]["TE_MEAL"]);
            //                    //TE_OTHERS.Text = Convert.ToString(dtMEP.Rows[j]["TE_OTHERS"]);
            //                    Application.DoEvents();
            //                    System.Threading.Thread.Sleep(1000);


            //                }

            //            }

            //        }
            //    }
            //    else
            //    {
            //        throw new Exception(rsltMnthSumm.message);

            //    }
            //}
           

        }

        

      



        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtOption.Visible = false;
            tbtSave.Visible = false;
            tbtAdd.Visible = false;
            tbtCancel.Visible = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;
            txtOption.Visible = false;
            lblUserName.Text = lblUserName.Text + " " + base.userID;
        }



        






        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }



        public CHRIS_Processing_MonthEndProcessDetail(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            
        }

        private void txtAns_TextChanged(object sender, EventArgs e)
        {
            if (txtAns.Text != "")
            {
                CHRIS_Processing_MonthEndProcess MonthEndProcessDialog = new CHRIS_Processing_MonthEndProcess();
                MonthEndProcessDialog.Close();
                this.Close();

            }

        }
              

      
        private void CHRIS_Processing_MonthEndProcessDetail_Shown(object sender, EventArgs e)
        {
            Result rsltMain, rsltMonth, rsltMnthSumm;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            try
            {
                
                DataTable dtMEP = new DataTable();
                

                //CALLING THE MONTHEND INITIAL//
                rsltMain = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_MAIN", "Initial", colsNVals);
                if (rsltMain.isSuccessful)
                {
                    if (rsltMain.dstResult.Tables.Count > 0)
                    {
                        if (rsltMain.dstResult.Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show("Processing Is In Progress On Other Terminal .......!");
                            return;
                        }
                    }

                    rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlag", colsNVals);
                }
                //CALLING THE P_MONTH PROCEDURES [CONTAIN 5 SPs]//
                rsltMonth = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_P_MONTH", "", colsNVals);

                if (rsltMonth.isSuccessful)
                {
                    //MessageBox.Show("count of Month =   " + rsltMonth.dstResult.Tables.Count); 
                    rsltMnthSumm = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_P_MONTH_SUMMARY", "", colsNVals);
                    if (rsltMnthSumm.isSuccessful)
                    {
                        if (rsltMnthSumm.dstResult.Tables.Count > 0)
                        {
                            dtMEP = rsltMnthSumm.dstResult.Tables[0];
                            if (dtMEP.Rows.Count > 0)
                            {

                                //for (int j = 0; j < dtMEP.Rows.Count; j++)
                                foreach (DataRow drw in dtMEP.Rows)
                                {

                                    TE_YEAR.Text = (drw.ItemArray[22].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[22].ToString();
                                    TE_P_NO.Text = (drw.ItemArray[0].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[0].ToString();
                                    TE_MONTH.Text = (drw.ItemArray[1].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[1].ToString();
                                    TE_BRANCH.Text = (drw.ItemArray[2].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[2].ToString();
                                    TE_DESIG.Text = (drw.ItemArray[6].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[6].ToString();
                                    TE_CATEGORY.Text = (drw.ItemArray[7].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[7].ToString();
                                    TE_EMPLOYEE_TYPE.Text = (drw.ItemArray[8].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[8].ToString();
                                    TE_BASIC.Text = (drw.ItemArray[11].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[11].ToString();
                                    TE_HOUSE.Text = (drw.ItemArray[13].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[13].ToString();
                                    TE_CONV.Text = (drw.ItemArray[14].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[14].ToString();
                                    TE_PFUND.Text = (drw.ItemArray[12].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[12].ToString();
                                    TE_OT_HRS.Text = (drw.ItemArray[15].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[15].ToString();
                                    TE_SEGMENT.Text = (drw.ItemArray[3].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[3].ToString();
                                    TE_GROUP.Text = (drw.ItemArray[4].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[4].ToString();
                                    TE_DEPT.Text = (drw.ItemArray[5].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[5].ToString();
                                    TE_CONTRIB_TOTAL.Text = (drw.ItemArray[10].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[10].ToString();
                                    TE_OT_COST.Text = (drw.ItemArray[16].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[16].ToString();
                                    TE_ATT_AWD.Text = (drw.ItemArray[9].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[9].ToString();
                                    TE_MEAL.Text = (drw.ItemArray[17].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[17].ToString();
                                    TE_OTHERS.Text = (drw.ItemArray[18].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[18].ToString();
                                    Application.DoEvents();

                                }

                            }
                            else
                            {
                                TE_YEAR.Text = "0";
                                TE_P_NO.Text = "0";
                                TE_MONTH.Text = "0";
                                TE_BRANCH.Text = "0";
                                TE_DESIG.Text = "0";
                                TE_CATEGORY.Text = "0";
                                TE_EMPLOYEE_TYPE.Text = "0";
                                TE_BASIC.Text = "0";
                                TE_HOUSE.Text = "0";
                                TE_CONV.Text = "0";
                                TE_PFUND.Text = "0";
                                TE_OT_HRS.Text = "0";
                                TE_SEGMENT.Text = "0";
                                TE_GROUP.Text = "0";
                                TE_DEPT.Text = "0";
                                TE_CONTRIB_TOTAL.Text = "0";
                                TE_OT_COST.Text = "0";
                                TE_ATT_AWD.Text = "0";
                                TE_MEAL.Text = "0";
                                TE_OTHERS.Text = "0";
                                
                                Application.DoEvents();
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(rsltMnthSumm.message);
                    }
                }

                rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlagToNull", colsNVals);
            }
            catch (Exception exp)
            {
                LogError(this.Name, "CHRIS_Processing_MonthEndProcessDetail_Shown", exp);
                rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlagToNull", colsNVals);
            }
        }
    }
}