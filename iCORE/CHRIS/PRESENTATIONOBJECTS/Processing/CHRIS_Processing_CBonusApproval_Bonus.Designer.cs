namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_CBonusApproval_Bonus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Processing_CBonusApproval_Bonus));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.slPnlTabularBonusCalculation = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.sldgvBonus = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.BO_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BO_10C_BONUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W_BASIC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BO_10C_APP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W_GOVT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.W_AREAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckRecord = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlTabularBonusCalculation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldgvBonus)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(739, 0);
            this.txtOption.Size = new System.Drawing.Size(41, 20);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(780, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(780, 60);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(308, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "___________________________________________";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(293, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "10-C BONUS FOR CLERICAL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(323, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "PAYROLL SYSTEM";
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "W_USER";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(108, 97);
            this.slTextBox1.MaxLength = 10;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(116, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 100;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "W_LOC";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(108, 120);
            this.slTextBox2.MaxLength = 10;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.ReadOnly = true;
            this.slTextBox2.Size = new System.Drawing.Size(116, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 7;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "User :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Location :";
            // 
            // slPnlTabularBonusCalculation
            // 
            this.slPnlTabularBonusCalculation.ConcurrentPanels = null;
            this.slPnlTabularBonusCalculation.Controls.Add(this.sldgvBonus);
            this.slPnlTabularBonusCalculation.DataManager = null;
            this.slPnlTabularBonusCalculation.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlTabularBonusCalculation.DependentPanels = null;
            this.slPnlTabularBonusCalculation.DisableDependentLoad = false;
            this.slPnlTabularBonusCalculation.EnableDelete = true;
            this.slPnlTabularBonusCalculation.EnableInsert = true;
            this.slPnlTabularBonusCalculation.EnableQuery = false;
            this.slPnlTabularBonusCalculation.EnableUpdate = true;
            this.slPnlTabularBonusCalculation.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabCommand";
            this.slPnlTabularBonusCalculation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPnlTabularBonusCalculation.Location = new System.Drawing.Point(14, 181);
            this.slPnlTabularBonusCalculation.MasterPanel = null;
            this.slPnlTabularBonusCalculation.Name = "slPnlTabularBonusCalculation";
            this.slPnlTabularBonusCalculation.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlTabularBonusCalculation.Size = new System.Drawing.Size(752, 409);
            this.slPnlTabularBonusCalculation.SPName = "CHRIS_SP_BonusApproval_Manager";
            this.slPnlTabularBonusCalculation.TabIndex = 48;
            // 
            // sldgvBonus
            // 
            this.sldgvBonus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sldgvBonus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BO_P_NO,
            this.W_NAME,
            this.BO_10C_BONUS,
            this.W_BASIC,
            this.BO_10C_APP,
            this.W_GOVT,
            this.W_AREAR,
            this.CheckRecord});
            this.sldgvBonus.ColumnToHide = null;
            this.sldgvBonus.ColumnWidth = null;
            this.sldgvBonus.CustomEnabled = true;
            this.sldgvBonus.DisplayColumnWrapper = null;
            this.sldgvBonus.GridDefaultRow = 0;
            this.sldgvBonus.Location = new System.Drawing.Point(18, 20);
            this.sldgvBonus.Name = "sldgvBonus";
            this.sldgvBonus.ReadOnlyColumns = null;
            this.sldgvBonus.RequiredColumns = null;
            this.sldgvBonus.Size = new System.Drawing.Size(707, 373);
            this.sldgvBonus.SkippingColumns = null;
            this.sldgvBonus.TabIndex = 0;
            this.sldgvBonus.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.sldgvBonus_CellValidating);
            this.sldgvBonus.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.sldgvBonus_EditingControlShowing);
            // 
            // BO_P_NO
            // 
            this.BO_P_NO.DataPropertyName = "COL_PR_P_NO";
            this.BO_P_NO.HeaderText = "P.NO";
            this.BO_P_NO.MaxInputLength = 6;
            this.BO_P_NO.Name = "BO_P_NO";
            this.BO_P_NO.ReadOnly = true;
            this.BO_P_NO.Width = 40;
            // 
            // W_NAME
            // 
            this.W_NAME.DataPropertyName = "COL_FULLNAME";
            this.W_NAME.HeaderText = "Name";
            this.W_NAME.MaxInputLength = 35;
            this.W_NAME.Name = "W_NAME";
            this.W_NAME.ReadOnly = true;
            this.W_NAME.Width = 190;
            // 
            // BO_10C_BONUS
            // 
            this.BO_10C_BONUS.DataPropertyName = "COL_10C_BONUS";
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.BO_10C_BONUS.DefaultCellStyle = dataGridViewCellStyle1;
            this.BO_10C_BONUS.HeaderText = "10C Bonus";
            this.BO_10C_BONUS.MaxInputLength = 11;
            this.BO_10C_BONUS.Name = "BO_10C_BONUS";
            this.BO_10C_BONUS.Width = 110;
            // 
            // W_BASIC
            // 
            this.W_BASIC.DataPropertyName = "COL_BASIC";
            this.W_BASIC.HeaderText = "Basic";
            this.W_BASIC.MaxInputLength = 11;
            this.W_BASIC.Name = "W_BASIC";
            this.W_BASIC.ReadOnly = true;
            this.W_BASIC.Width = 90;
            // 
            // BO_10C_APP
            // 
            this.BO_10C_APP.DataPropertyName = "COL_10C_APP";
            this.BO_10C_APP.HeaderText = "Approved";
            this.BO_10C_APP.MaxInputLength = 1;
            this.BO_10C_APP.Name = "BO_10C_APP";
            this.BO_10C_APP.Width = 80;
            // 
            // W_GOVT
            // 
            this.W_GOVT.DataPropertyName = "COL_GOVT";
            this.W_GOVT.HeaderText = "Govt";
            this.W_GOVT.MaxInputLength = 11;
            this.W_GOVT.Name = "W_GOVT";
            this.W_GOVT.ReadOnly = true;
            this.W_GOVT.Width = 50;
            // 
            // W_AREAR
            // 
            this.W_AREAR.DataPropertyName = "COL_AREAR";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.W_AREAR.DefaultCellStyle = dataGridViewCellStyle2;
            this.W_AREAR.HeaderText = "Arears";
            this.W_AREAR.MaxInputLength = 11;
            this.W_AREAR.Name = "W_AREAR";
            // 
            // CheckRecord
            // 
            this.CheckRecord.FalseValue = "N";
            this.CheckRecord.HeaderText = "CheckRecord";
            this.CheckRecord.Name = "CheckRecord";
            this.CheckRecord.TrueValue = "Y";
            this.CheckRecord.Visible = false;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(539, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(81, 13);
            this.lblUserName.TabIndex = 101;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Processing_CBonusApproval_Bonus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 668);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.slPnlTabularBonusCalculation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.slTextBox2);
            this.Controls.Add(this.slTextBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CHRIS_Processing_CBonusApproval_Bonus";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Processing_CBonusApproval_Bonus";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slTextBox1, 0);
            this.Controls.SetChildIndex(this.slTextBox2, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.slPnlTabularBonusCalculation, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlTabularBonusCalculation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sldgvBonus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPnlTabularBonusCalculation;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView sldgvBonus;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_10C_BONUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_BASIC;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_10C_APP;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_GOVT;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_AREAR;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckRecord;
    }
}