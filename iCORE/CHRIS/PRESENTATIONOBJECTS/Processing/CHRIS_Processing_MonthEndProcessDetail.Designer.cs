namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_MonthEndProcessDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlGenerationStatus = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtAns = new CrplControlLibrary.SLTextBox(this.components);
            this.lblTitle = new System.Windows.Forms.Label();
            this.TE_OT_HRS = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_PFUND = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_CONV = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_HOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_BASIC = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_ATT_AWD = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_MEAL = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_OTHERS = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_OT_COST = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_MONTH = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_CONTRIB_TOTAL = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_DEPT = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_GROUP = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_SEGMENT = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_EMPLOYEE_TYPE = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_CATEGORY = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.TE_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.lblPNO = new System.Windows.Forms.Label();
            this.lblSegment = new System.Windows.Forms.Label();
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblDept = new System.Windows.Forms.Label();
            this.lblContrib = new System.Windows.Forms.Label();
            this.lblOTCost = new System.Windows.Forms.Label();
            this.lblATT = new System.Windows.Forms.Label();
            this.lblMeal = new System.Windows.Forms.Label();
            this.lblOther = new System.Windows.Forms.Label();
            this.lblOthers = new System.Windows.Forms.Label();
            this.lblPFund = new System.Windows.Forms.Label();
            this.lblConv = new System.Windows.Forms.Label();
            this.lblHouse = new System.Windows.Forms.Label();
            this.lblBasic = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblCat = new System.Windows.Forms.Label();
            this.lblDesig = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtAnswer = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlGenerationStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 472);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlGenerationStatus
            // 
            this.pnlGenerationStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGenerationStatus.ConcurrentPanels = null;
            this.pnlGenerationStatus.Controls.Add(this.txtAns);
            this.pnlGenerationStatus.Controls.Add(this.lblTitle);
            this.pnlGenerationStatus.Controls.Add(this.TE_OT_HRS);
            this.pnlGenerationStatus.Controls.Add(this.TE_PFUND);
            this.pnlGenerationStatus.Controls.Add(this.TE_CONV);
            this.pnlGenerationStatus.Controls.Add(this.TE_HOUSE);
            this.pnlGenerationStatus.Controls.Add(this.TE_BASIC);
            this.pnlGenerationStatus.Controls.Add(this.TE_ATT_AWD);
            this.pnlGenerationStatus.Controls.Add(this.TE_MEAL);
            this.pnlGenerationStatus.Controls.Add(this.TE_OTHERS);
            this.pnlGenerationStatus.Controls.Add(this.TE_OT_COST);
            this.pnlGenerationStatus.Controls.Add(this.TE_DESIG);
            this.pnlGenerationStatus.Controls.Add(this.TE_MONTH);
            this.pnlGenerationStatus.Controls.Add(this.TE_CONTRIB_TOTAL);
            this.pnlGenerationStatus.Controls.Add(this.TE_DEPT);
            this.pnlGenerationStatus.Controls.Add(this.TE_GROUP);
            this.pnlGenerationStatus.Controls.Add(this.TE_SEGMENT);
            this.pnlGenerationStatus.Controls.Add(this.TE_YEAR);
            this.pnlGenerationStatus.Controls.Add(this.TE_EMPLOYEE_TYPE);
            this.pnlGenerationStatus.Controls.Add(this.TE_CATEGORY);
            this.pnlGenerationStatus.Controls.Add(this.TE_BRANCH);
            this.pnlGenerationStatus.Controls.Add(this.TE_P_NO);
            this.pnlGenerationStatus.Controls.Add(this.lblPNO);
            this.pnlGenerationStatus.Controls.Add(this.lblSegment);
            this.pnlGenerationStatus.Controls.Add(this.lblGroup);
            this.pnlGenerationStatus.Controls.Add(this.lblDept);
            this.pnlGenerationStatus.Controls.Add(this.lblContrib);
            this.pnlGenerationStatus.Controls.Add(this.lblOTCost);
            this.pnlGenerationStatus.Controls.Add(this.lblATT);
            this.pnlGenerationStatus.Controls.Add(this.lblMeal);
            this.pnlGenerationStatus.Controls.Add(this.lblOther);
            this.pnlGenerationStatus.Controls.Add(this.lblOthers);
            this.pnlGenerationStatus.Controls.Add(this.lblPFund);
            this.pnlGenerationStatus.Controls.Add(this.lblConv);
            this.pnlGenerationStatus.Controls.Add(this.lblHouse);
            this.pnlGenerationStatus.Controls.Add(this.lblBasic);
            this.pnlGenerationStatus.Controls.Add(this.lblType);
            this.pnlGenerationStatus.Controls.Add(this.lblCat);
            this.pnlGenerationStatus.Controls.Add(this.lblDesig);
            this.pnlGenerationStatus.Controls.Add(this.lblBranch);
            this.pnlGenerationStatus.Controls.Add(this.lblMonth);
            this.pnlGenerationStatus.Controls.Add(this.txtAnswer);
            this.pnlGenerationStatus.DataManager = "";
            this.pnlGenerationStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlGenerationStatus.DependentPanels = null;
            this.pnlGenerationStatus.DisableDependentLoad = false;
            this.pnlGenerationStatus.EnableDelete = false;
            this.pnlGenerationStatus.EnableInsert = false;
            this.pnlGenerationStatus.EnableQuery = false;
            this.pnlGenerationStatus.EnableUpdate = false;
            this.pnlGenerationStatus.EntityName = null;
            this.pnlGenerationStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlGenerationStatus.Location = new System.Drawing.Point(27, 39);
            this.pnlGenerationStatus.MasterPanel = null;
            this.pnlGenerationStatus.Name = "pnlGenerationStatus";
            this.pnlGenerationStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlGenerationStatus.Size = new System.Drawing.Size(614, 427);
            this.pnlGenerationStatus.SPName = "CHRIS_SP_MONTHENDPROCESS_MANAGER";
            this.pnlGenerationStatus.TabIndex = 41;
            // 
            // txtAns
            // 
            this.txtAns.AllowSpace = true;
            this.txtAns.AssociatedLookUpName = "";
            this.txtAns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAns.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAns.ContinuationTextBox = null;
            this.txtAns.CustomEnabled = true;
            this.txtAns.DataFieldMapping = "";
            this.txtAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAns.GetRecordsOnUpDownKeys = false;
            this.txtAns.IsDate = false;
            this.txtAns.Location = new System.Drawing.Point(353, 392);
            this.txtAns.MaxLength = 1;
            this.txtAns.Name = "txtAns";
            this.txtAns.NumberFormat = "###,###,##0.00";
            this.txtAns.Postfix = "";
            this.txtAns.Prefix = "";
            this.txtAns.Size = new System.Drawing.Size(62, 20);
            this.txtAns.SkipValidation = false;
            this.txtAns.TabIndex = 20;
            this.txtAns.TextType = CrplControlLibrary.TextType.String;
            this.txtAns.TextChanged += new System.EventHandler(this.txtAns_TextChanged);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(215, 359);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(164, 15);
            this.lblTitle.TabIndex = 63;
            this.lblTitle.Text = "    PROCESS   COMPLETED";
            // 
            // TE_OT_HRS
            // 
            this.TE_OT_HRS.AllowSpace = true;
            this.TE_OT_HRS.AssociatedLookUpName = "";
            this.TE_OT_HRS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_OT_HRS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_OT_HRS.ContinuationTextBox = null;
            this.TE_OT_HRS.CustomEnabled = true;
            this.TE_OT_HRS.DataFieldMapping = "";
            this.TE_OT_HRS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_OT_HRS.GetRecordsOnUpDownKeys = false;
            this.TE_OT_HRS.IsDate = false;
            this.TE_OT_HRS.Location = new System.Drawing.Point(82, 320);
            this.TE_OT_HRS.MaxLength = 10;
            this.TE_OT_HRS.Name = "TE_OT_HRS";
            this.TE_OT_HRS.NumberFormat = "###,###,##0.00";
            this.TE_OT_HRS.Postfix = "";
            this.TE_OT_HRS.Prefix = "";
            this.TE_OT_HRS.ReadOnly = true;
            this.TE_OT_HRS.Size = new System.Drawing.Size(136, 20);
            this.TE_OT_HRS.SkipValidation = false;
            this.TE_OT_HRS.TabIndex = 19;
            this.TE_OT_HRS.TabStop = false;
            this.TE_OT_HRS.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_PFUND
            // 
            this.TE_PFUND.AllowSpace = true;
            this.TE_PFUND.AssociatedLookUpName = "";
            this.TE_PFUND.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_PFUND.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_PFUND.ContinuationTextBox = null;
            this.TE_PFUND.CustomEnabled = true;
            this.TE_PFUND.DataFieldMapping = "";
            this.TE_PFUND.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_PFUND.GetRecordsOnUpDownKeys = false;
            this.TE_PFUND.IsDate = false;
            this.TE_PFUND.Location = new System.Drawing.Point(82, 294);
            this.TE_PFUND.MaxLength = 10;
            this.TE_PFUND.Name = "TE_PFUND";
            this.TE_PFUND.NumberFormat = "###,###,##0.00";
            this.TE_PFUND.Postfix = "";
            this.TE_PFUND.Prefix = "";
            this.TE_PFUND.ReadOnly = true;
            this.TE_PFUND.Size = new System.Drawing.Size(136, 20);
            this.TE_PFUND.SkipValidation = false;
            this.TE_PFUND.TabIndex = 17;
            this.TE_PFUND.TabStop = false;
            this.TE_PFUND.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_CONV
            // 
            this.TE_CONV.AllowSpace = true;
            this.TE_CONV.AssociatedLookUpName = "";
            this.TE_CONV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_CONV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_CONV.ContinuationTextBox = null;
            this.TE_CONV.CustomEnabled = true;
            this.TE_CONV.DataFieldMapping = "";
            this.TE_CONV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_CONV.GetRecordsOnUpDownKeys = false;
            this.TE_CONV.IsDate = false;
            this.TE_CONV.Location = new System.Drawing.Point(82, 268);
            this.TE_CONV.MaxLength = 10;
            this.TE_CONV.Name = "TE_CONV";
            this.TE_CONV.NumberFormat = "###,###,##0.00";
            this.TE_CONV.Postfix = "";
            this.TE_CONV.Prefix = "";
            this.TE_CONV.ReadOnly = true;
            this.TE_CONV.Size = new System.Drawing.Size(136, 20);
            this.TE_CONV.SkipValidation = false;
            this.TE_CONV.TabIndex = 15;
            this.TE_CONV.TabStop = false;
            this.TE_CONV.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_HOUSE
            // 
            this.TE_HOUSE.AllowSpace = true;
            this.TE_HOUSE.AssociatedLookUpName = "";
            this.TE_HOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_HOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_HOUSE.ContinuationTextBox = null;
            this.TE_HOUSE.CustomEnabled = true;
            this.TE_HOUSE.DataFieldMapping = "";
            this.TE_HOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_HOUSE.GetRecordsOnUpDownKeys = false;
            this.TE_HOUSE.IsDate = false;
            this.TE_HOUSE.Location = new System.Drawing.Point(82, 242);
            this.TE_HOUSE.MaxLength = 10;
            this.TE_HOUSE.Name = "TE_HOUSE";
            this.TE_HOUSE.NumberFormat = "###,###,##0.00";
            this.TE_HOUSE.Postfix = "";
            this.TE_HOUSE.Prefix = "";
            this.TE_HOUSE.ReadOnly = true;
            this.TE_HOUSE.Size = new System.Drawing.Size(136, 20);
            this.TE_HOUSE.SkipValidation = false;
            this.TE_HOUSE.TabIndex = 13;
            this.TE_HOUSE.TabStop = false;
            this.TE_HOUSE.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_BASIC
            // 
            this.TE_BASIC.AllowSpace = true;
            this.TE_BASIC.AssociatedLookUpName = "";
            this.TE_BASIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_BASIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_BASIC.ContinuationTextBox = null;
            this.TE_BASIC.CustomEnabled = true;
            this.TE_BASIC.DataFieldMapping = "";
            this.TE_BASIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_BASIC.GetRecordsOnUpDownKeys = false;
            this.TE_BASIC.IsDate = false;
            this.TE_BASIC.Location = new System.Drawing.Point(82, 216);
            this.TE_BASIC.MaxLength = 10;
            this.TE_BASIC.Name = "TE_BASIC";
            this.TE_BASIC.NumberFormat = "###,###,##0.00";
            this.TE_BASIC.Postfix = "";
            this.TE_BASIC.Prefix = "";
            this.TE_BASIC.ReadOnly = true;
            this.TE_BASIC.Size = new System.Drawing.Size(136, 20);
            this.TE_BASIC.SkipValidation = false;
            this.TE_BASIC.TabIndex = 11;
            this.TE_BASIC.TabStop = false;
            this.TE_BASIC.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_ATT_AWD
            // 
            this.TE_ATT_AWD.AllowSpace = true;
            this.TE_ATT_AWD.AssociatedLookUpName = "";
            this.TE_ATT_AWD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_ATT_AWD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_ATT_AWD.ContinuationTextBox = null;
            this.TE_ATT_AWD.CustomEnabled = true;
            this.TE_ATT_AWD.DataFieldMapping = "";
            this.TE_ATT_AWD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_ATT_AWD.GetRecordsOnUpDownKeys = false;
            this.TE_ATT_AWD.IsDate = false;
            this.TE_ATT_AWD.Location = new System.Drawing.Point(377, 242);
            this.TE_ATT_AWD.MaxLength = 10;
            this.TE_ATT_AWD.Name = "TE_ATT_AWD";
            this.TE_ATT_AWD.NumberFormat = "###,###,##0.00";
            this.TE_ATT_AWD.Postfix = "";
            this.TE_ATT_AWD.Prefix = "";
            this.TE_ATT_AWD.ReadOnly = true;
            this.TE_ATT_AWD.Size = new System.Drawing.Size(136, 20);
            this.TE_ATT_AWD.SkipValidation = false;
            this.TE_ATT_AWD.TabIndex = 14;
            this.TE_ATT_AWD.TabStop = false;
            this.TE_ATT_AWD.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_MEAL
            // 
            this.TE_MEAL.AllowSpace = true;
            this.TE_MEAL.AssociatedLookUpName = "";
            this.TE_MEAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_MEAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_MEAL.ContinuationTextBox = null;
            this.TE_MEAL.CustomEnabled = true;
            this.TE_MEAL.DataFieldMapping = "";
            this.TE_MEAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_MEAL.GetRecordsOnUpDownKeys = false;
            this.TE_MEAL.IsDate = false;
            this.TE_MEAL.Location = new System.Drawing.Point(377, 268);
            this.TE_MEAL.MaxLength = 10;
            this.TE_MEAL.Name = "TE_MEAL";
            this.TE_MEAL.NumberFormat = "###,###,##0.00";
            this.TE_MEAL.Postfix = "";
            this.TE_MEAL.Prefix = "";
            this.TE_MEAL.ReadOnly = true;
            this.TE_MEAL.Size = new System.Drawing.Size(136, 20);
            this.TE_MEAL.SkipValidation = false;
            this.TE_MEAL.TabIndex = 16;
            this.TE_MEAL.TabStop = false;
            this.TE_MEAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_OTHERS
            // 
            this.TE_OTHERS.AllowSpace = true;
            this.TE_OTHERS.AssociatedLookUpName = "";
            this.TE_OTHERS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_OTHERS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_OTHERS.ContinuationTextBox = null;
            this.TE_OTHERS.CustomEnabled = true;
            this.TE_OTHERS.DataFieldMapping = "";
            this.TE_OTHERS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_OTHERS.GetRecordsOnUpDownKeys = false;
            this.TE_OTHERS.IsDate = false;
            this.TE_OTHERS.Location = new System.Drawing.Point(377, 292);
            this.TE_OTHERS.MaxLength = 10;
            this.TE_OTHERS.Name = "TE_OTHERS";
            this.TE_OTHERS.NumberFormat = "###,###,##0.00";
            this.TE_OTHERS.Postfix = "";
            this.TE_OTHERS.Prefix = "";
            this.TE_OTHERS.ReadOnly = true;
            this.TE_OTHERS.Size = new System.Drawing.Size(136, 20);
            this.TE_OTHERS.SkipValidation = false;
            this.TE_OTHERS.TabIndex = 18;
            this.TE_OTHERS.TabStop = false;
            this.TE_OTHERS.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_OT_COST
            // 
            this.TE_OT_COST.AllowSpace = true;
            this.TE_OT_COST.AssociatedLookUpName = "";
            this.TE_OT_COST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_OT_COST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_OT_COST.ContinuationTextBox = null;
            this.TE_OT_COST.CustomEnabled = true;
            this.TE_OT_COST.DataFieldMapping = "";
            this.TE_OT_COST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_OT_COST.GetRecordsOnUpDownKeys = false;
            this.TE_OT_COST.IsDate = false;
            this.TE_OT_COST.Location = new System.Drawing.Point(377, 216);
            this.TE_OT_COST.MaxLength = 10;
            this.TE_OT_COST.Name = "TE_OT_COST";
            this.TE_OT_COST.NumberFormat = "###,###,##0.00";
            this.TE_OT_COST.Postfix = "";
            this.TE_OT_COST.Prefix = "";
            this.TE_OT_COST.ReadOnly = true;
            this.TE_OT_COST.Size = new System.Drawing.Size(136, 20);
            this.TE_OT_COST.SkipValidation = false;
            this.TE_OT_COST.TabIndex = 12;
            this.TE_OT_COST.TabStop = false;
            this.TE_OT_COST.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_DESIG
            // 
            this.TE_DESIG.AllowSpace = true;
            this.TE_DESIG.AssociatedLookUpName = "";
            this.TE_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_DESIG.ContinuationTextBox = null;
            this.TE_DESIG.CustomEnabled = true;
            this.TE_DESIG.DataFieldMapping = "";
            this.TE_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_DESIG.GetRecordsOnUpDownKeys = false;
            this.TE_DESIG.IsDate = false;
            this.TE_DESIG.Location = new System.Drawing.Point(82, 117);
            this.TE_DESIG.MaxLength = 10;
            this.TE_DESIG.Name = "TE_DESIG";
            this.TE_DESIG.NumberFormat = "###,###,##0.00";
            this.TE_DESIG.Postfix = "";
            this.TE_DESIG.Prefix = "";
            this.TE_DESIG.ReadOnly = true;
            this.TE_DESIG.Size = new System.Drawing.Size(136, 20);
            this.TE_DESIG.SkipValidation = false;
            this.TE_DESIG.TabIndex = 7;
            this.TE_DESIG.TabStop = false;
            this.TE_DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_MONTH
            // 
            this.TE_MONTH.AllowSpace = true;
            this.TE_MONTH.AssociatedLookUpName = "";
            this.TE_MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_MONTH.ContinuationTextBox = null;
            this.TE_MONTH.CustomEnabled = true;
            this.TE_MONTH.DataFieldMapping = "";
            this.TE_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_MONTH.GetRecordsOnUpDownKeys = false;
            this.TE_MONTH.IsDate = false;
            this.TE_MONTH.Location = new System.Drawing.Point(82, 67);
            this.TE_MONTH.MaxLength = 10;
            this.TE_MONTH.Name = "TE_MONTH";
            this.TE_MONTH.NumberFormat = "###,###,##0.00";
            this.TE_MONTH.Postfix = "";
            this.TE_MONTH.Prefix = "";
            this.TE_MONTH.ReadOnly = true;
            this.TE_MONTH.Size = new System.Drawing.Size(136, 20);
            this.TE_MONTH.SkipValidation = false;
            this.TE_MONTH.TabIndex = 3;
            this.TE_MONTH.TabStop = false;
            this.TE_MONTH.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_CONTRIB_TOTAL
            // 
            this.TE_CONTRIB_TOTAL.AllowSpace = true;
            this.TE_CONTRIB_TOTAL.AssociatedLookUpName = "";
            this.TE_CONTRIB_TOTAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_CONTRIB_TOTAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_CONTRIB_TOTAL.ContinuationTextBox = null;
            this.TE_CONTRIB_TOTAL.CustomEnabled = true;
            this.TE_CONTRIB_TOTAL.DataFieldMapping = "";
            this.TE_CONTRIB_TOTAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_CONTRIB_TOTAL.GetRecordsOnUpDownKeys = false;
            this.TE_CONTRIB_TOTAL.IsDate = false;
            this.TE_CONTRIB_TOTAL.Location = new System.Drawing.Point(377, 117);
            this.TE_CONTRIB_TOTAL.MaxLength = 10;
            this.TE_CONTRIB_TOTAL.Name = "TE_CONTRIB_TOTAL";
            this.TE_CONTRIB_TOTAL.NumberFormat = "###,###,##0.00";
            this.TE_CONTRIB_TOTAL.Postfix = "";
            this.TE_CONTRIB_TOTAL.Prefix = "";
            this.TE_CONTRIB_TOTAL.ReadOnly = true;
            this.TE_CONTRIB_TOTAL.Size = new System.Drawing.Size(136, 20);
            this.TE_CONTRIB_TOTAL.SkipValidation = false;
            this.TE_CONTRIB_TOTAL.TabIndex = 8;
            this.TE_CONTRIB_TOTAL.TabStop = false;
            this.TE_CONTRIB_TOTAL.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_DEPT
            // 
            this.TE_DEPT.AllowSpace = true;
            this.TE_DEPT.AssociatedLookUpName = "";
            this.TE_DEPT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_DEPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_DEPT.ContinuationTextBox = null;
            this.TE_DEPT.CustomEnabled = true;
            this.TE_DEPT.DataFieldMapping = "";
            this.TE_DEPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_DEPT.GetRecordsOnUpDownKeys = false;
            this.TE_DEPT.IsDate = false;
            this.TE_DEPT.Location = new System.Drawing.Point(377, 93);
            this.TE_DEPT.MaxLength = 10;
            this.TE_DEPT.Name = "TE_DEPT";
            this.TE_DEPT.NumberFormat = "###,###,##0.00";
            this.TE_DEPT.Postfix = "";
            this.TE_DEPT.Prefix = "";
            this.TE_DEPT.ReadOnly = true;
            this.TE_DEPT.Size = new System.Drawing.Size(136, 20);
            this.TE_DEPT.SkipValidation = false;
            this.TE_DEPT.TabIndex = 6;
            this.TE_DEPT.TabStop = false;
            this.TE_DEPT.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_GROUP
            // 
            this.TE_GROUP.AllowSpace = true;
            this.TE_GROUP.AssociatedLookUpName = "";
            this.TE_GROUP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_GROUP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_GROUP.ContinuationTextBox = null;
            this.TE_GROUP.CustomEnabled = true;
            this.TE_GROUP.DataFieldMapping = "";
            this.TE_GROUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_GROUP.GetRecordsOnUpDownKeys = false;
            this.TE_GROUP.IsDate = false;
            this.TE_GROUP.Location = new System.Drawing.Point(377, 67);
            this.TE_GROUP.MaxLength = 10;
            this.TE_GROUP.Name = "TE_GROUP";
            this.TE_GROUP.NumberFormat = "###,###,##0.00";
            this.TE_GROUP.Postfix = "";
            this.TE_GROUP.Prefix = "";
            this.TE_GROUP.ReadOnly = true;
            this.TE_GROUP.Size = new System.Drawing.Size(136, 20);
            this.TE_GROUP.SkipValidation = false;
            this.TE_GROUP.TabIndex = 4;
            this.TE_GROUP.TabStop = false;
            this.TE_GROUP.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_SEGMENT
            // 
            this.TE_SEGMENT.AllowSpace = true;
            this.TE_SEGMENT.AssociatedLookUpName = "";
            this.TE_SEGMENT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_SEGMENT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_SEGMENT.ContinuationTextBox = null;
            this.TE_SEGMENT.CustomEnabled = true;
            this.TE_SEGMENT.DataFieldMapping = "";
            this.TE_SEGMENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_SEGMENT.GetRecordsOnUpDownKeys = false;
            this.TE_SEGMENT.IsDate = false;
            this.TE_SEGMENT.Location = new System.Drawing.Point(377, 41);
            this.TE_SEGMENT.MaxLength = 10;
            this.TE_SEGMENT.Name = "TE_SEGMENT";
            this.TE_SEGMENT.NumberFormat = "###,###,##0.00";
            this.TE_SEGMENT.Postfix = "";
            this.TE_SEGMENT.Prefix = "";
            this.TE_SEGMENT.ReadOnly = true;
            this.TE_SEGMENT.Size = new System.Drawing.Size(136, 20);
            this.TE_SEGMENT.SkipValidation = false;
            this.TE_SEGMENT.TabIndex = 2;
            this.TE_SEGMENT.TabStop = false;
            this.TE_SEGMENT.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_YEAR
            // 
            this.TE_YEAR.AllowSpace = true;
            this.TE_YEAR.AssociatedLookUpName = "";
            this.TE_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_YEAR.ContinuationTextBox = null;
            this.TE_YEAR.CustomEnabled = true;
            this.TE_YEAR.DataFieldMapping = "";
            this.TE_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_YEAR.GetRecordsOnUpDownKeys = false;
            this.TE_YEAR.IsDate = false;
            this.TE_YEAR.Location = new System.Drawing.Point(82, 15);
            this.TE_YEAR.MaxLength = 10;
            this.TE_YEAR.Name = "TE_YEAR";
            this.TE_YEAR.NumberFormat = "###,###,##0.00";
            this.TE_YEAR.Postfix = "";
            this.TE_YEAR.Prefix = "";
            this.TE_YEAR.ReadOnly = true;
            this.TE_YEAR.Size = new System.Drawing.Size(136, 20);
            this.TE_YEAR.SkipValidation = false;
            this.TE_YEAR.TabIndex = 0;
            this.TE_YEAR.TabStop = false;
            this.TE_YEAR.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_EMPLOYEE_TYPE
            // 
            this.TE_EMPLOYEE_TYPE.AllowSpace = true;
            this.TE_EMPLOYEE_TYPE.AssociatedLookUpName = "";
            this.TE_EMPLOYEE_TYPE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_EMPLOYEE_TYPE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_EMPLOYEE_TYPE.ContinuationTextBox = null;
            this.TE_EMPLOYEE_TYPE.CustomEnabled = true;
            this.TE_EMPLOYEE_TYPE.DataFieldMapping = "";
            this.TE_EMPLOYEE_TYPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_EMPLOYEE_TYPE.GetRecordsOnUpDownKeys = false;
            this.TE_EMPLOYEE_TYPE.IsDate = false;
            this.TE_EMPLOYEE_TYPE.Location = new System.Drawing.Point(82, 169);
            this.TE_EMPLOYEE_TYPE.MaxLength = 10;
            this.TE_EMPLOYEE_TYPE.Name = "TE_EMPLOYEE_TYPE";
            this.TE_EMPLOYEE_TYPE.NumberFormat = "###,###,##0.00";
            this.TE_EMPLOYEE_TYPE.Postfix = "";
            this.TE_EMPLOYEE_TYPE.Prefix = "";
            this.TE_EMPLOYEE_TYPE.ReadOnly = true;
            this.TE_EMPLOYEE_TYPE.Size = new System.Drawing.Size(136, 20);
            this.TE_EMPLOYEE_TYPE.SkipValidation = false;
            this.TE_EMPLOYEE_TYPE.TabIndex = 10;
            this.TE_EMPLOYEE_TYPE.TabStop = false;
            this.TE_EMPLOYEE_TYPE.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_CATEGORY
            // 
            this.TE_CATEGORY.AllowSpace = true;
            this.TE_CATEGORY.AssociatedLookUpName = "";
            this.TE_CATEGORY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_CATEGORY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_CATEGORY.ContinuationTextBox = null;
            this.TE_CATEGORY.CustomEnabled = true;
            this.TE_CATEGORY.DataFieldMapping = "";
            this.TE_CATEGORY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_CATEGORY.GetRecordsOnUpDownKeys = false;
            this.TE_CATEGORY.IsDate = false;
            this.TE_CATEGORY.Location = new System.Drawing.Point(82, 143);
            this.TE_CATEGORY.MaxLength = 10;
            this.TE_CATEGORY.Name = "TE_CATEGORY";
            this.TE_CATEGORY.NumberFormat = "###,###,##0.00";
            this.TE_CATEGORY.Postfix = "";
            this.TE_CATEGORY.Prefix = "";
            this.TE_CATEGORY.ReadOnly = true;
            this.TE_CATEGORY.Size = new System.Drawing.Size(136, 20);
            this.TE_CATEGORY.SkipValidation = false;
            this.TE_CATEGORY.TabIndex = 9;
            this.TE_CATEGORY.TabStop = false;
            this.TE_CATEGORY.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_BRANCH
            // 
            this.TE_BRANCH.AllowSpace = true;
            this.TE_BRANCH.AssociatedLookUpName = "";
            this.TE_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_BRANCH.ContinuationTextBox = null;
            this.TE_BRANCH.CustomEnabled = true;
            this.TE_BRANCH.DataFieldMapping = "";
            this.TE_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_BRANCH.GetRecordsOnUpDownKeys = false;
            this.TE_BRANCH.IsDate = false;
            this.TE_BRANCH.Location = new System.Drawing.Point(82, 91);
            this.TE_BRANCH.MaxLength = 10;
            this.TE_BRANCH.Name = "TE_BRANCH";
            this.TE_BRANCH.NumberFormat = "###,###,##0.00";
            this.TE_BRANCH.Postfix = "";
            this.TE_BRANCH.Prefix = "";
            this.TE_BRANCH.ReadOnly = true;
            this.TE_BRANCH.Size = new System.Drawing.Size(136, 20);
            this.TE_BRANCH.SkipValidation = false;
            this.TE_BRANCH.TabIndex = 5;
            this.TE_BRANCH.TabStop = false;
            this.TE_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // TE_P_NO
            // 
            this.TE_P_NO.AllowSpace = true;
            this.TE_P_NO.AssociatedLookUpName = "";
            this.TE_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TE_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TE_P_NO.ContinuationTextBox = null;
            this.TE_P_NO.CustomEnabled = true;
            this.TE_P_NO.DataFieldMapping = "";
            this.TE_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TE_P_NO.GetRecordsOnUpDownKeys = false;
            this.TE_P_NO.IsDate = false;
            this.TE_P_NO.Location = new System.Drawing.Point(82, 41);
            this.TE_P_NO.MaxLength = 10;
            this.TE_P_NO.Name = "TE_P_NO";
            this.TE_P_NO.NumberFormat = "###,###,##0.00";
            this.TE_P_NO.Postfix = "";
            this.TE_P_NO.Prefix = "";
            this.TE_P_NO.ReadOnly = true;
            this.TE_P_NO.Size = new System.Drawing.Size(136, 20);
            this.TE_P_NO.SkipValidation = false;
            this.TE_P_NO.TabIndex = 1;
            this.TE_P_NO.TabStop = false;
            this.TE_P_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblPNO
            // 
            this.lblPNO.AutoSize = true;
            this.lblPNO.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPNO.Location = new System.Drawing.Point(36, 44);
            this.lblPNO.Name = "lblPNO";
            this.lblPNO.Size = new System.Drawing.Size(37, 15);
            this.lblPNO.TabIndex = 40;
            this.lblPNO.Text = "P.NO";
            // 
            // lblSegment
            // 
            this.lblSegment.AutoSize = true;
            this.lblSegment.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSegment.Location = new System.Drawing.Point(304, 44);
            this.lblSegment.Name = "lblSegment";
            this.lblSegment.Size = new System.Drawing.Size(66, 15);
            this.lblSegment.TabIndex = 41;
            this.lblSegment.Text = "SEGMENT";
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroup.Location = new System.Drawing.Point(320, 70);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(51, 15);
            this.lblGroup.TabIndex = 41;
            this.lblGroup.Text = "GROUP";
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDept.Location = new System.Drawing.Point(331, 96);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(39, 15);
            this.lblDept.TabIndex = 41;
            this.lblDept.Text = "DEPT";
            // 
            // lblContrib
            // 
            this.lblContrib.AutoSize = true;
            this.lblContrib.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContrib.Location = new System.Drawing.Point(267, 120);
            this.lblContrib.Name = "lblContrib";
            this.lblContrib.Size = new System.Drawing.Size(104, 15);
            this.lblContrib.TabIndex = 41;
            this.lblContrib.Text = "CONTRIBUTION";
            // 
            // lblOTCost
            // 
            this.lblOTCost.AutoSize = true;
            this.lblOTCost.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOTCost.Location = new System.Drawing.Point(304, 219);
            this.lblOTCost.Name = "lblOTCost";
            this.lblOTCost.Size = new System.Drawing.Size(66, 15);
            this.lblOTCost.TabIndex = 41;
            this.lblOTCost.Text = "O.T.COST";
            // 
            // lblATT
            // 
            this.lblATT.AutoSize = true;
            this.lblATT.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblATT.Location = new System.Drawing.Point(304, 245);
            this.lblATT.Name = "lblATT";
            this.lblATT.Size = new System.Drawing.Size(66, 15);
            this.lblATT.TabIndex = 41;
            this.lblATT.Text = "ATT.AWD";
            // 
            // lblMeal
            // 
            this.lblMeal.AutoSize = true;
            this.lblMeal.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeal.Location = new System.Drawing.Point(328, 271);
            this.lblMeal.Name = "lblMeal";
            this.lblMeal.Size = new System.Drawing.Size(42, 15);
            this.lblMeal.TabIndex = 41;
            this.lblMeal.Text = "MEAL";
            // 
            // lblOther
            // 
            this.lblOther.AutoSize = true;
            this.lblOther.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOther.Location = new System.Drawing.Point(316, 295);
            this.lblOther.Name = "lblOther";
            this.lblOther.Size = new System.Drawing.Size(58, 15);
            this.lblOther.TabIndex = 41;
            this.lblOther.Text = "OTHERS";
            // 
            // lblOthers
            // 
            this.lblOthers.AutoSize = true;
            this.lblOthers.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOthers.Location = new System.Drawing.Point(18, 323);
            this.lblOthers.Name = "lblOthers";
            this.lblOthers.Size = new System.Drawing.Size(54, 15);
            this.lblOthers.TabIndex = 41;
            this.lblOthers.Text = "OT.HRS";
            // 
            // lblPFund
            // 
            this.lblPFund.AutoSize = true;
            this.lblPFund.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPFund.Location = new System.Drawing.Point(26, 297);
            this.lblPFund.Name = "lblPFund";
            this.lblPFund.Size = new System.Drawing.Size(48, 15);
            this.lblPFund.TabIndex = 41;
            this.lblPFund.Text = "PFUND";
            // 
            // lblConv
            // 
            this.lblConv.AutoSize = true;
            this.lblConv.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConv.Location = new System.Drawing.Point(29, 271);
            this.lblConv.Name = "lblConv";
            this.lblConv.Size = new System.Drawing.Size(44, 15);
            this.lblConv.TabIndex = 41;
            this.lblConv.Text = "CONV";
            // 
            // lblHouse
            // 
            this.lblHouse.AutoSize = true;
            this.lblHouse.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouse.Location = new System.Drawing.Point(26, 245);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(49, 15);
            this.lblHouse.TabIndex = 41;
            this.lblHouse.Text = "HOUSE";
            // 
            // lblBasic
            // 
            this.lblBasic.AutoSize = true;
            this.lblBasic.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBasic.Location = new System.Drawing.Point(29, 219);
            this.lblBasic.Name = "lblBasic";
            this.lblBasic.Size = new System.Drawing.Size(46, 15);
            this.lblBasic.TabIndex = 41;
            this.lblBasic.Text = "BASIC";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(35, 172);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(38, 15);
            this.lblType.TabIndex = 41;
            this.lblType.Text = "TYPE";
            // 
            // lblCat
            // 
            this.lblCat.AutoSize = true;
            this.lblCat.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCat.Location = new System.Drawing.Point(3, 146);
            this.lblCat.Name = "lblCat";
            this.lblCat.Size = new System.Drawing.Size(76, 15);
            this.lblCat.TabIndex = 41;
            this.lblCat.Text = "CATEGORY";
            // 
            // lblDesig
            // 
            this.lblDesig.AutoSize = true;
            this.lblDesig.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesig.Location = new System.Drawing.Point(29, 120);
            this.lblDesig.Name = "lblDesig";
            this.lblDesig.Size = new System.Drawing.Size(44, 15);
            this.lblDesig.TabIndex = 41;
            this.lblDesig.Text = "DESIG";
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            this.lblBranch.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(18, 94);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(61, 15);
            this.lblBranch.TabIndex = 41;
            this.lblBranch.Text = "BRANCH";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonth.Location = new System.Drawing.Point(22, 70);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(54, 15);
            this.lblMonth.TabIndex = 41;
            this.lblMonth.Text = "MONTH";
            // 
            // txtAnswer
            // 
            this.txtAnswer.AutoSize = true;
            this.txtAnswer.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnswer.Location = new System.Drawing.Point(145, 395);
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(185, 15);
            this.txtAnswer.TabIndex = 38;
            this.txtAnswer.Text = "PRESS ANY KEY TO GO BACK";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(430, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 42;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Processing_MonthEndProcessDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(669, 532);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlGenerationStatus);
            this.Name = "CHRIS_Processing_MonthEndProcessDetail";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Month End Process Details";
            this.Shown += new System.EventHandler(this.CHRIS_Processing_MonthEndProcessDetail_Shown);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlGenerationStatus, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlGenerationStatus.ResumeLayout(false);
            this.pnlGenerationStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlGenerationStatus;
        private System.Windows.Forms.Label txtAnswer;
        private CrplControlLibrary.SLTextBox TE_OT_HRS;
        private CrplControlLibrary.SLTextBox TE_PFUND;
        private CrplControlLibrary.SLTextBox TE_CONV;
        private CrplControlLibrary.SLTextBox TE_HOUSE;
        private CrplControlLibrary.SLTextBox TE_BASIC;
        private CrplControlLibrary.SLTextBox TE_ATT_AWD;
        private CrplControlLibrary.SLTextBox TE_MEAL;
        private CrplControlLibrary.SLTextBox TE_OTHERS;
        private CrplControlLibrary.SLTextBox TE_OT_COST;
        private CrplControlLibrary.SLTextBox TE_DESIG;
        private CrplControlLibrary.SLTextBox TE_MONTH;
        private CrplControlLibrary.SLTextBox TE_CONTRIB_TOTAL;
        private CrplControlLibrary.SLTextBox TE_DEPT;
        private CrplControlLibrary.SLTextBox TE_GROUP;
        private CrplControlLibrary.SLTextBox TE_SEGMENT;
        private CrplControlLibrary.SLTextBox TE_YEAR;
        private CrplControlLibrary.SLTextBox TE_EMPLOYEE_TYPE;
        private CrplControlLibrary.SLTextBox TE_CATEGORY;
        private CrplControlLibrary.SLTextBox TE_BRANCH;
        private CrplControlLibrary.SLTextBox TE_P_NO;
        private System.Windows.Forms.Label lblPNO;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblSegment;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblDept;
        private System.Windows.Forms.Label lblContrib;
        private System.Windows.Forms.Label lblOTCost;
        private System.Windows.Forms.Label lblATT;
        private System.Windows.Forms.Label lblMeal;
        private System.Windows.Forms.Label lblOther;
        private System.Windows.Forms.Label lblOthers;
        private System.Windows.Forms.Label lblPFund;
        private System.Windows.Forms.Label lblConv;
        private System.Windows.Forms.Label lblHouse;
        private System.Windows.Forms.Label lblBasic;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblCat;
        private System.Windows.Forms.Label lblDesig;
        private System.Windows.Forms.Label lblBranch;
        private CrplControlLibrary.SLTextBox txtAns;
        private System.Windows.Forms.Label lblUserName;
    }
}