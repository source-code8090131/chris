using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_PayrollHistoryUpdationProcess : ChrisSimpleForm 
    {
        System.Data.SqlClient.SqlTransaction processTrans = null;
        System.Data.SqlClient.SqlConnection proCon = new System.Data.SqlClient.SqlConnection();
        public int MARKUP_RATE_DAYS = -1;
        private static XMS.DATAOBJECTS.ConnectionBean connbean = null;

        public CHRIS_Processing_PayrollHistoryUpdationProcess()
        {
            InitializeComponent();
            
        }
    
        public CHRIS_Processing_PayrollHistoryUpdationProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
          
        }

        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

        }


        protected override void OnLoad(EventArgs e)
        {
 	        base.OnLoad(e);
            this.txtUser.Text = this.userID;

           

        }

        private void slTxtBoxProcess_TextChanged(object sender, EventArgs e)
        {

            
    
        }

        private void slTxtBoxProcess_Leave(object sender, EventArgs e)
        {
            if (this.slTxtBoxProcess.Text.Length > 0)
            {
                if (this.slTxtBoxProcess.Text == "YES" || this.slTxtBoxProcess.Text == "NO" && txtPersNo.Text != string.Empty)
                {
                    if (this.slTxtBoxProcess.Text == "YES")
                    {
                        int pr_p_no = 0;
                        Dictionary<String, Object> paramValues = new Dictionary<String, Object>();
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        slTxtBoxProcess.Text = String.Empty;
                        try 
                        {
                            connbean = SQLManager.GetConnectionBean();
                            if (connbean != null)
                            {
                                proCon = connbean.getDatabaseConnection(0);
                                processTrans = proCon.BeginTransaction();

                                lblProcessing.Visible = true;
                                Application.DoEvents();
                                Int32.TryParse(txtPersNo.Text, out pr_p_no);

                                paramValues.Clear();
                                paramValues.Add("PR_P_NO", pr_p_no);
                                paramValues.Add("W_DATE", dtpProcessingDate.Value);

                                    rslt = cmnDM.Execute("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "VALID_PNO", paramValues,ref processTrans ,60000);

                                if (!rslt.isSuccessful)
                                {
                                    if (processTrans != null && processTrans.Connection != null)
                                        processTrans.Rollback();
                                    base.LogException(this.Name, "PayrollHistoryUpdationProcess", rslt.exp);
                                    lblProcessing.Text = "Process Failed!";
                                    slTxtBoxProcess.Focus();
                                    slTxtBoxProcess.Select();
                                }
                                else
                                {
                                    lblProcessing.Text = "Process Completed.";
                                    pnlFinalAnswer.Visible = true;
                                    txtAnswerDialog.Focus();
                                    txtAnswerDialog.Select();
                                }
                                
                            }
                        }
                        catch (Exception dbEcxep)
                        {
                            if (processTrans != null && processTrans.Connection != null)
                                processTrans.Rollback();
                            base.LogException(this.Name, "PayrollHistoryUpdationProcess", dbEcxep);
                        }
                        finally
                        {
                            slTxtBoxProcess.Text = String.Empty;
                        }
                    }
                    else
                        this.Close();
                }
                else if (this.slTxtBoxProcess.Text == "NO")
                {
                    this.Close();
                }
            }
        }

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            Dictionary<String, Object> paramValues = new Dictionary<String, Object>();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            if (txtPersNo.Text.Trim().Equals(String.Empty))
            {
                txtPersNo.Focus();
                txtPersNo.Select();
                return;
            }

            
            int pr_p_no = 0;
            Int32.TryParse(txtPersNo.Text, out pr_p_no);
            paramValues.Clear();
            paramValues.Add("PR_P_NO", pr_p_no);
            paramValues.Add("W_DATE", dtpProcessingDate.Value);

            // IS FAST CHECK
            rsltCode = cmnDM.GetData("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "IS_FAST_NO", paramValues);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult != null && rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Enter P.No. For IS Or FAST Only", "", MessageBoxButtons.OK);
                        txtPersNo.Focus();
                        txtPersNo.Select();
                        return;
                    }
                }
            }

            // FN MONTH CHECK
            rsltCode = cmnDM.GetData("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "FIN_CHECK", paramValues);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult != null && rsltCode.dstResult.Tables.Count > 0)
                {
                    if (!rsltCode.dstResult.Tables[0].Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Finance Installment Has Already Deducted For The Given Month", "", MessageBoxButtons.OK);
                        txtPersNo.Focus();
                        txtPersNo.Select();
                        return;
                    }
                }
            }

        }

        private void lblProcessing_VisibleChanged(object sender, EventArgs e)
        {
            txtPersNo.Enabled = !lblProcessing.Visible;
            dtpProcessingDate.Enabled = !lblProcessing.Visible;
            slTxtBoxProcess.Enabled = !lblProcessing.Visible;
        }

        private void txtAnswerDialog_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().Equals("\r"))
            {
                try
                {

                    if (txtAnswerDialog.Text != string.Empty)
                    {
                        if (txtAnswerDialog.Text.ToUpper() == "YES")
                        {
                            processTrans.Commit();
                        }
                        else if (txtAnswerDialog.Text.ToUpper() == "NO")
                        {
                            processTrans.Rollback();
                        }
                    }
                }
                catch (Exception exce)
                {
                    if (processTrans != null && processTrans.Connection != null)
                        processTrans.Rollback();
                    base.LogException(this.Name, "txtAnswerDialog_KeyPress", exce);
                }
                finally
                {
                    txtAnswerDialog.Text = String.Empty;
                    lblProcessing.Visible = false;
                    pnlFinalAnswer.Visible = false;
                    txtPersNo.Focus();
                    txtPersNo.Select();
                }
            }
        }

    
    }
}