namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_FinancePayrollHistoryUpdationProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblHeader = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtAfter = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new CrplControlLibrary.SLButton();
            this.dtpPayroll = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtBefore = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtMessages = new CrplControlLibrary.SLTextBox(this.components);
            this.lblPayrollDate = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblUserName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(605, 0);
            // 
            // lblHeader
            // 
            this.lblHeader.BackColor = System.Drawing.Color.White;
            this.lblHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(128, 49);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(385, 34);
            this.lblHeader.TabIndex = 13;
            this.lblHeader.Text = "PAYROLL HISTORY UPDATION PROCESS";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.txtAfter);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.btnStart);
            this.pnlDetail.Controls.Add(this.dtpPayroll);
            this.pnlDetail.Controls.Add(this.txtBefore);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtMessages);
            this.pnlDetail.Controls.Add(this.lblPayrollDate);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "";
            this.pnlDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.pnlDetail.Location = new System.Drawing.Point(3, 16);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(574, 220);
            this.pnlDetail.SPName = "CHRIS_SP_FINANCEPAYROLLHISTORY_MANAGER";
            this.pnlDetail.TabIndex = 0;
            // 
            // txtAfter
            // 
            this.txtAfter.AllowSpace = true;
            this.txtAfter.AssociatedLookUpName = "";
            this.txtAfter.BackColor = System.Drawing.SystemColors.Control;
            this.txtAfter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAfter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAfter.ContinuationTextBox = null;
            this.txtAfter.CustomEnabled = false;
            this.txtAfter.DataFieldMapping = "";
            this.txtAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAfter.GetRecordsOnUpDownKeys = false;
            this.txtAfter.IsDate = false;
            this.txtAfter.Location = new System.Drawing.Point(130, 144);
            this.txtAfter.MaxLength = 11;
            this.txtAfter.Name = "txtAfter";
            this.txtAfter.NumberFormat = "###,###,##0.00";
            this.txtAfter.Postfix = "";
            this.txtAfter.Prefix = "";
            this.txtAfter.Size = new System.Drawing.Size(150, 20);
            this.txtAfter.SkipValidation = false;
            this.txtAfter.TabIndex = 20;
            this.txtAfter.Text = "0";
            this.txtAfter.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(36, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "After";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnStart
            // 
            this.btnStart.ActionType = "";
            this.btnStart.Location = new System.Drawing.Point(320, 120);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(132, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // dtpPayroll
            // 
            this.dtpPayroll.CustomEnabled = true;
            this.dtpPayroll.CustomFormat = "dd/MM/yyyy";
            this.dtpPayroll.DataFieldMapping = "";
            this.dtpPayroll.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayroll.Location = new System.Drawing.Point(130, 24);
            this.dtpPayroll.Name = "dtpPayroll";
            this.dtpPayroll.NullValue = " ";
            this.dtpPayroll.Size = new System.Drawing.Size(150, 20);
            this.dtpPayroll.TabIndex = 18;
            this.dtpPayroll.Value = new System.DateTime(2010, 11, 26, 19, 29, 39, 543);
            // 
            // txtBefore
            // 
            this.txtBefore.AllowSpace = true;
            this.txtBefore.AssociatedLookUpName = "";
            this.txtBefore.BackColor = System.Drawing.SystemColors.Control;
            this.txtBefore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBefore.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBefore.ContinuationTextBox = null;
            this.txtBefore.CustomEnabled = false;
            this.txtBefore.DataFieldMapping = "";
            this.txtBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBefore.GetRecordsOnUpDownKeys = false;
            this.txtBefore.IsDate = false;
            this.txtBefore.Location = new System.Drawing.Point(130, 100);
            this.txtBefore.MaxLength = 11;
            this.txtBefore.Name = "txtBefore";
            this.txtBefore.NumberFormat = "###,###,##0.00";
            this.txtBefore.Postfix = "";
            this.txtBefore.Prefix = "";
            this.txtBefore.Size = new System.Drawing.Size(150, 20);
            this.txtBefore.SkipValidation = false;
            this.txtBefore.TabIndex = 17;
            this.txtBefore.Text = "0";
            this.txtBefore.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(36, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Before";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMessages
            // 
            this.txtMessages.AllowSpace = true;
            this.txtMessages.AssociatedLookUpName = "";
            this.txtMessages.BackColor = System.Drawing.SystemColors.Control;
            this.txtMessages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMessages.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMessages.ContinuationTextBox = null;
            this.txtMessages.CustomEnabled = true;
            this.txtMessages.DataFieldMapping = "";
            this.txtMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMessages.GetRecordsOnUpDownKeys = false;
            this.txtMessages.IsDate = false;
            this.txtMessages.Location = new System.Drawing.Point(130, 61);
            this.txtMessages.MaxLength = 250;
            this.txtMessages.Name = "txtMessages";
            this.txtMessages.NumberFormat = "###,###,##0.00";
            this.txtMessages.Postfix = "";
            this.txtMessages.Prefix = "";
            this.txtMessages.ReadOnly = true;
            this.txtMessages.Size = new System.Drawing.Size(428, 20);
            this.txtMessages.SkipValidation = false;
            this.txtMessages.TabIndex = 12;
            this.txtMessages.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblPayrollDate
            // 
            this.lblPayrollDate.Location = new System.Drawing.Point(36, 24);
            this.lblPayrollDate.Name = "lblPayrollDate";
            this.lblPayrollDate.Size = new System.Drawing.Size(83, 20);
            this.lblPayrollDate.TabIndex = 9;
            this.lblPayrollDate.Text = "Payroll Date";
            this.lblPayrollDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(36, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Messages";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlDetail);
            this.groupBox1.Location = new System.Drawing.Point(27, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 239);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(413, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 14;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Processing_FinancePayrollHistoryUpdationProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(641, 388);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblHeader);
            this.CurrentPanelBlock = "pnlDetail";
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "CHRIS_Processing_FinancePayrollHistoryUpdationProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS -  Finance  Payroll  History  Updation  Process";
            this.Controls.SetChildIndex(this.lblHeader, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtAfter;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLButton btnStart;
        private CrplControlLibrary.SLDatePicker dtpPayroll;
        private CrplControlLibrary.SLTextBox txtBefore;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtMessages;
        private System.Windows.Forms.Label lblPayrollDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblUserName;
    }
}