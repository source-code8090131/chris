using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_FinancePayrollHistoryUpdationProcess : ChrisSimpleForm
    {

        #region Declaration
        DataTable dtFResult = new DataTable();
        #endregion

        #region Constructor

        public CHRIS_Processing_FinancePayrollHistoryUpdationProcess()
        {
            InitializeComponent();
        }
        public CHRIS_Processing_FinancePayrollHistoryUpdationProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            lblUserName.Text += "   "  + this.UserName;
            txtOption.Visible = false;

        }
        
        #endregion

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

        }

        #region Events
       
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            foreach (ToolStripItem tlbItm in tlbMain.Items)
            {
                if (tlbItm.Name == "tbtDelete" || tlbItm.Name == "tbtEdit" || tlbItm.Name == "tbtSave" || tlbItm.Name == "tbtList")
                    tlbItm.Available = false;
                else
                    tlbItm.Available = true;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// METHOD WILL DELETE THE RECORDS FROM
        /// PAYROLL,PAYROLL_ALLOW,PAYROLL_DEDUC,MONTH_OVERTIME,
        /// MONTH_SUMMARY,SAL_ADVANCE,BONUS_TAB AND SAL_AREAR
        /// WHERE DATE IS LESS THAN = 30/06/CURRENTYEAR
        /// ALSO INSERT THE RECORDS IN TABLES :
        /// HPAYROLL,HISTLOG,hpayroll_allow,hpayroll_deduc,
        /// HMONTH_OVERTIME,HMONTH_SUMMARY,HSAL_ADVANCE,
        /// HBONUS_TAB AND HSAL_AREAR
        /// </summary>
        /// <returns></returns>
        public DataTable GenerateFinanceHistory()
        {

            try
            {
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                DataTable dtFPH = new DataTable();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                //rsltCode = cmnDM.GetData(this.pnlDetail.SPName, String.Empty);

                rsltCode = cmnDM.GetData("CHRIS_SP_FINANCEPAYROLLHISTORY_MANAGER", "", colsNVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        dtFPH = rsltCode.dstResult.Tables[0];
                    }
                }
                return dtFPH;
            }
            catch (Exception exp)
            {
                LogError(this.Name, "GenerateFinanceHistory", exp);
                return null;
            }
        }
           
        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                //MessageBox.Show("in button start");
                dtFResult = GenerateFinanceHistory();

                if (dtFResult.Rows.Count != 0)
                {
                    for (int z = 0; z < dtFResult.Rows.Count; z++)
                    {
                        Application.DoEvents();
                        dtpPayroll.Text = Convert.ToString(dtFResult.Rows[0]["W_DATE1"]);
                        txtMessages.Text = Convert.ToString(dtFResult.Rows[z]["Messages"]);
                        txtBefore.Text = Convert.ToString(dtFResult.Rows[z]["tBefore"]);
                        txtAfter.Text = Convert.ToString(dtFResult.Rows[z]["tAfter"]);
                        //System.Threading.Thread.Sleep(100);
                    }
                }

                MessageBox.Show("SUCCESSFULL END OF PROGRAM press <ENTER> ...." , "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception exp)
            {
                LogError(this.Name, "btnStart_Click", exp);
            }
        }
        #endregion
    }
}