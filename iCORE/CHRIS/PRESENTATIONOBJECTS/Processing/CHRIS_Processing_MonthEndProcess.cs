using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_MonthEndProcess : ChrisSimpleForm
    {
        public CHRIS_Processing_MonthEndProcess()
        {
            InitializeComponent();
            txtReply.Focus();
            
        }

        public CHRIS_Processing_MonthEndProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.ShowStatusBar = false;
            this.txtOption.Visible = false;
            txtReply.Focus();
            lblUserName.Text += "   " + this.UserName;
                      
        }



        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

        }






        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtUser.Text = this.userID; 
            this.txtCurrDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            tbtAdd.Visible = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
                        
        }

        /// <summary>
        /// /***********************************************************************
        /// Need to Have Data in TEMP_TAB,OVERTIME,PAY_ATTEND_INT,SAL_AREAR
        /// IT WILL EVENTUALLY DELETE DATA FROM TEMP_DATA, OVERTIME_HISTORY,SHIFT_TAB 
        /// AND OVERTIME && INSERT DATA IN OVERTIME_HISTORY,TEMP_TAB, MONTH_SUMMARY
        /// AND MONTH_SUMMARY_CONT
        /// ***********************************************************************/
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtReply_TextChanged(object sender, EventArgs e)
        {

            //if (txtReply.Text.Length > 0 && txtReply.Text.Equals("\r"))
            //{
            //    if (txtReply.Text == "YES" || txtReply.Text == "NO")
            //    {
            //        if (txtReply.Text == "YES")
            //        {
            //            //string prosDate = dtpProcessingDate.Text;
            //            //int procNum = Convert.ToInt32(txtPersNo.Text);
            //            CHRIS_Processing_MonthEndProcessDetail MonthEndProcessDetailDialog = new CHRIS_Processing_MonthEndProcessDetail();
            //            MonthEndProcessDetailDialog.ShowDialog();
            //            if (MonthEndProcessDetailDialog.IsCompleted)
            //            {
            //                this.Close();
            //            }
            //            txtReply.Text = String.Empty;
            //        }
            //        else
            //            this.Close();
            //    }
            //}


        }

        private void txtReply_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtReply.Text.Length > 0 && e.KeyChar.Equals('\r'))
            {
                if (txtReply.Text == "YES" || txtReply.Text == "NO")
                {
                    if (txtReply.Text == "YES")
                    {
                        //string prosDate = dtpProcessingDate.Text;
                        //int procNum = Convert.ToInt32(txtPersNo.Text);
                        CHRIS_Processing_MonthEndProcessDetail MonthEndProcessDetailDialog = new CHRIS_Processing_MonthEndProcessDetail();
                        MonthEndProcessDetailDialog.ShowDialog();
                        if (MonthEndProcessDetailDialog.IsCompleted)
                        {
                            this.Close();
                        }
                        txtReply.Text = String.Empty;
                    }
                    else
                        this.Close();
                }
            }

        }

    }
}