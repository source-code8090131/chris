using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_CBonusApproval_BonusPercentDialog : Form
    {

        #region Properties

        private string m_BonusYear;
        public String BonusYear
        {
            get { return m_BonusYear; }
            set { m_BonusYear = value; }
        }

        private string m_BonusPercent;
        public String BonusPercent
        {
            get { return m_BonusPercent; }
            set { m_BonusPercent = value; }

        }

        bool isCompleted = false;
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        #endregion

        #region Declaration
        CHRIS_Processing_CBonusApproval_Bonus objCHRIS_Processing_CBonusApproval_Bonus;
        #endregion

        #region Constructor
        public CHRIS_Processing_CBonusApproval_BonusPercentDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void slTxtBonusPercent_KeyPress(object sender, KeyPressEventArgs e)        
        {
            if ((this.slTxtBonusPercent.Text.Trim() != String.Empty) && (e.KeyChar == '\r'))
            {
                if (double.Parse(slTxtBonusPercent.Text) < 100.0)
                {
                    isCompleted = true;
                    BonusPercent = this.slTxtBonusPercent.Text;
                    //objCHRIS_Processing_CBonusApproval_Bonus = new CHRIS_Processing_CBonusApproval_Bonus();

                    //objCHRIS_Processing_CBonusApproval_Bonus.BonusYear = BonusYear;
                    //objCHRIS_Processing_CBonusApproval_Bonus.BonusPercent = this.slTxtBonusPercent.Text;

                    //objCHRIS_Processing_CBonusApproval_Bonus.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Field must be of form 99.99", "Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void CHRIS_Processing_CBonusApproval_BonusPercentDialog_Load(object sender, EventArgs e)
        {

        }
        #endregion
    }
}