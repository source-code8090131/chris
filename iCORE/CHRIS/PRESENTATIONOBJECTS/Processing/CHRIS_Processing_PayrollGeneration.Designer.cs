namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_PayrollGeneration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPnlMainProcessing = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.groupBoxMainProcess = new System.Windows.Forms.GroupBox();
            this.labelStatusBar = new System.Windows.Forms.Label();
            this.slTB_Status = new CrplControlLibrary.SLTextBox(this.components);
            this.slTB_Reply_For_Generation = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.slTB_Pay_Date = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slDatePicker_Sys_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.slTB_PF_Exempt = new CrplControlLibrary.SLTextBox(this.components);
            this.slTBMarginal_Impact = new CrplControlLibrary.SLTextBox(this.components);
            this.slTB_Transport_Vp = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkResetTermFlag = new CrplControlLibrary.SLCheckBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.slTB_TotalTaxableAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.slTB_LoanTaxAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.slTB_No = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBoxProcessRunning = new System.Windows.Forms.GroupBox();
            this.slTB_EmpGroup_Answer = new CrplControlLibrary.SLTextBox(this.components);
            this.slTB_Pr_P_No = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.groupBoxProcessCompleted = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.slTB_FinalAnswer = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.btnFlagReset = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlTermFlag = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlMainProcessing.SuspendLayout();
            this.groupBoxMainProcess.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxProcessRunning.SuspendLayout();
            this.groupBoxProcessCompleted.SuspendLayout();
            this.pnlTermFlag.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPnlMainProcessing
            // 
            this.slPnlMainProcessing.ConcurrentPanels = null;
            this.slPnlMainProcessing.Controls.Add(this.groupBoxMainProcess);
            this.slPnlMainProcessing.DataManager = null;
            this.slPnlMainProcessing.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlMainProcessing.DependentPanels = null;
            this.slPnlMainProcessing.DisableDependentLoad = false;
            this.slPnlMainProcessing.EnableDelete = true;
            this.slPnlMainProcessing.EnableInsert = true;
            this.slPnlMainProcessing.EnableQuery = false;
            this.slPnlMainProcessing.EnableUpdate = true;
            this.slPnlMainProcessing.EntityName = null;
            this.slPnlMainProcessing.Location = new System.Drawing.Point(150, 118);
            this.slPnlMainProcessing.MasterPanel = null;
            this.slPnlMainProcessing.Name = "slPnlMainProcessing";
            this.slPnlMainProcessing.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlMainProcessing.Size = new System.Drawing.Size(515, 225);
            this.slPnlMainProcessing.SPName = null;
            this.slPnlMainProcessing.TabIndex = 9;
            // 
            // groupBoxMainProcess
            // 
            this.groupBoxMainProcess.Controls.Add(this.labelStatusBar);
            this.groupBoxMainProcess.Controls.Add(this.slTB_Status);
            this.groupBoxMainProcess.Controls.Add(this.slTB_Reply_For_Generation);
            this.groupBoxMainProcess.Controls.Add(this.label5);
            this.groupBoxMainProcess.Controls.Add(this.slTB_Pay_Date);
            this.groupBoxMainProcess.Controls.Add(this.groupBox1);
            this.groupBoxMainProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxMainProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxMainProcess.ForeColor = System.Drawing.Color.Maroon;
            this.groupBoxMainProcess.Location = new System.Drawing.Point(0, 0);
            this.groupBoxMainProcess.Name = "groupBoxMainProcess";
            this.groupBoxMainProcess.Size = new System.Drawing.Size(515, 225);
            this.groupBoxMainProcess.TabIndex = 0;
            this.groupBoxMainProcess.TabStop = false;
            this.groupBoxMainProcess.Text = "Payroll Generation";
            // 
            // labelStatusBar
            // 
            this.labelStatusBar.AutoSize = true;
            this.labelStatusBar.BackColor = System.Drawing.Color.White;
            this.labelStatusBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelStatusBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatusBar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelStatusBar.Location = new System.Drawing.Point(11, 180);
            this.labelStatusBar.Name = "labelStatusBar";
            this.labelStatusBar.Size = new System.Drawing.Size(72, 15);
            this.labelStatusBar.TabIndex = 21;
            this.labelStatusBar.Text = "test label 123";
            this.labelStatusBar.Visible = false;
            // 
            // slTB_Status
            // 
            this.slTB_Status.AllowSpace = true;
            this.slTB_Status.AssociatedLookUpName = "";
            this.slTB_Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_Status.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_Status.ContinuationTextBox = null;
            this.slTB_Status.CustomEnabled = true;
            this.slTB_Status.DataFieldMapping = "";
            this.slTB_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_Status.GetRecordsOnUpDownKeys = false;
            this.slTB_Status.IsDate = false;
            this.slTB_Status.Location = new System.Drawing.Point(11, 199);
            this.slTB_Status.MaxLength = 2000;
            this.slTB_Status.Name = "slTB_Status";
            this.slTB_Status.NumberFormat = "###,###,##0.00";
            this.slTB_Status.Postfix = "";
            this.slTB_Status.Prefix = "";
            this.slTB_Status.ReadOnly = true;
            this.slTB_Status.Size = new System.Drawing.Size(496, 20);
            this.slTB_Status.SkipValidation = false;
            this.slTB_Status.TabIndex = 5;
            this.slTB_Status.TabStop = false;
            this.slTB_Status.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTB_Reply_For_Generation
            // 
            this.slTB_Reply_For_Generation.AcceptsTab = true;
            this.slTB_Reply_For_Generation.AllowSpace = true;
            this.slTB_Reply_For_Generation.AssociatedLookUpName = "";
            this.slTB_Reply_For_Generation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_Reply_For_Generation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_Reply_For_Generation.ContinuationTextBox = null;
            this.slTB_Reply_For_Generation.CustomEnabled = true;
            this.slTB_Reply_For_Generation.DataFieldMapping = "";
            this.slTB_Reply_For_Generation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_Reply_For_Generation.GetRecordsOnUpDownKeys = false;
            this.slTB_Reply_For_Generation.IsDate = false;
            this.slTB_Reply_For_Generation.IsRequired = true;
            this.slTB_Reply_For_Generation.Location = new System.Drawing.Point(309, 164);
            this.slTB_Reply_For_Generation.MaxLength = 3;
            this.slTB_Reply_For_Generation.Name = "slTB_Reply_For_Generation";
            this.slTB_Reply_For_Generation.NumberFormat = "###,###,##0.00";
            this.slTB_Reply_For_Generation.Postfix = "";
            this.slTB_Reply_For_Generation.Prefix = "";
            this.slTB_Reply_For_Generation.Size = new System.Drawing.Size(57, 20);
            this.slTB_Reply_For_Generation.SkipValidation = true;
            this.slTB_Reply_For_Generation.TabIndex = 4;
            this.slTB_Reply_For_Generation.TextType = CrplControlLibrary.TextType.String;
            this.slTB_Reply_For_Generation.Validating += new System.ComponentModel.CancelEventHandler(this.slTB_Reply_For_Generation_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(72, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(228, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "[YES] Start Generation OR [NO] to Exit";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTB_Pay_Date
            // 
            this.slTB_Pay_Date.AllowSpace = true;
            this.slTB_Pay_Date.AssociatedLookUpName = "";
            this.slTB_Pay_Date.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_Pay_Date.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_Pay_Date.ContinuationTextBox = null;
            this.slTB_Pay_Date.CustomEnabled = true;
            this.slTB_Pay_Date.DataFieldMapping = "";
            this.slTB_Pay_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_Pay_Date.GetRecordsOnUpDownKeys = false;
            this.slTB_Pay_Date.IsDate = true;
            this.slTB_Pay_Date.IsRequired = true;
            this.slTB_Pay_Date.Location = new System.Drawing.Point(310, 140);
            this.slTB_Pay_Date.MaxLength = 10;
            this.slTB_Pay_Date.Name = "slTB_Pay_Date";
            this.slTB_Pay_Date.NumberFormat = "###,###,##0.00";
            this.slTB_Pay_Date.Postfix = "";
            this.slTB_Pay_Date.Prefix = "";
            this.slTB_Pay_Date.Size = new System.Drawing.Size(107, 20);
            this.slTB_Pay_Date.SkipValidation = false;
            this.slTB_Pay_Date.TabIndex = 20;
            this.slTB_Pay_Date.TabStop = false;
            this.slTB_Pay_Date.TextType = CrplControlLibrary.TextType.DateTime;
            this.toolTip1.SetToolTip(this.slTB_Pay_Date, "Please Enter Processing Date .....");
            this.slTB_Pay_Date.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.slDatePicker_Sys_Date);
            this.groupBox1.Controls.Add(this.slTB_PF_Exempt);
            this.groupBox1.Controls.Add(this.slTBMarginal_Impact);
            this.groupBox1.Controls.Add(this.slTB_Transport_Vp);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.errorProvider1.SetIconAlignment(this.groupBox1, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.groupBox1.Location = new System.Drawing.Point(40, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 115);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // slDatePicker_Sys_Date
            // 
            this.slDatePicker_Sys_Date.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slDatePicker_Sys_Date.CustomEnabled = true;
            this.slDatePicker_Sys_Date.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker_Sys_Date.DataFieldMapping = "";
            this.slDatePicker_Sys_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slDatePicker_Sys_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker_Sys_Date.HasChanges = true;
            this.slDatePicker_Sys_Date.IsRequired = true;
            this.slDatePicker_Sys_Date.Location = new System.Drawing.Point(270, 14);
            this.slDatePicker_Sys_Date.Name = "slDatePicker_Sys_Date";
            this.slDatePicker_Sys_Date.NullValue = " ";
            this.slDatePicker_Sys_Date.Size = new System.Drawing.Size(107, 21);
            this.slDatePicker_Sys_Date.TabIndex = 0;
            this.toolTip1.SetToolTip(this.slDatePicker_Sys_Date, "Please Enter Processing Date .....");
            this.slDatePicker_Sys_Date.Value = new System.DateTime(2011, 1, 5, 0, 0, 0, 0);
            // 
            // slTB_PF_Exempt
            // 
            this.slTB_PF_Exempt.AllowSpace = true;
            this.slTB_PF_Exempt.AssociatedLookUpName = "";
            this.slTB_PF_Exempt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_PF_Exempt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_PF_Exempt.ContinuationTextBox = null;
            this.slTB_PF_Exempt.CustomEnabled = true;
            this.slTB_PF_Exempt.DataFieldMapping = "";
            this.slTB_PF_Exempt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_PF_Exempt.GetRecordsOnUpDownKeys = false;
            this.slTB_PF_Exempt.IsDate = true;
            this.slTB_PF_Exempt.IsRequired = true;
            this.slTB_PF_Exempt.Location = new System.Drawing.Point(270, 85);
            this.slTB_PF_Exempt.MaxLength = 10;
            this.slTB_PF_Exempt.Name = "slTB_PF_Exempt";
            this.slTB_PF_Exempt.NumberFormat = "###,###,##";
            this.slTB_PF_Exempt.Postfix = "";
            this.slTB_PF_Exempt.Prefix = "";
            this.slTB_PF_Exempt.Size = new System.Drawing.Size(107, 20);
            this.slTB_PF_Exempt.SkipValidation = false;
            this.slTB_PF_Exempt.TabIndex = 3;
            this.slTB_PF_Exempt.Text = "100,000";
            this.slTB_PF_Exempt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTB_PF_Exempt.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // slTBMarginal_Impact
            // 
            this.slTBMarginal_Impact.AllowSpace = true;
            this.slTBMarginal_Impact.AssociatedLookUpName = "";
            this.slTBMarginal_Impact.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTBMarginal_Impact.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTBMarginal_Impact.ContinuationTextBox = null;
            this.slTBMarginal_Impact.CustomEnabled = true;
            this.slTBMarginal_Impact.DataFieldMapping = "";
            this.slTBMarginal_Impact.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTBMarginal_Impact.GetRecordsOnUpDownKeys = false;
            this.slTBMarginal_Impact.IsDate = false;
            this.slTBMarginal_Impact.IsRequired = true;
            this.slTBMarginal_Impact.Location = new System.Drawing.Point(270, 62);
            this.slTBMarginal_Impact.MaxLength = 1;
            this.slTBMarginal_Impact.Name = "slTBMarginal_Impact";
            this.slTBMarginal_Impact.NumberFormat = "###,###,##0.00";
            this.slTBMarginal_Impact.Postfix = "";
            this.slTBMarginal_Impact.Prefix = "";
            this.slTBMarginal_Impact.Size = new System.Drawing.Size(57, 20);
            this.slTBMarginal_Impact.SkipValidation = false;
            this.slTBMarginal_Impact.TabIndex = 2;
            this.slTBMarginal_Impact.Text = "Y";
            this.slTBMarginal_Impact.TextType = CrplControlLibrary.TextType.String;
            this.slTBMarginal_Impact.Validating += new System.ComponentModel.CancelEventHandler(this.slTBMarginal_Impact_Validating);
            // 
            // slTB_Transport_Vp
            // 
            this.slTB_Transport_Vp.AllowSpace = true;
            this.slTB_Transport_Vp.AssociatedLookUpName = "";
            this.slTB_Transport_Vp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_Transport_Vp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_Transport_Vp.ContinuationTextBox = null;
            this.slTB_Transport_Vp.CustomEnabled = true;
            this.slTB_Transport_Vp.DataFieldMapping = "";
            this.slTB_Transport_Vp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_Transport_Vp.GetRecordsOnUpDownKeys = false;
            this.slTB_Transport_Vp.IsDate = true;
            this.slTB_Transport_Vp.IsRequired = true;
            this.slTB_Transport_Vp.Location = new System.Drawing.Point(270, 39);
            this.slTB_Transport_Vp.MaxLength = 8;
            this.slTB_Transport_Vp.Name = "slTB_Transport_Vp";
            this.slTB_Transport_Vp.NumberFormat = "###,###,##";
            this.slTB_Transport_Vp.Postfix = "";
            this.slTB_Transport_Vp.Prefix = "";
            this.slTB_Transport_Vp.Size = new System.Drawing.Size(107, 20);
            this.slTB_Transport_Vp.SkipValidation = false;
            this.slTB_Transport_Vp.TabIndex = 1;
            this.slTB_Transport_Vp.Text = "40,000";
            this.slTB_Transport_Vp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTB_Transport_Vp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(108, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Provident Fund Exempt :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(96, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Marginal Impact [Yes/No] :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(81, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Transportation For VP && RVP :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Payroll Processing Date ( DD/MM/YYYY )  :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkResetTermFlag
            // 
            this.chkResetTermFlag.AutoSize = true;
            this.chkResetTermFlag.CustomEnabled = true;
            this.chkResetTermFlag.DataFieldMapping = "";
            this.chkResetTermFlag.Location = new System.Drawing.Point(86, 15);
            this.chkResetTermFlag.Name = "chkResetTermFlag";
            this.chkResetTermFlag.Size = new System.Drawing.Size(15, 14);
            this.chkResetTermFlag.TabIndex = 22;
            this.chkResetTermFlag.UseVisualStyleBackColor = true;
            this.chkResetTermFlag.CheckedChanged += new System.EventHandler(this.chkResetTermFlag_CheckedChanged);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(213, 70);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(382, 45);
            this.lblTitle.TabIndex = 10;
            this.lblTitle.Text = "Centralized Human Resource Information System (CHRIS) \n\n Salary Generation Proces" +
                "s";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Total Taxable Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Subs Loan Tax Amount";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTB_TotalTaxableAmount
            // 
            this.slTB_TotalTaxableAmount.AllowSpace = true;
            this.slTB_TotalTaxableAmount.AssociatedLookUpName = "";
            this.slTB_TotalTaxableAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_TotalTaxableAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_TotalTaxableAmount.ContinuationTextBox = null;
            this.slTB_TotalTaxableAmount.CustomEnabled = true;
            this.slTB_TotalTaxableAmount.DataFieldMapping = "";
            this.slTB_TotalTaxableAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_TotalTaxableAmount.GetRecordsOnUpDownKeys = false;
            this.slTB_TotalTaxableAmount.IsDate = true;
            this.slTB_TotalTaxableAmount.IsRequired = true;
            this.slTB_TotalTaxableAmount.Location = new System.Drawing.Point(3, 193);
            this.slTB_TotalTaxableAmount.MaxLength = 100;
            this.slTB_TotalTaxableAmount.Name = "slTB_TotalTaxableAmount";
            this.slTB_TotalTaxableAmount.NumberFormat = "###,###,##";
            this.slTB_TotalTaxableAmount.Postfix = "";
            this.slTB_TotalTaxableAmount.Prefix = "";
            this.slTB_TotalTaxableAmount.ReadOnly = true;
            this.slTB_TotalTaxableAmount.Size = new System.Drawing.Size(144, 20);
            this.slTB_TotalTaxableAmount.SkipValidation = false;
            this.slTB_TotalTaxableAmount.TabIndex = 13;
            this.slTB_TotalTaxableAmount.TabStop = false;
            this.slTB_TotalTaxableAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTB_TotalTaxableAmount.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // slTB_LoanTaxAmount
            // 
            this.slTB_LoanTaxAmount.AllowSpace = true;
            this.slTB_LoanTaxAmount.AssociatedLookUpName = "";
            this.slTB_LoanTaxAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_LoanTaxAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_LoanTaxAmount.ContinuationTextBox = null;
            this.slTB_LoanTaxAmount.CustomEnabled = true;
            this.slTB_LoanTaxAmount.DataFieldMapping = "";
            this.slTB_LoanTaxAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_LoanTaxAmount.GetRecordsOnUpDownKeys = false;
            this.slTB_LoanTaxAmount.IsDate = true;
            this.slTB_LoanTaxAmount.IsRequired = true;
            this.slTB_LoanTaxAmount.Location = new System.Drawing.Point(4, 255);
            this.slTB_LoanTaxAmount.MaxLength = 100;
            this.slTB_LoanTaxAmount.Name = "slTB_LoanTaxAmount";
            this.slTB_LoanTaxAmount.NumberFormat = "###,###,##";
            this.slTB_LoanTaxAmount.Postfix = "";
            this.slTB_LoanTaxAmount.Prefix = "";
            this.slTB_LoanTaxAmount.ReadOnly = true;
            this.slTB_LoanTaxAmount.Size = new System.Drawing.Size(143, 20);
            this.slTB_LoanTaxAmount.SkipValidation = false;
            this.slTB_LoanTaxAmount.TabIndex = 14;
            this.slTB_LoanTaxAmount.TabStop = false;
            this.slTB_LoanTaxAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTB_LoanTaxAmount.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // slTB_No
            // 
            this.slTB_No.AllowSpace = true;
            this.slTB_No.AssociatedLookUpName = "";
            this.slTB_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_No.ContinuationTextBox = null;
            this.slTB_No.CustomEnabled = true;
            this.slTB_No.DataFieldMapping = "";
            this.slTB_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_No.GetRecordsOnUpDownKeys = false;
            this.slTB_No.IsDate = true;
            this.slTB_No.IsRequired = true;
            this.slTB_No.Location = new System.Drawing.Point(4, 306);
            this.slTB_No.MaxLength = 30;
            this.slTB_No.Name = "slTB_No";
            this.slTB_No.NumberFormat = "###,###,##";
            this.slTB_No.Postfix = "";
            this.slTB_No.Prefix = "";
            this.slTB_No.ReadOnly = true;
            this.slTB_No.Size = new System.Drawing.Size(143, 20);
            this.slTB_No.SkipValidation = false;
            this.slTB_No.TabIndex = 15;
            this.slTB_No.TabStop = false;
            this.slTB_No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTB_No.TextType = CrplControlLibrary.TextType.String;
            // 
            // groupBoxProcessRunning
            // 
            this.groupBoxProcessRunning.Controls.Add(this.slTB_EmpGroup_Answer);
            this.groupBoxProcessRunning.Controls.Add(this.slTB_Pr_P_No);
            this.groupBoxProcessRunning.Controls.Add(this.label8);
            this.groupBoxProcessRunning.Location = new System.Drawing.Point(150, 349);
            this.groupBoxProcessRunning.Name = "groupBoxProcessRunning";
            this.groupBoxProcessRunning.Size = new System.Drawing.Size(515, 63);
            this.groupBoxProcessRunning.TabIndex = 16;
            this.groupBoxProcessRunning.TabStop = false;
            // 
            // slTB_EmpGroup_Answer
            // 
            this.slTB_EmpGroup_Answer.AllowSpace = true;
            this.slTB_EmpGroup_Answer.AssociatedLookUpName = "";
            this.slTB_EmpGroup_Answer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_EmpGroup_Answer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_EmpGroup_Answer.ContinuationTextBox = null;
            this.slTB_EmpGroup_Answer.CustomEnabled = true;
            this.slTB_EmpGroup_Answer.DataFieldMapping = "";
            this.slTB_EmpGroup_Answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_EmpGroup_Answer.GetRecordsOnUpDownKeys = false;
            this.slTB_EmpGroup_Answer.IsDate = false;
            this.slTB_EmpGroup_Answer.Location = new System.Drawing.Point(310, 38);
            this.slTB_EmpGroup_Answer.MaxLength = 1;
            this.slTB_EmpGroup_Answer.Name = "slTB_EmpGroup_Answer";
            this.slTB_EmpGroup_Answer.NumberFormat = "###,###,##0.00";
            this.slTB_EmpGroup_Answer.Postfix = "";
            this.slTB_EmpGroup_Answer.Prefix = "";
            this.slTB_EmpGroup_Answer.Size = new System.Drawing.Size(57, 20);
            this.slTB_EmpGroup_Answer.SkipValidation = false;
            this.slTB_EmpGroup_Answer.TabIndex = 5;
            this.slTB_EmpGroup_Answer.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTB_Pr_P_No
            // 
            this.slTB_Pr_P_No.AllowSpace = true;
            this.slTB_Pr_P_No.AssociatedLookUpName = "";
            this.slTB_Pr_P_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_Pr_P_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_Pr_P_No.ContinuationTextBox = null;
            this.slTB_Pr_P_No.CustomEnabled = true;
            this.slTB_Pr_P_No.DataFieldMapping = "";
            this.slTB_Pr_P_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_Pr_P_No.GetRecordsOnUpDownKeys = false;
            this.slTB_Pr_P_No.IsDate = false;
            this.slTB_Pr_P_No.Location = new System.Drawing.Point(310, 12);
            this.slTB_Pr_P_No.MaxLength = 30;
            this.slTB_Pr_P_No.Name = "slTB_Pr_P_No";
            this.slTB_Pr_P_No.NumberFormat = "###,###,##0.00";
            this.slTB_Pr_P_No.Postfix = "";
            this.slTB_Pr_P_No.Prefix = "";
            this.slTB_Pr_P_No.ReadOnly = true;
            this.slTB_Pr_P_No.Size = new System.Drawing.Size(57, 20);
            this.slTB_Pr_P_No.SkipValidation = false;
            this.slTB_Pr_P_No.TabIndex = 5;
            this.slTB_Pr_P_No.TabStop = false;
            this.slTB_Pr_P_No.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(98, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(163, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Processing Employee No.  :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxProcessCompleted
            // 
            this.groupBoxProcessCompleted.Controls.Add(this.label11);
            this.groupBoxProcessCompleted.Controls.Add(this.label10);
            this.groupBoxProcessCompleted.Controls.Add(this.slTB_FinalAnswer);
            this.groupBoxProcessCompleted.Controls.Add(this.label9);
            this.groupBoxProcessCompleted.Location = new System.Drawing.Point(150, 413);
            this.groupBoxProcessCompleted.Name = "groupBoxProcessCompleted";
            this.groupBoxProcessCompleted.Size = new System.Drawing.Size(515, 75);
            this.groupBoxProcessCompleted.TabIndex = 17;
            this.groupBoxProcessCompleted.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(179, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(156, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "PROCESS    COMPLETED";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(96, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "[NO] Exit Without Save";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // slTB_FinalAnswer
            // 
            this.slTB_FinalAnswer.AllowSpace = false;
            this.slTB_FinalAnswer.AssociatedLookUpName = "";
            this.slTB_FinalAnswer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_FinalAnswer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_FinalAnswer.ContinuationTextBox = null;
            this.slTB_FinalAnswer.CustomEnabled = true;
            this.slTB_FinalAnswer.DataFieldMapping = "";
            this.slTB_FinalAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_FinalAnswer.GetRecordsOnUpDownKeys = false;
            this.slTB_FinalAnswer.IsDate = false;
            this.slTB_FinalAnswer.Location = new System.Drawing.Point(309, 36);
            this.slTB_FinalAnswer.MaxLength = 3;
            this.slTB_FinalAnswer.Name = "slTB_FinalAnswer";
            this.slTB_FinalAnswer.NumberFormat = "###,###,##0.00";
            this.slTB_FinalAnswer.Postfix = "";
            this.slTB_FinalAnswer.Prefix = "";
            this.slTB_FinalAnswer.Size = new System.Drawing.Size(57, 20);
            this.slTB_FinalAnswer.SkipValidation = false;
            this.slTB_FinalAnswer.TabIndex = 6;
            this.slTB_FinalAnswer.TextType = CrplControlLibrary.TextType.String;
            this.slTB_FinalAnswer.Leave += new System.EventHandler(this.slTB_FinalAnswer_Leave);
            this.slTB_FinalAnswer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.slTB_FinalAnswer_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(96, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "[YES] Exit With Save";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFlagReset
            // 
            this.btnFlagReset.Location = new System.Drawing.Point(13, 353);
            this.btnFlagReset.Name = "btnFlagReset";
            this.btnFlagReset.Size = new System.Drawing.Size(75, 23);
            this.btnFlagReset.TabIndex = 18;
            this.btnFlagReset.Text = "Reset Flag";
            this.btnFlagReset.UseVisualStyleBackColor = true;
            this.btnFlagReset.Click += new System.EventHandler(this.btnFlagReset_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Term Flag";
            // 
            // pnlTermFlag
            // 
            this.pnlTermFlag.ConcurrentPanels = null;
            this.pnlTermFlag.Controls.Add(this.chkResetTermFlag);
            this.pnlTermFlag.Controls.Add(this.label12);
            this.pnlTermFlag.DataManager = null;
            this.pnlTermFlag.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTermFlag.DependentPanels = null;
            this.pnlTermFlag.DisableDependentLoad = false;
            this.pnlTermFlag.EnableDelete = true;
            this.pnlTermFlag.EnableInsert = true;
            this.pnlTermFlag.EnableQuery = false;
            this.pnlTermFlag.EnableUpdate = true;
            this.pnlTermFlag.EntityName = null;
            this.pnlTermFlag.Location = new System.Drawing.Point(4, 383);
            this.pnlTermFlag.MasterPanel = null;
            this.pnlTermFlag.Name = "pnlTermFlag";
            this.pnlTermFlag.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTermFlag.Size = new System.Drawing.Size(140, 100);
            this.pnlTermFlag.SPName = null;
            this.pnlTermFlag.TabIndex = 24;
            this.pnlTermFlag.Visible = false;
            // 
            // CHRIS_Processing_PayrollGeneration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 518);
            this.Controls.Add(this.pnlTermFlag);
            this.Controls.Add(this.btnFlagReset);
            this.Controls.Add(this.groupBoxProcessCompleted);
            this.Controls.Add(this.groupBoxProcessRunning);
            this.Controls.Add(this.slTB_No);
            this.Controls.Add(this.slTB_LoanTaxAmount);
            this.Controls.Add(this.slTB_TotalTaxableAmount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.slPnlMainProcessing);
            this.Controls.Add(this.lblTitle);
            this.Name = "CHRIS_Processing_PayrollGeneration";
            this.Text = "iCORE CHRIS Processing_Payroll_Generation";
            this.Load += new System.EventHandler(this.CHRIS_Processing_PayrollGeneration_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CHRIS_Processing_PayrollGeneration_FormClosing);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.slPnlMainProcessing, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.slTB_TotalTaxableAmount, 0);
            this.Controls.SetChildIndex(this.slTB_LoanTaxAmount, 0);
            this.Controls.SetChildIndex(this.slTB_No, 0);
            this.Controls.SetChildIndex(this.groupBoxProcessRunning, 0);
            this.Controls.SetChildIndex(this.groupBoxProcessCompleted, 0);
            this.Controls.SetChildIndex(this.btnFlagReset, 0);
            this.Controls.SetChildIndex(this.pnlTermFlag, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlMainProcessing.ResumeLayout(false);
            this.groupBoxMainProcess.ResumeLayout(false);
            this.groupBoxMainProcess.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxProcessRunning.ResumeLayout(false);
            this.groupBoxProcessRunning.PerformLayout();
            this.groupBoxProcessCompleted.ResumeLayout(false);
            this.groupBoxProcessCompleted.PerformLayout();
            this.pnlTermFlag.ResumeLayout(false);
            this.pnlTermFlag.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPnlMainProcessing;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.GroupBox groupBoxMainProcess;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox slTBMarginal_Impact;
        private CrplControlLibrary.SLTextBox slTB_Transport_Vp;
        private CrplControlLibrary.SLTextBox slTB_Pay_Date;
        private CrplControlLibrary.SLTextBox slTB_Reply_For_Generation;
        private CrplControlLibrary.SLTextBox slTB_PF_Exempt;
        private CrplControlLibrary.SLTextBox slTB_Status;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox slTB_TotalTaxableAmount;
        private CrplControlLibrary.SLTextBox slTB_LoanTaxAmount;
        private CrplControlLibrary.SLTextBox slTB_No;
        private System.Windows.Forms.GroupBox groupBoxProcessRunning;
        private CrplControlLibrary.SLTextBox slTB_Pr_P_No;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox slTB_EmpGroup_Answer;
        private System.Windows.Forms.GroupBox groupBoxProcessCompleted;
        private CrplControlLibrary.SLTextBox slTB_FinalAnswer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLDatePicker slDatePicker_Sys_Date;
        private System.Windows.Forms.Label labelStatusBar;
        private CrplControlLibrary.SLCheckBox chkResetTermFlag;
        private System.Windows.Forms.Button btnFlagReset;
        private System.Windows.Forms.Label label12;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlTermFlag;
    }
}