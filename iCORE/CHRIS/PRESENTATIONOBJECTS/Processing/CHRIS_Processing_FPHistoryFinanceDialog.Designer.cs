namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_FPHistoryFinanceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanel1 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.lblHeader = new System.Windows.Forms.Label();
            this.txtFinanceDialog = new CrplControlLibrary.SLTextBox(this.components);
            this.lblYN = new System.Windows.Forms.Label();
            this.slPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanel1
            // 
            this.slPanel1.ConcurrentPanels = null;
            this.slPanel1.Controls.Add(this.lblHeader);
            this.slPanel1.Controls.Add(this.txtFinanceDialog);
            this.slPanel1.Controls.Add(this.lblYN);
            this.slPanel1.DataManager = null;
            this.slPanel1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel1.DependentPanels = null;
            this.slPanel1.DisableDependentLoad = false;
            this.slPanel1.EnableDelete = true;
            this.slPanel1.EnableInsert = true;
            this.slPanel1.EnableUpdate = true;
            this.slPanel1.EntityName = null;
            this.slPanel1.Location = new System.Drawing.Point(34, 10);
            this.slPanel1.MasterPanel = null;
            this.slPanel1.Name = "slPanel1";
            this.slPanel1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel1.Size = new System.Drawing.Size(599, 86);
            this.slPanel1.SPName = null;
            this.slPanel1.TabIndex = 10;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(121, 9);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(305, 19);
            this.lblHeader.TabIndex = 11;
            this.lblHeader.Text = "FINANCE PROCESSING STARTED FOR ";
            // 
            // txtFinanceDialog
            // 
            this.txtFinanceDialog.AllowSpace = true;
            this.txtFinanceDialog.AssociatedLookUpName = "";
            this.txtFinanceDialog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceDialog.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceDialog.ContinuationTextBox = null;
            this.txtFinanceDialog.CustomEnabled = true;
            this.txtFinanceDialog.DataFieldMapping = "";
            this.txtFinanceDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceDialog.GetRecordsOnUpDownKeys = false;
            this.txtFinanceDialog.IsDate = false;
            this.txtFinanceDialog.Location = new System.Drawing.Point(300, 43);
            this.txtFinanceDialog.MaxLength = 3;
            this.txtFinanceDialog.Name = "txtFinanceDialog";
            this.txtFinanceDialog.NumberFormat = "###,###,##0.00";
            this.txtFinanceDialog.Postfix = "";
            this.txtFinanceDialog.Prefix = "";
            this.txtFinanceDialog.Size = new System.Drawing.Size(117, 20);
            this.txtFinanceDialog.SkipValidation = false;
            this.txtFinanceDialog.TabIndex = 10;
            this.txtFinanceDialog.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblYN
            // 
            this.lblYN.AutoSize = true;
            this.lblYN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYN.Location = new System.Drawing.Point(210, 48);
            this.lblYN.Name = "lblYN";
            this.lblYN.Size = new System.Drawing.Size(84, 15);
            this.lblYN.TabIndex = 9;
            this.lblYN.Text = " Finance No. : ";
            // 
            // CHRIS_Processing_FPHistoryFinanceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(667, 107);
            this.ControlBox = false;
            this.Controls.Add(this.slPanel1);
            this.Name = "CHRIS_Processing_FPHistoryFinanceDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.slPanel1.ResumeLayout(false);
            this.slPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel1;
        private System.Windows.Forms.Label lblHeader;
        private CrplControlLibrary.SLTextBox txtFinanceDialog;
        private System.Windows.Forms.Label lblYN;

    }
}