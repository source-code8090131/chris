namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_MonthEndProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHeading = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblLoc = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtReply = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlGenerationStatus = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlGenerationStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 324);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = true;
            this.labelHeading.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeading.Location = new System.Drawing.Point(275, 48);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(153, 45);
            this.labelHeading.TabIndex = 31;
            this.labelHeading.Text = "          PAYROLL SYSTEM \n \n               TODAY IS ";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(39, 63);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(40, 15);
            this.lblUser.TabIndex = 32;
            this.lblUser.Text = "User :";
            // 
            // lblLoc
            // 
            this.lblLoc.AutoSize = true;
            this.lblLoc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoc.Location = new System.Drawing.Point(29, 89);
            this.lblLoc.Name = "lblLoc";
            this.lblLoc.Size = new System.Drawing.Size(64, 15);
            this.lblLoc.TabIndex = 33;
            this.lblLoc.Text = "Location : ";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(264, 183);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(183, 15);
            this.lblTitle.TabIndex = 37;
            this.lblTitle.Text = "   MONTH      END      PROCESS";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(60, 23);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(230, 15);
            this.lblMessage.TabIndex = 38;
            this.lblMessage.Text = " [YES] Start Generation or  [NO]   to Exit ";
            // 
            // txtReply
            // 
            this.txtReply.AllowSpace = true;
            this.txtReply.AssociatedLookUpName = "";
            this.txtReply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReply.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReply.ContinuationTextBox = null;
            this.txtReply.CustomEnabled = true;
            this.txtReply.DataFieldMapping = "";
            this.txtReply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReply.GetRecordsOnUpDownKeys = false;
            this.txtReply.IsDate = false;
            this.txtReply.Location = new System.Drawing.Point(312, 20);
            this.txtReply.MaxLength = 3;
            this.txtReply.Name = "txtReply";
            this.txtReply.NumberFormat = "###,###,##0.00";
            this.txtReply.Postfix = "";
            this.txtReply.Prefix = "";
            this.txtReply.Size = new System.Drawing.Size(62, 20);
            this.txtReply.SkipValidation = false;
            this.txtReply.TabIndex = 39;
            this.txtReply.TextType = CrplControlLibrary.TextType.String;
            this.txtReply.TextChanged += new System.EventHandler(this.txtReply_TextChanged);
            this.txtReply.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReply_KeyPress);
            // 
            // txtCurrDate
            // 
            this.txtCurrDate.AllowSpace = true;
            this.txtCurrDate.AssociatedLookUpName = "";
            this.txtCurrDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrDate.ContinuationTextBox = null;
            this.txtCurrDate.CustomEnabled = true;
            this.txtCurrDate.DataFieldMapping = "";
            this.txtCurrDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrDate.GetRecordsOnUpDownKeys = false;
            this.txtCurrDate.IsDate = false;
            this.txtCurrDate.Location = new System.Drawing.Point(296, 107);
            this.txtCurrDate.MaxLength = 10;
            this.txtCurrDate.Name = "txtCurrDate";
            this.txtCurrDate.NumberFormat = "###,###,##0.00";
            this.txtCurrDate.Postfix = "";
            this.txtCurrDate.Prefix = "";
            this.txtCurrDate.ReadOnly = true;
            this.txtCurrDate.Size = new System.Drawing.Size(123, 20);
            this.txtCurrDate.SkipValidation = false;
            this.txtCurrDate.TabIndex = 36;
            this.txtCurrDate.TabStop = false;
            this.txtCurrDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(99, 86);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 35;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(99, 60);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 34;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlGenerationStatus
            // 
            this.pnlGenerationStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGenerationStatus.ConcurrentPanels = null;
            this.pnlGenerationStatus.Controls.Add(this.txtReply);
            this.pnlGenerationStatus.Controls.Add(this.lblMessage);
            this.pnlGenerationStatus.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlGenerationStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlGenerationStatus.DependentPanels = null;
            this.pnlGenerationStatus.DisableDependentLoad = false;
            this.pnlGenerationStatus.EnableDelete = false;
            this.pnlGenerationStatus.EnableInsert = false;
            this.pnlGenerationStatus.EnableQuery = false;
            this.pnlGenerationStatus.EnableUpdate = false;
            this.pnlGenerationStatus.EntityName = null;
            this.pnlGenerationStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlGenerationStatus.Location = new System.Drawing.Point(131, 232);
            this.pnlGenerationStatus.MasterPanel = null;
            this.pnlGenerationStatus.Name = "pnlGenerationStatus";
            this.pnlGenerationStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.pnlGenerationStatus.Size = new System.Drawing.Size(473, 64);
            this.pnlGenerationStatus.SPName = "";
            this.pnlGenerationStatus.TabIndex = 40;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(389, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 43;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Processing_MonthEndProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(669, 384);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlGenerationStatus);
            this.Controls.Add(this.txtCurrDate);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.labelHeading);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblLoc);
            this.Name = "CHRIS_Processing_MonthEndProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS  -  Month End Process";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblLoc, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.Controls.SetChildIndex(this.labelHeading, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.txtCurrDate, 0);
            this.Controls.SetChildIndex(this.pnlGenerationStatus, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlGenerationStatus.ResumeLayout(false);
            this.pnlGenerationStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtCurrDate;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label labelHeading;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblLoc;
        private System.Windows.Forms.Label lblTitle;
        private CrplControlLibrary.SLTextBox txtReply;
        private System.Windows.Forms.Label lblMessage;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlGenerationStatus;
        private System.Windows.Forms.Label lblUserName;
    }
}