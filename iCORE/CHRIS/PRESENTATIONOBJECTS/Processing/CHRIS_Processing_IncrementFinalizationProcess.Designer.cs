namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_IncrementFinalizationProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.pnlGenerationStatus = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtReply = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.labelHeading = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblLoc = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlGenerationStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 322);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(442, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 44;
            this.lblUserName.Text = "User Name  :";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(60, 23);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(230, 15);
            this.lblMessage.TabIndex = 38;
            this.lblMessage.Text = " [YES] Start Generation or  [NO]   to Exit ";
            // 
            // pnlGenerationStatus
            // 
            this.pnlGenerationStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGenerationStatus.ConcurrentPanels = null;
            this.pnlGenerationStatus.Controls.Add(this.slTextBox1);
            this.pnlGenerationStatus.Controls.Add(this.txtReply);
            this.pnlGenerationStatus.Controls.Add(this.lblMessage);
            this.pnlGenerationStatus.DataManager = "";
            this.pnlGenerationStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlGenerationStatus.DependentPanels = null;
            this.pnlGenerationStatus.DisableDependentLoad = false;
            this.pnlGenerationStatus.EnableDelete = false;
            this.pnlGenerationStatus.EnableInsert = false;
            this.pnlGenerationStatus.EnableQuery = false;
            this.pnlGenerationStatus.EnableUpdate = false;
            this.pnlGenerationStatus.EntityName = null;
            this.pnlGenerationStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlGenerationStatus.Location = new System.Drawing.Point(124, 234);
            this.pnlGenerationStatus.MasterPanel = null;
            this.pnlGenerationStatus.Name = "pnlGenerationStatus";
            this.pnlGenerationStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.pnlGenerationStatus.Size = new System.Drawing.Size(473, 64);
            this.pnlGenerationStatus.SPName = "";
            this.pnlGenerationStatus.TabIndex = 52;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(4, 39);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(0, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 40;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtReply
            // 
            this.txtReply.AllowSpace = true;
            this.txtReply.AssociatedLookUpName = "";
            this.txtReply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReply.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReply.ContinuationTextBox = null;
            this.txtReply.CustomEnabled = true;
            this.txtReply.DataFieldMapping = "";
            this.txtReply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReply.GetRecordsOnUpDownKeys = false;
            this.txtReply.IsDate = false;
            this.txtReply.Location = new System.Drawing.Point(312, 20);
            this.txtReply.MaxLength = 3;
            this.txtReply.Name = "txtReply";
            this.txtReply.NumberFormat = "###,###,##0.00";
            this.txtReply.Postfix = "";
            this.txtReply.Prefix = "";
            this.txtReply.Size = new System.Drawing.Size(62, 20);
            this.txtReply.SkipValidation = false;
            this.txtReply.TabIndex = 39;
            this.txtReply.TextType = CrplControlLibrary.TextType.String;
           
            this.txtReply.Validating += new System.ComponentModel.CancelEventHandler(this.txtReply_Validating);
            // 
            // txtCurrDate
            // 
            this.txtCurrDate.AllowSpace = true;
            this.txtCurrDate.AssociatedLookUpName = "";
            this.txtCurrDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrDate.ContinuationTextBox = null;
            this.txtCurrDate.CustomEnabled = true;
            this.txtCurrDate.DataFieldMapping = "";
            this.txtCurrDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrDate.GetRecordsOnUpDownKeys = false;
            this.txtCurrDate.IsDate = false;
            this.txtCurrDate.Location = new System.Drawing.Point(291, 129);
            this.txtCurrDate.MaxLength = 10;
            this.txtCurrDate.Name = "txtCurrDate";
            this.txtCurrDate.NumberFormat = "###,###,##0.00";
            this.txtCurrDate.Postfix = "";
            this.txtCurrDate.Prefix = "";
            this.txtCurrDate.ReadOnly = true;
            this.txtCurrDate.Size = new System.Drawing.Size(123, 20);
            this.txtCurrDate.SkipValidation = false;
            this.txtCurrDate.TabIndex = 50;
            this.txtCurrDate.TabStop = false;
            this.txtCurrDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(259, 191);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(190, 15);
            this.lblTitle.TabIndex = 51;
            this.lblTitle.Text = "  Increment  Finalization  Process";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(105, 78);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 49;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(105, 52);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 48;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = true;
            this.labelHeading.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeading.Location = new System.Drawing.Point(264, 42);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(207, 75);
            this.labelHeading.TabIndex = 45;
            this.labelHeading.Text = "          PAYROLL SYSTEM \n \n  (FOR REGULAR STAFF ONLY)      \n \n                  " +
                "TODAY IS ";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(45, 55);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(40, 15);
            this.lblUser.TabIndex = 46;
            this.lblUser.Text = "User :";
            // 
            // lblLoc
            // 
            this.lblLoc.AutoSize = true;
            this.lblLoc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoc.Location = new System.Drawing.Point(35, 81);
            this.lblLoc.Name = "lblLoc";
            this.lblLoc.Size = new System.Drawing.Size(64, 15);
            this.lblLoc.TabIndex = 47;
            this.lblLoc.Text = "Location : ";
            // 
            // CHRIS_Processing_IncrementFinalizationProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(669, 382);
            this.Controls.Add(this.pnlGenerationStatus);
            this.Controls.Add(this.txtCurrDate);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.labelHeading);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblLoc);
            this.Controls.Add(this.lblUserName);
            this.Name = "CHRIS_Processing_IncrementFinalizationProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS -  Increment  Finalization  Process";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblLoc, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.Controls.SetChildIndex(this.labelHeading, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.txtCurrDate, 0);
            this.Controls.SetChildIndex(this.pnlGenerationStatus, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlGenerationStatus.ResumeLayout(false);
            this.pnlGenerationStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblMessage;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlGenerationStatus;
        private CrplControlLibrary.SLTextBox txtReply;
        private CrplControlLibrary.SLTextBox txtCurrDate;
        private System.Windows.Forms.Label lblTitle;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label labelHeading;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblLoc;
        private CrplControlLibrary.SLTextBox slTextBox1;
    }
}