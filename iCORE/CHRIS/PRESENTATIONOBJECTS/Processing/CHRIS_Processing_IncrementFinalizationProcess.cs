using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    
    
    public partial class CHRIS_Processing_IncrementFinalizationProcess : ChrisSimpleForm
    {

        DataTable dtIFP = new DataTable();
        Result rsltIFP,rsltIFPDetail;
        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> colsNVals = new Dictionary<string, object>(); 

        
        public CHRIS_Processing_IncrementFinalizationProcess()
        {
            InitializeComponent();
            txtReply.Focus();
        }


        public CHRIS_Processing_IncrementFinalizationProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.ShowStatusBar = false;
            this.txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtUser.Text = this.userID;
            this.txtCurrDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;
            txtReply.Focus();
            txtReply.Select();
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(pnlGenerationStatus.Controls);
            }


        }




        /**********************************************************
        * In order to Proceed, Blank the table INC_PROMOTION-recordset
        * WHERE pr_current_previous = 'C'AND pr_final = 'F' +
        * MAKE term_flag from term_check NULL 
        * NET RESULT : TERM FLAG becomes 'X' OR NULL and INC_PROMOTION's
        * PR_FINAL becomes 'F'!
        *********************************************************/
        private void txtReply_Validating(object sender, CancelEventArgs e)
        {

            if (txtReply.Text.Length > 0)
            {

                if (txtReply.Text == "YES" || txtReply.Text == "NO")
                {
                    rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "Initial", colsNVals);

                    if (txtReply.Text == "YES")
                    {
                        if (rsltIFP.isSuccessful)
                        {
                            //MessageBox.Show("count of Month =   " + rsltMonth.dstResult.Tables.Count); 
                            //if (rsltIFP.dstResult.ExtendedProperties.Count > 0)  //rsltIFP.dstResult.Tables.Count > 0
                            if (rsltIFP.dstResult.Tables.Count > 0 && rsltIFP.dstResult.Tables[0].Rows.Count > 0)
                            {
                                MessageBox.Show("FINALIZATION PROCESS HAS ALREADY BEEN DONE");
                                return;
                            }
                            //else if (rsltIFP.dstResult.ExtendedProperties.Count == 0)
                            else if (rsltIFP.dstResult.Tables.Count > 0 && rsltIFP.dstResult.Tables[0].Rows.Count == 0)
                            {
                                rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "GetTermFlag", colsNVals);
                                //if (rsltIFP.dstResult.ExtendedProperties.Count > 0)
                                if (rsltIFP.dstResult.Tables.Count > 0 && rsltIFP.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "0" )
                                {
                                    MessageBox.Show("Processing Is In Progress On Other Terminal .......!");
                                    return;
                                }
                                else
                                {
                                    rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "UpdateTermFlag", colsNVals);
                                }

                                //updating INCREMENT_PROMOTION WHERE PR_INC_PER IS NULL// 
                                rsltIFP = cmnDM.Execute("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "UpdateIncPromo", colsNVals, 900000);


                                //updating INCREMENT_PROMOTION WHERE PR_INC_PER IS NOT NULL// 
                                //rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "UpdateIncPromoNotNull", colsNVals);
                                
                                
                                CHRIS_Processing_IFProcessAnswerDialog IFAnswerDialog = new CHRIS_Processing_IFProcessAnswerDialog();
                                IFAnswerDialog.ShowDialog();
                                //if (IFAnswerDialog.IsCompleted)
                                //{
                                //    this.Close();
                                //}

                            
                            }

                        }

                    }
                    else
                        this.Close();

                }
            }
           







        }

       




    }
}