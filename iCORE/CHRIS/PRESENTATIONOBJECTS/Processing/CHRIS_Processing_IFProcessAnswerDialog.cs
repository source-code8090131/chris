using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_IFProcessAnswerDialog : Form
    {

        bool isCompleted = false;
        DataTable dtIFP = new DataTable();
        Result rsltIFPDetail;
        CmnDataManager cmnDM = new CmnDataManager();
        Dictionary<string, object> colsNVals = new Dictionary<string, object>(); 



        public CHRIS_Processing_IFProcessAnswerDialog()
        {
            InitializeComponent();
        }


        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        private void txtAnswer_TextChanged(object sender, EventArgs e)
        {

           if (txtAnswer.Text != "")
           {
               rsltIFPDetail = cmnDM.Execute("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "TermFlagNull", colsNVals);
               CHRIS_Processing_IncrementFinalizationProcess IncrementFnlProcess = new CHRIS_Processing_IncrementFinalizationProcess();
               IncrementFnlProcess.Close();
               this.Close();

           }


        }




    }
}