namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.slTxtSaveRecord = new CrplControlLibrary.SLTextBox(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(93, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Do You Want To Save Above Record [Y/N]";
            // 
            // slTxtSaveRecord
            // 
            this.slTxtSaveRecord.AllowSpace = true;
            this.slTxtSaveRecord.AssociatedLookUpName = "";
            this.slTxtSaveRecord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtSaveRecord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtSaveRecord.ContinuationTextBox = null;
            this.slTxtSaveRecord.CustomEnabled = true;
            this.slTxtSaveRecord.DataFieldMapping = "";
            this.slTxtSaveRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtSaveRecord.GetRecordsOnUpDownKeys = false;
            this.slTxtSaveRecord.IsDate = false;
            this.slTxtSaveRecord.Location = new System.Drawing.Point(364, 7);
            this.slTxtSaveRecord.MaxLength = 1;
            this.slTxtSaveRecord.Name = "slTxtSaveRecord";
            this.slTxtSaveRecord.NumberFormat = "###,###,##0.00";
            this.slTxtSaveRecord.Postfix = "";
            this.slTxtSaveRecord.Prefix = "";
            this.slTxtSaveRecord.Size = new System.Drawing.Size(24, 20);
            this.slTxtSaveRecord.SkipValidation = false;
            this.slTxtSaveRecord.TabIndex = 48;
            this.slTxtSaveRecord.TextType = CrplControlLibrary.TextType.String;
            this.slTxtSaveRecord.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.slTxtSaveRecord_KeyPress);
            // 
            // CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(492, 31);
            this.ControlBox = false;
            this.Controls.Add(this.slTxtSaveRecord);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTxtSaveRecord;
    }
}