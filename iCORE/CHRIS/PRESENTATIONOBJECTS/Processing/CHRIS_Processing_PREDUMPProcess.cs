using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient; 
using Microsoft.SqlServer.Management; 
using Microsoft.SqlServer.Management.Smo; 
using Microsoft.SqlServer.Management.Common;
using System.IO;
using iCORE.XMS.DATAOBJECTS;






namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_PREDUMPProcess : ChrisSimpleForm
    {

        private static Server srvSql;
        
        public CHRIS_Processing_PREDUMPProcess()
        {
            InitializeComponent();
        }



        public CHRIS_Processing_PREDUMPProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            lblUserName.Text += "   "  + this.UserName;
            txtOption.Visible = false;
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

        }



        /// <summary>
        /// RUNNING PERFECTLY WITH LOCAL MACHINE DB
        /// NEED TO DO ADJUSTMENTS REGARDING BACKUP PLACEMENT
        /// ON DB SERVER 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult Rslt = MessageBox.Show("Are You Sure to Continue This Process......", "CHRIS", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (Rslt == DialogResult.Yes)
                {
                   
                    string destName = "PRE" + System.DateTime.Now.ToString("yyyyMMddhhmmtt") ;
                    
                    string dbName = Convert.ToString(ConfigurationManager.AppSettings["database"]);
                    //string dbName = "CMG";

                    string dbSource = Convert.ToString(ConfigurationManager.AppSettings["DataSource"]);
                    //string dbSource = "KHIPC303\\MSSQL2005";
                    
                    saveBackUpDialog.FileName = Convert.ToString(destName);
                    //openBackUpDialog.FileName = Convert.ToString(destName);
                    

                    ServerConnection srvConn = new ServerConnection(Convert.ToString(dbSource));
                    //SqlConnection srvConn = new SqlConnection();
                  
                    srvConn.LoginSecure = false;
                    srvConn.Login = "sa";
                    srvConn.Password = "sql@2005";
                    //srvConn.Password = "capslock";
                    //// Create a new SQL Server object using the connection we created
                    srvSql = new Server(srvConn);

                    if (srvSql != null)
                    {
                        if (saveBackUpDialog.ShowDialog() == DialogResult.OK)
                        {
                            //Create a new backup operation
                            Backup bkpDatabase = new Backup();
                            // Set the backup type to a database backup
                            bkpDatabase.Action = BackupActionType.Database;
                            // Set the database that we want to perform a backup on
                            bkpDatabase.Database = Convert.ToString(dbName);

                            //Set the backup device to a file
                            BackupDeviceItem bkpDevice = new BackupDeviceItem(saveBackUpDialog.FileName, DeviceType.File);
                            // Add the backup device to the backup
                            bkpDatabase.Devices.Add(bkpDevice);
                            // Perform the backup
                            bkpDatabase.SqlBackup(srvSql);

                            //Restore rstDatabase = new Restore();
                            //rstDatabase.Action = RestoreActionType.Database;
                            //rstDatabase.Database = Convert.ToString(dbName);
                            //BackupDeviceItem bkpDevice = new BackupDeviceItem(openBackUpDialog.FileName, DeviceType.File);
                            //rstDatabase.Devices.Add(bkpDevice);
                            //rstDatabase.ReplaceDatabase = true;
                            //rstDatabase.SqlRestore(srvSql);

                        }

                   }
           
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// Not Used NOW
        /// </summary>
        private void GeneratePreDumpProcess()
        {

            DataTable dtFPH = new DataTable();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_PREDUMPPROCESS", "");
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    dtFPH = rsltCode.dstResult.Tables[0];
                }
            }

            //return dtFPH;


        }

                           







    }
}