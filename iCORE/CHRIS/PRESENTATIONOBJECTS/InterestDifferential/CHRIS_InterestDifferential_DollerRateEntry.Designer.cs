namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    partial class CHRIS_InterestDifferential_DollerRateEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_InterestDifferential_DollerRateEntry));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnldetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtpTranDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpInputDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.lbtnDate = new CrplControlLibrary.LookupButton(this.components);
            this.txtStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRate = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.pnldetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(583, 0);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.txtDate);
            this.pnlHeader.Controls.Add(this.txtUser);
            this.pnlHeader.Controls.Add(this.label6);
            this.pnlHeader.Controls.Add(this.W_LOC);
            this.pnlHeader.Controls.Add(this.label7);
            this.pnlHeader.Controls.Add(this.label8);
            this.pnlHeader.Controls.Add(this.label9);
            this.pnlHeader.Controls.Add(this.label10);
            this.pnlHeader.Location = new System.Drawing.Point(12, 58);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(595, 72);
            this.pnlHeader.TabIndex = 12;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(503, 40);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 8;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(53, 14);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 6;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 18);
            this.label6.TabIndex = 0;
            this.label6.Text = "User :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(53, 40);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(86, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 7;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Loc :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(455, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 18);
            this.label8.TabIndex = 2;
            this.label8.Text = "Date :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(145, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(352, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Interest Differential";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(145, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(352, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "U.S. $ Rate Entry (Maintenance)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnldetail
            // 
            this.pnldetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnldetail.ConcurrentPanels = null;
            this.pnldetail.Controls.Add(this.dtpTranDate);
            this.pnldetail.Controls.Add(this.dtpInputDate);
            this.pnldetail.Controls.Add(this.label11);
            this.pnldetail.Controls.Add(this.lbtnDate);
            this.pnldetail.Controls.Add(this.txtStatus);
            this.pnldetail.Controls.Add(this.txtRate);
            this.pnldetail.Controls.Add(this.label5);
            this.pnldetail.Controls.Add(this.label3);
            this.pnldetail.Controls.Add(this.label2);
            this.pnldetail.Controls.Add(this.label1);
            this.pnldetail.DataManager = null;
            this.pnldetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnldetail.DependentPanels = null;
            this.pnldetail.DisableDependentLoad = true;
            this.pnldetail.EnableDelete = true;
            this.pnldetail.EnableInsert = true;
            this.pnldetail.EnableUpdate = true;
            this.pnldetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.USRateCommand";
            this.pnldetail.Location = new System.Drawing.Point(12, 136);
            this.pnldetail.MasterPanel = null;
            this.pnldetail.Name = "pnldetail";
            this.pnldetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnldetail.Size = new System.Drawing.Size(595, 166);
            this.pnldetail.SPName = "CHRIS_SP_FN_USRATE_MANAGER";
            this.pnldetail.TabIndex = 11;
            // 
            // dtpTranDate
            // 
            this.dtpTranDate.CustomEnabled = true;
            this.dtpTranDate.CustomFormat = "dd/MM/yyyy";
            this.dtpTranDate.DataFieldMapping = "FN_TRN_DATE";
            this.dtpTranDate.Enabled = false;
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Location = new System.Drawing.Point(300, 56);
            this.dtpTranDate.Name = "dtpTranDate";
            this.dtpTranDate.NullValue = " ";
            this.dtpTranDate.Size = new System.Drawing.Size(100, 20);
            this.dtpTranDate.TabIndex = 36;
            this.dtpTranDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            this.dtpTranDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpTranDate_Validating);
            // 
            // dtpInputDate
            // 
            this.dtpInputDate.CustomEnabled = true;
            this.dtpInputDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInputDate.DataFieldMapping = "FN_DATE";
            this.dtpInputDate.Enabled = false;
            this.dtpInputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInputDate.Location = new System.Drawing.Point(300, 31);
            this.dtpInputDate.Name = "dtpInputDate";
            this.dtpInputDate.NullValue = " ";
            this.dtpInputDate.Size = new System.Drawing.Size(100, 20);
            this.dtpInputDate.TabIndex = 35;
            this.dtpInputDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(337, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "[C] Close";
            // 
            // lbtnDate
            // 
            this.lbtnDate.ActionLOVExists = "CountryCodeLovExists";
            this.lbtnDate.ActionType = "FNDateLOV0";
            this.lbtnDate.ConditionalFields = "";
            this.lbtnDate.CustomEnabled = true;
            this.lbtnDate.DataFieldMapping = "";
            this.lbtnDate.DependentLovControls = "";
            this.lbtnDate.HiddenColumns = "PR_COUNTRY_AC|PR_CITIMAIL|PR_STATUS";
            this.lbtnDate.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDate.Image")));
            this.lbtnDate.LoadDependentEntities = true;
            this.lbtnDate.Location = new System.Drawing.Point(406, 55);
            this.lbtnDate.LookUpTitle = null;
            this.lbtnDate.Name = "lbtnDate";
            this.lbtnDate.Size = new System.Drawing.Size(26, 21);
            this.lbtnDate.SkipValidationOnLeave = true;
            this.lbtnDate.SPName = "CHRIS_SP_FN_USRATE_MANAGER";
            this.lbtnDate.TabIndex = 10;
            this.lbtnDate.TabStop = false;
            this.lbtnDate.Tag = "";
            this.lbtnDate.UseVisualStyleBackColor = true;
            // 
            // txtStatus
            // 
            this.txtStatus.AllowSpace = true;
            this.txtStatus.AssociatedLookUpName = "";
            this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStatus.ContinuationTextBox = null;
            this.txtStatus.CustomEnabled = true;
            this.txtStatus.DataFieldMapping = "FN_STATUS";
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.GetRecordsOnUpDownKeys = false;
            this.txtStatus.IsDate = false;
            this.txtStatus.Location = new System.Drawing.Point(300, 108);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.NumberFormat = "###,###,##0.00";
            this.txtStatus.Postfix = "";
            this.txtStatus.Prefix = "";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(31, 20);
            this.txtStatus.SkipValidation = false;
            this.txtStatus.TabIndex = 5;
            this.txtStatus.TabStop = false;
            this.txtStatus.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRate
            // 
            this.txtRate.AllowSpace = true;
            this.txtRate.AssociatedLookUpName = "";
            this.txtRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRate.ContinuationTextBox = null;
            this.txtRate.CustomEnabled = true;
            this.txtRate.DataFieldMapping = "FN_RATE";
            this.txtRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRate.GetRecordsOnUpDownKeys = false;
            this.txtRate.IsDate = false;
            this.txtRate.Location = new System.Drawing.Point(300, 82);
            this.txtRate.MaxLength = 12;
            this.txtRate.Name = "txtRate";
            this.txtRate.NumberFormat = "###,###,##0.0000";
            this.txtRate.Postfix = "";
            this.txtRate.Prefix = "";
            this.txtRate.Size = new System.Drawing.Size(131, 20);
            this.txtRate.SkipValidation = false;
            this.txtRate.TabIndex = 3;
            this.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRate.TextType = CrplControlLibrary.TextType.Amount;
            this.txtRate.Leave += new System.EventHandler(this.txtRate_Leave);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(174, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Record Status :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(174, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "US $ Rate :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(174, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Transaction Date :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(174, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input Date :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CHRIS_InterestDifferential_DollerRateEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 401);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnldetail);
            this.F3OptionText = "[F3]=Re-Open Record";
            this.F4OptionText = "[F4]=Save Record ";
            this.F6OptionText = "[F6]=Exit W/O Save";
            this.F7OptionText = "[F7]=Close Record";
            this.Name = "CHRIS_InterestDifferential_DollerRateEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS - U.S. $ Rate Entry";
            this.Controls.SetChildIndex(this.pnldetail, 0);
            this.Controls.SetChildIndex(this.pnlHeader, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnldetail.ResumeLayout(false);
            this.pnldetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox W_LOC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnldetail;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.LookupButton lbtnDate;
        private CrplControlLibrary.SLTextBox txtStatus;
        private CrplControlLibrary.SLTextBox txtRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker dtpTranDate;
        private CrplControlLibrary.SLDatePicker dtpInputDate;
    }
}