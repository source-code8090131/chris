namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    partial class CHRIS_InterestDifferential_InterestDifferentailProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.SLToDate1 = new CrplControlLibrary.SLDatePicker(this.components);
            this.SLFromDate1 = new CrplControlLibrary.SLDatePicker(this.components);
            this.SLToDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.SLFromDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtPer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRate = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.slButton2 = new CrplControlLibrary.SLButton();
            this.btnStart = new CrplControlLibrary.SLButton();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(538, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(574, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 313);
            this.panel1.Size = new System.Drawing.Size(574, 60);
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.SLToDate1);
            this.pnlDetail.Controls.Add(this.SLFromDate1);
            this.pnlDetail.Controls.Add(this.SLToDate);
            this.pnlDetail.Controls.Add(this.SLFromDate);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.txtPer);
            this.pnlDetail.Controls.Add(this.txtPersonal);
            this.pnlDetail.Controls.Add(this.txtRate);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.slButton2);
            this.pnlDetail.Controls.Add(this.btnStart);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.DataManager = null;
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "";
            this.pnlDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.55F);
            this.pnlDetail.Location = new System.Drawing.Point(12, 84);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(546, 255);
            this.pnlDetail.SPName = "CHRIS_SP_fin_Differential_INPUTS_PROC";
            this.pnlDetail.TabIndex = 42;
            // 
            // SLToDate1
            // 
            this.SLToDate1.CustomEnabled = true;
            this.SLToDate1.CustomFormat = "dd/MM/yyyy";
            this.SLToDate1.DataFieldMapping = "";
            this.SLToDate1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SLToDate1.HasChanges = true;
            this.SLToDate1.Location = new System.Drawing.Point(342, 77);
            this.SLToDate1.Name = "SLToDate1";
            this.SLToDate1.NullValue = " ";
            this.SLToDate1.Size = new System.Drawing.Size(110, 20);
            this.SLToDate1.TabIndex = 3;
            this.SLToDate1.Value = new System.DateTime(2011, 2, 28, 17, 13, 7, 520);
            // 
            // SLFromDate1
            // 
            this.SLFromDate1.CustomEnabled = true;
            this.SLFromDate1.CustomFormat = "dd/MM/yyyy";
            this.SLFromDate1.DataFieldMapping = "";
            this.SLFromDate1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SLFromDate1.HasChanges = true;
            this.SLFromDate1.Location = new System.Drawing.Point(342, 51);
            this.SLFromDate1.Name = "SLFromDate1";
            this.SLFromDate1.NullValue = " ";
            this.SLFromDate1.Size = new System.Drawing.Size(110, 20);
            this.SLFromDate1.TabIndex = 2;
            this.SLFromDate1.Value = new System.DateTime(2011, 2, 28, 17, 13, 7, 520);
            // 
            // SLToDate
            // 
            this.SLToDate.CustomEnabled = true;
            this.SLToDate.CustomFormat = "dd/MM/yyyy";
            this.SLToDate.DataFieldMapping = "";
            this.SLToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SLToDate.HasChanges = true;
            this.SLToDate.Location = new System.Drawing.Point(226, 77);
            this.SLToDate.Name = "SLToDate";
            this.SLToDate.NullValue = " ";
            this.SLToDate.Size = new System.Drawing.Size(110, 20);
            this.SLToDate.TabIndex = 1;
            this.SLToDate.Value = new System.DateTime(2011, 2, 28, 17, 13, 7, 520);
            // 
            // SLFromDate
            // 
            this.SLFromDate.CustomEnabled = true;
            this.SLFromDate.CustomFormat = "dd/MM/yyyy";
            this.SLFromDate.DataFieldMapping = "";
            this.SLFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SLFromDate.HasChanges = true;
            this.SLFromDate.Location = new System.Drawing.Point(226, 51);
            this.SLFromDate.Name = "SLFromDate";
            this.SLFromDate.NullValue = " ";
            this.SLFromDate.Size = new System.Drawing.Size(110, 20);
            this.SLFromDate.TabIndex = 0;
            this.SLFromDate.Value = new System.DateTime(2011, 2, 28, 17, 13, 7, 520);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(339, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 123;
            this.label5.Text = "DELETE CRITERIA :";
            // 
            // txtPer
            // 
            this.txtPer.AllowSpace = true;
            this.txtPer.AssociatedLookUpName = "";
            this.txtPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPer.ContinuationTextBox = null;
            this.txtPer.CustomEnabled = false;
            this.txtPer.DataFieldMapping = "";
            this.txtPer.Enabled = false;
            this.txtPer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPer.GetRecordsOnUpDownKeys = false;
            this.txtPer.IsDate = false;
            this.txtPer.Location = new System.Drawing.Point(226, 129);
            this.txtPer.MaxLength = 10;
            this.txtPer.Name = "txtPer";
            this.txtPer.NumberFormat = "###,###,##0.00";
            this.txtPer.Postfix = "";
            this.txtPer.Prefix = "";
            this.txtPer.Size = new System.Drawing.Size(110, 20);
            this.txtPer.SkipValidation = false;
            this.txtPer.TabIndex = 122;
            this.txtPer.TabStop = false;
            this.txtPer.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonal
            // 
            this.txtPersonal.AllowSpace = true;
            this.txtPersonal.AssociatedLookUpName = "";
            this.txtPersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonal.ContinuationTextBox = null;
            this.txtPersonal.CustomEnabled = true;
            this.txtPersonal.DataFieldMapping = "";
            this.txtPersonal.Enabled = false;
            this.txtPersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonal.GetRecordsOnUpDownKeys = false;
            this.txtPersonal.IsDate = false;
            this.txtPersonal.Location = new System.Drawing.Point(226, 103);
            this.txtPersonal.MaxLength = 10;
            this.txtPersonal.Name = "txtPersonal";
            this.txtPersonal.NumberFormat = "###,###,##0.00";
            this.txtPersonal.Postfix = "";
            this.txtPersonal.Prefix = "";
            this.txtPersonal.Size = new System.Drawing.Size(110, 20);
            this.txtPersonal.SkipValidation = false;
            this.txtPersonal.TabIndex = 4;
            this.txtPersonal.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtRate
            // 
            this.txtRate.AllowSpace = true;
            this.txtRate.AssociatedLookUpName = "";
            this.txtRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRate.ContinuationTextBox = null;
            this.txtRate.CustomEnabled = true;
            this.txtRate.DataFieldMapping = "";
            this.txtRate.Enabled = false;
            this.txtRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRate.GetRecordsOnUpDownKeys = false;
            this.txtRate.IsDate = false;
            this.txtRate.Location = new System.Drawing.Point(226, 156);
            this.txtRate.MaxLength = 10;
            this.txtRate.Name = "txtRate";
            this.txtRate.NumberFormat = "###,###,##0.00";
            this.txtRate.Postfix = "";
            this.txtRate.Prefix = "";
            this.txtRate.Size = new System.Drawing.Size(110, 20);
            this.txtRate.SkipValidation = false;
            this.txtRate.TabIndex = 5;
            this.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRate.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(146, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 15);
            this.label4.TabIndex = 48;
            this.label4.Text = "Enter Rate :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(20, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 15);
            this.label2.TabIndex = 46;
            this.label2.Text = "Enter Personnel #  Or Null for All :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(158, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 15);
            this.label1.TabIndex = 44;
            this.label1.Text = "To Date :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(141, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 42;
            this.label8.Text = "From Date :";
            // 
            // slButton2
            // 
            this.slButton2.ActionType = "";
            this.slButton2.Location = new System.Drawing.Point(357, 212);
            this.slButton2.Name = "slButton2";
            this.slButton2.Size = new System.Drawing.Size(95, 23);
            this.slButton2.TabIndex = 7;
            this.slButton2.Text = "Exit";
            this.slButton2.UseVisualStyleBackColor = true;
            this.slButton2.Click += new System.EventHandler(this.slButton2_Click);
            // 
            // btnStart
            // 
            this.btnStart.ActionType = "";
            this.btnStart.Location = new System.Drawing.Point(226, 212);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(98, 23);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start Process";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(150, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "Country Interest Differential Process";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(427, 12);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 44;
            this.lblUserName.Text = "User Name:";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(366, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "User Name:";
            // 
            // CHRIS_InterestDifferential_InterestDifferentailProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 373);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.pnlDetail);
            this.Name = "CHRIS_InterestDifferential_InterestDifferentailProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_InterestDifferential_InterestDifferentailProcess";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtRate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLButton slButton2;
        private CrplControlLibrary.SLButton btnStart;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtPersonal;
        private CrplControlLibrary.SLTextBox txtPer;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLDatePicker SLToDate1;
        private CrplControlLibrary.SLDatePicker SLFromDate1;
        private CrplControlLibrary.SLDatePicker SLToDate;
        private CrplControlLibrary.SLDatePicker SLFromDate;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label17;
    }
}