using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_FNREP31 : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_InterestDifferential_FNREP31()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_FNREP31(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            W_SEG.Text = "GF";

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(5);
            this.txtNumCopies.Text = "1";
            this.txtDest_Format.Text = "cond";
            this.W_SEG.Text = "GF";
            this.fdate.Value = null;
            this.tdate.Value = null;
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
       
            base.RptFileName = "FNREP31";
            if (this.txtDest_Format.Text == String.Empty)
            {
                this.txtDest_Format.Text = "PDF";
            }
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer" )
            {
                base.PrintCustomReport(this.txtNumCopies.Text );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;

                if (this.txtDestName.Text == string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";
                }

                else
                {
                    DestName = this.txtDestName.Text;
                }

                    base.ExportCustomReport(DestName,"pdf");

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                
                string RecipentName;
                if (this.txtDestName.Text == string.Empty)
                {
                    RecipentName = "";
                }

                else
                {
                    RecipentName = this.txtDestName.Text;
                }

                    base.EmailToReport(DestName, "pdf", RecipentName);

            }


        }

       

    }
}