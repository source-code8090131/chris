using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_LSOtherAuthorization : ChrisSimpleForm
    {
       #region Constructors

        public CHRIS_InterestDifferential_LSOtherAuthorization()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_LSOtherAuthorization(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            this.txtOption.Visible = false;

        }
        
        #endregion

       #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.ShowOptionTextBox = false;

            this.pnlHeader.SendToBack();
            this.lblUserName.Text = this.UserName;
            
            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.ShowStatusBar = false;
            this.FunctionConfig.F7 = Function.Cancel;
            this.FunctionConfig.F3 = Function.Modify;
            this.FunctionConfig.F4 = Function.Save;
            this.FunctionConfig.F6 = Function.Quit;

            this.CurrentPanelBlock = this.pnlDetail.Name;
            this.tbtAdd.Available = false;
            //this.tbtDelete.Available = false;
            this.operationMode = Mode.Edit;
            this.dtpMarkerDate.Value = null;
            this.txtPersNo.Select();
            this.txtPersNo.Focus();
            

        }

        private void RecordCount()
        {
            bool flag = false;
            Dictionary<string, object> param = new Dictionary<string, object>();
            
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INT_DIFF_AUTH_MANAGER", "RecordExist", param);

            if (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            {
            }
            else
            {
                MessageBox.Show("No Record For Authorization.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            this.txtPersName.Select();
            this.txtPersName.Focus();
            if (actionType == "Save")
            {
                if (!this.IsValidated())
                    return;
            }
            
            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Save")
            {
                base.Cancel();
            }
            else if (actionType == "Cancel")
            {
                dtpMarkerDate.Value = null;
            }
            else if (actionType == "List")
            {
                if (lblUserName.Text == txtMarkerName.Text)
                {
                    MessageBox.Show("Record Not Authorized ...Because Maker & Authorizer is Same", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            

            this.txtPersNo.Select();
            this.txtPersNo.Focus();
        }
        
        protected override bool Cancel()
        {
            bool flag = false;

            if (this.IsValidated())
            {
                if (this.txtMarkerName.Text == this.txtUser.Text)
                {
                    MessageBox.Show("Record Not Authorized ...Because Maker & Authorizer is Same...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                FNIntDiffCommand ent = (FNIntDiffCommand)(this.pnlDetail.CurrentBusinessEntity);
                if (ent != null)
                {
                    ent.FN_AUTH_FLAG = "A";
                    ent.FN_AUTH_NAME = this.txtUser.Text;
                    ent.FN_AUTH_DATE = DateTime.Parse(this.CurrentDate.ToShortDateString());
                    ent.FN_AUTH_TIME = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes + "" + this.CurrentDate.TimeOfDay.Seconds;
                    ent.FN_AUTH_LOC  = this.CurrentLocation;
                }

                //this.txtAuthName.Text = this.txtUser.Text;
                //this.txtAuthDate.Text = this.CurrentDate.ToShortDateString();
                //this.txtAuthTime.Text = this.CurrentDate.ToString("hhmmss");
                ////txtAuthTerm.Text = this.Term
                //this.txtAuthLocation.Text = this.CurrentLocation;
               
                this.txtAuth.Text = "A";
                //base.Save();
                ////this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
                //base.Cancel();
                this.operationMode = Mode.Edit;
            }
            return flag;
        }

        protected override bool Save()
        {
            FNIntDiffCommand ent = (FNIntDiffCommand)(this.pnlDetail.CurrentBusinessEntity);
            ent.FN_AUTH_LOC = this.CurrentLocation;
            return base.Save();
        }

        protected override bool Edit()
        {
            bool flag = false;

            this.txtAuth.Text = "";

            //base.Save();

            //base.Cancel();
            this.operationMode = Mode.Edit;

            return flag;
            //return base.Edit();
        }

        private bool IsValidated()
        {
            bool validated = true;

            if (this.txtPersNo.Text == "")
            {
                validated = false;
                this.errorProvider1.SetError(this.txtPersNo, "Select valid Personnel number.");
            }
            if (this.txtSegment.Text == "GF" || this.txtSegment.Text == "GCB")
            {

            }
            else
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtSegment, "Enter valid Segment i.e. GF or GCB.");
            }
            if (this.txtCountryCode.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            } 
            //if (this.txtAuth.Text == "")
            //{
            //    validated = false;
            //    this.errorProvider1.SetError(this.txtAuth, "Mark as Authenticat/.");
            //}

            return validated;
        }

        private bool Exists(string code)
        {
            bool flag = false;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "IsExists", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }

         /// <summary>
        /// Shortcut Implementation of oracle forms
        /// </summary>
        /// <param name="KeyEventArgs e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.F8 && this.FunctionConfig.EnableF8)
            {
                this.txtOption.Text = this.FunctionConfig.OptionLetterF8;
                DoToolbarActions(this.Controls, "List");
            }
        }
        
        #endregion

        #region Event
        private void txtSegment_Validating(object sender, CancelEventArgs e)
        {
            if (txtSegment.Text != "GF" && txtSegment.Text != "GCB")
            {
                MessageBox.Show("Valid Segment is GF & GCB .... Try Again.", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void CHRIS_InterestDifferential_LSOtherAuthorization_Shown(object sender, EventArgs e)
        {
            RecordCount();
        }

        private void CHRIS_InterestDifferential_LSOtherAuthorization_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "LOVPersonnel")
            {
                if (lblUserName.Text == txtMarkerName.Text)
                {
                    MessageBox.Show("Record Not Authorized ...Because Maker & Authorizer is Same", "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


                if (Exists(txtPersNo.Text))
                {
                    base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");
                    this.tbtDelete.Visible = true;
                    this.tbtDelete.Enabled = true;
                }
                else
                {
                    this.tbtDelete.Visible = false;
                    this.tbtDelete.Enabled = false;
                }
            }
        }
        #endregion

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            if (Exists(txtPersNo.Text))
            {
                base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");
                this.tbtDelete.Visible = true;
                this.tbtDelete.Enabled = true;
            }
            else
            {
                this.tbtDelete.Visible = false;
                this.tbtDelete.Enabled = false;
            }
        }
    }
}