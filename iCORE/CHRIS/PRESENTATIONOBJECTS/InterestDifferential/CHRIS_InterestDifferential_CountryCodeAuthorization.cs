using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_CountryCodeAuthorization : ChrisSimpleForm
    {
        #region Constructors

        public CHRIS_InterestDifferential_CountryCodeAuthorization()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_CountryCodeAuthorization(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.ShowOptionTextBox = false;

            this.pnlHeader.SendToBack();
            this.ShowStatusBar      = false;
            this.lblUserName.Text   = this.UserName;
            this.txtUser.Text       = this.userID;
            this.W_LOC.Text         = this.CurrentLocation;
            this.txtDate.Text       = this.Now().ToString("dd/MM/yyyy");

            this.FunctionConfig.F7 = Function.Cancel;
            this.FunctionConfig.F3 = Function.Modify;
            this.FunctionConfig.F4 = Function.Save;
            this.FunctionConfig.F6 = Function.Quit;

            this.CurrentPanelBlock      = this.pnlDetail.Name;

            this.tbtAdd.Available       = false;
            this.operationMode          = Mode.Edit;
            this.dtpMarkerDate.Value    = null;
            this.txtCountryCode.Select();
            this.txtCountryCode.Focus();

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            if (actionType == "List")
            {
                txtCountryName.SkipValidation = true;
            }

            //this.txtCountryName.Select();
            //this.txtCountryName.Focus();
            if (actionType == "Save")
            {
                if (!this.IsValidated())
                    return;
                else
                {
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    txtCountryCode.SkipValidation   = true;
                    txtCountryCode.IsRequired       = false;
                    base.Cancel();
                    return;
                }
            }

            if (actionType == "Cancel")
            {
                txtCountryCode.SkipValidation = true;
                txtCountryCode.IsRequired = false;
                this.dtpMarkerDate.Value = null;
                
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtCountryCode.Focus();
                txtCountryCode.Select();
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);

            
            this.txtCountryCode.Select();
            this.txtCountryCode.Focus();
        }
        protected override bool Cancel()
        {
            bool flag = false;

            if (this.IsValidated())
            {
                if (this.txtMarkerName.Text == this.txtUser.Text)
                {
                    MessageBox.Show("Record Not Authorized ...Because Maker & Authorizer is Same...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                PRCountryCommand ent = (PRCountryCommand)(this.pnlDetail.CurrentBusinessEntity);
                if (ent != null)
                {
                    ent.PR_AUTH_FLAG    = "A";
                    ent.PR_AUTH_NAME    = this.txtUser.Text;
                    ent.PR_AUTH_DATE    = DateTime.Parse(this.CurrentDate.ToShortDateString());
                    ent.PR_AUTH_TIME    = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes + "" + this.CurrentDate.TimeOfDay.Seconds;
                    ent.PR_AUTH_LOC     = this.CurrentLocation;
                }

                this.txtAuth.Text = "A";
                //base.Save();
                //base.Cancel();
                this.operationMode = Mode.Edit;
            }

            //if (this.IsValidated())
            //{
            //    this.txtAuth.Text = "A";
            //    base.Save();
            //    base.Cancel();
            //    this.operationMode = Mode.Edit;
            //}

            return flag;
            //return base.Edit();
        }
        protected override bool Edit()
        {
            bool flag = false;

            this.txtAuth.Text = null;

            //base.Save();

            //base.Cancel();
            this.operationMode = Mode.Edit;

            return flag;
            //return base.Edit();
        }

        private bool IsValidated()
        {
            bool validated = true;

            if (this.txtCountryCode.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid Country Code.");
            }
            if (this.txtCountryName.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryCode, "Select valid country code.");
            }
            if (this.txtCountryAc.Text == "")
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtCountryAc, "Mark as Authenticat/.");
            }
            //if (this.txtCitiMail.Text == "")
            //{
            //    validated = false;
            //    //this.errorProvider1.SetError(this.txtCitiMail, "Mark as Authenticat/.");
            //}


            return validated;
        }

        #endregion

        private void CHRIS_InterestDifferential_CountryCodeAuthorization_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "UnAuthCountryCodeLov")
            {
                base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");
                this.tbtDelete.Visible = true;
                this.tbtDelete.Enabled = true;
            }

        }

        private void CHRIS_InterestDifferential_CountryCodeAuthorization_Shown(object sender, EventArgs e)
        {
            this.RecordCount();
        }

        private void RecordCount()
        {
            bool flag = false;
            Dictionary<string, object> param = new Dictionary<string, object>();

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_PR_COUNTRY_AUTHORIZATION_MANAGER", "UnAuthCountryCodeLov", param);

            if (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            {
            }
            else
            {
                MessageBox.Show("No Record For Authorization.", "Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
    }
}