namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    partial class CHRIS_InterestDifferential_CountryCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_InterestDifferential_CountryCode));
            this.pnldetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtCountryName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAuthName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.lbCountryCode = new CrplControlLibrary.LookupButton(this.components);
            this.txtPrStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCitiMail = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryAc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryCode = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnldetail.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(545, 0);
            this.txtOption.Size = new System.Drawing.Size(47, 20);
            this.txtOption.TabIndex = 1;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(592, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 343);
            this.panel1.Size = new System.Drawing.Size(592, 60);
            // 
            // pnldetail
            // 
            this.pnldetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnldetail.ConcurrentPanels = null;
            this.pnldetail.Controls.Add(this.txtCountryName);
            this.pnldetail.Controls.Add(this.txtAuthName);
            this.pnldetail.Controls.Add(this.txtID);
            this.pnldetail.Controls.Add(this.label11);
            this.pnldetail.Controls.Add(this.lbCountryCode);
            this.pnldetail.Controls.Add(this.txtPrStatus);
            this.pnldetail.Controls.Add(this.txtCitiMail);
            this.pnldetail.Controls.Add(this.txtCountryAc);
            this.pnldetail.Controls.Add(this.txtCountryCode);
            this.pnldetail.Controls.Add(this.label5);
            this.pnldetail.Controls.Add(this.label4);
            this.pnldetail.Controls.Add(this.label3);
            this.pnldetail.Controls.Add(this.label2);
            this.pnldetail.Controls.Add(this.label1);
            this.pnldetail.DataManager = null;
            this.pnldetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnldetail.DependentPanels = null;
            this.pnldetail.DisableDependentLoad = true;
            this.pnldetail.EnableDelete = true;
            this.pnldetail.EnableInsert = true;
            this.pnldetail.EnableQuery = false;
            this.pnldetail.EnableUpdate = true;
            this.pnldetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PRCountryCommand";
            this.pnldetail.Location = new System.Drawing.Point(13, 130);
            this.pnldetail.MasterPanel = null;
            this.pnldetail.Name = "pnldetail";
            this.pnldetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnldetail.Size = new System.Drawing.Size(567, 203);
            this.pnldetail.SPName = "CHRIS_PR_COUNTRY_MANAGER";
            this.pnldetail.TabIndex = 0;
            // 
            // txtCountryName
            // 
            this.txtCountryName.AllowSpace = true;
            this.txtCountryName.AssociatedLookUpName = "";
            this.txtCountryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryName.ContinuationTextBox = null;
            this.txtCountryName.CustomEnabled = true;
            this.txtCountryName.DataFieldMapping = "PR_COUNTRY_DESC";
            this.txtCountryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryName.GetRecordsOnUpDownKeys = false;
            this.txtCountryName.IsDate = false;
            this.txtCountryName.IsRequired = true;
            this.txtCountryName.Location = new System.Drawing.Point(231, 59);
            this.txtCountryName.MaxLength = 12;
            this.txtCountryName.Name = "txtCountryName";
            this.txtCountryName.NumberFormat = "###,###,##0.00";
            this.txtCountryName.Postfix = "";
            this.txtCountryName.Prefix = "";
            this.txtCountryName.Size = new System.Drawing.Size(192, 20);
            this.txtCountryName.SkipValidation = false;
            this.txtCountryName.TabIndex = 1;
            this.txtCountryName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAuthName
            // 
            this.txtAuthName.AllowSpace = true;
            this.txtAuthName.AssociatedLookUpName = "";
            this.txtAuthName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAuthName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAuthName.ContinuationTextBox = null;
            this.txtAuthName.CustomEnabled = true;
            this.txtAuthName.DataFieldMapping = "FN_AUTH_NAME";
            this.txtAuthName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthName.GetRecordsOnUpDownKeys = false;
            this.txtAuthName.IsDate = false;
            this.txtAuthName.IsLookUpField = true;
            this.txtAuthName.Location = new System.Drawing.Point(518, 53);
            this.txtAuthName.MaxLength = 3;
            this.txtAuthName.Name = "txtAuthName";
            this.txtAuthName.NumberFormat = "###,###,##0.00";
            this.txtAuthName.Postfix = "";
            this.txtAuthName.Prefix = "";
            this.txtAuthName.Size = new System.Drawing.Size(36, 20);
            this.txtAuthName.SkipValidation = true;
            this.txtAuthName.TabIndex = 13;
            this.txtAuthName.TabStop = false;
            this.txtAuthName.TextType = CrplControlLibrary.TextType.String;
            this.txtAuthName.Visible = false;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.IsLookUpField = true;
            this.txtID.Location = new System.Drawing.Point(518, 27);
            this.txtID.MaxLength = 3;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(36, 20);
            this.txtID.SkipValidation = true;
            this.txtID.TabIndex = 12;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(270, 163);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "[C] Close";
            // 
            // lbCountryCode
            // 
            this.lbCountryCode.ActionLOVExists = "CountryCodeLovExists";
            this.lbCountryCode.ActionType = "CountryCodeLov";
            this.lbCountryCode.ConditionalFields = "";
            this.lbCountryCode.CustomEnabled = true;
            this.lbCountryCode.DataFieldMapping = "";
            this.lbCountryCode.DependentLovControls = "";
            this.lbCountryCode.HiddenColumns = "PR_COUNTRY_AC|PR_CITIMAIL|PR_STATUS|PR_AUTH_NAME";
            this.lbCountryCode.Image = ((System.Drawing.Image)(resources.GetObject("lbCountryCode.Image")));
            this.lbCountryCode.LoadDependentEntities = true;
            this.lbCountryCode.Location = new System.Drawing.Point(339, 30);
            this.lbCountryCode.LookUpTitle = null;
            this.lbCountryCode.Name = "lbCountryCode";
            this.lbCountryCode.Size = new System.Drawing.Size(26, 21);
            this.lbCountryCode.SkipValidationOnLeave = true;
            this.lbCountryCode.SPName = "CHRIS_SP_PR_COUNTRY_MANAGER";
            this.lbCountryCode.TabIndex = 10;
            this.lbCountryCode.TabStop = false;
            this.lbCountryCode.Tag = "";
            this.lbCountryCode.UseVisualStyleBackColor = true;
            // 
            // txtPrStatus
            // 
            this.txtPrStatus.AllowSpace = true;
            this.txtPrStatus.AssociatedLookUpName = "";
            this.txtPrStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrStatus.ContinuationTextBox = null;
            this.txtPrStatus.CustomEnabled = false;
            this.txtPrStatus.DataFieldMapping = "PR_STATUS";
            this.txtPrStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrStatus.GetRecordsOnUpDownKeys = false;
            this.txtPrStatus.IsDate = false;
            this.txtPrStatus.Location = new System.Drawing.Point(231, 159);
            this.txtPrStatus.Name = "txtPrStatus";
            this.txtPrStatus.NumberFormat = "###,###,##0.00";
            this.txtPrStatus.Postfix = "";
            this.txtPrStatus.Prefix = "";
            this.txtPrStatus.ReadOnly = true;
            this.txtPrStatus.Size = new System.Drawing.Size(31, 20);
            this.txtPrStatus.SkipValidation = false;
            this.txtPrStatus.TabIndex = 5;
            this.txtPrStatus.TabStop = false;
            this.txtPrStatus.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCitiMail
            // 
            this.txtCitiMail.AllowSpace = true;
            this.txtCitiMail.AssociatedLookUpName = "";
            this.txtCitiMail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCitiMail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCitiMail.ContinuationTextBox = null;
            this.txtCitiMail.CustomEnabled = true;
            this.txtCitiMail.DataFieldMapping = "PR_CITIMAIL";
            this.txtCitiMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCitiMail.GetRecordsOnUpDownKeys = false;
            this.txtCitiMail.IsDate = false;
            this.txtCitiMail.Location = new System.Drawing.Point(231, 117);
            this.txtCitiMail.MaxLength = 1;
            this.txtCitiMail.Name = "txtCitiMail";
            this.txtCitiMail.NumberFormat = "###,###,##0.00";
            this.txtCitiMail.Postfix = "";
            this.txtCitiMail.Prefix = "";
            this.txtCitiMail.Size = new System.Drawing.Size(31, 20);
            this.txtCitiMail.SkipValidation = true;
            this.txtCitiMail.TabIndex = 3;
            this.txtCitiMail.TextType = CrplControlLibrary.TextType.String;
            this.txtCitiMail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCitiMail_KeyPress);
            // 
            // txtCountryAc
            // 
            this.txtCountryAc.AllowSpace = true;
            this.txtCountryAc.AssociatedLookUpName = "";
            this.txtCountryAc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryAc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryAc.ContinuationTextBox = null;
            this.txtCountryAc.CustomEnabled = true;
            this.txtCountryAc.DataFieldMapping = "PR_COUNTRY_AC";
            this.txtCountryAc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryAc.GetRecordsOnUpDownKeys = false;
            this.txtCountryAc.IsDate = false;
            this.txtCountryAc.IsRequired = true;
            this.txtCountryAc.Location = new System.Drawing.Point(231, 88);
            this.txtCountryAc.MaxLength = 12;
            this.txtCountryAc.Name = "txtCountryAc";
            this.txtCountryAc.NumberFormat = "###,###,##0.00";
            this.txtCountryAc.Postfix = "";
            this.txtCountryAc.Prefix = "";
            this.txtCountryAc.Size = new System.Drawing.Size(100, 20);
            this.txtCountryAc.SkipValidation = false;
            this.txtCountryAc.TabIndex = 2;
            this.txtCountryAc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCountryCode
            // 
            this.txtCountryCode.AllowSpace = true;
            this.txtCountryCode.AssociatedLookUpName = "lbCountryCode";
            this.txtCountryCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryCode.ContinuationTextBox = null;
            this.txtCountryCode.CustomEnabled = true;
            this.txtCountryCode.DataFieldMapping = "PR_COUNTRY_CODE";
            this.txtCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryCode.GetRecordsOnUpDownKeys = false;
            this.txtCountryCode.IsDate = false;
            this.txtCountryCode.IsLookUpField = true;
            this.txtCountryCode.IsRequired = true;
            this.txtCountryCode.Location = new System.Drawing.Point(231, 30);
            this.txtCountryCode.MaxLength = 3;
            this.txtCountryCode.Name = "txtCountryCode";
            this.txtCountryCode.NumberFormat = "###,###,##0.00";
            this.txtCountryCode.Postfix = "";
            this.txtCountryCode.Prefix = "";
            this.txtCountryCode.Size = new System.Drawing.Size(100, 20);
            this.txtCountryCode.SkipValidation = true;
            this.txtCountryCode.TabIndex = 0;
            this.txtCountryCode.TextType = CrplControlLibrary.TextType.String;
            this.txtCountryCode.Validated += new System.EventHandler(this.txtCountryCode_Validated);
            this.txtCountryCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtCountryCode_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(130, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Record Status :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(139, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "CitiMail(Y/N) :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(144, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Debit A/c.# :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(132, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Country Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(135, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Country Code :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(471, 34);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 8;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(53, 40);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(86, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 7;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(53, 14);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 6;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(157, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(253, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Country Code Entry(Maintenance)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(192, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Interest Differential";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(423, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Date :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Loc :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "User :";
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.txtDate);
            this.pnlHeader.Controls.Add(this.txtUser);
            this.pnlHeader.Controls.Add(this.label6);
            this.pnlHeader.Controls.Add(this.W_LOC);
            this.pnlHeader.Controls.Add(this.label7);
            this.pnlHeader.Controls.Add(this.label8);
            this.pnlHeader.Controls.Add(this.label9);
            this.pnlHeader.Controls.Add(this.label10);
            this.pnlHeader.Location = new System.Drawing.Point(13, 56);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(567, 72);
            this.pnlHeader.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(370, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "User Name :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(437, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 11;
            // 
            // CHRIS_InterestDifferential_CountryCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 403);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnldetail);
            this.CurrentPanelBlock = "pnldetail";
            this.F3OptionText = "[F3]=Reopen Record";
            this.F4OptionText = "[F4]=Save";
            this.F6OptionText = "[F6]=Exit W/O Save";
            this.F7OptionText = "[F7]=Close Record";
            this.Name = "CHRIS_InterestDifferential_CountryCode";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRIS - Country Code";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_InterestDifferential_CountryCode_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnldetail, 0);
            this.Controls.SetChildIndex(this.pnlHeader, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnldetail.ResumeLayout(false);
            this.pnldetail.PerformLayout();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnldetail;
        private CrplControlLibrary.SLTextBox txtPrStatus;
        private CrplControlLibrary.SLTextBox txtCitiMail;
        private CrplControlLibrary.SLTextBox txtCountryAc;
        private CrplControlLibrary.SLTextBox txtCountryCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox W_LOC;
        private CrplControlLibrary.SLTextBox txtUser;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.LookupButton lbCountryCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox txtID;
        private CrplControlLibrary.SLTextBox txtAuthName;
        private CrplControlLibrary.SLTextBox txtCountryName;
    }
}