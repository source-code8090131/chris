using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using System.Threading;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_InterestDifferentailProcess : ChrisSimpleForm
    {

        #region Data Members

        Result rsltCode;
        CmnDataManager cmnDM = new CmnDataManager();
        public int MARKUP_RATE_DAYS = -1;
        #endregion

        #region Constructors
        public CHRIS_InterestDifferential_InterestDifferentailProcess()
        {
            InitializeComponent();
        }

        public CHRIS_InterestDifferential_InterestDifferentailProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();


        }
        #endregion

        #region Methods
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_TERM_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                tbtSave.Enabled = false;
                tbtAdd.Enabled = false;

                tbtDelete.Enabled = false;
                tbtList.Enabled = false;

                tbtSave.Visible = false;
                tbtAdd.Visible = false;
                tbtList.Visible = false;
                tbtDelete.Visible = false;

                txtOption.Visible = false;
                SLToDate1.Value = null;
                SLFromDate1.Value = null;
                SLToDate.Value = Convert.ToDateTime("30/12/1998");
                SLFromDate.Value = Convert.ToDateTime("01/01/1998");

                SLFromDate.Select();
                SLFromDate.Focus();
                lblUserName.Text = this.UserName;
            }
            catch(Exception exp)
            {

            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                ClearForm(pnlDetail.Controls);
                SLFromDate1.Value   = null;
                SLToDate1.Value     = null;
                SLToDate.Value      = Convert.ToDateTime("30/12/1998");
                SLFromDate.Value    = Convert.ToDateTime("01/01/1998");

                SLFromDate.Select();
                SLFromDate.Focus();
                return;
            }
        }

        private void StartProcess()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, object> paramD = new Dictionary<string, object>();
            Dictionary<string, object> paramBook = new Dictionary<string, object>();
            Dictionary<string, object> param1 = new Dictionary<string, object>();
            param1.Add("W_PNumber", txtPersonal.Text);

            DataTable dtPrNos = this.GetData("CHRIS_SP_fin_Differential_INPUTS_PROC", "", param1);

            paramD.Add("W_PNUMBER", txtPersonal.Text);
            paramD.Add("FromDate2", (SLFromDate1.Value  == null ? null : Convert.ToDateTime(SLFromDate1.Value).ToShortDateString()));
            paramD.Add("ToDate2",   (SLToDate1.Value    == null ? null : Convert.ToDateTime(SLToDate1.Value).ToShortDateString()));

            cmnDM.Execute("CHRIS_SP_FIN_DIFFERENTIAL_DELETE_LEDGER_PROC", "", paramD);


            if (dtPrNos != null)
            {
                if (dtPrNos.Rows.Count > 0)
                {
                    foreach (DataRow dtPr in dtPrNos.Rows)
                    {
                        txtPer.Text = dtPr.ItemArray[0].ToString();
                        Application.DoEvents();
                        param.Add("W_PNumber",  dtPr.ItemArray[0].ToString());
                        param.Add("FromDate2",  (SLFromDate1.Value  == null ? null  : Convert.ToDateTime(SLFromDate1.Value).ToShortDateString()));
                        param.Add("ToDate2",    (SLToDate1.Value    == null ? null  : Convert.ToDateTime(SLToDate1.Value).ToShortDateString()));
                        param.Add("FromDate",   (SLFromDate.Value   == null ? null  : Convert.ToDateTime(SLFromDate.Value).ToShortDateString()));
                        param.Add("ToDate",     (SLToDate.Value     == null ? null  : Convert.ToDateTime(SLToDate.Value).ToShortDateString()));
                        param.Add("strRate",    txtRate.Text);
                        rsltCode = cmnDM.Execute("CHRIS_SP_fin_Differential_CALCULATION_PROC","", param);
                        
                        param.Clear();
                    }
                }
            }
            MessageBox.Show("PROCESS HAS BEEN COMPLETED", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (dtPrNos.Rows.Count > 1)
            {
                txtPer.Text = "0";
            }
        }
   
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode1;
            CmnDataManager cmnDM1 = new CmnDataManager();
            rsltCode1 = cmnDM1.GetData(sp, actionType, param);

            if (rsltCode1.isSuccessful)
            {
                if (rsltCode1.dstResult.Tables.Count > 0 && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode1.dstResult.Tables[0];
                }
            }

            return dt;

        }
        
        private DataTable GetDataTable(string sp, string actionType)
        {
            DataTable dt = null;

            Result rsltCode1;
            CmnDataManager cmnDM1 = new CmnDataManager();
            rsltCode1 = cmnDM1.GetData(sp, actionType);

            if (rsltCode1.isSuccessful)
            {
                if (rsltCode1.dstResult.Tables.Count > 0 && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode1.dstResult.Tables[0];
                }
            }

            return dt;

        }
        #endregion

        #region Event Handlers
        private void btnStart_Click(object sender, EventArgs e)
        {
            this.StartProcess();
        }

        private void slButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}