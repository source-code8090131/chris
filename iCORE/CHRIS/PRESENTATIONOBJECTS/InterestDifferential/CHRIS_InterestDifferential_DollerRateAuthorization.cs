using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_DollerRateAuthorization : ChrisSimpleForm
    {
         #region Constructors

        public CHRIS_InterestDifferential_DollerRateAuthorization()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_DollerRateAuthorization(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

         #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.ShowOptionTextBox = false;

            this.pnlHeader.SendToBack();
            this.txtUser.Text = this.UserName;
            this.W_LOC.Text = this.CurrentLocation;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");

            this.FunctionConfig.F7 = Function.Cancel;
            this.FunctionConfig.F3 = Function.Modify;
            this.FunctionConfig.F4 = Function.Save;
            this.FunctionConfig.F6 = Function.Quit;

            this.CurrentPanelBlock = this.pnldetail.Name;

            this.tbtAdd.Available = false;
            this.tbtDelete.Available = false;

            this.operationMode = Mode.Edit;

            FormFill();

            //this.dtpMarkerDate.Value = null;

        }


        /// <summary>
        /// FILL THE FORM ONLOAD
        /// </summary>
        private void FormFill()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_FN_USRATE_AUTHORIZATION_MANAGER", "FirstRecord", param);

            if (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            {
                this.dtpInputDate.Value     = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["FN_Date"].ToString());
                this.dtpTranDate.Value      = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["FN_TRN_DATE"].ToString());
                this.txtRate.Text           = rsltCode.dstResult.Tables[0].Rows[0]["FN_RATE"].ToString();
                this.txtStatus.Text         = rsltCode.dstResult.Tables[0].Rows[0]["FN_STATUS"].ToString();
                this.txtAuth.Text           = rsltCode.dstResult.Tables[0].Rows[0]["AUTH_FLAG"].ToString();
                this.txtMarkerName.Text     = rsltCode.dstResult.Tables[0].Rows[0]["FN_MAKER_NAME"].ToString();
                this.dtpMarkerDate.Value    = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["FN_MAKER_DATE"].ToString());
                this.txtMarkerTime.Text     = rsltCode.dstResult.Tables[0].Rows[0]["FN_MAKER_TIME"].ToString();

            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                if (!this.IsValidated())
                    return;
            }
            if (actionType == "Cancel")
            {
                this.operationMode = Mode.Edit;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
            this.dtpInputDate.Select();
            this.dtpInputDate.Focus();

            if (actionType == "Save")
                FormFill();

            #region OLD
            //if (actionType == "Save")
            //{
            //    if (!IsValidated())
            //        return;

            //    DateTime dTime = (DateTime)(this.dtpInputDate.Value);
            //    dTime = DateTime.Parse(dTime.ToString("MM/dd/yyyy"));
            //    this.dtpInputDate.Value = dTime;

            //    dTime = (DateTime)(this.dtpTranDate.Value);
            //    dTime = DateTime.Parse(dTime.ToString("MM/dd/yyyy"));
            //    this.dtpTranDate.Value = dTime;

            //    //if (this.Exists(dTime.ToShortDateString()))
            //    //{
            //    //    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            //    //}
            //    base.DoToolbarActions(ctrlsCollection, actionType);
            //    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));

            //    return;
            //    //USRateCommand ent = (USRateCommand)(this.pnldetail.CurrentBusinessEntity);
            //    //ent.FN_DATE = DateTime.Parse(ent.FN_DATE.ToShortDateString());
            //    //ent.FN_TRN_DATE = DateTime.Parse(ent.FN_TRN_DATE.ToShortDateString());
            //}
            //if (actionType == "Cancel")
            //{
            //    base.DoToolbarActions(ctrlsCollection, actionType);
            //    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            //    this.dtpInputDate.Value = null;
            //    this.dtpTranDate.Value = null;
            //    this.txtRate.Text = "";
            //    errorProvider1.Clear();
            //    return;
            //}
            //base.DoToolbarActions(ctrlsCollection, actionType);
            #endregion
        }
        
        protected override bool Cancel()
        {
            bool flag = false;

            if (this.IsValidated())
            {
                if (this.txtMarkerName.Text == this.txtUser.Text)
                {
                    MessageBox.Show("Record Not Authorized ...Because Maker & Authorizer is Same...","Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                USRateCommand ent = (USRateCommand)(this.pnldetail.CurrentBusinessEntity);
                if (ent != null)
                {
                    ent.FN_AUTH_FLAG = "A";
                    ent.FN_AUTH_NAME = this.txtUser.Text;
                    ent.FN_AUTH_DATE = DateTime.Parse(this.CurrentDate.ToShortDateString());
                    ent.FN_AUTH_TIME = this.CurrentDate.ToString("hhmmss");
                    //ent.FN_AUTH_TERM = this.Term
                    ent.FN_AUTH_LOC = this.CurrentLocation;
                }
                this.txtAuth.Text = "A";
                this.operationMode = Mode.Edit;
                //base.Save();
                //base.Cancel();
                this.operationMode = Mode.Edit;
            }

            return flag;
            //return base.Edit();
        }
        
        protected override bool Edit()
        {
            bool flag = false;

            USRateCommand ent = (USRateCommand)(this.pnldetail.CurrentBusinessEntity);
            if (ent != null)
            {
                ent.FN_AUTH_FLAG = "";
            }

            this.txtAuth.Text = "";

            //base.Save();
            //base.Cancel();
            //this.operationMode = Mode.Edit;

            return flag;
            //return base.Edit();
        }

        private bool IsValidated()
        {
            bool validated = true;

            if (this.dtpInputDate.Value == null)
            {
                validated = false;
                this.errorProvider1.SetError(this.dtpInputDate, "Select Input Date.");
            }
            if (this.dtpTranDate.Value == null)
            {
                validated = false;
                this.errorProvider1.SetError(this.dtpTranDate, "Select Transaction Date.");
            }
            if (this.dtpInputDate.Value != null && this.dtpTranDate.Value != null)
            {
                DateTime dtInDate = (DateTime)(this.dtpInputDate.Value);
                dtInDate = DateTime.ParseExact(dtInDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                this.dtpInputDate.Value = dtInDate;

                DateTime dtTrnDate = (DateTime)(this.dtpTranDate.Value);
                dtTrnDate = DateTime.ParseExact(dtTrnDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                this.dtpTranDate.Value = dtTrnDate;
                if (dtTrnDate > dtInDate)
                {
                    validated = false;
                    this.errorProvider1.SetError(this.dtpTranDate, "Transaction Date should be less then or equal to input date.");
                }
            }

            double rate = 0;
            try
            {
                rate = double.Parse(this.txtRate.Text == "" ? "0" : this.txtRate.Text);
            }
            catch { }

            if (rate <= 0)
            {
                validated = false;
                //this.errorProvider1.SetError(this.txtRate, "Must be greater then zero.");
            }

            return validated;
        }

        #endregion

        

    }
}