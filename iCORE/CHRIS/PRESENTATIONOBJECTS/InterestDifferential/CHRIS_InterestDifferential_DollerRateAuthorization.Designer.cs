namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    partial class CHRIS_InterestDifferential_DollerRateAuthorization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_InterestDifferential_DollerRateAuthorization));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnldetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnDate = new CrplControlLibrary.LookupButton(this.components);
            this.dtpMarkerDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtMarkerTime = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMarkerName = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.txtAuth = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpTranDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpInputDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRate = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.pnldetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.txtDate);
            this.pnlHeader.Controls.Add(this.txtUser);
            this.pnlHeader.Controls.Add(this.label6);
            this.pnlHeader.Controls.Add(this.W_LOC);
            this.pnlHeader.Controls.Add(this.label7);
            this.pnlHeader.Controls.Add(this.label8);
            this.pnlHeader.Controls.Add(this.label9);
            this.pnlHeader.Controls.Add(this.label10);
            this.pnlHeader.Location = new System.Drawing.Point(12, 58);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(620, 72);
            this.pnlHeader.TabIndex = 14;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(526, 34);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 8;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(53, 14);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 6;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "User :";
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(53, 40);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(86, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 7;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Loc :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(478, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Date :";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(145, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(375, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Interest Differential";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(145, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(375, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "U.S. $ Rate Entry (Authorization)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnldetail
            // 
            this.pnldetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnldetail.ConcurrentPanels = null;
            this.pnldetail.Controls.Add(this.lbtnDate);
            this.pnldetail.Controls.Add(this.dtpMarkerDate);
            this.pnldetail.Controls.Add(this.txtMarkerTime);
            this.pnldetail.Controls.Add(this.label16);
            this.pnldetail.Controls.Add(this.label15);
            this.pnldetail.Controls.Add(this.txtMarkerName);
            this.pnldetail.Controls.Add(this.label14);
            this.pnldetail.Controls.Add(this.txtAuth);
            this.pnldetail.Controls.Add(this.label12);
            this.pnldetail.Controls.Add(this.label13);
            this.pnldetail.Controls.Add(this.dtpTranDate);
            this.pnldetail.Controls.Add(this.dtpInputDate);
            this.pnldetail.Controls.Add(this.label11);
            this.pnldetail.Controls.Add(this.txtStatus);
            this.pnldetail.Controls.Add(this.txtRate);
            this.pnldetail.Controls.Add(this.label5);
            this.pnldetail.Controls.Add(this.label3);
            this.pnldetail.Controls.Add(this.label2);
            this.pnldetail.Controls.Add(this.label1);
            this.pnldetail.DataManager = null;
            this.pnldetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnldetail.DependentPanels = null;
            this.pnldetail.DisableDependentLoad = true;
            this.pnldetail.EnableDelete = true;
            this.pnldetail.EnableInsert = true;
            this.pnldetail.EnableUpdate = true;
            this.pnldetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.USRateCommand";
            this.pnldetail.Location = new System.Drawing.Point(12, 136);
            this.pnldetail.MasterPanel = null;
            this.pnldetail.Name = "pnldetail";
            this.pnldetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnldetail.Size = new System.Drawing.Size(620, 209);
            this.pnldetail.SPName = "CHRIS_SP_FN_USRATE_AUTHORIZATION_MANAGER";
            this.pnldetail.TabIndex = 13;
            // 
            // lbtnDate
            // 
            this.lbtnDate.ActionLOVExists = "CountryCodeLovExists";
            this.lbtnDate.ActionType = "FNDateLOV0";
            this.lbtnDate.ConditionalFields = "";
            this.lbtnDate.CustomEnabled = true;
            this.lbtnDate.DataFieldMapping = "";
            this.lbtnDate.DependentLovControls = "";
            this.lbtnDate.HiddenColumns = "PR_COUNTRY_AC|PR_CITIMAIL|PR_STATUS";
            this.lbtnDate.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDate.Image")));
            this.lbtnDate.LoadDependentEntities = false;
            this.lbtnDate.Location = new System.Drawing.Point(386, 31);
            this.lbtnDate.LookUpTitle = null;
            this.lbtnDate.Name = "lbtnDate";
            this.lbtnDate.Size = new System.Drawing.Size(26, 21);
            this.lbtnDate.SkipValidationOnLeave = false;
            this.lbtnDate.SPName = "CHRIS_SP_FN_USRATE_AUTHORIZATION_MANAGER";
            this.lbtnDate.TabIndex = 46;
            this.lbtnDate.TabStop = false;
            this.lbtnDate.UseVisualStyleBackColor = true;
            // 
            // dtpMarkerDate
            // 
            this.dtpMarkerDate.CustomEnabled = false;
            this.dtpMarkerDate.CustomFormat = "dd/MM/yyyy";
            this.dtpMarkerDate.DataFieldMapping = "FN_MAKER_DATE";
            this.dtpMarkerDate.Enabled = false;
            this.dtpMarkerDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMarkerDate.Location = new System.Drawing.Point(350, 173);
            this.dtpMarkerDate.Name = "dtpMarkerDate";
            this.dtpMarkerDate.NullValue = " ";
            this.dtpMarkerDate.Size = new System.Drawing.Size(90, 20);
            this.dtpMarkerDate.TabIndex = 45;
            this.dtpMarkerDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            // 
            // txtMarkerTime
            // 
            this.txtMarkerTime.AllowSpace = true;
            this.txtMarkerTime.AssociatedLookUpName = "";
            this.txtMarkerTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkerTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkerTime.ContinuationTextBox = null;
            this.txtMarkerTime.CustomEnabled = false;
            this.txtMarkerTime.DataFieldMapping = "FN_MAKER_TIME";
            this.txtMarkerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkerTime.GetRecordsOnUpDownKeys = false;
            this.txtMarkerTime.IsDate = false;
            this.txtMarkerTime.Location = new System.Drawing.Point(542, 173);
            this.txtMarkerTime.MaxLength = 12;
            this.txtMarkerTime.Name = "txtMarkerTime";
            this.txtMarkerTime.NumberFormat = "###,###,##0.00";
            this.txtMarkerTime.Postfix = "";
            this.txtMarkerTime.Prefix = "";
            this.txtMarkerTime.ReadOnly = true;
            this.txtMarkerTime.Size = new System.Drawing.Size(56, 20);
            this.txtMarkerTime.SkipValidation = false;
            this.txtMarkerTime.TabIndex = 44;
            this.txtMarkerTime.TabStop = false;
            this.txtMarkerTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkerTime.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(446, 173);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 20);
            this.label16.TabIndex = 43;
            this.label16.Text = "Marker Time :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(249, 173);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 20);
            this.label15.TabIndex = 42;
            this.label15.Text = "Marker Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMarkerName
            // 
            this.txtMarkerName.AllowSpace = true;
            this.txtMarkerName.AssociatedLookUpName = "";
            this.txtMarkerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkerName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkerName.ContinuationTextBox = null;
            this.txtMarkerName.CustomEnabled = false;
            this.txtMarkerName.DataFieldMapping = "FN_MAKER_NAME";
            this.txtMarkerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkerName.GetRecordsOnUpDownKeys = false;
            this.txtMarkerName.IsDate = false;
            this.txtMarkerName.Location = new System.Drawing.Point(123, 173);
            this.txtMarkerName.MaxLength = 12;
            this.txtMarkerName.Name = "txtMarkerName";
            this.txtMarkerName.NumberFormat = "###,###,##0.00";
            this.txtMarkerName.Postfix = "";
            this.txtMarkerName.Prefix = "";
            this.txtMarkerName.ReadOnly = true;
            this.txtMarkerName.Size = new System.Drawing.Size(120, 20);
            this.txtMarkerName.SkipValidation = false;
            this.txtMarkerName.TabIndex = 41;
            this.txtMarkerName.TabStop = false;
            this.txtMarkerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkerName.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 173);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 20);
            this.label14.TabIndex = 40;
            this.label14.Text = "Marker Name :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAuth
            // 
            this.txtAuth.AllowSpace = true;
            this.txtAuth.AssociatedLookUpName = "";
            this.txtAuth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAuth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAuth.ContinuationTextBox = null;
            this.txtAuth.CustomEnabled = true;
            this.txtAuth.DataFieldMapping = "AUTH_FLAG";
            this.txtAuth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuth.GetRecordsOnUpDownKeys = false;
            this.txtAuth.IsDate = false;
            this.txtAuth.Location = new System.Drawing.Point(270, 134);
            this.txtAuth.MaxLength = 1;
            this.txtAuth.Name = "txtAuth";
            this.txtAuth.NumberFormat = "###,###,##0.00";
            this.txtAuth.Postfix = "";
            this.txtAuth.Prefix = "";
            this.txtAuth.ReadOnly = true;
            this.txtAuth.Size = new System.Drawing.Size(31, 20);
            this.txtAuth.SkipValidation = false;
            this.txtAuth.TabIndex = 39;
            this.txtAuth.TabStop = false;
            this.txtAuth.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(307, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(180, 20);
            this.label12.TabIndex = 38;
            this.label12.Text = "[A] Authorized Record";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(144, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 20);
            this.label13.TabIndex = 37;
            this.label13.Text = "Auth. Flag :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpTranDate
            // 
            this.dtpTranDate.CustomEnabled = false;
            this.dtpTranDate.CustomFormat = "dd/MM/yyyy";
            this.dtpTranDate.DataFieldMapping = "FN_TRN_DATE";
            this.dtpTranDate.Enabled = false;
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Location = new System.Drawing.Point(270, 56);
            this.dtpTranDate.Name = "dtpTranDate";
            this.dtpTranDate.NullValue = " ";
            this.dtpTranDate.Size = new System.Drawing.Size(100, 20);
            this.dtpTranDate.TabIndex = 36;
            this.dtpTranDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            // 
            // dtpInputDate
            // 
            this.dtpInputDate.CustomEnabled = true;
            this.dtpInputDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInputDate.DataFieldMapping = "FN_DATE";
            this.dtpInputDate.Enabled = false;
            this.dtpInputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInputDate.Location = new System.Drawing.Point(270, 31);
            this.dtpInputDate.Name = "dtpInputDate";
            this.dtpInputDate.NullValue = " ";
            this.dtpInputDate.Size = new System.Drawing.Size(100, 20);
            this.dtpInputDate.TabIndex = 35;
            this.dtpInputDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(307, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "[C] Close";
            // 
            // txtStatus
            // 
            this.txtStatus.AllowSpace = true;
            this.txtStatus.AssociatedLookUpName = "";
            this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStatus.ContinuationTextBox = null;
            this.txtStatus.CustomEnabled = true;
            this.txtStatus.DataFieldMapping = "FN_STATUS";
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.GetRecordsOnUpDownKeys = false;
            this.txtStatus.IsDate = false;
            this.txtStatus.Location = new System.Drawing.Point(270, 108);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.NumberFormat = "###,###,##0.00";
            this.txtStatus.Postfix = "";
            this.txtStatus.Prefix = "";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(31, 20);
            this.txtStatus.SkipValidation = false;
            this.txtStatus.TabIndex = 5;
            this.txtStatus.TabStop = false;
            this.txtStatus.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRate
            // 
            this.txtRate.AllowSpace = true;
            this.txtRate.AssociatedLookUpName = "";
            this.txtRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRate.ContinuationTextBox = null;
            this.txtRate.CustomEnabled = false;
            this.txtRate.DataFieldMapping = "FN_RATE";
            this.txtRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRate.GetRecordsOnUpDownKeys = false;
            this.txtRate.IsDate = false;
            this.txtRate.Location = new System.Drawing.Point(270, 82);
            this.txtRate.MaxLength = 12;
            this.txtRate.Name = "txtRate";
            this.txtRate.NumberFormat = "###,###,##0.0000";
            this.txtRate.Postfix = "";
            this.txtRate.Prefix = "";
            this.txtRate.Size = new System.Drawing.Size(131, 20);
            this.txtRate.SkipValidation = false;
            this.txtRate.TabIndex = 3;
            this.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRate.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(144, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Record Status :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(144, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "US $ Rate :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(144, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Transaction Date :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(144, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input Date :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CHRIS_InterestDifferential_DollerRateAuthorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 411);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnldetail);
            this.F3OptionText = "[F3]=Un-Auth.";
            this.F4OptionText = "[F4]Save Rcord ";
            this.F6OptionText = "[F6]=Exit W/O Save";
            this.F7OptionText = "[F7]=Authorized Record";
            this.Name = "CHRIS_InterestDifferential_DollerRateAuthorization";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS - U.S. $ Rate Entry (Authorization)";
            this.Controls.SetChildIndex(this.pnldetail, 0);
            this.Controls.SetChildIndex(this.pnlHeader, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnldetail.ResumeLayout(false);
            this.pnldetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox W_LOC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnldetail;
        private CrplControlLibrary.SLDatePicker dtpTranDate;
        private CrplControlLibrary.SLDatePicker dtpInputDate;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtStatus;
        private CrplControlLibrary.SLTextBox txtRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtAuth;
        private CrplControlLibrary.SLDatePicker dtpMarkerDate;
        private CrplControlLibrary.SLTextBox txtMarkerTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtMarkerName;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.LookupButton lbtnDate;
    }
}