using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using System.Collections;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_CountryCode : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        bool AllowClose     = true;

        #region Constructors

        public CHRIS_InterestDifferential_CountryCode()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_CountryCode(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.ShowOptionTextBox  = false;
            this.txtOption.Enabled  = false;
            this.pnlHeader.SendToBack();
            this.ShowStatusBar      = false;
            this.txtUser.Text       = this.userID;
            this.W_LOC.Text         = this.CurrentLocation;
            this.lblUserName.Text   = this.UserName;
            this.txtDate.Text       = this.Now().ToString("dd/MM/yyyy");
            this.FunctionConfig.F7  = Function.Cancel;
            this.FunctionConfig.F3  = Function.Modify;
            this.FunctionConfig.F4  = Function.Save;
            this.FunctionConfig.F6  = Function.Quit;
            this.txtCitiMail.Text   = "N";
            this.tbtAdd.Visible     = false;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            //if (actionType == "Save" && this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add && IsValidated())
            if (actionType == "Save" && IsValidated())
            {
                PRCountryCommand ent = (PRCountryCommand)(this.pnldetail.CurrentBusinessEntity);
                if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add && (!this.Exists(this.txtCountryCode.Text)))
                {
                    if (ent != null)
                    {
                        ent.PR_STATUS = null;
                        ent.PR_MAKER_NAME = this.userID;
                        ent.PR_MAKER_DATE = DateTime.Parse(this.CurrentDate.ToShortDateString());
                        ent.PR_MAKER_TIME = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes + "" + this.CurrentDate.TimeOfDay.Seconds;
                        ent.PR_MAKER_LOC = this.CurrentLocation == "" ? null : this.CurrentLocation;

                        txtCountryCode.IsRequired   = false;
                        txtCountryAc.IsRequired     = false;
                        txtCountryName.IsRequired   = false;
                        base.DoToolbarActions(ctrlsCollection, actionType);
                        base.Cancel();
                        txtCountryName.IsRequired   = true;
                        txtCountryCode.IsRequired   = true;
                        txtCountryAc.IsRequired     = true;

                        return;
                    }
                }
                else
                {
                    if (ent != null)
                    {
                        ent.PR_STATUS       = this.txtPrStatus.Text;
                        ent.PR_MAKER_NAME   = this.userID;
                        ent.PR_MAKER_DATE   = DateTime.Parse(this.CurrentDate.ToShortDateString());
                        ent.PR_MAKER_TIME   = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes;
                        ent.PR_MAKER_LOC    = this.CurrentLocation;

                        ent.PR_AUTH_NAME    = "";
                        ent.PR_AUTH_DATE    = new DateTime(1900, 01, 01);
                        ent.PR_AUTH_TERM    = "";
                        ent.PR_AUTH_TIME    = "";
                        ent.PR_AUTH_FLAG    = "";
                        this.operationMode  = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                        
                        if (txtID.Text != string.Empty)
                            this.m_intPKID  = int.Parse(txtID.Text);

                        base.DoToolbarActions(ctrlsCollection, actionType);
                        base.Cancel();
                        return;
                    }
                }
            }
            
            if (actionType == "Cancel")
            {
                this.ReadOnlyForm(false);
                this.CustomEnableForm(true);
                this.errorProvider1.Clear();
                this.txtCountryName.IsRequired  = false;
                this.txtCountryAc.IsRequired    = false;
                this.txtCountryCode.IsRequired  = false;
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.operationMode              = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                this.txtCitiMail.Text           = "N";
                this.txtCountryCode.IsRequired  = true;
                this.txtCountryName.IsRequired  = true;
                this.txtCountryAc.IsRequired    = true;
                this.txtCountryCode.IsRequired  = true;
                txtCountryCode.Select();
                txtCountryCode.Focus();
               
                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Delete")
            {
                this.errorProvider1.Clear();
                this.ReadOnlyForm(false);
                this.CustomEnableForm(true);
                this.errorProvider1.Clear();
                this.txtCountryName.IsRequired  = false;
                this.txtCountryAc.IsRequired    = false;
                this.txtCountryCode.IsRequired  = false;
                ClearForm(pnldetail.Controls);
                
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                this.txtCitiMail.Text = "N";
                txtCountryCode.Select();
                txtCountryCode.Focus();
                this.txtCountryCode.IsRequired  = true;
                this.txtCountryName.IsRequired  = true;
                this.txtCountryAc.IsRequired    = true;
                this.txtCountryCode.IsRequired  = true;
                
                
                return;
            }

            this.txtCountryName.IsRequired  = true;
            this.txtCountryAc.IsRequired    = true;
            this.txtCountryCode.IsRequired  = true;
            txtCountryCode.Select();
            txtCountryCode.Focus();
            this.txtCitiMail.Text = "N";
        }
        private bool IsValidated()
        {
            bool validated = true;

            if (txtCountryCode.Text == string.Empty)
            {
                validated = false;
            }
            else if (txtCountryAc.Text == string.Empty)
            {
                validated = false;
            }
            else if (txtCountryName.Text == string.Empty)
            {
                validated = false;
            }

            return validated;
        }

        protected override bool Quit()
        {
            if (this.txtCountryCode.Text != string.Empty)
            {
                this.AutoValidate = AutoValidate.Disable;
                base.DoToolbarActions(pnldetail.Controls, "Cancel");
                //this.ClearForm(this.pnldetail.Controls);
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
            }
            else
            {
                this.AutoValidate = AutoValidate.Disable;
                base.Quit();
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
            }
            return true;
        }

        protected override bool Cancel()
        {
            if (AllowClose)
            {
                bool flag = false;
                this.txtPrStatus.Text = "C";
                MessageBox.Show("Press [F4] To Save This Record Or [F6] To Exit W/O Save...", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return false;
        }
        protected override bool Edit()
        {
            if (AllowClose)
            {
                this.txtPrStatus.Text = "";
                MessageBox.Show("Press [F4] To Save This Record Or [F6] To Exit W/O Save...", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            else
            {
                MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            return false;
        }
        private bool Exists(string code)
        {
            bool flag = false;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_COUNTRY_CODE", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_PR_COUNTRY_MANAGER", "CountryCodeExists", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }
        #endregion

        #region Event Handlers
        
        private void txtCitiMail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'Y' || e.KeyChar == 'y' || e.KeyChar == 'N' || e.KeyChar == 'n' || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        
        private void CHRIS_InterestDifferential_CountryCode_AfterLOVSelection(DataRow selectedRow, string actionType)
        {

            if (this.txtPrStatus.Text == "C")
            {
                MessageBox.Show("This Record Is Closed...Press [F3] To Re-open or [F6] To Exit...");
                this.txtCountryCode.Focus();
            }


            //if (this.lblUserName.Text == txtAuthName.Text)
            //{
            //    MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    this.DoFunction(Function.Cancel);
            //    //this.EnableForm(true);
            //    this.CustomEnableForm(false);
            //    return;
            //}
            //else
            //{
            //    this.ReadOnlyForm(false);
            //    this.CustomEnableForm(true);
            //}

            base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");
            this.tbtDelete.Visible = true;
            this.tbtDelete.Enabled = true;
        }

        private void ReadOnlyForm(bool enable)
        {
            this.txtCountryAc.ReadOnly      = enable;
            this.txtCountryName.ReadOnly    = enable;
            this.txtCitiMail.ReadOnly       = enable;
        }

        private void CustomEnableForm(bool enable)
        {
            this.txtCountryAc.CustomEnabled     = enable;
            this.txtCountryName.CustomEnabled   = enable;
            this.txtCitiMail.CustomEnabled      = enable;

            this.txtCountryAc.Enabled   = enable;
            this.txtCountryName.Enabled = enable;
            this.txtCitiMail.Enabled    = enable;
        }

        private void txtCountryCode_Validating(object sender, CancelEventArgs e)
        {
            if (txtCountryCode.Text == string.Empty && lbCountryCode.Focused != true)
            {
                e.Cancel = true;
            }


            //if (this.FunctionConfig.CurrentOption == Function.Modify)
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_COUNTRY_CODE", this.txtCountryCode.Text);

                rslt = cmnDM.GetData("CHRIS_SP_PR_COUNTRY_MANAGER", "CountryCodeExists", param);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            txtCountryCode.Text = rslt.dstResult.Tables[0].Rows[0]["PR_Country_Code"].ToString();
                            txtCountryName.Text = rslt.dstResult.Tables[0].Rows[0]["PR_Country_Desc"].ToString();
                            txtCountryAc.Text   = rslt.dstResult.Tables[0].Rows[0]["PR_COUNTRY_AC"].ToString();
                            txtCitiMail.Text    = rslt.dstResult.Tables[0].Rows[0]["PR_CITIMAIL"].ToString();
                            txtPrStatus.Text    = rslt.dstResult.Tables[0].Rows[0]["PR_STATUS"].ToString();
                            txtID.Text          = rslt.dstResult.Tables[0].Rows[0]["ID"].ToString();
                            txtAuthName.Text    = rslt.dstResult.Tables[0].Rows[0]["PR_AUTH_NAME"].ToString();
                            //int Country_Count = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                            //if (Country_Count > 1)
                            //    MessageBox.Show("Record For This Country Code Already Exist....");
                            //e.Cancel = true;
                            
                            //if (this.lblUserName.Text == txtAuthName.Text)
                            //{
                            //    MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            //    this.DoFunction(Function.Cancel);
                            //    //this.EnableForm(true);
                            //    this.CustomEnableForm(false);
                            //    return;
                            //}
                            //else
                            //{
                            //    this.ReadOnlyForm(false);
                            //    this.CustomEnableForm(true);
                            //}

                            if (this.txtPrStatus.Text == "C" && lbCountryCode.Focused != true)
                            {
                                MessageBox.Show("This Record Is Closed...Press [F3] To Re-open or [F6] To Exit...");
                                e.Cancel = true;
                            }

                            this.tbtDelete.Visible = true;
                            this.tbtDelete.Enabled = true;
                            base.m_intPKID = int.Parse(txtID.Text != string.Empty ? txtID.Text : "0");

                        }
                    }
                }


                if (this.txtUser.Text == txtAuthName.Text && lbCountryCode.Focused != true)
                {
                    MessageBox.Show("User Not Authrized To Change This Record..", "Note", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    AllowClose = false;
                    e.Cancel = true;
                    return;
                }
                else
                {
                    AllowClose = true;
                }
            }
        }

        private void txtCountryCode_Validated(object sender, EventArgs e)
        {
            if (txtCountryCode.Text != string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_COUNTRY_CODE", txtCountryCode.Text);
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_PR_COUNTRY_MANAGER", "CountryCodeExists", param);

                if (rsltCode.isSuccessful
                          && rsltCode.dstResult.Tables.Count > 0
                          && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtID.Text = rsltCode.dstResult.Tables[0].Rows[0]["ID"].ToString();

                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                }
            }
        }

        #endregion
    }
}