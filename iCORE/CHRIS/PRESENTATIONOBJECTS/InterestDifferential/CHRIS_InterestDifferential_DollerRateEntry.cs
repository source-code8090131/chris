using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    public partial class CHRIS_InterestDifferential_DollerRateEntry : ChrisSimpleForm
    {
        #region Constructors

        public CHRIS_InterestDifferential_DollerRateEntry()
        {
            InitializeComponent();
        }
        public CHRIS_InterestDifferential_DollerRateEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }


        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.ShowOptionTextBox = false;

            this.CurrentPanelBlock = this.pnldetail.Name;

            this.pnlHeader.SendToBack();
            this.txtUser.Text = this.UserName;
            this.W_LOC.Text = this.CurrentLocation;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");

            this.FunctionConfig.F7 = Function.Cancel;
            this.FunctionConfig.F3 = Function.Modify;
            this.FunctionConfig.F4 = Function.Save;
            this.FunctionConfig.F6 = Function.Quit;

            this.dtpInputDate.Value = null;
            this.dtpTranDate.Value = null;

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save") //&& this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
            {
                if (!this.IsValidated())
                    return;

                USRateCommand ent = (USRateCommand)(this.pnldetail.CurrentBusinessEntity);
                if (ent != null)
                {
                    if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
                    {
                        ent.FN_STATUS = "";
                        ent.FN_MAKER_NAME = this.UserName;
                        ent.FN_MAKER_DATE = DateTime.Parse(this.CurrentDate.ToShortDateString());
                        ent.FN_MAKER_TIME = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes + "" + this.CurrentDate.TimeOfDay.Seconds;
                        //ent.FN_MAKER_TERM = this.Term
                        ent.FN_MAKER_LOC = this.CurrentLocation;
                    }
                    else
                    {
                        //if (this.FunctionConfig.CurrentOption == Function.Cancel) { ent.FN_STATUS = "C"; }
                        //else { ent.FN_STATUS = ""; }
                        ent.FN_STATUS = this.txtStatus.Text; 
                        ent.FN_MAKER_NAME = this.UserName;
                        ent.FN_MAKER_DATE = DateTime.Parse(this.CurrentDate.ToShortDateString());
                        ent.FN_MAKER_TIME = this.CurrentDate.TimeOfDay.Hours + "" + this.CurrentDate.TimeOfDay.Minutes;
                        //ent.FN_MAKER_TERM = this.Term
                        ent.FN_MAKER_LOC = this.CurrentLocation;

                        ent.FN_AUTH_NAME = "";
                        ent.FN_AUTH_DATE = new DateTime(1900, 01, 01);
                        ent.FN_AUTH_TERM = "";
                        ent.FN_AUTH_TIME = "";
                        ent.FN_AUTH_FLAG = "";

                    }
                }
                DateTime dTime = (DateTime)(this.dtpInputDate.Value);
                dTime = DateTime.ParseExact(dTime.ToString("MM/dd/yyyy"), "MM/dd/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                this.dtpInputDate.Value = dTime;

                dTime = (DateTime)(this.dtpTranDate.Value);
                dTime = DateTime.ParseExact(dTime.ToString("MM/dd/yyyy"), "MM/dd/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                this.dtpTranDate.Value = dTime;

                if (this.Exists(dTime.ToShortDateString()))
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                }
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));

                return;
            }
            if (actionType == "Cancel")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                this.dtpInputDate.Value = null;
                this.dtpTranDate.Value = null;
                this.txtRate.Text = "";
                errorProvider1.Clear();
                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);

        }
        protected override bool Cancel()
        {
            bool flag = false;
            //USRateCommand ent = (USRateCommand)(this.pnldetail.CurrentBusinessEntity);

            if (!this.IsValidated())
                return flag;

            this.txtStatus.Text = "C";

            return flag;
            //return base.Edit();
        }
        protected override bool Edit()
        {
            bool flag = false;

            this.txtStatus.Text = "";

            return flag;
            //return base.Edit();
        }

        private bool Exists(string code)
        {
            bool flag = false;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("FN_Trn_Date", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_FN_USRATE_MANAGER", "DollarRateExists", param);

            if (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            {
                //this.dtpInputDate.Value = rsltCode.dstResult.Tables[0].Rows[0]["FN_Date"];
                //this.dtpTranDate.Value = rsltCode.dstResult.Tables[0].Rows[0]["FN_Trn_Date"];
                //this.txtRate.Text = rsltCode.dstResult.Tables[0].Rows[0]["FN_Rate"].ToString();
                //this.txtStatus.Text = rsltCode.dstResult.Tables[0].Rows[0]["FN_Status"].ToString();
                flag = true;
            }

            return flag;
        }

        private bool IsValidated()
        {
            bool validated = true;

            if (this.dtpInputDate.Value == null)
            {
                validated = false;
                this.errorProvider1.SetError(this.dtpInputDate, "Select Input Date.");
            }
            if (this.dtpTranDate.Value == null)
            {
                validated = false;
                this.errorProvider1.SetError(this.dtpTranDate, "Select Transaction Date.");
            }
            if (this.dtpInputDate.Value != null && this.dtpTranDate.Value != null)
            {
                //DateTime dtInDate = (DateTime)(this.dtpInputDate.Value);
                //dtInDate = DateTime.Parse(dtInDate.ToString("MM/dd/yyyy"));
                //this.dtpInputDate.Value = dtInDate;

                //DateTime dtTrnDate = (DateTime)(this.dtpTranDate.Value);
                //dtTrnDate = DateTime.Parse(dtTrnDate.ToString("MM/dd/yyyy"));
                //this.dtpTranDate.Value = dtTrnDate;
                //if (dtTrnDate > dtInDate)

                if (DateTime.Compare(Convert.ToDateTime(dtpTranDate.Value), Convert.ToDateTime(dtpInputDate.Value)) > 0)
                {
                    validated = false;
                    this.errorProvider1.SetError(this.dtpTranDate, "Transaction Date should be less then or equal to input date.");
                }
            }

            double rate = 0;
            try
            {
                rate = double.Parse(this.txtRate.Text == "" ? "0" : this.txtRate.Text);
            }
            catch { }

            if (rate <= 0)
            {
                validated = false;
                this.errorProvider1.SetError(this.txtRate, "Must be greater then zero.");
            }

            return validated;
        }

        #endregion

        #region Events

        /// <summary>
        /// On Textbox Leave reset the value null if Oraginal value was 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRate_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtRate.Text != string.Empty)
                {
                    double Rate;
                    string strRate = txtRate.Text;
                    Rate = double.Parse(strRate);
                    if (Rate == 0)
                    {
                        txtRate.Text = string.Empty;
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txtRate_Leave", exp);
            }
        }

        #endregion


        /// <summary>
        /// If Transaction DAte Exist then fill the Dollar Rate
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool TranscationExists(string TransDate, string InputDate)
        {
            bool flag = false;

            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("FN_DATE", InputDate);
                param.Add("FN_Trn_Date", TransDate);
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_FN_USRATE_MANAGER", "FillDollarRateExists", param);

                if (rsltCode.isSuccessful
                          && rsltCode.dstResult.Tables.Count > 0
                          && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    //this.dtpInputDate.Value = rsltCode.dstResult.Tables[0].Rows[0]["FN_Date"];
                    //this.dtpTranDate.Value = rsltCode.dstResult.Tables[0].Rows[0]["FN_Trn_Date"];
                    this.txtRate.Text = rsltCode.dstResult.Tables[0].Rows[0]["FN_Rate"].ToString();
                    //this.txtStatus.Text = rsltCode.dstResult.Tables[0].Rows[0]["FN_Status"].ToString();
                    flag = true;
                }

                return flag;
            }
            catch(Exception exp)
            {
                LogException(this.Name, "TranscationExists", exp);
                return flag;
            }
        }

        /// <summary>
        /// Trans Date Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpTranDate_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                bool Validate = true;

                if (this.dtpInputDate.Value == null)
                {
                    Validate = false;
                    this.errorProvider1.SetError(this.dtpInputDate, "Select Input Date.");
                }
                if (this.dtpTranDate.Value == null)
                {
                    Validate = false;
                    this.errorProvider1.SetError(this.dtpTranDate, "Select Transaction Date.");
                }
                if (this.dtpInputDate.Value != null && this.dtpTranDate.Value != null)
                {
                    if (DateTime.Compare(Convert.ToDateTime(dtpTranDate.Value), Convert.ToDateTime(dtpInputDate.Value)) > 0)
                    {
                        this.errorProvider1.SetError(this.dtpTranDate, "Transaction Date should be less then or equal to input date.");
                        e.Cancel = true;
                        Validate = false;
                    }
                }

                if (Validate == true)
                {
                    DateTime dTimeTran;
                    dTimeTran = (DateTime)(this.dtpTranDate.Value);
                    dTimeTran = DateTime.ParseExact(dTimeTran.ToString("MM/dd/yyyy"), "MM/dd/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                    DateTime dTimeInput;
                    dTimeInput = (DateTime)(this.dtpInputDate.Value);
                    dTimeInput = DateTime.ParseExact(dTimeInput.ToString("MM/dd/yyyy"), "MM/dd/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat);

                    this.TranscationExists(dTimeTran.ToShortDateString(), dTimeInput.ToShortDateString());
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dtpTranDate_Validating", exp);
            }
        }
    }
}