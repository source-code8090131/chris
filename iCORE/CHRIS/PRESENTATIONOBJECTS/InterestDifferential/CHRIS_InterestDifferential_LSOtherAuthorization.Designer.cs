namespace iCORE.CHRIS.PRESENTATIONOBJECTS.InterestDifferential
{
    partial class CHRIS_InterestDifferential_LSOtherAuthorization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_InterestDifferential_LSOtherAuthorization));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersNo = new CrplControlLibrary.LookupButton(this.components);
            this.dtpMarkerDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtMarkerTime = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMarkerName = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAuth = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtSegment = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtPersName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.lbtnCountryCode = new CrplControlLibrary.LookupButton(this.components);
            this.txtRecStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRentGuarantee = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCountryCode = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 390);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.label8);
            this.pnlHeader.Controls.Add(this.txtDate);
            this.pnlHeader.Controls.Add(this.txtUser);
            this.pnlHeader.Controls.Add(this.label6);
            this.pnlHeader.Controls.Add(this.txtLocation);
            this.pnlHeader.Controls.Add(this.label7);
            this.pnlHeader.Controls.Add(this.label9);
            this.pnlHeader.Controls.Add(this.label10);
            this.pnlHeader.Location = new System.Drawing.Point(32, 72);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(576, 72);
            this.pnlHeader.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(423, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Date :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(475, 40);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 8;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(59, 14);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 6;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "User :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(59, 40);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 7;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Loc :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(146, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(319, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Interest Differential";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(146, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(319, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "I.S / Others Entry  (Authorization)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlDetail
            // 
            this.pnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.txtID);
            this.pnlDetail.Controls.Add(this.lbtnPersNo);
            this.pnlDetail.Controls.Add(this.dtpMarkerDate);
            this.pnlDetail.Controls.Add(this.txtMarkerTime);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.txtMarkerName);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.txtAuth);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtSegment);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtPersName);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.lbtnCountryCode);
            this.pnlDetail.Controls.Add(this.txtRecStatus);
            this.pnlDetail.Controls.Add(this.txtRentGuarantee);
            this.pnlDetail.Controls.Add(this.txtCountryName);
            this.pnlDetail.Controls.Add(this.txtCountryCode);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.DataManager = null;
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = true;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FNIntDiffCommand";
            this.pnlDetail.Location = new System.Drawing.Point(32, 150);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(576, 231);
            this.pnlDetail.SPName = "CHRIS_SP_FN_INT_DIFF_AUTH_MANAGER";
            this.pnlDetail.TabIndex = 11;
            this.pnlDetail.TabStop = true;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(536, 18);
            this.txtID.MaxLength = 1;
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(31, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 27;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // lbtnPersNo
            // 
            this.lbtnPersNo.ActionLOVExists = "LOVPersonnelExists";
            this.lbtnPersNo.ActionType = "LOVPersonnel";
            this.lbtnPersNo.ConditionalFields = "";
            this.lbtnPersNo.CustomEnabled = true;
            this.lbtnPersNo.DataFieldMapping = "";
            this.lbtnPersNo.DependentLovControls = "";
            this.lbtnPersNo.HiddenColumns = resources.GetString("lbtnPersNo.HiddenColumns");
            this.lbtnPersNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersNo.Image")));
            this.lbtnPersNo.LoadDependentEntities = false;
            this.lbtnPersNo.Location = new System.Drawing.Point(250, 18);
            this.lbtnPersNo.LookUpTitle = null;
            this.lbtnPersNo.Name = "lbtnPersNo";
            this.lbtnPersNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersNo.SkipValidationOnLeave = false;
            this.lbtnPersNo.SPName = "CHRIS_SP_FN_INT_DIFF_AUTH_MANAGER";
            this.lbtnPersNo.TabIndex = 26;
            this.lbtnPersNo.TabStop = false;
            this.lbtnPersNo.Tag = "";
            this.lbtnPersNo.UseVisualStyleBackColor = true;
            // 
            // dtpMarkerDate
            // 
            this.dtpMarkerDate.CustomEnabled = false;
            this.dtpMarkerDate.CustomFormat = "dd/MM/yyyy";
            this.dtpMarkerDate.DataFieldMapping = "FN_MAKER_DATE";
            this.dtpMarkerDate.Enabled = false;
            this.dtpMarkerDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMarkerDate.HasChanges = false;
            this.dtpMarkerDate.Location = new System.Drawing.Point(329, 201);
            this.dtpMarkerDate.Name = "dtpMarkerDate";
            this.dtpMarkerDate.NullValue = " ";
            this.dtpMarkerDate.Size = new System.Drawing.Size(90, 20);
            this.dtpMarkerDate.TabIndex = 25;
            this.dtpMarkerDate.Value = new System.DateTime(2010, 12, 20, 13, 43, 0, 996);
            // 
            // txtMarkerTime
            // 
            this.txtMarkerTime.AllowSpace = true;
            this.txtMarkerTime.AssociatedLookUpName = "";
            this.txtMarkerTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkerTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkerTime.ContinuationTextBox = null;
            this.txtMarkerTime.CustomEnabled = false;
            this.txtMarkerTime.DataFieldMapping = "FN_MAKER_TIME";
            this.txtMarkerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkerTime.GetRecordsOnUpDownKeys = false;
            this.txtMarkerTime.IsDate = false;
            this.txtMarkerTime.Location = new System.Drawing.Point(511, 201);
            this.txtMarkerTime.MaxLength = 12;
            this.txtMarkerTime.Name = "txtMarkerTime";
            this.txtMarkerTime.NumberFormat = "###,###,##0.00";
            this.txtMarkerTime.Postfix = "";
            this.txtMarkerTime.Prefix = "";
            this.txtMarkerTime.ReadOnly = true;
            this.txtMarkerTime.Size = new System.Drawing.Size(56, 20);
            this.txtMarkerTime.SkipValidation = false;
            this.txtMarkerTime.TabIndex = 24;
            this.txtMarkerTime.TabStop = false;
            this.txtMarkerTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkerTime.TextType = CrplControlLibrary.TextType.String;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(417, 201);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 20);
            this.label16.TabIndex = 23;
            this.label16.Text = "Maker Time :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(228, 201);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 20);
            this.label15.TabIndex = 21;
            this.label15.Text = "Maker Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMarkerName
            // 
            this.txtMarkerName.AllowSpace = true;
            this.txtMarkerName.AssociatedLookUpName = "";
            this.txtMarkerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkerName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkerName.ContinuationTextBox = null;
            this.txtMarkerName.CustomEnabled = false;
            this.txtMarkerName.DataFieldMapping = "FN_MAKER_NAME";
            this.txtMarkerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkerName.GetRecordsOnUpDownKeys = false;
            this.txtMarkerName.IsDate = false;
            this.txtMarkerName.Location = new System.Drawing.Point(102, 201);
            this.txtMarkerName.MaxLength = 12;
            this.txtMarkerName.Name = "txtMarkerName";
            this.txtMarkerName.NumberFormat = "###,###,##0.00";
            this.txtMarkerName.Postfix = "";
            this.txtMarkerName.Prefix = "";
            this.txtMarkerName.ReadOnly = true;
            this.txtMarkerName.Size = new System.Drawing.Size(120, 20);
            this.txtMarkerName.SkipValidation = false;
            this.txtMarkerName.TabIndex = 20;
            this.txtMarkerName.TabStop = false;
            this.txtMarkerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkerName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1, 201);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Maker Name :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(236, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "[A] Authorized Record";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAuth
            // 
            this.txtAuth.AllowSpace = true;
            this.txtAuth.AssociatedLookUpName = "";
            this.txtAuth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAuth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAuth.ContinuationTextBox = null;
            this.txtAuth.CustomEnabled = true;
            this.txtAuth.DataFieldMapping = "FN_AUTH_FLAG";
            this.txtAuth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuth.GetRecordsOnUpDownKeys = false;
            this.txtAuth.IsDate = false;
            this.txtAuth.Location = new System.Drawing.Point(199, 150);
            this.txtAuth.MaxLength = 1;
            this.txtAuth.Name = "txtAuth";
            this.txtAuth.NumberFormat = "###,###,##0.00";
            this.txtAuth.Postfix = "";
            this.txtAuth.Prefix = "";
            this.txtAuth.ReadOnly = true;
            this.txtAuth.Size = new System.Drawing.Size(31, 20);
            this.txtAuth.SkipValidation = false;
            this.txtAuth.TabIndex = 17;
            this.txtAuth.TabStop = false;
            this.txtAuth.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(73, 150);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 20);
            this.label13.TabIndex = 16;
            this.label13.Text = "Auth. Flag :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSegment
            // 
            this.txtSegment.AllowSpace = true;
            this.txtSegment.AssociatedLookUpName = "";
            this.txtSegment.BackColor = System.Drawing.SystemColors.Control;
            this.txtSegment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSegment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSegment.ContinuationTextBox = null;
            this.txtSegment.CustomEnabled = true;
            this.txtSegment.DataFieldMapping = "FN_SEGMENT";
            this.txtSegment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegment.GetRecordsOnUpDownKeys = false;
            this.txtSegment.IsDate = false;
            this.txtSegment.Location = new System.Drawing.Point(199, 44);
            this.txtSegment.MaxLength = 3;
            this.txtSegment.Name = "txtSegment";
            this.txtSegment.NumberFormat = "###,###,##0.00";
            this.txtSegment.Postfix = "";
            this.txtSegment.Prefix = "";
            this.txtSegment.ReadOnly = true;
            this.txtSegment.Size = new System.Drawing.Size(31, 20);
            this.txtSegment.SkipValidation = true;
            this.txtSegment.TabIndex = 3;
            this.txtSegment.TabStop = false;
            this.txtSegment.TextType = CrplControlLibrary.TextType.String;
            this.txtSegment.Validating += new System.ComponentModel.CancelEventHandler(this.txtSegment_Validating);
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(73, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 20);
            this.label12.TabIndex = 15;
            this.label12.Text = "Segment :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPersName
            // 
            this.txtPersName.AllowSpace = true;
            this.txtPersName.AssociatedLookUpName = "";
            this.txtPersName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersName.ContinuationTextBox = null;
            this.txtPersName.CustomEnabled = false;
            this.txtPersName.DataFieldMapping = "Name";
            this.txtPersName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersName.GetRecordsOnUpDownKeys = false;
            this.txtPersName.IsDate = false;
            this.txtPersName.Location = new System.Drawing.Point(282, 18);
            this.txtPersName.Name = "txtPersName";
            this.txtPersName.NumberFormat = "###,###,##0.00";
            this.txtPersName.Postfix = "";
            this.txtPersName.Prefix = "";
            this.txtPersName.ReadOnly = true;
            this.txtPersName.Size = new System.Drawing.Size(230, 20);
            this.txtPersName.SkipValidation = true;
            this.txtPersName.TabIndex = 2;
            this.txtPersName.TabStop = false;
            this.txtPersName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPersNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CausesValidation = false;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsLookUpField = true;
            this.txtPersNo.Location = new System.Drawing.Point(199, 18);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(45, 20);
            this.txtPersNo.SkipValidation = true;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press [F9] for List of Values");
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(236, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 20);
            this.label11.TabIndex = 11;
            this.label11.Text = "[C] Close";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbtnCountryCode
            // 
            this.lbtnCountryCode.ActionLOVExists = "LOVCountry_EXISTS";
            this.lbtnCountryCode.ActionType = "LOVCountry";
            this.lbtnCountryCode.ConditionalFields = "";
            this.lbtnCountryCode.CustomEnabled = true;
            this.lbtnCountryCode.DataFieldMapping = "";
            this.lbtnCountryCode.DependentLovControls = "";
            this.lbtnCountryCode.HiddenColumns = "PR_COUNTRY_AC|PR_CITIMAIL|PR_STATUS";
            this.lbtnCountryCode.Image = ((System.Drawing.Image)(resources.GetObject("lbtnCountryCode.Image")));
            this.lbtnCountryCode.LoadDependentEntities = false;
            this.lbtnCountryCode.Location = new System.Drawing.Point(250, 70);
            this.lbtnCountryCode.LookUpTitle = null;
            this.lbtnCountryCode.Name = "lbtnCountryCode";
            this.lbtnCountryCode.Size = new System.Drawing.Size(26, 21);
            this.lbtnCountryCode.SkipValidationOnLeave = false;
            this.lbtnCountryCode.SPName = "CHRIS_SP_FN_INT_DIFF_MANAGER";
            this.lbtnCountryCode.TabIndex = 5;
            this.lbtnCountryCode.TabStop = false;
            this.lbtnCountryCode.Tag = "";
            this.lbtnCountryCode.UseVisualStyleBackColor = true;
            this.lbtnCountryCode.Visible = false;
            // 
            // txtRecStatus
            // 
            this.txtRecStatus.AllowSpace = true;
            this.txtRecStatus.AssociatedLookUpName = "";
            this.txtRecStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRecStatus.ContinuationTextBox = null;
            this.txtRecStatus.CustomEnabled = true;
            this.txtRecStatus.DataFieldMapping = "FN_STATUS";
            this.txtRecStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecStatus.GetRecordsOnUpDownKeys = false;
            this.txtRecStatus.IsDate = false;
            this.txtRecStatus.Location = new System.Drawing.Point(199, 122);
            this.txtRecStatus.MaxLength = 1;
            this.txtRecStatus.Name = "txtRecStatus";
            this.txtRecStatus.NumberFormat = "###,###,##0.00";
            this.txtRecStatus.Postfix = "";
            this.txtRecStatus.Prefix = "";
            this.txtRecStatus.ReadOnly = true;
            this.txtRecStatus.Size = new System.Drawing.Size(31, 20);
            this.txtRecStatus.SkipValidation = false;
            this.txtRecStatus.TabIndex = 8;
            this.txtRecStatus.TabStop = false;
            this.txtRecStatus.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRentGuarantee
            // 
            this.txtRentGuarantee.AllowSpace = true;
            this.txtRentGuarantee.AssociatedLookUpName = "";
            this.txtRentGuarantee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRentGuarantee.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRentGuarantee.ContinuationTextBox = null;
            this.txtRentGuarantee.CustomEnabled = true;
            this.txtRentGuarantee.DataFieldMapping = "FN_RENTAL_GUARANT";
            this.txtRentGuarantee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRentGuarantee.GetRecordsOnUpDownKeys = false;
            this.txtRentGuarantee.IsDate = false;
            this.txtRentGuarantee.Location = new System.Drawing.Point(199, 96);
            this.txtRentGuarantee.MaxLength = 12;
            this.txtRentGuarantee.Name = "txtRentGuarantee";
            this.txtRentGuarantee.NumberFormat = "###,###,##0.00";
            this.txtRentGuarantee.Postfix = "";
            this.txtRentGuarantee.Prefix = "";
            this.txtRentGuarantee.ReadOnly = true;
            this.txtRentGuarantee.Size = new System.Drawing.Size(100, 20);
            this.txtRentGuarantee.SkipValidation = false;
            this.txtRentGuarantee.TabIndex = 7;
            this.txtRentGuarantee.TabStop = false;
            this.txtRentGuarantee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRentGuarantee.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtCountryName
            // 
            this.txtCountryName.AllowSpace = true;
            this.txtCountryName.AssociatedLookUpName = "";
            this.txtCountryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryName.ContinuationTextBox = null;
            this.txtCountryName.CustomEnabled = false;
            this.txtCountryName.DataFieldMapping = "FN_COUNTRY_DESC";
            this.txtCountryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryName.GetRecordsOnUpDownKeys = false;
            this.txtCountryName.IsDate = false;
            this.txtCountryName.Location = new System.Drawing.Point(282, 70);
            this.txtCountryName.Name = "txtCountryName";
            this.txtCountryName.NumberFormat = "###,###,##0.00";
            this.txtCountryName.Postfix = "";
            this.txtCountryName.Prefix = "";
            this.txtCountryName.ReadOnly = true;
            this.txtCountryName.Size = new System.Drawing.Size(230, 20);
            this.txtCountryName.SkipValidation = true;
            this.txtCountryName.TabIndex = 6;
            this.txtCountryName.TabStop = false;
            this.txtCountryName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCountryCode
            // 
            this.txtCountryCode.AllowSpace = true;
            this.txtCountryCode.AssociatedLookUpName = "lbtnCountryCode";
            this.txtCountryCode.BackColor = System.Drawing.SystemColors.Control;
            this.txtCountryCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountryCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCountryCode.ContinuationTextBox = null;
            this.txtCountryCode.CustomEnabled = true;
            this.txtCountryCode.DataFieldMapping = "FN_COUNTRY_CODE";
            this.txtCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountryCode.GetRecordsOnUpDownKeys = false;
            this.txtCountryCode.IsDate = false;
            this.txtCountryCode.IsLookUpField = true;
            this.txtCountryCode.Location = new System.Drawing.Point(199, 70);
            this.txtCountryCode.MaxLength = 3;
            this.txtCountryCode.Name = "txtCountryCode";
            this.txtCountryCode.NumberFormat = "###,###,##0.00";
            this.txtCountryCode.Postfix = "";
            this.txtCountryCode.Prefix = "";
            this.txtCountryCode.ReadOnly = true;
            this.txtCountryCode.Size = new System.Drawing.Size(45, 20);
            this.txtCountryCode.SkipValidation = true;
            this.txtCountryCode.TabIndex = 4;
            this.txtCountryCode.TabStop = false;
            this.txtCountryCode.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(73, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Record Status :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(73, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rentail Guarnatee :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Personnel No. :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(73, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Country Code :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(468, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(399, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "User Name :";
            // 
            // CHRIS_InterestDifferential_LSOtherAuthorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 450);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.label18);
            this.F3OptionText = "[F3]=Un-Auth.";
            this.F4OptionText = "[F4]=Save Record";
            this.F6OptionText = "[F6]=Exit W/O Save";
            this.F7OptionText = "[F7]=Authorized Record";
            this.Name = "CHRIS_InterestDifferential_LSOtherAuthorization";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRIS - LS/Others Entry Authorization";
            this.Shown += new System.EventHandler(this.CHRIS_InterestDifferential_LSOtherAuthorization_Shown);
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_InterestDifferential_LSOtherAuthorization_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label18, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlHeader, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtSegment;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtPersName;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtRecStatus;
        private CrplControlLibrary.SLTextBox txtRentGuarantee;
        private CrplControlLibrary.SLTextBox txtCountryName;
        private CrplControlLibrary.SLTextBox txtCountryCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtAuth;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtMarkerName;
        private CrplControlLibrary.SLTextBox txtMarkerTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLDatePicker dtpMarkerDate;
        private CrplControlLibrary.LookupButton lbtnCountryCode;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.LookupButton lbtnPersNo;
        private CrplControlLibrary.SLTextBox txtID;
    }
}