using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_CitiWiseProvidentFundBalances : BaseRptForm
    {
        //private string Dest_format = "PDF";
        public CHRIS_PFund_CitiWiseProvidentFundBalances()
        {
            InitializeComponent();
        }
        string DestFormat1 = "PDF";


        public CHRIS_PFund_CitiWiseProvidentFundBalances(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #region code by Irfan Farooqui
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.YEAR.Text = "1995";
            this.BRANCH.Text = "FBD";
            this.copies.Text = "1";
            this.DestFormat.Text = "cond";
        }



        private void RUN_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "PFR02";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (copies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(copies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }

                //if (Dest_Type.Text == "Printer")
                //{

                //    base.PrintCustomReport();

                //}

                if (Dest_Type.Text == "File")
                {
                    if (this.DestFormat.Text != string.Empty && Dest_name.Text != string.Empty)
                    {
                        base.ExportCustomReport(Dest_name.Text, DestFormat1);
                    }






                }

            }

            if (Dest_Type.Text == "Mail")
            {
                if (this.DestFormat.Text != string.Empty)
                {
                    base.EmailToReport("C:\\iCORE-Spool\\Report", DestFormat1);

                }


            }





        }


        private void CLOSE_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }



        #endregion 

              

    }
}


