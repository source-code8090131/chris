namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_MonthlyPFUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.slPanel1 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.txtPFund = new CrplControlLibrary.SLTextBox(this.components);
            this.slPanel2 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.txtPFAREARS = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpPayrollDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo2 = new CrplControlLibrary.SLTextBox(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanel1.SuspendLayout();
            this.slPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(586, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(622, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 434);
            this.panel1.Size = new System.Drawing.Size(622, 60);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(367, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 17;
            this.lblUserName.Text = "User Name  :";
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(119, 16);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(234, 24);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Provident Fund Updation";
            // 
            // slPanel1
            // 
            this.slPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanel1.ConcurrentPanels = null;
            this.slPanel1.Controls.Add(this.txtPFund);
            this.slPanel1.Controls.Add(this.slPanel2);
            this.slPanel1.DataManager = null;
            this.slPanel1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel1.DependentPanels = null;
            this.slPanel1.DisableDependentLoad = false;
            this.slPanel1.EnableDelete = true;
            this.slPanel1.EnableInsert = true;
            this.slPanel1.EnableQuery = false;
            this.slPanel1.EnableUpdate = true;
            this.slPanel1.EntityName = null;
            this.slPanel1.Location = new System.Drawing.Point(27, 85);
            this.slPanel1.MasterPanel = null;
            this.slPanel1.Name = "slPanel1";
            this.slPanel1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel1.Size = new System.Drawing.Size(548, 346);
            this.slPanel1.SPName = null;
            this.slPanel1.TabIndex = 18;
            // 
            // txtPFund
            // 
            this.txtPFund.AllowSpace = true;
            this.txtPFund.AssociatedLookUpName = "";
            this.txtPFund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPFund.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPFund.ContinuationTextBox = null;
            this.txtPFund.CustomEnabled = true;
            this.txtPFund.DataFieldMapping = "PFND";
            this.txtPFund.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPFund.GetRecordsOnUpDownKeys = false;
            this.txtPFund.IsDate = false;
            this.txtPFund.Location = new System.Drawing.Point(421, 318);
            this.txtPFund.Name = "txtPFund";
            this.txtPFund.NumberFormat = "###,###,##0.00";
            this.txtPFund.Postfix = "";
            this.txtPFund.Prefix = "";
            this.txtPFund.Size = new System.Drawing.Size(100, 20);
            this.txtPFund.SkipValidation = false;
            this.txtPFund.TabIndex = 6;
            this.txtPFund.TextType = CrplControlLibrary.TextType.String;
            this.txtPFund.Visible = false;
            // 
            // slPanel2
            // 
            this.slPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slPanel2.ConcurrentPanels = null;
            this.slPanel2.Controls.Add(this.txtPFAREARS);
            this.slPanel2.Controls.Add(this.label2);
            this.slPanel2.Controls.Add(this.label1);
            this.slPanel2.Controls.Add(this.dtpPayrollDate);
            this.slPanel2.Controls.Add(this.label9);
            this.slPanel2.Controls.Add(this.txtPersonnelNo);
            this.slPanel2.Controls.Add(this.txtPersonnelNo2);
            this.slPanel2.Controls.Add(this.btnExit);
            this.slPanel2.Controls.Add(this.btnProcess);
            this.slPanel2.Controls.Add(this.groupBox1);
            this.slPanel2.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanel2.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel2.DependentPanels = null;
            this.slPanel2.DisableDependentLoad = false;
            this.slPanel2.EnableDelete = true;
            this.slPanel2.EnableInsert = true;
            this.slPanel2.EnableQuery = false;
            this.slPanel2.EnableUpdate = true;
            this.slPanel2.EntityName = null;
            this.slPanel2.Location = new System.Drawing.Point(27, 9);
            this.slPanel2.MasterPanel = null;
            this.slPanel2.Name = "slPanel2";
            this.slPanel2.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel2.Size = new System.Drawing.Size(498, 303);
            this.slPanel2.SPName = "CHRIS_SP_PROVIDENTFUNDUPDATE_MANAGER";
            this.slPanel2.TabIndex = 7;
            // 
            // txtPFAREARS
            // 
            this.txtPFAREARS.AllowSpace = true;
            this.txtPFAREARS.AssociatedLookUpName = "";
            this.txtPFAREARS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPFAREARS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPFAREARS.ContinuationTextBox = null;
            this.txtPFAREARS.CustomEnabled = true;
            this.txtPFAREARS.DataFieldMapping = "PFAR";
            this.txtPFAREARS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPFAREARS.GetRecordsOnUpDownKeys = false;
            this.txtPFAREARS.IsDate = false;
            this.txtPFAREARS.Location = new System.Drawing.Point(287, 308);
            this.txtPFAREARS.Name = "txtPFAREARS";
            this.txtPFAREARS.NumberFormat = "###,###,##0.00";
            this.txtPFAREARS.Postfix = "";
            this.txtPFAREARS.Prefix = "";
            this.txtPFAREARS.Size = new System.Drawing.Size(100, 20);
            this.txtPFAREARS.SkipValidation = false;
            this.txtPFAREARS.TabIndex = 5;
            this.txtPFAREARS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(96, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 78;
            this.label2.Text = "Personnel No. :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 13);
            this.label1.TabIndex = 77;
            this.label1.Text = "Personnel No. Or NULL (ALL)  :";
            // 
            // dtpPayrollDate
            // 
            this.dtpPayrollDate.CustomEnabled = true;
            this.dtpPayrollDate.CustomFormat = "dd/MM/yyyy";
            this.dtpPayrollDate.DataFieldMapping = "";
            this.dtpPayrollDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayrollDate.HasChanges = true;
            this.dtpPayrollDate.IsRequired = true;
            this.dtpPayrollDate.Location = new System.Drawing.Point(208, 99);
            this.dtpPayrollDate.Name = "dtpPayrollDate";
            this.dtpPayrollDate.NullValue = " ";
            this.dtpPayrollDate.Size = new System.Drawing.Size(145, 20);
            this.dtpPayrollDate.TabIndex = 0;
            this.toolTip1.SetToolTip(this.dtpPayrollDate, "date format : dd/mm/yyyy");
            this.dtpPayrollDate.Value = new System.DateTime(2011, 1, 12, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(107, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 76;
            this.label9.Text = "Payroll Date :";
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.Location = new System.Drawing.Point(208, 132);
            this.txtPersonnelNo.MaxLength = 4;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(145, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 1;
            this.txtPersonnelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtPersonnelNo2
            // 
            this.txtPersonnelNo2.AllowSpace = true;
            this.txtPersonnelNo2.AssociatedLookUpName = "";
            this.txtPersonnelNo2.BackColor = System.Drawing.SystemColors.Window;
            this.txtPersonnelNo2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo2.ContinuationTextBox = null;
            this.txtPersonnelNo2.CustomEnabled = true;
            this.txtPersonnelNo2.DataFieldMapping = "PNUM";
            this.txtPersonnelNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo2.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo2.IsDate = false;
            this.txtPersonnelNo2.Location = new System.Drawing.Point(208, 167);
            this.txtPersonnelNo2.Name = "txtPersonnelNo2";
            this.txtPersonnelNo2.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo2.Postfix = "";
            this.txtPersonnelNo2.Prefix = "";
            this.txtPersonnelNo2.ReadOnly = true;
            this.txtPersonnelNo2.Size = new System.Drawing.Size(145, 20);
            this.txtPersonnelNo2.SkipValidation = false;
            this.txtPersonnelNo2.TabIndex = 99;
            this.txtPersonnelNo2.TextType = CrplControlLibrary.TextType.String;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnExit.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(258, 247);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 32);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnProcess.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(155, 247);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(100, 32);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Process Start";
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblHeader);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(496, 48);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // CHRIS_PFund_MonthlyPFUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(622, 494);
            this.Controls.Add(this.slPanel1);
            this.Controls.Add(this.lblUserName);
            this.Name = "CHRIS_PFund_MonthlyPFUpdate";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Monthly PF Update";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.slPanel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanel1.ResumeLayout(false);
            this.slPanel1.PerformLayout();
            this.slPanel2.ResumeLayout(false);
            this.slPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel1;
        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker dtpPayrollDate;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private CrplControlLibrary.SLTextBox txtPersonnelNo2;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtPFAREARS;
        private CrplControlLibrary.SLTextBox txtPFund;
    }
}