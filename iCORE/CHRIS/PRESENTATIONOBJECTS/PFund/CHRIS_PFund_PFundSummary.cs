﻿using iCORE.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_PFundSummary : Form
    {
        public CHRIS_PFund_PFundSummary()
        {
            InitializeComponent();
        }

        private void btnPFCalculate_Click(object sender, EventArgs e)
        {

            //param.Add("ww_date", Convert.ToDateTime(dtww_date.Value).ToShortDateString());
            DateTime fromDate = Convert.ToDateTime(dtRptFrmDate.Value);
            DateTime ToDate = Convert.ToDateTime(sldpkToDate.Value);
            string PR_Emp_No = CHRIS_PFund_PFundQuery.SetValuePersonalNo;

            if (!string.IsNullOrEmpty(PR_Emp_No))
            {
                DataTable dt = new DataTable();
                dt = SQLManager.exPFund(fromDate, ToDate, PR_Emp_No).Tables[0];
                string EmpShare = string.Empty;
                string EmplyerShare = string.Empty;


                if(dt.Rows.Count < 1)
                {
                    MessageBox.Show("No Record Found");
                    return;
                }

                foreach (DataRow rowCust in dt.Rows)
                {
                    EmplyerShare = string.IsNullOrEmpty(rowCust["PF_BNK_CONT"].ToString().Trim()) ? "0" : rowCust["PF_BNK_CONT"].ToString().Trim();
                    EmpShare = string.IsNullOrEmpty(rowCust["PF_EMP_CONT"].ToString().Trim()) ? "0" : rowCust["PF_EMP_CONT"].ToString().Trim();

                    double emp = Convert.ToDouble(EmplyerShare);
                    double bank = Convert.ToDouble(EmpShare);
                    slTxtBxTotalAmount.Text = Convert.ToString((emp + bank));
                    slTxtBxEmployerShare.Text = EmplyerShare;
                    slTxtBxEmpShare.Text = EmpShare;

                }
            }
            else
            {
                MessageBox.Show("No Record Found");
            }
        }
    }
}
