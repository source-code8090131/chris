using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_ProvidentFundContribution : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PFund_ProvidentFundContribution()
        {
            InitializeComponent();
        }
        string DestFormat;
        public CHRIS_PFund_ProvidentFundContribution(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.mode.Items.RemoveAt(this.mode.Items.Count - 1);
            this.brn.Text = "LHR";
            this.w_year.Text = "1997";
            this.desformat.Text = "dflt";
            this.desname.Text = "C:\\iCORE-Spool\\ABC.LIS";
            this.cmbDescType.Text = "File";
            this.mode.Text = "Character";
            this.Copies.Text = "1";

        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PFR08";
            if (this.desformat.Text == string.Empty) 
                DestFormat = "PDF";
            else
	
                DestFormat = this.desformat.Text;
	
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.Copies.Text );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                if (this.desname.Text == string.Empty)

                    DestName = @"C:\iCORE-Spool\Report";
                else
                {
                    DestName = this.desname.Text; 
                }
                    base.ExportCustomReport(DestName, DestFormat );

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                
                string RecipentName;
                RecipentName = "";
                if (this.desname.Text == string.Empty)

                    RecipentName = "";
                else
                {
                    RecipentName  = this.desname.Text;
                }

                    base.EmailToReport(DestName, DestFormat , RecipentName);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
      
         

        
    }
}