using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using iCORE.XMS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_YearEndClosingDialog : Form
    {

        string pfYear;
        
        public CHRIS_PFund_YearEndClosingDialog(string pfYr)
        {
            InitializeComponent();
            this.pfYear = pfYr;
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

        }

        private void CHRIS_PFund_YearEndClosingDialog_Shown(object sender, EventArgs e)
        {


            Dictionary<string, object> objVals = new Dictionary<string, object>();
            DataTable dtPFund = new DataTable();
            Result rsltPFund,rsltMain;
            CmnDataManager cmnDM = new CmnDataManager();

            objVals.Clear();
            objVals.Add("PR_YEAR", this.pfYear);
            
            //DELETING FROM PFUND//
            rsltPFund = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "DEL_PFUND", objVals);
            if (rsltPFund.isSuccessful)
            {
                //if ( rsltPFund.dstResult.Tables.Count > 0 )  //rsltPFund.dstResult.ExtendedProperties.Count > 0
                //{
            
                    //CALLING THE MAIN VALID_PNO [CONTAIN 2 SPs]//
                    rsltMain = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "VALID_PNO", objVals);

                    if (rsltMain.isSuccessful)
                    {

                        if (rsltMain.dstResult.Tables[0].Rows.Count > 0) 
                        {

                            dtPFund = rsltMain.dstResult.Tables[0];
                            if (dtPFund.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtPFund.Rows.Count; j++)
                                {
                                    txtPersonnelNo.Text = Convert.ToString(dtPFund.Rows[j]["PR_P_NO"]);
                                    Application.DoEvents();
                                    objVals.Clear();
                                    objVals.Add("PR_P_NUM", txtPersonnelNo.Text);
                                    objVals.Add("PR_YEAR", this.pfYear);
                                    cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "PROCESS_PFUND", objVals);
                                    //System.Threading.Thread.Sleep(200);
                                }
                            }

                        }

                            this.Close();

                    }

               // }
               
            }



          }

        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {




        }

   }
}