using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_ResignISandOtherIPFB : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PFund_ResignISandOtherIPFB()
        {
            InitializeComponent();
        }
        String DestFormat; 
        public CHRIS_PFund_ResignISandOtherIPFB(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.mode.Items.RemoveAt(this.mode.Items.Count - 1);
            this.year.Text = "1996";
            this.desformat.Text = "NO2_FF";
            this.mode.Text = "Character";
            this.Copies.Text = "1";
        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PFR06";
            if (this.desformat.Text == String.Empty)
                DestFormat = "PDF";
            else
                DestFormat = this.desformat.Text; 

            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.Copies.Text );
                //base.PrintNoofCopiesReport(1);
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                
                if (this.desname.Text == string.Empty)
                    DestName = @"C:\iCORE-Spool\Report";
                else
                    DestName = this.desname.Text;
                if (DestName != string.Empty)
                {
                    base.ExportCustomReport(DestName, DestFormat );
                }
            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                
                string RecipentName;
                RecipentName = "";
                if (this.desname.Text == string.Empty)
                    RecipentName = "";
                else
                    RecipentName = this.desname.Text;

                if (DestName != string.Empty)
                {
                    base.EmailToReport(DestName, DestFormat , RecipentName);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
      
         

        
    }
}