using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_PersonalProvidentFundBalance : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PFund_PersonalProvidentFundBalance()
        {
            InitializeComponent();
        }

        public CHRIS_PFund_PersonalProvidentFundBalance(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        String DesFormat;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.mode.Items.RemoveAt(this.mode.Items.Count - 1);
            this.w_brn.Text = "ALL";
            this.w_year.Text = "1995";
            this.w_from.Text = "40";
            this.w_pto.Text = "40";
            this.w_dept.Text = "ALL";
            this.desformat.Text = "NO2_FF";
            this.Copies.Text = "1";

        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PFR04";
            if (this.desformat.Text == String.Empty ) 
                 DesFormat = "PDF";
            else
                 DesFormat = this.desformat.Text;

            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.Copies.Text );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;


                if (this.desname.Text != string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";

                }
                else
                {
                    DestName = this.desname.Text;
                }

                base.ExportCustomReport(DestName, DesFormat);
            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string RecipentName;
                RecipentName = "";

                if (this.desname.Text != string.Empty)
                {
                    RecipentName = "";

                }
                else
                {
                    RecipentName = this.desname.Text;
                }



                    base.EmailToReport(DestName, DesFormat , RecipentName);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
      
         

        
    }
}