namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_PFundInterestEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PFund_PFundInterestEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.dgvPFInterest = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PFI_YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PFI_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PFI_RES_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.lblNewCriteria = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPFInterest)).BeginInit();
            this.PnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(598, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(634, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 515);
            this.panel1.Size = new System.Drawing.Size(634, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.lblHeader);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Location = new System.Drawing.Point(90, 54);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(462, 69);
            this.pnlHead.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(87, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(218, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "P.Fund Interest Entry";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeader
            // 
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(58, 9);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(276, 20);
            this.lblHeader.TabIndex = 19;
            this.lblHeader.Text = "Provident Fund Module";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(371, 23);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(328, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvPFInterest
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPFInterest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPFInterest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPFInterest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PFI_YEAR,
            this.PFI_RATE,
            this.PFI_RES_RATE});
            this.dgvPFInterest.ColumnToHide = null;
            this.dgvPFInterest.ColumnWidth = null;
            this.dgvPFInterest.CustomEnabled = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPFInterest.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPFInterest.DisplayColumnWrapper = null;
            this.dgvPFInterest.GridDefaultRow = 0;
            this.dgvPFInterest.Location = new System.Drawing.Point(12, 16);
            this.dgvPFInterest.Name = "dgvPFInterest";
            this.dgvPFInterest.ReadOnlyColumns = null;
            this.dgvPFInterest.RequiredColumns = "PFI_RATE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPFInterest.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvPFInterest.Size = new System.Drawing.Size(510, 301);
            this.dgvPFInterest.SkippingColumns = null;
            this.dgvPFInterest.TabIndex = 0;
            this.dgvPFInterest.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvPFInterest_CellValidating);
            this.dgvPFInterest.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvPFInterest_UserAddedRow);
            // 
            // PFI_YEAR
            // 
            this.PFI_YEAR.DataPropertyName = "PFI_YEAR";
            dataGridViewCellStyle2.NullValue = null;
            this.PFI_YEAR.DefaultCellStyle = dataGridViewCellStyle2;
            this.PFI_YEAR.HeaderText = "Year";
            this.PFI_YEAR.MaxInputLength = 4;
            this.PFI_YEAR.Name = "PFI_YEAR";
            this.PFI_YEAR.ToolTipText = "Field Must Be Enter";
            this.PFI_YEAR.Width = 110;
            // 
            // PFI_RATE
            // 
            this.PFI_RATE.DataPropertyName = "PFI_RATE";
            dataGridViewCellStyle3.NullValue = null;
            this.PFI_RATE.DefaultCellStyle = dataGridViewCellStyle3;
            this.PFI_RATE.HeaderText = "Interest Rate %";
            this.PFI_RATE.MaxInputLength = 10;
            this.PFI_RATE.Name = "PFI_RATE";
            this.PFI_RATE.Width = 215;
            // 
            // PFI_RES_RATE
            // 
            this.PFI_RES_RATE.DataPropertyName = "PFI_RES_RATE";
            dataGridViewCellStyle4.Format = "##0.000000";
            this.PFI_RES_RATE.DefaultCellStyle = dataGridViewCellStyle4;
            this.PFI_RES_RATE.HeaderText = "Resinges Interest Rate %";
            this.PFI_RES_RATE.MaxInputLength = 12;
            this.PFI_RES_RATE.Name = "PFI_RES_RATE";
            this.PFI_RES_RATE.Width = 210;
            // 
            // PnlDetail
            // 
            this.PnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.dgvPFInterest);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = false;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFundInterestEntryCommand";
            this.PnlDetail.Location = new System.Drawing.Point(53, 145);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(535, 332);
            this.PnlDetail.SPName = "CHRIS_PFUND_INTEREST_MANAGER";
            this.PnlDetail.TabIndex = 0;
            this.PnlDetail.TabStop = true;
            // 
            // lblNewCriteria
            // 
            this.lblNewCriteria.AutoSize = true;
            this.lblNewCriteria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewCriteria.Location = new System.Drawing.Point(12, 489);
            this.lblNewCriteria.Name = "lblNewCriteria";
            this.lblNewCriteria.Size = new System.Drawing.Size(125, 13);
            this.lblNewCriteria.TabIndex = 74;
            this.lblNewCriteria.Text = "[F10] = Save Record";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.Location = new System.Drawing.Point(143, 489);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(65, 13);
            this.lblExit.TabIndex = 75;
            this.lblExit.Text = "[F6] = Exit";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(390, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 76;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_PFund_PFundInterestEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 575);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.lblNewCriteria);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.PnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "PnlDetail";
            this.Name = "CHRIS_PFund_PFundInterestEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS : PFund Interest Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.Controls.SetChildIndex(this.lblExit, 0);
            this.Controls.SetChildIndex(this.lblNewCriteria, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPFInterest)).EndInit();
            this.PnlDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblHeader;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvPFInterest;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetail;
        private System.Windows.Forms.Label lblNewCriteria;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFI_YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFI_RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFI_RES_RATE;
    }
}