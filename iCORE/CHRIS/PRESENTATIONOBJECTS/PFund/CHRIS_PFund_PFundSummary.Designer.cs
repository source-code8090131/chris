﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_PFundSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtRptFrmDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.btnPFCalculate = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.slTxtBxTotalAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.slTxtBxEmpShare = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.slTxtBxEmployerShare = new CrplControlLibrary.SLTextBox(this.components);
            this.sldpkToDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.sldpkToDate);
            this.panel2.Controls.Add(this.dtRptFrmDate);
            this.panel2.Controls.Add(this.btnPFCalculate);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.slTxtBxTotalAmount);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.slTxtBxEmpShare);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.slTxtBxEmployerShare);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(419, 235);
            this.panel2.TabIndex = 74;
            // 
            // dtRptFrmDate
            // 
            this.dtRptFrmDate.Checked = false;
            this.dtRptFrmDate.CustomEnabled = true;
            this.dtRptFrmDate.CustomFormat = "";
            this.dtRptFrmDate.DataFieldMapping = "";
            this.dtRptFrmDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRptFrmDate.HasChanges = true;
            this.dtRptFrmDate.Location = new System.Drawing.Point(17, 45);
            this.dtRptFrmDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtRptFrmDate.Name = "dtRptFrmDate";
            this.dtRptFrmDate.NullValue = " ";
            this.dtRptFrmDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dtRptFrmDate.Size = new System.Drawing.Size(159, 22);
            this.dtRptFrmDate.TabIndex = 74;
            this.dtRptFrmDate.Value = new System.DateTime(2021, 12, 31, 0, 0, 0, 0);
            // 
            // btnPFCalculate
            // 
            this.btnPFCalculate.Location = new System.Drawing.Point(244, 190);
            this.btnPFCalculate.Name = "btnPFCalculate";
            this.btnPFCalculate.Size = new System.Drawing.Size(138, 34);
            this.btnPFCalculate.TabIndex = 75;
            this.btnPFCalculate.Text = "Calculate";
            this.btnPFCalculate.UseVisualStyleBackColor = true;
            this.btnPFCalculate.Click += new System.EventHandler(this.btnPFCalculate_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(17, 165);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(104, 17);
            this.label34.TabIndex = 74;
            this.label34.Text = "Total Amount";
            // 
            // slTxtBxTotalAmount
            // 
            this.slTxtBxTotalAmount.AllowSpace = true;
            this.slTxtBxTotalAmount.AssociatedLookUpName = "";
            this.slTxtBxTotalAmount.BackColor = System.Drawing.SystemColors.Control;
            this.slTxtBxTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBxTotalAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBxTotalAmount.ContinuationTextBox = null;
            this.slTxtBxTotalAmount.CustomEnabled = true;
            this.slTxtBxTotalAmount.DataFieldMapping = "PF_YEAR_TOT";
            this.slTxtBxTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBxTotalAmount.GetRecordsOnUpDownKeys = false;
            this.slTxtBxTotalAmount.IsDate = false;
            this.slTxtBxTotalAmount.Location = new System.Drawing.Point(20, 196);
            this.slTxtBxTotalAmount.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtBxTotalAmount.Name = "slTxtBxTotalAmount";
            this.slTxtBxTotalAmount.NumberFormat = "###,###,##0.00";
            this.slTxtBxTotalAmount.Postfix = "";
            this.slTxtBxTotalAmount.Prefix = "";
            this.slTxtBxTotalAmount.ReadOnly = true;
            this.slTxtBxTotalAmount.Size = new System.Drawing.Size(171, 24);
            this.slTxtBxTotalAmount.SkipValidation = false;
            this.slTxtBxTotalAmount.TabIndex = 73;
            this.slTxtBxTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTxtBxTotalAmount.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(35, 16);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 17);
            this.label30.TabIndex = 67;
            this.label30.Text = "From Date";
            // 
            // slTxtBxEmpShare
            // 
            this.slTxtBxEmpShare.AllowSpace = true;
            this.slTxtBxEmpShare.AssociatedLookUpName = "";
            this.slTxtBxEmpShare.BackColor = System.Drawing.SystemColors.Control;
            this.slTxtBxEmpShare.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBxEmpShare.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBxEmpShare.ContinuationTextBox = null;
            this.slTxtBxEmpShare.CustomEnabled = true;
            this.slTxtBxEmpShare.DataFieldMapping = "PF_YEAR_TOT";
            this.slTxtBxEmpShare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBxEmpShare.GetRecordsOnUpDownKeys = false;
            this.slTxtBxEmpShare.IsDate = false;
            this.slTxtBxEmpShare.Location = new System.Drawing.Point(211, 123);
            this.slTxtBxEmpShare.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtBxEmpShare.Name = "slTxtBxEmpShare";
            this.slTxtBxEmpShare.NumberFormat = "###,###,##0.00";
            this.slTxtBxEmpShare.Postfix = "";
            this.slTxtBxEmpShare.Prefix = "";
            this.slTxtBxEmpShare.ReadOnly = true;
            this.slTxtBxEmpShare.Size = new System.Drawing.Size(171, 24);
            this.slTxtBxEmpShare.SkipValidation = false;
            this.slTxtBxEmpShare.TabIndex = 72;
            this.slTxtBxEmpShare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTxtBxEmpShare.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(208, 93);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 17);
            this.label33.TabIndex = 71;
            this.label33.Text = "Employee Share";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(17, 93);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(132, 17);
            this.label32.TabIndex = 70;
            this.label32.Text = "Employeer Share";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(235, 16);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(66, 17);
            this.label31.TabIndex = 68;
            this.label31.Text = "To Date";
            // 
            // slTxtBxEmployerShare
            // 
            this.slTxtBxEmployerShare.AllowSpace = true;
            this.slTxtBxEmployerShare.AssociatedLookUpName = "";
            this.slTxtBxEmployerShare.BackColor = System.Drawing.SystemColors.Control;
            this.slTxtBxEmployerShare.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBxEmployerShare.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBxEmployerShare.ContinuationTextBox = null;
            this.slTxtBxEmployerShare.CustomEnabled = true;
            this.slTxtBxEmployerShare.DataFieldMapping = "PF_YEAR_TOT";
            this.slTxtBxEmployerShare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBxEmployerShare.GetRecordsOnUpDownKeys = false;
            this.slTxtBxEmployerShare.IsDate = false;
            this.slTxtBxEmployerShare.Location = new System.Drawing.Point(20, 123);
            this.slTxtBxEmployerShare.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtBxEmployerShare.Name = "slTxtBxEmployerShare";
            this.slTxtBxEmployerShare.NumberFormat = "###,###,##0.00";
            this.slTxtBxEmployerShare.Postfix = "";
            this.slTxtBxEmployerShare.Prefix = "";
            this.slTxtBxEmployerShare.ReadOnly = true;
            this.slTxtBxEmployerShare.Size = new System.Drawing.Size(171, 24);
            this.slTxtBxEmployerShare.SkipValidation = false;
            this.slTxtBxEmployerShare.TabIndex = 69;
            this.slTxtBxEmployerShare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTxtBxEmployerShare.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // sldpkToDate
            // 
            this.sldpkToDate.Checked = false;
            this.sldpkToDate.CustomEnabled = true;
            this.sldpkToDate.CustomFormat = "";
            this.sldpkToDate.DataFieldMapping = "";
            this.sldpkToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sldpkToDate.HasChanges = true;
            this.sldpkToDate.Location = new System.Drawing.Point(211, 46);
            this.sldpkToDate.Margin = new System.Windows.Forms.Padding(4);
            this.sldpkToDate.Name = "sldpkToDate";
            this.sldpkToDate.NullValue = " ";
            this.sldpkToDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.sldpkToDate.Size = new System.Drawing.Size(159, 22);
            this.sldpkToDate.TabIndex = 79;
            this.sldpkToDate.Value = new System.DateTime(2021, 12, 31, 0, 0, 0, 0);
            // 
            // CHRIS_PFund_PFundSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 259);
            this.Controls.Add(this.panel2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_PFund_PFundSummary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_PFund_PFundSummary";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private CrplControlLibrary.SLDatePicker dtRptFrmDate;
        private System.Windows.Forms.Button btnPFCalculate;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox slTxtBxTotalAmount;
        private System.Windows.Forms.Label label30;
        private CrplControlLibrary.SLTextBox slTxtBxEmpShare;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLTextBox slTxtBxEmployerShare;
        private CrplControlLibrary.SLDatePicker sldpkToDate;
    }
}