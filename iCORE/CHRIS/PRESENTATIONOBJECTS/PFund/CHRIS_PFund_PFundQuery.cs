using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_PFundQuery : ChrisSimpleForm
    {
        CmnDataManager CnmDM = new CmnDataManager();
        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();
      
        #region variables
        bool ValidateProc1 = false;
        double pf_rate = 0;
        double PF_TOTAL = 0;
        double INS_YEAR_TOT= 0;
        double EMP_YEAR_TOT =0;
        double BNK_YEAR_TOT =0;
        double F_PREMIUM =0;
       // EMP_TOT,BNK_TOT,INS_TOT,PF_INTEREST,PF_BNK_CONT,PF_EMP_CONT,PF_TOT,INS_TOT,BNK_TOT,EMP_TOT,PF_YEAR_TOT
        double EMP_TOT = 0;
        double INS_TOT = 0;
        double PF_INTEREST = 0;
        double PF_BNK_CONT = 0;
        double PF_EMP_CONT = 0;
        double PF_TOT = 0;
        double BNK_TOT = 0;
        double PF_YEAR_TOT = 0;
        string pfundQry = "0";
        int count = 0;
        #endregion

        # region Constructor

        public CHRIS_PFund_PFundQuery()
        {
            InitializeComponent();
        }
        public CHRIS_PFund_PFundQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
                InitializeComponent();

            }

        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {
        
            base.OnLoad(e);
            txtOption.Visible = false;
            //this.txtUser.Text = this.UserName;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            label29.Text += "   " + this.UserName;
            txtUser.Text = this.userID;
            tbtList.Visible = false;
            
            tbtSave.Visible  = false;
            tbtAdd.Visible  = false;
            tbtDelete.Visible = false;

            


        }
        private void  proc1()
        {



            BNK_YEAR_TOT = 0;
            EMP_YEAR_TOT = 0;
            
           
        int month;
        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("pf_pr_p_no",txtPersonnel.Text);
        param.Add("ww_date", Convert.ToDateTime(dtww_date.Value).ToShortDateString());
        rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Proc_1", param);
        int index=0;
        if (rslt.isSuccessful)
        {
           if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
           {
               int counter = rslt.dstResult.Tables[0].Rows.Count;
             // foreach(counter cs in rslt.dstResult.Tables[0])
             while (counter != 0)
               {
                    month = Int32.Parse(rslt.dstResult.Tables[0].Rows[index].ItemArray[0].ToString());
                   switch (month)
                   { 
                       case 1 :
                           
                           txtjan_bnk.Text = "";
                           txtJan_emp.Text = "";
                         

                           txtjan_bnk.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                           txtJan_emp.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;
                           if (txtjan_bnk.Text != "" && txtJan_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtjan_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtJan_emp.Text);
                           }
                           else if (txtjan_bnk.Text == "" && txtJan_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       case 2 :

                           txtfeb_bnk.Text = "";
                           txtfeb_emp.Text = "";
                         
     

                           txtfeb_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtfeb_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter- 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;
                           if (txtfeb_bnk.Text != "" && txtfeb_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtfeb_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtfeb_emp.Text);
                           }
                           else if (txtfeb_bnk.Text == "" && txtfeb_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       
                       case 3 :

                           txtMar_bnk.Text = "";
                           txtMar_emp.Text = "";
                         
                        
                           txtMar_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtMar_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;
                           if (txtMar_bnk.Text != "" && txtMar_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtMar_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtMar_emp.Text);
                           }
                           else if (txtMar_bnk.Text == "" && txtMar_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       
                       case 4 :

                           txtapr_bnk.Text = "";
                           txtApr_emp.Text = "";
                          

                           txtapr_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtApr_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;

                           if (txtapr_bnk.Text != "" && txtApr_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtapr_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtApr_emp.Text);
                           }
                           else if (txtapr_bnk.Text == "" && txtApr_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }

                           break;
                       
                       case 5 :

                           txtMay_bnk.Text = "";
                           txtMay_emp.Text = "";
                           

                           txtMay_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtMay_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;
                           if (txtMay_bnk.Text != "" && txtMay_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtMay_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtMay_emp.Text);
                           }
                           else if (txtMay_bnk.Text == "" && txtMay_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       case 6 :

                           txtjun_bnk.Text = "";
                           txtJun_emp.Text = "";
                          

                           txtjun_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtJun_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;
                           if (txtjun_bnk.Text != "" && txtJun_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtjun_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtJun_emp.Text);
                           }
                           else if (txtjun_bnk.Text == "" && txtJun_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;

                       case 7 :

                           txtjul_bnk.Text = "";
                           txtJul_emp.Text = "";
                        
                           txtjul_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtJul_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;

                           if (txtjul_bnk.Text != "" && txtJul_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtjul_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtJul_emp.Text);
                           }
                           else if (txtjul_bnk.Text == "" && txtJul_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       
                       case 8 :

                           txtaug_bnk.Text = "";
                           txtaug_emp.Text = "";
                        
                           txtaug_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtaug_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;

                           if (txtaug_bnk.Text != "" && txtaug_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtaug_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtaug_emp.Text);
                           }
                           else if (txtaug_bnk.Text == "" && txtaug_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       case 9 :

                           txtsep_bnk.Text = "";
                           txtsep_emp.Text = "";
                         


                           txtsep_bnk.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtsep_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;
                           if (txtsep_bnk.Text != "" && txtsep_emp.Text != "")
                           {

                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtsep_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtsep_emp.Text);
                           }
                           else if (txtsep_bnk.Text == "" && txtsep_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       case 10:

                           txtoct_bnk.Text = "";
                           txtoct_emp.Text = "";
                       


                           txtoct_bnk.Text=rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtoct_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;

                           if (txtoct_bnk.Text != "" && txtoct_bnk.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtoct_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtoct_emp.Text);

                           }
                           else if (txtoct_bnk.Text == "" && txtoct_bnk.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                           
                       case 11:

                           txtNov_bnk.Text = "";
                           txtNov_emp.Text = "";
                         
                           txtNov_bnk.Text=rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtNov_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;

                           if (txtNov_bnk.Text != "" && txtNov_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtNov_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtNov_emp.Text);

                           }
                           else if (txtNov_bnk.Text == "" && txtNov_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;
                       
                       case 12:

                           txtdec_bnk.Text = "";
                           txtDec_emp.Text = "";
                      

                           txtdec_bnk.Text=rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           txtDec_emp.Text = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                           index = index + 1;
                           counter = counter - 1;
                           //EMP_YEAR_TOT = 0;
                           //BNK_YEAR_TOT = 0;

                           if (txtdec_bnk.Text != "" && txtDec_emp.Text != "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtdec_bnk.Text);
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtDec_emp.Text);

                           }
                           else if (txtdec_bnk.Text == "" && txtDec_emp.Text == "")
                           {
                               EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                               BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                           }
                           break;           


                   }

               //txtyearEmp.Text = "";
               //txtyearBnk.Text = "";
            
               txtyearEmp.Text= EMP_YEAR_TOT.ToString();
               txtyearBnk.Text = BNK_YEAR_TOT.ToString();
               }  
              
                                
               //  dtww_date.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

           }


        }
        }
        protected void Clearform()
        {
            String personnelNo = txtPersonnel.Text;
            String personnelName = txtname.Text;
            this.ClearForm(PnlPFund.Controls);
            txtPersonnel.Text = personnelNo;
            txtname.Text = personnelName;
            txtPersonnel.Focus();

          //  txtAccIntrst.Text = "";
          //  txtBalance.Text = "";
          //  txtBranch.Text = "";
          //  txtPfBankcont.Text = "";
          //  txtPremium.Text = "";
          //  txtToatal.Text = "";
          //  txtTotalEmp.Text = "";
          //  txtTotalPF.Text = "";
          //  txtTotBnk.Text = "";
          //  txtTotIns.Text = "";
          //  txtyearBnk.Text = "";
          //  txtyearEmp.Text = "";
          //  txtyearIns.Text = "";
          //  txtyearPF.Text = "";
          //  txtPFbalnce.Text = "";
          //  txtyearIns.Text = "";
          //  txtTotIns.Text = "";

          //  txtjan_bnk.Text = "";
          //  txtJan_emp.Text = "";

          //  txtfeb_bnk.Text = "";
          //  txtfeb_emp.Text = "";

          //  txtMar_bnk.Text = "";
          //  txtMar_emp.Text = "";

          //  txtapr_bnk.Text = "";
          //  txtApr_emp.Text = "";

          //  txtMay_bnk.Text = "";
          //  txtMay_emp.Text = "";

          //  txtjun_bnk.Text = "";
          //  txtJun_emp.Text = "";

          //  txtjul_bnk.Text = "";
          //  txtJul_emp.Text = "";

          //  txtaug_bnk.Text = "";
          //  txtaug_emp.Text = "";

          //  txtsep_bnk.Text = "";
          //  txtsep_emp.Text = "";

          //  txtoct_bnk.Text = "";
          //  txtoct_emp.Text = "";

          //  txtNov_bnk.Text = "";
          //  txtNov_emp.Text = "";

          //  txtdec_bnk.Text = "";
          //  txtDec_emp.Text = "";

          ////  txtname.Text = "";
            
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {  return; }
            if (actionType == "Cancel")
            {
                this.ClearForm(PnlPFund.Controls);
                txtPersonnel.Focus();
            }
            if (actionType == "Add")
            { return; }
            if (actionType == "Delete")
            { return; }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        private bool chkPersonnelNo()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("pf_pr_p_no",this.txtPersonnel.Text);
            rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "IfPrpValid", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                {
                    return true;

                }
                else
                {
                    return false;
                
                }
            
            }
            else
            {
                return false;

            }



        
        
        }


        #endregion


        #region Events
        private void txtPersonnel_Validating(object sender, CancelEventArgs e)
        {
            // this.ClearForm(PnlPFund.Controls);
            //Clearform();
            //if (txtPersonnel.Text == "")
            //{
            //    MessageBox.Show("Enter Personnel No. or [F9] To Help or [F6] To Exit...");
            //    this.ClearForm(PnlPFund.Controls);
            //    txtPersonnel.Focus();
            //    return;
            //}

            bool chkprpno = true;//chkPersonnelNo();
            ValidateProc1 = true;
            if (ValidateProc1 && chkprpno)
            {
                PF_YEAR_TOT = 0;
                EMP_TOT = 0;
                BNK_TOT = 0;
                INS_TOT = 0;
                PF_TOT = 0;

                Clearform();



                pf_rate = 0;
                PF_TOTAL = 0;
                INS_YEAR_TOT = 0;
                EMP_YEAR_TOT = 0;
                BNK_YEAR_TOT = 0;
                F_PREMIUM = 0;
                EMP_TOT = 0;
                INS_TOT = 0;
                PF_INTEREST = 0;
                PF_BNK_CONT = 0;
                PF_EMP_CONT = 0;
                PF_TOT = 0;
                BNK_TOT = 0;
                PF_YEAR_TOT = 0;



                if (txtPersonnel.Text != "")
                {


                    param.Clear();
                    param.Add("PF_PR_P_NO", txtPersonnel.Text);
                    rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PP_VERIFY", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            MessageBox.Show("Invalid Personnel No. Entered..Or Not Confirmed...");
                            txtPersonnel.Focus();
                            return;

                        }
                    }


                }




                this.txtPersonnel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(CheckKeys);



                //-----------------------------------------------------//

                # region Key Next



                //PF_YEAR_TOT = 0;
                //EMP_TOT = 0;
                //BNK_TOT = 0;
                //INS_TOT = 0;
                //PF_TOT = 0;
                //txtyearEmp.Text = "";
                //txtyearBnk.Text = "";
                //txtyearIns.Text = "";
                //txtyearPF.Text = "";
                //txtPremium.Text = "";
                //txtTotalEmp.Text = "";
                //txtTotBnk.Text = "";
                //txtTotIns.Text = "";
                //txtTotalPF.Text = "";


                if (txtPersonnel.Text == "")
                {
                    return;

                }




                //count++;
                //if (count == 1)
                //{
                //    txtPersonnel2.Text = txtPersonnel.Text;

                //}
                //if (count == 2)
                //{
                //    pfundQry = txtPersonnel.Text;
                //    if (pfundQry == txtPersonnel2.Text)
                //    {
                //        count = 0;
                //        return;
                //    }

                //}
                //if (count >= 100 || pfundQry == txtPersonnel2.Text)
                //{
                //    count = 0;
                //    return;
                //}





                if (PF_TOTAL > 0)
                {

                    PF_EMP_CONT = 0;
                    PF_BNK_CONT = 0;
                    PF_INTEREST = 0;
                    PF_TOTAL = 0;
                    PF_EMP_CONT = 0;
                    PF_BNK_CONT = 0;
                    PF_INTEREST = 0;
                    F_PREMIUM = 0;

                }

                param.Clear();
                param.Add("PF_PR_P_NO", txtPersonnel.Text);
                rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Keynxt_PfYear", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        string chknull = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (chknull != string.Empty && chknull != "")
                        { dtww_date.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()); }
                        else
                        {
                            Result rslt4;
                            Dictionary<string, object> param4 = new Dictionary<string, object>();
                            param4.Add("PF_PR_P_NO", txtPersonnel.Text);
                            rslt4 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Keynxt_IfNull", param4);
                            if (rslt4.isSuccessful)
                            {
                                if (rslt4.dstResult.Tables.Count > 0 && rslt4.dstResult.Tables[0].Rows.Count > 0)
                                {

                                    string chk = rslt4.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                                    if (chk != string.Empty && chk != "")
                                        dtww_date.Value = Convert.ToDateTime(rslt4.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());


                                }
                            }
                        }

                    }


                }

                Result rslt5;
                Dictionary<string, object> param5 = new Dictionary<string, object>();
                param5.Clear();
                param5.Add("PF_PR_P_NO", txtPersonnel.Text);
                rslt5 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "GetYear", param5);
                if (rslt5.isSuccessful)
                {
                    if (rslt5.dstResult.Tables.Count > 0 && rslt5.dstResult.Tables[0].Rows.Count > 0)
                    {
                        string chknull = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (chknull != "" && chknull != string.Empty)
                        { txtBalance.Text = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(); }

                    }
                }







                #endregion

                Result rslt3;
                Dictionary<string, object> param3 = new Dictionary<string, object>();
                param3.Clear();
                param3.Add("pf_pr_p_no", txtPersonnel.Text);
                rslt3 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Execute_query", param3);

                if (rslt3.isSuccessful)
                {
                    if (rslt3.dstResult.Tables.Count > 0 && rslt3.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtPFbalnce.Text = rslt3.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        txtPfBankcont.Text = rslt3.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                        txtAccIntrst.Text = rslt3.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                        PF_EMP_CONT = double.Parse(txtPFbalnce.Text);
                        PF_BNK_CONT = double.Parse(txtPfBankcont.Text);
                        PF_INTEREST = double.Parse(txtAccIntrst.Text);
                        PF_TOTAL = PF_EMP_CONT + PF_BNK_CONT + PF_INTEREST;
                        txtToatal.Text = PF_TOTAL.ToString();

                    }
                }




                proc1();

                Result rslt2;
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                param2.Add("pf_pr_p_no", txtPersonnel.Text);
                param2.Add("ww_date", Convert.ToDateTime(dtww_date.Value).ToShortDateString());
                rslt2 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "KeyNxtf_Premium", param2);

                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtPremium.Text = "";
                        txtPremium.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        F_PREMIUM = double.Parse(txtPremium.Text);
                    }
                }

                rslt2 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "KeyNxtf_Rate", param2);
                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                    {
                        pf_rate = double.Parse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                    }
                }


                if (pf_rate > 0)
                {
                    INS_YEAR_TOT = (((EMP_YEAR_TOT) + (BNK_YEAR_TOT) + (PF_TOTAL)) - (F_PREMIUM)) * (pf_rate / 100);
                }




                PF_YEAR_TOT = (EMP_YEAR_TOT) + (BNK_YEAR_TOT) + (INS_YEAR_TOT);
                EMP_TOT = (PF_EMP_CONT) + (EMP_YEAR_TOT) - (F_PREMIUM);
                BNK_TOT = (PF_BNK_CONT) + (BNK_YEAR_TOT);
                INS_TOT = (PF_INTEREST) + (INS_YEAR_TOT);
                PF_TOT = (EMP_TOT) + (BNK_TOT) + (INS_TOT);




                txtyearPF.Text = PF_YEAR_TOT.ToString();
                txtyearIns.Text = INS_YEAR_TOT.ToString();
                if (txtyearIns.Text == "0" || txtyearIns.Text == null)
                {
                    txtyearIns.Text = "";
                }


                txtTotalEmp.Text = EMP_TOT.ToString();
                txtTotBnk.Text = BNK_TOT.ToString();
                txtTotIns.Text = INS_TOT.ToString();
                txtTotalPF.Text = PF_TOT.ToString();

                #region PostChng

                Result rslt1;
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("PF_PR_P_NO", txtPersonnel.Text);
                rslt1 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PostChng", param1);
                if (rslt.isSuccessful)
                {
                    if (rslt1.dstResult.Tables.Count > 0 && rslt1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtBranch.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray.ToString() == "" ? "" : rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    }
                }
                #endregion





                ValidateProc1 = false;
            }





        }     

        private void txtPersonnel_TextChanged(object sender, EventArgs e)
        {
           //// this.ClearForm(PnlPFund.Controls);
           // //Clearform();
           // //if (txtPersonnel.Text == "")
           // //{
           // //    MessageBox.Show("Enter Personnel No. or [F9] To Help or [F6] To Exit...");
           // //    this.ClearForm(PnlPFund.Controls);
           // //    txtPersonnel.Focus();
           // //    return;
           // //}

           // bool chkprpno = chkPersonnelNo();

           // if (ValidateProc1 && chkprpno)
           // {
           //     PF_YEAR_TOT = 0;
           //     EMP_TOT = 0;
           //     BNK_TOT = 0;
           //     INS_TOT = 0;
           //     PF_TOT = 0;

           //     Clearform();



           //     //pf_rate = 0;
           //     //PF_TOTAL = 0;
           //     //INS_YEAR_TOT = 0;
           //     //EMP_YEAR_TOT = 0;
           //     //BNK_YEAR_TOT = 0;
           //     //F_PREMIUM = 0;
           //     //EMP_TOT = 0;
           //     //INS_TOT = 0;
           //     //PF_INTEREST = 0;
           //     //PF_BNK_CONT = 0;
           //     //PF_EMP_CONT = 0;
           //     //PF_TOT = 0;
           //     //BNK_TOT = 0;
           //     //PF_YEAR_TOT = 0;



           //     if (txtPersonnel.Text != "")
           //     {


           //         param.Clear();
           //         param.Add("PF_PR_P_NO", txtPersonnel.Text);
           //         rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PP_VERIFY", param);
           //         if (rslt.isSuccessful)
           //         {
           //             if (rslt.dstResult.Tables[0].Rows.Count == 0)
           //             {
           //                 MessageBox.Show("Invalid Personnel No. Entered..Or Not Confirmed...");
           //                 txtPersonnel.Focus();
           //                 return;

           //             }
           //         }


           //     }




           //     this.txtPersonnel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(CheckKeys);



           //     //-----------------------------------------------------//

           //     # region Key Next



           //     //PF_YEAR_TOT = 0;
           //     //EMP_TOT = 0;
           //     //BNK_TOT = 0;
           //     //INS_TOT = 0;
           //     //PF_TOT = 0;
           //     //txtyearEmp.Text = "";
           //     //txtyearBnk.Text = "";
           //     //txtyearIns.Text = "";
           //     //txtyearPF.Text = "";
           //     //txtPremium.Text = "";
           //     //txtTotalEmp.Text = "";
           //     //txtTotBnk.Text = "";
           //     //txtTotIns.Text = "";
           //     //txtTotalPF.Text = "";


           //     if (txtPersonnel.Text == "")
           //     {
           //         return;

           //     }




           //     //count++;
           //     //if (count == 1)
           //     //{
           //     //    txtPersonnel2.Text = txtPersonnel.Text;

           //     //}
           //     //if (count == 2)
           //     //{
           //     //    pfundQry = txtPersonnel.Text;
           //     //    if (pfundQry == txtPersonnel2.Text)
           //     //    {
           //     //        count = 0;
           //     //        return;
           //     //    }

           //     //}
           //     //if (count >= 100 || pfundQry == txtPersonnel2.Text)
           //     //{
           //     //    count = 0;
           //     //    return;
           //     //}





           //     if (PF_TOTAL > 0)
           //     {

           //         PF_EMP_CONT = 0;
           //         PF_BNK_CONT = 0;
           //         PF_INTEREST = 0;
           //         PF_TOTAL = 0;
           //         PF_EMP_CONT = 0;
           //         PF_BNK_CONT = 0;
           //         PF_INTEREST = 0;
           //         F_PREMIUM = 0;

           //     }

           //     param.Clear();
           //     param.Add("PF_PR_P_NO", txtPersonnel.Text);
           //     rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Keynxt_PfYear", param);
           //     if (rslt.isSuccessful)
           //     {
           //         if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
           //         {
           //             string chknull = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

           //             if (chknull != string.Empty && chknull != "")
           //             { dtww_date.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()); }
           //             else
           //             {
           //                 Result rslt4;
           //                 Dictionary<string, object> param4 = new Dictionary<string, object>();
           //                 param4.Add("PF_PR_P_NO", txtPersonnel.Text);
           //                 rslt4 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Keynxt_IfNull", param4);
           //                 if (rslt4.isSuccessful)
           //                 {
           //                     if (rslt4.dstResult.Tables.Count > 0 && rslt4.dstResult.Tables[0].Rows.Count > 0)
           //                     {

           //                         string chk = rslt4.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

           //                         if (chk != string.Empty && chk != "")
           //                             dtww_date.Value = Convert.ToDateTime(rslt4.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());


           //                     }
           //                 }
           //             }

           //         }


           //     }

           //     Result rslt5;
           //     Dictionary<string, object> param5 = new Dictionary<string, object>();
           //     param5.Clear();
           //     param5.Add("PF_PR_P_NO", txtPersonnel.Text);
           //     rslt5 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "GetYear", param5);
           //     if (rslt5.isSuccessful)
           //     {
           //         if (rslt5.dstResult.Tables.Count > 0 && rslt5.dstResult.Tables[0].Rows.Count > 0)
           //         {
           //             string chknull = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

           //             if (chknull != "" && chknull != string.Empty)
           //             { txtBalance.Text = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(); }

           //         }
           //     }







           //     #endregion

           //     Result rslt3;
           //     Dictionary<string, object> param3 = new Dictionary<string, object>();
           //     param3.Clear();
           //     param3.Add("pf_pr_p_no", txtPersonnel.Text);
           //     rslt3 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Execute_query", param3);

           //     if (rslt3.isSuccessful)
           //     {
           //         if (rslt3.dstResult.Tables.Count > 0 && rslt3.dstResult.Tables[0].Rows.Count > 0)
           //         {
           //             txtPFbalnce.Text = rslt3.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
           //             txtPfBankcont.Text = rslt3.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
           //             txtAccIntrst.Text = rslt3.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
           //             PF_EMP_CONT = double.Parse(txtPFbalnce.Text);
           //             PF_BNK_CONT = double.Parse(txtPfBankcont.Text);
           //             PF_INTEREST = double.Parse(txtAccIntrst.Text);
           //             PF_TOTAL = PF_EMP_CONT + PF_BNK_CONT + PF_INTEREST;
           //             txtToatal.Text = PF_TOTAL.ToString();

           //         }
           //     }




           //     proc1();

           //     Result rslt2;
           //     Dictionary<string, object> param2 = new Dictionary<string, object>();
           //     param2.Add("pf_pr_p_no", txtPersonnel.Text);
           //     param2.Add("ww_date", Convert.ToDateTime(dtww_date.Value).ToShortDateString());
           //     rslt2 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "KeyNxtf_Premium", param2);

           //     if (rslt2.isSuccessful)
           //     {
           //         if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
           //         {
           //             txtPremium.Text = "";
           //             txtPremium.Text = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
           //             F_PREMIUM = double.Parse(txtPremium.Text);
           //         }
           //     }

           //     rslt2 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "KeyNxtf_Rate", param2);
           //     if (rslt2.isSuccessful)
           //     {
           //         if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
           //         {
           //             pf_rate = double.Parse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

           //         }
           //     }


           //     if (pf_rate > 0)
           //     {
           //         INS_YEAR_TOT = (((EMP_YEAR_TOT) + (BNK_YEAR_TOT) + (PF_TOTAL)) - (F_PREMIUM)) * (pf_rate / 100);
           //     }




           //     PF_YEAR_TOT = (EMP_YEAR_TOT) + (BNK_YEAR_TOT) + (INS_YEAR_TOT);
           //     EMP_TOT = (PF_EMP_CONT) + (EMP_YEAR_TOT) - (F_PREMIUM);
           //     BNK_TOT = (PF_BNK_CONT) + (BNK_YEAR_TOT);
           //     INS_TOT = (PF_INTEREST) + (INS_YEAR_TOT);
           //     PF_TOT = (EMP_TOT) + (BNK_TOT) + (INS_TOT);




           //     txtyearPF.Text = PF_YEAR_TOT.ToString();
           //     txtyearIns.Text = INS_YEAR_TOT.ToString();
           //     if (txtyearIns.Text == "0" || txtyearIns.Text == null)
           //     {
           //         txtyearIns.Text = "";
           //     }


           //     txtTotalEmp.Text = EMP_TOT.ToString();
           //     txtTotBnk.Text = BNK_TOT.ToString();
           //     txtTotIns.Text = INS_TOT.ToString();
           //     txtTotalPF.Text = PF_TOT.ToString();

           //     #region PostChng

           //     Result rslt1;
           //     Dictionary<string, object> param1 = new Dictionary<string, object>();
           //     param1.Add("PF_PR_P_NO", txtPersonnel.Text);
           //     rslt1 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PostChng", param1);
           //     if (rslt.isSuccessful)
           //     {
           //         if (rslt1.dstResult.Tables.Count > 0 && rslt1.dstResult.Tables[0].Rows.Count > 0)
           //         {
           //             txtBranch.Text = rslt1.dstResult.Tables[0].Rows[0].ItemArray.ToString() == "" ? "" : rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
           //         }
           //     }
           //     #endregion

          



           //     ValidateProc1 = false;
           // }





        }     
        #endregion

        private void txtPersonnel_Leave(object sender, EventArgs e)
        {

                   }

        private void CHRIS_PFund_PFundQuery_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "prpNo" || actionType == "prpNoExists")
            {
                ValidateProc1 = true;
            }
        }

        private void txtPersonnel_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8) || (e.KeyChar ==(char)13))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }


        private void CheckKeys(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                return;
            }
        }

        private void txtPersonnel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
         {
            if (e.KeyCode == Keys.Enter)
            {
                
                return;
                txtPersonnel2.Focus();  
            }

            if (e.KeyCode == Keys.Tab)
            {
                return;
                txtPersonnel2.Focus();
            }
        }

        public static string SetValuePersonalNo = "";
        private void btnPFCalculate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPersonnel.Text))
            {
                MessageBox.Show("Please Enter Personnel Number .", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SetValuePersonalNo = txtPersonnel.Text;
            CHRIS_PFund_PFundSummary PFundCalculation = new CHRIS_PFund_PFundSummary();
            PFundCalculation.ShowDialog();

        }
    }



    
}