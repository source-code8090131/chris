namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_GlobalParameterSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_GlobalParameterSetup));
            this.slPnlTabulerDDSParam = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDgvParameterValues = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.soeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.globalParamID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbFilter = new System.Windows.Forms.GroupBox();
            this.cmbParamName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlTabulerDDSParam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDgvParameterValues)).BeginInit();
            this.gbFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(416, 0);
            this.txtOption.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(452, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 434);
            this.panel1.Size = new System.Drawing.Size(452, 60);
            // 
            // slPnlTabulerDDSParam
            // 
            this.slPnlTabulerDDSParam.AutoSize = true;
            this.slPnlTabulerDDSParam.ConcurrentPanels = null;
            this.slPnlTabulerDDSParam.Controls.Add(this.slDgvParameterValues);
            this.slPnlTabulerDDSParam.DataManager = null;
            this.slPnlTabulerDDSParam.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlTabulerDDSParam.DependentPanels = null;
            this.slPnlTabulerDDSParam.DisableDependentLoad = true;
            this.slPnlTabulerDDSParam.EnableDelete = true;
            this.slPnlTabulerDDSParam.EnableInsert = true;
            this.slPnlTabulerDDSParam.EnableQuery = false;
            this.slPnlTabulerDDSParam.EnableUpdate = true;
            this.slPnlTabulerDDSParam.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GlobalParameterMappingCommand";
            this.slPnlTabulerDDSParam.Location = new System.Drawing.Point(30, 128);
            this.slPnlTabulerDDSParam.MasterPanel = null;
            this.slPnlTabulerDDSParam.Name = "slPnlTabulerDDSParam";
            this.slPnlTabulerDDSParam.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlTabulerDDSParam.Size = new System.Drawing.Size(397, 300);
            this.slPnlTabulerDDSParam.SPName = "CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER";
            this.slPnlTabulerDDSParam.TabIndex = 10;
            // 
            // slDgvParameterValues
            // 
            this.slDgvParameterValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.slDgvParameterValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.soeID,
            this.Value,
            this.id,
            this.globalParamID});
            this.slDgvParameterValues.ColumnToHide = null;
            this.slDgvParameterValues.ColumnWidth = null;
            this.slDgvParameterValues.CustomEnabled = true;
            this.slDgvParameterValues.DisplayColumnWrapper = null;
            this.slDgvParameterValues.GridDefaultRow = 0;
            this.slDgvParameterValues.Location = new System.Drawing.Point(15, 12);
            this.slDgvParameterValues.Name = "slDgvParameterValues";
            this.slDgvParameterValues.ReadOnlyColumns = null;
            this.slDgvParameterValues.RequiredColumns = "VALUE";
            this.slDgvParameterValues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.slDgvParameterValues.Size = new System.Drawing.Size(379, 272);
            this.slDgvParameterValues.SkippingColumns = null;
            this.slDgvParameterValues.TabIndex = 2;
            this.slDgvParameterValues.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.slDgvParameterValues_RowEnter);
            // 
            // soeID
            // 
            this.soeID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.soeID.DataPropertyName = "SOE_ID";
            this.soeID.HeaderText = "Soe Id";
            this.soeID.MaxInputLength = 15;
            this.soeID.Name = "soeID";
            this.soeID.Width = 120;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.DataPropertyName = "GLOBAL_PARAM_VALUE";
            this.Value.HeaderText = "Value";
            this.Value.MaxInputLength = 500;
            this.Value.Name = "Value";
            // 
            // id
            // 
            this.id.DataPropertyName = "ID";
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // globalParamID
            // 
            this.globalParamID.DataPropertyName = "GLOBAL_PARAM_ID";
            this.globalParamID.HeaderText = "Global Parameter ID";
            this.globalParamID.Name = "globalParamID";
            this.globalParamID.ReadOnly = true;
            this.globalParamID.Visible = false;
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.cmbParamName);
            this.gbFilter.Controls.Add(this.label2);
            this.gbFilter.Location = new System.Drawing.Point(45, 75);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(379, 53);
            this.gbFilter.TabIndex = 12;
            this.gbFilter.TabStop = false;
            // 
            // cmbParamName
            // 
            this.cmbParamName.CausesValidation = false;
            this.cmbParamName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParamName.FormattingEnabled = true;
            this.cmbParamName.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.cmbParamName.Location = new System.Drawing.Point(105, 22);
            this.cmbParamName.Name = "cmbParamName";
            this.cmbParamName.Size = new System.Drawing.Size(173, 21);
            this.cmbParamName.TabIndex = 7;
            this.toolTip1.SetToolTip(this.cmbParamName, "Select Parameter Name");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Paramter Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(137, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "Global Parameters Setup";
            // 
            // CHRIS_Setup_GlobalParameterSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 494);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbFilter);
            this.Controls.Add(this.slPnlTabulerDDSParam);
            this.CurrentPanelBlock = "slPnlTabulerDDSParam";
            this.Name = "CHRIS_Setup_GlobalParameterSetup";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Setup_GlobalParameterSetup";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPnlTabulerDDSParam, 0);
            this.Controls.SetChildIndex(this.gbFilter, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlTabulerDDSParam.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDgvParameterValues)).EndInit();
            this.gbFilter.ResumeLayout(false);
            this.gbFilter.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPnlTabulerDDSParam;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDgvParameterValues;
        private System.Windows.Forms.GroupBox gbFilter;
        private System.Windows.Forms.ComboBox cmbParamName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn soeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn globalParamID;
    }
}