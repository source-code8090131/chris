﻿using iCORE.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_DeptCostCenterEntry : Form
    {
        public CHRIS_Setup_DeptCostCenterEntry()
        {
            InitializeComponent();
        }

        private void btnChkData_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        #region Application logs

        StreamWriter log;
        FileStream fileStream = null;
        DirectoryInfo logDirInfo = null;
        FileInfo logFileInfo;

        FileInfo logFileInfoX;
        FileInfo logFileCust;
        FileInfo MDFileInfo;
        #endregion

        private void btnUploadPath_Click(object sender, EventArgs e)
        {
            string filePath = string.Empty;
            string fileExt = string.Empty;
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filePath = file.FileName;
                fileExt = Path.GetExtension(filePath);
                if (fileExt.CompareTo(".xlsx") == 0 || fileExt.CompareTo(".xls") == 0)
                    textBxConsumerFile.Text = filePath;
                else
                    MessageBox.Show("Please choose .xlsx and .xls file only.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnStartProcess_Click(object sender, EventArgs e)
        {
            //btnChkData.Visible = false;
            string userID = "";// (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                                      // (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            if (string.IsNullOrEmpty(textBxConsumerFile.Text))
            {
                MessageBox.Show("Please Select File for Upload.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string logFilePath = "C:\\iCORE-Spool\\";
            logFilePath = logFilePath + "Log Cost Center -" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);

            log.WriteLine("************************* Cost Center Uploading to run : " + DateTime.Now);

            try
            {
                CheckExcellProcesses("excel");
                DataTable mPayment = GetMasterDataWorkSheet(1, log);
                KillExcel("excel");

                DataRow[] dtr = mPayment.Select("DeptCode not like 'DeptCode'");
                foreach (System.Data.DataRow drow in dtr)
                {
                    string paymentT = string.IsNullOrEmpty(drow["CostCode"].ToString().Trim()) ? "" : drow["CostCode"].ToString().Trim();
                    if (paymentT.ToLower().Contains("cost") || paymentT.Equals("-"))
                        drow.Delete();
                }
                mPayment.AcceptChanges();

                #region Convert excel to table
                DataTable genLinkFile = new DataTable();
                genLinkFile.Columns.Add("DeptCode", typeof(string));
                genLinkFile.Columns.Add("CostCode", typeof(string));
                genLinkFile.Columns.Add("userID", typeof(string));

                DataRow genLinkRow;

                string Detail0 = string.Empty;
                string Detail4 = string.Empty;
                string Detail12 = string.Empty;
                string Detail16 = string.Empty;
                string Detail20 = string.Empty;

                //"DeptCode", "CostCode",

                foreach (System.Data.DataRow dtrow in mPayment.Rows)
                {
                    genLinkRow = genLinkFile.NewRow();

                    genLinkRow["DeptCode"] = string.IsNullOrEmpty(dtrow["DeptCode"].ToString().Trim()) ? "" : dtrow["DeptCode"].ToString().Trim();
                    genLinkRow["CostCode"] = string.IsNullOrEmpty(dtrow["CostCode"].ToString().Trim()) ? "" : dtrow["CostCode"].ToString().Trim();

                    genLinkRow["userID"] = userID;
                    // genLinkRow["SourceCode"] = string.IsNullOrEmpty(dtrow["SourceCode"].ToString().Trim()) ? "" : dtrow["SourceCode"].ToString().Trim();
                    genLinkFile.Rows.Add(genLinkRow);
                }

                #endregion

                uploadMasterData(genLinkFile);


                #region Update Table 

                //DataTable getDept = new DataTable();
                //string deptName = string.Empty;
                //string CostCode = string.Empty;

                //getDept = SQLManager.ExecutegetDept().Tables[0];

                //DataView view = new DataView(getDept);
                //System.Data.DataTable distinctValues = view.ToTable(true, "SP_DEPT");
                //foreach (System.Data.DataRow drowx in distinctValues.Rows)
                //{
                //    deptName = string.IsNullOrEmpty(drowx["SP_DEPT"].ToString().Trim()) ? "" : drowx["SP_DEPT"].ToString().Trim();

                //    if (!deptName.Contains("'"))
                //    {
                //        DataRow[] findDept = genLinkFile.Select("DeptCode like '" + deptName + "'");
                //        foreach (System.Data.DataRow drow in findDept)
                //        {
                //            CostCode = string.IsNullOrEmpty(drow["CostCode"].ToString().Trim()) ? "" : drow["CostCode"].ToString().Trim();
                //            log.WriteLine("Cost Center Uploading to run : " + DateTime.Now + " Cost Center Code"+ CostCode + " And Dept Code : " + deptName);

                //            SQLManager.ExecuteUpdateDept("DEPT", deptName, CostCode);

                //        }

                //    }

                //}

                #endregion

                DataTable dt = new DataTable();
                userID = "";
                dt = SQLManager.ExecutegetDept_CostCodeData(userID).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dGVCostCenter.DataSource = dt;
                    dGVCostCenter.Update();
                    dGVCostCenter.Visible = true;
                }
                else
                {
                    dGVCostCenter.Visible = false;
                }

                MessageBox.Show("Successfull Upload Data");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : Please check your attached file containing additional or mismatch information which is validate the agreed template.");
                log.WriteLine("Cost Center " + DateTime.Now + " Exception :  " + ex.ToString());
                log.WriteLine("*************************************** IBFT Cost Center " + DateTime.Now + " ******************************************** ");
                log.Close();
                return;
            }

            log.WriteLine("Cost Center " + DateTime.Now + " Process Complete  ");
            log.WriteLine("*************************************** IBFT Cost Center " + DateTime.Now + " ******************************************** ");
            log.Close();
        }

        static Hashtable myHashtable;

        private static void CheckExcellProcesses(string filename)
        {
            try
            {
                Process[] AllProcesses = Process.GetProcessesByName(filename);
                myHashtable = new Hashtable();
                int iCount = 0;

                foreach (Process ExcelProcess in AllProcesses)
                {
                    myHashtable.Add(ExcelProcess.Id, iCount);
                    iCount = iCount + 1;
                }
            }
            catch (Exception exp)
            {

            }
        }

        private static void KillExcel(string filename)
        {
            try
            {
                //Console.WriteLine("Closing Excel Processes...");
                Process[] AllProcesses = Process.GetProcessesByName(filename);

                // check to kill the right process
                foreach (Process outlookProcess in AllProcesses)
                {
                    if (myHashtable.ContainsKey(outlookProcess.Id) == true)
                        outlookProcess.Kill();
                }

                AllProcesses = null;
            }
            catch (System.Exception exp)
            {

            }
        }

        public static void uploadMasterData(DataTable dt)
        {
            try
            {
                SQLManager.ExecuteCostCenterDetailUpload(dt);
            }
            catch (Exception ex)
            {

            }
        }

        public DataTable GetMasterDataWorkSheet(int workSheetID, StreamWriter log)
        {
            log.WriteLine("Cost Center" + DateTime.Now + "****************************  Start Reading Excel file ***********************************");
            //string pathOfExcelFile = "";
            DataTable dt = new DataTable();

            Excel.Application excelApp = new Excel.Application();
            excelApp.DisplayAlerts = false; //Don't want Excel to display error messageboxes
            Excel.Workbook workbook = excelApp.Workbooks.Open(textBxConsumerFile.Text, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing); //This opens the file
            Excel.Worksheet sheet = (Excel.Worksheet)workbook.Sheets.get_Item(workSheetID); //Get the first sheet in the file

            try
            {
                int lastRow = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Row;
                log.WriteLine("Cost Center" + DateTime.Now + "Total Number of Rows : " + lastRow);
                int lastColumn = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Column;
                log.WriteLine("Cost Center" + DateTime.Now + "Total Number of Columns : " + lastColumn);
                Excel.Range oRange = sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[lastRow, lastColumn]);//("A1",lastColumnIndex + lastRow.ToString());
                oRange.EntireColumn.AutoFit();

                string[] ManualColumn = { "DeptCode", "CostCode", };

                log.WriteLine("Cost Center" + DateTime.Now + "Columns reading : " + lastColumn);
                for (int i = 0; i < oRange.Columns.Count; i++)
                {
                    dt.Columns.Add(ManualColumn[i].ToString());
                }

                object[,] cellValues = (object[,])oRange.Value2;
                object[] values = new object[lastColumn];

                log.WriteLine("Cost Center" + DateTime.Now + "Start reading data : ");
                for (int i = 1; i <= lastRow; i++)
                {

                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        values[j] = cellValues[i, j + 1] == null ? "-" : cellValues[i, j + 1];
                    }
                    dt.Rows.Add(values);
                }
                log.WriteLine("Cost Center" + DateTime.Now + "Excel file read finished");
            }
            catch (Exception ex)
            {
                log.WriteLine("Cost Center" + DateTime.Now + "Exception : " + ex.ToString());
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                excelApp.Quit();
            }
            return dt;

        }
    }
}
