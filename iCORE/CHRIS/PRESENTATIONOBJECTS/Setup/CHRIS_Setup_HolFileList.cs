using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_HolFileList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_Setup_HolFileList()
        {
            InitializeComponent();
        }


        public CHRIS_Setup_HolFileList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            //this.copies.Text = "1";
        }
                

        private void btnRun_Click(object sender, EventArgs e)
        {
            String DestName = @"C:\iCORE-Spool\Report";

            base.RptFileName = "SPREP09";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport();
            }
            else if (cmbDescType.Text == "File")
            {
                base.ExportCustomReport(DestName, "PDF");
            }

            else if (cmbDescType.Text == "Mail")
            {
                base.EmailToReport(DestName ,"PDF");
            }
        }

        private void CHRIS_Setup_HolFileList_Load(object sender, EventArgs e)
        {

        }

       

              






    }
}