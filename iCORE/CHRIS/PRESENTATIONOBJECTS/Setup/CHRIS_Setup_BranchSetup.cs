using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_BranchSetup : ChrisSimpleForm
    {
        public CHRIS_Setup_BranchSetup()
        {
           
            InitializeComponent();
        }


        public CHRIS_Setup_BranchSetup(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            txtUserName.Text += "   " + this.UserName;
            this.CurrentPanelBlock = this.pnlDetail.Name;
            this.txtBranchCode.Select();
            this.txtBranchCode.Focus();
        }



        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
       
            this.ShowOptionTextBox = false;
            txtUserName.Text = "User Name: " + this.UserName;
            this.txtUser.Text = this.userID;
            this.txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");

            tbtAdd.Visible = false;
            this.FunctionConfig.EnableF4 = true;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F4 = Function.Save;
            this.FunctionConfig.F6 = Function.Quit;
           // this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;

            tbtDelete.Enabled = false; 
            this.CurrentPanelBlock = this.pnlDetail.Name;
            lbtnbrnCode.SkipValidationOnLeave = true;
            
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BranchCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BranchCommand)this.pnlDetail.CurrentBusinessEntity;
           
                      
            //if (ent != null)
            //{
            //    ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
            //}

            //this.txtBranchName.SkipValidation = true;
            //this.txtBranchAdd1.SkipValidation = true;
            //this.txtBranchAdd2.SkipValidation = true;
            //this.txtBranchAdd2.SkipValidation = true;
            //this.txtCrAccount.SkipValidation = true;
            //this.txtDrAccount.SkipValidation = true;

            if (txtBranchCode.Text == "" || txtBranchName.Text=="" || txtBranchAdd1.Text =="" || txtCrAccount.Text=="" || txtDrAccount.Text==""  )
            {
                return;
            }
 
            txtBranchCode.SkipValidation = true;
            base.SkipLovValidationOnSave = false;
            txtBranchCode.IsRequired = false;
            txtBranchName.IsRequired = false;
            txtBranchAdd1.IsRequired = false;
            txtCrAccount.IsRequired = false;
            txtDrAccount.IsRequired = false;


            base.DoToolbarActions(ctrlsCollection, actionType);
         
            txtBranchCode.IsRequired = true;
            txtBranchName.IsRequired = true;
            txtBranchAdd1.IsRequired = true;
            txtCrAccount.IsRequired = true;
            txtDrAccount.IsRequired = true;
            if (actionType == "Save")
            {
                base.ClearForm(pnlDetail.Controls);

                txtBranchName.SkipValidation = true;
                txtBranchAdd1.SkipValidation = true;
                txtBranchAdd2.SkipValidation = true;
                txtBranchAdd3.SkipValidation = true;
                txtCrAccount.SkipValidation = true;
                txtDrAccount.SkipValidation = true;
                txtBranchCode.Focus();
                return;
            }
            //if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit && this.FunctionConfig.CurrentOption == Function.Modify)
            //{
            //    this.lbtnbrnCode.SkipValidationOnLeave = false;
            //}

            //else if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
            //{
            //    this.lbtnbrnCode.SkipValidationOnLeave = true;
            //}

            //base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Cancel")
            {
                txtBranchCode.Focus(); 
            }
           


        }



        private DataTable GetBranchCount(string txtbrnCode)
        {

            DataTable dt = null;

            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }

        private void txtBranchCode_Validated(object sender, EventArgs e)
        {
            //DataTable dt;
            //if (this.txtBranchCode.Text.Length > 0 && this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
            //{
            //    dt = GetBranchCount(txtBranchCode.Text);
            //    if (dt != null)
            //    {
            //        dt.DefaultView.RowFilter = string.Format("BRN_CODE='{0}'", txtBranchCode.Text);
            //        if (dt.DefaultView.Count > 0)
            //        {
            //            MessageBox.Show("Invaild Branch Code Enter Try again...");
            //            txtBranchCode.Focus();
            //        }
            //    }
            //}
        }

        private void txtBranchCode_Leave(object sender, EventArgs e)
        {

            if (this.tbtClose.Pressed)
                return;


            tbtDelete.Enabled = true;
            DataTable dt;
            if (this.txtBranchCode.Text.Length > 0)// && this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
            {
                dt = GetBranchCount(txtBranchCode.Text);
                if (dt != null)
                {
                    dt.DefaultView.RowFilter = string.Format("BRN_CODE='{0}'", txtBranchCode.Text);
                    if (dt.DefaultView.Count > 0)
                    {
                        //MessageBox.Show("Invaild Branch Code Enter Try again...");
                        //txtBranchCode.Focus();
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                    }
                    else
                    {
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                    }
                }
                else
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                }
            }


      }

        private void CHRIS_Setup_BranchSetup_Load(object sender, EventArgs e)
        {

        }

      

       

      



    }
}