using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_DesignationEntry : ChrisSimpleForm
    {
        char flag = 'N';

        DataTable dt = new DataTable();
        Dictionary<String, Object> objValues = new Dictionary<String, Object>();
        Result rslt;
        CmnDataManager cmnDM = new CmnDataManager();

        # region constructor
        public CHRIS_Setup_DesignationEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Setup_DesignationEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();


        }
        #endregion
        # region Methods
        private bool isFormEmpty()
        {

            if (txtDesg.Text == string.Empty && txtCategory.Text == string.Empty &&
                txtCatDesc.Text == string.Empty && txtBranchcode.Text == string.Empty &&
                txtBrnName.Text == string.Empty && txtLevel.Text == string.Empty &&
                txtDesgDesc1.Text == string.Empty && txtDesgDesc2.Text == string.Empty &&
                txtSp_current.Text == string.Empty && txtMin.Text == string.Empty &&
                txtMid.Text == string.Empty && txtMax.Text == string.Empty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetFormReadonly(bool check)
        {
            txtBranchcode.ReadOnly  = check;
            txtBrnName.ReadOnly     = check;
            txtCatDesc.ReadOnly     = check;
            txtCategory.ReadOnly    = check;
            txtConfPeriod.ReadOnly  = check;
            txtDesgDesc1.ReadOnly   = check;
            txtDesgDesc2.ReadOnly   = check;
            txtIncAmt.ReadOnly      = check;
            txtLevel.ReadOnly       = check;
            txtMax.ReadOnly         = check;
            txtMid.ReadOnly         = check;
            txtMin.ReadOnly         = check;
            txtSp_current.ReadOnly  = check;
            txtBranchcode.ReadOnly  = check;
            DtEffectiveFrm.Enabled  = !check;


            txtBranchcode.CustomEnabled = !check;
            txtBrnName.CustomEnabled = !check;
            txtCatDesc.CustomEnabled = !check;
            txtCategory.CustomEnabled = !check;
            txtConfPeriod.CustomEnabled = !check;
            txtDesgDesc1.CustomEnabled = !check;
            txtDesgDesc2.CustomEnabled = !check;
            txtIncAmt.CustomEnabled = !check;
            txtLevel.CustomEnabled = !check;
            txtMax.CustomEnabled = !check;
            txtMid.CustomEnabled = !check;
            txtMin.CustomEnabled = !check;
            txtSp_current.CustomEnabled = !check;
            txtBranchcode.CustomEnabled = !check;
            DtEffectiveFrm.CustomEnabled = !check;


        }

        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);
            //List<SLPanel> lstConcurrentPnls = new List<SLPanel>();
            //lstConcurrentPnls.Add(PnlDesigConcurr);
            //PnlDesig.ConcurrentPanels = lstConcurrentPnls;            
            txtOption.Visible = false;
            this.label17.Text += " " + this.UserName;
            DtEffectiveFrm.Value = null;
            this.tbtAdd.Visible = false;
            this.tbtList.Visible = false;

            this.PnlDesig.Enabled = false;


        }
        
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                bool dd = isFormEmpty();
                if (dd)
                {
                    return;

                }
            }

            if (actionType == "Cancel")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                DtEffectiveFrm.Value = null;
                this.PnlDesig.Enabled = false;
                return;

            }


            base.DoToolbarActions(ctrlsCollection, actionType);

        }

        protected override bool Add()
        {
            base.Add();
            this.FunctionConfig.CurrentOption = Function.Add;
            return false;
        }
        #endregion
        #region Events
        private void txtMid_Validating(object sender, CancelEventArgs e)
        {
            if (txtMid.Text != string.Empty)
            {

                double sp_mid = double.Parse(txtMid.Text);
                double sp_min = double.Parse(txtMin.Text);

                if (sp_mid < sp_min)
                {
                    MessageBox.Show("Mid value must be greater than Min Value.");
                    e.Cancel = true;

                }

            }
        }
        private void BtnInsert_Click(object sender, EventArgs e)
        {

            if (this.PnlDesig.Enabled == false)
            {
                this.PnlDesig.Enabled = true;
                //return;
            }

            GetFormReadonly(false);

            this.Add();
            this.LbDesig.SkipValidationOnLeave = true;
            this.LBCat.SkipValidationOnLeave = true;
            this.Lov_BRN.SkipValidationOnLeave = true;
            base.ClearForm(PnlDesig.Controls);
            this.LbDesig.ActionType = "Lov_DSG";
            txtDesg.Select();
            txtDesg.Focus();
            //   base.ClearForm(PnlDesigConcurr.Controls);

        }
        private void btnView_Click(object sender, EventArgs e)
        {
            if (this.PnlDesig.Enabled == false)
            {
                this.PnlDesig.Enabled = true;
               // return;
            }

            flag = 'Y';
            this.View();
            this.LbDesig.ActionType = "GetAll_Columns";
            txtDesg.Select();
            txtDesg.Focus();
        }
        private void LbDesig_MouseDown(object sender, MouseEventArgs e)
        {
            if (flag == 'N')
            {
                this.LbDesig.Enabled = false;
                this.LbDesig.Enabled = true;
            }
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                this.LbDesig.Enabled = false;
                this.LbDesig.Enabled = true;
            }


        }
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (this.PnlDesig.Enabled == false)
            {
                this.PnlDesig.Enabled = true;
                
                //return;
            }
            GetFormReadonly(false);
            flag = 'Y';
            this.Cancel();
            this.PnlDesig.Enabled = true;
            this.Edit();
            this.LbDesig.ActionType = "GetAll_Columns";
            txtDesg.Select();
            txtDesg.Focus();
            this.DtEffectiveFrm.Value = null;
            //  base.ClearForm(PnlDesigConcurr.Controls);
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (this.PnlDesig.Enabled == false)
            {
                this.PnlDesig.Enabled = true;
                //return;
            }

            flag = 'Y';
            this.Delete();
            this.LbDesig.ActionType = "GetAll_Columns";
            base.DoToolbarActions(this.PnlDesig.Controls, "Delete");
            base.ClearForm(PnlDesig.Controls);
            txtDesg.Select();
            txtDesg.Focus();
            //   base.ClearForm(PnlDesigConcurr.Controls);
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (this.PnlDesig.Enabled == false)
            {
                this.PnlDesig.Enabled = true;
                return;
            }

            if (FunctionConfig.CurrentOption == Function.Add)
            {
                if (txtDesg.Text != string.Empty)
                {

                    this.SkipLovValidationOnSave = true;
                    base.DoToolbarActions(this.Controls, "Save");
                    base.ClearForm(PnlDesig.Controls);
                    DtEffectiveFrm.Value = null;
                    FunctionConfig.CurrentOption = Function.None;
                    this.PnlDesig.Enabled = false;
                }
            }

            if (FunctionConfig.CurrentOption == Function.Delete)
            {
                if (txtDesg.Text != string.Empty)
                {
                    this.SkipLovValidationOnSave = true;
                    base.DoToolbarActions(this.Controls, "Delete");
                    base.ClearForm(PnlDesig.Controls);
                    DtEffectiveFrm.Value = null;
                    FunctionConfig.CurrentOption = Function.None;
                    this.PnlDesig.Enabled = false;
                }
            }

            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                if (txtDesg.Text != string.Empty)
                {
                    this.SkipLovValidationOnSave = true;
                    base.DoToolbarActions(this.Controls, "Save");
                    base.ClearForm(PnlDesig.Controls);
                    DtEffectiveFrm.Value = null;
                    FunctionConfig.CurrentOption = Function.None;
                    this.PnlDesig.Enabled = false;
                }

            }
            txtDesg.Select();
            txtDesg.Focus();

        }
        private void BtnClear_Click(object sender, EventArgs e)
        {

            this.Cancel();
            DtEffectiveFrm.Value = null;
            txtDesg.Select();
            txtDesg.Focus();
            FunctionConfig.CurrentOption = Function.None;
        }
        private void CHRIS_Setup_DesignationEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            flag = 'Y';
            if (FunctionConfig.CurrentOption == Function.View)
            {
                GetFormReadonly(true);

            }

            //if (actionType == "GetAll_Columns")
            //{   txtIncAmt.Text = txtSp_incremnt_hidden.Text;
            //    txtMax.Text = txtMAxHidden.Text;
            //    txtMid.Text=txtMidHidden.Text;
            //    txtMin.Text=txtMinHidden.Text;
            //    DtEffectiveFrm.Value = dtsp_effecvtive_hidden.Value;           
            //}
        }
        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));

        }
        private void LBCat_MouseDown(object sender, MouseEventArgs e)
        {
            if (flag == 'N')
            {
                this.LBCat.Enabled = false;
                this.LBCat.Enabled = true;

            }
        }
        private void Lov_BRN_MouseDown(object sender, MouseEventArgs e)
        {
            if (flag == 'N')
            {
                this.Lov_BRN.Enabled = false;
                this.Lov_BRN.Enabled = true;

            }
        }
        private void txtMid_Leave(object sender, EventArgs e)
        {

            if (txtMid.Text != "" && txtMin.Text != "")
            {

                if (Convert.ToDecimal(txtMin.Text) > Convert.ToDecimal(txtMid.Text))
                {
                    MessageBox.Show("Mid value must be greater than Min Value.");
                    txtMid.Focus();
                    return;
                }

            }
        }
        #endregion

        private void txtCategory_Leave(object sender, EventArgs e)
        {

            if (txtCategory.Text != "")
            {


                this.objValues.Clear();
                this.objValues.Add("SP_CATEGORY", txtCategory.Text);

                rslt = cmnDM.GetData("CHRIS_SP_DESIG_MANAGER", "GetDesc", objValues);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dt = rslt.dstResult.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            txtCatDesc.Text = Convert.ToString(dt.Rows[0]["SP_DESC"]);
                        }
                    }
                    else
                    {

                        txtCatDesc.Text = "";
                    }
                }

            }
        }

        private void txtBranchcode_Leave(object sender, EventArgs e)
        {

            if (txtBranchcode.Text != "")
            {


                this.objValues.Clear();
                this.objValues.Add("SP_BRANCH", txtBranchcode.Text);

                rslt = cmnDM.GetData("CHRIS_SP_DESIG_MANAGER", "GetBranchName", objValues);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dt = rslt.dstResult.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            txtBrnName.Text = Convert.ToString(dt.Rows[0]["BRN_NAME"]);
                        }
                    }
                    else
                    {

                        txtBrnName.Text = "";
                    }
                }

            }



        }




    }
}