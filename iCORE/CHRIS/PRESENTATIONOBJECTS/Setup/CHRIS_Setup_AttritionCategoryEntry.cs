using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_AttritionCategoryEntry : ChrisTabularForm
    {
        #region Constructors
        decimal temp = 0;

        public CHRIS_Setup_AttritionCategoryEntry()
        {
            InitializeComponent();
        }


        public CHRIS_Setup_AttritionCategoryEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.pnlHead.SendToBack();
            this.txtOption.Select();
            this.txtOption.Focus();
            this.txtOption.Visible = false;
            txtUserName.Text += "   " + this.UserName;

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            //this.txtUserName.Text = "User Name:  " + this.UserName;
            ATTR_CATEGORY_CODE.Width = 210;
            ATTR_CATEGORY_DESC.Width = 435;
            this.ATTR_CATEGORY_CODE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.ATTR_CATEGORY_DESC.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);

            ATTR_CATEGORY_DESC.SkipValidationOnLeave = true;


        }
        #endregion



        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            this.DGVAttrition.EndEdit();
            this.DGVAttrition.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (actionType == "Save")
            {

                DataTable dt = (DataTable)DGVAttrition.DataSource;

                if (dt != null)
                {

                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);

                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);

                    if (dtAdded != null || dtUpdated != null)
                    {


                        for (int j = 0; j < DGVAttrition.Rows.Count -1; j++)
                        {
                            if (Convert.ToString(dt.Rows[j]["ATTR_CATEGORY_DESC"]) == "")
                            {
                                DGVAttrition.Focus();
                                return;
                            }

                        }



                        DialogResult dr = MessageBox.Show("Do You Want to Save Changes.", "", MessageBoxButtons.YesNo,

                        MessageBoxIcon.Question);

                        if (dr == DialogResult.No)
                        {

                            return;

                        }
                        else
                        {
                            this.DGVAttrition.SaveGrid(this.pnlTabular);
                            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
                            return;
                        }

                    }

                }
                return;
            }

            if (actionType != "Close" && actionType != "Cancel" && this.txtOption.Focused)
            {
                return;
            }
           



            base.DoToolbarActions(ctrlsCollection, actionType);

        }

        private void DGVAttrition_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (e.ColumnIndex == 1)
            {
                if (DGVAttrition.CurrentRow.Cells[1].IsInEditMode || DGVAttrition.CurrentRow.Cells[1].FormattedValue.ToString() !="" )
                {
                    //if (!(Decimal.TryParse(DGVAttrition.CurrentRow.Cells[0].EditedFormattedValue.ToString(), out temp)))
                    //{
                    //    e.Cancel = true;
                    //    return;

                    //}
                }
               
            }

        }

        private void DGVAttrition_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 1)
            {
                if (DGVAttrition.CurrentRow.Cells[1].IsInEditMode || DGVAttrition.CurrentRow.Cells[1].FormattedValue.ToString() !="")
                {
                    if (!(Decimal.TryParse(DGVAttrition.CurrentRow.Cells[0].EditedFormattedValue.ToString(), out temp)))
                    {
                        
                        return;

                    }
                }

            }

        }

 

    }
}