﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_DeptCostCenterEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dGVCostCenter = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnChkData = new System.Windows.Forms.Button();
            this.btnStartProcess = new System.Windows.Forms.Button();
            this.btnUploadPath = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBxConsumerFile = new System.Windows.Forms.TextBox();
            this.folderBrwDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dGVCostCenter)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dGVCostCenter
            // 
            this.dGVCostCenter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVCostCenter.Location = new System.Drawing.Point(12, 175);
            this.dGVCostCenter.Name = "dGVCostCenter";
            this.dGVCostCenter.ReadOnly = true;
            this.dGVCostCenter.RowTemplate.Height = 24;
            this.dGVCostCenter.Size = new System.Drawing.Size(293, 349);
            this.dGVCostCenter.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnChkData);
            this.panel1.Controls.Add(this.btnStartProcess);
            this.panel1.Controls.Add(this.btnUploadPath);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.textBxConsumerFile);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 157);
            this.panel1.TabIndex = 1;
            // 
            // btnChkData
            // 
            this.btnChkData.Location = new System.Drawing.Point(441, 117);
            this.btnChkData.Margin = new System.Windows.Forms.Padding(4);
            this.btnChkData.Name = "btnChkData";
            this.btnChkData.Size = new System.Drawing.Size(93, 32);
            this.btnChkData.TabIndex = 75;
            this.btnChkData.Text = "Close";
            this.btnChkData.UseVisualStyleBackColor = true;
            this.btnChkData.Click += new System.EventHandler(this.btnChkData_Click);
            // 
            // btnStartProcess
            // 
            this.btnStartProcess.Location = new System.Drawing.Point(319, 117);
            this.btnStartProcess.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartProcess.Name = "btnStartProcess";
            this.btnStartProcess.Size = new System.Drawing.Size(114, 31);
            this.btnStartProcess.TabIndex = 74;
            this.btnStartProcess.Text = "Run";
            this.btnStartProcess.UseVisualStyleBackColor = true;
            this.btnStartProcess.Click += new System.EventHandler(this.btnStartProcess_Click);
            // 
            // btnUploadPath
            // 
            this.btnUploadPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadPath.Location = new System.Drawing.Point(522, 74);
            this.btnUploadPath.Margin = new System.Windows.Forms.Padding(4);
            this.btnUploadPath.Name = "btnUploadPath";
            this.btnUploadPath.Size = new System.Drawing.Size(49, 25);
            this.btnUploadPath.TabIndex = 73;
            this.btnUploadPath.Text = "...";
            this.btnUploadPath.UseVisualStyleBackColor = true;
            this.btnUploadPath.Click += new System.EventHandler(this.btnUploadPath_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.citinewimage;
            this.pictureBox1.Location = new System.Drawing.Point(500, 4);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(86, 49);
            this.pictureBox1.TabIndex = 72;
            this.pictureBox1.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(18, 77);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 20);
            this.label26.TabIndex = 17;
            this.label26.Text = "Select File";
            // 
            // textBxConsumerFile
            // 
            this.textBxConsumerFile.Location = new System.Drawing.Point(114, 77);
            this.textBxConsumerFile.Margin = new System.Windows.Forms.Padding(4);
            this.textBxConsumerFile.Name = "textBxConsumerFile";
            this.textBxConsumerFile.ReadOnly = true;
            this.textBxConsumerFile.Size = new System.Drawing.Size(400, 22);
            this.textBxConsumerFile.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(216, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 22);
            this.label1.TabIndex = 76;
            this.label1.Text = "Cost Center Uploader";
            // 
            // CHRIS_Setup_DeptCostCenterEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(611, 530);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dGVCostCenter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Setup_DeptCostCenterEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_Setup_DeptCostCenterEntry";
            ((System.ComponentModel.ISupportInitialize)(this.dGVCostCenter)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dGVCostCenter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBxConsumerFile;
        private System.Windows.Forms.Button btnChkData;
        private System.Windows.Forms.Button btnStartProcess;
        private System.Windows.Forms.Button btnUploadPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrwDialog;
        private System.Windows.Forms.Label label1;
    }
}