namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_TestPayroll_ProcessDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtPrNum = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(552, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(588, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 107);
            this.panel1.Size = new System.Drawing.Size(588, 60);
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.txtPrNum);
            this.slPanelSimple1.Controls.Add(this.label1);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(41, 52);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(507, 52);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 10;
            this.slPanelSimple1.TabStop = true;
            // 
            // txtPrNum
            // 
            this.txtPrNum.AllowSpace = true;
            this.txtPrNum.AssociatedLookUpName = "";
            this.txtPrNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrNum.ContinuationTextBox = null;
            this.txtPrNum.CustomEnabled = true;
            this.txtPrNum.DataFieldMapping = "W_ANSWER";
            this.txtPrNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrNum.GetRecordsOnUpDownKeys = false;
            this.txtPrNum.IsDate = false;
            this.txtPrNum.Location = new System.Drawing.Point(318, 17);
            this.txtPrNum.MaxLength = 6;
            this.txtPrNum.Name = "txtPrNum";
            this.txtPrNum.NumberFormat = "###,###,##0.00";
            this.txtPrNum.Postfix = "";
            this.txtPrNum.Prefix = "";
            this.txtPrNum.Size = new System.Drawing.Size(90, 20);
            this.txtPrNum.SkipValidation = false;
            this.txtPrNum.TabIndex = 0;
            this.txtPrNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(74, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 39);
            this.label1.TabIndex = 9;
            this.label1.Text = "[YES] Exit with Save \n \n [NO] Exit without Save ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(227, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "PROCESS    COMPLETED";
            // 
            // CHRIS_Setup_TestPayroll_ProcessDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 167);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.slPanelSimple1);
            this.Name = "CHRIS_Setup_TestPayroll_ProcessDialog";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCORE CHRIS - TestPayrollProcessCompletion";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private CrplControlLibrary.SLTextBox txtPrNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}