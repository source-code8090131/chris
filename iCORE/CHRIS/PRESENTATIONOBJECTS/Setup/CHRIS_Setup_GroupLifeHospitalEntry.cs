using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_GroupLifeHospitalEntry : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        private ToolTip tp = new ToolTip();


        # region Constructor
        public CHRIS_Setup_GroupLifeHospitalEntry()
        {
            InitializeComponent();
        }


        public CHRIS_Setup_GroupLifeHospitalEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        # endregion      
        # region methods
        protected override bool Add()
        {
            this.lbTypeCod.ActionType = "Type_code";
            this.lbTypeCod.SkipValidationOnLeave = true;
            this.lbsatfftype.SkipValidationOnLeave = true;
            this.lbLevel.SkipValidationOnLeave = true;
            this.lbDesig.SkipValidationOnLeave = true;
            
            return base.Add();
        }
        void proc_delete()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_type_code",txtLifeIns.Text);
            param.Add("sp_staff_type",txtoffical.Text);
            param.Add("sp_desig_in",txtdesignation.Text);
            param.Add("sp_level_in",txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "proc_delete", param);
              
        }
        void proc_modify()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_type_code", txtLifeIns.Text);
            param.Add("sp_staff_type", txtoffical.Text);
            param.Add("sp_desig_in", txtdesignation.Text);
            param.Add("sp_level_in", txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "proc_modify", param); 
        
        }
        void proc_mod_Del()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_type_code", txtLifeIns.Text);
            param.Add("sp_staff_type", txtoffical.Text);
            param.Add("sp_desig_in", txtdesignation.Text);
            param.Add("sp_level_in", txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "proc_mod_Del", param);

            if (rslt.isSuccessful)
            { 
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {

                    txtLifeIns.Text=rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtoffical.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtnameIncGrp1.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    txtnameIncGrp2.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0][4].ToString() == string.Empty)
                    {
                        dtRenewal.Value = null;
                    }
                    else
                    {
                        dtRenewal.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString());
                    }
                    txtPolicyno.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString(); 
                    txtLifeCovg.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                    txtHospitalCovg.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString(); 
                    txtdesignation.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtLevel.Text= rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtMultiply.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();


                }
            
            }



        }
        void proc_view_add()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_type_code", txtLifeIns.Text);
            param.Add("sp_staff_type", txtoffical.Text);
            param.Add("sp_desig_in", txtdesignation.Text);
            param.Add("sp_level_in", txtLevel.Text);
            rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "proc_view_add", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {

                    txtLifeIns.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtoffical.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtnameIncGrp1.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    txtnameIncGrp2.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0][4].ToString() == string.Empty)
                    {
                        dtRenewal.Value = null;
                    }
                    else
                    {
                        dtRenewal.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString());
                    }
                    txtPolicyno.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    txtLifeCovg.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                    txtHospitalCovg.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtLevel.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtMultiply.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();


                }
                //else
                //{
                //    base.ClearForm(this.PnlIncGrp.Controls);
                //    txtOption.Focus();

                //}

                //DialogResult dRes = MessageBox.Show("Do You Want to View More Records [Y/N]", "Note"
                //                     , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (dRes == DialogResult.Yes)
                //{
                   
                //    base.ClearForm(this.PnlIncGrp.Controls);
                //    txtLifeIns.Select();
                //    txtLifeIns.Focus();
                //    return;
                    

                //}
                //else if (dRes == DialogResult.No)
                //{
                    
                //    base.ClearForm(this.PnlIncGrp.Controls);
                //    txtOption.Select();
                //    txtOption.Focus();
                //    return;
                    
                //}


            }
        
        
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                base.DoToolbarActions(ctrlsCollection, "Cancel");
                //dtRenewal.Value = null;
                return;
            }

            if (actionType == "List")
            {
                if (FunctionConfig.CurrentOption == Function.None)
                {
                    return;

                }
                else
                {
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    return;
                }
                   
            }
            if (actionType == "Save")
            {
                if (FunctionConfig.CurrentOption == Function.View || FunctionConfig.CurrentOption == Function.Delete)
                {
                    return;
                
                }
                else
                {
                    //COMMENTING IT INORDER TO STOP THE DOUBLE CONFIRMATION MESSAGE//
                    //base.DoToolbarActions(ctrlsCollection, actionType);
                }
            
            
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        
        
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            label19.Text += "   " + this.UserName;
            this.slTextBox1.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            //dtRenewal.Value = null;
            this.tbtAdd.Visible = false;
            this.tbtList.Visible = false;
            this.tbtDelete.Visible = false;
        }
        #endregion            
        #region Events




        private void txtLifeIns_Validating(object sender, CancelEventArgs e)
        {
            if ((txtLifeIns.Text.ToUpper() != "I") && (txtLifeIns.Text.ToUpper() != "H") && txtLifeIns.Text.ToUpper() != ""  )
            {
                MessageBox.Show("Invalid Entry, Press [I]nsurance Or [H]ospitalization....");
                e.Cancel = true;
            }
        }        
        private void txtoffical_Validating(object sender, CancelEventArgs e)
        {
            if ((txtoffical.Text.ToUpper() != "O") && (txtoffical.Text.ToUpper() != "U"))
            {
                MessageBox.Show("Invalid Entry, Press [O]fficials Or [U]nionized....");
                e.Cancel = true;
            }

        }      
        private void txtdesignation_Validating(object sender, CancelEventArgs e)
        {
            //Result rslt;
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //param.Clear();
            //param.Add("sp_desig_in",txtdesignation.Text == "" ? "0" : txtdesignation.Text);

            //if (txtoffical.Text.ToUpper() == "U")
            //{
            //    rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_desig_IfClause", param);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            //txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

            //        }
            //        else
            //        {
            //            MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
            //            txtdesignation.Select();
            //            txtdesignation.Focus();
            //        }
                
            //    }
            
            
            //}
            //else
            //{
            //    rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_desig_ElseClause", param);


            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            //txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

            //        }
            //        else
            //        {
            //            MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
            //            txtdesignation.Select();
            //            txtdesignation.Focus();
            //        }

            //    }


            //}


        }
        private void txtLevel_Validating(object sender, CancelEventArgs e)
        {
            # region key Next ITem

            if ((FunctionConfig.CurrentOption == Function.Modify) || (FunctionConfig.CurrentOption == Function.Delete))
            {
                proc_mod_Del();
            }

            if ((FunctionConfig.CurrentOption == Function.View) || (FunctionConfig.CurrentOption == Function.Add))
            {
                proc_view_add();
            }

            if ((FunctionConfig.CurrentOption == Function.Add) && txtnameIncGrp1.Text.ToString() != string.Empty)
            {
                MessageBox.Show("Record Already Entered");
                base.ClearForm(PnlIncGrp.Controls);
                //dtRenewal.Value = null;
                txtOption.Select();
                txtOption.Focus();
            }

            if (FunctionConfig.CurrentOption == Function.View)
            {
                if (MessageBox.Show("Do You Want To View this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    base.ClearForm(PnlIncGrp.Controls);
                    txtLifeIns.Select();
                    txtLifeIns.Focus();
                }
                else
                {
                    this.Cancel();
                    
                
                }
            }


            if (FunctionConfig.CurrentOption == Function.Delete)
            {

                if (MessageBox.Show("Do You Want To Delete this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    proc_delete();
                    this.ClearForm(PnlIncGrp.Controls);
                }
                else
                {
                    this.ClearForm(PnlIncGrp.Controls);
                    txtLifeIns.Focus(); 
                }


            }
            #endregion
            # region Validate ITem
            //Result rslt;
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //param.Add("sp_level_in", txtLevel.Text == "" ? "0" : txtLevel.Text);
            //param.Add("sp_desig_in", this.txtdesignation.Text == "" ? "0" : this.txtdesignation.Text);
            //if (txtLifeIns.Text.ToUpper() == "U")
            //{
            //    rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_Level_IfClause", param);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            //txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

            //        }
            //        else
            //        {
            //            MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
            //            if (txtdesignation.Text != "")
            //            {
            //                txtdesignation.Select();
            //                txtdesignation.Focus();
            //            }
                        
            //        }

            //    }

            //}

            //else 
            //{
            //    rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_Level_ElseClause", param);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            ///txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

            //        }
            //        else
            //        {
            //            MessageBox.Show("Invalid Entry... Press [F9] To Display The List");

            //            if (txtdesignation.Text != "")
            //            {
            //                txtdesignation.Select();
            //                txtdesignation.Focus();
            //            }
            //        }

            //    }
            
            //}
         #endregion

            if (dtRenewal.Value == null)
            {
                dtRenewal.Value = Convert.ToDateTime(this.Now().ToString("dd/MM/yyyy"));

            }

            
        }
        private void txtPolicyno_Validating(object sender, CancelEventArgs e)
        {
            if ((FunctionConfig.CurrentOption == Function.Add) &&  txtnameIncGrp1.Text.ToString() != string.Empty )
            {
                //if (MessageBox.Show("Do You Want To Save this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                //{

                //    //base.DoToolbarActions(this.Controls,"Save");


                //}
            
            }
        
        }
        private void dtRenewal_Validating(object sender, CancelEventArgs e)
        {
            if ((txtLifeIns.Text.ToUpper() == "O") && (txtoffical.Text.ToUpper() == "I"))
            {
                txtMultiply.Focus();
                txtMultiply.Select();
            }


            if ((txtLifeIns.Text.ToUpper() == "I") && (txtoffical.Text.ToUpper() == "U"))
            {
                txtLifeCovg.Select();
                txtLifeCovg.Focus();
            }
            if (txtLifeIns.Text.ToUpper() == "H")
            {

                txtHospitalCovg.Select();
                txtHospitalCovg.Focus();
            }

        }
        private void txtMultiply_Validating(object sender, CancelEventArgs e)
        {
            if ((txtLifeIns.Text.ToUpper() == "o") && (txtMultiply.Text.ToString() == string.Empty))
            {
                MessageBox.Show("Enter Value ...........!");
                e.Cancel = true;
            }
            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                proc_modify();
            }

            if (FunctionConfig.CurrentOption == Function.Add && txtnameIncGrp1.Text.ToString() != string.Empty)
            {
                if (MessageBox.Show("Do You Want To Save this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    base.DoToolbarActions(this.Controls, "Save");
                    dtRenewal.Value = null;
                }
                else
                {
                    base.ClearForm(PnlIncGrp.Controls);
                    //dtRenewal.Value = null;
                    txtOption.Select();
                    txtOption.Focus();
                }
            
            }


        }        
        private void txtLifeCovg_Validating(object sender, CancelEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                proc_modify();
            }
            if (FunctionConfig.CurrentOption == Function.Add && txtnameIncGrp1.Text.ToString() != string.Empty)
            {
                if (MessageBox.Show("Do You Want To Save this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    base.DoToolbarActions(this.Controls, "Save");
                    dtRenewal.Value = null;
                }
                else
                {
                    base.ClearForm(PnlIncGrp.Controls);
                    //dtRenewal.Value = null;
                    txtOption.Select();
                    txtOption.Focus();

                }

            }

        }     
        private void txtHospitalCovg_Validating(object sender, CancelEventArgs e)
        {
            //if (FunctionConfig.CurrentOption == Function.Modify)
            //{
            //    proc_modify();
            //}
            ////never arrive here a/c to CHRIS_SL//
            //if (FunctionConfig.CurrentOption == Function.Add && txtnameIncGrp1.Text.ToString() != string.Empty)
            //{
            //    if (MessageBox.Show("Do You Want To Save this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //    {
            //     base.DoToolbarActions(this.Controls, "Save");
            //     dtRenewal.Value = null;
            //    }

            //    else
            //    {
            //        base.ClearForm(PnlIncGrp.Controls);
            //        dtRenewal.Value = null;
            //        txtOption.Select();
            //        txtOption.Focus();

            //    }
            //}
        }
        private void lbTypeCod_MouseDown(object sender, MouseEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.None)
            {
                lbTypeCod.Enabled = false;
                lbTypeCod.Enabled = true;

            }
        }
        private void lbsatfftype_MouseDown(object sender, MouseEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.None)
            {
                lbsatfftype.Enabled = false;
                lbsatfftype.Enabled = true;

            }
        }
        private void lbDesig_MouseDown(object sender, MouseEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.None)
            {
                lbDesig.Enabled = false;
                lbDesig.Enabled = true;

            }
        }
        private void lbLevel_MouseDown(object sender, MouseEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.None)
            {
                lbLevel.Enabled = false;
                lbLevel.Enabled = true;

            }

        }
        #endregion

        private void txtMultiply_Leave(object sender, EventArgs e)
        {

            if (FunctionConfig.CurrentOption == Function.Add && txtnameIncGrp1.Text.ToString() != string.Empty)
            {
                if (MessageBox.Show("Do You Want To Save this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    base.DoToolbarActions(this.Controls, "Save");
                    dtRenewal.Value = null;
                }

                else
                {
                    base.ClearForm(PnlIncGrp.Controls);
                    //dtRenewal.Value = null;
                    txtOption.Select();
                    txtOption.Focus();

                }
            }


        }

        private void txtHospitalCovg_Leave(object sender, EventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.Modify)
            {
                proc_modify();
            }
            //never arrive here a/c to CHRIS_SL//
            if (FunctionConfig.CurrentOption == Function.Add && txtnameIncGrp1.Text.ToString() != string.Empty)
            {
                if (MessageBox.Show("Do You Want To Save this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    base.DoToolbarActions(this.Controls, "Save");
                    dtRenewal.Value = null;
                }

                else
                {
                    base.ClearForm(PnlIncGrp.Controls);
                    //dtRenewal.Value = null;
                    txtOption.Select();
                    txtOption.Focus();

                }
            }
        }

        private void txtLevel_Leave(object sender, EventArgs e)
        {

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_level_in", txtLevel.Text == "" ? "0" : txtLevel.Text);
            param.Add("sp_desig_in", this.txtdesignation.Text == "" ? "0" : this.txtdesignation.Text);
            if (txtLifeIns.Text.ToUpper() == "U")
            {
                rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_Level_IfClause", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        //txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        //tp.Hide();
                    }
                    else
                    {
                        tp.Show("Invalid Entry... Press [F9] To Display The List", this.txtdesignation);
                        //MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
                        if (txtdesignation.Text != "" || txtLifeIns.Text != "" || txtoffical.Text != "")
                        {
                            txtdesignation.Select();
                            txtdesignation.Focus();
                        }

                    }

                }

            }

            else
            {
                rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_Level_ElseClause", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        ///txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        tp.Hide(this.txtdesignation);
                    }
                    else
                    {
                        //MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
                        tp.Show("Invalid Entry... Press [F9] To Display The List",this);
                        if (txtdesignation.Text != "" || txtLifeIns.Text !="" || txtoffical.Text !="")
                        {
                            txtdesignation.Select();
                            txtdesignation.Focus();
                        }
                    }

                }

            }

           

        }

        private void txtdesignation_Leave(object sender, EventArgs e)
        {


            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Clear();
            param.Add("sp_desig_in", txtdesignation.Text == "" ? "0" : txtdesignation.Text);

            if (txtoffical.Text.ToUpper() == "U")
            {
                rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_desig_IfClause", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        //txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        tp.Hide(this.txtdesignation);
                    }
                    else
                    {
                        //MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
                        tp.Show("Invalid Entry... Press [F9] To Display The List", this.txtdesignation);
                        if (txtdesignation.Text != "" || txtLifeIns.Text != "" || txtoffical.Text != "")
                        {
                            txtdesignation.Select();
                            txtdesignation.Focus();
                        }
                    }

                }


            }
            else
            {
                rslt = cmnDM.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "sp_desig_ElseClause", param);


                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        //txtdesignation.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        tp.Hide(this.txtdesignation);
                    }
                    else
                    {
                        //MessageBox.Show("Invalid Entry... Press [F9] To Display The List");
                        tp.Show("Invalid Entry... Press [F9] To Display The List", this.txtdesignation);
                        if (txtdesignation.Text != "" || txtLifeIns.Text != "" || txtoffical.Text != "")
                        {
                            txtdesignation.Select();
                            txtdesignation.Focus();
                        }
                    }

                }

            }

        }

      

        
    }

}