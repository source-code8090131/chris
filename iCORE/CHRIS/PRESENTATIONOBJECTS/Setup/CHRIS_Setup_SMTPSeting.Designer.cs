namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_SMTPSeting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.lbSubject = new System.Windows.Forms.Label();
            this.lbMsgBiody = new System.Windows.Forms.Label();
            this.rtbBody = new System.Windows.Forms.RichTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lbProfileName = new System.Windows.Forms.Label();
            this.lbShareFolderPath = new System.Windows.Forms.Label();
            this.txtbProfileName = new System.Windows.Forms.TextBox();
            this.txtbSharedFolderPath = new System.Windows.Forms.TextBox();
            this.txtbSubject = new System.Windows.Forms.TextBox();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(525, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(561, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 317);
            this.panel1.Size = new System.Drawing.Size(561, 60);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(379, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Username :";
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lbTitle.Location = new System.Drawing.Point(217, 47);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(128, 20);
            this.lbTitle.TabIndex = 2;
            this.lbTitle.Text = "SMTP Settings";
            // 
            // lbSubject
            // 
            this.lbSubject.AutoSize = true;
            this.lbSubject.Location = new System.Drawing.Point(108, 150);
            this.lbSubject.Name = "lbSubject";
            this.lbSubject.Size = new System.Drawing.Size(49, 13);
            this.lbSubject.TabIndex = 7;
            this.lbSubject.Text = "Subject :";
            // 
            // lbMsgBiody
            // 
            this.lbMsgBiody.AutoSize = true;
            this.lbMsgBiody.Location = new System.Drawing.Point(108, 174);
            this.lbMsgBiody.Name = "lbMsgBiody";
            this.lbMsgBiody.Size = new System.Drawing.Size(83, 13);
            this.lbMsgBiody.TabIndex = 9;
            this.lbMsgBiody.Text = "Message Body :";
            // 
            // rtbBody
            // 
            this.rtbBody.Location = new System.Drawing.Point(111, 190);
            this.rtbBody.Name = "rtbBody";
            this.rtbBody.Size = new System.Drawing.Size(329, 92);
            this.rtbBody.TabIndex = 10;
            this.rtbBody.Text = "";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(365, 288);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lbProfileName
            // 
            this.lbProfileName.AutoSize = true;
            this.lbProfileName.Location = new System.Drawing.Point(108, 90);
            this.lbProfileName.Name = "lbProfileName";
            this.lbProfileName.Size = new System.Drawing.Size(73, 13);
            this.lbProfileName.TabIndex = 3;
            this.lbProfileName.Text = "Profile Name :";
            this.lbProfileName.Visible = false;
            // 
            // lbShareFolderPath
            // 
            this.lbShareFolderPath.AutoSize = true;
            this.lbShareFolderPath.Location = new System.Drawing.Point(108, 120);
            this.lbShareFolderPath.Name = "lbShareFolderPath";
            this.lbShareFolderPath.Size = new System.Drawing.Size(104, 13);
            this.lbShareFolderPath.TabIndex = 5;
            this.lbShareFolderPath.Text = "Shared Folder Path :";
            // 
            // txtbProfileName
            // 
            this.txtbProfileName.Location = new System.Drawing.Point(221, 90);
            this.txtbProfileName.Name = "txtbProfileName";
            this.txtbProfileName.Size = new System.Drawing.Size(219, 20);
            this.txtbProfileName.TabIndex = 12;
            this.txtbProfileName.Visible = false;
            // 
            // txtbSharedFolderPath
            // 
            this.txtbSharedFolderPath.Location = new System.Drawing.Point(221, 118);
            this.txtbSharedFolderPath.Name = "txtbSharedFolderPath";
            this.txtbSharedFolderPath.Size = new System.Drawing.Size(219, 20);
            this.txtbSharedFolderPath.TabIndex = 13;
            // 
            // txtbSubject
            // 
            this.txtbSubject.Location = new System.Drawing.Point(221, 146);
            this.txtbSubject.Name = "txtbSubject";
            this.txtbSubject.Size = new System.Drawing.Size(219, 20);
            this.txtbSubject.TabIndex = 14;
            // 
            // CHRIS_Setup_SMTPSeting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(561, 377);
            this.Controls.Add(this.txtbSubject);
            this.Controls.Add(this.txtbSharedFolderPath);
            this.Controls.Add(this.txtbProfileName);
            this.Controls.Add(this.lbShareFolderPath);
            this.Controls.Add(this.lbProfileName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.rtbBody);
            this.Controls.Add(this.lbMsgBiody);
            this.Controls.Add(this.lbSubject);
            this.Controls.Add(this.lbTitle);
            this.Controls.Add(this.label9);
            this.Name = "CHRIS_Setup_SMTPSeting";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  SMTP Settings Setup";
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lbTitle, 0);
            this.Controls.SetChildIndex(this.lbSubject, 0);
            this.Controls.SetChildIndex(this.lbMsgBiody, 0);
            this.Controls.SetChildIndex(this.rtbBody, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.lbProfileName, 0);
            this.Controls.SetChildIndex(this.lbShareFolderPath, 0);
            this.Controls.SetChildIndex(this.txtbProfileName, 0);
            this.Controls.SetChildIndex(this.txtbSharedFolderPath, 0);
            this.Controls.SetChildIndex(this.txtbSubject, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.Label lbSubject;
        private System.Windows.Forms.Label lbMsgBiody;
        private System.Windows.Forms.RichTextBox rtbBody;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lbProfileName;
        private System.Windows.Forms.Label lbShareFolderPath;
        private System.Windows.Forms.TextBox txtbProfileName;
        private System.Windows.Forms.TextBox txtbSharedFolderPath;
        private System.Windows.Forms.TextBox txtbSubject;
    }
}
