﻿using iCORE.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_TaxableIncomeUpload : Form
    {
        public CHRIS_Setup_TaxableIncomeUpload()
        {
            InitializeComponent();
        }

        #region Application logs

        StreamWriter log;
        FileStream fileStream = null;
        DirectoryInfo logDirInfo = null;
        FileInfo logFileInfo;

        FileInfo logFileInfoX;
        FileInfo logFileCust;
        FileInfo MDFileInfo;
        #endregion

        private void btnUploadPath_Click(object sender, EventArgs e)
        {
            string filePath = string.Empty;
            string fileExt = string.Empty;
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filePath = file.FileName;
                fileExt = Path.GetExtension(filePath);
                if (fileExt.CompareTo(".xlsx") == 0 || fileExt.CompareTo(".xls") == 0)
                    textBxConsumerFile.Text = filePath;
                else
                    MessageBox.Show("Please choose .xlsx and .xls file only.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnStartProcess_Click(object sender, EventArgs e)
        {
            //btnChkData.Visible = false;
            string userID = "";// (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                               // (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            if (string.IsNullOrEmpty(textBxConsumerFile.Text))
            {
                MessageBox.Show("Please Select File for Upload.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string logFilePath = "C:\\iCORE-Spool\\";
            logFilePath = logFilePath + "Log Cost Center -" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);

            log.WriteLine("************************* Cost Center Uploading to run : " + DateTime.Now);

            try
            {
              //  CheckExcellProcesses("excel");
                DataTable mPayment = GetMasterDataWorkSheet(1, log);
              //  KillExcel("excel");

                DataRow[] dtr = mPayment.Select("HZR_PA_P_NO not like 'P_NO'");
                foreach (System.Data.DataRow drow in dtr)
                {
                    string paymentT = string.IsNullOrEmpty(drow["HZR_PA_P_NO"].ToString().Trim()) ? "" : drow["HZR_PA_P_NO"].ToString().Trim();
                    if (paymentT.ToUpper().Contains("PA_P_NO") || paymentT.Equals("-"))
                        drow.Delete();
                }
                mPayment.AcceptChanges();

                #region Convert excel to table
                DataTable genLinkFile = new DataTable();
                genLinkFile.Columns.Add("HZR_PA_P_NO", typeof(int));
                genLinkFile.Columns.Add("HouseFinance", typeof(Double));
                genLinkFile.Columns.Add("ZakatFinance", typeof(Double));
                genLinkFile.Columns.Add("InvestFinance", typeof(Double));
                genLinkFile.Columns.Add("Rebate", typeof(Double));
                genLinkFile.Columns.Add("FinDate", typeof(DateTime));
                genLinkFile.Columns.Add("FinFields_1", typeof(Double));
                genLinkFile.Columns.Add("FinFields_Text_1", typeof(string));

                DataRow genLinkRow;

                string Detail0 = string.Empty;
                string Detail4 = string.Empty;
                string Detail12 = string.Empty;
                string Detail16 = string.Empty;
                string Detail20 = string.Empty;

                //"DeptCode", "CostCode",

                foreach (System.Data.DataRow dtrow in mPayment.Rows)
                {
                    genLinkRow = genLinkFile.NewRow();

                    genLinkRow["HZR_PA_P_NO"] = string.IsNullOrEmpty(dtrow["HZR_PA_P_NO"].ToString().Trim()) ? "" : dtrow["HZR_PA_P_NO"].ToString().Trim();
                    genLinkRow["HouseFinance"] = string.IsNullOrEmpty(dtrow["HouseFinance"].ToString().Trim()) ? "0" : dtrow["HouseFinance"].ToString().Trim();
                    genLinkRow["ZakatFinance"] = string.IsNullOrEmpty(dtrow["ZakatFinance"].ToString().Trim()) ? "0" : dtrow["ZakatFinance"].ToString().Trim();
                    genLinkRow["InvestFinance"] = string.IsNullOrEmpty(dtrow["InvestFinance"].ToString().Trim()) ? "0" : dtrow["InvestFinance"].ToString().Trim();
                    genLinkRow["Rebate"] = string.IsNullOrEmpty(dtrow["Rebate"].ToString().Trim()) ? "0" : dtrow["Rebate"].ToString().Trim();
                    //DateTime.ParseExact(Detail0, "dd/MM/yyyy", null);
                    Detail0 = string.IsNullOrEmpty(dtrow["FinDate"].ToString().Trim()) ? "" : dtrow["FinDate"].ToString().Trim();
                    genLinkRow["FinDate"] = Convert.ToDateTime(Detail0);//DateTime.ParseExact(Detail0, "dd/MM/yyyy", null);

                    genLinkRow["FinFields_1"] = 0;
                    genLinkRow["FinFields_Text_1"] = "";


                    genLinkFile.Rows.Add(genLinkRow);
                }

                #endregion

                uploadMasterData(genLinkFile);

                DataTable dt = new DataTable();
                userID = "";
                dt = SQLManager.ExecuteINCOMETAX_FUNDData().Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dGVCostCenter.DataSource = dt;
                    dGVCostCenter.Update();
                    dGVCostCenter.Visible = true;
                }
                else
                {
                    dGVCostCenter.Visible = false;
                }

                MessageBox.Show("Successfull Upload Data");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : Please check your attached file containing additional or mismatch information which is validate the agreed template.");
                log.WriteLine("Cost Center " + DateTime.Now + " Exception :  " + ex.ToString());
                log.WriteLine("*************************************** IBFT Cost Center " + DateTime.Now + " ******************************************** ");
                log.Close();
                return;
            }

            log.WriteLine("Cost Center " + DateTime.Now + " Process Complete  ");
            log.WriteLine("*************************************** IBFT Cost Center " + DateTime.Now + " ******************************************** ");
            log.Close();
        }

        private void btnChkData_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        static Hashtable myHashtable;
        private static void CheckExcellProcesses(string filename)
        {
            try
            {
                Process[] AllProcesses = Process.GetProcessesByName(filename);
                myHashtable = new Hashtable();
                int iCount = 0;

                foreach (Process ExcelProcess in AllProcesses)
                {
                    myHashtable.Add(ExcelProcess.Id, iCount);
                    iCount = iCount + 1;
                }
            }
            catch (Exception exp)
            {

            }
        }

        private static void KillExcel(string filename)
        {
            try
            {
                //Console.WriteLine("Closing Excel Processes...");
                Process[] AllProcesses = Process.GetProcessesByName(filename);

                // check to kill the right process
                foreach (Process outlookProcess in AllProcesses)
                {
                    if (myHashtable.ContainsKey(outlookProcess.Id) == true)
                        outlookProcess.Kill();
                }

                AllProcesses = null;
            }
            catch (System.Exception exp)
            {

            }
        }

        public static void uploadMasterData(DataTable dt)
        {
            try
            {
                SQLManager.ExecuteIncomeTaxAdditionalFieldUploader(dt);
            }
            catch (Exception ex)
            {

            }
        }

        public DataTable GetMasterDataWorkSheet(int workSheetID, StreamWriter log)
        {
            log.WriteLine("Cost Center" + DateTime.Now + "****************************  Start Reading Excel file ***********************************");
            //string pathOfExcelFile = "";
            DataTable dt = new DataTable();

            Excel.Application excelApp = new Excel.Application();
            excelApp.DisplayAlerts = false; //Don't want Excel to display error messageboxes
            Excel.Workbook workbook = excelApp.Workbooks.Open(textBxConsumerFile.Text, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing); //This opens the file
            Excel.Worksheet sheet = (Excel.Worksheet)workbook.Sheets.get_Item(workSheetID); //Get the first sheet in the file

            try
            {
                int lastRow = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Row;
                log.WriteLine("Cost Center" + DateTime.Now + "Total Number of Rows : " + lastRow);
                int lastColumn = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Column;
                log.WriteLine("Cost Center" + DateTime.Now + "Total Number of Columns : " + lastColumn);
                Excel.Range oRange = sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[lastRow, lastColumn]);//("A1",lastColumnIndex + lastRow.ToString());
                oRange.EntireColumn.AutoFit();

                string[] ManualColumn = { "HZR_PA_P_NO", "HouseFinance", "ZakatFinance", "InvestFinance", "Rebate", "FinDate", };

                log.WriteLine("Cost Center" + DateTime.Now + "Columns reading : " + lastColumn);
                for (int i = 0; i < oRange.Columns.Count; i++)
                {
                    dt.Columns.Add(ManualColumn[i].ToString());
                }

                object[,] cellValues = (object[,])oRange.Value2;
                object[] values = new object[lastColumn];

                log.WriteLine("Cost Center" + DateTime.Now + "Start reading data : ");
                for (int i = 1; i <= lastRow; i++)
                {

                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        values[j] = cellValues[i, j + 1] == null ? "-" : cellValues[i, j + 1];
                    }
                    dt.Rows.Add(values);
                }
                log.WriteLine("Cost Center" + DateTime.Now + "Excel file read finished");
            }
            catch (Exception ex)
            {
                log.WriteLine("Cost Center" + DateTime.Now + "Exception : " + ex.ToString());
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                excelApp.Quit();
            }
            return dt;

        }

        private void btnFindData_Click(object sender, EventArgs e)
        {
            //showMasterGridView("[dbo].[SP_OneBillerRefreshDetails]");
            string logFilePath = "C:\\iCORE-Spool\\";
            logFilePath = logFilePath + "OneBiller -" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);

            log.WriteLine("************************* OneBiller Reporting Application begin to run : " + DateTime.Now);

            try
            {

                DateTime fromDate = dtFromConData.Value;
                DateTime ToDate = dtToConData.Value;

                string EmployeePA_NO = string.IsNullOrEmpty(txtEmpNo.Text.Trim()) ? "0" : txtEmpNo.Text.Trim();
                int EmpNo = Convert.ToInt32(EmployeePA_NO);
                DataTable dt = new DataTable();

             //   dt = SQLManager.ExecuteOneBillerConsumer(fromDate, ToDate).Tables[0];

                if(EmpNo < 0)
                {
                    EmpNo = 0;
                }

                dt = SQLManager.ExecuteFindEmployee(fromDate, ToDate, EmpNo).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    dGVCostCenter.DataSource = dt;
                    dGVCostCenter.Update();
                    dGVCostCenter.Visible = true;
                }
                else
                {
                    dGVCostCenter.Visible = false;
                    MessageBox.Show("No Record Found");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : Please check your uploaded data, there is something wrong data");
                log.WriteLine("OneBiller " + DateTime.Now + " Exception :  " + ex.ToString());
                log.WriteLine("*************************************** OneBiller " + DateTime.Now + " ******************************************** ");
                log.Close();
                //   wfileStream.Close();
                return;
            }
            log.WriteLine("OneBiller " + DateTime.Now + " Process Complete  ");
            log.WriteLine("*************************************** OneBiller " + DateTime.Now + " ******************************************** ");
            log.Close();
        }
    }
}
