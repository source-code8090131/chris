namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_MarginalTaxSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_MarginalTaxSetup));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grbMarginalTax = new System.Windows.Forms.GroupBox();
            this.pnlMarginalTax = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvRecords = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.txtUserName = new System.Windows.Forms.Label();
            this.DateFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartingAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndingAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Percentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grbMarginalTax.SuspendLayout();
            this.pnlMarginalTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(527, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(563, 22);
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(563, 60);
            // 
            // grbMarginalTax
            // 
            this.grbMarginalTax.Controls.Add(this.pnlMarginalTax);
            this.grbMarginalTax.Location = new System.Drawing.Point(12, 98);
            this.grbMarginalTax.Name = "grbMarginalTax";
            this.grbMarginalTax.Size = new System.Drawing.Size(539, 316);
            this.grbMarginalTax.TabIndex = 8;
            this.grbMarginalTax.TabStop = false;
            // 
            // pnlMarginalTax
            // 
            this.pnlMarginalTax.ConcurrentPanels = null;
            this.pnlMarginalTax.Controls.Add(this.dgvRecords);
            this.pnlMarginalTax.DataManager = null;
            this.pnlMarginalTax.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMarginalTax.DependentPanels = null;
            this.pnlMarginalTax.DisableDependentLoad = false;
            this.pnlMarginalTax.EnableDelete = true;
            this.pnlMarginalTax.EnableInsert = true;
            this.pnlMarginalTax.EnableQuery = false;
            this.pnlMarginalTax.EnableUpdate = true;
            this.pnlMarginalTax.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MarginalTaxCommand";
            this.pnlMarginalTax.Location = new System.Drawing.Point(18, 20);
            this.pnlMarginalTax.MasterPanel = null;
            this.pnlMarginalTax.Name = "pnlMarginalTax";
            this.pnlMarginalTax.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMarginalTax.Size = new System.Drawing.Size(498, 261);
            this.pnlMarginalTax.SPName = "CHRIS_SP_MARGINAL_TAX_MANAGER";
            this.pnlMarginalTax.TabIndex = 0;
            // 
            // dgvRecords
            // 
            this.dgvRecords.AllowDrop = true;
            this.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecords.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DateFrom,
            this.DateTo,
            this.StartingAmount,
            this.EndingAmount,
            this.Percentage});
            this.dgvRecords.ColumnToHide = null;
            this.dgvRecords.ColumnWidth = null;
            this.dgvRecords.CustomEnabled = true;
            this.dgvRecords.DisplayColumnWrapper = null;
            this.dgvRecords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRecords.GridDefaultRow = 0;
            this.dgvRecords.Location = new System.Drawing.Point(0, 0);
            this.dgvRecords.Name = "dgvRecords";
            this.dgvRecords.ReadOnlyColumns = null;
            this.dgvRecords.RequiredColumns = null;
            this.dgvRecords.Size = new System.Drawing.Size(498, 261);
            this.dgvRecords.SkippingColumns = null;
            this.dgvRecords.TabIndex = 0;
            this.dgvRecords.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvRecords_CellValidating);
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(318, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(72, 13);
            this.txtUserName.TabIndex = 121;
            this.txtUserName.Text = "User  Name : ";
            // 
            // DateFrom
            // 
            this.DateFrom.DataPropertyName = "MT_DATE_FROM";
            this.DateFrom.HeaderText = "Date From";
            this.DateFrom.MaxInputLength = 10;
            this.DateFrom.Name = "DateFrom";
            this.DateFrom.Width = 105;
            // 
            // DateTo
            // 
            this.DateTo.DataPropertyName = "MT_DATE_TO";
            this.DateTo.HeaderText = "Date To";
            this.DateTo.MaxInputLength = 10;
            this.DateTo.Name = "DateTo";
            this.DateTo.Width = 105;
            // 
            // StartingAmount
            // 
            this.StartingAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StartingAmount.DataPropertyName = "MT_AMT_FROM";
            dataGridViewCellStyle1.Format = "N3";
            dataGridViewCellStyle1.NullValue = null;
            this.StartingAmount.DefaultCellStyle = dataGridViewCellStyle1;
            this.StartingAmount.HeaderText = "Starting Amount";
            this.StartingAmount.MaxInputLength = 10;
            this.StartingAmount.Name = "StartingAmount";
            // 
            // EndingAmount
            // 
            this.EndingAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EndingAmount.DataPropertyName = "MT_AMT_TO";
            dataGridViewCellStyle2.Format = "N3";
            dataGridViewCellStyle2.NullValue = null;
            this.EndingAmount.DefaultCellStyle = dataGridViewCellStyle2;
            this.EndingAmount.HeaderText = "Ending Amount";
            this.EndingAmount.MaxInputLength = 10;
            this.EndingAmount.Name = "EndingAmount";
            // 
            // Percentage
            // 
            this.Percentage.DataPropertyName = "MT_PERCENTAGE";
            this.Percentage.HeaderText = "%";
            this.Percentage.MaxInputLength = 3;
            this.Percentage.Name = "Percentage";
            // 
            // CHRIS_Setup_MarginalTaxSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 438);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.grbMarginalTax);
            this.CurrentPanelBlock = "pnlMarginalTax";
            this.Name = "CHRIS_Setup_MarginalTaxSetup";
            this.SetFormTitle = "Marginal Tax Setup";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS  - Marginal Tax Setup";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.grbMarginalTax, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grbMarginalTax.ResumeLayout(false);
            this.pnlMarginalTax.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbMarginalTax;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlMarginalTax;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvRecords;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartingAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndingAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Percentage;
    }
}