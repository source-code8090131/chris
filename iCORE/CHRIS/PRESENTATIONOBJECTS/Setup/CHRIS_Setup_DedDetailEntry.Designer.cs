namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_DedDetailEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_DedDetailEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlTblDedDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDedDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.grdPersonnelNo = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.grdDeducCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblDedDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTblDedDetail
            // 
            this.pnlTblDedDetail.ConcurrentPanels = null;
            this.pnlTblDedDetail.Controls.Add(this.dgvDedDetail);
            this.pnlTblDedDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDedDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDedDetail.DependentPanels = null;
            this.pnlTblDedDetail.DisableDependentLoad = false;
            this.pnlTblDedDetail.EnableDelete = true;
            this.pnlTblDedDetail.EnableInsert = true;
            this.pnlTblDedDetail.EnableUpdate = true;
            this.pnlTblDedDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DedDetailEntCommand";
            this.pnlTblDedDetail.Location = new System.Drawing.Point(12, 77);
            this.pnlTblDedDetail.MasterPanel = null;
            this.pnlTblDedDetail.Name = "pnlTblDedDetail";
            this.pnlTblDedDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDedDetail.Size = new System.Drawing.Size(475, 196);
            this.pnlTblDedDetail.SPName = "CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER";
            this.pnlTblDedDetail.TabIndex = 0;
            // 
            // dgvDedDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDedDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDedDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdPersonnelNo,
            this.grdDeducCode,
            this.id,
            this.grdName,
            this.grdRemarks});
            this.dgvDedDetail.ColumnToHide = null;
            this.dgvDedDetail.ColumnWidth = null;
            this.dgvDedDetail.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDedDetail.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDedDetail.DisplayColumnWrapper = null;
            this.dgvDedDetail.GridDefaultRow = 0;
            this.dgvDedDetail.Location = new System.Drawing.Point(3, 3);
            this.dgvDedDetail.Name = "dgvDedDetail";
            this.dgvDedDetail.ReadOnlyColumns = null;
            this.dgvDedDetail.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDedDetail.Size = new System.Drawing.Size(468, 190);
            this.dgvDedDetail.SkippingColumns = "GRDNAME";
            this.dgvDedDetail.TabIndex = 0;
            this.dgvDedDetail.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDedDetail_CellLeave);
            this.dgvDedDetail.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDedDetail_RowEnter);
            this.dgvDedDetail.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDedDetail_CellValidated);
            this.dgvDedDetail.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvDedDetail_CellValidating);
            this.dgvDedDetail.SelectionChanged += new System.EventHandler(this.dgvDedDetail_SelectionChanged);
            // 
            // grdPersonnelNo
            // 
            this.grdPersonnelNo.ActionLOV = "SP_P_NO_LOV";
            this.grdPersonnelNo.ActionLOVExists = "SP_P_NO_LOV_EXISTS";
            this.grdPersonnelNo.AttachParentEntity = false;
            this.grdPersonnelNo.DataPropertyName = "SP_P_NO";
            this.grdPersonnelNo.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DedDetailEntCommand";
            this.grdPersonnelNo.Frozen = true;
            this.grdPersonnelNo.HeaderText = "Personnel No.";
            this.grdPersonnelNo.LookUpTitle = null;
            this.grdPersonnelNo.LOVFieldMapping = "SP_P_NO";
            this.grdPersonnelNo.Name = "grdPersonnelNo";
            this.grdPersonnelNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPersonnelNo.SearchColumn = "SP_P_NO";
            this.grdPersonnelNo.SkipValidationOnLeave = false;
            this.grdPersonnelNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.grdPersonnelNo.SpName = "CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER";
            this.grdPersonnelNo.Width = 70;
            // 
            // grdDeducCode
            // 
            this.grdDeducCode.DataPropertyName = "SP_DEDUC_CODE";
            this.grdDeducCode.HeaderText = "";
            this.grdDeducCode.Name = "grdDeducCode";
            this.grdDeducCode.Visible = false;
            // 
            // id
            // 
            this.id.DataPropertyName = "ID";
            this.id.Frozen = true;
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // grdName
            // 
            this.grdName.DataPropertyName = "Name";
            this.grdName.Frozen = true;
            this.grdName.HeaderText = "Name";
            this.grdName.Name = "grdName";
            this.grdName.ReadOnly = true;
            this.grdName.Width = 170;
            // 
            // grdRemarks
            // 
            this.grdRemarks.DataPropertyName = "SP_REMARKS";
            this.grdRemarks.HeaderText = "Remarks";
            this.grdRemarks.Name = "grdRemarks";
            this.grdRemarks.Width = 180;
            // 
            // CHRIS_Setup_DedDetailEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 293);
            this.Controls.Add(this.pnlTblDedDetail);
            this.Name = "CHRIS_Setup_DedDetailEntry";
            this.SetFormTitle = "";
            this.Text = "CHRIS_Setup_DedDetailEntry";
            this.Controls.SetChildIndex(this.pnlTblDedDetail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblDedDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDedDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDedDetail;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn grdPersonnelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdDeducCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdName;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdRemarks;
    }
}