using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_CategoryEntry : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        #region Constructors

        public CHRIS_Setup_CategoryEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Setup_CategoryEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            label7.Text += "   " + this.UserName;
            DGVCategory.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 

        }

        #endregion

        #region Methods
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("MM/dd/yyyy");
            this.CurrrentOptionTextBox = this.txtCurrOption;

            //stsOptions.Visible = false;
            //this.Category_Code.SkipValidationOnLeave = true;

           // txtOption.Enabled = false;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType != "Close" && actionType != "Cancel" && this.txtOption.Focused)
            {
                return;
            }
            if (actionType == "Cancel")
            {
                this.DGVCategory.RejectChanges();
                return;
            }
            if (actionType == "Save")
            {
                DataTable dt = (DataTable)DGVCategory.DataSource;
                if (dt != null || dt.Rows[0]["SP_DESC"] != null || dt.Rows[0]["SP_CAT_CODE"] != System.DBNull.Value.ToString() || dt.Rows[0]["SP_CAT_CODE"] != "" || dt.Rows[0]["SP_CAT_CODE"] != string.Empty)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                        DialogResult dr = MessageBox.Show("Do You Want to Save the Record [Y/N]", "", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                        if (dr == DialogResult.No)
                        {
                            return;
                        }
                    }
                }
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override bool Add()
        {
            //((DataGridViewLOVColumn)DGVCategory.Columns["Category_Code"]).SkipValidationOnLeave = true;
            this.Category_Code.SkipValidationOnLeave = true;
            return base.Add();
        }
        protected override bool Edit()
        {
           // ((DataGridViewLOVColumn)DGVCategory.Columns["Category_Code"]).SkipValidationOnLeave = true;
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
            return base.Edit();
        }
        protected override bool Delete()
        {
            this.Category_Code.SkipValidationOnLeave = false;
            return base.Delete();
        }
        private DataTable proc_mod_Del()
        {
            DataTable dt = null;
            //Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            //colsNVals.Add("EFFECTIVE_DATE", "01/25/2010");

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "Proc_mod_del");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private String proc_view_add(string Pram)
        
        {
            string sp_Desc = string.Empty;
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Add("SP_CAT_CODE", Pram);

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "proc_view_add", colsNVals);
            
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                    sp_Desc = dt.Rows[0][1].ToString();
                 
                }
            }
            return sp_Desc;
        }

        private int Get_ID(string Pram)
        {
            int ID =0;
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Add("SP_CAT_CODE", Pram);

            Result rsltCode;
           
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "Get_ID", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                    ID = Int32.Parse(dt.Rows[0][0].ToString());

                }
            }
            return ID;
                
        }



        private DataTable GetAll()
        {
            DataTable dt = null;
            //Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            //colsNVals.Add("EFFECTIVE_DATE", "01/25/2010");

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "proc_view_add");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private void btn_Click(String  actiontype)
        {
            //Button btn = (Button)sender;
            if (actiontype == null)
            {
                return;
            }
            string caption = actiontype;

            
            switch(caption)
            {
                case "Add":
                   
                   
                    DoFunction(Function.Add);
                                      
                    break;
                case "Save":
                 
                    DoFunction(Function.Save);
                    this.FunctionConfig.CurrentOption = Function.None;
                    break;
                //case "Modify":
                    
                    
                //    DoFunction(Function.Modify);
                  
                case "Query":
                    
                    DoFunction(Function.Modify);
                    DGVCategory.CurrentRow.Cells[0].ToolTipText = "Press [Up & Dn] Arrow to See Different Records..Or Press [F6] to Exit";
                    { MessageBox.Show("You cannot update this record"); }
                    
                    break;
                case "Delete":
                   
                    this.FunctionConfig.CurrentOption = Function.Modify;
                  
                    break;
                case "Clear":
                   
                    DoFunction(Function.Cancel);
                    this.FunctionConfig.CurrentOption = Function.None;
                    break;
                case "Exit":
                   
                    DoFunction(Function.Quit);
                    break;
                                    
            }
        }
        private bool Exists(string code)
        {
            bool flag = false;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_cat_code", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "LovCatCodeExisit", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }

        private void getcatcode(string obj)
        {
            this.DGVCategory.CellValidating -=new DataGridViewCellValidatingEventHandler(DGVCategory_CellValidating);
            BindingSource packetSource = new BindingSource();
            Dictionary<String, Object> paramWithVals = new Dictionary<String, Object>();
            Result rsltCode;
            DataSet dsResult = new DataSet();
            paramWithVals.Clear();
            paramWithVals.Add("sp_cat_code", obj.ToString());
            rsltCode = cmnDM.GetData(PnlDetail.SPName, "GetCatCode", paramWithVals);
            if (rsltCode.dstResult != null)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    packetSource = new BindingSource();
                    packetSource.DataSource = rsltCode.dstResult.Tables[0];
                    DGVCategory.DataSource = packetSource;
                    this.operationMode = Mode.Edit;
                    
                    //       FieldInfo info = typeof(SLDataGridView).GetField("m_oDT", BindingFlags.Instance | BindingFlags.NonPublic);
                    //        info.SetValue(slDgvPackets, rsltCode.dstResult.Tables[0]);//slDgvPackets.GetType().ge    //rsltCode.dstResult.Tables[0];
                    //        //packetSource.DataSource = ((DataTable)slDgvPackets.DataSource).DefaultView;//rsltCode.dstResult.Tables[0].DefaultView;
                }
            }
            this.DGVCategory.CellValidating += new DataGridViewCellValidatingEventHandler(DGVCategory_CellValidating);
        }

        #endregion 
 
        #region Events

        private void DGVCategory_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string sp_Desc = string.Empty;

            
            if (e.ColumnIndex == 0)
            {
                if (DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode)
                {

                    if (DGVCategory.CurrentCell.Value.ToString() == "" || DGVCategory.CurrentCell.Value == null)
                    {
                        e.Cancel = true;
                    }



                    if (((DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() == "B")
                        || (DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() == "A")))
                        //&& DGVCategory.Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString() == string.Empty
                    {
                        MessageBox.Show("Category Code Should Start From [C] .....!");
                        e.Cancel = true;
                    }
                    if (this.FunctionConfig.CurrentOption == Function.Modify
                            || this.FunctionConfig.CurrentOption == Function.Delete)
                    {
                        this.proc_mod_Del();
                    }

                    if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Add)
                    {
                        string desc = this.proc_view_add(DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper());
                        this.DGVCategory.CurrentRow.Cells[1].Value = desc;
                    }

                    if (this.FunctionConfig.CurrentOption == Function.Add)

                    {

                        object obj = DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue;
                        if (obj != null && obj.ToString() != string.Empty)
                        {
                            if (this.Exists(obj.ToString()))
                            {
                                this.SetMessage("CHRIS", "Category code '" + DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() + "' already exists."
                                                , "", iCORE.Common.PRESENTATIONOBJECTS.Cmn.MessageType.Information);
                                this.Message.ShowMessage();

                                DGVCategory.Rows[e.RowIndex].Cells[0].Value = "";
                                DGVCategory.Rows[e.RowIndex].Cells[1].Value = "";

                                e.Cancel = true;
                                return;

                            }
                        }
                    }



                    if (this.FunctionConfig.CurrentOption != Function.Add)
                    {
                        this.operationMode = Mode.Edit;
                        string obj = DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString();

                        //getcatcode(obj.ToString());


                        IDataManager dataManager = RuntimeClassLoader.GetDataManager("");
                        int ID = this.Get_ID(obj);
                        string desc = this.proc_view_add(obj);
                        this.DGVCategory.CurrentRow.Cells[2].Value = ID;
                        this.DGVCategory.CurrentRow.Cells[1].Value = desc;
                        this.operationMode = Mode.Edit;
                        this.DGVCategory.CurrentMode = Enumerations.eGridMode.Search;
                        //this.DGVCategory.CurrentMode = Enumerations.eGridMode.Search;
                        //if (obj != null && obj.ToString() != string.Empty)
                        //{
                        //    DataRow[] drow;
                        //    string filter = "SP_CAT_CODE='" + obj.ToString() + "'";
                        //    drow = (DGVCategory.DataSource as DataTable).Select(filter);//string.Format("SP_CAT_CODE='{0}'", obj.ToString())
                        //    if (drow != null && drow.Length > 0)
                        //    {
                        //        this.SetMessage("CHRIS", "Category code '" + obj.ToString().ToUpper() + "' already exists.", "", iCORE.Common.PRESENTATIONOBJECTS.Cmn.MessageType.Information);
                        //        this.Message.ShowMessage();
                        //        e.Cancel = true;
                        //    }

                        //}
                        //else
                        //{
                        //    this.SetMessage("CHRIS", "Category code '" + DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() + "' Does Not exists."
                        //                      , "", iCORE.Common.PRESENTATIONOBJECTS.Cmn.MessageType.Information);
                        //    this.Message.ShowMessage();

                        //    DGVCategory.Rows[e.RowIndex].Cells[0].Value = "";
                        //    DGVCategory.Rows[e.RowIndex].Cells[1].Value = "";

                        //    e.Cancel = true;
                        //    return;
                        //}
                        
                                                           
                    }
                 


                }

              
            }

            if (e.ColumnIndex == 1)
            {
                if (DGVCategory.CurrentRow.Cells[1].EditedFormattedValue.ToString() != "")
                {
                    if (DGVCategory.CurrentRow.Cells[0].Value.ToString() == "")
                    {
                        DGVCategory.CurrentRow.Cells[1].Value = "";
                        e.Cancel = true;
                        return;
                    }
                }


            }



        }
        private void DGVCategory_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
           
            if (this.FunctionConfig.CurrentOption != Function.Add)
            {
                if ((DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString().ToUpper() != String.Empty) && (DGVCategory.Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString() == string.Empty))
                {
                    DGVCategory.Rows[e.RowIndex].Cells[1].Value = proc_view_add(DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString().ToUpper());

                }
            }
        }
        
        #endregion

        private void DGVCategory_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }




        }

        

        
       
    }
}