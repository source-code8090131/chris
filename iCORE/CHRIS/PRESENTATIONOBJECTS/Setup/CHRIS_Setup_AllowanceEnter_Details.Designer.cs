namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_AllowanceEnter_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_AllowanceEnter_Details));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlDetails = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVCategory = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_ALLOW_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_P_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.PersonnelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_REMARKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVCategory)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 358);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // PnlDetails
            // 
            this.PnlDetails.ConcurrentPanels = null;
            this.PnlDetails.Controls.Add(this.DGVCategory);
            this.PnlDetails.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlDetails.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetails.DependentPanels = null;
            this.PnlDetails.DisableDependentLoad = false;
            this.PnlDetails.EnableDelete = true;
            this.PnlDetails.EnableInsert = true;
            this.PnlDetails.EnableQuery = false;
            this.PnlDetails.EnableUpdate = true;
            this.PnlDetails.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.PnlDetails.Location = new System.Drawing.Point(14, 85);
            this.PnlDetails.MasterPanel = null;
            this.PnlDetails.Name = "PnlDetails";
            this.PnlDetails.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetails.Size = new System.Drawing.Size(617, 267);
            this.PnlDetails.SPName = "CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER";
            this.PnlDetails.TabIndex = 11;
            // 
            // DGVCategory
            // 
            this.DGVCategory.AllowUserToAddRows = false;
            this.DGVCategory.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCategory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVCategory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_ALLOW_CODE,
            this.Column1,
            this.SP_P_NO,
            this.PersonnelName,
            this.SP_REMARKS});
            this.DGVCategory.ColumnToHide = null;
            this.DGVCategory.ColumnWidth = null;
            this.DGVCategory.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVCategory.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVCategory.DisplayColumnWrapper = null;
            this.DGVCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVCategory.GridDefaultRow = 0;
            this.DGVCategory.Location = new System.Drawing.Point(0, 0);
            this.DGVCategory.Name = "DGVCategory";
            this.DGVCategory.ReadOnlyColumns = null;
            this.DGVCategory.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCategory.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVCategory.Size = new System.Drawing.Size(617, 267);
            this.DGVCategory.SkippingColumns = null;
            this.DGVCategory.TabIndex = 0;
            this.DGVCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGVCategory_KeyDown);
            // 
            // SP_ALLOW_CODE
            // 
            this.SP_ALLOW_CODE.DataPropertyName = "SP_ALL_CODE";
            this.SP_ALLOW_CODE.HeaderText = "SP_ALLOW_CODE";
            this.SP_ALLOW_CODE.Name = "SP_ALLOW_CODE";
            this.SP_ALLOW_CODE.ReadOnly = true;
            this.SP_ALLOW_CODE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_ALLOW_CODE.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // SP_P_NO
            // 
            this.SP_P_NO.ActionLOV = "Pr_PNO_LOV";
            this.SP_P_NO.ActionLOVExists = "Pr_PNO_LOVExists";
            this.SP_P_NO.AttachParentEntity = false;
            this.SP_P_NO.DataPropertyName = "SP_P_NO";
            this.SP_P_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.SP_P_NO.HeaderText = "Personnel No.";
            this.SP_P_NO.LookUpTitle = null;
            this.SP_P_NO.LOVFieldMapping = "SP_P_NO";
            this.SP_P_NO.Name = "SP_P_NO";
            this.SP_P_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_P_NO.SearchColumn = "SP_P_NO";
            this.SP_P_NO.SkipValidationOnLeave = false;
            this.SP_P_NO.SpName = "CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER";
            // 
            // PersonnelName
            // 
            this.PersonnelName.DataPropertyName = "PR_Name";
            this.PersonnelName.HeaderText = "Name";
            this.PersonnelName.Name = "PersonnelName";
            // 
            // SP_REMARKS
            // 
            this.SP_REMARKS.DataPropertyName = "SP_REMARKS";
            this.SP_REMARKS.HeaderText = "Remarks";
            this.SP_REMARKS.Name = "SP_REMARKS";
            // 
            // CHRIS_Setup_AllowanceEnter_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 418);
            this.Controls.Add(this.PnlDetails);
            this.Name = "CHRIS_Setup_AllowanceEnter_Details";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Setup_AllowanceEnter_Details";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDetails, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVCategory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetails;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_ALLOW_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonnelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_REMARKS;
    }
}