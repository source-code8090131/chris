namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_AllowanceEnter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_AllowanceEnter));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnCategory = new CrplControlLibrary.LookupButton(this.components);
            this.txtSP_ALL_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnLevel = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnAccount = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnDesignation = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnBranch = new CrplControlLibrary.LookupButton(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.txtSP_BRANCH_A = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnCode = new CrplControlLibrary.LookupButton(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txtW_ACC_DESC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSP_ACOUNT_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.txtSP_ASR_BASIC = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.txtSP_FORECAST_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpSP_VALID_TO = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpFromSP_VALID_FROM = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtSP_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.txtSP_MEAL_CONV = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtSP_RELATED_LEAV = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.txtSP_ATT_RELATED = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.txtSP_PER_ASR = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSP_ALL_IND = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDescription = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtDesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PnlDetails = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVCategory = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_ALLOW_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_P_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.PersonnelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_REMARKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.PnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVCategory)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 588);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label28);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(12, 69);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(602, 94);
            this.pnlHead.TabIndex = 10;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(79, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(480, 18);
            this.label28.TabIndex = 21;
            this.label28.Text = "[Allowances Excluding Housing/Conv.Off./Bonus Should Be Coded]";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(165, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(276, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "ALLOWANCE ENTRY";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(165, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(276, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Set Up";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 44);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(498, 18);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(455, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(447, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Option:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 44);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 18);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Visible = false;
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.lbtnCategory);
            this.pnlDetail.Controls.Add(this.txtSP_ALL_CODE);
            this.pnlDetail.Controls.Add(this.lbtnLevel);
            this.pnlDetail.Controls.Add(this.lbtnAccount);
            this.pnlDetail.Controls.Add(this.lbtnDesignation);
            this.pnlDetail.Controls.Add(this.lbtnBranch);
            this.pnlDetail.Controls.Add(this.label20);
            this.pnlDetail.Controls.Add(this.txtSP_BRANCH_A);
            this.pnlDetail.Controls.Add(this.lbtnCode);
            this.pnlDetail.Controls.Add(this.label18);
            this.pnlDetail.Controls.Add(this.txtW_ACC_DESC);
            this.pnlDetail.Controls.Add(this.txtSP_ACOUNT_NO);
            this.pnlDetail.Controls.Add(this.label16);
            this.pnlDetail.Controls.Add(this.txtSP_ASR_BASIC);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.txtSP_FORECAST_TAX);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.txtAmount);
            this.pnlDetail.Controls.Add(this.txtCategory);
            this.pnlDetail.Controls.Add(this.dtpSP_VALID_TO);
            this.pnlDetail.Controls.Add(this.dtpFromSP_VALID_FROM);
            this.pnlDetail.Controls.Add(this.txtSP_TAX);
            this.pnlDetail.Controls.Add(this.label21);
            this.pnlDetail.Controls.Add(this.txtSP_MEAL_CONV);
            this.pnlDetail.Controls.Add(this.label23);
            this.pnlDetail.Controls.Add(this.txtSP_RELATED_LEAV);
            this.pnlDetail.Controls.Add(this.label25);
            this.pnlDetail.Controls.Add(this.txtSP_ATT_RELATED);
            this.pnlDetail.Controls.Add(this.label27);
            this.pnlDetail.Controls.Add(this.txtSP_PER_ASR);
            this.pnlDetail.Controls.Add(this.label17);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.Controls.Add(this.txtSP_ALL_IND);
            this.pnlDetail.Controls.Add(this.label15);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.txtDescription);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtDesignation);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Cascading;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceSetupCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 162);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(526, 419);
            this.pnlDetail.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.pnlDetail.TabIndex = 21;
            this.pnlDetail.TabStop = true;
            // 
            // lbtnCategory
            // 
            this.lbtnCategory.ActionLOVExists = "CATGORY_LOV_EXISTS";
            this.lbtnCategory.ActionType = "CATGORY_LOV";
            this.lbtnCategory.ConditionalFields = "";
            this.lbtnCategory.CustomEnabled = true;
            this.lbtnCategory.DataFieldMapping = "";
            this.lbtnCategory.DependentLovControls = "";
            this.lbtnCategory.HiddenColumns = "";
            this.lbtnCategory.Image = ((System.Drawing.Image)(resources.GetObject("lbtnCategory.Image")));
            this.lbtnCategory.LoadDependentEntities = false;
            this.lbtnCategory.Location = new System.Drawing.Point(292, 85);
            this.lbtnCategory.LookUpTitle = null;
            this.lbtnCategory.Name = "lbtnCategory";
            this.lbtnCategory.Size = new System.Drawing.Size(26, 21);
            this.lbtnCategory.SkipValidationOnLeave = false;
            this.lbtnCategory.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnCategory.TabIndex = 121;
            this.lbtnCategory.TabStop = false;
            this.lbtnCategory.UseVisualStyleBackColor = true;
            // 
            // txtSP_ALL_CODE
            // 
            this.txtSP_ALL_CODE.AllowSpace = true;
            this.txtSP_ALL_CODE.AssociatedLookUpName = "lbtnCode";
            this.txtSP_ALL_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_ALL_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_ALL_CODE.ContinuationTextBox = null;
            this.txtSP_ALL_CODE.CustomEnabled = true;
            this.txtSP_ALL_CODE.DataFieldMapping = "SP_ALL_CODE";
            this.txtSP_ALL_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_ALL_CODE.GetRecordsOnUpDownKeys = false;
            this.txtSP_ALL_CODE.IsDate = false;
            this.txtSP_ALL_CODE.Location = new System.Drawing.Point(237, 10);
            this.txtSP_ALL_CODE.MaxLength = 3;
            this.txtSP_ALL_CODE.Name = "txtSP_ALL_CODE";
            this.txtSP_ALL_CODE.NumberFormat = "###,###,##0.00";
            this.txtSP_ALL_CODE.Postfix = "";
            this.txtSP_ALL_CODE.Prefix = "";
            this.txtSP_ALL_CODE.Size = new System.Drawing.Size(49, 20);
            this.txtSP_ALL_CODE.SkipValidation = false;
            this.txtSP_ALL_CODE.TabIndex = 2;
            this.txtSP_ALL_CODE.TextType = CrplControlLibrary.TextType.String;
            this.txtSP_ALL_CODE.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_ALL_CODE_Validating);
            // 
            // lbtnLevel
            // 
            this.lbtnLevel.ActionLOVExists = "LEVEL_LOV_EXISTS";
            this.lbtnLevel.ActionType = "LEVEL_LOV";
            this.lbtnLevel.ConditionalFields = "txtDesignation";
            this.lbtnLevel.CustomEnabled = true;
            this.lbtnLevel.DataFieldMapping = "";
            this.lbtnLevel.DependentLovControls = "";
            this.lbtnLevel.HiddenColumns = "";
            this.lbtnLevel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnLevel.Image")));
            this.lbtnLevel.LoadDependentEntities = false;
            this.lbtnLevel.Location = new System.Drawing.Point(292, 60);
            this.lbtnLevel.LookUpTitle = null;
            this.lbtnLevel.Name = "lbtnLevel";
            this.lbtnLevel.Size = new System.Drawing.Size(26, 21);
            this.lbtnLevel.SkipValidationOnLeave = false;
            this.lbtnLevel.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnLevel.TabIndex = 120;
            this.lbtnLevel.TabStop = false;
            this.lbtnLevel.UseVisualStyleBackColor = true;
            // 
            // lbtnAccount
            // 
            this.lbtnAccount.ActionLOVExists = "Account_LOVExists";
            this.lbtnAccount.ActionType = "Account_LOV";
            this.lbtnAccount.ConditionalFields = "";
            this.lbtnAccount.CustomEnabled = true;
            this.lbtnAccount.DataFieldMapping = "";
            this.lbtnAccount.DependentLovControls = "";
            this.lbtnAccount.HiddenColumns = "";
            this.lbtnAccount.Image = ((System.Drawing.Image)(resources.GetObject("lbtnAccount.Image")));
            this.lbtnAccount.LoadDependentEntities = false;
            this.lbtnAccount.Location = new System.Drawing.Point(340, 387);
            this.lbtnAccount.LookUpTitle = null;
            this.lbtnAccount.Name = "lbtnAccount";
            this.lbtnAccount.Size = new System.Drawing.Size(26, 21);
            this.lbtnAccount.SkipValidationOnLeave = true;
            this.lbtnAccount.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnAccount.TabIndex = 77;
            this.lbtnAccount.TabStop = false;
            this.lbtnAccount.Tag = "";
            this.lbtnAccount.UseVisualStyleBackColor = true;
            this.lbtnAccount.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnCode_MouseDown);
            // 
            // lbtnDesignation
            // 
            this.lbtnDesignation.ActionLOVExists = "Designation_LOVExists";
            this.lbtnDesignation.ActionType = "Designation_LOV";
            this.lbtnDesignation.ConditionalFields = "";
            this.lbtnDesignation.CustomEnabled = true;
            this.lbtnDesignation.DataFieldMapping = "";
            this.lbtnDesignation.DependentLovControls = "";
            this.lbtnDesignation.HiddenColumns = "";
            this.lbtnDesignation.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDesignation.Image")));
            this.lbtnDesignation.LoadDependentEntities = true;
            this.lbtnDesignation.Location = new System.Drawing.Point(292, 35);
            this.lbtnDesignation.LookUpTitle = null;
            this.lbtnDesignation.Name = "lbtnDesignation";
            this.lbtnDesignation.Size = new System.Drawing.Size(26, 21);
            this.lbtnDesignation.SkipValidationOnLeave = false;
            this.lbtnDesignation.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnDesignation.TabIndex = 76;
            this.lbtnDesignation.TabStop = false;
            this.lbtnDesignation.Tag = "";
            this.lbtnDesignation.UseVisualStyleBackColor = true;
            this.lbtnDesignation.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnCode_MouseDown);
            // 
            // lbtnBranch
            // 
            this.lbtnBranch.ActionLOVExists = "BranchCode_LOVExists";
            this.lbtnBranch.ActionType = "BranchCode_LOV";
            this.lbtnBranch.ConditionalFields = "";
            this.lbtnBranch.CustomEnabled = true;
            this.lbtnBranch.DataFieldMapping = "";
            this.lbtnBranch.DependentLovControls = "";
            this.lbtnBranch.HiddenColumns = "";
            this.lbtnBranch.Image = ((System.Drawing.Image)(resources.GetObject("lbtnBranch.Image")));
            this.lbtnBranch.LoadDependentEntities = true;
            this.lbtnBranch.Location = new System.Drawing.Point(467, 9);
            this.lbtnBranch.LookUpTitle = null;
            this.lbtnBranch.Name = "lbtnBranch";
            this.lbtnBranch.Size = new System.Drawing.Size(26, 21);
            this.lbtnBranch.SkipValidationOnLeave = false;
            this.lbtnBranch.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnBranch.TabIndex = 75;
            this.lbtnBranch.TabStop = false;
            this.lbtnBranch.Tag = "";
            this.lbtnBranch.UseVisualStyleBackColor = true;
            this.lbtnBranch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnCode_MouseDown);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(346, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 20);
            this.label20.TabIndex = 74;
            this.label20.Text = "Branch";
            // 
            // txtSP_BRANCH_A
            // 
            this.txtSP_BRANCH_A.AllowSpace = true;
            this.txtSP_BRANCH_A.AssociatedLookUpName = "lbtnBranch";
            this.txtSP_BRANCH_A.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtSP_BRANCH_A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_BRANCH_A.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_BRANCH_A.ContinuationTextBox = null;
            this.txtSP_BRANCH_A.CustomEnabled = true;
            this.txtSP_BRANCH_A.DataFieldMapping = "SP_BRANCH_A";
            this.txtSP_BRANCH_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_BRANCH_A.GetRecordsOnUpDownKeys = false;
            this.txtSP_BRANCH_A.IsDate = false;
            this.txtSP_BRANCH_A.Location = new System.Drawing.Point(412, 9);
            this.txtSP_BRANCH_A.MaxLength = 3;
            this.txtSP_BRANCH_A.Name = "txtSP_BRANCH_A";
            this.txtSP_BRANCH_A.NumberFormat = "###,###,##0.00";
            this.txtSP_BRANCH_A.Postfix = "";
            this.txtSP_BRANCH_A.Prefix = "";
            this.txtSP_BRANCH_A.Size = new System.Drawing.Size(49, 20);
            this.txtSP_BRANCH_A.SkipValidation = false;
            this.txtSP_BRANCH_A.TabIndex = 3;
            this.txtSP_BRANCH_A.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnCode
            // 
            this.lbtnCode.ActionLOVExists = "AllowanceCode_LOVExists";
            this.lbtnCode.ActionType = "AllowanceCode_LOV";
            this.lbtnCode.ConditionalFields = "";
            this.lbtnCode.CustomEnabled = true;
            this.lbtnCode.DataFieldMapping = "";
            this.lbtnCode.DependentLovControls = "";
            this.lbtnCode.HiddenColumns = "";
            this.lbtnCode.Image = ((System.Drawing.Image)(resources.GetObject("lbtnCode.Image")));
            this.lbtnCode.LoadDependentEntities = true;
            this.lbtnCode.Location = new System.Drawing.Point(292, 11);
            this.lbtnCode.LookUpTitle = null;
            this.lbtnCode.Name = "lbtnCode";
            this.lbtnCode.Size = new System.Drawing.Size(26, 21);
            this.lbtnCode.SkipValidationOnLeave = false;
            this.lbtnCode.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnCode.TabIndex = 72;
            this.lbtnCode.TabStop = false;
            this.lbtnCode.Tag = "";
            this.lbtnCode.UseVisualStyleBackColor = true;
            this.lbtnCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnCode_MouseDown);
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(376, 359);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(146, 20);
            this.label18.TabIndex = 71;
            this.label18.Text = "Account description";
            // 
            // txtW_ACC_DESC
            // 
            this.txtW_ACC_DESC.AllowSpace = true;
            this.txtW_ACC_DESC.AssociatedLookUpName = "";
            this.txtW_ACC_DESC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtW_ACC_DESC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtW_ACC_DESC.ContinuationTextBox = null;
            this.txtW_ACC_DESC.CustomEnabled = true;
            this.txtW_ACC_DESC.DataFieldMapping = "sp_acc_desc";
            this.txtW_ACC_DESC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtW_ACC_DESC.GetRecordsOnUpDownKeys = false;
            this.txtW_ACC_DESC.IsDate = false;
            this.txtW_ACC_DESC.Location = new System.Drawing.Point(372, 387);
            this.txtW_ACC_DESC.MaxLength = 1;
            this.txtW_ACC_DESC.Name = "txtW_ACC_DESC";
            this.txtW_ACC_DESC.NumberFormat = "###,###,##0.00";
            this.txtW_ACC_DESC.Postfix = "";
            this.txtW_ACC_DESC.Prefix = "";
            this.txtW_ACC_DESC.Size = new System.Drawing.Size(142, 20);
            this.txtW_ACC_DESC.SkipValidation = false;
            this.txtW_ACC_DESC.TabIndex = 20;
            this.txtW_ACC_DESC.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtW_ACC_DESC, "Enter [Y/N]");
            // 
            // txtSP_ACOUNT_NO
            // 
            this.txtSP_ACOUNT_NO.AllowSpace = true;
            this.txtSP_ACOUNT_NO.AssociatedLookUpName = "lbtnAccount";
            this.txtSP_ACOUNT_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_ACOUNT_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_ACOUNT_NO.ContinuationTextBox = null;
            this.txtSP_ACOUNT_NO.CustomEnabled = true;
            this.txtSP_ACOUNT_NO.DataFieldMapping = "SP_ACOUNT_NO";
            this.txtSP_ACOUNT_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_ACOUNT_NO.GetRecordsOnUpDownKeys = false;
            this.txtSP_ACOUNT_NO.IsDate = false;
            this.txtSP_ACOUNT_NO.Location = new System.Drawing.Point(237, 387);
            this.txtSP_ACOUNT_NO.MaxLength = 1;
            this.txtSP_ACOUNT_NO.Name = "txtSP_ACOUNT_NO";
            this.txtSP_ACOUNT_NO.NumberFormat = "###,###,##0.00";
            this.txtSP_ACOUNT_NO.Postfix = "";
            this.txtSP_ACOUNT_NO.Prefix = "";
            this.txtSP_ACOUNT_NO.Size = new System.Drawing.Size(100, 20);
            this.txtSP_ACOUNT_NO.SkipValidation = true;
            this.txtSP_ACOUNT_NO.TabIndex = 19;
            this.txtSP_ACOUNT_NO.TextType = CrplControlLibrary.TextType.String;
            this.txtSP_ACOUNT_NO.Validated += new System.EventHandler(this.txtSP_ACOUNT_NO_Validated);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(55, 387);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(183, 20);
            this.label16.TabIndex = 69;
            this.label16.Text = "16)Account No. To Effect";
            // 
            // txtSP_ASR_BASIC
            // 
            this.txtSP_ASR_BASIC.AllowSpace = true;
            this.txtSP_ASR_BASIC.AssociatedLookUpName = "";
            this.txtSP_ASR_BASIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_ASR_BASIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_ASR_BASIC.ContinuationTextBox = null;
            this.txtSP_ASR_BASIC.CustomEnabled = true;
            this.txtSP_ASR_BASIC.DataFieldMapping = "SP_ASR_BASIC";
            this.txtSP_ASR_BASIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_ASR_BASIC.GetRecordsOnUpDownKeys = false;
            this.txtSP_ASR_BASIC.IsDate = false;
            this.txtSP_ASR_BASIC.Location = new System.Drawing.Point(237, 361);
            this.txtSP_ASR_BASIC.MaxLength = 1;
            this.txtSP_ASR_BASIC.Name = "txtSP_ASR_BASIC";
            this.txtSP_ASR_BASIC.NumberFormat = "###,###,##0.00";
            this.txtSP_ASR_BASIC.Postfix = "";
            this.txtSP_ASR_BASIC.Prefix = "";
            this.txtSP_ASR_BASIC.Size = new System.Drawing.Size(49, 20);
            this.txtSP_ASR_BASIC.SkipValidation = false;
            this.txtSP_ASR_BASIC.TabIndex = 18;
            this.txtSP_ASR_BASIC.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_ASR_BASIC, "Enter [A/B]");
            this.txtSP_ASR_BASIC.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_ASR_BASIC_Validating);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(55, 361);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(183, 20);
            this.label14.TabIndex = 67;
            this.label14.Text = "15)ASR / Basic";
            // 
            // txtSP_FORECAST_TAX
            // 
            this.txtSP_FORECAST_TAX.AllowSpace = true;
            this.txtSP_FORECAST_TAX.AssociatedLookUpName = "";
            this.txtSP_FORECAST_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_FORECAST_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_FORECAST_TAX.ContinuationTextBox = null;
            this.txtSP_FORECAST_TAX.CustomEnabled = true;
            this.txtSP_FORECAST_TAX.DataFieldMapping = "SP_FORECAST_TAX";
            this.txtSP_FORECAST_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_FORECAST_TAX.GetRecordsOnUpDownKeys = false;
            this.txtSP_FORECAST_TAX.IsDate = false;
            this.txtSP_FORECAST_TAX.Location = new System.Drawing.Point(237, 335);
            this.txtSP_FORECAST_TAX.MaxLength = 1;
            this.txtSP_FORECAST_TAX.Name = "txtSP_FORECAST_TAX";
            this.txtSP_FORECAST_TAX.NumberFormat = "###,###,##0.00";
            this.txtSP_FORECAST_TAX.Postfix = "";
            this.txtSP_FORECAST_TAX.Prefix = "";
            this.txtSP_FORECAST_TAX.Size = new System.Drawing.Size(49, 20);
            this.txtSP_FORECAST_TAX.SkipValidation = false;
            this.txtSP_FORECAST_TAX.TabIndex = 17;
            this.txtSP_FORECAST_TAX.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_FORECAST_TAX, "Enter [Y/N]");
            this.txtSP_FORECAST_TAX.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_FORECAST_TAX_Validating);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(55, 335);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(183, 20);
            this.label13.TabIndex = 65;
            this.label13.Text = "14)Tax Forecasted";
            // 
            // txtAmount
            // 
            this.txtAmount.AllowSpace = true;
            this.txtAmount.AssociatedLookUpName = "";
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAmount.ContinuationTextBox = null;
            this.txtAmount.CustomEnabled = true;
            this.txtAmount.DataFieldMapping = "SP_ALL_AMOUNT";
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.GetRecordsOnUpDownKeys = false;
            this.txtAmount.IsDate = false;
            this.txtAmount.Location = new System.Drawing.Point(237, 186);
            this.txtAmount.MaxLength = 30;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.NumberFormat = "###,###,##0.00";
            this.txtAmount.Postfix = "";
            this.txtAmount.Prefix = "";
            this.txtAmount.Size = new System.Drawing.Size(49, 20);
            this.txtAmount.SkipValidation = false;
            this.txtAmount.TabIndex = 11;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextType = CrplControlLibrary.TextType.Double;
            this.txtAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAmount_Validating);
            // 
            // txtCategory
            // 
            this.txtCategory.AllowSpace = true;
            this.txtCategory.AssociatedLookUpName = "lbtnCategory";
            this.txtCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategory.ContinuationTextBox = null;
            this.txtCategory.CustomEnabled = true;
            this.txtCategory.DataFieldMapping = "SP_CATEGORY_A";
            this.txtCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.GetRecordsOnUpDownKeys = false;
            this.txtCategory.IsDate = false;
            this.txtCategory.Location = new System.Drawing.Point(237, 85);
            this.txtCategory.MaxLength = 3;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.NumberFormat = "###,###,##0.00";
            this.txtCategory.Postfix = "";
            this.txtCategory.Prefix = "";
            this.txtCategory.Size = new System.Drawing.Size(49, 20);
            this.txtCategory.SkipValidation = false;
            this.txtCategory.TabIndex = 6;
            this.txtCategory.TextType = CrplControlLibrary.TextType.String;
            this.txtCategory.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory_Validating);
            // 
            // dtpSP_VALID_TO
            // 
            this.dtpSP_VALID_TO.CustomEnabled = true;
            this.dtpSP_VALID_TO.CustomFormat = "dd/MM/yyyy";
            this.dtpSP_VALID_TO.DataFieldMapping = "SP_VALID_TO";
            this.dtpSP_VALID_TO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSP_VALID_TO.HasChanges = true;
            this.dtpSP_VALID_TO.Location = new System.Drawing.Point(351, 136);
            this.dtpSP_VALID_TO.Name = "dtpSP_VALID_TO";
            this.dtpSP_VALID_TO.NullValue = " ";
            this.dtpSP_VALID_TO.Size = new System.Drawing.Size(100, 20);
            this.dtpSP_VALID_TO.TabIndex = 9;
            this.dtpSP_VALID_TO.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpSP_VALID_TO.Validating += new System.ComponentModel.CancelEventHandler(this.dtpSP_VALID_TO_Validating);
            // 
            // dtpFromSP_VALID_FROM
            // 
            this.dtpFromSP_VALID_FROM.CustomEnabled = true;
            this.dtpFromSP_VALID_FROM.CustomFormat = "dd/MM/yyyy";
            this.dtpFromSP_VALID_FROM.DataFieldMapping = "SP_VALID_FROM";
            this.dtpFromSP_VALID_FROM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromSP_VALID_FROM.HasChanges = true;
            this.dtpFromSP_VALID_FROM.Location = new System.Drawing.Point(237, 136);
            this.dtpFromSP_VALID_FROM.Name = "dtpFromSP_VALID_FROM";
            this.dtpFromSP_VALID_FROM.NullValue = " ";
            this.dtpFromSP_VALID_FROM.Size = new System.Drawing.Size(100, 20);
            this.dtpFromSP_VALID_FROM.TabIndex = 8;
            this.dtpFromSP_VALID_FROM.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            this.dtpFromSP_VALID_FROM.Validating += new System.ComponentModel.CancelEventHandler(this.dtpFromSP_VALID_FROM_Validating);
            // 
            // txtSP_TAX
            // 
            this.txtSP_TAX.AllowSpace = true;
            this.txtSP_TAX.AssociatedLookUpName = "";
            this.txtSP_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_TAX.ContinuationTextBox = null;
            this.txtSP_TAX.CustomEnabled = true;
            this.txtSP_TAX.DataFieldMapping = "SP_TAX";
            this.txtSP_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_TAX.GetRecordsOnUpDownKeys = false;
            this.txtSP_TAX.IsDate = false;
            this.txtSP_TAX.Location = new System.Drawing.Point(237, 309);
            this.txtSP_TAX.MaxLength = 1;
            this.txtSP_TAX.Name = "txtSP_TAX";
            this.txtSP_TAX.NumberFormat = "###,###,##0.00";
            this.txtSP_TAX.Postfix = "";
            this.txtSP_TAX.Prefix = "";
            this.txtSP_TAX.Size = new System.Drawing.Size(49, 20);
            this.txtSP_TAX.SkipValidation = false;
            this.txtSP_TAX.TabIndex = 16;
            this.txtSP_TAX.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_TAX, "Enter [Y/N]");
            this.txtSP_TAX.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_TAX_Validating);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(55, 309);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(183, 20);
            this.label21.TabIndex = 55;
            this.label21.Text = "13)Taxable";
            // 
            // txtSP_MEAL_CONV
            // 
            this.txtSP_MEAL_CONV.AllowSpace = true;
            this.txtSP_MEAL_CONV.AssociatedLookUpName = "";
            this.txtSP_MEAL_CONV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_MEAL_CONV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_MEAL_CONV.ContinuationTextBox = null;
            this.txtSP_MEAL_CONV.CustomEnabled = true;
            this.txtSP_MEAL_CONV.DataFieldMapping = "SP_MEAL_CONV";
            this.txtSP_MEAL_CONV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_MEAL_CONV.GetRecordsOnUpDownKeys = false;
            this.txtSP_MEAL_CONV.IsDate = false;
            this.txtSP_MEAL_CONV.Location = new System.Drawing.Point(237, 285);
            this.txtSP_MEAL_CONV.MaxLength = 1;
            this.txtSP_MEAL_CONV.Name = "txtSP_MEAL_CONV";
            this.txtSP_MEAL_CONV.NumberFormat = "###,###,##0.00";
            this.txtSP_MEAL_CONV.Postfix = "";
            this.txtSP_MEAL_CONV.Prefix = "";
            this.txtSP_MEAL_CONV.Size = new System.Drawing.Size(49, 20);
            this.txtSP_MEAL_CONV.SkipValidation = false;
            this.txtSP_MEAL_CONV.TabIndex = 15;
            this.txtSP_MEAL_CONV.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_MEAL_CONV, "Enter [M/C]");
            this.txtSP_MEAL_CONV.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_MEAL_CONV_Validating);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(55, 285);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(183, 20);
            this.label23.TabIndex = 52;
            this.label23.Text = "12)Meal / Conveyance";
            // 
            // txtSP_RELATED_LEAV
            // 
            this.txtSP_RELATED_LEAV.AllowSpace = true;
            this.txtSP_RELATED_LEAV.AssociatedLookUpName = "";
            this.txtSP_RELATED_LEAV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_RELATED_LEAV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_RELATED_LEAV.ContinuationTextBox = null;
            this.txtSP_RELATED_LEAV.CustomEnabled = true;
            this.txtSP_RELATED_LEAV.DataFieldMapping = "SP_RELATED_LEAV";
            this.txtSP_RELATED_LEAV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_RELATED_LEAV.GetRecordsOnUpDownKeys = false;
            this.txtSP_RELATED_LEAV.IsDate = false;
            this.txtSP_RELATED_LEAV.Location = new System.Drawing.Point(237, 261);
            this.txtSP_RELATED_LEAV.MaxLength = 1;
            this.txtSP_RELATED_LEAV.Name = "txtSP_RELATED_LEAV";
            this.txtSP_RELATED_LEAV.NumberFormat = "###,###,##0.00";
            this.txtSP_RELATED_LEAV.Postfix = "";
            this.txtSP_RELATED_LEAV.Prefix = "";
            this.txtSP_RELATED_LEAV.Size = new System.Drawing.Size(49, 20);
            this.txtSP_RELATED_LEAV.SkipValidation = false;
            this.txtSP_RELATED_LEAV.TabIndex = 14;
            this.txtSP_RELATED_LEAV.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_RELATED_LEAV, "Enter [Y/N]");
            this.txtSP_RELATED_LEAV.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_RELATED_LEAV_Validating);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(55, 261);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(183, 20);
            this.label25.TabIndex = 49;
            this.label25.Text = "11)Related With Leave";
            // 
            // txtSP_ATT_RELATED
            // 
            this.txtSP_ATT_RELATED.AllowSpace = true;
            this.txtSP_ATT_RELATED.AssociatedLookUpName = "";
            this.txtSP_ATT_RELATED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_ATT_RELATED.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_ATT_RELATED.ContinuationTextBox = null;
            this.txtSP_ATT_RELATED.CustomEnabled = true;
            this.txtSP_ATT_RELATED.DataFieldMapping = "SP_ATT_RELATED";
            this.txtSP_ATT_RELATED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_ATT_RELATED.GetRecordsOnUpDownKeys = false;
            this.txtSP_ATT_RELATED.IsDate = false;
            this.txtSP_ATT_RELATED.Location = new System.Drawing.Point(237, 236);
            this.txtSP_ATT_RELATED.MaxLength = 1;
            this.txtSP_ATT_RELATED.Name = "txtSP_ATT_RELATED";
            this.txtSP_ATT_RELATED.NumberFormat = "###,###,##0.00";
            this.txtSP_ATT_RELATED.Postfix = "";
            this.txtSP_ATT_RELATED.Prefix = "";
            this.txtSP_ATT_RELATED.Size = new System.Drawing.Size(49, 20);
            this.txtSP_ATT_RELATED.SkipValidation = false;
            this.txtSP_ATT_RELATED.TabIndex = 13;
            this.txtSP_ATT_RELATED.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_ATT_RELATED, "Enter [Y/N]");
            this.txtSP_ATT_RELATED.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_ATT_RELATED_Validating);
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(55, 236);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(183, 20);
            this.label27.TabIndex = 46;
            this.label27.Text = "10)Related With Attendance";
            // 
            // txtSP_PER_ASR
            // 
            this.txtSP_PER_ASR.AllowSpace = true;
            this.txtSP_PER_ASR.AssociatedLookUpName = "";
            this.txtSP_PER_ASR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_PER_ASR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_PER_ASR.ContinuationTextBox = null;
            this.txtSP_PER_ASR.CustomEnabled = true;
            this.txtSP_PER_ASR.DataFieldMapping = "SP_PER_ASR";
            this.txtSP_PER_ASR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_PER_ASR.GetRecordsOnUpDownKeys = false;
            this.txtSP_PER_ASR.IsDate = false;
            this.txtSP_PER_ASR.Location = new System.Drawing.Point(237, 210);
            this.txtSP_PER_ASR.MaxLength = 2;
            this.txtSP_PER_ASR.Name = "txtSP_PER_ASR";
            this.txtSP_PER_ASR.NumberFormat = "###,###,##0.00";
            this.txtSP_PER_ASR.Postfix = "";
            this.txtSP_PER_ASR.Prefix = "";
            this.txtSP_PER_ASR.Size = new System.Drawing.Size(49, 20);
            this.txtSP_PER_ASR.SkipValidation = false;
            this.txtSP_PER_ASR.TabIndex = 12;
            this.txtSP_PER_ASR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSP_PER_ASR.TextType = CrplControlLibrary.TextType.Double;
            this.txtSP_PER_ASR.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_PER_ASR_Validating);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(55, 210);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(183, 20);
            this.label17.TabIndex = 43;
            this.label17.Text = "9)% With Annual Package";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(55, 186);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(183, 20);
            this.label19.TabIndex = 40;
            this.label19.Text = "8)Amount";
            // 
            // txtSP_ALL_IND
            // 
            this.txtSP_ALL_IND.AllowSpace = true;
            this.txtSP_ALL_IND.AssociatedLookUpName = "";
            this.txtSP_ALL_IND.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_ALL_IND.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_ALL_IND.ContinuationTextBox = null;
            this.txtSP_ALL_IND.CustomEnabled = true;
            this.txtSP_ALL_IND.DataFieldMapping = "SP_ALL_IND";
            this.txtSP_ALL_IND.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_ALL_IND.GetRecordsOnUpDownKeys = false;
            this.txtSP_ALL_IND.IsDate = false;
            this.txtSP_ALL_IND.IsRequired = true;
            this.txtSP_ALL_IND.Location = new System.Drawing.Point(237, 162);
            this.txtSP_ALL_IND.MaxLength = 1;
            this.txtSP_ALL_IND.Name = "txtSP_ALL_IND";
            this.txtSP_ALL_IND.NumberFormat = "###,###,##0.00";
            this.txtSP_ALL_IND.Postfix = "";
            this.txtSP_ALL_IND.Prefix = "";
            this.txtSP_ALL_IND.Size = new System.Drawing.Size(49, 20);
            this.txtSP_ALL_IND.SkipValidation = false;
            this.txtSP_ALL_IND.TabIndex = 10;
            this.txtSP_ALL_IND.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_ALL_IND, "Enter I/M]");
            this.txtSP_ALL_IND.Validated += new System.EventHandler(this.txtSP_ALL_IND_Validated);
            this.txtSP_ALL_IND.Validating += new System.ComponentModel.CancelEventHandler(this.txtSP_ALL_IND_Validating);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(55, 162);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(183, 20);
            this.label15.TabIndex = 37;
            this.label15.Text = "7)Individual / All";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(55, 136);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(183, 20);
            this.label12.TabIndex = 34;
            this.label12.Text = "6)Valid From/To";
            // 
            // txtDescription
            // 
            this.txtDescription.AllowSpace = true;
            this.txtDescription.AssociatedLookUpName = "";
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescription.ContinuationTextBox = null;
            this.txtDescription.CustomEnabled = true;
            this.txtDescription.DataFieldMapping = "SP_DESC";
            this.txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.GetRecordsOnUpDownKeys = false;
            this.txtDescription.IsDate = false;
            this.txtDescription.IsRequired = true;
            this.txtDescription.Location = new System.Drawing.Point(237, 110);
            this.txtDescription.MaxLength = 30;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.NumberFormat = "###,###,##0.00";
            this.txtDescription.Postfix = "";
            this.txtDescription.Prefix = "";
            this.txtDescription.Size = new System.Drawing.Size(202, 20);
            this.txtDescription.SkipValidation = false;
            this.txtDescription.TabIndex = 7;
            this.txtDescription.TextType = CrplControlLibrary.TextType.String;
            this.txtDescription.Validating += new System.ComponentModel.CancelEventHandler(this.txtDescription_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(55, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(183, 20);
            this.label11.TabIndex = 32;
            this.label11.Text = "5)Description";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(55, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(183, 20);
            this.label10.TabIndex = 30;
            this.label10.Text = "4)Category";
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "lbtnLevel";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "SP_LEVEL_A";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(237, 60);
            this.txtLevel.MaxLength = 1;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(49, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 5;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtLevel, "Press [T]ermination or [R]esignation or [S]eparation or [M]is Conduct");
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(55, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(183, 20);
            this.label9.TabIndex = 28;
            this.label9.Text = "3)Level";
            // 
            // txtDesignation
            // 
            this.txtDesignation.AllowSpace = true;
            this.txtDesignation.AssociatedLookUpName = "lbtnDesignation";
            this.txtDesignation.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesignation.ContinuationTextBox = null;
            this.txtDesignation.CustomEnabled = true;
            this.txtDesignation.DataFieldMapping = "SP_DESG_A";
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.GetRecordsOnUpDownKeys = false;
            this.txtDesignation.IsDate = false;
            this.txtDesignation.Location = new System.Drawing.Point(237, 35);
            this.txtDesignation.MaxLength = 3;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.NumberFormat = "###,###,##0.00";
            this.txtDesignation.Postfix = "";
            this.txtDesignation.Prefix = "";
            this.txtDesignation.Size = new System.Drawing.Size(49, 20);
            this.txtDesignation.SkipValidation = false;
            this.txtDesignation.TabIndex = 4;
            this.txtDesignation.TextType = CrplControlLibrary.TextType.String;
            this.txtDesignation.Validating += new System.ComponentModel.CancelEventHandler(this.txtDesignation_Validating);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(55, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(183, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "2)Designation";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(55, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "1)Allow.  Abbreviation";
            // 
            // PnlDetails
            // 
            this.PnlDetails.ConcurrentPanels = null;
            this.PnlDetails.Controls.Add(this.DGVCategory);
            this.PnlDetails.DataManager = "iCORE.Common.CommonDataManager";
            this.PnlDetails.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Cascading;
            this.PnlDetails.DependentPanels = null;
            this.PnlDetails.DisableDependentLoad = false;
            this.PnlDetails.EnableDelete = true;
            this.PnlDetails.EnableInsert = true;
            this.PnlDetails.EnableQuery = false;
            this.PnlDetails.EnableUpdate = true;
            this.PnlDetails.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.PnlDetails.Location = new System.Drawing.Point(544, 372);
            this.PnlDetails.MasterPanel = this.pnlDetail;
            this.PnlDetails.Name = "PnlDetails";
            this.PnlDetails.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetails.Size = new System.Drawing.Size(81, 128);
            this.PnlDetails.SPName = "CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER";
            this.PnlDetails.TabIndex = 22;
            this.PnlDetails.Visible = false;
            // 
            // DGVCategory
            // 
            this.DGVCategory.AllowUserToAddRows = false;
            this.DGVCategory.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCategory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVCategory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_ALLOW_CODE,
            this.Column1,
            this.SP_P_NO,
            this.PersonnelName,
            this.SP_REMARKS});
            this.DGVCategory.ColumnToHide = null;
            this.DGVCategory.ColumnWidth = null;
            this.DGVCategory.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVCategory.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVCategory.DisplayColumnWrapper = null;
            this.DGVCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVCategory.GridDefaultRow = 0;
            this.DGVCategory.Location = new System.Drawing.Point(0, 0);
            this.DGVCategory.Name = "DGVCategory";
            this.DGVCategory.ReadOnlyColumns = null;
            this.DGVCategory.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVCategory.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVCategory.Size = new System.Drawing.Size(81, 128);
            this.DGVCategory.SkippingColumns = null;
            this.DGVCategory.TabIndex = 0;
            // 
            // SP_ALLOW_CODE
            // 
            this.SP_ALLOW_CODE.DataPropertyName = "SP_ALL_CODE";
            this.SP_ALLOW_CODE.HeaderText = "SP_ALLOW_CODE";
            this.SP_ALLOW_CODE.Name = "SP_ALLOW_CODE";
            this.SP_ALLOW_CODE.ReadOnly = true;
            this.SP_ALLOW_CODE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_ALLOW_CODE.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // SP_P_NO
            // 
            this.SP_P_NO.ActionLOV = "Pr_PNO_LOV";
            this.SP_P_NO.ActionLOVExists = "Pr_PNO_LOVExists";
            this.SP_P_NO.AttachParentEntity = false;
            this.SP_P_NO.DataPropertyName = "SP_P_NO";
            this.SP_P_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.SP_P_NO.HeaderText = "Personnel No.";
            this.SP_P_NO.LookUpTitle = null;
            this.SP_P_NO.LOVFieldMapping = "SP_P_NO";
            this.SP_P_NO.Name = "SP_P_NO";
            this.SP_P_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_P_NO.SearchColumn = "SP_P_NO";
            this.SP_P_NO.SkipValidationOnLeave = false;
            this.SP_P_NO.SpName = "CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER";
            // 
            // PersonnelName
            // 
            this.PersonnelName.DataPropertyName = "PR_Name";
            this.PersonnelName.HeaderText = "Name";
            this.PersonnelName.Name = "PersonnelName";
            // 
            // SP_REMARKS
            // 
            this.SP_REMARKS.DataPropertyName = "SP_REMARKS";
            this.SP_REMARKS.HeaderText = "Remarks";
            this.SP_REMARKS.Name = "SP_REMARKS";
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(421, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(75, 13);
            this.txtUserName.TabIndex = 119;
            this.txtUserName.Text = "User  Name  : ";
            // 
            // CHRIS_Setup_AllowanceEnter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(644, 648);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.PnlDetails);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Setup_AllowanceEnter";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCore CHRIS :  Allowance Enter";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.PnlDetails, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.PnlDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVCategory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtSP_TAX;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtSP_MEAL_CONV;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtSP_RELATED_LEAV;
        private System.Windows.Forms.Label label25;
        private CrplControlLibrary.SLTextBox txtSP_ATT_RELATED;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox txtSP_PER_ASR;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtSP_ALL_IND;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtDescription;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtLevel;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtDesignation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLTextBox txtCategory;
        private CrplControlLibrary.SLDatePicker dtpSP_VALID_TO;
        private CrplControlLibrary.SLDatePicker dtpFromSP_VALID_FROM;
        private CrplControlLibrary.SLTextBox txtW_ACC_DESC;
        private CrplControlLibrary.SLTextBox txtSP_ACOUNT_NO;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtSP_ASR_BASIC;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtSP_FORECAST_TAX;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtAmount;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.LookupButton lbtnCode;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtSP_BRANCH_A;
        private CrplControlLibrary.LookupButton lbtnBranch;
        private CrplControlLibrary.LookupButton lbtnDesignation;
        private CrplControlLibrary.LookupButton lbtnAccount;
     
        private CrplControlLibrary.SLTextBox txtSP_ALL_CODE;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetails;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_ALLOW_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonnelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_REMARKS;
        private System.Windows.Forms.Label txtUserName;
        private CrplControlLibrary.LookupButton lbtnCategory;
        private CrplControlLibrary.LookupButton lbtnLevel;
       
    }
}