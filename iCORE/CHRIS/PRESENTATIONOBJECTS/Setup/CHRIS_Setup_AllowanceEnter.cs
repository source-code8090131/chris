using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_AllowanceEnter : ChrisMasterDetailForm
    {

        iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEntry_MasterDetail frmAllowanceMD;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details frm_Detail;
        public string SeeDtlpp = string.Empty;
        public string DeleteRecpp = string.Empty;
        public string DeleteDtlpp = string.Empty;
        public string ModifyDtlpp = string.Empty;
        bool NoDetails = false;
        bool returnValue = false;
        public CHRIS_Setup_AllowanceEnter()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_AllowanceEnter(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlDetails);
            this.pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);

            this.FunctionConfig.EnableF8 = true;
            this.FunctionConfig.F8 = Function.Query;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtSave.Available = false;

            tbtDelete.Enabled = false;
            this.txtUserName.Text += "   " + this.UserName;
            
        

        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.FunctionConfig.CurrentOption = Function.None;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
        }

        protected override bool Add()
        {

            lbtnCode.SkipValidationOnLeave = true;
            lbtnCode.ActionType = "AllowanceNULL_LOV";
            lbtnCode.ActionLOVExists = "";
             base.Add();
           

             return false;
        }
        protected override bool Edit()
        {

            lbtnCode.ActionType = "AllowanceCode_LOV";
            lbtnCode.ActionLOVExists = "AllowanceCode_LOVExists";
            base.Edit();
            lbtnCode.SkipValidationOnLeave = false;

            return false;
        }

        protected override bool View()
        {

            lbtnCode.ActionType = "AllowanceCode_LOV";
            lbtnCode.ActionLOVExists = "AllowanceCode_LOVExists";
            base.View();
            lbtnCode.SkipValidationOnLeave = false;

            return false;
        }

        protected override bool Delete()
        {

            lbtnCode.ActionType = "AllowanceCode_LOV";
            lbtnCode.ActionLOVExists = "AllowanceCode_LOVExists";
            base.Delete();
            lbtnCode.SkipValidationOnLeave = false;

            return false;
        }

        protected override bool Quit()
        {


            base.Quit();
          

            return false;
        }
        protected override bool Query()
        {
            base.Query();
            lbtnCode.SkipValidationOnLeave = true;
            frmAllowanceMD = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEntry_MasterDetail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
            frmAllowanceMD.MdiParent = null;
            frmAllowanceMD.ShowDialog();

            return false;
        }

        private void txtDesignation_Validating(object sender, CancelEventArgs e)
        {



            if (txtDesignation.Text == string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                if (txtSP_ALL_CODE.Text != string.Empty)
                    param.Add("SP_ALL_CODE", txtSP_ALL_CODE.Text);

                if (txtSP_BRANCH_A.Text != string.Empty)
                    param.Add("SP_BRANCH_A", txtSP_BRANCH_A.Text);

                rsltCode = cmnDM.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "DESIGNATION_FROM_FILL", param);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtDescription.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    dtpFromSP_VALID_FROM.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    dtpSP_VALID_TO.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString());
                    txtSP_ALL_IND.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtAmount.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtSP_PER_ASR.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtSP_ATT_RELATED.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtSP_RELATED_LEAV.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtSP_MEAL_CONV.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[14].ToString();
                    txtSP_TAX.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtSP_FORECAST_TAX.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                    txtSP_ASR_BASIC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    txtSP_ACOUNT_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();

                }
                if (txtOption.Text == "M" || txtOption.Text == "D")
                {
                    returnValue = proc_mod_Del();
                }
                if (txtOption.Text == "V" || txtOption.Text == "A")
                {
                    returnValue = proc_view_add();
                }



                if (this.FunctionConfig.CurrentOption == Function.Add && txtDescription.Text != string.Empty)
                {
                    MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    base.Cancel();
                    txtOption.Focus();


                }
                else if (returnValue != false)
                {
                    txtSP_ALL_CODE.Select();
                    txtSP_ALL_CODE.Focus();
                }
                else
                {
                    e.Cancel = true;
                }

                if (this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    txtSP_ALL_CODE.Select();
                    txtSP_ALL_CODE.Focus();
                }
                if (this.FunctionConfig.CurrentOption == Function.View && returnValue != false)
                {
                    //go_field('blk_head.w_ans5');
                    DialogResult dRes = MessageBox.Show("Do You Want To See The Details [Y]es [N]o", "Note"
                                                     , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        if (txtSP_ALL_IND.Text == "A")
                        {
                            MessageBox.Show("Sorry No Details Available");
                            return;
                        }

                        if (this.FunctionConfig.CurrentOption == Function.View)
                        {
                            frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);

                            frm_Detail.MdiParent = null;
                            frm_Detail.ShowDialog();
                        }

                        if (this.FunctionConfig.CurrentOption == Function.Delete)
                        {
                            frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);

                            frm_Detail.MdiParent = null;
                            frm_Detail.ShowDialog();

                            DialogResult dRes1 = MessageBox.Show("Do you want to Delete the Record [Y/N]..", "Note"
                                                     , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dRes1 == DialogResult.Yes)
                            {

                                base.DoToolbarActions(this.Controls, "Delete");
                                //this.Reset();
                                //call save
                                return;

                            }
                            else if (dRes1 == DialogResult.No)
                            {
                                // this.Reset();
                                txtOption.Focus();

                                return;
                            }
                        }
                        //this.Reset();
                        //call save
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        // this.Reset();
                        txtOption.Focus();

                        return;
                    }
                }
                if (this.FunctionConfig.CurrentOption == Function.Delete)
                {
                    //go_field('blk_head.w_ans7');
                    frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);

                    frm_Detail.MdiParent = null;
                    frm_Detail.ShowDialog();

                    DialogResult dRes1 = MessageBox.Show("Do you want to Delete the Record [Y/N]..", "Note"
                                             , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes1 == DialogResult.Yes)
                    {
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                        base.DoToolbarActions(this.Controls, "Delete");
                        //this.Reset();
                        //call save
                        return;

                    }
                    else if (dRes1 == DialogResult.No)
                    {
                        // this.Reset();
                        txtOption.Focus();

                        return;
                    }
                }



            }
        }

        private void txtDescription_Validating(object sender, CancelEventArgs e)
        {
            if (FunctionConfig.CurrentOption == Function.Add)
            {
                dtpFromSP_VALID_FROM.Focus();
               
            }
            else
            {
                dtpSP_VALID_TO.Focus();
            }
        }

        private void dtpFromSP_VALID_FROM_Validating(object sender, CancelEventArgs e)
        {

            DataTable DtPROC_1;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("SP_VALID_FROM", dtpFromSP_VALID_FROM.Value);
         
            DtPROC_1 = GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CheckPayroll", colsNVals);
            if (DtPROC_1 != null)
            {
                if (DtPROC_1.Rows.Count > 0)
                {
                    MessageBox.Show("Payroll Generated For The Given Date Range");
                    dtpFromSP_VALID_FROM.Focus();
                }
            }

        }


        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        private void dtpSP_VALID_TO_Validating(object sender, CancelEventArgs e)
        {
            if (dtpSP_VALID_TO.Value != null)
            {
                DateTime dt1 = Convert.ToDateTime(dtpSP_VALID_TO.Value);
                DateTime dt2 = Convert.ToDateTime(dtpFromSP_VALID_FROM.Value);
                if (DateTime.Compare(dt2, dt1) > 0)
                {
                    MessageBox.Show("To Date Must Be Greater Than From Date");
                    dtpSP_VALID_TO.Focus();
                    return;
                }

            }
        }

        private void txtSP_ALL_IND_Validating(object sender, CancelEventArgs e)
        {
            if (txtSP_ALL_IND.Text != string.Empty)
            {
                if (txtSP_ALL_IND.Text != "A" && txtSP_ALL_IND.Text != "I")
                {
                    MessageBox.Show("Invalid Entry ... Please Enter  A  Or  I");
                    txtSP_ALL_IND.Focus();
                    txtSP_ALL_IND.Text = "";
                    return;
                }
            }

        }

        private void txtAmount_Validating(object sender, CancelEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                if (txtAmount.Text != string.Empty)
                {
                    txtSP_ATT_RELATED.Focus();
                }
            }

            else
            {
                if (txtAmount.Text != string.Empty)
                {
                    txtSP_PER_ASR.Text = "";
                    txtSP_TAX.Focus();
                }
            }
        }

        private void txtSP_PER_ASR_Validating(object sender, CancelEventArgs e)
        {
            if (txtAmount.Text == string.Empty && txtSP_PER_ASR.Text == string.Empty)
            {
                MessageBox.Show("Enter Percentage .........!");
                txtAmount.Focus();
                return;
            }
            if (this.FunctionConfig.CurrentOption == Function.Modify)
            {
                txtSP_TAX.Focus();
            }
        }

        private void txtSP_ATT_RELATED_Validating(object sender, CancelEventArgs e)
        {
            if (txtSP_ATT_RELATED.Text != string.Empty)
            {
                if (txtSP_ATT_RELATED.Text != "M" && txtSP_ATT_RELATED.Text != "Q" && txtSP_ATT_RELATED.Text != "Y")
                {
                    MessageBox.Show("Press [M]onthly  or  [Q]uaterly  or  [Y]early");
                    txtSP_ATT_RELATED.Focus();
                    txtSP_ATT_RELATED.Text = "";
                    return;
                }
            }

        }

        private void txtSP_RELATED_LEAV_Validating(object sender, CancelEventArgs e)
        {
            if (txtSP_RELATED_LEAV.Text != string.Empty )
            {
                if(txtSP_RELATED_LEAV.Text !="L")
                {
                MessageBox.Show("Entry Must Be ''L'' Or Blank");
                txtSP_RELATED_LEAV.Text = "";
                txtSP_RELATED_LEAV.Focus();
                return;
                }
            }
        }

        private void txtSP_MEAL_CONV_Validating(object sender, CancelEventArgs e)
        {
            DataTable DtPROC_2;
            Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
            if (txtSP_MEAL_CONV.Text != string.Empty)
            {
                if (txtSP_MEAL_CONV.Text != "M" && txtSP_MEAL_CONV.Text != "C")
                {
                    MessageBox.Show("Invalid Entry ... Please Enter  M  Or  C");
                    txtSP_MEAL_CONV.Focus();
                    txtSP_MEAL_CONV.Text = "";
                    return;
                }
                else
                {
                     
            
                colsNVals1.Clear();
                colsNVals1.Add("SP_BRANCH_A", txtSP_BRANCH_A.Text);
                colsNVals1.Add("SP_MEAL_CONV", txtSP_MEAL_CONV.Text);

                DtPROC_2 = GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CheckMealOrConveyance", colsNVals1);
                if (DtPROC_2 != null)
                {

                    if (DtPROC_2.Rows.Count > 0)
                    {
                        MessageBox.Show("Duplicate Entry Not Allowed");
                        this.Focus();
                        return;

                    }
                }
                    
                }


            }
        }

        private void txtSP_TAX_Validating(object sender, CancelEventArgs e)
        {
            txtSP_ASR_BASIC.Text = "";
            if (txtSP_TAX.Text != string.Empty)
            {
                if (txtSP_TAX.Text != "Y" && txtSP_TAX.Text != "N")
                {
                    MessageBox.Show("Please Enter ''Y''For Yes Or ''N''For No");
                    txtSP_TAX.Focus();
                    txtSP_TAX.Text = "";
                    return;
                }

                if (txtSP_TAX.Text == "N")
                {
                    txtSP_ACOUNT_NO.Focus();

                }
                if (txtSP_TAX.Text == "Y")
                {
                    txtSP_FORECAST_TAX.Focus();
                }
            }
        }

        private void txtSP_FORECAST_TAX_Validating(object sender, CancelEventArgs e)
        {
             txtSP_ASR_BASIC.Text = "";
             if (txtSP_FORECAST_TAX.Text != string.Empty)
             {
                 if (txtSP_FORECAST_TAX.Text != "Y" && txtSP_FORECAST_TAX.Text != "N")
                 {
                     MessageBox.Show("Please Enter ''Y''For Yes Or ''N''For No");
                     txtSP_FORECAST_TAX.Focus();
                     txtSP_FORECAST_TAX.Text = "";
                     return;
                 }

             }
        }

        private void txtSP_ASR_BASIC_Validating(object sender, CancelEventArgs e)
        {
            if (txtSP_TAX.Text == "Y")
            {
                if (txtSP_ASR_BASIC.Text != string.Empty)
                {
                    if (txtSP_ASR_BASIC.Text != "A" && txtSP_ASR_BASIC.Text != "B")
                    {
                        MessageBox.Show("Add In [A]SR Then Breakup Or Add In [B]asic Salary");
                        txtSP_ASR_BASIC.Focus();
                        txtSP_ASR_BASIC.Text = "";
                        return;
                    }

                }
            }
        }

        private void txtSP_ACOUNT_NO_Validated(object sender, EventArgs e)
        {
            if (!lbtnAccount.Focused)
            {
                //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand)this.PnlDetails.CurrentBusinessEntity;
                //if (ent != null)
                //{
                //    ent.SP_ALL_CODE = (this.txtSP_ALL_CODE.Text);
                //}
                if (this.FunctionConfig.CurrentOption == Function.Modify)
                {


                    GoToPage8();
                }

                if (this.FunctionConfig.CurrentOption == Function.Add && txtDescription.Text != string.Empty)
                {

                    DialogResult dRes = MessageBox.Show("Do you want to save the Record [Y/N]..", "Note"
                                                     , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {

                        base.DoToolbarActions(this.Controls, "Save");
                        //this.Reset();
                        base.ClearForm(pnlDetail.Controls);
                        dtpFromSP_VALID_FROM.Value = null;
                        dtpSP_VALID_TO.Value = null;
                        txtSP_ALL_CODE.Focus();
                        //call save
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        // this.Reset();
                        base.ClearForm(pnlDetail.Controls);
                        dtpFromSP_VALID_FROM.Value = null;
                        dtpSP_VALID_TO.Value = null;
                        txtOption.Focus();

                        return;
                    }

                }
            }
        }


        public void GoToPage8()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Modify The Details [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    ModifyDtlpp = "Yes";
                    if (txtSP_ALL_IND.Text == "A")
                    {
                        MessageBox.Show(" Sorry No Details Available", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NoDetails = true;
                        base.ClearForm(pnlDetail.Controls);
                        txtSP_ALL_CODE.Focus();
                    }

                    if (NoDetails == false)
                    {
                        frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);

                        frm_Detail.MdiParent = null;
                        frm_Detail.ShowDialog();
                    }

                }
                else if (dRslt == DialogResult.No)
                {
                    ModifyDtlpp = "No";
                    base.DoToolbarActions(this.Controls, "Save");

                    if (txtSP_ALL_IND.Text == "A")
                    {
                        if (txtSP_ALL_CODE.Text != string.Empty)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("SP_ALL_CODE", txtSP_ALL_CODE.Text);
                            CmnDataManager cmnDM = new CmnDataManager();
                            Result rsltCode;

                            rsltCode = cmnDM.Execute("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "DELETE_ALLOWANCES_DETAIL", param);
                        }
                    }
                }
             
                DeleteRecpp = string.Empty;
                
                SeeDtlpp = string.Empty;

                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage8", exp);
            }
        }


        private void txtSP_ALL_IND_Validated(object sender, EventArgs e)
        {
            if(txtSP_ALL_IND.Text  !=string.Empty)
            {
                if (txtSP_ALL_IND.Text == "I" && this.FunctionConfig.CurrentOption == Function.Add)
                {

                    frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);
                    
                    frm_Detail.MdiParent = null;
                    frm_Detail.ShowDialog();
                  
                }

                if (this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    txtAmount.Focus();

                }
            }
        }

        private void txtSP_ALL_CODE_Validating(object sender, CancelEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.Modify)
            {
                if (txtSP_ALL_CODE.Text != string.Empty)
                {
                    iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceSetupCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceSetupCommand)this.pnlDetail.CurrentBusinessEntity;
                    if (ent != null)
                    {
                        ent.SP_ALL_CODE = (this.txtSP_ALL_CODE.Text);
                    }
                }


                this.pnlDetail.LoadDependentPanels();
            }
        }

        private void lbtnCode_MouseDown(object sender, MouseEventArgs e)
        {

            LookupButton btn = (LookupButton)sender;
            ///Code by FAIsal Iqbal
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                btn.Enabled = false;
               btn.Enabled = true;

            }
        }




        private bool proc_mod_Del()
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_BRANCH_A", txtSP_BRANCH_A.Text);
                param.Add("SP_DESG_A", txtDesignation.Text);
                param.Add("SP_LEVEL_A", txtLevel.Text);
                param.Add("SP_CATEGORY_A", txtCategory.Text);



                rsltCode = cmnDM.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CATEGORY_VALIDATED", param);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtDescription.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString() != string.Empty)
                    {
                        dtpFromSP_VALID_FROM.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    }
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString() != string.Empty)
                    {
                        dtpSP_VALID_TO.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString());
                    }
                    txtSP_ALL_IND.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtAmount.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtSP_PER_ASR.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtSP_ATT_RELATED.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtSP_RELATED_LEAV.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtSP_MEAL_CONV.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[14].ToString();
                    txtSP_TAX.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtSP_FORECAST_TAX.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                    txtSP_ASR_BASIC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    txtSP_ACOUNT_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                        

                    return true;
                }
                else if (rsltCode.dstResult.Tables[0].Rows.Count < 0)
                {
                    if ((txtOption.Text != "A" || txtOption.Text == "M") && txtSP_ALL_CODE.Text == string.Empty)
                    {
                        base.Cancel();
                        return false;
                    }
                    else if (txtOption.Text != "A")
                    {
                        MessageBox.Show(" Record Not Found... Press [Delete] Key", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                return false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "proc_mod_Del", exp);
                return false;
            }
        }




        private bool proc_view_add()
        {
            try
            {
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();

                if (txtSP_ALL_CODE.Text != string.Empty)
                    param2.Add("SP_ALL_CODE", txtSP_ALL_CODE.Text);

                if (txtSP_BRANCH_A.Text != string.Empty)
                    param2.Add("SP_BRANCH_A", txtSP_BRANCH_A.Text);

                if (txtDesignation.Text != string.Empty)
                    param2.Add("SP_DESG_A", txtDesignation.Text);

                if (txtLevel.Text != string.Empty)
                    param2.Add("SP_LEVEL_A", txtLevel.Text);

                if (txtCategory.Text != string.Empty)
                    param2.Add("SP_CATEGORY_A", txtCategory.Text);

                rsltCode = cmnDM.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CATEGORY_VALIDATED", param2);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtDescription.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString() != string.Empty)
                    {
                        dtpFromSP_VALID_FROM.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    }
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString() != string.Empty)
                    {
                        dtpSP_VALID_TO.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString());
                    }
                    txtSP_ALL_IND.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtAmount.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtSP_PER_ASR.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtSP_ATT_RELATED.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtSP_RELATED_LEAV.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtSP_MEAL_CONV.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[14].ToString();
                    txtSP_TAX.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtSP_FORECAST_TAX.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                    txtSP_ASR_BASIC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    txtSP_ACOUNT_NO.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                        
                    return true;
                }
                else if (rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    if ((txtOption.Text != "A" || txtOption.Text == "V") && txtSP_ALL_CODE.Text == string.Empty)
                    {
                        base.Cancel();
                        return false;
                    }
                    else if (txtOption.Text == "V")
                    {
                        MessageBox.Show(" Record Not Found...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                return true;

            }
            catch (Exception exp)
            {
                LogException(this.Name, "proc_view_add", exp);
                return false;
            }
        }

        private void txtCategory_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txtCategory.Text != string.Empty)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    param.Add("SP_CATEGORY_A", txtCategory.Text);
                    param.Add("SP_DESG_A", txtDesignation.Text);
                    param.Add("SP_LEVEL_A", txtLevel.Text);

                    rsltCode = cmnDM.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CATEGORY_VALIDATING", param);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count < 0)
                    {
                        e.Cancel = true;
                        return;
                    }

                     Dictionary<string, object> param1 = new Dictionary<string, object>();
                    Result rsltCode1;
                    param1.Add("SP_CATEGORY_A", txtCategory.Text);
                    param1.Add("SP_DESG_A", txtDesignation.Text);
                    param1.Add("SP_LEVEL_A", txtLevel.Text);
                    param1.Add("SP_ALL_CODE", txtSP_ALL_CODE.Text);
                    param1.Add("SP_BRANCH_A", txtSP_BRANCH_A.Text);

                    rsltCode1 = cmnDM.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CATEGORY_FROM_FILL", param1);
                    if (rsltCode1.isSuccessful && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtDescription.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                        if (rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[6].ToString() != string.Empty)
                        {
                            dtpFromSP_VALID_FROM.Value = Convert.ToDateTime(rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                        }
                        if (rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[7].ToString() != string.Empty)
                        {
                            dtpSP_VALID_TO.Value = Convert.ToDateTime(rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[7].ToString());
                        }
                    txtSP_ALL_IND.Text      = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[15].ToString();
                    txtAmount.Text          = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txtSP_PER_ASR.Text      = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txtSP_ATT_RELATED.Text  = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txtSP_RELATED_LEAV.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[12].ToString();
                    txtSP_MEAL_CONV.Text    = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[14].ToString();
                    txtSP_TAX.Text          = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();
                    txtSP_FORECAST_TAX.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[17].ToString();
                    txtSP_ASR_BASIC.Text    = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[13].ToString();
                    txtSP_ACOUNT_NO.Text    = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[16].ToString();
                    txtW_ACC_DESC.Text      = rsltCode1.dstResult.Tables[0].Rows[0]["sp_acc_desc"].ToString();
                        
                    }


                     if (txtOption.Text == "M" || txtOption.Text == "D")
                    {
                        returnValue = proc_mod_Del();

                        
                    }

                    if (txtOption.Text == "V" || txtOption.Text == "A")
                        returnValue =  proc_view_add();


                    if (txtOption.Text == "A" && txtDescription.Text != string.Empty)
                    {
                        MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        base.Cancel();
                    }

                    if (txtOption.Text == "V" && returnValue != false)
                    {
                        //go_field('blk_head.w_ans5');
                        GoToPage7();

                        if (NoDetails == true)
                        {
                            e.Cancel = true;
                            return;
                        }


                    }


                    if (this.FunctionConfig.CurrentOption == Function.Delete)
                    {
                        //go_field('blk_head.w_ans7');

                        GoToPage9();
                        if (DeleteDtlpp == "Yes" && NoDetails == true)
                        {
                            e.Cancel = true;
                        }

                    }


                }

             else
                {
                    MessageBox.Show("Record Not Found", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_CATEGORY_A_Validating", exp);
            }


            //}
        }


        public void GoToPage3()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Delete The Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    //this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                    //base.DoToolbarActions(this.Controls, "Delete");
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
                    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));

                    base.ClearForm(pnlDetail.Controls);
                    txtOption.Focus();
                }
                else if (dRslt == DialogResult.No)
                {
                    base.ClearForm(pnlDetail.Controls);
                    txtOption.Focus();
                }
               
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage3", exp);
            }
        }




        public void GoToPage7()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To See The Details [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    

                    if (txtSP_ALL_IND.Text == "A")
                    {
                        MessageBox.Show(" Sorry No Details Available", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NoDetails = true;
                        GoToPage7();
                        return;
                    }

                    if (txtSP_ALL_IND.Text == "I")
                    {
                        frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);

                        frm_Detail.MdiParent = null;
                        frm_Detail.ShowDialog();


                    }

                    if (txtOption.Text == "D")
                    {
                        GoToPage3();
                    }
                }
                else if (dRslt == DialogResult.No)
                {
                    NoDetails = false;
                    SeeDtlpp = "No";
                    GoToPage2();

                }



              
                ModifyDtlpp=string.Empty;
                DeleteRecpp = string.Empty;
                DeleteDtlpp = string.Empty;
                           }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage7", exp);
            }
        }



        public void GoToPage2()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To View More Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {

                    base.ClearForm(pnlDetail.Controls);
                    txtSP_ALL_CODE.Select();
                    txtSP_ALL_CODE.Focus();

                }
                else if (dRslt == DialogResult.No)
                {
                    base.ClearForm(pnlDetail.Controls);
                    txtOption.Select();
                    txtOption.Focus();
                }


                DeleteRecpp = string.Empty;
                ModifyDtlpp = string.Empty;
                SeeDtlpp = string.Empty;

                
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage2", exp);
            }
        }


        public void GoToPage9()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Delete The Details [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    DeleteDtlpp = "Yes";

                    if (txtSP_ALL_IND.Text == "A")
                    {
                        MessageBox.Show(" Sorry No Details Available", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NoDetails = true;
                        return;
                    }

                    if (txtSP_ALL_IND.Text == "I")
                    {
                        frm_Detail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_AllowanceEnter_Details(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, DGVCategory.GridSource);

                        frm_Detail.MdiParent = null;
                        frm_Detail.ShowDialog();


                    }
                    
                }
                else if (dRslt == DialogResult.No)
                {
                    NoDetails = false;
                    DeleteDtlpp = "No";
                    GoToPage3();
                }
               
                DeleteRecpp = string.Empty;
                ModifyDtlpp = string.Empty;
                SeeDtlpp = string.Empty;
               
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage9", exp);
            }
        }


     
     
    
      
     

    
    }
}