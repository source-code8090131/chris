namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_DeductionEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_DeductionEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlDeductionMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_ID = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnAccountNo = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnCategory = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnLevel = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnDesignation = new CrplControlLibrary.LookupButton(this.components);
            this.txt_SP_VALID_TO_D = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_VALID_FROM_D = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtnBranch = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnDedection = new CrplControlLibrary.LookupButton(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txt_W_ACC_DESC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_BRANCH_D = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ACCOUNT_NO_D = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_PER = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ALL_IND = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CATEGORY_D = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_DESC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_LEVEL_D = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DESG_D = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OPTION_DIS = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvDedDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.grdDeducCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdPersonnelNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDedDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.llbusername = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDeductionMain.SuspendLayout();
            this.pnlHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).BeginInit();
            this.pnlDedDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(536, 0);
            this.txtOption.Leave += new System.EventHandler(this.txtOption_Leave);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(572, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 455);
            this.panel1.Size = new System.Drawing.Size(572, 60);
            // 
            // pnlDeductionMain
            // 
            this.pnlDeductionMain.ConcurrentPanels = null;
            this.pnlDeductionMain.Controls.Add(this.txt_ID);
            this.pnlDeductionMain.Controls.Add(this.lbtnAccountNo);
            this.pnlDeductionMain.Controls.Add(this.lbtnCategory);
            this.pnlDeductionMain.Controls.Add(this.lbtnLevel);
            this.pnlDeductionMain.Controls.Add(this.lbtnDesignation);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_VALID_TO_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_VALID_FROM_D);
            this.pnlDeductionMain.Controls.Add(this.lbtnBranch);
            this.pnlDeductionMain.Controls.Add(this.lbtnDedection);
            this.pnlDeductionMain.Controls.Add(this.label18);
            this.pnlDeductionMain.Controls.Add(this.txt_W_ACC_DESC);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_BRANCH_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_ACCOUNT_NO_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_PER);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_AMOUNT);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_ALL_IND);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_CATEGORY_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_DESC);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_LEVEL_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DESG_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_CODE);
            this.pnlDeductionMain.Controls.Add(this.label17);
            this.pnlDeductionMain.Controls.Add(this.label16);
            this.pnlDeductionMain.Controls.Add(this.label15);
            this.pnlDeductionMain.Controls.Add(this.label14);
            this.pnlDeductionMain.Controls.Add(this.label13);
            this.pnlDeductionMain.Controls.Add(this.label12);
            this.pnlDeductionMain.Controls.Add(this.label11);
            this.pnlDeductionMain.Controls.Add(this.label10);
            this.pnlDeductionMain.Controls.Add(this.label9);
            this.pnlDeductionMain.Controls.Add(this.label8);
            this.pnlDeductionMain.Controls.Add(this.label7);
            this.pnlDeductionMain.Controls.Add(this.label2);
            this.pnlDeductionMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDeductionMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDeductionMain.DependentPanels = null;
            this.pnlDeductionMain.DisableDependentLoad = false;
            this.pnlDeductionMain.EnableDelete = true;
            this.pnlDeductionMain.EnableInsert = true;
            this.pnlDeductionMain.EnableQuery = false;
            this.pnlDeductionMain.EnableUpdate = true;
            this.pnlDeductionMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeducEntCommand";
            this.pnlDeductionMain.Location = new System.Drawing.Point(8, 163);
            this.pnlDeductionMain.MasterPanel = null;
            this.pnlDeductionMain.Name = "pnlDeductionMain";
            this.pnlDeductionMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDeductionMain.Size = new System.Drawing.Size(560, 272);
            this.pnlDeductionMain.SPName = "CHRIS_SP_DeducEnt_DEDUCTION_MANAGER";
            this.pnlDeductionMain.TabIndex = 0;
            // 
            // txt_ID
            // 
            this.txt_ID.AllowSpace = true;
            this.txt_ID.AssociatedLookUpName = "";
            this.txt_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ID.ContinuationTextBox = null;
            this.txt_ID.CustomEnabled = true;
            this.txt_ID.DataFieldMapping = "ID";
            this.txt_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ID.GetRecordsOnUpDownKeys = false;
            this.txt_ID.IsDate = false;
            this.txt_ID.Location = new System.Drawing.Point(463, 19);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.NumberFormat = "###,###,##0.00";
            this.txt_ID.Postfix = "";
            this.txt_ID.Prefix = "";
            this.txt_ID.Size = new System.Drawing.Size(59, 20);
            this.txt_ID.SkipValidation = false;
            this.txt_ID.TabIndex = 33;
            this.txt_ID.TextType = CrplControlLibrary.TextType.String;
            this.txt_ID.Visible = false;
            // 
            // lbtnAccountNo
            // 
            this.lbtnAccountNo.ActionLOVExists = "ACCOUNT_LOV_EXIST";
            this.lbtnAccountNo.ActionType = "ACCOUNT_LOV";
            this.lbtnAccountNo.ConditionalFields = "";
            this.lbtnAccountNo.CustomEnabled = true;
            this.lbtnAccountNo.DataFieldMapping = "";
            this.lbtnAccountNo.DependentLovControls = "";
            this.lbtnAccountNo.HiddenColumns = "";
            this.lbtnAccountNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnAccountNo.Image")));
            this.lbtnAccountNo.LoadDependentEntities = false;
            this.lbtnAccountNo.Location = new System.Drawing.Point(273, 248);
            this.lbtnAccountNo.LookUpTitle = null;
            this.lbtnAccountNo.Name = "lbtnAccountNo";
            this.lbtnAccountNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnAccountNo.SkipValidationOnLeave = false;
            this.lbtnAccountNo.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnAccountNo.TabIndex = 32;
            this.lbtnAccountNo.TabStop = false;
            this.lbtnAccountNo.UseVisualStyleBackColor = true;
            // 
            // lbtnCategory
            // 
            this.lbtnCategory.ActionLOVExists = "CATGORY_LOV_EXISTS";
            this.lbtnCategory.ActionType = "CATGORY_LOV";
            this.lbtnCategory.ConditionalFields = "";
            this.lbtnCategory.CustomEnabled = true;
            this.lbtnCategory.DataFieldMapping = "";
            this.lbtnCategory.DependentLovControls = "";
            this.lbtnCategory.HiddenColumns = "";
            this.lbtnCategory.Image = ((System.Drawing.Image)(resources.GetObject("lbtnCategory.Image")));
            this.lbtnCategory.LoadDependentEntities = false;
            this.lbtnCategory.Location = new System.Drawing.Point(242, 96);
            this.lbtnCategory.LookUpTitle = null;
            this.lbtnCategory.Name = "lbtnCategory";
            this.lbtnCategory.Size = new System.Drawing.Size(26, 21);
            this.lbtnCategory.SkipValidationOnLeave = false;
            this.lbtnCategory.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnCategory.TabIndex = 31;
            this.lbtnCategory.TabStop = false;
            this.lbtnCategory.UseVisualStyleBackColor = true;
            // 
            // lbtnLevel
            // 
            this.lbtnLevel.ActionLOVExists = "LEVEL_LOV_EXISTS";
            this.lbtnLevel.ActionType = "LEVEL_LOV";
            this.lbtnLevel.ConditionalFields = "txt_SP_DESG_D";
            this.lbtnLevel.CustomEnabled = true;
            this.lbtnLevel.DataFieldMapping = "";
            this.lbtnLevel.DependentLovControls = "";
            this.lbtnLevel.HiddenColumns = "";
            this.lbtnLevel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnLevel.Image")));
            this.lbtnLevel.LoadDependentEntities = false;
            this.lbtnLevel.Location = new System.Drawing.Point(242, 73);
            this.lbtnLevel.LookUpTitle = null;
            this.lbtnLevel.Name = "lbtnLevel";
            this.lbtnLevel.Size = new System.Drawing.Size(26, 21);
            this.lbtnLevel.SkipValidationOnLeave = false;
            this.lbtnLevel.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnLevel.TabIndex = 30;
            this.lbtnLevel.TabStop = false;
            this.lbtnLevel.UseVisualStyleBackColor = true;
            // 
            // lbtnDesignation
            // 
            this.lbtnDesignation.ActionLOVExists = "DESG_LOV_EXISTS";
            this.lbtnDesignation.ActionType = "DESG_LOV";
            this.lbtnDesignation.ConditionalFields = "";
            this.lbtnDesignation.CustomEnabled = true;
            this.lbtnDesignation.DataFieldMapping = "";
            this.lbtnDesignation.DependentLovControls = "";
            this.lbtnDesignation.HiddenColumns = "";
            this.lbtnDesignation.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDesignation.Image")));
            this.lbtnDesignation.LoadDependentEntities = false;
            this.lbtnDesignation.Location = new System.Drawing.Point(242, 46);
            this.lbtnDesignation.LookUpTitle = null;
            this.lbtnDesignation.Name = "lbtnDesignation";
            this.lbtnDesignation.Size = new System.Drawing.Size(26, 21);
            this.lbtnDesignation.SkipValidationOnLeave = false;
            this.lbtnDesignation.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnDesignation.TabIndex = 29;
            this.lbtnDesignation.TabStop = false;
            this.lbtnDesignation.UseVisualStyleBackColor = true;
            // 
            // txt_SP_VALID_TO_D
            // 
            this.txt_SP_VALID_TO_D.CustomEnabled = true;
            this.txt_SP_VALID_TO_D.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_VALID_TO_D.DataFieldMapping = "SP_VALID_TO_D";
            this.txt_SP_VALID_TO_D.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_VALID_TO_D.HasChanges = true;
            this.txt_SP_VALID_TO_D.Location = new System.Drawing.Point(311, 148);
            this.txt_SP_VALID_TO_D.Name = "txt_SP_VALID_TO_D";
            this.txt_SP_VALID_TO_D.NullValue = " ";
            this.txt_SP_VALID_TO_D.Size = new System.Drawing.Size(85, 20);
            this.txt_SP_VALID_TO_D.TabIndex = 7;
            this.txt_SP_VALID_TO_D.Value = new System.DateTime(2011, 2, 2, 0, 0, 0, 0);
            this.txt_SP_VALID_TO_D.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_VALID_TO_D_Validating);
            // 
            // txt_SP_VALID_FROM_D
            // 
            this.txt_SP_VALID_FROM_D.CustomEnabled = true;
            this.txt_SP_VALID_FROM_D.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_VALID_FROM_D.DataFieldMapping = "SP_VALID_FROM_D";
            this.txt_SP_VALID_FROM_D.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_VALID_FROM_D.HasChanges = true;
            this.txt_SP_VALID_FROM_D.Location = new System.Drawing.Point(182, 148);
            this.txt_SP_VALID_FROM_D.Name = "txt_SP_VALID_FROM_D";
            this.txt_SP_VALID_FROM_D.NullValue = " ";
            this.txt_SP_VALID_FROM_D.Size = new System.Drawing.Size(85, 20);
            this.txt_SP_VALID_FROM_D.TabIndex = 6;
            this.txt_SP_VALID_FROM_D.Value = new System.DateTime(2011, 2, 2, 0, 0, 0, 0);
            this.txt_SP_VALID_FROM_D.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_VALID_FROM_D_Validating);
            // 
            // lbtnBranch
            // 
            this.lbtnBranch.ActionLOVExists = "BRANCH_LOV_EXISTS";
            this.lbtnBranch.ActionType = "BRANCH_LOV";
            this.lbtnBranch.ConditionalFields = "";
            this.lbtnBranch.CustomEnabled = true;
            this.lbtnBranch.DataFieldMapping = "";
            this.lbtnBranch.DependentLovControls = "";
            this.lbtnBranch.HiddenColumns = "";
            this.lbtnBranch.Image = ((System.Drawing.Image)(resources.GetObject("lbtnBranch.Image")));
            this.lbtnBranch.LoadDependentEntities = false;
            this.lbtnBranch.Location = new System.Drawing.Point(411, 23);
            this.lbtnBranch.LookUpTitle = null;
            this.lbtnBranch.Name = "lbtnBranch";
            this.lbtnBranch.Size = new System.Drawing.Size(26, 21);
            this.lbtnBranch.SkipValidationOnLeave = false;
            this.lbtnBranch.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnBranch.TabIndex = 26;
            this.lbtnBranch.TabStop = false;
            this.lbtnBranch.UseVisualStyleBackColor = true;
            // 
            // lbtnDedection
            // 
            this.lbtnDedection.ActionLOVExists = "DUC_LOV_EXISTS";
            this.lbtnDedection.ActionType = "DUC_LOV";
            this.lbtnDedection.ConditionalFields = "";
            this.lbtnDedection.CustomEnabled = true;
            this.lbtnDedection.DataFieldMapping = "";
            this.lbtnDedection.DependentLovControls = "";
            this.lbtnDedection.HiddenColumns = "";
            this.lbtnDedection.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDedection.Image")));
            this.lbtnDedection.LoadDependentEntities = true;
            this.lbtnDedection.Location = new System.Drawing.Point(242, 19);
            this.lbtnDedection.LookUpTitle = null;
            this.lbtnDedection.Name = "lbtnDedection";
            this.lbtnDedection.Size = new System.Drawing.Size(26, 21);
            this.lbtnDedection.SkipValidationOnLeave = false;
            this.lbtnDedection.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnDedection.TabIndex = 25;
            this.lbtnDedection.TabStop = false;
            this.lbtnDedection.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(283, 152);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "/";
            // 
            // txt_W_ACC_DESC
            // 
            this.txt_W_ACC_DESC.AllowSpace = true;
            this.txt_W_ACC_DESC.AssociatedLookUpName = "";
            this.txt_W_ACC_DESC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_ACC_DESC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_ACC_DESC.ContinuationTextBox = null;
            this.txt_W_ACC_DESC.CustomEnabled = true;
            this.txt_W_ACC_DESC.DataFieldMapping = "W_ACC_DESC";
            this.txt_W_ACC_DESC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_ACC_DESC.GetRecordsOnUpDownKeys = false;
            this.txt_W_ACC_DESC.IsDate = false;
            this.txt_W_ACC_DESC.Location = new System.Drawing.Point(302, 248);
            this.txt_W_ACC_DESC.Name = "txt_W_ACC_DESC";
            this.txt_W_ACC_DESC.NumberFormat = "###,###,##0.00";
            this.txt_W_ACC_DESC.Postfix = "";
            this.txt_W_ACC_DESC.Prefix = "";
            this.txt_W_ACC_DESC.Size = new System.Drawing.Size(190, 20);
            this.txt_W_ACC_DESC.SkipValidation = false;
            this.txt_W_ACC_DESC.TabIndex = 12;
            this.txt_W_ACC_DESC.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_BRANCH_D
            // 
            this.txt_SP_BRANCH_D.AllowSpace = true;
            this.txt_SP_BRANCH_D.AssociatedLookUpName = "lbtnBranch";
            this.txt_SP_BRANCH_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_BRANCH_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_BRANCH_D.ContinuationTextBox = null;
            this.txt_SP_BRANCH_D.CustomEnabled = true;
            this.txt_SP_BRANCH_D.DataFieldMapping = "SP_BRANCH_D";
            this.txt_SP_BRANCH_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_BRANCH_D.GetRecordsOnUpDownKeys = false;
            this.txt_SP_BRANCH_D.IsDate = false;
            this.txt_SP_BRANCH_D.Location = new System.Drawing.Point(337, 23);
            this.txt_SP_BRANCH_D.Name = "txt_SP_BRANCH_D";
            this.txt_SP_BRANCH_D.NumberFormat = "###,###,##0.00";
            this.txt_SP_BRANCH_D.Postfix = "";
            this.txt_SP_BRANCH_D.Prefix = "";
            this.txt_SP_BRANCH_D.Size = new System.Drawing.Size(59, 20);
            this.txt_SP_BRANCH_D.SkipValidation = false;
            this.txt_SP_BRANCH_D.TabIndex = 1;
            this.txt_SP_BRANCH_D.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_ACCOUNT_NO_D
            // 
            this.txt_SP_ACCOUNT_NO_D.AllowSpace = true;
            this.txt_SP_ACCOUNT_NO_D.AssociatedLookUpName = "lbtnAccountNo";
            this.txt_SP_ACCOUNT_NO_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ACCOUNT_NO_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ACCOUNT_NO_D.ContinuationTextBox = null;
            this.txt_SP_ACCOUNT_NO_D.CustomEnabled = true;
            this.txt_SP_ACCOUNT_NO_D.DataFieldMapping = "SP_ACOUNT_NO_D";
            this.txt_SP_ACCOUNT_NO_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ACCOUNT_NO_D.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ACCOUNT_NO_D.IsDate = false;
            this.txt_SP_ACCOUNT_NO_D.Location = new System.Drawing.Point(182, 248);
            this.txt_SP_ACCOUNT_NO_D.Name = "txt_SP_ACCOUNT_NO_D";
            this.txt_SP_ACCOUNT_NO_D.NumberFormat = "###,###,##0.00";
            this.txt_SP_ACCOUNT_NO_D.Postfix = "";
            this.txt_SP_ACCOUNT_NO_D.Prefix = "";
            this.txt_SP_ACCOUNT_NO_D.Size = new System.Drawing.Size(85, 20);
            this.txt_SP_ACCOUNT_NO_D.SkipValidation = false;
            this.txt_SP_ACCOUNT_NO_D.TabIndex = 11;
            this.txt_SP_ACCOUNT_NO_D.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_ACCOUNT_NO_D.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_ACCOUNT_NO_D_Validating);
            // 
            // txt_SP_DED_PER
            // 
            this.txt_SP_DED_PER.AllowSpace = true;
            this.txt_SP_DED_PER.AssociatedLookUpName = "";
            this.txt_SP_DED_PER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_PER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_PER.ContinuationTextBox = null;
            this.txt_SP_DED_PER.CustomEnabled = true;
            this.txt_SP_DED_PER.DataFieldMapping = "SP_DED_PER";
            this.txt_SP_DED_PER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_PER.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_PER.IsDate = false;
            this.txt_SP_DED_PER.Location = new System.Drawing.Point(182, 223);
            this.txt_SP_DED_PER.MaxLength = 2;
            this.txt_SP_DED_PER.Name = "txt_SP_DED_PER";
            this.txt_SP_DED_PER.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_PER.Postfix = "";
            this.txt_SP_DED_PER.Prefix = "";
            this.txt_SP_DED_PER.Size = new System.Drawing.Size(48, 20);
            this.txt_SP_DED_PER.SkipValidation = false;
            this.txt_SP_DED_PER.TabIndex = 10;
            this.txt_SP_DED_PER.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_SP_DED_PER.TextType = CrplControlLibrary.TextType.Double;
            this.txt_SP_DED_PER.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_DED_PER_Validating);
            // 
            // txt_SP_DED_AMOUNT
            // 
            this.txt_SP_DED_AMOUNT.AllowSpace = true;
            this.txt_SP_DED_AMOUNT.AssociatedLookUpName = "";
            this.txt_SP_DED_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_AMOUNT.ContinuationTextBox = null;
            this.txt_SP_DED_AMOUNT.CustomEnabled = true;
            this.txt_SP_DED_AMOUNT.DataFieldMapping = "SP_DED_AMOUNT";
            this.txt_SP_DED_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_AMOUNT.IsDate = false;
            this.txt_SP_DED_AMOUNT.Location = new System.Drawing.Point(182, 198);
            this.txt_SP_DED_AMOUNT.MaxLength = 6;
            this.txt_SP_DED_AMOUNT.Name = "txt_SP_DED_AMOUNT";
            this.txt_SP_DED_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_AMOUNT.Postfix = "";
            this.txt_SP_DED_AMOUNT.Prefix = "";
            this.txt_SP_DED_AMOUNT.Size = new System.Drawing.Size(75, 20);
            this.txt_SP_DED_AMOUNT.SkipValidation = false;
            this.txt_SP_DED_AMOUNT.TabIndex = 9;
            this.txt_SP_DED_AMOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_SP_DED_AMOUNT.TextType = CrplControlLibrary.TextType.Double;
            this.txt_SP_DED_AMOUNT.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_DED_AMOUNT_Validating);
            // 
            // txt_SP_ALL_IND
            // 
            this.txt_SP_ALL_IND.AllowSpace = true;
            this.txt_SP_ALL_IND.AssociatedLookUpName = "";
            this.txt_SP_ALL_IND.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ALL_IND.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ALL_IND.ContinuationTextBox = null;
            this.txt_SP_ALL_IND.CustomEnabled = true;
            this.txt_SP_ALL_IND.DataFieldMapping = "SP_ALL_IND";
            this.txt_SP_ALL_IND.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ALL_IND.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ALL_IND.IsDate = false;
            this.txt_SP_ALL_IND.Location = new System.Drawing.Point(182, 173);
            this.txt_SP_ALL_IND.MaxLength = 1;
            this.txt_SP_ALL_IND.Name = "txt_SP_ALL_IND";
            this.txt_SP_ALL_IND.NumberFormat = "###,###,##0.00";
            this.txt_SP_ALL_IND.Postfix = "";
            this.txt_SP_ALL_IND.Prefix = "";
            this.txt_SP_ALL_IND.Size = new System.Drawing.Size(48, 20);
            this.txt_SP_ALL_IND.SkipValidation = false;
            this.txt_SP_ALL_IND.TabIndex = 8;
            this.txt_SP_ALL_IND.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_ALL_IND.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_ALL_IND_Validating);
            this.txt_SP_ALL_IND.Validated += new System.EventHandler(this.txt_SP_ALL_IND_Validated);
            // 
            // txt_SP_CATEGORY_D
            // 
            this.txt_SP_CATEGORY_D.AllowSpace = true;
            this.txt_SP_CATEGORY_D.AssociatedLookUpName = "lbtnCategory";
            this.txt_SP_CATEGORY_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CATEGORY_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CATEGORY_D.ContinuationTextBox = null;
            this.txt_SP_CATEGORY_D.CustomEnabled = true;
            this.txt_SP_CATEGORY_D.DataFieldMapping = "SP_CATEGORY_D";
            this.txt_SP_CATEGORY_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CATEGORY_D.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CATEGORY_D.IsDate = false;
            this.txt_SP_CATEGORY_D.Location = new System.Drawing.Point(182, 98);
            this.txt_SP_CATEGORY_D.Name = "txt_SP_CATEGORY_D";
            this.txt_SP_CATEGORY_D.NumberFormat = "###,###,##0.00";
            this.txt_SP_CATEGORY_D.Postfix = "";
            this.txt_SP_CATEGORY_D.Prefix = "";
            this.txt_SP_CATEGORY_D.Size = new System.Drawing.Size(48, 20);
            this.txt_SP_CATEGORY_D.SkipValidation = false;
            this.txt_SP_CATEGORY_D.TabIndex = 4;
            this.txt_SP_CATEGORY_D.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_CATEGORY_D.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_CATEGORY_D_Validating);
            // 
            // txt_SP_DED_DESC
            // 
            this.txt_SP_DED_DESC.AllowSpace = true;
            this.txt_SP_DED_DESC.AssociatedLookUpName = "";
            this.txt_SP_DED_DESC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_DESC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_DESC.ContinuationTextBox = null;
            this.txt_SP_DED_DESC.CustomEnabled = true;
            this.txt_SP_DED_DESC.DataFieldMapping = "SP_DED_DESC";
            this.txt_SP_DED_DESC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_DESC.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_DESC.IsDate = false;
            this.txt_SP_DED_DESC.Location = new System.Drawing.Point(182, 123);
            this.txt_SP_DED_DESC.Name = "txt_SP_DED_DESC";
            this.txt_SP_DED_DESC.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_DESC.Postfix = "";
            this.txt_SP_DED_DESC.Prefix = "";
            this.txt_SP_DED_DESC.Size = new System.Drawing.Size(310, 20);
            this.txt_SP_DED_DESC.SkipValidation = false;
            this.txt_SP_DED_DESC.TabIndex = 5;
            this.txt_SP_DED_DESC.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_DED_DESC.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_DED_DESC_Validating);
            // 
            // txt_SP_LEVEL_D
            // 
            this.txt_SP_LEVEL_D.AllowSpace = true;
            this.txt_SP_LEVEL_D.AssociatedLookUpName = "lbtnLevel";
            this.txt_SP_LEVEL_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_LEVEL_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_LEVEL_D.ContinuationTextBox = null;
            this.txt_SP_LEVEL_D.CustomEnabled = true;
            this.txt_SP_LEVEL_D.DataFieldMapping = "SP_LEVEL_D";
            this.txt_SP_LEVEL_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_LEVEL_D.GetRecordsOnUpDownKeys = false;
            this.txt_SP_LEVEL_D.IsDate = false;
            this.txt_SP_LEVEL_D.Location = new System.Drawing.Point(182, 73);
            this.txt_SP_LEVEL_D.Name = "txt_SP_LEVEL_D";
            this.txt_SP_LEVEL_D.NumberFormat = "###,###,##0.00";
            this.txt_SP_LEVEL_D.Postfix = "";
            this.txt_SP_LEVEL_D.Prefix = "";
            this.txt_SP_LEVEL_D.Size = new System.Drawing.Size(48, 20);
            this.txt_SP_LEVEL_D.SkipValidation = false;
            this.txt_SP_LEVEL_D.TabIndex = 3;
            this.txt_SP_LEVEL_D.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_DESG_D
            // 
            this.txt_SP_DESG_D.AllowSpace = true;
            this.txt_SP_DESG_D.AssociatedLookUpName = "lbtnDesignation";
            this.txt_SP_DESG_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DESG_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DESG_D.ContinuationTextBox = null;
            this.txt_SP_DESG_D.CustomEnabled = true;
            this.txt_SP_DESG_D.DataFieldMapping = "SP_DESG_D";
            this.txt_SP_DESG_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DESG_D.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DESG_D.IsDate = false;
            this.txt_SP_DESG_D.Location = new System.Drawing.Point(182, 48);
            this.txt_SP_DESG_D.Name = "txt_SP_DESG_D";
            this.txt_SP_DESG_D.NumberFormat = "###,###,##0.00";
            this.txt_SP_DESG_D.Postfix = "";
            this.txt_SP_DESG_D.Prefix = "";
            this.txt_SP_DESG_D.Size = new System.Drawing.Size(48, 20);
            this.txt_SP_DESG_D.SkipValidation = false;
            this.txt_SP_DESG_D.TabIndex = 2;
            this.txt_SP_DESG_D.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_DESG_D.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_DESG_D_Validating);
            // 
            // txt_SP_DED_CODE
            // 
            this.txt_SP_DED_CODE.AllowSpace = true;
            this.txt_SP_DED_CODE.AssociatedLookUpName = "lbtnDedection";
            this.txt_SP_DED_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_CODE.ContinuationTextBox = null;
            this.txt_SP_DED_CODE.CustomEnabled = true;
            this.txt_SP_DED_CODE.DataFieldMapping = "SP_DED_CODE";
            this.txt_SP_DED_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_CODE.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_CODE.IsDate = false;
            this.txt_SP_DED_CODE.Location = new System.Drawing.Point(182, 23);
            this.txt_SP_DED_CODE.Name = "txt_SP_DED_CODE";
            this.txt_SP_DED_CODE.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_CODE.Postfix = "";
            this.txt_SP_DED_CODE.Prefix = "";
            this.txt_SP_DED_CODE.ShortcutsEnabled = false;
            this.txt_SP_DED_CODE.Size = new System.Drawing.Size(48, 20);
            this.txt_SP_DED_CODE.SkipValidation = false;
            this.txt_SP_DED_CODE.TabIndex = 0;
            this.txt_SP_DED_CODE.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_DED_CODE.Enter += new System.EventHandler(this.txt_SP_DED_CODE_Enter);
            this.txt_SP_DED_CODE.Leave += new System.EventHandler(this.txt_SP_DED_CODE_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(308, 225);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(122, 13);
            this.label17.TabIndex = 11;
            this.label17.Text = "Account Description";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(274, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Branch";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 252);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(157, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "10) Account No. To Effect";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(20, 227);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(155, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "9) % with Annual Package";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 202);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "9) Amount";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 177);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "7) Individual / All";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 152);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "6) Valid From / To";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "5) Description";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "4) Category";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "3) Level";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "2) Designation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "1) Deduc. Abbreviation";
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txt_W_OPTION_DIS);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Location = new System.Drawing.Point(8, 83);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(560, 80);
            this.pnlHead.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(88, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(341, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Deductions Excluding P.Fund/Gratuity/Insurance Premium ";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = false;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(463, 30);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(89, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OPTION_DIS
            // 
            this.txt_W_OPTION_DIS.AllowSpace = true;
            this.txt_W_OPTION_DIS.AssociatedLookUpName = "";
            this.txt_W_OPTION_DIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OPTION_DIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OPTION_DIS.ContinuationTextBox = null;
            this.txt_W_OPTION_DIS.CustomEnabled = false;
            this.txt_W_OPTION_DIS.DataFieldMapping = "";
            this.txt_W_OPTION_DIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OPTION_DIS.GetRecordsOnUpDownKeys = false;
            this.txt_W_OPTION_DIS.IsDate = false;
            this.txt_W_OPTION_DIS.Location = new System.Drawing.Point(463, 8);
            this.txt_W_OPTION_DIS.Name = "txt_W_OPTION_DIS";
            this.txt_W_OPTION_DIS.NumberFormat = "###,###,##0.00";
            this.txt_W_OPTION_DIS.Postfix = "";
            this.txt_W_OPTION_DIS.Prefix = "";
            this.txt_W_OPTION_DIS.Size = new System.Drawing.Size(57, 20);
            this.txt_W_OPTION_DIS.SkipValidation = false;
            this.txt_W_OPTION_DIS.TabIndex = 8;
            this.txt_W_OPTION_DIS.TabStop = false;
            this.txt_W_OPTION_DIS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(421, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(413, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Option :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(177, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "D E D U C T I O N    E N T R Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "S e t  U p";
            // 
            // dgvDedDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDedDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDedDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdDeducCode,
            this.ID,
            this.grdPersonnelNo,
            this.grdName,
            this.grdRemarks});
            this.dgvDedDetail.ColumnToHide = null;
            this.dgvDedDetail.ColumnWidth = null;
            this.dgvDedDetail.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDedDetail.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDedDetail.DisplayColumnWrapper = null;
            this.dgvDedDetail.GridDefaultRow = 0;
            this.dgvDedDetail.Location = new System.Drawing.Point(3, 3);
            this.dgvDedDetail.Name = "dgvDedDetail";
            this.dgvDedDetail.ReadOnlyColumns = null;
            this.dgvDedDetail.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDedDetail.Size = new System.Drawing.Size(253, 217);
            this.dgvDedDetail.SkippingColumns = null;
            this.dgvDedDetail.TabIndex = 0;
            this.dgvDedDetail.TabStop = false;
            // 
            // grdDeducCode
            // 
            this.grdDeducCode.DataPropertyName = "SP_DED_CODE";
            this.grdDeducCode.HeaderText = "";
            this.grdDeducCode.Name = "grdDeducCode";
            this.grdDeducCode.Width = 20;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Width = 30;
            // 
            // grdPersonnelNo
            // 
            this.grdPersonnelNo.DataPropertyName = "SP_P_NO";
            this.grdPersonnelNo.HeaderText = "Personnel No.";
            this.grdPersonnelNo.Name = "grdPersonnelNo";
            this.grdPersonnelNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // grdName
            // 
            this.grdName.DataPropertyName = "Name";
            this.grdName.HeaderText = "Name";
            this.grdName.Name = "grdName";
            // 
            // grdRemarks
            // 
            this.grdRemarks.DataPropertyName = "SP_REMARKS";
            this.grdRemarks.HeaderText = "Remarks";
            this.grdRemarks.Name = "grdRemarks";
            // 
            // pnlDedDetail
            // 
            this.pnlDedDetail.ConcurrentPanels = null;
            this.pnlDedDetail.Controls.Add(this.dgvDedDetail);
            this.pnlDedDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDedDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDedDetail.DependentPanels = null;
            this.pnlDedDetail.DisableDependentLoad = false;
            this.pnlDedDetail.EnableDelete = true;
            this.pnlDedDetail.EnableInsert = true;
            this.pnlDedDetail.EnableQuery = false;
            this.pnlDedDetail.EnableUpdate = true;
            this.pnlDedDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DedDetailEntCommand";
            this.pnlDedDetail.Location = new System.Drawing.Point(601, 169);
            this.pnlDedDetail.MasterPanel = null;
            this.pnlDedDetail.Name = "pnlDedDetail";
            this.pnlDedDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDedDetail.Size = new System.Drawing.Size(256, 232);
            this.pnlDedDetail.SPName = "CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER";
            this.pnlDedDetail.TabIndex = 16;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(342, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "User Name :";
            // 
            // llbusername
            // 
            this.llbusername.AutoSize = true;
            this.llbusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbusername.Location = new System.Drawing.Point(407, 9);
            this.llbusername.Name = "llbusername";
            this.llbusername.Size = new System.Drawing.Size(66, 13);
            this.llbusername.TabIndex = 17;
            this.llbusername.Text = "User Name :";
            // 
            // CHRIS_Setup_DeductionEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(572, 515);
            this.Controls.Add(this.llbusername);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.pnlDedDetail);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlDeductionMain);
            this.Name = "CHRIS_Setup_DeductionEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Setup_DeductionEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDeductionMain, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlDedDetail, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.llbusername, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDeductionMain.ResumeLayout(false);
            this.pnlDeductionMain.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).EndInit();
            this.pnlDedDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDeductionMain;
        private System.Windows.Forms.Panel pnlHead;
        public CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txt_W_OPTION_DIS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.LookupButton lbtnDedection;
        private CrplControlLibrary.LookupButton lbtnBranch;
        private CrplControlLibrary.LookupButton lbtnCategory;
        private CrplControlLibrary.LookupButton lbtnLevel;
        private CrplControlLibrary.LookupButton lbtnDesignation;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDedDetail;
        public CrplControlLibrary.SLTextBox txt_SP_DED_CODE;
        private CrplControlLibrary.LookupButton lbtnAccountNo;
        public CrplControlLibrary.SLTextBox txt_W_ACC_DESC;
        public CrplControlLibrary.SLTextBox txt_SP_BRANCH_D;
        public CrplControlLibrary.SLTextBox txt_SP_ACCOUNT_NO_D;
        public CrplControlLibrary.SLTextBox txt_SP_DED_PER;
        public CrplControlLibrary.SLTextBox txt_SP_DED_AMOUNT;
        public CrplControlLibrary.SLTextBox txt_SP_ALL_IND;
        public CrplControlLibrary.SLTextBox txt_SP_CATEGORY_D;
        public CrplControlLibrary.SLTextBox txt_SP_DED_DESC;
        public CrplControlLibrary.SLTextBox txt_SP_LEVEL_D;
        public CrplControlLibrary.SLTextBox txt_SP_DESG_D;
        public CrplControlLibrary.SLDatePicker txt_SP_VALID_TO_D;
        public CrplControlLibrary.SLDatePicker txt_SP_VALID_FROM_D;
        public CrplControlLibrary.SLTextBox txt_ID;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlDedDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdDeducCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdPersonnelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdName;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdRemarks;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label llbusername;
    }
}