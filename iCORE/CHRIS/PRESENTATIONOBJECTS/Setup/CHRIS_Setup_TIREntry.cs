using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_TIREntry : ChrisTabularForm 
    {
        #region --Variable--
        decimal MinVal = 0;
        decimal MaxVal = 0;
        decimal AvgVal = 0;
        bool SkipLevel = false;
        bool SkipValidation = false;
        double temp = 0;
        #endregion

        #region --Constructor--
        public CHRIS_Setup_TIREntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_TIREntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            
            //List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(this.pnlTblBlkTwo);

            //this.pnlHead.DependentPanels = lstDependentPanels;
            //this.IndependentPanels.Add(pnlHead);
        }
        #endregion

        #region --EVENTS--
        
        /// <summary>
        /// Set the User Name
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
           
            try
            {
                base.OnLoad(e);
                this.lblUserName.Text = "  " + this.UserName;
                this.txt_W_YEAR.Select();
                this.txt_W_YEAR.Focus();
                Font newFontStyle = new Font(dgvTir.Font, FontStyle.Bold);
                dgvTir.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
                

            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.txtOption.Visible = false;
                //this.Level.SkipValidationOnLeave = true;
                txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                this.CurrentPanelBlock = this.pnlTblBlkTwo.Name;

                //for (int i = 0; i < this.tlbMain.Items.Count; i++)
                //{
                //    if (this.tlbMain.Items[i].Text == "&Search")
                //    {
                //        this.tlbMain.Items[i].Available = false;
                //    }
                //}
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }

            //tbtSave.Enabled = true;
            //tbtList.Enabled = true;
            //tbtDelete.Enabled = true;
            //tbtEdit.Enabled = true; 
        }

       
        protected override bool Quit()
        {
            if (dgvTir.Rows.Count > 1)
            {
                this.Cancel();
            }
            else
            {
                base.Quit();
            }
            return false;
        }




        /// <summary>
        /// On Row Leave Fill the Grid if not then add it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_W_YEAR_Leave(object sender, EventArgs e)
        {
            try
            {
                bool ChkValue;
                decimal Year;
                ChkValue = decimal.TryParse(txt_W_YEAR.Text, out Year);
                if (ChkValue)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("SP_YEAR", txt_W_YEAR.Text);
                    txt_W_YEAR.Text.GetType();


                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "W_YEAR", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables.Count > 0)
                    {
                        txt_W_TIR_PER.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                        dgvTir.GridSource = rsltCode.dstResult.Tables[0];
                        dgvTir.DataSource = dgvTir.GridSource;
                        //dgvTir.ReadOnly = true;
                        //txt_W_YEAR.ReadOnly = true;
                        //txt_W_TIR_PER.ReadOnly = true;
                        MessageBox.Show("Please Acknowledge.....");
                        this.ClearForm(pnlTblBlkTwo.Controls);
                        txt_W_TIR_PER.Text = "";
                        txt_W_YEAR.Focus(); 
                    }
                    else if (rsltCode.isSuccessful)
                    {
                        //MessageBox.Show("Record Not Found", "Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //txt_W_YEAR.Focus();
                        //return;

                    }
                    if (txt_W_YEAR.Text != string.Empty)
                    {
                        dgvTir.Enabled = true;
                        dgvTir.ReadOnly = false;
                    }
                }
                //else
                //{
                //    //txt_W_YEAR.Select();
                //    //txt_W_YEAR.Focus();
                //    //dgvTir.EndEdit();
                //    //dgvTir.Focus();
                //}
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_YEAR_Leave", exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvTir_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (this.tbtClose.Pressed)
                return;
            

            if (!dgvTir.CurrentCell.IsInEditMode || operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Cancel)
                return;

            if (dgvTir.RowCount > 1 && SkipValidation == false)
            {
                if (dgvTir.CurrentCell.OwningColumn.Name == "Rank")
                {
                    if (dgvTir.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                    {
                        //e.Cancel = true;
                        dgvTir.SkippingColumns = "Level";
                        Level.SkipValidationOnLeave = true;
                        //SkipLevel = true;
                        return;

                    }
                    else
                    {
                        //dgvTir.SkippingColumns = "";
                        //Level.SkipValidationOnLeave = false;
                        //SkipLevel = true;
                    }
                }
                else if (dgvTir.CurrentCell.OwningColumn.Name == "Level" && SkipLevel == false)
                {
                    if (dgvTir.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                    {
                        MessageBox.Show(" Value Has To Be Entered ", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }

                    if (txt_W_TIR_PER.Text != string.Empty)
                        dgvTir.CurrentRow.Cells["Tri"].Value = txt_W_TIR_PER.Text;

                    if (txt_W_YEAR.Text != string.Empty)
                        dgvTir.CurrentRow.Cells["Year"].Value = txt_W_YEAR.Text;
                }
                else if (dgvTir.CurrentCell.OwningColumn.Name == "MinAmt")
                {
                    if (dgvTir.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                    {
                        MessageBox.Show("Field Must Be Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }

                }
                else if (dgvTir.CurrentCell.OwningColumn.Name == "grdMaxAmt")
                {
                    if (dgvTir.CurrentCell.EditedFormattedValue.ToString() != string.Empty && dgvTir.CurrentCell.EditedFormattedValue.ToString() != "")
                    {

                        if (!(double.TryParse(dgvTir.CurrentCell.EditedFormattedValue.ToString(), out temp)))
                        {
                            e.Cancel = true;
                            return;
                        }
                        
                        if (dgvTir.CurrentRow.Cells["MinAmt"].Value != null)
                        {
                            if (dgvTir.CurrentRow.Cells["MinAmt"].Value.ToString() != string.Empty)
                                MinVal = Convert.ToDecimal(dgvTir.CurrentRow.Cells["MinAmt"].Value.ToString());
                        }

                        if (dgvTir.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                            MaxVal = Convert.ToDecimal(dgvTir.CurrentRow.Cells["grdMaxAmt"].EditedFormattedValue.ToString());

                        //if (Convert.ToDecimal(dgvTir.CurrentCell.EditedFormattedValue.ToString()) < Convert.ToDecimal(dgvTir.CurrentRow.Cells["MinAmt"].EditedFormattedValue.ToString()))
                        if (MaxVal < MinVal)
                        {
                            MessageBox.Show("Maximum Percentage Must Be Greater Than Minimum Percentage", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Field Must Be Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                        e.Cancel = true;
                    }
                }
                else if (dgvTir.CurrentCell.OwningColumn.Name == "Average")
                {
                    if (dgvTir.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                    {
                        MessageBox.Show("Field Must Be Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (dgvTir.CurrentRow.Cells[3].EditedFormattedValue.ToString()  != string.Empty || dgvTir.CurrentRow.Cells[3].Value != null)
                        {
                            dgvTir.CurrentRow.Cells[5].Value  = (Convert.ToDecimal(dgvTir.CurrentRow.Cells[3].Value) + Convert.ToDecimal(dgvTir.CurrentRow.Cells[4].Value)) / 2;
                        }
                        e.Cancel = true;
                    }

                }
            }
            else
            {
                SkipValidation = false;
            }
        }

        /// <summary>
        /// Override Toolbar: Check if W_YEAR already entered or not.
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("SP_YEAR", txt_W_YEAR.Text);
                ////////////////////////
                base.AllGridsEndEdit(this);
                ///////////////////////

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "W_YEAR", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables.Count >0) 
                {
                    MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txt_W_YEAR.Text != "")
                    (pnlTblBlkTwo.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIREntCommand).SP_YEAR = Convert.ToDecimal(txt_W_YEAR.Text);
                if (txt_W_TIR_PER.Text != "")
                    (pnlTblBlkTwo.CurrentBusinessEntity as iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIREntCommand).SP_TIR_PER = Convert.ToDecimal(txt_W_TIR_PER.Text);

                DataTable dt = (DataTable)dgvTir.DataSource;
                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {

                        for (int j = 0; j < dgvTir.Rows.Count - 1; j++)
                        {
                            if (Convert.ToString(dt.Rows[j]["SP_RANK"]) == "" || Convert.ToString(dt.Rows[j]["SP_LEVEL"]) == "" || Convert.ToString(dt.Rows[j]["SP_MIN_PER"]) == "" || Convert.ToString(dt.Rows[j]["SP_MAX_PER"]) == "" || Convert.ToString(dt.Rows[j]["SP_AVERAGE"]) == "")
                            {
                                MessageBox.Show("Value has to be Entered");
                                dgvTir.Focus();
                                return;
                            }
                        }



                        DialogResult dr = MessageBox.Show("Do You Want to Save Changes?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            bool isSaved = dgvTir.SaveGrid(this.pnlTblBlkTwo);
                            if (isSaved)
                            {
                                MessageBox.Show("Changes Saved Successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dt.AcceptChanges();
                                this.tbtSave.Enabled = false;
                                this.ClearForm(pnlHead.Controls); 
                                this.ClearForm(pnlTblBlkTwo.Controls); 
                                CallReportNew();
                                CallReportOld();
                            }
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }

            if (actionType == "Cancel")
            {
                dgvTir.CancelEdit();
                dgvTir.EndEdit();

                base.ClearForm(this.pnlTblBlkTwo.Controls);
                //dgvTir.RejectChanges();
                //dgvTir.DataSource = null;
                //dgvTir.Rows.Clear();
                
                

                //SkipValidation = true;

                this.txt_W_YEAR.CustomEnabled = true;
                this.txt_W_TIR_PER.CustomEnabled = true;

                this.txt_W_YEAR.ReadOnly = false;
                this.txt_W_TIR_PER.ReadOnly = false;

                this.txt_W_YEAR.Text = "";
                this.txt_W_TIR_PER.Text = "";
                this.txt_W_YEAR.Select();
                this.txt_W_YEAR.Focus();
                return;

            }
            else if(actionType != "Close")
            {
                SkipValidation = false;

            }


            //if (actionType == "Search")
            //{
            //    if (dgvTir.Rows.Count > 1)
            //    {
            //        //dgvTir.ReadOnly = false;
            //        //dgvTir.CurrentRow.Cells[0].ReadOnly = true;
            //        for (int i = 0; i < dgvTir.Rows.Count - 1; i++)
            //        {
            //            dgvTir.Rows[i].Cells[0].ReadOnly = true;
            //        }
            //        ///dgvTir.Focus();
            //    }
            //}
            
            base.DoToolbarActions(ctrlsCollection, actionType);

            
        }

        /// <summary>
        /// After Cell Validated: Calculate the Avg
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvTir_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTir.CurrentCell != null)
            {
                if (dgvTir.CurrentCell.OwningColumn.Name == "grdMaxAmt")
                {
                    if (dgvTir.CurrentRow.Cells["MinAmt"].Value != null)
                    {
                        if (dgvTir.CurrentRow.Cells["MinAmt"].Value.ToString() != string.Empty)
                            MinVal = Convert.ToDecimal(dgvTir.CurrentRow.Cells["MinAmt"].Value.ToString());
                        else
                            return;
                    }

                    if (dgvTir.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                        MaxVal = Convert.ToDecimal(dgvTir.CurrentRow.Cells["grdMaxAmt"].EditedFormattedValue.ToString());


                    AvgVal = ((MaxVal + MinVal) / 2);
                    dgvTir.CurrentRow.Cells["Average"].Value = AvgVal;
                    MinVal = 0;
                    MaxVal = 0;

                }
            }
        }


        #endregion
        
        #region --Method--
        /// <summary>
        /// Call Old Report
        /// </summary>
        private void CallReportOld()
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string FN1;
            FN1 = "AUPR" + Now().ToString("yyyyMMddHHmmss") + ".Lis";
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            //txtDESTYPE.DataFieldMapping = "SECURITY_CODE";
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = "C:\\iCORE-Spool\\TEST";      //":GLOBAL.AUDIT_PATH||FN1";
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox YEAR1 = new CrplControlLibrary.SLTextBox(comp);
            YEAR1.Name = "YEAR1";
            YEAR1.Text = txt_W_YEAR.Text;
            pnl.Controls.Add(YEAR1);

            CrplControlLibrary.SLTextBox USER = new CrplControlLibrary.SLTextBox(comp);
            USER.Name = "USER";
            USER.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(USER);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "txtST";
            txtST.Text = "OLD";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString();
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit20A";
            frm.Owner = this;
            //frm.RunReport();
            frm.ExportCustomReportToTXT("C:\\iCORE-Spool\\TESTOLD", "TXT");
        }

        /// <summary>
        /// Call New Report
        /// </summary>
        private void CallReportNew()
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string FN1;
            FN1 = "AUPO" + Now().ToString("yyyyMMddHHmmss") + ".Lis";

            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            //txtDESTYPE.DataFieldMapping = "SECURITY_CODE";
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = "C:\\iCORE-Spool\\TEST";      //":GLOBAL.AUDIT_PATH||FN1";
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox YEAR1 = new CrplControlLibrary.SLTextBox(comp);
            YEAR1.Name = "YEAR1";
            YEAR1.Text = txt_W_YEAR.Text;
            pnl.Controls.Add(YEAR1);

            CrplControlLibrary.SLTextBox USER = new CrplControlLibrary.SLTextBox(comp);
            USER.Name = "USER";
            USER.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(USER);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "txtST";
            txtST.Text = "NEW";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString();
            pnl.Controls.Add(txtDT);

            frm.Controls.Add(pnl);
            frm.RptFileName = "audit20A";
            frm.Owner = this;
            //frm.RunReport();
            frm.ExportCustomReportToTXT("C:\\iCORE-Spool\\TESTNew", "TXT");
        }

        #endregion

        private void dgvTir_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvTir.CurrentCell != null)
            //{
            //    if (dgvTir.CurrentCell.OwningColumn.Name == "Level")
            //    {
            //        return;
            //        //Level.SkipValidationOnLeave = true;
            //    }
            //}
        }
    }
}