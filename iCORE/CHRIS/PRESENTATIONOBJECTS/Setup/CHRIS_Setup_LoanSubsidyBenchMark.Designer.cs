namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_LoanSubsidyBenchMark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_LoanSubsidyBenchMark));
            this.pnlLoanTax = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvRecords = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.TaxYearFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxYearTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlLoanTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLoanTax
            // 
            this.pnlLoanTax.ConcurrentPanels = null;
            this.pnlLoanTax.Controls.Add(this.dgvRecords);
            this.pnlLoanTax.DataManager = null;
            this.pnlLoanTax.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlLoanTax.DependentPanels = null;
            this.pnlLoanTax.DisableDependentLoad = false;
            this.pnlLoanTax.EnableDelete = true;
            this.pnlLoanTax.EnableInsert = true;
            this.pnlLoanTax.EnableQuery = false;
            this.pnlLoanTax.EnableUpdate = true;
            this.pnlLoanTax.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LoanTaxCommand";
            this.pnlLoanTax.Location = new System.Drawing.Point(28, 99);
            this.pnlLoanTax.MasterPanel = null;
            this.pnlLoanTax.Name = "pnlLoanTax";
            this.pnlLoanTax.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlLoanTax.Size = new System.Drawing.Size(426, 250);
            this.pnlLoanTax.SPName = "CHRIS_SP_LOAN_TAX_MANAGER";
            this.pnlLoanTax.TabIndex = 8;
            // 
            // dgvRecords
            // 
            this.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecords.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TaxYearFrom,
            this.TaxYearTo,
            this.TaxPercentage});
            this.dgvRecords.ColumnToHide = null;
            this.dgvRecords.ColumnWidth = null;
            this.dgvRecords.CustomEnabled = true;
            this.dgvRecords.DisplayColumnWrapper = null;
            this.dgvRecords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRecords.GridDefaultRow = 0;
            this.dgvRecords.Location = new System.Drawing.Point(0, 0);
            this.dgvRecords.Name = "dgvRecords";
            this.dgvRecords.ReadOnlyColumns = null;
            this.dgvRecords.RequiredColumns = null;
            this.dgvRecords.Size = new System.Drawing.Size(426, 250);
            this.dgvRecords.SkippingColumns = null;
            this.dgvRecords.TabIndex = 1;
            this.dgvRecords.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvRecords_CellValidating);
            // 
            // TaxYearFrom
            // 
            this.TaxYearFrom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TaxYearFrom.DataPropertyName = "TAX_YEAR_FROM";
            this.TaxYearFrom.HeaderText = "Tax Year From";
            this.TaxYearFrom.MaxInputLength = 11;
            this.TaxYearFrom.Name = "TaxYearFrom";
            // 
            // TaxYearTo
            // 
            this.TaxYearTo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TaxYearTo.DataPropertyName = "TAX_YEAR_TO";
            this.TaxYearTo.HeaderText = "Tax Year To";
            this.TaxYearTo.MaxInputLength = 11;
            this.TaxYearTo.Name = "TaxYearTo";
            // 
            // TaxPercentage
            // 
            this.TaxPercentage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TaxPercentage.DataPropertyName = "TAX_PER";
            this.TaxPercentage.HeaderText = "Tax %";
            this.TaxPercentage.MaxInputLength = 5;
            this.TaxPercentage.Name = "TaxPercentage";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(234, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 19;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Setup_LoanSubsidyBenchMark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 438);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlLoanTax);
            this.CurrentPanelBlock = "pnlLoanTax";
            this.Name = "CHRIS_Setup_LoanSubsidyBenchMark";
            this.SetFormTitle = "Loan Tax Benchmark Setup";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS :  Loan Tax Slab";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlLoanTax, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlLoanTax.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlLoanTax;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvRecords;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxYearFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxYearTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxPercentage;
    }
}