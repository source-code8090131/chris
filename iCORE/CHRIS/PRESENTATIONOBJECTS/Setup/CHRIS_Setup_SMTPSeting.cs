using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_SMTPSeting : iCORE.CHRISCOMMON.PRESENTATIONOBJECTS.ChrisSimpleForm
    {
        #region Fields
        CmnDataManager cmnDM = new CmnDataManager();
        #endregion
       
        #region Constructor
        public CHRIS_Setup_SMTPSeting()
        {
            InitializeComponent();
        }
        public CHRIS_Setup_SMTPSeting(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //DgvIncome.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            label9.Text += "   " + this.UserName;
        }
        #endregion

        #region Methods
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            txtOption.Visible = false;
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;
            tbtCancel.Visible = false;
            tbtEdit.Visible = false;
            LoadSMTPSettings();
        }
        private void SaveSMTPSettings()
        {
            Dictionary<string, object> dictParam = new Dictionary<string, object>();
            dictParam.Add("ProfileName", txtbProfileName.Text);
            dictParam.Add("SharedFolderPath", txtbSharedFolderPath.Text);
            dictParam.Add("Subject", txtbSubject.Text);
            dictParam.Add("Body", rtbBody.Text);

            rslt = cmnDM.GetData("CHRIS_SP_SMTP_SETTINGS", "Save", dictParam);

            if (rslt.isSuccessful)
                MessageBox.Show("SMTP Settings Saved");
            else
                MessageBox.Show(rslt.message);
        }

        private void LoadSMTPSettings()
        {
            rslt = cmnDM.GetData("CHRIS_SP_SMTP_SETTINGS", "List");
            if (rslt.isSuccessful && rslt.dstResult.Tables[0].Rows.Count>0)
            {
                txtbProfileName.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                txtbSharedFolderPath.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                txtbSubject.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                rtbBody.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
             }
        }
       
    #endregion

        #region Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveSMTPSettings();
        }
        #endregion
    }
}

