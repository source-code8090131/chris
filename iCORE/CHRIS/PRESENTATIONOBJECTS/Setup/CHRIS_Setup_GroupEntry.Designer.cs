namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_GroupEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_GroupEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlGroup = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVGroup = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_SEGMENT = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.SP_GROUP_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DESC_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DESC_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_GROUP_HEAD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DATE_FROM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DATE_TO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 431);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(18, 82);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(614, 75);
            this.pnlHead.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(165, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(276, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "G R O U P   E N T R Y";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(165, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(276, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Set Up";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 42);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(455, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 42);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 15);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlGroup
            // 
            this.pnlGroup.ConcurrentPanels = null;
            this.pnlGroup.Controls.Add(this.DGVGroup);
            this.pnlGroup.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlGroup.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlGroup.DependentPanels = null;
            this.pnlGroup.DisableDependentLoad = false;
            this.pnlGroup.EnableDelete = true;
            this.pnlGroup.EnableInsert = true;
            this.pnlGroup.EnableQuery = false;
            this.pnlGroup.EnableUpdate = true;
            this.pnlGroup.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GroupSetupCommand";
            this.pnlGroup.Location = new System.Drawing.Point(18, 164);
            this.pnlGroup.MasterPanel = null;
            this.pnlGroup.Name = "pnlGroup";
            this.pnlGroup.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlGroup.Size = new System.Drawing.Size(614, 250);
            this.pnlGroup.SPName = "CHRIS_SP_SETUP_GROUP1_MANAGER";
            this.pnlGroup.TabIndex = 14;
            // 
            // DGVGroup
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_SEGMENT,
            this.SP_GROUP_CODE,
            this.SP_DESC_1,
            this.SP_DESC_2,
            this.SP_GROUP_HEAD,
            this.SP_DATE_FROM,
            this.SP_DATE_TO,
            this.ID});
            this.DGVGroup.ColumnToHide = null;
            this.DGVGroup.ColumnWidth = null;
            this.DGVGroup.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVGroup.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGVGroup.DisplayColumnWrapper = null;
            this.DGVGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVGroup.GridDefaultRow = 0;
            this.DGVGroup.Location = new System.Drawing.Point(0, 0);
            this.DGVGroup.Name = "DGVGroup";
            this.DGVGroup.ReadOnlyColumns = null;
            this.DGVGroup.RequiredColumns = null;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVGroup.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGVGroup.Size = new System.Drawing.Size(614, 250);
            this.DGVGroup.SkippingColumns = null;
            this.DGVGroup.TabIndex = 0;
            this.DGVGroup.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVGroup_RowEnter);
            this.DGVGroup.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGVGroup_RowValidating);
            this.DGVGroup.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVGroup_CellValidating);
            // 
            // SP_SEGMENT
            // 
            this.SP_SEGMENT.ActionLOV = "Segment_LOVs";
            this.SP_SEGMENT.ActionLOVExists = "Segment_LOVsExists";
            this.SP_SEGMENT.AttachParentEntity = false;
            this.SP_SEGMENT.DataPropertyName = "SP_SEGMENT";
            this.SP_SEGMENT.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GroupSetupCommand";
            this.SP_SEGMENT.HeaderText = "Segment";
            this.SP_SEGMENT.LookUpTitle = null;
            this.SP_SEGMENT.LOVFieldMapping = "SP_SEGMENT";
            this.SP_SEGMENT.MaxInputLength = 3;
            this.SP_SEGMENT.Name = "SP_SEGMENT";
            this.SP_SEGMENT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_SEGMENT.SearchColumn = "SP_SEGMENT";
            this.SP_SEGMENT.SkipValidationOnLeave = false;
            this.SP_SEGMENT.SpName = "CHRIS_SP_SETUP_GROUP1_MANAGER";
            this.SP_SEGMENT.Width = 85;
            // 
            // SP_GROUP_CODE
            // 
            this.SP_GROUP_CODE.DataPropertyName = "SP_GROUP_CODE";
            this.SP_GROUP_CODE.HeaderText = "Group";
            this.SP_GROUP_CODE.MaxInputLength = 5;
            this.SP_GROUP_CODE.Name = "SP_GROUP_CODE";
            this.SP_GROUP_CODE.Width = 80;
            // 
            // SP_DESC_1
            // 
            this.SP_DESC_1.DataPropertyName = "SP_DESC_1";
            this.SP_DESC_1.HeaderText = "Description 1";
            this.SP_DESC_1.MaxInputLength = 20;
            this.SP_DESC_1.Name = "SP_DESC_1";
            this.SP_DESC_1.Width = 125;
            // 
            // SP_DESC_2
            // 
            this.SP_DESC_2.DataPropertyName = "SP_DESC_2";
            this.SP_DESC_2.HeaderText = "Description 2";
            this.SP_DESC_2.MaxInputLength = 20;
            this.SP_DESC_2.Name = "SP_DESC_2";
            this.SP_DESC_2.Width = 125;
            // 
            // SP_GROUP_HEAD
            // 
            this.SP_GROUP_HEAD.DataPropertyName = "SP_GROUP_HEAD";
            this.SP_GROUP_HEAD.HeaderText = "Group Head";
            this.SP_GROUP_HEAD.MaxInputLength = 20;
            this.SP_GROUP_HEAD.Name = "SP_GROUP_HEAD";
            // 
            // SP_DATE_FROM
            // 
            this.SP_DATE_FROM.DataPropertyName = "SP_DATE_FROM";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.SP_DATE_FROM.DefaultCellStyle = dataGridViewCellStyle2;
            this.SP_DATE_FROM.HeaderText = "From ";
            this.SP_DATE_FROM.MaxInputLength = 10;
            this.SP_DATE_FROM.Name = "SP_DATE_FROM";
            this.SP_DATE_FROM.Width = 80;
            // 
            // SP_DATE_TO
            // 
            this.SP_DATE_TO.DataPropertyName = "SP_DATE_TO";
            dataGridViewCellStyle3.Format = "dd/MM/yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.SP_DATE_TO.DefaultCellStyle = dataGridViewCellStyle3;
            this.SP_DATE_TO.HeaderText = "To";
            this.SP_DATE_TO.MaxInputLength = 10;
            this.SP_DATE_TO.Name = "SP_DATE_TO";
            this.SP_DATE_TO.Width = 80;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(415, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(78, 13);
            this.txtUserName.TabIndex = 121;
            this.txtUserName.Text = "User  Name :   ";
            // 
            // CHRIS_Setup_GroupEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 491);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlGroup);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlGroup";
            this.Name = "CHRIS_Setup_GroupEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Group Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlGroup, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlGroup;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVGroup;
        private System.Windows.Forms.Label txtUserName;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_SEGMENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_GROUP_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DESC_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DESC_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_GROUP_HEAD;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DATE_FROM;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DATE_TO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}