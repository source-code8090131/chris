using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_AttritionTypeEntry : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();

        public CHRIS_Setup_AttritionTypeEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_AttritionTypeEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this.pnlHead.SendToBack();
            this.txtOption.Select();
            this.txtOption.Focus();
            this.txtOption.Visible = false;
            txtUserName.Text += "   " + this.UserName;
            DGVAttrition.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

          

        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Select();
            this.txtOption.Focus();
            this.txtOption.Visible = false;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.txtLocation.Text = this.CurrentLocation;
            this.txtUser.Text = this.userID;
            txtUserName.Text = "User Name:  " + this.UserName;
            //ATTR_CATEGORY_CODE.Width = 100;
            //ATTR_CATEGORY_DESC.Width = 205;
            //ATTR_TYPE_CODE.Width = 100;
            //ATTR_TYPE_DESC.Width = 220;
            this.ATTR_CATEGORY_CODE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.ATTR_CATEGORY_DESC.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.ATTR_TYPE_CODE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.ATTR_TYPE_DESC.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            DGVAttrition.Columns["ATTR_CATEGORY_CODE"].ValueType = typeof(double);

            ATTR_CATEGORY_CODE.ActionLOVExists = "";
            ATTR_TYPE_DESC.ActionLOVExists = "";
            ATTR_TYPE_DESC.SkipValidationOnLeave = true;

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;

            this.FunctionConfig.EnableF4 = true;
            this.FunctionConfig.F4 = Function.Delete;



        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            bool returnValue = false;
            if (actionType == "Save")
            {

                DataTable dt = (DataTable)DGVAttrition.DataSource;

                if (dt != null)
                {
                    
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);

                    if (dtAdded != null || dtUpdated != null)
                    {
                    
                        for (int j = 0; j < DGVAttrition.Rows.Count - 1; j++)
                        {
                            if (Convert.ToString(dt.Rows[j]["ATTR_TYPE_DESC"]) == "")
                            {
                                MessageBox.Show("Enter Attrition Description");
                                DGVAttrition.Focus();
                                return;
                            }
                        }

                        DialogResult dr = MessageBox.Show("Do You Want to Save Changes.", "", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);

                        if (dr == DialogResult.No)
                        {
                            return;
                        }

                        if (dr == DialogResult.Yes)
                        {
                            returnValue=this.DGVAttrition.SaveGrid(pnlTabular);
                            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
                            return;

                        }


                    }

                }

            }

            if (actionType != "Close" && actionType != "Cancel" && this.txtOption.Focused)
            {
                return;
            }
            if (actionType == "Cancel")
            {
                this.DGVAttrition.RejectChanges();
                base.DoToolbarActions(ctrlsCollection, actionType);
                return;
            }

            if (actionType == "Delete")
            {
                //It checks (TypeCode) in Attrition/AttritionCategory and delete from Attr_Type//
                string delchk = DGVAttrition.CurrentRow.Cells["attr_type_code"].EditedFormattedValue.ToString();

                if (delchk != string.Empty )
                {

                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Clear();
                    param.Add("attr_type_code", delchk);
                    rslt = cmnDM.GetData("CHRIS_SP_SETUP_ATTR_TYPE_MANAGER", "Deletecheck", param);

                    if (rslt.isSuccessful)
                    {
                       if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult != null)
                       {
                           //if (rslt.dstResult.Tables[0].Rows[0].ToString() != string.Empty)
                           if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "0" || rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                           {
                                base.DoToolbarActions(ctrlsCollection, actionType);
                           }
                          
                       }
                       else
                       {
                           MessageBox.Show("Record Found Cannot Delete!");
                           return;

                       }              
                    
                    }
                
                
                }



            }


        
         else   base.DoToolbarActions(ctrlsCollection, actionType);

        }


        protected override bool Delete()
        {

            //MessageBox.Show("CURRENT OPTION =  " + this.FunctionConfig.CurrentOption);
            return base.Delete();
        }


        private void DGVAttrition_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (e.ColumnIndex == 3)
            {

                if (DGVAttrition.Rows[e.RowIndex].Cells[3].IsInEditMode)
                {

                    if (DGVAttrition.CurrentRow.Cells[3].EditedFormattedValue.ToString() == "")
                    {
                        tbtSave.Enabled = false;
                        e.Cancel = true;
                        return;
                    }
                }
            }

        }

        private void DGVAttrition_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }


        }

        

    }
}