namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_HolidayEnter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_HolidayEnter));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnltblHoliday = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVHoliday = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_HOL_DATE = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.SP_H_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnltblHoliday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVHoliday)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(578, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(614, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 428);
            this.panel1.Size = new System.Drawing.Size(614, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(12, 85);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(590, 91);
            this.pnlHead.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(165, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(276, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "HOLIDAY ENTRY";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(165, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(276, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Set Up";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 58);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(455, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnltblHoliday
            // 
            this.pnltblHoliday.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnltblHoliday.ConcurrentPanels = null;
            this.pnltblHoliday.Controls.Add(this.DGVHoliday);
            this.pnltblHoliday.DataManager = "iCORE.Common.CommonDataManager";
            this.pnltblHoliday.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnltblHoliday.DependentPanels = null;
            this.pnltblHoliday.DisableDependentLoad = false;
            this.pnltblHoliday.EnableDelete = true;
            this.pnltblHoliday.EnableInsert = true;
            this.pnltblHoliday.EnableQuery = false;
            this.pnltblHoliday.EnableUpdate = true;
            this.pnltblHoliday.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.HolidayCommand";
            this.pnltblHoliday.Location = new System.Drawing.Point(18, 182);
            this.pnltblHoliday.MasterPanel = null;
            this.pnltblHoliday.Name = "pnltblHoliday";
            this.pnltblHoliday.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnltblHoliday.Size = new System.Drawing.Size(572, 230);
            this.pnltblHoliday.SPName = "CHRIS_SP_HOLIDAY_MANAGER";
            this.pnltblHoliday.TabIndex = 13;
            // 
            // DGVHoliday
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVHoliday.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVHoliday.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVHoliday.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_HOL_DATE,
            this.SP_H_DESC,
            this.ID});
            this.DGVHoliday.ColumnToHide = null;
            this.DGVHoliday.ColumnWidth = null;
            this.DGVHoliday.CustomEnabled = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVHoliday.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGVHoliday.DisplayColumnWrapper = null;
            this.DGVHoliday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVHoliday.GridDefaultRow = 0;
            this.DGVHoliday.Location = new System.Drawing.Point(0, 0);
            this.DGVHoliday.Name = "DGVHoliday";
            this.DGVHoliday.ReadOnlyColumns = null;
            this.DGVHoliday.RequiredColumns = null;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVHoliday.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGVHoliday.Size = new System.Drawing.Size(570, 228);
            this.DGVHoliday.SkippingColumns = null;
            this.DGVHoliday.TabIndex = 0;
            this.DGVHoliday.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVHoliday_RowEnter);
            this.DGVHoliday.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DGVHoliday_CellFormatting);
            this.DGVHoliday.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVHoliday_CellValidating);
            // 
            // SP_HOL_DATE
            // 
            this.SP_HOL_DATE.ActionLOV = "HolidayLOV";
            this.SP_HOL_DATE.ActionLOVExists = "HolidayLOVExists";
            this.SP_HOL_DATE.AttachParentEntity = false;
            this.SP_HOL_DATE.DataPropertyName = "SP_HOL_DATE";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            this.SP_HOL_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.SP_HOL_DATE.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.HolidayCommand";
            this.SP_HOL_DATE.HeaderText = "Date";
            this.SP_HOL_DATE.LookUpTitle = null;
            this.SP_HOL_DATE.LOVFieldMapping = "SP_HOL_DATE";
            this.SP_HOL_DATE.MaxInputLength = 10;
            this.SP_HOL_DATE.Name = "SP_HOL_DATE";
            this.SP_HOL_DATE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_HOL_DATE.SearchColumn = "SP_HOL_DATE_string";
            this.SP_HOL_DATE.SkipValidationOnLeave = false;
            this.SP_HOL_DATE.SpName = "CHRIS_SP_HOLIDAY_MANAGER";
            // 
            // SP_H_DESC
            // 
            this.SP_H_DESC.DataPropertyName = "SP_H_DESC";
            this.SP_H_DESC.HeaderText = "Description";
            this.SP_H_DESC.MaxInputLength = 20;
            this.SP_H_DESC.Name = "SP_H_DESC";
            this.SP_H_DESC.Width = 150;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(357, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(72, 13);
            this.txtUserName.TabIndex = 120;
            this.txtUserName.Text = "User  Name : ";
            // 
            // CHRIS_Setup_HolidayEnter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 488);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnltblHoliday);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnltblHoliday";
            this.Name = "CHRIS_Setup_HolidayEnter";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS  :  Setup Holiday Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnltblHoliday, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnltblHoliday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVHoliday)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnltblHoliday;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVHoliday;
        private System.Windows.Forms.Label txtUserName;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_HOL_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_H_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}