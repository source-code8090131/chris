namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_TIREntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_TIREntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlTblBlkTwo = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvTir = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Rank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.MinAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdMaxAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Average = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_TIR_PER = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblBlkTwo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTir)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(533, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(569, 22);
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(569, 60);
            // 
            // pnlTblBlkTwo
            // 
            this.pnlTblBlkTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTblBlkTwo.ConcurrentPanels = null;
            this.pnlTblBlkTwo.Controls.Add(this.dgvTir);
            this.pnlTblBlkTwo.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBlkTwo.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBlkTwo.DependentPanels = null;
            this.pnlTblBlkTwo.DisableDependentLoad = false;
            this.pnlTblBlkTwo.EnableDelete = true;
            this.pnlTblBlkTwo.EnableInsert = true;
            this.pnlTblBlkTwo.EnableQuery = false;
            this.pnlTblBlkTwo.EnableUpdate = true;
            this.pnlTblBlkTwo.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIREntCommand";
            this.pnlTblBlkTwo.Location = new System.Drawing.Point(12, 178);
            this.pnlTblBlkTwo.MasterPanel = null;
            this.pnlTblBlkTwo.Name = "pnlTblBlkTwo";
            this.pnlTblBlkTwo.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBlkTwo.Size = new System.Drawing.Size(549, 228);
            this.pnlTblBlkTwo.SPName = "CHRIS_SP_TIREnt_TIR_MANAGER";
            this.pnlTblBlkTwo.TabIndex = 0;
            // 
            // dgvTir
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTir.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTir.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTir.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Rank,
            this.ID,
            this.Level,
            this.MinAmt,
            this.grdMaxAmt,
            this.Average,
            this.Year,
            this.TRI});
            this.dgvTir.ColumnToHide = null;
            this.dgvTir.ColumnWidth = null;
            this.dgvTir.CustomEnabled = true;
            this.dgvTir.DisplayColumnWrapper = null;
            this.dgvTir.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvTir.GridDefaultRow = 0;
            this.dgvTir.Location = new System.Drawing.Point(3, 3);
            this.dgvTir.Name = "dgvTir";
            this.dgvTir.ReadOnlyColumns = null;
            this.dgvTir.RequiredColumns = null;
            this.dgvTir.Size = new System.Drawing.Size(541, 218);
            this.dgvTir.SkippingColumns = null;
            this.dgvTir.TabIndex = 0;
            this.dgvTir.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTir_CellValidated);
            this.dgvTir.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvTir_CellValidating);
            this.dgvTir.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTir_CellEnter);
            // 
            // Rank
            // 
            this.Rank.DataPropertyName = "SP_RANK";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.Rank.DefaultCellStyle = dataGridViewCellStyle2;
            this.Rank.HeaderText = "Rank";
            this.Rank.MaxInputLength = 2;
            this.Rank.Name = "Rank";
            this.Rank.Width = 115;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // Level
            // 
            this.Level.ActionLOV = "LEVEL_LOV";
            this.Level.ActionLOVExists = "LEVEL_LOV_EXIST";
            this.Level.AttachParentEntity = false;
            this.Level.DataPropertyName = "SP_LEVEL";
            this.Level.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIREntCommand";
            this.Level.HeaderText = "Level";
            this.Level.LookUpTitle = null;
            this.Level.LOVFieldMapping = "SP_LEVEL";
            this.Level.MaxInputLength = 3;
            this.Level.Name = "Level";
            this.Level.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Level.SearchColumn = "SP_LEVEL";
            this.Level.SkipValidationOnLeave = false;
            this.Level.SpName = "CHRIS_SP_TIREnt_TIR_MANAGER";
            this.Level.Width = 110;
            // 
            // MinAmt
            // 
            this.MinAmt.DataPropertyName = "SP_MIN_PER";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.MinAmt.DefaultCellStyle = dataGridViewCellStyle3;
            this.MinAmt.HeaderText = "Min %";
            this.MinAmt.MaxInputLength = 2;
            this.MinAmt.Name = "MinAmt";
            this.MinAmt.Width = 120;
            // 
            // grdMaxAmt
            // 
            this.grdMaxAmt.DataPropertyName = "SP_MAX_PER";
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.grdMaxAmt.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdMaxAmt.HeaderText = "Max %";
            this.grdMaxAmt.MaxInputLength = 2;
            this.grdMaxAmt.Name = "grdMaxAmt";
            this.grdMaxAmt.Width = 120;
            // 
            // Average
            // 
            this.Average.DataPropertyName = "SP_AVERAGE";
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.Average.DefaultCellStyle = dataGridViewCellStyle5;
            this.Average.HeaderText = "Average %";
            this.Average.MaxInputLength = 2;
            this.Average.Name = "Average";
            this.Average.Width = 120;
            // 
            // Year
            // 
            this.Year.DataPropertyName = "SP_YEAR";
            this.Year.HeaderText = "Year";
            this.Year.MaxInputLength = 4;
            this.Year.Name = "Year";
            this.Year.Visible = false;
            this.Year.Width = 110;
            // 
            // TRI
            // 
            this.TRI.DataPropertyName = "SP_TIR_PER";
            this.TRI.HeaderText = "TRI";
            this.TRI.MaxInputLength = 4;
            this.TRI.Name = "TRI";
            this.TRI.Visible = false;
            this.TRI.Width = 110;
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.ConcurrentPanels = null;
            this.pnlHead.Controls.Add(this.txt_W_TIR_PER);
            this.pnlHead.Controls.Add(this.txt_W_YEAR);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlHead.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlHead.DependentPanels = null;
            this.pnlHead.DisableDependentLoad = false;
            this.pnlHead.EnableDelete = true;
            this.pnlHead.EnableInsert = true;
            this.pnlHead.EnableQuery = false;
            this.pnlHead.EnableUpdate = true;
            this.pnlHead.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIREntCommand";
            this.pnlHead.Location = new System.Drawing.Point(12, 133);
            this.pnlHead.MasterPanel = null;
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlHead.Size = new System.Drawing.Size(546, 45);
            this.pnlHead.SPName = "CHRIS_SP_TIREnt_TIR_MANAGER";
            this.pnlHead.TabIndex = 1;
            // 
            // txt_W_TIR_PER
            // 
            this.txt_W_TIR_PER.AllowSpace = true;
            this.txt_W_TIR_PER.AssociatedLookUpName = "";
            this.txt_W_TIR_PER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_TIR_PER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_TIR_PER.ContinuationTextBox = null;
            this.txt_W_TIR_PER.CustomEnabled = true;
            this.txt_W_TIR_PER.DataFieldMapping = "W_TIR_PER";
            this.txt_W_TIR_PER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_TIR_PER.GetRecordsOnUpDownKeys = false;
            this.txt_W_TIR_PER.IsDate = false;
            this.txt_W_TIR_PER.Location = new System.Drawing.Point(363, 14);
            this.txt_W_TIR_PER.MaxLength = 4;
            this.txt_W_TIR_PER.Name = "txt_W_TIR_PER";
            this.txt_W_TIR_PER.NumberFormat = "###,###,##0";
            this.txt_W_TIR_PER.Postfix = "";
            this.txt_W_TIR_PER.Prefix = "";
            this.txt_W_TIR_PER.Size = new System.Drawing.Size(58, 20);
            this.txt_W_TIR_PER.SkipValidation = false;
            this.txt_W_TIR_PER.TabIndex = 1;
            this.txt_W_TIR_PER.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_TIR_PER.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txt_W_YEAR
            // 
            this.txt_W_YEAR.AllowSpace = true;
            this.txt_W_YEAR.AssociatedLookUpName = "";
            this.txt_W_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_YEAR.ContinuationTextBox = null;
            this.txt_W_YEAR.CustomEnabled = true;
            this.txt_W_YEAR.DataFieldMapping = "SP_YEAR";
            this.txt_W_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_YEAR.GetRecordsOnUpDownKeys = false;
            this.txt_W_YEAR.IsDate = false;
            this.txt_W_YEAR.Location = new System.Drawing.Point(191, 14);
            this.txt_W_YEAR.MaxLength = 4;
            this.txt_W_YEAR.Name = "txt_W_YEAR";
            this.txt_W_YEAR.NumberFormat = "###,###,##0.00";
            this.txt_W_YEAR.Postfix = "";
            this.txt_W_YEAR.Prefix = "";
            this.txt_W_YEAR.Size = new System.Drawing.Size(62, 20);
            this.txt_W_YEAR.SkipValidation = false;
            this.txt_W_YEAR.TabIndex = 0;
            this.txt_W_YEAR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_W_YEAR.TextType = CrplControlLibrary.TextType.Double;
            this.txt_W_YEAR.Leave += new System.EventHandler(this.txt_W_YEAR_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(308, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "TIR % :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(96, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "For The Year :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtCurrOption);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(14, 73);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(543, 59);
            this.panel2.TabIndex = 14;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(437, 6);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(89, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 19;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(383, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "Option:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label7.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(437, 31);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(89, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(395, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(185, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "TIR %  FOR IS / FAST";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(224, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Set Up";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(340, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "User Name:";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(402, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 15;
            this.lblUserName.Text = "User Name:";
            // 
            // CHRIS_Setup_TIREntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 438);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlTblBlkTwo);
            this.Controls.Add(this.pnlHead);
            this.Name = "CHRIS_Setup_TIREntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  TIR Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlTblBlkTwo, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblBlkTwo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTir)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBlkTwo;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlHead;
        private CrplControlLibrary.SLTextBox txt_W_TIR_PER;
        private CrplControlLibrary.SLTextBox txt_W_YEAR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvTir;
        private System.Windows.Forms.Panel panel2;
        public CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rank;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn Level;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdMaxAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Average;
        private System.Windows.Forms.DataGridViewTextBoxColumn Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn TRI;
    }
}