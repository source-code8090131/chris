using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;
using Microsoft.SqlServer.Management;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.Data.SqlClient;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_TestPayrollProcess : ChrisMasterDetailForm
    {
        private static Server srvSql;
        DataTable dtPayroll = new DataTable();
        //Result rslt;
        //CmnDataManager cmnDataManager = new CmnDataManager();

        //*******dtPersonnels will contain personnel records,fetched via cursor in oracle.*******
        DataTable dtPersonnels = null;
        //******* not included in current logic.*******
        DataTable dtEmpPayRollDetail = null;
        CmnDataManager cmnDM = new CmnDataManager();
        //*******for Pre-processing like term-set.*******
        Result rslt;
        //******* used for main processing.*******
        Result flowRslt;
        //*******Payroll processing status,"Failure" will abort processing.******* 
        public enum FormMode { Processing, Failure, NotStarted, Successfull, PreReqPassed };
        private FormMode processState;
        //ds used for data manipulation and management while processing. 
        Dictionary<string, object> dicEmpPayRollDetail = new Dictionary<string, object>();
               
        Dictionary<string, object> paramList = new Dictionary<string, object>();
        DataManager DM = new CommonDataManager();

        /********payroll form once entered in "Processing" mode ,will use this SQL trans,and if any
         error/exception occurs whole process will be aborted and payroll mode will be "Failure" ******/
        SqlTransaction processTrans = null;
        SqlConnection proCon = new SqlConnection();
        private static XMS.DATAOBJECTS.ConnectionBean connbean = null;
        private String GLOBAL_W_10_AMT;
        private String GLOBAL_W_INC_AMT;
        private String GLOBAL_PERKS;
        int indx = 0;
        public int MARKUP_RATE_DAYS = -1;
        public CHRIS_Setup_TestPayrollProcess()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_TestPayrollProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
     
            InitializeComponent();
            txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
            txtPersNo.Focus();
            txtRVP.Text = "40000";
            dgvSFL.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
           
            //System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection();
            //string dbName = Convert.ToString(ConfigurationManager.AppSettings["database"]);

            //string conn = "Data Source=.\\MSSQL2005;Initial Catalog=AHADTOP;User ID=sa;Password=capslock;";
            //con.ConnectionString = conn;
            //con.Open();
          
         }

         /// <summary>
         /// Muhammad Zia Ullah Baig
         /// (COM-788) CHRIS - Housing loan markup update to 365 days
         /// </summary>
         protected override bool VerifyMarkUpRate()
         {
             CmnDataManager cmnDM = new CmnDataManager();
             bool isMarkUp_Rate_Valid = false;
             isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_TESTPAYROLLPROCESS_MANAGER", ref MARKUP_RATE_DAYS);
             if (!isMarkUp_Rate_Valid)
             {
                 MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                 return false;
             }
             else
                 return true;
         }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;
            txtRVP.Text = "40000";
            this.Text = "iCORE CHRIS-Payroll Test Process";

            //System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection();
            //string dbName = Convert.ToString(ConfigurationManager.AppSettings["database"]);

            //string conn = "Data Source=.\\MSSQL2005;Initial Catalog=AHADTOP;User ID=sa;Password=capslock;";
            //con.ConnectionString = conn;
            //con.Open();



            //string connectionString = "Server=.\\MSSQL2005;Initial Catalog=AHADTOP;User ID=sa;Password=capslock";
                      
            //SqlConnection connection = new SqlConnection(); 
            //connection.ConnectionString = connectionString; 
            ////command.CommandType = CommandType.StoredProcedure;
            /////SqlCommand cmnd = new SqlCommand("", conn);
            //connection.Open();
            //MessageBox.Show("Connection Info :   " + connection.DataSource);


        }

        
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                txtNo.Text = "";
                txtProcName.Text = "";
                txtPersNo.Text = ""; 
                txtSubsLoanTAmnt.Text = "";
                txtTotalTaxAmnt.Text = "";
                txtGenStatus.Text = "";
                this.ClearForm(groupBoxMainProcess.Controls);
                txtPersNo.Focus();
                this.dgvSFL.Rows.Clear();
                this.dgvSFL.Refresh();

                this.dgvSFL.Columns[0].HeaderText = "SNO";
                this.dgvSFL.Columns[1].HeaderText = "FIELD";
                this.dgvSFL.Columns[2].HeaderText = "LOG Entry";

                txtRVP.Text = "40000";
               
            }
        }

        private void txtPersNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }


        private void txtGenStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) )
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;

            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            this.ClearForm(groupBoxMainProcess.Controls); 


        }

        private void txtGenStatus_Leave(object sender, EventArgs e)
        {
            
            if (this.txtGenStatus.Text == "YES" && txtPersNo.Text !="")
            {
                indx = 0;   
                PreProcessing();
               
            }
            else if (this.txtGenStatus.Text == "YES" && txtPersNo.Text == "")
            {
                txtNo.Text = "";
                txtProcName.Text = "";
                txtSubsLoanTAmnt.Text = "";
                txtTotalTaxAmnt.Text = ""; 
                this.dgvSFL.Rows[0].Cells[0].Value = "END";
                this.dgvSFL.Rows[0].Cells[1].Value = "END";
                this.dgvSFL.Rows[0].Cells[2].Value = "PAYROLL PROCESS COMPLETED SUCCESSFULLY";
        
            }
            else if (this.txtGenStatus.Text == "NO")
            {
                this.Close();

            }


        }


        private void InitializeProcessingFlow()
        {
            PreProcessing();
        }

        private void SetFormInInitialState()
        {
            txtRVP.Text= "40,000";
            
            groupBoxProcessCompleted.Visible = false;
            groupBoxProcessRunning.Visible = false;

            txtPersNo.Text = "";
            txtProcName.Text = "";
            txtSubsLoanTAmnt.Text = "";
            txtNo.Text = "";
            txtTotalTaxAmnt.Text = "";
            slTB_Pr_P_No.Text = string.Empty;     
            this.processState = FormMode.NotStarted;
        }


        private void TermSet()
        {
            //*************will check if processing is going on another terminal.************
            
            //CHRIS_SP_TERM_MANAGER
            rslt = cmnDM.Get("CHRIS_SP_TESTPAYROLLPROCESS_MANAGER", "GetTermStatus");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                {
                    this.rslt = cmnDM.Get("CHRIS_SP_TESTPAYROLLPROCESS_MANAGER", "UpdateToProcess");
                    if (this.rslt.isSuccessful)
                    {
                        this.processState = FormMode.PreReqPassed;
                        SetFormInProcessingState();
                    }

                }
                else
                {
                    this.processState = FormMode.Failure;
                    MessageBox.Show("Processing Is In Progress On Other Terminal .......!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                this.processState = FormMode.Failure;
                MessageBox.Show("Processing Is In Progress On Other Terminal .......!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

        }

        private void StartProcessing()
        {
            GetAllEmployees();
            InitializeList();
            this.processState = FormMode.Processing;
            InitiateProcess();  //BIG FISH//
            if (processState == FormMode.Failure)
            {

            }


        }

        private void InitializeList()
        {
            dicEmpPayRollDetail.Clear();
            dicEmpPayRollDetail.Add("PR_P_NO", null);
            dicEmpPayRollDetail.Add("W_AN_PACK", null);
            dicEmpPayRollDetail.Add("W_CATE", null);
            dicEmpPayRollDetail.Add("W_CONF_FLAG", null);
            dicEmpPayRollDetail.Add("W_DESIG", null);
            dicEmpPayRollDetail.Add("W_LEVEL", null);
            dicEmpPayRollDetail.Add("W_ZAKAT", null);
            dicEmpPayRollDetail.Add("W_PRV_INC", null);
            dicEmpPayRollDetail.Add("W_PRV_TAX_PD", null);
            dicEmpPayRollDetail.Add("W_TAX_USED", null);
            dicEmpPayRollDetail.Add("W_TRANS_DATE", null);
            dicEmpPayRollDetail.Add("W_PR_MONTH", null);
            dicEmpPayRollDetail.Add("W_JOINING_DATE", null);
            dicEmpPayRollDetail.Add("W_BRANCH", null);
            dicEmpPayRollDetail.Add("W_PR_DATE", null);
            dicEmpPayRollDetail.Add("W_REFUND", null);
            dicEmpPayRollDetail.Add("W_ATT_FLAG", null);
            dicEmpPayRollDetail.Add("W_PAY_FLAG", null);
            dicEmpPayRollDetail.Add("W_CONFIRM_ON", null);


            dicEmpPayRollDetail.Add("W_ASR", 0.00);
            dicEmpPayRollDetail.Add("W_HRENT", 0.00);
            dicEmpPayRollDetail.Add("W_CONV", 0.00);
            dicEmpPayRollDetail.Add("W_GRAT", 0.00);
            dicEmpPayRollDetail.Add("W_GOVT_ALL", 0.00);
            dicEmpPayRollDetail.Add("MDAYS", 0.00);
            dicEmpPayRollDetail.Add("W_DAYS", 0.00);
            dicEmpPayRollDetail.Add("W_DIVISOR", 0.00);
            dicEmpPayRollDetail.Add("WW_BASIC", 0.00);
            dicEmpPayRollDetail.Add("WW_CONV", 0.00);
            dicEmpPayRollDetail.Add("WW_HOUSE", 0.00);
            dicEmpPayRollDetail.Add("W_FLAG", null);
            dicEmpPayRollDetail.Add("W_DEPT_FT", null);
            dicEmpPayRollDetail.Add("W_SW_PAY", 1.00);
            dicEmpPayRollDetail.Add("WW_DATE", null);
            dicEmpPayRollDetail.Add("W_SAL_ADV", 0.00);
            dicEmpPayRollDetail.Add("W_PROMOTED", null);
            dicEmpPayRollDetail.Add("W_DATE", null);
            dicEmpPayRollDetail.Add("W_PFUND", 0.00);
            dicEmpPayRollDetail.Add("GLOBAL_FLAG", null);
            //dicEmpPayRollDetail.Add("W_PR_MONTH", null);
            dicEmpPayRollDetail.Add("GLOBAL_PERKS", this.GLOBAL_PERKS);
            dicEmpPayRollDetail.Add("W_OT_ASR", 0.00);
            //proc_6
            dicEmpPayRollDetail.Add("W_PA_DATE", null);
            dicEmpPayRollDetail.Add("W_INC_MONTHS", 0.00);
            dicEmpPayRollDetail.Add("W_BASIC_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_HOUSE_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_CONV_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_PF_AREAR", 0.00);
            dicEmpPayRollDetail.Add("GLOBAL_W_FOUND", null);
            //Sal_advance
            dicEmpPayRollDetail.Add("W_WORK_DAY", null);
            //dicEmpPayRollDetail.Add("W_SAL_ADV", 0.00);
            //FIN BOOK
            dicEmpPayRollDetail.Add("W_MARKUP_L", 0.00);
            dicEmpPayRollDetail.Add("W_INST_L", 0.00);
            dicEmpPayRollDetail.Add("W_MARKUP_F", 0.00);
            dicEmpPayRollDetail.Add("W_INST_F", 0.00);
            dicEmpPayRollDetail.Add("W_EXCISE", 0.00);
            dicEmpPayRollDetail.Add("W_FINANCE", null);
            dicEmpPayRollDetail.Add("W_MONTH_DED", 0.00);
            dicEmpPayRollDetail.Add("W_EXTRA", null);
            dicEmpPayRollDetail.Add("W_ITAX", 0.00);

            dicEmpPayRollDetail.Add("WPER_DAY", null);

            // Z_TAX
            //dicEmpPayRollDetail.Add("W_PF_EXEMPT", slTB_PF_Exempt.Text);
            dicEmpPayRollDetail.Add("W_VP",txtRVP.Text);
            dicEmpPayRollDetail.Add("W_SYS", dtpProcDate.Value);
            dicEmpPayRollDetail.Add("GLOBAL_W_10_AMT", this.GLOBAL_W_10_AMT);
            dicEmpPayRollDetail.Add("GLOBAL_W_INC_AMT", this.GLOBAL_W_INC_AMT);
            dicEmpPayRollDetail.Add("SUBS_LOAN_TAX_AMT", 0.00);
            dicEmpPayRollDetail.Add("Z_TAXABLE_AMOUNT", 0.00);
            
            dicEmpPayRollDetail.Add("LOG_NO", null);
            dicEmpPayRollDetail.Add("LOG_FLD", null);
            dicEmpPayRollDetail.Add("LOG_ENTRY", null);
            

        }



        private void GetAllEmployees()
        {
            //fetch all employees with condition described in cursor emp_no(oracle form,Valid_Pno). 
            ////CHRIS_SP_GET_PERSONNEL_INFO
            rslt = cmnDM.GetData("CHRIS_SP_GET_PAYROLLTESTPERSONNEL_INFO", "");

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    dtPersonnels = rslt.dstResult.Tables[0];
                }
            }

        }


        private bool IsPayRollAlreadyGenerated()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Clear();
            param.Add("W_DATE", dtpProcDate.Value);
            //CHRIS_SP_TESTPAYROLLPROCESS_MANAGER
            //CHRIS_SP_TERM_MANAGER
            rslt = cmnDM.GetData("CHRIS_SP_TESTPAYROLLPROCESS_MANAGER", "IsPayRollGenerated", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                    this.processState = FormMode.PreReqPassed;
                    return false;
                }
                else
                {
                    this.processState = FormMode.Failure;
                    DateTime dt = Convert.ToDateTime(dtpProcDate.Value);
                    /////MessageBox.Show("PAYROLL HAS ALREADY BEEN GENERATED FOR " + dt.ToString("MMM-yyyy").ToUpper(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return true;
                }
            }
            return true;
        }

        private void PreProcessing()
        {
            if (!IsPayRollAlreadyGenerated())
            {
                TermSet();
                if (this.processState == FormMode.PreReqPassed)
                {
                     StartProcessing();
                }
            }
            else
            {
                SetFormInInitialState();
            }
        }



        private void InitiateProcess()
        {
            try
            {
                connbean = SQLManager.GetConnectionBean();
                if (connbean != null)
                {
                    proCon = connbean.getDatabaseConnection();
                    processTrans = proCon.BeginTransaction();
                    int personnel_Cursor = 0;

                    DateTime dt = Convert.ToDateTime(dtpProcDate.Value);
                    dicEmpPayRollDetail["W_DATE"] = dt;
                    if (dtPersonnels != null && processState == FormMode.Processing)
                    {
                        for (int rowPersonnelNum = 0; rowPersonnelNum < dtPersonnels.Rows.Count; rowPersonnelNum++)
                        {
                            RefreshProgressControls();

                            slTB_Pr_P_No.Text = dtPersonnels.Rows[rowPersonnelNum]["PR_P_NO"].ToString();
                            InitializeList();
                            FillPayRollDataWithPersonnelBasicInfo(rowPersonnelNum);//(rowPersonnelNum);
                            dicEmpPayRollDetail["W_DATE"] = dt;
                            #region promotion_proc
                            //********************calling procedure to check promotion****************************
                            txtNo.Text = "";
                            paramList.Clear();
                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                            paramList.Add("W_PR_DATE", dicEmpPayRollDetail["W_PR_DATE"]);
                            paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                            paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                            paramList.Add("W_PROMOTED", dicEmpPayRollDetail["W_PROMOTED"]);
                           
                            ////CHRIS_SP_PROMOTION_PROC
                            flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTPROMOTION_PROC", "", paramList, ref processTrans);
                            SetStatusBar("Processing in Promotion (promotion_proc)");
                            if (flowRslt.isSuccessful)
                            {
                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables.Count > 0 && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    FillListWithOutputValues();
                                }
                            }
                            #endregion

                            #region Valid P_No Part 1

                            String prCategory;
                            Double prNewAnnualPack;
                            Double wAsr;
                            Double wHrent;
                            Double wConv;
                            Double wGrat;

                            prCategory = (dicEmpPayRollDetail["W_CATE"] == null || dicEmpPayRollDetail["W_CATE"].ToString().Equals(String.Empty)) ? String.Empty : dicEmpPayRollDetail["W_CATE"].ToString();
                            prNewAnnualPack = (dicEmpPayRollDetail["W_AN_PACK"] == null || dicEmpPayRollDetail["W_AN_PACK"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_AN_PACK"].ToString());

                            if (prCategory.ToUpper().Equals("C"))
                            {
                                dicEmpPayRollDetail["W_ASR"] = prNewAnnualPack / 12;
                                #region Govt_allowance

                                if (flowRslt.isSuccessful && processState == FormMode.Processing)
                                {
                                    //********************calling procedure for govt. allowance amount ------**********
                                    txtNo.Text = "";
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_GRAT", dicEmpPayRollDetail["W_GRAT"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    
                                    ////CHRIS_SP_GOVT_ALL_AND_ALLOWANCES
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTGOVT_ALL_AND_ALLOWANCES", "", paramList, ref processTrans);
                                    SetStatusBar("Processing in Govt. Allowance (govt_all)");
                                    RefreshProgressControls();
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                }
                                #endregion
                                dicEmpPayRollDetail["W_HRENT"] = 0;
                                dicEmpPayRollDetail["W_CONV"] = 0;
                                wAsr = (dicEmpPayRollDetail["W_ASR"] == null || dicEmpPayRollDetail["W_ASR"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_ASR"].ToString());
                                dicEmpPayRollDetail["W_GRAT"] = wAsr / 12;
                            }
                            else
                            {
                                dicEmpPayRollDetail["W_ASR"] = ((prNewAnnualPack / 150.00) * 100.00) / 12.00;
                                dicEmpPayRollDetail["W_HRENT"] = ((prNewAnnualPack / 150.00) * 40.00) / 12.00;

                                dicEmpPayRollDetail["W_CONV"] = ((prNewAnnualPack / 150.00) * 10.00) / 12.00;
                                dicEmpPayRollDetail["W_GRAT"] = ((prNewAnnualPack / 150.00) * 100.00 * 1.30) / 12.00;
                                //------MAKE G V FOR PKS

                                wAsr = (dicEmpPayRollDetail["W_ASR"] == null || dicEmpPayRollDetail["W_ASR"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_ASR"].ToString());
                                wHrent = (dicEmpPayRollDetail["W_HRENT"] == null || dicEmpPayRollDetail["W_HRENT"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_HRENT"].ToString());
                                wConv = (dicEmpPayRollDetail["W_CONV"] == null || dicEmpPayRollDetail["W_CONV"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_CONV"].ToString());
                                wGrat = (dicEmpPayRollDetail["W_GRAT"] == null || dicEmpPayRollDetail["W_GRAT"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_GRAT"].ToString());
                                dicEmpPayRollDetail["GLOBAL_PERKS"] = wAsr + wHrent + wConv + wGrat;
                            }
                            #endregion


                            #region Provident Fund
                            if (flowRslt.isSuccessful)
                            {
                                //--************* Provident Fund Calculation **************************--
                                txtNo.Text = "5000412";
                                paramList.Clear();
                                paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_CONFIRM_ON", dicEmpPayRollDetail["W_CONFIRM_ON"]);
                                paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                ////CHRIS_SP_PROVIDENT_FUND 
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTEST_PROVIDENT_FUND", "", paramList, ref processTrans);
                                SetStatusBar("Calculating P.F. (Valid_pno)");
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }

                                }
                                if (Convert.ToString(dicEmpPayRollDetail["LOG_NO"]) != "")
                                {
                                    dgvSFL.Rows.Add();
                                    dgvSFL.Rows[indx].Cells[0].Value = dicEmpPayRollDetail["LOG_NO"].ToString();
                                    dgvSFL.Rows[indx].Cells[1].Value = dicEmpPayRollDetail["LOG_FLD"].ToString();
                                    dgvSFL.Rows[indx].Cells[2].Value = dicEmpPayRollDetail["LOG_ENTRY"].ToString();
                                    indx++;

                                }

                            }
                            #endregion

                            #region Proc Shift
                            if (flowRslt.isSuccessful)
                            {
                                //--************* shift PROC FOR ALLOWANCE TABLE UPDATION**************************--
                                txtNo.Text = "5000416";
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                ////CHRIS_SP_PROC_SHIFT
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTEST_PROC_SHIFT", "", paramList, ref processTrans);
                                SetStatusBar("Processing in Allowance (proc_shift)");
                                RefreshProgressControls();
                                if (flowRslt.isSuccessful)
                                {//if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    //{
                                    //    FillListWithOutputValues();
                                    //}
                                }
                            }


                            #endregion

                            #region Overtime & inc_promo_check
                            if (flowRslt.isSuccessful)
                            {
                                //--************* -- check to calculate overtime for ctt to officers --  *******************--

                                DateTime pr_date, temp_w_date;
                                if (dicEmpPayRollDetail["W_PR_DATE"] != null && dicEmpPayRollDetail["W_PR_DATE"].ToString() != string.Empty)
                                {
                                    pr_date = Convert.ToDateTime(dicEmpPayRollDetail["W_PR_DATE"]);
                                    temp_w_date = Convert.ToDateTime(dicEmpPayRollDetail["W_DATE"]);
                                    if (pr_date.Month == temp_w_date.Month && pr_date.Year == temp_w_date.Year)
                                    {
                                        //--  flag up because inc_promo_check calls forcasted procedure to --********
                                        //txtNo.Text = "";
                                        dicEmpPayRollDetail["GLOBAL_FLAG"] = "T";
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("GLOBAL_FLAG", dicEmpPayRollDetail["GLOBAL_FLAG"]);
                                        paramList.Add("W_ATT_FLAG", dicEmpPayRollDetail["W_ATT_FLAG"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                        paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                        paramList.Add("W_PR_DATE", dicEmpPayRollDetail["W_PR_DATE"]);
                                        paramList.Add("W_PROMOTED", dicEmpPayRollDetail["W_PROMOTED"]);
                                        paramList.Add("W_OT_ASR", dicEmpPayRollDetail["W_OT_ASR"]);
                                        paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                        
                                        ////CHRIS_SP_INC_PROMO_CHECK
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTINC_PROMO_CHECK", "", paramList, ref processTrans);
                                        SetStatusBar("Calculate OT For Ctt to Officers (inc_promo_check)");
                                        RefreshProgressControls();
                                        if (rslt.isSuccessful)
                                        {
                                            if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                            {
                                                FillListWithOutputValues();
                                            }
                                        }

                                    }
                                    else
                                    {
                                        //****************-- calculate ot for regular --*******************
                                        //txtNo.Text = "";
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                        paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                        paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);

                                        
                                        ////CHRIS_SP_OVER_TIME
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTOVER_TIME", "", paramList, ref processTrans);
                                        SetStatusBar("Processing in Overtime (over_time)");
                                        RefreshProgressControls();
                                        if (flowRslt.isSuccessful)
                                        {
                                            if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                            {
                                                FillListWithOutputValues();
                                            }
                                        }

                                    }
                                }
                                else
                                {
                                    //****************-- calculate ot for regular --*******************
                                    //txtNo.Text = "";
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                    ////CHRIS_SP_OVER_TIME
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTOVER_TIME", "", paramList, ref processTrans);
                                    SetStatusBar("Processing in Overtime (over_time)");
                                    RefreshProgressControls();
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }

                                }

                            }
                            #endregion

                            #region Proc_allow

                            if (flowRslt.isSuccessful)
                            {
                                if (dicEmpPayRollDetail["W_PR_MONTH"] != null && dicEmpPayRollDetail["W_PR_MONTH"].ToString() != string.Empty)
                                {
                                    int temp_month = Convert.ToInt32(dicEmpPayRollDetail["W_PR_MONTH"]);
                                    if (temp_month == 12 && dt.Month == 1) //i.e. W_date.monthName =="JAN"
                                    {
                                        txtNo.Text = "50002";
                                        SetStatusBar("Processing in Yearly Award (proc_allow)");
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                        paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                        RefreshProgressControls();
                                        
                                        ////CHRIS_SP_PROC_ALLOW
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTPROC_ALLOW", "", paramList, ref processTrans);

                                    }
                                }


                            }

                            #endregion

                            #region att_award
                            if (flowRslt.isSuccessful)
                            {
                                if (dicEmpPayRollDetail["W_CATE"] != null && dicEmpPayRollDetail["W_CATE"].ToString() != string.Empty)
                                {
                                    if (dicEmpPayRollDetail["W_CATE"].ToString().Equals("C"))
                                    {
                                        txtNo.Text = "50007";
                                        SetStatusBar("Processing in Att. Award (att_award)");
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_ATT_FLAG", dicEmpPayRollDetail["W_ATT_FLAG"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                        
                                        ////CHRIS_SP_ATT_AWARD
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTATT_AWARD", "", paramList, ref processTrans);
                                        RefreshProgressControls();
                                        if (flowRslt.isSuccessful)
                                        {
                                            if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                            {
                                                FillListWithOutputValues();
                                            }
                                        }

                                    }
                                }
                            }
                            #endregion

                            #region proc_1

                            if (flowRslt.isSuccessful)
                            {
                                //Processing for Last Payroll Date (proc_1)
                                txtNo.Text = "500016";
                                SetStatusBar("Processing for Last Payroll Date (proc_1)");
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_SW_PAY", dicEmpPayRollDetail["W_SW_PAY"]);
                                paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                paramList.Add("W_DIVISOR", dicEmpPayRollDetail["W_DIVISOR"]);
                                paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                paramList.Add("WW_BASIC", dicEmpPayRollDetail["WW_BASIC"]);
                                paramList.Add("WW_CONV", dicEmpPayRollDetail["WW_CONV"]);
                                paramList.Add("WW_HOUSE", dicEmpPayRollDetail["WW_HOUSE"]);
                                RefreshProgressControls();


                                
                                ////CHRIS_SP_PROC_1
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTPROC_1", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }


                            }
                            #endregion

                            #region Proc_6
                            if (flowRslt.isSuccessful)
                            {
                                //-- TO CALCULATE BACK DATED SALARY AREARS --
                                txtNo.Text = "500018";
                                SetStatusBar("Processing in Salary Arears (proc_6)");
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                paramList.Add("W_PA_DATE", dicEmpPayRollDetail["W_PA_DATE"]);
                                paramList.Add("W_INC_MONTHS", dicEmpPayRollDetail["W_INC_MONTHS"]);
                                paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                paramList.Add("W_BASIC_AREAR", dicEmpPayRollDetail["W_BASIC_AREAR"]);
                                paramList.Add("W_HOUSE_AREAR", dicEmpPayRollDetail["W_HOUSE_AREAR"]);
                                paramList.Add("W_CONV_AREAR", dicEmpPayRollDetail["W_CONV_AREAR"]);
                                paramList.Add("W_PF_AREAR", dicEmpPayRollDetail["W_PF_AREAR"]);
                                paramList.Add("GLOBAL_W_FOUND", dicEmpPayRollDetail["GLOBAL_W_FOUND"]);
                                RefreshProgressControls();

                                
                                ////CHRIS_SP_PROC_6
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTPROC_6", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables.Count > 0 && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }


                            }
                            #endregion

                            #region Pro-Rate Income Or Income-Tax

                            if (flowRslt.isSuccessful)
                            {
                                //--*********** TO PICK ALLOWANCES IN CASE OF LAST JOIN AND JOIN B/W THE MONTH*********************--

                                if (dicEmpPayRollDetail["W_FLAG"] != null && dicEmpPayRollDetail["W_FLAG"].ToString() != string.Empty)
                                {
                                    #region Pro-rate Income
                                    if (dicEmpPayRollDetail["W_FLAG"].ToString() == "L" || dicEmpPayRollDetail["W_FLAG"].ToString() == "J")
                                    {
                                        //--  ****Processing in Allowance for L,J (pro_rate_income)****
                                        txtNo.Text = "500021";
                                        SetStatusBar("Processing in Allowance for L,J (pro_rate_income)");
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                        paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                        paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                        paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                        paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                        paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                        paramList.Add("W_DIVISOR", dicEmpPayRollDetail["W_DIVISOR"]);
                                        paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                        RefreshProgressControls();

                                        
                                        ////CHRIS_SP_PRO_RATE_INCOME
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTPRO_RATE_INCOME", "", paramList, ref processTrans);

                                        //if (rslt.isSuccessful)
                                        //{if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        //    {FillListWithOutputValues();}}

                                    }
                                    #endregion

                                    #region Income-Tax
                                    else
                                    {
                                        //**************Processing in Allowance (income_tax)*********************
                                        SetStatusBar("Processing in Allowance (income_tax)");
                                        txtNo.Text = "500023";
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                        paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                        paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                        RefreshProgressControls();

                                        
                                        ////CHRIS_SP_INCOME_TAX
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTINCOME_TAX", "", paramList, ref processTrans);
                                    }
                                    #endregion
                                }

                            }

                            #endregion

                            #region Salary_advance

                            if (flowRslt.isSuccessful)
                            {
                                //-- SALARY ADVANCE IF GIVEN BEFORE PAYROLL--
                                txtNo.Text = "5000261";
                                SetStatusBar("Processing in Salary Advance(salary_advance)");
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_SAL_ADV", dicEmpPayRollDetail["W_SAL_ADV"]);
                                RefreshProgressControls();

                                
                                ////CHRIS_SP_SALARY_ADVANCE
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTSALARY_ADVANCE", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }


                            }
                            #endregion

                            #region Up Deduction

                            if (flowRslt.isSuccessful)
                            {
                                //-- TO PICK DEDUCTION--
                                txtNo.Text = "5000271";
                                SetStatusBar("Processing in Deduction (up_deduction)");
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                RefreshProgressControls();

                                
                                ////CHRIS_SP_UP_DEDUCTION
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTUP_DEDUCTION", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    //if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    //{
                                    //    FillListWithOutputValues();
                                    //}
                                }
                            }

                            #endregion

                            #region Fin-Book

                            if (flowRslt.isSuccessful)
                            {
                                //-- TO CALCULATE MONTH INSTALLMENT AND CALL PAYROLLTESTFIN_CALC
                                txtNo.Text = "500029";
                                SetStatusBar("Processing in Finance (fin_book)");
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                paramList.Add("W_MARKUP_L", dicEmpPayRollDetail["W_MARKUP_L"]);
                                paramList.Add("W_INST_L", dicEmpPayRollDetail["W_INST_L"]);
                                paramList.Add("W_MARKUP_F", dicEmpPayRollDetail["W_MARKUP_F"]);
                                paramList.Add("W_INST_F", dicEmpPayRollDetail["W_INST_F"]);
                                paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                paramList.Add("W_FINANCE", dicEmpPayRollDetail["W_FINANCE"]);
                                paramList.Add("W_MONTH_DED", dicEmpPayRollDetail["W_MONTH_DED"]);
                                paramList.Add("W_EXTRA", dicEmpPayRollDetail["W_EXTRA"]);
                                RefreshProgressControls();

                                ////CHRIS_SP_FIN_BOOK                                
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTESTFIN_BOOK", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }

                                if (Convert.ToString(dicEmpPayRollDetail["LOG_NO"]) != "")
                                {
                                    dgvSFL.Rows.Add();
                                    dgvSFL.Rows[indx].Cells[0].Value = dicEmpPayRollDetail["LOG_NO"].ToString();
                                    dgvSFL.Rows[indx].Cells[1].Value = dicEmpPayRollDetail["LOG_FLD"].ToString();
                                    dgvSFL.Rows[indx].Cells[2].Value = dicEmpPayRollDetail["LOG_ENTRY"].ToString();
                                    indx++;

                                }

                            }
                            #endregion

                            #region Valid PNo. Calculations

                            if (flowRslt.isSuccessful)
                            {
                                //-- TO CALCULATE BACK DATED SALARY AREARS --
                                txtNo.Text = "500030";
                                SetStatusBar("Processing in P.F. Arears (Valid_pno)");
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                paramList.Add("W_CONFIRM_ON", dicEmpPayRollDetail["W_CONFIRM_ON"]);
                                paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                paramList.Add("WW_BASIC", dicEmpPayRollDetail["WW_BASIC"]);
                                paramList.Add("WW_CONV", dicEmpPayRollDetail["WW_CONV"]);
                                paramList.Add("WW_HOUSE", dicEmpPayRollDetail["WW_HOUSE"]);
                                paramList.Add("W_BASIC_AREAR", dicEmpPayRollDetail["W_BASIC_AREAR"]);
                                paramList.Add("W_HOUSE_AREAR", dicEmpPayRollDetail["W_HOUSE_AREAR"]);
                                paramList.Add("W_CONV_AREAR", dicEmpPayRollDetail["W_CONV_AREAR"]);
                                paramList.Add("W_PF_AREAR", dicEmpPayRollDetail["W_PF_AREAR"]);
                                paramList.Add("GLOBAL_W_FOUND", dicEmpPayRollDetail["GLOBAL_W_FOUND"]);
                                paramList.Add("MDAYS", dicEmpPayRollDetail["MDAYS"]);
                                paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                paramList.Add("WPER_DAY", dicEmpPayRollDetail["WPER_DAY"]);
                                paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                paramList.Add("W_WORK_DAY", dicEmpPayRollDetail["W_WORK_DAY"]);
                                RefreshProgressControls();

                                ////CHRIS_SP_PAYROLL_CALCULATIONS 
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTEST_CALCULATIONS", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }


                            }


                            #endregion

                            #region Z_TAX
                            
                            if (flowRslt.isSuccessful)
                            {
                                //--*********** inserting record in payroll table ******** --
                                txtNo.Text = "500034";
                                SetStatusBar("Processing Processing in Tax (z_tax)");
                                paramList.Clear();

                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                paramList.Add("W_ZAKAT", dicEmpPayRollDetail["W_ZAKAT"]);
                                paramList.Add("W_PRV_INC", dicEmpPayRollDetail["W_PRV_INC"]);
                                paramList.Add("W_TAX_USED", dicEmpPayRollDetail["W_TAX_USED"]);
                                paramList.Add("W_REFUND", dicEmpPayRollDetail["W_REFUND"]);
                                paramList.Add("W_PRV_TAX_PD", dicEmpPayRollDetail["W_PRV_TAX_PD"]);
                                paramList.Add("W_ITAX", dicEmpPayRollDetail["W_ITAX"]);
                                //paramList.Add("W_PF_EXEMPT", dicEmpPayRollDetail["W_PF_EXEMPT"]);
                                paramList.Add("W_SYS", dicEmpPayRollDetail["W_SYS"]);
                                paramList.Add("GLOBAL_W_10_AMT", dicEmpPayRollDetail["GLOBAL_W_10_AMT"]);
                                paramList.Add("GLOBAL_W_INC_AMT", dicEmpPayRollDetail["GLOBAL_W_INC_AMT"]);
                                paramList.Add("W_VP", dicEmpPayRollDetail["W_VP"]);

                                RefreshProgressControls();

                                ////CHRIS_SP_PAYROLLTEST_GENERATION_Z_TAX
                                ////CHRIS_SP_PAYROLL_GENERATION_Z_TAX
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTEST_GENERATION_Z_TAX", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }

                                }
                                txtTotalTaxAmnt.Text = dicEmpPayRollDetail["Z_TAXABLE_AMOUNT"].ToString();
                                txtSubsLoanTAmnt.Text = dicEmpPayRollDetail["SUBS_LOAN_TAX_AMT"].ToString();
                                //POPULATING THE GRID//

                                if (Convert.ToString(dicEmpPayRollDetail["LOG_NO"]) != "")
                                {
                                    dgvSFL.Rows.Add();
                                    dgvSFL.Rows[indx].Cells[0].Value = dicEmpPayRollDetail["LOG_NO"].ToString();
                                    dgvSFL.Rows[indx].Cells[1].Value = dicEmpPayRollDetail["LOG_FLD"].ToString();
                                    dgvSFL.Rows[indx].Cells[2].Value = dicEmpPayRollDetail["LOG_ENTRY"].ToString();
                                    indx++;
                                    
                                }
                            }
                            #endregion

                            #region Payroll_insertions

                            if (flowRslt.isSuccessful)
                            {
                                //--*********** inserting record in payroll table ******** --
                                txtNo.Text = "500033";
                                SetStatusBar("Processing in P.F. Arears (Valid_pno)");

                                Double W_ASR;
                                Double W_HRENT;
                                Double W_CONV;
                                Double W_PFUND;
                                Double W_GRAT;
                                Double W_ITAX;
                                Double W_SAL_ADV;
                                Double W_MARKUP_F;
                                Double W_INST_F;
                                Double W_MARKUP_L;
                                Double W_INST_L;
                                Double W_EXCISE;
                                Double W_ZAKAT;

                                if (Double.TryParse(dicEmpPayRollDetail["W_ASR"].ToString(), out W_ASR))
                                    dicEmpPayRollDetail["W_ASR"] = Math.Round(W_ASR, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_HRENT"].ToString(), out W_HRENT))
                                    dicEmpPayRollDetail["W_HRENT"] = Math.Round(W_HRENT, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_CONV"].ToString(), out W_CONV))
                                    dicEmpPayRollDetail["W_CONV"] = Math.Round(W_CONV, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_PFUND"].ToString(), out W_PFUND))
                                    dicEmpPayRollDetail["W_PFUND"] = Math.Round(W_PFUND, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_GRAT"].ToString(), out W_GRAT))
                                    dicEmpPayRollDetail["W_GRAT"] = Math.Round(W_GRAT, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_ITAX"].ToString(), out W_ITAX))
                                    dicEmpPayRollDetail["W_ITAX"] = Math.Round(W_ITAX, 0);

                                if (Double.TryParse(dicEmpPayRollDetail["W_SAL_ADV"].ToString(), out W_SAL_ADV))
                                    dicEmpPayRollDetail["W_SAL_ADV"] = Math.Round(W_SAL_ADV, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_MARKUP_F"].ToString(), out W_MARKUP_F))
                                    dicEmpPayRollDetail["W_MARKUP_F"] = Math.Round(W_MARKUP_F, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_INST_F"].ToString(), out W_INST_F))
                                    dicEmpPayRollDetail["W_INST_F"] = Math.Round(W_INST_F, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_MARKUP_L"].ToString(), out W_MARKUP_L))
                                    dicEmpPayRollDetail["W_MARKUP_L"] = Math.Round(W_MARKUP_L, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_INST_L"].ToString(), out W_INST_L))
                                    dicEmpPayRollDetail["W_INST_L"] = Math.Round(W_INST_L, 0);

                                if (Double.TryParse(dicEmpPayRollDetail["W_EXCISE"].ToString(), out W_EXCISE))
                                    dicEmpPayRollDetail["W_EXCISE"] = Math.Round(W_EXCISE, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_ZAKAT"].ToString(), out W_ZAKAT))
                                    dicEmpPayRollDetail["W_ZAKAT"] = Math.Round(W_ZAKAT, 2);

                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                paramList.Add("W_WORK_DAY", dicEmpPayRollDetail["W_WORK_DAY"]);
                                paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                paramList.Add("W_GRAT", dicEmpPayRollDetail["W_GRAT"]);
                                paramList.Add("W_ITAX", dicEmpPayRollDetail["W_ITAX"]);
                                paramList.Add("W_SAL_ADV", dicEmpPayRollDetail["W_SAL_ADV"]);
                                paramList.Add("W_MARKUP_F", dicEmpPayRollDetail["W_MARKUP_F"]);
                                paramList.Add("W_INST_F", dicEmpPayRollDetail["W_INST_F"]);
                                paramList.Add("W_MARKUP_L", dicEmpPayRollDetail["W_MARKUP_L"]);
                                paramList.Add("W_INST_L", dicEmpPayRollDetail["W_INST_L"]);
                                paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                paramList.Add("W_ZAKAT", dicEmpPayRollDetail["W_ZAKAT"]);
                                paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                RefreshProgressControls();

                                ////CHRIS_SP_PAYROLL_INSERTIONS
                                flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLLTEST_INSERTIONS", "", paramList, ref processTrans);
                                if (flowRslt.isSuccessful)
                                {
                                    
                                }

                            }
                            #endregion


                            #region If flowRslt is False
                            else if (!flowRslt.isSuccessful)
                            {
                                processState = FormMode.Failure;
                                if (processTrans != null && processTrans.Connection != null)
                                    processTrans.Rollback();
                                //if (conn.State != ConnectionState.Closed)
                                //    conn.Close();
                                base.LogError(this.Name, "InitiateProcess()", flowRslt.exp);
                                MessageBox.Show(flowRslt.exp.Message, "Error during Payroll Generation", MessageBoxButtons.OK);
                                return;// break;
                            }
                            #endregion


                        }
                        if (processState != FormMode.Failure)
                            processState = FormMode.Successfull;
                    }
                    else
                        processState = FormMode.Failure;

                    if (processState == FormMode.Failure)
                    {
                        MessageBox.Show("Payroll cannot be generated.Check Log for Error Details", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        SetFormInInitialState();

                    }
                    else if (processState == FormMode.Successfull)
                    {
                        groupBoxProcessCompleted.Visible = true;
                        groupBoxProcessRunning.Enabled = false;
                        groupBoxMainProcess.Enabled = false;
                        
                        
                        ////slTB_FinalAnswer.Focus();
                        ////when done then TermSet_Clear();
                    }
                }

            }
            catch (Exception dbEcxep)
            {
                processState = FormMode.Failure;
                if (processTrans != null && processTrans.Connection != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
                base.LogException(this.Name, "InitiateProcess()", dbEcxep);
            }
            finally
            {
                //slTB_Reply_For_Generation.Text = String.Empty;
            }

        }

        /// <summary>
        /// Fill DS with Updated Values.
        /// </summary>
        private void FillListWithOutputValues()
        {
            try
            {
                for (int tableIndex = 0; tableIndex < flowRslt.dstResult.Tables.Count; tableIndex++)
                {
                    for (int outVarCount = 0; outVarCount < flowRslt.dstResult.Tables[tableIndex].Columns.Count; outVarCount++)
                    {
                        if (dicEmpPayRollDetail.ContainsKey(flowRslt.dstResult.Tables[tableIndex].Columns[outVarCount].ColumnName))
                        {
                            dicEmpPayRollDetail[flowRslt.dstResult.Tables[tableIndex].Columns[outVarCount].ColumnName] = flowRslt.dstResult.Tables[tableIndex].Rows[0][outVarCount];
                        }
                    }
                }
            }
            catch (Exception fillValue)
            {
                processState = FormMode.Failure;
                base.LogException(this.Name, "FillListWithOutputValues", fillValue);
            }
        }


        private void RefreshProgressControls()
        {
            this.Refresh();
            slTB_Pr_P_No.Refresh();
            txtProcName.Refresh();
            //labelStatusBar.Refresh();
            //txtPersNo.Refresh();  
            //txtTotalTaxAmnt.Refresh();
            //txtSubsLoanTAmnt.Refresh();
        }



        private void FillPayRollDataWithPersonnelBasicInfo(int pRow)
        {
            try
            {

                for (int pCol = 0; pCol < dtPersonnels.Columns.Count; pCol++)
                {
                    if (dicEmpPayRollDetail.ContainsKey(dtPersonnels.Columns[pCol].ColumnName))
                    {
                        if (dtPersonnels.Columns[pCol].ColumnName.ToUpper() != "W_DATE")
                            dicEmpPayRollDetail[dtPersonnels.Columns[pCol].ColumnName] = dtPersonnels.Rows[pRow][pCol];
                    }
                    //if (dtEmpPayRollDetail.Columns.Contains(dtPersonnels.Columns[pCol].ColumnName))
                    //    {dtEmpPayRollDetail.Rows[pRow][dtPersonnels.Columns[pCol].ColumnName] = dtPersonnels.Rows[pRow][pCol];}
                }
            }
            catch (Exception e)
            {
                base.LogException(this.Name, "FillPayRollDataWithPersonnelBasicInfo(int personnelNumberRow)", e);
                //error in copying data.
            }
        }



        private void SetStatusBar(string StatusMessage)
        {
            txtProcName.Text = StatusMessage;   // "not initialized";
            //labelStatusBar.Text = StatusMessage; // "not initialized";
        }


        private void SetFormInProcessingState()
        {
            groupBoxProcessRunning.Visible = true;
            //this.groupBoxMainProcess.Enabled = false;
        }




        private void slTB_FinalAnswer_Leave(object sender, EventArgs e)
        {
            try
            {
                if (slTB_FinalAnswer.Text != string.Empty)
                {
                    if (slTB_FinalAnswer.Text.ToUpper() == "YES")
                    {
                        if (processState == FormMode.Successfull)
                            processTrans.Commit();
                        TermSet_Clear();
                        this.Close();//commit trans

                    }
                    else if (slTB_FinalAnswer.Text.ToUpper() == "NO")
                    {
                        if (processState == FormMode.Successfull)
                            processTrans.Rollback();
                        TermSet_Clear();
                        this.Close();
                        //rollback trans
                    }
                    else
                    {
                        //  e.Cancel = true;

                    }
                }
            }
            catch (Exception exce)
            {
                if (processTrans != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
                base.LogException(this.Name, "slTB_FinalAnswer_Validating", exce);
            }

        }

        private void slTB_FinalAnswer_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar.ToString().Equals("\r"))
                {
                    if (slTB_FinalAnswer.Text != string.Empty)
                    {
                        if (slTB_FinalAnswer.Text.ToUpper() == "YES")
                        {
                            if (processState == FormMode.Successfull)
                                processTrans.Commit();
                            TermSet_Clear();
                            this.Close();//commit trans

                        }
                        else if (slTB_FinalAnswer.Text.ToUpper() == "NO")
                        {
                            if (processState == FormMode.Successfull)
                                processTrans.Rollback();
                            TermSet_Clear();
                            this.Close();
                            //rollback trans
                        }
                        else
                        {
                            //  e.Cancel = true;

                        }
                    }
                }
            }
            catch (Exception exce)
            {
                if (processTrans != null && processTrans.Connection != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
                base.LogException(this.Name, "slTB_FinalAnswer_Validating", exce);
                this.Close();

            }
        }


        private void TermSet_Clear()
        {
            this.rslt = cmnDM.Get("CHRIS_SP_TESTPAYROLLPROCESS_MANAGER", "UpdateToNull");
            if (this.rslt.isSuccessful)
            {
                this.processState = FormMode.NotStarted;
                SetFormInInitialState();
            }
        }

    }
}