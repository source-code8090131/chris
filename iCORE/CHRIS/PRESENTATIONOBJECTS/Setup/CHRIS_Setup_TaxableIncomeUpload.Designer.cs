﻿namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_TaxableIncomeUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnChkData = new System.Windows.Forms.Button();
            this.btnStartProcess = new System.Windows.Forms.Button();
            this.btnUploadPath = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBxConsumerFile = new System.Windows.Forms.TextBox();
            this.dGVCostCenter = new System.Windows.Forms.DataGridView();
            this.folderBrwDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnFindData = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.dtToConData = new System.Windows.Forms.DateTimePicker();
            this.label29 = new System.Windows.Forms.Label();
            this.dtFromConData = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEmpNo = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVCostCenter)).BeginInit();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnChkData);
            this.panel1.Controls.Add(this.btnStartProcess);
            this.panel1.Controls.Add(this.btnUploadPath);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.textBxConsumerFile);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 157);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(216, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 22);
            this.label1.TabIndex = 76;
            this.label1.Text = "Tax Adjustment Uploader";
            // 
            // btnChkData
            // 
            this.btnChkData.Location = new System.Drawing.Point(441, 117);
            this.btnChkData.Margin = new System.Windows.Forms.Padding(4);
            this.btnChkData.Name = "btnChkData";
            this.btnChkData.Size = new System.Drawing.Size(93, 32);
            this.btnChkData.TabIndex = 75;
            this.btnChkData.Text = "Close";
            this.btnChkData.UseVisualStyleBackColor = true;
            this.btnChkData.Click += new System.EventHandler(this.btnChkData_Click);
            // 
            // btnStartProcess
            // 
            this.btnStartProcess.Location = new System.Drawing.Point(319, 117);
            this.btnStartProcess.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartProcess.Name = "btnStartProcess";
            this.btnStartProcess.Size = new System.Drawing.Size(114, 31);
            this.btnStartProcess.TabIndex = 74;
            this.btnStartProcess.Text = "Run";
            this.btnStartProcess.UseVisualStyleBackColor = true;
            this.btnStartProcess.Click += new System.EventHandler(this.btnStartProcess_Click);
            // 
            // btnUploadPath
            // 
            this.btnUploadPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadPath.Location = new System.Drawing.Point(522, 74);
            this.btnUploadPath.Margin = new System.Windows.Forms.Padding(4);
            this.btnUploadPath.Name = "btnUploadPath";
            this.btnUploadPath.Size = new System.Drawing.Size(49, 25);
            this.btnUploadPath.TabIndex = 73;
            this.btnUploadPath.Text = "...";
            this.btnUploadPath.UseVisualStyleBackColor = true;
            this.btnUploadPath.Click += new System.EventHandler(this.btnUploadPath_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.citinewimage;
            this.pictureBox1.Location = new System.Drawing.Point(500, 4);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(86, 49);
            this.pictureBox1.TabIndex = 72;
            this.pictureBox1.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(18, 77);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 19);
            this.label26.TabIndex = 17;
            this.label26.Text = "Select File";
            // 
            // textBxConsumerFile
            // 
            this.textBxConsumerFile.Location = new System.Drawing.Point(114, 77);
            this.textBxConsumerFile.Margin = new System.Windows.Forms.Padding(4);
            this.textBxConsumerFile.Name = "textBxConsumerFile";
            this.textBxConsumerFile.ReadOnly = true;
            this.textBxConsumerFile.Size = new System.Drawing.Size(400, 22);
            this.textBxConsumerFile.TabIndex = 18;
            // 
            // dGVCostCenter
            // 
            this.dGVCostCenter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVCostCenter.Location = new System.Drawing.Point(9, 299);
            this.dGVCostCenter.Name = "dGVCostCenter";
            this.dGVCostCenter.ReadOnly = true;
            this.dGVCostCenter.RowTemplate.Height = 24;
            this.dGVCostCenter.Size = new System.Drawing.Size(590, 349);
            this.dGVCostCenter.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtEmpNo);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.btnFindData);
            this.panel9.Controls.Add(this.label30);
            this.panel9.Controls.Add(this.dtToConData);
            this.panel9.Controls.Add(this.label29);
            this.panel9.Controls.Add(this.dtFromConData);
            this.panel9.Location = new System.Drawing.Point(13, 176);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(589, 116);
            this.panel9.TabIndex = 22;
            // 
            // btnFindData
            // 
            this.btnFindData.Location = new System.Drawing.Point(451, 67);
            this.btnFindData.Margin = new System.Windows.Forms.Padding(4);
            this.btnFindData.Name = "btnFindData";
            this.btnFindData.Size = new System.Drawing.Size(119, 33);
            this.btnFindData.TabIndex = 21;
            this.btnFindData.Text = "Find";
            this.btnFindData.UseVisualStyleBackColor = true;
            this.btnFindData.Click += new System.EventHandler(this.btnFindData_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(17, 43);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(64, 19);
            this.label30.TabIndex = 96;
            this.label30.Text = "To Date";
            // 
            // dtToConData
            // 
            this.dtToConData.Location = new System.Drawing.Point(168, 40);
            this.dtToConData.Margin = new System.Windows.Forms.Padding(4);
            this.dtToConData.Name = "dtToConData";
            this.dtToConData.Size = new System.Drawing.Size(264, 22);
            this.dtToConData.TabIndex = 95;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(17, 13);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 19);
            this.label29.TabIndex = 94;
            this.label29.Text = "From Date";
            // 
            // dtFromConData
            // 
            this.dtFromConData.Location = new System.Drawing.Point(168, 10);
            this.dtFromConData.Margin = new System.Windows.Forms.Padding(4);
            this.dtFromConData.Name = "dtFromConData";
            this.dtFromConData.Size = new System.Drawing.Size(264, 22);
            this.dtFromConData.TabIndex = 93;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 19);
            this.label2.TabIndex = 97;
            this.label2.Text = "Employee ID";
            // 
            // txtEmpNo
            // 
            this.txtEmpNo.Location = new System.Drawing.Point(168, 73);
            this.txtEmpNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmpNo.MaxLength = 20;
            this.txtEmpNo.Name = "txtEmpNo";
            this.txtEmpNo.Size = new System.Drawing.Size(264, 22);
            this.txtEmpNo.TabIndex = 98;
            // 
            // CHRIS_Setup_TaxableIncomeUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 660);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dGVCostCenter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Setup_TaxableIncomeUpload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_Setup_TaxableIncomeUpload";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVCostCenter)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnChkData;
        private System.Windows.Forms.Button btnStartProcess;
        private System.Windows.Forms.Button btnUploadPath;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBxConsumerFile;
        private System.Windows.Forms.DataGridView dGVCostCenter;
        private System.Windows.Forms.FolderBrowserDialog folderBrwDialog;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnFindData;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DateTimePicker dtToConData;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dtFromConData;
        internal System.Windows.Forms.TextBox txtEmpNo;
        private System.Windows.Forms.Label label2;
    }
}