namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_AllowanceEntry_MasterDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_AllowanceEntry_MasterDetail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlTblDedDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDedDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.grdDeducCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdPersonnelNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlAllowanceMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtSP_FORECAST_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtSP_ASR_BASIC = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.txtSP_TAX = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.lbtnAllowance = new CrplControlLibrary.LookupButton(this.components);
            this.txt_SP_VALID_TO = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_VALID_FROM = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_sp_desc = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ACOUNT_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_sp_all_code = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblDedDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).BeginInit();
            this.pnlAllowanceMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 508);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // pnlTblDedDetail
            // 
            this.pnlTblDedDetail.ConcurrentPanels = null;
            this.pnlTblDedDetail.Controls.Add(this.dgvDedDetail);
            this.pnlTblDedDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDedDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDedDetail.DependentPanels = null;
            this.pnlTblDedDetail.DisableDependentLoad = false;
            this.pnlTblDedDetail.EnableDelete = true;
            this.pnlTblDedDetail.EnableInsert = true;
            this.pnlTblDedDetail.EnableUpdate = true;
            this.pnlTblDedDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceDatailsCommand";
            this.pnlTblDedDetail.Location = new System.Drawing.Point(19, 185);
            this.pnlTblDedDetail.MasterPanel = this.pnlAllowanceMain;
            this.pnlTblDedDetail.Name = "pnlTblDedDetail";
            this.pnlTblDedDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDedDetail.Size = new System.Drawing.Size(613, 320);
            this.pnlTblDedDetail.SPName = "CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER";
            this.pnlTblDedDetail.TabIndex = 11;
            // 
            // dgvDedDetail
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDedDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDedDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdDeducCode,
            this.grdPersonnelNo,
            this.grdName,
            this.grdRemarks});
            this.dgvDedDetail.ColumnToHide = null;
            this.dgvDedDetail.ColumnWidth = null;
            this.dgvDedDetail.CustomEnabled = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDedDetail.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDedDetail.DisplayColumnWrapper = null;
            this.dgvDedDetail.GridDefaultRow = 0;
            this.dgvDedDetail.Location = new System.Drawing.Point(3, 3);
            this.dgvDedDetail.Name = "dgvDedDetail";
            this.dgvDedDetail.ReadOnlyColumns = null;
            this.dgvDedDetail.RequiredColumns = null;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDedDetail.Size = new System.Drawing.Size(607, 314);
            this.dgvDedDetail.SkippingColumns = "GRDNAME,GRDREMARKS";
            this.dgvDedDetail.TabIndex = 1;
            // 
            // grdDeducCode
            // 
            this.grdDeducCode.DataPropertyName = "SP_ALL_CODE";
            this.grdDeducCode.HeaderText = "";
            this.grdDeducCode.Name = "grdDeducCode";
            this.grdDeducCode.Visible = false;
            // 
            // grdPersonnelNo
            // 
            this.grdPersonnelNo.DataPropertyName = "SP_P_NO";
            this.grdPersonnelNo.HeaderText = "Personnel No.";
            this.grdPersonnelNo.Name = "grdPersonnelNo";
            this.grdPersonnelNo.ReadOnly = true;
            this.grdPersonnelNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPersonnelNo.Width = 70;
            // 
            // grdName
            // 
            this.grdName.DataPropertyName = "Name";
            this.grdName.HeaderText = "Name";
            this.grdName.Name = "grdName";
            this.grdName.ReadOnly = true;
            this.grdName.Width = 240;
            // 
            // grdRemarks
            // 
            this.grdRemarks.DataPropertyName = "SP_REMARKS";
            this.grdRemarks.HeaderText = "Remarks";
            this.grdRemarks.Name = "grdRemarks";
            this.grdRemarks.ReadOnly = true;
            this.grdRemarks.Width = 240;
            // 
            // pnlAllowanceMain
            // 
            this.pnlAllowanceMain.ConcurrentPanels = null;
            this.pnlAllowanceMain.Controls.Add(this.txtSP_FORECAST_TAX);
            this.pnlAllowanceMain.Controls.Add(this.label13);
            this.pnlAllowanceMain.Controls.Add(this.txtSP_ASR_BASIC);
            this.pnlAllowanceMain.Controls.Add(this.label14);
            this.pnlAllowanceMain.Controls.Add(this.txtSP_TAX);
            this.pnlAllowanceMain.Controls.Add(this.label21);
            this.pnlAllowanceMain.Controls.Add(this.lbtnAllowance);
            this.pnlAllowanceMain.Controls.Add(this.txt_SP_VALID_TO);
            this.pnlAllowanceMain.Controls.Add(this.txt_SP_VALID_FROM);
            this.pnlAllowanceMain.Controls.Add(this.txt_sp_desc);
            this.pnlAllowanceMain.Controls.Add(this.txt_SP_ACOUNT_NO);
            this.pnlAllowanceMain.Controls.Add(this.txt_SP_DED_AMOUNT);
            this.pnlAllowanceMain.Controls.Add(this.txt_sp_all_code);
            this.pnlAllowanceMain.Controls.Add(this.label8);
            this.pnlAllowanceMain.Controls.Add(this.label7);
            this.pnlAllowanceMain.Controls.Add(this.label6);
            this.pnlAllowanceMain.Controls.Add(this.label2);
            this.pnlAllowanceMain.Controls.Add(this.label1);
            this.pnlAllowanceMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlAllowanceMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlAllowanceMain.DependentPanels = null;
            this.pnlAllowanceMain.DisableDependentLoad = false;
            this.pnlAllowanceMain.EnableDelete = true;
            this.pnlAllowanceMain.EnableInsert = true;
            this.pnlAllowanceMain.EnableUpdate = true;
            this.pnlAllowanceMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AllowanceSetupCommand";
            this.pnlAllowanceMain.Location = new System.Drawing.Point(19, 93);
            this.pnlAllowanceMain.MasterPanel = null;
            this.pnlAllowanceMain.Name = "pnlAllowanceMain";
            this.pnlAllowanceMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlAllowanceMain.Size = new System.Drawing.Size(613, 86);
            this.pnlAllowanceMain.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.pnlAllowanceMain.TabIndex = 10;
            // 
            // txtSP_FORECAST_TAX
            // 
            this.txtSP_FORECAST_TAX.AllowSpace = true;
            this.txtSP_FORECAST_TAX.AssociatedLookUpName = "";
            this.txtSP_FORECAST_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_FORECAST_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_FORECAST_TAX.ContinuationTextBox = null;
            this.txtSP_FORECAST_TAX.CustomEnabled = true;
            this.txtSP_FORECAST_TAX.DataFieldMapping = "SP_FORECAST_TAX";
            this.txtSP_FORECAST_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_FORECAST_TAX.GetRecordsOnUpDownKeys = false;
            this.txtSP_FORECAST_TAX.IsDate = false;
            this.txtSP_FORECAST_TAX.Location = new System.Drawing.Point(550, 59);
            this.txtSP_FORECAST_TAX.MaxLength = 1;
            this.txtSP_FORECAST_TAX.Name = "txtSP_FORECAST_TAX";
            this.txtSP_FORECAST_TAX.NumberFormat = "###,###,##0.00";
            this.txtSP_FORECAST_TAX.Postfix = "";
            this.txtSP_FORECAST_TAX.Prefix = "";
            this.txtSP_FORECAST_TAX.ReadOnly = true;
            this.txtSP_FORECAST_TAX.Size = new System.Drawing.Size(49, 20);
            this.txtSP_FORECAST_TAX.SkipValidation = false;
            this.txtSP_FORECAST_TAX.TabIndex = 70;
            this.txtSP_FORECAST_TAX.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_FORECAST_TAX, "Enter [Y/N]");
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(466, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 17);
            this.label13.TabIndex = 71;
            this.label13.Text = "Tax Forecast";
            // 
            // txtSP_ASR_BASIC
            // 
            this.txtSP_ASR_BASIC.AllowSpace = true;
            this.txtSP_ASR_BASIC.AssociatedLookUpName = "";
            this.txtSP_ASR_BASIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_ASR_BASIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_ASR_BASIC.ContinuationTextBox = null;
            this.txtSP_ASR_BASIC.CustomEnabled = true;
            this.txtSP_ASR_BASIC.DataFieldMapping = "SP_ASR_BASIC";
            this.txtSP_ASR_BASIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_ASR_BASIC.GetRecordsOnUpDownKeys = false;
            this.txtSP_ASR_BASIC.IsDate = false;
            this.txtSP_ASR_BASIC.Location = new System.Drawing.Point(550, 33);
            this.txtSP_ASR_BASIC.MaxLength = 1;
            this.txtSP_ASR_BASIC.Name = "txtSP_ASR_BASIC";
            this.txtSP_ASR_BASIC.NumberFormat = "###,###,##0.00";
            this.txtSP_ASR_BASIC.Postfix = "";
            this.txtSP_ASR_BASIC.Prefix = "";
            this.txtSP_ASR_BASIC.ReadOnly = true;
            this.txtSP_ASR_BASIC.Size = new System.Drawing.Size(49, 20);
            this.txtSP_ASR_BASIC.SkipValidation = false;
            this.txtSP_ASR_BASIC.TabIndex = 68;
            this.txtSP_ASR_BASIC.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_ASR_BASIC, "Enter [A/B]");
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(466, 36);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 20);
            this.label14.TabIndex = 69;
            this.label14.Text = "ASR / Basic";
            // 
            // txtSP_TAX
            // 
            this.txtSP_TAX.AllowSpace = true;
            this.txtSP_TAX.AssociatedLookUpName = "";
            this.txtSP_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSP_TAX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSP_TAX.ContinuationTextBox = null;
            this.txtSP_TAX.CustomEnabled = true;
            this.txtSP_TAX.DataFieldMapping = "SP_TAX";
            this.txtSP_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSP_TAX.GetRecordsOnUpDownKeys = false;
            this.txtSP_TAX.IsDate = false;
            this.txtSP_TAX.Location = new System.Drawing.Point(550, 10);
            this.txtSP_TAX.MaxLength = 1;
            this.txtSP_TAX.Name = "txtSP_TAX";
            this.txtSP_TAX.NumberFormat = "###,###,##0.00";
            this.txtSP_TAX.Postfix = "";
            this.txtSP_TAX.Prefix = "";
            this.txtSP_TAX.ReadOnly = true;
            this.txtSP_TAX.Size = new System.Drawing.Size(49, 20);
            this.txtSP_TAX.SkipValidation = false;
            this.txtSP_TAX.TabIndex = 56;
            this.txtSP_TAX.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtSP_TAX, "Enter [Y/N]");
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(466, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 20);
            this.label21.TabIndex = 57;
            this.label21.Text = "Taxable";
            // 
            // lbtnAllowance
            // 
            this.lbtnAllowance.ActionLOVExists = "";
            this.lbtnAllowance.ActionType = "AllowanceDetailCode_LOV";
            this.lbtnAllowance.ConditionalFields = "";
            this.lbtnAllowance.CustomEnabled = true;
            this.lbtnAllowance.DataFieldMapping = "";
            this.lbtnAllowance.DependentLovControls = "";
            this.lbtnAllowance.HiddenColumns = "";
            this.lbtnAllowance.Image = ((System.Drawing.Image)(resources.GetObject("lbtnAllowance.Image")));
            this.lbtnAllowance.LoadDependentEntities = true;
            this.lbtnAllowance.Location = new System.Drawing.Point(163, 11);
            this.lbtnAllowance.LookUpTitle = null;
            this.lbtnAllowance.Name = "lbtnAllowance";
            this.lbtnAllowance.Size = new System.Drawing.Size(26, 21);
            this.lbtnAllowance.SkipValidationOnLeave = true;
            this.lbtnAllowance.SPName = "CHRIS_SP_SETUP_ALLOWANCE_MANAGER";
            this.lbtnAllowance.TabIndex = 26;
            this.lbtnAllowance.TabStop = false;
            this.lbtnAllowance.UseVisualStyleBackColor = true;
            // 
            // txt_SP_VALID_TO
            // 
            this.txt_SP_VALID_TO.CustomEnabled = true;
            this.txt_SP_VALID_TO.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_VALID_TO.DataFieldMapping = "SP_VALID_TO";
            this.txt_SP_VALID_TO.Enabled = false;
            this.txt_SP_VALID_TO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_VALID_TO.Location = new System.Drawing.Point(210, 36);
            this.txt_SP_VALID_TO.Name = "txt_SP_VALID_TO";
            this.txt_SP_VALID_TO.NullValue = " ";
            this.txt_SP_VALID_TO.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_VALID_TO.TabIndex = 10;
            this.txt_SP_VALID_TO.Value = new System.DateTime(2011, 2, 4, 15, 36, 2, 606);
            // 
            // txt_SP_VALID_FROM
            // 
            this.txt_SP_VALID_FROM.CustomEnabled = true;
            this.txt_SP_VALID_FROM.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_VALID_FROM.DataFieldMapping = "SP_VALID_FROM";
            this.txt_SP_VALID_FROM.Enabled = false;
            this.txt_SP_VALID_FROM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_VALID_FROM.Location = new System.Drawing.Point(89, 36);
            this.txt_SP_VALID_FROM.Name = "txt_SP_VALID_FROM";
            this.txt_SP_VALID_FROM.NullValue = " ";
            this.txt_SP_VALID_FROM.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_VALID_FROM.TabIndex = 9;
            this.txt_SP_VALID_FROM.Value = new System.DateTime(2011, 2, 4, 15, 36, 2, 606);
            // 
            // txt_sp_desc
            // 
            this.txt_sp_desc.AllowSpace = true;
            this.txt_sp_desc.AssociatedLookUpName = "";
            this.txt_sp_desc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_sp_desc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_sp_desc.ContinuationTextBox = null;
            this.txt_sp_desc.CustomEnabled = true;
            this.txt_sp_desc.DataFieldMapping = "sp_desc";
            this.txt_sp_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_sp_desc.GetRecordsOnUpDownKeys = false;
            this.txt_sp_desc.IsDate = false;
            this.txt_sp_desc.Location = new System.Drawing.Point(210, 13);
            this.txt_sp_desc.Name = "txt_sp_desc";
            this.txt_sp_desc.NumberFormat = "###,###,##0.00";
            this.txt_sp_desc.Postfix = "";
            this.txt_sp_desc.Prefix = "";
            this.txt_sp_desc.ReadOnly = true;
            this.txt_sp_desc.Size = new System.Drawing.Size(252, 20);
            this.txt_sp_desc.SkipValidation = false;
            this.txt_sp_desc.TabIndex = 8;
            this.txt_sp_desc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_ACOUNT_NO
            // 
            this.txt_SP_ACOUNT_NO.AllowSpace = true;
            this.txt_SP_ACOUNT_NO.AssociatedLookUpName = "";
            this.txt_SP_ACOUNT_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ACOUNT_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ACOUNT_NO.ContinuationTextBox = null;
            this.txt_SP_ACOUNT_NO.CustomEnabled = true;
            this.txt_SP_ACOUNT_NO.DataFieldMapping = "SP_ACOUNT_NO";
            this.txt_SP_ACOUNT_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ACOUNT_NO.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ACOUNT_NO.IsDate = false;
            this.txt_SP_ACOUNT_NO.Location = new System.Drawing.Point(89, 59);
            this.txt_SP_ACOUNT_NO.Name = "txt_SP_ACOUNT_NO";
            this.txt_SP_ACOUNT_NO.NumberFormat = "###,###,##0.00";
            this.txt_SP_ACOUNT_NO.Postfix = "";
            this.txt_SP_ACOUNT_NO.Prefix = "";
            this.txt_SP_ACOUNT_NO.ReadOnly = true;
            this.txt_SP_ACOUNT_NO.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_ACOUNT_NO.SkipValidation = false;
            this.txt_SP_ACOUNT_NO.TabIndex = 7;
            this.txt_SP_ACOUNT_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_DED_AMOUNT
            // 
            this.txt_SP_DED_AMOUNT.AllowSpace = true;
            this.txt_SP_DED_AMOUNT.AssociatedLookUpName = "";
            this.txt_SP_DED_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_AMOUNT.ContinuationTextBox = null;
            this.txt_SP_DED_AMOUNT.CustomEnabled = true;
            this.txt_SP_DED_AMOUNT.DataFieldMapping = "SP_ALL_AMOUNT";
            this.txt_SP_DED_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_AMOUNT.IsDate = false;
            this.txt_SP_DED_AMOUNT.Location = new System.Drawing.Point(371, 36);
            this.txt_SP_DED_AMOUNT.Name = "txt_SP_DED_AMOUNT";
            this.txt_SP_DED_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_AMOUNT.Postfix = "";
            this.txt_SP_DED_AMOUNT.Prefix = "";
            this.txt_SP_DED_AMOUNT.ReadOnly = true;
            this.txt_SP_DED_AMOUNT.Size = new System.Drawing.Size(91, 20);
            this.txt_SP_DED_AMOUNT.SkipValidation = false;
            this.txt_SP_DED_AMOUNT.TabIndex = 6;
            this.txt_SP_DED_AMOUNT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_sp_all_code
            // 
            this.txt_sp_all_code.AllowSpace = true;
            this.txt_sp_all_code.AssociatedLookUpName = "lbtnAllowance";
            this.txt_sp_all_code.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_sp_all_code.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_sp_all_code.ContinuationTextBox = null;
            this.txt_sp_all_code.CustomEnabled = true;
            this.txt_sp_all_code.DataFieldMapping = "SP_ALL_CODE";
            this.txt_sp_all_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_sp_all_code.GetRecordsOnUpDownKeys = false;
            this.txt_sp_all_code.IsDate = false;
            this.txt_sp_all_code.Location = new System.Drawing.Point(89, 13);
            this.txt_sp_all_code.MaxLength = 3;
            this.txt_sp_all_code.Name = "txt_sp_all_code";
            this.txt_sp_all_code.NumberFormat = "###,###,##0.00";
            this.txt_sp_all_code.Postfix = "";
            this.txt_sp_all_code.Prefix = "";
            this.txt_sp_all_code.Size = new System.Drawing.Size(62, 20);
            this.txt_sp_all_code.SkipValidation = false;
            this.txt_sp_all_code.TabIndex = 5;
            this.txt_sp_all_code.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(316, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Amount";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(195, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "/";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Account No.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "From / To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code";
            // 
            // CHRIS_Setup_AllowanceEntry_MasterDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 568);
            this.Controls.Add(this.pnlTblDedDetail);
            this.Controls.Add(this.pnlAllowanceMain);
            this.Name = "CHRIS_Setup_AllowanceEntry_MasterDetail";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Setup_AllowanceEntry_MasterDetail";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlAllowanceMain, 0);
            this.Controls.SetChildIndex(this.pnlTblDedDetail, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblDedDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).EndInit();
            this.pnlAllowanceMain.ResumeLayout(false);
            this.pnlAllowanceMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDedDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDedDetail;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlAllowanceMain;
        private CrplControlLibrary.LookupButton lbtnAllowance;
        private CrplControlLibrary.SLDatePicker txt_SP_VALID_TO;
        private CrplControlLibrary.SLDatePicker txt_SP_VALID_FROM;
        private CrplControlLibrary.SLTextBox txt_sp_desc;
        private CrplControlLibrary.SLTextBox txt_SP_ACOUNT_NO;
        private CrplControlLibrary.SLTextBox txt_SP_DED_AMOUNT;
        private CrplControlLibrary.SLTextBox txt_sp_all_code;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtSP_TAX;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtSP_ASR_BASIC;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtSP_FORECAST_TAX;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdDeducCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdPersonnelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdName;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdRemarks;
    }
}