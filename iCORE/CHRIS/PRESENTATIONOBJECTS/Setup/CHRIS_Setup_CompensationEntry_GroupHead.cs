using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_CompensationEntry_GroupHead : ChrisTabularForm
    {
        public bool PopUPFormOpen = false;
        public bool delStatus = false;     

        #region --Construstor--
        public CHRIS_Setup_CompensationEntry_GroupHead()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_CompensationEntry_GroupHead(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #endregion

        #region --Method--

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible     = false;
                this.tbtList.Visible    = false;
                this.tbtSave.Visible    = false;
                this.tbtCancel.Visible  = false;
                this.tbtDelete.Visible  = false;
                this.tlbMain.Items["tbtSearch"].Available = false;
                // this.tlbMain.Visible = false;
                this.txtOption.Visible  = false;
                Font newFontStyle = new Font(dgvGrpHead.Font, FontStyle.Bold);
                dgvGrpHead.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        #endregion

        #region --Event--
        
        /// <summary>
        /// Validate the Cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvGrpHead_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (!dgvGrpHead.CurrentCell.IsInEditMode)
                    return;

                if (dgvGrpHead.Rows.Count > 1)
                {
                    if (dgvGrpHead.CurrentCell.OwningColumn.Name == "gpHead" && dgvGrpHead.CurrentCell.EditedFormattedValue != "")
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("GRHD_NO", dgvGrpHead.CurrentCell.EditedFormattedValue.ToString());
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();

                        rsltCode = cmnDM.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "Group_Exist", param);

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            MessageBox.Show("Group Head Code Already Exist Enter Unique GRoup Head Code", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "dgvGrpHead_CellValidating", exp);
            }
        }

        /// <summary>
        /// Move to First Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScroll_UP_Click(object sender, EventArgs e)
        {
            if (dgvGrpHead.Rows.Count > 1)
            {
                dgvGrpHead.Rows[0].Selected = true;
                dgvGrpHead.FirstDisplayedScrollingRowIndex = dgvGrpHead.SelectedCells[0].RowIndex;
            }
        }

        /// <summary>
        /// Move to Previous Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUP_Click(object sender, EventArgs e)
        {
            if (dgvGrpHead.Rows.Count > 1)
            {
                int CurrentRow;
                CurrentRow = dgvGrpHead.SelectedCells[0].RowIndex;
                if (CurrentRow > 0)
                {
                    dgvGrpHead.Rows[CurrentRow - 1].Selected = true;
                    dgvGrpHead.FirstDisplayedScrollingRowIndex = dgvGrpHead.SelectedCells[0].RowIndex;
                }
            }
        }

        /// <summary>
        /// Move to Next Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (dgvGrpHead.Rows.Count > 1)
            {
                int CurrentRow;
                CurrentRow = dgvGrpHead.SelectedCells[0].RowIndex;
                if (CurrentRow < (dgvGrpHead.RowCount - 1))
                {
                    dgvGrpHead.Rows[CurrentRow + 1].Selected = true;
                    dgvGrpHead.FirstDisplayedScrollingRowIndex = dgvGrpHead.SelectedCells[0].RowIndex;
                }
            }
        }
        
        /// <summary>
        /// SCroll Down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScrollDown_Click(object sender, EventArgs e)
        {
            if (dgvGrpHead.Rows.Count > 1)
            {
                dgvGrpHead.Rows[dgvGrpHead.Rows.Count - 1].Selected = true;
                dgvGrpHead.FirstDisplayedScrollingRowIndex = dgvGrpHead.SelectedCells[0].RowIndex;
            }
        }
        
        /// <summary>
        /// Delete the Selected Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (delStatus == false)
            {
                dgvGrpHead.DeleteSelectedGridRow();
            }
            else
            { 
                base.DoToolbarActions(pnlTblGrpHead.Controls, "Delete");
                base.Delete();
            }

        }

        /// <summary>
        /// Exit from Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)dgvGrpHead.DataSource;
            DialogResult dr = MessageBox.Show("Do You Want to Save the Changes You Have Made?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.No)
            {
                PopUPFormOpen = true;
                base.Quit();
            }
            else
            {

                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);

                    if (dtAdded != null || dtUpdated != null)
                    {
                        base.Save();
                    }
                
                    PopUPFormOpen = true;
                    base.Quit();
                }

            }
                 
        
        }



        /// <summary>
        /// Fill the Grid with Value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            delStatus = true;
            Dictionary<string, object> param = new Dictionary<string, object>();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "List", param);

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() != string.Empty)
            {
                dgvGrpHead.GridSource = rsltCode.dstResult.Tables[0];
                dgvGrpHead.DataSource = dgvGrpHead.GridSource;
            }
        }

        /// <summary>
        /// Save the Rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                base.Save();
            }
            catch (Exception exp)
            {
                LogError(this.Name, "btnSave_Click", exp);
            }
        }

        /// <summary>
        /// Back to previous Screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        /// <summary>
        /// Override the F6 functionality.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6 && this.FunctionConfig.EnableF6)
            {
                return;
            }
            else if (e.KeyCode == Keys.F8 && this.FunctionConfig.EnableF8)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "List", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() != string.Empty)
                {
                    dgvGrpHead.GridSource = rsltCode.dstResult.Tables[0];
                    dgvGrpHead.DataSource = dgvGrpHead.GridSource;
                }
            }
        }
        #endregion


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.tbtEdit.Available = false;
            this.lblusername.Text = this.UserName;
            this.tlbMain.Items["tbtSearch"].Available = false;
        }
        

    }
}