using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_AllowanceEnter_Details : ChrisTabularForm
    {
        private CHRIS_Setup_AllowanceEnter _mainForm = null;
        private DataTable dtDept;

        public CHRIS_Setup_AllowanceEnter_Details()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_AllowanceEnter_Details(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Setup_AllowanceEnter mainForm, DataTable _dtDept)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtDept = _dtDept;
            this._mainForm = mainForm;
        }


        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
           
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;
                this.txtOption.Visible = false;
                DGVCategory.Focus();
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
           
                base.OnLoad(e);

                DGVCategory.GridSource = dtDept;
                DGVCategory.DataSource = DGVCategory.GridSource;

                
            
        }

        private void DGVCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (DGVCategory.CurrentCell.OwningColumn.Name == "SP_P_NO")//&& dgvBlkDept.CurrentCell.Value != null)
            {
                if (_mainForm.FunctionConfig.CurrentOption.ToString() == "Add" && e.KeyCode.ToString() == Keys.Enter.ToString())
                {
                    this.Close();
                }

            }
        }

      


    }
}