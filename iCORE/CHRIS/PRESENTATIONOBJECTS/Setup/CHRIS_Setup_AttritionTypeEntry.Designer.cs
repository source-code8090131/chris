namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_AttritionTypeEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_AttritionTypeEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlTabular = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVAttrition = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.ATTR_CATEGORY_CODE = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.ATTR_CATEGORY_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ATTR_TYPE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ATTR_TYPE_DESC = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlTabular.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVAttrition)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(602, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(638, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 508);
            this.panel1.Size = new System.Drawing.Size(638, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label8);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label10);
            this.pnlHead.Controls.Add(this.label11);
            this.pnlHead.Location = new System.Drawing.Point(12, 101);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(615, 59);
            this.pnlHead.TabIndex = 13;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(500, 20);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(100, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 20;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(448, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "Date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(185, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ATTRITION CATEGORY/TYPE ENTRY";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(75, 32);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(90, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(276, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Set Up";
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(75, 5);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(90, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(1, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Loc:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(30, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "User:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlTabular
            // 
            this.pnlTabular.ConcurrentPanels = null;
            this.pnlTabular.Controls.Add(this.DGVAttrition);
            this.pnlTabular.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTabular.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTabular.DependentPanels = null;
            this.pnlTabular.DisableDependentLoad = false;
            this.pnlTabular.EnableDelete = true;
            this.pnlTabular.EnableInsert = true;
            this.pnlTabular.EnableQuery = false;
            this.pnlTabular.EnableUpdate = true;
            this.pnlTabular.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AttritionTypeCommand";
            this.pnlTabular.Location = new System.Drawing.Point(12, 160);
            this.pnlTabular.MasterPanel = null;
            this.pnlTabular.Name = "pnlTabular";
            this.pnlTabular.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTabular.Size = new System.Drawing.Size(615, 345);
            this.pnlTabular.SPName = "CHRIS_SP_SETUP_ATTR_TYPE_MANAGER";
            this.pnlTabular.TabIndex = 14;
            // 
            // DGVAttrition
            // 
            this.DGVAttrition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVAttrition.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ATTR_CATEGORY_CODE,
            this.ATTR_CATEGORY_DESC,
            this.ATTR_TYPE_CODE,
            this.ATTR_TYPE_DESC});
            this.DGVAttrition.ColumnToHide = null;
            this.DGVAttrition.ColumnWidth = null;
            this.DGVAttrition.CustomEnabled = true;
            this.DGVAttrition.DisplayColumnWrapper = null;
            this.DGVAttrition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVAttrition.GridDefaultRow = 0;
            this.DGVAttrition.Location = new System.Drawing.Point(0, 0);
            this.DGVAttrition.Name = "DGVAttrition";
            this.DGVAttrition.ReadOnlyColumns = null;
            this.DGVAttrition.RequiredColumns = null;
            this.DGVAttrition.Size = new System.Drawing.Size(615, 345);
            this.DGVAttrition.SkippingColumns = null;
            this.DGVAttrition.TabIndex = 0;
            this.DGVAttrition.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DGVAttrition_CellFormatting);
            this.DGVAttrition.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVAttrition_CellValidating);
            // 
            // ATTR_CATEGORY_CODE
            // 
            this.ATTR_CATEGORY_CODE.ActionLOV = "Category_LOV";
            this.ATTR_CATEGORY_CODE.ActionLOVExists = "Category_LOVExists";
            this.ATTR_CATEGORY_CODE.AttachParentEntity = false;
            this.ATTR_CATEGORY_CODE.DataPropertyName = "ATTR_CATEGORY_CODE";
            dataGridViewCellStyle1.Format = "N0";
            this.ATTR_CATEGORY_CODE.DefaultCellStyle = dataGridViewCellStyle1;
            this.ATTR_CATEGORY_CODE.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AttritionTypeCommand";
            this.ATTR_CATEGORY_CODE.FillWeight = 150F;
            this.ATTR_CATEGORY_CODE.HeaderText = "Category Code";
            this.ATTR_CATEGORY_CODE.LookUpTitle = null;
            this.ATTR_CATEGORY_CODE.LOVFieldMapping = "ATTR_CATEGORY_CODE";
            this.ATTR_CATEGORY_CODE.MaxInputLength = 3;
            this.ATTR_CATEGORY_CODE.Name = "ATTR_CATEGORY_CODE";
            this.ATTR_CATEGORY_CODE.ReadOnly = true;
            this.ATTR_CATEGORY_CODE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ATTR_CATEGORY_CODE.SearchColumn = "ATTR_CATEGORY_CODE";
            this.ATTR_CATEGORY_CODE.SkipValidationOnLeave = false;
            this.ATTR_CATEGORY_CODE.SpName = "CHRIS_SP_SETUP_ATTR_TYPE_MANAGER";
            this.ATTR_CATEGORY_CODE.Width = 140;
            // 
            // ATTR_CATEGORY_DESC
            // 
            this.ATTR_CATEGORY_DESC.DataPropertyName = "ATTR_CATEGORY_DESC";
            this.ATTR_CATEGORY_DESC.FillWeight = 250F;
            this.ATTR_CATEGORY_DESC.HeaderText = "Category Description";
            this.ATTR_CATEGORY_DESC.MaxInputLength = 20;
            this.ATTR_CATEGORY_DESC.Name = "ATTR_CATEGORY_DESC";
            this.ATTR_CATEGORY_DESC.ReadOnly = true;
            this.ATTR_CATEGORY_DESC.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ATTR_CATEGORY_DESC.Width = 225;
            // 
            // ATTR_TYPE_CODE
            // 
            this.ATTR_TYPE_CODE.DataPropertyName = "ATTR_TYPE_CODE";
            this.ATTR_TYPE_CODE.FillWeight = 150F;
            this.ATTR_TYPE_CODE.HeaderText = "Type Code";
            this.ATTR_TYPE_CODE.MaxInputLength = 3;
            this.ATTR_TYPE_CODE.Name = "ATTR_TYPE_CODE";
            this.ATTR_TYPE_CODE.ReadOnly = true;
            this.ATTR_TYPE_CODE.Width = 140;
            // 
            // ATTR_TYPE_DESC
            // 
            this.ATTR_TYPE_DESC.ActionLOV = "Category_LOV";
            this.ATTR_TYPE_DESC.ActionLOVExists = "";
            this.ATTR_TYPE_DESC.AttachParentEntity = false;
            this.ATTR_TYPE_DESC.DataPropertyName = "ATTR_TYPE_DESC";
            this.ATTR_TYPE_DESC.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AttritionTypeCommand";
            this.ATTR_TYPE_DESC.FillWeight = 300F;
            this.ATTR_TYPE_DESC.HeaderText = "Type Description";
            this.ATTR_TYPE_DESC.LookUpTitle = null;
            this.ATTR_TYPE_DESC.LOVFieldMapping = "ATTR_TYPE_DESC";
            this.ATTR_TYPE_DESC.MaxInputLength = 20;
            this.ATTR_TYPE_DESC.Name = "ATTR_TYPE_DESC";
            this.ATTR_TYPE_DESC.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ATTR_TYPE_DESC.SearchColumn = "ATTR_TYPE_DESC";
            this.ATTR_TYPE_DESC.SkipValidationOnLeave = false;
            this.ATTR_TYPE_DESC.SpName = "CHRIS_SP_SETUP_ATTR_TYPE_MANAGER";
            this.ATTR_TYPE_DESC.Width = 168;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(360, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(78, 13);
            this.txtUserName.TabIndex = 125;
            this.txtUserName.Text = "User  Name :   ";
            // 
            // CHRIS_Setup_AttritionTypeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 568);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlTabular);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlTabular";
            this.Name = "CHRIS_Setup_AttritionTypeEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Attrition Type Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlTabular, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlTabular.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVAttrition)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtLocation;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTabular;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVAttrition;
        private System.Windows.Forms.Label txtUserName;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn ATTR_CATEGORY_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ATTR_CATEGORY_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ATTR_TYPE_CODE;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn ATTR_TYPE_DESC;
    }
}