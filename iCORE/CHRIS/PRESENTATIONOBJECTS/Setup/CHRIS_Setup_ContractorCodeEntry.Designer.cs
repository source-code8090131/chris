namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_ContractorCodeEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_ContractorCodeEntry));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OPTION_DIS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_USER = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.pnlContractorCode = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbtnConCode = new CrplControlLibrary.LookupButton(this.components);
            this.txt_SP_CONTRA_EXP_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_CONTRA_AGRE_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_CONTRA_NID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_PHONE2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_PHONE1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_SP_CONTRA_ADD3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_ADD2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_ADD1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_COMP_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_CONTRA_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlContractorCode.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(620, 0);
            this.txtOption.Validating += new System.ComponentModel.CancelEventHandler(this.txtOption_Validating);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(656, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 421);
            this.panel1.Size = new System.Drawing.Size(656, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txt_W_OPTION_DIS);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txt_W_USER);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.lblLocation);
            this.pnlHead.Controls.Add(this.lblUser);
            this.pnlHead.Location = new System.Drawing.Point(12, 85);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(633, 57);
            this.pnlHead.TabIndex = 12;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(533, 30);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(89, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OPTION_DIS
            // 
            this.txt_W_OPTION_DIS.AllowSpace = true;
            this.txt_W_OPTION_DIS.AssociatedLookUpName = "";
            this.txt_W_OPTION_DIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OPTION_DIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OPTION_DIS.ContinuationTextBox = null;
            this.txt_W_OPTION_DIS.CustomEnabled = true;
            this.txt_W_OPTION_DIS.DataFieldMapping = "";
            this.txt_W_OPTION_DIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OPTION_DIS.GetRecordsOnUpDownKeys = false;
            this.txt_W_OPTION_DIS.IsDate = false;
            this.txt_W_OPTION_DIS.Location = new System.Drawing.Point(533, 8);
            this.txt_W_OPTION_DIS.Name = "txt_W_OPTION_DIS";
            this.txt_W_OPTION_DIS.NumberFormat = "###,###,##0.00";
            this.txt_W_OPTION_DIS.Postfix = "";
            this.txt_W_OPTION_DIS.Prefix = "";
            this.txt_W_OPTION_DIS.ReadOnly = true;
            this.txt_W_OPTION_DIS.Size = new System.Drawing.Size(57, 20);
            this.txt_W_OPTION_DIS.SkipValidation = false;
            this.txt_W_OPTION_DIS.TabIndex = 8;
            this.txt_W_OPTION_DIS.TabStop = false;
            this.txt_W_OPTION_DIS.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(81, 31);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(100, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 7;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_USER
            // 
            this.txt_W_USER.AllowSpace = true;
            this.txt_W_USER.AssociatedLookUpName = "";
            this.txt_W_USER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_USER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_USER.ContinuationTextBox = null;
            this.txt_W_USER.CustomEnabled = true;
            this.txt_W_USER.DataFieldMapping = "";
            this.txt_W_USER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_USER.GetRecordsOnUpDownKeys = false;
            this.txt_W_USER.IsDate = false;
            this.txt_W_USER.Location = new System.Drawing.Point(81, 8);
            this.txt_W_USER.Name = "txt_W_USER";
            this.txt_W_USER.NumberFormat = "###,###,##0.00";
            this.txt_W_USER.Postfix = "";
            this.txt_W_USER.Prefix = "";
            this.txt_W_USER.ReadOnly = true;
            this.txt_W_USER.Size = new System.Drawing.Size(100, 20);
            this.txt_W_USER.SkipValidation = false;
            this.txt_W_USER.TabIndex = 6;
            this.txt_W_USER.TabStop = false;
            this.txt_W_USER.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(491, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(483, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Option :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(241, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "C O N T R A C T O R   C O D E";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(301, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "S e t  U p";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocation.Location = new System.Drawing.Point(10, 31);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(64, 13);
            this.lblLocation.TabIndex = 1;
            this.lblLocation.Text = "Location :";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(33, 8);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(41, 13);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "User :";
            // 
            // pnlContractorCode
            // 
            this.pnlContractorCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlContractorCode.ConcurrentPanels = null;
            this.pnlContractorCode.Controls.Add(this.label23);
            this.pnlContractorCode.Controls.Add(this.label22);
            this.pnlContractorCode.Controls.Add(this.label21);
            this.pnlContractorCode.Controls.Add(this.label20);
            this.pnlContractorCode.Controls.Add(this.label19);
            this.pnlContractorCode.Controls.Add(this.label18);
            this.pnlContractorCode.Controls.Add(this.label17);
            this.pnlContractorCode.Controls.Add(this.label16);
            this.pnlContractorCode.Controls.Add(this.label15);
            this.pnlContractorCode.Controls.Add(this.lbtnConCode);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_EXP_DATE);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_AGRE_DATE);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_NID);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_PHONE2);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_PHONE1);
            this.pnlContractorCode.Controls.Add(this.label10);
            this.pnlContractorCode.Controls.Add(this.label13);
            this.pnlContractorCode.Controls.Add(this.label12);
            this.pnlContractorCode.Controls.Add(this.label11);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_ADD3);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_ADD2);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_ADD1);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_COMP_NAME);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_NAME);
            this.pnlContractorCode.Controls.Add(this.txt_SP_CONTRA_CODE);
            this.pnlContractorCode.Controls.Add(this.label9);
            this.pnlContractorCode.Controls.Add(this.label8);
            this.pnlContractorCode.Controls.Add(this.label7);
            this.pnlContractorCode.Controls.Add(this.label2);
            this.pnlContractorCode.Controls.Add(this.label1);
            this.pnlContractorCode.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlContractorCode.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlContractorCode.DependentPanels = null;
            this.pnlContractorCode.DisableDependentLoad = false;
            this.pnlContractorCode.EnableDelete = true;
            this.pnlContractorCode.EnableInsert = true;
            this.pnlContractorCode.EnableQuery = false;
            this.pnlContractorCode.EnableUpdate = true;
            this.pnlContractorCode.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ConCodEntCommand";
            this.pnlContractorCode.Location = new System.Drawing.Point(12, 148);
            this.pnlContractorCode.MasterPanel = null;
            this.pnlContractorCode.Name = "pnlContractorCode";
            this.pnlContractorCode.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlContractorCode.Size = new System.Drawing.Size(633, 269);
            this.pnlContractorCode.SPName = "CHRIS_SP_ConCod_CONTRACTER_MANAGER";
            this.pnlContractorCode.TabIndex = 13;
            this.pnlContractorCode.TabStop = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(165, 247);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 40;
            this.label23.Text = ":";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(165, 220);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 13);
            this.label22.TabIndex = 39;
            this.label22.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(165, 191);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = ":";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(165, 167);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(165, 87);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = ":";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(165, 63);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(165, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(165, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = ":";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(426, 166);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = ":";
            // 
            // lbtnConCode
            // 
            this.lbtnConCode.ActionLOVExists = "CONTRA_CODE_EXIST";
            this.lbtnConCode.ActionType = "CONTRA_CODE";
            this.lbtnConCode.ConditionalFields = "";
            this.lbtnConCode.CustomEnabled = true;
            this.lbtnConCode.DataFieldMapping = "";
            this.lbtnConCode.DependentLovControls = "";
            this.lbtnConCode.HiddenColumns = "SP_CONTRA_COMP_NAME|SP_CONTRA_ADD1|SP_CONTRA_ADD2|SP_CONTRA_ADD3|SP_CONTRA_PHONE1" +
                "|SP_CONTRA_PHONE2|SP_CONTRA_AGRE_DATE|SP_CONTRA_EXP_DATE|SP_CONTRA_NID";
            this.lbtnConCode.Image = ((System.Drawing.Image)(resources.GetObject("lbtnConCode.Image")));
            this.lbtnConCode.LoadDependentEntities = false;
            this.lbtnConCode.Location = new System.Drawing.Point(229, 9);
            this.lbtnConCode.LookUpTitle = null;
            this.lbtnConCode.Name = "lbtnConCode";
            this.lbtnConCode.Size = new System.Drawing.Size(26, 21);
            this.lbtnConCode.SkipValidationOnLeave = false;
            this.lbtnConCode.SPName = "CHRIS_SP_ConCod_CONTRACTER_MANAGER";
            this.lbtnConCode.TabIndex = 31;
            this.lbtnConCode.TabStop = false;
            this.lbtnConCode.UseVisualStyleBackColor = true;
            // 
            // txt_SP_CONTRA_EXP_DATE
            // 
            this.txt_SP_CONTRA_EXP_DATE.CustomEnabled = true;
            this.txt_SP_CONTRA_EXP_DATE.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_CONTRA_EXP_DATE.DataFieldMapping = "SP_CONTRA_EXP_DATE";
            this.txt_SP_CONTRA_EXP_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_CONTRA_EXP_DATE.HasChanges = true;
            this.txt_SP_CONTRA_EXP_DATE.IsRequired = true;
            this.txt_SP_CONTRA_EXP_DATE.Location = new System.Drawing.Point(185, 216);
            this.txt_SP_CONTRA_EXP_DATE.Name = "txt_SP_CONTRA_EXP_DATE";
            this.txt_SP_CONTRA_EXP_DATE.NullValue = " ";
            this.txt_SP_CONTRA_EXP_DATE.Size = new System.Drawing.Size(117, 20);
            this.txt_SP_CONTRA_EXP_DATE.TabIndex = 19;
            this.txt_SP_CONTRA_EXP_DATE.Value = new System.DateTime(2011, 1, 28, 0, 0, 0, 0);
            this.txt_SP_CONTRA_EXP_DATE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_CONTRA_EXP_DATE_Validating);
            // 
            // txt_SP_CONTRA_AGRE_DATE
            // 
            this.txt_SP_CONTRA_AGRE_DATE.CustomEnabled = true;
            this.txt_SP_CONTRA_AGRE_DATE.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_CONTRA_AGRE_DATE.DataFieldMapping = "SP_CONTRA_AGRE_DATE";
            this.txt_SP_CONTRA_AGRE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_CONTRA_AGRE_DATE.HasChanges = true;
            this.txt_SP_CONTRA_AGRE_DATE.IsRequired = true;
            this.txt_SP_CONTRA_AGRE_DATE.Location = new System.Drawing.Point(185, 190);
            this.txt_SP_CONTRA_AGRE_DATE.Name = "txt_SP_CONTRA_AGRE_DATE";
            this.txt_SP_CONTRA_AGRE_DATE.NullValue = " ";
            this.txt_SP_CONTRA_AGRE_DATE.Size = new System.Drawing.Size(117, 20);
            this.txt_SP_CONTRA_AGRE_DATE.TabIndex = 18;
            this.txt_SP_CONTRA_AGRE_DATE.Value = new System.DateTime(2011, 1, 28, 0, 0, 0, 0);
            // 
            // txt_SP_CONTRA_NID
            // 
            this.txt_SP_CONTRA_NID.AllowSpace = true;
            this.txt_SP_CONTRA_NID.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_NID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_NID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_NID.ContinuationTextBox = null;
            this.txt_SP_CONTRA_NID.CustomEnabled = true;
            this.txt_SP_CONTRA_NID.DataFieldMapping = "SP_CONTRA_NID";
            this.txt_SP_CONTRA_NID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_NID.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_NID.IsDate = false;
            this.txt_SP_CONTRA_NID.IsRequired = true;
            this.txt_SP_CONTRA_NID.Location = new System.Drawing.Point(185, 244);
            this.txt_SP_CONTRA_NID.MaxLength = 5543543;
            this.txt_SP_CONTRA_NID.Name = "txt_SP_CONTRA_NID";
            this.txt_SP_CONTRA_NID.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_NID.Postfix = "";
            this.txt_SP_CONTRA_NID.Prefix = "";
            this.txt_SP_CONTRA_NID.Size = new System.Drawing.Size(117, 20);
            this.txt_SP_CONTRA_NID.SkipValidation = false;
            this.txt_SP_CONTRA_NID.TabIndex = 20;
            this.txt_SP_CONTRA_NID.TextType = CrplControlLibrary.TextType.DigitAndHyphen;
            this.toolTip1.SetToolTip(this.txt_SP_CONTRA_NID, "Field must be of form 999-99-999999");
            this.txt_SP_CONTRA_NID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_SP_CONTRA_NID_KeyPress);
            this.txt_SP_CONTRA_NID.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_CONTRA_NID_Validating);
            // 
            // txt_SP_CONTRA_PHONE2
            // 
            this.txt_SP_CONTRA_PHONE2.AllowSpace = true;
            this.txt_SP_CONTRA_PHONE2.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_PHONE2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_PHONE2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_PHONE2.ContinuationTextBox = null;
            this.txt_SP_CONTRA_PHONE2.CustomEnabled = true;
            this.txt_SP_CONTRA_PHONE2.DataFieldMapping = "SP_CONTRA_PHONE2";
            this.txt_SP_CONTRA_PHONE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_PHONE2.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_PHONE2.IsDate = false;
            this.txt_SP_CONTRA_PHONE2.Location = new System.Drawing.Point(446, 164);
            this.txt_SP_CONTRA_PHONE2.MaxLength = 9;
            this.txt_SP_CONTRA_PHONE2.Name = "txt_SP_CONTRA_PHONE2";
            this.txt_SP_CONTRA_PHONE2.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_PHONE2.Postfix = "";
            this.txt_SP_CONTRA_PHONE2.Prefix = "";
            this.txt_SP_CONTRA_PHONE2.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_CONTRA_PHONE2.SkipValidation = false;
            this.txt_SP_CONTRA_PHONE2.TabIndex = 17;
            this.txt_SP_CONTRA_PHONE2.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_CONTRA_PHONE2.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_CONTRA_PHONE2_Validating);
            // 
            // txt_SP_CONTRA_PHONE1
            // 
            this.txt_SP_CONTRA_PHONE1.AllowSpace = true;
            this.txt_SP_CONTRA_PHONE1.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_PHONE1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_PHONE1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_PHONE1.ContinuationTextBox = null;
            this.txt_SP_CONTRA_PHONE1.CustomEnabled = true;
            this.txt_SP_CONTRA_PHONE1.DataFieldMapping = "SP_CONTRA_PHONE1";
            this.txt_SP_CONTRA_PHONE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_PHONE1.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_PHONE1.IsDate = false;
            this.txt_SP_CONTRA_PHONE1.Location = new System.Drawing.Point(185, 162);
            this.txt_SP_CONTRA_PHONE1.MaxLength = 9;
            this.txt_SP_CONTRA_PHONE1.Name = "txt_SP_CONTRA_PHONE1";
            this.txt_SP_CONTRA_PHONE1.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_PHONE1.Postfix = "";
            this.txt_SP_CONTRA_PHONE1.Prefix = "";
            this.txt_SP_CONTRA_PHONE1.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_CONTRA_PHONE1.SkipValidation = false;
            this.txt_SP_CONTRA_PHONE1.TabIndex = 16;
            this.txt_SP_CONTRA_PHONE1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(319, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "6) Telephone #2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(24, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(144, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "7) Agreement Start Date";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(24, 220);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "8) Expiry Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(24, 247);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "9) N.I.D Card No.";
            // 
            // txt_SP_CONTRA_ADD3
            // 
            this.txt_SP_CONTRA_ADD3.AllowSpace = true;
            this.txt_SP_CONTRA_ADD3.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_ADD3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_ADD3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_ADD3.ContinuationTextBox = null;
            this.txt_SP_CONTRA_ADD3.CustomEnabled = true;
            this.txt_SP_CONTRA_ADD3.DataFieldMapping = "SP_CONTRA_ADD3";
            this.txt_SP_CONTRA_ADD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_ADD3.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_ADD3.IsDate = false;
            this.txt_SP_CONTRA_ADD3.Location = new System.Drawing.Point(185, 136);
            this.txt_SP_CONTRA_ADD3.MaxLength = 20;
            this.txt_SP_CONTRA_ADD3.Name = "txt_SP_CONTRA_ADD3";
            this.txt_SP_CONTRA_ADD3.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_ADD3.Postfix = "";
            this.txt_SP_CONTRA_ADD3.Prefix = "";
            this.txt_SP_CONTRA_ADD3.Size = new System.Drawing.Size(117, 20);
            this.txt_SP_CONTRA_ADD3.SkipValidation = false;
            this.txt_SP_CONTRA_ADD3.TabIndex = 15;
            this.txt_SP_CONTRA_ADD3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_CONTRA_ADD2
            // 
            this.txt_SP_CONTRA_ADD2.AllowSpace = true;
            this.txt_SP_CONTRA_ADD2.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_ADD2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_ADD2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_ADD2.ContinuationTextBox = null;
            this.txt_SP_CONTRA_ADD2.CustomEnabled = true;
            this.txt_SP_CONTRA_ADD2.DataFieldMapping = "SP_CONTRA_ADD2";
            this.txt_SP_CONTRA_ADD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_ADD2.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_ADD2.IsDate = false;
            this.txt_SP_CONTRA_ADD2.Location = new System.Drawing.Point(185, 110);
            this.txt_SP_CONTRA_ADD2.MaxLength = 20;
            this.txt_SP_CONTRA_ADD2.Name = "txt_SP_CONTRA_ADD2";
            this.txt_SP_CONTRA_ADD2.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_ADD2.Postfix = "";
            this.txt_SP_CONTRA_ADD2.Prefix = "";
            this.txt_SP_CONTRA_ADD2.Size = new System.Drawing.Size(117, 20);
            this.txt_SP_CONTRA_ADD2.SkipValidation = false;
            this.txt_SP_CONTRA_ADD2.TabIndex = 14;
            this.txt_SP_CONTRA_ADD2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_CONTRA_ADD1
            // 
            this.txt_SP_CONTRA_ADD1.AllowSpace = true;
            this.txt_SP_CONTRA_ADD1.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_ADD1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_ADD1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_ADD1.ContinuationTextBox = null;
            this.txt_SP_CONTRA_ADD1.CustomEnabled = true;
            this.txt_SP_CONTRA_ADD1.DataFieldMapping = "SP_CONTRA_ADD1";
            this.txt_SP_CONTRA_ADD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_ADD1.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_ADD1.IsDate = false;
            this.txt_SP_CONTRA_ADD1.Location = new System.Drawing.Point(185, 84);
            this.txt_SP_CONTRA_ADD1.MaxLength = 20;
            this.txt_SP_CONTRA_ADD1.Name = "txt_SP_CONTRA_ADD1";
            this.txt_SP_CONTRA_ADD1.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_ADD1.Postfix = "";
            this.txt_SP_CONTRA_ADD1.Prefix = "";
            this.txt_SP_CONTRA_ADD1.Size = new System.Drawing.Size(117, 20);
            this.txt_SP_CONTRA_ADD1.SkipValidation = false;
            this.txt_SP_CONTRA_ADD1.TabIndex = 13;
            this.txt_SP_CONTRA_ADD1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_CONTRA_COMP_NAME
            // 
            this.txt_SP_CONTRA_COMP_NAME.AllowSpace = true;
            this.txt_SP_CONTRA_COMP_NAME.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_COMP_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_COMP_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_COMP_NAME.ContinuationTextBox = null;
            this.txt_SP_CONTRA_COMP_NAME.CustomEnabled = true;
            this.txt_SP_CONTRA_COMP_NAME.DataFieldMapping = "SP_CONTRA_COMP_NAME";
            this.txt_SP_CONTRA_COMP_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_COMP_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_COMP_NAME.IsDate = false;
            this.txt_SP_CONTRA_COMP_NAME.Location = new System.Drawing.Point(185, 60);
            this.txt_SP_CONTRA_COMP_NAME.MaxLength = 30;
            this.txt_SP_CONTRA_COMP_NAME.Name = "txt_SP_CONTRA_COMP_NAME";
            this.txt_SP_CONTRA_COMP_NAME.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_COMP_NAME.Postfix = "";
            this.txt_SP_CONTRA_COMP_NAME.Prefix = "";
            this.txt_SP_CONTRA_COMP_NAME.Size = new System.Drawing.Size(219, 20);
            this.txt_SP_CONTRA_COMP_NAME.SkipValidation = false;
            this.txt_SP_CONTRA_COMP_NAME.TabIndex = 12;
            this.txt_SP_CONTRA_COMP_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_CONTRA_NAME
            // 
            this.txt_SP_CONTRA_NAME.AllowSpace = true;
            this.txt_SP_CONTRA_NAME.AssociatedLookUpName = "";
            this.txt_SP_CONTRA_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_NAME.ContinuationTextBox = null;
            this.txt_SP_CONTRA_NAME.CustomEnabled = true;
            this.txt_SP_CONTRA_NAME.DataFieldMapping = "SP_CONTRA_NAME";
            this.txt_SP_CONTRA_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_NAME.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_NAME.IsDate = false;
            this.txt_SP_CONTRA_NAME.Location = new System.Drawing.Point(185, 35);
            this.txt_SP_CONTRA_NAME.MaxLength = 20;
            this.txt_SP_CONTRA_NAME.Name = "txt_SP_CONTRA_NAME";
            this.txt_SP_CONTRA_NAME.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_NAME.Postfix = "";
            this.txt_SP_CONTRA_NAME.Prefix = "";
            this.txt_SP_CONTRA_NAME.Size = new System.Drawing.Size(147, 20);
            this.txt_SP_CONTRA_NAME.SkipValidation = false;
            this.txt_SP_CONTRA_NAME.TabIndex = 11;
            this.txt_SP_CONTRA_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_CONTRA_CODE
            // 
            this.txt_SP_CONTRA_CODE.AllowSpace = true;
            this.txt_SP_CONTRA_CODE.AssociatedLookUpName = "lbtnConCode";
            this.txt_SP_CONTRA_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_CONTRA_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_CONTRA_CODE.ContinuationTextBox = null;
            this.txt_SP_CONTRA_CODE.CustomEnabled = true;
            this.txt_SP_CONTRA_CODE.DataFieldMapping = "SP_CONTRA_CODE";
            this.txt_SP_CONTRA_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_CONTRA_CODE.GetRecordsOnUpDownKeys = false;
            this.txt_SP_CONTRA_CODE.IsDate = false;
            this.txt_SP_CONTRA_CODE.Location = new System.Drawing.Point(185, 9);
            this.txt_SP_CONTRA_CODE.MaxLength = 3;
            this.txt_SP_CONTRA_CODE.Name = "txt_SP_CONTRA_CODE";
            this.txt_SP_CONTRA_CODE.NumberFormat = "###,###,##0.00";
            this.txt_SP_CONTRA_CODE.Postfix = "";
            this.txt_SP_CONTRA_CODE.Prefix = "";
            this.txt_SP_CONTRA_CODE.Size = new System.Drawing.Size(38, 20);
            this.txt_SP_CONTRA_CODE.SkipValidation = false;
            this.txt_SP_CONTRA_CODE.TabIndex = 10;
            this.txt_SP_CONTRA_CODE.TextType = CrplControlLibrary.TextType.String;
            this.txt_SP_CONTRA_CODE.Validated += new System.EventHandler(this.txt_SP_CONTRA_CODE_Validated);
            this.txt_SP_CONTRA_CODE.Leave += new System.EventHandler(this.txt_SP_CONTRA_CODE_Leave);
            this.txt_SP_CONTRA_CODE.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SP_CONTRA_CODE_Validating);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "1) Contractor Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "4) Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "3) Company Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "2) Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "5) Telephone #1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(414, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "UserName :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(476, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(19, 13);
            this.lblUserName.TabIndex = 14;
            this.lblUserName.Text = "    ";
            // 
            // CHRIS_Setup_ContractorCodeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(656, 481);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pnlContractorCode);
            this.Controls.Add(this.pnlHead);
            this.Name = "CHRIS_Setup_ContractorCodeEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "iCORE CHRIS :  Contractor Code Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlContractorCode, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlContractorCode.ResumeLayout(false);
            this.pnlContractorCode.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        public CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txt_W_OPTION_DIS;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txt_W_USER;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label lblUser;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlContractorCode;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_NID;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_PHONE2;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_PHONE1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_ADD3;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_ADD2;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_ADD1;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_COMP_NAME;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_NAME;
        private CrplControlLibrary.SLTextBox txt_SP_CONTRA_CODE;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker txt_SP_CONTRA_EXP_DATE;
        private CrplControlLibrary.SLDatePicker txt_SP_CONTRA_AGRE_DATE;
        private CrplControlLibrary.LookupButton lbtnConCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;

    }
}