using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_LoanSubsidyBenchMark : ChrisTabularForm
    {
        public CHRIS_Setup_LoanSubsidyBenchMark()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_LoanSubsidyBenchMark(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.dgvRecords.AutoGenerateColumns = false;
            this.FormTitleColor = Color.Black;
            this.lblUserName.Text = "User Name:    " + this.UserName;

            dgvRecords.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            txtOption.Visible = false;
            stsOptions.Visible = false;
            this.DoToolbarActions(this.pnlLoanTax.Controls, "Search");
            this.dgvRecords.Focus();
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            base.DoToolbarActions(ctrlsCollection, actionType);
            //if (actionType == "Cancel")
            //{
            //    base.DoToolbarActions(ctrlsCollection, "Search");
            //}
        }

        private void dgvRecords_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                //********************TaxYearFrom*******************************

                #region TaxFrom
                if (e.ColumnIndex == dgvRecords.Columns["TaxYearFrom"].Index && dgvRecords.CurrentCell.IsInEditMode)
                {
                    if (this.dgvRecords.CurrentCell.EditedFormattedValue != null && this.dgvRecords.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                    {
                        DateTime dtFrom = new DateTime();
                        if (DateTime.TryParse(this.dgvRecords.CurrentCell.EditedFormattedValue.ToString(), out dtFrom))
                        {
                            //1st Condition
                            DateTime dtTo = new DateTime();
                            if (dgvRecords["TaxYearTo", e.RowIndex].Value != DBNull.Value && DateTime.TryParse(this.dgvRecords["TaxYearTo", e.RowIndex].Value.ToString(), out dtTo))
                            {
                                if (dtTo <= dtFrom)
                                {
                                    MessageBox.Show("Tax Year FROM date should be lesser then TO date...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    e.Cancel = true;
                                    return;
                                }
                            }


                            //2nd Condition
                            string text = "#" + dtFrom.ToString("dd MMM yyyy") + "# >=TAX_YEAR_FROM AND #" + dtFrom.ToString("dd MMM yyyy") + "# <=TAX_YEAR_TO";
                            DataRow[] drows = null;
                            drows = (dgvRecords.DataSource as DataTable).Select(text);
                            if (drows != null && drows.Length > 0)
                            {
                                MessageBox.Show("Tax Percentage for this Year/Date has Already been Defined...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                                return;
                            }


                            //3rd Condition****************************-- <<<<<< BACKWARD CHECK <<<<<<***************************************
                            drows = null;
                            drows = (dgvRecords.DataSource as DataTable).Select("TAX_YEAR_FROM = MIN(TAX_YEAR_FROM)");
                            if (drows != null && drows.Length > 0)
                            {
                                if (Convert.ToDateTime(drows[0]["TAX_YEAR_FROM"].ToString()) > dtFrom)
                                {
                                    MessageBox.Show("Tax period current to this Date Already specified", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    e.Cancel = true;
                                    return;
                                }
                            }

                            //4th Condition****************************-- <<<<<< FORWARD CHECK ><<<<<***************************************
                            drows = null;
                            drows = (dgvRecords.DataSource as DataTable).Select("TAX_YEAR_TO = MAX(TAX_YEAR_TO)");
                            if (drows != null && drows.Length > 0)
                            {
                                DateTime dtMax = new DateTime();
                                DateTime.TryParse(drows[0]["TAX_YEAR_TO"].ToString(), out dtMax);
                                dtMax = dtMax.AddDays(1);
                                if (dtMax > dtFrom)
                                {
                                    MessageBox.Show("This date specification leaves GAP with previously defined Tax Year ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    e.Cancel = true;
                                    return;

                                }
                            }

                        }
                    }

                }
                #endregion

                //********************TaxYearTo*********************************

                #region TaxYearTo
                if (e.ColumnIndex == dgvRecords.Columns["TaxYearTo"].Index && dgvRecords.CurrentCell.IsInEditMode)
                {
                    if (this.dgvRecords.CurrentCell.EditedFormattedValue != null && this.dgvRecords.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                    {
                        DateTime dtTo = new DateTime();
                        if (DateTime.TryParse(this.dgvRecords.CurrentCell.EditedFormattedValue.ToString(), out dtTo))
                        {
                            //1st Condition
                            DateTime dtFrom = new DateTime();
                            if (dgvRecords["TaxYearFrom", e.RowIndex].Value != DBNull.Value && DateTime.TryParse(this.dgvRecords["TaxYearFrom", e.RowIndex].Value.ToString(), out dtFrom))
                            {
                                if (dtTo <= dtFrom)
                                {
                                    MessageBox.Show("Tax Year TO date should be greater then FROM date...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    e.Cancel = true;
                                    return;
                                }
                            }
                            //2nd Condition
                            string text = "#" + dtTo.ToString("dd MMM yyyy") + "# >=TAX_YEAR_FROM AND #" + dtTo.ToString("dd MMM yyyy") + "# <=TAX_YEAR_TO";
                            DataRow[] drows = null;
                            drows = (dgvRecords.DataSource as DataTable).Select(text);
                            if (drows != null && drows.Length > 0)
                            {
                                MessageBox.Show("Tax Percentage for this Year/Date has Already been Defined...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Cancel = true;
                                return;
                            }

                        }
                    }

                }

                #endregion

                //*******************TaxPercentage******************************

                #region TaxPercentage
                if (e.ColumnIndex == dgvRecords.Columns["TaxPercentage"].Index && dgvRecords.CurrentCell.IsInEditMode)
                {
                    DateTime dtFrom = new DateTime();
                    DateTime dtTo = new DateTime();
                    if (dgvRecords["TaxYearFrom", e.RowIndex].Value != DBNull.Value && DateTime.TryParse(this.dgvRecords["TaxYearFrom", e.RowIndex].Value.ToString(), out dtFrom) &&
                        dgvRecords["TaxYearTo", e.RowIndex].Value != DBNull.Value && DateTime.TryParse(this.dgvRecords["TaxYearTo", e.RowIndex].Value.ToString(), out dtTo))
                    {
                        if (dtTo <= dtFrom)
                        {
                            MessageBox.Show("Tax Year FROM and TO Dates must be specified Properly...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Tax Year FROM and TO Dates must be specified Properly...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                #endregion
            }
        }


    }
}