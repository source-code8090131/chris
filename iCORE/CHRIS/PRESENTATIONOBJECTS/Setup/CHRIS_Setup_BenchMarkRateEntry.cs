using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_BenchMarkRateEntry : ChrisMasterDetailForm
    {

        # region Variables
        Dictionary<string, object> colsNVals = new Dictionary<string, object>();
        DataTable dtBMR = new DataTable();
        Result rsltMain;
        CmnDataManager cmnDM = new CmnDataManager();
        Double refVar = 0;
        #endregion

        #region --Construstor--
        public CHRIS_Setup_BenchMarkRateEntry()
        {
            InitializeComponent();
        }


        public CHRIS_Setup_BenchMarkRateEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlTblBenchmark);
            this.pnlsimpleSearch.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlsimpleSearch);
            this.DefaultDataBlock = pnlTblBenchmark;
            this.CurrentPanelBlock = "pnlTblBenchmark";


        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            dtben_Date.CustomEnabled = false;
            dtben_Date.Select();
            dtben_Date.Focus();
            tbtDelete.Visible = true;
            lbtnbenchDate.Click += new EventHandler(lookupBtnDate_Click);
            slDtGrdViwBenchmark.Columns["BEN_RATE"].ValueType = typeof(double);
             
        }

        #endregion


        #region --Events--
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.ShowOptionTextBox = false;

            
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.txtUser.Visible = false;
            this.W_LOC.Visible = false;

            this.FunctionConfig.EnableF4 = true;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F4 = Function.Save;
            this.FunctionConfig.F6 = Function.Quit;
           // this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
         
            this.tbtList.Visible = false;
            this.txtUserName.Text = "User Name: " +this.UserName;
            dtben_Date.Select();
            dtben_Date.Focus();
            //this.BEN_FIN_TYPE.Width = 170;
            //this.BEN_RATE.Width = 110;
            //this.w_description.Width = 270;
            this.BEN_FIN_TYPE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.BEN_RATE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.w_description.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            slDtGrdViwBenchmark.AllowUserToAddRows = false;
        }

        private void lookupBtnDate_Click(object sender, EventArgs e)
        {

            InitializeFormObject(pnlsimpleSearch);
            base.IterateFormControls(pnlsimpleSearch.Controls, false, false, false);
            pnlsimpleSearch.LoadDependentPanels();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
        }

        

        protected override bool Quit()
        {
            if (slDtGrdViwBenchmark.Rows.Count > 1)
            {
                this.Cancel();
            }
            else
            {
                base.Quit();
            }
            return false;
        }







        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }


            if (actionType == "Close")
            {
                this.Close();
            }


            if (actionType == "Delete")
            {
                FunctionConfig.CurrentOption = Function.Delete;
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Clear();
                param.Add("ID", slDtGrdViwBenchmark.CurrentRow.Cells["ID"].EditedFormattedValue.ToString());

                rslt = cmnDM.GetData("CHRIS_SP_BENCHMARK_MANAGER", "Get", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult != null)
                    {
                        DialogResult response = MessageBox.Show("Are you sure you want to delete this record?", "Delete row?",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question,
                                                            MessageBoxDefaultButton.Button2);
                        if (response == DialogResult.No)
                        {
                            return;
                        }
                        else
                        {
                           
                            rslt = cmnDM.GetData("CHRIS_SP_BENCHMARK_MANAGER", "Delete", param);
                            if (rslt.isSuccessful)
                            {
                                slDtGrdViwBenchmark.DeleteSelectedGridRow();
                            }
                        }

                    }

                }  
                
                return;
            }

            if (actionType == "Save")
            {
                slDtGrdViwBenchmark.AllowUserToAddRows = false;
                foreach (DataGridViewRow benchMarkRow in this.slDtGrdViwBenchmark.Rows)
                {
                    benchMarkRow.Cells[2].Value = (benchMarkRow.Cells[2].EditedFormattedValue.ToString().Trim().Equals(String.Empty)) ? DBNull.Value : benchMarkRow.Cells[2].EditedFormattedValue;
                }

                ////this.slDtGrdViwBenchmark.CancelEdit();
                base.AllGridsEndEdit(slDtGrdViwBenchmark); 

                DataTable dt = (DataTable)slDtGrdViwBenchmark.DataSource;
                //foreach (DataRow dr in dt.Rows)
                //{
                //    dr[0] = dr[0];
                //}
               
                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                        slDtGrdViwBenchmark.GridSource = dt;

                        this.slDtGrdViwBenchmark.SaveGrid(this.pnlTblBenchmark);
                        MessageBox.Show("Record Updated Successfully");
                        //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
                        return;
                    }
                    else //in case of Blank BENCH_RATE //
                    {
                        slDtGrdViwBenchmark.GridSource = dt;
                        this.slDtGrdViwBenchmark.SaveGrid(this.pnlTblBenchmark);

                    }
                }
                return;

            }
            
            
            if (actionType == "Cancel")
            {
                this.dtben_Date.Enabled = true;
                this.ClearForm(pnlTblBenchmark.Controls);
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }




        private void slDtGrdViwBenchmark_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string sp_Desc = string.Empty;
            Double dr;
            if (e.ColumnIndex == 2)
            {
                if (slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode)
                {

                    //if (slDtGrdViwBenchmark.CurrentRow.Cells[2].EditedFormattedValue.ToString() == "")
                    //{
                    //    return;
                    //}  

                    if ((slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue != null || slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString() != string.Empty) && Double.TryParse(slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out refVar) )
                    {
                        //object obj = slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue;
                        //dr = Convert.ToDouble(obj);
                                              

                        dr = Convert.ToDouble(slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);

                        if (dr > 999.99)
                        {
                            MessageBox.Show("Field must be of form 999.99.");
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            if (dr != null)
                            {
                               slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].Value=Convert.ToDecimal(slDtGrdViwBenchmark.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue);
                            }
                            e.Cancel = false;
                        }
                    }
                    if (dtben_Date.Value != null)
                    {
                        slDtGrdViwBenchmark.Rows[e.RowIndex].Cells["Ben_Date"].Value = dtben_Date.Value;
                    }
                }

            }
        }

        #endregion

        private void dtben_Date_Leave(object sender, EventArgs e)
        {

            if (dtben_Date.Value != null )
            {

                colsNVals.Clear();
                colsNVals.Add("BEN_DATE", dtben_Date.Text);

                rsltMain = cmnDM.GetData("CHRIS_SP_BENCHMARK_MANAGER", "List", colsNVals);
                if (rsltMain.isSuccessful)
                {
                    if (rsltMain.dstResult.ExtendedProperties.Count > 0 || rsltMain.dstResult.Tables.Count > 0)
                    {
                        dtBMR = rsltMain.dstResult.Tables[0];
                        if (dtBMR.Rows.Count > 0)
                        {
                            slDtGrdViwBenchmark.DataSource = dtBMR;

                        }
                        else
                        {
                           // colsNVals.Clear();
                            rsltMain = cmnDM.GetData("CHRIS_SP_BENCHMARK_MANAGER", "PRC01", colsNVals);
                            if (rsltMain.isSuccessful)
                            {
                                dtBMR = rsltMain.dstResult.Tables[0];
                                if (dtBMR.Rows.Count > 0)
                                {
                                    slDtGrdViwBenchmark.DataSource = dtBMR;
                                    this.slDtGrdViwBenchmark.CancelEdit();
                                }
                                else
                                {
                                    slDtGrdViwBenchmark.DataSource = null;
                                    slDtGrdViwBenchmark.Refresh();

                                }
                            }

                        }
                    }

              }


              SearchBenchmarkCommand ent = (SearchBenchmarkCommand)this.pnlsimpleSearch.CurrentBusinessEntity;
              ent.BEN_DATE = Convert.ToDateTime(dtben_Date.Value);
          
            
            }
            else
            {
                MessageBox.Show("Date Must Be Entered..");
                dtben_Date.Focus();
            }



        }

    }
}