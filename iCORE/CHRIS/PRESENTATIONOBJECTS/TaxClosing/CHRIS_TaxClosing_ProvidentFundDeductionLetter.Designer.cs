namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_ProvidentFundDeductionLetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_ProvidentFundDeductionLetter));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.close = new CrplControlLibrary.SLButton();
            this.run = new CrplControlLibrary.SLButton();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.hr = new CrplControlLibrary.SLTextBox(this.components);
            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
            this.DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.w_desig = new CrplControlLibrary.SLTextBox(this.components);
            this.w_sig = new CrplControlLibrary.SLTextBox(this.components);
            this.wt_pno = new CrplControlLibrary.SLTextBox(this.components);
            this.wf_pno = new CrplControlLibrary.SLTextBox(this.components);
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker1 = new CrplControlLibrary.SLDatePicker(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.close);
            this.groupBox1.Controls.Add(this.run);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.hr);
            this.groupBox1.Controls.Add(this.w_brn);
            this.groupBox1.Controls.Add(this.DATE);
            this.groupBox1.Controls.Add(this.w_desig);
            this.groupBox1.Controls.Add(this.w_sig);
            this.groupBox1.Controls.Add(this.wt_pno);
            this.groupBox1.Controls.Add(this.wf_pno);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.Dest_format);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.slDatePicker1);
            this.groupBox1.Location = new System.Drawing.Point(13, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(478, 446);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(412, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 67);
            this.pictureBox1.TabIndex = 133;
            this.pictureBox1.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(110, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(224, 16);
            this.label13.TabIndex = 28;
            this.label13.Text = "Enter values for the parameters";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(147, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 16);
            this.label14.TabIndex = 27;
            this.label14.Text = "Report Parameters";
            // 
            // close
            // 
            this.close.ActionType = "";
            this.close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.close.Location = new System.Drawing.Point(319, 412);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 14;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click_1);
            // 
            // run
            // 
            this.run.ActionType = "";
            this.run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.run.Location = new System.Drawing.Point(238, 412);
            this.run.Name = "run";
            this.run.Size = new System.Drawing.Size(75, 23);
            this.run.TabIndex = 13;
            this.run.Text = "Run";
            this.run.UseVisualStyleBackColor = true;
            this.run.Click += new System.EventHandler(this.run_Click);
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(238, 120);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(152, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(42, 388);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Hr";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(42, 363);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "ENTER BRANCH";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(42, 338);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "DATE";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(42, 309);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "DESIGNATION";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(42, 282);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "SIGNEE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(42, 255);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "TO PERSONNEL NO.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(42, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "FROM PERSONNEL NO.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(42, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Copies";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(42, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Desformat";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(42, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Destname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Destype";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Yr";
            // 
            // hr
            // 
            this.hr.AllowSpace = true;
            this.hr.AssociatedLookUpName = "";
            this.hr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.hr.ContinuationTextBox = null;
            this.hr.CustomEnabled = true;
            this.hr.DataFieldMapping = "";
            this.hr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hr.GetRecordsOnUpDownKeys = false;
            this.hr.IsDate = false;
            this.hr.Location = new System.Drawing.Point(238, 386);
            this.hr.MaxLength = 30;
            this.hr.Name = "hr";
            this.hr.NumberFormat = "###,###,##0.00";
            this.hr.Postfix = "";
            this.hr.Prefix = "";
            this.hr.Size = new System.Drawing.Size(152, 20);
            this.hr.SkipValidation = false;
            this.hr.TabIndex = 12;
            this.hr.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_brn
            // 
            this.w_brn.AllowSpace = true;
            this.w_brn.AssociatedLookUpName = "";
            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_brn.ContinuationTextBox = null;
            this.w_brn.CustomEnabled = true;
            this.w_brn.DataFieldMapping = "";
            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_brn.GetRecordsOnUpDownKeys = false;
            this.w_brn.IsDate = false;
            this.w_brn.Location = new System.Drawing.Point(238, 361);
            this.w_brn.MaxLength = 3;
            this.w_brn.Name = "w_brn";
            this.w_brn.NumberFormat = "###,###,##0.00";
            this.w_brn.Postfix = "";
            this.w_brn.Prefix = "";
            this.w_brn.Size = new System.Drawing.Size(152, 20);
            this.w_brn.SkipValidation = false;
            this.w_brn.TabIndex = 11;
            this.w_brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // DATE
            // 
            this.DATE.CustomEnabled = true;
            this.DATE.CustomFormat = "dd/MM/yyyy";
            this.DATE.DataFieldMapping = "";
            this.DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DATE.HasChanges = true;
            this.DATE.Location = new System.Drawing.Point(238, 334);
            this.DATE.Name = "DATE";
            this.DATE.NullValue = " ";
            this.DATE.Size = new System.Drawing.Size(152, 20);
            this.DATE.TabIndex = 10;
            this.DATE.Value = new System.DateTime(2011, 1, 11, 0, 0, 0, 0);
            // 
            // w_desig
            // 
            this.w_desig.AllowSpace = true;
            this.w_desig.AssociatedLookUpName = "";
            this.w_desig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_desig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_desig.ContinuationTextBox = null;
            this.w_desig.CustomEnabled = true;
            this.w_desig.DataFieldMapping = "";
            this.w_desig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_desig.GetRecordsOnUpDownKeys = false;
            this.w_desig.IsDate = false;
            this.w_desig.Location = new System.Drawing.Point(238, 307);
            this.w_desig.MaxLength = 30;
            this.w_desig.Name = "w_desig";
            this.w_desig.NumberFormat = "###,###,##0.00";
            this.w_desig.Postfix = "";
            this.w_desig.Prefix = "";
            this.w_desig.Size = new System.Drawing.Size(152, 20);
            this.w_desig.SkipValidation = false;
            this.w_desig.TabIndex = 9;
            this.w_desig.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_sig
            // 
            this.w_sig.AllowSpace = true;
            this.w_sig.AssociatedLookUpName = "";
            this.w_sig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_sig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_sig.ContinuationTextBox = null;
            this.w_sig.CustomEnabled = true;
            this.w_sig.DataFieldMapping = "";
            this.w_sig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_sig.GetRecordsOnUpDownKeys = false;
            this.w_sig.IsDate = false;
            this.w_sig.Location = new System.Drawing.Point(238, 280);
            this.w_sig.MaxLength = 20;
            this.w_sig.Name = "w_sig";
            this.w_sig.NumberFormat = "###,###,##0.00";
            this.w_sig.Postfix = "";
            this.w_sig.Prefix = "";
            this.w_sig.Size = new System.Drawing.Size(152, 20);
            this.w_sig.SkipValidation = false;
            this.w_sig.TabIndex = 8;
            this.w_sig.TextType = CrplControlLibrary.TextType.String;
            // 
            // wt_pno
            // 
            this.wt_pno.AllowSpace = true;
            this.wt_pno.AssociatedLookUpName = "";
            this.wt_pno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wt_pno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wt_pno.ContinuationTextBox = null;
            this.wt_pno.CustomEnabled = true;
            this.wt_pno.DataFieldMapping = "";
            this.wt_pno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wt_pno.GetRecordsOnUpDownKeys = false;
            this.wt_pno.IsDate = false;
            this.wt_pno.Location = new System.Drawing.Point(238, 253);
            this.wt_pno.MaxLength = 6;
            this.wt_pno.Name = "wt_pno";
            this.wt_pno.NumberFormat = "###,###,##0.00";
            this.wt_pno.Postfix = "";
            this.wt_pno.Prefix = "";
            this.wt_pno.Size = new System.Drawing.Size(152, 20);
            this.wt_pno.SkipValidation = false;
            this.wt_pno.TabIndex = 7;
            this.wt_pno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // wf_pno
            // 
            this.wf_pno.AllowSpace = true;
            this.wf_pno.AssociatedLookUpName = "";
            this.wf_pno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wf_pno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wf_pno.ContinuationTextBox = null;
            this.wf_pno.CustomEnabled = true;
            this.wf_pno.DataFieldMapping = "";
            this.wf_pno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wf_pno.GetRecordsOnUpDownKeys = false;
            this.wf_pno.IsDate = false;
            this.wf_pno.Location = new System.Drawing.Point(238, 226);
            this.wf_pno.MaxLength = 6;
            this.wf_pno.Name = "wf_pno";
            this.wf_pno.NumberFormat = "###,###,##0.00";
            this.wf_pno.Postfix = "";
            this.wf_pno.Prefix = "";
            this.wf_pno.Size = new System.Drawing.Size(152, 20);
            this.wf_pno.SkipValidation = false;
            this.wf_pno.TabIndex = 6;
            this.wf_pno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(238, 199);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(152, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 5;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_format
            // 
            this.Dest_format.AllowSpace = true;
            this.Dest_format.AssociatedLookUpName = "";
            this.Dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_format.ContinuationTextBox = null;
            this.Dest_format.CustomEnabled = true;
            this.Dest_format.DataFieldMapping = "";
            this.Dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_format.GetRecordsOnUpDownKeys = false;
            this.Dest_format.IsDate = false;
            this.Dest_format.Location = new System.Drawing.Point(238, 173);
            this.Dest_format.MaxLength = 4;
            this.Dest_format.Name = "Dest_format";
            this.Dest_format.NumberFormat = "###,###,##0.00";
            this.Dest_format.Postfix = "";
            this.Dest_format.Prefix = "";
            this.Dest_format.Size = new System.Drawing.Size(152, 20);
            this.Dest_format.SkipValidation = false;
            this.Dest_format.TabIndex = 4;
            this.Dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.AutoCompleteCustomSource.AddRange(new string[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(238, 147);
            this.Dest_name.MaxLength = 78;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(152, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 3;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker1
            // 
            this.slDatePicker1.CustomEnabled = true;
            this.slDatePicker1.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker1.DataFieldMapping = "";
            this.slDatePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker1.HasChanges = true;
            this.slDatePicker1.Location = new System.Drawing.Point(238, 95);
            this.slDatePicker1.Name = "slDatePicker1";
            this.slDatePicker1.NullValue = " ";
            this.slDatePicker1.Size = new System.Drawing.Size(152, 20);
            this.slDatePicker1.TabIndex = 1;
            this.slDatePicker1.Value = new System.DateTime(2011, 1, 11, 0, 0, 0, 0);
            // 
            // CHRIS_TaxClosing_ProvidentFundDeductionLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 497);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_TaxClosing_ProvidentFundDeductionLetter";
            this.Text = "iCORE CHRIS - Provident Fund Deduction Letter";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLDatePicker slDatePicker1;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox hr;
        private CrplControlLibrary.SLTextBox w_brn;
        private CrplControlLibrary.SLDatePicker DATE;
        private CrplControlLibrary.SLTextBox w_desig;
        private CrplControlLibrary.SLTextBox w_sig;
        private CrplControlLibrary.SLTextBox wt_pno;
        private CrplControlLibrary.SLTextBox wf_pno;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox Dest_format;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLButton close;
        private CrplControlLibrary.SLButton run;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}