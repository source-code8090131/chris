using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_TaxAssessmentStatus : ChrisMasterDetailForm
    {
        #region  Constructors

        public CHRIS_TaxClosing_TaxAssessmentStatus()
        {
            InitializeComponent();
        }

        public CHRIS_TaxClosing_TaxAssessmentStatus(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPanelDetail);
            slPanelMasterQuery.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(slPanelMasterQuery);

        }

        #endregion

        #region Overridden Methods/Handlers

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.txt_PR_P_NO.Text = "";
                base.DoToolbarActions(ctrlsCollection, actionType);
                this.txt_location.Text = this.CurrentLocation;
                this.txt_username.Text = this.userID;
                this.txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                return;
            }
            if (actionType == "Save")
            {
                if (this.operationMode == Mode.Edit)//this.FunctionConfig.CurrentOption != Function.None)
                {
                    //base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    this.Cancel();
                }

                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        protected override void OnLoad(EventArgs e)
        {
           
            base.OnLoad(e);
            this.txt_location.Text = this.CurrentLocation;
            this.txt_username.Text = this.userID;
            lblUserName.Text += "   " + this.UserName;
            this.txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            tbtAdd.Visible = false;
            //tbtCancel.Visible = false;
            ////tbtClose.Visible = false;
            //tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            this.lookup_Pr_P_No.Enabled = false;
            this.ShowF10Option = true;
            this.F10OptionText = "[F10]=Save";
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.ShowF1Option = true;
            this.F1OptionText = "Start Entry";
            this.FunctionConfig.EnableF1 = true;
            this.FunctionConfig.F1 = Function.Add;

            this.FunctionConfig.EnableF2 = false;
            this.FunctionConfig.EnableF3 = false;
            this.FunctionConfig.EnableF4 = false;
            this.FunctionConfig.EnableF5 = false;
            this.FunctionConfig.EnableF7 = false;
            this.FunctionConfig.EnableF8 = false;

            this.dtpJoining.Value = null;

            this.CanEnableDisableControls = true;
            
            //tbtSave.Visible = false;


        }
        protected override bool Quit()
        {
            txt_PR_P_NO.IsRequired = false;
            if (dgvDetails.CurrentCell != null)
                if (dgvDetails.CurrentCell.IsInEditMode)
                {
                    dgvDetails.CancelEdit();
                }
            base.Quit();
            this.txtOption.Focus();
            txt_PR_P_NO.IsRequired = true;
            //bool flag = false;
            //if (this.FunctionConfig.CurrentOption == Function.None)
            //{
            //    if (base.tlbMain.Items.Count > 0)
            //    {
            //        base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
            //        //base.Close();
            //        //this.Dispose(true);
            //    }
            //    //else

            //}
            //else
            //{
            //    //this.FunctionConfig.CurrentOption = Function.None;
            //    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            //}
            //return flag;
            return false;
        }
        protected override bool Save()
        {
            bool flag = false;

            if (this.operationMode == Mode.Edit)//this.FunctionConfig.CurrentOption != Function.None)
            {
                base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
                //this.ClearForm(this.slPanelMasterQuery.Controls);
                //this.slPanelMasterQuery.ClearBusinessEntity();
                //this.slPanelMasterQuery.ClearDependentPanels();
                //this.txtOption.Text = "";
                //this.FunctionConfig.CurrentOption = Function.None;
                //this.txtOption.Select();
                //this.txtOption.Focus();
            }
            this.Cancel();
            return flag;
        }
        protected override bool Add()
        {
            bool flag = false;
            this.lookup_Pr_P_No.Enabled = true;
            this.operationMode = Mode.Add;
            this.FunctionConfig.CurrentOption = Function.Add;
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Entry";
            this.txtOption.Text = "S";

            return flag;
        }

        #endregion

        #region Events Handlers
        
        private void dgvDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                if (dgvDetails.CurrentCell.ColumnIndex > 0)
                {
                    ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
                }
            }
        }
        private void dgvDetails_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (dgvDetails.CurrentCell == null || !dgvDetails.CurrentCell.IsInEditMode)
                return;

            if (e.ColumnIndex == 3 || e.ColumnIndex == 4)
            {
                if (e.ColumnIndex == dgvDetails.Columns["col_ETS_ASST_IT"].Index || e.ColumnIndex == dgvDetails.Columns["col_ETS_ASST_WT"].Index)
                {

                    if (dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue != null)
                    {

                        if (dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper() == "YES" || dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper() == "NO" || dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper() == "N/A")
                        {
                            dgvDetails[e.ColumnIndex, e.RowIndex].Value = dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper();
                        }
                        else
                        {
                            MessageBox.Show("Valid Assessment Order Tax is [Yes],[No] or [N/A]....", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            e.Cancel = true;

                        }

                    }
                    //else
                    //{
                    //    MessageBox.Show("Valid Assessment Order Tax is [Yes],[No] or [N/A]....", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    //    e.Cancel = true;
                    //}
                }
            }

            //if (e.RowIndex > -1 && e.ColumnIndex > 2)
            //{
            //    if (e.ColumnIndex == dgvDetails.Columns["col_ETS_ASST_IT"].Index || e.ColumnIndex == dgvDetails.Columns["col_ETS_ASST_WT"].Index)
            //    {

            //        if (dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue != null && dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString() != string.Empty)
            //        {

            //            if (dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper() == "YES" || dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper() == "NO" || dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper() == "N/A")
            //            {
            //                dgvDetails[e.ColumnIndex, e.RowIndex].Value = dgvDetails[e.ColumnIndex, e.RowIndex].EditedFormattedValue.ToString().ToUpper();
            //            }
            //            else
            //            {
            //                MessageBox.Show("Valid Assessment Order Tax is [Yes],[No] or [N/A]....", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            //                e.Cancel = true;

            //            }

            //        }
            //        //else
            //        //{
            //        //    MessageBox.Show("Valid Assessment Order Tax is [Yes],[No] or [N/A]....", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            //        //    e.Cancel = true;
            //        //}
            //    }
            //}
        }
        private void dgvDetails_Enter(object sender, EventArgs e)
        {
            if (dgvDetails.Rows.Count > 0)
            {
                dgvDetails.CurrentCell = dgvDetails.Rows[0].Cells[2];
                dgvDetails.CurrentCell.Selected = true;
            }
        }

        private void txtNtnCertRcvd_Validating(object sender, CancelEventArgs e)
        {
            if (txtNtnCertRcvd.Text.ToUpper() == "Y" || txtNtnCertRcvd.Text.ToUpper() == "N")
            {
                txtNtnCertRcvd.Text = txtNtnCertRcvd.Text.ToUpper();
            }
            else
            {
                MessageBox.Show("Enter N.T.N. Certificate Received [Y]es or [N]o.....", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }
        private void txtCardRcvd_Validating(object sender, CancelEventArgs e)
        {
            if (txtCardRcvd.Text.ToUpper() == "Y" || txtCardRcvd.Text.ToUpper() == "N")
            {
                txtCardRcvd.Text = txtCardRcvd.Text.ToUpper();
            }
            else
            {
                MessageBox.Show("Enter N.T.N. Card Received [Y]es or [N]o.....", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
           
        }
        private void txtCardRcvd_Validated(object sender, EventArgs e)
        {
            if (dgvDetails[3, 0] != null)
                dgvDetails.CurrentCell = dgvDetails[3, 0];
        }

        private void lookup_Pr_P_No_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                this.lookup_Pr_P_No.Enabled = false;
                this.lookup_Pr_P_No.Enabled = true;
            }
        }

        #endregion

    }
}