namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_DeductionLetterReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_DeductionLetterReport));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.fn_to_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.fn_from_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.FDESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.CLOSE = new CrplControlLibrary.SLButton();
            this.RUN = new CrplControlLibrary.SLButton();
            this.label13 = new System.Windows.Forms.Label();
            this.W_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            this.W_EPNO = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.W_SPNO = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.FNAME = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.Dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.fn_to_date);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.fn_from_date);
            this.groupBox1.Controls.Add(this.FDESIG);
            this.groupBox1.Controls.Add(this.CLOSE);
            this.groupBox1.Controls.Add(this.RUN);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.W_DATE);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.W_BRN);
            this.groupBox1.Controls.Add(this.W_EPNO);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.W_SPNO);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.FNAME);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Dest_format);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 429);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(34, 360);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 138;
            this.label14.Text = "Fn To Date";
            // 
            // fn_to_date
            // 
            this.fn_to_date.CustomEnabled = true;
            this.fn_to_date.CustomFormat = "DD/MM/YYYY";
            this.fn_to_date.DataFieldMapping = "";
            this.fn_to_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fn_to_date.HasChanges = true;
            this.fn_to_date.Location = new System.Drawing.Point(236, 356);
            this.fn_to_date.Name = "fn_to_date";
            this.fn_to_date.NullValue = " ";
            this.fn_to_date.Size = new System.Drawing.Size(168, 20);
            this.fn_to_date.TabIndex = 11;
            this.fn_to_date.Value = new System.DateTime(2011, 1, 25, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(34, 334);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 136;
            this.label12.Text = "Fn From Date";
            // 
            // fn_from_date
            // 
            this.fn_from_date.CustomEnabled = true;
            this.fn_from_date.CustomFormat = "DD/MM/YYYY";
            this.fn_from_date.DataFieldMapping = "";
            this.fn_from_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fn_from_date.HasChanges = true;
            this.fn_from_date.Location = new System.Drawing.Point(236, 330);
            this.fn_from_date.Name = "fn_from_date";
            this.fn_from_date.NullValue = " ";
            this.fn_from_date.Size = new System.Drawing.Size(168, 20);
            this.fn_from_date.TabIndex = 10;
            this.fn_from_date.Value = new System.DateTime(2011, 1, 25, 0, 0, 0, 0);
            // 
            // FDESIG
            // 
            this.FDESIG.AllowSpace = true;
            this.FDESIG.AssociatedLookUpName = "";
            this.FDESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FDESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FDESIG.ContinuationTextBox = null;
            this.FDESIG.CustomEnabled = true;
            this.FDESIG.DataFieldMapping = "";
            this.FDESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FDESIG.GetRecordsOnUpDownKeys = false;
            this.FDESIG.IsDate = false;
            this.FDESIG.Location = new System.Drawing.Point(236, 200);
            this.FDESIG.MaxLength = 50;
            this.FDESIG.Name = "FDESIG";
            this.FDESIG.NumberFormat = "###,###,##0.00";
            this.FDESIG.Postfix = "";
            this.FDESIG.Prefix = "";
            this.FDESIG.Size = new System.Drawing.Size(168, 20);
            this.FDESIG.SkipValidation = false;
            this.FDESIG.TabIndex = 5;
            this.FDESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // CLOSE
            // 
            this.CLOSE.ActionType = "";
            this.CLOSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.CLOSE.Location = new System.Drawing.Point(316, 387);
            this.CLOSE.Name = "CLOSE";
            this.CLOSE.Size = new System.Drawing.Size(75, 23);
            this.CLOSE.TabIndex = 13;
            this.CLOSE.Text = "Close";
            this.CLOSE.UseVisualStyleBackColor = true;
            this.CLOSE.Click += new System.EventHandler(this.CLOSE_Click);
            // 
            // RUN
            // 
            this.RUN.ActionType = "";
            this.RUN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.RUN.Location = new System.Drawing.Point(236, 387);
            this.RUN.Name = "RUN";
            this.RUN.Size = new System.Drawing.Size(75, 23);
            this.RUN.TabIndex = 12;
            this.RUN.Text = "Run";
            this.RUN.UseVisualStyleBackColor = true;
            this.RUN.Click += new System.EventHandler(this.RUN_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(34, 282);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "DATE             :";
            // 
            // W_DATE
            // 
            this.W_DATE.CustomEnabled = true;
            this.W_DATE.CustomFormat = "DD/MM/YYYY";
            this.W_DATE.DataFieldMapping = "";
            this.W_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.W_DATE.HasChanges = true;
            this.W_DATE.Location = new System.Drawing.Point(236, 278);
            this.W_DATE.Name = "W_DATE";
            this.W_DATE.NullValue = " ";
            this.W_DATE.Size = new System.Drawing.Size(168, 20);
            this.W_DATE.TabIndex = 8;
            this.W_DATE.Value = new System.DateTime(2011, 1, 25, 0, 0, 0, 0);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(34, 304);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "ENTER BRANCH       :";
            // 
            // W_BRN
            // 
            this.W_BRN.AllowSpace = true;
            this.W_BRN.AssociatedLookUpName = "";
            this.W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRN.ContinuationTextBox = null;
            this.W_BRN.CustomEnabled = true;
            this.W_BRN.DataFieldMapping = "";
            this.W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRN.GetRecordsOnUpDownKeys = false;
            this.W_BRN.IsDate = false;
            this.W_BRN.Location = new System.Drawing.Point(236, 304);
            this.W_BRN.MaxLength = 3;
            this.W_BRN.Name = "W_BRN";
            this.W_BRN.NumberFormat = "###,###,##0.00";
            this.W_BRN.Postfix = "";
            this.W_BRN.Prefix = "";
            this.W_BRN.Size = new System.Drawing.Size(168, 20);
            this.W_BRN.SkipValidation = false;
            this.W_BRN.TabIndex = 9;
            this.W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_EPNO
            // 
            this.W_EPNO.AllowSpace = true;
            this.W_EPNO.AssociatedLookUpName = "";
            this.W_EPNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_EPNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_EPNO.ContinuationTextBox = null;
            this.W_EPNO.CustomEnabled = true;
            this.W_EPNO.DataFieldMapping = "";
            this.W_EPNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_EPNO.GetRecordsOnUpDownKeys = false;
            this.W_EPNO.IsDate = false;
            this.W_EPNO.Location = new System.Drawing.Point(236, 252);
            this.W_EPNO.MaxLength = 6;
            this.W_EPNO.Name = "W_EPNO";
            this.W_EPNO.NumberFormat = "###,###,##0.00";
            this.W_EPNO.Postfix = "";
            this.W_EPNO.Prefix = "";
            this.W_EPNO.Size = new System.Drawing.Size(168, 20);
            this.W_EPNO.SkipValidation = false;
            this.W_EPNO.TabIndex = 7;
            this.W_EPNO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(34, 254);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "END PERSONNEL NO   :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "START PERSONNEL NO  :";
            // 
            // W_SPNO
            // 
            this.W_SPNO.AllowSpace = true;
            this.W_SPNO.AssociatedLookUpName = "";
            this.W_SPNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SPNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SPNO.ContinuationTextBox = null;
            this.W_SPNO.CustomEnabled = true;
            this.W_SPNO.DataFieldMapping = "";
            this.W_SPNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SPNO.GetRecordsOnUpDownKeys = false;
            this.W_SPNO.IsDate = false;
            this.W_SPNO.Location = new System.Drawing.Point(236, 226);
            this.W_SPNO.MaxLength = 6;
            this.W_SPNO.Name = "W_SPNO";
            this.W_SPNO.NumberFormat = "###,###,##0.00";
            this.W_SPNO.Postfix = "";
            this.W_SPNO.Prefix = "";
            this.W_SPNO.Size = new System.Drawing.Size(168, 20);
            this.W_SPNO.SkipValidation = false;
            this.W_SPNO.TabIndex = 6;
            this.W_SPNO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "DESIGNATION  :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(34, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "ENTER FROM NAME  :";
            // 
            // FNAME
            // 
            this.FNAME.AllowSpace = true;
            this.FNAME.AssociatedLookUpName = "";
            this.FNAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FNAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FNAME.ContinuationTextBox = null;
            this.FNAME.CustomEnabled = true;
            this.FNAME.DataFieldMapping = "";
            this.FNAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FNAME.GetRecordsOnUpDownKeys = false;
            this.FNAME.IsDate = false;
            this.FNAME.Location = new System.Drawing.Point(236, 173);
            this.FNAME.MaxLength = 50;
            this.FNAME.Name = "FNAME";
            this.FNAME.NumberFormat = "###,###,##0.00";
            this.FNAME.Postfix = "";
            this.FNAME.Prefix = "";
            this.FNAME.Size = new System.Drawing.Size(168, 20);
            this.FNAME.SkipValidation = false;
            this.FNAME.TabIndex = 4;
            this.FNAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Number of Copies";
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(236, 146);
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(168, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 3;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Destination Format";
            // 
            // Dest_format
            // 
            this.Dest_format.AllowSpace = true;
            this.Dest_format.AssociatedLookUpName = "";
            this.Dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_format.ContinuationTextBox = null;
            this.Dest_format.CustomEnabled = true;
            this.Dest_format.DataFieldMapping = "";
            this.Dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_format.GetRecordsOnUpDownKeys = false;
            this.Dest_format.IsDate = false;
            this.Dest_format.Location = new System.Drawing.Point(236, 120);
            this.Dest_format.Name = "Dest_format";
            this.Dest_format.NumberFormat = "###,###,##0.00";
            this.Dest_format.Postfix = "";
            this.Dest_format.Prefix = "";
            this.Dest_format.Size = new System.Drawing.Size(168, 20);
            this.Dest_format.SkipValidation = false;
            this.Dest_format.TabIndex = 2;
            this.Dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destination Type";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(236, 94);
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(168, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(236, 67);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(168, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(179, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(193, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Enter Parameter for Report\r\n";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(211, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Report Parameter";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(447, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 139;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_TaxClosing_DeductionLetterReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 483);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_TaxClosing_DeductionLetterReport";
            this.Text = "CHRIS_TaxClosing_DeductionLetterReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLButton CLOSE;
        private CrplControlLibrary.SLButton RUN;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLDatePicker W_DATE;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox W_BRN;
        private CrplControlLibrary.SLTextBox W_EPNO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox W_SPNO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox FNAME;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox copies;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox Dest_format;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLDatePicker fn_to_date;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLDatePicker fn_from_date;
        private CrplControlLibrary.SLTextBox FDESIG;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}