namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_SalaryCertificate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_SalaryCertificate));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CLOSE = new CrplControlLibrary.SLButton();
            this.RUN = new CrplControlLibrary.SLButton();
            this.label13 = new System.Windows.Forms.Label();
            this.DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.SIGN = new CrplControlLibrary.SLTextBox(this.components);
            this.TPNO = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.FPNO = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.MODE = new CrplControlLibrary.SLComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.Dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.CLOSE);
            this.groupBox1.Controls.Add(this.RUN);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.DATE);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.DESIG);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.SIGN);
            this.groupBox1.Controls.Add(this.TPNO);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.FPNO);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.MODE);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.BRANCH);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Dest_format);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(520, 429);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(456, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 65);
            this.pictureBox1.TabIndex = 133;
            this.pictureBox1.TabStop = false;
            // 
            // CLOSE
            // 
            this.CLOSE.ActionType = "";
            this.CLOSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.CLOSE.Location = new System.Drawing.Point(341, 367);
            this.CLOSE.Name = "CLOSE";
            this.CLOSE.Size = new System.Drawing.Size(75, 23);
            this.CLOSE.TabIndex = 44;
            this.CLOSE.Text = "Close";
            this.CLOSE.UseVisualStyleBackColor = true;
            this.CLOSE.Click += new System.EventHandler(this.CLOSE_Click_1);
            // 
            // RUN
            // 
            this.RUN.ActionType = "";
            this.RUN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.RUN.Location = new System.Drawing.Point(261, 367);
            this.RUN.Name = "RUN";
            this.RUN.Size = new System.Drawing.Size(75, 23);
            this.RUN.TabIndex = 43;
            this.RUN.Text = "Run";
            this.RUN.UseVisualStyleBackColor = true;
            this.RUN.Click += new System.EventHandler(this.RUN_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(40, 335);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(132, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "DATE                      :";
            // 
            // DATE
            // 
            this.DATE.CustomEnabled = true;
            this.DATE.CustomFormat = "DD/MM/YYYY";
            this.DATE.DataFieldMapping = "";
            this.DATE.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DATE.HasChanges = true;
            this.DATE.Location = new System.Drawing.Point(261, 331);
            this.DATE.Name = "DATE";
            this.DATE.NullValue = " ";
            this.DATE.Size = new System.Drawing.Size(168, 20);
            this.DATE.TabIndex = 41;
            this.DATE.Value = new System.DateTime(2011, 1, 25, 9, 27, 53, 731);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(40, 307);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(156, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "DESIGNATION               :";
            // 
            // DESIG
            // 
            this.DESIG.AllowSpace = true;
            this.DESIG.AssociatedLookUpName = "";
            this.DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DESIG.ContinuationTextBox = null;
            this.DESIG.CustomEnabled = true;
            this.DESIG.DataFieldMapping = "";
            this.DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DESIG.GetRecordsOnUpDownKeys = false;
            this.DESIG.IsDate = false;
            this.DESIG.Location = new System.Drawing.Point(261, 305);
            this.DESIG.Name = "DESIG";
            this.DESIG.NumberFormat = "###,###,##0.00";
            this.DESIG.Postfix = "";
            this.DESIG.Prefix = "";
            this.DESIG.Size = new System.Drawing.Size(168, 20);
            this.DESIG.SkipValidation = false;
            this.DESIG.TabIndex = 39;
            this.DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(40, 280);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "SIGNEE                   :";
            // 
            // SIGN
            // 
            this.SIGN.AllowSpace = true;
            this.SIGN.AssociatedLookUpName = "";
            this.SIGN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SIGN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SIGN.ContinuationTextBox = null;
            this.SIGN.CustomEnabled = true;
            this.SIGN.DataFieldMapping = "";
            this.SIGN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SIGN.GetRecordsOnUpDownKeys = false;
            this.SIGN.IsDate = false;
            this.SIGN.Location = new System.Drawing.Point(261, 278);
            this.SIGN.Name = "SIGN";
            this.SIGN.NumberFormat = "###,###,##0.00";
            this.SIGN.Postfix = "";
            this.SIGN.Prefix = "";
            this.SIGN.Size = new System.Drawing.Size(168, 20);
            this.SIGN.SkipValidation = false;
            this.SIGN.TabIndex = 37;
            this.SIGN.TextType = CrplControlLibrary.TextType.String;
            // 
            // TPNO
            // 
            this.TPNO.AllowSpace = true;
            this.TPNO.AssociatedLookUpName = "";
            this.TPNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TPNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TPNO.ContinuationTextBox = null;
            this.TPNO.CustomEnabled = true;
            this.TPNO.DataFieldMapping = "";
            this.TPNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TPNO.GetRecordsOnUpDownKeys = false;
            this.TPNO.IsDate = false;
            this.TPNO.Location = new System.Drawing.Point(261, 254);
            this.TPNO.MaxLength = 6;
            this.TPNO.Name = "TPNO";
            this.TPNO.NumberFormat = "###,###,##0.00";
            this.TPNO.Postfix = "";
            this.TPNO.Prefix = "";
            this.TPNO.Size = new System.Drawing.Size(168, 20);
            this.TPNO.SkipValidation = false;
            this.TPNO.TabIndex = 36;
            this.TPNO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(40, 256);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(189, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "PERSONNEL NO. TO OR ALL  :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(207, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "PERSONNEL NO. FROM OR ALL  :";
            // 
            // FPNO
            // 
            this.FPNO.AllowSpace = true;
            this.FPNO.AssociatedLookUpName = "";
            this.FPNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FPNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FPNO.ContinuationTextBox = null;
            this.FPNO.CustomEnabled = true;
            this.FPNO.DataFieldMapping = "";
            this.FPNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FPNO.GetRecordsOnUpDownKeys = false;
            this.FPNO.IsDate = false;
            this.FPNO.Location = new System.Drawing.Point(261, 228);
            this.FPNO.MaxLength = 6;
            this.FPNO.Name = "FPNO";
            this.FPNO.NumberFormat = "###,###,##0.00";
            this.FPNO.Postfix = "";
            this.FPNO.Prefix = "";
            this.FPNO.Size = new System.Drawing.Size(168, 20);
            this.FPNO.SkipValidation = false;
            this.FPNO.TabIndex = 33;
            this.FPNO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Mode";
            // 
            // MODE
            // 
            this.MODE.BusinessEntity = "";
            this.MODE.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.MODE.CustomEnabled = true;
            this.MODE.DataFieldMapping = "";
            this.MODE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MODE.FormattingEnabled = true;
            this.MODE.Items.AddRange(new object[] {
            "Default",
            "Bitmap",
            "Character"});
            this.MODE.Location = new System.Drawing.Point(261, 202);
            this.MODE.LOVType = "";
            this.MODE.Name = "MODE";
            this.MODE.Size = new System.Drawing.Size(168, 21);
            this.MODE.SPName = "";
            this.MODE.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "BRANCH CODE OR ALL      :";
            // 
            // BRANCH
            // 
            this.BRANCH.AllowSpace = true;
            this.BRANCH.AssociatedLookUpName = "";
            this.BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BRANCH.ContinuationTextBox = null;
            this.BRANCH.CustomEnabled = true;
            this.BRANCH.DataFieldMapping = "";
            this.BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRANCH.GetRecordsOnUpDownKeys = false;
            this.BRANCH.IsDate = false;
            this.BRANCH.Location = new System.Drawing.Point(261, 175);
            this.BRANCH.MaxLength = 3;
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.NumberFormat = "###,###,##0.00";
            this.BRANCH.Postfix = "";
            this.BRANCH.Prefix = "";
            this.BRANCH.Size = new System.Drawing.Size(168, 20);
            this.BRANCH.SkipValidation = false;
            this.BRANCH.TabIndex = 29;
            this.BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Copies";
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(261, 148);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(168, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 27;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Desformat";
            // 
            // Dest_format
            // 
            this.Dest_format.AllowSpace = true;
            this.Dest_format.AssociatedLookUpName = "";
            this.Dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_format.ContinuationTextBox = null;
            this.Dest_format.CustomEnabled = true;
            this.Dest_format.DataFieldMapping = "";
            this.Dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_format.GetRecordsOnUpDownKeys = false;
            this.Dest_format.IsDate = false;
            this.Dest_format.Location = new System.Drawing.Point(261, 122);
            this.Dest_format.Name = "Dest_format";
            this.Dest_format.NumberFormat = "###,###,##0.00";
            this.Dest_format.Postfix = "";
            this.Dest_format.Prefix = "";
            this.Dest_format.Size = new System.Drawing.Size(168, 20);
            this.Dest_format.SkipValidation = false;
            this.Dest_format.TabIndex = 21;
            this.Dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destype";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(261, 96);
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(168, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 18;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(261, 69);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(168, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(144, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(225, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Enter values for the Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(181, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Report Parameters";
            // 
            // CHRIS_TaxClosing_SalaryCertificate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 498);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_TaxClosing_SalaryCertificate";
            this.Text = "iCORE CHRIS - SALARY CERTIFICATE";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox Dest_format;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox copies;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox BRANCH;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox FPNO;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLComboBox MODE;
        private CrplControlLibrary.SLTextBox TPNO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox SIGN;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox DESIG;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLDatePicker DATE;
        private CrplControlLibrary.SLButton CLOSE;
        private CrplControlLibrary.SLButton RUN;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}