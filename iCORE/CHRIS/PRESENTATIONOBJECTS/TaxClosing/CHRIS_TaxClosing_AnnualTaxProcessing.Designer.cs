namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_AnnualTaxProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gboTaxYearEndClosing = new System.Windows.Forms.GroupBox();
            this.slpnlTaxYearEndClosing = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtTaxToDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxFromDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtStartProcessing = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTransportAllowance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPno = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxTo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxFrom = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.gboTaxYearEndClosing.SuspendLayout();
            this.slpnlTaxYearEndClosing.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(631, 0);
            this.txtOption.Visible = false;
            // 
            // gboTaxYearEndClosing
            // 
            this.gboTaxYearEndClosing.Controls.Add(this.slpnlTaxYearEndClosing);
            this.gboTaxYearEndClosing.Location = new System.Drawing.Point(12, 88);
            this.gboTaxYearEndClosing.Name = "gboTaxYearEndClosing";
            this.gboTaxYearEndClosing.Size = new System.Drawing.Size(643, 412);
            this.gboTaxYearEndClosing.TabIndex = 2;
            this.gboTaxYearEndClosing.TabStop = false;
            // 
            // slpnlTaxYearEndClosing
            // 
            this.slpnlTaxYearEndClosing.ConcurrentPanels = null;
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtTaxToDate);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtTaxFromDate);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtStartProcessing);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtTransportAllowance);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtPno);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtAmount);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtTaxTo);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtTaxFrom);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtLocation);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtUser);
            this.slpnlTaxYearEndClosing.Controls.Add(this.txtDate);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label9);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label8);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label7);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label6);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label5);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label4);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label3);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label2);
            this.slpnlTaxYearEndClosing.Controls.Add(this.label1);
            this.slpnlTaxYearEndClosing.DataManager = null;
            this.slpnlTaxYearEndClosing.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slpnlTaxYearEndClosing.DependentPanels = null;
            this.slpnlTaxYearEndClosing.DisableDependentLoad = false;
            this.slpnlTaxYearEndClosing.EnableDelete = true;
            this.slpnlTaxYearEndClosing.EnableInsert = true;
            this.slpnlTaxYearEndClosing.EnableUpdate = true;
            this.slpnlTaxYearEndClosing.EntityName = null;
            this.slpnlTaxYearEndClosing.Location = new System.Drawing.Point(16, 35);
            this.slpnlTaxYearEndClosing.MasterPanel = null;
            this.slpnlTaxYearEndClosing.Name = "slpnlTaxYearEndClosing";
            this.slpnlTaxYearEndClosing.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slpnlTaxYearEndClosing.Size = new System.Drawing.Size(608, 357);
            this.slpnlTaxYearEndClosing.SPName = null;
            this.slpnlTaxYearEndClosing.TabIndex = 0;
            // 
            // txtTaxToDate
            // 
            this.txtTaxToDate.AllowSpace = true;
            this.txtTaxToDate.AssociatedLookUpName = "";
            this.txtTaxToDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxToDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxToDate.ContinuationTextBox = null;
            this.txtTaxToDate.CustomEnabled = true;
            this.txtTaxToDate.DataFieldMapping = "";
            this.txtTaxToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxToDate.GetRecordsOnUpDownKeys = false;
            this.txtTaxToDate.IsDate = false;
            this.txtTaxToDate.Location = new System.Drawing.Point(176, 129);
            this.txtTaxToDate.MaxLength = 11;
            this.txtTaxToDate.Name = "txtTaxToDate";
            this.txtTaxToDate.NumberFormat = "###,###,##0.00";
            this.txtTaxToDate.Postfix = "";
            this.txtTaxToDate.Prefix = "";
            this.txtTaxToDate.ReadOnly = true;
            this.txtTaxToDate.Size = new System.Drawing.Size(115, 20);
            this.txtTaxToDate.SkipValidation = false;
            this.txtTaxToDate.TabIndex = 19;
            this.txtTaxToDate.TabStop = false;
            this.txtTaxToDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTaxFromDate
            // 
            this.txtTaxFromDate.AllowSpace = true;
            this.txtTaxFromDate.AssociatedLookUpName = "";
            this.txtTaxFromDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxFromDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxFromDate.ContinuationTextBox = null;
            this.txtTaxFromDate.CustomEnabled = true;
            this.txtTaxFromDate.DataFieldMapping = "";
            this.txtTaxFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxFromDate.GetRecordsOnUpDownKeys = false;
            this.txtTaxFromDate.IsDate = false;
            this.txtTaxFromDate.Location = new System.Drawing.Point(176, 107);
            this.txtTaxFromDate.MaxLength = 11;
            this.txtTaxFromDate.Name = "txtTaxFromDate";
            this.txtTaxFromDate.NumberFormat = "###,###,##0.00";
            this.txtTaxFromDate.Postfix = "";
            this.txtTaxFromDate.Prefix = "";
            this.txtTaxFromDate.ReadOnly = true;
            this.txtTaxFromDate.Size = new System.Drawing.Size(115, 20);
            this.txtTaxFromDate.SkipValidation = false;
            this.txtTaxFromDate.TabIndex = 18;
            this.txtTaxFromDate.TabStop = false;
            this.txtTaxFromDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtStartProcessing
            // 
            this.txtStartProcessing.AllowSpace = true;
            this.txtStartProcessing.AssociatedLookUpName = "";
            this.txtStartProcessing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStartProcessing.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStartProcessing.ContinuationTextBox = null;
            this.txtStartProcessing.CustomEnabled = true;
            this.txtStartProcessing.DataFieldMapping = "";
            this.txtStartProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStartProcessing.GetRecordsOnUpDownKeys = false;
            this.txtStartProcessing.IsDate = false;
            this.txtStartProcessing.Location = new System.Drawing.Point(390, 276);
            this.txtStartProcessing.MaxLength = 3;
            this.txtStartProcessing.Name = "txtStartProcessing";
            this.txtStartProcessing.NumberFormat = "###,###,##0.00";
            this.txtStartProcessing.Postfix = "";
            this.txtStartProcessing.Prefix = "";
            this.txtStartProcessing.Size = new System.Drawing.Size(39, 20);
            this.txtStartProcessing.SkipValidation = false;
            this.txtStartProcessing.TabIndex = 0;
            this.txtStartProcessing.TextType = CrplControlLibrary.TextType.String;
            this.txtStartProcessing.Leave += new System.EventHandler(this.txtStartProcessing_Leave);
            // 
            // txtTransportAllowance
            // 
            this.txtTransportAllowance.AllowSpace = true;
            this.txtTransportAllowance.AssociatedLookUpName = "";
            this.txtTransportAllowance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTransportAllowance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTransportAllowance.ContinuationTextBox = null;
            this.txtTransportAllowance.CustomEnabled = true;
            this.txtTransportAllowance.DataFieldMapping = "";
            this.txtTransportAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransportAllowance.GetRecordsOnUpDownKeys = false;
            this.txtTransportAllowance.IsDate = false;
            this.txtTransportAllowance.Location = new System.Drawing.Point(390, 239);
            this.txtTransportAllowance.MaxLength = 15;
            this.txtTransportAllowance.Name = "txtTransportAllowance";
            this.txtTransportAllowance.NumberFormat = "###,###,##0.00";
            this.txtTransportAllowance.Postfix = "";
            this.txtTransportAllowance.Prefix = "";
            this.txtTransportAllowance.ReadOnly = true;
            this.txtTransportAllowance.Size = new System.Drawing.Size(66, 20);
            this.txtTransportAllowance.SkipValidation = false;
            this.txtTransportAllowance.TabIndex = 1;
            this.txtTransportAllowance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransportAllowance.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPno
            // 
            this.txtPno.AllowSpace = true;
            this.txtPno.AssociatedLookUpName = "";
            this.txtPno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPno.ContinuationTextBox = null;
            this.txtPno.CustomEnabled = true;
            this.txtPno.DataFieldMapping = "";
            this.txtPno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPno.GetRecordsOnUpDownKeys = false;
            this.txtPno.IsDate = false;
            this.txtPno.Location = new System.Drawing.Point(488, 129);
            this.txtPno.MaxLength = 30;
            this.txtPno.Name = "txtPno";
            this.txtPno.NumberFormat = "###,###,##0.00";
            this.txtPno.Postfix = "";
            this.txtPno.Prefix = "";
            this.txtPno.ReadOnly = true;
            this.txtPno.Size = new System.Drawing.Size(100, 20);
            this.txtPno.SkipValidation = false;
            this.txtPno.TabIndex = 15;
            this.txtPno.TabStop = false;
            this.txtPno.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAmount
            // 
            this.txtAmount.AllowSpace = true;
            this.txtAmount.AssociatedLookUpName = "";
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAmount.ContinuationTextBox = null;
            this.txtAmount.CustomEnabled = true;
            this.txtAmount.DataFieldMapping = "";
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.GetRecordsOnUpDownKeys = false;
            this.txtAmount.IsDate = false;
            this.txtAmount.Location = new System.Drawing.Point(488, 107);
            this.txtAmount.MaxLength = 30;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.NumberFormat = "###,###,##0.00";
            this.txtAmount.Postfix = "";
            this.txtAmount.Prefix = "";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.SkipValidation = false;
            this.txtAmount.TabIndex = 14;
            this.txtAmount.TabStop = false;
            this.txtAmount.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTaxTo
            // 
            this.txtTaxTo.AllowSpace = true;
            this.txtTaxTo.AssociatedLookUpName = "";
            this.txtTaxTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxTo.ContinuationTextBox = null;
            this.txtTaxTo.CustomEnabled = true;
            this.txtTaxTo.DataFieldMapping = "";
            this.txtTaxTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxTo.GetRecordsOnUpDownKeys = false;
            this.txtTaxTo.IsDate = false;
            this.txtTaxTo.Location = new System.Drawing.Point(86, 129);
            this.txtTaxTo.MaxLength = 4;
            this.txtTaxTo.Name = "txtTaxTo";
            this.txtTaxTo.NumberFormat = "###,###,##0.00";
            this.txtTaxTo.Postfix = "";
            this.txtTaxTo.Prefix = "";
            this.txtTaxTo.ReadOnly = true;
            this.txtTaxTo.Size = new System.Drawing.Size(88, 20);
            this.txtTaxTo.SkipValidation = false;
            this.txtTaxTo.TabIndex = 13;
            this.txtTaxTo.TabStop = false;
            this.txtTaxTo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTaxFrom
            // 
            this.txtTaxFrom.AllowSpace = true;
            this.txtTaxFrom.AssociatedLookUpName = "";
            this.txtTaxFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxFrom.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxFrom.ContinuationTextBox = null;
            this.txtTaxFrom.CustomEnabled = true;
            this.txtTaxFrom.DataFieldMapping = "";
            this.txtTaxFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxFrom.GetRecordsOnUpDownKeys = false;
            this.txtTaxFrom.IsDate = false;
            this.txtTaxFrom.Location = new System.Drawing.Point(86, 107);
            this.txtTaxFrom.MaxLength = 4;
            this.txtTaxFrom.Name = "txtTaxFrom";
            this.txtTaxFrom.NumberFormat = "###,###,##0.00";
            this.txtTaxFrom.Postfix = "";
            this.txtTaxFrom.Prefix = "";
            this.txtTaxFrom.ReadOnly = true;
            this.txtTaxFrom.Size = new System.Drawing.Size(88, 20);
            this.txtTaxFrom.SkipValidation = false;
            this.txtTaxFrom.TabIndex = 12;
            this.txtTaxFrom.TabStop = false;
            this.txtTaxFrom.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = false;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(14, 43);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(87, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 11;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = false;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(14, 20);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(87, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 10;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(514, 29);
            this.txtDate.MaxLength = 11;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(83, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(156, 283);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(228, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "START PROCESSING [YES/NO] ........";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(133, 241);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(251, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "TRANSPORT ALLOWANCE FOR [VP/RVP]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(466, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(425, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "P.No :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(425, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Amount :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tax To :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tax From :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(198, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tax Year End Closing Process";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Annual Tax Closing Module";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(388, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 110;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_TaxClosing_AnnualTaxProcessing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 666);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.gboTaxYearEndClosing);
            this.Name = "CHRIS_TaxClosing_AnnualTaxProcessing";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "Tax Year End Closing Process";
            this.Controls.SetChildIndex(this.gboTaxYearEndClosing, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.gboTaxYearEndClosing.ResumeLayout(false);
            this.slpnlTaxYearEndClosing.ResumeLayout(false);
            this.slpnlTaxYearEndClosing.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboTaxYearEndClosing;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slpnlTaxYearEndClosing;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtStartProcessing;
        private CrplControlLibrary.SLTextBox txtTransportAllowance;
        private CrplControlLibrary.SLTextBox txtPno;
        private CrplControlLibrary.SLTextBox txtAmount;
        private CrplControlLibrary.SLTextBox txtTaxTo;
        private CrplControlLibrary.SLTextBox txtTaxFrom;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtTaxToDate;
        private CrplControlLibrary.SLTextBox txtTaxFromDate;
        private System.Windows.Forms.Label lblUserName;
    }
}