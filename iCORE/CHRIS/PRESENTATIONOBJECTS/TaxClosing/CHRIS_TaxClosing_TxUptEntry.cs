using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_TxUptEntry : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();

        public CHRIS_TaxClosing_TxUptEntry()
        {
            InitializeComponent();
        }


        public CHRIS_TaxClosing_TxUptEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        

        protected override void OnLoad(EventArgs e)
        {
            txtPersonal.Focus();
            txtPersonal.Select();
            txtOption.Visible       = false;
            
            base.OnLoad(e);
            
            this.txtDate.Text       = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtUser.Text = this.userID;
            labeltest.Text          += "   " + this.UserName;
            
            slDatePicker1.Value     = null;
            slDatePicker2.Value     = null;
            //tbtList.Visible         = false;

            


            tbtAdd.Available = false;
            this.CurrentPanelBlock  = this.pnlTaxStatementUpdate.Name;

        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                //if (checkprpNo() == false)
                if (this.txtPersonal.Text.Trim() != "")
                {
                    if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit)
                    {
                        if(checkprpNo() == false)
                        {
                            return;
                        }
                    }
                    if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
                    {
                        Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                        bool returnValue = false;
                        Result rsltCode;
                        colsNVals.Clear();
                        colsNVals.Add("F_PNO", txtPersonal.Text);

                        CmnDataManager cmnDM = new CmnDataManager();

                        rsltCode = cmnDM.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "proc1", colsNVals);

                        if (rsltCode.isSuccessful)
                        {

                            if (rsltCode.dstResult.Tables.Count == 0 || rsltCode.dstResult.Tables[0].Rows.Count == 0)
                            {
                                return;
                            }

                        }
                    }
                    this.tbtSave.Select();
                    lbtnPersonel.SkipValidationOnLeave = true;
                    txtPersonal.SkipValidation = true;
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    lbtnPersonel.SkipValidationOnLeave = false;
                    txtPersonal.SkipValidation = false;
                }
            }
            if (actionType == "Delete")
            {
                //this.ClearForm(pnlTaxStatementUpdate.Controls);
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtPersonal.Focus();
            }
            if (actionType == "List")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtPersonal.Focus();
            }

            if (actionType == "Cancel")
            {
                this.AutoValidate = AutoValidate.Disable;

                txtPersonal.Select();
                txtPersonal.Focus();
                txtPersonal.IsRequired = false;
                base.DoToolbarActions(ctrlsCollection, actionType);
                txtPersonal.IsRequired = true;
                //this.ClearForm(pnlTaxStatementUpdate.Controls);
                slDatePicker1.Value = null;
                slDatePicker2.Value = null;
                txtPersonal.Focus();
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                
            }
        }




        private void txtRegulr_Validating(object sender, CancelEventArgs e)
        {
            if (txtRegulr.Text == "1" || txtRegulr.Text == "2")
            {
                return;
            }
            else
            {
                MessageBox.Show("Please Enter '1' For Reguler Staff Or '2' For Resignees ....!");
                e.Cancel = true;
                //txtRegulr.Focus();
            }

           //if ((txtRegulr.Text != string.Empty) && (txtRegulr.Text != "1" && txtRegulr.Text != "2"))
           //     MessageBox.Show("Please Enter 1 For Reguler Staff Or 2 For Resginees ....!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
           // //txtRegulr.Focus();
        }

        protected void getTax()
        {
            param.Clear();
            param.Add("F_PNO", this.txtPersonal.Text);
            rslt = cmnDM.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "proc1", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtNameNew.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtDesig.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    txtLevel.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    txtCategry.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    txtBranch.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

                }
            }

        }

        private void txtPersonal_Leave(object sender, EventArgs e)
        {
            if (txtPersonal.Text != "")
            {
                Proc_1();

                //param.Clear();
                //param.Add("F_PNO", txtPersonal.Text);

                //rslt = cmnDM.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "PP_VERIFY", param);
                //if (rslt.isSuccessful)
                //{
                //    if (rslt.dstResult.Tables[0].Rows.Count == 0)
                //    {
                //        MessageBox.Show("Personnel No. Not Found In Tax Work File ........!");
                //        this.ClearForm(pnlTaxStatementUpdate.Controls);
                //        Proc_1();
                //        txtPersonal.Focus();
                //        return;

                //    }
                //}

            }
        }

        private void txtPersonal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txtRegulr_TextChanged(object sender, EventArgs e)
        {

            

        }

        private void txtRegulr_Leave(object sender, EventArgs e)
        {

            //if (txtRegulr.Text != "1" && txtRegulr.Text != "2" )
            //{
            //    MessageBox.Show("Please Enter '1' For Reguler Staff Or '2' For Resginees ....!");
            //    txtRegulr.Focus();
            //    return;
            //}
            //if (txtRegulr.Text == "")
            //{
            //    MessageBox.Show("Please Enter '1' For Reguler Staff Or '2' For Resginees ....!");
            //    txtRegulr.Focus();
            //    return;
            //}

        }

        public bool checkprpNo()
        {
            bool returnValue = false;
            bool proc1_Ind = false;
            if (txtPersonal.Text != "")
            {
                param.Clear();
                param.Add("F_PNO", txtPersonal.Text);

                rslt = cmnDM.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "PP_VERIFY", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        //MessageBox.Show("Personnel No. Not Found In Tax Work File ........!");
                        //this.ClearForm(pnlTaxStatementUpdate.Controls);
                        //txtPersonal.Focus();
                      
                        returnValue = true;
                        

                    }
                }

            }
            return returnValue;
        }

        private bool Proc_1()
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            bool returnValue = false;
            Result rsltCode;
            colsNVals.Clear();
            colsNVals.Add("F_PNO", txtPersonal.Text);
         
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "proc1", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    if (dt.Rows[0]["an_p_name"].ToString() != string.Empty)
                    {
                        txtNameNew.Text = dt.Rows[0]["an_p_name"].ToString();
                    }
                    if (dt.Rows[0]["AN_DESIG"].ToString() != string.Empty)
                    {
                        txtDesig.Text = dt.Rows[0]["AN_DESIG"].ToString();
                    }

                    if (dt.Rows[0]["an_level"].ToString() != string.Empty)
                    {
                        txtLevel.Text = dt.Rows[0]["an_level"].ToString();
                    }
                    if (dt.Rows[0]["an_category"].ToString() != string.Empty)
                    {
                        txtCategry.Text = dt.Rows[0]["an_category"].ToString();
                    }

                    if (dt.Rows[0]["an_branch"].ToString() != string.Empty)
                    {
                        txtBranch.Text = dt.Rows[0]["an_branch"].ToString();
                    }
                }
            }
            //else
            //{
            //    MessageBox.Show("Invalid Personnel No. .......!");
            //    this.ClearForm(pnlTaxStatementUpdate.Controls);
            //    txtPersonal.Focus();
            //    returnValue = true;
            //}

            return returnValue;
        }

        private void txtPersonal_Validating(object sender, CancelEventArgs e)
        {
            //if (txtPersonal.Text != "")
            //{
            //    param.Clear();
            //    param.Add("F_PNO", txtPersonal.Text);

            //    rslt = cmnDM.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "proc1", param);
            //    if (rslt.isSuccessful && rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
            //    {
            //        DataTable dt = rslt.dstResult.Tables[0];

            //        txtNameNew.Text = (dt.Rows[0]["an_p_name"] == null ? "" : dt.Rows[0]["an_p_name"].ToString());
            //        txtDesig.Text = (dt.Rows[0]["AN_DESIG"] == null ? "" : dt.Rows[0]["AN_DESIG"].ToString());
            //        txtLevel.Text = (dt.Rows[0]["an_level"] == null ? "" : dt.Rows[0]["an_level"].ToString());
            //        txtCategry.Text = (dt.Rows[0]["an_category"] == null ? "" : dt.Rows[0]["an_category"].ToString());
            //        txtBranch.Text = (dt.Rows[0]["an_branch"] == null ? "" : dt.Rows[0]["an_branch"].ToString());

            //        if (this.txtID.Text != "")
            //        {
            //            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            //            this.tbtDelete.Enabled = true;
            //        }
            //        else
            //        {
            //            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            //            this.tbtDelete.Enabled = false;
            //        }

            //    }
            //    else
            //    {
            //        MessageBox.Show("Personnel No. Not Found In Tax Work File ........!");
            //        this.ClearForm(pnlTaxStatementUpdate.Controls);
            //        //Proc_1();
            //        //txtPersonal.Focus();
            //        e.Cancel = true;
            //    }
            //}
            //else
            //{
            //    //e.Cancel = true;
            //}
        }

        private void CHRIS_TaxClosing_TxUptEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "MASTER_F_PNO_LOV0" || actionType == "LOV_EXISTS" || actionType == "List")
            {
                if (this.txtID.Text != "")
                {
                    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                    this.tbtDelete.Enabled = true;
                }
                else
                {
                    //if (this.operationMode != iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
                    {
                        MessageBox.Show("Personnel No. Not Found In Tax Work File ........!");
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                        this.tbtDelete.Enabled = false;
                    }
                }
            }
        }
    }
}
