using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using System.Data.SqlClient;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    public partial class CHRIS_TaxClosing_AnnualTaxProcessing : ChrisSimpleForm
    {

        #region Declarations

       
        /*------For custom call in database------- */
        CmnDataManager objCmnDataManager;

        /*------To store data on form level-------------*/
        Dictionary<string, object> dicProcessingDates = new Dictionary<string, object>();
      
        /*------If any exception occurs whole process will be aborted -----*/
        SqlTransaction objSqlTransaction = null;
        SqlConnection objSqlConnection = new SqlConnection();
        private static XMS.DATAOBJECTS.ConnectionBean objConnectionBean = null;

        #endregion

        #region Constructor
        public CHRIS_TaxClosing_AnnualTaxProcessing()
        {
            InitializeComponent();
        }
        public CHRIS_TaxClosing_AnnualTaxProcessing(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #endregion

        #region Methods

        /// <summary>
        /// Override it to perform extra functionaliy
        /// </summary>
        protected override void CommonOnLoadMethods()
        {           
                base.CommonOnLoadMethods();

                this.txtOption.Visible = false;
                this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                lblUserName.Text += "   " + ConfigurationManager.AppSettings["database"];
                this.txtStartProcessing.Focus();
                this.tlbMain.Visible = false;
        }      

        /// <summary>
        /// Initialize block label variables in dictionary
        /// </summary>
        private void InitializeDictionary()
        {
            dicProcessingDates.Clear();

            dicProcessingDates.Add("W_SYS", null);
            dicProcessingDates.Add("W_DATE", null);
            dicProcessingDates.Add("W_TAX_TO", null);
            dicProcessingDates.Add("W_TAX_FROM", null);
            dicProcessingDates.Add("W_TAX_TO_DATE", null);
            dicProcessingDates.Add("W_TAX_FROM_DATE", null);
    
        }

        /// <summary>
        /// Get Processing Dates (From date and To Date)
        /// </summary>
        private void GetProcessingDates()
        {
            try
            {
                
                objCmnDataManager = new CmnDataManager();
                Result rsltCodeProcessingDates = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_MANAGER", "YEARENDCLOSING_YES");

                if (rsltCodeProcessingDates.isSuccessful)
                {
                    if (rsltCodeProcessingDates.dstResult.Tables.Count > 0)
                    {
                        if (rsltCodeProcessingDates.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if ((!String.IsNullOrEmpty(Convert.ToString(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_SYS"])))&&(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_SYS"] != System.DBNull.Value))
                            {
                                DateTime dtSysDateTime = DateTime.Parse(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_SYS"].ToString());
                                dicProcessingDates["W_SYS"] = dtSysDateTime;
                                dicProcessingDates["W_DATE"] = dtSysDateTime;
                                this.txtDate.Text = dtSysDateTime.ToString("dd/MM/yyyy");
                            }
                            if ((!String.IsNullOrEmpty(Convert.ToString(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"]))) && (rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"] != System.DBNull.Value))
                            {
                                dicProcessingDates["W_TAX_TO"] = rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"];
                                this.txtTaxTo.Text = rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"].ToString();
                            }
                            if ((!String.IsNullOrEmpty(Convert.ToString(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"]))) && (rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"] != System.DBNull.Value))
                            {
                                dicProcessingDates["W_TAX_FROM"] = rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"];
                                this.txtTaxFrom.Text = rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"].ToString();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Events
        private void txtStartProcessing_Leave(object sender, EventArgs e)
        {
            if ((this.txtStartProcessing.Text != "YES") && (this.txtStartProcessing.Text != "NO"))
            {
                MessageBox.Show("PLEASE ENTER [YES] OR [NO] ........");
            }
            else if (this.txtStartProcessing.Text == "NO")
            {
                this.Close();
            }
            else if (this.txtStartProcessing.Text == "YES")
            {
                //MessageBox.Show("Processing Please Wait.....");
                MessageBox.Show("Please Acknowledge.....");
              
                InitializeDictionary();
                GetProcessingDates();

                /*------To open processing dialog box------ */
                CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor objCHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor = new CHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor();

                objCHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor.W_DATE = DateTime.Parse(dicProcessingDates["W_DATE"].ToString());
                objCHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor.W_SYS = DateTime.Parse(dicProcessingDates["W_SYS"].ToString());
                objCHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor.W_TAX_FROM = Convert.ToInt32(dicProcessingDates["W_TAX_FROM"]);
                objCHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor.W_TAX_TO = Convert.ToInt32(dicProcessingDates["W_TAX_TO"]);
                
                objCHRIS_TaxClosing_AnnualTaxProcessing_ProcessingTaxFor.Show();
            }
        }
        #endregion

    }
}