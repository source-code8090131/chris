namespace iCORE.CHRIS.PRESENTATIONOBJECTS.TaxClosing
{
    partial class CHRIS_TaxClosing_TxUptEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_TaxClosing_TxUptEntry));
            this.pnlTaxStatementUpdate = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNameNew = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersonel = new CrplControlLibrary.LookupButton(this.components);
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtPrivate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRFfurnshed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRFUnfurnshed = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSurchrge = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxRemaing = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFixedSlab = new CrplControlLibrary.SLTextBox(this.components);
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtRemaingAmt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRebate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSlabAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtLigtWatr = new CrplControlLibrary.SLTextBox(this.components);
            this.txtServentSal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtEducation = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtTaxRefund = new CrplControlLibrary.SLTextBox(this.components);
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTaxP = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxPaid = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRegulr = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTaxIncme = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxAsr = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSevar = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSubLoan = new CrplControlLibrary.SLTextBox(this.components);
            this.txtGH = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxAllow = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOver = new CrplControlLibrary.SLTextBox(this.components);
            this.txtZakat = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncBonus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBonus = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPfund = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConAdd = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConvync = new CrplControlLibrary.SLTextBox(this.components);
            this.txtHousRent = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBasic = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker2 = new CrplControlLibrary.SLDatePicker(this.components);
            this.slDatePicker1 = new CrplControlLibrary.SLDatePicker(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTax = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCategry = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonal = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labeltest = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTaxStatementUpdate.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(649, 0);
            this.txtOption.Size = new System.Drawing.Size(10, 20);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(659, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 578);
            this.panel1.Size = new System.Drawing.Size(659, 60);
            // 
            // pnlTaxStatementUpdate
            // 
            this.pnlTaxStatementUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTaxStatementUpdate.ConcurrentPanels = null;
            this.pnlTaxStatementUpdate.Controls.Add(this.txtID);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtNameNew);
            this.pnlTaxStatementUpdate.Controls.Add(this.lbtnPersonel);
            this.pnlTaxStatementUpdate.Controls.Add(this.label45);
            this.pnlTaxStatementUpdate.Controls.Add(this.label44);
            this.pnlTaxStatementUpdate.Controls.Add(this.label43);
            this.pnlTaxStatementUpdate.Controls.Add(this.label42);
            this.pnlTaxStatementUpdate.Controls.Add(this.label41);
            this.pnlTaxStatementUpdate.Controls.Add(this.label40);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtPrivate);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtRFfurnshed);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtRFUnfurnshed);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtSurchrge);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxRemaing);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtFixedSlab);
            this.pnlTaxStatementUpdate.Controls.Add(this.label39);
            this.pnlTaxStatementUpdate.Controls.Add(this.label38);
            this.pnlTaxStatementUpdate.Controls.Add(this.label37);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtRemaingAmt);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtRebate);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtSlabAmount);
            this.pnlTaxStatementUpdate.Controls.Add(this.label36);
            this.pnlTaxStatementUpdate.Controls.Add(this.label35);
            this.pnlTaxStatementUpdate.Controls.Add(this.label34);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtLigtWatr);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtServentSal);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtEducation);
            this.pnlTaxStatementUpdate.Controls.Add(this.label33);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxRefund);
            this.pnlTaxStatementUpdate.Controls.Add(this.label32);
            this.pnlTaxStatementUpdate.Controls.Add(this.label31);
            this.pnlTaxStatementUpdate.Controls.Add(this.label30);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxP);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxPaid);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtRegulr);
            this.pnlTaxStatementUpdate.Controls.Add(this.label29);
            this.pnlTaxStatementUpdate.Controls.Add(this.label28);
            this.pnlTaxStatementUpdate.Controls.Add(this.label27);
            this.pnlTaxStatementUpdate.Controls.Add(this.label26);
            this.pnlTaxStatementUpdate.Controls.Add(this.label25);
            this.pnlTaxStatementUpdate.Controls.Add(this.label24);
            this.pnlTaxStatementUpdate.Controls.Add(this.label23);
            this.pnlTaxStatementUpdate.Controls.Add(this.label22);
            this.pnlTaxStatementUpdate.Controls.Add(this.label21);
            this.pnlTaxStatementUpdate.Controls.Add(this.label20);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxIncme);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxAsr);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtSevar);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtSubLoan);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtGH);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTaxAllow);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtOver);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtZakat);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtIncBonus);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtBonus);
            this.pnlTaxStatementUpdate.Controls.Add(this.label19);
            this.pnlTaxStatementUpdate.Controls.Add(this.label18);
            this.pnlTaxStatementUpdate.Controls.Add(this.label17);
            this.pnlTaxStatementUpdate.Controls.Add(this.label16);
            this.pnlTaxStatementUpdate.Controls.Add(this.label15);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtPfund);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtConAdd);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtConvync);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtHousRent);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtBasic);
            this.pnlTaxStatementUpdate.Controls.Add(this.slDatePicker2);
            this.pnlTaxStatementUpdate.Controls.Add(this.slDatePicker1);
            this.pnlTaxStatementUpdate.Controls.Add(this.label9);
            this.pnlTaxStatementUpdate.Controls.Add(this.label8);
            this.pnlTaxStatementUpdate.Controls.Add(this.label7);
            this.pnlTaxStatementUpdate.Controls.Add(this.label6);
            this.pnlTaxStatementUpdate.Controls.Add(this.label5);
            this.pnlTaxStatementUpdate.Controls.Add(this.label4);
            this.pnlTaxStatementUpdate.Controls.Add(this.label3);
            this.pnlTaxStatementUpdate.Controls.Add(this.label2);
            this.pnlTaxStatementUpdate.Controls.Add(this.label1);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtTax);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtBranch);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtCategry);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtLevel);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtDesig);
            this.pnlTaxStatementUpdate.Controls.Add(this.txtPersonal);
            this.pnlTaxStatementUpdate.DataManager = null;
            this.pnlTaxStatementUpdate.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTaxStatementUpdate.DependentPanels = null;
            this.pnlTaxStatementUpdate.DisableDependentLoad = false;
            this.pnlTaxStatementUpdate.EnableDelete = true;
            this.pnlTaxStatementUpdate.EnableInsert = true;
            this.pnlTaxStatementUpdate.EnableQuery = false;
            this.pnlTaxStatementUpdate.EnableUpdate = true;
            this.pnlTaxStatementUpdate.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ANNUAL_TAXCommand";
            this.pnlTaxStatementUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlTaxStatementUpdate.Location = new System.Drawing.Point(5, 156);
            this.pnlTaxStatementUpdate.MasterPanel = null;
            this.pnlTaxStatementUpdate.Name = "pnlTaxStatementUpdate";
            this.pnlTaxStatementUpdate.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTaxStatementUpdate.Size = new System.Drawing.Size(644, 407);
            this.pnlTaxStatementUpdate.SPName = "CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER";
            this.pnlTaxStatementUpdate.TabIndex = 1;
            this.pnlTaxStatementUpdate.TabStop = true;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = false;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(595, 3);
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(44, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 87;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtID, "Field must be of form 999,999,999.99.");
            this.txtID.Visible = false;
            // 
            // txtNameNew
            // 
            this.txtNameNew.AllowSpace = true;
            this.txtNameNew.AssociatedLookUpName = "";
            this.txtNameNew.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNameNew.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNameNew.ContinuationTextBox = null;
            this.txtNameNew.CustomEnabled = true;
            this.txtNameNew.DataFieldMapping = "AN_P_NAME";
            this.txtNameNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameNew.GetRecordsOnUpDownKeys = false;
            this.txtNameNew.IsDate = false;
            this.txtNameNew.IsRequired = true;
            this.txtNameNew.Location = new System.Drawing.Point(260, 13);
            this.txtNameNew.MaxLength = 40;
            this.txtNameNew.Name = "txtNameNew";
            this.txtNameNew.NumberFormat = "###,###,##0.00";
            this.txtNameNew.Postfix = "";
            this.txtNameNew.Prefix = "";
            this.txtNameNew.ReadOnly = true;
            this.txtNameNew.Size = new System.Drawing.Size(214, 20);
            this.txtNameNew.SkipValidation = false;
            this.txtNameNew.TabIndex = 1;
            this.txtNameNew.TabStop = false;
            this.txtNameNew.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPersonel
            // 
            this.lbtnPersonel.ActionLOVExists = "LOV_EXISTS";
            this.lbtnPersonel.ActionType = "MASTER_F_PNO_LOV0";
            this.lbtnPersonel.ConditionalFields = "";
            this.lbtnPersonel.CustomEnabled = true;
            this.lbtnPersonel.DataFieldMapping = "";
            this.lbtnPersonel.DependentLovControls = "";
            this.lbtnPersonel.HiddenColumns = resources.GetString("lbtnPersonel.HiddenColumns");
            this.lbtnPersonel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonel.Image")));
            this.lbtnPersonel.LoadDependentEntities = true;
            this.lbtnPersonel.Location = new System.Drawing.Point(189, 13);
            this.lbtnPersonel.LookUpTitle = null;
            this.lbtnPersonel.Name = "lbtnPersonel";
            this.lbtnPersonel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonel.SkipValidationOnLeave = false;
            this.lbtnPersonel.SPName = "CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER";
            this.lbtnPersonel.TabIndex = 86;
            this.lbtnPersonel.TabStop = false;
            this.lbtnPersonel.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(485, 376);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(51, 13);
            this.label45.TabIndex = 85;
            this.label45.Text = "Private:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(451, 353);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(90, 13);
            this.label44.TabIndex = 84;
            this.label44.Text = "R.F.Furnished:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(440, 332);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(103, 13);
            this.label43.TabIndex = 83;
            this.label43.Text = "R.F.Unfurnished:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(268, 383);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(69, 13);
            this.label42.TabIndex = 82;
            this.label42.Text = "Surcharge:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(211, 356);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(138, 13);
            this.label41.TabIndex = 81;
            this.label41.Text = "Tax on Remaining Amt:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(204, 335);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(138, 13);
            this.label40.TabIndex = 80;
            this.label40.Text = "Fixed Tax on Slab Amt:";
            // 
            // txtPrivate
            // 
            this.txtPrivate.AllowSpace = true;
            this.txtPrivate.AssociatedLookUpName = "";
            this.txtPrivate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrivate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrivate.ContinuationTextBox = null;
            this.txtPrivate.CustomEnabled = true;
            this.txtPrivate.DataFieldMapping = "PRIVATE";
            this.txtPrivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrivate.GetRecordsOnUpDownKeys = false;
            this.txtPrivate.IsDate = false;
            this.txtPrivate.Location = new System.Drawing.Point(530, 376);
            this.txtPrivate.MaxLength = 10;
            this.txtPrivate.Name = "txtPrivate";
            this.txtPrivate.NumberFormat = "###,###,##0.00";
            this.txtPrivate.Postfix = "";
            this.txtPrivate.Prefix = "";
            this.txtPrivate.Size = new System.Drawing.Size(100, 20);
            this.txtPrivate.SkipValidation = false;
            this.txtPrivate.TabIndex = 39;
            this.txtPrivate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrivate.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtPrivate, "Field must be of form 999,999,999.99.");
            // 
            // txtRFfurnshed
            // 
            this.txtRFfurnshed.AllowSpace = true;
            this.txtRFfurnshed.AssociatedLookUpName = "";
            this.txtRFfurnshed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRFfurnshed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRFfurnshed.ContinuationTextBox = null;
            this.txtRFfurnshed.CustomEnabled = true;
            this.txtRFfurnshed.DataFieldMapping = "AN_RNT_FREE_FURNISHED";
            this.txtRFfurnshed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRFfurnshed.GetRecordsOnUpDownKeys = false;
            this.txtRFfurnshed.IsDate = false;
            this.txtRFfurnshed.Location = new System.Drawing.Point(529, 353);
            this.txtRFfurnshed.MaxLength = 10;
            this.txtRFfurnshed.Name = "txtRFfurnshed";
            this.txtRFfurnshed.NumberFormat = "###,###,##0.00";
            this.txtRFfurnshed.Postfix = "";
            this.txtRFfurnshed.Prefix = "";
            this.txtRFfurnshed.Size = new System.Drawing.Size(100, 20);
            this.txtRFfurnshed.SkipValidation = false;
            this.txtRFfurnshed.TabIndex = 38;
            this.txtRFfurnshed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRFfurnshed.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtRFfurnshed, "Field must be of form 999,999,999.99.");
            // 
            // txtRFUnfurnshed
            // 
            this.txtRFUnfurnshed.AllowSpace = true;
            this.txtRFUnfurnshed.AssociatedLookUpName = "";
            this.txtRFUnfurnshed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRFUnfurnshed.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRFUnfurnshed.ContinuationTextBox = null;
            this.txtRFUnfurnshed.CustomEnabled = true;
            this.txtRFUnfurnshed.DataFieldMapping = "AN_RNT_FREE_UNFURNISHED";
            this.txtRFUnfurnshed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRFUnfurnshed.GetRecordsOnUpDownKeys = false;
            this.txtRFUnfurnshed.IsDate = false;
            this.txtRFUnfurnshed.Location = new System.Drawing.Point(529, 330);
            this.txtRFUnfurnshed.MaxLength = 10;
            this.txtRFUnfurnshed.Name = "txtRFUnfurnshed";
            this.txtRFUnfurnshed.NumberFormat = "###,###,##0.00";
            this.txtRFUnfurnshed.Postfix = "";
            this.txtRFUnfurnshed.Prefix = "";
            this.txtRFUnfurnshed.Size = new System.Drawing.Size(100, 20);
            this.txtRFUnfurnshed.SkipValidation = false;
            this.txtRFUnfurnshed.TabIndex = 37;
            this.txtRFUnfurnshed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRFUnfurnshed.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtRFUnfurnshed, "Field must be of form 999,999,999.99.");
            // 
            // txtSurchrge
            // 
            this.txtSurchrge.AllowSpace = true;
            this.txtSurchrge.AssociatedLookUpName = "";
            this.txtSurchrge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSurchrge.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSurchrge.ContinuationTextBox = null;
            this.txtSurchrge.CustomEnabled = true;
            this.txtSurchrge.DataFieldMapping = "AN_SUR";
            this.txtSurchrge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSurchrge.GetRecordsOnUpDownKeys = false;
            this.txtSurchrge.IsDate = false;
            this.txtSurchrge.Location = new System.Drawing.Point(332, 378);
            this.txtSurchrge.MaxLength = 10;
            this.txtSurchrge.Name = "txtSurchrge";
            this.txtSurchrge.NumberFormat = "###,###,##0.00";
            this.txtSurchrge.Postfix = "";
            this.txtSurchrge.Prefix = "";
            this.txtSurchrge.Size = new System.Drawing.Size(100, 20);
            this.txtSurchrge.SkipValidation = false;
            this.txtSurchrge.TabIndex = 33;
            this.txtSurchrge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSurchrge.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtSurchrge, "Field must be of form 999,999,999.99.");
            // 
            // txtTaxRemaing
            // 
            this.txtTaxRemaing.AllowSpace = true;
            this.txtTaxRemaing.AssociatedLookUpName = "";
            this.txtTaxRemaing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxRemaing.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxRemaing.ContinuationTextBox = null;
            this.txtTaxRemaing.CustomEnabled = true;
            this.txtTaxRemaing.DataFieldMapping = "AN_TAX_REM";
            this.txtTaxRemaing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxRemaing.GetRecordsOnUpDownKeys = false;
            this.txtTaxRemaing.IsDate = false;
            this.txtTaxRemaing.Location = new System.Drawing.Point(332, 355);
            this.txtTaxRemaing.MaxLength = 10;
            this.txtTaxRemaing.Name = "txtTaxRemaing";
            this.txtTaxRemaing.NumberFormat = "###,###,##0.00";
            this.txtTaxRemaing.Postfix = "";
            this.txtTaxRemaing.Prefix = "";
            this.txtTaxRemaing.Size = new System.Drawing.Size(100, 20);
            this.txtTaxRemaing.SkipValidation = false;
            this.txtTaxRemaing.TabIndex = 32;
            this.txtTaxRemaing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxRemaing.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxRemaing, "Field must be of form 999,999,999.99.");
            // 
            // txtFixedSlab
            // 
            this.txtFixedSlab.AllowSpace = true;
            this.txtFixedSlab.AssociatedLookUpName = "";
            this.txtFixedSlab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFixedSlab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFixedSlab.ContinuationTextBox = null;
            this.txtFixedSlab.CustomEnabled = true;
            this.txtFixedSlab.DataFieldMapping = "AN_FIX_TAX";
            this.txtFixedSlab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFixedSlab.GetRecordsOnUpDownKeys = false;
            this.txtFixedSlab.IsDate = false;
            this.txtFixedSlab.Location = new System.Drawing.Point(332, 332);
            this.txtFixedSlab.MaxLength = 10;
            this.txtFixedSlab.Name = "txtFixedSlab";
            this.txtFixedSlab.NumberFormat = "###,###,##0.00";
            this.txtFixedSlab.Postfix = "";
            this.txtFixedSlab.Prefix = "";
            this.txtFixedSlab.Size = new System.Drawing.Size(100, 20);
            this.txtFixedSlab.SkipValidation = false;
            this.txtFixedSlab.TabIndex = 31;
            this.txtFixedSlab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFixedSlab.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtFixedSlab, "Field must be of form 999,999,999.99.");
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 383);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(95, 13);
            this.label39.TabIndex = 73;
            this.label39.Text = "Remaining Amt:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(41, 358);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(52, 13);
            this.label38.TabIndex = 72;
            this.label38.Text = "Rebate:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(17, 337);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(78, 13);
            this.label37.TabIndex = 71;
            this.label37.Text = "SlabAmount:";
            // 
            // txtRemaingAmt
            // 
            this.txtRemaingAmt.AllowSpace = true;
            this.txtRemaingAmt.AssociatedLookUpName = "";
            this.txtRemaingAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemaingAmt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRemaingAmt.ContinuationTextBox = null;
            this.txtRemaingAmt.CustomEnabled = true;
            this.txtRemaingAmt.DataFieldMapping = "AN_REM_AMT";
            this.txtRemaingAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemaingAmt.GetRecordsOnUpDownKeys = false;
            this.txtRemaingAmt.IsDate = false;
            this.txtRemaingAmt.Location = new System.Drawing.Point(88, 381);
            this.txtRemaingAmt.MaxLength = 10;
            this.txtRemaingAmt.Name = "txtRemaingAmt";
            this.txtRemaingAmt.NumberFormat = "###,###,##0.00";
            this.txtRemaingAmt.Postfix = "";
            this.txtRemaingAmt.Prefix = "";
            this.txtRemaingAmt.Size = new System.Drawing.Size(100, 20);
            this.txtRemaingAmt.SkipValidation = false;
            this.txtRemaingAmt.TabIndex = 30;
            this.txtRemaingAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRemaingAmt.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtRemaingAmt, "Field must be of form 999,999,999.99.");
            // 
            // txtRebate
            // 
            this.txtRebate.AllowSpace = true;
            this.txtRebate.AssociatedLookUpName = "";
            this.txtRebate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRebate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRebate.ContinuationTextBox = null;
            this.txtRebate.CustomEnabled = true;
            this.txtRebate.DataFieldMapping = "AN_REBATE";
            this.txtRebate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRebate.GetRecordsOnUpDownKeys = false;
            this.txtRebate.IsDate = false;
            this.txtRebate.Location = new System.Drawing.Point(88, 358);
            this.txtRebate.MaxLength = 10;
            this.txtRebate.Name = "txtRebate";
            this.txtRebate.NumberFormat = "###,###,##0.00";
            this.txtRebate.Postfix = "";
            this.txtRebate.Prefix = "";
            this.txtRebate.Size = new System.Drawing.Size(100, 20);
            this.txtRebate.SkipValidation = false;
            this.txtRebate.TabIndex = 29;
            this.txtRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRebate.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtRebate, "Field must be of form 999,999,999.99.");
            // 
            // txtSlabAmount
            // 
            this.txtSlabAmount.AllowSpace = true;
            this.txtSlabAmount.AssociatedLookUpName = "";
            this.txtSlabAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSlabAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSlabAmount.ContinuationTextBox = null;
            this.txtSlabAmount.CustomEnabled = true;
            this.txtSlabAmount.DataFieldMapping = "AN_TAX_FROM_AMT";
            this.txtSlabAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSlabAmount.GetRecordsOnUpDownKeys = false;
            this.txtSlabAmount.IsDate = false;
            this.txtSlabAmount.Location = new System.Drawing.Point(88, 335);
            this.txtSlabAmount.MaxLength = 10;
            this.txtSlabAmount.Name = "txtSlabAmount";
            this.txtSlabAmount.NumberFormat = "###,###,##0.00";
            this.txtSlabAmount.Postfix = "";
            this.txtSlabAmount.Prefix = "";
            this.txtSlabAmount.Size = new System.Drawing.Size(100, 20);
            this.txtSlabAmount.SkipValidation = false;
            this.txtSlabAmount.TabIndex = 28;
            this.txtSlabAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSlabAmount.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtSlabAmount, "Field must be of form 999,999,999.99.");
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(450, 286);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(90, 13);
            this.label36.TabIndex = 67;
            this.label36.Text = "Light.H.Water:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(459, 263);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 13);
            this.label35.TabIndex = 66;
            this.label35.Text = "Servent.Sal:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(466, 241);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 13);
            this.label34.TabIndex = 65;
            this.label34.Text = "Education:";
            // 
            // txtLigtWatr
            // 
            this.txtLigtWatr.AllowSpace = true;
            this.txtLigtWatr.AssociatedLookUpName = "";
            this.txtLigtWatr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLigtWatr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLigtWatr.ContinuationTextBox = null;
            this.txtLigtWatr.CustomEnabled = true;
            this.txtLigtWatr.DataFieldMapping = "AN_L_H_WATER";
            this.txtLigtWatr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLigtWatr.GetRecordsOnUpDownKeys = false;
            this.txtLigtWatr.IsDate = false;
            this.txtLigtWatr.Location = new System.Drawing.Point(529, 284);
            this.txtLigtWatr.MaxLength = 10;
            this.txtLigtWatr.Name = "txtLigtWatr";
            this.txtLigtWatr.NumberFormat = "###,###,##0.00";
            this.txtLigtWatr.Postfix = "";
            this.txtLigtWatr.Prefix = "";
            this.txtLigtWatr.Size = new System.Drawing.Size(100, 20);
            this.txtLigtWatr.SkipValidation = false;
            this.txtLigtWatr.TabIndex = 36;
            this.txtLigtWatr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLigtWatr.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtLigtWatr, "Field must be of form 999,999,999.99.");
            // 
            // txtServentSal
            // 
            this.txtServentSal.AllowSpace = true;
            this.txtServentSal.AssociatedLookUpName = "";
            this.txtServentSal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServentSal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtServentSal.ContinuationTextBox = null;
            this.txtServentSal.CustomEnabled = true;
            this.txtServentSal.DataFieldMapping = "AN_SER_SAL";
            this.txtServentSal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServentSal.GetRecordsOnUpDownKeys = false;
            this.txtServentSal.IsDate = false;
            this.txtServentSal.Location = new System.Drawing.Point(529, 261);
            this.txtServentSal.MaxLength = 10;
            this.txtServentSal.Name = "txtServentSal";
            this.txtServentSal.NumberFormat = "###,###,##0.00";
            this.txtServentSal.Postfix = "";
            this.txtServentSal.Prefix = "";
            this.txtServentSal.Size = new System.Drawing.Size(100, 20);
            this.txtServentSal.SkipValidation = false;
            this.txtServentSal.TabIndex = 35;
            this.txtServentSal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtServentSal.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtServentSal, "Field must be of form 999,999,999.99.");
            // 
            // txtEducation
            // 
            this.txtEducation.AllowSpace = true;
            this.txtEducation.AssociatedLookUpName = "";
            this.txtEducation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEducation.ContinuationTextBox = null;
            this.txtEducation.CustomEnabled = true;
            this.txtEducation.DataFieldMapping = "AN_EDUCATION";
            this.txtEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEducation.GetRecordsOnUpDownKeys = false;
            this.txtEducation.IsDate = false;
            this.txtEducation.Location = new System.Drawing.Point(529, 239);
            this.txtEducation.MaxLength = 10;
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.NumberFormat = "###,###,##0.00";
            this.txtEducation.Postfix = "";
            this.txtEducation.Prefix = "";
            this.txtEducation.Size = new System.Drawing.Size(100, 20);
            this.txtEducation.SkipValidation = false;
            this.txtEducation.TabIndex = 34;
            this.txtEducation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEducation.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtEducation, "Field must be of form 999,999,999.99.");
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(262, 289);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 13);
            this.label33.TabIndex = 61;
            this.label33.Text = "Tax Refund:";
            // 
            // txtTaxRefund
            // 
            this.txtTaxRefund.AllowSpace = true;
            this.txtTaxRefund.AssociatedLookUpName = "";
            this.txtTaxRefund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxRefund.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxRefund.ContinuationTextBox = null;
            this.txtTaxRefund.CustomEnabled = true;
            this.txtTaxRefund.DataFieldMapping = "AN_REFUND_AMT";
            this.txtTaxRefund.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxRefund.GetRecordsOnUpDownKeys = false;
            this.txtTaxRefund.IsDate = false;
            this.txtTaxRefund.Location = new System.Drawing.Point(332, 286);
            this.txtTaxRefund.MaxLength = 10;
            this.txtTaxRefund.Name = "txtTaxRefund";
            this.txtTaxRefund.NumberFormat = "###,###,##0.00";
            this.txtTaxRefund.Postfix = "";
            this.txtTaxRefund.Prefix = "";
            this.txtTaxRefund.Size = new System.Drawing.Size(100, 20);
            this.txtTaxRefund.SkipValidation = false;
            this.txtTaxRefund.TabIndex = 27;
            this.txtTaxRefund.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxRefund.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxRefund, "Field must be of form 999,999,999.99.");
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(280, 265);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(57, 13);
            this.label32.TabIndex = 59;
            this.label32.Text = "TaxPaid:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(282, 244);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(57, 13);
            this.label31.TabIndex = 58;
            this.label31.Text = "TaxPaid:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(4, 290);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(93, 13);
            this.label30.TabIndex = 57;
            this.label30.Text = "Regular/Resig:";
            // 
            // txtTaxP
            // 
            this.txtTaxP.AllowSpace = true;
            this.txtTaxP.AssociatedLookUpName = "";
            this.txtTaxP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxP.ContinuationTextBox = null;
            this.txtTaxP.CustomEnabled = true;
            this.txtTaxP.DataFieldMapping = "AN_LFA";
            this.txtTaxP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxP.GetRecordsOnUpDownKeys = false;
            this.txtTaxP.IsDate = false;
            this.txtTaxP.Location = new System.Drawing.Point(332, 263);
            this.txtTaxP.MaxLength = 10;
            this.txtTaxP.Name = "txtTaxP";
            this.txtTaxP.NumberFormat = "###,###,##0.00";
            this.txtTaxP.Postfix = "";
            this.txtTaxP.Prefix = "";
            this.txtTaxP.Size = new System.Drawing.Size(100, 20);
            this.txtTaxP.SkipValidation = false;
            this.txtTaxP.TabIndex = 26;
            this.txtTaxP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxP.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxP, "Field must be of form 999,999,999.99.");
            // 
            // txtTaxPaid
            // 
            this.txtTaxPaid.AllowSpace = true;
            this.txtTaxPaid.AssociatedLookUpName = "";
            this.txtTaxPaid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxPaid.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxPaid.ContinuationTextBox = null;
            this.txtTaxPaid.CustomEnabled = true;
            this.txtTaxPaid.DataFieldMapping = "AN_TAX_PAID";
            this.txtTaxPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxPaid.GetRecordsOnUpDownKeys = false;
            this.txtTaxPaid.IsDate = false;
            this.txtTaxPaid.Location = new System.Drawing.Point(332, 241);
            this.txtTaxPaid.MaxLength = 10;
            this.txtTaxPaid.Name = "txtTaxPaid";
            this.txtTaxPaid.NumberFormat = "###,###,##0.00";
            this.txtTaxPaid.Postfix = "";
            this.txtTaxPaid.Prefix = "";
            this.txtTaxPaid.Size = new System.Drawing.Size(100, 20);
            this.txtTaxPaid.SkipValidation = false;
            this.txtTaxPaid.TabIndex = 25;
            this.txtTaxPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxPaid.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxPaid, "Field must be of form 999,999,999.99.");
            // 
            // txtRegulr
            // 
            this.txtRegulr.AllowSpace = true;
            this.txtRegulr.AssociatedLookUpName = "";
            this.txtRegulr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegulr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRegulr.ContinuationTextBox = null;
            this.txtRegulr.CustomEnabled = true;
            this.txtRegulr.DataFieldMapping = "AN_FLAG";
            this.txtRegulr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegulr.GetRecordsOnUpDownKeys = false;
            this.txtRegulr.IsDate = false;
            this.txtRegulr.Location = new System.Drawing.Point(88, 286);
            this.txtRegulr.MaxLength = 1;
            this.txtRegulr.Name = "txtRegulr";
            this.txtRegulr.NumberFormat = "###,###,##0.00";
            this.txtRegulr.Postfix = "";
            this.txtRegulr.Prefix = "";
            this.txtRegulr.Size = new System.Drawing.Size(48, 20);
            this.txtRegulr.SkipValidation = false;
            this.txtRegulr.TabIndex = 24;
            this.txtRegulr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRegulr.TextType = CrplControlLibrary.TextType.Double;
            this.toolTip1.SetToolTip(this.txtRegulr, "Field must be of form 999,999,999.99.");
            this.txtRegulr.TextChanged += new System.EventHandler(this.txtRegulr_TextChanged);
            this.txtRegulr.Leave += new System.EventHandler(this.txtRegulr_Leave);
            this.txtRegulr.Validating += new System.ComponentModel.CancelEventHandler(this.txtRegulr_Validating);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(-3, 267);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 13);
            this.label29.TabIndex = 53;
            this.label29.Text = "Taxable Income:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(34, 246);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 13);
            this.label28.TabIndex = 51;
            this.label28.Text = "Tax Asr:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(440, 193);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 13);
            this.label27.TabIndex = 49;
            this.label27.Text = "Sevarance Paid:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(444, 172);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 13);
            this.label26.TabIndex = 47;
            this.label26.Text = "Subs.Loan Tax:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(451, 124);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 13);
            this.label25.TabIndex = 46;
            this.label25.Text = "G.H.Allow:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(244, 194);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(90, 13);
            this.label24.TabIndex = 44;
            this.label24.Text = "Taxable Allow:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(269, 172);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "Overtime:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(271, 145);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "Zakat:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(262, 124);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "Inc.Bonus:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(255, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "10 C Bonus:";
            // 
            // txtTaxIncme
            // 
            this.txtTaxIncme.AllowSpace = true;
            this.txtTaxIncme.AssociatedLookUpName = "";
            this.txtTaxIncme.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxIncme.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxIncme.ContinuationTextBox = null;
            this.txtTaxIncme.CustomEnabled = true;
            this.txtTaxIncme.DataFieldMapping = "AN_TAXABLE_INC";
            this.txtTaxIncme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxIncme.GetRecordsOnUpDownKeys = false;
            this.txtTaxIncme.IsDate = false;
            this.txtTaxIncme.Location = new System.Drawing.Point(88, 265);
            this.txtTaxIncme.MaxLength = 10;
            this.txtTaxIncme.Name = "txtTaxIncme";
            this.txtTaxIncme.NumberFormat = "###,###,##0.00";
            this.txtTaxIncme.Postfix = "";
            this.txtTaxIncme.Prefix = "";
            this.txtTaxIncme.Size = new System.Drawing.Size(100, 20);
            this.txtTaxIncme.SkipValidation = false;
            this.txtTaxIncme.TabIndex = 23;
            this.txtTaxIncme.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxIncme.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxIncme, "Field must be of form 999,999,999.99.");
            // 
            // txtTaxAsr
            // 
            this.txtTaxAsr.AllowSpace = true;
            this.txtTaxAsr.AssociatedLookUpName = "";
            this.txtTaxAsr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxAsr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxAsr.ContinuationTextBox = null;
            this.txtTaxAsr.CustomEnabled = true;
            this.txtTaxAsr.DataFieldMapping = "AN_TAX_ASR";
            this.txtTaxAsr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAsr.GetRecordsOnUpDownKeys = false;
            this.txtTaxAsr.IsDate = false;
            this.txtTaxAsr.Location = new System.Drawing.Point(88, 244);
            this.txtTaxAsr.MaxLength = 10;
            this.txtTaxAsr.Name = "txtTaxAsr";
            this.txtTaxAsr.NumberFormat = "###,###,##0.00";
            this.txtTaxAsr.Postfix = "";
            this.txtTaxAsr.Prefix = "";
            this.txtTaxAsr.Size = new System.Drawing.Size(100, 20);
            this.txtTaxAsr.SkipValidation = false;
            this.txtTaxAsr.TabIndex = 22;
            this.txtTaxAsr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxAsr.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxAsr, "Total ASR is Total Of Basic + House + Conveyance");
            // 
            // txtSevar
            // 
            this.txtSevar.AllowSpace = true;
            this.txtSevar.AssociatedLookUpName = "";
            this.txtSevar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSevar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSevar.ContinuationTextBox = null;
            this.txtSevar.CustomEnabled = true;
            this.txtSevar.DataFieldMapping = "AN_SEVARANCE_AMT";
            this.txtSevar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSevar.GetRecordsOnUpDownKeys = false;
            this.txtSevar.IsDate = false;
            this.txtSevar.Location = new System.Drawing.Point(529, 191);
            this.txtSevar.MaxLength = 12;
            this.txtSevar.Name = "txtSevar";
            this.txtSevar.NumberFormat = "###,###,##0.00";
            this.txtSevar.Postfix = "";
            this.txtSevar.Prefix = "";
            this.txtSevar.Size = new System.Drawing.Size(100, 20);
            this.txtSevar.SkipValidation = false;
            this.txtSevar.TabIndex = 21;
            this.txtSevar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSevar.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtSevar, "Field must be of form 999,999,999.99.");
            // 
            // txtSubLoan
            // 
            this.txtSubLoan.AllowSpace = true;
            this.txtSubLoan.AssociatedLookUpName = "";
            this.txtSubLoan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubLoan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSubLoan.ContinuationTextBox = null;
            this.txtSubLoan.CustomEnabled = true;
            this.txtSubLoan.DataFieldMapping = "AN_SUBS_LOAN_TAX";
            this.txtSubLoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubLoan.GetRecordsOnUpDownKeys = false;
            this.txtSubLoan.IsDate = false;
            this.txtSubLoan.Location = new System.Drawing.Point(529, 167);
            this.txtSubLoan.MaxLength = 12;
            this.txtSubLoan.Name = "txtSubLoan";
            this.txtSubLoan.NumberFormat = "###,###,##0.00";
            this.txtSubLoan.Postfix = "";
            this.txtSubLoan.Prefix = "";
            this.txtSubLoan.Size = new System.Drawing.Size(100, 20);
            this.txtSubLoan.SkipValidation = false;
            this.txtSubLoan.TabIndex = 20;
            this.txtSubLoan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSubLoan.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtSubLoan, "Field must be of form 999,999,999.99.");
            // 
            // txtGH
            // 
            this.txtGH.AllowSpace = true;
            this.txtGH.AssociatedLookUpName = "";
            this.txtGH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGH.ContinuationTextBox = null;
            this.txtGH.CustomEnabled = true;
            this.txtGH.DataFieldMapping = "AN_GHA_ALL";
            this.txtGH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGH.GetRecordsOnUpDownKeys = false;
            this.txtGH.IsDate = false;
            this.txtGH.Location = new System.Drawing.Point(528, 117);
            this.txtGH.MaxLength = 8;
            this.txtGH.Name = "txtGH";
            this.txtGH.NumberFormat = "###,###,##0.00";
            this.txtGH.Postfix = "";
            this.txtGH.Prefix = "";
            this.txtGH.Size = new System.Drawing.Size(100, 20);
            this.txtGH.SkipValidation = false;
            this.txtGH.TabIndex = 16;
            this.txtGH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGH.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtGH, "Field must be of form 999,999,999.99.");
            // 
            // txtTaxAllow
            // 
            this.txtTaxAllow.AllowSpace = true;
            this.txtTaxAllow.AssociatedLookUpName = "";
            this.txtTaxAllow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxAllow.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxAllow.ContinuationTextBox = null;
            this.txtTaxAllow.CustomEnabled = true;
            this.txtTaxAllow.DataFieldMapping = "AN_TAXABLE_ALL";
            this.txtTaxAllow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAllow.GetRecordsOnUpDownKeys = false;
            this.txtTaxAllow.IsDate = false;
            this.txtTaxAllow.Location = new System.Drawing.Point(326, 190);
            this.txtTaxAllow.MaxLength = 10;
            this.txtTaxAllow.Name = "txtTaxAllow";
            this.txtTaxAllow.NumberFormat = "###,###,##0.00";
            this.txtTaxAllow.Postfix = "";
            this.txtTaxAllow.Prefix = "";
            this.txtTaxAllow.Size = new System.Drawing.Size(107, 20);
            this.txtTaxAllow.SkipValidation = false;
            this.txtTaxAllow.TabIndex = 19;
            this.txtTaxAllow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxAllow.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtTaxAllow, "Field must be of form 999,999,999.99.");
            // 
            // txtOver
            // 
            this.txtOver.AllowSpace = true;
            this.txtOver.AssociatedLookUpName = "";
            this.txtOver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOver.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOver.ContinuationTextBox = null;
            this.txtOver.CustomEnabled = true;
            this.txtOver.DataFieldMapping = "AN_OVT";
            this.txtOver.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOver.GetRecordsOnUpDownKeys = false;
            this.txtOver.IsDate = false;
            this.txtOver.Location = new System.Drawing.Point(325, 167);
            this.txtOver.MaxLength = 10;
            this.txtOver.Name = "txtOver";
            this.txtOver.NumberFormat = "###,###,##0.00";
            this.txtOver.Postfix = "";
            this.txtOver.Prefix = "";
            this.txtOver.Size = new System.Drawing.Size(107, 20);
            this.txtOver.SkipValidation = false;
            this.txtOver.TabIndex = 18;
            this.txtOver.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOver.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtOver, "Field must be of form 999,999,999.99.");
            // 
            // txtZakat
            // 
            this.txtZakat.AllowSpace = true;
            this.txtZakat.AssociatedLookUpName = "";
            this.txtZakat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtZakat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtZakat.ContinuationTextBox = null;
            this.txtZakat.CustomEnabled = true;
            this.txtZakat.DataFieldMapping = "AN_ZAKAT";
            this.txtZakat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZakat.GetRecordsOnUpDownKeys = false;
            this.txtZakat.IsDate = false;
            this.txtZakat.Location = new System.Drawing.Point(325, 144);
            this.txtZakat.MaxLength = 10;
            this.txtZakat.Name = "txtZakat";
            this.txtZakat.NumberFormat = "###,###,##0.00";
            this.txtZakat.Postfix = "";
            this.txtZakat.Prefix = "";
            this.txtZakat.Size = new System.Drawing.Size(107, 20);
            this.txtZakat.SkipValidation = false;
            this.txtZakat.TabIndex = 17;
            this.txtZakat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZakat.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtZakat, "Field must be of form 999,999,999.99.");
            // 
            // txtIncBonus
            // 
            this.txtIncBonus.AllowSpace = true;
            this.txtIncBonus.AssociatedLookUpName = "";
            this.txtIncBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncBonus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncBonus.ContinuationTextBox = null;
            this.txtIncBonus.CustomEnabled = true;
            this.txtIncBonus.DataFieldMapping = "AN_INC_BONUS";
            this.txtIncBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncBonus.GetRecordsOnUpDownKeys = false;
            this.txtIncBonus.IsDate = false;
            this.txtIncBonus.Location = new System.Drawing.Point(325, 121);
            this.txtIncBonus.MaxLength = 10;
            this.txtIncBonus.Name = "txtIncBonus";
            this.txtIncBonus.NumberFormat = "###,###,##0.00";
            this.txtIncBonus.Postfix = "";
            this.txtIncBonus.Prefix = "";
            this.txtIncBonus.Size = new System.Drawing.Size(107, 20);
            this.txtIncBonus.SkipValidation = false;
            this.txtIncBonus.TabIndex = 15;
            this.txtIncBonus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncBonus.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtIncBonus, "Field must be of form 999,999,999.99.");
            // 
            // txtBonus
            // 
            this.txtBonus.AllowSpace = true;
            this.txtBonus.AssociatedLookUpName = "";
            this.txtBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBonus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBonus.ContinuationTextBox = null;
            this.txtBonus.CustomEnabled = true;
            this.txtBonus.DataFieldMapping = "AN_10C_BONUS";
            this.txtBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBonus.GetRecordsOnUpDownKeys = false;
            this.txtBonus.IsDate = false;
            this.txtBonus.Location = new System.Drawing.Point(325, 98);
            this.txtBonus.MaxLength = 10;
            this.txtBonus.Name = "txtBonus";
            this.txtBonus.NumberFormat = "###,###,##0.00";
            this.txtBonus.Postfix = "";
            this.txtBonus.Prefix = "";
            this.txtBonus.Size = new System.Drawing.Size(107, 20);
            this.txtBonus.SkipValidation = false;
            this.txtBonus.TabIndex = 14;
            this.txtBonus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBonus.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtBonus, "Field must be of form 999,999,999.99.");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(42, 189);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "P.Fund:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(17, 164);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "Conv.Added:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(17, 143);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Conveyance:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(18, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "House Rent:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(49, 100);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Basic:";
            // 
            // txtPfund
            // 
            this.txtPfund.AllowSpace = true;
            this.txtPfund.AssociatedLookUpName = "";
            this.txtPfund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPfund.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPfund.ContinuationTextBox = null;
            this.txtPfund.CustomEnabled = true;
            this.txtPfund.DataFieldMapping = "AN_PFUND";
            this.txtPfund.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPfund.GetRecordsOnUpDownKeys = false;
            this.txtPfund.IsDate = false;
            this.txtPfund.Location = new System.Drawing.Point(88, 186);
            this.txtPfund.MaxLength = 10;
            this.txtPfund.Name = "txtPfund";
            this.txtPfund.NumberFormat = "###,###,##0.00";
            this.txtPfund.Postfix = "";
            this.txtPfund.Prefix = "";
            this.txtPfund.Size = new System.Drawing.Size(100, 20);
            this.txtPfund.SkipValidation = false;
            this.txtPfund.TabIndex = 13;
            this.txtPfund.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPfund.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtPfund, "Field must be of form 999,999,999.99.");
            // 
            // txtConAdd
            // 
            this.txtConAdd.AllowSpace = true;
            this.txtConAdd.AssociatedLookUpName = "";
            this.txtConAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConAdd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConAdd.ContinuationTextBox = null;
            this.txtConAdd.CustomEnabled = true;
            this.txtConAdd.DataFieldMapping = "AN_CONV_ADDED";
            this.txtConAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConAdd.GetRecordsOnUpDownKeys = false;
            this.txtConAdd.IsDate = false;
            this.txtConAdd.Location = new System.Drawing.Point(88, 163);
            this.txtConAdd.MaxLength = 10;
            this.txtConAdd.Name = "txtConAdd";
            this.txtConAdd.NumberFormat = "###,###,##0.00";
            this.txtConAdd.Postfix = "";
            this.txtConAdd.Prefix = "";
            this.txtConAdd.Size = new System.Drawing.Size(100, 20);
            this.txtConAdd.SkipValidation = false;
            this.txtConAdd.TabIndex = 12;
            this.txtConAdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtConAdd.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtConAdd, "Field must be of form 999,999,999.99.");
            // 
            // txtConvync
            // 
            this.txtConvync.AllowSpace = true;
            this.txtConvync.AssociatedLookUpName = "";
            this.txtConvync.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConvync.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConvync.ContinuationTextBox = null;
            this.txtConvync.CustomEnabled = true;
            this.txtConvync.DataFieldMapping = "AN_CONV";
            this.txtConvync.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConvync.GetRecordsOnUpDownKeys = false;
            this.txtConvync.IsDate = false;
            this.txtConvync.Location = new System.Drawing.Point(88, 141);
            this.txtConvync.MaxLength = 10;
            this.txtConvync.Name = "txtConvync";
            this.txtConvync.NumberFormat = "###,###,##0.00";
            this.txtConvync.Postfix = "";
            this.txtConvync.Prefix = "";
            this.txtConvync.Size = new System.Drawing.Size(100, 20);
            this.txtConvync.SkipValidation = false;
            this.txtConvync.TabIndex = 11;
            this.txtConvync.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtConvync.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtConvync, "Field must be of form 999,999,999.99.");
            // 
            // txtHousRent
            // 
            this.txtHousRent.AllowSpace = true;
            this.txtHousRent.AssociatedLookUpName = "";
            this.txtHousRent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHousRent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHousRent.ContinuationTextBox = null;
            this.txtHousRent.CustomEnabled = true;
            this.txtHousRent.DataFieldMapping = "AN_HOUSE_RENT";
            this.txtHousRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHousRent.GetRecordsOnUpDownKeys = false;
            this.txtHousRent.IsDate = false;
            this.txtHousRent.Location = new System.Drawing.Point(88, 118);
            this.txtHousRent.MaxLength = 10;
            this.txtHousRent.Name = "txtHousRent";
            this.txtHousRent.NumberFormat = "###,###,##0.00";
            this.txtHousRent.Postfix = "";
            this.txtHousRent.Prefix = "";
            this.txtHousRent.Size = new System.Drawing.Size(100, 20);
            this.txtHousRent.SkipValidation = false;
            this.txtHousRent.TabIndex = 10;
            this.txtHousRent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHousRent.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtHousRent, "Field must be of form 999,999,999.99.");
            // 
            // txtBasic
            // 
            this.txtBasic.AllowSpace = true;
            this.txtBasic.AssociatedLookUpName = "";
            this.txtBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBasic.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBasic.ContinuationTextBox = null;
            this.txtBasic.CustomEnabled = true;
            this.txtBasic.DataFieldMapping = "AN_BASIC";
            this.txtBasic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasic.GetRecordsOnUpDownKeys = false;
            this.txtBasic.IsDate = false;
            this.txtBasic.Location = new System.Drawing.Point(88, 96);
            this.txtBasic.MaxLength = 10;
            this.txtBasic.Name = "txtBasic";
            this.txtBasic.NumberFormat = "###,###,##0.00";
            this.txtBasic.Postfix = "";
            this.txtBasic.Prefix = "";
            this.txtBasic.Size = new System.Drawing.Size(100, 20);
            this.txtBasic.SkipValidation = false;
            this.txtBasic.TabIndex = 9;
            this.txtBasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBasic.TextType = CrplControlLibrary.TextType.Amount;
            this.toolTip1.SetToolTip(this.txtBasic, "Field must be of form 999,999,999.99.");
            // 
            // slDatePicker2
            // 
            this.slDatePicker2.CustomEnabled = true;
            this.slDatePicker2.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker2.DataFieldMapping = "AN_TAX_TO_DATE";
            this.slDatePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slDatePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker2.HasChanges = true;
            this.slDatePicker2.Location = new System.Drawing.Point(260, 60);
            this.slDatePicker2.Name = "slDatePicker2";
            this.slDatePicker2.NullValue = " ";
            this.slDatePicker2.Size = new System.Drawing.Size(106, 20);
            this.slDatePicker2.TabIndex = 7;
            this.slDatePicker2.Value = new System.DateTime(2011, 1, 14, 0, 0, 0, 0);
            // 
            // slDatePicker1
            // 
            this.slDatePicker1.CustomEnabled = true;
            this.slDatePicker1.CustomFormat = "dd/MM/yyyy";
            this.slDatePicker1.DataFieldMapping = "AN_TAX_FROM_DATE";
            this.slDatePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slDatePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.slDatePicker1.HasChanges = true;
            this.slDatePicker1.Location = new System.Drawing.Point(88, 58);
            this.slDatePicker1.Name = "slDatePicker1";
            this.slDatePicker1.NullValue = " ";
            this.slDatePicker1.Size = new System.Drawing.Size(100, 20);
            this.slDatePicker1.TabIndex = 6;
            this.slDatePicker1.Value = new System.DateTime(2011, 1, 14, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(463, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Inc.TaxNo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(480, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Branch:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(380, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Category:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(233, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "To:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(219, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Level:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Tax From:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Designation:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Personnel No:";
            // 
            // txtTax
            // 
            this.txtTax.AllowSpace = true;
            this.txtTax.AssociatedLookUpName = "";
            this.txtTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTax.ContinuationTextBox = null;
            this.txtTax.CustomEnabled = true;
            this.txtTax.DataFieldMapping = "AN_TAX_NUM";
            this.txtTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTax.GetRecordsOnUpDownKeys = false;
            this.txtTax.IsDate = false;
            this.txtTax.Location = new System.Drawing.Point(529, 58);
            this.txtTax.MaxLength = 14;
            this.txtTax.Name = "txtTax";
            this.txtTax.NumberFormat = "###,###,##0.00";
            this.txtTax.Postfix = "";
            this.txtTax.Prefix = "";
            this.txtTax.Size = new System.Drawing.Size(100, 20);
            this.txtTax.SkipValidation = false;
            this.txtTax.TabIndex = 8;
            this.txtTax.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "AN_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(529, 36);
            this.txtBranch.MaxLength = 3;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(100, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 5;
            this.txtBranch.TabStop = false;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCategry
            // 
            this.txtCategry.AllowSpace = true;
            this.txtCategry.AssociatedLookUpName = "";
            this.txtCategry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategry.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategry.ContinuationTextBox = null;
            this.txtCategry.CustomEnabled = true;
            this.txtCategry.DataFieldMapping = "AN_CATEGORY";
            this.txtCategry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategry.GetRecordsOnUpDownKeys = false;
            this.txtCategry.IsDate = false;
            this.txtCategry.Location = new System.Drawing.Point(435, 38);
            this.txtCategry.MaxLength = 1;
            this.txtCategry.Name = "txtCategry";
            this.txtCategry.NumberFormat = "###,###,##0.00";
            this.txtCategry.Postfix = "";
            this.txtCategry.Prefix = "";
            this.txtCategry.ReadOnly = true;
            this.txtCategry.Size = new System.Drawing.Size(39, 20);
            this.txtCategry.SkipValidation = false;
            this.txtCategry.TabIndex = 4;
            this.txtCategry.TabStop = false;
            this.txtCategry.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "AN_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(260, 36);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(100, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 3;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "AN_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(88, 36);
            this.txtDesig.MaxLength = 3;
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(100, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 2;
            this.txtDesig.TabStop = false;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonal
            // 
            this.txtPersonal.AllowSpace = true;
            this.txtPersonal.AssociatedLookUpName = "lbtnPersonel";
            this.txtPersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonal.ContinuationTextBox = null;
            this.txtPersonal.CustomEnabled = true;
            this.txtPersonal.DataFieldMapping = "F_PNO";
            this.txtPersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonal.GetRecordsOnUpDownKeys = false;
            this.txtPersonal.IsDate = false;
            this.txtPersonal.IsRequired = true;
            this.txtPersonal.Location = new System.Drawing.Point(88, 14);
            this.txtPersonal.MaxLength = 6;
            this.txtPersonal.Name = "txtPersonal";
            this.txtPersonal.NumberFormat = "###,###,##0.00";
            this.txtPersonal.Postfix = "";
            this.txtPersonal.Prefix = "";
            this.txtPersonal.Size = new System.Drawing.Size(100, 20);
            this.txtPersonal.SkipValidation = false;
            this.txtPersonal.TabIndex = 0;
            this.txtPersonal.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonal.Leave += new System.EventHandler(this.txtPersonal_Leave);
            this.txtPersonal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersonal_KeyPress);
            this.txtPersonal.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonal_Validating);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label11);
            this.pnlHead.Controls.Add(this.label10);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label12);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label13);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Location = new System.Drawing.Point(5, 93);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(642, 57);
            this.pnlHead.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(249, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(187, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Annual Tax Statement Updation";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(283, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Annual Tax System";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(549, 11);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 3;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(503, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 15);
            this.label12.TabIndex = 16;
            this.label12.Text = "Date:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(90, 32);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(90, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 2;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(90, 6);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(90, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 1;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(3, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 20);
            this.label13.TabIndex = 2;
            this.label13.Text = "Loc:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(32, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 20);
            this.label14.TabIndex = 1;
            this.label14.Text = "User:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labeltest
            // 
            this.labeltest.AutoSize = true;
            this.labeltest.Location = new System.Drawing.Point(378, 9);
            this.labeltest.Name = "labeltest";
            this.labeltest.Size = new System.Drawing.Size(66, 13);
            this.labeltest.TabIndex = 12;
            this.labeltest.Text = "UserName : ";
            // 
            // CHRIS_TaxClosing_TxUptEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(659, 638);
            this.Controls.Add(this.labeltest);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlTaxStatementUpdate);
            this.CurrentPanelBlock = "slPanelSimple1";
            this.Name = "CHRIS_TaxClosing_TxUptEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS -  TaxUpdationEntry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_TaxClosing_TxUptEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlTaxStatementUpdate, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.labeltest, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTaxStatementUpdate.ResumeLayout(false);
            this.pnlTaxStatementUpdate.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlTaxStatementUpdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtTax;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtCategry;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtPersonal;
        private CrplControlLibrary.SLDatePicker slDatePicker2;
        private CrplControlLibrary.SLDatePicker slDatePicker1;
        private System.Windows.Forms.Panel pnlHead;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtOver;
        private CrplControlLibrary.SLTextBox txtZakat;
        private CrplControlLibrary.SLTextBox txtIncBonus;
        private CrplControlLibrary.SLTextBox txtBonus;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtPfund;
        private CrplControlLibrary.SLTextBox txtConAdd;
        private CrplControlLibrary.SLTextBox txtConvync;
        private CrplControlLibrary.SLTextBox txtHousRent;
        private CrplControlLibrary.SLTextBox txtBasic;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLTextBox txtTaxIncme;
        private CrplControlLibrary.SLTextBox txtTaxAsr;
        private CrplControlLibrary.SLTextBox txtSevar;
        private CrplControlLibrary.SLTextBox txtSubLoan;
        private CrplControlLibrary.SLTextBox txtGH;
        private CrplControlLibrary.SLTextBox txtTaxAllow;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private CrplControlLibrary.SLTextBox txtTaxP;
        private CrplControlLibrary.SLTextBox txtTaxPaid;
        private CrplControlLibrary.SLTextBox txtRegulr;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtTaxRefund;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txtLigtWatr;
        private CrplControlLibrary.SLTextBox txtServentSal;
        private CrplControlLibrary.SLTextBox txtEducation;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private CrplControlLibrary.SLTextBox txtRemaingAmt;
        private CrplControlLibrary.SLTextBox txtRebate;
        private CrplControlLibrary.SLTextBox txtSlabAmount;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private CrplControlLibrary.SLTextBox txtPrivate;
        private CrplControlLibrary.SLTextBox txtRFfurnshed;
        private CrplControlLibrary.SLTextBox txtRFUnfurnshed;
        private CrplControlLibrary.SLTextBox txtSurchrge;
        private CrplControlLibrary.SLTextBox txtTaxRemaing;
        private CrplControlLibrary.SLTextBox txtFixedSlab;
        private CrplControlLibrary.LookupButton lbtnPersonel;
        private System.Windows.Forms.Label labeltest;
        private CrplControlLibrary.SLTextBox txtNameNew;
        private CrplControlLibrary.SLTextBox txtID;
    }
}