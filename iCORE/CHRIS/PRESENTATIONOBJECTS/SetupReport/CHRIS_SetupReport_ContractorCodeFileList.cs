using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.SetupReport
{
    public partial class CHRIS_SetupReport_ContractorCodeFileList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_SetupReport_ContractorCodeFileList()
        {
            InitializeComponent();
        }

        public CHRIS_SetupReport_ContractorCodeFileList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        private void CHRIS_SetupReport_ContractorCodeFileList_Load(object sender, EventArgs e)
        {

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            slComboBox1.Items.RemoveAt(this.slComboBox1.Items.Count - 1);
        }


        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "SPREP12";
            if (slComboBox1.Text == "Screen" || slComboBox1.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            if (slComboBox1.Text == "Mail")
            {
                base.EmailToReport("C:\\iCORE-Spool\\SPREP12", "pdf");
            }
            if (slComboBox1.Text == "Printer")
            {
                base.PrintCustomReport();
            }

            if (slComboBox1.Text == "File")
            {
                base.ExportCustomReport("C:\\iCORE-Spool\\SPREP12", "pdf");
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
    }
}