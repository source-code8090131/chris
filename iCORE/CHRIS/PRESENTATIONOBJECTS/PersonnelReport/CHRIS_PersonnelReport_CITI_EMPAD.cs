using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_CITI_EMPAD : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_CITI_EMPAD()
        {
            InitializeComponent();
        }
        String DestFormat; 
        public CHRIS_PersonnelReport_CITI_EMPAD(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            MODE.Items.RemoveAt(3);
            nocopies.Text = "1";
            Dest_Format.Text = "no2_ff";
            this.spno.Text = "";
            this.tpno.Text = "999999";
            this.dpt.Text = "ALL";
            DataTable dt = new DataTable();
            dt.Columns.Add("Segment");
            dt.Rows.Add(new object[] { "ALL" });
            dt.Rows.Add(new object[] { "GF" });
            dt.Rows.Add(new object[] { "GCB" });

            this.seg.ValueMember = "Segment";
            this.seg.DisplayMember = "Segment";
            this.seg.DataSource = dt;
            fillbrnCombo();
            filllevelCombo();
            filldesigCombo();
            fillgrpCombo();


        }
        private void fillbrnCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_BRANCH");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.brn.DisplayMember = "pr_new_branch";
                    this.brn.ValueMember = "pr_new_branch";
                    this.brn.DataSource = rsltCode.dstResult.Tables[0];
                    this.brn.Text = "ALL";
                }

            }

        }

        private void filllevelCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_LEVEL");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.lev.DisplayMember = "lvl";
                    this.lev.ValueMember = "lvl";
                    this.lev.DataSource = rsltCode.dstResult.Tables[0];
                    this.lev.Text = "ALL";
                }

            }



        }



        private void filldesigCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_DESIG");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.p_desig.DisplayMember = "Pr_Desig";
                    this.p_desig.ValueMember = "Pr_Desig";
                    this.p_desig.DataSource = rsltCode.dstResult.Tables[0];
                    this.p_desig.Text = "ALL";
                }

            }



        }


        private void fillgrpCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_GROUP");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.p_grp.DisplayMember = "GRP";
                    this.p_grp.ValueMember = "GRP";
                    this.p_grp.DataSource = rsltCode.dstResult.Tables[0];
                    this.p_grp.Text = "ALL";
                }

            }



        }

        private void Run_Click(object sender, EventArgs e)
        {
            {

                base.RptFileName = "citi_empad";
                //base.btnCallReport_Click(sender, e);
                if (this.Dest_Format.Text == String.Empty)
                    DestFormat = "PDF";
                else
                    DestFormat = this.Dest_Format.Text;

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.nocopies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;

                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";

                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }



                    base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }

        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

    }
}