using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_OverTimeEntryReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_OverTimeEntryReport()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_OverTimeEntryReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.copies1.Text = "1";
            this.W_P_NO.Text = "1";
            this.W_P_NO1.Text = "999999";
            
        }


        private void Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PY017";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }

            if (cmbDescType.Text == "File")
            {
                string d = "C:\\iCORE-Spool\\Report";
                if (this.destype.Text != string.Empty)
                    d = this.destype.Text;

                base.ExportCustomReport(d, "pdf");


            }

            if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.copies1.Text);
            }
 
            if (cmbDescType.Text == "Mail")
            {
                string d = "";
                if (destype.Text != string.Empty)
                    d = destype.Text;


                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }
            
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.Close();
        }
    }
}