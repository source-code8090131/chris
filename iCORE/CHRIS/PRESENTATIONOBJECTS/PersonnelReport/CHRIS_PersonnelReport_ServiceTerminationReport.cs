using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ServiceTerminationReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm

    {
        public CHRIS_PersonnelReport_ServiceTerminationReport()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";//@"C:\PRREP07";
        string DestFormat = "PDF";

        public CHRIS_PersonnelReport_ServiceTerminationReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

    
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            Dest_Format.Text = "WIDE";
            Dest_name.Text = @"C:\iCORE-Spool\PRREP07";//DestName;
            mode.Items.RemoveAt(3);
            w_BRN.Text = "ALL";
            w_desig.Text = "ALL";
            w_hold.Text = "ALL";
            w_seg.Text = "ALL";
            
            this.s_date.Value = new DateTime(2011, 03, 14);
            this.e_date.Value = new DateTime(2011,03,14);
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }

      
        private void Run_Click(object sender, EventArgs e)
        {

            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);

            {
                base.RptFileName = "PRREP07";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {
                     base.PrintNoofCopiesReport(no_of_copies);
                }



                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }


            }

        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        
        }

     

       
        
 

        
    }
}