namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_ReportForConfirmationDue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_ReportForConfirmationDue));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Close = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Pr_Desig = new CrplControlLibrary.SLTextBox(this.components);
            this.Pr_Segment = new CrplControlLibrary.SLTextBox(this.components);
            this.Expiry_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.Issue_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.slComboBox2 = new CrplControlLibrary.SLComboBox();
            this.Pr_New_Brn = new CrplControlLibrary.SLTextBox(this.components);
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Pr_Desig);
            this.groupBox1.Controls.Add(this.Pr_Segment);
            this.groupBox1.Controls.Add(this.Expiry_Date);
            this.groupBox1.Controls.Add(this.Issue_Date);
            this.groupBox1.Controls.Add(this.slComboBox2);
            this.groupBox1.Controls.Add(this.Pr_New_Brn);
            this.groupBox1.Controls.Add(this.nocopies);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(463, 380);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // Close
            // 
            this.Close.ActionType = "";
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(268, 314);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 11;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(187, 314);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 10;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(403, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(37, 291);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Designation Code or All :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(146, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(163, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Enter Values For The Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(37, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Segment Code or All :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(37, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Confirmation Due Date :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(172, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 16);
            this.label10.TabIndex = 2;
            this.label10.Text = "Report Parameters";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(37, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Confirmation Due From :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Mode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(37, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Branch Code or All :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destype";
            // 
            // Pr_Desig
            // 
            this.Pr_Desig.AllowSpace = true;
            this.Pr_Desig.AssociatedLookUpName = "";
            this.Pr_Desig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pr_Desig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Pr_Desig.ContinuationTextBox = null;
            this.Pr_Desig.CustomEnabled = true;
            this.Pr_Desig.DataFieldMapping = "";
            this.Pr_Desig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pr_Desig.GetRecordsOnUpDownKeys = false;
            this.Pr_Desig.IsDate = false;
            this.Pr_Desig.Location = new System.Drawing.Point(187, 288);
            this.Pr_Desig.MaxLength = 6;
            this.Pr_Desig.Name = "Pr_Desig";
            this.Pr_Desig.NumberFormat = "###,###,##0.00";
            this.Pr_Desig.Postfix = "";
            this.Pr_Desig.Prefix = "";
            this.Pr_Desig.Size = new System.Drawing.Size(156, 20);
            this.Pr_Desig.SkipValidation = false;
            this.Pr_Desig.TabIndex = 9;
            this.Pr_Desig.TextType = CrplControlLibrary.TextType.String;
            // 
            // Pr_Segment
            // 
            this.Pr_Segment.AllowSpace = true;
            this.Pr_Segment.AssociatedLookUpName = "";
            this.Pr_Segment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pr_Segment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Pr_Segment.ContinuationTextBox = null;
            this.Pr_Segment.CustomEnabled = true;
            this.Pr_Segment.DataFieldMapping = "";
            this.Pr_Segment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pr_Segment.GetRecordsOnUpDownKeys = false;
            this.Pr_Segment.IsDate = false;
            this.Pr_Segment.Location = new System.Drawing.Point(187, 262);
            this.Pr_Segment.MaxLength = 3;
            this.Pr_Segment.Name = "Pr_Segment";
            this.Pr_Segment.NumberFormat = "###,###,##0.00";
            this.Pr_Segment.Postfix = "";
            this.Pr_Segment.Prefix = "";
            this.Pr_Segment.Size = new System.Drawing.Size(156, 20);
            this.Pr_Segment.SkipValidation = false;
            this.Pr_Segment.TabIndex = 8;
            this.Pr_Segment.TextType = CrplControlLibrary.TextType.String;
            // 
            // Expiry_Date
            // 
            this.Expiry_Date.CustomEnabled = true;
            this.Expiry_Date.CustomFormat = "dd/MM/yyyy";
            this.Expiry_Date.DataFieldMapping = "";
            this.Expiry_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Expiry_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Expiry_Date.HasChanges = true;
            this.Expiry_Date.Location = new System.Drawing.Point(187, 236);
            this.Expiry_Date.Name = "Expiry_Date";
            this.Expiry_Date.NullValue = " ";
            this.Expiry_Date.Size = new System.Drawing.Size(156, 20);
            this.Expiry_Date.TabIndex = 7;
            this.Expiry_Date.Value = new System.DateTime(2010, 12, 21, 0, 0, 0, 0);
            // 
            // Issue_Date
            // 
            this.Issue_Date.CustomEnabled = true;
            this.Issue_Date.CustomFormat = "dd/MM/yyyy";
            this.Issue_Date.DataFieldMapping = "";
            this.Issue_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Issue_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Issue_Date.HasChanges = true;
            this.Issue_Date.Location = new System.Drawing.Point(187, 210);
            this.Issue_Date.Name = "Issue_Date";
            this.Issue_Date.NullValue = " ";
            this.Issue_Date.Size = new System.Drawing.Size(156, 20);
            this.Issue_Date.TabIndex = 6;
            this.Issue_Date.Value = new System.DateTime(2010, 12, 21, 0, 0, 0, 0);
            // 
            // slComboBox2
            // 
            this.slComboBox2.BusinessEntity = "";
            this.slComboBox2.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.slComboBox2.CustomEnabled = true;
            this.slComboBox2.DataFieldMapping = "";
            this.slComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.slComboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slComboBox2.FormattingEnabled = true;
            this.slComboBox2.Items.AddRange(new object[] {
            "Default",
            "Bitmap",
            "Character"});
            this.slComboBox2.Location = new System.Drawing.Point(187, 184);
            this.slComboBox2.LOVType = "";
            this.slComboBox2.Name = "slComboBox2";
            this.slComboBox2.Size = new System.Drawing.Size(156, 21);
            this.slComboBox2.SPName = "";
            this.slComboBox2.TabIndex = 5;
            // 
            // Pr_New_Brn
            // 
            this.Pr_New_Brn.AllowSpace = true;
            this.Pr_New_Brn.AssociatedLookUpName = "";
            this.Pr_New_Brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pr_New_Brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Pr_New_Brn.ContinuationTextBox = null;
            this.Pr_New_Brn.CustomEnabled = true;
            this.Pr_New_Brn.DataFieldMapping = "";
            this.Pr_New_Brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pr_New_Brn.GetRecordsOnUpDownKeys = false;
            this.Pr_New_Brn.IsDate = false;
            this.Pr_New_Brn.Location = new System.Drawing.Point(187, 158);
            this.Pr_New_Brn.MaxLength = 3;
            this.Pr_New_Brn.Name = "Pr_New_Brn";
            this.Pr_New_Brn.NumberFormat = "###,###,##0.00";
            this.Pr_New_Brn.Postfix = "";
            this.Pr_New_Brn.Prefix = "";
            this.Pr_New_Brn.Size = new System.Drawing.Size(156, 20);
            this.Pr_New_Brn.SkipValidation = false;
            this.Pr_New_Brn.TabIndex = 4;
            this.Pr_New_Brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(187, 132);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(156, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 3;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(187, 106);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(156, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(187, 80);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(156, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // CHRIS_PersonnelReport_ReportForConfirmationDue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 451);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_ReportForConfirmationDue";
            this.Text = "iCORE-Chris-ReportForConfirmationDue";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox Pr_Desig;
        private CrplControlLibrary.SLTextBox Pr_Segment;
        private CrplControlLibrary.SLDatePicker Expiry_Date;
        private CrplControlLibrary.SLDatePicker Issue_Date;
        private CrplControlLibrary.SLComboBox slComboBox2;
        private CrplControlLibrary.SLTextBox Pr_New_Brn;
        private CrplControlLibrary.SLTextBox nocopies;
        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CrplControlLibrary.SLButton Run;
        private CrplControlLibrary.SLButton Close;
    }
}