using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_RptForNewInduction : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm 
        
      
    {
        public CHRIS_PersonnelReport_RptForNewInduction()
        {
            InitializeComponent();
        }
        string DestName = @"C:\Report";
        string DestFormat = "PDF";

        public CHRIS_PersonnelReport_RptForNewInduction(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            slComboBox1.Items.RemoveAt(3);
            this.Dest_name.Text = "c:\\prep06.lis";
            Dest_Format.Text = "WIDE";
            W_BRN.Text = "ALL";
            W_DESIG.Text = "ALL";
            W_GROUP.Text = "ALL";
            W_SEG.Text = "ALL";


        }


        private void Btn_Run_Click(object sender, EventArgs e)
        {

            base.RptFileName = "PRREP06";

            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (Dest_Type.Text == "Printer")
            {
                PrintCustomReport();
            }
             //if (Dest_Type.Text == "File")
             //   {
             //       if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
             //       {
             //           base.ExportCustomReport(Dest_name.Text, Dest_name.Text);

             //       }

             //   }

            if (Dest_Type.Text == "File")
            {
                string d = "c:\\report";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;

                base.ExportCustomReport(d, "pdf");


            }


            if (Dest_Type.Text == "Mail")
            {
                string d = "";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;


                base.EmailToReport(@"C:\Report", "PDF", d);

            }
                

            }

        

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

        
    }
}