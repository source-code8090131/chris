namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_AttritionReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_AttritionReport));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ENDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.STDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Close = new System.Windows.Forms.Button();
            this.Run = new System.Windows.Forms.Button();
            this.DPT = new CrplControlLibrary.SLComboBox();
            this.GP = new CrplControlLibrary.SLComboBox();
            this.SG = new CrplControlLibrary.SLComboBox();
            this.BRN = new CrplControlLibrary.SLComboBox();
            this.TYPCD = new CrplControlLibrary.SLComboBox();
            this.CATCD = new CrplControlLibrary.SLComboBox();
            this.order = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.ENDATE);
            this.groupBox1.Controls.Add(this.STDATE);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.DPT);
            this.groupBox1.Controls.Add(this.GP);
            this.groupBox1.Controls.Add(this.SG);
            this.groupBox1.Controls.Add(this.BRN);
            this.groupBox1.Controls.Add(this.TYPCD);
            this.groupBox1.Controls.Add(this.CATCD);
            this.groupBox1.Controls.Add(this.order);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(9, 46);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(635, 508);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(549, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(85, 81);
            this.pictureBox1.TabIndex = 133;
            this.pictureBox1.TabStop = false;
            // 
            // ENDATE
            // 
            this.ENDATE.CustomEnabled = true;
            this.ENDATE.CustomFormat = "dd/MM/yyyy";
            this.ENDATE.DataFieldMapping = "";
            this.ENDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ENDATE.HasChanges = true;
            this.ENDATE.Location = new System.Drawing.Point(296, 198);
            this.ENDATE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ENDATE.Name = "ENDATE";
            this.ENDATE.NullValue = " ";
            this.ENDATE.Size = new System.Drawing.Size(243, 22);
            this.ENDATE.TabIndex = 38;
            this.ENDATE.Value = new System.DateTime(2011, 2, 8, 0, 0, 0, 0);
            // 
            // STDATE
            // 
            this.STDATE.CustomEnabled = true;
            this.STDATE.CustomFormat = "dd/MM/yyyy";
            this.STDATE.DataFieldMapping = "";
            this.STDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.STDATE.HasChanges = true;
            this.STDATE.Location = new System.Drawing.Point(296, 166);
            this.STDATE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.STDATE.Name = "STDATE";
            this.STDATE.NullValue = " ";
            this.STDATE.Size = new System.Drawing.Size(243, 22);
            this.STDATE.TabIndex = 37;
            this.STDATE.Value = new System.DateTime(2011, 2, 8, 0, 0, 0, 0);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(63, 436);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 17);
            this.label13.TabIndex = 36;
            this.label13.Text = "Enter Sorting Order";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(63, 402);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(189, 17);
            this.label12.TabIndex = 35;
            this.label12.Text = "Enter Department or Null";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(63, 368);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(150, 17);
            this.label11.TabIndex = 34;
            this.label11.Text = "Enter Group or Null";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(63, 334);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 17);
            this.label10.TabIndex = 33;
            this.label10.Text = "Enter Segment or Null";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(63, 300);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 17);
            this.label7.TabIndex = 32;
            this.label7.Text = "Enter Branch or Null";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 265);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 17);
            this.label6.TabIndex = 31;
            this.label6.Text = "Enter Type or Null";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(63, 236);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 17);
            this.label5.TabIndex = 30;
            this.label5.Text = "Enter Category or Null";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(63, 203);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 17);
            this.label4.TabIndex = 29;
            this.label4.Text = "End Date (DD/MM/YYYY)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(63, 171);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 17);
            this.label3.TabIndex = 28;
            this.label3.Text = "Start Date (DD/MM/YYYY)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 139);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 17);
            this.label2.TabIndex = 27;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 106);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "Destination Type";
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(404, 465);
            this.Close.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(100, 28);
            this.Close.TabIndex = 25;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Run
            // 
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(296, 465);
            this.Run.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(100, 28);
            this.Run.TabIndex = 24;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // DPT
            // 
            this.DPT.BusinessEntity = "";
            this.DPT.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.DPT.CustomEnabled = true;
            this.DPT.DataFieldMapping = "";
            this.DPT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DPT.FormattingEnabled = true;
            this.DPT.Location = new System.Drawing.Point(296, 399);
            this.DPT.LOVType = "";
            this.DPT.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DPT.Name = "DPT";
            this.DPT.Size = new System.Drawing.Size(243, 24);
            this.DPT.SPName = "";
            this.DPT.TabIndex = 23;
            // 
            // GP
            // 
            this.GP.BusinessEntity = "";
            this.GP.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.GP.CustomEnabled = true;
            this.GP.DataFieldMapping = "";
            this.GP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GP.FormattingEnabled = true;
            this.GP.Location = new System.Drawing.Point(296, 364);
            this.GP.LOVType = "";
            this.GP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GP.Name = "GP";
            this.GP.Size = new System.Drawing.Size(243, 24);
            this.GP.SPName = "";
            this.GP.TabIndex = 22;
            // 
            // SG
            // 
            this.SG.BusinessEntity = "";
            this.SG.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.SG.CustomEnabled = true;
            this.SG.DataFieldMapping = "";
            this.SG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SG.FormattingEnabled = true;
            this.SG.Location = new System.Drawing.Point(296, 330);
            this.SG.LOVType = "";
            this.SG.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SG.Name = "SG";
            this.SG.Size = new System.Drawing.Size(243, 24);
            this.SG.SPName = "";
            this.SG.TabIndex = 21;
            // 
            // BRN
            // 
            this.BRN.BusinessEntity = "";
            this.BRN.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.BRN.CustomEnabled = true;
            this.BRN.DataFieldMapping = "";
            this.BRN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BRN.FormattingEnabled = true;
            this.BRN.Location = new System.Drawing.Point(296, 297);
            this.BRN.LOVType = "";
            this.BRN.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BRN.Name = "BRN";
            this.BRN.Size = new System.Drawing.Size(243, 24);
            this.BRN.SPName = "";
            this.BRN.TabIndex = 20;
            // 
            // TYPCD
            // 
            this.TYPCD.BusinessEntity = "";
            this.TYPCD.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.TYPCD.CustomEnabled = true;
            this.TYPCD.DataFieldMapping = "";
            this.TYPCD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TYPCD.FormatString = "STRING";
            this.TYPCD.FormattingEnabled = true;
            this.TYPCD.Location = new System.Drawing.Point(296, 265);
            this.TYPCD.LOVType = "";
            this.TYPCD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TYPCD.Name = "TYPCD";
            this.TYPCD.Size = new System.Drawing.Size(243, 24);
            this.TYPCD.SPName = "";
            this.TYPCD.TabIndex = 19;
            // 
            // CATCD
            // 
            this.CATCD.BusinessEntity = "";
            this.CATCD.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.CATCD.CustomEnabled = true;
            this.CATCD.DataFieldMapping = "";
            this.CATCD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CATCD.FormatString = "STRING";
            this.CATCD.FormattingEnabled = true;
            this.CATCD.Location = new System.Drawing.Point(296, 233);
            this.CATCD.LOVType = "";
            this.CATCD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CATCD.Name = "CATCD";
            this.CATCD.Size = new System.Drawing.Size(243, 24);
            this.CATCD.SPName = "";
            this.CATCD.TabIndex = 18;
            // 
            // order
            // 
            this.order.AllowSpace = true;
            this.order.AssociatedLookUpName = "";
            this.order.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.order.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.order.ContinuationTextBox = null;
            this.order.CustomEnabled = true;
            this.order.DataFieldMapping = "";
            this.order.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.order.GetRecordsOnUpDownKeys = false;
            this.order.IsDate = false;
            this.order.Location = new System.Drawing.Point(296, 433);
            this.order.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.order.MaxLength = 100;
            this.order.Name = "order";
            this.order.NumberFormat = "###,###,##0.00";
            this.order.Postfix = "";
            this.order.Prefix = "";
            this.order.Size = new System.Drawing.Size(243, 24);
            this.order.SkipValidation = false;
            this.order.TabIndex = 17;
            this.order.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(296, 137);
            this.Dest_Name.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(243, 24);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 14;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(296, 102);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(243, 24);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(164, 38);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(276, 20);
            this.label9.TabIndex = 12;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(213, 12);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 20);
            this.label8.TabIndex = 11;
            this.label8.Text = "Report Parameters";
            // 
            // CHRIS_PersonnelReport_AttritionReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 567);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(9, 7, 9, 7);
            this.Name = "CHRIS_PersonnelReport_AttritionReport";
            this.Text = "CHRIS_PersonnelReport_AttritionReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLComboBox DPT;
        private CrplControlLibrary.SLComboBox GP;
        private CrplControlLibrary.SLComboBox SG;
        private CrplControlLibrary.SLComboBox BRN;
        private CrplControlLibrary.SLComboBox TYPCD;
        private CrplControlLibrary.SLComboBox CATCD;
        private CrplControlLibrary.SLTextBox order;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Run;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLDatePicker ENDATE;
        private CrplControlLibrary.SLDatePicker STDATE;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}