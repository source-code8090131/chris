namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_AcademicQualificationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_AcademicQualificationReport));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.label6 = new System.Windows.Forms.Label();
            this.Run = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.w_year = new CrplControlLibrary.SLTextBox(this.components);
            this.W_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            this.DESNAME = new CrplControlLibrary.SLTextBox(this.components);
            this.TODAY = new CrplControlLibrary.SLDatePicker(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.w_year);
            this.groupBox1.Controls.Add(this.W_DESIG);
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.W_BRN);
            this.groupBox1.Controls.Add(this.DESNAME);
            this.groupBox1.Controls.Add(this.TODAY);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 372);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(377, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(66, 292);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Last Edu. Year [YYYY]";
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(298, 319);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 8;
            this.slButton1.Text = "Close";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(66, 265);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Enter valid designation :";
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(211, 319);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 7;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(66, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Enter valid segment :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(66, 211);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Enter valid branch :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(66, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Desname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(66, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Today";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Destype";
            // 
            // w_year
            // 
            this.w_year.AllowSpace = true;
            this.w_year.AssociatedLookUpName = "";
            this.w_year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_year.ContinuationTextBox = null;
            this.w_year.CustomEnabled = true;
            this.w_year.DataFieldMapping = "";
            this.w_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_year.GetRecordsOnUpDownKeys = false;
            this.w_year.IsDate = false;
            this.w_year.Location = new System.Drawing.Point(211, 290);
            this.w_year.MaxLength = 4;
            this.w_year.Name = "w_year";
            this.w_year.NumberFormat = "###,###,##0.00";
            this.w_year.Postfix = "";
            this.w_year.Prefix = "";
            this.w_year.Size = new System.Drawing.Size(163, 20);
            this.w_year.SkipValidation = false;
            this.w_year.TabIndex = 6;
            this.w_year.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // W_DESIG
            // 
            this.W_DESIG.AllowSpace = true;
            this.W_DESIG.AssociatedLookUpName = "";
            this.W_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_DESIG.ContinuationTextBox = null;
            this.W_DESIG.CustomEnabled = true;
            this.W_DESIG.DataFieldMapping = "";
            this.W_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_DESIG.GetRecordsOnUpDownKeys = false;
            this.W_DESIG.IsDate = false;
            this.W_DESIG.Location = new System.Drawing.Point(211, 263);
            this.W_DESIG.MaxLength = 6;
            this.W_DESIG.Name = "W_DESIG";
            this.W_DESIG.NumberFormat = "###,###,##0.00";
            this.W_DESIG.Postfix = "";
            this.W_DESIG.Prefix = "";
            this.W_DESIG.Size = new System.Drawing.Size(163, 20);
            this.W_DESIG.SkipValidation = false;
            this.W_DESIG.TabIndex = 5;
            this.W_DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(211, 236);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(163, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 4;
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_BRN
            // 
            this.W_BRN.AllowSpace = true;
            this.W_BRN.AssociatedLookUpName = "";
            this.W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRN.ContinuationTextBox = null;
            this.W_BRN.CustomEnabled = true;
            this.W_BRN.DataFieldMapping = "";
            this.W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRN.GetRecordsOnUpDownKeys = false;
            this.W_BRN.IsDate = false;
            this.W_BRN.Location = new System.Drawing.Point(211, 209);
            this.W_BRN.MaxLength = 3;
            this.W_BRN.Name = "W_BRN";
            this.W_BRN.NumberFormat = "###,###,##0.00";
            this.W_BRN.Postfix = "";
            this.W_BRN.Prefix = "";
            this.W_BRN.Size = new System.Drawing.Size(163, 20);
            this.W_BRN.SkipValidation = false;
            this.W_BRN.TabIndex = 3;
            this.W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // DESNAME
            // 
            this.DESNAME.AllowSpace = true;
            this.DESNAME.AssociatedLookUpName = "";
            this.DESNAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DESNAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DESNAME.ContinuationTextBox = null;
            this.DESNAME.CustomEnabled = true;
            this.DESNAME.DataFieldMapping = "";
            this.DESNAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DESNAME.GetRecordsOnUpDownKeys = false;
            this.DESNAME.IsDate = false;
            this.DESNAME.Location = new System.Drawing.Point(211, 182);
            this.DESNAME.MaxLength = 50;
            this.DESNAME.Name = "DESNAME";
            this.DESNAME.NumberFormat = "###,###,##0.00";
            this.DESNAME.Postfix = "";
            this.DESNAME.Prefix = "";
            this.DESNAME.Size = new System.Drawing.Size(163, 20);
            this.DESNAME.SkipValidation = false;
            this.DESNAME.TabIndex = 2;
            this.DESNAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // TODAY
            // 
            this.TODAY.CustomEnabled = true;
            this.TODAY.CustomFormat = "dd/MM/yyyy";
            this.TODAY.DataFieldMapping = "";
            this.TODAY.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TODAY.HasChanges = true;
            this.TODAY.Location = new System.Drawing.Point(211, 155);
            this.TODAY.Name = "TODAY";
            this.TODAY.NullValue = " ";
            this.TODAY.Size = new System.Drawing.Size(163, 20);
            this.TODAY.TabIndex = 1;
            this.TODAY.Value = new System.DateTime(2010, 12, 6, 0, 0, 0, 0);
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(211, 100);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(163, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(153, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Report Parameters";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(115, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(66, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 83;
            this.label10.Text = "Destination Name";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(211, 128);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(163, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 82;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_PersonnelReport_AcademicQualificationReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 424);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_AcademicQualificationReport";
            this.Text = "iCORE-Chris - Academic Qualification Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLDatePicker TODAY;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox w_year;
        private CrplControlLibrary.SLTextBox W_DESIG;
        private CrplControlLibrary.SLTextBox W_SEG;
        private CrplControlLibrary.SLTextBox W_BRN;
        private CrplControlLibrary.SLTextBox DESNAME;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLButton Run;
        private CrplControlLibrary.SLButton slButton1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox Dest_name;
    }
}