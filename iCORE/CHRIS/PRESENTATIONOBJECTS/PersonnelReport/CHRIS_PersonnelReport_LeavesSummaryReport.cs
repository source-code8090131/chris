using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
 
    public partial class CHRIS_PersonnelReport_LeavesSummaryReport  : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
      //  public string a;
        public CHRIS_PersonnelReport_LeavesSummaryReport()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_LeavesSummaryReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.Copies.Text = "1";
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }
        private void RUN_Click(object sender, EventArgs e)
        {
            base.RptFileName = "Prrep17";
            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }

            if (Dest_Type.Text == "File")
            {
                string d = "C:\\iCORE-Spool\\Report";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;

                base.ExportCustomReport(d, "pdf");


            }


            if (Dest_Type.Text == "Mail")
            {
                string d = "";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;


                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }

            if (Dest_Type.Text == "Printer")
            {
                PrintCustomReport(this.Copies.Text );
            }

        }

        private void CLOSE_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

      
        

        
    }
}