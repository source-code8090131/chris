namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_NewInduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_NewInduction));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Close = new CrplControlLibrary.SLButton();
            this.W_GROUP = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.W_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.Btn_Run = new CrplControlLibrary.SLButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.E_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.S_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.slComboBox1 = new CrplControlLibrary.SLComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.W_GROUP);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.W_BRN);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.W_DESIG);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.Btn_Run);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.E_DATE);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.S_DATE);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.slComboBox1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(28, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 367);
            this.groupBox1.TabIndex = 85;
            this.groupBox1.TabStop = false;
            // 
            // Close
            // 
            this.Close.ActionType = "";
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(269, 323);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 12;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // W_GROUP
            // 
            this.W_GROUP.AllowSpace = true;
            this.W_GROUP.AssociatedLookUpName = "";
            this.W_GROUP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_GROUP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_GROUP.ContinuationTextBox = null;
            this.W_GROUP.CustomEnabled = true;
            this.W_GROUP.DataFieldMapping = "";
            this.W_GROUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_GROUP.GetRecordsOnUpDownKeys = false;
            this.W_GROUP.IsDate = false;
            this.W_GROUP.Location = new System.Drawing.Point(195, 297);
            this.W_GROUP.MaxLength = 6;
            this.W_GROUP.Name = "W_GROUP";
            this.W_GROUP.NumberFormat = "###,###,##0.00";
            this.W_GROUP.Postfix = "";
            this.W_GROUP.Prefix = "";
            this.W_GROUP.Size = new System.Drawing.Size(139, 20);
            this.W_GROUP.SkipValidation = false;
            this.W_GROUP.TabIndex = 10;
            this.W_GROUP.Text = "ALL";
            this.W_GROUP.TextType = CrplControlLibrary.TextType.String;
            this.W_GROUP.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(139, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(185, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "Enter values for the parameters";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // W_BRN
            // 
            this.W_BRN.AllowSpace = true;
            this.W_BRN.AssociatedLookUpName = "";
            this.W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRN.ContinuationTextBox = null;
            this.W_BRN.CustomEnabled = true;
            this.W_BRN.DataFieldMapping = "";
            this.W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRN.GetRecordsOnUpDownKeys = false;
            this.W_BRN.IsDate = false;
            this.W_BRN.Location = new System.Drawing.Point(195, 271);
            this.W_BRN.MaxLength = 3;
            this.W_BRN.Name = "W_BRN";
            this.W_BRN.NumberFormat = "###,###,##0.00";
            this.W_BRN.Postfix = "";
            this.W_BRN.Prefix = "";
            this.W_BRN.Size = new System.Drawing.Size(139, 20);
            this.W_BRN.SkipValidation = false;
            this.W_BRN.TabIndex = 9;
            this.W_BRN.Text = "ALL";
            this.W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(166, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Report Parameters";
            // 
            // W_DESIG
            // 
            this.W_DESIG.AllowSpace = true;
            this.W_DESIG.AssociatedLookUpName = "";
            this.W_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_DESIG.ContinuationTextBox = null;
            this.W_DESIG.CustomEnabled = true;
            this.W_DESIG.DataFieldMapping = "";
            this.W_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_DESIG.GetRecordsOnUpDownKeys = false;
            this.W_DESIG.IsDate = false;
            this.W_DESIG.Location = new System.Drawing.Point(195, 245);
            this.W_DESIG.MaxLength = 6;
            this.W_DESIG.Name = "W_DESIG";
            this.W_DESIG.NumberFormat = "###,###,##0.00";
            this.W_DESIG.Postfix = "";
            this.W_DESIG.Prefix = "";
            this.W_DESIG.Size = new System.Drawing.Size(139, 20);
            this.W_DESIG.SkipValidation = false;
            this.W_DESIG.TabIndex = 8;
            this.W_DESIG.Text = "ALL";
            this.W_DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(367, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 80;
            this.pictureBox2.TabStop = false;
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(195, 219);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(139, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 7;
            this.W_SEG.Text = "ALL";
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // Btn_Run
            // 
            this.Btn_Run.ActionType = "";
            this.Btn_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Run.Location = new System.Drawing.Point(191, 323);
            this.Btn_Run.Name = "Btn_Run";
            this.Btn_Run.Size = new System.Drawing.Size(72, 23);
            this.Btn_Run.TabIndex = 11;
            this.Btn_Run.Text = "Run";
            this.Btn_Run.UseVisualStyleBackColor = true;
            this.Btn_Run.Click += new System.EventHandler(this.Btn_Run_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(163, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Enter Group Code or \'ALL\' :";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(169, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Enter Branch Code or \'ALL\' :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Enter Desig.Code or \'ALL\' :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 218);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Enter Valid Segment :";
            // 
            // E_DATE
            // 
            this.E_DATE.CustomEnabled = true;
            this.E_DATE.CustomFormat = "dd/MM/yyyy";
            this.E_DATE.DataFieldMapping = "";
            this.E_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.E_DATE.HasChanges = true;
            this.E_DATE.Location = new System.Drawing.Point(195, 193);
            this.E_DATE.Name = "E_DATE";
            this.E_DATE.NullValue = " ";
            this.E_DATE.Size = new System.Drawing.Size(139, 20);
            this.E_DATE.TabIndex = 6;
            this.E_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(168, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Joining To [DD/MM/YYYY] :";
            // 
            // S_DATE
            // 
            this.S_DATE.CustomEnabled = true;
            this.S_DATE.CustomFormat = "dd/MM/yyyy";
            this.S_DATE.DataFieldMapping = "";
            this.S_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.S_DATE.HasChanges = true;
            this.S_DATE.Location = new System.Drawing.Point(195, 167);
            this.S_DATE.Name = "S_DATE";
            this.S_DATE.NullValue = " ";
            this.S_DATE.Size = new System.Drawing.Size(139, 20);
            this.S_DATE.TabIndex = 5;
            this.S_DATE.Value = new System.DateTime(2000, 12, 31, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Joining From [DD/MM/YYYY] :";
            // 
            // slComboBox1
            // 
            this.slComboBox1.BusinessEntity = "";
            this.slComboBox1.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.slComboBox1.CustomEnabled = true;
            this.slComboBox1.DataFieldMapping = "";
            this.slComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.slComboBox1.FormattingEnabled = true;
            this.slComboBox1.Items.AddRange(new object[] {
            "Default",
            "Bitmap",
            "Character"});
            this.slComboBox1.Location = new System.Drawing.Point(195, 140);
            this.slComboBox1.LOVType = "";
            this.slComboBox1.Name = "slComboBox1";
            this.slComboBox1.Size = new System.Drawing.Size(139, 21);
            this.slComboBox1.SPName = "";
            this.slComboBox1.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Mode";
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(195, 114);
            this.Dest_Format.MaxLength = 50;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(139, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 3;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Desformat";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(195, 90);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(139, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Desname";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(195, 63);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(139, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Destype";
            // 
            // CHRIS_PersonnelReport_NewInduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 415);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_NewInduction";
            this.Text = "CHRIS_PersonnelReport_NewInduction";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLTextBox W_GROUP;
        private CrplControlLibrary.SLTextBox W_BRN;
        private CrplControlLibrary.SLTextBox W_DESIG;
        private CrplControlLibrary.SLTextBox W_SEG;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLDatePicker E_DATE;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLDatePicker S_DATE;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLComboBox slComboBox1;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLButton Btn_Run;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLButton Close;

    }
}