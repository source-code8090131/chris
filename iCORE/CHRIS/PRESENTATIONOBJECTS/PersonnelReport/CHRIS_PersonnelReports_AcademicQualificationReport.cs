using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport


{
    public partial class CHRIS_PersonnelReport_AcademicQualificationReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_AcademicQualificationReport()
        {
            InitializeComponent();
        }
        public CHRIS_PersonnelReport_AcademicQualificationReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        string DestName = @"C:\Report";
        string DestFormat = "PDF";
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.W_BRN.Text = "All";
            this.W_SEG.Text = "All";
            this.W_DESIG.Text = "All";
            this.w_year.Text = "1995";
        }
        private void Run_Click(object sender, EventArgs e)
        {
       
            base.RptFileName = "PRREP32";
           
            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (Dest_Type.Text == "Printer")
            {
                PrintCustomReport();

               
            }
            if (Dest_Type.Text == "File")
            {
                if (DestFormat!= string.Empty && Dest_name.Text != string.Empty)
                {
                    base.ExportCustomReport(Dest_name.Text, DestFormat);
                }

            }

            if (Dest_Type.Text == "Mail")
            {

                if (DestFormat != string.Empty || Dest_name.Text != string.Empty)
                {
                    base.EmailToReport(@"C:\Report", "PDF");
                }
            }

        }

        
        private void slButton1_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

       
    }
}