namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_AttritionSummaryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_AttritionSummaryReport));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Btn_Close = new CrplControlLibrary.SLButton();
            this.Btn_Run = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.sg = new CrplControlLibrary.SLComboBox();
            this.brn = new CrplControlLibrary.SLComboBox();
            this.ENDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.STDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDesType = new CrplControlLibrary.SLComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(366, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 75;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Btn_Close);
            this.groupBox1.Controls.Add(this.Btn_Run);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.sg);
            this.groupBox1.Controls.Add(this.brn);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.ENDATE);
            this.groupBox1.Controls.Add(this.STDATE);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDesType);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 290);
            this.groupBox1.TabIndex = 77;
            this.groupBox1.TabStop = false;
            // 
            // Btn_Close
            // 
            this.Btn_Close.ActionType = "";
            this.Btn_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Close.Location = new System.Drawing.Point(266, 254);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(75, 23);
            this.Btn_Close.TabIndex = 7;
            this.Btn_Close.Text = "Close";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // Btn_Run
            // 
            this.Btn_Run.ActionType = "";
            this.Btn_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Run.Location = new System.Drawing.Point(180, 254);
            this.Btn_Run.Name = "Btn_Run";
            this.Btn_Run.Size = new System.Drawing.Size(80, 23);
            this.Btn_Run.TabIndex = 6;
            this.Btn_Run.Text = "Run";
            this.Btn_Run.UseVisualStyleBackColor = true;
            this.Btn_Run.Click += new System.EventHandler(this.Btn_Run_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 109;
            this.label5.Text = "Destination Name";
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(180, 119);
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(159, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 1;
            this.Dest_Name.Text = "1";
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            this.Dest_Name.TextChanged += new System.EventHandler(this.Dest_Name_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(122, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 16);
            this.label6.TabIndex = 107;
            this.label6.Text = "Report Parameters";
            // 
            // sg
            // 
            this.sg.BusinessEntity = "";
            this.sg.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.sg.CustomEnabled = true;
            this.sg.DataFieldMapping = "";
            this.sg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sg.FormattingEnabled = true;
            this.sg.Location = new System.Drawing.Point(180, 227);
            this.sg.LOVType = "";
            this.sg.Name = "sg";
            this.sg.Size = new System.Drawing.Size(159, 21);
            this.sg.SPName = "";
            this.sg.TabIndex = 5;
            this.sg.SelectedIndexChanged += new System.EventHandler(this.sg_SelectedIndexChanged);
            // 
            // brn
            // 
            this.brn.BusinessEntity = "";
            this.brn.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.brn.CustomEnabled = true;
            this.brn.DataFieldMapping = "";
            this.brn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.brn.FormattingEnabled = true;
            this.brn.Location = new System.Drawing.Point(180, 200);
            this.brn.LOVType = "";
            this.brn.Name = "brn";
            this.brn.Size = new System.Drawing.Size(159, 21);
            this.brn.SPName = "";
            this.brn.TabIndex = 4;
            this.brn.SelectedIndexChanged += new System.EventHandler(this.brn_SelectedIndexChanged);
            // 
            // ENDATE
            // 
            this.ENDATE.CustomEnabled = true;
            this.ENDATE.CustomFormat = "dd/MM/yyyy";
            this.ENDATE.DataFieldMapping = "";
            this.ENDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ENDATE.HasChanges = true;
            this.ENDATE.Location = new System.Drawing.Point(180, 174);
            this.ENDATE.Name = "ENDATE";
            this.ENDATE.NullValue = " ";
            this.ENDATE.Size = new System.Drawing.Size(159, 20);
            this.ENDATE.TabIndex = 3;
            this.ENDATE.Value = new System.DateTime(2010, 12, 27, 0, 0, 0, 0);
            this.ENDATE.ValueChanged += new System.EventHandler(this.ENDATE_ValueChanged);
            // 
            // STDATE
            // 
            this.STDATE.CustomEnabled = true;
            this.STDATE.CustomFormat = "dd/MM/yyyy";
            this.STDATE.DataFieldMapping = "";
            this.STDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.STDATE.HasChanges = true;
            this.STDATE.Location = new System.Drawing.Point(180, 148);
            this.STDATE.Name = "STDATE";
            this.STDATE.NullValue = " ";
            this.STDATE.Size = new System.Drawing.Size(159, 20);
            this.STDATE.TabIndex = 2;
            this.STDATE.Value = new System.DateTime(2010, 12, 27, 0, 0, 0, 0);
            this.STDATE.ValueChanged += new System.EventHandler(this.STDATE_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 13);
            this.label4.TabIndex = 100;
            this.label4.Text = "End Date (DD/MM/YYYY)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 13);
            this.label3.TabIndex = 99;
            this.label3.Text = "Start Date (DD/MM/YYYY)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 95;
            this.label2.Text = "Enter Segment or Null";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 93;
            this.label1.Text = "Enter Branch or Null";
            // 
            // cmbDesType
            // 
            this.cmbDesType.BusinessEntity = "";
            this.cmbDesType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDesType.CustomEnabled = true;
            this.cmbDesType.DataFieldMapping = "";
            this.cmbDesType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDesType.FormattingEnabled = true;
            this.cmbDesType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDesType.Location = new System.Drawing.Point(180, 92);
            this.cmbDesType.LOVType = "";
            this.cmbDesType.Name = "cmbDesType";
            this.cmbDesType.Size = new System.Drawing.Size(159, 21);
            this.cmbDesType.SPName = "";
            this.cmbDesType.TabIndex = 0;
            this.cmbDesType.SelectedIndexChanged += new System.EventHandler(this.cmbDesType_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Destination Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(82, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(224, 16);
            this.label7.TabIndex = 29;
            this.label7.Text = "Enter values for the parameters";
            // 
            // CHRIS_PersonnelReport_AttritionSummaryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 338);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_AttritionSummaryReport";
            this.Text = "iCORE CHRIS - Attrition Summary Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLComboBox cmbDesType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLDatePicker ENDATE;
        private CrplControlLibrary.SLDatePicker STDATE;
        private CrplControlLibrary.SLComboBox brn;
        private CrplControlLibrary.SLComboBox sg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLButton Btn_Run;
        private CrplControlLibrary.SLButton Btn_Close;
    }
}