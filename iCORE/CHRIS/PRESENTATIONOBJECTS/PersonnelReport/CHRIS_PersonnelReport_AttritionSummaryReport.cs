using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_AttritionSummaryReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_AttritionSummaryReport()
        {
            InitializeComponent();
        }


        public CHRIS_PersonnelReport_AttritionSummaryReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cmbDesType.Items.RemoveAt(5);
            this.fillBranchCombo();
            this.fillsegCombo();
            this.STDATE.Value = new DateTime(2001,01,01);
            this.ENDATE.Value = new DateTime(2001, 12, 31);
            this.brn.SelectedIndex = (this.brn.Items.Count > 1 ? 1 : 0);
            this.sg.SelectedIndex = (this.sg.Items.Count > 1 ? 1 : 0);
            //this.sg.Text = "GF";
            //Dest_Format.Text = "PDF";
            //this.PrintNoofCopiesReport(1);

        }


        private void RUN_Click(object sender, EventArgs e) //ATTR_REP_SUMM
        {
            //base.RptFileName = "ATTR_REP_SUMM";
            //if (cmbDesType.Text == "Screen")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //if (cmbDesType.Text == "Mail")
            //{
            //    EmailToReport("C:\\ATTR_REP_SUMM", "pdf");
            //}
            //if (cmbDesType.Text == "Printer")
            //{
            //    PrintCustomReport();
            //}
        }

        private void CLOSE_Click(object sender, EventArgs e)
        {
            //base.btnCloseReport_Click(sender, e);
        }
        private void fillBranchCombo()
        {


            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BASE_LOV_ACTION", "BRANCHCODENAMENULL");


            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.brn.DisplayMember = "BRN_NAME";
                this.brn.ValueMember = "BRN_CODE";
                this.brn.DataSource = rsltCode.dstResult.Tables[0];
            }



        }
        private void fillsegCombo()
        {


            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();


            rsltCode = cmnDM.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEWNULL");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {


                    this.sg.DisplayMember = "pr_segment";
                    this.sg.ValueMember = "pr_segment";
                    this.sg.DataSource = rsltCode.dstResult.Tables[0];

                }

            }

        }

        private void Btn_Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "ATTR_REP_SUMM";
            if (cmbDesType.Text == "Screen" || cmbDesType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            if (cmbDesType.Text == "Mail")
            {
                String cc = "";
                if (Dest_Name.Text != String.Empty)
                    cc = Dest_Name.Text;
                EmailToReport("C:\\iCORE-Spool\\ATTR_REP_SUMM", "pdf", cc);
            }
            if (cmbDesType.Text == "Printer")
            {
                PrintCustomReport();
            }
            if (cmbDesType.Text == "File")
            {
                String cc = "C:\\iCORE-Spool\\ATTR_REP_SUMM";
                if (Dest_Name.Text != String.Empty)
                    cc = Dest_Name.Text;
                base.ExportCustomReport(cc, "pdf");
            }


        }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void sg_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void brn_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ENDATE_ValueChanged(object sender, EventArgs e)
        {

        }

        private void STDATE_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cmbDesType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Dest_Name_TextChanged(object sender, EventArgs e)
        {

        }

    }
}