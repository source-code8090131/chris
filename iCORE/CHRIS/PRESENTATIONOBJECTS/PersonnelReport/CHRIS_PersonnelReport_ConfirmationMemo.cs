using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ConfirmationMemo :iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_ConfirmationMemo()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        public CHRIS_PersonnelReport_ConfirmationMemo(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        private void slTextBox2_TextChanged(object sender, EventArgs e)
        {

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            
            nocopies.Text = "1";
            Dest_Format.Text = "dflt";
            w_name.Text = "RUKHSANA ASGHAR";

            //this.PrintNoofCopiesReport(1);

        }

        //private void slButton1_Click(object sender, EventArgs e)
        //{
        //    if (cmbDescType.Text == "Screen")
        //    {
        //        base.btnCallReport_Click(sender, e);
        //    }
        //    else if (cmbDescType.Text == "Mail")
        //    {
        //        base.EmailToReport("c:\\PRREP15", "PDF");
        //    }

        //}

        //private void slButton2_Click(object sender, EventArgs e)
        //{
        //    this.Close();
        //}

        private void slButton3_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "PRREP15";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }


                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                        base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }


            }


            //base.RptFileName = "PRREP15";
            //if (cmbDescType.Text == "Screen")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //else if (cmbDescType.Text == "Mail")
            //{
            //    base.EmailToReport("c:\\PRREP15", "PDF");
            //}
        }

        private void slButton4_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void Dest_name_TextChanged(object sender, EventArgs e)
        {

        }

        private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void DT_ValueChanged(object sender, EventArgs e)
        {

        }

        private void w_year_TextChanged(object sender, EventArgs e)
        {

        }

        private void w_mon_TextChanged(object sender, EventArgs e)
        {

        }

        private void w_name_TextChanged(object sender, EventArgs e)
        {

        }

        private void nocopies_TextChanged(object sender, EventArgs e)
        {

        }

        private void Dest_Format_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}