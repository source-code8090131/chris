using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_EmployeesCurrent : BaseRptForm
    {
        public CHRIS_PersonnelReport_EmployeesCurrent()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        public CHRIS_PersonnelReport_EmployeesCurrent(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
           base.OnLoad(e);
           Dest_Type.Items.RemoveAt(5);
           MODE.Items.RemoveAt(3);
           this.Dest_Format.Text = "wide";
           this.nocopies.Text = "1";
           this.Dest_name.Text = "C:\\iCORE-Spool\\PRREP01.LIS";
           w_brn.Text = "ALL";
           w_dept.Text = "ALL";
           w_desig.Text = "ALL";
           w_Group.Text = "ALL";
           w_level.Text = "ALL";
           w_seg.Text = "GCB";


        }


        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
             {
                base.RptFileName = "PRREP01";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                 
                }
                if (Dest_Type.Text == "Printer")
                {
                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                       
                        base.PrintNoofCopiesReport(no_of_copies);
                    }

                }

               
                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (this.Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                        base.ExportCustomReport(d, "pdf");


                }

                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.EmailToReport("C:\\iCORE-Spool\\Report", "PDF",d);
                }
              
            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

        private void w_level_TextChanged(object sender, EventArgs e)
        {

        }

    }
}