using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ReportForDepartment : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_ReportForDepartment()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_ReportForDepartment(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            
            nocopies.Text = "1";
            Dest_Format.Text = "WIDE";
            W_SEG.Text = "GF";
            W_BRN.Text = "ALL";
            W_GRP.Text = "ALL";
            DEPT1.Text = "ALL";
            Dest_name.Text = "C:\\iCORE-Spool\\";

           }

       

        private void btnRun_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "PRREP02";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }


                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }
                

            }

         
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

       
    }
}