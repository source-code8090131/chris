using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_PersonalOnePagerReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_PersonalOnePagerReport()
        {
            InitializeComponent();
        }
        String DestFormat; 
        public CHRIS_PersonnelReport_PersonalOnePagerReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            MODE.Items.RemoveAt(3);
            nocopies.Text = "1";
            Dest_Format.Text = "no2_ff";
            this.SPNO.Text = "1";
            this.TPNO.Text = "999999";
            
            DataTable dt = new DataTable();
            dt.Columns.Add("Segment");
            dt.Rows.Add(new object[] { "ALL" });
            dt.Rows.Add(new object[] { "GF" });
            dt.Rows.Add(new object[] { "GCB" });

            this.SEG.ValueMember = "Segment";
            this.SEG.DisplayMember = "Segment";
            this.SEG.DataSource = dt;
            fillbrnCombo();
            filllevelCombo();
            filldesigCombo();
            fillgrpCombo();
            filldptCombo(); 

        }
        private void fillbrnCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_BRANCH");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.BRN.DisplayMember = "pr_new_branch";
                    this.BRN.ValueMember = "pr_new_branch";
                    this.BRN.DataSource = rsltCode.dstResult.Tables[0];
                    this.BRN.Text = "ALL";
                }

            }

        }

        private void filldptCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_DEPT");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.dpt.DisplayMember = "pr_dept";
                    this.dpt.ValueMember = "pr_dept";
                    this.dpt.DataSource = rsltCode.dstResult.Tables[0];
                    this.dpt.Text = "ALL";
                }

            }

        }

        private void filllevelCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_LEVEL");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.LEV.DisplayMember = "lvl";
                    this.LEV.ValueMember = "lvl";
                    this.LEV.DataSource = rsltCode.dstResult.Tables[0];
                    this.LEV.Text = "ALL";
                }

            }



        }



        private void filldesigCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_DESIG");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.P_DESIG.DisplayMember = "Pr_Desig";
                    this.P_DESIG.ValueMember = "Pr_Desig";
                    this.P_DESIG.DataSource = rsltCode.dstResult.Tables[0];
                    this.P_DESIG.Text = "ALL";
                    
                }

            }



        }


        private void fillgrpCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_GROUP");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    this.P_GRP.DisplayMember = "GRP";
                    this.P_GRP.ValueMember = "GRP";
                    this.P_GRP.DataSource = rsltCode.dstResult.Tables[0];
                    this.P_GRP.Text = "ALL";
                }

            }



        }

        private void Run_Click(object sender, EventArgs e)
        {
            {

                base.RptFileName = "citi_emp2";
                //base.btnCallReport_Click(sender, e);
                if (this.Dest_Format.Text == String.Empty)
                    DestFormat = "PDF";
                else
                    DestFormat = this.Dest_Format.Text;

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.nocopies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;

                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";

                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }



                    base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }

        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

    }
}