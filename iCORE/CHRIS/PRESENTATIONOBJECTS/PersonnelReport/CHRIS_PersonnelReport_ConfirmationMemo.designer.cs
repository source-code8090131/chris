namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_ConfirmationMemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_ConfirmationMemo));
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox4 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox5 = new CrplControlLibrary.SLTextBox(this.components);
            this.slDatePicker1 = new CrplControlLibrary.SLDatePicker(this.components);
            this.slComboBox1 = new CrplControlLibrary.SLComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.slTextBox6 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.slButton2 = new CrplControlLibrary.SLButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.slButton4 = new CrplControlLibrary.SLButton();
            this.label19 = new System.Windows.Forms.Label();
            this.slButton3 = new CrplControlLibrary.SLButton();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.DT = new CrplControlLibrary.SLDatePicker(this.components);
            this.w_year = new CrplControlLibrary.SLTextBox(this.components);
            this.w_mon = new CrplControlLibrary.SLTextBox(this.components);
            this.w_name = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(249, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(151, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "Report Parametres";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(229, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(194, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Enter Values For The Parametres";
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(211, 142);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(200, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 0;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(212, 168);
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(200, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 1;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "";
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.Location = new System.Drawing.Point(212, 195);
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.Size = new System.Drawing.Size(200, 20);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 2;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox4
            // 
            this.slTextBox4.AllowSpace = true;
            this.slTextBox4.AssociatedLookUpName = "";
            this.slTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox4.ContinuationTextBox = null;
            this.slTextBox4.CustomEnabled = true;
            this.slTextBox4.DataFieldMapping = "";
            this.slTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox4.GetRecordsOnUpDownKeys = false;
            this.slTextBox4.IsDate = false;
            this.slTextBox4.Location = new System.Drawing.Point(211, 222);
            this.slTextBox4.Name = "slTextBox4";
            this.slTextBox4.NumberFormat = "###,###,##0.00";
            this.slTextBox4.Postfix = "";
            this.slTextBox4.Prefix = "";
            this.slTextBox4.Size = new System.Drawing.Size(200, 20);
            this.slTextBox4.SkipValidation = false;
            this.slTextBox4.TabIndex = 3;
            this.slTextBox4.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox5
            // 
            this.slTextBox5.AllowSpace = true;
            this.slTextBox5.AssociatedLookUpName = "";
            this.slTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox5.ContinuationTextBox = null;
            this.slTextBox5.CustomEnabled = true;
            this.slTextBox5.DataFieldMapping = "";
            this.slTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox5.GetRecordsOnUpDownKeys = false;
            this.slTextBox5.IsDate = false;
            this.slTextBox5.Location = new System.Drawing.Point(211, 249);
            this.slTextBox5.Name = "slTextBox5";
            this.slTextBox5.NumberFormat = "###,###,##0.00";
            this.slTextBox5.Postfix = "";
            this.slTextBox5.Prefix = "";
            this.slTextBox5.Size = new System.Drawing.Size(200, 20);
            this.slTextBox5.SkipValidation = false;
            this.slTextBox5.TabIndex = 4;
            this.slTextBox5.TextType = CrplControlLibrary.TextType.String;
            // 
            // slDatePicker1
            // 
            this.slDatePicker1.CustomEnabled = true;
            this.slDatePicker1.DataFieldMapping = "";
            this.slDatePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.slDatePicker1.HasChanges = true;
            this.slDatePicker1.Location = new System.Drawing.Point(212, 89);
            this.slDatePicker1.Name = "slDatePicker1";
            this.slDatePicker1.NullValue = " ";
            this.slDatePicker1.Size = new System.Drawing.Size(200, 21);
            this.slDatePicker1.TabIndex = 6;
            this.slDatePicker1.Value = new System.DateTime(2010, 12, 14, 0, 0, 0, 0);
            // 
            // slComboBox1
            // 
            this.slComboBox1.BusinessEntity = "";
            this.slComboBox1.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.slComboBox1.CustomEnabled = true;
            this.slComboBox1.DataFieldMapping = "";
            this.slComboBox1.FormattingEnabled = true;
            this.slComboBox1.Location = new System.Drawing.Point(212, 62);
            this.slComboBox1.LOVType = "";
            this.slComboBox1.Name = "slComboBox1";
            this.slComboBox1.Size = new System.Drawing.Size(200, 23);
            this.slComboBox1.SPName = "";
            this.slComboBox1.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Destination Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Date ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(49, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Destination Format";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "Number of Copies";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(50, 195);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "From :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(49, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "Enter Month";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(49, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Enter Year";
            // 
            // slTextBox6
            // 
            this.slTextBox6.AllowSpace = true;
            this.slTextBox6.AssociatedLookUpName = "";
            this.slTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox6.ContinuationTextBox = null;
            this.slTextBox6.CustomEnabled = true;
            this.slTextBox6.DataFieldMapping = "";
            this.slTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox6.GetRecordsOnUpDownKeys = false;
            this.slTextBox6.IsDate = false;
            this.slTextBox6.Location = new System.Drawing.Point(211, 116);
            this.slTextBox6.Name = "slTextBox6";
            this.slTextBox6.NumberFormat = "###,###,##0.00";
            this.slTextBox6.Postfix = "";
            this.slTextBox6.Prefix = "";
            this.slTextBox6.Size = new System.Drawing.Size(200, 20);
            this.slTextBox6.SkipValidation = false;
            this.slTextBox6.TabIndex = 16;
            this.slTextBox6.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(49, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Destination Name ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.slTextBox6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.slComboBox1);
            this.groupBox1.Controls.Add(this.slDatePicker1);
            this.groupBox1.Controls.Add(this.slTextBox5);
            this.groupBox1.Controls.Add(this.slTextBox4);
            this.groupBox1.Controls.Add(this.slTextBox3);
            this.groupBox1.Controls.Add(this.slTextBox2);
            this.groupBox1.Controls.Add(this.slTextBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(98, 223);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(474, 311);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Location = new System.Drawing.Point(220, 558);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 6;
            this.slButton1.Text = "Run";
            this.slButton1.UseVisualStyleBackColor = true;
            // 
            // slButton2
            // 
            this.slButton2.ActionType = "";
            this.slButton2.Location = new System.Drawing.Point(348, 558);
            this.slButton2.Name = "slButton2";
            this.slButton2.Size = new System.Drawing.Size(75, 23);
            this.slButton2.TabIndex = 7;
            this.slButton2.Text = "Close";
            this.slButton2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nocopies);
            this.groupBox2.Controls.Add(this.Dest_Format);
            this.groupBox2.Controls.Add(this.Dest_name);
            this.groupBox2.Controls.Add(this.Dest_Type);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.slButton4);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.slButton3);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.DT);
            this.groupBox2.Controls.Add(this.w_year);
            this.groupBox2.Controls.Add(this.w_mon);
            this.groupBox2.Controls.Add(this.w_name);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 39);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(475, 327);
            this.groupBox2.TabIndex = 75;
            this.groupBox2.TabStop = false;
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(200, 191);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(155, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 5;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            this.nocopies.TextChanged += new System.EventHandler(this.nocopies_TextChanged);
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(200, 165);
            this.Dest_Format.MaxLength = 50;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(155, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 4;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            this.Dest_Format.TextChanged += new System.EventHandler(this.Dest_Format_TextChanged);
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(200, 139);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(155, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 3;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            this.Dest_name.TextChanged += new System.EventHandler(this.Dest_name_TextChanged);
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(200, 83);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(155, 23);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            this.Dest_Type.SelectedIndexChanged += new System.EventHandler(this.Dest_Type_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(408, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 80;
            this.pictureBox2.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 15);
            this.label11.TabIndex = 17;
            this.label11.Text = "Destination Name ";
            // 
            // slButton4
            // 
            this.slButton4.ActionType = "";
            this.slButton4.Location = new System.Drawing.Point(270, 294);
            this.slButton4.Name = "slButton4";
            this.slButton4.Size = new System.Drawing.Size(61, 23);
            this.slButton4.TabIndex = 10;
            this.slButton4.Text = "Close";
            this.slButton4.UseVisualStyleBackColor = true;
            this.slButton4.Click += new System.EventHandler(this.slButton4_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(158, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(139, 16);
            this.label19.TabIndex = 78;
            this.label19.Text = "Report Parametres";
            // 
            // slButton3
            // 
            this.slButton3.ActionType = "";
            this.slButton3.Location = new System.Drawing.Point(200, 294);
            this.slButton3.Name = "slButton3";
            this.slButton3.Size = new System.Drawing.Size(61, 23);
            this.slButton3.TabIndex = 9;
            this.slButton3.Text = "Run";
            this.slButton3.UseVisualStyleBackColor = true;
            this.slButton3.Click += new System.EventHandler(this.slButton3_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(117, 35);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(224, 16);
            this.label20.TabIndex = 79;
            this.label20.Text = "Enter values for the parametres";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 274);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 15);
            this.label12.TabIndex = 14;
            this.label12.Text = "Enter Year :";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 248);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 15);
            this.label13.TabIndex = 13;
            this.label13.Text = "Enter Month :";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(20, 218);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(48, 15);
            this.label14.TabIndex = 12;
            this.label14.Text = "From :";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 191);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 15);
            this.label15.TabIndex = 11;
            this.label15.Text = "Number of Copies";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(20, 165);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 15);
            this.label16.TabIndex = 10;
            this.label16.Text = "Destination Format";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(20, 111);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 15);
            this.label17.TabIndex = 9;
            this.label17.Text = "Dt";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 15);
            this.label18.TabIndex = 8;
            this.label18.Text = "Destination Type";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // DT
            // 
            this.DT.CustomEnabled = true;
            this.DT.CustomFormat = "dd/MM/yyyy";
            this.DT.DataFieldMapping = "";
            this.DT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DT.HasChanges = true;
            this.DT.Location = new System.Drawing.Point(200, 111);
            this.DT.Name = "DT";
            this.DT.NullValue = " ";
            this.DT.Size = new System.Drawing.Size(155, 21);
            this.DT.TabIndex = 2;
            this.DT.Value = new System.DateTime(2010, 12, 14, 0, 0, 0, 0);
            this.DT.ValueChanged += new System.EventHandler(this.DT_ValueChanged);
            // 
            // w_year
            // 
            this.w_year.AllowSpace = true;
            this.w_year.AssociatedLookUpName = "";
            this.w_year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_year.ContinuationTextBox = null;
            this.w_year.CustomEnabled = true;
            this.w_year.DataFieldMapping = "";
            this.w_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_year.GetRecordsOnUpDownKeys = false;
            this.w_year.IsDate = false;
            this.w_year.Location = new System.Drawing.Point(200, 269);
            this.w_year.MaxLength = 4;
            this.w_year.Name = "w_year";
            this.w_year.NumberFormat = "###,###,##0.00";
            this.w_year.Postfix = "";
            this.w_year.Prefix = "";
            this.w_year.Size = new System.Drawing.Size(155, 20);
            this.w_year.SkipValidation = false;
            this.w_year.TabIndex = 8;
            this.w_year.TextType = CrplControlLibrary.TextType.Integer;
            this.w_year.TextChanged += new System.EventHandler(this.w_year_TextChanged);
            // 
            // w_mon
            // 
            this.w_mon.AllowSpace = true;
            this.w_mon.AssociatedLookUpName = "";
            this.w_mon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_mon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_mon.ContinuationTextBox = null;
            this.w_mon.CustomEnabled = true;
            this.w_mon.DataFieldMapping = "";
            this.w_mon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_mon.GetRecordsOnUpDownKeys = false;
            this.w_mon.IsDate = false;
            this.w_mon.Location = new System.Drawing.Point(200, 245);
            this.w_mon.MaxLength = 2;
            this.w_mon.Name = "w_mon";
            this.w_mon.NumberFormat = "###,###,##0.00";
            this.w_mon.Postfix = "";
            this.w_mon.Prefix = "";
            this.w_mon.Size = new System.Drawing.Size(155, 20);
            this.w_mon.SkipValidation = false;
            this.w_mon.TabIndex = 7;
            this.w_mon.TextType = CrplControlLibrary.TextType.Integer;
            this.w_mon.TextChanged += new System.EventHandler(this.w_mon_TextChanged);
            // 
            // w_name
            // 
            this.w_name.AllowSpace = true;
            this.w_name.AssociatedLookUpName = "";
            this.w_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_name.ContinuationTextBox = null;
            this.w_name.CustomEnabled = true;
            this.w_name.DataFieldMapping = "";
            this.w_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_name.GetRecordsOnUpDownKeys = false;
            this.w_name.IsDate = false;
            this.w_name.Location = new System.Drawing.Point(200, 218);
            this.w_name.Name = "w_name";
            this.w_name.NumberFormat = "###,###,##0.00";
            this.w_name.Postfix = "";
            this.w_name.Prefix = "";
            this.w_name.Size = new System.Drawing.Size(155, 20);
            this.w_name.SkipValidation = false;
            this.w_name.TabIndex = 6;
            this.w_name.TextType = CrplControlLibrary.TextType.String;
            this.w_name.TextChanged += new System.EventHandler(this.w_name_TextChanged);
            // 
            // CHRIS_PersonnelReport_ConfirmationMemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(500, 376);
            this.Controls.Add(this.groupBox2);
            this.Name = "CHRIS_PersonnelReport_ConfirmationMemo";
            this.Text = "iCORE CHRIS - Confirmation Memo";
            this.Controls.SetChildIndex(this.groupBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLTextBox slTextBox4;
        private CrplControlLibrary.SLTextBox slTextBox5;
        private CrplControlLibrary.SLDatePicker slDatePicker1;
        private CrplControlLibrary.SLComboBox slComboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox slTextBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLButton slButton1;
        private CrplControlLibrary.SLButton slButton2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLDatePicker DT;
        private CrplControlLibrary.SLTextBox w_year;
        private CrplControlLibrary.SLTextBox w_mon;
        private CrplControlLibrary.SLTextBox w_name;
        private CrplControlLibrary.SLButton slButton3;
        private CrplControlLibrary.SLButton slButton4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox nocopies;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private CrplControlLibrary.SLTextBox Dest_name;
    }
}