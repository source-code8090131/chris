using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

/*data is not entered and data is slow*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_FinanceBalance :  iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_FinanceBalance()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
     
        public CHRIS_FinanceReports_FinanceBalance(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
         
        }
       

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            
           // this.fillBranchCombo();
           // this.fillsegCombo();
            nocopies.Text = "1";
            Dest_Format.Text = "wide";
            w_branch.Text = "LHR";
            seg.Text = "GF";
            
            
            //this.PrintNoofCopiesReport(1);

        }
            
        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);
           //int no_of_copies;
           
            {
                base.RptFileName = "FNREP01";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                   
                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintNoofCopiesReport(no_of_copies);


                }
               
                if (Dest_Type.Text == "File")
                {
                    string desname = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;
                        base.ExportCustomReport(desname, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {

                    string desname = "";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;

                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", desname);

                }
            

            }



        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

      

        #region
        //private void fillBranchCombo()
        //{


        //    Result rsltCode;
        //    CmnDataManager cmnDM = new CmnDataManager();
        //    rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");


        //    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
        //    {
        //        this.w_branch.DisplayMember = "BRN_CODE";
        //        this.w_branch.ValueMember = "BRN_CODE";
        //        this.w_branch.DataSource = rsltCode.dstResult.Tables[0];
        //    }
            
          

        //}

        //private void fillsegCombo()
        //{


        //    Result rsltCode;
        //    CmnDataManager cmnDM = new CmnDataManager();

        
        //    rsltCode = cmnDM.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

        //    if (rsltCode.isSuccessful)
        //    {
        //        if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
        //        {


        //            this.seg.DisplayMember = "pr_segment";
        //            this.seg.ValueMember = "pr_segment";
        //            this.seg.DataSource = rsltCode.dstResult.Tables[0];
                    
        //        }

        //    }

        //}






        //private void CHRIS_FinanceReports_FinanceBalance_Load(object sender, EventArgs e)
        //{
        //    fillBranchCombo();
        //    fillsegCombo();

        //}


        #endregion


    }
}



