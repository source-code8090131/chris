using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_NewlyFinanceBooking : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_NewlyFinanceBooking()
        {
            InitializeComponent();
        }
        public CHRIS_FinanceReports_NewlyFinanceBooking(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count-1);
            this.txtNumCopies.Text = "1";
            this.brn.Text = "ALL";
            this.seg.Text = "ALL";
            this.stdate.Value = null;
            this.todate.Value = null;
            this.txtDest_Format.Text = "cond";

        }


        private void btnRun_Click(object sender, EventArgs e)
        {
       
            base.RptFileName = "FNREP22";
            if (this.txtDest_Format.Text == String.Empty)
            {
                this.txtDest_Format.Text = "PDF";
            }
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer" )
            {
                base.PrintCustomReport(this.txtNumCopies.Text );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                if (this.txtDestName.Text == string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";
                }

                else
                {
                    DestName = this.txtDestName.Text;
                }

                    base.ExportCustomReport(DestName,"pdf");

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                if (this.txtDestName.Text == string.Empty)
                {
                    RecipentName = "";
                }

                else
                {
                    RecipentName = this.txtDestName.Text;
                }

                    base.EmailToReport(DestName, "pdf", RecipentName);

            }


        }

       

    }
}