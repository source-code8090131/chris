using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_CategoryWiseOutstandingBalanceOut : BaseRptForm
    {
        private string DestFormat = "PDF";
        public CHRIS_FinanceReports_CategoryWiseOutstandingBalanceOut()
        {
            InitializeComponent();
        }


        public CHRIS_FinanceReports_CategoryWiseOutstandingBalanceOut(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.FillBranch();
            this.fillsegCombo();
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.Copies.Text = "1";
            this.txtDest_Format.Text  = "wide";
            
        }

        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCountAll");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.w_branch.DisplayMember = "BRN_CODE";
                this.w_branch.ValueMember = "BRN_CODE";
                this.w_branch.DataSource = rsltCode.dstResult.Tables[0];
            }

        }
        
        private void fillsegCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.W_SEG.DisplayMember = "pr_segment";
                    this.W_SEG.ValueMember = "pr_segment";
                    this.W_SEG.DataSource = rsltCode.dstResult.Tables[0];
                }

            }

        }
        
        private void CHRIS_FinanceReports_CategoryWiseOutstandingBalanceOut_Load(object sender, EventArgs e)
        {

        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            {
                base.RptFileName = "FNREP04_17";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport(this.Copies.Text);
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    String DesName;
                    if (Dest_name.Text == string.Empty)
                        DesName = "c:\\iCORE-Spool\\report";
                    else
                        DesName = Dest_name.Text;
                    base.ExportCustomReport(DesName, "pdf");




                    //base.ExportCustomReport();



                }

            }

            if (Dest_Type.Text == "Mail")
            {
                String DesName;
                String Res; 
                if (Dest_name.Text == string.Empty)
                    Res = "";
                else
                    Res = Dest_name.Text;

                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Res);

            }



        }

        private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }

        
    }
}


