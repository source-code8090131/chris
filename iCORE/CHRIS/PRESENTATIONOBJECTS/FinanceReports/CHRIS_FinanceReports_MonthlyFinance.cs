using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
   
{
    public partial class CHRIS_FinanceReports_MonthlyFinance : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_MonthlyFinance()
        {
            InitializeComponent();
        }
        public CHRIS_FinanceReports_MonthlyFinance(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }


        string DestFormat = "PDF";

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            //Dest_Format.Text = "wide";
        
         }
         

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }
            no_of_copies = Convert.ToInt16(nocopies.Text);
            {
                base.RptFileName = "FNREP02";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    
                }
            }
            if (Dest_Type.Text == "Printer")
            {
                
                    base.PrintNoofCopiesReport(no_of_copies);
            }

     
             if (Dest_Type.Text == "File")
                {
                    string desname = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != String.Empty)
                        desname = Dest_name.Text;
                    
                        base.ExportCustomReport(desname, "pdf");


                }
            if (Dest_Type.Text == "Mail")
            {
                string desname = "";
                if (Dest_name.Text != String.Empty)
                    desname = Dest_name.Text;
                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", desname);



            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);


        }
    }
}