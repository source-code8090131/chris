using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*Data is not entered*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_FinanceLedger : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        #region Constructors
        public CHRIS_FinanceReports_FinanceLedger()
        {
            InitializeComponent();
        }
        
        string DestFormat = "PDF";
        public CHRIS_FinanceReports_FinanceLedger(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #endregion

        #region Events

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            Dest_name.Text = "C:\\iCORE-Spool\\";
            //Dest_Format.Text = "PDF";
        }
      
        private void Run_Click(object sender, EventArgs e)
        {

            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            {
                base.RptFileName = "FNREP03";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                   
                }
                if (Dest_Type.Text == "Printer")
                {
                   
                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                       
                        base.PrintNoofCopiesReport(no_of_copies);
                    }

                }
                if (Dest_Type.Text == "File")
                {
                    String destName = "c:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != string.Empty)
                        destName = Dest_name.Text;

                    base.ExportCustomReport(destName, "pdf");



                }


                if (Dest_Type.Text == "Mail")
                {
                    String destName = "";
                    if (Dest_name.Text != string.Empty)
                        destName = Dest_name.Text;


                    base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", destName);



                    
                }
            

            }
        }


        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
        #endregion

        private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }


       
}

