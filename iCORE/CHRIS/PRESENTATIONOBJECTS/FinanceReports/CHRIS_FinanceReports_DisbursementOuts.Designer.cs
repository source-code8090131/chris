namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_DisbursementOuts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_DisbursementOuts));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.to_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.w_level = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.w_dept = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.w_desig = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.w_type = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.today = new CrplControlLibrary.SLDatePicker(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.fr_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtDest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNumCopies = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDestName = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.to_date);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.w_level);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.w_dept);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.w_desig);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.w_type);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.w_brn);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.today);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.fr_date);
            this.groupBox1.Controls.Add(this.txtDest_Format);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.w_seg);
            this.groupBox1.Controls.Add(this.txtNumCopies);
            this.groupBox1.Controls.Add(this.txtDestName);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(11, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 481);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // to_date
            // 
            this.to_date.CustomEnabled = true;
            this.to_date.CustomFormat = "dd/MM/yyyy";
            this.to_date.DataFieldMapping = "";
            this.to_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.to_date.HasChanges = true;
            this.to_date.Location = new System.Drawing.Point(222, 401);
            this.to_date.Name = "to_date";
            this.to_date.NullValue = " ";
            this.to_date.Size = new System.Drawing.Size(199, 20);
            this.to_date.TabIndex = 12;
            this.to_date.Value = new System.DateTime(2010, 12, 8, 16, 12, 1, 625);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 408);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(155, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "TO DATE (DD/MM/YYYY)";
            // 
            // w_level
            // 
            this.w_level.AllowSpace = true;
            this.w_level.AssociatedLookUpName = "";
            this.w_level.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_level.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_level.ContinuationTextBox = null;
            this.w_level.CustomEnabled = true;
            this.w_level.DataFieldMapping = "";
            this.w_level.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_level.GetRecordsOnUpDownKeys = false;
            this.w_level.IsDate = false;
            this.w_level.Location = new System.Drawing.Point(222, 323);
            this.w_level.MaxLength = 30;
            this.w_level.Name = "w_level";
            this.w_level.NumberFormat = "###,###,##0.00";
            this.w_level.Postfix = "";
            this.w_level.Prefix = "";
            this.w_level.Size = new System.Drawing.Size(199, 20);
            this.w_level.SkipValidation = false;
            this.w_level.TabIndex = 9;
            this.w_level.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 325);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(177, 13);
            this.label14.TabIndex = 40;
            this.label14.Text = "ENTER LEVEL CODE OR ALL";
            // 
            // w_dept
            // 
            this.w_dept.AllowSpace = true;
            this.w_dept.AssociatedLookUpName = "";
            this.w_dept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_dept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_dept.ContinuationTextBox = null;
            this.w_dept.CustomEnabled = true;
            this.w_dept.DataFieldMapping = "";
            this.w_dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_dept.GetRecordsOnUpDownKeys = false;
            this.w_dept.IsDate = false;
            this.w_dept.Location = new System.Drawing.Point(222, 297);
            this.w_dept.MaxLength = 30;
            this.w_dept.Name = "w_dept";
            this.w_dept.NumberFormat = "###,###,##0.00";
            this.w_dept.Postfix = "";
            this.w_dept.Prefix = "";
            this.w_dept.Size = new System.Drawing.Size(199, 20);
            this.w_dept.SkipValidation = false;
            this.w_dept.TabIndex = 8;
            this.w_dept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(23, 297);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(186, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "ENTER DEPARTMENT OR ALL";
            // 
            // w_desig
            // 
            this.w_desig.AllowSpace = true;
            this.w_desig.AssociatedLookUpName = "";
            this.w_desig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_desig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_desig.ContinuationTextBox = null;
            this.w_desig.CustomEnabled = true;
            this.w_desig.DataFieldMapping = "";
            this.w_desig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_desig.GetRecordsOnUpDownKeys = false;
            this.w_desig.IsDate = false;
            this.w_desig.Location = new System.Drawing.Point(222, 267);
            this.w_desig.MaxLength = 30;
            this.w_desig.Name = "w_desig";
            this.w_desig.NumberFormat = "###,###,##0.00";
            this.w_desig.Postfix = "";
            this.w_desig.Prefix = "";
            this.w_desig.Size = new System.Drawing.Size(199, 20);
            this.w_desig.SkipValidation = false;
            this.w_desig.TabIndex = 7;
            this.w_desig.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(24, 269);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(186, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "ENTER DESIGNATION OR ALL";
            // 
            // w_type
            // 
            this.w_type.AllowSpace = true;
            this.w_type.AssociatedLookUpName = "";
            this.w_type.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_type.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_type.ContinuationTextBox = null;
            this.w_type.CustomEnabled = true;
            this.w_type.DataFieldMapping = "";
            this.w_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_type.GetRecordsOnUpDownKeys = false;
            this.w_type.IsDate = false;
            this.w_type.Location = new System.Drawing.Point(222, 349);
            this.w_type.MaxLength = 1;
            this.w_type.Name = "w_type";
            this.w_type.NumberFormat = "###,###,##0.00";
            this.w_type.Postfix = "";
            this.w_type.Prefix = "";
            this.w_type.Size = new System.Drawing.Size(199, 20);
            this.w_type.SkipValidation = false;
            this.w_type.TabIndex = 10;
            this.w_type.TextType = CrplControlLibrary.TextType.String;
            this.w_type.TextChanged += new System.EventHandler(this.w_type_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 351);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(178, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "ENTER LOAN TYPE OR [A]LL";
            // 
            // w_brn
            // 
            this.w_brn.AllowSpace = true;
            this.w_brn.AssociatedLookUpName = "";
            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_brn.ContinuationTextBox = null;
            this.w_brn.CustomEnabled = true;
            this.w_brn.DataFieldMapping = "";
            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_brn.GetRecordsOnUpDownKeys = false;
            this.w_brn.IsDate = false;
            this.w_brn.Location = new System.Drawing.Point(222, 215);
            this.w_brn.MaxLength = 3;
            this.w_brn.Name = "w_brn";
            this.w_brn.NumberFormat = "###,###,##0.00";
            this.w_brn.Postfix = "";
            this.w_brn.Prefix = "";
            this.w_brn.Size = new System.Drawing.Size(199, 20);
            this.w_brn.SkipValidation = false;
            this.w_brn.TabIndex = 5;
            this.w_brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 217);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(194, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "ENTER BRANCH CODE OR  ALL";
            // 
            // today
            // 
            this.today.CustomEnabled = true;
            this.today.CustomFormat = "dd/MM/yyyy";
            this.today.DataFieldMapping = "";
            this.today.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.today.HasChanges = true;
            this.today.Location = new System.Drawing.Point(222, 111);
            this.today.Name = "today";
            this.today.NullValue = " ";
            this.today.Size = new System.Drawing.Size(199, 20);
            this.today.TabIndex = 1;
            this.today.Value = new System.DateTime(2010, 12, 8, 16, 12, 1, 625);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Today";
            // 
            // fr_date
            // 
            this.fr_date.CustomEnabled = true;
            this.fr_date.CustomFormat = "dd/MM/yyyy";
            this.fr_date.DataFieldMapping = "";
            this.fr_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fr_date.HasChanges = true;
            this.fr_date.Location = new System.Drawing.Point(222, 375);
            this.fr_date.Name = "fr_date";
            this.fr_date.NullValue = " ";
            this.fr_date.Size = new System.Drawing.Size(199, 20);
            this.fr_date.TabIndex = 11;
            this.fr_date.Value = new System.DateTime(2010, 12, 8, 16, 12, 1, 625);
            // 
            // txtDest_Format
            // 
            this.txtDest_Format.AllowSpace = true;
            this.txtDest_Format.AssociatedLookUpName = "";
            this.txtDest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDest_Format.ContinuationTextBox = null;
            this.txtDest_Format.CustomEnabled = true;
            this.txtDest_Format.DataFieldMapping = "";
            this.txtDest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDest_Format.GetRecordsOnUpDownKeys = false;
            this.txtDest_Format.IsDate = false;
            this.txtDest_Format.Location = new System.Drawing.Point(222, 163);
            this.txtDest_Format.MaxLength = 40;
            this.txtDest_Format.Name = "txtDest_Format";
            this.txtDest_Format.NumberFormat = "###,###,##0.00";
            this.txtDest_Format.Postfix = "";
            this.txtDest_Format.Prefix = "";
            this.txtDest_Format.Size = new System.Drawing.Size(199, 20);
            this.txtDest_Format.SkipValidation = false;
            this.txtDest_Format.TabIndex = 3;
            this.txtDest_Format.Text = "PDF";
            this.txtDest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Destination Format";
            // 
            // w_seg
            // 
            this.w_seg.AllowSpace = true;
            this.w_seg.AssociatedLookUpName = "";
            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_seg.ContinuationTextBox = null;
            this.w_seg.CustomEnabled = true;
            this.w_seg.DataFieldMapping = "";
            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_seg.GetRecordsOnUpDownKeys = false;
            this.w_seg.IsDate = false;
            this.w_seg.Location = new System.Drawing.Point(222, 241);
            this.w_seg.MaxLength = 3;
            this.w_seg.Name = "w_seg";
            this.w_seg.NumberFormat = "###,###,##0.00";
            this.w_seg.Postfix = "";
            this.w_seg.Prefix = "";
            this.w_seg.Size = new System.Drawing.Size(199, 20);
            this.w_seg.SkipValidation = false;
            this.w_seg.TabIndex = 6;
            this.w_seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNumCopies
            // 
            this.txtNumCopies.AllowSpace = true;
            this.txtNumCopies.AssociatedLookUpName = "";
            this.txtNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumCopies.ContinuationTextBox = null;
            this.txtNumCopies.CustomEnabled = true;
            this.txtNumCopies.DataFieldMapping = "";
            this.txtNumCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCopies.GetRecordsOnUpDownKeys = false;
            this.txtNumCopies.IsDate = false;
            this.txtNumCopies.Location = new System.Drawing.Point(222, 189);
            this.txtNumCopies.MaxLength = 2;
            this.txtNumCopies.Name = "txtNumCopies";
            this.txtNumCopies.NumberFormat = "###,###,##0.00";
            this.txtNumCopies.Postfix = "";
            this.txtNumCopies.Prefix = "";
            this.txtNumCopies.Size = new System.Drawing.Size(199, 20);
            this.txtNumCopies.SkipValidation = false;
            this.txtNumCopies.TabIndex = 4;
            this.txtNumCopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtDestName
            // 
            this.txtDestName.AllowSpace = true;
            this.txtDestName.AssociatedLookUpName = "";
            this.txtDestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDestName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDestName.ContinuationTextBox = null;
            this.txtDestName.CustomEnabled = true;
            this.txtDestName.DataFieldMapping = "";
            this.txtDestName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestName.GetRecordsOnUpDownKeys = false;
            this.txtDestName.IsDate = false;
            this.txtDestName.Location = new System.Drawing.Point(222, 137);
            this.txtDestName.MaxLength = 50;
            this.txtDestName.Name = "txtDestName";
            this.txtDestName.NumberFormat = "###,###,##0.00";
            this.txtDestName.Postfix = "";
            this.txtDestName.Prefix = "";
            this.txtDestName.Size = new System.Drawing.Size(199, 20);
            this.txtDestName.SkipValidation = false;
            this.txtDestName.TabIndex = 2;
            this.txtDestName.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(222, 84);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(199, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(294, 440);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 23);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(225, 440);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 13;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "ENTER SEGMENT CODE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 382);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "FROM DATE (DD/MM/YYYY)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Number of Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Destination Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(190, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Enter values for the parameters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(207, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(474, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(62, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_FinanceReports_DisbursementOuts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 532);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_DisbursementOuts";
            this.Text = "CHRIS_FinanceReports_DisbursementOuts";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private CrplControlLibrary.SLTextBox w_seg;
        private CrplControlLibrary.SLTextBox txtNumCopies;
        private CrplControlLibrary.SLTextBox txtDestName;
        private CrplControlLibrary.SLTextBox txtDest_Format;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLDatePicker fr_date;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLDatePicker today;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox w_brn;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox w_level;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox w_dept;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox w_desig;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox w_type;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLDatePicker to_date;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}