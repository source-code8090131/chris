using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*Data is not entered*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_MCOReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_MCOReport()
        {
            InitializeComponent();
        }
        //string DestName = @"C:\Report";
        string DestFormat = "PDF";
        public CHRIS_FinanceReports_MCOReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.Option.Items.RemoveAt(this.Option.Items.Count - 1);
            //this.txtNumCopies.Text = "1";
        }


        private void Run_Click(object sender, EventArgs e)
        {
            {
                base.RptFileName = "MCOREP";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport();
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                        //if (Dest_name.Text != string.Empty)
                    String Res1 = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;

                    base.ExportCustomReport(Res1,"pdf");
                }
                    


                        //base.ExportCustomReport();

                if (Dest_Type.Text == "Mail")
                {
                    String Res1 = "";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;
                    base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Res1);
                        
                }

                    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                    //{
                    //    base.EmailToReport(Dest_name.Text, Dest_Format.Text);
                    //}
                }
                //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
                //{
                //    string Path =base.ExportReportInGivenFormat("PDF");

                //}





            }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }


        }
        
    }
