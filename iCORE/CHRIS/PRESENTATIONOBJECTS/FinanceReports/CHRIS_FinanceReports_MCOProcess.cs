using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using System.Threading;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_MCOProcess : ChrisSimpleForm
    {
        #region Data Members

        Result rsltCode;
        Result rsltFinBooking;
        CmnDataManager cmnDM= new CmnDataManager();
        DataTable dtResult  = new DataTable();
        int dLON            = 0;
        double W_INSTALL    = 0.0;
        int MOTH            = 0;
        int FinYear         = 0;
        DateTime Payroll_Date = new DateTime();
        DateTime C_FN_MDATE = new DateTime();
        int W_NO_DAYS       = 0;
        string W_FIN_TYPE   = string.Empty;
        string LON          = string.Empty;
        int FIN             = 0;
        string BRANCH       = string.Empty;
        string SEG          = string.Empty;
        string RDATE        = string.Empty;
        public int MARKUP_RATE_DAYS = -1;
        #endregion

        #region Constructors

        public CHRIS_FinanceReports_MCOProcess()
        {
            InitializeComponent();
        }
        public CHRIS_FinanceReports_MCOProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            tbtSave.Visible     = false;
            tbtAdd.Visible      = false;
            //tbtCancel.Visible   = false;
            tbtDelete.Visible   = false;
            tbtList.Visible     = false;
            txtOption.Visible   = false;
            dtAsofDate.Value    =  null;
            txtSeg.Select();
            txtSeg.Focus();
        }
        
        private void StartProcess()
        {
            dLON = 0;
            FIN = 0;
            Dictionary<string, object> param        = new Dictionary<string, object>();
            Dictionary<string, object> paramBook    = new Dictionary<string, object>();
            Dictionary<string, object> paramMonth   = new Dictionary<string, object>();
            Dictionary<string, object> param1       = new Dictionary<string, object>();

            cmnDM.Execute("CHRIS_SP_DELETE_MCO_REP", "", param);

            param1.Add("Param", 1);
            DataTable dtPickPayrollDate = this.GetData("MCO_Process_pcik_payrolldate", "", param1);
            if (dtPickPayrollDate != null)
            {
                if (dtPickPayrollDate.Rows.Count > 0)
                {
                    if (dtPickPayrollDate.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        Payroll_Date = Convert.ToDateTime(dtPickPayrollDate.Rows[0].ItemArray[0].ToString());
                    }
                }
            }

            DateTime As_of_Date;
            if (dtAsofDate.Value != null)
            {
                As_of_Date = DateTime.Parse(dtAsofDate.Value.ToString());
            }

            FillDataTable();
            DataTable dtFinType = this.GetDataTable("CHRIS_SP_fin_Type_PROC", "");
            DataTable dtFinNos;
            DataTable dtFinBooking;
            DataTable dtFinMonth;

            double month = 0;

            if (dtFinType != null)
            {
                if (dtFinType.Rows.Count > 0)
                {
                    foreach (DataRow Fintype in dtFinType.Rows)
                    {
                        paramBook.Clear();
                        paramBook.Add("fn_type",      Fintype.ItemArray[0].ToString());
                        paramBook.Add("branch",       txtBranchCode.Text);
                        paramBook.Add("seg",          txtSeg.Text);
                        
                        //dtFinBooking    = this.GetData("CHRIS_SP_fin_Booking_PROC", "", paramBook);
                        dtFinBooking = this.GetData("CHRIS_SP_FINBOOKING_PROC", "", paramBook);
                        
                        if (dtFinBooking != null)
                        {
                            if (dtFinBooking.Rows.Count > 0)
                            {
                                foreach (DataRow fbRow in dtFinBooking.Rows)
                                {
                                    W_FIN_TYPE = Fintype.ItemArray[0].ToString();
                                    txtFinNo.Text = fbRow.ItemArray[0].ToString();
                                    Application.DoEvents();
                                    //FinYear =  int.Parse(fbRow["FIN_YEAR"].ToString());
                                    FinYear = Convert.ToInt32(fbRow["FIN_YEAR"].ToString() == "" ? "0" : fbRow["FIN_YEAR"].ToString());

                                    if (FinYear <= 85 && FinYear >= 50)
                                    {
                                        dLON = dLON + 1;
                                    }

                                    paramMonth.Clear();
                                    paramMonth.Add("FN_TYPE",       Fintype.ItemArray[0]    == null ? string.Empty : Fintype.ItemArray[0].ToString());
                                    paramMonth.Add("BRANCH",        txtBranchCode.Text );
                                    paramMonth.Add("SEG",           txtSeg.Text);
                                    paramMonth.Add("FINANCE_NO",    fbRow.ItemArray[0]      == null ? string.Empty : fbRow.ItemArray[0].ToString());
                                    paramMonth.Add("ASOF",          (dtAsofDate.Value != null)? DateTime.Parse(dtAsofDate.Value.ToString()).ToShortDateString(): null);
                                    
                                    dtFinMonth = this.GetData("CHRIS_SP_FINMONTH_PROC", "", paramMonth);

                                    if (dtFinMonth != null)
                                    {
                                        if (dtFinMonth.Rows.Count > 0)
                                        {
                                            foreach (DataRow fmRow in dtFinMonth.Rows)
                                            {
                                                FIN = FIN + 1;
                                                CAL_INST(W_FIN_TYPE, dtPickPayrollDate, fmRow, fbRow, Fintype);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        paramBook.Clear();
                    }
                }
            }
            int row = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                dtResult.Rows[row]["FIN"] = FIN;
                row = row + 1;
            }

            BatchUpdate("CHRIS_SP_FILL_MCO_REP_ADD", dtResult);

            DialogResult ds =  MessageBox.Show("Process Has Been Completed", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Error);
            if (ds == DialogResult.OK)
            {
                //Open MCO_REP1 Report Form
                iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports.CHRIS_FinanceReports_MCO_REP1 rep1Frm = new iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports.CHRIS_FinanceReports_MCO_REP1((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent), this.connbean, txtSeg.Text, txtBranchCode.Text);
                rep1Frm.Show();
            }

        }

        private void CAL_INST(string W_FIN_TYPE, DataTable dtPayrollDate, DataRow dtFinMonth, DataRow dtFinBooking, DataRow dtFinType)
        {
            /*SP Parameters*/
            double C_FN_BALANCE = double.Parse(dtFinMonth["FN_BALANCE"].ToString() == "" ? "0" : dtFinMonth["FN_BALANCE"].ToString());
            int C_FN_PAY_LEFT = int.Parse(dtFinMonth["FN_PAY_LEFT"].ToString() == "" ? "0" : dtFinMonth["FN_PAY_LEFT"].ToString());
	        DateTime C_FN_MDATE	    = new DateTime();
	        DateTime W_DATE		    = new DateTime();
            DateTime W_FN_EXTRA_TIME = new DateTime();

            C_FN_MDATE              = DateTime.Parse(dtFinMonth["FN_MDATE"].ToString());
            W_DATE                  = DateTime.Parse(dtPayrollDate.Rows[0]["w_payroll_date"].ToString());
            TimeSpan ts             = W_DATE - C_FN_MDATE;
            int NO_OF_DAYS          = ts.Days;
            double W_FN_MARKUP      = double.Parse(dtFinType["fn_markup"].ToString() == "" ? "0" : dtFinType["fn_markup"].ToString());
            W_FN_EXTRA_TIME         = DateTime.Parse(dtFinBooking["FN_EXTRA_TIME"].ToString());
            double W_FN_MONTHLY_DED = double.Parse(dtFinBooking["FN_MONTHLY_DED"].ToString() == "" ? "0" : dtFinBooking["FN_MONTHLY_DED"].ToString());

            /*Declare Variable*/
            double W_INSTALL;
	        int B;
            int MOTH;
	        int EXTRA_DAYS ;
	        double W_MARKUP	;

            W_INSTALL   = 0;
            B           = 1;
            MOTH        = 1;
            EXTRA_DAYS  = 1;
            //C_FN_PAY_LEFT = 0;
            C_FN_PAY_LEFT = C_FN_PAY_LEFT + 1;

            while (MOTH <= C_FN_PAY_LEFT+1 && C_FN_BALANCE > 0)
            {
                if (MOTH == 11)
                {
                    //MessageBox.Show("EXTRA_DAYS =" + EXTRA_DAYS + ",  NO_OF_DAYS =" + NO_OF_DAYS + ", W_DATE=" + W_DATE.ToShortDateString() + ", FN_MDATE=" + C_FN_MDATE.ToShortDateString());
                }
                W_MARKUP = (C_FN_BALANCE * ((W_FN_MARKUP / 100) * (NO_OF_DAYS / MARKUP_RATE_DAYS)));

                if (EXTRA_DAYS > 0)
                {
                    TimeSpan tsp = W_FN_EXTRA_TIME - C_FN_MDATE ;
                    EXTRA_DAYS = tsp.Days;
                }


                if (EXTRA_DAYS <= 0)
                {
                    W_INSTALL = W_FN_MONTHLY_DED - W_MARKUP;

                    if (W_INSTALL < 0)
                        W_INSTALL = 0;
                    
                    if ((W_INSTALL > C_FN_BALANCE) || (MOTH == C_FN_PAY_LEFT+1)) 
                        W_INSTALL = C_FN_BALANCE ;

                    C_FN_BALANCE = C_FN_BALANCE - W_INSTALL;
                }

                CHK_COUNTER(W_FIN_TYPE, W_INSTALL, MOTH);

                MOTH            = MOTH + 1;
                NO_OF_DAYS      = 0;
                W_DATE          = base.AddMonth(W_DATE, 1);
                TimeSpan tspn   = W_DATE - C_FN_MDATE;
                NO_OF_DAYS      = tspn.Days;
                C_FN_MDATE      = base.AddMonth(C_FN_MDATE, 1);
                
            }
        }
        
        private void CHK_COUNTER(string FinType, double Install, int Month)
        {
            string FNT = FinType.Substring(0,1).ToString();

            if (FNT == "V" || FNT == "C")
            {
                SetValue("VEHICLE", Month, Install);
            }
            else if(FNT == "E")
            {
                SetValue("EMERGENCY", Month, Install);
            }
            else if(FNT == "S")
            {
                SetValue("STAFF", Month, Install);
            }
            else if (FNT == "H" || FNT == "1")
            {
                SetValue("HOUSE", Month, Install);
            }
        }

        private void FillDataTable()
        {
            dtResult.Columns.Clear();
            dtResult.Rows.Clear();
            dtResult.Columns.Add("DURATION",    typeof(string));
            dtResult.Columns.Add("VEHICLE",     typeof(double));
            dtResult.Columns.Add("EMERGENCY",   typeof(double));
            dtResult.Columns.Add("STAFF",       typeof(double));
            dtResult.Columns.Add("HOUSE",       typeof(double));
            dtResult.Columns.Add("SNO",         typeof(int));
            dtResult.Columns.Add("RDATE",       typeof(DateTime));
            dtResult.Columns.Add("SEG",         typeof(string));
            dtResult.Columns.Add("BRANCH",      typeof(string));
            dtResult.Columns.Add("LON",         typeof(int));
            dtResult.Columns.Add("FIN",         typeof(int));

            dtResult.Rows.Add("1W", null, null, null, null, 1, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("2W", null, null, null, null, 2, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("3W", null, null, null, null, 3, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("4W", null, null, null, null, 4, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("2M", null, null, null, null, 5, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("3M", null, null, null, null, 6, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("4M", null, null, null, null, 7, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("5M", null, null, null, null, 8, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("6M", null, null, null, null, 9, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("7M", null, null, null, null, 10, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("8M", null, null, null, null, 11, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("9M", null, null, null, null, 12, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("10M", null, null, null, null, 13, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("11M", null, null, null, null, 14, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("12M", null, null, null, null, 15, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("2Y1Q", null, null, null, null, 16, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("2Y2Q", null, null, null, null, 17, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("2Y3Q", null, null, null, null, 18, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("2Y4Q", null, null, null, null, 19, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("3Y", null, null, null, null, 20, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("4Y", null, null, null, null, 21, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("5Y", null, null, null, null, 22, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("5YT15", null, null, null, null, 23, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("OVER15", null, null, null, null, 24, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("6Y", null, null, null, null, 25, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("7Y", null, null, null, null, 26, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("8Y", null, null, null, null, 27, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("9Y", null, null, null, null, 28, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("10Y", null, null, null, null, 29, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);
            dtResult.Rows.Add("OVER10", null, null, null, null, 30, dtAsofDate.Value, txtSeg.Text, txtBranchCode.Text, null, null);

            
        }

        private void SetValue(string FinType, int Month, double Install)
        {
            double WEEK4    = 0.0;
         

            if (Month == 1)
            {
                dtResult.Rows[0]["LON"] = dLON;
                dtResult.Rows[1]["LON"] = dLON;
                dtResult.Rows[2]["LON"] = dLON;
                dtResult.Rows[3]["LON"] = dLON;
                

                WEEK4 = Convert.ToDouble(dtResult.Rows[3][FinType].ToString() == "" ? "0.0": dtResult.Rows[3][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[3][FinType] = WEEK4;
            }
            if (Month == 2)
            {
                dtResult.Rows[4]["LON"] = dLON;
                WEEK4 = double.Parse(dtResult.Rows[4][FinType].ToString() == "" ? "0.0" : dtResult.Rows[4][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[4][FinType] = WEEK4;
            }
            if (Month == 3)
            {
                dtResult.Rows[5]["LON"] = dLON;
                WEEK4 = double.Parse(dtResult.Rows[5][FinType].ToString() == "" ? "0.0" : dtResult.Rows[5][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[5][FinType] = WEEK4;
            }
            if (Month == 4)
            {
                dtResult.Rows[6]["LON"] = dLON;
                WEEK4 = double.Parse(dtResult.Rows[6][FinType].ToString() == "" ? "0.0" : dtResult.Rows[6][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[6][FinType] = WEEK4;
            }
            if (Month == 5)
            {
                //Month5 = Month5 + Install;
                dtResult.Rows[7]["LON"] = dLON;
                WEEK4 = double.Parse(dtResult.Rows[7][FinType].ToString() == "" ? "0.0" : dtResult.Rows[7][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[7][FinType] = WEEK4;
            }
            if (Month == 6)
            {
                dtResult.Rows[8]["LON"] = dLON;
                //Month6 = Month6 + Install;
                WEEK4 = double.Parse(dtResult.Rows[8][FinType].ToString() == "" ? "0.0" : dtResult.Rows[8][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[8][FinType] = WEEK4;
            }
            if (Month == 7)
            {
                dtResult.Rows[9]["LON"] = dLON;
                //Month7 = Month7 + Install;
                WEEK4 = double.Parse(dtResult.Rows[9][FinType].ToString() == "" ? "0.0" : dtResult.Rows[9][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[9][FinType] = WEEK4;
            }
            if (Month == 8)
            {
                dtResult.Rows[10]["LON"] = dLON;
                //Month8 = Month8 + Install;
                WEEK4 = double.Parse(dtResult.Rows[10][FinType].ToString() == "" ? "0.0" : dtResult.Rows[10][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[10][FinType] = WEEK4;
            }
            if (Month == 9)
            {
                dtResult.Rows[11]["LON"] = dLON;
                //Month9 = Month9 + Install;
                WEEK4 = double.Parse(dtResult.Rows[11][FinType].ToString() == "" ? "0.0" : dtResult.Rows[11][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[11][FinType] = WEEK4;
            }
            if (Month == 10)
            {
                dtResult.Rows[12]["LON"] = dLON;
                //Month10 = Month10 + Install;
                WEEK4 = double.Parse(dtResult.Rows[12][FinType].ToString() == "" ? "0.0" : dtResult.Rows[12][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[12][FinType] = WEEK4;
            }
            if (Month == 11)
            {
                dtResult.Rows[13]["LON"] = dLON;
               //Month11 = Month11 + Install;
                WEEK4 = double.Parse(dtResult.Rows[13][FinType].ToString() == "" ? "0.0" : dtResult.Rows[13][FinType].ToString());

                //MessageBox.Show("Fin No = " + this.txtFinNo.Text + ",  Installment = " + Install + ",  Total Instal = " + WEEK4);

                WEEK4 = WEEK4 + Install;
                dtResult.Rows[13][FinType] = WEEK4;
            }
            if (Month == 12)
            {
                dtResult.Rows[14]["LON"] = dLON;

                //Month12 = Month12 + Install;
                WEEK4 = double.Parse(dtResult.Rows[14][FinType].ToString() == "" ? "0.0" : dtResult.Rows[14][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[14][FinType] = WEEK4;
            }
            if (Month > 12 && Month < 16)
            {
                dtResult.Rows[15]["LON"] = dLON;

                //Year2_1QTR = Year2_1QTR + Install;
                WEEK4 = double.Parse(dtResult.Rows[15][FinType].ToString() == "" ? "0.0" : dtResult.Rows[15][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[15][FinType] = WEEK4;
            }
            if (Month > 15 && Month < 19)
            {
                dtResult.Rows[16]["LON"] = dLON;

                //Year2_2QTR = Year2_2QTR + Install;
                WEEK4 = double.Parse(dtResult.Rows[16][FinType].ToString() == "" ? "0.0" : dtResult.Rows[16][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[16][FinType] = WEEK4;
            }
            if (Month > 18 && Month < 22)
            {
                dtResult.Rows[17]["LON"] = dLON;

                //Year2_3QTR = Year2_3QTR + Install;
                WEEK4 = double.Parse(dtResult.Rows[17][FinType].ToString() == "" ? "0.0" : dtResult.Rows[17][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[17][FinType] = WEEK4;
            }
            if (Month > 21 && Month < 25)
            {
                dtResult.Rows[18]["LON"] = dLON;

                //Year2_4QTR = Year2_4QTR + Install;
                WEEK4 = double.Parse(dtResult.Rows[18][FinType].ToString() == "" ? "0.0" : dtResult.Rows[18][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[18][FinType] = WEEK4;
            }
            if (Month > 24 && Month < 37)
            {
                dtResult.Rows[19]["LON"] = dLON;

                WEEK4 = double.Parse(dtResult.Rows[19][FinType].ToString() == "" ? "0.0" : dtResult.Rows[19][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[19][FinType] = WEEK4;
            }
            if (Month > 36 && Month < 49)
            {
                dtResult.Rows[20]["LON"] = dLON;

                //year3
                WEEK4 = double.Parse(dtResult.Rows[20][FinType].ToString() == "" ? "0.0" : dtResult.Rows[20][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[20][FinType] = WEEK4;
            }
            if (Month > 48 && Month < 61)
            {
                dtResult.Rows[21]["LON"] = dLON;

                //Year5 = Year5 + Install;
                WEEK4 = double.Parse(dtResult.Rows[21][FinType].ToString() == "" ? "0.0" : dtResult.Rows[21][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[21][FinType] = WEEK4;
            }
            if (Month > 60 && Month < 181)
            {
                dtResult.Rows[22]["LON"] = dLON;

                //year5to15 = year5to15 + Install;
                WEEK4 = double.Parse(dtResult.Rows[22][FinType].ToString() == "" ? "0.0" : dtResult.Rows[22][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[22][FinType] = WEEK4;

            }
            if (Month > 60 && Month < 73)
            {
                dtResult.Rows[24]["LON"] = dLON;

                //Year6 = Year6 + Install;
                WEEK4 = double.Parse(dtResult.Rows[24][FinType].ToString() == "" ? "0.0" : dtResult.Rows[24][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[24][FinType] = WEEK4;
            }
            if (Month > 72 && Month < 85)
            {
                dtResult.Rows[25]["LON"] = dLON;

                //Year7 = Year7 + Install;
                WEEK4 = double.Parse(dtResult.Rows[25][FinType].ToString() == "" ? "0.0" : dtResult.Rows[25][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[25][FinType] = WEEK4;
            }
            if (Month > 84 && Month < 97)
            {
                dtResult.Rows[26]["LON"] = dLON;

                //Year8 = Year8 + Install;
                WEEK4 = double.Parse(dtResult.Rows[26][FinType].ToString() == "" ? "0.0" : dtResult.Rows[26][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[26][FinType] = WEEK4;
            }
            if (Month > 96 && Month < 109)
            {
                dtResult.Rows[27]["LON"] = dLON;

                //Year9 = Year9 + Install;
                WEEK4 = double.Parse(dtResult.Rows[27][FinType].ToString() == "" ? "0.0" : dtResult.Rows[27][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[27][FinType] = WEEK4;
            }
            if (Month > 108 && Month < 121)
            {
                dtResult.Rows[28]["LON"] = dLON;

                //Year10 = Year10 + Install;
                WEEK4 = double.Parse(dtResult.Rows[28][FinType].ToString() == "" ? "0.0" : dtResult.Rows[28][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[28][FinType] = WEEK4;
            }
            if (Month > 120)
            {
                dtResult.Rows[29]["LON"] = dLON;

                //OVER10 = OVER10 + Install;
                WEEK4 = double.Parse(dtResult.Rows[29][FinType].ToString() == "" ? "0.0" : dtResult.Rows[29][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[29][FinType] = WEEK4;
            }
            if (Month > 180)
            {
                dtResult.Rows[23]["LON"] = dLON;

                //over15 = over15 + Install;
                WEEK4 = double.Parse(dtResult.Rows[23][FinType].ToString() == "" ? "0.0" : dtResult.Rows[23][FinType].ToString());
                WEEK4 = WEEK4 + Install;
                dtResult.Rows[23][FinType] = WEEK4;
            }
        }

        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode1;
            CmnDataManager cmnDM1 = new CmnDataManager();
            rsltCode1 = cmnDM1.GetData(sp, actionType, param);

            if (rsltCode1.isSuccessful)
            {
                if (rsltCode1.dstResult.Tables.Count > 0 && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode1.dstResult.Tables[0];
                }
            }

            return dt;

        }
        
        private DataTable GetDataTable(string sp, string actionType)
        {
            DataTable dt = null;

            Result rsltCode1;
            CmnDataManager cmnDM1 = new CmnDataManager();
            rsltCode1 = cmnDM1.GetData(sp, actionType);

            if (rsltCode1.isSuccessful)
            {
                if (rsltCode1.dstResult.Tables.Count > 0 && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode1.dstResult.Tables[0];
                }
            }

            return dt;

        }

        public Result BatchUpdate(string pstrSPName, DataTable poDT)
        {
            DataSet dst = new DataSet();

            dst.Tables.Add(SQLManager.GetSPParams(pstrSPName));

            return SQLManager.ExecuteBatchUpdate(dst, poDT);
        }

        #endregion

        #region Event Handlers

        private void btnStart_Click(object sender, EventArgs e)
        {
            FIN = 0;
            this.StartProcess();

            #region
            //DateTime Payroll_Date = new DateTime();
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //Dictionary<string, object> paramBook = new Dictionary<string, object>();
            //Dictionary<string, object> param1 = new Dictionary<string, object>();
            //param1.Add("Param", 1);
            //DataTable dtPickPayrollDate = this.GetData("MCO_Process_pcik_payrolldate", "", param1);
            //if (dtPickPayrollDate != null)
            //{
            //    if (dtPickPayrollDate.Rows.Count > 0)
            //    {
            //        if (dtPickPayrollDate.Rows[0].ItemArray[0].ToString() != string.Empty)
            //        {
            //            Payroll_Date = Convert.ToDateTime(dtPickPayrollDate.Rows[0].ItemArray[0].ToString());
            //        }

            //    }

            //}
            //DateTime As_of_Date;
            //if (dtAsofDate.Value != null)
            //{
            //    As_of_Date = dtAsofDate.Value;
            //}

            //DataTable dtFinType = this.GetDataTable("CHRIS_SP_fin_Type_PROC", "");
            //DataTable dtFinNos;



            //if (dtFinType != null)
            //{
            //    if (dtFinType.Rows.Count > 0)
            //    {
            //        foreach (DataRow Fintype in dtFinType.Rows)
            //        {
            //            param.Add("w_fn_type", Fintype.ItemArray[0].ToString());
            //            param.Add("w_branch", txtBranchCode.Text);
            //            param.Add("w_seg", txtSeg.Text);
            //            dtFinNos = this.GetData("CHRIS_SP_fin_Number_PROC", "", param);
            //            if (dtFinNos != null)
            //            {
            //                if (dtFinNos.Rows.Count > 0)
            //                {
            //                    foreach (DataRow FinNo in dtFinNos.Rows)
            //                    {
            //                        txtFinNo.Text = FinNo.ItemArray[0].ToString();
            //                        Thread.Sleep(1000);
            //                        // dtFinNos = this.GetData("CHRIS_SP_fin_Number_PROC", "", param);
            //                        paramBook.Add("w_fn_type", Fintype.ItemArray[0].ToString());
            //                        paramBook.Add("w_branch", txtBranchCode.Text);
            //                        paramBook.Add("w_seg", txtSeg.Text);
            //                        paramBook.Add("W_PAYROLL_DATE", Payroll_Date);
            //                        paramBook.Add("ASofDate", As_of_Date);
            //                        paramBook.Add("w_fn_markup", Fintype.ItemArray[2]);
            //                        paramBook.Add("S_fn_finance_no", FinNo.ItemArray[0].ToString());
            //                        rsltCode = cmnDM.Execute("CHRIS_SP_fin_Booking_PROC", "", paramBook);


            //                        paramBook.Clear();



            //                    }

            //                }

            //            }
            //            param.Clear();

            //        }
            //    }

            //}

            #endregion
        }

        private void slButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        #endregion
       
    }
}