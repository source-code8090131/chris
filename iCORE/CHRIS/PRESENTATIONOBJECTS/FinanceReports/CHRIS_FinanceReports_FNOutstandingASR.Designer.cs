namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    partial class CHRIS_FinanceReports_FNOutstandingASR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_FinanceReports_FNOutstandingASR));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNumCopies = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDestName = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.BRANCH);
            this.groupBox1.Controls.Add(this.txtNumCopies);
            this.groupBox1.Controls.Add(this.txtDestName);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(11, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(510, 267);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(210, 189);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(199, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 27;
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            this.W_SEG.TextChanged += new System.EventHandler(this.W_SEG_TextChanged);
            // 
            // BRANCH
            // 
            this.BRANCH.AllowSpace = true;
            this.BRANCH.AssociatedLookUpName = "";
            this.BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BRANCH.ContinuationTextBox = null;
            this.BRANCH.CustomEnabled = true;
            this.BRANCH.DataFieldMapping = "";
            this.BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRANCH.GetRecordsOnUpDownKeys = false;
            this.BRANCH.IsDate = false;
            this.BRANCH.Location = new System.Drawing.Point(210, 163);
            this.BRANCH.MaxLength = 3;
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.NumberFormat = "###,###,##0.00";
            this.BRANCH.Postfix = "";
            this.BRANCH.Prefix = "";
            this.BRANCH.Size = new System.Drawing.Size(199, 20);
            this.BRANCH.SkipValidation = false;
            this.BRANCH.TabIndex = 26;
            this.BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNumCopies
            // 
            this.txtNumCopies.AllowSpace = true;
            this.txtNumCopies.AssociatedLookUpName = "";
            this.txtNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumCopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumCopies.ContinuationTextBox = null;
            this.txtNumCopies.CustomEnabled = true;
            this.txtNumCopies.DataFieldMapping = "";
            this.txtNumCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCopies.GetRecordsOnUpDownKeys = false;
            this.txtNumCopies.IsDate = false;
            this.txtNumCopies.Location = new System.Drawing.Point(210, 137);
            this.txtNumCopies.MaxLength = 2;
            this.txtNumCopies.Name = "txtNumCopies";
            this.txtNumCopies.NumberFormat = "###,###,##0.00";
            this.txtNumCopies.Postfix = "";
            this.txtNumCopies.Prefix = "";
            this.txtNumCopies.Size = new System.Drawing.Size(199, 20);
            this.txtNumCopies.SkipValidation = false;
            this.txtNumCopies.TabIndex = 25;
            this.txtNumCopies.TextType = CrplControlLibrary.TextType.Integer;
            this.txtNumCopies.TextChanged += new System.EventHandler(this.txtNumCopies_TextChanged);
            // 
            // txtDestName
            // 
            this.txtDestName.AllowSpace = true;
            this.txtDestName.AssociatedLookUpName = "";
            this.txtDestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDestName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDestName.ContinuationTextBox = null;
            this.txtDestName.CustomEnabled = true;
            this.txtDestName.DataFieldMapping = "";
            this.txtDestName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestName.GetRecordsOnUpDownKeys = false;
            this.txtDestName.IsDate = false;
            this.txtDestName.Location = new System.Drawing.Point(210, 111);
            this.txtDestName.MaxLength = 50;
            this.txtDestName.Name = "txtDestName";
            this.txtDestName.NumberFormat = "###,###,##0.00";
            this.txtDestName.Postfix = "";
            this.txtDestName.Prefix = "";
            this.txtDestName.Size = new System.Drawing.Size(199, 20);
            this.txtDestName.SkipValidation = false;
            this.txtDestName.TabIndex = 24;
            this.txtDestName.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(210, 84);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(199, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 23;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(280, 225);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 23);
            this.btnClose.TabIndex = 29;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(211, 225);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 28;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Enter Segment GF or GCB   :";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Enter Valid Branch    :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Number of Copies";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Destination Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(190, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Enter values for the parameters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(207, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(466, 36);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_FinanceReports_FNOutstandingASR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 317);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_FinanceReports_FNOutstandingASR";
            this.Text = "CHRIS_FinanceReports_FNOutstandingASR";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private CrplControlLibrary.SLTextBox W_SEG;
        private CrplControlLibrary.SLTextBox BRANCH;
        private CrplControlLibrary.SLTextBox txtNumCopies;
        private CrplControlLibrary.SLTextBox txtDestName;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}