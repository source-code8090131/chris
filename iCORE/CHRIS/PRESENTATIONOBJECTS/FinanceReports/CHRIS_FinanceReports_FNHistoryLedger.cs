using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
/*data is not entered and data is slow*/
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_FNHistoryLedger : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_FinanceReports_FNHistoryLedger()
        {
            InitializeComponent();
        }
        //string DestName = @"C:\Report";
        string DestFormat = "PDF";
        string Destname = @"C:\iCORE-Spool\Report";
        public CHRIS_FinanceReports_FNHistoryLedger(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }
        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.nocopies.Text = "1";
            //Output_mod.Items.RemoveAt(3);
        }


        private void btnRun_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);

            base.RptFileName = "FNREP12";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (cmbDescType.Text == "Printer")
            {

                base.PrintNoofCopiesReport(no_of_copies);


            }


            //if (Dest_Type.Text == "Printer")
            //{

            //    base.PrintNoofCopiesReport(no_of_copies);


            //}

            if (cmbDescType.Text == "Mail")
            {


                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF");

            }


            if (cmbDescType.Text == "File")
            {
                base.ExportCustomReport(@"C:\iCORE-Spool\Report", "pdf");


            }
            #region commented code

            //    int no_of_copies;
        //    {
            
        //    base.RptFileName = "FNREP12";
            
        //    if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
        //    {
        //        base.btnCallReport_Click(sender, e);
        //    }
        //    else if (cmbDescType.Text == "Printer")
        //    {
        //        base.PrintCustomReport();
        //    }
        //    else if (cmbDescType.Text == "File")
        //    {

        //        string DestName;
        //        string DestFormat;
            
        //            DestName = @"C:\Report";
            
        //        if (DestName != string.Empty)
        //        {
        //            base.ExportCustomReport(DestName, "PDF");
        //        }
        //    }
        //    else if (cmbDescType.Text == "Mail")
        //    {
        //        string DestName = @"C:\Report";
        //        string DestFormat;
        //        string RecipentName;
        //        if (DestName != string.Empty)
        //        {
        //            base.EmailToReport(DestName, "PDF");
        //        }
            //    }
            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        #endregion 

    }
}