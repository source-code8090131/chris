using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports
{
    public partial class CHRIS_FinanceReports_CategoryWiseOutstandingBalanceLocal : BaseRptForm
    {
        private string DestFormat = "PDF";
        private string DestName = "c:\\iCORE-Spool\\report";
        public CHRIS_FinanceReports_CategoryWiseOutstandingBalanceLocal()
        {
            InitializeComponent();
        }


        public CHRIS_FinanceReports_CategoryWiseOutstandingBalanceLocal(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.FillBranch();
            this.fillsegCombo();
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.Copies.Text = "1";
            this.txtDest_Format.Text = "wide";
        }

        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCountAll");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.w_branch.DisplayMember = "BRN_CODE";
                this.w_branch.ValueMember = "BRN_CODE";
                this.w_branch.DataSource = rsltCode.dstResult.Tables[0];
            }

        }
        
        private void fillsegCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.W_SEG.DisplayMember = "pr_segment";
                    this.W_SEG.ValueMember = "pr_segment";
                    this.W_SEG.DataSource = rsltCode.dstResult.Tables[0];
                }

            }

        }
        

        private void Run_Click_1(object sender, EventArgs e)
        {
            {
                base.RptFileName = "FNREP04_01";

                if (this.txtDest_Format.Text == string.Empty)
                    DestFormat = "PDF";
                else
                    DestFormat = this.txtDest_Format.Text;
                if (this.Dest_name.Text == string.Empty)
                    DestName = "c:\\iCORE-Spool\\report";
                else
                    DestName = this.Dest_name.Text;
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport(this.Copies.Text);
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                    //if (Dest_name.Text != string.Empty)
                    String Rec = "";
                    if (this.Dest_name.Text == string.Empty)
                        Rec = "c:\\iCORE-Spool\\Report";
                    else
                    {
                        Rec = this.Dest_name.Text;
                    }

                    base.ExportCustomReport(Rec, "pdf");




                    //base.ExportCustomReport();



                }

            }

            if (Dest_Type.Text == "Mail")
            {
                String Rec = "";
                if (this.Dest_name.Text == string.Empty)
                    Rec = "";
                else
                {
                    Rec = this.Dest_name.Text;
                }
                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Rec);

                //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //{
                //    base.EmailToReport(Dest_name.Text, Dest_Format.Text);
                //}
            }
            //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
            //{
            //    string Path =base.ExportReportInGivenFormat("PDF");

            //}




        }

        private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }

        private void Dest_name_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}


