namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_BonusQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_BonusQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtAnnPkg = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.lblAnnPackge = new System.Windows.Forms.Label();
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.lblBranch = new System.Windows.Forms.Label();
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.lblLevel = new System.Windows.Forms.Label();
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.lblDesig = new System.Windows.Forms.Label();
            this.lblPrNum = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvBonusDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.BO_INC_D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BO_INC_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BO_10_D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BO_10C_BONUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlMaster.SuspendLayout();
            this.PnlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBonusDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(631, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(667, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 513);
            this.panel1.Size = new System.Drawing.Size(667, 60);
            // 
            // pnlMaster
            // 
            this.pnlMaster.ConcurrentPanels = null;
            this.pnlMaster.Controls.Add(this.txtAnnPkg);
            this.pnlMaster.Controls.Add(this.lbtnPNo);
            this.pnlMaster.Controls.Add(this.lblAnnPackge);
            this.pnlMaster.Controls.Add(this.txtPersNo);
            this.pnlMaster.Controls.Add(this.lblBranch);
            this.pnlMaster.Controls.Add(this.txtLastName);
            this.pnlMaster.Controls.Add(this.lblLevel);
            this.pnlMaster.Controls.Add(this.txtBranch);
            this.pnlMaster.Controls.Add(this.txtLevel);
            this.pnlMaster.Controls.Add(this.txtDesig);
            this.pnlMaster.Controls.Add(this.txtFirstName);
            this.pnlMaster.Controls.Add(this.lblDesig);
            this.pnlMaster.Controls.Add(this.lblPrNum);
            this.pnlMaster.Controls.Add(this.lblName);
            this.pnlMaster.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMaster.DependentPanels = null;
            this.pnlMaster.DisableDependentLoad = false;
            this.pnlMaster.EnableDelete = true;
            this.pnlMaster.EnableInsert = true;
            this.pnlMaster.EnableQuery = false;
            this.pnlMaster.EnableUpdate = true;
            this.pnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlMaster.Location = new System.Drawing.Point(56, 120);
            this.pnlMaster.MasterPanel = null;
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMaster.Size = new System.Drawing.Size(589, 126);
            this.pnlMaster.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.pnlMaster.TabIndex = 0;
            this.pnlMaster.TabStop = true;
            // 
            // txtAnnPkg
            // 
            this.txtAnnPkg.AllowSpace = true;
            this.txtAnnPkg.AssociatedLookUpName = "";
            this.txtAnnPkg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnPkg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnPkg.ContinuationTextBox = null;
            this.txtAnnPkg.CustomEnabled = true;
            this.txtAnnPkg.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtAnnPkg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnPkg.GetRecordsOnUpDownKeys = false;
            this.txtAnnPkg.IsDate = false;
            this.txtAnnPkg.Location = new System.Drawing.Point(121, 88);
            this.txtAnnPkg.MaxLength = 10;
            this.txtAnnPkg.Name = "txtAnnPkg";
            this.txtAnnPkg.NumberFormat = "###,###,##0.00";
            this.txtAnnPkg.Postfix = "";
            this.txtAnnPkg.Prefix = "";
            this.txtAnnPkg.ReadOnly = true;
            this.txtAnnPkg.Size = new System.Drawing.Size(117, 20);
            this.txtAnnPkg.SkipValidation = false;
            this.txtAnnPkg.TabIndex = 0;
            this.txtAnnPkg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnnPkg.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PP_NO_EXISTS";
            this.lbtnPNo.ActionType = "PP_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnPNo.HiddenColumns = "PR_NEW_ANNUAL_PACK";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(246, 12);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_BONUSQUERY_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.toolTip1.SetToolTip(this.lbtnPNo, "Select Desired Personnel Number");
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // lblAnnPackge
            // 
            this.lblAnnPackge.AutoSize = true;
            this.lblAnnPackge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnnPackge.Location = new System.Drawing.Point(15, 92);
            this.lblAnnPackge.Name = "lblAnnPackge";
            this.lblAnnPackge.Size = new System.Drawing.Size(100, 13);
            this.lblAnnPackge.TabIndex = 1;
            this.lblAnnPackge.Text = "Annual Package";
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(121, 12);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(117, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 1;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.TextChanged += new System.EventHandler(this.txtPersNo_TextChanged);
            this.txtPersNo.Leave += new System.EventHandler(this.txtPersNo_Leave);
            this.txtPersNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersNo_KeyPress);
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            this.lblBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(396, 93);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(47, 13);
            this.lblBranch.TabIndex = 1;
            this.lblBranch.Text = "Branch";
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "PR_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(449, 13);
            this.txtLastName.MaxLength = 10;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.ReadOnly = true;
            this.txtLastName.Size = new System.Drawing.Size(117, 20);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 0;
            this.txtLastName.TabStop = false;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(405, 55);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(38, 13);
            this.lblLevel.TabIndex = 1;
            this.lblLevel.Text = "Level";
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(449, 89);
            this.txtBranch.MaxLength = 10;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(117, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 0;
            this.txtBranch.TabStop = false;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(449, 51);
            this.txtLevel.MaxLength = 10;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(117, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 0;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(121, 50);
            this.txtDesig.MaxLength = 100;
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(199, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 0;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(326, 13);
            this.txtFirstName.MaxLength = 10;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(117, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 0;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblDesig
            // 
            this.lblDesig.AutoSize = true;
            this.lblDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesig.Location = new System.Drawing.Point(15, 54);
            this.lblDesig.Name = "lblDesig";
            this.lblDesig.Size = new System.Drawing.Size(39, 13);
            this.lblDesig.TabIndex = 1;
            this.lblDesig.Text = "Desig";
            // 
            // lblPrNum
            // 
            this.lblPrNum.AutoSize = true;
            this.lblPrNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrNum.Location = new System.Drawing.Point(15, 16);
            this.lblPrNum.Name = "lblPrNum";
            this.lblPrNum.Size = new System.Drawing.Size(35, 13);
            this.lblPrNum.TabIndex = 1;
            this.lblPrNum.Text = "PrNo";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(281, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name";
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(257, 49);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(165, 31);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Bonus Query";
            // 
            // PnlDetail
            // 
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.dgvBonusDetail);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = false;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabQueryCommand";
            this.PnlDetail.Location = new System.Drawing.Point(56, 273);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(589, 203);
            this.PnlDetail.SPName = "CHRIS_SP_BONUS_TAB_MANAGER";
            this.PnlDetail.TabIndex = 2;
            this.PnlDetail.TabStop = true;
            // 
            // dgvBonusDetail
            // 
            this.dgvBonusDetail.AllowUserToAddRows = false;
            this.dgvBonusDetail.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBonusDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBonusDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBonusDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BO_INC_D,
            this.BO_INC_AMOUNT,
            this.BO_10_D,
            this.BO_10C_BONUS});
            this.dgvBonusDetail.ColumnToHide = null;
            this.dgvBonusDetail.ColumnWidth = null;
            this.dgvBonusDetail.CustomEnabled = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBonusDetail.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvBonusDetail.DisplayColumnWrapper = null;
            this.dgvBonusDetail.GridDefaultRow = 0;
            this.dgvBonusDetail.Location = new System.Drawing.Point(16, 10);
            this.dgvBonusDetail.Name = "dgvBonusDetail";
            this.dgvBonusDetail.ReadOnly = true;
            this.dgvBonusDetail.ReadOnlyColumns = null;
            this.dgvBonusDetail.RequiredColumns = "";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBonusDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvBonusDetail.Size = new System.Drawing.Size(550, 183);
            this.dgvBonusDetail.SkippingColumns = null;
            this.dgvBonusDetail.TabIndex = 2;
            // 
            // BO_INC_D
            // 
            this.BO_INC_D.DataPropertyName = "BO_INC_D";
            dataGridViewCellStyle2.Format = "dd-MMM-yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.BO_INC_D.DefaultCellStyle = dataGridViewCellStyle2;
            this.BO_INC_D.HeaderText = "Bonus Inc Date";
            this.BO_INC_D.MaxInputLength = 10;
            this.BO_INC_D.Name = "BO_INC_D";
            this.BO_INC_D.ReadOnly = true;
            this.BO_INC_D.Width = 130;
            // 
            // BO_INC_AMOUNT
            // 
            this.BO_INC_AMOUNT.DataPropertyName = "BO_INC_AMOUNT";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.BO_INC_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            this.BO_INC_AMOUNT.HeaderText = "Bonus Inc Amount";
            this.BO_INC_AMOUNT.Name = "BO_INC_AMOUNT";
            this.BO_INC_AMOUNT.ReadOnly = true;
            this.BO_INC_AMOUNT.Width = 145;
            // 
            // BO_10_D
            // 
            this.BO_10_D.DataPropertyName = "BO_10_D";
            dataGridViewCellStyle4.Format = "dd-MMM-yyyy";
            dataGridViewCellStyle4.NullValue = null;
            this.BO_10_D.DefaultCellStyle = dataGridViewCellStyle4;
            this.BO_10_D.HeaderText = "Bonus 10 Date";
            this.BO_10_D.Name = "BO_10_D";
            this.BO_10_D.ReadOnly = true;
            this.BO_10_D.Width = 130;
            // 
            // BO_10C_BONUS
            // 
            this.BO_10C_BONUS.DataPropertyName = "BO_10C_BONUS";
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.BO_10C_BONUS.DefaultCellStyle = dataGridViewCellStyle5;
            this.BO_10C_BONUS.HeaderText = "Bonus 10c Bonus";
            this.BO_10C_BONUS.Name = "BO_10C_BONUS";
            this.BO_10C_BONUS.ReadOnly = true;
            this.BO_10C_BONUS.Width = 130;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(408, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 70;
            this.lblUserName.Text = "User Name :";
            // 
            // CHRIS_Query_BonusQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 573);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.PnlDetail);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.pnlMaster);
            this.CurrentPanelBlock = "pnlMaster";
            this.Name = "CHRIS_Query_BonusQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Bonus Query";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlMaster, 0);
            this.Controls.SetChildIndex(this.lblHeader, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            this.PnlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBonusDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlMaster;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Label lblAnnPackge;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label lblBranch;
        private CrplControlLibrary.SLTextBox txtLastName;
        private System.Windows.Forms.Label lblLevel;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtAnnPkg;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label lblDesig;
        private System.Windows.Forms.Label lblPrNum;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvBonusDetail;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_INC_D;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_INC_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_10_D;
        private System.Windows.Forms.DataGridViewTextBoxColumn BO_10C_BONUS;
    }
}