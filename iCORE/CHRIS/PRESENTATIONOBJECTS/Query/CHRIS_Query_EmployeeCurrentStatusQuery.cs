using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_EmployeeCurrentStatusQuery : ChrisMasterDetailForm 
    {
    
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            DataTable dtPrsnl = new DataTable();
            DataTable dtDptCnt = new DataTable();
            DataTable dtPrsnal = new DataTable();
            DataTable dtBldeg = new DataTable();
            DataTable dtChild = new DataTable();

            CmnDataManager cmnDM = new CmnDataManager();
            Result rsltEmp,rsltDptCnt,rsltBLDEG,rsltPrsnal,rsltChild;
        
        public CHRIS_Query_EmployeeCurrentStatusQuery()
        {
            InitializeComponent();
            
        }



        public CHRIS_Query_EmployeeCurrentStatusQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
     
            InitializeComponent();
            txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
            txtPersNo.Focus();
            txtUser.Text = this.userID;
            this.CurrentPanelBlock = this.pnlDetail.Name;

            dgvSDC.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 
            

        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(pnlDetail.Controls);
                this.ClearForm(pnlPersonalDtl.Controls);
                txtPersNo.Focus();
            }


        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;
        }

        protected override bool Quit()
        {
            foreach (Control tx in this.pnlDetail.Controls)
            {

                if (tx != null)
                    if (tx is SLTextBox)
                        if (tx.Text != string.Empty)
                            this.FunctionConfig.CurrentOption = Function.Query;

            }
            bool flag = false;
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            }
            lblUserName.Text += " " + this.UserName;
            this.txtUser.Text = this.userID;
            DateTime now = this.CurrentDate;
            this.txtDate.Text = now.ToString("dd/MM/yyyy");
            this.txtLocation.Text = this.CurrentLocation;
            return flag;
            //return true;
        }

        private void txtPersNo_TextChanged(object sender, EventArgs e)
        {

     
           // if (txtPersNo.Text != "")
           // {

            //    this.colsNVals.Clear(); 
            //    this.colsNVals.Add("PR_P_NO",txtPersNo.Text);

            //    rsltEmp = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNL_LIST", colsNVals);

            //    if (rsltEmp.isSuccessful)
            //    {
            //        //MessageBox.Show("count of Month =   " + rsltEmp.dstResult.Tables.Count);
            //        if (rsltEmp.dstResult.Tables.Count > 0)
            //        {
            //            dtPrsnl = rsltEmp.dstResult.Tables[0];
            //            if (dtPrsnl.Rows.Count > 0)
            //            {
            //                txtFirstName.Text = (dtPrsnl.Rows[0]["PR_FIRST_NAME"]=="NULL") ? string.Empty : Convert.ToString(dtPrsnl.Rows[0]["PR_FIRST_NAME"]);
            //                txtLastName.Text = (dtPrsnl.Rows[0]["PR_LAST_NAME"]=="NULL") ? string.Empty : Convert.ToString(dtPrsnl.Rows[0]["PR_LAST_NAME"]);
            //                txtFunctional.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FUNC_TITTLE1"]);
            //                txtTitle.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FUNC_TITTLE2"]);
            //                txtJoinDate.Text = Convert.ToDateTime(dtPrsnl.Rows[0]["PR_JOINING_DATE"]).ToString("dd/MM/yyyy");
            //                txtConfirm.Text = Convert.ToDateTime(dtPrsnl.Rows[0]["PR_CONFIRM"]).ToString("dd/MM/yyyy");
            //                txtConfirmDate.Text = Convert.ToDateTime(dtPrsnl.Rows[0]["PR_CONFIRM_ON"]).ToString("dd/MM/yyyy");
            //                txtLastAppr.Text = Convert.ToDateTime(dtPrsnl.Rows[0]["PR_LAST_INCREMENT"]).ToString("dd/MM/yyyy");
            //                txtNextAppr.Text = Convert.ToDateTime(dtPrsnl.Rows[0]["PR_NEXT_INCREMENT"]).ToString("dd/MM/yyyy");
            //                txtCategory.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_CATEGORY"]);
            //                txtAnnPkge.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_ANNUAL_PACK"]);
            //                txtAccNum.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_ACCOUNT_NO"]);
            //                txtNatTaxNum.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_NATIONAL_TAX"]);
                           

            //                rsltDptCnt = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "DPTCONT_GRID", colsNVals);
            //                if (rsltDptCnt.dstResult.Tables.Count >0)
            //                {
            //                    dtDptCnt = rsltDptCnt.dstResult.Tables[0];
            //                    this.dgvSDC.DataSource  = dtDptCnt;
            //                }


            //                rsltBLDEG = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "MISC_FILL", colsNVals);
            //                if (rsltBLDEG.isSuccessful)
            //                {
            //                    if (rsltBLDEG.dstResult.Tables.Count > 0)
            //                    {
            //                        dtBldeg = rsltBLDEG.dstResult.Tables[0];
            //                        if (dtBldeg.Rows.Count > 0)
            //                        {

            //                            txtDesig.Text = Convert.ToString(dtBldeg.Rows[0]["DGG"]);
            //                            txtLevel.Text = Convert.ToString(dtBldeg.Rows[0]["LEVEL"]);
            //                            txtBranch.Text = (dtBldeg.Rows[0]["BRNCHNAME"]=="") ? string.Empty : Convert.ToString(dtBldeg.Rows[0]["BRNCHNAME"]);
            //                            txtEducation.Text = Convert.ToString(dtBldeg.Rows[0]["DGREE"]);
            //                            txtGEID.Text = Convert.ToString(dtBldeg.Rows[0]["GEIDNO"]);
            //                        }
            //                        else
            //                        {
            //                            txtDesig.Text = "";
            //                            txtLevel.Text = "";
            //                            txtBranch.Text = "";
            //                            txtGEID.Text = "";
            //                            txtEducation.Text = "";

            //                        }
            //                    }

            //                }



            //                rsltPrsnal = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNAL", colsNVals);
            //                if (rsltPrsnal.isSuccessful)
            //                {
            //                    if (rsltPrsnal.dstResult.Tables.Count > 0)
            //                    {
            //                        dtPrsnal = rsltPrsnal.dstResult.Tables[0];
            //                        if (dtPrsnal.Rows.Count > 0)
            //                        {

            //                            txtAddress.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ADD1"]);
            //                            txtAddress2.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ADD2"]);
            //                            txtPhoneNum.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_PHONE1"]);
            //                            txtPhoneNum2.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_PHONE2"]);
            //                            txtNICNum.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ID_CARD_NO"]);
            //                            txtSex.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_SEX"]);
            //                            txtDoB.Text = Convert.ToDateTime(dtPrsnal.Rows[0]["PR_D_BIRTH"]).ToString("dd/MM/yyyy");
            //                            txtMartStatus.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_MARITAL"]);
            //                            txtMarriageDate.Text = Convert.ToDateTime(dtPrsnal.Rows[0]["PR_MARRIAGE"]).ToString("dd/MM/yyyy") ;
            //                            txtSpDoB.Text = Convert.ToDateTime(dtPrsnal.Rows[0]["PR_BIRTH_SP"]).ToString("dd/MM/yyyy");
            //                            //txtDateofBirth.Text = Convert.ToDateTime(dtPrsnal.Rows[0]["PR_DATE_BIRTH"]).ToString("dd/MM/yyyy"); 
            //                            //txtChildName.Text = txtChildName.Text + "   " + Convert.ToString(dtPrsnal.Rows[0]["PR_CHILD_NAME"]);
            //                            txtNoofChild.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_NO_OF_CHILD"]);
            //                            txtSpouseName.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_SPOUSE"]);

            //                            this.dgvChildren.DataSource = dtPrsnal; // Convert.ToString(dtPrsnal.Rows[0]["PR_CHILD_NAME"]);
            //                            this.dgvChildDB.DataSource = dtPrsnal; // Convert.ToString(dtPrsnal.Rows[0]["PR_DATE_BIRTH"]);
            //                        }
            //                        else
            //                        {
            //                            txtAddress.Text = "";
            //                            txtAddress2.Text = "";
            //                            txtPhoneNum.Text = "";
            //                            txtPhoneNum2.Text = "";
            //                            txtNICNum.Text = "";
            //                            txtSex.Text = "";
            //                            txtDoB.Text = "";
            //                            txtMartStatus.Text = "";
            //                            txtMarriageDate.Text = "";
            //                            txtSpDoB.Text = "";
            //                            txtNoofChild.Text = "";
            //                            txtSpouseName.Text = "";
            //                            dgvChildDB.DataSource = null;  
            //                            dgvChildDB.Refresh();
            //                            dgvChildren.DataSource = null; 
            //                            dgvChildren.Refresh(); 

            //                        }
            //                    }

            //                }

        
            //            }

            //        }

            //    }

            //}
            //else
            //{
            //    txtFirstName.Text = "";
            //    txtLastName.Text = "";
            //    txtDesig.Text = "";
            //    txtBranch.Text = "";
            //    txtLevel.Text = "";
            //    txtGEID.Text = "";
            //    txtEducation.Text = "";
            //    txtFunctional.Text="";
            //    txtTitle.Text = "";
            //    txtJoinDate.Text = "";
            //    txtConfirm.Text = "";
            //    txtConfirmDate.Text = "";
            //    txtLastAppr.Text = "";
            //    txtNextAppr.Text = "";
            //    txtCategory.Text = "";
            //    txtAnnPkge.Text = "";
            //    txtAccNum.Text = "";
            //    txtNatTaxNum.Text = "";
            //    dgvSDC.Refresh();
            //    txtPersNo.Focus();
            //}



        }

        private void txtPersNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }

        private void txtPersNo_Leave(object sender, EventArgs e)
        {

            if (txtPersNo.Text != "")
            {
                //lbtnPNo.Focus();

                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtDesig.Text = "";
                txtBranch.Text = "";
                txtLevel.Text = "";
                txtGEID.Text = "";
                txtEducation.Text = "";
                txtFunctional.Text = "";
                txtTitle.Text = "";
                txtJoinDate.Text = "";
                txtConfirm.Text = "";
                txtConfirmDate.Text = "";
                txtLastAppr.Text = "";
                txtNextAppr.Text = "";
                txtCategory.Text = "";
                txtAnnPkge.Text = "";
                txtAccNum.Text = "";
                txtNatTaxNum.Text = "";
                slTxtLocation.Text = "";
                dgvSDC.Refresh();
                dgvChildDB.Refresh();
                dgvChildren.Refresh();

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);
                rsltPrsnal = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PP_VERIFY", colsNVals);
                if (rsltPrsnal.isSuccessful)
                {
                    if (rsltPrsnal.dstResult.Tables.Count> 0 &&  rsltPrsnal.dstResult.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("YOU HAVE ENTERED AN INVALID NUMBER");
                        txtPersNo.Focus();
                        return;

                    }
                }

                //------------------------ FETCHING VALUES ---------------------------//

                this.colsNVals.Clear(); 
                this.colsNVals.Add("PR_P_NO",txtPersNo.Text);

                rsltEmp = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNL_LIST", colsNVals);

                if (rsltEmp.isSuccessful)
                {
                    //MessageBox.Show("count of Month =   " + rsltEmp.dstResult.Tables.Count);
                    if (rsltEmp.dstResult.Tables.Count > 0)
                    {
                        dtPrsnl = rsltEmp.dstResult.Tables[0];
                        if (dtPrsnl.Rows.Count > 0)
                        {
                            
                            txtFirstName.Text = "";
                            txtLastName.Text = "";

                            txtFirstName.Text = (dtPrsnl.Rows[0]["PR_FIRST_NAME"] == null) ? string.Empty : Convert.ToString(dtPrsnl.Rows[0]["PR_FIRST_NAME"]);
                            txtLastName.Text = (dtPrsnl.Rows[0]["PR_LAST_NAME"] == null) ? string.Empty : Convert.ToString(dtPrsnl.Rows[0]["PR_LAST_NAME"]);
                            txtFunctional.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FUNC_TITTLE1"]);
                            txtTitle.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FUNC_TITTLE2"]);
                            txtJoinDate.Text = dtPrsnl.Rows[0]["PR_JOINING_DATE"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_JOINING_DATE"]).ToString("dd/MM/yyyy");
                            txtConfirm.Text = dtPrsnl.Rows[0]["PR_CONFIRM"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_CONFIRM"]).ToString("dd/MM/yyyy");
                            txtConfirmDate.Text = dtPrsnl.Rows[0]["PR_CONFIRM_ON"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_CONFIRM_ON"]).ToString("dd/MM/yyyy");
                            txtLastAppr.Text = dtPrsnl.Rows[0]["PR_LAST_INCREMENT"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_LAST_INCREMENT"]).ToString("dd/MM/yyyy");
                            txtNextAppr.Text = dtPrsnl.Rows[0]["PR_NEXT_INCREMENT"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_NEXT_INCREMENT"]).ToString("dd/MM/yyyy");
                            txtCategory.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_CATEGORY"]);
                            txtAnnPkge.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_NEW_ANNUAL_PACK"]);
                            txtAccNum.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_ACCOUNT_NO"]);
                            txtNatTaxNum.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_NATIONAL_TAX"]);
                            slTxtLocation.Text = (dtPrsnl.Rows[0]["LOCATION"] == null)? String.Empty: Convert.ToString(dtPrsnl.Rows[0]["LOCATION"]);


                            rsltDptCnt = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "DPTCONT_GRID", colsNVals);
                            if (rsltDptCnt.dstResult.Tables.Count > 0)
                            {
                                dtDptCnt = rsltDptCnt.dstResult.Tables[0];
                                this.dgvSDC.DataSource = dtDptCnt;
                            }


                            rsltBLDEG = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "MISC_FILL", colsNVals);
                            if (rsltBLDEG.isSuccessful)
                            {
                                if (rsltBLDEG.dstResult.Tables.Count > 0)
                                {
                                    dtBldeg = rsltBLDEG.dstResult.Tables[0];
                                    if (dtBldeg.Rows.Count > 0)
                                    {

                                        txtDesig.Text = Convert.ToString(dtBldeg.Rows[0]["DGG"]);
                                        txtLevel.Text = Convert.ToString(dtBldeg.Rows[0]["LEVEL"]);
                                        txtBranch.Text = (dtBldeg.Rows[0]["BRNCHNAME"] == DBNull.Value) ? string.Empty : Convert.ToString(dtBldeg.Rows[0]["BRNCHNAME"]);
                                        txtEducation.Text = Convert.ToString(dtBldeg.Rows[0]["DGREE"]);
                                        txtGEID.Text = Convert.ToString(dtBldeg.Rows[0]["GEIDNO"]);
                                    }
                                    else
                                    {
                                        txtDesig.Text = "";
                                        txtLevel.Text = "";
                                        txtBranch.Text = "";
                                        txtGEID.Text = "";
                                        txtEducation.Text = "";

                                    }
                                }

                            }



                            rsltPrsnal = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNAL", colsNVals);
                            if (rsltPrsnal.isSuccessful)
                            {
                                if (rsltPrsnal.dstResult.Tables.Count > 0)
                                {
                                    dtPrsnal = rsltPrsnal.dstResult.Tables[0];
                                    if (dtPrsnal.Rows.Count > 0)
                                    {

                                        txtAddress.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ADD1"]);
                                        txtAddress2.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ADD2"]);
                                        txtPhoneNum.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_PHONE1"]);
                                        txtPhoneNum2.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_PHONE2"]);
                                        txtNICNum.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ID_CARD_NO"]);
                                        txtSex.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_SEX"]);
                                        txtDoB.Text = (dtPrsnal.Rows[0]["PR_D_BIRTH"]==DBNull.Value) ? ""  : Convert.ToDateTime(dtPrsnal.Rows[0]["PR_D_BIRTH"]).ToString("dd/MM/yyyy");
                                        txtMartStatus.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_MARITAL"]);
                                        txtMarriageDate.Text = (dtPrsnal.Rows[0]["PR_MARRIAGE"]== DBNull.Value) ? ""  : Convert.ToDateTime(dtPrsnal.Rows[0]["PR_MARRIAGE"]).ToString("dd/MM/yyyy");
                                        txtSpDoB.Text = (dtPrsnal.Rows[0]["PR_BIRTH_SP"]==DBNull.Value) ? ""  : Convert.ToDateTime(dtPrsnal.Rows[0]["PR_BIRTH_SP"]).ToString("dd/MM/yyyy");
                                        //txtDateofBirth.Text = Convert.ToDateTime(dtPrsnal.Rows[0]["PR_DATE_BIRTH"]).ToString("dd/MM/yyyy"); 
                                        //txtChildName.Text = txtChildName.Text + "   " + Convert.ToString(dtPrsnal.Rows[0]["PR_CHILD_NAME"]);
                                        txtNoofChild.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_NO_OF_CHILD"]);
                                        txtSpouseName.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_SPOUSE"]);

                                         rsltChild = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "CHILD", colsNVals);
                                        if (rsltChild.isSuccessful)
                                        {
                                            dtChild = rsltChild.dstResult.Tables[0];
                                            if (rsltChild.dstResult.Tables.Count > 0)
                                            {
                                                                     
                                                this.dgvChildren.DataSource = dtChild; // Convert.ToString(dtPrsnal.Rows[0]["PR_CHILD_NAME"]);
                                                this.dgvChildDB.DataSource = dtChild; // Convert.ToString(dtPrsnal.Rows[0]["PR_DATE_BIRTH"]);
                                            
                                            }
                                        }

                                     
                                    }
                                    else
                                    {
                                        txtAddress.Text = "";
                                        txtAddress2.Text = "";
                                        txtPhoneNum.Text = "";
                                        txtPhoneNum2.Text = "";
                                        txtNICNum.Text = "";
                                        txtSex.Text = "";
                                        txtDoB.Text = "";
                                        txtMartStatus.Text = "";
                                        txtMarriageDate.Text = "";
                                        txtSpDoB.Text = "";
                                        txtNoofChild.Text = "";
                                        txtSpouseName.Text = "";
                                        dgvChildDB.DataSource = null;
                                        dgvChildDB.Refresh();
                                        dgvChildren.DataSource = null;
                                        dgvChildren.Refresh();

                                    }
                                }

                            }

        
                        }

                    }

                }

            }
            else
            {
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtDesig.Text = "";
                txtBranch.Text = "";
                txtLevel.Text = "";
                txtGEID.Text = "";
                txtEducation.Text = "";
                txtFunctional.Text="";
                txtTitle.Text = "";
                txtJoinDate.Text = "";
                txtConfirm.Text = "";
                txtConfirmDate.Text = "";
                txtLastAppr.Text = "";
                txtNextAppr.Text = "";
                txtCategory.Text = "";
                txtAnnPkge.Text = "";
                txtAccNum.Text = "";
                txtNatTaxNum.Text = "";
                dgvSDC.Refresh();
                txtPersNo.Focus();
            }

            //-------------------------------------------------------------------//
            txtPersNo.Select();
            txtPersNo.Focus();  
        }

        private void CHRIS_Query_EmployeeCurrentStatusQuery_AfterLOVSelection(DataRow selectedRow, string actionType)
        {



            if (txtPersNo.Text != "")
            {
                //lbtnPNo.Focus();

                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtDesig.Text = "";
                txtBranch.Text = "";
                txtLevel.Text = "";
                txtGEID.Text = "";
                txtEducation.Text = "";
                txtFunctional.Text = "";
                txtTitle.Text = "";
                txtJoinDate.Text = "";
                txtConfirm.Text = "";
                txtConfirmDate.Text = "";
                txtLastAppr.Text = "";
                txtNextAppr.Text = "";
                txtCategory.Text = "";
                txtAnnPkge.Text = "";
                txtAccNum.Text = "";
                txtNatTaxNum.Text = "";
                slTxtLocation.Text = "";
                dgvSDC.Refresh();
                dgvChildDB.Refresh();
                dgvChildren.Refresh();
                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);
                rsltPrsnal = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PP_VERIFY", colsNVals);
                if (rsltPrsnal.isSuccessful)
                {
                    if (rsltPrsnal.dstResult.Tables.Count > 0 && rsltPrsnal.dstResult.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("YOU HAVE ENTERED AN INVALID NUMBER");
                        txtPersNo.Focus();
                        return;

                    }
                }


                //------------------------ FETCHING VALUES ---------------------------//

                this.colsNVals.Clear();
                this.colsNVals.Add("PR_P_NO", txtPersNo.Text);

                rsltEmp = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNL_LIST", colsNVals);

                if (rsltEmp.isSuccessful)
                {
                    //MessageBox.Show("count of Month =   " + rsltEmp.dstResult.Tables.Count);
                    if (rsltEmp.dstResult.Tables.Count > 0)
                    {
                        dtPrsnl = rsltEmp.dstResult.Tables[0];
                        if (dtPrsnl.Rows.Count > 0)
                        {

                            txtFirstName.Text = "";
                            txtLastName.Text = "";

                            txtFirstName.Text = (dtPrsnl.Rows[0]["PR_FIRST_NAME"] == null) ? string.Empty : Convert.ToString(dtPrsnl.Rows[0]["PR_FIRST_NAME"]);
                            txtLastName.Text = (dtPrsnl.Rows[0]["PR_LAST_NAME"] == null) ? string.Empty : Convert.ToString(dtPrsnl.Rows[0]["PR_LAST_NAME"]);
                            txtFunctional.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FUNC_TITTLE1"]);
                            txtTitle.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FUNC_TITTLE2"]);
                            txtJoinDate.Text = dtPrsnl.Rows[0]["PR_JOINING_DATE"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_JOINING_DATE"]).ToString("dd/MM/yyyy");
                            txtConfirm.Text = dtPrsnl.Rows[0]["PR_CONFIRM"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_CONFIRM"]).ToString("dd/MM/yyyy");
                            txtConfirmDate.Text = dtPrsnl.Rows[0]["PR_CONFIRM_ON"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_CONFIRM_ON"]).ToString("dd/MM/yyyy");
                            txtLastAppr.Text = dtPrsnl.Rows[0]["PR_LAST_INCREMENT"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_LAST_INCREMENT"]).ToString("dd/MM/yyyy");
                            txtNextAppr.Text = dtPrsnl.Rows[0]["PR_NEXT_INCREMENT"] == DBNull.Value ? "" : Convert.ToDateTime(dtPrsnl.Rows[0]["PR_NEXT_INCREMENT"]).ToString("dd/MM/yyyy");
                            txtCategory.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_CATEGORY"]);
                            txtAnnPkge.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_NEW_ANNUAL_PACK"]);
                            txtAccNum.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_ACCOUNT_NO"]);
                            txtNatTaxNum.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_NATIONAL_TAX"]);
                            slTxtLocation.Text = Convert.ToString(dtPrsnl.Rows[0]["LOCATION"]);


                            rsltDptCnt = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "DPTCONT_GRID", colsNVals);
                            if (rsltDptCnt.dstResult.Tables.Count > 0)
                            {
                                dtDptCnt = rsltDptCnt.dstResult.Tables[0];
                                this.dgvSDC.DataSource = dtDptCnt;
                            }


                            rsltBLDEG = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "MISC_FILL", colsNVals);
                            if (rsltBLDEG.isSuccessful)
                            {
                                if (rsltBLDEG.dstResult.Tables.Count > 0)
                                {
                                    dtBldeg = rsltBLDEG.dstResult.Tables[0];
                                    if (dtBldeg.Rows.Count > 0)
                                    {

                                        txtDesig.Text = Convert.ToString(dtBldeg.Rows[0]["DGG"]);
                                        txtLevel.Text = Convert.ToString(dtBldeg.Rows[0]["LEVEL"]);
                                        txtBranch.Text = (dtBldeg.Rows[0]["BRNCHNAME"] == "") ? string.Empty : Convert.ToString(dtBldeg.Rows[0]["BRNCHNAME"]);
                                        txtEducation.Text = Convert.ToString(dtBldeg.Rows[0]["DGREE"]);
                                        txtGEID.Text = Convert.ToString(dtBldeg.Rows[0]["GEIDNO"]);
                                    }
                                    else
                                    {
                                        txtDesig.Text = "";
                                        txtLevel.Text = "";
                                        txtBranch.Text = "";
                                        txtGEID.Text = "";
                                        txtEducation.Text = "";

                                    }
                                }

                            }



                            rsltPrsnal = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNAL", colsNVals);
                            if (rsltPrsnal.isSuccessful)
                            {
                                if (rsltPrsnal.dstResult.Tables.Count > 0)
                                {
                                    dtPrsnal = rsltPrsnal.dstResult.Tables[0];
                                    if (dtPrsnal.Rows.Count > 0)
                                    {

                                        txtAddress.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ADD1"]);
                                        txtAddress2.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ADD2"]);
                                        txtPhoneNum.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_PHONE1"]);
                                        txtPhoneNum2.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_PHONE2"]);
                                        txtNICNum.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_ID_CARD_NO"]);
                                        txtSex.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_SEX"]);
                                        txtDoB.Text = (dtPrsnal.Rows[0]["PR_D_BIRTH"] == DBNull.Value) ? "" : Convert.ToDateTime(dtPrsnal.Rows[0]["PR_D_BIRTH"]).ToString("dd/MM/yyyy");
                                        txtMartStatus.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_MARITAL"]);
                                        txtMarriageDate.Text = (dtPrsnal.Rows[0]["PR_MARRIAGE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtPrsnal.Rows[0]["PR_MARRIAGE"]).ToString("dd/MM/yyyy");
                                        txtSpDoB.Text = (dtPrsnal.Rows[0]["PR_BIRTH_SP"] == DBNull.Value) ? "" : Convert.ToDateTime(dtPrsnal.Rows[0]["PR_BIRTH_SP"]).ToString("dd/MM/yyyy");
                                        //txtDateofBirth.Text = Convert.ToDateTime(dtPrsnal.Rows[0]["PR_DATE_BIRTH"]).ToString("dd/MM/yyyy"); 
                                        //txtChildName.Text = txtChildName.Text + "   " + Convert.ToString(dtPrsnal.Rows[0]["PR_CHILD_NAME"]);
                                        txtNoofChild.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_NO_OF_CHILD"]);
                                        txtSpouseName.Text = Convert.ToString(dtPrsnal.Rows[0]["PR_SPOUSE"]);


                                        rsltChild = cmnDM.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "CHILD", colsNVals);
                                        if (rsltChild.isSuccessful)
                                        {
                                            dtChild = rsltChild.dstResult.Tables[0];
                                            if (rsltChild.dstResult.Tables.Count > 0)
                                            {

                                                this.dgvChildren.DataSource = dtChild; // Convert.ToString(dtPrsnal.Rows[0]["PR_CHILD_NAME"]);
                                                this.dgvChildDB.DataSource = dtChild; // Convert.ToString(dtPrsnal.Rows[0]["PR_DATE_BIRTH"]);

                                            }
                                        }
                                    }
                                    else
                                    {
                                        txtAddress.Text = "";
                                        txtAddress2.Text = "";
                                        txtPhoneNum.Text = "";
                                        txtPhoneNum2.Text = "";
                                        txtNICNum.Text = "";
                                        txtSex.Text = "";
                                        txtDoB.Text = "";
                                        txtMartStatus.Text = "";
                                        txtMarriageDate.Text = "";
                                        txtSpDoB.Text = "";
                                        txtNoofChild.Text = "";
                                        txtSpouseName.Text = "";
                                        dgvChildDB.DataSource = null;
                                        dgvChildDB.Refresh();
                                        dgvChildren.DataSource = null;
                                        dgvChildren.Refresh();

                                    }
                                }

                            }


                        }

                    }

                }

            }
            else
            {
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtDesig.Text = "";
                txtBranch.Text = "";
                txtLevel.Text = "";
                txtGEID.Text = "";
                txtEducation.Text = "";
                txtFunctional.Text = "";
                txtTitle.Text = "";
                txtJoinDate.Text = "";
                txtConfirm.Text = "";
                txtConfirmDate.Text = "";
                txtLastAppr.Text = "";
                txtNextAppr.Text = "";
                txtCategory.Text = "";
                txtAnnPkge.Text = "";
                txtAccNum.Text = "";
                txtNatTaxNum.Text = "";
                dgvSDC.Refresh();
                txtPersNo.Focus();
            }
            txtPersNo.Select();
            txtPersNo.Focus();  

            //-------------------------------------------------------------------//










        }

      




    }
}