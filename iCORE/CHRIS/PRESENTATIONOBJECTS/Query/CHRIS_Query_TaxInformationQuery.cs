using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_TaxInformationQuery : ChrisMasterDetailForm 
    {

        Dictionary<string, object> colsNVals = new Dictionary<string, object>();
        Result rslt,rsltDtl;
        CmnDataManager cmnDM = new CmnDataManager();

        DataTable dtDTaxPaid = new DataTable();
        DataTable dtPersnl = new DataTable();
        DataTable dtPyrlDate = new DataTable();
        DataTable dtTaxQuery = new DataTable();

        public CHRIS_Query_TaxInformationQuery()
        {
            InitializeComponent();
        }


        public CHRIS_Query_TaxInformationQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
     
            InitializeComponent();
            txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;
            //txtPersNo.Focus();

            dgvDT.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            dgvTDASR.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 
            
     
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            this.FunctionConfig.F3 = Function.View;
            this.FunctionConfig.F7 = Function.Modify;
         
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;
       

        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtDelete.Visible = false;
            tbtAdd.Visible = false;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;//Function.Cancel;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(pnlDetail.Controls);
                txtMSG.Text = "";
                txtTEST1.Text = "";
                txtPersNo.Focus();
            }
        }

        protected override bool Quit()
        {
            foreach (Control tx in this.pnlDetail.Controls)
            {

                if (tx != null)
                    if (tx is SLTextBox)
                        if (tx.Text != string.Empty)
                            this.FunctionConfig.CurrentOption = Function.Query;

            }
            bool flag = false;
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            }

            return flag;
        }
        //F3//
        protected override bool View()
        {

            string frmYear = txtTaxPeriod.Text;
            if (txtPersNo.Text != "")
            {
                int proNum = Convert.ToInt32(txtPersNo.Text);
                CHRIS_Query_TaxInformationAllowance TaxInformationAllowanceDialog = new CHRIS_Query_TaxInformationAllowance(frmYear, proNum, txtPayrollDate.Text);
                TaxInformationAllowanceDialog.ShowDialog();

            }
            return false;
        }

        //F7//
        protected override bool Edit()
        {
            if (txtPersNo.Text !="")
            {
                int proNum = Convert.ToInt32(txtPersNo.Text);
                CHRIS_Query_TaxInformationIncome TaxInformationIncomeDialog = new CHRIS_Query_TaxInformationIncome(proNum);
                TaxInformationIncomeDialog.ShowDialog();
            }
            return false;
        }

        private void txtPersNo_Leave(object sender, EventArgs e)
        {
           
            //colsNVals.Clear();
            //if (txtPersNo.Text == "")
            //{
            //    MessageBox.Show("Press [F6] to Exit Tax Query.....Or Enter Personnel No.");
            //    txtPersNo.Focus();
            //    return;
            //}



            //if (txtPersNo.Text != "")
            //{

            //    colsNVals.Clear();
            //    colsNVals.Add("PR_P_NO", txtPersNo.Text);

            //    rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PP_VERIFY", colsNVals);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables[0].Rows.Count == 0 )
            //        {
            //            MessageBox.Show("Invalid Personnel No. Enter Try Again Or [F6] To Exit...");
            //            txtPersNo.Focus();
            //            return;

            //        }
            //    }


            //    //populating Personnel FULL NAME//
            //    rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRSNL_LIST", colsNVals);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables.Count > 0)
            //        {
            //            dtPersnl = rslt.dstResult.Tables[0];

            //        }
            //        if (dtPersnl.Rows.Count > 0)
            //        {
            //            txtFirstName.Text = Convert.ToString(dtPersnl.Rows[0]["FIRSTNAME"]);
            //            txtLastName.Text = Convert.ToString(dtPersnl.Rows[0]["LASTNAME"]);
            //        }
            //        else
            //        {
            //            txtFirstName.Text = "";
            //            txtLastName.Text = "";
            //        }

            //    }
            //    ///populating Payroll Date///
            //    rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_DATE");
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            dtPyrlDate = rslt.dstResult.Tables[0];

            //        }
            //        if (dtPyrlDate.Rows.Count > 0)
            //        {
            //            txtPayrollDate.Text = Convert.ToString(dtPyrlDate.Rows[0]["PA_DATE"]);
            //        }
            //        else
            //        {
            //            txtPayrollDate.Text = "";
            //            MessageBox.Show("No Payroll Is Generated");
            //            return;
            //        }

            //    }

            //    /// populating DATE-TAX PAID grid ///
            //    if (txtTaxPeriod.Text != "" && txtTaxPeriod2.Text != "")
            //    {
                   
            //        colsNVals.Clear();
            //        colsNVals.Add("PR_P_NO", txtPersNo.Text);
            //        colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text);
            //        colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);

            //        rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRLDTAX_FILL", colsNVals);
            //        if (rslt.isSuccessful)
            //        {
            //            if (rslt.dstResult.Tables.Count > 0)
            //            {
            //                dtDTaxPaid = rslt.dstResult.Tables[0];
            //                this.dgvDT.DataSource = dtDTaxPaid;
            //            }
            //            else
            //            {
            //                this.dgvDT.DataSource = null;
            //                this.dgvDT.Refresh();
            //            }
            //        }


            //       //--------------CALLING THE SAME PROCEDURE @ PERSONNEL # CHANGE-----------//
                    
            //        colsNVals.Clear();
            //        colsNVals.Add("PR_P_NO", txtPersNo.Text);
            //        colsNVals.Add("W_SYS", txtPayrollDate.Text);
            //        colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text); //year//
            //        ///colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);
                                     

            //        //CALLING THE MAIN SP - VALID_PNO//
            //        rsltDtl = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_VALID_PNO", "", colsNVals);
            //        if (rsltDtl.isSuccessful)
            //        {
            //            if (rsltDtl.dstResult.Tables.Count > 0)
            //            {
            //                dtTaxQuery = rsltDtl.dstResult.Tables[0];

            //                txtTaxAllow.Text = "0";
            //                txtSalArear.Text = "0";
            //                txtEducation.Text = "0";
            //                txtForcastAll.Text = "0";
            //                txtTOTCost.Text = "0";
            //                txtAccNum.Text = "0";
            //                //txtNatTaxNum.Text = "0";

            //                if (dtTaxQuery.Rows.Count > 0)
            //                {

            //                    txtDesig.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]);
            //                    txtLevel.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_LEVEL"]);
            //                    txtCategory.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_CATE"]);
            //                    txtIncrDate.Text = (dtTaxQuery.Rows[0]["W_INC_EFF"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_INC_EFF"]).ToString("dd/MM/yyyy");
            //                    txtPromoDate.Text = (dtTaxQuery.Rows[0]["W_PR_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_PR_DATE"]).ToString("dd/MM/yyyy");
            //                    txtJoinDate.Text = (dtTaxQuery.Rows[0]["W_JOIN_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_JOIN_DATE"]).ToString("dd/MM/yyyy");
                                
            //                    //txtAnnPkge.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AN_PACK"]);
            //                    txtAnnPkge.Text = (dtTaxQuery.Rows[0]["W_AN_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_AN_PACK"])).ToString();
                                
            //                    //txtAnnBasic.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"]);
            //                    txtAnnBasic.Text = (dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"])).ToString();

            //                    txtRVPTrans.Text = "0";
                                
            //                    txtTotal.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TOTAL"]);

            //                    //txtFunctional.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PACK"]);
            //                    txtFunctional.Text = (dtTaxQuery.Rows[0]["W_TAX_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_TAX_PACK"])).ToString();


            //                    txtAccNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["SUBS_LOAN_TAXAMT"]);

            //                    txtNatTaxNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_GOVT_DISP"]);

            //                    txtOTCost.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ACTUAL_OT"]);
                                
            //                    txtAvgOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AVG"]);
                                                                         
            //                    txtOTArear.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AREARS"]);

            //                    txtForcastMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_MONTHS"]);

            //                    txtForcastOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_OT"]);

            //                    txtAvMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_OT_MONTHS"]);

            //                    //txtGEID.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_10_BONUS"]);
            //                    txtGEID.Text = (dtTaxQuery.Rows[0]["W_10_BONUS"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_10_BONUS"])).ToString();

            //                    txtTaxInc.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAXABLE_INC"]);


            //                    txtAnnTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_A_TAX"]);

            //                    txtTaxRfnd.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_REFUND"]);
                                
            //                    //txtTotalIncome.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_INCOME"]);
            //                    txtTotalIncome.Text = (dtTaxQuery.Rows[0]["W_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_INCOME"])).ToString();

            //                    txtTaxPaidUpdate.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PAID"]);
            //                    txtCurrMntTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ITAX"]);
            //                    txtMSG.Text = Convert.ToString(dtTaxQuery.Rows[0]["MSG"]);
            //                    txtTEST1.Text = Convert.ToString(dtTaxQuery.Rows[0]["TEST1"]);

            //                    this.dgvTDASR.DataSource = dtTaxQuery;


            //                }

            //            }
            //            else
            //            {
            //                txtDesig.Text = "";
            //                txtLevel.Text = "";
            //                txtCategory.Text = "";
            //                txtIncrDate.Text = "";
            //                txtPromoDate.Text = "";
            //                txtJoinDate.Text = "";
            //                txtAnnPkge.Text = "";
            //                txtAnnBasic.Text = "";
            //                txtRVPTrans.Text = "";
            //                txtTaxAllow.Text = "";
            //                txtTotal.Text = "";
            //                txtSalArear.Text = "";
            //                txtFunctional.Text = "";
            //                txtNatTaxNum.Text = "";
            //                txtAccNum.Text = "";
            //                txtEducation.Text = "";
            //                txtOTCost.Text = "";
            //                txtAvgOT.Text = "";
            //                txtOTArear.Text = "";
            //                txtForcastOT.Text = "";
            //                txtTOTCost.Text = "";
            //                txtAvMnt.Text = "";
            //                txtForcastAll.Text = "";
            //                txtTaxInc.Text = "";
            //                txtAnnTax.Text = "";
            //                txtTaxRfnd.Text = "";
            //                txtTotalIncome.Text = "";
            //                txtTaxPaidUpdate.Text = "";
            //                txtCurrMntTax.Text = "";
            //                txtMSG.Text = "";
            //                txtTEST1.Text = "";

            //                this.dgvTDASR.DataSource = null;
            //                this.dgvTDASR.Refresh();
            //            }
            //        }

            //     //--------------------------- END OF CALL ------------------------------//



            //    }

            //    txtPayrollDate.Focus();

            //}


        }

        private void txtTaxPeriod_Leave(object sender, EventArgs e)
        {

            //if (txtTaxPeriod.Text != "" && txtTaxPeriod2.Text != "")
            //{
            //    DataTable dtDTaxPaid = new DataTable();
            //    DataTable dtTaxQuery = new DataTable();

            //    colsNVals.Clear();
            //    colsNVals.Add("PR_P_NO", txtPersNo.Text);
            //    colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text);
            //    colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);
               
            //    rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRLDTAX_FILL", colsNVals);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables.Count > 0)
            //        {
            //            dtDTaxPaid = rslt.dstResult.Tables[0];
            //            this.dgvDT.DataSource = dtDTaxPaid;
            //        }
            //        else
            //        {
            //            this.dgvDT.DataSource = null;
            //            this.dgvDT.Refresh();
            //        }
            //    }

            //    colsNVals.Clear();
            //    colsNVals.Add("PR_P_NO", txtPersNo.Text);
            //    colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text);
            //    colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);
            //    colsNVals.Add("PAYROLL_DATE", txtPayrollDate.Text);

            //    rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "ASR_FILL", colsNVals);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables.Count > 0)
            //        {
            //            this.dgvTDASR.DataSource = rslt.dstResult.Tables[0];
            //        }
            //        else
            //        {
            //            this.dgvTDASR.DataSource = null;
            //            this.dgvTDASR.Refresh();
            //        }
            //    }

            //    colsNVals.Clear();
            //    colsNVals.Add("PR_P_NO", txtPersNo.Text);
            //    colsNVals.Add("W_SYS", txtPayrollDate.Text);
            //    colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text); //year//

            //    //CALLING THE MAIN SP - VALID_PNO//
            //    rsltDtl = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_VALID_PNO", "", colsNVals);
            //    if (rsltDtl.isSuccessful)
            //    {
            //        if (rsltDtl.dstResult.Tables.Count > 0)
            //        {
            //            dtTaxQuery = rsltDtl.dstResult.Tables[0];

            //            txtTaxAllow.Text = "0";
            //            txtSalArear.Text = "0";
            //            txtEducation.Text = "0";
            //            txtForcastAll.Text = "0";
            //            txtTOTCost.Text = "0";
            //            txtAccNum.Text = "0";
            //            //txtNatTaxNum.Text = "0";


            //            if (dtTaxQuery.Rows.Count > 0)
            //            {

            //                txtDesig.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]);
            //                txtLevel.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_LEVEL"]);
            //                txtCategory.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_CATE"]);
            //                txtIncrDate.Text = (dtTaxQuery.Rows[0]["W_INC_EFF"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_INC_EFF"]).ToString("dd/MM/yyyy");
            //                txtPromoDate.Text = (dtTaxQuery.Rows[0]["W_PR_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_PR_DATE"]).ToString("dd/MM/yyyy");
            //                txtJoinDate.Text = (dtTaxQuery.Rows[0]["W_JOIN_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_JOIN_DATE"]).ToString("dd/MM/yyyy");

            //                //txtAnnPkge.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AN_PACK"]);
            //                txtAnnPkge.Text = (dtTaxQuery.Rows[0]["W_AN_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_AN_PACK"])).ToString();

            //                //txtAnnBasic.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"]);
            //                txtAnnBasic.Text = (dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"])).ToString();

                            
            //                //txtRVPTrans.Text = dtTaxQuery.Rows[0]["W_VP"]==null? "0" : Convert.ToString(dtTaxQuery.Rows[0]["W_VP"]);
            //                txtRVPTrans.Text = "0";

            //                //txtGEID.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_10_BONUS"]);
            //                txtGEID.Text = (dtTaxQuery.Rows[0]["W_10_BONUS"]== DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_10_BONUS"])).ToString();


            //                txtTotal.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TOTAL"]);

            //                txtFunctional.Text = (dtTaxQuery.Rows[0]["W_TAX_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_TAX_PACK"])).ToString();
            //                //txtFunctional.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PACK"]);
                            
            //                txtAccNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["SUBS_LOAN_TAXAMT"]);

            //                txtOTCost.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ACTUAL_OT"]);
                          
                 
            //                txtAvgOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AVG"]);
                            
                                                       
            //                txtOTArear.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AREARS"]);

            //                txtForcastMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_MONTHS"]);
            //                txtForcastOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_OT"]);

            //                txtAvMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_OT_MONTHS"]);

            //                txtNatTaxNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_GOVT_DISP"]);
                          
            //                txtTaxInc.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAXABLE_INC"]);
            //                txtAnnTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_A_TAX"]);

            //                txtTaxRfnd.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_REFUND"]);
                            
            //                //txtTotalIncome.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_INCOME"]);
            //                txtTotalIncome.Text = (dtTaxQuery.Rows[0]["W_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_INCOME"])).ToString();

            //                txtTaxPaidUpdate.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PAID"]);
            //                txtCurrMntTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ITAX"]);
            //                txtMSG.Text = Convert.ToString(dtTaxQuery.Rows[0]["MSG"]);
            //                txtTEST1.Text = Convert.ToString(dtTaxQuery.Rows[0]["TEST1"]);

            //                //this.dgvTDASR.DataSource = dtTaxQuery;
            //            }

            //        }
            //        else
            //        {
            //            txtDesig.Text = "";
            //            txtLevel.Text = "";
            //            txtCategory.Text = "";
            //            txtIncrDate.Text = "";
            //            txtPromoDate.Text = "";
            //            txtJoinDate.Text = "";
            //            txtAnnPkge.Text = "";
            //            txtAnnBasic.Text = "";
            //            txtRVPTrans.Text = "";
            //            txtTaxAllow.Text = "";
            //            txtTotal.Text = "";
            //            txtSalArear.Text = "";
            //            txtFunctional.Text = "";
            //            txtNatTaxNum.Text = "";
            //            txtAccNum.Text = "";
            //            txtEducation.Text = "";
            //            txtOTCost.Text = "";
            //            txtAvgOT.Text = "";
            //            txtOTArear.Text = "";
            //            txtForcastOT.Text = "";
            //            txtTOTCost.Text = "";
            //            txtAvMnt.Text = "";
            //            txtForcastAll.Text = "";
            //            txtTaxInc.Text = "";
            //            txtAnnTax.Text = "";
            //            txtTaxRfnd.Text = "";
            //            txtTotalIncome.Text = "";
            //            txtTaxPaidUpdate.Text = "";
            //            txtCurrMntTax.Text = "";
            //            txtMSG.Text = "";
            //            txtTEST1.Text = "";

            //            this.dgvTDASR.DataSource = null;
            //            this.dgvTDASR.Refresh();
            //        }
            //    }



            //}

        }
        
        private void txtPersNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }

        private void txtPayrollDate_Leave(object sender, EventArgs e)
        {

            if (txtPersNo.Text != "" && txtPayrollDate.Text !="")
            {
                if (base.IsValidExpression(txtPayrollDate.Text, "\\d{2}/\\d{2}/\\d{4}") || base.IsValidExpression(txtPayrollDate.Text, "\\d{8}"))
                {
                    DateTime payrollDate;
                    if (base.IsValidExpression(txtPayrollDate.Text, "\\d{8}"))
                    {
                        payrollDate = new DateTime(Convert.ToInt16(txtPayrollDate.Text.Substring(4, 4)), Convert.ToInt16(txtPayrollDate.Text.Substring(2, 2)), Convert.ToInt16(txtPayrollDate.Text.Substring(0, 2)));
                        DateTime.TryParse(payrollDate.ToString("dd/MM/yyyy"), out payrollDate);
                    }
                    else
                    {
                        DateTime.TryParse(txtPayrollDate.Text, out payrollDate);
                    }

                    if (payrollDate.Equals(DateTime.MinValue))
                    {
                        MessageBox.Show("Enter Last Payroll Date (DD/MM/YYYY)");
                        txtPayrollDate.Focus();
                        txtPayrollDate.Select();
                        return;
                    }
                    else
                    {
                        txtPayrollDate.Text = payrollDate.ToString("dd/MM/yyyy");
                    }

                }
                else
                {
                    MessageBox.Show("Enter Last Payroll Date (DD/MM/YYYY)");
                    txtPayrollDate.Focus();
                    txtPayrollDate.Select();
                    return;
                }
                //string currYear = Convert.ToString(DateTime.Now.Year);
                //string lastYear = Convert.ToString(DateTime.Now.AddYears(-1).Year);
                
                int currYear = Convert.ToDateTime(txtPayrollDate.Text).Year;
                int payrollMonth = Convert.ToDateTime(txtPayrollDate.Text).Month;
                if (payrollMonth < 7)
                    currYear = currYear - 1;

                int nextYear = currYear + 1;

                txtTaxPeriod2.Text = nextYear.ToString();
                txtTaxPeriod.Text = currYear.ToString();


                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_MAX",colsNVals);

                if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "")
                {
                    MessageBox.Show("No Payroll is Generated");
                    return;
                }
                else
                {
                    dtTaxQuery = rslt.dstResult.Tables[0];
                    if (Convert.ToDateTime(txtPayrollDate.Text) > Convert.ToDateTime(dtTaxQuery.Rows[0]["W_SYS"]) )
                    {
                        MessageBox.Show("Last Payroll Was Generated On " + Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0]["W_SYS"]).Date.ToShortDateString());
                    }

                }

                colsNVals.Clear();
                colsNVals.Add("PAYROLL_DATE", txtPayrollDate.Text);
                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_VALID", colsNVals);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()=="0" )
                    {


                        rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_VOID", colsNVals);
                        if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "")
                        {
                            MessageBox.Show("Payroll Date Of The Given Month Is :  " + Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0]["Max_Payroll"]).Date.ToShortDateString());
                            CHRIS_Query_TaxInformationQuery objTaxInfo = new CHRIS_Query_TaxInformationQuery();

                            DialogResult result = MessageBox.Show("Close this Form ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                            if (result == DialogResult.Yes)
                            {
                                this.Close();
                            }
                            else if (result == DialogResult.No)
                            {
                                txtTaxPeriod.Focus();
                                return;
                            }

                        }
                    }
                 
                }


            }
            else
            {
                txtTaxPeriod.Text = "";
                txtTaxPeriod2.Text = "";
            }


            txtTaxPeriod.Focus();




        }

        private void txtPersNo_Validating(object sender, CancelEventArgs e)
        {
            colsNVals.Clear();
            if (txtPersNo.Text == "")
            {
                MessageBox.Show("Press [F6] to Exit Tax Query.....Or Enter Personnel No.");
                txtPersNo.Focus();
                return;
            }



            if (txtPersNo.Text != "")
            {

                txtDesig.Text = "";
                txtLevel.Text = "";
                txtCategory.Text = "";
                txtIncrDate.Text = "";
                txtPromoDate.Text = "";
                txtJoinDate.Text = "";
                txtAnnPkge.Text = "";
                txtAnnBasic.Text = "";
                txtRVPTrans.Text = "";
                txtTaxAllow.Text = "";
                txtTotal.Text = "";
                txtSalArear.Text = "";
                txtFunctional.Text = "";
                txtNatTaxNum.Text = "";
                txtAccNum.Text = "";
                txtEducation.Text = "";
                txtOTCost.Text = "";
                txtAvgOT.Text = "";
                txtOTArear.Text = "";
                txtForcastOT.Text = "";
                txtTOTCost.Text = "";
                txtAvMnt.Text = "";
                txtForcastAll.Text = "";
                txtTaxInc.Text = "";
                txtAnnTax.Text = "";
                txtTaxRfnd.Text = "";
                txtTotalIncome.Text = "";
                txtTaxPaidUpdate.Text = "";
                txtCurrMntTax.Text = "";
                txtMSG.Text = "";
                txtTEST1.Text = "";

                this.dgvTDASR.DataSource = null;
                this.dgvDT.DataSource = null;
                this.dgvTDASR.Refresh();
                this.dgvDT.Refresh();

                this.Refresh();

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);

                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PP_VERIFY", colsNVals);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("Invalid Personnel No. Enter Try Again Or [F6] To Exit...");
                        txtPersNo.Focus();
                        return;

                    }
                }


                //populating Personnel FULL NAME//
                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRSNL_LIST", colsNVals);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        dtPersnl = rslt.dstResult.Tables[0];

                    }
                    if (dtPersnl.Rows.Count > 0)
                    {
                        txtFirstName.Text = Convert.ToString(dtPersnl.Rows[0]["FIRSTNAME"]);
                        txtLastName.Text = Convert.ToString(dtPersnl.Rows[0]["LASTNAME"]);
                    }
                    else
                    {
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                    }

                }
                ///populating Payroll Date///
                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_DATE");
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dtPyrlDate = rslt.dstResult.Tables[0];

                    }
                    if (dtPyrlDate.Rows.Count > 0)
                    {
                        txtPayrollDate.Text = Convert.ToString(dtPyrlDate.Rows[0]["PA_DATE"]);
                    }
                    else
                    {
                        txtPayrollDate.Text = "";
                        MessageBox.Show("No Payroll Is Generated");
                        return;
                    }

                }

                /// populating DATE-TAX PAID grid ///
                if (txtTaxPeriod.Text != "" && txtTaxPeriod2.Text != "")
                {

                    colsNVals.Clear();
                    colsNVals.Add("PR_P_NO", txtPersNo.Text);
                    colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text);
                    colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);

                    rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRLDTAX_FILL", colsNVals);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            dtDTaxPaid = rslt.dstResult.Tables[0];
                            this.dgvDT.DataSource = dtDTaxPaid;
                        }
                        else
                        {
                            this.dgvDT.DataSource = null;
                            this.dgvDT.Refresh();
                        }
                    }


                    //--------------CALLING THE SAME PROCEDURE @ PERSONNEL # CHANGE-----------//

                    colsNVals.Clear();
                    colsNVals.Add("PR_P_NO", txtPersNo.Text);
                    colsNVals.Add("W_SYS", txtPayrollDate.Text);
                    colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text); //year//
                    ///colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);


                    //CALLING THE MAIN SP - VALID_PNO//
                    rsltDtl = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_VALID_PNO", "", colsNVals);
                    if (rsltDtl.isSuccessful)
                    {
                        if (rsltDtl.dstResult.Tables.Count > 0)
                        {
                            dtTaxQuery = rsltDtl.dstResult.Tables[0];

                            txtTaxAllow.Text = "0";
                            txtSalArear.Text = "0";
                            txtEducation.Text = "0";
                            txtForcastAll.Text = "0";
                            txtTOTCost.Text = "0";
                            txtAccNum.Text = "0";
                            //txtNatTaxNum.Text = "0";

                            if (dtTaxQuery.Rows.Count > 0)
                            {
                                txtEducation.Text = Convert.ToString(dtTaxQuery.Rows[0]["GLOBAL_W_INC_AMT"]);
                                txtDesig.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]);
                                txtLevel.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_LEVEL"]);
                                txtCategory.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_CATE"]);
                                txtIncrDate.Text = (dtTaxQuery.Rows[0]["W_INC_EFF"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_INC_EFF"]).ToString("dd/MM/yyyy");
                                txtPromoDate.Text = (dtTaxQuery.Rows[0]["W_PR_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_PR_DATE"]).ToString("dd/MM/yyyy");
                                txtJoinDate.Text = (dtTaxQuery.Rows[0]["W_JOIN_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_JOIN_DATE"]).ToString("dd/MM/yyyy");

                                //txtAnnPkge.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AN_PACK"]);
                                txtAnnPkge.Text = (dtTaxQuery.Rows[0]["W_AN_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_AN_PACK"])).ToString();

                                //txtAnnBasic.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"]);
                                //txtAnnBasic.Text = (dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"])).ToString();
                                txtAnnBasic.Text = (dtTaxQuery.Rows[0]["blk_W_ANNUAL_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["blk_W_ANNUAL_INCOME"])).ToString();

                                txtRVPTrans.Text = "0";

                                txtTotal.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TOTAL"]);

                                //txtFunctional.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PACK"]);
                                txtFunctional.Text = (dtTaxQuery.Rows[0]["W_TAX_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_TAX_PACK"])).ToString();


                                txtAccNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["SUBS_LOAN_TAXAMT"]);

                                txtNatTaxNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_GOVT_DISP"]);

                                txtOTCost.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ACTUAL_OT"]);

                                txtAvgOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AVG"]);

                                txtOTArear.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AREARS"]);

                                txtForcastMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_MONTHS"]);

                                txtForcastOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_OT"]);

                                txtAvMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_OT_MONTHS"]);

                                //txtGEID.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_10_BONUS"]);
                                txtGEID.Text = (dtTaxQuery.Rows[0]["W_10_BONUS"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_10_BONUS"])).ToString();
                                if (Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]).ToUpper() == "VP" || Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]).ToUpper() == "RVP")
                                {
                                    txtGEID.Text = Convert.ToString(dtTaxQuery.Rows[0]["w_gha_forcast"]);
                                }
                                txtTaxInc.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAXABLE_INC"]);


                                txtAnnTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_A_TAX"]);

                                txtTaxRfnd.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_REFUND"]);

                                //txtTotalIncome.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_INCOME"]);
                                txtTotalIncome.Text = (dtTaxQuery.Rows[0]["W_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_INCOME"])).ToString();

                                txtTaxPaidUpdate.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PAID"]);
                                txtCurrMntTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ITAX"]);
                                txtMSG.Text = Convert.ToString(dtTaxQuery.Rows[0]["MSG"]);
                                txtTEST1.Text = Convert.ToString(dtTaxQuery.Rows[0]["TEST1"]);
                                txtForcastAll.Text = Convert.ToString(dtTaxQuery.Rows[0]["w_forcast_all"]); 
                                //this.dgvTDASR.DataSource = dtTaxQuery;


                            }

                        }
                        else
                        {
                            txtDesig.Text = "";
                            txtLevel.Text = "";
                            txtCategory.Text = "";
                            txtIncrDate.Text = "";
                            txtPromoDate.Text = "";
                            txtJoinDate.Text = "";
                            txtAnnPkge.Text = "";
                            txtAnnBasic.Text = "";
                            txtRVPTrans.Text = "";
                            txtTaxAllow.Text = "";
                            txtTotal.Text = "";
                            txtSalArear.Text = "";
                            txtFunctional.Text = "";
                            txtNatTaxNum.Text = "";
                            txtAccNum.Text = "";
                            txtEducation.Text = "";
                            txtOTCost.Text = "";
                            txtAvgOT.Text = "";
                            txtOTArear.Text = "";
                            txtForcastOT.Text = "";
                            txtTOTCost.Text = "";
                            txtAvMnt.Text = "";
                            txtForcastAll.Text = "";
                            txtTaxInc.Text = "";
                            txtAnnTax.Text = "";
                            txtTaxRfnd.Text = "";
                            txtTotalIncome.Text = "";
                            txtTaxPaidUpdate.Text = "";
                            txtCurrMntTax.Text = "";
                            txtMSG.Text = "";
                            txtTEST1.Text = "";

                            this.dgvTDASR.DataSource = null;
                            this.dgvTDASR.Refresh();
                        }
                    }

                    //--------------------------- END OF CALL ------------------------------//



                }

                txtPayrollDate.Focus();

            }

        }

        private void txtTaxPeriod_Validating(object sender, CancelEventArgs e)
        {
            if (!txtTaxPeriod.Text.Equals(String.Empty))
            {
                if(!IsValidExpression(txtTaxPeriod.Text, "\\d{4}"))
                {
                    MessageBox.Show("Please type a valid year");
                    e.Cancel = true;
                }
            }
        }

        private void txtTaxPeriod2_Validating(object sender, CancelEventArgs e)
        {
            if (!txtTaxPeriod2.Text.Equals(String.Empty))
            {
                if (!IsValidExpression(txtTaxPeriod2.Text, "\\d{4}"))
                {
                    MessageBox.Show("Please type a valid year");
                    e.Cancel = true;
                }
            }

        }

        private void txtTaxPeriod2_Leave(object sender, EventArgs e)
        {
            if (txtTaxPeriod.Text != "" && txtTaxPeriod2.Text != "")
            {


                DataTable dtDTaxPaid = new DataTable();
                DataTable dtTaxQuery = new DataTable();

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);
                colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text);
                colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);

                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRLDTAX_FILL", colsNVals);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        dtDTaxPaid = rslt.dstResult.Tables[0];
                        this.dgvDT.DataSource = dtDTaxPaid;
                    }
                    else
                    {
                        this.dgvDT.DataSource = null;
                        this.dgvDT.Refresh();
                    }
                }

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);
                colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text);
                colsNVals.Add("W_TAX_TO", txtTaxPeriod2.Text);
                colsNVals.Add("PAYROLL_DATE", txtPayrollDate.Text);

                rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "ASR_FILL", colsNVals);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        this.dgvTDASR.DataSource = rslt.dstResult.Tables[0];
                    }
                    else
                    {
                        this.dgvTDASR.DataSource = null;
                        this.dgvTDASR.Refresh();
                    }
                }

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);
                colsNVals.Add("W_SYS", txtPayrollDate.Text);
                colsNVals.Add("W_TAX_FROM", txtTaxPeriod.Text); //year//
                

                //CALLING THE MAIN SP - VALID_PNO//
                rsltDtl = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_VALID_PNO", "", colsNVals);
                if (rsltDtl.isSuccessful)
                {
                    if (rsltDtl.dstResult.Tables.Count > 0)
                    {
                        dtTaxQuery = rsltDtl.dstResult.Tables[0];
                        
                        txtTaxAllow.Text = "0";
                        txtSalArear.Text = "0";
                        txtEducation.Text = "0";
                        txtForcastAll.Text = "0";
                        txtTOTCost.Text = "0";
                        txtAccNum.Text = "0";
                        //txtNatTaxNum.Text = "0";


                        if (dtTaxQuery.Rows.Count > 0)
                        {
                            txtSalArear.Text = Convert.ToString(dtTaxQuery.Rows[0]["w_sal_arear"]);
                            txtEducation.Text =  Convert.ToString(dtTaxQuery.Rows[0]["GLOBAL_W_INC_AMT"]);
                            txtDesig.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]);
                            txtLevel.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_LEVEL"]);
                            txtCategory.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_CATE"]);
                            txtIncrDate.Text = (dtTaxQuery.Rows[0]["W_INC_EFF"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_INC_EFF"]).ToString("dd/MM/yyyy");
                            txtPromoDate.Text = (dtTaxQuery.Rows[0]["W_PR_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_PR_DATE"]).ToString("dd/MM/yyyy");
                            txtJoinDate.Text = (dtTaxQuery.Rows[0]["W_JOIN_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_JOIN_DATE"]).ToString("dd/MM/yyyy");

                            //txtAnnPkge.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AN_PACK"]);
                            txtAnnPkge.Text = (dtTaxQuery.Rows[0]["W_AN_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_AN_PACK"])).ToString();

                            //txtAnnBasic.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"]);
                            //txtAnnBasic.Text = (dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"])).ToString();

                            txtAnnBasic.Text = (dtTaxQuery.Rows[0]["blk_w_annual_income"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["blk_w_annual_income"])).ToString();


                            //txtRVPTrans.Text = dtTaxQuery.Rows[0]["W_VP"]==null? "0" : Convert.ToString(dtTaxQuery.Rows[0]["W_VP"]);
                            txtRVPTrans.Text = "0";

                            //txtGEID.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_10_BONUS"]);
                            txtGEID.Text = (dtTaxQuery.Rows[0]["W_10_BONUS"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_10_BONUS"])).ToString();
                            if (Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]).ToUpper() == "VP" || Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]).ToUpper() == "RVP")
                            {
                                txtGEID.Text = Convert.ToString(dtTaxQuery.Rows[0]["w_gha_forcast"]);
                            }

                            txtTotal.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TOTAL"]);

                            txtFunctional.Text = (dtTaxQuery.Rows[0]["W_TAX_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_TAX_PACK"])).ToString();
                            //txtFunctional.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PACK"]);

                            txtAccNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["SUBS_LOAN_TAXAMT"]);

                            txtOTCost.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ACTUAL_OT"]);
                            txtTaxAllow.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_OTHER_ALL"]);

                            txtAvgOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AVG"]);


                            txtOTArear.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_AREARS"]);

                            txtForcastMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_MONTHS"]);
                            txtForcastOT.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_OT"]);

                            txtAvMnt.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_OT_MONTHS"]);

                            txtNatTaxNum.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_GOVT_DISP"]);

                            txtTaxInc.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAXABLE_INC"]);
                            txtAnnTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_A_TAX"]);

                            txtTaxRfnd.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_REFUND"]);

                            //txtTotalIncome.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_INCOME"]);
                            txtTotalIncome.Text = (dtTaxQuery.Rows[0]["W_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_INCOME"])).ToString();

                            txtTaxPaidUpdate.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PAID"]);
                            txtCurrMntTax.Text = Convert.ToString(dtTaxQuery.Rows[0]["W_ITAX"]);
                            txtMSG.Text = Convert.ToString(dtTaxQuery.Rows[0]["MSG"]);
                            txtTEST1.Text = Convert.ToString(dtTaxQuery.Rows[0]["TEST1"]);
                            txtForcastAll.Text = Convert.ToString(dtTaxQuery.Rows[0]["w_forcast_all"]); 

                            //this.dgvTDASR.DataSource = dtTaxQuery;
                        }

                    }
                    else
                    {
                        txtDesig.Text = "";
                        txtLevel.Text = "";
                        txtCategory.Text = "";
                        txtIncrDate.Text = "";
                        txtPromoDate.Text = "";
                        txtJoinDate.Text = "";
                        txtAnnPkge.Text = "";
                        txtAnnBasic.Text = "";
                        txtRVPTrans.Text = "";
                        txtTaxAllow.Text = "";
                        txtTotal.Text = "";
                        txtSalArear.Text = "";
                        txtFunctional.Text = "";
                        txtNatTaxNum.Text = "";
                        txtAccNum.Text = "";
                        txtEducation.Text = "";
                        txtOTCost.Text = "";
                        txtAvgOT.Text = "";
                        txtOTArear.Text = "";
                        txtForcastOT.Text = "";
                        txtTOTCost.Text = "";
                        txtAvMnt.Text = "";
                        txtForcastAll.Text = "";
                        txtTaxInc.Text = "";
                        txtAnnTax.Text = "";
                        txtTaxRfnd.Text = "";
                        txtTotalIncome.Text = "";
                        txtTaxPaidUpdate.Text = "";
                        txtCurrMntTax.Text = "";
                        txtMSG.Text = "";
                        txtTEST1.Text = "";

                        this.dgvTDASR.DataSource = null;
                        this.dgvTDASR.Refresh();
                    }
                }
            }
       }
    }
  }
