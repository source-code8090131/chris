namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_TaxInformationAllowance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_TaxInformationAllowance));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlAllowance = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvAllowance = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.W_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW_PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW_ALL_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlAllowance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllowance)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(550, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(586, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 282);
            this.panel1.Size = new System.Drawing.Size(586, 60);
            // 
            // pnlAllowance
            // 
            this.pnlAllowance.ConcurrentPanels = null;
            this.pnlAllowance.Controls.Add(this.dgvAllowance);
            this.pnlAllowance.DataManager = null;
            this.pnlAllowance.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlAllowance.DependentPanels = null;
            this.pnlAllowance.DisableDependentLoad = false;
            this.pnlAllowance.EnableDelete = true;
            this.pnlAllowance.EnableInsert = true;
            this.pnlAllowance.EnableQuery = false;
            this.pnlAllowance.EnableUpdate = true;
            this.pnlAllowance.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabQueryCommand";
            this.pnlAllowance.Location = new System.Drawing.Point(11, 39);
            this.pnlAllowance.MasterPanel = null;
            this.pnlAllowance.Name = "pnlAllowance";
            this.pnlAllowance.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlAllowance.Size = new System.Drawing.Size(562, 228);
            this.pnlAllowance.SPName = "CHRIS_SP_BONUS_TAB_MANAGER";
            this.pnlAllowance.TabIndex = 0;
            this.pnlAllowance.TabStop = true;
            // 
            // dgvAllowance
            // 
            this.dgvAllowance.AllowUserToAddRows = false;
            this.dgvAllowance.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAllowance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAllowance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllowance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.W_DESC,
            this.PW_PAY_DATE,
            this.PW_ALL_AMOUNT,
            this.PW_ACCOUNT});
            this.dgvAllowance.ColumnToHide = null;
            this.dgvAllowance.ColumnWidth = null;
            this.dgvAllowance.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAllowance.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvAllowance.DisplayColumnWrapper = null;
            this.dgvAllowance.GridDefaultRow = 0;
            this.dgvAllowance.Location = new System.Drawing.Point(14, 13);
            this.dgvAllowance.Name = "dgvAllowance";
            this.dgvAllowance.ReadOnly = true;
            this.dgvAllowance.ReadOnlyColumns = null;
            this.dgvAllowance.RequiredColumns = "";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAllowance.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvAllowance.Size = new System.Drawing.Size(536, 210);
            this.dgvAllowance.SkippingColumns = null;
            this.dgvAllowance.TabIndex = 0;
            // 
            // W_DESC
            // 
            this.W_DESC.DataPropertyName = "W_DESC";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.W_DESC.DefaultCellStyle = dataGridViewCellStyle2;
            this.W_DESC.HeaderText = "Allowance";
            this.W_DESC.MaxInputLength = 10;
            this.W_DESC.Name = "W_DESC";
            this.W_DESC.ReadOnly = true;
            this.W_DESC.Width = 200;
            // 
            // PW_PAY_DATE
            // 
            this.PW_PAY_DATE.DataPropertyName = "PW_PAY_DATE";
            this.PW_PAY_DATE.HeaderText = "Date";
            this.PW_PAY_DATE.Name = "PW_PAY_DATE";
            this.PW_PAY_DATE.ReadOnly = true;
            this.PW_PAY_DATE.Width = 90;
            // 
            // PW_ALL_AMOUNT
            // 
            this.PW_ALL_AMOUNT.DataPropertyName = "PW_ALL_AMOUNT";
            this.PW_ALL_AMOUNT.HeaderText = "Amount";
            this.PW_ALL_AMOUNT.Name = "PW_ALL_AMOUNT";
            this.PW_ALL_AMOUNT.ReadOnly = true;
            this.PW_ALL_AMOUNT.Width = 90;
            // 
            // PW_ACCOUNT
            // 
            this.PW_ACCOUNT.DataPropertyName = "PW_ACCOUNT";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.PW_ACCOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            this.PW_ACCOUNT.HeaderText = "Account";
            this.PW_ACCOUNT.Name = "PW_ACCOUNT";
            this.PW_ACCOUNT.ReadOnly = true;
            this.PW_ACCOUNT.Width = 110;
            // 
            // CHRIS_Query_TaxInformationAllowance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(586, 342);
            this.Controls.Add(this.pnlAllowance);
            this.Name = "CHRIS_Query_TaxInformationAllowance";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Tax Information Allowance";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlAllowance, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlAllowance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllowance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlAllowance;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvAllowance;
        private System.Windows.Forms.DataGridViewTextBoxColumn W_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_PAY_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_ALL_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_ACCOUNT;
    }
}