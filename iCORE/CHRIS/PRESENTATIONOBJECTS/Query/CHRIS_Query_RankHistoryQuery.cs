using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_RankHistoryQuery : ChrisMasterDetailForm
    {
        CmnDataManager cmnDm = new CmnDataManager();
        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();
            


        public CHRIS_Query_RankHistoryQuery()
        {
            InitializeComponent();
        }
        public CHRIS_Query_RankHistoryQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)

        {
            InitializeComponent();
        //List<SLPanel> lstDependentPanels = new List<SLPanel>();
        //lstDependentPanels.Add(PnlRank);
        //PnlPersonnel.DependentPanels = lstDependentPanels;
        //this.IndependentPanels.Add(PnlPersonnel);
        //this.CurrentPanelBlock = PnlPersonnel.Name;
            //if (FunctionConfig.CurrentOption == Function.Add)
            //{

            //}

            lblUserName.Text += "   " + this.UserName;

            this.txtOption.Visible = false;
            txtPrNo.Focus();
            stsOptions.Visible = false;

            slDataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 
             
               
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

           // txtOption.Visible = false;

         


            this.F1OptionText = "New Criteria";
            this.FunctionConfig.F1 = Function.View;
            this.FunctionConfig.F6 = Function.Cancel;
            
            //tbtSave.Enabled = false;
            tbtList.Enabled = true;
            tbtCancel.Enabled = true;
            //tbtDelete.Enabled = false;
          
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlRank);
            PnlPersonnel.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(PnlPersonnel);
            this.CurrentPanelBlock = PnlPersonnel.Name;
        }

        protected override bool Add()
        {
            txtPrNo.Select();
            txtPrNo.Focus();
            return base.Add();

            //bool focus = true;
            //this.txtPrNo.Focus();
            //return focus;
        }


        protected override bool View()
        {
            this.ClearForm(pnlBottom.Controls);
            this.ClearForm(PnlPersonnel.Controls);
            txtPrNo.Focus();
            return false;
        }


        protected override bool Cancel()
        {
            this.Close();
            return true;
        }


        protected void getDesig()
        {
            
            param.Clear();
            param.Add("PR_P_NO", txtPrNo.Text);
            rslt = cmnDm.GetData("CHRIS_SP_QUERY_RANK_PERSONNEL_PERSONNEL_MANAGER", "GET_DESIG_LEVEL",param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.txtDesig.Text = rslt.dstResult.Tables[0].Rows[0]["PR_DESIG"].ToString();
                    this.txtLevel.Text = rslt.dstResult.Tables[0].Rows[0]["PR_LEVEL"].ToString();
                
                
                }
            }
        }

        private void CHRIS_Query_RankHistoryQuery_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            this.getDesig();
            PnlPersonnel.LoadDependentPanels();
        }

        
        
        private void txtPrNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "List")
            {
                lbtnPersonel.PerformClick();  
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                this.ClearForm(PnlPersonnel.Controls);
                this.ClearForm(PnlRank.Controls);
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(PnlPersonnel.Controls);
                this.ClearForm(PnlRank.Controls);
                txtPrNo.Focus();
                
            }
        }

        private void txtPrNo_Leave(object sender, EventArgs e)
        {

            if (txtPrNo.Text != "")
            {
                param.Clear();
                param.Add("PR_P_NO", txtPrNo.Text);

                rslt = cmnDm.GetData("CHRIS_SP_QUERY_RANK_PERSONNEL_PERSONNEL_MANAGER", "PP_VERIFY", param);
                rslt = cmnDm.GetData("CHRIS_SP_QUERY_RANK_PERSONNEL_PERSONNELMANAGER", "PP_VERIFY", param);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                    {
                        MessageBox.Show("Invalid Personnel No.");
                        txtPrNo.Focus();
                        return;

                    }
                }
            }


        }
     

        //private void CHRIS_Query_DeptWiseQuery_BeforeLOVShown(iCORE.Common.PRESENTATIONOBJECTS.Cmn.Cmn_ListOfRecords LOV, string actionType)
        //{
        //    PersonnelSearchCommand ent = (PersonnelSearchCommand)this.PnlDepart.CurrentBusinessEntity;
        //    if (ent != null)
        //    {
        //        ent.W_BRANCH = this.txtBranchCode.Text;
        //    }
        //}

       
    }
}