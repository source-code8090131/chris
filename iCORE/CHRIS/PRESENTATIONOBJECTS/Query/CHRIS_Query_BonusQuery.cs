using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_BonusQuery : ChrisMasterDetailForm 
    {

        Dictionary<string, object> colsNVals = new Dictionary<string, object>();
        Result rsltCode;
        CmnDataManager cmnDM = new CmnDataManager();
        
        public CHRIS_Query_BonusQuery()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;
            txtPersNo.Focus();
            txtPersNo.Select();
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtDelete.Visible = false;
            txtPersNo.Focus();
            txtPersNo.Select();
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Cancel;
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(PnlDetail.Controls);
                this.ClearForm(pnlMaster.Controls);
                txtPersNo.Select();
                txtPersNo.Focus();
            }
             

        }





        public CHRIS_Query_BonusQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
     
            InitializeComponent();
            List<SLPanel> lstDtlPanel = new List<SLPanel>();
            lstDtlPanel.Add(PnlDetail);
            pnlMaster.DependentPanels = lstDtlPanel;
            
            this.IndependentPanels.Add(pnlMaster);
            this.txtOption.Visible = false;
            lblUserName.Text += "   " + this.UserName;

            txtPersNo.Focus();
            dgvBonusDetail.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 
            

        }

        private void txtPersNo_TextChanged(object sender, EventArgs e)
        {

            //colsNVals.Clear();
            //colsNVals.Add("PR_P_NO", this.txtPersNo.Text);
            ////dgvBonusDetail.Refresh(); 

            //DataTable dtFPH = new DataTable();
           
            //rsltCode = cmnDM.GetData("CHRIS_SP_BONUSQUERY_MANAGER", "PSN_INC_DSG", colsNVals);
            //if (rsltCode.isSuccessful)
            //{
            //    if (rsltCode.dstResult.Tables.Count > 0)
            //    {
            //        dtFPH = rsltCode.dstResult.Tables[0];

            //    }
            //    if (dtFPH.Rows.Count > 0)
            //    {

            //        txtDesig.Text = Convert.ToString(dtFPH.Rows[0]["DGG"]);
            //        txtLevel.Text= Convert.ToString(dtFPH.Rows[0]["LEVEL"]);
            //        txtBranch.Text= Convert.ToString(dtFPH.Rows[0]["BRANCH"]);
            //    }
            //    else
            //    {
            //        txtDesig.Text="";
            //        txtLevel.Text="";
            //        txtBranch.Text="";
                  
            //    }
            //}
            
        
        }

        private void txtPersNo_Leave(object sender, EventArgs e)
        {
            
            if (txtPersNo.Text != "")
            {
                txtLevel.Text = "";
                txtDesig.Text = "";
                txtBranch.Text = "";

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", txtPersNo.Text);

                rsltCode = cmnDM.GetData("CHRIS_SP_BONUSQUERY_MANAGER", "PP_VERIFY", colsNVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count == 0)
                    {
                        txtPersNo.Focus();
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        txtLevel.Text = "";
                        txtDesig.Text = "";
                        txtBranch.Text = "";
                        txtAnnPkg.Text = "";
                        this.ClearForm(PnlDetail.Controls);
                        return;

                    }
                }

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", this.txtPersNo.Text);
                //dgvBonusDetail.Refresh(); 

                DataTable dtFPH = new DataTable();

                rsltCode = cmnDM.GetData("CHRIS_SP_BONUSQUERY_MANAGER", "PSN_INC_DSG", colsNVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        dtFPH = rsltCode.dstResult.Tables[0];

                    }
                    if (dtFPH.Rows.Count > 0)
                    {

                        txtDesig.Text = Convert.ToString(dtFPH.Rows[0]["DGG"]);
                        txtLevel.Text = Convert.ToString(dtFPH.Rows[0]["LEVEL"]);
                        txtBranch.Text = Convert.ToString(dtFPH.Rows[0]["BRANCH"]);
                    }
                    else
                    {
                        txtDesig.Text = "";
                        txtLevel.Text = "";
                        txtBranch.Text = "";

                    }
                }
                dgvBonusDetail.Focus();
            }


        }

        private void txtPersNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }


        }

        






    }
}