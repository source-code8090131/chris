namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_TaxInformationIncome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_TaxInformationIncome));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvIncome = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.INCOME_TAX_MIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_TAX_MAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_EXTP_LIMIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_PERCENTAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_FIXED_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_AFT_EXTP_LTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_SLAB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_TOTAL_TAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            //this.SP_SURCHARGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            //this.SP_FSURCHARGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncome)).BeginInit();
            this.PnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(679, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(715, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 238);
            this.panel1.Size = new System.Drawing.Size(715, 60);
            // 
            // dgvIncome
            // 
            this.dgvIncome.AllowUserToAddRows = false;
            this.dgvIncome.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncome.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvIncome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIncome.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INCOME_TAX_MIN,
            this.INCOME_TAX_MAX,
            this.INCOME_EXTP_LIMIT,
            this.INCOME_PERCENTAGE,
            this.INCOME_FIXED_AMT,
            this.INCOME_AFT_EXTP_LTD,
            this.INCOME_SLAB,
            this.INCOME_TOTAL_TAX });
          //  this.SP_SURCHARGE,
          //  this.SP_FSURCHARGE});
            this.dgvIncome.ColumnToHide = null;
            this.dgvIncome.ColumnWidth = null;
            this.dgvIncome.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIncome.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvIncome.DisplayColumnWrapper = null;
            this.dgvIncome.GridDefaultRow = 0;
            this.dgvIncome.Location = new System.Drawing.Point(11, 5);
            this.dgvIncome.Name = "dgvIncome";
            this.dgvIncome.ReadOnly = true;
            this.dgvIncome.ReadOnlyColumns = null;
            this.dgvIncome.RequiredColumns = "";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncome.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvIncome.Size = new System.Drawing.Size(683, 178);
            this.dgvIncome.SkippingColumns = null;
            this.dgvIncome.TabIndex = 0;
            // 
            // SP_AMT_FROM
            // 
            this.INCOME_TAX_MIN.DataPropertyName = "INCOME_TAX_MIN";
            dataGridViewCellStyle2.NullValue = null;
            this.INCOME_TAX_MIN.DefaultCellStyle = dataGridViewCellStyle2;
            this.INCOME_TAX_MIN.HeaderText = "From";
            this.INCOME_TAX_MIN.Name = "INCOME_TAX_MIN";
            this.INCOME_TAX_MIN.ReadOnly = true;
            this.INCOME_TAX_MIN.Width = 65;
            // 
            // INCOME_TAX_MAX
            // 
            this.INCOME_TAX_MAX.DataPropertyName = "INCOME_TAX_MAX";
            this.INCOME_TAX_MAX.HeaderText = "To";
            this.INCOME_TAX_MAX.Name = "INCOME_TAX_MAX";
            this.INCOME_TAX_MAX.ReadOnly = true;
            this.INCOME_TAX_MAX.Width = 65;
            // 
            // INCOME_EXTP_LIMIT
            // 
            this.INCOME_EXTP_LIMIT.DataPropertyName = "INCOME_EXTP_LIMIT";
            this.INCOME_EXTP_LIMIT.HeaderText = "Rebate Male";
            this.INCOME_EXTP_LIMIT.Name = "INCOME_EXTP_LIMIT";
            this.INCOME_EXTP_LIMIT.ReadOnly = true;
            this.INCOME_EXTP_LIMIT.Width = 70;
            // 
            // INCOME_PERCENTAGE
            // 
            this.INCOME_PERCENTAGE.DataPropertyName = "INCOME_PERCENTAGE";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.INCOME_PERCENTAGE.DefaultCellStyle = dataGridViewCellStyle3;
            this.INCOME_PERCENTAGE.HeaderText = "Female";
            this.INCOME_PERCENTAGE.Name = "INCOME_PERCENTAGE";
            this.INCOME_PERCENTAGE.ReadOnly = true;
            this.INCOME_PERCENTAGE.Width = 70;
            // 
            // INCOME_FIXED_AMT
            // 
            this.INCOME_FIXED_AMT.DataPropertyName = "INCOME_FIXED_AMT";
            this.INCOME_FIXED_AMT.HeaderText = "% M.";
            this.INCOME_FIXED_AMT.Name = "INCOME_FIXED_AMT";
            this.INCOME_FIXED_AMT.ReadOnly = true;
            this.INCOME_FIXED_AMT.Width = 35;
            // 
            // INCOME_AFT_EXTP_LTD
            // 
            this.INCOME_AFT_EXTP_LTD.DataPropertyName = "INCOME_AFT_EXTP_LTD";
            this.INCOME_AFT_EXTP_LTD.HeaderText = "F.";
            this.INCOME_AFT_EXTP_LTD.Name = "INCOME_AFT_EXTP_LTD";
            this.INCOME_AFT_EXTP_LTD.ReadOnly = true;
            this.INCOME_AFT_EXTP_LTD.Width = 35;
            // 
            // INCOME_SLAB
            // 
            this.INCOME_SLAB.DataPropertyName = "INCOME_SLAB";
            this.INCOME_SLAB.HeaderText = "Tax Amnt Male";
            this.INCOME_SLAB.Name = "INCOME_SLAB";
            this.INCOME_SLAB.ReadOnly = true;
            this.INCOME_SLAB.Width = 70;
            // 
            // INCOME_TOTAL_TAX
            // 
            this.INCOME_TOTAL_TAX.DataPropertyName = "INCOME_TOTAL_TAX";
            this.INCOME_TOTAL_TAX.HeaderText = "Tax Amnt Female";
            this.INCOME_TOTAL_TAX.Name = "INCOME_TOTAL_TAX";
            this.INCOME_TOTAL_TAX.ReadOnly = true;
            this.INCOME_TOTAL_TAX.Width = 70;
            // 
            // SP_SURCHARGE
            // 
            //this.SP_SURCHARGE.DataPropertyName = "SP_SURCHARGE";
            //this.SP_SURCHARGE.HeaderText = "Surcharge M.";
            //this.SP_SURCHARGE.Name = "SP_SURCHARGE";
            //this.SP_SURCHARGE.ReadOnly = true;
            //this.SP_SURCHARGE.Width = 70;
            // 
            // SP_FSURCHARGE
            // 
            //this.SP_FSURCHARGE.DataPropertyName = "SP_FSURCHARGE";
            //this.SP_FSURCHARGE.HeaderText = "Surcharge F.";
            //this.SP_FSURCHARGE.Name = "SP_FSURCHARGE";
            //this.SP_FSURCHARGE.ReadOnly = true;
            //this.SP_FSURCHARGE.Width = 70;
            // 
            // PnlDetail
            // 
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.dgvIncome);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = false;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabQueryCommand";
            this.PnlDetail.Location = new System.Drawing.Point(8, 39);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(695, 193);
            this.PnlDetail.SPName = "CHRIS_SP_BONUS_TAB_MANAGER";
            this.PnlDetail.TabIndex = 0;
            this.PnlDetail.TabStop = true;
            // 
            // CHRIS_Query_TaxInformationIncome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(715, 298);
            this.Controls.Add(this.PnlDetail);
            this.Name = "CHRIS_Query_TaxInformationIncome";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Tax Information Income";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncome)).EndInit();
            this.PnlDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvIncome;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_TAX_MIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_TAX_MAX;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_EXTP_LIMIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_PERCENTAGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_FIXED_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_AFT_EXTP_LTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_SLAB;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCOME_TOTAL_TAX;
     //   private System.Windows.Forms.DataGridViewTextBoxColumn SP_SURCHARGE;
     //   private System.Windows.Forms.DataGridViewTextBoxColumn SP_FSURCHARGE;
    }
}