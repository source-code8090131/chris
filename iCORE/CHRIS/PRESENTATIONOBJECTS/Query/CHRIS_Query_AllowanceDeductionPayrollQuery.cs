using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_AllowanceDeductionPayrollQuery : ChrisMasterDetailForm
    {

        DataTable dtPrsnl = new DataTable();
        Dictionary<String, Object> objValues = new Dictionary<String, Object>();
        Result rsltPrsnl,rsltPAllow,rsltPDeduct;
        CmnDataManager cmnDM = new CmnDataManager();
        
        
        public CHRIS_Query_AllowanceDeductionPayrollQuery()
        {
            InitializeComponent();
        }


        public CHRIS_Query_AllowanceDeductionPayrollQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
     
            InitializeComponent();
            List<SLPanel> lstDtlPanel = new List<SLPanel>();
            lstDtlPanel.Add(pnlPayroll);
            lstDtlPanel.Add(pnlPayroll_Allow);
            lstDtlPanel.Add(pnlPayroll_Deduc);
            pnlMaster.DependentPanels = lstDtlPanel;

            //List<SLPanel> lstAllowPanel = new List<SLPanel>();
            //lstAllowPanel.Add(pnlPayroll_Allow);
            //lstDeducPanel.Add(pnlPayroll_Deduc);
            //lstDeducPanel.Add(pnlPayroll_Deduc);
            //pnlMaster.DependentPanels = lstAllowPanel;

            //List<SLPanel> lstDeducPanel = new List<SLPanel>();
            //lstDeducPanel.Add(pnlPayroll_Deduc);
            //pnlMaster.DependentPanels = lstDeducPanel; 

            this.IndependentPanels.Add(pnlMaster);
            this.txtOption.Visible = false;
            txtPersNo.Focus();
            lblUserName.Text += "   " + this.UserName;

            dgvAllowance.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            dgvDeduc.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            dgvPFundBranchBasic.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 


        }



        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            this.F1OptionText = "New Criteria";
            this.FunctionConfig.F1 = Function.View;
            this.FunctionConfig.F6 = Function.Quit;

          
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtDelete.Visible = false;
            txtPersNo.Focus();
            txtPersNo.Select();

        }

        protected override bool View()
        {
            bool focus = true;
            this.txtPersNo.Focus();
            return focus;

        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(pnlMaster.Controls);
                this.ClearForm(pnlPayroll.Controls);
                this.ClearForm(pnlPayroll_Allow.Controls);
                this.ClearForm(pnlPayroll_Deduc.Controls);

            }


        }

   

        private void txtPersNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }

        private void txtPersNo_Leave_1(object sender, EventArgs e)
        {


            if (txtPersNo.Text != "")
            {


                this.objValues.Clear();
                this.objValues.Add("PR_P_NO", txtPersNo.Text);

                rsltPrsnl = cmnDM.GetData("CHRIS_SP_ALLOWANCEDEDUCTIONPAYROLL_MANAGER", "PP_VERIFY", objValues);

                if (rsltPrsnl.isSuccessful)
                {
                    if (rsltPrsnl.dstResult.Tables[0].Rows.Count == 0)
                    {

                        MessageBox.Show("Personnel # Does Not Exist.......");
                        this.ClearForm(pnlPayroll.Controls);
                        this.ClearForm(pnlPayroll_Allow.Controls);
                        this.ClearForm(pnlPayroll_Deduc.Controls);
                        txtName.Text = "";
                        txtPersNo.Text = "";
                        txtPersNo.Focus();
                        txtPersNo.Select();
                        return;
                        
                    }
                    else
                    {
                        rsltPrsnl = cmnDM.GetData("CHRIS_SP_ALLOWANCEDEDUCTIONPAYROLL_MANAGER", "PP_VERIFY", objValues);
                        dtPrsnl = rsltPrsnl.dstResult.Tables[0];
                        if (dtPrsnl.Rows.Count > 0)
                        {
                            //txtName.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_FIRST_NAME"]) + "  " + Convert.ToString(dtPrsnl.Rows[0]["PR_LAST_NAME"]);
                            txtName.Text = Convert.ToString(dtPrsnl.Rows[0]["PR_NAME"]);
                        
                        }

                        rsltPAllow = cmnDM.GetData("CHRIS_SP_PAYROLL_ALLOW_MANAGER", "List", objValues);
                        if (rsltPAllow.isSuccessful)
                        {
                            if (rsltPAllow.dstResult.Tables[0].Rows.Count > 0)
                            {
                                this.dgvAllowance.DataSource = rsltPAllow.dstResult.Tables[0];

                            }
                            else
                            {
                                this.dgvAllowance.DataSource = null;
                                this.dgvAllowance.Refresh();
                            }

                        }

                        rsltPDeduct = cmnDM.GetData("CHRIS_SP_PAYROLL_DEDUC_MANAGER", "List", objValues);

                        if (rsltPDeduct.isSuccessful)
                        {
                            if (rsltPDeduct.dstResult.Tables[0].Rows.Count > 0)
                            {
                                this.dgvDeduc.DataSource = rsltPDeduct.dstResult.Tables[0];

                            }
                            else
                            {
                                this.dgvDeduc.DataSource = null;
                                this.dgvDeduc.Refresh();
                            }

                        }
                        

                    }

                }
                txtPersNo.Focus();
                txtPersNo.Select();

            }








        }

    

   

      





    }
}