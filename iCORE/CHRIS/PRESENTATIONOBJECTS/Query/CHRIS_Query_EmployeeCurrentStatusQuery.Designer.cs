namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_EmployeeCurrentStatusQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_EmployeeCurrentStatusQuery));
            this.lblHeader = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPersonnelHeader = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.slTxtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.dgvSDC = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.lblEducation = new System.Windows.Forms.Label();
            this.txtEducation = new CrplControlLibrary.SLTextBox(this.components);
            this.lblAccNum = new System.Windows.Forms.Label();
            this.txtAccNum = new CrplControlLibrary.SLTextBox(this.components);
            this.lblNatTaxNum = new System.Windows.Forms.Label();
            this.txtNatTaxNum = new CrplControlLibrary.SLTextBox(this.components);
            this.lblAnnPkg = new System.Windows.Forms.Label();
            this.txtAnnPkge = new CrplControlLibrary.SLTextBox(this.components);
            this.lblGEIDNum = new System.Windows.Forms.Label();
            this.txtGEID = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.lblNxtAppr = new System.Windows.Forms.Label();
            this.lblLstAppr = new System.Windows.Forms.Label();
            this.lblConfirmDate = new System.Windows.Forms.Label();
            this.lblConfirm = new System.Windows.Forms.Label();
            this.lblJoinDate = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblFunctional = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.lblLevel = new System.Windows.Forms.Label();
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.lblCategory = new System.Windows.Forms.Label();
            this.txtCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNextAppr = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLastAppr = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConfirmDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConfirm = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJoinDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTitle = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFunctional = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.lblDesig = new System.Windows.Forms.Label();
            this.lblPrNum = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPersonal = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblPhNum = new System.Windows.Forms.Label();
            this.txtAddress = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPhoneNum = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPhoneNum2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDoB = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSpouseName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNoofChild = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSex = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNICNum = new CrplControlLibrary.SLTextBox(this.components);
            this.lblNICNum = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.lblDoB = new System.Windows.Forms.Label();
            this.lblSpName = new System.Windows.Forms.Label();
            this.lblNoOfChild = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMarriageDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lblMDate = new System.Windows.Forms.Label();
            this.txtSpDoB = new CrplControlLibrary.SLTextBox(this.components);
            this.lblSpDoB = new System.Windows.Forms.Label();
            this.lblDateofBirth = new System.Windows.Forms.Label();
            this.txtAddress2 = new CrplControlLibrary.SLTextBox(this.components);
            this.dgvChildren = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_CHILD_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvChildDB = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_DATE_BIRTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlPersonalDtl = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lblMaritalStatus = new System.Windows.Forms.Label();
            this.txtMartStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.PR_SEGMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DEPT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CONTRIB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CostCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChildDB)).BeginInit();
            this.pnlPersonalDtl.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(852, 0);
            this.txtOption.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Margin = new System.Windows.Forms.Padding(5);
            this.pnlBottom.Size = new System.Drawing.Size(899, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 744);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(899, 74);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(344, 50);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(234, 20);
            this.lblHeader.TabIndex = 10;
            this.lblHeader.Text = "Employee\'s Current Status";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(627, 79);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 13;
            this.label8.Text = "Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(68, 55);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "User :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(39, 84);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Location :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(603, 11);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(91, 17);
            this.lblUserName.TabIndex = 26;
            this.lblUserName.Text = "User Name  :";
            // 
            // lblPersonnelHeader
            // 
            this.lblPersonnelHeader.AutoSize = true;
            this.lblPersonnelHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonnelHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonnelHeader.Location = new System.Drawing.Point(7, 102);
            this.lblPersonnelHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblPersonnelHeader.Name = "lblPersonnelHeader";
            this.lblPersonnelHeader.Size = new System.Drawing.Size(864, 24);
            this.lblPersonnelHeader.TabIndex = 27;
            this.lblPersonnelHeader.Text = "________________________________> PERSONNEL <_______________________________";
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.slTxtLocation);
            this.pnlDetail.Controls.Add(this.dgvSDC);
            this.pnlDetail.Controls.Add(this.lblEducation);
            this.pnlDetail.Controls.Add(this.txtEducation);
            this.pnlDetail.Controls.Add(this.lblAccNum);
            this.pnlDetail.Controls.Add(this.txtAccNum);
            this.pnlDetail.Controls.Add(this.lblNatTaxNum);
            this.pnlDetail.Controls.Add(this.txtNatTaxNum);
            this.pnlDetail.Controls.Add(this.lblAnnPkg);
            this.pnlDetail.Controls.Add(this.txtAnnPkge);
            this.pnlDetail.Controls.Add(this.lblGEIDNum);
            this.pnlDetail.Controls.Add(this.txtGEID);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.lblNxtAppr);
            this.pnlDetail.Controls.Add(this.lblLstAppr);
            this.pnlDetail.Controls.Add(this.lblConfirmDate);
            this.pnlDetail.Controls.Add(this.lblConfirm);
            this.pnlDetail.Controls.Add(this.lblJoinDate);
            this.pnlDetail.Controls.Add(this.lblTitle);
            this.pnlDetail.Controls.Add(this.lblFunctional);
            this.pnlDetail.Controls.Add(this.lblBranch);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.lblLevel);
            this.pnlDetail.Controls.Add(this.txtLastName);
            this.pnlDetail.Controls.Add(this.lblCategory);
            this.pnlDetail.Controls.Add(this.txtCategory);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.txtNextAppr);
            this.pnlDetail.Controls.Add(this.txtLastAppr);
            this.pnlDetail.Controls.Add(this.txtConfirmDate);
            this.pnlDetail.Controls.Add(this.txtConfirm);
            this.pnlDetail.Controls.Add(this.txtJoinDate);
            this.pnlDetail.Controls.Add(this.txtTitle);
            this.pnlDetail.Controls.Add(this.txtFunctional);
            this.pnlDetail.Controls.Add(this.txtBranch);
            this.pnlDetail.Controls.Add(this.txtDesig);
            this.pnlDetail.Controls.Add(this.txtFirstName);
            this.pnlDetail.Controls.Add(this.lblDesig);
            this.pnlDetail.Controls.Add(this.lblPrNum);
            this.pnlDetail.Controls.Add(this.lblName);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = false;
            this.pnlDetail.EnableInsert = false;
            this.pnlDetail.EnableQuery = true;
            this.pnlDetail.EnableUpdate = false;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlDetail.Location = new System.Drawing.Point(7, 128);
            this.pnlDetail.Margin = new System.Windows.Forms.Padding(4);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(880, 321);
            this.pnlDetail.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.pnlDetail.TabIndex = 0;
            this.pnlDetail.TabStop = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Location :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // slTxtLocation
            // 
            this.slTxtLocation.AllowSpace = true;
            this.slTxtLocation.AssociatedLookUpName = "";
            this.slTxtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtLocation.ContinuationTextBox = null;
            this.slTxtLocation.CustomEnabled = true;
            this.slTxtLocation.DataFieldMapping = "LOCATION";
            this.slTxtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtLocation.GetRecordsOnUpDownKeys = false;
            this.slTxtLocation.IsDate = false;
            this.slTxtLocation.Location = new System.Drawing.Point(141, 62);
            this.slTxtLocation.Margin = new System.Windows.Forms.Padding(4);
            this.slTxtLocation.MaxLength = 10;
            this.slTxtLocation.Name = "slTxtLocation";
            this.slTxtLocation.NumberFormat = "###,###,##0.00";
            this.slTxtLocation.Postfix = "";
            this.slTxtLocation.Prefix = "";
            this.slTxtLocation.ReadOnly = true;
            this.slTxtLocation.Size = new System.Drawing.Size(251, 24);
            this.slTxtLocation.SkipValidation = false;
            this.slTxtLocation.TabIndex = 18;
            this.slTxtLocation.TabStop = false;
            this.slTxtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // dgvSDC
            // 
            this.dgvSDC.AllowUserToAddRows = false;
            this.dgvSDC.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSDC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSDC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSDC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_SEGMENT,
            this.PR_DEPT,
            this.PR_CONTRIB,
            this.CostCode});
            this.dgvSDC.ColumnToHide = null;
            this.dgvSDC.ColumnWidth = null;
            this.dgvSDC.CustomEnabled = true;
            this.dgvSDC.DisplayColumnWrapper = null;
            this.dgvSDC.GridDefaultRow = 0;
            this.dgvSDC.Location = new System.Drawing.Point(599, 81);
            this.dgvSDC.Margin = new System.Windows.Forms.Padding(4);
            this.dgvSDC.Name = "dgvSDC";
            this.dgvSDC.ReadOnlyColumns = null;
            this.dgvSDC.RequiredColumns = null;
            this.dgvSDC.Size = new System.Drawing.Size(272, 185);
            this.dgvSDC.SkippingColumns = null;
            this.dgvSDC.TabIndex = 16;
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEducation.Location = new System.Drawing.Point(345, 297);
            this.lblEducation.Margin = new System.Windows.Forms.Padding(0);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(90, 17);
            this.lblEducation.TabIndex = 14;
            this.lblEducation.Text = "Education :";
            // 
            // txtEducation
            // 
            this.txtEducation.AllowSpace = true;
            this.txtEducation.AssociatedLookUpName = "";
            this.txtEducation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEducation.ContinuationTextBox = null;
            this.txtEducation.CustomEnabled = true;
            this.txtEducation.DataFieldMapping = "DGREE";
            this.txtEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEducation.GetRecordsOnUpDownKeys = false;
            this.txtEducation.IsDate = false;
            this.txtEducation.Location = new System.Drawing.Point(428, 292);
            this.txtEducation.Margin = new System.Windows.Forms.Padding(4);
            this.txtEducation.MaxLength = 10;
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.NumberFormat = "###,###,##0.00";
            this.txtEducation.Postfix = "";
            this.txtEducation.Prefix = "";
            this.txtEducation.ReadOnly = true;
            this.txtEducation.Size = new System.Drawing.Size(155, 24);
            this.txtEducation.SkipValidation = false;
            this.txtEducation.TabIndex = 15;
            this.txtEducation.TabStop = false;
            this.txtEducation.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblAccNum
            // 
            this.lblAccNum.AutoSize = true;
            this.lblAccNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccNum.Location = new System.Drawing.Point(333, 267);
            this.lblAccNum.Margin = new System.Windows.Forms.Padding(0);
            this.lblAccNum.Name = "lblAccNum";
            this.lblAccNum.Size = new System.Drawing.Size(101, 17);
            this.lblAccNum.TabIndex = 12;
            this.lblAccNum.Text = "Account No :";
            // 
            // txtAccNum
            // 
            this.txtAccNum.AllowSpace = true;
            this.txtAccNum.AssociatedLookUpName = "";
            this.txtAccNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAccNum.ContinuationTextBox = null;
            this.txtAccNum.CustomEnabled = true;
            this.txtAccNum.DataFieldMapping = "PR_ACCOUNT_NO";
            this.txtAccNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccNum.GetRecordsOnUpDownKeys = false;
            this.txtAccNum.IsDate = false;
            this.txtAccNum.Location = new System.Drawing.Point(428, 262);
            this.txtAccNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtAccNum.MaxLength = 10;
            this.txtAccNum.Name = "txtAccNum";
            this.txtAccNum.NumberFormat = "###,###,##0.00";
            this.txtAccNum.Postfix = "";
            this.txtAccNum.Prefix = "";
            this.txtAccNum.ReadOnly = true;
            this.txtAccNum.Size = new System.Drawing.Size(155, 24);
            this.txtAccNum.SkipValidation = false;
            this.txtAccNum.TabIndex = 13;
            this.txtAccNum.TabStop = false;
            this.txtAccNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblNatTaxNum
            // 
            this.lblNatTaxNum.AutoSize = true;
            this.lblNatTaxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNatTaxNum.Location = new System.Drawing.Point(300, 238);
            this.lblNatTaxNum.Margin = new System.Windows.Forms.Padding(0);
            this.lblNatTaxNum.Name = "lblNatTaxNum";
            this.lblNatTaxNum.Size = new System.Drawing.Size(139, 17);
            this.lblNatTaxNum.TabIndex = 10;
            this.lblNatTaxNum.Text = "National Tax No. :";
            // 
            // txtNatTaxNum
            // 
            this.txtNatTaxNum.AllowSpace = true;
            this.txtNatTaxNum.AssociatedLookUpName = "";
            this.txtNatTaxNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNatTaxNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNatTaxNum.ContinuationTextBox = null;
            this.txtNatTaxNum.CustomEnabled = true;
            this.txtNatTaxNum.DataFieldMapping = "PR_NATIONAL_TAX";
            this.txtNatTaxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNatTaxNum.GetRecordsOnUpDownKeys = false;
            this.txtNatTaxNum.IsDate = false;
            this.txtNatTaxNum.Location = new System.Drawing.Point(428, 233);
            this.txtNatTaxNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtNatTaxNum.MaxLength = 10;
            this.txtNatTaxNum.Name = "txtNatTaxNum";
            this.txtNatTaxNum.NumberFormat = "###,###,##0.00";
            this.txtNatTaxNum.Postfix = "";
            this.txtNatTaxNum.Prefix = "";
            this.txtNatTaxNum.ReadOnly = true;
            this.txtNatTaxNum.Size = new System.Drawing.Size(155, 24);
            this.txtNatTaxNum.SkipValidation = false;
            this.txtNatTaxNum.TabIndex = 11;
            this.txtNatTaxNum.TabStop = false;
            this.txtNatTaxNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblAnnPkg
            // 
            this.lblAnnPkg.AutoSize = true;
            this.lblAnnPkg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnnPkg.Location = new System.Drawing.Point(304, 208);
            this.lblAnnPkg.Margin = new System.Windows.Forms.Padding(0);
            this.lblAnnPkg.Name = "lblAnnPkg";
            this.lblAnnPkg.Size = new System.Drawing.Size(135, 17);
            this.lblAnnPkg.TabIndex = 8;
            this.lblAnnPkg.Text = "Annual Package :";
            // 
            // txtAnnPkge
            // 
            this.txtAnnPkge.AllowSpace = true;
            this.txtAnnPkge.AssociatedLookUpName = "";
            this.txtAnnPkge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnPkge.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnPkge.ContinuationTextBox = null;
            this.txtAnnPkge.CustomEnabled = true;
            this.txtAnnPkge.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtAnnPkge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnPkge.GetRecordsOnUpDownKeys = false;
            this.txtAnnPkge.IsDate = false;
            this.txtAnnPkge.Location = new System.Drawing.Point(428, 203);
            this.txtAnnPkge.Margin = new System.Windows.Forms.Padding(4);
            this.txtAnnPkge.MaxLength = 10;
            this.txtAnnPkge.Name = "txtAnnPkge";
            this.txtAnnPkge.NumberFormat = "###,###,##0.00";
            this.txtAnnPkge.Postfix = "";
            this.txtAnnPkge.Prefix = "";
            this.txtAnnPkge.ReadOnly = true;
            this.txtAnnPkge.Size = new System.Drawing.Size(155, 24);
            this.txtAnnPkge.SkipValidation = false;
            this.txtAnnPkge.TabIndex = 9;
            this.txtAnnPkge.TabStop = false;
            this.txtAnnPkge.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblGEIDNum
            // 
            this.lblGEIDNum.AutoSize = true;
            this.lblGEIDNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGEIDNum.Location = new System.Drawing.Point(339, 178);
            this.lblGEIDNum.Margin = new System.Windows.Forms.Padding(0);
            this.lblGEIDNum.Name = "lblGEIDNum";
            this.lblGEIDNum.Size = new System.Drawing.Size(95, 17);
            this.lblGEIDNum.TabIndex = 6;
            this.lblGEIDNum.Text = "G.E.ID. No :";
            // 
            // txtGEID
            // 
            this.txtGEID.AllowSpace = true;
            this.txtGEID.AssociatedLookUpName = "";
            this.txtGEID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGEID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGEID.ContinuationTextBox = null;
            this.txtGEID.CustomEnabled = true;
            this.txtGEID.DataFieldMapping = "GEIDNO";
            this.txtGEID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGEID.GetRecordsOnUpDownKeys = false;
            this.txtGEID.IsDate = false;
            this.txtGEID.Location = new System.Drawing.Point(428, 174);
            this.txtGEID.Margin = new System.Windows.Forms.Padding(4);
            this.txtGEID.MaxLength = 10;
            this.txtGEID.Name = "txtGEID";
            this.txtGEID.NumberFormat = "###,###,##0.00";
            this.txtGEID.Postfix = "";
            this.txtGEID.Prefix = "";
            this.txtGEID.ReadOnly = true;
            this.txtGEID.Size = new System.Drawing.Size(155, 24);
            this.txtGEID.SkipValidation = false;
            this.txtGEID.TabIndex = 7;
            this.txtGEID.TabStop = false;
            this.txtGEID.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PP_NO_EXISTS";
            this.lbtnPNo.ActionType = "PP_LOV";
            this.lbtnPNo.CausesValidation = false;
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(304, 4);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Margin = new System.Windows.Forms.Padding(4);
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.toolTip1.SetToolTip(this.lbtnPNo, "Select Desired Personnel Number");
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // lblNxtAppr
            // 
            this.lblNxtAppr.AutoSize = true;
            this.lblNxtAppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNxtAppr.Location = new System.Drawing.Point(27, 292);
            this.lblNxtAppr.Margin = new System.Windows.Forms.Padding(0);
            this.lblNxtAppr.Name = "lblNxtAppr";
            this.lblNxtAppr.Size = new System.Drawing.Size(123, 17);
            this.lblNxtAppr.TabIndex = 1;
            this.lblNxtAppr.Text = "Next Appraisal :";
            this.lblNxtAppr.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblLstAppr
            // 
            this.lblLstAppr.AutoSize = true;
            this.lblLstAppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLstAppr.Location = new System.Drawing.Point(29, 263);
            this.lblLstAppr.Margin = new System.Windows.Forms.Padding(0);
            this.lblLstAppr.Name = "lblLstAppr";
            this.lblLstAppr.Size = new System.Drawing.Size(122, 17);
            this.lblLstAppr.TabIndex = 1;
            this.lblLstAppr.Text = "Last Appraisal :";
            this.lblLstAppr.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblConfirmDate
            // 
            this.lblConfirmDate.AutoSize = true;
            this.lblConfirmDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfirmDate.Location = new System.Drawing.Point(37, 235);
            this.lblConfirmDate.Margin = new System.Windows.Forms.Padding(0);
            this.lblConfirmDate.Name = "lblConfirmDate";
            this.lblConfirmDate.Size = new System.Drawing.Size(112, 17);
            this.lblConfirmDate.TabIndex = 1;
            this.lblConfirmDate.Text = "Confirm Date :";
            this.lblConfirmDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblConfirm
            // 
            this.lblConfirm.AutoSize = true;
            this.lblConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfirm.Location = new System.Drawing.Point(41, 207);
            this.lblConfirm.Margin = new System.Windows.Forms.Padding(0);
            this.lblConfirm.Name = "lblConfirm";
            this.lblConfirm.Size = new System.Drawing.Size(107, 17);
            this.lblConfirm.TabIndex = 1;
            this.lblConfirm.Text = "Confirm Due :";
            this.lblConfirm.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblJoinDate
            // 
            this.lblJoinDate.AutoSize = true;
            this.lblJoinDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJoinDate.Location = new System.Drawing.Point(39, 178);
            this.lblJoinDate.Margin = new System.Windows.Forms.Padding(0);
            this.lblJoinDate.Name = "lblJoinDate";
            this.lblJoinDate.Size = new System.Drawing.Size(109, 17);
            this.lblJoinDate.TabIndex = 1;
            this.lblJoinDate.Text = "Joining Date :";
            this.lblJoinDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(92, 150);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(50, 17);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Title :";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblFunctional
            // 
            this.lblFunctional.AutoSize = true;
            this.lblFunctional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunctional.Location = new System.Drawing.Point(52, 122);
            this.lblFunctional.Margin = new System.Windows.Forms.Padding(0);
            this.lblFunctional.Name = "lblFunctional";
            this.lblFunctional.Size = new System.Drawing.Size(93, 17);
            this.lblFunctional.TabIndex = 1;
            this.lblFunctional.Text = "Functional :";
            this.lblFunctional.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            this.lblBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(75, 94);
            this.lblBranch.Margin = new System.Windows.Forms.Padding(0);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(69, 17);
            this.lblBranch.TabIndex = 1;
            this.lblBranch.Text = "Branch :";
            this.lblBranch.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(141, 4);
            this.txtPersNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(155, 24);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.TextChanged += new System.EventHandler(this.txtPersNo_TextChanged);
            this.txtPersNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersNo_KeyPress);
            this.txtPersNo.Leave += new System.EventHandler(this.txtPersNo_Leave);
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(593, 41);
            this.lblLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(57, 17);
            this.lblLevel.TabIndex = 1;
            this.lblLevel.Text = "Level :";
            this.lblLevel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "PR_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(651, 4);
            this.txtLastName.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastName.MaxLength = 10;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.ReadOnly = true;
            this.txtLastName.Size = new System.Drawing.Size(155, 24);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 2;
            this.txtLastName.TabStop = false;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(405, 42);
            this.lblCategory.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(83, 17);
            this.lblCategory.TabIndex = 1;
            this.lblCategory.Text = "Category :";
            this.lblCategory.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCategory
            // 
            this.txtCategory.AllowSpace = true;
            this.txtCategory.AssociatedLookUpName = "";
            this.txtCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategory.ContinuationTextBox = null;
            this.txtCategory.CustomEnabled = true;
            this.txtCategory.DataFieldMapping = "PR_CATEGORY";
            this.txtCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.GetRecordsOnUpDownKeys = false;
            this.txtCategory.IsDate = false;
            this.txtCategory.Location = new System.Drawing.Point(487, 36);
            this.txtCategory.Margin = new System.Windows.Forms.Padding(4);
            this.txtCategory.MaxLength = 1;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.NumberFormat = "###,###,##0.00";
            this.txtCategory.Postfix = "";
            this.txtCategory.Prefix = "";
            this.txtCategory.ReadOnly = true;
            this.txtCategory.Size = new System.Drawing.Size(57, 24);
            this.txtCategory.SkipValidation = false;
            this.txtCategory.TabIndex = 4;
            this.txtCategory.TabStop = false;
            this.txtCategory.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(651, 36);
            this.txtLevel.Margin = new System.Windows.Forms.Padding(4);
            this.txtLevel.MaxLength = 1;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(57, 24);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 4;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNextAppr
            // 
            this.txtNextAppr.AllowSpace = true;
            this.txtNextAppr.AssociatedLookUpName = "";
            this.txtNextAppr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNextAppr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNextAppr.ContinuationTextBox = null;
            this.txtNextAppr.CustomEnabled = true;
            this.txtNextAppr.DataFieldMapping = "PR_NEXT_INCREMENT";
            this.txtNextAppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNextAppr.GetRecordsOnUpDownKeys = false;
            this.txtNextAppr.IsDate = false;
            this.txtNextAppr.Location = new System.Drawing.Point(141, 289);
            this.txtNextAppr.Margin = new System.Windows.Forms.Padding(4);
            this.txtNextAppr.MaxLength = 10;
            this.txtNextAppr.Name = "txtNextAppr";
            this.txtNextAppr.NumberFormat = "###,###,##0.00";
            this.txtNextAppr.Postfix = "";
            this.txtNextAppr.Prefix = "";
            this.txtNextAppr.ReadOnly = true;
            this.txtNextAppr.Size = new System.Drawing.Size(155, 24);
            this.txtNextAppr.SkipValidation = false;
            this.txtNextAppr.TabIndex = 5;
            this.txtNextAppr.TabStop = false;
            this.txtNextAppr.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLastAppr
            // 
            this.txtLastAppr.AllowSpace = true;
            this.txtLastAppr.AssociatedLookUpName = "";
            this.txtLastAppr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastAppr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastAppr.ContinuationTextBox = null;
            this.txtLastAppr.CustomEnabled = true;
            this.txtLastAppr.DataFieldMapping = "PR_LAST_INCREMENT";
            this.txtLastAppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastAppr.GetRecordsOnUpDownKeys = false;
            this.txtLastAppr.IsDate = false;
            this.txtLastAppr.Location = new System.Drawing.Point(141, 261);
            this.txtLastAppr.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastAppr.MaxLength = 10;
            this.txtLastAppr.Name = "txtLastAppr";
            this.txtLastAppr.NumberFormat = "###,###,##0.00";
            this.txtLastAppr.Postfix = "";
            this.txtLastAppr.Prefix = "";
            this.txtLastAppr.ReadOnly = true;
            this.txtLastAppr.Size = new System.Drawing.Size(155, 24);
            this.txtLastAppr.SkipValidation = false;
            this.txtLastAppr.TabIndex = 5;
            this.txtLastAppr.TabStop = false;
            this.txtLastAppr.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtConfirmDate
            // 
            this.txtConfirmDate.AllowSpace = true;
            this.txtConfirmDate.AssociatedLookUpName = "";
            this.txtConfirmDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirmDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfirmDate.ContinuationTextBox = null;
            this.txtConfirmDate.CustomEnabled = true;
            this.txtConfirmDate.DataFieldMapping = "PR_CONFIRM_ON";
            this.txtConfirmDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmDate.GetRecordsOnUpDownKeys = false;
            this.txtConfirmDate.IsDate = false;
            this.txtConfirmDate.Location = new System.Drawing.Point(141, 233);
            this.txtConfirmDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfirmDate.MaxLength = 10;
            this.txtConfirmDate.Name = "txtConfirmDate";
            this.txtConfirmDate.NumberFormat = "###,###,##0.00";
            this.txtConfirmDate.Postfix = "";
            this.txtConfirmDate.Prefix = "";
            this.txtConfirmDate.ReadOnly = true;
            this.txtConfirmDate.Size = new System.Drawing.Size(155, 24);
            this.txtConfirmDate.SkipValidation = false;
            this.txtConfirmDate.TabIndex = 5;
            this.txtConfirmDate.TabStop = false;
            this.txtConfirmDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtConfirm
            // 
            this.txtConfirm.AllowSpace = true;
            this.txtConfirm.AssociatedLookUpName = "";
            this.txtConfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfirm.ContinuationTextBox = null;
            this.txtConfirm.CustomEnabled = true;
            this.txtConfirm.DataFieldMapping = "PR_CONFIRM";
            this.txtConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirm.GetRecordsOnUpDownKeys = false;
            this.txtConfirm.IsDate = false;
            this.txtConfirm.Location = new System.Drawing.Point(141, 204);
            this.txtConfirm.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfirm.MaxLength = 10;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.NumberFormat = "###,###,##0.00";
            this.txtConfirm.Postfix = "";
            this.txtConfirm.Prefix = "";
            this.txtConfirm.ReadOnly = true;
            this.txtConfirm.Size = new System.Drawing.Size(155, 24);
            this.txtConfirm.SkipValidation = false;
            this.txtConfirm.TabIndex = 5;
            this.txtConfirm.TabStop = false;
            this.txtConfirm.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtJoinDate
            // 
            this.txtJoinDate.AllowSpace = true;
            this.txtJoinDate.AssociatedLookUpName = "";
            this.txtJoinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJoinDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJoinDate.ContinuationTextBox = null;
            this.txtJoinDate.CustomEnabled = true;
            this.txtJoinDate.DataFieldMapping = "PR_JOINING_DATE";
            this.txtJoinDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJoinDate.GetRecordsOnUpDownKeys = false;
            this.txtJoinDate.IsDate = false;
            this.txtJoinDate.Location = new System.Drawing.Point(141, 176);
            this.txtJoinDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtJoinDate.MaxLength = 10;
            this.txtJoinDate.Name = "txtJoinDate";
            this.txtJoinDate.NumberFormat = "###,###,##0.00";
            this.txtJoinDate.Postfix = "";
            this.txtJoinDate.Prefix = "";
            this.txtJoinDate.ReadOnly = true;
            this.txtJoinDate.Size = new System.Drawing.Size(155, 24);
            this.txtJoinDate.SkipValidation = false;
            this.txtJoinDate.TabIndex = 5;
            this.txtJoinDate.TabStop = false;
            this.txtJoinDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTitle
            // 
            this.txtTitle.AllowSpace = true;
            this.txtTitle.AssociatedLookUpName = "";
            this.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTitle.ContinuationTextBox = null;
            this.txtTitle.CustomEnabled = true;
            this.txtTitle.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.GetRecordsOnUpDownKeys = false;
            this.txtTitle.IsDate = false;
            this.txtTitle.Location = new System.Drawing.Point(141, 148);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(4);
            this.txtTitle.MaxLength = 10;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.NumberFormat = "###,###,##0.00";
            this.txtTitle.Postfix = "";
            this.txtTitle.Prefix = "";
            this.txtTitle.ReadOnly = true;
            this.txtTitle.Size = new System.Drawing.Size(251, 24);
            this.txtTitle.SkipValidation = false;
            this.txtTitle.TabIndex = 5;
            this.txtTitle.TabStop = false;
            this.txtTitle.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFunctional
            // 
            this.txtFunctional.AllowSpace = true;
            this.txtFunctional.AssociatedLookUpName = "";
            this.txtFunctional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFunctional.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFunctional.ContinuationTextBox = null;
            this.txtFunctional.CustomEnabled = true;
            this.txtFunctional.DataFieldMapping = "PR_FUNC_TITTLE1";
            this.txtFunctional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFunctional.GetRecordsOnUpDownKeys = false;
            this.txtFunctional.IsDate = false;
            this.txtFunctional.Location = new System.Drawing.Point(141, 119);
            this.txtFunctional.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunctional.MaxLength = 30;
            this.txtFunctional.Name = "txtFunctional";
            this.txtFunctional.NumberFormat = "###,###,##0.00";
            this.txtFunctional.Postfix = "";
            this.txtFunctional.Prefix = "";
            this.txtFunctional.ReadOnly = true;
            this.txtFunctional.Size = new System.Drawing.Size(251, 24);
            this.txtFunctional.SkipValidation = false;
            this.txtFunctional.TabIndex = 5;
            this.txtFunctional.TabStop = false;
            this.txtFunctional.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(141, 91);
            this.txtBranch.Margin = new System.Windows.Forms.Padding(4);
            this.txtBranch.MaxLength = 10;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(251, 24);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 5;
            this.txtBranch.TabStop = false;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "DGG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(141, 32);
            this.txtDesig.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesig.MaxLength = 50;
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(251, 24);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 3;
            this.txtDesig.TabStop = false;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(487, 4);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 10;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(155, 24);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblDesig
            // 
            this.lblDesig.AutoSize = true;
            this.lblDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesig.Location = new System.Drawing.Point(43, 37);
            this.lblDesig.Margin = new System.Windows.Forms.Padding(0);
            this.lblDesig.Name = "lblDesig";
            this.lblDesig.Size = new System.Drawing.Size(104, 17);
            this.lblDesig.TabIndex = 1;
            this.lblDesig.Text = "Designation :";
            this.lblDesig.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPrNum
            // 
            this.lblPrNum.AutoSize = true;
            this.lblPrNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrNum.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPrNum.Location = new System.Drawing.Point(1, 9);
            this.lblPrNum.Margin = new System.Windows.Forms.Padding(0);
            this.lblPrNum.Name = "lblPrNum";
            this.lblPrNum.Size = new System.Drawing.Size(152, 17);
            this.lblPrNum.TabIndex = 1;
            this.lblPrNum.Text = "Personnel Number :";
            this.lblPrNum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(425, 9);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(64, 17);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name : ";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPersonal
            // 
            this.lblPersonal.AutoSize = true;
            this.lblPersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonal.Location = new System.Drawing.Point(3, 453);
            this.lblPersonal.Margin = new System.Windows.Forms.Padding(0);
            this.lblPersonal.Name = "lblPersonal";
            this.lblPersonal.Size = new System.Drawing.Size(893, 24);
            this.lblPersonal.TabIndex = 62;
            this.lblPersonal.Text = "_________________________________> PERSONAL <__________________________________";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(683, 74);
            this.txtDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(111, 24);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 16;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(123, 50);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(114, 24);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 14;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(123, 78);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(114, 24);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 15;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(69, 20);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(77, 17);
            this.lblAddress.TabIndex = 1;
            this.lblAddress.Text = "Address :";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPhNum
            // 
            this.lblPhNum.AutoSize = true;
            this.lblPhNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhNum.Location = new System.Drawing.Point(53, 52);
            this.lblPhNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhNum.Name = "lblPhNum";
            this.lblPhNum.Size = new System.Drawing.Size(94, 17);
            this.lblPhNum.TabIndex = 1;
            this.lblPhNum.Text = "Phone No. :";
            this.lblPhNum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAddress
            // 
            this.txtAddress.AllowSpace = true;
            this.txtAddress.AssociatedLookUpName = "";
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAddress.ContinuationTextBox = null;
            this.txtAddress.CustomEnabled = true;
            this.txtAddress.DataFieldMapping = "PR_ADD1";
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.GetRecordsOnUpDownKeys = false;
            this.txtAddress.IsDate = false;
            this.txtAddress.Location = new System.Drawing.Point(143, 12);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddress.MaxLength = 250;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.NumberFormat = "###,###,##0.00";
            this.txtAddress.Postfix = "";
            this.txtAddress.Prefix = "";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(339, 24);
            this.txtAddress.SkipValidation = false;
            this.txtAddress.TabIndex = 1;
            this.txtAddress.TabStop = false;
            this.txtAddress.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPhoneNum
            // 
            this.txtPhoneNum.AllowSpace = true;
            this.txtPhoneNum.AssociatedLookUpName = "";
            this.txtPhoneNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhoneNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhoneNum.ContinuationTextBox = null;
            this.txtPhoneNum.CustomEnabled = true;
            this.txtPhoneNum.DataFieldMapping = "PR_PHONE1";
            this.txtPhoneNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNum.GetRecordsOnUpDownKeys = false;
            this.txtPhoneNum.IsDate = false;
            this.txtPhoneNum.Location = new System.Drawing.Point(143, 41);
            this.txtPhoneNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhoneNum.MaxLength = 10;
            this.txtPhoneNum.Name = "txtPhoneNum";
            this.txtPhoneNum.NumberFormat = "###,###,##0.00";
            this.txtPhoneNum.Postfix = "";
            this.txtPhoneNum.Prefix = "";
            this.txtPhoneNum.ReadOnly = true;
            this.txtPhoneNum.Size = new System.Drawing.Size(155, 24);
            this.txtPhoneNum.SkipValidation = false;
            this.txtPhoneNum.TabIndex = 3;
            this.txtPhoneNum.TabStop = false;
            this.txtPhoneNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPhoneNum2
            // 
            this.txtPhoneNum2.AllowSpace = true;
            this.txtPhoneNum2.AssociatedLookUpName = "";
            this.txtPhoneNum2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhoneNum2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhoneNum2.ContinuationTextBox = null;
            this.txtPhoneNum2.CustomEnabled = true;
            this.txtPhoneNum2.DataFieldMapping = "PR_PHONE2";
            this.txtPhoneNum2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNum2.GetRecordsOnUpDownKeys = false;
            this.txtPhoneNum2.IsDate = false;
            this.txtPhoneNum2.Location = new System.Drawing.Point(303, 41);
            this.txtPhoneNum2.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhoneNum2.MaxLength = 10;
            this.txtPhoneNum2.Name = "txtPhoneNum2";
            this.txtPhoneNum2.NumberFormat = "###,###,##0.00";
            this.txtPhoneNum2.Postfix = "";
            this.txtPhoneNum2.Prefix = "";
            this.txtPhoneNum2.ReadOnly = true;
            this.txtPhoneNum2.Size = new System.Drawing.Size(155, 24);
            this.txtPhoneNum2.SkipValidation = false;
            this.txtPhoneNum2.TabIndex = 3;
            this.txtPhoneNum2.TabStop = false;
            this.txtPhoneNum2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDoB
            // 
            this.txtDoB.AllowSpace = true;
            this.txtDoB.AssociatedLookUpName = "";
            this.txtDoB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDoB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDoB.ContinuationTextBox = null;
            this.txtDoB.CustomEnabled = true;
            this.txtDoB.DataFieldMapping = "PR_D_BIRTH";
            this.txtDoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDoB.GetRecordsOnUpDownKeys = false;
            this.txtDoB.IsDate = false;
            this.txtDoB.Location = new System.Drawing.Point(141, 70);
            this.txtDoB.Margin = new System.Windows.Forms.Padding(4);
            this.txtDoB.MaxLength = 30;
            this.txtDoB.Name = "txtDoB";
            this.txtDoB.NumberFormat = "###,###,##0.00";
            this.txtDoB.Postfix = "";
            this.txtDoB.Prefix = "";
            this.txtDoB.ReadOnly = true;
            this.txtDoB.Size = new System.Drawing.Size(155, 24);
            this.txtDoB.SkipValidation = false;
            this.txtDoB.TabIndex = 5;
            this.txtDoB.TabStop = false;
            this.txtDoB.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSpouseName
            // 
            this.txtSpouseName.AllowSpace = true;
            this.txtSpouseName.AssociatedLookUpName = "";
            this.txtSpouseName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpouseName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSpouseName.ContinuationTextBox = null;
            this.txtSpouseName.CustomEnabled = true;
            this.txtSpouseName.DataFieldMapping = "PR_SPOUSE";
            this.txtSpouseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpouseName.GetRecordsOnUpDownKeys = false;
            this.txtSpouseName.IsDate = false;
            this.txtSpouseName.Location = new System.Drawing.Point(141, 100);
            this.txtSpouseName.Margin = new System.Windows.Forms.Padding(4);
            this.txtSpouseName.MaxLength = 10;
            this.txtSpouseName.Name = "txtSpouseName";
            this.txtSpouseName.NumberFormat = "###,###,##0.00";
            this.txtSpouseName.Postfix = "";
            this.txtSpouseName.Prefix = "";
            this.txtSpouseName.ReadOnly = true;
            this.txtSpouseName.Size = new System.Drawing.Size(251, 24);
            this.txtSpouseName.SkipValidation = false;
            this.txtSpouseName.TabIndex = 5;
            this.txtSpouseName.TabStop = false;
            this.txtSpouseName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNoofChild
            // 
            this.txtNoofChild.AllowSpace = true;
            this.txtNoofChild.AssociatedLookUpName = "";
            this.txtNoofChild.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoofChild.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoofChild.ContinuationTextBox = null;
            this.txtNoofChild.CustomEnabled = true;
            this.txtNoofChild.DataFieldMapping = "PR_NO_OF_CHILD";
            this.txtNoofChild.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoofChild.GetRecordsOnUpDownKeys = false;
            this.txtNoofChild.IsDate = false;
            this.txtNoofChild.Location = new System.Drawing.Point(141, 129);
            this.txtNoofChild.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoofChild.MaxLength = 5;
            this.txtNoofChild.Name = "txtNoofChild";
            this.txtNoofChild.NumberFormat = "###,###,##0.00";
            this.txtNoofChild.Postfix = "";
            this.txtNoofChild.Prefix = "";
            this.txtNoofChild.ReadOnly = true;
            this.txtNoofChild.Size = new System.Drawing.Size(57, 24);
            this.txtNoofChild.SkipValidation = false;
            this.txtNoofChild.TabIndex = 5;
            this.txtNoofChild.TabStop = false;
            this.txtNoofChild.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSex
            // 
            this.txtSex.AllowSpace = true;
            this.txtSex.AssociatedLookUpName = "";
            this.txtSex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSex.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSex.ContinuationTextBox = null;
            this.txtSex.CustomEnabled = true;
            this.txtSex.DataFieldMapping = "PR_SEX";
            this.txtSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSex.GetRecordsOnUpDownKeys = false;
            this.txtSex.IsDate = false;
            this.txtSex.Location = new System.Drawing.Point(773, 41);
            this.txtSex.Margin = new System.Windows.Forms.Padding(4);
            this.txtSex.MaxLength = 1;
            this.txtSex.Name = "txtSex";
            this.txtSex.NumberFormat = "###,###,##0.00";
            this.txtSex.Postfix = "";
            this.txtSex.Prefix = "";
            this.txtSex.ReadOnly = true;
            this.txtSex.Size = new System.Drawing.Size(54, 24);
            this.txtSex.SkipValidation = false;
            this.txtSex.TabIndex = 4;
            this.txtSex.TabStop = false;
            this.txtSex.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtNICNum
            // 
            this.txtNICNum.AllowSpace = true;
            this.txtNICNum.AssociatedLookUpName = "";
            this.txtNICNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNICNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNICNum.ContinuationTextBox = null;
            this.txtNICNum.CustomEnabled = true;
            this.txtNICNum.DataFieldMapping = "PR_ID_CARD_NO";
            this.txtNICNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNICNum.GetRecordsOnUpDownKeys = false;
            this.txtNICNum.IsDate = false;
            this.txtNICNum.Location = new System.Drawing.Point(551, 41);
            this.txtNICNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtNICNum.MaxLength = 1;
            this.txtNICNum.Name = "txtNICNum";
            this.txtNICNum.NumberFormat = "###,###,##0.00";
            this.txtNICNum.Postfix = "";
            this.txtNICNum.Prefix = "";
            this.txtNICNum.ReadOnly = true;
            this.txtNICNum.Size = new System.Drawing.Size(155, 24);
            this.txtNICNum.SkipValidation = false;
            this.txtNICNum.TabIndex = 4;
            this.txtNICNum.TabStop = false;
            this.txtNICNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblNICNum
            // 
            this.lblNICNum.AutoSize = true;
            this.lblNICNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNICNum.Location = new System.Drawing.Point(477, 46);
            this.lblNICNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNICNum.Name = "lblNICNum";
            this.lblNICNum.Size = new System.Drawing.Size(73, 17);
            this.lblNICNum.TabIndex = 1;
            this.lblNICNum.Text = "NIC No. :";
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSex.Location = new System.Drawing.Point(729, 46);
            this.lblSex.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(44, 17);
            this.lblSex.TabIndex = 1;
            this.lblSex.Text = "Sex :";
            this.lblSex.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDoB
            // 
            this.lblDoB.AutoSize = true;
            this.lblDoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDoB.Location = new System.Drawing.Point(39, 79);
            this.lblDoB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDoB.Name = "lblDoB";
            this.lblDoB.Size = new System.Drawing.Size(110, 17);
            this.lblDoB.TabIndex = 1;
            this.lblDoB.Text = "Date of Birth :";
            this.lblDoB.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSpName
            // 
            this.lblSpName.AutoSize = true;
            this.lblSpName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpName.Location = new System.Drawing.Point(32, 108);
            this.lblSpName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSpName.Name = "lblSpName";
            this.lblSpName.Size = new System.Drawing.Size(118, 17);
            this.lblSpName.TabIndex = 1;
            this.lblSpName.Text = "Spouse Name :";
            this.lblSpName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblNoOfChild
            // 
            this.lblNoOfChild.AutoSize = true;
            this.lblNoOfChild.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfChild.Location = new System.Drawing.Point(24, 138);
            this.lblNoOfChild.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNoOfChild.Name = "lblNoOfChild";
            this.lblNoOfChild.Size = new System.Drawing.Size(127, 17);
            this.lblNoOfChild.TabIndex = 1;
            this.lblNoOfChild.Text = "No. of Children :";
            this.lblNoOfChild.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(28, 186);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 17);
            this.label12.TabIndex = 1;
            this.label12.Text = "Children Name :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtMarriageDate
            // 
            this.txtMarriageDate.AllowSpace = true;
            this.txtMarriageDate.AssociatedLookUpName = "";
            this.txtMarriageDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarriageDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarriageDate.ContinuationTextBox = null;
            this.txtMarriageDate.CustomEnabled = true;
            this.txtMarriageDate.DataFieldMapping = "PR_MARRIAGE";
            this.txtMarriageDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarriageDate.GetRecordsOnUpDownKeys = false;
            this.txtMarriageDate.IsDate = false;
            this.txtMarriageDate.Location = new System.Drawing.Point(675, 70);
            this.txtMarriageDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtMarriageDate.MaxLength = 10;
            this.txtMarriageDate.Name = "txtMarriageDate";
            this.txtMarriageDate.NumberFormat = "###,###,##0.00";
            this.txtMarriageDate.Postfix = "";
            this.txtMarriageDate.Prefix = "";
            this.txtMarriageDate.ReadOnly = true;
            this.txtMarriageDate.Size = new System.Drawing.Size(153, 24);
            this.txtMarriageDate.SkipValidation = false;
            this.txtMarriageDate.TabIndex = 9;
            this.txtMarriageDate.TabStop = false;
            this.txtMarriageDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblMDate
            // 
            this.lblMDate.AutoSize = true;
            this.lblMDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMDate.Location = new System.Drawing.Point(564, 75);
            this.lblMDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMDate.Name = "lblMDate";
            this.lblMDate.Size = new System.Drawing.Size(121, 17);
            this.lblMDate.TabIndex = 8;
            this.lblMDate.Text = "Marraige Date :";
            this.lblMDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSpDoB
            // 
            this.txtSpDoB.AllowSpace = true;
            this.txtSpDoB.AssociatedLookUpName = "";
            this.txtSpDoB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpDoB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSpDoB.ContinuationTextBox = null;
            this.txtSpDoB.CustomEnabled = true;
            this.txtSpDoB.DataFieldMapping = "PR_BIRTH_SP";
            this.txtSpDoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpDoB.GetRecordsOnUpDownKeys = false;
            this.txtSpDoB.IsDate = false;
            this.txtSpDoB.Location = new System.Drawing.Point(675, 100);
            this.txtSpDoB.Margin = new System.Windows.Forms.Padding(4);
            this.txtSpDoB.MaxLength = 10;
            this.txtSpDoB.Name = "txtSpDoB";
            this.txtSpDoB.NumberFormat = "###,###,##0.00";
            this.txtSpDoB.Postfix = "";
            this.txtSpDoB.Prefix = "";
            this.txtSpDoB.ReadOnly = true;
            this.txtSpDoB.Size = new System.Drawing.Size(153, 24);
            this.txtSpDoB.SkipValidation = false;
            this.txtSpDoB.TabIndex = 11;
            this.txtSpDoB.TabStop = false;
            this.txtSpDoB.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblSpDoB
            // 
            this.lblSpDoB.AutoSize = true;
            this.lblSpDoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpDoB.Location = new System.Drawing.Point(520, 105);
            this.lblSpDoB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSpDoB.Name = "lblSpDoB";
            this.lblSpDoB.Size = new System.Drawing.Size(169, 17);
            this.lblSpDoB.TabIndex = 10;
            this.lblSpDoB.Text = "Spouse Date of Birth :";
            this.lblSpDoB.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDateofBirth
            // 
            this.lblDateofBirth.AutoSize = true;
            this.lblDateofBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateofBirth.Location = new System.Drawing.Point(573, 191);
            this.lblDateofBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateofBirth.Name = "lblDateofBirth";
            this.lblDateofBirth.Size = new System.Drawing.Size(110, 17);
            this.lblDateofBirth.TabIndex = 12;
            this.lblDateofBirth.Text = "Date of Birth :";
            // 
            // txtAddress2
            // 
            this.txtAddress2.AllowSpace = true;
            this.txtAddress2.AssociatedLookUpName = "";
            this.txtAddress2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAddress2.ContinuationTextBox = null;
            this.txtAddress2.CustomEnabled = true;
            this.txtAddress2.DataFieldMapping = "PR_ADD2";
            this.txtAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress2.GetRecordsOnUpDownKeys = false;
            this.txtAddress2.IsDate = false;
            this.txtAddress2.Location = new System.Drawing.Point(489, 12);
            this.txtAddress2.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddress2.MaxLength = 250;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.NumberFormat = "###,###,##0.00";
            this.txtAddress2.Postfix = "";
            this.txtAddress2.Prefix = "";
            this.txtAddress2.ReadOnly = true;
            this.txtAddress2.Size = new System.Drawing.Size(337, 24);
            this.txtAddress2.SkipValidation = false;
            this.txtAddress2.TabIndex = 14;
            this.txtAddress2.TabStop = false;
            this.txtAddress2.TextType = CrplControlLibrary.TextType.String;
            // 
            // dgvChildren
            // 
            this.dgvChildren.AllowUserToAddRows = false;
            this.dgvChildren.AllowUserToDeleteRows = false;
            this.dgvChildren.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChildren.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_CHILD_NAME});
            this.dgvChildren.ColumnToHide = null;
            this.dgvChildren.ColumnWidth = null;
            this.dgvChildren.CustomEnabled = true;
            this.dgvChildren.DisplayColumnWrapper = null;
            this.dgvChildren.GridDefaultRow = 0;
            this.dgvChildren.Location = new System.Drawing.Point(141, 160);
            this.dgvChildren.Margin = new System.Windows.Forms.Padding(4);
            this.dgvChildren.Name = "dgvChildren";
            this.dgvChildren.ReadOnlyColumns = null;
            this.dgvChildren.RequiredColumns = null;
            this.dgvChildren.Size = new System.Drawing.Size(307, 94);
            this.dgvChildren.SkippingColumns = null;
            this.dgvChildren.TabIndex = 15;
            // 
            // PR_CHILD_NAME
            // 
            this.PR_CHILD_NAME.DataPropertyName = "PR_CHILD_NAME";
            this.PR_CHILD_NAME.HeaderText = "";
            this.PR_CHILD_NAME.Name = "PR_CHILD_NAME";
            this.PR_CHILD_NAME.ReadOnly = true;
            this.PR_CHILD_NAME.Width = 225;
            // 
            // dgvChildDB
            // 
            this.dgvChildDB.AllowUserToAddRows = false;
            this.dgvChildDB.AllowUserToDeleteRows = false;
            this.dgvChildDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChildDB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_DATE_BIRTH});
            this.dgvChildDB.ColumnToHide = null;
            this.dgvChildDB.ColumnWidth = null;
            this.dgvChildDB.CustomEnabled = true;
            this.dgvChildDB.DisplayColumnWrapper = null;
            this.dgvChildDB.GridDefaultRow = 0;
            this.dgvChildDB.Location = new System.Drawing.Point(675, 129);
            this.dgvChildDB.Margin = new System.Windows.Forms.Padding(4);
            this.dgvChildDB.Name = "dgvChildDB";
            this.dgvChildDB.ReadOnlyColumns = null;
            this.dgvChildDB.RequiredColumns = null;
            this.dgvChildDB.Size = new System.Drawing.Size(152, 134);
            this.dgvChildDB.SkippingColumns = null;
            this.dgvChildDB.TabIndex = 16;
            // 
            // PR_DATE_BIRTH
            // 
            this.PR_DATE_BIRTH.DataPropertyName = "PR_DATE_BIRTH";
            this.PR_DATE_BIRTH.HeaderText = "";
            this.PR_DATE_BIRTH.Name = "PR_DATE_BIRTH";
            this.PR_DATE_BIRTH.ReadOnly = true;
            this.PR_DATE_BIRTH.Width = 82;
            // 
            // pnlPersonalDtl
            // 
            this.pnlPersonalDtl.ConcurrentPanels = null;
            this.pnlPersonalDtl.Controls.Add(this.dgvChildDB);
            this.pnlPersonalDtl.Controls.Add(this.dgvChildren);
            this.pnlPersonalDtl.Controls.Add(this.txtAddress2);
            this.pnlPersonalDtl.Controls.Add(this.lblDateofBirth);
            this.pnlPersonalDtl.Controls.Add(this.lblSpDoB);
            this.pnlPersonalDtl.Controls.Add(this.txtSpDoB);
            this.pnlPersonalDtl.Controls.Add(this.lblMDate);
            this.pnlPersonalDtl.Controls.Add(this.txtMarriageDate);
            this.pnlPersonalDtl.Controls.Add(this.lblMaritalStatus);
            this.pnlPersonalDtl.Controls.Add(this.txtMartStatus);
            this.pnlPersonalDtl.Controls.Add(this.label12);
            this.pnlPersonalDtl.Controls.Add(this.lblNoOfChild);
            this.pnlPersonalDtl.Controls.Add(this.lblSpName);
            this.pnlPersonalDtl.Controls.Add(this.lblDoB);
            this.pnlPersonalDtl.Controls.Add(this.lblSex);
            this.pnlPersonalDtl.Controls.Add(this.lblNICNum);
            this.pnlPersonalDtl.Controls.Add(this.txtNICNum);
            this.pnlPersonalDtl.Controls.Add(this.txtSex);
            this.pnlPersonalDtl.Controls.Add(this.txtNoofChild);
            this.pnlPersonalDtl.Controls.Add(this.txtSpouseName);
            this.pnlPersonalDtl.Controls.Add(this.txtDoB);
            this.pnlPersonalDtl.Controls.Add(this.txtPhoneNum2);
            this.pnlPersonalDtl.Controls.Add(this.txtPhoneNum);
            this.pnlPersonalDtl.Controls.Add(this.txtAddress);
            this.pnlPersonalDtl.Controls.Add(this.lblPhNum);
            this.pnlPersonalDtl.Controls.Add(this.lblAddress);
            this.pnlPersonalDtl.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPersonalDtl.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonalDtl.DependentPanels = null;
            this.pnlPersonalDtl.DisableDependentLoad = false;
            this.pnlPersonalDtl.EnableDelete = true;
            this.pnlPersonalDtl.EnableInsert = true;
            this.pnlPersonalDtl.EnableQuery = false;
            this.pnlPersonalDtl.EnableUpdate = true;
            this.pnlPersonalDtl.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlPersonalDtl.Location = new System.Drawing.Point(7, 473);
            this.pnlPersonalDtl.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPersonalDtl.MasterPanel = null;
            this.pnlPersonalDtl.Name = "pnlPersonalDtl";
            this.pnlPersonalDtl.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonalDtl.Size = new System.Drawing.Size(880, 268);
            this.pnlPersonalDtl.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.pnlPersonalDtl.TabIndex = 63;
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.AutoSize = true;
            this.lblMaritalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaritalStatus.Location = new System.Drawing.Point(355, 74);
            this.lblMaritalStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.Size = new System.Drawing.Size(118, 17);
            this.lblMaritalStatus.TabIndex = 6;
            this.lblMaritalStatus.Text = "Marital Status :";
            this.lblMaritalStatus.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtMartStatus
            // 
            this.txtMartStatus.AllowSpace = true;
            this.txtMartStatus.AssociatedLookUpName = "";
            this.txtMartStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMartStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMartStatus.ContinuationTextBox = null;
            this.txtMartStatus.CustomEnabled = true;
            this.txtMartStatus.DataFieldMapping = "PR_MARITAL";
            this.txtMartStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMartStatus.GetRecordsOnUpDownKeys = false;
            this.txtMartStatus.IsDate = false;
            this.txtMartStatus.Location = new System.Drawing.Point(464, 69);
            this.txtMartStatus.Margin = new System.Windows.Forms.Padding(4);
            this.txtMartStatus.MaxLength = 10;
            this.txtMartStatus.Name = "txtMartStatus";
            this.txtMartStatus.NumberFormat = "###,###,##0.00";
            this.txtMartStatus.Postfix = "";
            this.txtMartStatus.Prefix = "";
            this.txtMartStatus.ReadOnly = true;
            this.txtMartStatus.Size = new System.Drawing.Size(57, 24);
            this.txtMartStatus.SkipValidation = false;
            this.txtMartStatus.TabIndex = 7;
            this.txtMartStatus.TabStop = false;
            this.txtMartStatus.TextType = CrplControlLibrary.TextType.String;
            // 
            // PR_SEGMENT
            // 
            this.PR_SEGMENT.DataPropertyName = "PR_SEGMENT";
            this.PR_SEGMENT.FillWeight = 20F;
            this.PR_SEGMENT.HeaderText = "Seg.";
            this.PR_SEGMENT.Name = "PR_SEGMENT";
            this.PR_SEGMENT.ReadOnly = true;
            this.PR_SEGMENT.Width = 45;
            // 
            // PR_DEPT
            // 
            this.PR_DEPT.DataPropertyName = "PR_DEPT";
            this.PR_DEPT.FillWeight = 20F;
            this.PR_DEPT.HeaderText = "Dept.";
            this.PR_DEPT.Name = "PR_DEPT";
            this.PR_DEPT.ReadOnly = true;
            this.PR_DEPT.Width = 45;
            // 
            // PR_CONTRIB
            // 
            this.PR_CONTRIB.DataPropertyName = "PR_CONTRIB";
            this.PR_CONTRIB.FillWeight = 20F;
            this.PR_CONTRIB.HeaderText = "Contrib.";
            this.PR_CONTRIB.Name = "PR_CONTRIB";
            this.PR_CONTRIB.ReadOnly = true;
            this.PR_CONTRIB.Width = 55;
            // 
            // CostCode
            // 
            this.CostCode.DataPropertyName = "CostCode";
            this.CostCode.HeaderText = "CostCenter";
            this.CostCode.Name = "CostCode";
            // 
            // CHRIS_Query_EmployeeCurrentStatusQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(899, 818);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblPersonal);
            this.Controls.Add(this.pnlPersonalDtl);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.lblPersonnelHeader);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblHeader);
            this.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.Name = "CHRIS_Query_EmployeeCurrentStatusQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Employee Current Status Query";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Query_EmployeeCurrentStatusQuery_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblHeader, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.lblPersonnelHeader, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.pnlPersonalDtl, 0);
            this.Controls.SetChildIndex(this.lblPersonal, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChildDB)).EndInit();
            this.pnlPersonalDtl.ResumeLayout(false);
            this.pnlPersonalDtl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPersonnelHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Label lblBranch;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label lblLevel;
        private CrplControlLibrary.SLTextBox txtLastName;
        private System.Windows.Forms.Label lblCategory;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label lblDesig;
        private System.Windows.Forms.Label lblPrNum;
        private System.Windows.Forms.Label lblName;
        private CrplControlLibrary.SLTextBox txtNextAppr;
        private CrplControlLibrary.SLTextBox txtLastAppr;
        private CrplControlLibrary.SLTextBox txtConfirmDate;
        private CrplControlLibrary.SLTextBox txtConfirm;
        private CrplControlLibrary.SLTextBox txtJoinDate;
        private CrplControlLibrary.SLTextBox txtTitle;
        private CrplControlLibrary.SLTextBox txtFunctional;
        private CrplControlLibrary.SLTextBox txtCategory;
        private System.Windows.Forms.Label lblNxtAppr;
        private System.Windows.Forms.Label lblLstAppr;
        private System.Windows.Forms.Label lblConfirmDate;
        private System.Windows.Forms.Label lblConfirm;
        private System.Windows.Forms.Label lblJoinDate;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblFunctional;
        private System.Windows.Forms.Label lblGEIDNum;
        private CrplControlLibrary.SLTextBox txtGEID;
        private System.Windows.Forms.Label lblEducation;
        private CrplControlLibrary.SLTextBox txtEducation;
        private System.Windows.Forms.Label lblAccNum;
        private CrplControlLibrary.SLTextBox txtAccNum;
        private System.Windows.Forms.Label lblNatTaxNum;
        private CrplControlLibrary.SLTextBox txtNatTaxNum;
        private System.Windows.Forms.Label lblAnnPkg;
        private CrplControlLibrary.SLTextBox txtAnnPkge;
        private System.Windows.Forms.Label lblPersonal;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblPhNum;
        private CrplControlLibrary.SLTextBox txtAddress;
        private CrplControlLibrary.SLTextBox txtPhoneNum;
        private CrplControlLibrary.SLTextBox txtPhoneNum2;
        private CrplControlLibrary.SLTextBox txtDoB;
        private CrplControlLibrary.SLTextBox txtSpouseName;
        private CrplControlLibrary.SLTextBox txtNoofChild;
        private CrplControlLibrary.SLTextBox txtSex;
        private CrplControlLibrary.SLTextBox txtNICNum;
        private System.Windows.Forms.Label lblNICNum;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.Label lblDoB;
        private System.Windows.Forms.Label lblSpName;
        private System.Windows.Forms.Label lblNoOfChild;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtMarriageDate;
        private System.Windows.Forms.Label lblMDate;
        private CrplControlLibrary.SLTextBox txtSpDoB;
        private System.Windows.Forms.Label lblSpDoB;
        private System.Windows.Forms.Label lblDateofBirth;
        private CrplControlLibrary.SLTextBox txtAddress2;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvChildren;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvChildDB;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonalDtl;
        private System.Windows.Forms.Label lblMaritalStatus;
        private CrplControlLibrary.SLTextBox txtMartStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DATE_BIRTH;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CHILD_NAME;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTxtLocation;
        private COMMON.SLCONTROLS.SLDataGridView dgvSDC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_SEGMENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DEPT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CONTRIB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CostCode;
    }
}