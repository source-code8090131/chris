using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_SalarySettlementOfficer : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_SalarySettlementOfficer()
        {
            InitializeComponent();
        }
        
        public CHRIS_PayrollReports_SalarySettlementOfficer(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.copies.Text = "1";
            this.mode.Items.RemoveAt(this.mode.Items.Count - 1);
        }


        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
//            Dest_Type.Items.RemoveAt(5);
            w_branch.Text = "KHI";
            mode.Text = "Character";
            w_seg.Text = "ALL";
            w_dept.Text = "ALL";
            month.Text = "7";
            wyear.Text = "2004";
            fpno.Text = "1";
            tpno.Text = "99999";
            this.Dest_Format.Text = "wide";

        }
        private void button1_Click(object sender, EventArgs e)
        {
               


            {

                base.RptFileName = "py011_b";
                //base.btnCallReport_Click(sender, e);
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.copies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;
                    string DestFormat;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }                

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                        base.ExportCustomReport(DestName, "pdf");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    pBar.Visible = true;
                    //string DestName = @"C:\\SL_REPORT\";
                    string DestFormat;
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                    int _currentIndx = 0, _endIndx = 0;
                    
                    string strFromNoTemp = fpno.Text,strToNoTemp = tpno.Text;

                    
                    
                    Int32.TryParse(fpno.Text, out _currentIndx);
                    Int32.TryParse(tpno.Text, out _endIndx);

                    if (_currentIndx == 0)
                    {
                        MessageBox.Show("From P.No. cannot be 0");
                        fpno.Select();
                        return;
                    }
                    else if (_endIndx == 0)
                    {
                        MessageBox.Show("To P.No. cannot be 0");
                        tpno.Select();
                        return;
                    }

                    if (_endIndx < _currentIndx)
                    {
                        MessageBox.Show("To P.No. cannot be less than From P.No.");
                        tpno.Select();
                        return;
                    }

                    iCORE.CHRIS.DATAOBJECTS.CmnDataManager cmnDM = new iCORE.CHRIS.DATAOBJECTS.CmnDataManager();
                    
                   

                    rslt = cmnDM.GetData("CHRIS_SP_PAYROLL_SAL", "GetSMTPSettings");

                    if (!rslt.isSuccessful || rslt.dstResult.Tables[0].Rows.Count <= 0)
                    {
                        MessageBox.Show(rslt.message);
                        return;
                    }
                    else if (rslt.dstResult.Tables[0].Rows.Count <= 0)
                    {
                        MessageBox.Show("Please configure SMTP settings");
                        return;
                    }
                    
                    //Set SMTP Server Settings in Variables
                    string strProfileName = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    string strSharedFolderPath = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    string strSubject = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    string strBody = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();

                    //if (strProfileName == String.Empty)
                    //{
                    //    MessageBox.Show("Profile Name is Empty. Kindly check SMTP settings");
                    //    return;
                    //}
                    //else 

                    if (strSharedFolderPath == String.Empty)
                    {
                        MessageBox.Show("Shared Folder Path is Empty. Kindly check SMTP settings");
                        return;
                    }

                    try
                    {
                        System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(strSharedFolderPath);
                    }

                    catch (UnauthorizedAccessException)
                    {
                        MessageBox.Show("Permissions denied for shared folder path:" + strSharedFolderPath + ". Kindly check SMTP settings");
                        return;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Exception:" + ex.Message + ". Kindly check SMTP settings");
                        return;
                    }

                    Dictionary<string, object> dictParam = new Dictionary<string, object>();

                    dictParam.Add("PR_P_NO", 1);
                    dictParam.Add("Month", month.Text);
                    dictParam.Add("Year", wyear.Text);
                    dictParam.Add("PR_P_NO_FROM", _currentIndx);
                    dictParam.Add("PR_P_NO_TO", _endIndx + 1);
                    dictParam.Add("PR_BRANCH", w_branch.Text);
                    Result rslt2 = cmnDM.GetData("CHRIS_SP_PAYROLL_SAL", "GetMaxMinPNo", dictParam);
                    if (!rslt2.isSuccessful || rslt2.dstResult.Tables[0].Rows.Count <= 0)
                        MessageBox.Show(rslt2.message);

                    pBar.Minimum = Int32.Parse(rslt2.dstResult.Tables[0].Rows[0]["PR_P_NO"].ToString());
                    pBar.Maximum = Int32.Parse(rslt2.dstResult.Tables[0].Rows[rslt2.dstResult.Tables[0].Rows.Count - 1]["PR_P_NO"].ToString());

                    dictParam = new Dictionary<string, object>();

                    dictParam.Add("PR_P_NO", _currentIndx);
                    dictParam.Add("Month", month.Text);
                    dictParam.Add("Year", wyear.Text);
                    dictParam.Add("PR_P_NO_FROM", 1);
                    dictParam.Add("PR_P_NO_TO", 1);

                   
                    string dirPath = strSharedFolderPath+"\\SL_REPORTS";

                    if(!Directory.Exists(dirPath))
                        Directory.CreateDirectory(dirPath);

                    try
                    {
                        String DestName="";
                        foreach (DataRow dr in rslt2.dstResult.Tables[0].Rows)
                        {
                            dictParam.Remove("PR_P_NO");
                            dictParam.Add("PR_P_NO", Int32.Parse(dr["PR_P_NO"].ToString()));
                            fpno.Text = tpno.Text = dr["PR_P_NO"].ToString();
                            
                            rslt = cmnDM.GetData("CHRIS_SP_PAYROLL_SAL", "GetEmpEmail", dictParam);

                            if (rslt.isSuccessful && rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != String.Empty)
                            {
                                string text6 = this.rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                                string text7 = this.rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                                string filePassword = this.fpno.Text + text7;
                                string destinationFile = string.Concat(new string[]
										{
											dirPath,
											"\\",
											dr["GEI_GEID_NO"].ToString(),
											"_",
											this.wyear.Text,
											"-",
											this.month.Text,
											".pdf"
										});
                                if (dictParam.ContainsKey("EMAIL"))
                                {
                                    dictParam["EMAIL"] = text6;
                                    dictParam["CNIC"] = text7;
                                    dictParam["GEID"] = dr["GEI_GEID_NO"].ToString();
                                }
                                else
                                {
                                    dictParam.Add("EMAIL", text6);
                                    dictParam.Add("CNIC", text7);
                                    dictParam.Add("GEID", dr["GEI_GEID_NO"].ToString());
                                }



                                rslt = cmnDM.GetData("CHRIS_SP_ADD_EMAIL", "", dictParam);
                                base.EmailToReport(destinationFile, "pdf", strProfileName, strSubject, strBody, text6, filePassword);
                            }
                            pBar.Value = Int32.Parse(dr["PR_P_NO"].ToString());
                            pBar.Refresh();
                        }
                        //for (; _currentIndx <= _endIndx; _currentIndx++)
                        //{
                        //    dictParam.Remove("PR_P_NO");
                        //    dictParam.Add("PR_P_NO", _currentIndx);
                        //    fpno.Text = tpno.Text = _currentIndx.ToString();
                            
                        //    rslt = cmnDM.GetData("CHRIS_SP_PAYROLL_SAL", "GetEmpEmail", dictParam);

                        //    if (rslt.isSuccessful && rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != String.Empty)
                        //    {
                        //        String strToEmailAdd = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        //        DestName = dirPath + "\\PRP" + _currentIndx + "_" + wyear.Text + "-" + month.Text + ".pdf";
                        //        dictParam.Remove("EMAIL");
                        //        dictParam.Add("EMAIL", strToEmailAdd);
                        //        rslt = cmnDM.GetData("CHRIS_SP_ADD_EMAIL", "", dictParam);
                        //        base.EmailToReport(DestName, "pdf", strProfileName, strSubject, strBody, strToEmailAdd);
                        //    }
                        //    pBar.Value = _currentIndx +1;
                        //    pBar.Refresh();
                        //}
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                    finally
                    {
                        fpno.Text = strFromNoTemp;
                        tpno.Text = strToNoTemp;
                    }
                    //if(Directory.Exists(dirPath))
                    //    Directory.Delete(dirPath, true);
                  
                    MessageBox.Show("Send Email Process Completed","Success",MessageBoxButtons.OK,MessageBoxIcon.None);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            //base.Close();
        }

        

    }
}