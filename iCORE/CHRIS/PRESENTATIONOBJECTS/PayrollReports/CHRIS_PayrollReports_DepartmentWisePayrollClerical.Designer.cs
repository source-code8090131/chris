namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_DepartmentWisePayrollClerical
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_DepartmentWisePayrollClerical));
            this.Close = new System.Windows.Forms.Button();
            this.Run = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.Copies = new CrplControlLibrary.SLTextBox(this.components);
            this.MONTH = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(240, 298);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 10;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Run
            // 
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(141, 298);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 9;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.YEAR);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.SEG);
            this.groupBox1.Controls.Add(this.BRANCH);
            this.groupBox1.Controls.Add(this.Copies);
            this.groupBox1.Controls.Add(this.MONTH);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(14, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 356);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(405, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // YEAR
            // 
            this.YEAR.AllowSpace = true;
            this.YEAR.AssociatedLookUpName = "";
            this.YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.YEAR.ContinuationTextBox = null;
            this.YEAR.CustomEnabled = true;
            this.YEAR.DataFieldMapping = "";
            this.YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YEAR.GetRecordsOnUpDownKeys = false;
            this.YEAR.IsDate = false;
            this.YEAR.Location = new System.Drawing.Point(141, 266);
            this.YEAR.MaxLength = 4;
            this.YEAR.Name = "YEAR";
            this.YEAR.NumberFormat = "###,###,##0.00";
            this.YEAR.Postfix = "";
            this.YEAR.Prefix = "";
            this.YEAR.Size = new System.Drawing.Size(174, 20);
            this.YEAR.SkipValidation = false;
            this.YEAR.TabIndex = 8;
            this.YEAR.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 263);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Enter Year (YYYY)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Enter Valid Branch";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Enter Month (MM)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Enter Valid Segment";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Copies";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Desformat";
            // 
            // SEG
            // 
            this.SEG.AllowSpace = true;
            this.SEG.AssociatedLookUpName = "";
            this.SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SEG.ContinuationTextBox = null;
            this.SEG.CustomEnabled = true;
            this.SEG.DataFieldMapping = "";
            this.SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SEG.GetRecordsOnUpDownKeys = false;
            this.SEG.IsDate = false;
            this.SEG.Location = new System.Drawing.Point(141, 239);
            this.SEG.MaxLength = 3;
            this.SEG.Name = "SEG";
            this.SEG.NumberFormat = "###,###,##0.00";
            this.SEG.Postfix = "";
            this.SEG.Prefix = "";
            this.SEG.Size = new System.Drawing.Size(174, 20);
            this.SEG.SkipValidation = false;
            this.SEG.TabIndex = 7;
            this.SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // BRANCH
            // 
            this.BRANCH.AllowSpace = true;
            this.BRANCH.AssociatedLookUpName = "";
            this.BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BRANCH.ContinuationTextBox = null;
            this.BRANCH.CustomEnabled = true;
            this.BRANCH.DataFieldMapping = "";
            this.BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRANCH.GetRecordsOnUpDownKeys = false;
            this.BRANCH.IsDate = false;
            this.BRANCH.Location = new System.Drawing.Point(141, 214);
            this.BRANCH.MaxLength = 3;
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.NumberFormat = "###,###,##0.00";
            this.BRANCH.Postfix = "";
            this.BRANCH.Prefix = "";
            this.BRANCH.Size = new System.Drawing.Size(174, 20);
            this.BRANCH.SkipValidation = false;
            this.BRANCH.TabIndex = 6;
            this.BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // Copies
            // 
            this.Copies.AllowSpace = true;
            this.Copies.AssociatedLookUpName = "";
            this.Copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Copies.ContinuationTextBox = null;
            this.Copies.CustomEnabled = true;
            this.Copies.DataFieldMapping = "";
            this.Copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Copies.GetRecordsOnUpDownKeys = false;
            this.Copies.IsDate = false;
            this.Copies.Location = new System.Drawing.Point(141, 163);
            this.Copies.MaxLength = 2;
            this.Copies.Name = "Copies";
            this.Copies.NumberFormat = "###,###,##0.00";
            this.Copies.Postfix = "";
            this.Copies.Prefix = "";
            this.Copies.Size = new System.Drawing.Size(174, 20);
            this.Copies.SkipValidation = false;
            this.Copies.TabIndex = 4;
            this.Copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // MONTH
            // 
            this.MONTH.AllowSpace = true;
            this.MONTH.AssociatedLookUpName = "";
            this.MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.MONTH.ContinuationTextBox = null;
            this.MONTH.CustomEnabled = true;
            this.MONTH.DataFieldMapping = "";
            this.MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MONTH.GetRecordsOnUpDownKeys = false;
            this.MONTH.IsDate = false;
            this.MONTH.Location = new System.Drawing.Point(141, 189);
            this.MONTH.MaxLength = 2;
            this.MONTH.Name = "MONTH";
            this.MONTH.NumberFormat = "###,###,##0.00";
            this.MONTH.Postfix = "";
            this.MONTH.Prefix = "";
            this.MONTH.Size = new System.Drawing.Size(174, 20);
            this.MONTH.SkipValidation = false;
            this.MONTH.TabIndex = 5;
            this.MONTH.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(141, 138);
            this.Dest_Format.MaxLength = 50;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(174, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 3;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Destype";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(141, 112);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(174, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(141, 85);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(174, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(138, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 12;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(177, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 16);
            this.label8.TabIndex = 11;
            this.label8.Text = "Report Parameter";
            // 
            // CHRIS_PayrollReports_DepartmentWisePayrollClerical
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 410);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_DepartmentWisePayrollClerical";
            this.Text = "iCORE CHRIS - DEPARTMENT WISE PAYROLL (CLERICAL)";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Run;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox YEAR;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox SEG;
        private CrplControlLibrary.SLTextBox BRANCH;
        private CrplControlLibrary.SLTextBox Copies;
        private CrplControlLibrary.SLTextBox MONTH;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.PictureBox pictureBox2;

    }
}