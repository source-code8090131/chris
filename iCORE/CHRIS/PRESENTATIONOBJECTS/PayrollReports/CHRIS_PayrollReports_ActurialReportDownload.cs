using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ActurialReportDownload : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_ActurialReportDownload()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PayrollReports_ActurialReportDownload(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //Dest_Type.Items.RemoveAt(5);
            Dest_name.Text = @"C:\iCORE-Spool\GROO2.LIS";
        }

    

        //private void CHRIS_PayrollReport_ActurialReport_Load(object sender, EventArgs e)
        //{

        //}

        private void btnRun_Click(object sender, EventArgs e)
        {

            string d = "C:\\iCORE-Spool\\GROO2.LIS";
                base.RptFileName = "GR002ADOWNLOAD";
                if (this.Dest_name.Text != string.Empty)
                    d = this.Dest_name.Text;
                    base.ExportCustomReport(d, "pdf");

                #region commented code


                //if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                //{

                //    base.btnCallReport_Click(sender, e);

                //}
                //if (Dest_Type.Text == "Printer")
                //{

                //    PrintCustomReport();

                //}


                //if (Dest_Type.Text == "File")
                //{
                //    if (DestFormat != string.Empty && DestName != string.Empty)
                //    {
                //        base.ExportCustomReport(DestName, DestFormat);
                //    }

                //}


                //if (Dest_Type.Text == "Mail")
                //{

                //    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //    if (DestFormat != string.Empty && DestName != string.Empty)
                
                //    {
                //        base.EmailToReport(@"C:\Report", "PDF");
                //    }
                //}
                #endregion commented code

            }
        



         
        

        private void btnClose_Click(object sender, EventArgs e)
        {

            base.btnCloseReport_Click(sender, e);

        }

    }
}