namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_ActurialReportDownload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_ActurialReportDownload));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.RUNDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.btnRun = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.W_CATE = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.WSEG = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.WBRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.RUNDATE);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.W_CATE);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.WSEG);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.WBRANCH);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 233);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 89;
            this.label8.Text = "Desname";
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(194, 68);
            this.Dest_name.MaxLength = 80;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(156, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(195, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 78;
            this.label6.Text = "Report Parameters";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(276, 194);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(400, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(165, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 13);
            this.label7.TabIndex = 79;
            this.label7.Text = "Enter values for the parameters";
            // 
            // RUNDATE
            // 
            this.RUNDATE.CustomEnabled = true;
            this.RUNDATE.CustomFormat = "dd/MM/yyyy";
            this.RUNDATE.DataFieldMapping = "";
            this.RUNDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RUNDATE.HasChanges = true;
            this.RUNDATE.Location = new System.Drawing.Point(194, 167);
            this.RUNDATE.Name = "RUNDATE";
            this.RUNDATE.NullValue = " ";
            this.RUNDATE.Size = new System.Drawing.Size(156, 20);
            this.RUNDATE.TabIndex = 5;
            this.RUNDATE.Value = new System.DateTime(2010, 12, 6, 0, 0, 0, 0);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(194, 194);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(73, 23);
            this.btnRun.TabIndex = 6;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 87;
            this.label5.Text = "Enter Date   :";
            // 
            // W_CATE
            // 
            this.W_CATE.AllowSpace = true;
            this.W_CATE.AssociatedLookUpName = "";
            this.W_CATE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_CATE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_CATE.ContinuationTextBox = null;
            this.W_CATE.CustomEnabled = true;
            this.W_CATE.DataFieldMapping = "";
            this.W_CATE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_CATE.GetRecordsOnUpDownKeys = false;
            this.W_CATE.IsDate = false;
            this.W_CATE.Location = new System.Drawing.Point(194, 142);
            this.W_CATE.MaxLength = 1;
            this.W_CATE.Name = "W_CATE";
            this.W_CATE.NumberFormat = "###,###,##0.00";
            this.W_CATE.Postfix = "";
            this.W_CATE.Prefix = "";
            this.W_CATE.Size = new System.Drawing.Size(156, 20);
            this.W_CATE.SkipValidation = false;
            this.W_CATE.TabIndex = 4;
            this.W_CATE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(76, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 85;
            this.label4.Text = "[Blank]=Officer";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 84;
            this.label1.Text = "C=Clerks";
            // 
            // WSEG
            // 
            this.WSEG.AllowSpace = true;
            this.WSEG.AssociatedLookUpName = "";
            this.WSEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WSEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.WSEG.ContinuationTextBox = null;
            this.WSEG.CustomEnabled = true;
            this.WSEG.DataFieldMapping = "";
            this.WSEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WSEG.GetRecordsOnUpDownKeys = false;
            this.WSEG.IsDate = false;
            this.WSEG.Location = new System.Drawing.Point(194, 117);
            this.WSEG.MaxLength = 3;
            this.WSEG.Name = "WSEG";
            this.WSEG.NumberFormat = "###,###,##0.00";
            this.WSEG.Postfix = "";
            this.WSEG.Prefix = "";
            this.WSEG.Size = new System.Drawing.Size(156, 20);
            this.WSEG.SkipValidation = false;
            this.WSEG.TabIndex = 3;
            this.WSEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 82;
            this.label2.Text = "Enter GF/GCB   :";
            // 
            // WBRANCH
            // 
            this.WBRANCH.AllowSpace = true;
            this.WBRANCH.AssociatedLookUpName = "";
            this.WBRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WBRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.WBRANCH.ContinuationTextBox = null;
            this.WBRANCH.CustomEnabled = true;
            this.WBRANCH.DataFieldMapping = "";
            this.WBRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WBRANCH.GetRecordsOnUpDownKeys = false;
            this.WBRANCH.IsDate = false;
            this.WBRANCH.Location = new System.Drawing.Point(194, 93);
            this.WBRANCH.MaxLength = 3;
            this.WBRANCH.Name = "WBRANCH";
            this.WBRANCH.NumberFormat = "###,###,##0.00";
            this.WBRANCH.Postfix = "";
            this.WBRANCH.Prefix = "";
            this.WBRANCH.Size = new System.Drawing.Size(156, 20);
            this.WBRANCH.SkipValidation = false;
            this.WBRANCH.TabIndex = 2;
            this.WBRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 13);
            this.label3.TabIndex = 80;
            this.label3.Text = "Enter Branch or ALL   :";
            // 
            // CHRIS_PayrollReports_ActurialReportDownload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 279);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_ActurialReportDownload";
            this.Text = "iCORE CHRIS - Acturial Report Download";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox WBRANCH;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox WSEG;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox W_CATE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnClose;
        private CrplControlLibrary.SLDatePicker RUNDATE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox Dest_name;
    }
}