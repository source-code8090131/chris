using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_MonthlySalaryInter : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_MonthlySalaryInter()
        {
            InitializeComponent();
        }

        public CHRIS_PayrollReports_MonthlySalaryInter(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.copies.Text = "1";
            this.Year.Text = "1993";
        }


       
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PY015";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.copies.Text );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                DestName = @"C:\iCORE-Spool\Report";
                    base.ExportCustomReport(DestName, "PDF");

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                RecipentName = "";

                    base.EmailToReport(DestName, "PDF", RecipentName);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
         

        
    }
}