//<<<<<<< .mine
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_FTEDepartmentHeadsGroupWise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.w_group = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.W_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.w_group);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.W_DATE);
            this.groupBox1.Controls.Add(this.w_seg);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.w_brn);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(412, 344);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // w_group
            // 
            this.w_group.AllowSpace = true;
            this.w_group.AssociatedLookUpName = "";
            this.w_group.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_group.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_group.ContinuationTextBox = null;
            this.w_group.CustomEnabled = true;
            this.w_group.DataFieldMapping = "";
            this.w_group.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_group.GetRecordsOnUpDownKeys = false;
            this.w_group.IsDate = false;
            this.w_group.Location = new System.Drawing.Point(147, 246);
            this.w_group.MaxLength = 3;
            this.w_group.Name = "w_group";
            this.w_group.NumberFormat = "###,###,##0.00";
            this.w_group.Postfix = "";
            this.w_group.Prefix = "";
            this.w_group.Size = new System.Drawing.Size(128, 20);
            this.w_group.SkipValidation = false;
            this.w_group.TabIndex = 7;
            this.w_group.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 248);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(118, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Enter Group Or ALL";
            // 
            // W_DATE
            // 
            this.W_DATE.CustomEnabled = true;
            this.W_DATE.CustomFormat = "dd/MM/yyyy";
            this.W_DATE.DataFieldMapping = "";
            this.W_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.W_DATE.HasChanges = true;
            this.W_DATE.Location = new System.Drawing.Point(147, 194);
            this.W_DATE.Name = "W_DATE";
            this.W_DATE.NullValue = " ";
            this.W_DATE.Size = new System.Drawing.Size(128, 20);
            this.W_DATE.TabIndex = 5;
            this.W_DATE.Value = new System.DateTime(2011, 1, 14, 0, 0, 0, 0);
            // 
            // w_seg
            // 
            this.w_seg.AllowSpace = true;
            this.w_seg.AssociatedLookUpName = "";
            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_seg.ContinuationTextBox = null;
            this.w_seg.CustomEnabled = true;
            this.w_seg.DataFieldMapping = "";
            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_seg.GetRecordsOnUpDownKeys = false;
            this.w_seg.IsDate = false;
            this.w_seg.Location = new System.Drawing.Point(147, 220);
            this.w_seg.MaxLength = 3;
            this.w_seg.Name = "w_seg";
            this.w_seg.NumberFormat = "###,###,##0.00";
            this.w_seg.Postfix = "";
            this.w_seg.Prefix = "";
            this.w_seg.Size = new System.Drawing.Size(128, 20);
            this.w_seg.SkipValidation = false;
            this.w_seg.TabIndex = 6;
            this.w_seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 222);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Enter GF/GCB/ALL";
            // 
            // w_brn
            // 
            this.w_brn.AllowSpace = true;
            this.w_brn.AssociatedLookUpName = "";
            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_brn.ContinuationTextBox = null;
            this.w_brn.CustomEnabled = true;
            this.w_brn.DataFieldMapping = "";
            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_brn.GetRecordsOnUpDownKeys = false;
            this.w_brn.IsDate = false;
            this.w_brn.Location = new System.Drawing.Point(147, 168);
            this.w_brn.MaxLength = 3;
            this.w_brn.Name = "w_brn";
            this.w_brn.NumberFormat = "###,###,##0.00";
            this.w_brn.Postfix = "";
            this.w_brn.Prefix = "";
            this.w_brn.Size = new System.Drawing.Size(128, 20);
            this.w_brn.SkipValidation = false;
            this.w_brn.TabIndex = 4;
            this.w_brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Enter Branch or ALL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "As Of";
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(147, 63);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(128, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(212, 282);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(147, 142);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(128, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 3;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(145, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(147, 117);
            this.Dest_Format.MaxLength = 40;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(128, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 2;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(147, 90);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(128, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "No. of Copies";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Destination Format";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Destination Type";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
            this.pictureBox1.Location = new System.Drawing.Point(374, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 64);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(116, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(209, 15);
            this.label9.TabIndex = 26;
            this.label9.Text = "Enter values for the Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(158, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 15);
            this.label8.TabIndex = 25;
            this.label8.Text = "Report Parameters";
            // 
            // CHRIS_PayrollReports_FTEDepartmentHeadsGroupWise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 395);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_FTEDepartmentHeadsGroupWise";
            this.Text = "CHRIS PayrollReports Monthly Tax Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox w_seg;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox w_brn;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLDatePicker W_DATE;
        private CrplControlLibrary.SLTextBox w_group;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}//=======
//namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
//{
//    partial class CHRIS_PayrollReports_FTEDesignationGroup
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            this.groupBox1 = new System.Windows.Forms.GroupBox();
//            this.w_grp = new CrplControlLibrary.SLTextBox(this.components);
//            this.label10 = new System.Windows.Forms.Label();
//            this.W_DATE = new CrplControlLibrary.SLDatePicker(this.components);
//            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
//            this.label7 = new System.Windows.Forms.Label();
//            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
//            this.label6 = new System.Windows.Forms.Label();
//            this.label5 = new System.Windows.Forms.Label();
//            this.Dest_Type = new CrplControlLibrary.SLComboBox();
//            this.pictureBox1 = new System.Windows.Forms.PictureBox();
//            this.button2 = new System.Windows.Forms.Button();
//            this.copies = new CrplControlLibrary.SLTextBox(this.components);
//            this.button1 = new System.Windows.Forms.Button();
//            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
//            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
//            this.label9 = new System.Windows.Forms.Label();
//            this.label8 = new System.Windows.Forms.Label();
//            this.label4 = new System.Windows.Forms.Label();
//            this.label3 = new System.Windows.Forms.Label();
//            this.label2 = new System.Windows.Forms.Label();
//            this.label1 = new System.Windows.Forms.Label();
//            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
//            this.groupBox1.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // groupBox1
//            // 
//            this.groupBox1.Controls.Add(this.w_grp);
//            this.groupBox1.Controls.Add(this.label10);
//            this.groupBox1.Controls.Add(this.W_DATE);
//            this.groupBox1.Controls.Add(this.w_seg);
//            this.groupBox1.Controls.Add(this.label7);
//            this.groupBox1.Controls.Add(this.w_brn);
//            this.groupBox1.Controls.Add(this.label6);
//            this.groupBox1.Controls.Add(this.label5);
//            this.groupBox1.Controls.Add(this.Dest_Type);
//            this.groupBox1.Controls.Add(this.pictureBox1);
//            this.groupBox1.Controls.Add(this.button2);
//            this.groupBox1.Controls.Add(this.copies);
//            this.groupBox1.Controls.Add(this.button1);
//            this.groupBox1.Controls.Add(this.Dest_Format);
//            this.groupBox1.Controls.Add(this.Dest_name);
//            this.groupBox1.Controls.Add(this.label9);
//            this.groupBox1.Controls.Add(this.label8);
//            this.groupBox1.Controls.Add(this.label4);
//            this.groupBox1.Controls.Add(this.label3);
//            this.groupBox1.Controls.Add(this.label2);
//            this.groupBox1.Controls.Add(this.label1);
//            this.groupBox1.Location = new System.Drawing.Point(12, 39);
//            this.groupBox1.Name = "groupBox1";
//            this.groupBox1.Size = new System.Drawing.Size(422, 344);
//            this.groupBox1.TabIndex = 0;
//            this.groupBox1.TabStop = false;
//            // 
//            // w_grp
//            // 
//            this.w_grp.AllowSpace = true;
//            this.w_grp.AssociatedLookUpName = "";
//            this.w_grp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.w_grp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.w_grp.ContinuationTextBox = null;
//            this.w_grp.CustomEnabled = true;
//            this.w_grp.DataFieldMapping = "";
//            this.w_grp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.w_grp.GetRecordsOnUpDownKeys = false;
//            this.w_grp.IsDate = false;
//            this.w_grp.Location = new System.Drawing.Point(232, 277);
//            this.w_grp.MaxLength = 3;
//            this.w_grp.Name = "w_grp";
//            this.w_grp.NumberFormat = "###,###,##0.00";
//            this.w_grp.Postfix = "";
//            this.w_grp.Prefix = "";
//            this.w_grp.Size = new System.Drawing.Size(128, 20);
//            this.w_grp.SkipValidation = false;
//            this.w_grp.TabIndex = 7;
//            this.w_grp.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label10
//            // 
//            this.label10.AutoSize = true;
//            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label10.Location = new System.Drawing.Point(52, 279);
//            this.label10.Name = "label10";
//            this.label10.Size = new System.Drawing.Size(140, 13);
//            this.label10.TabIndex = 24;
//            this.label10.Text = "Enter Valid Group or All";
//            // 
//            // W_DATE
//            // 
//            this.W_DATE.CustomEnabled = true;
//            this.W_DATE.CustomFormat = "dd/MM/yyyy";
//            this.W_DATE.DataFieldMapping = "";
//            this.W_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
//            this.W_DATE.Location = new System.Drawing.Point(232, 225);
//            this.W_DATE.Name = "W_DATE";
//            this.W_DATE.NullValue = " ";
//            this.W_DATE.Size = new System.Drawing.Size(128, 20);
//            this.W_DATE.TabIndex = 5;
//            this.W_DATE.Value = new System.DateTime(2011, 1, 14, 10, 9, 54, 428);
//            // 
//            // w_seg
//            // 
//            this.w_seg.AllowSpace = true;
//            this.w_seg.AssociatedLookUpName = "";
//            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.w_seg.ContinuationTextBox = null;
//            this.w_seg.CustomEnabled = true;
//            this.w_seg.DataFieldMapping = "";
//            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.w_seg.GetRecordsOnUpDownKeys = false;
//            this.w_seg.IsDate = false;
//            this.w_seg.Location = new System.Drawing.Point(232, 251);
//            this.w_seg.MaxLength = 3;
//            this.w_seg.Name = "w_seg";
//            this.w_seg.NumberFormat = "###,###,##0.00";
//            this.w_seg.Postfix = "";
//            this.w_seg.Prefix = "";
//            this.w_seg.Size = new System.Drawing.Size(128, 20);
//            this.w_seg.SkipValidation = false;
//            this.w_seg.TabIndex = 6;
//            this.w_seg.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label7
//            // 
//            this.label7.AutoSize = true;
//            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label7.Location = new System.Drawing.Point(52, 253);
//            this.label7.Name = "label7";
//            this.label7.Size = new System.Drawing.Size(155, 13);
//            this.label7.TabIndex = 21;
//            this.label7.Text = "Enter Valid Segment or All";
//            // 
//            // w_brn
//            // 
//            this.w_brn.AllowSpace = true;
//            this.w_brn.AssociatedLookUpName = "";
//            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.w_brn.ContinuationTextBox = null;
//            this.w_brn.CustomEnabled = true;
//            this.w_brn.DataFieldMapping = "";
//            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.w_brn.GetRecordsOnUpDownKeys = false;
//            this.w_brn.IsDate = false;
//            this.w_brn.Location = new System.Drawing.Point(232, 199);
//            this.w_brn.MaxLength = 3;
//            this.w_brn.Name = "w_brn";
//            this.w_brn.NumberFormat = "###,###,##0.00";
//            this.w_brn.Postfix = "";
//            this.w_brn.Prefix = "";
//            this.w_brn.Size = new System.Drawing.Size(128, 20);
//            this.w_brn.SkipValidation = false;
//            this.w_brn.TabIndex = 4;
//            this.w_brn.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label6
//            // 
//            this.label6.AutoSize = true;
//            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label6.Location = new System.Drawing.Point(52, 201);
//            this.label6.Name = "label6";
//            this.label6.Size = new System.Drawing.Size(154, 13);
//            this.label6.TabIndex = 19;
//            this.label6.Text = "Enter Valid Branch or ALL";
//            // 
//            // label5
//            // 
//            this.label5.AutoSize = true;
//            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label5.Location = new System.Drawing.Point(52, 227);
//            this.label5.Name = "label5";
//            this.label5.Size = new System.Drawing.Size(166, 13);
//            this.label5.TabIndex = 17;
//            this.label5.Text = "Enter As Of [DD/MM/YYYY]";
//            // 
//            // Dest_Type
//            // 
//            this.Dest_Type.BusinessEntity = "";
//            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
//            this.Dest_Type.CustomEnabled = true;
//            this.Dest_Type.DataFieldMapping = "";
//            this.Dest_Type.FormattingEnabled = true;
//            this.Dest_Type.Items.AddRange(new object[] {
//            "Screen",
//            "Printer",
//            "File",
//            "Mail"});
//            this.Dest_Type.Location = new System.Drawing.Point(232, 94);
//            this.Dest_Type.LOVType = "";
//            this.Dest_Type.Name = "Dest_Type";
//            this.Dest_Type.Size = new System.Drawing.Size(128, 21);
//            this.Dest_Type.SPName = "";
//            this.Dest_Type.TabIndex = 0;
//            // 
//            // pictureBox1
//            // 
//            this.pictureBox1.Image = global::iCORE.Properties.Resources.Citi;
//            this.pictureBox1.Location = new System.Drawing.Point(354, 2);
//            this.pictureBox1.Name = "pictureBox1";
//            this.pictureBox1.Size = new System.Drawing.Size(68, 64);
//            this.pictureBox1.TabIndex = 11;
//            this.pictureBox1.TabStop = false;
//            // 
//            // button2
//            // 
//            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.button2.Location = new System.Drawing.Point(251, 315);
//            this.button2.Name = "button2";
//            this.button2.Size = new System.Drawing.Size(63, 23);
//            this.button2.TabIndex = 9;
//            this.button2.Text = "Close";
//            this.button2.UseVisualStyleBackColor = true;
//            this.button2.Click += new System.EventHandler(this.button2_Click);
//            // 
//            // copies
//            // 
//            this.copies.AllowSpace = true;
//            this.copies.AssociatedLookUpName = "";
//            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.copies.ContinuationTextBox = null;
//            this.copies.CustomEnabled = true;
//            this.copies.DataFieldMapping = "";
//            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.copies.GetRecordsOnUpDownKeys = false;
//            this.copies.IsDate = false;
//            this.copies.Location = new System.Drawing.Point(232, 173);
//            this.copies.Name = "copies";
//            this.copies.NumberFormat = "###,###,##0.00";
//            this.copies.Postfix = "";
//            this.copies.Prefix = "";
//            this.copies.Size = new System.Drawing.Size(128, 20);
//            this.copies.SkipValidation = false;
//            this.copies.TabIndex = 3;
//            this.copies.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // button1
//            // 
//            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.button1.Location = new System.Drawing.Point(184, 315);
//            this.button1.Name = "button1";
//            this.button1.Size = new System.Drawing.Size(63, 23);
//            this.button1.TabIndex = 8;
//            this.button1.Text = "Run";
//            this.button1.UseVisualStyleBackColor = true;
//            this.button1.Click += new System.EventHandler(this.button1_Click);
//            // 
//            // Dest_Format
//            // 
//            this.Dest_Format.AllowSpace = true;
//            this.Dest_Format.AssociatedLookUpName = "";
//            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.Dest_Format.ContinuationTextBox = null;
//            this.Dest_Format.CustomEnabled = true;
//            this.Dest_Format.DataFieldMapping = "";
//            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.Dest_Format.GetRecordsOnUpDownKeys = false;
//            this.Dest_Format.IsDate = false;
//            this.Dest_Format.Location = new System.Drawing.Point(232, 148);
//            this.Dest_Format.Name = "Dest_Format";
//            this.Dest_Format.NumberFormat = "###,###,##0.00";
//            this.Dest_Format.Postfix = "";
//            this.Dest_Format.Prefix = "";
//            this.Dest_Format.Size = new System.Drawing.Size(128, 20);
//            this.Dest_Format.SkipValidation = false;
//            this.Dest_Format.TabIndex = 2;
//            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // Dest_name
//            // 
//            this.Dest_name.AllowSpace = true;
//            this.Dest_name.AssociatedLookUpName = "";
//            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
//            this.Dest_name.ContinuationTextBox = null;
//            this.Dest_name.CustomEnabled = true;
//            this.Dest_name.DataFieldMapping = "";
//            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.Dest_name.GetRecordsOnUpDownKeys = false;
//            this.Dest_name.IsDate = false;
//            this.Dest_name.Location = new System.Drawing.Point(232, 121);
//            this.Dest_name.Name = "Dest_name";
//            this.Dest_name.NumberFormat = "###,###,##0.00";
//            this.Dest_name.Postfix = "";
//            this.Dest_name.Prefix = "";
//            this.Dest_name.Size = new System.Drawing.Size(128, 20);
//            this.Dest_name.SkipValidation = false;
//            this.Dest_name.TabIndex = 1;
//            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
//            // 
//            // label9
//            // 
//            this.label9.AutoSize = true;
//            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label9.Location = new System.Drawing.Point(110, 53);
//            this.label9.Name = "label9";
//            this.label9.Size = new System.Drawing.Size(168, 13);
//            this.label9.TabIndex = 8;
//            this.label9.Text = "Enter Values For Parameters";
//            // 
//            // label8
//            // 
//            this.label8.AutoSize = true;
//            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label8.Location = new System.Drawing.Point(124, 16);
//            this.label8.Name = "label8";
//            this.label8.Size = new System.Drawing.Size(142, 20);
//            this.label8.TabIndex = 7;
//            this.label8.Text = "Report Parameter";
//            // 
//            // label4
//            // 
//            this.label4.AutoSize = true;
//            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label4.Location = new System.Drawing.Point(52, 173);
//            this.label4.Name = "label4";
//            this.label4.Size = new System.Drawing.Size(109, 13);
//            this.label4.TabIndex = 3;
//            this.label4.Text = "Number Of Copies";
//            // 
//            // label3
//            // 
//            this.label3.AutoSize = true;
//            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label3.Location = new System.Drawing.Point(52, 148);
//            this.label3.Name = "label3";
//            this.label3.Size = new System.Drawing.Size(113, 13);
//            this.label3.TabIndex = 2;
//            this.label3.Text = "Destination Format";
//            // 
//            // label2
//            // 
//            this.label2.AutoSize = true;
//            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label2.Location = new System.Drawing.Point(52, 121);
//            this.label2.Name = "label2";
//            this.label2.Size = new System.Drawing.Size(107, 13);
//            this.label2.TabIndex = 1;
//            this.label2.Text = "Destination Name";
//            // 
//            // label1
//            // 
//            this.label1.AutoSize = true;
//            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.label1.Location = new System.Drawing.Point(52, 94);
//            this.label1.Name = "label1";
//            this.label1.Size = new System.Drawing.Size(103, 13);
//            this.label1.TabIndex = 0;
//            this.label1.Text = "Destination Type";
//            // 
//            // CHRIS_PayrollReports_FTEDesignationGroup
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(446, 395);
//            this.Controls.Add(this.groupBox1);
//            this.Name = "CHRIS_PayrollReports_FTEDesignationGroup";
//            this.Text = "CHRIS PayrollReports Monthly Tax Report";
//            this.Controls.SetChildIndex(this.groupBox1, 0);
//            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
//            this.groupBox1.ResumeLayout(false);
//            this.groupBox1.PerformLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
//            this.ResumeLayout(false);
//            this.PerformLayout();

//        }

//        #endregion

//        private System.Windows.Forms.GroupBox groupBox1;
//        private System.Windows.Forms.Label label4;
//        private System.Windows.Forms.Label label3;
//        private System.Windows.Forms.Label label2;
//        private System.Windows.Forms.Label label1;
//        private CrplControlLibrary.SLTextBox copies;
//        private CrplControlLibrary.SLTextBox Dest_Format;
//        private CrplControlLibrary.SLTextBox Dest_name;
//        private System.Windows.Forms.Label label9;
//        private System.Windows.Forms.Label label8;
//        private System.Windows.Forms.Button button1;
//        private System.Windows.Forms.Button button2;
//        private System.Windows.Forms.PictureBox pictureBox1;
//        private CrplControlLibrary.SLComboBox Dest_Type;
//        private System.Windows.Forms.Label label5;
//        private CrplControlLibrary.SLTextBox w_seg;
//        private System.Windows.Forms.Label label7;
//        private CrplControlLibrary.SLTextBox w_brn;
//        private System.Windows.Forms.Label label6;
//        private CrplControlLibrary.SLDatePicker W_DATE;
//        private CrplControlLibrary.SLTextBox w_grp;
//        private System.Windows.Forms.Label label10;
//    }
//}>>>>>>> .r15851
