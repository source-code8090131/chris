using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ATRIncentiveAllow : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_ATRIncentiveAllow()
        {
            InitializeComponent();
        }
        
        public CHRIS_PayrollReports_ATRIncentiveAllow(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_name.Text = "C:\\iCORE-Spool\\";

        }


        private void button1_Click(object sender, EventArgs e)
        {
               
            base.RptFileName = "atr_incentive_allow";
            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            if (Dest_Type.Text == "Printer")
            {
                //if (nocopies.Text != string.Empty)
                {
                    //no_of_copies = Convert.ToInt16(nocopies.Text);
                    base.PrintNoofCopiesReport(1);
                }
            }
            if (Dest_Type.Text == "File")
            {
                string d = "c:\\iCORE-Spool\\report";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;
                base.ExportCustomReport(d, "PDF");
            }
            if (Dest_Type.Text == "Mail")
            {
                string d = "";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;
                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", d);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}