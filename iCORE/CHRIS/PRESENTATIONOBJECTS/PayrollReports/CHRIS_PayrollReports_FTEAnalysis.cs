using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_FTEAnalysis : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_FTEAnalysis()
        {
            InitializeComponent();
        }
        
        public CHRIS_PayrollReports_FTEAnalysis(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.wdate.Value = null;
            this.Copies.Text = "1";

        }


        private void button1_Click(object sender, EventArgs e)
        {
               


            {
                if (this.wdate.Value  != null) 
                    this.my.Text = this.wdate.Text;
                else
                    this.my.Text = "0";
 
                base.RptFileName = "py014";

                //base.btnCallReport_Click(sender, e);
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (Dest_Type.Text == "Printer")
                {
                    base.PrintCustomReport(this.Copies.Text );
                }
                else if (Dest_Type.Text == "File")
                {

                    string DestName;
                    string DestFormat;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_name.Text;
                    }

                        base.ExportCustomReport(DestName, "PDF");

                }
                else if (Dest_Type.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";
                    string DestFormat;
                    string RecipentName;
                    if (this.Dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_name.Text;
                    }

                        base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}