using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_PayrollRegister : BaseRptForm
    {
        private string DestFormat = "PDF";
        public CHRIS_PayrollReports_PayrollRegister()
        {
            InitializeComponent();
        }


        public CHRIS_PayrollReports_PayrollRegister(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.FillBranch();
            this.fillsegCombo();
            this.Copies.Text = "1";
            this.W_SEG.Text = "GF";
            this.branch.Text = "FBD";
            this.MONTH.Text  = "10";
            this.YEAR.Text = "2004";
        }

        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.branch.DisplayMember = "BRN_CODE";
                this.branch.ValueMember = "BRN_CODE";
                this.branch.DataSource = rsltCode.dstResult.Tables[0];
            }

        }
        private void fillsegCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDataManager = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.W_SEG.DisplayMember = "pr_segment";
                    this.W_SEG.ValueMember = "pr_segment";
                    this.W_SEG.DataSource = rsltCode.dstResult.Tables[0];
                }

            }

        }
        private void CHRIS_PayrollReports_PayrollRegister_Load(object sender, EventArgs e)
        {

        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            {
                base.RptFileName = "PY007";

                CmnDataManager cmnDataManager = new CmnDataManager();
                Dictionary<String, Object> paramList = new Dictionary<string, object>();

                try
                {
                    paramList.Clear();
                    paramList.Add("branch", branch.Text);
                    paramList.Add("w_seg", W_SEG.Text);
                    paramList.Add("month", MONTH.Text);
                    paramList.Add("year", YEAR.Text);
                    paramList.Add("cat", CAT.Text);

                    cmnDataManager.Execute("CHRIS_RPT_SP_PY007U", "", paramList);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An unexpected error occured while generating Payroll Register Report.\nPlease verify your input data.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    LogException("CHRIS_PayrollReports_PayrollRegister", "Run_Click_1", ex);
                }
                finally
                {
                    cmnDataManager = null;
                    paramList = null;
                }

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport(this.Copies.Text );
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }
                //Export Data
                if (Dest_Type.Text == "File")
                {
                    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                    //if (Dest_name.Text != string.Empty)
                    //String Res1 = "c:\\iCORE-Spool\\Report";
                    //if (Dest_name.Text != String.Empty)
                    //    Res1 = Dest_name.Text;
                    //    base.ExportCustomReport(Res1, "pdf");
                    //base.ExportCustomReport();

                    string DestinationFile = "c:\\iCORE-Spool\\Report";
                    saveFileDialog1.FileName = "CHRIS_RPT_SP_PY007" + ".xls";
                    saveFileDialog1.AddExtension = true;
                    saveFileDialog1.Filter = "xls files (*.xls)|*.xls|All files (*.*)|*.*";
                    DialogResult drs = saveFileDialog1.ShowDialog();
                    DestinationFile = saveFileDialog1.FileName;
                    
                    DataTable dt = new DataTable();
                    dt = SQLManager.ExecuteCHRIS_RPT_SP_PY007(branch.Text.Trim(), W_SEG.Text.Trim(), Convert.ToInt32(MONTH.Text.Trim()), Convert.ToInt32(YEAR.Text.Trim()), CAT.Text.Trim()).Tables[0];

                    if (drs == DialogResult.OK)
                    {
                        writeExcel(dt, DestinationFile);

                        MessageBox.Show("File Save Successfully", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }

            if (Dest_Type.Text == "Mail")
            {
                String Res1 = "";
                if (Dest_name.Text != String.Empty)
                    Res1 = Dest_name.Text;

                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", Res1);



                //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //{
                //    base.EmailToReport(Dest_name.Text, Dest_Format.Text);
                //}
            }
            //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
            //{
            //    string Path =base.ExportReportInGivenFormat("PDF");

            //}
        }

        public static void writeExcel(System.Data.DataTable dt, string fileSaveName)
        {
            try
            {

                if (dt == null || dt.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Null or empty input table!\n");
                }
               // string Filepath = filepath;
                string SheetName = "Sheet1";
                // Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel._Worksheet xlWorkSheet;
                // Microsoft.Office.Interop.Excel.Range xlRange = null;
                object misValue = Type.Missing;

                Excel.Application xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(System.Reflection.Missing.Value);

                // xlWorkBook = xlApp.Workbooks.Open(Filepath, misValue, false, misValue, misValue, misValue, true, misValue, misValue, misValue, misValue, misValue, false, misValue, misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Sheets[SheetName];

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.ActiveSheet;

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Sheets.get_Item(SheetName);
                xlWorkSheet.Activate();

                string sDate = string.Empty;
                string dateTime = string.Empty;

                string[] ManualColumn = { "pno", "name", "wdays", "basic", "house", "conv", "pfund", "tax", "sal_adv", "excis", "zakat", "premium", "category" ,
                                          "contrib", "cnt", "pr_dept", "otaamt", "SOT", "DOT", "SCOST", "DCOST", "MCOST", "CCOST", "F_INST", "L_INST", "F_INTR",
                                          "L_INTR", "F_INST1", "L_INST1", "F_INTR1", "L_INTR1", "TOT_D_AMT", "TOT_A_AMT", "MONTH", "YEAR", "BRNNAME", "ETYPE", "SEGMENT"};

                for (int i = 0; i < 1; i++)
                {
                    for (int j = 0; j < ManualColumn.Length; j++)
                    {
                        xlApp.Cells[i + 2, j + 1] = ManualColumn[j].ToString();
                    }
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        xlApp.Cells[i + 3, j + 1] = dt.Rows[i][j].ToString();
                    }
                }

             //   if (Filepath != null || Filepath != "")
                {
                    try
                    {
                        xlApp.ActiveWorkbook.SaveAs(fileSaveName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                        xlApp.Quit();

                        xlWorkSheet = null;
                        xlWorkBook = null;
                        xlApp = null;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Can not save file" + ex.Message);
                    }
                    finally
                    {
                        GC.Collect();
                    }
                }
                //else
                //{
                //    xlApp.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }

        
    }
}


