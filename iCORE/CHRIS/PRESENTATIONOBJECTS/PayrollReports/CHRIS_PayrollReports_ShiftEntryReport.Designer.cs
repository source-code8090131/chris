namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_ShiftEntryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_ShiftEntryReport));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.w_empl2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.w_empl1 = new CrplControlLibrary.SLTextBox(this.components);
            this.w_month = new CrplControlLibrary.SLTextBox(this.components);
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.w_empl2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.w_empl1);
            this.groupBox1.Controls.Add(this.w_month);
            this.groupBox1.Controls.Add(this.nocopies);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 224);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(29, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 13);
            this.label5.TabIndex = 155;
            this.label5.Text = "Ending Employee No. :";
            // 
            // w_empl2
            // 
            this.w_empl2.AllowSpace = true;
            this.w_empl2.AssociatedLookUpName = "";
            this.w_empl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_empl2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_empl2.ContinuationTextBox = null;
            this.w_empl2.CustomEnabled = true;
            this.w_empl2.DataFieldMapping = "";
            this.w_empl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_empl2.GetRecordsOnUpDownKeys = false;
            this.w_empl2.IsDate = false;
            this.w_empl2.Location = new System.Drawing.Point(171, 157);
            this.w_empl2.MaxLength = 6;
            this.w_empl2.Name = "w_empl2";
            this.w_empl2.NumberFormat = "###,###,##0.00";
            this.w_empl2.Postfix = "";
            this.w_empl2.Prefix = "";
            this.w_empl2.Size = new System.Drawing.Size(156, 20);
            this.w_empl2.SkipValidation = false;
            this.w_empl2.TabIndex = 5;
            this.w_empl2.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 13);
            this.label3.TabIndex = 153;
            this.label3.Text = "Starting Employee No. :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 152;
            this.label2.Text = "Enter Month";
            // 
            // w_empl1
            // 
            this.w_empl1.AllowSpace = true;
            this.w_empl1.AssociatedLookUpName = "";
            this.w_empl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_empl1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_empl1.ContinuationTextBox = null;
            this.w_empl1.CustomEnabled = true;
            this.w_empl1.DataFieldMapping = "";
            this.w_empl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_empl1.GetRecordsOnUpDownKeys = false;
            this.w_empl1.IsDate = false;
            this.w_empl1.Location = new System.Drawing.Point(171, 132);
            this.w_empl1.MaxLength = 6;
            this.w_empl1.Name = "w_empl1";
            this.w_empl1.NumberFormat = "###,###,##0.00";
            this.w_empl1.Postfix = "";
            this.w_empl1.Prefix = "";
            this.w_empl1.Size = new System.Drawing.Size(156, 20);
            this.w_empl1.SkipValidation = false;
            this.w_empl1.TabIndex = 4;
            this.w_empl1.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // w_month
            // 
            this.w_month.AllowSpace = true;
            this.w_month.AssociatedLookUpName = "";
            this.w_month.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_month.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_month.ContinuationTextBox = null;
            this.w_month.CustomEnabled = true;
            this.w_month.DataFieldMapping = "";
            this.w_month.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_month.GetRecordsOnUpDownKeys = false;
            this.w_month.IsDate = false;
            this.w_month.Location = new System.Drawing.Point(171, 108);
            this.w_month.MaxLength = 4;
            this.w_month.Name = "w_month";
            this.w_month.NumberFormat = "###,###,##0.00";
            this.w_month.Postfix = "";
            this.w_month.Prefix = "";
            this.w_month.Size = new System.Drawing.Size(156, 20);
            this.w_month.SkipValidation = false;
            this.w_month.TabIndex = 3;
            this.w_month.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(171, 84);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(156, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 2;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 148;
            this.label4.Text = "Copies";
            // 
            // btnClose
            // 
            this.btnClose.ActionType = "";
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(254, 192);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(73, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(171, 191);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(74, 23);
            this.Run.TabIndex = 6;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(171, 60);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(156, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 144;
            this.label1.Text = "Destype";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(158, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(194, 15);
            this.label13.TabIndex = 142;
            this.label13.Text = "Report Parameters";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(158, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(194, 13);
            this.label14.TabIndex = 143;
            this.label14.Text = "Enter values for the parameters";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(439, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 141;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_PayrollReports_ShiftEntryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(533, 271);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_ShiftEntryReport";
            this.Text = "iCORE-CHRIS-ShiftEntryReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox w_empl2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox w_empl1;
        private CrplControlLibrary.SLTextBox w_month;
        private CrplControlLibrary.SLTextBox nocopies;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLButton btnClose;
        private CrplControlLibrary.SLButton Run;
    }
}