using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_IcentiveBonusReciept : BaseRptForm
    {
        public CHRIS_PayrollReports_IcentiveBonusReciept()
        {
            InitializeComponent();
        }
        public CHRIS_PayrollReports_IcentiveBonusReciept(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.nocopies.Text = "1";
            this.Dest_Format.Text = "dflt";
            Dest_name.Text = "C:\\iCORE-Spool\\";
          
        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }
            no_of_copies = Convert.ToInt16(nocopies.Text);

            {
                base.RptFileName = "PY037";

                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    
                }
                if (Dest_Type.Text == "Printer")
                {
                    base.PrintNoofCopiesReport(no_of_copies);
                  
                }

                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }



                //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
                //{
                //    string Path =base.ExportReportInGivenFormat("PDF");

                //}


            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

              

    }
}