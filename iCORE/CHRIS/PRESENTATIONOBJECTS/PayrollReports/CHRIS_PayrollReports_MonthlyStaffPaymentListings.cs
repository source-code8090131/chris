using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_MonthlyStaffPaymentListings : BaseRptForm
    {
        //private string DestFormat = "PDF";
        public CHRIS_PayrollReports_MonthlyStaffPaymentListings()
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";


        public CHRIS_PayrollReports_MonthlyStaffPaymentListings(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.W_MONTH.Text = "3";
            this.YEAR.Text = "2011";
            this.W_MONTH.Text = DateTime.Today.Month.ToString();
            this.YEAR.Text = DateTime.Today.Year.ToString();
            this.W_SEG.Text = "ALL";
            this.c_flag.Text = "Y";
            this.nocopies.Text = "1";
            this.FillBranch();
            
        }

        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.W_BRANCH.DisplayMember = "BRN_CODE";
                this.W_BRANCH.ValueMember = "BRN_CODE";
                this.W_BRANCH.DataSource = rsltCode.dstResult.Tables[0];
            }

        }
        
        private void CHRIS_PayrollReports_MonthlyStaffPaymentListings_Load(object sender, EventArgs e)
        {

        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            int no_of_copies;
            if (this.nocopies.Text == String.Empty)
            {
                nocopies.Text = "1";
            }

            no_of_copies = Convert.ToInt16(nocopies.Text);


            {
                base.RptFileName = "PY002";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    //if (nocopies.Text != string.Empty)
                    //{
                    //    no_of_copies = Convert.ToInt16(nocopies.Text);

                    base.PrintNoofCopiesReport(no_of_copies);
                }




                //if (Dest_Type.Text == "Printer")
                //{

                //    base.PrintCustomReport();

                //}

                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }






            }

        }

       
        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }


        #endregion 



    }
}


