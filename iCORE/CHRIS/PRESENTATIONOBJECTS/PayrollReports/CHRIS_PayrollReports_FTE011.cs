using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_FTE011 : BaseRptForm
    {
       // private string DestFormat = "PDF";
        public CHRIS_PayrollReports_FTE011()
        {
            InitializeComponent();
        }
        //string DestName = @"C:\Report";
        string DestFormat = "PDF";


        public CHRIS_PayrollReports_FTE011(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            //Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
           // this.fillsegCombo();
            this.month.Text = "3";
//            this.month.Text = DateTime.Today.Month.ToString();
//            this.year.Text = DateTime.Today.Year.ToString();

            DataTable dt = new DataTable();
            dt.Columns.Add("Year");
            dt.Rows.Add(new object[] { DateTime.Today.Year.ToString() });

            this.cmbyear.ValueMember = "Year";
            this.cmbyear.DisplayMember = "Year";
            this.cmbyear.DataSource = dt;

            DataTable month = new DataTable();
            month.Columns.Add("Month");
            month.Rows.Add(new object[] { DateTime.Today.Month.ToString() });

            this.cmbmonth.ValueMember = "Month";
            this.cmbmonth.DisplayMember = "Month";
            this.cmbmonth.DataSource = month;
            Dest_name.Text = "C:\\iCORE-Spool\\";

          
        }

        
        //private void fillsegCombo()
        //{
        //    Result rsltCode;
        //    CmnDataManager cmnDataManager = new CmnDataManager();

        //    Dictionary<string, object> param = new Dictionary<string, object>();
        //    rsltCode = cmnDataManager.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

        //    if (rsltCode.isSuccessful)
        //    {
        //        if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
        //        {
        //            this.seg.DisplayMember = "pr_segment";
        //            this.seg.ValueMember = "pr_segment";
        //            this.seg.DataSource = rsltCode.dstResult.Tables[0];
        //        }

        //    }

        //}


        private void Run_Click(object sender, EventArgs e)
        {
            {
                base.RptFileName = "FTE011";

                string Str = this.cmbmonth.Text.Trim();

                double Num;

                bool isNum = double.TryParse(Str, out Num);

                if (isNum)


                    this.month.Text = this.cmbmonth.Text;
                else
                    this.month.Text = "0"; //string data


                string Str1 = this.cmbyear.Text.Trim();

                double Num1;

                bool isNum1 = double.TryParse(Str1, out Num1);

                if (isNum1)


                    this.year.Text = this.cmbyear.Text;
                else
                    this.year.Text = "0"; //string data


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    base.PrintCustomReport();


                }

                if (Dest_Type.Text == "File")
                {
                    String d = "c:\\iCORE-Spool\\report";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String d = "";
                    if (this.Dest_name.Text != string.Empty)
                        d = this.Dest_name.Text;
                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }

            }
        }

        //private void Dest_Type_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }
        #endregion 

    }
}


