using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    public partial class CHRIS_PayrollReports_ReconcilimentReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PayrollReports_ReconcilimentReport()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PayrollReports_ReconcilimentReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }


        //private void CHRIS_PayrollReport_ReconcilimentReport_Load(object sender, EventArgs e)
        //{

        //}


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.fillBranchCombo();
            this.fillsegCombo();
            nocopies.Text = "1";
            this.month.Text = "2";
            DataTable dt = new DataTable();
            dt.Columns.Add("Year");
            dt.Rows.Add(new object[] { DateTime.Today.Year.ToString() });
            dt.Rows.Add(new object[] { "2010" });

            this.year.ValueMember = "Year";
            this.year.DisplayMember = "Year";
            this.year.DataSource = dt;
            Dest_name.Text = "C:\\iCORE-Spool\\";
            //Dest_Format.Text = "PDF";
            //this.PrintNoofCopiesReport(1);

        }
           

        private void Btn_Run_Click(object sender, EventArgs e)
        {

            int no_of_copies;

            if (this.month.Text != string.Empty)
            {
                int m;
                try
                {
                    m = int.Parse(this.month.Text);
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Invalid parameter input.");
                    return;
                }
                //if (m > 12)
                //{
                //    MessageBox.Show("Invalid parameter input.");
                //    return;
                //}
            }

            {
                base.RptFileName = "PY006B";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }


                if (Dest_Type.Text == "File")
                {
                    String Res1 = "C:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;
                     

                        base.ExportCustomReport(Res1, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    String Res1 = "";
                    if (Dest_name.Text != String.Empty)
                        Res1 = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", Res1);

                }


            }
       

        }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
         private void fillBranchCombo()
        {


            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount");


            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.Branch.DisplayMember = "BRN_CODE";
                this.Branch.ValueMember = "BRN_CODE";
                this.Branch.DataSource = rsltCode.dstResult.Tables[0];
            }
            
          

        }

        private void fillsegCombo()
        {


            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();

        
            rsltCode = cmnDM.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEW");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                  
                    this.w_seg.DisplayMember = "pr_segment";
                    this.w_seg.ValueMember = "pr_segment";
                    this.w_seg.DataSource = rsltCode.dstResult.Tables[0];
                    
                }

            }

        }

        private void CHRIS_PayrollReports_ReconcilimentReport_Load(object sender, EventArgs e)
        {
            fillBranchCombo();
            fillsegCombo();

        }








       
        

       
    }
}