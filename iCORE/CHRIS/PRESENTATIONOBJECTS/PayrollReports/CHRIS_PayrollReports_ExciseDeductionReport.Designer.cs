namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PayrollReports
{
    partial class CHRIS_PayrollReports_ExciseDeductionReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PayrollReports_ExciseDeductionReport));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CAT = new CrplControlLibrary.SLTextBox(this.components);
            this.YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.MNTH = new CrplControlLibrary.SLTextBox(this.components);
            this.btnRun = new System.Windows.Forms.Button();
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.W_CITY = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nocopies);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.CAT);
            this.groupBox1.Controls.Add(this.YEAR);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.MNTH);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.W_CITY);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(520, 378);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(65, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 85;
            this.label10.Text = "Destination Name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(65, 138);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 13);
            this.label9.TabIndex = 84;
            this.label9.Text = "Number of Copies";
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(217, 135);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(156, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 3;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(217, 109);
            this.Dest_name.MaxLength = 80;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(156, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 2;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(453, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(217, 82);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(156, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(189, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 17);
            this.label6.TabIndex = 80;
            this.label6.Text = "Report Parameters";
            // 
            // CAT
            // 
            this.CAT.AllowSpace = true;
            this.CAT.AssociatedLookUpName = "";
            this.CAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CAT.ContinuationTextBox = null;
            this.CAT.CustomEnabled = true;
            this.CAT.DataFieldMapping = "";
            this.CAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CAT.GetRecordsOnUpDownKeys = false;
            this.CAT.IsDate = false;
            this.CAT.Location = new System.Drawing.Point(217, 266);
            this.CAT.MaxLength = 1;
            this.CAT.Name = "CAT";
            this.CAT.NumberFormat = "###,###,##0.00";
            this.CAT.Postfix = "";
            this.CAT.Prefix = "";
            this.CAT.Size = new System.Drawing.Size(156, 20);
            this.CAT.SkipValidation = false;
            this.CAT.TabIndex = 8;
            this.CAT.TextType = CrplControlLibrary.TextType.String;
            // 
            // YEAR
            // 
            this.YEAR.AllowSpace = true;
            this.YEAR.AssociatedLookUpName = "";
            this.YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.YEAR.ContinuationTextBox = null;
            this.YEAR.CustomEnabled = true;
            this.YEAR.DataFieldMapping = "";
            this.YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YEAR.GetRecordsOnUpDownKeys = false;
            this.YEAR.IsDate = false;
            this.YEAR.Location = new System.Drawing.Point(217, 240);
            this.YEAR.MaxLength = 4;
            this.YEAR.Name = "YEAR";
            this.YEAR.NumberFormat = "###,###,##0.00";
            this.YEAR.Postfix = "";
            this.YEAR.Prefix = "";
            this.YEAR.Size = new System.Drawing.Size(156, 20);
            this.YEAR.SkipValidation = false;
            this.YEAR.TabIndex = 7;
            this.YEAR.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(149, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(239, 17);
            this.label7.TabIndex = 36;
            this.label7.Text = "Enter values for the parameters";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(298, 292);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // MNTH
            // 
            this.MNTH.AllowSpace = true;
            this.MNTH.AssociatedLookUpName = "";
            this.MNTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MNTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.MNTH.ContinuationTextBox = null;
            this.MNTH.CustomEnabled = true;
            this.MNTH.DataFieldMapping = "";
            this.MNTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MNTH.GetRecordsOnUpDownKeys = false;
            this.MNTH.IsDate = false;
            this.MNTH.Location = new System.Drawing.Point(217, 214);
            this.MNTH.MaxLength = 2;
            this.MNTH.Name = "MNTH";
            this.MNTH.NumberFormat = "###,###,##0.00";
            this.MNTH.Postfix = "";
            this.MNTH.Prefix = "";
            this.MNTH.Size = new System.Drawing.Size(156, 20);
            this.MNTH.SkipValidation = false;
            this.MNTH.TabIndex = 6;
            this.MNTH.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(217, 292);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 9;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(217, 188);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(156, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 5;
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(65, 268);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "Enter Category or null";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(65, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "Enter Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(65, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Enter Month (MM)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(65, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Enter Valid Segment";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(65, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 40;
            this.label2.Text = "Enter Valid Branch";
            // 
            // W_CITY
            // 
            this.W_CITY.AllowSpace = true;
            this.W_CITY.AssociatedLookUpName = "";
            this.W_CITY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_CITY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_CITY.ContinuationTextBox = null;
            this.W_CITY.CustomEnabled = true;
            this.W_CITY.DataFieldMapping = "";
            this.W_CITY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_CITY.GetRecordsOnUpDownKeys = false;
            this.W_CITY.IsDate = false;
            this.W_CITY.Location = new System.Drawing.Point(217, 162);
            this.W_CITY.MaxLength = 3;
            this.W_CITY.Name = "W_CITY";
            this.W_CITY.NumberFormat = "###,###,##0.00";
            this.W_CITY.Postfix = "";
            this.W_CITY.Prefix = "";
            this.W_CITY.Size = new System.Drawing.Size(156, 20);
            this.W_CITY.SkipValidation = false;
            this.W_CITY.TabIndex = 4;
            this.W_CITY.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(65, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Destination Type";
            // 
            // CHRIS_PayrollReports_ExciseDeductionReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 429);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PayrollReports_ExciseDeductionReport";
            this.Text = "iCORE CHRIS - Excise Deduction Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox W_CITY;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox W_SEG;
        private CrplControlLibrary.SLTextBox CAT;
        private CrplControlLibrary.SLTextBox YEAR;
        private CrplControlLibrary.SLTextBox MNTH;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox Dest_name;
        private CrplControlLibrary.SLTextBox nocopies;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}