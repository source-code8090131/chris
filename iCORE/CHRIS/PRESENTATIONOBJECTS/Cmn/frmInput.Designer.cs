namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn
{
    partial class frmInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtVal = new CrplControlLibrary.SLTextBox(this.components);
            this.lblCaption = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelExtraText = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtVal
            // 
            this.txtVal.AllowSpace = true;
            this.txtVal.AssociatedLookUpName = "";
            this.txtVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVal.ContinuationTextBox = null;
            this.txtVal.CustomEnabled = true;
            this.txtVal.DataFieldMapping = "";
            this.txtVal.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVal.GetRecordsOnUpDownKeys = false;
            this.txtVal.IsDate = false;
            this.txtVal.Location = new System.Drawing.Point(290, 3);
            this.txtVal.MaxLength = 1;
            this.txtVal.Name = "txtVal";
            this.txtVal.NumberFormat = "###,###,##0.00";
            this.txtVal.Postfix = "";
            this.txtVal.Prefix = "";
            this.txtVal.Size = new System.Drawing.Size(51, 20);
            this.txtVal.SkipValidation = false;
            this.txtVal.TabIndex = 8;
            this.txtVal.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Location = new System.Drawing.Point(3, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(281, 30);
            this.lblCaption.TabIndex = 1;
            this.lblCaption.Text = "Caption Required :";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.55556F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.tableLayoutPanel1.Controls.Add(this.lblCaption, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtVal, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(518, 30);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // labelExtraText
            // 
            this.labelExtraText.AutoSize = true;
            this.labelExtraText.Location = new System.Drawing.Point(389, 52);
            this.labelExtraText.Name = "labelExtraText";
            this.labelExtraText.Size = new System.Drawing.Size(0, 13);
            this.labelExtraText.TabIndex = 14;
            this.labelExtraText.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // frmInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(542, 74);
            this.Controls.Add(this.labelExtraText);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInput";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtVal;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label labelExtraText;
    }
}