using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Collections;
using iCORE.XMS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn
{

    public partial class ReportViewer : Form //iCORE.Common.PRESENTATIONOBJECTS.Cmn.ReportViewer
    {
        public ReportViewer()
        {
            InitializeComponent();

        }

        #region --Constant--
        public const string m_ParamType         = "ParamType";
        public const string m_Values            = "Values";
        public const string m_ParamName         = "ParamName";
        public const string m_DateTimeParameter = "DateTimeParameter";
        public string m_SPPrefix                = "CHRIS_RPT_SP_";
        #endregion

        #region --Variable--
        public string m_DataSource;
        public string m_Database;
        public string m_UserID;
        public string m_SP;
        public string m_Password;
        public XmlDocument xDoc;
        public string subPath;
        public string m_RptName;
        public string m_RptPath;
        
        public string m_SPName          = string.Empty;
        public string m_Sub_SPName      = string.Empty;
        public DataSet Ds               = new DataSet();
        

        public static XMS.DATAOBJECTS.ConnectionBean connbean = null;
        public ReportDocument crDoc     = new ReportDocument();
        protected int sub_spCount       = 0;
        public string subsp             = string.Empty;
        SqlConnection conn              = new SqlConnection();
        private string m_rptFile        = string.Empty;
        private string m_rptFileName    = string.Empty;
        private bool bBusy              = false;
        DataSet dsSupRpt                = new DataSet();
        DataTable dtSubRptParam         = new DataTable();
        DataTable dtSubLinkParam        = new DataTable();
        public DataSet dsSubLinkParam   = new DataSet();
        public string MainRptName       = string.Empty;
        #endregion

        /*Added By Umair on 23 Jun 2011 For Loading Subreport paramaneters and data*/
        public DataSet LoadParametersWithSubReports(DataTable datatable, string[] Credentials)
        {

            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.ReportSource = null;
            crystalReportViewer1.ShowRefreshButton = false;

            crDoc.Load(m_RptPath);

            Credentials[1]  = "a$" + Credentials[1];
            String Srv      = Credentials[2];
            String Db       = Credentials[3];
            String usrId    = Credentials[0];
            String pwd      = Credentials[1];

            ConnectionInfo conn = new ConnectionInfo();
            conn.DatabaseName   = Db;
            conn.Password       = pwd;
            conn.ServerName     = Srv;
            conn.UserID         = usrId;
            DoCrystalReportLogin(crDoc, Credentials);
            crDoc.SetDatabaseLogon(usrId, pwd, Srv, Db);

            /* Added By Umair on 22 Jun 2011*/
            DataSet ds = new DataSet();
            ds.Tables.Clear();
           //ds.Tables.Remove(true);
            ds.Tables.Add(datatable);
            SetDataSource(crDoc, datatable);
            /*End*/

            SetParametersList(crDoc, datatable);
            crystalReportViewer1.ParameterFieldInfo = crDoc.ParameterFields;
            crystalReportViewer1.ReportSource = crDoc;
            foreach (CrystalDecisions.Shared.TableLogOnInfo cnInfo in crystalReportViewer1.LogOnInfo)
            {
                cnInfo.ConnectionInfo = conn;
            }
            return dsSupRpt;

        }

        public DataSet LoadParametersWithSubReportsCustom(DataTable datatable, string[] Credentials)
        {

            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.ReportSource = null;
            crystalReportViewer1.ShowRefreshButton = false;

            crDoc.Load(m_RptPath);

            Credentials[1] = "a$" + Credentials[1];
            String Srv = Credentials[2];
            String Db = Credentials[3];
            String usrId = Credentials[0];
            String pwd = Credentials[1];

            ConnectionInfo conn = new ConnectionInfo();
            conn.DatabaseName = Db;
            conn.Password = pwd;
            conn.ServerName = Srv;
            conn.UserID = usrId;
            DoCrystalReportLogin(crDoc, Credentials);
            crDoc.SetDatabaseLogon(usrId, pwd, Srv, Db);

            /* Added By Umair on 22 Jun 2011*/
            DataSet ds = new DataSet();
            ds.Tables.Clear();
            //ds.Tables.Remove(true);
            ds.Tables.Add(datatable);
            SetDataSourceCustom(crDoc, datatable);
            /*End*/

            SetParametersList(crDoc, datatable);
            crystalReportViewer1.ParameterFieldInfo = crDoc.ParameterFields;
            crystalReportViewer1.ReportSource = crDoc;
            foreach (CrystalDecisions.Shared.TableLogOnInfo cnInfo in crystalReportViewer1.LogOnInfo)
            {
                cnInfo.ConnectionInfo = conn;
            }
            return dsSupRpt;

        }

        public void LoadParameters(DataTable datatable,string[] Credentials)
        {

            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.ReportSource = null;
            crystalReportViewer1.ShowRefreshButton = false; 
            
            crDoc.Load(m_RptPath);

            Credentials[1] = "a$" + Credentials[1];
            String Srv = Credentials[2];
            String Db = Credentials[3];
            String usrId = Credentials[0];
            String pwd = Credentials[1];

            ConnectionInfo conn = new ConnectionInfo();
            conn.DatabaseName = Db;
            conn.Password = pwd;
            conn.ServerName = Srv;
            conn.UserID = usrId;
            DoCrystalReportLogin(crDoc, Credentials);
            crDoc.SetDatabaseLogon(usrId, pwd, Srv, Db);

            //ConnectionInfo conn = new ConnectionInfo();
            //conn.DatabaseName = Db;
            //conn.Password = pwd;
            //conn.ServerName = Srv;
            //conn.UserID = usrId;

            SetParametersList(crDoc, datatable);
            crystalReportViewer1.ParameterFieldInfo = crDoc.ParameterFields;
            crystalReportViewer1.ReportSource = crDoc;
            foreach (CrystalDecisions.Shared.TableLogOnInfo cnInfo in crystalReportViewer1.LogOnInfo)
            {
                cnInfo.ConnectionInfo = conn;
            }
        }

        private void SetParametersList(ReportDocument crDoc, DataTable dt)
        {
            int index = 0;
            int CustIndex = 0;

            CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions
                crParamFieldDefinitions = crDoc.DataDefinition.ParameterFields;

            //bool bFound = false;

            foreach (CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition
                              def in crParamFieldDefinitions)
            {
                if (string.IsNullOrEmpty(def.ReportName))
                    crDoc.SetParameterValue(def.ParameterFieldName, dt.Rows[0][def.ParameterFieldName.Replace("@", "")]);
                //    string paramName = def.ParameterFieldName.ToUpper();
                //    CrystalDecisions.Shared.ParameterValueKind kind = def.ParameterValueKind;

                //    if (!paramName.Contains("@"))
                //    {
                //        if (CustIndex < dt.Columns.Count)
                //        {}
                //        else
                //        {
                //            CustIndex = 0;
                //        }

                //        Parameter(def, dt, CustIndex, kind);
                //        CustIndex++;
                //    }
                //    else
                //    {
                //        if (index < dt.Columns.Count)
                //        {}
                //        else
                //        {
                //            index = 0;
                //        }
                //        Parameter(def, dt, index, kind);
                //        index++;
                //    }
            }

        }


        private void Parameter(CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition def,
                                DataTable dt, int index, CrystalDecisions.Shared.ParameterValueKind kind)
        {
            string currentValue = "";
            bool bFound = false;
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    currentValue = dr.ItemArray[index].ToString();

                    if (currentValue == "")
                    {
                        if (def.ParameterValueKind.ToString() == "DateTimeParameter")
                        {
                            currentValue = null;
                        }
                        else
                        {
                            currentValue = string.Empty;
                        }

                    }
                    bFound = true;
                }
                catch { }

                if (bFound)
                {
                    bFound = false;
                    // create new Parameter Discrete Value object
                    CrystalDecisions.Shared.ParameterDiscreteValue crParamDiscreteValue =
                        new CrystalDecisions.Shared.ParameterDiscreteValue();

                    // Set value of Discrete value object 
                    crParamDiscreteValue.Value = currentValue;

                    // extract collection of current values
                    CrystalDecisions.Shared.ParameterValues crCurrentValues = def.CurrentValues;

                    // Add Discrete value object to collection of current values
                    crCurrentValues.Add(crParamDiscreteValue);

                    // apply modified current values to param collection
                    def.ApplyCurrentValues(crCurrentValues);
                }
            }
        }

        /*--Not using now--*/
        public bool DoCrystalReportLogin(CrystalDecisions.CrystalReports.Engine.ReportDocument rptDoc,string[] Credentials)
        {

            TableLogOnInfo conInfo = new TableLogOnInfo();
            
            bool testReturn = true;
            //String Srv = ConfigurationManager.AppSettings["DataSource"];
            //String Db = ConfigurationManager.AppSettings["Database"];
            //String usrId = ConfigurationManager.AppSettings["UserID"];
            //String pwd = ConfigurationManager.AppSettings["Password"];

            conInfo.ConnectionInfo.ServerName = Credentials[2];
            conInfo.ConnectionInfo.UserID = Credentials[0];
            conInfo.ConnectionInfo.Password = Credentials[1];
            conInfo.ConnectionInfo.DatabaseName = Credentials[3];

            foreach (CrystalDecisions.CrystalReports.Engine.Table tbl in rptDoc.Database.Tables)
            {
                tbl.ApplyLogOnInfo(conInfo);
                if (!tbl.TestConnectivity())
                {
                    testReturn = false;
                    break;
                }

                if (tbl.Location.IndexOf(".") > 0)
                {
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1);
                }
                else
                {
                    tbl.Location = tbl.Location;
                }

            }

            if (testReturn && rptDoc.ReportDefinition.ReportObjects.Count > 0)
            {
                int index, count, counter;
                SubreportObject mySubReportObject;
                ReportDocument mySubRepDoc;

                for (index = 0; index <= rptDoc.ReportDefinition.Sections.Count - 1; index++)
                {
                    for (count = 0; count <= rptDoc.ReportDefinition.Sections[index].ReportObjects.Count - 1; count++)
                    {
                        Section rptSection = rptDoc.ReportDefinition.Sections[index];
                        if (rptSection.ReportObjects[count].Kind == ReportObjectKind.SubreportObject)
                        {
                            mySubReportObject = (SubreportObject)rptSection.ReportObjects[count];
                            mySubRepDoc = mySubReportObject.OpenSubreport(mySubReportObject.SubreportName);
                            for (counter = 0; counter <= mySubRepDoc.Database.Tables.Count - 1; counter++)
                            {
                                mySubRepDoc.Database.Tables[counter].ApplyLogOnInfo(conInfo);
                                if (!mySubRepDoc.Database.Tables[counter].TestConnectivity())
                                {
                                    testReturn = false;
                                    break;
                                }

                            }

                        }
                    }

                }

            }

            return testReturn;

        }

        /*For Exporting Report To Excel Added By Umair on 22 Jun 2011*/
        public DataSet SetDataSource(ReportDocument crDoc, DataTable dtParam)
        {
            Result rslt;
            DataSet tempds  = new DataSet();
            DataTable dt    = new DataTable();

            int subCount    = 0;
            MainRptName     = crDoc.Name;
            rslt            = SQLManager.SelectDataSet(dtParam.DataSet, 0);
            tempds          = rslt.dstResult;
            dt              = tempds.Tables[0];
            
            dsSupRpt.Tables.Add(dt.Copy());
            dt.Rows.Clear();
            dt.Columns.Clear();

            if (!crDoc.IsSubreport)
            {
                //DataTable dtSubRptParam = new DataTable();
                //dtSubLinkParam.Clear();
                dtSubLinkParam.Rows.Add();
                dtSubLinkParam.Columns.Add("MainReportName");
                dtSubLinkParam.Columns.Add("SubReportName");
                dtSubLinkParam.Columns.Add("SubLinkParamName");
                dtSubLinkParam.Columns.Add("MainLinkParamName");

                foreach (ReportDocument rpt in crDoc.Subreports)
                {
                    /*First Check Either heir is any Link Param between Main Report and SubReport
                      If their is any Link Param between Main and sub then Insert Param Name and their Values in Datatable.
                     */
                    if (rpt.DataDefinition.RecordSelectionFormula.Substring(rpt.DataDefinition.RecordSelectionFormula.LastIndexOf('.') + 1) != "")
                    {
                        string MainRptParamName = string.Empty;
                        int val;
                        /*Find the Main Report Link Parameter Name*/
                        MainRptParamName    = rpt.DataDefinition.RecordSelectionFormula.Substring(rpt.DataDefinition.RecordSelectionFormula.LastIndexOf('.') + 1);
                        val                 = MainRptParamName.IndexOf('}');
                        MainRptParamName    = MainRptParamName.Substring(0, val);

                        /*Find the Sub Report Link Parameter Name*/
                        string LinkParmName = string.Empty;
                        LinkParmName        = rpt.DataDefinition.RecordSelectionFormula.Split('.')[1];
                        val                 = LinkParmName.IndexOf('}');
                        LinkParmName        = LinkParmName.Substring(0, val);

                        /*Putting Values in Datatable dtSubLinkParam*/
                        dtSubLinkParam.TableName = rpt.Name;


                        dtSubLinkParam.Rows[0]["MainReportName"]    = rpt.Name;
                        dtSubLinkParam.Rows[0]["SubReportName"]     = rpt.Name;
                        dtSubLinkParam.Rows[0]["SubLinkParamName"]  = LinkParmName;
                        dtSubLinkParam.Rows[0]["MainLinkParamName"] = MainRptParamName;


                        /*Putting Datatable in Dataset to Use it Later*/
                        dsSubLinkParam.Tables.Add(dtSubLinkParam.Copy());
                    }
                    ReportDocument subReport = crDoc.OpenSubreport(rpt.Name);
                    dt.TableName = subReport.Name;

                        /*Finding the Subreport Paramaters from SP*/
                        dtSubRptParam = SQLManager.GetSPParams((rpt.Database.Tables[0].Name.Split(';'))[0]);

                        /*If find and Paramter from SP than assinging its values into datatable*/
                        if (dtSubRptParam.Columns.Count > 0)
                        {
                            dtSubRptParam.Rows.Add();

                            foreach (DataColumn DCSub in dtSubRptParam.Columns)
                            {
                                foreach (DataColumn DCMain in dtParam.Columns)
                                {
                                    if (DCSub.ColumnName.ToUpper().Equals(DCMain.ColumnName.ToUpper()))
                                    {
                                        if (dtSubRptParam.Rows.Count > 0)
                                        {
                                            dtSubRptParam.Rows[0][DCSub.ColumnName]
                                                = dtParam.Rows[0][DCSub.ColumnName];
                                            if (dt.Columns.Contains(DCSub.ColumnName))
                                            {
                                                dt.Columns.Add(DCSub.ColumnName + "1");
                                            }
                                            else
                                                dt.Columns.Add(DCSub.ColumnName);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    
                    dtSubRptParam.TableName     = (subReport.Database.Tables[0].Name.Split(';'))[0];
                    DataSet Dst                 = new DataSet();
                    Dst.Tables.Add(dtSubRptParam.Copy());
                    rslt                        = SQLManager.SelectDataSet(Dst, 0);
                    tempds                      = rslt.dstResult;
                    dt                          = tempds.Tables[0];

                    if (rpt.DataDefinition.RecordSelectionFormula.Substring(rpt.DataDefinition.RecordSelectionFormula.LastIndexOf('.') + 1) == "")
                    {
                        foreach (DataColumn DCSub in dtSubRptParam.Columns)
                        {
                            foreach (DataColumn DCMain in dtParam.Columns)
                            {
                                if (DCSub.ColumnName.ToUpper().Equals(DCMain.ColumnName.ToUpper()))
                                {
                                    if (dtSubRptParam.Rows.Count > 0)
                                    {
                                        dt.Columns.Add(DCSub.ColumnName);
                                        break;
                                    }
                                }
                            }
                        }


                        foreach (DataColumn dc in dtParam.Columns)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if(dt.Columns.Contains(dc.ColumnName))
                                    dr[dc.ColumnName] = dtParam.Rows[0][dc.ColumnName].ToString();
                            }
                        }
                    }
                    //dt.Merge(dtParam,true);
                    if (dt.Rows.Count > 0)
                        dsSupRpt.Tables.Add(dt.Copy());
                    subCount++;
                }
            }

            //if (rslt.isSuccessful)
            //{
            //    return rslt.dstResult;
            //}
            return dsSupRpt;
        }
        public DataSet SetDataSourceCustom(ReportDocument crDoc, DataTable dtParam)
        {
            Result rslt;
            DataSet tempds = new DataSet();
            DataTable dt = new DataTable();

            int subCount = 0;
            MainRptName = crDoc.Name;
            rslt = SQLManager.SelectDataSet(dtParam.DataSet, 0);
            tempds = rslt.dstResult;
            dt = tempds.Tables[0];

            dsSupRpt.Tables.Add(dt.Copy());
            dt.Rows.Clear();
            dt.Columns.Clear();

            if (!crDoc.IsSubreport)
            {
                //DataTable dtSubRptParam = new DataTable();
                //dtSubLinkParam.Clear();
                dtSubLinkParam.Rows.Add();
                dtSubLinkParam.Columns.Add("MainReportName");
                dtSubLinkParam.Columns.Add("SubReportName");
                dtSubLinkParam.Columns.Add("SubLinkParamName");
                dtSubLinkParam.Columns.Add("MainLinkParamName");

                foreach (ReportDocument rpt in crDoc.Subreports)
                {
                    /*First Check Either heir is any Link Param between Main Report and SubReport
                      If their is any Link Param between Main and sub then Insert Param Name and their Values in Datatable.
                     */
                    if (rpt.DataDefinition.RecordSelectionFormula.Substring(rpt.DataDefinition.RecordSelectionFormula.LastIndexOf('.') + 1) != "")
                    {
                        string MainRptParamName = string.Empty;
                        int val;
                        /*Find the Main Report Link Parameter Name*/
                        MainRptParamName = rpt.DataDefinition.RecordSelectionFormula.Substring(rpt.DataDefinition.RecordSelectionFormula.LastIndexOf('.') + 1);
                        val = MainRptParamName.IndexOf('}');
                        MainRptParamName = MainRptParamName.Substring(0, val);

                        /*Find the Sub Report Link Parameter Name*/
                        string LinkParmName = string.Empty;
                        LinkParmName = rpt.DataDefinition.RecordSelectionFormula.Split('.')[1];
                        val = LinkParmName.IndexOf('}');
                        LinkParmName = LinkParmName.Substring(0, val);

                        /*Putting Values in Datatable dtSubLinkParam*/
                        dtSubLinkParam.TableName = rpt.Name;


                        dtSubLinkParam.Rows[0]["MainReportName"] = rpt.Name;
                        dtSubLinkParam.Rows[0]["SubReportName"] = rpt.Name;
                        dtSubLinkParam.Rows[0]["SubLinkParamName"] = LinkParmName;
                        dtSubLinkParam.Rows[0]["MainLinkParamName"] = MainRptParamName;


                        /*Putting Datatable in Dataset to Use it Later*/
                        dsSubLinkParam.Tables.Add(dtSubLinkParam.Copy());
                    }
                    ReportDocument subReport = crDoc.OpenSubreport(rpt.Name);
                    dt.TableName = subReport.Name;

                    /*Finding the Subreport Paramaters from SP*/
                    dtSubRptParam = SQLManager.GetSPParams((rpt.Database.Tables[0].Name.Split(';'))[0]);

                    /*If find and Paramter from SP than assinging its values into datatable*/
                    if (dtSubRptParam.Columns.Count > 0)
                    {
                        dtSubRptParam.Rows.Add();

                        foreach (DataColumn DCSub in dtSubRptParam.Columns)
                        {
                            foreach (DataColumn DCMain in dtParam.Columns)
                            {
                                if (DCSub.ColumnName.ToUpper().Equals(DCMain.ColumnName.ToUpper()))
                                {
                                    if (dtSubRptParam.Rows.Count > 0)
                                    {
                                        dtSubRptParam.Rows[0][DCSub.ColumnName]
                                            = dtParam.Rows[0][DCSub.ColumnName];
                                        if (dt.Columns.Contains(DCSub.ColumnName))
                                        {
                                            dt.Columns.Add(DCSub.ColumnName + "1");
                                        }
                                        else
                                            dt.Columns.Add(DCSub.ColumnName);
                                        break;
                                    }
                                }
                            }
                        }
                    }


                    dtSubRptParam.TableName = (subReport.Database.Tables[0].Name.Split(';'))[0];
                    DataSet Dst = new DataSet();
                    Dst.Tables.Add(dtSubRptParam.Copy());
                    rslt = SQLManager.SelectDataSet(Dst, 0);
                    tempds = rslt.dstResult;
                    dt = tempds.Tables[0];

                    if (rpt.DataDefinition.RecordSelectionFormula.Substring(rpt.DataDefinition.RecordSelectionFormula.LastIndexOf('.') + 1) == "")
                    {
                        foreach (DataColumn DCSub in dtSubRptParam.Columns)
                        {
                            foreach (DataColumn DCMain in dtParam.Columns)
                            {
                                if (DCSub.ColumnName.ToUpper().Equals(DCMain.ColumnName.ToUpper()))
                                {
                                    if (dtSubRptParam.Rows.Count > 0)
                                    {
                                        dt.Columns.Add(DCSub.ColumnName);
                                        break;
                                    }
                                }
                            }
                        }


                        foreach (DataColumn dc in dtParam.Columns)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dt.Columns.Contains(dc.ColumnName))
                                    dr[dc.ColumnName] = dtParam.Rows[0][dc.ColumnName].ToString();
                            }
                        }
                    }
                    //dt.Merge(dtParam,true);
                    if (dt.Rows.Count > 0)
                    {
                        dsSupRpt.Tables.Add(dt.Copy());
                        rpt.DataSourceConnections.Clear();
                        rpt.SetDataSource(dt);
                    }    
                    subCount++;
                }
            }

            //if (rslt.isSuccessful)
            //{
            //    return rslt.dstResult;
            //}
            return dsSupRpt;
        }
    }
}