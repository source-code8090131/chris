namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_TransferFinanceType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_TransferFinanceType));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.dtTransfer = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtnNewType = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnFinNo = new CrplControlLibrary.LookupButton(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAdjAmnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtnewDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTransfertype = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOldMrkUp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOldFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.PnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(607, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(643, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 449);
            this.panel1.Size = new System.Drawing.Size(643, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(9, 84);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(622, 60);
            this.pnlHead.TabIndex = 51;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(188, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 30;
            this.label3.Text = "Transfer Finance Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(185, -2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 29;
            this.label14.Text = "Fianance System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(518, 29);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 28;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(518, 3);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 27;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(475, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 26;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(467, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 25;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(99, 29);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 24;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(99, 3);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 23;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(23, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 22;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(32, 3);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 21;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PnlDetail
            // 
            this.PnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.label13);
            this.PnlDetail.Controls.Add(this.dtTransfer);
            this.PnlDetail.Controls.Add(this.lbtnNewType);
            this.PnlDetail.Controls.Add(this.lbtnFinNo);
            this.PnlDetail.Controls.Add(this.label10);
            this.PnlDetail.Controls.Add(this.label7);
            this.PnlDetail.Controls.Add(this.txtAdjAmnt);
            this.PnlDetail.Controls.Add(this.txtnewDesc);
            this.PnlDetail.Controls.Add(this.txtTransfertype);
            this.PnlDetail.Controls.Add(this.txtMarkup);
            this.PnlDetail.Controls.Add(this.txtPersonnelNo);
            this.PnlDetail.Controls.Add(this.txtPersonnelName);
            this.PnlDetail.Controls.Add(this.txtFinDesc);
            this.PnlDetail.Controls.Add(this.txtOldMrkUp);
            this.PnlDetail.Controls.Add(this.txtOldFinType);
            this.PnlDetail.Controls.Add(this.txtFinNo);
            this.PnlDetail.Controls.Add(this.label9);
            this.PnlDetail.Controls.Add(this.label8);
            this.PnlDetail.Controls.Add(this.label6);
            this.PnlDetail.Controls.Add(this.label5);
            this.PnlDetail.Controls.Add(this.label4);
            this.PnlDetail.Controls.Add(this.label2);
            this.PnlDetail.Controls.Add(this.label1);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = true;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FN_TransferCommand";
            this.PnlDetail.Location = new System.Drawing.Point(9, 150);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(622, 266);
            this.PnlDetail.SPName = "CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER";
            this.PnlDetail.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(-4, 125);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(631, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "_________________________________________________________________________________" +
                "_______________________";
            // 
            // dtTransfer
            // 
            this.dtTransfer.CustomEnabled = true;
            this.dtTransfer.CustomFormat = "dd/MM/yyyy";
            this.dtTransfer.DataFieldMapping = "FNT_DATE";
            this.dtTransfer.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTransfer.HasChanges = true;
            this.dtTransfer.IsRequired = true;
            this.dtTransfer.Location = new System.Drawing.Point(147, 173);
            this.dtTransfer.Name = "dtTransfer";
            this.dtTransfer.NullValue = " ";
            this.dtTransfer.Size = new System.Drawing.Size(100, 20);
            this.dtTransfer.TabIndex = 2;
            this.dtTransfer.Value = new System.DateTime(2010, 12, 21, 17, 52, 52, 692);
            this.dtTransfer.Validating += new System.ComponentModel.CancelEventHandler(this.dtTransfer_Validating);
            // 
            // lbtnNewType
            // 
            this.lbtnNewType.ActionLOVExists = "NewTypeExist";
            this.lbtnNewType.ActionType = "NewType";
            this.lbtnNewType.ConditionalFields = "txtOldMrkUp|txtOldFinType";
            this.lbtnNewType.CustomEnabled = true;
            this.lbtnNewType.DataFieldMapping = "";
            this.lbtnNewType.DependentLovControls = "";
            this.lbtnNewType.HiddenColumns = "";
            this.lbtnNewType.Image = ((System.Drawing.Image)(resources.GetObject("lbtnNewType.Image")));
            this.lbtnNewType.LoadDependentEntities = true;
            this.lbtnNewType.Location = new System.Drawing.Point(224, 198);
            this.lbtnNewType.LookUpTitle = null;
            this.lbtnNewType.Name = "lbtnNewType";
            this.lbtnNewType.Size = new System.Drawing.Size(26, 21);
            this.lbtnNewType.SkipValidationOnLeave = false;
            this.lbtnNewType.SPName = "CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER";
            this.lbtnNewType.TabIndex = 24;
            this.lbtnNewType.TabStop = false;
            this.lbtnNewType.UseVisualStyleBackColor = true;
            // 
            // lbtnFinNo
            // 
            this.lbtnFinNo.ActionLOVExists = "Fin_NoExist";
            this.lbtnFinNo.ActionType = "Fin_No";
            this.lbtnFinNo.ConditionalFields = "";
            this.lbtnFinNo.CustomEnabled = true;
            this.lbtnFinNo.DataFieldMapping = "";
            this.lbtnFinNo.DependentLovControls = "";
            this.lbtnFinNo.HiddenColumns = "";
            this.lbtnFinNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnFinNo.Image")));
            this.lbtnFinNo.LoadDependentEntities = true;
            this.lbtnFinNo.Location = new System.Drawing.Point(255, 27);
            this.lbtnFinNo.LookUpTitle = null;
            this.lbtnFinNo.Name = "lbtnFinNo";
            this.lbtnFinNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnFinNo.SkipValidationOnLeave = false;
            this.lbtnFinNo.SPName = "CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER";
            this.lbtnFinNo.TabIndex = 23;
            this.lbtnFinNo.TabStop = false;
            this.lbtnFinNo.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(324, 232);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "+= DR  -= CR";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(314, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "MarkUp % :";
            // 
            // txtAdjAmnt
            // 
            this.txtAdjAmnt.AllowSpace = true;
            this.txtAdjAmnt.AssociatedLookUpName = "";
            this.txtAdjAmnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdjAmnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAdjAmnt.ContinuationTextBox = null;
            this.txtAdjAmnt.CustomEnabled = true;
            this.txtAdjAmnt.DataFieldMapping = "FNT_ADJ_AMT";
            this.txtAdjAmnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdjAmnt.GetRecordsOnUpDownKeys = false;
            this.txtAdjAmnt.IsDate = false;
            this.txtAdjAmnt.Location = new System.Drawing.Point(147, 225);
            this.txtAdjAmnt.MaxLength = 8;
            this.txtAdjAmnt.Name = "txtAdjAmnt";
            this.txtAdjAmnt.NumberFormat = "###,###,##0.00";
            this.txtAdjAmnt.Postfix = "";
            this.txtAdjAmnt.Prefix = "";
            this.txtAdjAmnt.Size = new System.Drawing.Size(134, 20);
            this.txtAdjAmnt.SkipValidation = false;
            this.txtAdjAmnt.TabIndex = 4;
            this.txtAdjAmnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAdjAmnt.TextType = CrplControlLibrary.TextType.String;
            this.txtAdjAmnt.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtAdjAmnt_PreviewKeyDown);
            this.txtAdjAmnt.Leave += new System.EventHandler(this.txtAdjAmnt_Leave);
            this.txtAdjAmnt.Validating += new System.ComponentModel.CancelEventHandler(this.txtAdjAmnt_Validating);
            // 
            // txtnewDesc
            // 
            this.txtnewDesc.AllowSpace = true;
            this.txtnewDesc.AssociatedLookUpName = "";
            this.txtnewDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnewDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnewDesc.ContinuationTextBox = null;
            this.txtnewDesc.CustomEnabled = false;
            this.txtnewDesc.DataFieldMapping = "F_NEW_DESC";
            this.txtnewDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnewDesc.GetRecordsOnUpDownKeys = false;
            this.txtnewDesc.IsDate = false;
            this.txtnewDesc.Location = new System.Drawing.Point(256, 199);
            this.txtnewDesc.Name = "txtnewDesc";
            this.txtnewDesc.NumberFormat = "###,###,##0.00";
            this.txtnewDesc.Postfix = "";
            this.txtnewDesc.Prefix = "";
            this.txtnewDesc.Size = new System.Drawing.Size(183, 20);
            this.txtnewDesc.SkipValidation = false;
            this.txtnewDesc.TabIndex = 17;
            this.txtnewDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTransfertype
            // 
            this.txtTransfertype.AllowSpace = true;
            this.txtTransfertype.AssociatedLookUpName = "lbtnNewType";
            this.txtTransfertype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTransfertype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTransfertype.ContinuationTextBox = null;
            this.txtTransfertype.CustomEnabled = true;
            this.txtTransfertype.DataFieldMapping = "FNT_NEW_TYPE";
            this.txtTransfertype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransfertype.GetRecordsOnUpDownKeys = false;
            this.txtTransfertype.IsDate = false;
            this.txtTransfertype.IsRequired = true;
            this.txtTransfertype.Location = new System.Drawing.Point(147, 199);
            this.txtTransfertype.Name = "txtTransfertype";
            this.txtTransfertype.NumberFormat = "###,###,##0.00";
            this.txtTransfertype.Postfix = "";
            this.txtTransfertype.Prefix = "";
            this.txtTransfertype.Size = new System.Drawing.Size(71, 20);
            this.txtTransfertype.SkipValidation = false;
            this.txtTransfertype.TabIndex = 3;
            this.txtTransfertype.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtTransfertype, "Enter Transfer Type  Press F9 for Help");
            // 
            // txtMarkup
            // 
            this.txtMarkup.AllowSpace = true;
            this.txtMarkup.AssociatedLookUpName = "";
            this.txtMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkup.ContinuationTextBox = null;
            this.txtMarkup.CustomEnabled = false;
            this.txtMarkup.DataFieldMapping = "F_NMARKUP";
            this.txtMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkup.GetRecordsOnUpDownKeys = false;
            this.txtMarkup.IsDate = false;
            this.txtMarkup.Location = new System.Drawing.Point(392, 171);
            this.txtMarkup.Name = "txtMarkup";
            this.txtMarkup.NumberFormat = "###,###,##0.00";
            this.txtMarkup.Postfix = "";
            this.txtMarkup.Prefix = "";
            this.txtMarkup.Size = new System.Drawing.Size(100, 20);
            this.txtMarkup.SkipValidation = false;
            this.txtMarkup.TabIndex = 15;
            this.txtMarkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkup.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = false;
            this.txtPersonnelNo.DataFieldMapping = "FNT_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.Location = new System.Drawing.Point(147, 88);
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(71, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 13;
            this.txtPersonnelNo.TabStop = false;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelName
            // 
            this.txtPersonnelName.AllowSpace = true;
            this.txtPersonnelName.AssociatedLookUpName = "";
            this.txtPersonnelName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelName.ContinuationTextBox = null;
            this.txtPersonnelName.CustomEnabled = false;
            this.txtPersonnelName.DataFieldMapping = "F_NAME";
            this.txtPersonnelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelName.IsDate = false;
            this.txtPersonnelName.Location = new System.Drawing.Point(224, 88);
            this.txtPersonnelName.Name = "txtPersonnelName";
            this.txtPersonnelName.NumberFormat = "###,###,##0.00";
            this.txtPersonnelName.Postfix = "";
            this.txtPersonnelName.Prefix = "";
            this.txtPersonnelName.Size = new System.Drawing.Size(183, 20);
            this.txtPersonnelName.SkipValidation = false;
            this.txtPersonnelName.TabIndex = 12;
            this.txtPersonnelName.TabStop = false;
            this.txtPersonnelName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinDesc
            // 
            this.txtFinDesc.AllowSpace = true;
            this.txtFinDesc.AssociatedLookUpName = "";
            this.txtFinDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinDesc.ContinuationTextBox = null;
            this.txtFinDesc.CustomEnabled = false;
            this.txtFinDesc.DataFieldMapping = "F_OLD_DESC";
            this.txtFinDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinDesc.GetRecordsOnUpDownKeys = false;
            this.txtFinDesc.IsDate = false;
            this.txtFinDesc.Location = new System.Drawing.Point(224, 55);
            this.txtFinDesc.Name = "txtFinDesc";
            this.txtFinDesc.NumberFormat = "###,###,##0.00";
            this.txtFinDesc.Postfix = "";
            this.txtFinDesc.Prefix = "";
            this.txtFinDesc.Size = new System.Drawing.Size(183, 20);
            this.txtFinDesc.SkipValidation = false;
            this.txtFinDesc.TabIndex = 11;
            this.txtFinDesc.TabStop = false;
            this.txtFinDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtOldMrkUp
            // 
            this.txtOldMrkUp.AllowSpace = true;
            this.txtOldMrkUp.AssociatedLookUpName = "";
            this.txtOldMrkUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOldMrkUp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOldMrkUp.ContinuationTextBox = null;
            this.txtOldMrkUp.CustomEnabled = false;
            this.txtOldMrkUp.DataFieldMapping = "F_OMARKUP";
            this.txtOldMrkUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOldMrkUp.GetRecordsOnUpDownKeys = false;
            this.txtOldMrkUp.IsDate = false;
            this.txtOldMrkUp.Location = new System.Drawing.Point(396, 29);
            this.txtOldMrkUp.Name = "txtOldMrkUp";
            this.txtOldMrkUp.NumberFormat = "###,###,##0.00";
            this.txtOldMrkUp.Postfix = "";
            this.txtOldMrkUp.Prefix = "";
            this.txtOldMrkUp.Size = new System.Drawing.Size(100, 20);
            this.txtOldMrkUp.SkipValidation = false;
            this.txtOldMrkUp.TabIndex = 10;
            this.txtOldMrkUp.TabStop = false;
            this.txtOldMrkUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOldMrkUp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtOldFinType
            // 
            this.txtOldFinType.AllowSpace = true;
            this.txtOldFinType.AssociatedLookUpName = "";
            this.txtOldFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOldFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOldFinType.ContinuationTextBox = null;
            this.txtOldFinType.CustomEnabled = false;
            this.txtOldFinType.DataFieldMapping = "FNT_OLD_TYPE";
            this.txtOldFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOldFinType.GetRecordsOnUpDownKeys = false;
            this.txtOldFinType.IsDate = false;
            this.txtOldFinType.Location = new System.Drawing.Point(147, 55);
            this.txtOldFinType.Name = "txtOldFinType";
            this.txtOldFinType.NumberFormat = "###,###,##0.00";
            this.txtOldFinType.Postfix = "";
            this.txtOldFinType.Prefix = "";
            this.txtOldFinType.Size = new System.Drawing.Size(71, 20);
            this.txtOldFinType.SkipValidation = false;
            this.txtOldFinType.TabIndex = 9;
            this.txtOldFinType.TabStop = false;
            this.txtOldFinType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinNo
            // 
            this.txtFinNo.AllowSpace = true;
            this.txtFinNo.AssociatedLookUpName = "lbtnFinNo";
            this.txtFinNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinNo.ContinuationTextBox = null;
            this.txtFinNo.CustomEnabled = true;
            this.txtFinNo.DataFieldMapping = "FNT_FIN_NO";
            this.txtFinNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinNo.GetRecordsOnUpDownKeys = false;
            this.txtFinNo.IsDate = false;
            this.txtFinNo.IsRequired = true;
            this.txtFinNo.Location = new System.Drawing.Point(147, 27);
            this.txtFinNo.Name = "txtFinNo";
            this.txtFinNo.NumberFormat = "###,###,##0.00";
            this.txtFinNo.Postfix = "";
            this.txtFinNo.Prefix = "";
            this.txtFinNo.Size = new System.Drawing.Size(100, 20);
            this.txtFinNo.SkipValidation = false;
            this.txtFinNo.TabIndex = 1;
            this.txtFinNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtFinNo, "Enter Finance No. Press F9 for Help");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 232);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Adjustment Amount :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Transfer Type :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Transfer Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Personnel Number  :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(322, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "MarkUp % :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Old Finance Type :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Finance Number :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(464, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 52;
            this.label11.Text = "User Name :  ";
            // 
            // CHRIS_Finance_TransferFinanceType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 509);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.PnlDetail);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "PnlDetail";
            this.Name = "CHRIS_Finance_TransferFinanceType";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRISTOP - Transfer Finance Type";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_TransferFinanceType_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.PnlDetail.ResumeLayout(false);
            this.PnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlDetail;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private CrplControlLibrary.SLTextBox txtPersonnelName;
        private CrplControlLibrary.SLTextBox txtFinDesc;
        private CrplControlLibrary.SLTextBox txtOldMrkUp;
        private CrplControlLibrary.SLTextBox txtOldFinType;
        private CrplControlLibrary.SLTextBox txtFinNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtAdjAmnt;
        private CrplControlLibrary.SLTextBox txtnewDesc;
        private CrplControlLibrary.SLTextBox txtTransfertype;
        private CrplControlLibrary.SLTextBox txtMarkup;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.LookupButton lbtnNewType;
        private CrplControlLibrary.LookupButton lbtnFinNo;
        private CrplControlLibrary.SLDatePicker dtTransfer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
    }
}