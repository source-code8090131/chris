namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinLiquidationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinLiquidationEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtpersonal = new CrplControlLibrary.SLTextBox(this.components);
            this.fnfinNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlFinanceLiquidation = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.transferDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTstartdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPersonnalName = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlTblDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dtGrdDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_Segment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_Dept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cont = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPRBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfndebit = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInstLeft = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInstRec = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtfnMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnCRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMarkupLeft = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMarkUpRec = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtAdjustment = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPrNewAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnliqFlag = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnCredit = new CrplControlLibrary.SLTextBox(this.components);
            this.DTPAY_GEN_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtExtratime = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTFN_END_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbType = new CrplControlLibrary.LookupButton(this.components);
            this.txtFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbtnPersonnal = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersonnalNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlFinanceLiquidation.SuspendLayout();
            this.pnlTblDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGrdDept)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(588, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(624, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(624, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtpersonal);
            this.pnlHead.Controls.Add(this.fnfinNo);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 77);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(596, 74);
            this.pnlHead.TabIndex = 42;
            // 
            // txtpersonal
            // 
            this.txtpersonal.AllowSpace = true;
            this.txtpersonal.AssociatedLookUpName = "";
            this.txtpersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpersonal.ContinuationTextBox = null;
            this.txtpersonal.CustomEnabled = true;
            this.txtpersonal.DataFieldMapping = "";
            this.txtpersonal.Enabled = false;
            this.txtpersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpersonal.GetRecordsOnUpDownKeys = false;
            this.txtpersonal.IsDate = false;
            this.txtpersonal.Location = new System.Drawing.Point(400, 41);
            this.txtpersonal.MaxLength = 10;
            this.txtpersonal.Name = "txtpersonal";
            this.txtpersonal.NumberFormat = "###,###,##0.00";
            this.txtpersonal.Postfix = "";
            this.txtpersonal.Prefix = "";
            this.txtpersonal.Size = new System.Drawing.Size(52, 20);
            this.txtpersonal.SkipValidation = false;
            this.txtpersonal.TabIndex = 44;
            this.txtpersonal.TabStop = false;
            this.txtpersonal.TextType = CrplControlLibrary.TextType.String;
            this.txtpersonal.Visible = false;
            // 
            // fnfinNo
            // 
            this.fnfinNo.AllowSpace = true;
            this.fnfinNo.AssociatedLookUpName = "";
            this.fnfinNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fnfinNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.fnfinNo.ContinuationTextBox = null;
            this.fnfinNo.CustomEnabled = true;
            this.fnfinNo.DataFieldMapping = "";
            this.fnfinNo.Enabled = false;
            this.fnfinNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnfinNo.GetRecordsOnUpDownKeys = false;
            this.fnfinNo.IsDate = false;
            this.fnfinNo.Location = new System.Drawing.Point(400, 20);
            this.fnfinNo.MaxLength = 10;
            this.fnfinNo.Name = "fnfinNo";
            this.fnfinNo.NumberFormat = "###,###,##0.00";
            this.fnfinNo.Postfix = "";
            this.fnfinNo.Prefix = "";
            this.fnfinNo.Size = new System.Drawing.Size(52, 20);
            this.fnfinNo.SkipValidation = false;
            this.fnfinNo.TabIndex = 43;
            this.fnfinNo.TabStop = false;
            this.fnfinNo.TextType = CrplControlLibrary.TextType.String;
            this.fnfinNo.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(180, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "LIQUIDATION OF FINANCE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(180, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(237, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "FINANCE SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(505, 38);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            this.txtDate.Visible = false;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(505, 12);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(458, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label15.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(450, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 37);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 11);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // pnlFinanceLiquidation
            // 
            this.pnlFinanceLiquidation.ConcurrentPanels = null;
            this.pnlFinanceLiquidation.Controls.Add(this.transferDate);
            this.pnlFinanceLiquidation.Controls.Add(this.DTstartdate);
            this.pnlFinanceLiquidation.Controls.Add(this.txtPersonnalName);
            this.pnlFinanceLiquidation.Controls.Add(this.pnlTblDept);
            this.pnlFinanceLiquidation.Controls.Add(this.txtPRBranch);
            this.pnlFinanceLiquidation.Controls.Add(this.txtBal);
            this.pnlFinanceLiquidation.Controls.Add(this.txtfndebit);
            this.pnlFinanceLiquidation.Controls.Add(this.txtInstLeft);
            this.pnlFinanceLiquidation.Controls.Add(this.txtInstRec);
            this.pnlFinanceLiquidation.Controls.Add(this.label9);
            this.pnlFinanceLiquidation.Controls.Add(this.label10);
            this.pnlFinanceLiquidation.Controls.Add(this.txtfnMarkup);
            this.pnlFinanceLiquidation.Controls.Add(this.txtfnCRatio);
            this.pnlFinanceLiquidation.Controls.Add(this.txtMarkupLeft);
            this.pnlFinanceLiquidation.Controls.Add(this.txtMarkUpRec);
            this.pnlFinanceLiquidation.Controls.Add(this.label6);
            this.pnlFinanceLiquidation.Controls.Add(this.label7);
            this.pnlFinanceLiquidation.Controls.Add(this.dtAdjustment);
            this.pnlFinanceLiquidation.Controls.Add(this.txtPrNewAnnual);
            this.pnlFinanceLiquidation.Controls.Add(this.txtfnliqFlag);
            this.pnlFinanceLiquidation.Controls.Add(this.txtfnCredit);
            this.pnlFinanceLiquidation.Controls.Add(this.DTPAY_GEN_DATE);
            this.pnlFinanceLiquidation.Controls.Add(this.dtExtratime);
            this.pnlFinanceLiquidation.Controls.Add(this.DTFN_END_DATE);
            this.pnlFinanceLiquidation.Controls.Add(this.label24);
            this.pnlFinanceLiquidation.Controls.Add(this.label22);
            this.pnlFinanceLiquidation.Controls.Add(this.label20);
            this.pnlFinanceLiquidation.Controls.Add(this.label19);
            this.pnlFinanceLiquidation.Controls.Add(this.label13);
            this.pnlFinanceLiquidation.Controls.Add(this.label12);
            this.pnlFinanceLiquidation.Controls.Add(this.txtFinanceNo);
            this.pnlFinanceLiquidation.Controls.Add(this.label8);
            this.pnlFinanceLiquidation.Controls.Add(this.lbType);
            this.pnlFinanceLiquidation.Controls.Add(this.txtFinType);
            this.pnlFinanceLiquidation.Controls.Add(this.label4);
            this.pnlFinanceLiquidation.Controls.Add(this.label2);
            this.pnlFinanceLiquidation.Controls.Add(this.lbtnPersonnal);
            this.pnlFinanceLiquidation.Controls.Add(this.txtPersonnalNo);
            this.pnlFinanceLiquidation.Controls.Add(this.label1);
            this.pnlFinanceLiquidation.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlFinanceLiquidation.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlFinanceLiquidation.DependentPanels = null;
            this.pnlFinanceLiquidation.DisableDependentLoad = false;
            this.pnlFinanceLiquidation.EnableDelete = true;
            this.pnlFinanceLiquidation.EnableInsert = true;
            this.pnlFinanceLiquidation.EnableQuery = false;
            this.pnlFinanceLiquidation.EnableUpdate = true;
            this.pnlFinanceLiquidation.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinLiquidationfnMonthCommand";
            this.pnlFinanceLiquidation.Location = new System.Drawing.Point(12, 160);
            this.pnlFinanceLiquidation.MasterPanel = null;
            this.pnlFinanceLiquidation.Name = "pnlFinanceLiquidation";
            this.pnlFinanceLiquidation.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlFinanceLiquidation.Size = new System.Drawing.Size(596, 419);
            this.pnlFinanceLiquidation.SPName = "CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER";
            this.pnlFinanceLiquidation.TabIndex = 43;
            // 
            // transferDate
            // 
            this.transferDate.CustomEnabled = false;
            this.transferDate.CustomFormat = "dd/MM/yyyy";
            this.transferDate.DataFieldMapping = "pr_transfer_date";
            this.transferDate.Enabled = false;
            this.transferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.transferDate.HasChanges = false;
            this.transferDate.Location = new System.Drawing.Point(419, 78);
            this.transferDate.Name = "transferDate";
            this.transferDate.NullValue = " ";
            this.transferDate.Size = new System.Drawing.Size(100, 20);
            this.transferDate.TabIndex = 109;
            this.transferDate.TabStop = false;
            this.transferDate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.transferDate.Visible = false;
            // 
            // DTstartdate
            // 
            this.DTstartdate.CustomEnabled = false;
            this.DTstartdate.CustomFormat = "dd/MM/yyyy";
            this.DTstartdate.DataFieldMapping = "FN_START_DATE";
            this.DTstartdate.Enabled = false;
            this.DTstartdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTstartdate.HasChanges = false;
            this.DTstartdate.IsRequired = true;
            this.DTstartdate.Location = new System.Drawing.Point(174, 123);
            this.DTstartdate.Name = "DTstartdate";
            this.DTstartdate.NullValue = " ";
            this.DTstartdate.Size = new System.Drawing.Size(100, 20);
            this.DTstartdate.TabIndex = 108;
            this.DTstartdate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // txtPersonnalName
            // 
            this.txtPersonnalName.AllowSpace = true;
            this.txtPersonnalName.AssociatedLookUpName = "";
            this.txtPersonnalName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalName.ContinuationTextBox = null;
            this.txtPersonnalName.CustomEnabled = false;
            this.txtPersonnalName.DataFieldMapping = "Pr_Name";
            this.txtPersonnalName.Enabled = false;
            this.txtPersonnalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalName.IsDate = false;
            this.txtPersonnalName.Location = new System.Drawing.Point(174, 48);
            this.txtPersonnalName.MaxLength = 40;
            this.txtPersonnalName.Name = "txtPersonnalName";
            this.txtPersonnalName.NumberFormat = "###,###,##0.00";
            this.txtPersonnalName.Postfix = "";
            this.txtPersonnalName.Prefix = "";
            this.txtPersonnalName.Size = new System.Drawing.Size(321, 20);
            this.txtPersonnalName.SkipValidation = false;
            this.txtPersonnalName.TabIndex = 25;
            this.txtPersonnalName.TabStop = false;
            this.txtPersonnalName.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlTblDept
            // 
            this.pnlTblDept.ConcurrentPanels = null;
            this.pnlTblDept.Controls.Add(this.dtGrdDept);
            this.pnlTblDept.DataManager = null;
            this.pnlTblDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept.DependentPanels = null;
            this.pnlTblDept.DisableDependentLoad = false;
            this.pnlTblDept.EnableDelete = true;
            this.pnlTblDept.EnableInsert = true;
            this.pnlTblDept.EnableQuery = false;
            this.pnlTblDept.EnableUpdate = true;
            this.pnlTblDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceLiquidateDeptCommand";
            this.pnlTblDept.Location = new System.Drawing.Point(289, 155);
            this.pnlTblDept.MasterPanel = this.pnlFinanceLiquidation;
            this.pnlTblDept.Name = "pnlTblDept";
            this.pnlTblDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept.Size = new System.Drawing.Size(292, 183);
            this.pnlTblDept.SPName = "CHRIS_SP_Fin_Liquidate_DEPT_CONT_MANAGER";
            this.pnlTblDept.TabIndex = 45;
            // 
            // dtGrdDept
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGrdDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtGrdDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGrdDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_Segment,
            this.PR_Dept,
            this.Cont});
            this.dtGrdDept.ColumnToHide = null;
            this.dtGrdDept.ColumnWidth = null;
            this.dtGrdDept.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtGrdDept.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtGrdDept.DisplayColumnWrapper = null;
            this.dtGrdDept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGrdDept.GridDefaultRow = 0;
            this.dtGrdDept.Location = new System.Drawing.Point(0, 0);
            this.dtGrdDept.Name = "dtGrdDept";
            this.dtGrdDept.ReadOnlyColumns = null;
            this.dtGrdDept.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGrdDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtGrdDept.Size = new System.Drawing.Size(292, 183);
            this.dtGrdDept.SkippingColumns = null;
            this.dtGrdDept.TabIndex = 44;
            this.dtGrdDept.TabStop = false;
            // 
            // PR_Segment
            // 
            this.PR_Segment.DataPropertyName = "PR_SEGMENT";
            this.PR_Segment.HeaderText = "Segment";
            this.PR_Segment.Name = "PR_Segment";
            this.PR_Segment.ReadOnly = true;
            // 
            // PR_Dept
            // 
            this.PR_Dept.DataPropertyName = "PR_DEPT";
            this.PR_Dept.HeaderText = "Dept";
            this.PR_Dept.Name = "PR_Dept";
            this.PR_Dept.ReadOnly = true;
            // 
            // Cont
            // 
            this.Cont.DataPropertyName = "PR_CONTRIB";
            this.Cont.HeaderText = "Cont";
            this.Cont.Name = "Cont";
            this.Cont.ReadOnly = true;
            // 
            // txtPRBranch
            // 
            this.txtPRBranch.AllowSpace = true;
            this.txtPRBranch.AssociatedLookUpName = "";
            this.txtPRBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPRBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPRBranch.ContinuationTextBox = null;
            this.txtPRBranch.CustomEnabled = true;
            this.txtPRBranch.DataFieldMapping = "FN_N_BRANCH";
            this.txtPRBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRBranch.GetRecordsOnUpDownKeys = false;
            this.txtPRBranch.IsDate = false;
            this.txtPRBranch.Location = new System.Drawing.Point(329, 21);
            this.txtPRBranch.MaxLength = 500;
            this.txtPRBranch.Name = "txtPRBranch";
            this.txtPRBranch.NumberFormat = "###,###,##0.00";
            this.txtPRBranch.Postfix = "";
            this.txtPRBranch.Prefix = "";
            this.txtPRBranch.ReadOnly = true;
            this.txtPRBranch.Size = new System.Drawing.Size(34, 20);
            this.txtPRBranch.SkipValidation = false;
            this.txtPRBranch.TabIndex = 107;
            this.txtPRBranch.TabStop = false;
            this.txtPRBranch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPRBranch.TextType = CrplControlLibrary.TextType.String;
            this.txtPRBranch.Visible = false;
            // 
            // txtBal
            // 
            this.txtBal.AllowSpace = true;
            this.txtBal.AssociatedLookUpName = "";
            this.txtBal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBal.ContinuationTextBox = null;
            this.txtBal.CustomEnabled = true;
            this.txtBal.DataFieldMapping = "FN_BALANCE";
            this.txtBal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBal.GetRecordsOnUpDownKeys = false;
            this.txtBal.IsDate = false;
            this.txtBal.Location = new System.Drawing.Point(341, 113);
            this.txtBal.Name = "txtBal";
            this.txtBal.NumberFormat = "###,###,##0.00";
            this.txtBal.Postfix = "";
            this.txtBal.Prefix = "";
            this.txtBal.Size = new System.Drawing.Size(59, 20);
            this.txtBal.SkipValidation = false;
            this.txtBal.TabIndex = 106;
            this.txtBal.TabStop = false;
            this.txtBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBal.TextType = CrplControlLibrary.TextType.Double;
            this.txtBal.Visible = false;
            // 
            // txtfndebit
            // 
            this.txtfndebit.AllowSpace = true;
            this.txtfndebit.AssociatedLookUpName = "";
            this.txtfndebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfndebit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfndebit.ContinuationTextBox = null;
            this.txtfndebit.CustomEnabled = true;
            this.txtfndebit.DataFieldMapping = "FN_DEBIT";
            this.txtfndebit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfndebit.GetRecordsOnUpDownKeys = false;
            this.txtfndebit.IsDate = false;
            this.txtfndebit.Location = new System.Drawing.Point(289, 113);
            this.txtfndebit.Name = "txtfndebit";
            this.txtfndebit.NumberFormat = "###,###,##0.00";
            this.txtfndebit.Postfix = "";
            this.txtfndebit.Prefix = "";
            this.txtfndebit.Size = new System.Drawing.Size(46, 20);
            this.txtfndebit.SkipValidation = false;
            this.txtfndebit.TabIndex = 105;
            this.txtfndebit.TabStop = false;
            this.txtfndebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfndebit.TextType = CrplControlLibrary.TextType.Double;
            this.txtfndebit.Visible = false;
            // 
            // txtInstLeft
            // 
            this.txtInstLeft.AllowSpace = true;
            this.txtInstLeft.AssociatedLookUpName = "";
            this.txtInstLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInstLeft.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInstLeft.ContinuationTextBox = null;
            this.txtInstLeft.CustomEnabled = true;
            this.txtInstLeft.DataFieldMapping = "";
            this.txtInstLeft.Enabled = false;
            this.txtInstLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstLeft.GetRecordsOnUpDownKeys = false;
            this.txtInstLeft.IsDate = false;
            this.txtInstLeft.Location = new System.Drawing.Point(174, 367);
            this.txtInstLeft.MaxLength = 9;
            this.txtInstLeft.Name = "txtInstLeft";
            this.txtInstLeft.NumberFormat = "###,###,##0.00";
            this.txtInstLeft.Postfix = "";
            this.txtInstLeft.Prefix = "";
            this.txtInstLeft.Size = new System.Drawing.Size(100, 20);
            this.txtInstLeft.SkipValidation = false;
            this.txtInstLeft.TabIndex = 104;
            this.txtInstLeft.TabStop = false;
            this.txtInstLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInstLeft.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtInstRec
            // 
            this.txtInstRec.AllowSpace = true;
            this.txtInstRec.AssociatedLookUpName = "";
            this.txtInstRec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInstRec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInstRec.ContinuationTextBox = null;
            this.txtInstRec.CustomEnabled = true;
            this.txtInstRec.DataFieldMapping = "";
            this.txtInstRec.Enabled = false;
            this.txtInstRec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstRec.GetRecordsOnUpDownKeys = false;
            this.txtInstRec.IsDate = false;
            this.txtInstRec.Location = new System.Drawing.Point(174, 342);
            this.txtInstRec.Name = "txtInstRec";
            this.txtInstRec.NumberFormat = "###,###,##0.00";
            this.txtInstRec.Postfix = "";
            this.txtInstRec.Prefix = "";
            this.txtInstRec.Size = new System.Drawing.Size(100, 20);
            this.txtInstRec.SkipValidation = false;
            this.txtInstRec.TabIndex = 103;
            this.txtInstRec.TabStop = false;
            this.txtInstRec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInstRec.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(72, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 15);
            this.label9.TabIndex = 102;
            this.label9.Text = "No. Of Inst. Left :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(72, 344);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 15);
            this.label10.TabIndex = 101;
            this.label10.Text = "No. Of Inst. Rec :";
            // 
            // txtfnMarkup
            // 
            this.txtfnMarkup.AllowSpace = true;
            this.txtfnMarkup.AssociatedLookUpName = "";
            this.txtfnMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnMarkup.ContinuationTextBox = null;
            this.txtfnMarkup.CustomEnabled = true;
            this.txtfnMarkup.DataFieldMapping = "finMarkup";
            this.txtfnMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnMarkup.GetRecordsOnUpDownKeys = false;
            this.txtfnMarkup.IsDate = false;
            this.txtfnMarkup.Location = new System.Drawing.Point(301, 76);
            this.txtfnMarkup.MaxLength = 500;
            this.txtfnMarkup.Name = "txtfnMarkup";
            this.txtfnMarkup.NumberFormat = "###,###,##0.00";
            this.txtfnMarkup.Postfix = "";
            this.txtfnMarkup.Prefix = "";
            this.txtfnMarkup.ReadOnly = true;
            this.txtfnMarkup.Size = new System.Drawing.Size(34, 20);
            this.txtfnMarkup.SkipValidation = false;
            this.txtfnMarkup.TabIndex = 100;
            this.txtfnMarkup.TabStop = false;
            this.txtfnMarkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnMarkup.TextType = CrplControlLibrary.TextType.Double;
            this.txtfnMarkup.Visible = false;
            // 
            // txtfnCRatio
            // 
            this.txtfnCRatio.AllowSpace = true;
            this.txtfnCRatio.AssociatedLookUpName = "";
            this.txtfnCRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnCRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnCRatio.ContinuationTextBox = null;
            this.txtfnCRatio.CustomEnabled = true;
            this.txtfnCRatio.DataFieldMapping = "FN_C_RATIO";
            this.txtfnCRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnCRatio.GetRecordsOnUpDownKeys = false;
            this.txtfnCRatio.IsDate = false;
            this.txtfnCRatio.Location = new System.Drawing.Point(261, 76);
            this.txtfnCRatio.MaxLength = 500;
            this.txtfnCRatio.Name = "txtfnCRatio";
            this.txtfnCRatio.NumberFormat = "###,###,##0.00";
            this.txtfnCRatio.Postfix = "";
            this.txtfnCRatio.Prefix = "";
            this.txtfnCRatio.ReadOnly = true;
            this.txtfnCRatio.Size = new System.Drawing.Size(34, 20);
            this.txtfnCRatio.SkipValidation = false;
            this.txtfnCRatio.TabIndex = 99;
            this.txtfnCRatio.TabStop = false;
            this.txtfnCRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnCRatio.TextType = CrplControlLibrary.TextType.Double;
            this.txtfnCRatio.Visible = false;
            // 
            // txtMarkupLeft
            // 
            this.txtMarkupLeft.AllowSpace = true;
            this.txtMarkupLeft.AssociatedLookUpName = "";
            this.txtMarkupLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkupLeft.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkupLeft.ContinuationTextBox = null;
            this.txtMarkupLeft.CustomEnabled = true;
            this.txtMarkupLeft.DataFieldMapping = "FN_MARKUP";
            this.txtMarkupLeft.Enabled = false;
            this.txtMarkupLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkupLeft.GetRecordsOnUpDownKeys = false;
            this.txtMarkupLeft.IsDate = false;
            this.txtMarkupLeft.Location = new System.Drawing.Point(174, 316);
            this.txtMarkupLeft.MaxLength = 9;
            this.txtMarkupLeft.Name = "txtMarkupLeft";
            this.txtMarkupLeft.NumberFormat = "########0.00";
            this.txtMarkupLeft.Postfix = "";
            this.txtMarkupLeft.Prefix = "";
            this.txtMarkupLeft.Size = new System.Drawing.Size(100, 20);
            this.txtMarkupLeft.SkipValidation = false;
            this.txtMarkupLeft.TabIndex = 98;
            this.txtMarkupLeft.TabStop = false;
            this.txtMarkupLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkupLeft.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtMarkUpRec
            // 
            this.txtMarkUpRec.AllowSpace = true;
            this.txtMarkUpRec.AssociatedLookUpName = "";
            this.txtMarkUpRec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkUpRec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkUpRec.ContinuationTextBox = null;
            this.txtMarkUpRec.CustomEnabled = true;
            this.txtMarkUpRec.DataFieldMapping = "";
            this.txtMarkUpRec.Enabled = false;
            this.txtMarkUpRec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkUpRec.GetRecordsOnUpDownKeys = false;
            this.txtMarkUpRec.IsDate = false;
            this.txtMarkUpRec.Location = new System.Drawing.Point(174, 291);
            this.txtMarkUpRec.Name = "txtMarkUpRec";
            this.txtMarkUpRec.NumberFormat = "###,###,##0.00";
            this.txtMarkUpRec.Postfix = "";
            this.txtMarkUpRec.Prefix = "";
            this.txtMarkUpRec.Size = new System.Drawing.Size(100, 20);
            this.txtMarkUpRec.SkipValidation = false;
            this.txtMarkUpRec.TabIndex = 97;
            this.txtMarkUpRec.TabStop = false;
            this.txtMarkUpRec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkUpRec.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(93, 318);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 15);
            this.label6.TabIndex = 96;
            this.label6.Text = "Markup Left :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(61, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 15);
            this.label7.TabIndex = 95;
            this.label7.Text = "Markup Received :";
            // 
            // dtAdjustment
            // 
            this.dtAdjustment.CustomEnabled = true;
            this.dtAdjustment.CustomFormat = "dd/MM/yyyy";
            this.dtAdjustment.DataFieldMapping = "FN_MDATE";
            this.dtAdjustment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAdjustment.HasChanges = true;
            this.dtAdjustment.IsRequired = true;
            this.dtAdjustment.Location = new System.Drawing.Point(174, 238);
            this.dtAdjustment.Name = "dtAdjustment";
            this.dtAdjustment.NullValue = " ";
            this.dtAdjustment.Size = new System.Drawing.Size(100, 20);
            this.dtAdjustment.TabIndex = 4;
            this.dtAdjustment.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.dtAdjustment.Validating += new System.ComponentModel.CancelEventHandler(this.dtAdjustment_Validating);
            // 
            // txtPrNewAnnual
            // 
            this.txtPrNewAnnual.AllowSpace = true;
            this.txtPrNewAnnual.AssociatedLookUpName = "";
            this.txtPrNewAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrNewAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrNewAnnual.ContinuationTextBox = null;
            this.txtPrNewAnnual.CustomEnabled = true;
            this.txtPrNewAnnual.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtPrNewAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrNewAnnual.GetRecordsOnUpDownKeys = false;
            this.txtPrNewAnnual.IsDate = false;
            this.txtPrNewAnnual.Location = new System.Drawing.Point(289, 21);
            this.txtPrNewAnnual.MaxLength = 500;
            this.txtPrNewAnnual.Name = "txtPrNewAnnual";
            this.txtPrNewAnnual.NumberFormat = "###,###,##0.00";
            this.txtPrNewAnnual.Postfix = "";
            this.txtPrNewAnnual.Prefix = "";
            this.txtPrNewAnnual.ReadOnly = true;
            this.txtPrNewAnnual.Size = new System.Drawing.Size(34, 20);
            this.txtPrNewAnnual.SkipValidation = false;
            this.txtPrNewAnnual.TabIndex = 86;
            this.txtPrNewAnnual.TabStop = false;
            this.txtPrNewAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrNewAnnual.TextType = CrplControlLibrary.TextType.Double;
            this.txtPrNewAnnual.Visible = false;
            // 
            // txtfnliqFlag
            // 
            this.txtfnliqFlag.AllowSpace = true;
            this.txtfnliqFlag.AssociatedLookUpName = "";
            this.txtfnliqFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnliqFlag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnliqFlag.ContinuationTextBox = null;
            this.txtfnliqFlag.CustomEnabled = true;
            this.txtfnliqFlag.DataFieldMapping = "FN_LIQ_FLAG";
            this.txtfnliqFlag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnliqFlag.GetRecordsOnUpDownKeys = false;
            this.txtfnliqFlag.IsDate = false;
            this.txtfnliqFlag.Location = new System.Drawing.Point(448, 21);
            this.txtfnliqFlag.MaxLength = 500;
            this.txtfnliqFlag.Name = "txtfnliqFlag";
            this.txtfnliqFlag.NumberFormat = "###,###,##0.00";
            this.txtfnliqFlag.Postfix = "";
            this.txtfnliqFlag.Prefix = "";
            this.txtfnliqFlag.ReadOnly = true;
            this.txtfnliqFlag.Size = new System.Drawing.Size(34, 20);
            this.txtfnliqFlag.SkipValidation = false;
            this.txtfnliqFlag.TabIndex = 85;
            this.txtfnliqFlag.TabStop = false;
            this.txtfnliqFlag.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnliqFlag.TextType = CrplControlLibrary.TextType.String;
            this.txtfnliqFlag.Visible = false;
            // 
            // txtfnCredit
            // 
            this.txtfnCredit.AllowSpace = true;
            this.txtfnCredit.AssociatedLookUpName = "";
            this.txtfnCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnCredit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnCredit.ContinuationTextBox = null;
            this.txtfnCredit.CustomEnabled = true;
            this.txtfnCredit.DataFieldMapping = "FN_CREDIT";
            this.txtfnCredit.Enabled = false;
            this.txtfnCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnCredit.GetRecordsOnUpDownKeys = false;
            this.txtfnCredit.IsDate = false;
            this.txtfnCredit.Location = new System.Drawing.Point(174, 265);
            this.txtfnCredit.MaxLength = 9;
            this.txtfnCredit.Name = "txtfnCredit";
            this.txtfnCredit.NumberFormat = "###,###,##0.00";
            this.txtfnCredit.Postfix = "";
            this.txtfnCredit.Prefix = "";
            this.txtfnCredit.Size = new System.Drawing.Size(100, 20);
            this.txtfnCredit.SkipValidation = false;
            this.txtfnCredit.TabIndex = 64;
            this.txtfnCredit.TabStop = false;
            this.txtfnCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnCredit.TextType = CrplControlLibrary.TextType.Double;
            // 
            // DTPAY_GEN_DATE
            // 
            this.DTPAY_GEN_DATE.CustomEnabled = false;
            this.DTPAY_GEN_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTPAY_GEN_DATE.DataFieldMapping = "";
            this.DTPAY_GEN_DATE.Enabled = false;
            this.DTPAY_GEN_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPAY_GEN_DATE.HasChanges = false;
            this.DTPAY_GEN_DATE.Location = new System.Drawing.Point(174, 212);
            this.DTPAY_GEN_DATE.Name = "DTPAY_GEN_DATE";
            this.DTPAY_GEN_DATE.NullValue = " ";
            this.DTPAY_GEN_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTPAY_GEN_DATE.TabIndex = 60;
            this.DTPAY_GEN_DATE.TabStop = false;
            this.DTPAY_GEN_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // dtExtratime
            // 
            this.dtExtratime.CustomEnabled = false;
            this.dtExtratime.CustomFormat = "dd/MM/yyyy";
            this.dtExtratime.DataFieldMapping = "FN_EXTRA_TIME";
            this.dtExtratime.Enabled = false;
            this.dtExtratime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExtratime.HasChanges = false;
            this.dtExtratime.Location = new System.Drawing.Point(174, 170);
            this.dtExtratime.Name = "dtExtratime";
            this.dtExtratime.NullValue = " ";
            this.dtExtratime.Size = new System.Drawing.Size(100, 20);
            this.dtExtratime.TabIndex = 59;
            this.dtExtratime.TabStop = false;
            this.dtExtratime.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // DTFN_END_DATE
            // 
            this.DTFN_END_DATE.CustomEnabled = false;
            this.DTFN_END_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTFN_END_DATE.DataFieldMapping = "FN_END_DATE";
            this.DTFN_END_DATE.Enabled = false;
            this.DTFN_END_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTFN_END_DATE.HasChanges = false;
            this.DTFN_END_DATE.Location = new System.Drawing.Point(174, 147);
            this.DTFN_END_DATE.Name = "DTFN_END_DATE";
            this.DTFN_END_DATE.NullValue = " ";
            this.DTFN_END_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTFN_END_DATE.TabIndex = 58;
            this.DTFN_END_DATE.TabStop = false;
            this.DTFN_END_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(63, 267);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(124, 15);
            this.label24.TabIndex = 52;
            this.label24.Text = "Principal Amount :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(67, 240);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(120, 15);
            this.label22.TabIndex = 50;
            this.label22.Text = "Adjustment Date :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(89, 215);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 15);
            this.label20.TabIndex = 48;
            this.label20.Text = "Payroll Date :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(98, 174);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 15);
            this.label19.TabIndex = 47;
            this.label19.Text = "Extra Time :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(94, 151);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 15);
            this.label13.TabIndex = 46;
            this.label13.Text = "Expiry Date :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(103, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 15);
            this.label12.TabIndex = 45;
            this.label12.Text = "Start Date :";
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = true;
            this.txtFinanceNo.DataFieldMapping = "FN_FIN_NO";
            this.txtFinanceNo.Enabled = false;
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.Location = new System.Drawing.Point(174, 100);
            this.txtFinanceNo.MaxLength = 10;
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(74, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 38;
            this.txtFinanceNo.TabStop = false;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(109, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 15);
            this.label8.TabIndex = 37;
            this.label8.Text = "L/Fin No. :";
            // 
            // lbType
            // 
            this.lbType.ActionLOVExists = "FinTypeLovEXISTS";
            this.lbType.ActionType = "FinTypeLov";
            this.lbType.ConditionalFields = "txtPersonnalNo";
            this.lbType.CustomEnabled = true;
            this.lbType.DataFieldMapping = "";
            this.lbType.DependentLovControls = "";
            this.lbType.HiddenColumns = "FN_C_RATIO|finMarkup|FN_START_DATE|FN_END_DATE|FN_EXTRA_TIME|INST_REC";
            this.lbType.Image = ((System.Drawing.Image)(resources.GetObject("lbType.Image")));
            this.lbType.LoadDependentEntities = false;
            this.lbType.Location = new System.Drawing.Point(229, 74);
            this.lbType.LookUpTitle = null;
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(26, 21);
            this.lbType.SkipValidationOnLeave = false;
            this.lbType.SPName = "CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER";
            this.lbType.TabIndex = 31;
            this.lbType.TabStop = false;
            this.lbType.Tag = "";
            this.lbType.UseVisualStyleBackColor = true;
            // 
            // txtFinType
            // 
            this.txtFinType.AllowSpace = true;
            this.txtFinType.AssociatedLookUpName = "lbType";
            this.txtFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinType.ContinuationTextBox = null;
            this.txtFinType.CustomEnabled = true;
            this.txtFinType.DataFieldMapping = "FN_TYPE";
            this.txtFinType.Enabled = false;
            this.txtFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinType.GetRecordsOnUpDownKeys = false;
            this.txtFinType.IsDate = false;
            this.txtFinType.IsRequired = true;
            this.txtFinType.Location = new System.Drawing.Point(174, 74);
            this.txtFinType.MaxLength = 6;
            this.txtFinType.Name = "txtFinType";
            this.txtFinType.NumberFormat = "###,###,##0.00";
            this.txtFinType.Postfix = "";
            this.txtFinType.Prefix = "";
            this.txtFinType.Size = new System.Drawing.Size(52, 20);
            this.txtFinType.SkipValidation = false;
            this.txtFinType.TabIndex = 3;
            this.txtFinType.TextType = CrplControlLibrary.TextType.String;
            this.txtFinType.Validating += new System.ComponentModel.CancelEventHandler(this.txtFinType_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(101, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 27;
            this.label4.Text = "L/Fin Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(97, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 26;
            this.label2.Text = "First Name :";
            // 
            // lbtnPersonnal
            // 
            this.lbtnPersonnal.ActionLOVExists = "FinPersonalLovExists";
            this.lbtnPersonnal.ActionType = "FinPersonalLov";
            this.lbtnPersonnal.ConditionalFields = "";
            this.lbtnPersonnal.CustomEnabled = true;
            this.lbtnPersonnal.DataFieldMapping = "";
            this.lbtnPersonnal.DependentLovControls = "";
            this.lbtnPersonnal.HiddenColumns = "pr_transfer_date|PR_NEW_ANNUAL_PACK|FN_M_BRANCH";
            this.lbtnPersonnal.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnal.Image")));
            this.lbtnPersonnal.LoadDependentEntities = true;
            this.lbtnPersonnal.Location = new System.Drawing.Point(229, 21);
            this.lbtnPersonnal.LookUpTitle = null;
            this.lbtnPersonnal.Name = "lbtnPersonnal";
            this.lbtnPersonnal.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnal.SkipValidationOnLeave = false;
            this.lbtnPersonnal.SPName = "CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER";
            this.lbtnPersonnal.TabIndex = 24;
            this.lbtnPersonnal.TabStop = false;
            this.lbtnPersonnal.Tag = "";
            this.lbtnPersonnal.UseVisualStyleBackColor = true;
            // 
            // txtPersonnalNo
            // 
            this.txtPersonnalNo.AllowSpace = true;
            this.txtPersonnalNo.AssociatedLookUpName = "lbtnPersonnal";
            this.txtPersonnalNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalNo.ContinuationTextBox = null;
            this.txtPersonnalNo.CustomEnabled = true;
            this.txtPersonnalNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnalNo.Enabled = false;
            this.txtPersonnalNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalNo.IsDate = false;
            this.txtPersonnalNo.IsRequired = true;
            this.txtPersonnalNo.Location = new System.Drawing.Point(174, 22);
            this.txtPersonnalNo.MaxLength = 4;
            this.txtPersonnalNo.Name = "txtPersonnalNo";
            this.txtPersonnalNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnalNo.Postfix = "";
            this.txtPersonnalNo.Prefix = "";
            this.txtPersonnalNo.Size = new System.Drawing.Size(52, 20);
            this.txtPersonnalNo.SkipValidation = false;
            this.txtPersonnalNo.TabIndex = 2;
            this.txtPersonnalNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnalNo.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(80, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Personnel No. :";
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(398, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 118;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Finance_FinLiquidationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(624, 668);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlFinanceLiquidation);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlFinanceLiquidation";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.F7OptionText = "[F7]=View";
            this.Name = "CHRIS_Finance_FinLiquidationEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Finance_FinLiquidationEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlFinanceLiquidation, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlFinanceLiquidation.ResumeLayout(false);
            this.pnlFinanceLiquidation.PerformLayout();
            this.pnlTblDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtGrdDept)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlFinanceLiquidation;
        private CrplControlLibrary.SLDatePicker dtAdjustment;
        private CrplControlLibrary.SLTextBox txtPrNewAnnual;
        private CrplControlLibrary.SLTextBox txtfnliqFlag;
        private CrplControlLibrary.SLTextBox txtfnCredit;
        private CrplControlLibrary.SLDatePicker DTPAY_GEN_DATE;
        private CrplControlLibrary.SLDatePicker dtExtratime;
        private CrplControlLibrary.SLDatePicker DTFN_END_DATE;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbType;
        private CrplControlLibrary.SLTextBox txtFinType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtPersonnalName;
        private CrplControlLibrary.LookupButton lbtnPersonnal;
        private CrplControlLibrary.SLTextBox txtPersonnalNo;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtMarkupLeft;
        private CrplControlLibrary.SLTextBox txtMarkUpRec;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtfnMarkup;
        private CrplControlLibrary.SLTextBox txtfnCRatio;
        private CrplControlLibrary.SLTextBox txtInstLeft;
        private CrplControlLibrary.SLTextBox txtInstRec;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtfndebit;
        private CrplControlLibrary.SLTextBox txtBal;
        private CrplControlLibrary.SLTextBox txtPRBranch;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dtGrdDept;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_Segment;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cont;
        private System.Windows.Forms.Label txtUserName;
        private CrplControlLibrary.SLDatePicker DTstartdate;
        private CrplControlLibrary.SLTextBox txtpersonal;
        private CrplControlLibrary.SLTextBox fnfinNo;
        private CrplControlLibrary.SLDatePicker transferDate;
    }
}