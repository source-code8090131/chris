namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_SelfInsuraneEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_SelfInsuraneEntry));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.DeleteID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPaDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOptionMonth = new CrplControlLibrary.SLTextBox(this.components);
            this.dtLastPay = new CrplControlLibrary.SLDatePicker(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.txtAnuualReduction = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtInsuranceType = new CrplControlLibrary.SLTextBox(this.components);
            this.dtPay = new CrplControlLibrary.SLDatePicker(this.components);
            this.LBPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFirstName1 = new System.Windows.Forms.Label();
            this.txtPersonnelNo1 = new System.Windows.Forms.Label();
            this.txtPA_Premium = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVehicleChasisNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVehicleEngineNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAssignedBank = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInsuranceAnuualPremuim = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInsuranceAmount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPolicyNumber = new CrplControlLibrary.SLTextBox(this.components);
            this.txtComp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlPersonnel.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(598, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(634, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 547);
            this.panel1.Size = new System.Drawing.Size(634, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 84);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(609, 63);
            this.pnlHead.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(168, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "SELF INSURANCE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(165, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Finance System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 34);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(498, 8);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(455, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(447, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 34);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 8);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 34);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPersonnel
            // 
            this.pnlPersonnel.ConcurrentPanels = null;
            this.pnlPersonnel.Controls.Add(this.DeleteID);
            this.pnlPersonnel.Controls.Add(this.txtPaDate);
            this.pnlPersonnel.Controls.Add(this.txtOptionMonth);
            this.pnlPersonnel.Controls.Add(this.dtLastPay);
            this.pnlPersonnel.Controls.Add(this.label19);
            this.pnlPersonnel.Controls.Add(this.txtAnuualReduction);
            this.pnlPersonnel.Controls.Add(this.label12);
            this.pnlPersonnel.Controls.Add(this.txtInsuranceType);
            this.pnlPersonnel.Controls.Add(this.dtPay);
            this.pnlPersonnel.Controls.Add(this.LBPersonnel);
            this.pnlPersonnel.Controls.Add(this.label13);
            this.pnlPersonnel.Controls.Add(this.label11);
            this.pnlPersonnel.Controls.Add(this.label10);
            this.pnlPersonnel.Controls.Add(this.label9);
            this.pnlPersonnel.Controls.Add(this.label8);
            this.pnlPersonnel.Controls.Add(this.label7);
            this.pnlPersonnel.Controls.Add(this.label6);
            this.pnlPersonnel.Controls.Add(this.label5);
            this.pnlPersonnel.Controls.Add(this.label4);
            this.pnlPersonnel.Controls.Add(this.label2);
            this.pnlPersonnel.Controls.Add(this.txtFirstName1);
            this.pnlPersonnel.Controls.Add(this.txtPersonnelNo1);
            this.pnlPersonnel.Controls.Add(this.txtPA_Premium);
            this.pnlPersonnel.Controls.Add(this.txtVehicleChasisNo);
            this.pnlPersonnel.Controls.Add(this.txtVehicleEngineNo);
            this.pnlPersonnel.Controls.Add(this.txtAssignedBank);
            this.pnlPersonnel.Controls.Add(this.txtInsuranceAnuualPremuim);
            this.pnlPersonnel.Controls.Add(this.txtInsuranceAmount);
            this.pnlPersonnel.Controls.Add(this.txtPolicyNumber);
            this.pnlPersonnel.Controls.Add(this.txtComp);
            this.pnlPersonnel.Controls.Add(this.txtFirstName);
            this.pnlPersonnel.Controls.Add(this.txtPersonnelNo);
            this.pnlPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonnel.DependentPanels = null;
            this.pnlPersonnel.DisableDependentLoad = false;
            this.pnlPersonnel.EnableDelete = true;
            this.pnlPersonnel.EnableInsert = true;
            this.pnlPersonnel.EnableQuery = false;
            this.pnlPersonnel.EnableUpdate = true;
            this.pnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SelfInsuranceCommand";
            this.pnlPersonnel.Location = new System.Drawing.Point(12, 157);
            this.pnlPersonnel.MasterPanel = null;
            this.pnlPersonnel.Name = "pnlPersonnel";
            this.pnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonnel.Size = new System.Drawing.Size(609, 334);
            this.pnlPersonnel.SPName = "CHRIS_SP_FN_INSURANCE_MANAGER";
            this.pnlPersonnel.TabIndex = 0;
            // 
            // DeleteID
            // 
            this.DeleteID.AllowSpace = true;
            this.DeleteID.AssociatedLookUpName = "";
            this.DeleteID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DeleteID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DeleteID.ContinuationTextBox = null;
            this.DeleteID.CustomEnabled = true;
            this.DeleteID.DataFieldMapping = "";
            this.DeleteID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteID.GetRecordsOnUpDownKeys = false;
            this.DeleteID.IsDate = false;
            this.DeleteID.Location = new System.Drawing.Point(287, 157);
            this.DeleteID.MaxLength = 1;
            this.DeleteID.Name = "DeleteID";
            this.DeleteID.NumberFormat = "###,###,##0.00";
            this.DeleteID.Postfix = "";
            this.DeleteID.Prefix = "";
            this.DeleteID.Size = new System.Drawing.Size(34, 20);
            this.DeleteID.SkipValidation = false;
            this.DeleteID.TabIndex = 103;
            this.DeleteID.TabStop = false;
            this.DeleteID.TextType = CrplControlLibrary.TextType.String;
            this.DeleteID.Visible = false;
            // 
            // txtPaDate
            // 
            this.txtPaDate.AllowSpace = true;
            this.txtPaDate.AssociatedLookUpName = "";
            this.txtPaDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPaDate.ContinuationTextBox = null;
            this.txtPaDate.CustomEnabled = false;
            this.txtPaDate.DataFieldMapping = "PA_DATE";
            this.txtPaDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaDate.GetRecordsOnUpDownKeys = false;
            this.txtPaDate.IsDate = false;
            this.txtPaDate.Location = new System.Drawing.Point(443, 299);
            this.txtPaDate.Name = "txtPaDate";
            this.txtPaDate.NumberFormat = "###,###,##0.00";
            this.txtPaDate.Postfix = "";
            this.txtPaDate.Prefix = "";
            this.txtPaDate.Size = new System.Drawing.Size(88, 20);
            this.txtPaDate.SkipValidation = false;
            this.txtPaDate.TabIndex = 102;
            this.txtPaDate.TabStop = false;
            this.txtPaDate.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPaDate.Visible = false;
            // 
            // txtOptionMonth
            // 
            this.txtOptionMonth.AllowSpace = true;
            this.txtOptionMonth.AssociatedLookUpName = "";
            this.txtOptionMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOptionMonth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOptionMonth.ContinuationTextBox = null;
            this.txtOptionMonth.CustomEnabled = true;
            this.txtOptionMonth.DataFieldMapping = "";
            this.txtOptionMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOptionMonth.GetRecordsOnUpDownKeys = false;
            this.txtOptionMonth.IsDate = false;
            this.txtOptionMonth.Location = new System.Drawing.Point(498, 17);
            this.txtOptionMonth.MaxLength = 1;
            this.txtOptionMonth.Name = "txtOptionMonth";
            this.txtOptionMonth.NumberFormat = "###,###,##0.00";
            this.txtOptionMonth.Postfix = "";
            this.txtOptionMonth.Prefix = "";
            this.txtOptionMonth.Size = new System.Drawing.Size(34, 20);
            this.txtOptionMonth.SkipValidation = false;
            this.txtOptionMonth.TabIndex = 43;
            this.txtOptionMonth.TabStop = false;
            this.txtOptionMonth.TextType = CrplControlLibrary.TextType.String;
            this.txtOptionMonth.Visible = false;
            // 
            // dtLastPay
            // 
            this.dtLastPay.CustomEnabled = true;
            this.dtLastPay.CustomFormat = "dd/MM/yyyy";
            this.dtLastPay.DataFieldMapping = "FNI_LAST_PAY";
            this.dtLastPay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtLastPay.HasChanges = true;
            this.dtLastPay.IsRequired = true;
            this.dtLastPay.Location = new System.Drawing.Point(182, 168);
            this.dtLastPay.Name = "dtLastPay";
            this.dtLastPay.NullValue = " ";
            this.dtLastPay.Size = new System.Drawing.Size(100, 20);
            this.dtLastPay.TabIndex = 9;
            this.dtLastPay.Value = new System.DateTime(2010, 12, 14, 13, 10, 11, 0);
            this.dtLastPay.Validated += new System.EventHandler(this.dtLastPay_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(341, 147);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(116, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "Annual Reduction :";
            // 
            // txtAnuualReduction
            // 
            this.txtAnuualReduction.AllowSpace = true;
            this.txtAnuualReduction.AssociatedLookUpName = "";
            this.txtAnuualReduction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnuualReduction.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnuualReduction.ContinuationTextBox = null;
            this.txtAnuualReduction.CustomEnabled = false;
            this.txtAnuualReduction.DataFieldMapping = "FNI_ANNUAL_RED";
            this.txtAnuualReduction.Enabled = false;
            this.txtAnuualReduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnuualReduction.GetRecordsOnUpDownKeys = false;
            this.txtAnuualReduction.IsDate = false;
            this.txtAnuualReduction.Location = new System.Drawing.Point(443, 143);
            this.txtAnuualReduction.MaxLength = 7;
            this.txtAnuualReduction.Name = "txtAnuualReduction";
            this.txtAnuualReduction.NumberFormat = "##,##,##0";
            this.txtAnuualReduction.Postfix = "";
            this.txtAnuualReduction.Prefix = "";
            this.txtAnuualReduction.Size = new System.Drawing.Size(79, 20);
            this.txtAnuualReduction.SkipValidation = false;
            this.txtAnuualReduction.TabIndex = 7;
            this.txtAnuualReduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnuualReduction.TextType = CrplControlLibrary.TextType.Amount;
            this.txtAnuualReduction.Validating += new System.ComponentModel.CancelEventHandler(this.txtAnuualReduction_Validating);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(339, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(118, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "Type of Insurance :";
            // 
            // txtInsuranceType
            // 
            this.txtInsuranceType.AllowSpace = true;
            this.txtInsuranceType.AssociatedLookUpName = "";
            this.txtInsuranceType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsuranceType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInsuranceType.ContinuationTextBox = null;
            this.txtInsuranceType.CustomEnabled = true;
            this.txtInsuranceType.DataFieldMapping = "FNI_TYPE";
            this.txtInsuranceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsuranceType.GetRecordsOnUpDownKeys = false;
            this.txtInsuranceType.IsDate = false;
            this.txtInsuranceType.IsRequired = true;
            this.txtInsuranceType.Location = new System.Drawing.Point(443, 118);
            this.txtInsuranceType.MaxLength = 1;
            this.txtInsuranceType.Name = "txtInsuranceType";
            this.txtInsuranceType.NumberFormat = "###,###,#";
            this.txtInsuranceType.Postfix = "";
            this.txtInsuranceType.Prefix = "";
            this.txtInsuranceType.Size = new System.Drawing.Size(42, 20);
            this.txtInsuranceType.SkipValidation = false;
            this.txtInsuranceType.TabIndex = 4;
            this.txtInsuranceType.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtInsuranceType, "Enter [L] for Life OR [M] for Mortgage");
            this.txtInsuranceType.Validated += new System.EventHandler(this.slTextBox1_Validated);
            // 
            // dtPay
            // 
            this.dtPay.CustomEnabled = true;
            this.dtPay.CustomFormat = "dd/MM/yyyy";
            this.dtPay.DataFieldMapping = "FNI_PAY_DATE";
            this.dtPay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPay.HasChanges = true;
            this.dtPay.IsRequired = true;
            this.dtPay.Location = new System.Drawing.Point(443, 168);
            this.dtPay.Name = "dtPay";
            this.dtPay.NullValue = " ";
            this.dtPay.Size = new System.Drawing.Size(100, 20);
            this.dtPay.TabIndex = 8;
            this.dtPay.Value = new System.DateTime(2010, 12, 14, 13, 10, 11, 0);
            // 
            // LBPersonnel
            // 
            this.LBPersonnel.ActionLOVExists = "PersonalLovExists";
            this.LBPersonnel.ActionType = "PersonalLov";
            this.LBPersonnel.BackColor = System.Drawing.Color.Gainsboro;
            this.LBPersonnel.ConditionalFields = "";
            this.LBPersonnel.CustomEnabled = true;
            this.LBPersonnel.DataFieldMapping = "";
            this.LBPersonnel.DependentLovControls = "";
            this.LBPersonnel.HiddenColumns = "FNI_COMP|FNI_POLICY|FNI_TYPE|FNI_INS_AMT|FNI_ANNUAL_RED|FNI_ANNUAL_PRE|FNI_PAY_DA" +
                "TE|FNI_LAST_PAY|FNI_NOMINATION|FNI_ASSIG_BANK|PA_INS_PREMIUM|ID";
            this.LBPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("LBPersonnel.Image")));
            this.LBPersonnel.LoadDependentEntities = false;
            this.LBPersonnel.Location = new System.Drawing.Point(300, 10);
            this.LBPersonnel.LookUpTitle = null;
            this.LBPersonnel.Name = "LBPersonnel";
            this.LBPersonnel.Size = new System.Drawing.Size(26, 21);
            this.LBPersonnel.SkipValidationOnLeave = true;
            this.LBPersonnel.SPName = "CHRIS_SP_FN_INSURANCE_MANAGER";
            this.LBPersonnel.TabIndex = 101;
            this.LBPersonnel.TabStop = false;
            this.LBPersonnel.UseVisualStyleBackColor = false;
            this.LBPersonnel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnCode_MouseDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(348, 172);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Date of Payment :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(332, 255);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Monthly Deduction  :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(98, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "For the Month :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(374, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Nomination :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(80, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Assigned to Bank :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(73, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Date Last Payment :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(88, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Annual Premium :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(77, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Insurance Amount :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(115, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Policy No. :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(70, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Insurance Company :";
            // 
            // txtFirstName1
            // 
            this.txtFirstName1.AutoSize = true;
            this.txtFirstName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName1.Location = new System.Drawing.Point(115, 44);
            this.txtFirstName1.Name = "txtFirstName1";
            this.txtFirstName1.Size = new System.Drawing.Size(75, 13);
            this.txtFirstName1.TabIndex = 22;
            this.txtFirstName1.Text = "First Name :";
            // 
            // txtPersonnelNo1
            // 
            this.txtPersonnelNo1.AutoSize = true;
            this.txtPersonnelNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo1.Location = new System.Drawing.Point(100, 19);
            this.txtPersonnelNo1.Name = "txtPersonnelNo1";
            this.txtPersonnelNo1.Size = new System.Drawing.Size(91, 13);
            this.txtPersonnelNo1.TabIndex = 21;
            this.txtPersonnelNo1.Text = "Personnel No :";
            // 
            // txtPA_Premium
            // 
            this.txtPA_Premium.AllowSpace = true;
            this.txtPA_Premium.AssociatedLookUpName = "";
            this.txtPA_Premium.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPA_Premium.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPA_Premium.ContinuationTextBox = null;
            this.txtPA_Premium.CustomEnabled = true;
            this.txtPA_Premium.DataFieldMapping = "INS_PREMIUM";
            this.txtPA_Premium.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPA_Premium.GetRecordsOnUpDownKeys = false;
            this.txtPA_Premium.IsDate = false;
            this.txtPA_Premium.Location = new System.Drawing.Point(443, 251);
            this.txtPA_Premium.MaxLength = 5;
            this.txtPA_Premium.Name = "txtPA_Premium";
            this.txtPA_Premium.NumberFormat = "######0";
            this.txtPA_Premium.Postfix = "";
            this.txtPA_Premium.Prefix = "";
            this.txtPA_Premium.Size = new System.Drawing.Size(90, 20);
            this.txtPA_Premium.SkipValidation = false;
            this.txtPA_Premium.TabIndex = 13;
            this.txtPA_Premium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPA_Premium.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPA_Premium.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPA_Premium_PreviewKeyDown);
            // 
            // txtVehicleChasisNo
            // 
            this.txtVehicleChasisNo.AllowSpace = true;
            this.txtVehicleChasisNo.AssociatedLookUpName = "";
            this.txtVehicleChasisNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicleChasisNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehicleChasisNo.ContinuationTextBox = null;
            this.txtVehicleChasisNo.CustomEnabled = false;
            this.txtVehicleChasisNo.DataFieldMapping = "Month";
            this.txtVehicleChasisNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicleChasisNo.GetRecordsOnUpDownKeys = false;
            this.txtVehicleChasisNo.IsDate = false;
            this.txtVehicleChasisNo.Location = new System.Drawing.Point(182, 251);
            this.txtVehicleChasisNo.Name = "txtVehicleChasisNo";
            this.txtVehicleChasisNo.NumberFormat = "###,###,##0.00";
            this.txtVehicleChasisNo.Postfix = "";
            this.txtVehicleChasisNo.Prefix = "";
            this.txtVehicleChasisNo.Size = new System.Drawing.Size(34, 20);
            this.txtVehicleChasisNo.SkipValidation = false;
            this.txtVehicleChasisNo.TabIndex = 12;
            this.txtVehicleChasisNo.TabStop = false;
            this.txtVehicleChasisNo.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtVehicleEngineNo
            // 
            this.txtVehicleEngineNo.AllowSpace = true;
            this.txtVehicleEngineNo.AssociatedLookUpName = "";
            this.txtVehicleEngineNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicleEngineNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehicleEngineNo.ContinuationTextBox = null;
            this.txtVehicleEngineNo.CustomEnabled = true;
            this.txtVehicleEngineNo.DataFieldMapping = "FNI_NOMINATION";
            this.txtVehicleEngineNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicleEngineNo.GetRecordsOnUpDownKeys = false;
            this.txtVehicleEngineNo.IsDate = false;
            this.txtVehicleEngineNo.Location = new System.Drawing.Point(443, 192);
            this.txtVehicleEngineNo.Name = "txtVehicleEngineNo";
            this.txtVehicleEngineNo.NumberFormat = "###,###,##0.00";
            this.txtVehicleEngineNo.Postfix = "";
            this.txtVehicleEngineNo.Prefix = "";
            this.txtVehicleEngineNo.Size = new System.Drawing.Size(110, 20);
            this.txtVehicleEngineNo.SkipValidation = false;
            this.txtVehicleEngineNo.TabIndex = 10;
            this.txtVehicleEngineNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAssignedBank
            // 
            this.txtAssignedBank.AllowSpace = true;
            this.txtAssignedBank.AssociatedLookUpName = "";
            this.txtAssignedBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssignedBank.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAssignedBank.ContinuationTextBox = null;
            this.txtAssignedBank.CustomEnabled = true;
            this.txtAssignedBank.DataFieldMapping = "FNI_ASSIG_BANK";
            this.txtAssignedBank.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssignedBank.GetRecordsOnUpDownKeys = false;
            this.txtAssignedBank.IsDate = false;
            this.txtAssignedBank.IsRequired = true;
            this.txtAssignedBank.Location = new System.Drawing.Point(182, 192);
            this.txtAssignedBank.MaxLength = 1;
            this.txtAssignedBank.Name = "txtAssignedBank";
            this.txtAssignedBank.NumberFormat = "###,###,##0.00";
            this.txtAssignedBank.Postfix = "";
            this.txtAssignedBank.Prefix = "";
            this.txtAssignedBank.Size = new System.Drawing.Size(34, 20);
            this.txtAssignedBank.SkipValidation = false;
            this.txtAssignedBank.TabIndex = 11;
            this.txtAssignedBank.TextType = CrplControlLibrary.TextType.String;
            this.txtAssignedBank.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtAssignedBank_PreviewKeyDown);
            // 
            // txtInsuranceAnuualPremuim
            // 
            this.txtInsuranceAnuualPremuim.AllowSpace = true;
            this.txtInsuranceAnuualPremuim.AssociatedLookUpName = "";
            this.txtInsuranceAnuualPremuim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsuranceAnuualPremuim.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInsuranceAnuualPremuim.ContinuationTextBox = null;
            this.txtInsuranceAnuualPremuim.CustomEnabled = false;
            this.txtInsuranceAnuualPremuim.DataFieldMapping = "FNI_ANNUAL_PRE";
            this.txtInsuranceAnuualPremuim.Enabled = false;
            this.txtInsuranceAnuualPremuim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsuranceAnuualPremuim.GetRecordsOnUpDownKeys = false;
            this.txtInsuranceAnuualPremuim.IsDate = false;
            this.txtInsuranceAnuualPremuim.Location = new System.Drawing.Point(182, 143);
            this.txtInsuranceAnuualPremuim.MaxLength = 6;
            this.txtInsuranceAnuualPremuim.Name = "txtInsuranceAnuualPremuim";
            this.txtInsuranceAnuualPremuim.NumberFormat = "#,##,##0";
            this.txtInsuranceAnuualPremuim.Postfix = "";
            this.txtInsuranceAnuualPremuim.Prefix = "";
            this.txtInsuranceAnuualPremuim.Size = new System.Drawing.Size(90, 20);
            this.txtInsuranceAnuualPremuim.SkipValidation = false;
            this.txtInsuranceAnuualPremuim.TabIndex = 6;
            this.txtInsuranceAnuualPremuim.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInsuranceAnuualPremuim.TextType = CrplControlLibrary.TextType.Amount;
            this.txtInsuranceAnuualPremuim.Validated += new System.EventHandler(this.txtInsuranceAnuualPremuim_Validated);
            // 
            // txtInsuranceAmount
            // 
            this.txtInsuranceAmount.AllowSpace = true;
            this.txtInsuranceAmount.AssociatedLookUpName = "";
            this.txtInsuranceAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsuranceAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInsuranceAmount.ContinuationTextBox = null;
            this.txtInsuranceAmount.CustomEnabled = true;
            this.txtInsuranceAmount.DataFieldMapping = "FNI_INS_AMT";
            this.txtInsuranceAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsuranceAmount.GetRecordsOnUpDownKeys = false;
            this.txtInsuranceAmount.IsDate = false;
            this.txtInsuranceAmount.IsRequired = true;
            this.txtInsuranceAmount.Location = new System.Drawing.Point(182, 118);
            this.txtInsuranceAmount.MaxLength = 7;
            this.txtInsuranceAmount.Name = "txtInsuranceAmount";
            this.txtInsuranceAmount.NumberFormat = "##,##,###";
            this.txtInsuranceAmount.Postfix = "";
            this.txtInsuranceAmount.Prefix = "";
            this.txtInsuranceAmount.Size = new System.Drawing.Size(100, 20);
            this.txtInsuranceAmount.SkipValidation = false;
            this.txtInsuranceAmount.TabIndex = 5;
            this.txtInsuranceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInsuranceAmount.TextType = CrplControlLibrary.TextType.Amount;
            this.txtInsuranceAmount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtInsuranceAmount_PreviewKeyDown);
            // 
            // txtPolicyNumber
            // 
            this.txtPolicyNumber.AllowSpace = true;
            this.txtPolicyNumber.AssociatedLookUpName = "";
            this.txtPolicyNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPolicyNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPolicyNumber.ContinuationTextBox = null;
            this.txtPolicyNumber.CustomEnabled = true;
            this.txtPolicyNumber.DataFieldMapping = "FNI_POLICY";
            this.txtPolicyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPolicyNumber.GetRecordsOnUpDownKeys = false;
            this.txtPolicyNumber.IsDate = false;
            this.txtPolicyNumber.IsRequired = true;
            this.txtPolicyNumber.Location = new System.Drawing.Point(182, 91);
            this.txtPolicyNumber.MaxLength = 25;
            this.txtPolicyNumber.Name = "txtPolicyNumber";
            this.txtPolicyNumber.NumberFormat = "###,###,##0.00";
            this.txtPolicyNumber.Postfix = "";
            this.txtPolicyNumber.Prefix = "";
            this.txtPolicyNumber.Size = new System.Drawing.Size(208, 20);
            this.txtPolicyNumber.SkipValidation = false;
            this.txtPolicyNumber.TabIndex = 3;
            this.txtPolicyNumber.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtComp
            // 
            this.txtComp.AllowSpace = true;
            this.txtComp.AssociatedLookUpName = "";
            this.txtComp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComp.ContinuationTextBox = null;
            this.txtComp.CustomEnabled = true;
            this.txtComp.DataFieldMapping = "FNI_COMP";
            this.txtComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp.GetRecordsOnUpDownKeys = false;
            this.txtComp.IsDate = false;
            this.txtComp.IsRequired = true;
            this.txtComp.Location = new System.Drawing.Point(182, 65);
            this.txtComp.MaxLength = 40;
            this.txtComp.Name = "txtComp";
            this.txtComp.NumberFormat = "###,###,##0.00";
            this.txtComp.Postfix = "";
            this.txtComp.Prefix = "";
            this.txtComp.Size = new System.Drawing.Size(367, 20);
            this.txtComp.SkipValidation = false;
            this.txtComp.TabIndex = 2;
            this.txtComp.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = false;
            this.txtFirstName.DataFieldMapping = "pr_first_name";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.IsRequired = true;
            this.txtFirstName.Location = new System.Drawing.Point(182, 40);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(168, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 100;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "LBPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "FNI_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(182, 12);
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 1;
            this.txtPersonnelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Double;
            this.txtPersonnelNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNo_Validating);
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(432, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 44;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Finance_SelfInsuraneEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(634, 607);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlPersonnel);
            this.CurrentPanelBlock = "pnlPersonnel";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Finance_SelfInsuraneEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Finance_SelfInsuraneEntry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_SelfInsuraneEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlPersonnel.ResumeLayout(false);
            this.pnlPersonnel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonnel;
        private CrplControlLibrary.SLDatePicker dtPay;
        private CrplControlLibrary.LookupButton LBPersonnel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtFirstName1;
        private System.Windows.Forms.Label txtPersonnelNo1;
        private CrplControlLibrary.SLTextBox txtPA_Premium;
        private CrplControlLibrary.SLTextBox txtVehicleChasisNo;
        private CrplControlLibrary.SLTextBox txtVehicleEngineNo;
        private CrplControlLibrary.SLTextBox txtInsuranceAnuualPremuim;
        private CrplControlLibrary.SLTextBox txtInsuranceAmount;
        private CrplControlLibrary.SLTextBox txtPolicyNumber;
        private CrplControlLibrary.SLTextBox txtComp;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtInsuranceType;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtAnuualReduction;
        private CrplControlLibrary.SLDatePicker dtLastPay;
        private CrplControlLibrary.SLTextBox txtAssignedBank;
        private CrplControlLibrary.SLTextBox txtOptionMonth;
        private System.Windows.Forms.Label txtUserName;
        private CrplControlLibrary.SLTextBox txtPaDate;
        private CrplControlLibrary.SLTextBox DeleteID;

    }
}