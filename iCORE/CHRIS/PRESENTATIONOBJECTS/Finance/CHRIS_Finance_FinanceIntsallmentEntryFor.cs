using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinanceIntsallmentEntryFor : ChrisMasterDetailForm
    {

        CmnDataManager cmnDM = new CmnDataManager();
        bool isbool = false;

        # region constructor
        public CHRIS_Finance_FinanceIntsallmentEntryFor()
        {
            InitializeComponent();
        }

        public CHRIS_Finance_FinanceIntsallmentEntryFor(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlDetail);
            pnlMaster.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlMaster);

            this.txtOption.Select();
            this.txtOption.Focus();


        }
        #endregion
        #region Method
        private DateTime datecheck()
        {
            DateTime Max_date = DateTime.Now;
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPrPno.Text);
            param.Add("FN_TYPE", txtLoantype.Text);
            param.Add("FN_FIN_NO", txtFinanceNo.Text);
            rslt = cmnDM.GetData("CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment", "Max_datechk", param);
            if (rslt.isSuccessful && rslt.dstResult.Tables[0].Rows.Count > 0)
            {

                Max_date = DateTime.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());


            }
            return Max_date;

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtUser.Text = this.UserName;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");

            this.FunctionConfig.EnableF2 = false;
            this.FunctionConfig.EnableF3 = false;
            this.FunctionConfig.EnableF4 = true;
            this.FunctionConfig.F4 = Function.Delete;
            this.FunctionConfig.EnableF5 = false;

            this.FunctionConfig.EnableF7 = false;
            this.FunctionConfig.EnableF8 = false;
            this.FunctionConfig.EnableF9 = false;

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF11 = false;
            this.FunctionConfig.EnableF12 = false;


            this.FunctionConfig.EnableF1 = true;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;

            this.F1OptionText = "[S]tart Entry";
            this.FunctionConfig.OptionLetterF1 = "S";

            this.tbtDelete.Visible = false;
            this.tbtList.Visible = false;
        


            this.txtUser.Text = this.UserName;
            this.label9.Text += " " + this.UserName;
            dtExtratime.Value = null;


            DGVDetail.Columns["FN_DEBIT"].ValueType = typeof(double);
            DGVDetail.Columns["FN_CREDIT"].ValueType = typeof(double);
            DGVDetail.Columns["FN_MARKUP"].ValueType = typeof(double);
        }
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            FN_MDATE.HeaderCell.Style.Font = new Font("Microsoft sans serif",8.25f,FontStyle.Bold);
            FN_DEBIT.HeaderCell.Style.Font = new Font("Microsoft sans serif",8.25f,FontStyle.Bold);
            FN_CREDIT.HeaderCell.Style.Font = new Font("Microsoft Sans serif",8.25f,FontStyle.Bold);
            FN_MARKUP.HeaderCell.Style.Font = new Font("Microsoft sans Serif",8.25f,FontStyle.Bold);
            FN_BALANCE.HeaderCell.Style.Font = new Font("Microsoft sans Serif ",8.25f,FontStyle.Bold);
            grdFN_LOAN_BALANCE.HeaderCell.Style.Font = new Font("Microsoft sans serif",8.25f,FontStyle.Bold);
            FN_PAY_LEFT.HeaderCell.Style.Font = new Font("Microsoft sans serif" , 8.25f, FontStyle.Bold);
            grdFN_LIQ_FLAG.HeaderCell.Style.Font = new Font("Microsoft sans serif", 8.25f, FontStyle.Bold);

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "List")
            {
                if (FunctionConfig.CurrentOption == Function.None)
                { return; }
                //if (FunctionConfig.CurrentOption == Function.Add)
                {

                    base.DoToolbarActions(ctrlsCollection, actionType);
                    txtaviledamt.Select();
                    txtaviledamt.Focus();
                    return;
                }

            }

            if (actionType == "Cancel")
            {
                this.DGVDetail.CancelEdit();
                if (txtPrPno.Text == string.Empty)
                {
                    txtOption.Select();
                    txtOption.Focus();
                    return;
                }
                base.DoToolbarActions(ctrlsCollection, actionType);

                txtOption.Select();
                txtOption.Focus();
                isbool = false;
                this.txtUser.Text = this.UserName;
                this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
              
                return;
            }
            if (actionType == "Save")
            {
                if (DGVDetail.Rows.Count < 2)
                    return;
                //if (DGVDetail.CurrentRow.Cells["FN_MDATE"].EditedFormattedValue.ToString() != string.Empty)
                //{

                DataTable dt = (DataTable)DGVDetail.DataSource;
                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                        DialogResult dr = MessageBox.Show("Do you want to save changes.", "", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                        if (dr == DialogResult.No)
                        {
                            return;
                        }

                        else
                        {
                            isbool = false;
                            CallReport("AUPR", "OLD");
                            double AviledAmt, CreditRatio, Monded;
                            if (double.TryParse(txtaviledamt.Text, out AviledAmt))
                            {
                                ((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)(this.pnlMaster.CurrentBusinessEntity)).FN_AMT_AVAILED
                                = AviledAmt;    
                            }
                            else
                            {
                                ((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)(this.pnlMaster.CurrentBusinessEntity)).FN_AMT_AVAILED 
                                    = null;
                            }
                            if (double.TryParse(txtcreditratio.Text, out CreditRatio))
                            {
                                ((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)(this.pnlMaster.CurrentBusinessEntity)).FN_C_RATIO_PER
                                = CreditRatio;  
                            }
                            else
                            {
                                ((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)(this.pnlMaster.CurrentBusinessEntity)).FN_C_RATIO_PER
                                    = null;
                            }
                            if (double.TryParse(txtmonded.Text, out Monded))
                            {
                                ((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)(this.pnlMaster.CurrentBusinessEntity)).FN_MONTHLY_DED
                                = Monded;   
                            }
                            else
                            {
                                ((iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)(this.pnlMaster.CurrentBusinessEntity)).FN_MONTHLY_DED
                                    = null;
                            }
                            base.DoToolbarActions(ctrlsCollection, actionType);                            
                            CallReport("AUP0", "NEW");
                            this.Cancel();
                            return;

                        }
                    }
                }



            }
            else
            {

                base.DoToolbarActions(ctrlsCollection, actionType);
            }
        }
        protected override bool Delete()
        {
            if (this.ActiveControl is SLDataGridView)
            {
                if (DGVDetail.CurrentRow.Selected)
                 base.DoToolbarActions(this.Controls, "Delete");

            }
            return false;// base.Delete();
        }
        protected override bool Save()
        {
          this.DoToolbarActions(this.Controls, "Save");
          return false;
            // return base.Save();
        }
        protected override bool Quit()
        {
            this.Close();
            return true;
        }
        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            //txtDESNAME.Name = "txtDESNAME";
            //txtDESNAME.Text = "";      //":GLOBAL.AUDIT_PATH||FN1";
            //pnl.Controls.Add(txtDESNAME);

            //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            //txtMODE.Name = "txtMODE";
            //txtMODE.Text = "CHARACTER";
            //pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "pno";
            txtPNO.Text = txtPrPno.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "fin_No";
            txtfinNo.Text = txtFinanceNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "Audit06A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }

        #endregion
        # region Events

        // private void CHRIS_Finance_FinanceIntsallmentEntryFor_AfterLOVSelection(DataRow selectedRow, string actionType)
        //{
        ////if (actionType == "List")
        //{


        //    txtaviledamt.Select();
        //    txtaviledamt.Focus();



        //}
        // }
        private void DGVDetail_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {


            int days;
            # region case 0
            if (e.ColumnIndex == 0)
            {

                //if (DGVDetail.Rows[e.RowIndex].Cells["FN_MDATE"].EditedFormattedValue.ToString() != string.Empty || DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].EditedFormattedValue.ToString() != "")
                //{ }
                //else
                //{
                //    e.Cancel = true;
                //}

                if (DGVDetail.Rows[e.RowIndex].Cells["FN_MDATE"].IsInEditMode)
                {
                    //if (DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == string.Empty || DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == null || DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == "")
                    //{
                    DateTime dTime = new DateTime(1900, 1, 1);
                    DateTime dt_Maxdt;

                    try
                    {
                        #region comment

                        //System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                        //dtf.ShortDatePattern = "dd/MM/yyyy";
                        //dTime = DateTime.Parse((DGVDetail.Rows[e.RowIndex].Cells["FN_MDATE"].EditedFormattedValue.ToString()));

                        //dt_Maxdt = datecheck();
                        ////  dt_Maxdt = Convert.ToDateTime(dt_Maxdt_db1);


                        //TimeSpan ts = dTime.Subtract(dt_Maxdt);

                        //days = ts.Days;

                        //if (days <= 0)
                        //{
                        //    MessageBox.Show("Date should be greater than" + " " + dt_Maxdt.ToString(" dd/MM/yyyy"));
                        //    e.Cancel = true;
                        //}



                        // else
                        //  {

                        #endregion

                        if ((DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].EditedFormattedValue.ToString() != string.Empty || DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].EditedFormattedValue.ToString() != string.Empty) && (DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == string.Empty || DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == null || DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == ""))
                        {
                            e.Cancel = true;
                        }
                        //DGVDetail.EndEdit();
                        if (DGVDetail.Rows.Count > 2)
                        {
                            DataTable newdatatable = (DGVDetail.GridSource);
                            {
                                object obj = newdatatable.Compute("Max(FN_MDATE)", "");
                                DateTime dataTbMaxDt = DateTime.Parse(obj.ToString());
                                DateTime dtcurrentdate = DateTime.Parse(DGVDetail.CurrentRow.Cells["FN_MDATE"].EditedFormattedValue.ToString());

                                TimeSpan ts1 = dtcurrentdate.Subtract(dataTbMaxDt);
                                int currdays = ts1.Days;

                                if (currdays <= 0)
                                {
                                    MessageBox.Show("Date should be greater than" + " " + dataTbMaxDt.ToString(" dd/MM/yyyy"));
                                    DGVDetail.BeginEdit(false);
                                    e.Cancel = true;
                                }


                            }
                        }


                        //   }

                    }
                    catch (Exception ex)
                    {

                        if (dTime == new DateTime(1900, 1, 1))
                        {
                            //     DGVDetail.CurrentCell.ErrorText = "Incorrect date format";
                            e.Cancel = true;
                            return;
                        }
                    }


                    //}
                }
            }
            #endregion
            # region case 1
            if (e.ColumnIndex == 1)
            {
                double value = 0;
                double debit = 0;

                if ((DGVDetail.CurrentCell.IsInEditMode) && (DGVDetail.CurrentCell.EditedFormattedValue.ToString() != string.Empty))
                {
                    try
                    {
                        value = double.Parse(DGVDetail.CurrentCell.EditedFormattedValue.ToString());
                        if (value == 0)
                        {
                            DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].ReadOnly = false;
                            DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].ReadOnly = false;
                            DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].Value = "0.00";
                        }
                        else
                        {
                            DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].ReadOnly = true;
                            DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].ReadOnly = true;
                            DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].Value = "0.00";
                        }
                    }
                    catch { }

                    //if (value > 0)
                    //{
                    //    this.DGVDetail.CurrentCell = this.DGVDetail.CurrentRow.Cells[0];
                    //    return;
                    //}

                    //if (value > 0)
                    //{
                    //    //DGVDetail.CurrentCell = DGVDetail.Rows[DGVDetail.CurrentRow.Index].Cells["FN_MDATE"];
                    //DGVDetail.SkippingColumns = "";// "FN_BALANCE,Column6,FN_PAY_LEFT,grdFN_LIQ_FLAG,grdFN_LOAN_BALANCE";
                    //}

                }
                else
                {
                    DGVDetail.SkippingColumns = "";
                }

                if (double.TryParse(DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString(), out value)) ;
                {
                    if (value != 0)
                    {
                        debit = double.Parse(DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString());
                        if (DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].IsInEditMode)
                        {
                            //if (FunctionConfig.CurrentOption == Function.Add)
                            //{
                                // double debit = double.Parse( DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString() == "" ? "0" :DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString()) ;
                                DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].ReadOnly = false;
                                DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].ReadOnly = false;

                                if (DGVDetail.Rows.Count > 2)
                                {
                                    if (debit > 0)
                                    {
                                        DGVDetail.CurrentRow.Cells["FN_CREDIT"].Value = 0;
                                        DGVDetail.CurrentRow.Cells["FN_MARKUP"].Value = 0;

                                        DGVDetail.CurrentRow.Cells["FN_PAY_LEFT"].Value =
                                      (DGVDetail.Rows[e.RowIndex - 1].Cells["FN_PAY_LEFT"].Value.ToString() == "" ? "0" : DGVDetail.Rows[e.RowIndex - 1].Cells["FN_PAY_LEFT"].Value);


                                        DGVDetail.CurrentRow.Cells["FN_BALANCE"].Value = double.Parse(DGVDetail.Rows[e.RowIndex - 1].Cells["FN_BALANCE"].EditedFormattedValue.ToString() == "" ? "0" : DGVDetail.Rows[e.RowIndex - 1].Cells["FN_BALANCE"].EditedFormattedValue.ToString())
                                            + debit;

                                        DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].ReadOnly = true;
                                        DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].ReadOnly = true;

                                    }
                                    else
                                    {

                                        DGVDetail.CurrentRow.Cells["FN_CREDIT"].ReadOnly = false;
                                        DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].ReadOnly = false;

                                        //  }
                                    }
                                }
                        }
                    }
                }
            }
            #endregion
            # region case 2
            if (e.ColumnIndex == 2)
            {

                double credit;
                double debit;

                if (double.TryParse(DGVDetail.CurrentRow.Cells["FN_CREDIT"].EditedFormattedValue.ToString(), out credit)
                    // && double.TryParse(DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString(), out debit)
                    )
                {

                    credit = double.Parse(DGVDetail.CurrentRow.Cells["FN_CREDIT"].EditedFormattedValue.ToString());
                    //  debit = double.Parse(DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString());


                    if (DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].IsInEditMode)
                    {
                        //if (FunctionConfig.CurrentOption == Function.Add)
                        //{

                        if (DGVDetail.Rows.Count > 2)
                        {
                            if (credit > 0)
                            {
                                DGVDetail.CurrentRow.Cells["FN_DEBIT"].Value = 0;
                                DGVDetail.CurrentRow.Cells["FN_BALANCE"].Value = double.Parse(DGVDetail.Rows[e.RowIndex - 1].Cells["FN_BALANCE"].EditedFormattedValue.ToString() == "" ? "0" : DGVDetail.Rows[e.RowIndex - 1].Cells["FN_BALANCE"].EditedFormattedValue.ToString())
                                - credit;
                                DGVDetail.CurrentRow.Cells["FN_PAY_LEFT"].Value =
                                double.Parse(DGVDetail.Rows[e.RowIndex - 1].Cells["FN_PAY_LEFT"].Value.ToString() == "" ? "0" : DGVDetail.Rows[e.RowIndex - 1].Cells["FN_PAY_LEFT"].Value.ToString()) - 1;
                                DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].ReadOnly = true;
                                // DGVDetail.CurrentRow.RequiredColumns = true;

                            }

                            else
                                if (credit == 0)
                                {
                                    DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].ReadOnly = false;
                                    DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].ReadOnly = true;
                                    DGVDetail.Rows[e.RowIndex].Cells["FN_MARKUP"].Value = "0.00";

                                }
                        }
                            //else if (debit == 0)
                            //{
                            //    MessageBox.Show("Invalid entry ..........");

                            //    e.Cancel = true;
                            //}


                      //  }
                    }

                }
            }

            #endregion
            #region case 3

            if (e.ColumnIndex == 3)
            {
                double fnmarkup = 0;
                double fncredit = 0;


                if ((DGVDetail.CurrentRow.Cells["FN_CREDIT"].EditedFormattedValue.ToString() != string.Empty) && (DGVDetail.CurrentRow.Cells["FN_CREDIT"].IsInEditMode))
                {
                    e.Cancel = true;

                }

                //else
                //{ DGVDetail.RequiredColumns = "FN_MDATE"; }                

                if (DGVDetail.CurrentRow.Cells["FN_MARKUP"].IsInEditMode)
                {
                    if (double.TryParse(DGVDetail.CurrentRow.Cells["FN_MARKUP"].EditedFormattedValue.ToString(), out fnmarkup))
                        if (double.TryParse(DGVDetail.CurrentRow.Cells["FN_CREDIT"].EditedFormattedValue.ToString(), out fncredit))
                        {
                            if (fncredit != 0)
                            {
                                fncredit = double.Parse(DGVDetail.CurrentRow.Cells["FN_CREDIT"].EditedFormattedValue.ToString());
                            }
                        }
                    {
                        if (fnmarkup != 0)
                        {
                            if (fnmarkup > fncredit)
                            {
                                MessageBox.Show("Mark up can not be greater than Credit amount .......");
                                e.Cancel = true;

                            }
                        }
                    }
                }
            }
            #endregion

        }
        private void txtOption_TextChanged(object sender, EventArgs e)
        {
            if (txtOption.Text == "S")
            {
                txtCurrOption.Text = "Entry";


            }
        }
        private void DGVDetail_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (isbool == false)
            {
                for (int index = 0; index < DGVDetail.Rows.Count - 1; index++)
                {
                    DGVDetail.Rows[index].ReadOnly = true;
                    
                }


            }
        }
        private void txtPrPno_Validating(object sender, CancelEventArgs e)
        {
            if (txtPrPno.Text != string.Empty)
            {
                txtaviledamt.Focus();
            }
            else
            {
                txtPrPno.Select();
                txtPrPno.Focus();
                return;
            
            }


        }
        private void txtPrPno_Leave(object sender, EventArgs e)
        {
            //txtaviledamt.Select();
            //txtaviledamt.Focus();
        }
        private void DGVDetail_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            #region comment code
            //if (e.ColumnIndex == 1)
            //{
            //    if (DGVDetail.CurrentCell != null)
            //    {

            //        if (DGVDetail.CurrentCell.FormattedValue.ToString() != string.Empty)
            //        {
            //              DGVDetail.CurrentCell = DGVDetail.Rows[DGVDetail.CurrentRow.Index].Cells["FN_MDATE"];

            //        }

            //    }
            //}
            #endregion
        }
        private void DGVDetail_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
         {
            if (((DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].EditedFormattedValue.ToString() != string.Empty || DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].EditedFormattedValue.ToString() != string.Empty)) && (DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue == null || DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == ""))
            {
                MessageBox.Show("Date cannot be Leave Empty");
                DGVDetail.CurrentCell = DGVDetail.CurrentRow.Cells["FN_MDATE"];
                DGVDetail.Focus();
                e.Cancel = true;
                return;

            }
            if ((DGVDetail.CurrentRow.Cells["FN_MDATE"].EditedFormattedValue.ToString() != string.Empty) && (DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].EditedFormattedValue.ToString() == string.Empty) && (DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].EditedFormattedValue.ToString() == string.Empty))
            {
                e.Cancel = true;
                return;
            }

            else
            {
                if ((DGVDetail.Rows[e.RowIndex].Cells["FN_CREDIT"].EditedFormattedValue.ToString() != string.Empty || DGVDetail.Rows[e.RowIndex].Cells["FN_DEBIT"].EditedFormattedValue.ToString() != string.Empty) && (DGVDetail.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() != ""))
                {
                    DGVDetail.CurrentRow.ReadOnly = true;
                }
            }



            #region comment code
            double value = 0;
            //if (DGVDetail.CurrentCell.OwningColumn.Name == "grdFN_LIQ_FLAG")
            //{
            //    if ((DGVDetail.CurrentCell.IsInEditMode) && (DGVDetail.CurrentCell.FormattedValue.ToString() != string.Empty))
            //    {
            //        value = 5;

            //        if (value > 0)
            //        {
            //            DGVDetail.SkippingColumns = "FN_BALANCE,Column6,FN_PAY_LEFT,grdFN_LIQ_FLAG,grdFN_LOAN_BALANCE";
            //            DGVDetail.CurrentCell = DGVDetail.CurrentRow.Cells["FN_DEBIT"];
            //            DGVDetail.Focus();
            //            e.Cancel = true;
            //        }
            //        else
            //        {
            //            DGVDetail.SkippingColumns = "";
            //            e.Cancel = false;
            //        }
            //    }
            //  }
            #endregion comment code

        }
        private void DGVDetail_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            //this.DGVDetail.EndEdit();
            //if ((DGVDetail.CurrentRow.Cells["FN_DEBIT"].EditedFormattedValue.ToString() != string.Empty))
            //{
            //    this.DGVDetail.CurrentCell = this.DGVDetail.CurrentRow.Cells[0];
            //    return;
            //}
        }
        private void DGVDetail_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (this.DGVDetail.CurrentRow != null)
            //{
            //    if (this.DGVDetail.CurrentRow.Cells[1].Value != null && this.DGVDetail.CurrentRow.Cells[1].Value.ToString() != "")
            //    {
            //        this.DGVDetail.CurrentRow.Cells[0].Selected = true;
            //        //this.DGVDetail.CurrentCell = this.DGVDetail.CurrentRow.Cells[0];
            //    }
            //}
        }
        private void DGVDetail_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            isbool = true;
        }
        private void txtaviledamt_Validating(object sender, CancelEventArgs e)
        {
            //double value;
            //if (!(double.TryParse(txtaviledamt.Text, out value)))
            //{ 
            //     txtaviledamt.Text = "";
            //    txtaviledamt.Select();
            //    txtaviledamt.Focus();
            //    return;
            //}
            // isbool = false;
        }
        private void dtExtratime_Validating(object sender, CancelEventArgs e)
        {
            isbool = false;
        }
        private void DTstartdate_Validating(object sender, CancelEventArgs e)
        {
            isbool = false;
        }
        private void DTEndDate_Validating(object sender, CancelEventArgs e)
        {
            isbool = false;
        }
        private void txtcreditratio_Validating(object sender, CancelEventArgs e)
        {

            //double value;
            //if (!(double.TryParse(txtcreditratio.Text, out value)))
            //{
            //    txtcreditratio.Text = "";
            //    txtcreditratio.Select();
            //    txtcreditratio.Focus();
            //    return;
            //}

            //isbool = false;
        }
        private void txtmonded_Validating(object sender, CancelEventArgs e)
        {
            //double value;
            //if (!(double.TryParse(txtmonded.Text, out value)))
            //{
            //    txtmonded.Text = "";
            //    txtmonded.Select();
            //    txtmonded.Focus();
            //    return;
            //}

            //isbool = false;
        }        
        private void lbtnPersonnel_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                this.lbtnPersonnel.Enabled = false;
            }
            this.lbtnPersonnel.Enabled = true;
        }
         #endregion

        private void CHRIS_Finance_FinanceIntsallmentEntryFor_AfterLOVSelection(DataRow selectedRow, string actionType)
        {

        }
    }
}