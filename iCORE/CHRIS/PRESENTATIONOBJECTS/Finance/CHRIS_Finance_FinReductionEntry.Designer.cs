namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinReductionEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinReductionEntry));
            this.pnlFinanceReduction = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.dtPayG = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTstartdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTFN_END_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTPAY_GEN_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.W_Bal = new CrplControlLibrary.SLTextBox(this.components);
            this.dtExtratime = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtPersonnalName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCreditRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtFIN_CREDIT = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtMARKUP_REC = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtTOT_INST = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtfnBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotalMonthInstall = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.txtCanBeAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtFactor = new CrplControlLibrary.SLTextBox(this.components);
            this.label32 = new System.Windows.Forms.Label();
            this.txtAvailable = new CrplControlLibrary.SLTextBox(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.txtRatioAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtFnMonthMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFnMonthCredit = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFnMonthDebit = new CrplControlLibrary.SLTextBox(this.components);
            this.wval5 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFnPAyLeft = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnCRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.dtEnhancementDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtRepay = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVal3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtpr_transfer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtWTenCBonus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrNewAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtpr_level = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBookRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.txtENHAN_AMT = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpr_category = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbType = new CrplControlLibrary.LookupButton(this.components);
            this.txtFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbtnPersonnal = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersonnalNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtpersonal = new CrplControlLibrary.SLTextBox(this.components);
            this.fnfinNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlFinanceReduction.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlFinanceReduction
            // 
            this.pnlFinanceReduction.ConcurrentPanels = null;
            this.pnlFinanceReduction.Controls.Add(this.label23);
            this.pnlFinanceReduction.Controls.Add(this.dtPayG);
            this.pnlFinanceReduction.Controls.Add(this.DTstartdate);
            this.pnlFinanceReduction.Controls.Add(this.DTFN_END_DATE);
            this.pnlFinanceReduction.Controls.Add(this.DTPAY_GEN_DATE);
            this.pnlFinanceReduction.Controls.Add(this.W_Bal);
            this.pnlFinanceReduction.Controls.Add(this.dtExtratime);
            this.pnlFinanceReduction.Controls.Add(this.txtPersonnalName);
            this.pnlFinanceReduction.Controls.Add(this.txtCreditRatio);
            this.pnlFinanceReduction.Controls.Add(this.txtMarkup);
            this.pnlFinanceReduction.Controls.Add(this.label11);
            this.pnlFinanceReduction.Controls.Add(this.txtFIN_CREDIT);
            this.pnlFinanceReduction.Controls.Add(this.label10);
            this.pnlFinanceReduction.Controls.Add(this.txtMARKUP_REC);
            this.pnlFinanceReduction.Controls.Add(this.label9);
            this.pnlFinanceReduction.Controls.Add(this.txtTOT_INST);
            this.pnlFinanceReduction.Controls.Add(this.label7);
            this.pnlFinanceReduction.Controls.Add(this.txtfnBalance);
            this.pnlFinanceReduction.Controls.Add(this.label6);
            this.pnlFinanceReduction.Controls.Add(this.txtTotalMonthInstall);
            this.pnlFinanceReduction.Controls.Add(this.label34);
            this.pnlFinanceReduction.Controls.Add(this.txtCanBeAvail);
            this.pnlFinanceReduction.Controls.Add(this.label33);
            this.pnlFinanceReduction.Controls.Add(this.txtFactor);
            this.pnlFinanceReduction.Controls.Add(this.label32);
            this.pnlFinanceReduction.Controls.Add(this.txtAvailable);
            this.pnlFinanceReduction.Controls.Add(this.label31);
            this.pnlFinanceReduction.Controls.Add(this.txtRatioAvail);
            this.pnlFinanceReduction.Controls.Add(this.label30);
            this.pnlFinanceReduction.Controls.Add(this.label29);
            this.pnlFinanceReduction.Controls.Add(this.txtFnMonthMarkup);
            this.pnlFinanceReduction.Controls.Add(this.txtFnMonthCredit);
            this.pnlFinanceReduction.Controls.Add(this.txtFnMonthDebit);
            this.pnlFinanceReduction.Controls.Add(this.wval5);
            this.pnlFinanceReduction.Controls.Add(this.txtFnPAyLeft);
            this.pnlFinanceReduction.Controls.Add(this.txtfnMarkup);
            this.pnlFinanceReduction.Controls.Add(this.txtfnCRatio);
            this.pnlFinanceReduction.Controls.Add(this.label21);
            this.pnlFinanceReduction.Controls.Add(this.dtEnhancementDate);
            this.pnlFinanceReduction.Controls.Add(this.label5);
            this.pnlFinanceReduction.Controls.Add(this.txtRepay);
            this.pnlFinanceReduction.Controls.Add(this.txtVal3);
            this.pnlFinanceReduction.Controls.Add(this.txtpr_transfer);
            this.pnlFinanceReduction.Controls.Add(this.txtWTenCBonus);
            this.pnlFinanceReduction.Controls.Add(this.txtPrNewAnnual);
            this.pnlFinanceReduction.Controls.Add(this.txtfnBranch);
            this.pnlFinanceReduction.Controls.Add(this.txtpr_level);
            this.pnlFinanceReduction.Controls.Add(this.txtBookRatio);
            this.pnlFinanceReduction.Controls.Add(this.txtENHAN_AMT);
            this.pnlFinanceReduction.Controls.Add(this.label24);
            this.pnlFinanceReduction.Controls.Add(this.label22);
            this.pnlFinanceReduction.Controls.Add(this.label20);
            this.pnlFinanceReduction.Controls.Add(this.label19);
            this.pnlFinanceReduction.Controls.Add(this.label13);
            this.pnlFinanceReduction.Controls.Add(this.label12);
            this.pnlFinanceReduction.Controls.Add(this.txtpr_category);
            this.pnlFinanceReduction.Controls.Add(this.txtFinanceNo);
            this.pnlFinanceReduction.Controls.Add(this.label8);
            this.pnlFinanceReduction.Controls.Add(this.lbType);
            this.pnlFinanceReduction.Controls.Add(this.txtFinType);
            this.pnlFinanceReduction.Controls.Add(this.label4);
            this.pnlFinanceReduction.Controls.Add(this.label2);
            this.pnlFinanceReduction.Controls.Add(this.lbtnPersonnal);
            this.pnlFinanceReduction.Controls.Add(this.txtPersonnalNo);
            this.pnlFinanceReduction.Controls.Add(this.label1);
            this.pnlFinanceReduction.Controls.Add(this.panel2);
            this.pnlFinanceReduction.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlFinanceReduction.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlFinanceReduction.DependentPanels = null;
            this.pnlFinanceReduction.DisableDependentLoad = false;
            this.pnlFinanceReduction.EnableDelete = true;
            this.pnlFinanceReduction.EnableInsert = true;
            this.pnlFinanceReduction.EnableQuery = false;
            this.pnlFinanceReduction.EnableUpdate = true;
            this.pnlFinanceReduction.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinEnhancementFnMonthCommand";
            this.pnlFinanceReduction.Location = new System.Drawing.Point(16, 192);
            this.pnlFinanceReduction.MasterPanel = null;
            this.pnlFinanceReduction.Name = "pnlFinanceReduction";
            this.pnlFinanceReduction.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlFinanceReduction.Size = new System.Drawing.Size(640, 384);
            this.pnlFinanceReduction.SPName = "CHRIS_SP_FinReduction_FN_MONTH_MANAGER";
            this.pnlFinanceReduction.TabIndex = 44;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(57, 199);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 15);
            this.label23.TabIndex = 130;
            this.label23.Text = "Annual Pack :";
            // 
            // dtPayG
            // 
            this.dtPayG.CustomEnabled = true;
            this.dtPayG.CustomFormat = "dd/MM/yyyy";
            this.dtPayG.DataFieldMapping = "Pay_Gen";
            this.dtPayG.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPayG.HasChanges = true;
            this.dtPayG.Location = new System.Drawing.Point(446, 22);
            this.dtPayG.Name = "dtPayG";
            this.dtPayG.NullValue = " ";
            this.dtPayG.Size = new System.Drawing.Size(100, 20);
            this.dtPayG.TabIndex = 129;
            this.dtPayG.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.dtPayG.Validating += new System.ComponentModel.CancelEventHandler(this.dtPayG_Validating);
            // 
            // DTstartdate
            // 
            this.DTstartdate.CustomEnabled = false;
            this.DTstartdate.CustomFormat = "dd/MM/yyyy";
            this.DTstartdate.DataFieldMapping = "Start_Date";
            this.DTstartdate.Enabled = false;
            this.DTstartdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTstartdate.HasChanges = false;
            this.DTstartdate.Location = new System.Drawing.Point(145, 127);
            this.DTstartdate.Name = "DTstartdate";
            this.DTstartdate.NullValue = " ";
            this.DTstartdate.Size = new System.Drawing.Size(100, 20);
            this.DTstartdate.TabIndex = 128;
            this.DTstartdate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // DTFN_END_DATE
            // 
            this.DTFN_END_DATE.CustomEnabled = false;
            this.DTFN_END_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTFN_END_DATE.DataFieldMapping = "End_Date";
            this.DTFN_END_DATE.Enabled = false;
            this.DTFN_END_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTFN_END_DATE.HasChanges = false;
            this.DTFN_END_DATE.Location = new System.Drawing.Point(146, 150);
            this.DTFN_END_DATE.Name = "DTFN_END_DATE";
            this.DTFN_END_DATE.NullValue = " ";
            this.DTFN_END_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTFN_END_DATE.TabIndex = 127;
            this.DTFN_END_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // DTPAY_GEN_DATE
            // 
            this.DTPAY_GEN_DATE.CustomEnabled = false;
            this.DTPAY_GEN_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTPAY_GEN_DATE.DataFieldMapping = "Pay_Gen_Date";
            this.DTPAY_GEN_DATE.Enabled = false;
            this.DTPAY_GEN_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPAY_GEN_DATE.HasChanges = false;
            this.DTPAY_GEN_DATE.Location = new System.Drawing.Point(146, 220);
            this.DTPAY_GEN_DATE.Name = "DTPAY_GEN_DATE";
            this.DTPAY_GEN_DATE.NullValue = " ";
            this.DTPAY_GEN_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTPAY_GEN_DATE.TabIndex = 126;
            this.DTPAY_GEN_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // W_Bal
            // 
            this.W_Bal.AllowSpace = true;
            this.W_Bal.AssociatedLookUpName = "";
            this.W_Bal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_Bal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_Bal.ContinuationTextBox = null;
            this.W_Bal.CustomEnabled = true;
            this.W_Bal.DataFieldMapping = "";
            this.W_Bal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_Bal.GetRecordsOnUpDownKeys = false;
            this.W_Bal.IsDate = false;
            this.W_Bal.Location = new System.Drawing.Point(559, 18);
            this.W_Bal.MaxLength = 500;
            this.W_Bal.Name = "W_Bal";
            this.W_Bal.NumberFormat = "###,###,##0.00";
            this.W_Bal.Postfix = "";
            this.W_Bal.Prefix = "";
            this.W_Bal.ReadOnly = true;
            this.W_Bal.Size = new System.Drawing.Size(34, 20);
            this.W_Bal.SkipValidation = false;
            this.W_Bal.TabIndex = 124;
            this.W_Bal.TabStop = false;
            this.W_Bal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.W_Bal.TextType = CrplControlLibrary.TextType.Double;
            this.W_Bal.Visible = false;
            // 
            // dtExtratime
            // 
            this.dtExtratime.CustomEnabled = false;
            this.dtExtratime.CustomFormat = "dd/MM/yyyy";
            this.dtExtratime.DataFieldMapping = "ExtraTime";
            this.dtExtratime.Enabled = false;
            this.dtExtratime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExtratime.HasChanges = false;
            this.dtExtratime.Location = new System.Drawing.Point(146, 174);
            this.dtExtratime.Name = "dtExtratime";
            this.dtExtratime.NullValue = " ";
            this.dtExtratime.Size = new System.Drawing.Size(100, 20);
            this.dtExtratime.TabIndex = 123;
            this.dtExtratime.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            // 
            // txtPersonnalName
            // 
            this.txtPersonnalName.AllowSpace = true;
            this.txtPersonnalName.AssociatedLookUpName = "";
            this.txtPersonnalName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalName.ContinuationTextBox = null;
            this.txtPersonnalName.CustomEnabled = true;
            this.txtPersonnalName.DataFieldMapping = "Pr_Name";
            this.txtPersonnalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalName.IsDate = false;
            this.txtPersonnalName.Location = new System.Drawing.Point(146, 48);
            this.txtPersonnalName.MaxLength = 40;
            this.txtPersonnalName.Name = "txtPersonnalName";
            this.txtPersonnalName.NumberFormat = "###,###,##0.00";
            this.txtPersonnalName.Postfix = "";
            this.txtPersonnalName.Prefix = "";
            this.txtPersonnalName.ReadOnly = true;
            this.txtPersonnalName.Size = new System.Drawing.Size(385, 20);
            this.txtPersonnalName.SkipValidation = false;
            this.txtPersonnalName.TabIndex = 4;
            this.txtPersonnalName.TabStop = false;
            this.txtPersonnalName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCreditRatio
            // 
            this.txtCreditRatio.AllowSpace = true;
            this.txtCreditRatio.AssociatedLookUpName = "lbtnPNo";
            this.txtCreditRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreditRatio.ContinuationTextBox = null;
            this.txtCreditRatio.CustomEnabled = true;
            this.txtCreditRatio.DataFieldMapping = "";
            this.txtCreditRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditRatio.GetRecordsOnUpDownKeys = false;
            this.txtCreditRatio.IsDate = false;
            this.txtCreditRatio.Location = new System.Drawing.Point(466, 89);
            this.txtCreditRatio.MaxLength = 500;
            this.txtCreditRatio.Name = "txtCreditRatio";
            this.txtCreditRatio.NumberFormat = "########0.00";
            this.txtCreditRatio.Postfix = "";
            this.txtCreditRatio.Prefix = "";
            this.txtCreditRatio.ReadOnly = true;
            this.txtCreditRatio.Size = new System.Drawing.Size(78, 20);
            this.txtCreditRatio.SkipValidation = false;
            this.txtCreditRatio.TabIndex = 42;
            this.txtCreditRatio.TabStop = false;
            this.txtCreditRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditRatio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtMarkup
            // 
            this.txtMarkup.AllowSpace = true;
            this.txtMarkup.AssociatedLookUpName = "";
            this.txtMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkup.ContinuationTextBox = null;
            this.txtMarkup.CustomEnabled = true;
            this.txtMarkup.DataFieldMapping = "FN_MARKUP";
            this.txtMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkup.GetRecordsOnUpDownKeys = false;
            this.txtMarkup.IsDate = false;
            this.txtMarkup.Location = new System.Drawing.Point(466, 353);
            this.txtMarkup.MaxLength = 500;
            this.txtMarkup.Name = "txtMarkup";
            this.txtMarkup.NumberFormat = "########0.00";
            this.txtMarkup.Postfix = "";
            this.txtMarkup.Prefix = "";
            this.txtMarkup.ReadOnly = true;
            this.txtMarkup.Size = new System.Drawing.Size(78, 20);
            this.txtMarkup.SkipValidation = false;
            this.txtMarkup.TabIndex = 103;
            this.txtMarkup.TabStop = false;
            this.txtMarkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkup.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(272, 353);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(191, 20);
            this.label11.TabIndex = 104;
            this.label11.Text = "Markup :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFIN_CREDIT
            // 
            this.txtFIN_CREDIT.AllowSpace = true;
            this.txtFIN_CREDIT.AssociatedLookUpName = "";
            this.txtFIN_CREDIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFIN_CREDIT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFIN_CREDIT.ContinuationTextBox = null;
            this.txtFIN_CREDIT.CustomEnabled = true;
            this.txtFIN_CREDIT.DataFieldMapping = "";
            this.txtFIN_CREDIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFIN_CREDIT.GetRecordsOnUpDownKeys = false;
            this.txtFIN_CREDIT.IsDate = false;
            this.txtFIN_CREDIT.Location = new System.Drawing.Point(466, 327);
            this.txtFIN_CREDIT.MaxLength = 500;
            this.txtFIN_CREDIT.Name = "txtFIN_CREDIT";
            this.txtFIN_CREDIT.NumberFormat = "########0.00";
            this.txtFIN_CREDIT.Postfix = "";
            this.txtFIN_CREDIT.Prefix = "";
            this.txtFIN_CREDIT.ReadOnly = true;
            this.txtFIN_CREDIT.Size = new System.Drawing.Size(78, 20);
            this.txtFIN_CREDIT.SkipValidation = false;
            this.txtFIN_CREDIT.TabIndex = 101;
            this.txtFIN_CREDIT.TabStop = false;
            this.txtFIN_CREDIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFIN_CREDIT.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(272, 327);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(191, 20);
            this.label10.TabIndex = 102;
            this.label10.Text = "Installment Amount :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMARKUP_REC
            // 
            this.txtMARKUP_REC.AllowSpace = true;
            this.txtMARKUP_REC.AssociatedLookUpName = "";
            this.txtMARKUP_REC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMARKUP_REC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMARKUP_REC.ContinuationTextBox = null;
            this.txtMARKUP_REC.CustomEnabled = true;
            this.txtMARKUP_REC.DataFieldMapping = "";
            this.txtMARKUP_REC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMARKUP_REC.GetRecordsOnUpDownKeys = false;
            this.txtMARKUP_REC.IsDate = false;
            this.txtMARKUP_REC.Location = new System.Drawing.Point(466, 301);
            this.txtMARKUP_REC.MaxLength = 500;
            this.txtMARKUP_REC.Name = "txtMARKUP_REC";
            this.txtMARKUP_REC.NumberFormat = "########0.00";
            this.txtMARKUP_REC.Postfix = "";
            this.txtMARKUP_REC.Prefix = "";
            this.txtMARKUP_REC.ReadOnly = true;
            this.txtMARKUP_REC.Size = new System.Drawing.Size(78, 20);
            this.txtMARKUP_REC.SkipValidation = false;
            this.txtMARKUP_REC.TabIndex = 99;
            this.txtMARKUP_REC.TabStop = false;
            this.txtMARKUP_REC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMARKUP_REC.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(272, 301);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(191, 20);
            this.label9.TabIndex = 100;
            this.label9.Text = "Markup Received :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTOT_INST
            // 
            this.txtTOT_INST.AllowSpace = true;
            this.txtTOT_INST.AssociatedLookUpName = "";
            this.txtTOT_INST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTOT_INST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTOT_INST.ContinuationTextBox = null;
            this.txtTOT_INST.CustomEnabled = true;
            this.txtTOT_INST.DataFieldMapping = "";
            this.txtTOT_INST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTOT_INST.GetRecordsOnUpDownKeys = false;
            this.txtTOT_INST.IsDate = false;
            this.txtTOT_INST.Location = new System.Drawing.Point(466, 275);
            this.txtTOT_INST.MaxLength = 500;
            this.txtTOT_INST.Name = "txtTOT_INST";
            this.txtTOT_INST.NumberFormat = "########0.00";
            this.txtTOT_INST.Postfix = "";
            this.txtTOT_INST.Prefix = "";
            this.txtTOT_INST.ReadOnly = true;
            this.txtTOT_INST.Size = new System.Drawing.Size(78, 20);
            this.txtTOT_INST.SkipValidation = false;
            this.txtTOT_INST.TabIndex = 97;
            this.txtTOT_INST.TabStop = false;
            this.txtTOT_INST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTOT_INST.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(272, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(191, 20);
            this.label7.TabIndex = 98;
            this.label7.Text = "Installment Received :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtfnBalance
            // 
            this.txtfnBalance.AllowSpace = true;
            this.txtfnBalance.AssociatedLookUpName = "";
            this.txtfnBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnBalance.ContinuationTextBox = null;
            this.txtfnBalance.CustomEnabled = true;
            this.txtfnBalance.DataFieldMapping = "FN_BALANCE";
            this.txtfnBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnBalance.GetRecordsOnUpDownKeys = false;
            this.txtfnBalance.IsDate = false;
            this.txtfnBalance.Location = new System.Drawing.Point(466, 249);
            this.txtfnBalance.MaxLength = 500;
            this.txtfnBalance.Name = "txtfnBalance";
            this.txtfnBalance.NumberFormat = "########0.00";
            this.txtfnBalance.Postfix = "";
            this.txtfnBalance.Prefix = "";
            this.txtfnBalance.ReadOnly = true;
            this.txtfnBalance.Size = new System.Drawing.Size(78, 20);
            this.txtfnBalance.SkipValidation = false;
            this.txtfnBalance.TabIndex = 95;
            this.txtfnBalance.TabStop = false;
            this.txtfnBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnBalance.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(272, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(191, 20);
            this.label6.TabIndex = 96;
            this.label6.Text = "Balance :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalMonthInstall
            // 
            this.txtTotalMonthInstall.AllowSpace = true;
            this.txtTotalMonthInstall.AssociatedLookUpName = "";
            this.txtTotalMonthInstall.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalMonthInstall.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalMonthInstall.ContinuationTextBox = null;
            this.txtTotalMonthInstall.CustomEnabled = true;
            this.txtTotalMonthInstall.DataFieldMapping = "TOT_MONTHLY_INSTALL";
            this.txtTotalMonthInstall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMonthInstall.GetRecordsOnUpDownKeys = false;
            this.txtTotalMonthInstall.IsDate = false;
            this.txtTotalMonthInstall.Location = new System.Drawing.Point(466, 223);
            this.txtTotalMonthInstall.MaxLength = 500;
            this.txtTotalMonthInstall.Name = "txtTotalMonthInstall";
            this.txtTotalMonthInstall.NumberFormat = "########0.000";
            this.txtTotalMonthInstall.Postfix = "";
            this.txtTotalMonthInstall.Prefix = "";
            this.txtTotalMonthInstall.ReadOnly = true;
            this.txtTotalMonthInstall.Size = new System.Drawing.Size(78, 20);
            this.txtTotalMonthInstall.SkipValidation = false;
            this.txtTotalMonthInstall.TabIndex = 79;
            this.txtTotalMonthInstall.TabStop = false;
            this.txtTotalMonthInstall.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalMonthInstall.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(272, 223);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(191, 20);
            this.label34.TabIndex = 80;
            this.label34.Text = "Total Monthly Installments :";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCanBeAvail
            // 
            this.txtCanBeAvail.AllowSpace = true;
            this.txtCanBeAvail.AssociatedLookUpName = "";
            this.txtCanBeAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCanBeAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCanBeAvail.ContinuationTextBox = null;
            this.txtCanBeAvail.CustomEnabled = true;
            this.txtCanBeAvail.DataFieldMapping = "CAN_BE";
            this.txtCanBeAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCanBeAvail.GetRecordsOnUpDownKeys = false;
            this.txtCanBeAvail.IsDate = false;
            this.txtCanBeAvail.Location = new System.Drawing.Point(466, 194);
            this.txtCanBeAvail.MaxLength = 500;
            this.txtCanBeAvail.Name = "txtCanBeAvail";
            this.txtCanBeAvail.NumberFormat = "########0.00";
            this.txtCanBeAvail.Postfix = "";
            this.txtCanBeAvail.Prefix = "";
            this.txtCanBeAvail.ReadOnly = true;
            this.txtCanBeAvail.Size = new System.Drawing.Size(78, 20);
            this.txtCanBeAvail.SkipValidation = false;
            this.txtCanBeAvail.TabIndex = 77;
            this.txtCanBeAvail.TabStop = false;
            this.txtCanBeAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCanBeAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(268, 194);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(195, 20);
            this.label33.TabIndex = 78;
            this.label33.Text = "Amt Of Loan Can Be Availed :";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFactor
            // 
            this.txtFactor.AllowSpace = true;
            this.txtFactor.AssociatedLookUpName = "";
            this.txtFactor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFactor.ContinuationTextBox = null;
            this.txtFactor.CustomEnabled = true;
            this.txtFactor.DataFieldMapping = "";
            this.txtFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactor.GetRecordsOnUpDownKeys = false;
            this.txtFactor.IsDate = false;
            this.txtFactor.Location = new System.Drawing.Point(466, 167);
            this.txtFactor.MaxLength = 500;
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.NumberFormat = "########0.0000";
            this.txtFactor.Postfix = "";
            this.txtFactor.Prefix = "";
            this.txtFactor.ReadOnly = true;
            this.txtFactor.Size = new System.Drawing.Size(78, 20);
            this.txtFactor.SkipValidation = false;
            this.txtFactor.TabIndex = 75;
            this.txtFactor.TabStop = false;
            this.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFactor.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(363, 168);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 20);
            this.label32.TabIndex = 76;
            this.label32.Text = "Factor :";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAvailable
            // 
            this.txtAvailable.AllowSpace = true;
            this.txtAvailable.AssociatedLookUpName = "";
            this.txtAvailable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvailable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvailable.ContinuationTextBox = null;
            this.txtAvailable.CustomEnabled = true;
            this.txtAvailable.DataFieldMapping = "AVAILABLE";
            this.txtAvailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvailable.GetRecordsOnUpDownKeys = false;
            this.txtAvailable.IsDate = false;
            this.txtAvailable.Location = new System.Drawing.Point(466, 141);
            this.txtAvailable.MaxLength = 500;
            this.txtAvailable.Name = "txtAvailable";
            this.txtAvailable.NumberFormat = "########0.00";
            this.txtAvailable.Postfix = "";
            this.txtAvailable.Prefix = "";
            this.txtAvailable.ReadOnly = true;
            this.txtAvailable.Size = new System.Drawing.Size(78, 20);
            this.txtAvailable.SkipValidation = false;
            this.txtAvailable.TabIndex = 73;
            this.txtAvailable.TabStop = false;
            this.txtAvailable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAvailable.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(339, 141);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(124, 20);
            this.label31.TabIndex = 74;
            this.label31.Text = "Available Ratio :";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRatioAvail
            // 
            this.txtRatioAvail.AllowSpace = true;
            this.txtRatioAvail.AssociatedLookUpName = "lbtnPNo";
            this.txtRatioAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatioAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRatioAvail.ContinuationTextBox = null;
            this.txtRatioAvail.CustomEnabled = true;
            this.txtRatioAvail.DataFieldMapping = "RATIO_AVAILED";
            this.txtRatioAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatioAvail.GetRecordsOnUpDownKeys = false;
            this.txtRatioAvail.IsDate = false;
            this.txtRatioAvail.Location = new System.Drawing.Point(466, 115);
            this.txtRatioAvail.MaxLength = 500;
            this.txtRatioAvail.Name = "txtRatioAvail";
            this.txtRatioAvail.NumberFormat = "########0.00";
            this.txtRatioAvail.Postfix = "";
            this.txtRatioAvail.Prefix = "";
            this.txtRatioAvail.ReadOnly = true;
            this.txtRatioAvail.Size = new System.Drawing.Size(78, 20);
            this.txtRatioAvail.SkipValidation = false;
            this.txtRatioAvail.TabIndex = 71;
            this.txtRatioAvail.TabStop = false;
            this.txtRatioAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRatioAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(272, 115);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(191, 20);
            this.label30.TabIndex = 72;
            this.label30.Text = "Less Ratio Availed :";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(363, 89);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 20);
            this.label29.TabIndex = 43;
            this.label29.Text = "Credit Ratio :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFnMonthMarkup
            // 
            this.txtFnMonthMarkup.AllowSpace = true;
            this.txtFnMonthMarkup.AssociatedLookUpName = "";
            this.txtFnMonthMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnMonthMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnMonthMarkup.ContinuationTextBox = null;
            this.txtFnMonthMarkup.CustomEnabled = true;
            this.txtFnMonthMarkup.DataFieldMapping = "FN_MARKUP";
            this.txtFnMonthMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnMonthMarkup.GetRecordsOnUpDownKeys = false;
            this.txtFnMonthMarkup.IsDate = false;
            this.txtFnMonthMarkup.Location = new System.Drawing.Point(559, 247);
            this.txtFnMonthMarkup.MaxLength = 500;
            this.txtFnMonthMarkup.Name = "txtFnMonthMarkup";
            this.txtFnMonthMarkup.NumberFormat = "###,###,##0.00";
            this.txtFnMonthMarkup.Postfix = "";
            this.txtFnMonthMarkup.Prefix = "";
            this.txtFnMonthMarkup.ReadOnly = true;
            this.txtFnMonthMarkup.Size = new System.Drawing.Size(34, 20);
            this.txtFnMonthMarkup.SkipValidation = false;
            this.txtFnMonthMarkup.TabIndex = 113;
            this.txtFnMonthMarkup.TabStop = false;
            this.txtFnMonthMarkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFnMonthMarkup.TextType = CrplControlLibrary.TextType.Double;
            this.txtFnMonthMarkup.Visible = false;
            // 
            // txtFnMonthCredit
            // 
            this.txtFnMonthCredit.AllowSpace = true;
            this.txtFnMonthCredit.AssociatedLookUpName = "";
            this.txtFnMonthCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnMonthCredit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnMonthCredit.ContinuationTextBox = null;
            this.txtFnMonthCredit.CustomEnabled = true;
            this.txtFnMonthCredit.DataFieldMapping = "";
            this.txtFnMonthCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnMonthCredit.GetRecordsOnUpDownKeys = false;
            this.txtFnMonthCredit.IsDate = false;
            this.txtFnMonthCredit.Location = new System.Drawing.Point(559, 221);
            this.txtFnMonthCredit.MaxLength = 500;
            this.txtFnMonthCredit.Name = "txtFnMonthCredit";
            this.txtFnMonthCredit.NumberFormat = "###,###,##0.00";
            this.txtFnMonthCredit.Postfix = "";
            this.txtFnMonthCredit.Prefix = "";
            this.txtFnMonthCredit.ReadOnly = true;
            this.txtFnMonthCredit.Size = new System.Drawing.Size(34, 20);
            this.txtFnMonthCredit.SkipValidation = false;
            this.txtFnMonthCredit.TabIndex = 112;
            this.txtFnMonthCredit.TabStop = false;
            this.txtFnMonthCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFnMonthCredit.TextType = CrplControlLibrary.TextType.Double;
            this.txtFnMonthCredit.Visible = false;
            // 
            // txtFnMonthDebit
            // 
            this.txtFnMonthDebit.AllowSpace = true;
            this.txtFnMonthDebit.AssociatedLookUpName = "";
            this.txtFnMonthDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnMonthDebit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnMonthDebit.ContinuationTextBox = null;
            this.txtFnMonthDebit.CustomEnabled = true;
            this.txtFnMonthDebit.DataFieldMapping = "FN_DEBIT";
            this.txtFnMonthDebit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnMonthDebit.GetRecordsOnUpDownKeys = false;
            this.txtFnMonthDebit.IsDate = false;
            this.txtFnMonthDebit.Location = new System.Drawing.Point(559, 196);
            this.txtFnMonthDebit.MaxLength = 500;
            this.txtFnMonthDebit.Name = "txtFnMonthDebit";
            this.txtFnMonthDebit.NumberFormat = "###,###,##0.00";
            this.txtFnMonthDebit.Postfix = "";
            this.txtFnMonthDebit.Prefix = "";
            this.txtFnMonthDebit.ReadOnly = true;
            this.txtFnMonthDebit.Size = new System.Drawing.Size(34, 20);
            this.txtFnMonthDebit.SkipValidation = false;
            this.txtFnMonthDebit.TabIndex = 111;
            this.txtFnMonthDebit.TabStop = false;
            this.txtFnMonthDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFnMonthDebit.TextType = CrplControlLibrary.TextType.Double;
            this.txtFnMonthDebit.Visible = false;
            // 
            // wval5
            // 
            this.wval5.AllowSpace = true;
            this.wval5.AssociatedLookUpName = "";
            this.wval5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wval5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wval5.ContinuationTextBox = null;
            this.wval5.CustomEnabled = true;
            this.wval5.DataFieldMapping = "";
            this.wval5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wval5.GetRecordsOnUpDownKeys = false;
            this.wval5.IsDate = false;
            this.wval5.Location = new System.Drawing.Point(559, 128);
            this.wval5.MaxLength = 500;
            this.wval5.Name = "wval5";
            this.wval5.NumberFormat = "###,###,##0.00";
            this.wval5.Postfix = "";
            this.wval5.Prefix = "";
            this.wval5.ReadOnly = true;
            this.wval5.Size = new System.Drawing.Size(34, 20);
            this.wval5.SkipValidation = false;
            this.wval5.TabIndex = 110;
            this.wval5.TabStop = false;
            this.wval5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.wval5.TextType = CrplControlLibrary.TextType.Double;
            this.wval5.Visible = false;
            // 
            // txtFnPAyLeft
            // 
            this.txtFnPAyLeft.AllowSpace = true;
            this.txtFnPAyLeft.AssociatedLookUpName = "";
            this.txtFnPAyLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnPAyLeft.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnPAyLeft.ContinuationTextBox = null;
            this.txtFnPAyLeft.CustomEnabled = true;
            this.txtFnPAyLeft.DataFieldMapping = "FN_PAY_LEFT";
            this.txtFnPAyLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnPAyLeft.GetRecordsOnUpDownKeys = false;
            this.txtFnPAyLeft.IsDate = false;
            this.txtFnPAyLeft.Location = new System.Drawing.Point(559, 167);
            this.txtFnPAyLeft.MaxLength = 500;
            this.txtFnPAyLeft.Name = "txtFnPAyLeft";
            this.txtFnPAyLeft.NumberFormat = "###,###,##0.00";
            this.txtFnPAyLeft.Postfix = "";
            this.txtFnPAyLeft.Prefix = "";
            this.txtFnPAyLeft.ReadOnly = true;
            this.txtFnPAyLeft.Size = new System.Drawing.Size(34, 20);
            this.txtFnPAyLeft.SkipValidation = false;
            this.txtFnPAyLeft.TabIndex = 109;
            this.txtFnPAyLeft.TabStop = false;
            this.txtFnPAyLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFnPAyLeft.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtfnMarkup
            // 
            this.txtfnMarkup.AllowSpace = true;
            this.txtfnMarkup.AssociatedLookUpName = "";
            this.txtfnMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnMarkup.ContinuationTextBox = null;
            this.txtfnMarkup.CustomEnabled = true;
            this.txtfnMarkup.DataFieldMapping = "finMarkup";
            this.txtfnMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnMarkup.GetRecordsOnUpDownKeys = false;
            this.txtfnMarkup.IsDate = false;
            this.txtfnMarkup.Location = new System.Drawing.Point(237, 24);
            this.txtfnMarkup.MaxLength = 500;
            this.txtfnMarkup.Name = "txtfnMarkup";
            this.txtfnMarkup.NumberFormat = "###,###,##0.00";
            this.txtfnMarkup.Postfix = "";
            this.txtfnMarkup.Prefix = "";
            this.txtfnMarkup.ReadOnly = true;
            this.txtfnMarkup.Size = new System.Drawing.Size(34, 20);
            this.txtfnMarkup.SkipValidation = false;
            this.txtfnMarkup.TabIndex = 108;
            this.txtfnMarkup.TabStop = false;
            this.txtfnMarkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnMarkup.TextType = CrplControlLibrary.TextType.Double;
            this.txtfnMarkup.Visible = false;
            // 
            // txtfnCRatio
            // 
            this.txtfnCRatio.AllowSpace = true;
            this.txtfnCRatio.AssociatedLookUpName = "";
            this.txtfnCRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnCRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnCRatio.ContinuationTextBox = null;
            this.txtfnCRatio.CustomEnabled = true;
            this.txtfnCRatio.DataFieldMapping = "FN_C_RATIO";
            this.txtfnCRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnCRatio.GetRecordsOnUpDownKeys = false;
            this.txtfnCRatio.IsDate = false;
            this.txtfnCRatio.Location = new System.Drawing.Point(242, 3);
            this.txtfnCRatio.MaxLength = 500;
            this.txtfnCRatio.Name = "txtfnCRatio";
            this.txtfnCRatio.NumberFormat = "###,###,##0.00";
            this.txtfnCRatio.Postfix = "";
            this.txtfnCRatio.Prefix = "";
            this.txtfnCRatio.ReadOnly = true;
            this.txtfnCRatio.Size = new System.Drawing.Size(34, 20);
            this.txtfnCRatio.SkipValidation = false;
            this.txtfnCRatio.TabIndex = 107;
            this.txtfnCRatio.TabStop = false;
            this.txtfnCRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnCRatio.TextType = CrplControlLibrary.TextType.Double;
            this.txtfnCRatio.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(296, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(156, 15);
            this.label21.TabIndex = 105;
            this.label21.Text = "Expected Payroll Date :";
            // 
            // dtEnhancementDate
            // 
            this.dtEnhancementDate.CustomEnabled = true;
            this.dtEnhancementDate.CustomFormat = "dd/MM/yyyy";
            this.dtEnhancementDate.DataFieldMapping = "FN_MDATE";
            this.dtEnhancementDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEnhancementDate.HasChanges = true;
            this.dtEnhancementDate.IsRequired = true;
            this.dtEnhancementDate.Location = new System.Drawing.Point(146, 246);
            this.dtEnhancementDate.Name = "dtEnhancementDate";
            this.dtEnhancementDate.NullValue = " ";
            this.dtEnhancementDate.Size = new System.Drawing.Size(100, 20);
            this.dtEnhancementDate.TabIndex = 6;
            this.dtEnhancementDate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.dtEnhancementDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtEnhancementDate_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(41, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 15);
            this.label5.TabIndex = 93;
            this.label5.Text = "Reduction Date :";
            // 
            // txtRepay
            // 
            this.txtRepay.AllowSpace = true;
            this.txtRepay.AssociatedLookUpName = "";
            this.txtRepay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRepay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRepay.ContinuationTextBox = null;
            this.txtRepay.CustomEnabled = true;
            this.txtRepay.DataFieldMapping = "";
            this.txtRepay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepay.GetRecordsOnUpDownKeys = false;
            this.txtRepay.IsDate = false;
            this.txtRepay.Location = new System.Drawing.Point(605, 97);
            this.txtRepay.MaxLength = 500;
            this.txtRepay.Name = "txtRepay";
            this.txtRepay.NumberFormat = "###,###,##0.00";
            this.txtRepay.Postfix = "";
            this.txtRepay.Prefix = "";
            this.txtRepay.ReadOnly = true;
            this.txtRepay.Size = new System.Drawing.Size(34, 20);
            this.txtRepay.SkipValidation = false;
            this.txtRepay.TabIndex = 92;
            this.txtRepay.TabStop = false;
            this.txtRepay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRepay.TextType = CrplControlLibrary.TextType.Double;
            this.txtRepay.Visible = false;
            // 
            // txtVal3
            // 
            this.txtVal3.AllowSpace = true;
            this.txtVal3.AssociatedLookUpName = "";
            this.txtVal3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVal3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVal3.ContinuationTextBox = null;
            this.txtVal3.CustomEnabled = true;
            this.txtVal3.DataFieldMapping = "";
            this.txtVal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVal3.GetRecordsOnUpDownKeys = false;
            this.txtVal3.IsDate = false;
            this.txtVal3.Location = new System.Drawing.Point(559, 97);
            this.txtVal3.MaxLength = 500;
            this.txtVal3.Name = "txtVal3";
            this.txtVal3.NumberFormat = "###,###,##0.00";
            this.txtVal3.Postfix = "";
            this.txtVal3.Prefix = "";
            this.txtVal3.ReadOnly = true;
            this.txtVal3.Size = new System.Drawing.Size(34, 20);
            this.txtVal3.SkipValidation = false;
            this.txtVal3.TabIndex = 91;
            this.txtVal3.TabStop = false;
            this.txtVal3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVal3.TextType = CrplControlLibrary.TextType.Double;
            this.txtVal3.Visible = false;
            // 
            // txtpr_transfer
            // 
            this.txtpr_transfer.AllowSpace = true;
            this.txtpr_transfer.AssociatedLookUpName = "";
            this.txtpr_transfer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpr_transfer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpr_transfer.ContinuationTextBox = null;
            this.txtpr_transfer.CustomEnabled = true;
            this.txtpr_transfer.DataFieldMapping = "pr_transfer";
            this.txtpr_transfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpr_transfer.GetRecordsOnUpDownKeys = false;
            this.txtpr_transfer.IsDate = false;
            this.txtpr_transfer.Location = new System.Drawing.Point(406, 0);
            this.txtpr_transfer.MaxLength = 500;
            this.txtpr_transfer.Name = "txtpr_transfer";
            this.txtpr_transfer.NumberFormat = "###,###,##0.00";
            this.txtpr_transfer.Postfix = "";
            this.txtpr_transfer.Prefix = "";
            this.txtpr_transfer.ReadOnly = true;
            this.txtpr_transfer.Size = new System.Drawing.Size(34, 20);
            this.txtpr_transfer.SkipValidation = false;
            this.txtpr_transfer.TabIndex = 89;
            this.txtpr_transfer.TabStop = false;
            this.txtpr_transfer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtpr_transfer.TextType = CrplControlLibrary.TextType.Integer;
            this.txtpr_transfer.Visible = false;
            // 
            // txtWTenCBonus
            // 
            this.txtWTenCBonus.AllowSpace = true;
            this.txtWTenCBonus.AssociatedLookUpName = "";
            this.txtWTenCBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWTenCBonus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWTenCBonus.ContinuationTextBox = null;
            this.txtWTenCBonus.CustomEnabled = true;
            this.txtWTenCBonus.DataFieldMapping = "";
            this.txtWTenCBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWTenCBonus.GetRecordsOnUpDownKeys = false;
            this.txtWTenCBonus.IsDate = false;
            this.txtWTenCBonus.Location = new System.Drawing.Point(589, 45);
            this.txtWTenCBonus.MaxLength = 500;
            this.txtWTenCBonus.Name = "txtWTenCBonus";
            this.txtWTenCBonus.NumberFormat = "###,###,##0.00";
            this.txtWTenCBonus.Postfix = "";
            this.txtWTenCBonus.Prefix = "";
            this.txtWTenCBonus.ReadOnly = true;
            this.txtWTenCBonus.Size = new System.Drawing.Size(34, 20);
            this.txtWTenCBonus.SkipValidation = false;
            this.txtWTenCBonus.TabIndex = 88;
            this.txtWTenCBonus.TabStop = false;
            this.txtWTenCBonus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWTenCBonus.TextType = CrplControlLibrary.TextType.Double;
            this.txtWTenCBonus.Visible = false;
            // 
            // txtPrNewAnnual
            // 
            this.txtPrNewAnnual.AllowSpace = true;
            this.txtPrNewAnnual.AssociatedLookUpName = "";
            this.txtPrNewAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrNewAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrNewAnnual.ContinuationTextBox = null;
            this.txtPrNewAnnual.CustomEnabled = true;
            this.txtPrNewAnnual.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtPrNewAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrNewAnnual.GetRecordsOnUpDownKeys = false;
            this.txtPrNewAnnual.IsDate = false;
            this.txtPrNewAnnual.Location = new System.Drawing.Point(145, 197);
            this.txtPrNewAnnual.MaxLength = 500;
            this.txtPrNewAnnual.Name = "txtPrNewAnnual";
            this.txtPrNewAnnual.NumberFormat = "###,###,##0.00";
            this.txtPrNewAnnual.Postfix = "";
            this.txtPrNewAnnual.Prefix = "";
            this.txtPrNewAnnual.ReadOnly = true;
            this.txtPrNewAnnual.Size = new System.Drawing.Size(100, 20);
            this.txtPrNewAnnual.SkipValidation = false;
            this.txtPrNewAnnual.TabIndex = 86;
            this.txtPrNewAnnual.TabStop = false;
            this.txtPrNewAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrNewAnnual.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtfnBranch
            // 
            this.txtfnBranch.AllowSpace = true;
            this.txtfnBranch.AssociatedLookUpName = "";
            this.txtfnBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnBranch.ContinuationTextBox = null;
            this.txtfnBranch.CustomEnabled = true;
            this.txtfnBranch.DataFieldMapping = "FN_M_BRANCH";
            this.txtfnBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnBranch.GetRecordsOnUpDownKeys = false;
            this.txtfnBranch.IsDate = false;
            this.txtfnBranch.Location = new System.Drawing.Point(322, 3);
            this.txtfnBranch.MaxLength = 500;
            this.txtfnBranch.Name = "txtfnBranch";
            this.txtfnBranch.NumberFormat = "###,###,##0.00";
            this.txtfnBranch.Postfix = "";
            this.txtfnBranch.Prefix = "";
            this.txtfnBranch.ReadOnly = true;
            this.txtfnBranch.Size = new System.Drawing.Size(34, 20);
            this.txtfnBranch.SkipValidation = false;
            this.txtfnBranch.TabIndex = 85;
            this.txtfnBranch.TabStop = false;
            this.txtfnBranch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnBranch.TextType = CrplControlLibrary.TextType.String;
            this.txtfnBranch.Visible = false;
            // 
            // txtpr_level
            // 
            this.txtpr_level.AllowSpace = true;
            this.txtpr_level.AssociatedLookUpName = "";
            this.txtpr_level.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpr_level.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpr_level.ContinuationTextBox = null;
            this.txtpr_level.CustomEnabled = true;
            this.txtpr_level.DataFieldMapping = "pr_level";
            this.txtpr_level.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpr_level.GetRecordsOnUpDownKeys = false;
            this.txtpr_level.IsDate = false;
            this.txtpr_level.Location = new System.Drawing.Point(366, 0);
            this.txtpr_level.MaxLength = 500;
            this.txtpr_level.Name = "txtpr_level";
            this.txtpr_level.NumberFormat = "###,###,##0.00";
            this.txtpr_level.Postfix = "";
            this.txtpr_level.Prefix = "";
            this.txtpr_level.ReadOnly = true;
            this.txtpr_level.Size = new System.Drawing.Size(34, 20);
            this.txtpr_level.SkipValidation = false;
            this.txtpr_level.TabIndex = 84;
            this.txtpr_level.TabStop = false;
            this.txtpr_level.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtpr_level.TextType = CrplControlLibrary.TextType.String;
            this.txtpr_level.Visible = false;
            // 
            // txtBookRatio
            // 
            this.txtBookRatio.AllowSpace = true;
            this.txtBookRatio.AssociatedLookUpName = "";
            this.txtBookRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBookRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBookRatio.ContinuationTextBox = null;
            this.txtBookRatio.CustomEnabled = false;
            this.txtBookRatio.DataFieldMapping = "FN_C_RATIO_PER";
            this.txtBookRatio.Enabled = false;
            this.txtBookRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBookRatio.GetRecordsOnUpDownKeys = false;
            this.txtBookRatio.IsDate = false;
            this.txtBookRatio.Location = new System.Drawing.Point(146, 295);
            this.txtBookRatio.MaxLength = 9;
            this.txtBookRatio.Name = "txtBookRatio";
            this.txtBookRatio.NumberFormat = "########0.00";
            this.txtBookRatio.Postfix = "";
            this.txtBookRatio.Prefix = "";
            this.txtBookRatio.Size = new System.Drawing.Size(100, 20);
            this.txtBookRatio.SkipValidation = false;
            this.txtBookRatio.TabIndex = 9;
            this.txtBookRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBookRatio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtENHAN_AMT
            // 
            this.txtENHAN_AMT.AllowSpace = true;
            this.txtENHAN_AMT.AssociatedLookUpName = "";
            this.txtENHAN_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtENHAN_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtENHAN_AMT.ContinuationTextBox = null;
            this.txtENHAN_AMT.CustomEnabled = true;
            this.txtENHAN_AMT.DataFieldMapping = "FN_CREDIT";
            this.txtENHAN_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtENHAN_AMT.GetRecordsOnUpDownKeys = false;
            this.txtENHAN_AMT.IsDate = false;
            this.txtENHAN_AMT.IsRequired = true;
            this.txtENHAN_AMT.Location = new System.Drawing.Point(146, 270);
            this.txtENHAN_AMT.Name = "txtENHAN_AMT";
            this.txtENHAN_AMT.NumberFormat = "########0.00";
            this.txtENHAN_AMT.Postfix = "";
            this.txtENHAN_AMT.Prefix = "";
            this.txtENHAN_AMT.Size = new System.Drawing.Size(100, 20);
            this.txtENHAN_AMT.SkipValidation = false;
            this.txtENHAN_AMT.TabIndex = 8;
            this.txtENHAN_AMT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtENHAN_AMT.TextType = CrplControlLibrary.TextType.Amount;
            this.txtENHAN_AMT.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtENHAN_AMT_PreviewKeyDown);
            this.txtENHAN_AMT.Validated += new System.EventHandler(this.txtENHAN_AMT_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(48, 295);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 15);
            this.label24.TabIndex = 52;
            this.label24.Text = "Credit Ratio % :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(23, 270);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(132, 15);
            this.label22.TabIndex = 50;
            this.label22.Text = "Reduction Amount :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(58, 221);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 15);
            this.label20.TabIndex = 48;
            this.label20.Text = "Payroll Date :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(67, 176);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 15);
            this.label19.TabIndex = 47;
            this.label19.Text = "Extra Time :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(77, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 15);
            this.label13.TabIndex = 46;
            this.label13.Text = "End Date :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(72, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 15);
            this.label12.TabIndex = 45;
            this.label12.Text = "Start Date :";
            // 
            // txtpr_category
            // 
            this.txtpr_category.AllowSpace = true;
            this.txtpr_category.AssociatedLookUpName = "";
            this.txtpr_category.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpr_category.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpr_category.ContinuationTextBox = null;
            this.txtpr_category.CustomEnabled = true;
            this.txtpr_category.DataFieldMapping = "pr_category";
            this.txtpr_category.Enabled = false;
            this.txtpr_category.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpr_category.GetRecordsOnUpDownKeys = false;
            this.txtpr_category.IsDate = false;
            this.txtpr_category.Location = new System.Drawing.Point(561, 73);
            this.txtpr_category.MaxLength = 3;
            this.txtpr_category.Name = "txtpr_category";
            this.txtpr_category.NumberFormat = "###,###,##0.00";
            this.txtpr_category.Postfix = "";
            this.txtpr_category.Prefix = "";
            this.txtpr_category.Size = new System.Drawing.Size(23, 20);
            this.txtpr_category.SkipValidation = false;
            this.txtpr_category.TabIndex = 44;
            this.txtpr_category.TabStop = false;
            this.txtpr_category.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtpr_category.TextType = CrplControlLibrary.TextType.String;
            this.txtpr_category.Visible = false;
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = false;
            this.txtFinanceNo.DataFieldMapping = "FN_FIN_NO";
            this.txtFinanceNo.Enabled = false;
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.Location = new System.Drawing.Point(146, 102);
            this.txtFinanceNo.MaxLength = 10;
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(74, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 6;
            this.txtFinanceNo.TabStop = false;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(78, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 15);
            this.label8.TabIndex = 37;
            this.label8.Text = "L/Fin No. :";
            // 
            // lbType
            // 
            this.lbType.ActionLOVExists = "FinTypeLovExists";
            this.lbType.ActionType = "FinTypeLov";
            this.lbType.ConditionalFields = "txtPersonnalNo";
            this.lbType.CustomEnabled = true;
            this.lbType.DataFieldMapping = "";
            this.lbType.DependentLovControls = "";
            this.lbType.HiddenColumns = "";
            this.lbType.Image = ((System.Drawing.Image)(resources.GetObject("lbType.Image")));
            this.lbType.LoadDependentEntities = false;
            this.lbType.Location = new System.Drawing.Point(205, 72);
            this.lbType.LookUpTitle = null;
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(26, 21);
            this.lbType.SkipValidationOnLeave = false;
            this.lbType.SPName = "CHRIS_SP_FinReduction_FN_MONTH_MANAGER";
            this.lbType.TabIndex = 31;
            this.lbType.TabStop = false;
            this.lbType.Tag = "";
            this.lbType.UseVisualStyleBackColor = true;
            // 
            // txtFinType
            // 
            this.txtFinType.AllowSpace = true;
            this.txtFinType.AssociatedLookUpName = "lbType";
            this.txtFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinType.ContinuationTextBox = null;
            this.txtFinType.CustomEnabled = true;
            this.txtFinType.DataFieldMapping = "FN_TYPE";
            this.txtFinType.Enabled = false;
            this.txtFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinType.GetRecordsOnUpDownKeys = false;
            this.txtFinType.IsDate = false;
            this.txtFinType.Location = new System.Drawing.Point(146, 74);
            this.txtFinType.MaxLength = 6;
            this.txtFinType.Name = "txtFinType";
            this.txtFinType.NumberFormat = "###,###,##0.00";
            this.txtFinType.Postfix = "";
            this.txtFinType.Prefix = "";
            this.txtFinType.Size = new System.Drawing.Size(52, 20);
            this.txtFinType.SkipValidation = false;
            this.txtFinType.TabIndex = 5;
            this.txtFinType.TextType = CrplControlLibrary.TextType.String;
            this.txtFinType.Validated += new System.EventHandler(this.txtFinType_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(70, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 27;
            this.label4.Text = "L/Fin Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(66, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 26;
            this.label2.Text = "First Name :";
            // 
            // lbtnPersonnal
            // 
            this.lbtnPersonnal.ActionLOVExists = "PersonalLovExists";
            this.lbtnPersonnal.ActionType = "PersonalLov";
            this.lbtnPersonnal.ConditionalFields = "";
            this.lbtnPersonnal.CustomEnabled = true;
            this.lbtnPersonnal.DataFieldMapping = "";
            this.lbtnPersonnal.DependentLovControls = "";
            this.lbtnPersonnal.HiddenColumns = "";
            this.lbtnPersonnal.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnal.Image")));
            this.lbtnPersonnal.LoadDependentEntities = false;
            this.lbtnPersonnal.Location = new System.Drawing.Point(205, 20);
            this.lbtnPersonnal.LookUpTitle = null;
            this.lbtnPersonnal.Name = "lbtnPersonnal";
            this.lbtnPersonnal.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnal.SkipValidationOnLeave = false;
            this.lbtnPersonnal.SPName = "CHRIS_SP_FinReduction_FN_MONTH_MANAGER";
            this.lbtnPersonnal.TabIndex = 24;
            this.lbtnPersonnal.TabStop = false;
            this.lbtnPersonnal.Tag = "";
            this.lbtnPersonnal.UseVisualStyleBackColor = true;
            // 
            // txtPersonnalNo
            // 
            this.txtPersonnalNo.AllowSpace = true;
            this.txtPersonnalNo.AssociatedLookUpName = "lbtnPersonnal";
            this.txtPersonnalNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalNo.ContinuationTextBox = null;
            this.txtPersonnalNo.CustomEnabled = true;
            this.txtPersonnalNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnalNo.Enabled = false;
            this.txtPersonnalNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalNo.IsDate = false;
            this.txtPersonnalNo.IsRequired = true;
            this.txtPersonnalNo.Location = new System.Drawing.Point(146, 21);
            this.txtPersonnalNo.MaxLength = 4;
            this.txtPersonnalNo.Name = "txtPersonnalNo";
            this.txtPersonnalNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnalNo.Postfix = "";
            this.txtPersonnalNo.Prefix = "";
            this.txtPersonnalNo.Size = new System.Drawing.Size(52, 20);
            this.txtPersonnalNo.SkipValidation = false;
            this.txtPersonnalNo.TabIndex = 3;
            this.txtPersonnalNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnalNo.TextType = CrplControlLibrary.TextType.Double;
            this.txtPersonnalNo.Validated += new System.EventHandler(this.txtPersonnalNo_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(45, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Personnel No. :";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label35);
            this.panel2.Location = new System.Drawing.Point(263, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(336, 310);
            this.panel2.TabIndex = 114;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(282, 100);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 13);
            this.label35.TabIndex = 41;
            this.label35.Text = "/";
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtpersonal);
            this.pnlHead.Controls.Add(this.fnfinNo);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(17, 90);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(639, 97);
            this.pnlHead.TabIndex = 43;
            // 
            // txtpersonal
            // 
            this.txtpersonal.AllowSpace = true;
            this.txtpersonal.AssociatedLookUpName = "";
            this.txtpersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpersonal.ContinuationTextBox = null;
            this.txtpersonal.CustomEnabled = true;
            this.txtpersonal.DataFieldMapping = "";
            this.txtpersonal.Enabled = false;
            this.txtpersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpersonal.GetRecordsOnUpDownKeys = false;
            this.txtpersonal.IsDate = false;
            this.txtpersonal.Location = new System.Drawing.Point(178, 28);
            this.txtpersonal.MaxLength = 10;
            this.txtpersonal.Name = "txtpersonal";
            this.txtpersonal.NumberFormat = "###,###,##0.00";
            this.txtpersonal.Postfix = "";
            this.txtpersonal.Prefix = "";
            this.txtpersonal.Size = new System.Drawing.Size(52, 20);
            this.txtpersonal.SkipValidation = false;
            this.txtpersonal.TabIndex = 44;
            this.txtpersonal.TabStop = false;
            this.txtpersonal.TextType = CrplControlLibrary.TextType.String;
            this.txtpersonal.Visible = false;
            // 
            // fnfinNo
            // 
            this.fnfinNo.AllowSpace = true;
            this.fnfinNo.AssociatedLookUpName = "";
            this.fnfinNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fnfinNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.fnfinNo.ContinuationTextBox = null;
            this.fnfinNo.CustomEnabled = true;
            this.fnfinNo.DataFieldMapping = "";
            this.fnfinNo.Enabled = false;
            this.fnfinNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnfinNo.GetRecordsOnUpDownKeys = false;
            this.fnfinNo.IsDate = false;
            this.fnfinNo.Location = new System.Drawing.Point(178, 7);
            this.fnfinNo.MaxLength = 10;
            this.fnfinNo.Name = "fnfinNo";
            this.fnfinNo.NumberFormat = "###,###,##0.00";
            this.fnfinNo.Postfix = "";
            this.fnfinNo.Prefix = "";
            this.fnfinNo.Size = new System.Drawing.Size(52, 20);
            this.fnfinNo.SkipValidation = false;
            this.fnfinNo.TabIndex = 43;
            this.fnfinNo.TabStop = false;
            this.fnfinNo.TextType = CrplControlLibrary.TextType.String;
            this.fnfinNo.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(192, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "REDUCTION OF FINANCE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(190, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "FINANCE SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(535, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(535, 35);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(488, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(480, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(446, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 119;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Finance_FinReductionEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlFinanceReduction);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlFinanceReduction";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Finance_FinReductionEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Finance_FinReductionEntry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_FinReductionEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlFinanceReduction, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlFinanceReduction.ResumeLayout(false);
            this.pnlFinanceReduction.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlFinanceReduction;
        private CrplControlLibrary.SLTextBox txtFnMonthMarkup;
        private CrplControlLibrary.SLTextBox txtFnMonthCredit;
        private CrplControlLibrary.SLTextBox txtFnMonthDebit;
        private CrplControlLibrary.SLTextBox wval5;
        private CrplControlLibrary.SLTextBox txtFnPAyLeft;
        private CrplControlLibrary.SLTextBox txtfnMarkup;
        private CrplControlLibrary.SLTextBox txtfnCRatio;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtMarkup;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtFIN_CREDIT;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtMARKUP_REC;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtTOT_INST;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtfnBalance;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLDatePicker dtEnhancementDate;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtRepay;
        private CrplControlLibrary.SLTextBox txtVal3;
        private CrplControlLibrary.SLTextBox txtpr_transfer;
        private CrplControlLibrary.SLTextBox txtWTenCBonus;
        private CrplControlLibrary.SLTextBox txtPrNewAnnual;
        private CrplControlLibrary.SLTextBox txtfnBranch;
        private CrplControlLibrary.SLTextBox txtpr_level;
        private CrplControlLibrary.SLTextBox txtTotalMonthInstall;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txtCanBeAvail;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtFactor;
        private System.Windows.Forms.Label label32;
        private CrplControlLibrary.SLTextBox txtAvailable;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLTextBox txtRatioAvail;
        private System.Windows.Forms.Label label30;
        private CrplControlLibrary.SLTextBox txtCreditRatio;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txtBookRatio;
        private CrplControlLibrary.SLTextBox txtENHAN_AMT;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtpr_category;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbType;
        private CrplControlLibrary.SLTextBox txtFinType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtPersonnalName;
        private CrplControlLibrary.LookupButton lbtnPersonnal;
        private CrplControlLibrary.SLTextBox txtPersonnalNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Panel panel2;
        private CrplControlLibrary.SLDatePicker dtExtratime;
        private CrplControlLibrary.SLTextBox txtpersonal;
        private CrplControlLibrary.SLTextBox fnfinNo;
        private CrplControlLibrary.SLTextBox W_Bal;
        private CrplControlLibrary.SLDatePicker DTPAY_GEN_DATE;
        private CrplControlLibrary.SLDatePicker DTFN_END_DATE;
        private CrplControlLibrary.SLDatePicker DTstartdate;
        private CrplControlLibrary.SLDatePicker dtPayG;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label23;
    }
}