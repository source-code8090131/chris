namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinanceBookingEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinanceBookingEntry));
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtpersonal = new CrplControlLibrary.SLTextBox(this.components);
            this.fnfinNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.GCount = new CrplControlLibrary.SLTextBox(this.components);
            this.FinNoType = new CrplControlLibrary.SLTextBox(this.components);
            this.GYear = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlFinanceBooking = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtWBasic = new CrplControlLibrary.SLTextBox(this.components);
            this.txtprLoanPack = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRepay = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotalMonthInstall = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.txtCanBeAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label33 = new System.Windows.Forms.Label();
            this.txtFactor = new CrplControlLibrary.SLTextBox(this.components);
            this.label32 = new System.Windows.Forms.Label();
            this.txtAvailable = new CrplControlLibrary.SLTextBox(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.txtRatioAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.txtCreditRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.dtprdBirth = new System.Windows.Forms.DateTimePicker();
            this.txtVal3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnMonthly = new CrplControlLibrary.SLTextBox(this.components);
            this.txtWTenCBonus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrNewAnnual = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfnPaySchedule = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtprCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnCity = new CrplControlLibrary.LookupButton(this.components);
            this.txtFN_MORTG_PROP_VALVR = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFN_MORTG_PROP_VALSA = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFN_MORTG_PROP_CITY = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFN_MORTG_PROP_ADD = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBookRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBookMarkUp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInstallment = new CrplControlLibrary.SLTextBox(this.components);
            this.txtaviledamt = new CrplControlLibrary.SLTextBox(this.components);
            this.DTPAY_GEN_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtExtratime = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTFN_END_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTstartdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSchedule = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbSubType = new CrplControlLibrary.LookupButton(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtSubDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinanceSubType = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.lbType = new CrplControlLibrary.LookupButton(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtFinDescription = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPersonnalName = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersonnal = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersonnalNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlFinanceBooking.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtpersonal);
            this.pnlHead.Controls.Add(this.fnfinNo);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(10, 93);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(648, 65);
            this.pnlHead.TabIndex = 40;
            // 
            // txtpersonal
            // 
            this.txtpersonal.AllowSpace = true;
            this.txtpersonal.AssociatedLookUpName = "";
            this.txtpersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpersonal.ContinuationTextBox = null;
            this.txtpersonal.CustomEnabled = true;
            this.txtpersonal.DataFieldMapping = "";
            this.txtpersonal.Enabled = false;
            this.txtpersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpersonal.GetRecordsOnUpDownKeys = false;
            this.txtpersonal.IsDate = false;
            this.txtpersonal.Location = new System.Drawing.Point(178, 27);
            this.txtpersonal.MaxLength = 10;
            this.txtpersonal.Name = "txtpersonal";
            this.txtpersonal.NumberFormat = "###,###,##0.00";
            this.txtpersonal.Postfix = "";
            this.txtpersonal.Prefix = "";
            this.txtpersonal.Size = new System.Drawing.Size(52, 20);
            this.txtpersonal.SkipValidation = false;
            this.txtpersonal.TabIndex = 40;
            this.txtpersonal.TabStop = false;
            this.txtpersonal.TextType = CrplControlLibrary.TextType.String;
            this.txtpersonal.Visible = false;
            // 
            // fnfinNo
            // 
            this.fnfinNo.AllowSpace = true;
            this.fnfinNo.AssociatedLookUpName = "";
            this.fnfinNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fnfinNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.fnfinNo.ContinuationTextBox = null;
            this.fnfinNo.CustomEnabled = true;
            this.fnfinNo.DataFieldMapping = "";
            this.fnfinNo.Enabled = false;
            this.fnfinNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnfinNo.GetRecordsOnUpDownKeys = false;
            this.fnfinNo.IsDate = false;
            this.fnfinNo.Location = new System.Drawing.Point(178, 6);
            this.fnfinNo.MaxLength = 10;
            this.fnfinNo.Name = "fnfinNo";
            this.fnfinNo.NumberFormat = "###,###,##0.00";
            this.fnfinNo.Postfix = "";
            this.fnfinNo.Prefix = "";
            this.fnfinNo.Size = new System.Drawing.Size(52, 20);
            this.fnfinNo.SkipValidation = false;
            this.fnfinNo.TabIndex = 39;
            this.fnfinNo.TabStop = false;
            this.fnfinNo.TextType = CrplControlLibrary.TextType.String;
            this.fnfinNo.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(216, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "FINANCE BOOKING";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(213, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "FINANCE SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(552, 35);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(552, 9);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(505, 37);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(497, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 35);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 9);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 35);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GCount
            // 
            this.GCount.AllowSpace = true;
            this.GCount.AssociatedLookUpName = "";
            this.GCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GCount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.GCount.ContinuationTextBox = null;
            this.GCount.CustomEnabled = true;
            this.GCount.DataFieldMapping = "FinNo_Count";
            this.GCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GCount.GetRecordsOnUpDownKeys = false;
            this.GCount.IsDate = false;
            this.GCount.Location = new System.Drawing.Point(52, 154);
            this.GCount.MaxLength = 500;
            this.GCount.Name = "GCount";
            this.GCount.NumberFormat = "###,###,##0.00";
            this.GCount.Postfix = "";
            this.GCount.Prefix = "";
            this.GCount.ReadOnly = true;
            this.GCount.Size = new System.Drawing.Size(34, 20);
            this.GCount.SkipValidation = false;
            this.GCount.TabIndex = 122;
            this.GCount.TabStop = false;
            this.GCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.GCount.TextType = CrplControlLibrary.TextType.String;
            this.GCount.Visible = false;
            // 
            // FinNoType
            // 
            this.FinNoType.AllowSpace = true;
            this.FinNoType.AssociatedLookUpName = "";
            this.FinNoType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FinNoType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FinNoType.ContinuationTextBox = null;
            this.FinNoType.CustomEnabled = true;
            this.FinNoType.DataFieldMapping = "FinNo_Type";
            this.FinNoType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FinNoType.GetRecordsOnUpDownKeys = false;
            this.FinNoType.IsDate = false;
            this.FinNoType.Location = new System.Drawing.Point(23, 178);
            this.FinNoType.MaxLength = 500;
            this.FinNoType.Name = "FinNoType";
            this.FinNoType.NumberFormat = "###,###,##0.00";
            this.FinNoType.Postfix = "";
            this.FinNoType.Prefix = "";
            this.FinNoType.ReadOnly = true;
            this.FinNoType.Size = new System.Drawing.Size(34, 20);
            this.FinNoType.SkipValidation = false;
            this.FinNoType.TabIndex = 121;
            this.FinNoType.TabStop = false;
            this.FinNoType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.FinNoType.TextType = CrplControlLibrary.TextType.String;
            this.FinNoType.Visible = false;
            // 
            // GYear
            // 
            this.GYear.AllowSpace = true;
            this.GYear.AssociatedLookUpName = "";
            this.GYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.GYear.ContinuationTextBox = null;
            this.GYear.CustomEnabled = true;
            this.GYear.DataFieldMapping = "FinNo_Year";
            this.GYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GYear.GetRecordsOnUpDownKeys = false;
            this.GYear.IsDate = false;
            this.GYear.Location = new System.Drawing.Point(13, 154);
            this.GYear.MaxLength = 500;
            this.GYear.Name = "GYear";
            this.GYear.NumberFormat = "###,###,##0.00";
            this.GYear.Postfix = "";
            this.GYear.Prefix = "";
            this.GYear.ReadOnly = true;
            this.GYear.Size = new System.Drawing.Size(34, 20);
            this.GYear.SkipValidation = false;
            this.GYear.TabIndex = 120;
            this.GYear.TabStop = false;
            this.GYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.GYear.TextType = CrplControlLibrary.TextType.String;
            this.GYear.Visible = false;
            // 
            // pnlFinanceBooking
            // 
            this.pnlFinanceBooking.ConcurrentPanels = null;
            this.pnlFinanceBooking.Controls.Add(this.GCount);
            this.pnlFinanceBooking.Controls.Add(this.FinNoType);
            this.pnlFinanceBooking.Controls.Add(this.txtWBasic);
            this.pnlFinanceBooking.Controls.Add(this.txtprLoanPack);
            this.pnlFinanceBooking.Controls.Add(this.GYear);
            this.pnlFinanceBooking.Controls.Add(this.txtRepay);
            this.pnlFinanceBooking.Controls.Add(this.txtTotalMonthInstall);
            this.pnlFinanceBooking.Controls.Add(this.label34);
            this.pnlFinanceBooking.Controls.Add(this.txtCanBeAvail);
            this.pnlFinanceBooking.Controls.Add(this.label33);
            this.pnlFinanceBooking.Controls.Add(this.txtFactor);
            this.pnlFinanceBooking.Controls.Add(this.label32);
            this.pnlFinanceBooking.Controls.Add(this.txtAvailable);
            this.pnlFinanceBooking.Controls.Add(this.label31);
            this.pnlFinanceBooking.Controls.Add(this.txtRatioAvail);
            this.pnlFinanceBooking.Controls.Add(this.label30);
            this.pnlFinanceBooking.Controls.Add(this.txtCreditRatio);
            this.pnlFinanceBooking.Controls.Add(this.label29);
            this.pnlFinanceBooking.Controls.Add(this.panel2);
            this.pnlFinanceBooking.Controls.Add(this.dtprdBirth);
            this.pnlFinanceBooking.Controls.Add(this.txtVal3);
            this.pnlFinanceBooking.Controls.Add(this.txtfnMonthly);
            this.pnlFinanceBooking.Controls.Add(this.txtWTenCBonus);
            this.pnlFinanceBooking.Controls.Add(this.txtPrNewAnnual);
            this.pnlFinanceBooking.Controls.Add(this.txtfnBranch);
            this.pnlFinanceBooking.Controls.Add(this.txtfnPaySchedule);
            this.pnlFinanceBooking.Controls.Add(this.txtPrLevel);
            this.pnlFinanceBooking.Controls.Add(this.txtprCategory);
            this.pnlFinanceBooking.Controls.Add(this.lbtnCity);
            this.pnlFinanceBooking.Controls.Add(this.txtFN_MORTG_PROP_VALVR);
            this.pnlFinanceBooking.Controls.Add(this.txtFN_MORTG_PROP_VALSA);
            this.pnlFinanceBooking.Controls.Add(this.txtFN_MORTG_PROP_CITY);
            this.pnlFinanceBooking.Controls.Add(this.txtFN_MORTG_PROP_ADD);
            this.pnlFinanceBooking.Controls.Add(this.txtBookRatio);
            this.pnlFinanceBooking.Controls.Add(this.txtBookMarkUp);
            this.pnlFinanceBooking.Controls.Add(this.txtInstallment);
            this.pnlFinanceBooking.Controls.Add(this.txtaviledamt);
            this.pnlFinanceBooking.Controls.Add(this.DTPAY_GEN_DATE);
            this.pnlFinanceBooking.Controls.Add(this.dtExtratime);
            this.pnlFinanceBooking.Controls.Add(this.DTFN_END_DATE);
            this.pnlFinanceBooking.Controls.Add(this.DTstartdate);
            this.pnlFinanceBooking.Controls.Add(this.label28);
            this.pnlFinanceBooking.Controls.Add(this.label27);
            this.pnlFinanceBooking.Controls.Add(this.label26);
            this.pnlFinanceBooking.Controls.Add(this.label25);
            this.pnlFinanceBooking.Controls.Add(this.label24);
            this.pnlFinanceBooking.Controls.Add(this.label23);
            this.pnlFinanceBooking.Controls.Add(this.label22);
            this.pnlFinanceBooking.Controls.Add(this.label21);
            this.pnlFinanceBooking.Controls.Add(this.label20);
            this.pnlFinanceBooking.Controls.Add(this.label19);
            this.pnlFinanceBooking.Controls.Add(this.label13);
            this.pnlFinanceBooking.Controls.Add(this.label12);
            this.pnlFinanceBooking.Controls.Add(this.txtSchedule);
            this.pnlFinanceBooking.Controls.Add(this.label11);
            this.pnlFinanceBooking.Controls.Add(this.txtRatio);
            this.pnlFinanceBooking.Controls.Add(this.label10);
            this.pnlFinanceBooking.Controls.Add(this.txtMarkup);
            this.pnlFinanceBooking.Controls.Add(this.label9);
            this.pnlFinanceBooking.Controls.Add(this.txtFinanceNo);
            this.pnlFinanceBooking.Controls.Add(this.label8);
            this.pnlFinanceBooking.Controls.Add(this.lbSubType);
            this.pnlFinanceBooking.Controls.Add(this.label6);
            this.pnlFinanceBooking.Controls.Add(this.txtSubDesc);
            this.pnlFinanceBooking.Controls.Add(this.txtFinanceSubType);
            this.pnlFinanceBooking.Controls.Add(this.label7);
            this.pnlFinanceBooking.Controls.Add(this.lbType);
            this.pnlFinanceBooking.Controls.Add(this.label5);
            this.pnlFinanceBooking.Controls.Add(this.txtFinDescription);
            this.pnlFinanceBooking.Controls.Add(this.txtFinType);
            this.pnlFinanceBooking.Controls.Add(this.label4);
            this.pnlFinanceBooking.Controls.Add(this.label2);
            this.pnlFinanceBooking.Controls.Add(this.txtPersonnalName);
            this.pnlFinanceBooking.Controls.Add(this.lbtnPersonnal);
            this.pnlFinanceBooking.Controls.Add(this.txtPersonnalNo);
            this.pnlFinanceBooking.Controls.Add(this.label1);
            this.pnlFinanceBooking.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlFinanceBooking.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlFinanceBooking.DependentPanels = null;
            this.pnlFinanceBooking.DisableDependentLoad = false;
            this.pnlFinanceBooking.EnableDelete = true;
            this.pnlFinanceBooking.EnableInsert = true;
            this.pnlFinanceBooking.EnableQuery = false;
            this.pnlFinanceBooking.EnableUpdate = true;
            this.pnlFinanceBooking.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceBookingCommand";
            this.pnlFinanceBooking.Location = new System.Drawing.Point(10, 164);
            this.pnlFinanceBooking.MasterPanel = null;
            this.pnlFinanceBooking.Name = "pnlFinanceBooking";
            this.pnlFinanceBooking.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlFinanceBooking.Size = new System.Drawing.Size(648, 427);
            this.pnlFinanceBooking.SPName = "CHRIS_SP_Finance_fin_Booking_MANAGER";
            this.pnlFinanceBooking.TabIndex = 41;
            // 
            // txtWBasic
            // 
            this.txtWBasic.AllowSpace = true;
            this.txtWBasic.AssociatedLookUpName = "";
            this.txtWBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWBasic.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWBasic.ContinuationTextBox = null;
            this.txtWBasic.CustomEnabled = true;
            this.txtWBasic.DataFieldMapping = "";
            this.txtWBasic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWBasic.GetRecordsOnUpDownKeys = false;
            this.txtWBasic.IsDate = false;
            this.txtWBasic.Location = new System.Drawing.Point(551, 16);
            this.txtWBasic.MaxLength = 500;
            this.txtWBasic.Name = "txtWBasic";
            this.txtWBasic.NumberFormat = "###,###,##0.00";
            this.txtWBasic.Postfix = "";
            this.txtWBasic.Prefix = "";
            this.txtWBasic.ReadOnly = true;
            this.txtWBasic.Size = new System.Drawing.Size(34, 20);
            this.txtWBasic.SkipValidation = false;
            this.txtWBasic.TabIndex = 119;
            this.txtWBasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWBasic.TextType = CrplControlLibrary.TextType.Double;
            this.txtWBasic.Visible = false;
            // 
            // txtprLoanPack
            // 
            this.txtprLoanPack.AllowSpace = true;
            this.txtprLoanPack.AssociatedLookUpName = "";
            this.txtprLoanPack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprLoanPack.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtprLoanPack.ContinuationTextBox = null;
            this.txtprLoanPack.CustomEnabled = true;
            this.txtprLoanPack.DataFieldMapping = "";
            this.txtprLoanPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprLoanPack.GetRecordsOnUpDownKeys = false;
            this.txtprLoanPack.IsDate = false;
            this.txtprLoanPack.Location = new System.Drawing.Point(513, 16);
            this.txtprLoanPack.MaxLength = 500;
            this.txtprLoanPack.Name = "txtprLoanPack";
            this.txtprLoanPack.NumberFormat = "###,###,##0.00";
            this.txtprLoanPack.Postfix = "";
            this.txtprLoanPack.Prefix = "";
            this.txtprLoanPack.ReadOnly = true;
            this.txtprLoanPack.Size = new System.Drawing.Size(34, 20);
            this.txtprLoanPack.SkipValidation = false;
            this.txtprLoanPack.TabIndex = 118;
            this.txtprLoanPack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtprLoanPack.TextType = CrplControlLibrary.TextType.String;
            this.txtprLoanPack.Visible = false;
            // 
            // txtRepay
            // 
            this.txtRepay.AllowSpace = true;
            this.txtRepay.AssociatedLookUpName = "";
            this.txtRepay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRepay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRepay.ContinuationTextBox = null;
            this.txtRepay.CustomEnabled = true;
            this.txtRepay.DataFieldMapping = "";
            this.txtRepay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepay.GetRecordsOnUpDownKeys = false;
            this.txtRepay.IsDate = false;
            this.txtRepay.Location = new System.Drawing.Point(605, 233);
            this.txtRepay.MaxLength = 500;
            this.txtRepay.Name = "txtRepay";
            this.txtRepay.NumberFormat = "###,###,##0.00";
            this.txtRepay.Postfix = "";
            this.txtRepay.Prefix = "";
            this.txtRepay.ReadOnly = true;
            this.txtRepay.Size = new System.Drawing.Size(34, 20);
            this.txtRepay.SkipValidation = false;
            this.txtRepay.TabIndex = 92;
            this.txtRepay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRepay.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtTotalMonthInstall
            // 
            this.txtTotalMonthInstall.AllowSpace = true;
            this.txtTotalMonthInstall.AssociatedLookUpName = "";
            this.txtTotalMonthInstall.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalMonthInstall.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalMonthInstall.ContinuationTextBox = null;
            this.txtTotalMonthInstall.CustomEnabled = true;
            this.txtTotalMonthInstall.DataFieldMapping = "TOT_MONTHLY_INSTALL";
            this.txtTotalMonthInstall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMonthInstall.GetRecordsOnUpDownKeys = false;
            this.txtTotalMonthInstall.IsDate = false;
            this.txtTotalMonthInstall.Location = new System.Drawing.Point(513, 289);
            this.txtTotalMonthInstall.MaxLength = 500;
            this.txtTotalMonthInstall.Name = "txtTotalMonthInstall";
            this.txtTotalMonthInstall.NumberFormat = "########0.00";
            this.txtTotalMonthInstall.Postfix = "";
            this.txtTotalMonthInstall.Prefix = "";
            this.txtTotalMonthInstall.ReadOnly = true;
            this.txtTotalMonthInstall.Size = new System.Drawing.Size(78, 20);
            this.txtTotalMonthInstall.SkipValidation = false;
            this.txtTotalMonthInstall.TabIndex = 79;
            this.txtTotalMonthInstall.TabStop = false;
            this.txtTotalMonthInstall.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalMonthInstall.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(338, 289);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(169, 20);
            this.label34.TabIndex = 80;
            this.label34.Text = "Total Monthly Install:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCanBeAvail
            // 
            this.txtCanBeAvail.AllowSpace = true;
            this.txtCanBeAvail.AssociatedLookUpName = "";
            this.txtCanBeAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCanBeAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCanBeAvail.ContinuationTextBox = null;
            this.txtCanBeAvail.CustomEnabled = true;
            this.txtCanBeAvail.DataFieldMapping = "CAN_BE";
            this.txtCanBeAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCanBeAvail.GetRecordsOnUpDownKeys = false;
            this.txtCanBeAvail.IsDate = false;
            this.txtCanBeAvail.Location = new System.Drawing.Point(513, 260);
            this.txtCanBeAvail.MaxLength = 500;
            this.txtCanBeAvail.Name = "txtCanBeAvail";
            this.txtCanBeAvail.NumberFormat = "########0.00";
            this.txtCanBeAvail.Postfix = "";
            this.txtCanBeAvail.Prefix = "";
            this.txtCanBeAvail.ReadOnly = true;
            this.txtCanBeAvail.Size = new System.Drawing.Size(78, 20);
            this.txtCanBeAvail.SkipValidation = false;
            this.txtCanBeAvail.TabIndex = 77;
            this.txtCanBeAvail.TabStop = false;
            this.txtCanBeAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCanBeAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(335, 260);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(172, 20);
            this.label33.TabIndex = 78;
            this.label33.Text = "Amt Of Loan Can  Avail:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFactor
            // 
            this.txtFactor.AllowSpace = true;
            this.txtFactor.AssociatedLookUpName = "";
            this.txtFactor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFactor.ContinuationTextBox = null;
            this.txtFactor.CustomEnabled = true;
            this.txtFactor.DataFieldMapping = "FACTOR";
            this.txtFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactor.GetRecordsOnUpDownKeys = false;
            this.txtFactor.IsDate = false;
            this.txtFactor.Location = new System.Drawing.Point(513, 233);
            this.txtFactor.MaxLength = 500;
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.NumberFormat = "###,###,##0.000000";
            this.txtFactor.Postfix = "";
            this.txtFactor.Prefix = "";
            this.txtFactor.ReadOnly = true;
            this.txtFactor.Size = new System.Drawing.Size(78, 20);
            this.txtFactor.SkipValidation = false;
            this.txtFactor.TabIndex = 75;
            this.txtFactor.TabStop = false;
            this.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFactor.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(378, 234);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(129, 20);
            this.label32.TabIndex = 76;
            this.label32.Text = "Factor/ Repayment:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAvailable
            // 
            this.txtAvailable.AllowSpace = true;
            this.txtAvailable.AssociatedLookUpName = "";
            this.txtAvailable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvailable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvailable.ContinuationTextBox = null;
            this.txtAvailable.CustomEnabled = true;
            this.txtAvailable.DataFieldMapping = "AVAILABLE";
            this.txtAvailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvailable.GetRecordsOnUpDownKeys = false;
            this.txtAvailable.IsDate = false;
            this.txtAvailable.Location = new System.Drawing.Point(513, 207);
            this.txtAvailable.MaxLength = 500;
            this.txtAvailable.Name = "txtAvailable";
            this.txtAvailable.NumberFormat = "########0.00";
            this.txtAvailable.Postfix = "";
            this.txtAvailable.Prefix = "";
            this.txtAvailable.ReadOnly = true;
            this.txtAvailable.Size = new System.Drawing.Size(78, 20);
            this.txtAvailable.SkipValidation = false;
            this.txtAvailable.TabIndex = 73;
            this.txtAvailable.TabStop = false;
            this.txtAvailable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAvailable.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(407, 207);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 20);
            this.label31.TabIndex = 74;
            this.label31.Text = "Available Ratio:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRatioAvail
            // 
            this.txtRatioAvail.AllowSpace = true;
            this.txtRatioAvail.AssociatedLookUpName = "lbtnPNo";
            this.txtRatioAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatioAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRatioAvail.ContinuationTextBox = null;
            this.txtRatioAvail.CustomEnabled = true;
            this.txtRatioAvail.DataFieldMapping = "RATIO_AVAILED";
            this.txtRatioAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatioAvail.GetRecordsOnUpDownKeys = false;
            this.txtRatioAvail.IsDate = false;
            this.txtRatioAvail.Location = new System.Drawing.Point(513, 181);
            this.txtRatioAvail.MaxLength = 500;
            this.txtRatioAvail.Name = "txtRatioAvail";
            this.txtRatioAvail.NumberFormat = "########0.00";
            this.txtRatioAvail.Postfix = "";
            this.txtRatioAvail.Prefix = "";
            this.txtRatioAvail.ReadOnly = true;
            this.txtRatioAvail.Size = new System.Drawing.Size(78, 20);
            this.txtRatioAvail.SkipValidation = false;
            this.txtRatioAvail.TabIndex = 71;
            this.txtRatioAvail.TabStop = false;
            this.txtRatioAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRatioAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(332, 181);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(175, 20);
            this.label30.TabIndex = 72;
            this.label30.Text = "Less Ratio Availed:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCreditRatio
            // 
            this.txtCreditRatio.AllowSpace = true;
            this.txtCreditRatio.AssociatedLookUpName = "lbtnPNo";
            this.txtCreditRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreditRatio.ContinuationTextBox = null;
            this.txtCreditRatio.CustomEnabled = true;
            this.txtCreditRatio.DataFieldMapping = "CREDIT_RATIO";
            this.txtCreditRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditRatio.GetRecordsOnUpDownKeys = false;
            this.txtCreditRatio.IsDate = false;
            this.txtCreditRatio.Location = new System.Drawing.Point(513, 155);
            this.txtCreditRatio.MaxLength = 500;
            this.txtCreditRatio.Name = "txtCreditRatio";
            this.txtCreditRatio.NumberFormat = "########0.00";
            this.txtCreditRatio.Postfix = "";
            this.txtCreditRatio.Prefix = "";
            this.txtCreditRatio.ReadOnly = true;
            this.txtCreditRatio.Size = new System.Drawing.Size(78, 20);
            this.txtCreditRatio.SkipValidation = false;
            this.txtCreditRatio.TabIndex = 42;
            this.txtCreditRatio.TabStop = false;
            this.txtCreditRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditRatio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(407, 155);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 20);
            this.label29.TabIndex = 43;
            this.label29.Text = "Credit Ratio:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label35);
            this.panel2.Location = new System.Drawing.Point(325, 151);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(318, 165);
            this.panel2.TabIndex = 117;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(264, 84);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 13);
            this.label35.TabIndex = 40;
            this.label35.Text = "/";
            // 
            // dtprdBirth
            // 
            this.dtprdBirth.CustomFormat = "dd/MM/yyyy";
            this.dtprdBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtprdBirth.Location = new System.Drawing.Point(358, 16);
            this.dtprdBirth.Name = "dtprdBirth";
            this.dtprdBirth.Size = new System.Drawing.Size(100, 20);
            this.dtprdBirth.TabIndex = 116;
            this.dtprdBirth.Visible = false;
            // 
            // txtVal3
            // 
            this.txtVal3.AllowSpace = true;
            this.txtVal3.AssociatedLookUpName = "";
            this.txtVal3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVal3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVal3.ContinuationTextBox = null;
            this.txtVal3.CustomEnabled = true;
            this.txtVal3.DataFieldMapping = "";
            this.txtVal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVal3.GetRecordsOnUpDownKeys = false;
            this.txtVal3.IsDate = false;
            this.txtVal3.Location = new System.Drawing.Point(405, 377);
            this.txtVal3.MaxLength = 500;
            this.txtVal3.Name = "txtVal3";
            this.txtVal3.NumberFormat = "###,###,##0.00";
            this.txtVal3.Postfix = "";
            this.txtVal3.Prefix = "";
            this.txtVal3.ReadOnly = true;
            this.txtVal3.Size = new System.Drawing.Size(34, 20);
            this.txtVal3.SkipValidation = false;
            this.txtVal3.TabIndex = 91;
            this.txtVal3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVal3.TextType = CrplControlLibrary.TextType.Double;
            this.txtVal3.Visible = false;
            // 
            // txtfnMonthly
            // 
            this.txtfnMonthly.AllowSpace = true;
            this.txtfnMonthly.AssociatedLookUpName = "";
            this.txtfnMonthly.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnMonthly.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnMonthly.ContinuationTextBox = null;
            this.txtfnMonthly.CustomEnabled = true;
            this.txtfnMonthly.DataFieldMapping = "FN_MONTHLY_DED";
            this.txtfnMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnMonthly.GetRecordsOnUpDownKeys = false;
            this.txtfnMonthly.IsDate = false;
            this.txtfnMonthly.Location = new System.Drawing.Point(381, 355);
            this.txtfnMonthly.MaxLength = 500;
            this.txtfnMonthly.Name = "txtfnMonthly";
            this.txtfnMonthly.NumberFormat = "###,###,##0.00";
            this.txtfnMonthly.Postfix = "";
            this.txtfnMonthly.Prefix = "";
            this.txtfnMonthly.ReadOnly = true;
            this.txtfnMonthly.Size = new System.Drawing.Size(98, 20);
            this.txtfnMonthly.SkipValidation = false;
            this.txtfnMonthly.TabIndex = 89;
            this.txtfnMonthly.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnMonthly.TextType = CrplControlLibrary.TextType.Double;
            this.txtfnMonthly.Visible = false;
            // 
            // txtWTenCBonus
            // 
            this.txtWTenCBonus.AllowSpace = true;
            this.txtWTenCBonus.AssociatedLookUpName = "";
            this.txtWTenCBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWTenCBonus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtWTenCBonus.ContinuationTextBox = null;
            this.txtWTenCBonus.CustomEnabled = true;
            this.txtWTenCBonus.DataFieldMapping = "";
            this.txtWTenCBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWTenCBonus.GetRecordsOnUpDownKeys = false;
            this.txtWTenCBonus.IsDate = false;
            this.txtWTenCBonus.Location = new System.Drawing.Point(455, 396);
            this.txtWTenCBonus.MaxLength = 500;
            this.txtWTenCBonus.Name = "txtWTenCBonus";
            this.txtWTenCBonus.NumberFormat = "###,###,##0.00";
            this.txtWTenCBonus.Postfix = "";
            this.txtWTenCBonus.Prefix = "";
            this.txtWTenCBonus.ReadOnly = true;
            this.txtWTenCBonus.Size = new System.Drawing.Size(34, 20);
            this.txtWTenCBonus.SkipValidation = false;
            this.txtWTenCBonus.TabIndex = 88;
            this.txtWTenCBonus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWTenCBonus.TextType = CrplControlLibrary.TextType.Double;
            this.txtWTenCBonus.Visible = false;
            // 
            // txtPrNewAnnual
            // 
            this.txtPrNewAnnual.AllowSpace = true;
            this.txtPrNewAnnual.AssociatedLookUpName = "";
            this.txtPrNewAnnual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrNewAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrNewAnnual.ContinuationTextBox = null;
            this.txtPrNewAnnual.CustomEnabled = true;
            this.txtPrNewAnnual.DataFieldMapping = "";
            this.txtPrNewAnnual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrNewAnnual.GetRecordsOnUpDownKeys = false;
            this.txtPrNewAnnual.IsDate = false;
            this.txtPrNewAnnual.Location = new System.Drawing.Point(320, 18);
            this.txtPrNewAnnual.MaxLength = 500;
            this.txtPrNewAnnual.Name = "txtPrNewAnnual";
            this.txtPrNewAnnual.NumberFormat = "###,###,##0.00";
            this.txtPrNewAnnual.Postfix = "";
            this.txtPrNewAnnual.Prefix = "";
            this.txtPrNewAnnual.ReadOnly = true;
            this.txtPrNewAnnual.Size = new System.Drawing.Size(34, 20);
            this.txtPrNewAnnual.SkipValidation = false;
            this.txtPrNewAnnual.TabIndex = 86;
            this.txtPrNewAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrNewAnnual.TextType = CrplControlLibrary.TextType.Double;
            this.txtPrNewAnnual.Visible = false;
            // 
            // txtfnBranch
            // 
            this.txtfnBranch.AllowSpace = true;
            this.txtfnBranch.AssociatedLookUpName = "";
            this.txtfnBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnBranch.ContinuationTextBox = null;
            this.txtfnBranch.CustomEnabled = true;
            this.txtfnBranch.DataFieldMapping = "FN_BRANCH";
            this.txtfnBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnBranch.GetRecordsOnUpDownKeys = false;
            this.txtfnBranch.IsDate = false;
            this.txtfnBranch.Location = new System.Drawing.Point(473, 16);
            this.txtfnBranch.MaxLength = 500;
            this.txtfnBranch.Name = "txtfnBranch";
            this.txtfnBranch.NumberFormat = "###,###,##0.00";
            this.txtfnBranch.Postfix = "";
            this.txtfnBranch.Prefix = "";
            this.txtfnBranch.ReadOnly = true;
            this.txtfnBranch.Size = new System.Drawing.Size(34, 20);
            this.txtfnBranch.SkipValidation = false;
            this.txtfnBranch.TabIndex = 85;
            this.txtfnBranch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnBranch.TextType = CrplControlLibrary.TextType.String;
            this.txtfnBranch.Visible = false;
            // 
            // txtfnPaySchedule
            // 
            this.txtfnPaySchedule.AllowSpace = true;
            this.txtfnPaySchedule.AssociatedLookUpName = "";
            this.txtfnPaySchedule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnPaySchedule.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnPaySchedule.ContinuationTextBox = null;
            this.txtfnPaySchedule.CustomEnabled = true;
            this.txtfnPaySchedule.DataFieldMapping = "FN_PAY_SCHED";
            this.txtfnPaySchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnPaySchedule.GetRecordsOnUpDownKeys = false;
            this.txtfnPaySchedule.IsDate = false;
            this.txtfnPaySchedule.Location = new System.Drawing.Point(485, 370);
            this.txtfnPaySchedule.MaxLength = 500;
            this.txtfnPaySchedule.Name = "txtfnPaySchedule";
            this.txtfnPaySchedule.NumberFormat = "###,###,##0.00";
            this.txtfnPaySchedule.Postfix = "";
            this.txtfnPaySchedule.Prefix = "";
            this.txtfnPaySchedule.ReadOnly = true;
            this.txtfnPaySchedule.Size = new System.Drawing.Size(34, 20);
            this.txtfnPaySchedule.SkipValidation = false;
            this.txtfnPaySchedule.TabIndex = 84;
            this.txtfnPaySchedule.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnPaySchedule.TextType = CrplControlLibrary.TextType.Double;
            this.txtfnPaySchedule.Visible = false;
            // 
            // txtPrLevel
            // 
            this.txtPrLevel.AllowSpace = true;
            this.txtPrLevel.AssociatedLookUpName = "";
            this.txtPrLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrLevel.ContinuationTextBox = null;
            this.txtPrLevel.CustomEnabled = true;
            this.txtPrLevel.DataFieldMapping = "";
            this.txtPrLevel.Enabled = false;
            this.txtPrLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrLevel.GetRecordsOnUpDownKeys = false;
            this.txtPrLevel.IsDate = false;
            this.txtPrLevel.Location = new System.Drawing.Point(296, 18);
            this.txtPrLevel.MaxLength = 3;
            this.txtPrLevel.Name = "txtPrLevel";
            this.txtPrLevel.NumberFormat = "###,###,##0.00";
            this.txtPrLevel.Postfix = "";
            this.txtPrLevel.Prefix = "";
            this.txtPrLevel.Size = new System.Drawing.Size(21, 20);
            this.txtPrLevel.SkipValidation = false;
            this.txtPrLevel.TabIndex = 82;
            this.txtPrLevel.TextType = CrplControlLibrary.TextType.String;
            this.txtPrLevel.Visible = false;
            // 
            // txtprCategory
            // 
            this.txtprCategory.AllowSpace = true;
            this.txtprCategory.AssociatedLookUpName = "";
            this.txtprCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtprCategory.ContinuationTextBox = null;
            this.txtprCategory.CustomEnabled = true;
            this.txtprCategory.DataFieldMapping = "";
            this.txtprCategory.Enabled = false;
            this.txtprCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprCategory.GetRecordsOnUpDownKeys = false;
            this.txtprCategory.IsDate = false;
            this.txtprCategory.Location = new System.Drawing.Point(265, 18);
            this.txtprCategory.MaxLength = 3;
            this.txtprCategory.Name = "txtprCategory";
            this.txtprCategory.NumberFormat = "###,###,##0.00";
            this.txtprCategory.Postfix = "";
            this.txtprCategory.Prefix = "";
            this.txtprCategory.Size = new System.Drawing.Size(25, 20);
            this.txtprCategory.SkipValidation = false;
            this.txtprCategory.TabIndex = 81;
            this.txtprCategory.TextType = CrplControlLibrary.TextType.String;
            this.txtprCategory.Visible = false;
            // 
            // lbtnCity
            // 
            this.lbtnCity.ActionLOVExists = "FinBookingCityLovExists";
            this.lbtnCity.ActionType = "FinBookingCityLov";
            this.lbtnCity.ConditionalFields = "";
            this.lbtnCity.CustomEnabled = true;
            this.lbtnCity.DataFieldMapping = "";
            this.lbtnCity.DependentLovControls = "";
            this.lbtnCity.HiddenColumns = "";
            this.lbtnCity.Image = ((System.Drawing.Image)(resources.GetObject("lbtnCity.Image")));
            this.lbtnCity.LoadDependentEntities = false;
            this.lbtnCity.Location = new System.Drawing.Point(325, 355);
            this.lbtnCity.LookUpTitle = null;
            this.lbtnCity.Name = "lbtnCity";
            this.lbtnCity.Size = new System.Drawing.Size(26, 21);
            this.lbtnCity.SkipValidationOnLeave = false;
            this.lbtnCity.SPName = "CHRIS_SP_Finance_fin_Booking_MANAGER";
            this.lbtnCity.TabIndex = 69;
            this.lbtnCity.TabStop = false;
            this.lbtnCity.Tag = "";
            this.lbtnCity.UseVisualStyleBackColor = true;
            // 
            // txtFN_MORTG_PROP_VALVR
            // 
            this.txtFN_MORTG_PROP_VALVR.AllowSpace = true;
            this.txtFN_MORTG_PROP_VALVR.AssociatedLookUpName = "";
            this.txtFN_MORTG_PROP_VALVR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_MORTG_PROP_VALVR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_MORTG_PROP_VALVR.ContinuationTextBox = null;
            this.txtFN_MORTG_PROP_VALVR.CustomEnabled = true;
            this.txtFN_MORTG_PROP_VALVR.DataFieldMapping = "FN_MORTG_PROP_VALVR";
            this.txtFN_MORTG_PROP_VALVR.Enabled = false;
            this.txtFN_MORTG_PROP_VALVR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_MORTG_PROP_VALVR.GetRecordsOnUpDownKeys = false;
            this.txtFN_MORTG_PROP_VALVR.IsDate = false;
            this.txtFN_MORTG_PROP_VALVR.Location = new System.Drawing.Point(178, 400);
            this.txtFN_MORTG_PROP_VALVR.MaxLength = 15;
            this.txtFN_MORTG_PROP_VALVR.Name = "txtFN_MORTG_PROP_VALVR";
            this.txtFN_MORTG_PROP_VALVR.NumberFormat = "###,###,##0.00";
            this.txtFN_MORTG_PROP_VALVR.Postfix = "";
            this.txtFN_MORTG_PROP_VALVR.Prefix = "";
            this.txtFN_MORTG_PROP_VALVR.Size = new System.Drawing.Size(139, 20);
            this.txtFN_MORTG_PROP_VALVR.SkipValidation = false;
            this.txtFN_MORTG_PROP_VALVR.TabIndex = 68;
            this.txtFN_MORTG_PROP_VALVR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFN_MORTG_PROP_VALVR.TextType = CrplControlLibrary.TextType.Double;
            this.txtFN_MORTG_PROP_VALVR.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtFN_MORTG_PROP_VALVR_PreviewKeyDown);
            this.txtFN_MORTG_PROP_VALVR.Validated += new System.EventHandler(this.txtFN_MORTG_PROP_VALVR_Validated);
            // 
            // txtFN_MORTG_PROP_VALSA
            // 
            this.txtFN_MORTG_PROP_VALSA.AllowSpace = true;
            this.txtFN_MORTG_PROP_VALSA.AssociatedLookUpName = "";
            this.txtFN_MORTG_PROP_VALSA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_MORTG_PROP_VALSA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_MORTG_PROP_VALSA.ContinuationTextBox = null;
            this.txtFN_MORTG_PROP_VALSA.CustomEnabled = true;
            this.txtFN_MORTG_PROP_VALSA.DataFieldMapping = "FN_MORTG_PROP_VALSA";
            this.txtFN_MORTG_PROP_VALSA.Enabled = false;
            this.txtFN_MORTG_PROP_VALSA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_MORTG_PROP_VALSA.GetRecordsOnUpDownKeys = false;
            this.txtFN_MORTG_PROP_VALSA.IsDate = false;
            this.txtFN_MORTG_PROP_VALSA.Location = new System.Drawing.Point(178, 378);
            this.txtFN_MORTG_PROP_VALSA.MaxLength = 15;
            this.txtFN_MORTG_PROP_VALSA.Name = "txtFN_MORTG_PROP_VALSA";
            this.txtFN_MORTG_PROP_VALSA.NumberFormat = "###,###,##0.00";
            this.txtFN_MORTG_PROP_VALSA.Postfix = "";
            this.txtFN_MORTG_PROP_VALSA.Prefix = "";
            this.txtFN_MORTG_PROP_VALSA.Size = new System.Drawing.Size(139, 20);
            this.txtFN_MORTG_PROP_VALSA.SkipValidation = false;
            this.txtFN_MORTG_PROP_VALSA.TabIndex = 67;
            this.txtFN_MORTG_PROP_VALSA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFN_MORTG_PROP_VALSA.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtFN_MORTG_PROP_CITY
            // 
            this.txtFN_MORTG_PROP_CITY.AllowSpace = true;
            this.txtFN_MORTG_PROP_CITY.AssociatedLookUpName = "lbtnCity";
            this.txtFN_MORTG_PROP_CITY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_MORTG_PROP_CITY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_MORTG_PROP_CITY.ContinuationTextBox = null;
            this.txtFN_MORTG_PROP_CITY.CustomEnabled = true;
            this.txtFN_MORTG_PROP_CITY.DataFieldMapping = "FN_MORTG_PROP_CITY";
            this.txtFN_MORTG_PROP_CITY.Enabled = false;
            this.txtFN_MORTG_PROP_CITY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_MORTG_PROP_CITY.GetRecordsOnUpDownKeys = false;
            this.txtFN_MORTG_PROP_CITY.IsDate = false;
            this.txtFN_MORTG_PROP_CITY.Location = new System.Drawing.Point(178, 355);
            this.txtFN_MORTG_PROP_CITY.MaxLength = 20;
            this.txtFN_MORTG_PROP_CITY.Name = "txtFN_MORTG_PROP_CITY";
            this.txtFN_MORTG_PROP_CITY.NumberFormat = "###,###,##0.00";
            this.txtFN_MORTG_PROP_CITY.Postfix = "";
            this.txtFN_MORTG_PROP_CITY.Prefix = "";
            this.txtFN_MORTG_PROP_CITY.Size = new System.Drawing.Size(139, 20);
            this.txtFN_MORTG_PROP_CITY.SkipValidation = false;
            this.txtFN_MORTG_PROP_CITY.TabIndex = 66;
            this.txtFN_MORTG_PROP_CITY.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFN_MORTG_PROP_ADD
            // 
            this.txtFN_MORTG_PROP_ADD.AllowSpace = true;
            this.txtFN_MORTG_PROP_ADD.AssociatedLookUpName = "";
            this.txtFN_MORTG_PROP_ADD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_MORTG_PROP_ADD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_MORTG_PROP_ADD.ContinuationTextBox = null;
            this.txtFN_MORTG_PROP_ADD.CustomEnabled = true;
            this.txtFN_MORTG_PROP_ADD.DataFieldMapping = "FN_MORTG_PROP_ADD";
            this.txtFN_MORTG_PROP_ADD.Enabled = false;
            this.txtFN_MORTG_PROP_ADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_MORTG_PROP_ADD.GetRecordsOnUpDownKeys = false;
            this.txtFN_MORTG_PROP_ADD.IsDate = false;
            this.txtFN_MORTG_PROP_ADD.Location = new System.Drawing.Point(178, 332);
            this.txtFN_MORTG_PROP_ADD.MaxLength = 50;
            this.txtFN_MORTG_PROP_ADD.Name = "txtFN_MORTG_PROP_ADD";
            this.txtFN_MORTG_PROP_ADD.NumberFormat = "###,###,##0.00";
            this.txtFN_MORTG_PROP_ADD.Postfix = "";
            this.txtFN_MORTG_PROP_ADD.Prefix = "";
            this.txtFN_MORTG_PROP_ADD.Size = new System.Drawing.Size(385, 20);
            this.txtFN_MORTG_PROP_ADD.SkipValidation = false;
            this.txtFN_MORTG_PROP_ADD.TabIndex = 65;
            this.txtFN_MORTG_PROP_ADD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBookRatio
            // 
            this.txtBookRatio.AllowSpace = true;
            this.txtBookRatio.AssociatedLookUpName = "";
            this.txtBookRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBookRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBookRatio.ContinuationTextBox = null;
            this.txtBookRatio.CustomEnabled = false;
            this.txtBookRatio.DataFieldMapping = "FN_C_RATIO_PER";
            this.txtBookRatio.Enabled = false;
            this.txtBookRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBookRatio.GetRecordsOnUpDownKeys = false;
            this.txtBookRatio.IsDate = false;
            this.txtBookRatio.Location = new System.Drawing.Point(178, 309);
            this.txtBookRatio.MaxLength = 9;
            this.txtBookRatio.Name = "txtBookRatio";
            this.txtBookRatio.NumberFormat = "########0.00";
            this.txtBookRatio.Postfix = "";
            this.txtBookRatio.Prefix = "";
            this.txtBookRatio.Size = new System.Drawing.Size(100, 20);
            this.txtBookRatio.SkipValidation = false;
            this.txtBookRatio.TabIndex = 64;
            this.txtBookRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBookRatio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtBookMarkUp
            // 
            this.txtBookMarkUp.AllowSpace = true;
            this.txtBookMarkUp.AssociatedLookUpName = "";
            this.txtBookMarkUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBookMarkUp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBookMarkUp.ContinuationTextBox = null;
            this.txtBookMarkUp.CustomEnabled = false;
            this.txtBookMarkUp.DataFieldMapping = "";
            this.txtBookMarkUp.Enabled = false;
            this.txtBookMarkUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBookMarkUp.GetRecordsOnUpDownKeys = false;
            this.txtBookMarkUp.IsDate = false;
            this.txtBookMarkUp.Location = new System.Drawing.Point(178, 287);
            this.txtBookMarkUp.Name = "txtBookMarkUp";
            this.txtBookMarkUp.NumberFormat = "########0.00";
            this.txtBookMarkUp.Postfix = "";
            this.txtBookMarkUp.Prefix = "";
            this.txtBookMarkUp.Size = new System.Drawing.Size(100, 20);
            this.txtBookMarkUp.SkipValidation = false;
            this.txtBookMarkUp.TabIndex = 63;
            this.txtBookMarkUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBookMarkUp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtInstallment
            // 
            this.txtInstallment.AllowSpace = true;
            this.txtInstallment.AssociatedLookUpName = "";
            this.txtInstallment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInstallment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInstallment.ContinuationTextBox = null;
            this.txtInstallment.CustomEnabled = false;
            this.txtInstallment.DataFieldMapping = "";
            this.txtInstallment.Enabled = false;
            this.txtInstallment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstallment.GetRecordsOnUpDownKeys = false;
            this.txtInstallment.IsDate = false;
            this.txtInstallment.Location = new System.Drawing.Point(178, 265);
            this.txtInstallment.Name = "txtInstallment";
            this.txtInstallment.NumberFormat = "########0.00";
            this.txtInstallment.Postfix = "";
            this.txtInstallment.Prefix = "";
            this.txtInstallment.Size = new System.Drawing.Size(100, 20);
            this.txtInstallment.SkipValidation = false;
            this.txtInstallment.TabIndex = 62;
            this.txtInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInstallment.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtaviledamt
            // 
            this.txtaviledamt.AllowSpace = true;
            this.txtaviledamt.AssociatedLookUpName = "";
            this.txtaviledamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaviledamt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtaviledamt.ContinuationTextBox = null;
            this.txtaviledamt.CustomEnabled = true;
            this.txtaviledamt.DataFieldMapping = "FN_AMT_AVAILED";
            this.txtaviledamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaviledamt.GetRecordsOnUpDownKeys = false;
            this.txtaviledamt.IsDate = false;
            this.txtaviledamt.IsRequired = true;
            this.txtaviledamt.Location = new System.Drawing.Point(178, 241);
            this.txtaviledamt.MaxLength = 15;
            this.txtaviledamt.Name = "txtaviledamt";
            this.txtaviledamt.NumberFormat = "########0.00";
            this.txtaviledamt.Postfix = "";
            this.txtaviledamt.Prefix = "";
            this.txtaviledamt.Size = new System.Drawing.Size(100, 20);
            this.txtaviledamt.SkipValidation = false;
            this.txtaviledamt.TabIndex = 61;
            this.txtaviledamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtaviledamt.TextType = CrplControlLibrary.TextType.Amount;
            this.txtaviledamt.Validated += new System.EventHandler(this.txtaviledamt_Validated);
            // 
            // DTPAY_GEN_DATE
            // 
            this.DTPAY_GEN_DATE.CustomEnabled = true;
            this.DTPAY_GEN_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTPAY_GEN_DATE.DataFieldMapping = "PAY_GEN_DATE";
            this.DTPAY_GEN_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPAY_GEN_DATE.HasChanges = true;
            this.DTPAY_GEN_DATE.IsRequired = true;
            this.DTPAY_GEN_DATE.Location = new System.Drawing.Point(178, 219);
            this.DTPAY_GEN_DATE.Name = "DTPAY_GEN_DATE";
            this.DTPAY_GEN_DATE.NullValue = " ";
            this.DTPAY_GEN_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTPAY_GEN_DATE.TabIndex = 60;
            this.DTPAY_GEN_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.DTPAY_GEN_DATE.Validating += new System.ComponentModel.CancelEventHandler(this.DTPAY_GEN_DATE_Validating);
            // 
            // dtExtratime
            // 
            this.dtExtratime.CustomEnabled = true;
            this.dtExtratime.CustomFormat = "dd/MM/yyyy";
            this.dtExtratime.DataFieldMapping = "FN_EXTRA_TIME";
            this.dtExtratime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExtratime.HasChanges = true;
            this.dtExtratime.Location = new System.Drawing.Point(178, 197);
            this.dtExtratime.Name = "dtExtratime";
            this.dtExtratime.NullValue = " ";
            this.dtExtratime.Size = new System.Drawing.Size(100, 20);
            this.dtExtratime.TabIndex = 59;
            this.dtExtratime.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.dtExtratime.Validating += new System.ComponentModel.CancelEventHandler(this.dtExtratime_Validating);
            // 
            // DTFN_END_DATE
            // 
            this.DTFN_END_DATE.CustomEnabled = true;
            this.DTFN_END_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTFN_END_DATE.DataFieldMapping = "FN_END_DATE";
            this.DTFN_END_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTFN_END_DATE.HasChanges = true;
            this.DTFN_END_DATE.IsRequired = true;
            this.DTFN_END_DATE.Location = new System.Drawing.Point(178, 174);
            this.DTFN_END_DATE.Name = "DTFN_END_DATE";
            this.DTFN_END_DATE.NullValue = " ";
            this.DTFN_END_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTFN_END_DATE.TabIndex = 58;
            this.DTFN_END_DATE.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.DTFN_END_DATE.Validating += new System.ComponentModel.CancelEventHandler(this.DTFN_END_DATE_Validating);
            // 
            // DTstartdate
            // 
            this.DTstartdate.CustomEnabled = true;
            this.DTstartdate.CustomFormat = "dd/MM/yyyy";
            this.DTstartdate.DataFieldMapping = "FN_START_DATE";
            this.DTstartdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTstartdate.HasChanges = true;
            this.DTstartdate.IsRequired = true;
            this.DTstartdate.Location = new System.Drawing.Point(178, 151);
            this.DTstartdate.Name = "DTstartdate";
            this.DTstartdate.NullValue = " ";
            this.DTstartdate.Size = new System.Drawing.Size(100, 20);
            this.DTstartdate.TabIndex = 57;
            this.DTstartdate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.DTstartdate.Validating += new System.ComponentModel.CancelEventHandler(this.DTstartdate_Validating);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(18, 401);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(176, 13);
            this.label28.TabIndex = 56;
            this.label28.Text = "Value (Valuation Agreement) :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(38, 379);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(154, 13);
            this.label27.TabIndex = 55;
            this.label27.Text = "Value (Sales Agreement) :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(60, 356);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(127, 13);
            this.label26.TabIndex = 54;
            this.label26.Text = "Mortg. Property City :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(40, 333);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(151, 13);
            this.label25.TabIndex = 53;
            this.label25.Text = "Mortg. Property Address :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(88, 310);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 13);
            this.label24.TabIndex = 52;
            this.label24.Text = "Credit Ratio % :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(115, 288);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 13);
            this.label23.TabIndex = 51;
            this.label23.Text = "Mark Up :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(82, 266);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 13);
            this.label22.TabIndex = 50;
            this.label22.Text = "Installment Amt :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(85, 244);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 13);
            this.label21.TabIndex = 49;
            this.label21.Text = "Amount Taken :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(96, 222);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 13);
            this.label20.TabIndex = 48;
            this.label20.Text = "Payroll Date :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(105, 200);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "Extra Time :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(100, 177);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Expiry Date :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(106, 154);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Start Date :";
            // 
            // txtSchedule
            // 
            this.txtSchedule.AllowSpace = true;
            this.txtSchedule.AssociatedLookUpName = "";
            this.txtSchedule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSchedule.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSchedule.ContinuationTextBox = null;
            this.txtSchedule.CustomEnabled = false;
            this.txtSchedule.DataFieldMapping = "fn_schedule";
            this.txtSchedule.Enabled = false;
            this.txtSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSchedule.GetRecordsOnUpDownKeys = false;
            this.txtSchedule.IsDate = false;
            this.txtSchedule.Location = new System.Drawing.Point(591, 126);
            this.txtSchedule.MaxLength = 3;
            this.txtSchedule.Name = "txtSchedule";
            this.txtSchedule.NumberFormat = "###,###,##0.00";
            this.txtSchedule.Postfix = "";
            this.txtSchedule.Prefix = "";
            this.txtSchedule.Size = new System.Drawing.Size(52, 20);
            this.txtSchedule.SkipValidation = false;
            this.txtSchedule.TabIndex = 44;
            this.txtSchedule.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSchedule.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(521, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "Schedule :";
            // 
            // txtRatio
            // 
            this.txtRatio.AllowSpace = true;
            this.txtRatio.AssociatedLookUpName = "";
            this.txtRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRatio.ContinuationTextBox = null;
            this.txtRatio.CustomEnabled = false;
            this.txtRatio.DataFieldMapping = "FN_C_RATIO";
            this.txtRatio.Enabled = false;
            this.txtRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatio.GetRecordsOnUpDownKeys = false;
            this.txtRatio.IsDate = false;
            this.txtRatio.Location = new System.Drawing.Point(466, 126);
            this.txtRatio.MaxLength = 3;
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.NumberFormat = "###,###,##0.00";
            this.txtRatio.Postfix = "";
            this.txtRatio.Prefix = "";
            this.txtRatio.Size = new System.Drawing.Size(52, 20);
            this.txtRatio.SkipValidation = false;
            this.txtRatio.TabIndex = 42;
            this.txtRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRatio.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(409, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "C.R. % :";
            // 
            // txtMarkup
            // 
            this.txtMarkup.AllowSpace = true;
            this.txtMarkup.AssociatedLookUpName = "";
            this.txtMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkup.ContinuationTextBox = null;
            this.txtMarkup.CustomEnabled = false;
            this.txtMarkup.DataFieldMapping = "FN_MARKUP";
            this.txtMarkup.Enabled = false;
            this.txtMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkup.GetRecordsOnUpDownKeys = false;
            this.txtMarkup.IsDate = false;
            this.txtMarkup.Location = new System.Drawing.Point(358, 126);
            this.txtMarkup.MaxLength = 3;
            this.txtMarkup.Name = "txtMarkup";
            this.txtMarkup.NumberFormat = "###,###,##0.00";
            this.txtMarkup.Postfix = "";
            this.txtMarkup.Prefix = "";
            this.txtMarkup.Size = new System.Drawing.Size(47, 20);
            this.txtMarkup.SkipValidation = false;
            this.txtMarkup.TabIndex = 40;
            this.txtMarkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMarkup.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(286, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "Markup % :";
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = false;
            this.txtFinanceNo.DataFieldMapping = "FN_FIN_NO";
            this.txtFinanceNo.Enabled = false;
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.Location = new System.Drawing.Point(178, 126);
            this.txtFinanceNo.MaxLength = 10;
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(52, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 38;
            this.txtFinanceNo.TabStop = false;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(110, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "L/Fin No. :";
            // 
            // lbSubType
            // 
            this.lbSubType.ActionLOVExists = "FinBookingSubTypeLovExists";
            this.lbSubType.ActionType = "FinBookingSubTypeLov";
            this.lbSubType.ConditionalFields = "txtFinType";
            this.lbSubType.CustomEnabled = true;
            this.lbSubType.DataFieldMapping = "";
            this.lbSubType.DependentLovControls = "";
            this.lbSubType.HiddenColumns = "";
            this.lbSubType.Image = ((System.Drawing.Image)(resources.GetObject("lbSubType.Image")));
            this.lbSubType.LoadDependentEntities = false;
            this.lbSubType.Location = new System.Drawing.Point(237, 99);
            this.lbSubType.LookUpTitle = null;
            this.lbSubType.Name = "lbSubType";
            this.lbSubType.Size = new System.Drawing.Size(26, 21);
            this.lbSubType.SkipValidationOnLeave = false;
            this.lbSubType.SPName = "CHRIS_SP_Finance_fin_Booking_MANAGER";
            this.lbSubType.TabIndex = 36;
            this.lbSubType.TabStop = false;
            this.lbSubType.Tag = "";
            this.lbSubType.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(264, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Sub. Descrip. :";
            // 
            // txtSubDesc
            // 
            this.txtSubDesc.AllowSpace = true;
            this.txtSubDesc.AssociatedLookUpName = "";
            this.txtSubDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSubDesc.ContinuationTextBox = null;
            this.txtSubDesc.CustomEnabled = false;
            this.txtSubDesc.DataFieldMapping = "fn_subdesc";
            this.txtSubDesc.Enabled = false;
            this.txtSubDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubDesc.GetRecordsOnUpDownKeys = false;
            this.txtSubDesc.IsDate = false;
            this.txtSubDesc.Location = new System.Drawing.Point(358, 103);
            this.txtSubDesc.MaxLength = 40;
            this.txtSubDesc.Name = "txtSubDesc";
            this.txtSubDesc.NumberFormat = "###,###,##0.00";
            this.txtSubDesc.Postfix = "";
            this.txtSubDesc.Prefix = "";
            this.txtSubDesc.Size = new System.Drawing.Size(175, 20);
            this.txtSubDesc.SkipValidation = false;
            this.txtSubDesc.TabIndex = 34;
            this.txtSubDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinanceSubType
            // 
            this.txtFinanceSubType.AllowSpace = true;
            this.txtFinanceSubType.AssociatedLookUpName = "lbSubType";
            this.txtFinanceSubType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceSubType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceSubType.ContinuationTextBox = null;
            this.txtFinanceSubType.CustomEnabled = true;
            this.txtFinanceSubType.DataFieldMapping = "FN_SUBTYPE";
            this.txtFinanceSubType.Enabled = false;
            this.txtFinanceSubType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceSubType.GetRecordsOnUpDownKeys = false;
            this.txtFinanceSubType.IsDate = false;
            this.txtFinanceSubType.Location = new System.Drawing.Point(178, 101);
            this.txtFinanceSubType.MaxLength = 2;
            this.txtFinanceSubType.Name = "txtFinanceSubType";
            this.txtFinanceSubType.NumberFormat = "###,###,##0.00";
            this.txtFinanceSubType.Postfix = "";
            this.txtFinanceSubType.Prefix = "";
            this.txtFinanceSubType.Size = new System.Drawing.Size(52, 20);
            this.txtFinanceSubType.SkipValidation = false;
            this.txtFinanceSubType.TabIndex = 3;
            this.txtFinanceSubType.TextType = CrplControlLibrary.TextType.String;
            this.txtFinanceSubType.Validated += new System.EventHandler(this.txtFinanceSubType_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(83, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "L/Fin SubType :";
            // 
            // lbType
            // 
            this.lbType.ActionLOVExists = "FinBookingTypeLovExists";
            this.lbType.ActionType = "FinBookingTypeLov";
            this.lbType.ConditionalFields = "";
            this.lbType.CustomEnabled = true;
            this.lbType.DataFieldMapping = "";
            this.lbType.DependentLovControls = "";
            this.lbType.HiddenColumns = "";
            this.lbType.Image = ((System.Drawing.Image)(resources.GetObject("lbType.Image")));
            this.lbType.LoadDependentEntities = false;
            this.lbType.Location = new System.Drawing.Point(237, 72);
            this.lbType.LookUpTitle = null;
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(26, 21);
            this.lbType.SkipValidationOnLeave = false;
            this.lbType.SPName = "CHRIS_SP_Finance_fin_Booking_MANAGER";
            this.lbType.TabIndex = 31;
            this.lbType.TabStop = false;
            this.lbType.Tag = "";
            this.lbType.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(294, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Descrip. :";
            // 
            // txtFinDescription
            // 
            this.txtFinDescription.AllowSpace = true;
            this.txtFinDescription.AssociatedLookUpName = "";
            this.txtFinDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinDescription.ContinuationTextBox = null;
            this.txtFinDescription.CustomEnabled = false;
            this.txtFinDescription.DataFieldMapping = "fn_desc";
            this.txtFinDescription.Enabled = false;
            this.txtFinDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinDescription.GetRecordsOnUpDownKeys = false;
            this.txtFinDescription.IsDate = false;
            this.txtFinDescription.Location = new System.Drawing.Point(358, 76);
            this.txtFinDescription.MaxLength = 40;
            this.txtFinDescription.Name = "txtFinDescription";
            this.txtFinDescription.NumberFormat = "###,###,##0.00";
            this.txtFinDescription.Postfix = "";
            this.txtFinDescription.Prefix = "";
            this.txtFinDescription.Size = new System.Drawing.Size(175, 20);
            this.txtFinDescription.SkipValidation = false;
            this.txtFinDescription.TabIndex = 29;
            this.txtFinDescription.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinType
            // 
            this.txtFinType.AllowSpace = true;
            this.txtFinType.AssociatedLookUpName = "lbType";
            this.txtFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinType.ContinuationTextBox = null;
            this.txtFinType.CustomEnabled = true;
            this.txtFinType.DataFieldMapping = "FN_TYPE";
            this.txtFinType.Enabled = false;
            this.txtFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinType.GetRecordsOnUpDownKeys = false;
            this.txtFinType.IsDate = false;
            this.txtFinType.IsRequired = true;
            this.txtFinType.Location = new System.Drawing.Point(178, 74);
            this.txtFinType.MaxLength = 6;
            this.txtFinType.Name = "txtFinType";
            this.txtFinType.NumberFormat = "###,###,##0.00";
            this.txtFinType.Postfix = "";
            this.txtFinType.Prefix = "";
            this.txtFinType.Size = new System.Drawing.Size(52, 20);
            this.txtFinType.SkipValidation = false;
            this.txtFinType.TabIndex = 2;
            this.txtFinType.TextType = CrplControlLibrary.TextType.String;
            this.txtFinType.Validated += new System.EventHandler(this.txtFinType_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(102, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "L/Fin Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(104, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "First Name :";
            // 
            // txtPersonnalName
            // 
            this.txtPersonnalName.AllowSpace = true;
            this.txtPersonnalName.AssociatedLookUpName = "";
            this.txtPersonnalName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalName.ContinuationTextBox = null;
            this.txtPersonnalName.CustomEnabled = false;
            this.txtPersonnalName.DataFieldMapping = "P_Name";
            this.txtPersonnalName.Enabled = false;
            this.txtPersonnalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalName.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalName.IsDate = false;
            this.txtPersonnalName.Location = new System.Drawing.Point(178, 48);
            this.txtPersonnalName.MaxLength = 40;
            this.txtPersonnalName.Name = "txtPersonnalName";
            this.txtPersonnalName.NumberFormat = "###,###,##0.00";
            this.txtPersonnalName.Postfix = "";
            this.txtPersonnalName.Prefix = "";
            this.txtPersonnalName.Size = new System.Drawing.Size(385, 20);
            this.txtPersonnalName.SkipValidation = false;
            this.txtPersonnalName.TabIndex = 25;
            this.txtPersonnalName.TabStop = false;
            this.txtPersonnalName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPersonnal
            // 
            this.lbtnPersonnal.ActionLOVExists = "FinBookingPersonalLovExists";
            this.lbtnPersonnal.ActionType = "FinBookingPersonalLov";
            this.lbtnPersonnal.ConditionalFields = "";
            this.lbtnPersonnal.CustomEnabled = true;
            this.lbtnPersonnal.DataFieldMapping = "";
            this.lbtnPersonnal.DependentLovControls = "";
            this.lbtnPersonnal.HiddenColumns = "";
            this.lbtnPersonnal.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnal.Image")));
            this.lbtnPersonnal.LoadDependentEntities = false;
            this.lbtnPersonnal.Location = new System.Drawing.Point(237, 21);
            this.lbtnPersonnal.LookUpTitle = null;
            this.lbtnPersonnal.Name = "lbtnPersonnal";
            this.lbtnPersonnal.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnal.SkipValidationOnLeave = false;
            this.lbtnPersonnal.SPName = "CHRIS_SP_Finance_fin_Booking_MANAGER";
            this.lbtnPersonnal.TabIndex = 24;
            this.lbtnPersonnal.TabStop = false;
            this.lbtnPersonnal.Tag = "";
            this.lbtnPersonnal.UseVisualStyleBackColor = true;
            // 
            // txtPersonnalNo
            // 
            this.txtPersonnalNo.AllowSpace = true;
            this.txtPersonnalNo.AssociatedLookUpName = "lbtnPersonnal";
            this.txtPersonnalNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnalNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnalNo.ContinuationTextBox = null;
            this.txtPersonnalNo.CustomEnabled = true;
            this.txtPersonnalNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnalNo.Enabled = false;
            this.txtPersonnalNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnalNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnalNo.IsDate = false;
            this.txtPersonnalNo.IsRequired = true;
            this.txtPersonnalNo.Location = new System.Drawing.Point(178, 22);
            this.txtPersonnalNo.MaxLength = 8;
            this.txtPersonnalNo.Name = "txtPersonnalNo";
            this.txtPersonnalNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnalNo.Postfix = "";
            this.txtPersonnalNo.Prefix = "";
            this.txtPersonnalNo.Size = new System.Drawing.Size(52, 20);
            this.txtPersonnalNo.SkipValidation = false;
            this.txtPersonnalNo.TabIndex = 1;
            this.txtPersonnalNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnalNo.TextType = CrplControlLibrary.TextType.Double;
            this.txtPersonnalNo.Validated += new System.EventHandler(this.txtPersonnalNo_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(86, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Personnel No. :";
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(434, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 117;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // CHRIS_Finance_FinanceBookingEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlFinanceBooking);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlFinanceBooking";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Finance_FinanceBookingEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Finance_FinanceBookingEntry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlFinanceBooking, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlFinanceBooking.ResumeLayout(false);
            this.pnlFinanceBooking.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlFinanceBooking;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtPersonnalName;
        private CrplControlLibrary.LookupButton lbtnPersonnal;
        private CrplControlLibrary.SLTextBox txtPersonnalNo;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtFinType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtFinDescription;
        private CrplControlLibrary.LookupButton lbType;
        private CrplControlLibrary.LookupButton lbSubType;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtSubDesc;
        private CrplControlLibrary.SLTextBox txtFinanceSubType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private CrplControlLibrary.SLTextBox txtMarkup;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtSchedule;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtRatio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLDatePicker DTFN_END_DATE;
        private CrplControlLibrary.SLDatePicker DTstartdate;
        private CrplControlLibrary.SLDatePicker DTPAY_GEN_DATE;
        private CrplControlLibrary.SLTextBox txtInstallment;
        private CrplControlLibrary.SLTextBox txtaviledamt;
        private CrplControlLibrary.SLTextBox txtBookRatio;
        private CrplControlLibrary.SLTextBox txtBookMarkUp;
        private CrplControlLibrary.SLTextBox txtFN_MORTG_PROP_VALVR;
        private CrplControlLibrary.SLTextBox txtFN_MORTG_PROP_VALSA;
        private CrplControlLibrary.SLTextBox txtFN_MORTG_PROP_CITY;
        private CrplControlLibrary.SLTextBox txtFN_MORTG_PROP_ADD;
        private CrplControlLibrary.LookupButton lbtnCity;
        private CrplControlLibrary.SLTextBox txtCreditRatio;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txtRatioAvail;
        private System.Windows.Forms.Label label30;
        private CrplControlLibrary.SLTextBox txtAvailable;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLTextBox txtFactor;
        private System.Windows.Forms.Label label32;
        private CrplControlLibrary.SLTextBox txtCanBeAvail;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtTotalMonthInstall;
        private System.Windows.Forms.Label label34;
        private CrplControlLibrary.SLTextBox txtPrLevel;
        private CrplControlLibrary.SLTextBox txtprCategory;
        private CrplControlLibrary.SLTextBox txtfnPaySchedule;
        private CrplControlLibrary.SLTextBox txtfnBranch;
        private CrplControlLibrary.SLTextBox txtPrNewAnnual;
        private CrplControlLibrary.SLTextBox txtWTenCBonus;
        private CrplControlLibrary.SLTextBox txtfnMonthly;
        private CrplControlLibrary.SLTextBox txtVal3;
        private CrplControlLibrary.SLTextBox txtRepay;
        private System.Windows.Forms.DateTimePicker dtprdBirth;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Panel panel2;
        private CrplControlLibrary.SLTextBox txtprLoanPack;
        private CrplControlLibrary.SLTextBox txtWBasic;
        private CrplControlLibrary.SLTextBox txtpersonal;
        private CrplControlLibrary.SLTextBox fnfinNo;
        private CrplControlLibrary.SLTextBox GCount;
        private CrplControlLibrary.SLTextBox FinNoType;
        private CrplControlLibrary.SLTextBox GYear;
        private CrplControlLibrary.SLDatePicker dtExtratime;
        private System.Windows.Forms.Label label35;
    }
}