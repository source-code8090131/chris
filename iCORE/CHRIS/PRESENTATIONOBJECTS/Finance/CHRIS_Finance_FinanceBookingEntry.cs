using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Setup;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinanceBookingEntry : ChrisSimpleForm
    {
        public int MARKUP_RATE_DAYS = -1;
        private DateTime NewDate = new DateTime();
        private string g_wcnt = "";
            DateTime  fnStartDate;
            DateTime fnMatDate;
            DataTable dtStartDate=null;
            DataTable dtfnMatDate = null;


        iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput frm1 = null;
        public CHRIS_Finance_FinanceBookingEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Finance_FinanceBookingEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

          
        }
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_Finance_fin_Booking_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }
      
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text  = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = false;
            tbtDelete.Enabled = false;
            tbtList.Visible = false;
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;
            dtExtratime.Value = null;


            this.txtUserName.Text = "User Name: " +this.UserName;

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.AutoValidate = AutoValidate.Disable;

                this.txtPersonnalNo.Text = "";
                this.txtFinType.Text = "";
                this.txtFinanceSubType.Text = "";
                this.txtFN_MORTG_PROP_VALVR.Text = "";
                txtaviledamt.Enabled = true;
                txtaviledamt.CustomEnabled = true;
                base.DoToolbarActions(ctrlsCollection, actionType);

                dtExtratime.Value = null;
                txtOption.Focus();

                this.AutoValidate = AutoValidate.EnablePreventFocusChange;

                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override bool Add()
        {


            base.Add();
           
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            return false;
        }

        protected override bool Edit()
        {
            base.Edit();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;

        }

        protected override bool Delete()
        {
            base.Delete();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;
        }

        protected override bool View()
        {
            base.View();

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
            return false;
        }

        public void FinanceNo_Gen(string FinType,string fnSubType,string prpNo)
        {

            DataTable dt = GetfnFinanceBookingCounts(FinType);
            int g_Year = 0;
            int cnt=0;
            int wcnt = 0;
            string finNo="";

            g_Year = DateTime.Now.Year;

            DataTable dt1 = GetfnFinanceBookingCountForTotal(FinType, fnSubType, prpNo);

            if (dt1 != null )
            {
                if (dt1.Rows.Count > 0 && this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
                {
                    MessageBox.Show("Value Already There For This Loan ..... Press Any Key To Continue");
                }
            }
            cnt = Convert.ToInt32(dt.Rows[0]["Count_Serial"].ToString() == "" ? "0" : dt.Rows[0]["Count_Serial"].ToString());
            wcnt = Convert.ToInt32(dt.Rows[0]["MaxFinNo"].ToString() == "" ? "0" : dt.Rows[0]["MaxFinNo"].ToString());
           
           if( cnt == 0 )
           {
            wcnt =1;
            g_wcnt = wcnt.ToString("0000") ;
               if(FinType != string.Empty )
               {
                  finNo= FinType + "-" + g_wcnt + "/" + g_Year.ToString().Substring(2);
               }
     
           }
           else{
               wcnt =wcnt+1;
                 g_wcnt = wcnt.ToString("0000") ;
                if(FinType != string.Empty )
               {
                   finNo = FinType + "-" + g_wcnt + "/" + g_Year.ToString().Substring(2);
               }
           }
           txtFinanceNo.Text = finNo;
           fin_no_gen2(FinType, g_wcnt, g_Year.ToString().Substring(2));
        }

     

        private DataTable GetDateofBirth()
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", txtPersonnalNo.Text);
           
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "PR_Birth", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }

        private DataTable GetStartDateRecCount()
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", txtPersonnalNo.Text);
            colsNVals.Add("FN_TYPE", txtFinType.Text);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingStartDateRecCount", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }


        private DataTable GetStartDateSubTypeCount()
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", txtPersonnalNo.Text);
            colsNVals.Add("FN_TYPE", txtFinType.Text);
            colsNVals.Add("FN_SUBTYPE", txtFinanceSubType.Text);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingStartDateSubTypeCount", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }


        private DataTable GetfnMatDateCount()
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", txtPersonnalNo.Text);
            colsNVals.Add("FN_TYPE", txtFinType.Text);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingFnMatDate", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }


            private DataTable GetfnMatDateSubTypeCount()
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", txtPersonnalNo.Text);
            colsNVals.Add("FN_TYPE", txtFinType.Text);
            colsNVals.Add("FN_SUBTYPE", txtFinanceSubType.Text);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingFnMatDateSubType", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }

        private void fin_no_gen2 (string FNType,string gCount,string year)
        {
           // year = year.Substring(2);
            int FNYEAR = int.Parse(year);
            double FNFINANCENo = double.Parse(gCount == "" ? "0" : gCount);

            GCount.Text=FNFINANCENo.ToString();
            GYear.Text=FNYEAR.ToString();
            FinNoType.Text = FNType.ToString();

          
            //if (rsltCode.isSuccessful)
            //{
            //    this.PROC_1();
            //}
        }

        private void SaveFinanceNumber()
        {
            if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();

                param.Add("FinType", FinNoType.Text);
                param.Add("FN_FINANCE_NO", GCount.Text);
                param.Add("FN_YEAR", GYear.Text);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.Execute("CHRIS_SP_Other_fin_Booking_MANAGER", "UPDATE_Finance_Serial_No", param);
            }
           
        }


     

        /// <summary> 

        /// Get the last day of the month for any 

        /// full date 

        /// </summary> 

        /// <param name="dtDate"></param> 

        /// <returns></returns> 

        private int GetLastDayOfMonth(DateTime dtDate)
        {

            // set return value to the last day of the month 

            // for any date passed in to the method 



            // create a datetime variable set to the passed in date 

            DateTime dtTo = dtDate;



            // overshoot the date by a month 

            dtTo = dtTo.AddMonths(1);



            // remove all of the days in the next month 

            // to get bumped down to the last day of the 

            // previous month 

            dtTo = dtTo.AddDays(-(dtTo.Day));



            // return the last day of the month 

            return dtTo.Day;

        } 

           private DataTable GetfnFinanceBookingCounts(string finType)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();
            colsNVals.Add("FinType", finType);

            rsltCode = cmnDM.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "FinBookingFinanceNo", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }

        private DataTable GetfnFinanceBookingCountForTotal(string finType,string subtype,string pr_p_no)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();
            colsNVals.Add("FinType", finType);
            colsNVals.Add("FN_SUB_TYPE", subtype);
            colsNVals.Add("PR_P_NO", pr_p_no);
            

            rsltCode = cmnDM.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "FinBookingCount", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }

        private void DTstartdate_Validating(object sender, CancelEventArgs e)
        {
             DateTime w_ysy = new DateTime();
             DateTime checkDOB = new DateTime();

             decimal DateDiff = 0;
             decimal wSum = 0;
             decimal dlschedule = 0;
            if (this.DTstartdate.Value == null)
            {
                MessageBox.Show("DATE HAS TO BE ENTERED AND >= THE CURRENT YEAR");
                DTstartdate.Focus();
                e.Cancel = true;
               
            }
            else
            {
                checkDOB = Convert.ToDateTime(this.dtprdBirth.Value);
                if (checkDOB.Date.CompareTo(DateTime.Now.Date) != 0)
                {
                    int lschedule = 0;
                    DateTime lStartDate = Convert.ToDateTime(this.DTstartdate.Value);
                    DateTime lPRDBirth = Convert.ToDateTime(this.dtprdBirth.Value);
                    if (txtSchedule.Text != string.Empty)
                    {
                        lschedule = int.Parse(txtSchedule.Text);
                    }
                    //int lschedule2 = Convert.ToInt32(lschedule);
                    w_ysy = lStartDate.AddMonths(lschedule);
                    //TimeSpan ts = NewDate.Subtract(lPRDBirth);
                    //int mMonth = NewDate.Month - lPRDBirth.Month;
                    //int wSum = (12 * (NewDate.Year - lPRDBirth.Year)) + mMonth;

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FN_START_DATE", w_ysy);
                    param.Add("FN_END_DATE", lPRDBirth);


                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "MONTH_BETWEEN", param);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateDiff = decimal.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        wSum = DateDiff;
                    }
                    dlschedule = Convert.ToDecimal(lschedule);
                    if (wSum > 696)
                    {
                        wSum = wSum - 696;
                        dlschedule = Math.Abs(dlschedule - wSum);
                       
                    }
                    lschedule = Convert.ToInt32(Math.Round(dlschedule, 0));
                    txtfnPaySchedule.Text = lschedule.ToString();
                    NewDate = lStartDate.AddMonths(lschedule);

                    DTFN_END_DATE.Value = NewDate;

                }

                else
                {
                    if (this.txtPersonnalNo.Text != "")
                    {
                        e.Cancel = true;
                        MessageBox.Show("Birth Date does not Exists.");
                        
                        txtOption.Focus();
                       
                       
                    }
                    
                    
                }
            
            }
        }

        private void DTFN_END_DATE_Validating(object sender, CancelEventArgs e)
        {

            if (DTFN_END_DATE.Value != null)
            {
                DateTime dt1 = Convert.ToDateTime(DTFN_END_DATE.Value);
                DateTime dt2 = Convert.ToDateTime(DTstartdate.Value);
                if (DateTime.Compare(dt1.Date, dt2.Date) < 0 || DateTime.Compare(NewDate.Date, dt1.Date) < 0)
                {
                  
                    MessageBox.Show("DATE HAS TO BE B/W START DATE & EXPECTED END DATE I.E. " + NewDate.ToString("dd/MM/YYYY"));
                    DTFN_END_DATE.Focus();
                    e.Cancel = true;
                    return;



                }

            }
            else
            {
                
                MessageBox.Show("DATE HAS TO BE B/W START DATE & EXPECTED END DATE I.E. "+ NewDate.ToString("dd/MM/YYYY"));
                DTFN_END_DATE.Focus();
                e.Cancel = true;
                return;
            }

 
        }

        private void dtExtratime_Validating(object sender, CancelEventArgs e)
        {
            if (dtExtratime.Value != null)
            {
                DateTime lStartDate = Convert.ToDateTime(this.DTstartdate.Value);
                DateTime dt3 = Convert.ToDateTime(dtExtratime.Value);
                DateTime ExtraTimeDate = new DateTime();
                ExtraTimeDate = lStartDate.AddMonths(12);
                DateTime dt1 = Convert.ToDateTime(DTFN_END_DATE.Value);
                DateTime dt2 = Convert.ToDateTime(DTstartdate.Value);
                if (DateTime.Compare(dt3.Date, dt2.Date) < 0 || DateTime.Compare(dt3.Date, dt1.Date) > 0 || DateTime.Compare(ExtraTimeDate.Date, dt3.Date) < 0)
                {

                    MessageBox.Show("THIS DATE IS A GRACE PERIOD, HAS TO BE  WITHIN 1 YR OF START DATE");
                    dtExtratime.Focus();
                    e.Cancel = true;
                    return;
                }

            }

            else
            {
               
                DTPAY_GEN_DATE.Focus();
            }
 
        }

        private void DTPAY_GEN_DATE_Validating(object sender, CancelEventArgs e)
        {
            bool returnValue = false;
            if (DTPAY_GEN_DATE.Value != null)
            {
                DateTime dt1 = Convert.ToDateTime(DTPAY_GEN_DATE.Value);
                DateTime dt2 = Convert.ToDateTime(DTstartdate.Value);
                double val3 = 0.0;
                int last_Day = 0;
                double val4 = 0.0;
                if (DateTime.Compare(dt1.Date, dt2.Date) <= 0)
                {
                    MessageBox.Show("PAYROLL DATE HAS TO BE ENTERED & SHOULD BE AFTER THE START DATE");
                    DTPAY_GEN_DATE.Focus();
                    e.Cancel = true;
                    return;
                }
                else
                {

                    DateTime dt5 = new DateTime();
                    dt5 = dt2.AddMonths(1);
                    if (!(dt1.Date >= dt2.Date && dt1.Date <= dt5.Date))
                    {
                        MessageBox.Show("ENTER THE DATE WHICH IS NEAREST TO STARTDATE COMING AFTER IT");
                        DTPAY_GEN_DATE.Focus();
                        e.Cancel = true;
                        return;
                    }
                    else
                    {
                        if (dt1.Month == dt2.Month && dt1.Year == dt2.Year)
                        {
                            System.TimeSpan diffResult = dt1.Subtract(dt2);
                            val3 = Math.Round(diffResult.TotalDays);

                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dt2);
                            val4 = last_Day - dt2.Day;
                            val3 = val4 + dt1.Day;

                        }


                        if (val3 > 30)
                        {
                            MessageBox.Show("PAYROLL  DATE SHOULD BE THE ONE WHICH IS JUST AFTER START DATE");
                            DTPAY_GEN_DATE.Focus();
                            return;
                        }

                        txtVal3.Text = val3.ToString();

                        txtaviledamt.Focus();
                        returnValue=PROC_1();
                        if (returnValue == false)
                        {
                            txtOption.Focus();
                            //this.Reset();
                            e.Cancel = true;
                            return;
                        }
                    }

                }

            }
            else
            {
                MessageBox.Show("PAYROLL DATE HAS TO BE ENTERED & SHOULD BE AFTER THE START DATE");
                DTPAY_GEN_DATE.Focus();
                e.Cancel = true;
                return;
            }

        }


        private bool PROC_1()
        {

           double  lCREDIT_RATIO = 0;
           double  lCREDIT_RATIO_MONTH;
           double  lLESS_RATIO_AVAILED;
           double  lAVAILABLE_RATIO;
           double  lFACTOR;
           double  lAMT_OF_LOAN_CAN_AVAIL;
           double lTOTAL_MONTHLY_INSTALLMENT;
           int CREDIT_RATIO_PER;
           int AUX1;
           double AUX2;
           double AUX3;
           int AUX4;
           int AUX5;
           int AUX7;
           double AUX8;
           double AUX9;
           double dis_repay;
           bool returnValue = false;

           AUX2 = 0;
           AUX7 = 0;

            /// Monthly Deduction in AUX2
           Dictionary<string, object> param = new Dictionary<string, object>();
           param.Add("PR_P_NO", txtPersonnalNo.Text);
           DataTable dtMonthlyDeduction = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "MonthlyDeduction", param);
           if (dtMonthlyDeduction != null && dtMonthlyDeduction.Rows.Count > 0)
           {
               AUX2 = double.Parse(dtMonthlyDeduction.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtMonthlyDeduction.Rows[0].ItemArray[0].ToString());
           }
            //// New Annual Pack in AUX9 
           //AUX9 = double.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text);
           AUX9 = double.Parse(txtprLoanPack.Text == "" ? "0" : txtprLoanPack.Text);

           Decimal lCREDIT_RATIODecimal;
           Decimal lCREDIT_RATIODecimal2;


           DataTable dtfnBookCount = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnBookingProcCount", param);
           if (dtfnBookCount != null)
           {
               if (dtfnBookCount.Rows.Count > 0)
               {
                   lCREDIT_RATIO = (AUX9 / 100) * double.Parse(txtRatio.Text == "" ? "0" : this.PROC_3(this.txtFinType.Text));
                   //this.PROC_3(this.txtFinType.Text);
               }
               
           }
           else
           {
               lCREDIT_RATIO = (AUX9 / 100) * double.Parse(txtRatio.Text == "" ? "0" : txtRatio.Text);
           }


            ///-------FURTHER CALC--------------////
            ///
           //double tmp = 33.33;
           //lCREDIT_RATIO = (AUX9 / 100) * double.Parse(txtRatio.Text == "" ? "0" : txtRatio.Text);
            ///txtRatio = finType.Ratio
            ///
   

            lCREDIT_RATIO_MONTH = lCREDIT_RATIO/12;

            lCREDIT_RATIO_MONTH = Math.Round(lCREDIT_RATIO_MONTH, 2, MidpointRounding.AwayFromZero);

            lLESS_RATIO_AVAILED = AUX2;

            lAVAILABLE_RATIO    = lCREDIT_RATIO_MONTH - lLESS_RATIO_AVAILED;

            dis_repay = double.Parse(txtfnPaySchedule.Text == "" ? "0" : txtfnPaySchedule.Text);


            txtRepay.Text = dis_repay.ToString();
            if (txtMarkup.Text != string.Empty && txtMarkup.Text !="0")
            {
                AUX8 = double.Parse(txtMarkup.Text == "" ? "0" : txtMarkup.Text) / 1200;
               AUX8   =  AUX8 + 1;

                //this can be made some prob
               AUX3 = Convert.ToDouble(Math.Pow(AUX8, -double.Parse(txtfnPaySchedule.Text == "" ? "0" : txtfnPaySchedule.Text)));

               lFACTOR = (AUX8 - 1)/ (1 - AUX3) * 1000;
            }
            else 
            {
                lFACTOR = 1000 / double.Parse(txtfnPaySchedule.Text == "" ? "0" : txtfnPaySchedule.Text);
            }


            ///--------
           
            txtCreditRatio.Text=lCREDIT_RATIO_MONTH.ToString();
            txtRatioAvail.Text=lLESS_RATIO_AVAILED.ToString();
            txtAvailable.Text=lAVAILABLE_RATIO.ToString();
            txtFactor.Text=lFACTOR.ToString();
            lAMT_OF_LOAN_CAN_AVAIL = (lAVAILABLE_RATIO * 1000) / Math.Round(lFACTOR, 6);
            txtCanBeAvail.Text = lAMT_OF_LOAN_CAN_AVAIL.ToString();
            if (this.FunctionConfig.CurrentOption== Function.Modify)
            {
                lAMT_OF_LOAN_CAN_AVAIL = double.Parse(txtaviledamt.Text == "" ? "0" : txtaviledamt.Text) + lAMT_OF_LOAN_CAN_AVAIL;
                txtCanBeAvail.Text = lAMT_OF_LOAN_CAN_AVAIL.ToString();
            }

            if (lAMT_OF_LOAN_CAN_AVAIL != 0)
            {
                txtaviledamt.Focus();
                returnValue = true; ;
            }

            
            else
            {
                MessageBox.Show("THE CREDIT RATIO HAS BEEN FULLY AVAILED .PRESS [ENTER] TO CONT ...");

                returnValue = false; ;
            }
            return returnValue;
            }
        

        /*** This Procedure Checks If House loan Has Been Taken ***/
        private String PROC_3(string FinT)
        {
            String ratio = "0";
                if (txtFinType.Text != string.Empty)
                {
                    if (txtFinType.Text.Length > 0)
                    {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FinType", FinT.Substring(0, 1));
                    DataTable dt = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "HouseLoanEntryCheck", param);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            MessageBox.Show("More than One House Loan Entries");
                        }
                        else if (dt.Rows.Count.Equals(1))
                            ratio = dt.Rows[0][0].ToString();
                    }

                    }

                }
                return ratio;

        }


        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        private void txtaviledamt_Validated(object sender, EventArgs e)
        {
            double LoanCanAvail = 0.0;
            double.TryParse(txtCanBeAvail.Text, out LoanCanAvail);
            if (txtaviledamt.Text =="0.00")
            {
                txtaviledamt.Text = "";
                
            }

            if (txtaviledamt.Text == string.Empty )
            {
                MessageBox.Show("VALUE HAS TO BE ENTERED & SHOULD BE <  " + txtCanBeAvail.Text);
                txtaviledamt.Focus();
                return;

              

            }
            else
            {

                if (double.Parse(txtaviledamt.Text) > LoanCanAvail)
                {
                    MessageBox.Show("VALUE HAS TO BE ENTERED & SHOULD BE <  " + txtCanBeAvail.Text);
                    txtaviledamt.Focus();
                    return;
                }




                double AUX44;
                double AUX55;
                int AUX99;
                double wVal3;
                string Cat = "";
                double fnCreditRatioPer;
                double lLESS_RATIO_AVAILED;
                double lAVAILABLE_RATIO;
                double lFACTOR;
                double lAMT_OF_LOAN_CAN_AVAIL;
                double lTOTAL_MONTHLY_INSTALLMENT;
                double fnAvailedAmount;
                double fnMonthlyMarkup;
                double CREDIT_RATIO;
                double lmarkup;
                double wInstallment;
                double ClericalBonus=0.0;
                double spAllowanceAmt = 0.0;

                AUX99 = int.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text);
                Cat = txtprCategory.Text;

                lAMT_OF_LOAN_CAN_AVAIL = double.Parse(txtCanBeAvail.Text == "" ? "0" : txtCanBeAvail.Text);
                fnAvailedAmount = double.Parse(txtaviledamt.Text == "" ? "0" : txtaviledamt.Text );
                lAMT_OF_LOAN_CAN_AVAIL = lAMT_OF_LOAN_CAN_AVAIL - fnAvailedAmount;

            

                lFACTOR = double.Parse(txtFactor.Text== "" ? "0" : txtFactor.Text);

                lAVAILABLE_RATIO = lFACTOR * lAMT_OF_LOAN_CAN_AVAIL / 1000;

                lAVAILABLE_RATIO = Math.Round(lAVAILABLE_RATIO, 2, MidpointRounding.AwayFromZero);
               
                CREDIT_RATIO = double.Parse(txtCreditRatio.Text == "" ? "0" : txtCreditRatio.Text);

                lLESS_RATIO_AVAILED = CREDIT_RATIO - lAVAILABLE_RATIO;

              

                lTOTAL_MONTHLY_INSTALLMENT = fnAvailedAmount * lFACTOR / 1000;
             

                wVal3 = double.Parse(txtVal3.Text == "" ? "0" : txtVal3.Text);
                lmarkup = double.Parse(txtMarkup.Text == "" ? "0" : txtMarkup.Text);
                fnMonthlyMarkup = fnAvailedAmount *( wVal3/100 ) * lmarkup / MARKUP_RATE_DAYS;


                txtCanBeAvail.Text = lAMT_OF_LOAN_CAN_AVAIL.ToString();
                txtAvailable.Text = lAVAILABLE_RATIO.ToString();
                txtRatioAvail.Text = lLESS_RATIO_AVAILED.ToString();
                txtTotalMonthInstall.Text = lTOTAL_MONTHLY_INSTALLMENT.ToString();

                txtBookMarkUp.Text = fnMonthlyMarkup.ToString();
                AUX44 = AUX99 / 12;
                AUX55 = AUX99 / 12;

                if (txtprCategory.Text == "C")
                {
                    //coding pending
                   ClericalBonus=Double.Parse(txtWTenCBonus.Text == "" ? "0" :txtWTenCBonus.Text );
                   AUX44 = (ClericalBonus / 100) + 1;
                   AUX55 = AUX55 * AUX44;

                     Dictionary<string, object> param = new Dictionary<string, object>();
                     param.Clear();
                     param.Add("category", txtprCategory.Text);
                     param.Add("level", txtPrLevel.Text);
                     param.Add("fn_branch", txtfnBranch.Text);
                     DataTable dtClericalAllowance = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "ClericalStaffCheck", param);
                     if (dtClericalAllowance != null)
                     {
                         if (dtClericalAllowance.Rows.Count > 0)
                         {
                             spAllowanceAmt = double.Parse(dtClericalAllowance.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtClericalAllowance.Rows[0].ItemArray[0].ToString());
                             AUX55 = AUX55 + spAllowanceAmt;
                         }
                     }

                     else
                     {
                         MessageBox.Show("UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE");
                         this.Reset();
                         base.ClearForm(this.Controls);
                         this.Cancel();
                         txtOption.Focus();

                         CHRIS_Setup_AllowanceEnter AllowanceDialog = new CHRIS_Setup_AllowanceEnter(null, null);
                         AllowanceDialog.ShowDialog();
                         return;
                      
                     }
                }


                double monthly = 0.0;
                double fnCred = 0;
                monthly = Math.Round(lTOTAL_MONTHLY_INSTALLMENT, 2);
                txtfnMonthly.Text = monthly.ToString();

                wInstallment = lTOTAL_MONTHLY_INSTALLMENT - fnMonthlyMarkup;
                txtInstallment.Text = wInstallment.ToString();
               // fnCred=(lLESS_RATIO_AVAILED / AUX55)
                fnCreditRatioPer = (Math.Round(lLESS_RATIO_AVAILED,2) / AUX55) *100;
                //fnCreditRatioPer = Math.Round(fnCreditRatioPer, 2, MidpointRounding.AwayFromZero);
                txtBookRatio.Text = fnCreditRatioPer.ToString();

                txtaviledamt.Enabled = false;
                txtaviledamt.CustomEnabled = false;
                txtFN_MORTG_PROP_ADD.Focus();
            }

        }

        private bool OverlimitApproval()
        {

            DialogResult dRes = MessageBox.Show("Do you want to process this Over limit Loan Booking.", "Note"
                                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                return true;
            }
            else if (dRes == DialogResult.No)
            {

                return false;
            }
            return false;

        }


        public void ShowDeleteMsg(CHRIS_Finance_View_FinanceBooking frm)
        {
            DialogResult dRes = MessageBox.Show("Do you want to Delete this record [Y/N]..", "Note"
                                                 , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {

                //base.DoToolbarActions(this.Controls, "Delete");
                CustomDelete();
                this.Reset();
                this.Cancel();
                frm.Close();
              
                //call save
                return;

            }
            else if (dRes == DialogResult.No)
            {
                frm.Close();
                this.Reset();
                base.DoToolbarActions(this.Controls, "Cancel");
               
                //txtOption.Select();
                //txtOption.Focus();

                return;
            }
        }

        public void ShowViewMsg(CHRIS_Finance_View_FinanceBooking frm)
        {

            if (MessageBox.Show("DO YOU WANT TO VIEW MORE RECORDS [Y]es [N]o", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {



                this.Reset(); 
                frm.Close();
                txtPersonnalNo.Select();
                txtPersonnalNo.Focus();
               
              
               //return;

            }
            else
            {
                frm.Close();
               // this.Reset();
                this.Cancel();
                //return;

                //txtOption.Select();
                //txtOption.Focus();
                //return;

            }

        }

        private void txtFN_MORTG_PROP_VALVR_Validated(object sender, EventArgs e)
        {
            //DateTime SaveDate = new DateTime(1900,1,1);
            //if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption== Function.Modify)

            //{
            //    DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
            //                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (dRes == DialogResult.Yes)
            //    {

            //        if (dtExtratime.Value == null)
            //        {
            //            dtExtratime.Value = SaveDate;
            //        }

                  
            //       //CallReport();
            //        txtpersonal.Text = txtPersonnalNo.Text;
            //        fnfinNo.Text = txtFinanceNo.Text;
            //        base.DoToolbarActions(this.Controls, "Save");
            //        if (this.FunctionConfig.CurrentOption == Function.Add)
            //        {
            //            SaveFinanceNumber();
            //        }
            //        CallReport("AUPR","OLD");
            //        CallReport("AUP0", "NEW");

            //        this.Reset();
            //        //call save
            //        return;

            //    }
            //    else if (dRes == DialogResult.No)
            //    {
            //        this.Reset();
            //        txtOption.Focus();

            //        return;
            //    }
            //}



        }

        public void FinanceBookingView()
        {

           String _FinType;
             String _FinNo;
            DateTime _StartDate=new DateTime();
            DateTime _ExpiryDate = new DateTime();
            DateTime _ExtraTime = new DateTime();
            Double _InstallMentAmt;
            Double _CreditRatio;
            Double _Amt;
            _ExtraTime = DateTime.Now;
            int _ID;

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnalNo.Text);
            param.Add("FN_TYPE", txtFinType.Text);
            DataTable dt = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingViewPage", param);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    _FinType = dt.Rows[0]["FN_TYPE"].ToString();
                    _FinNo = dt.Rows[0]["FN_FINANCE_NO"].ToString();
                   
                        _StartDate = Convert.ToDateTime(dt.Rows[0]["FN_START_DATE"]);
                  
                        _ExpiryDate = Convert.ToDateTime(dt.Rows[0]["FN_END_DATE"]);
                 
                        //_ExtraTime = Convert.ToDateTime(dt.Rows[0]["FN_EXTRA_TIME"]);
                  
                    _InstallMentAmt = Convert.ToDouble(dt.Rows[0]["FN_MONTHLY_DED"]);
                    _CreditRatio = Convert.ToDouble(dt.Rows[0]["FN_C_RATIO_PER"]);
                    _Amt = Convert.ToDouble(dt.Rows[0]["FN_AMT_AVAILED"]);
                    _ID = Convert.ToInt32(dt.Rows[0]["ID"]);
                    //txtPersonnalNo.Focus();
                   txtOption.Focus();
                    iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceBookingCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceBookingCommand)this.pnlFinanceBooking.CurrentBusinessEntity;
                    if (ent != null)
                    {
                        ent.ID = _ID;
                        this.m_intPKID = ent.ID;
                    }
                  //  txtID.Text = _ID.ToString();
                   
                    CHRIS_Finance_View_FinanceBooking process = new CHRIS_Finance_View_FinanceBooking(null, this.connbean, this);
                    process.ShowDialog();

                }

            }

        }

        private void Reset()
        {
            this.txtPersonnalNo.Text = "";
            this.txtPersonnalName.Text = "";
            this.txtFinType.Text = "";
           this.txtFinanceSubType.Text = "";
           this.txtFinDescription.Text = "";
           this.txtSubDesc.Text = "";
           this.txtMarkup.Text = "";
           this.txtFinanceNo.Text = "";

           this.txtRatio.Text = "";
           this.txtSchedule.Text = "";
           this.txtaviledamt.Text = "";
           this.txtInstallment.Text = "";
           this.txtBookMarkUp.Text = "";
           this.txtBookRatio.Text = "";
           this.txtFN_MORTG_PROP_ADD.Text = "";
           this.txtFN_MORTG_PROP_CITY.Text = "";
           this.txtFN_MORTG_PROP_VALSA.Text = "";
           this.txtFN_MORTG_PROP_VALVR.Text = "";



        }

        private void txtPersonnalNo_Validated(object sender, EventArgs e)
        {

            if (txtPersonnalNo.Text != string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txtPersonnalNo.Text);
                DataTable dtPersonalFields = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingPersonalLovValidated", param);
                if (dtPersonalFields != null)
                {
                    if (dtPersonalFields.Rows.Count > 0)
                    {
                        txtprCategory.Text = dtPersonalFields.Rows[0].ItemArray[2].ToString();
                        txtPrLevel.Text = dtPersonalFields.Rows[0].ItemArray[3].ToString();
                        txtPrNewAnnual.Text = dtPersonalFields.Rows[0].ItemArray[6].ToString();
                        if (dtPersonalFields.Rows[0].ItemArray[4].ToString() != string.Empty)
                        {
                            dtprdBirth.Value = Convert.ToDateTime(dtPersonalFields.Rows[0].ItemArray[4].ToString());
                        }
                      
                        txtfnBranch.Text = dtPersonalFields.Rows[0].ItemArray[5].ToString();

                        Proc_Level();

                        if (txtprCategory.Text == "C" && (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify))
                        {
                            if (this.FunctionConfig.CurrentOption != Function.View)
                            {
                                frm1 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput("ENTER THE % OF 10_C_BONUS  ......", CrplControlLibrary.TextType.Double);
                                frm1.TextBox.MaxLength = 5;
                                this.frm1.TextBox.KeyPress += new KeyPressEventHandler(txt_KeyPress);
                                frm1.ShowDialog();
                                txtWTenCBonus.Text = frm1.Value;
                                txtFinType.IsRequired = false;
                                BonusClericalCalc();

                                txtFinType.IsRequired = true;
                                //txtPersonnalNo.Focus();
                                return;
                            
                            }
                        }
                        else
                        {
                            txtprLoanPack.Text = txtPrNewAnnual.Text;
                        }

                    }
                    else
                    {
                        MessageBox.Show("NO DATA FOR THIS Per No.PRSSS [F9] (HELP)/ [Enter] FOR EXIT");
                        this.Cancel();
                        return;


                    }

                }
                else
                {

                    MessageBox.Show("NO DATA FOR THIS Per No.PRSSS [F9] (HELP)/ [Enter] FOR EXIT");
                    this.Cancel();
                    return;
                }

            }

               
        }

        public void Proc_Level()
        {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txtPersonnalNo.Text);
                DataTable dtProcLevel = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingProc_Level", param);
                if (dtProcLevel != null)
                {
                    if (dtProcLevel.Rows.Count > 0)
                    {
                        if (dtProcLevel.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            txtPrLevel.Text = dtProcLevel.Rows[0].ItemArray[0].ToString();
                        }
                    }
                }

        }

        protected void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            CrplControlLibrary.SLTextBox txt = (CrplControlLibrary.SLTextBox)sender;
            if (txt != null && txt.Text != "" && txt.Text != "0")
            {
                if (e.KeyChar == '\r' || e.KeyChar == '\t')
                {
                    this.frm1.Close();
                   
                  


                }
            }
        }

        private void BonusClericalCalc()
        {
            double PR_LOAN_PACK = 0.0;
            double Pr_New_Annual_Pack = 0.0;
            double tenCBonus=0.0;
            if (txtWTenCBonus.Text != string.Empty)
            {
                txtFinType.IsRequired = false;
                Pr_New_Annual_Pack  = double.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text) ;
                PR_LOAN_PACK        = Pr_New_Annual_Pack + 2400;
                tenCBonus           = double.Parse(txtWTenCBonus.Text == "" ? "0" : txtWTenCBonus.Text);
                PR_LOAN_PACK        = PR_LOAN_PACK + ((PR_LOAN_PACK * tenCBonus) / 100);
                txtprLoanPack.Text  = PR_LOAN_PACK.ToString();

                txtFinType.IsRequired = true;
            }
            
        }
        
        private void SetModifyfields()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnalNo.Text);
            param.Add("FN_TYPE", txtFinType.Text);
            DataTable dtList = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingMODList", param);
            if (dtList != null)
            {
                if (dtList.Rows.Count > 0)
                {
                  
                        txtFinanceNo.Text = dtList.Rows[0].ItemArray[2].ToString();
                   
                        if (dtList.Rows[0].ItemArray[3].ToString() != string.Empty)
                        {
                            DTstartdate.Value = Convert.ToDateTime(dtList.Rows[0].ItemArray[3].ToString());
                        }
                            txtaviledamt.Text = dtList.Rows[0].ItemArray[4].ToString();
                            txtInstallment.Text = dtList.Rows[0].ItemArray[5].ToString();
                            txtBookRatio.Text = dtList.Rows[0].ItemArray[6].ToString();
                            txtFN_MORTG_PROP_ADD.Text = dtList.Rows[0].ItemArray[8].ToString();
                            txtFN_MORTG_PROP_CITY.Text = dtList.Rows[0].ItemArray[9].ToString();
                            txtFN_MORTG_PROP_VALSA.Text = dtList.Rows[0].ItemArray[10].ToString();
                            txtFN_MORTG_PROP_VALVR.Text = dtList.Rows[0].ItemArray[11].ToString();


                        }

                    }
              
        }

        private void SetSubTypeModifyfields()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnalNo.Text);
            param.Add("FN_TYPE", txtFinType.Text);
             param.Add("FN_SUBTYPE", txtFinanceSubType.Text);
            DataTable dtList = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingMODSubTypeList", param);
            if (dtList != null)
            {
                if (dtList.Rows.Count > 0)
                {
                  
                        txtFinanceNo.Text = dtList.Rows[0].ItemArray[2].ToString();
                   
                        if (dtList.Rows[0].ItemArray[3].ToString() != string.Empty)
                        {
                            DTstartdate.Value = Convert.ToDateTime(dtList.Rows[0].ItemArray[3].ToString());
                        }
                            txtaviledamt.Text = dtList.Rows[0].ItemArray[4].ToString();
                            txtInstallment.Text = dtList.Rows[0].ItemArray[5].ToString();
                            txtBookRatio.Text = dtList.Rows[0].ItemArray[6].ToString();
                            txtFN_MORTG_PROP_ADD.Text = dtList.Rows[0].ItemArray[8].ToString();
                            txtFN_MORTG_PROP_CITY.Text = dtList.Rows[0].ItemArray[9].ToString();
                            txtFN_MORTG_PROP_VALSA.Text = dtList.Rows[0].ItemArray[10].ToString();
                            txtFN_MORTG_PROP_VALVR.Text = dtList.Rows[0].ItemArray[11].ToString();


                        }

                    }
              
        }

        private void txtFinType_Validated(object sender, EventArgs e)
        {

            DateTime Date1;
            DateTime Date2;
            Double calc1=0.0;
            Double calc2=0.0;
            Double calc3 = 0.0;
            
         
            if (txtFinType.Text != string.Empty)
            {

                Dictionary<string, object> paramSub = new Dictionary<string, object>();
                paramSub.Add("FN_TYPE", txtFinType.Text);
                DataTable dtSubTypeCount = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinSubTypeCount", paramSub);
                if (dtSubTypeCount != null)
                {
                    if (dtSubTypeCount.Rows.Count > 0)
                    {
                        txtFinanceSubType.Focus();
                    }
                   
                }
                else
                {
                    if (this.FunctionConfig.CurrentOption == Function.Add)
                    {

                        if (txtFinanceNo.Text == string.Empty || txtFinanceNo.Text.Substring(1, 1) != txtFinType.Text.Substring(1, 1))
                        {
                            FinanceNo_Gen(txtFinType.Text.Substring(0, 1), txtFinanceSubType.Text, txtPersonnalNo.Text);
                        }
                        DTstartdate.Focus();
                    }
                    else
                    {
                        dtStartDate = GetStartDateRecCount();
                        if (dtStartDate != null)
                        {
                            if (dtStartDate.Rows[0].ItemArray[0].ToString() == string.Empty)
                            {
                                MessageBox.Show("NO RECORD FOUND FOR THIS TYPE");
                                base.ClearForm(pnlFinanceBooking.Controls);
                                txtOption.Focus();
                                return;


                            }
                            else
                            {
                                if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                                {
                                    dtfnMatDate = GetfnMatDateCount();
                                    if (dtfnMatDate != null)
                                    {
                                        if (dtfnMatDate.Rows.Count > 0)
                                        {
                                            DateTime.TryParse(dtfnMatDate.Rows[0].ItemArray[0].ToString(), out Date1);
                                            DateTime.TryParse(dtStartDate.Rows[0].ItemArray[0].ToString(), out Date2);
                                            if (DateTime.Compare(Date1, Date2) == 0)
                                            {
                                                if (this.FunctionConfig.CurrentOption == Function.Delete)
                                                {
                                                    FinanceBookingView();
                                                    return;

                                                   

                                                 
                                                }

                                                else
                                                {///Modify Fin Booking 
                                                    /// Get Some fields on the basis of prpNo        

                                                    Dictionary<string, object> paramSood = new Dictionary<string, object>();
                                                    paramSood.Add("PR_P_NO", txtPersonnalNo.Text);
                                                    paramSood.Add("FN_TYPE", txtFinType.Text);
                                                    DataTable dtSoodFields = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinTypeSOOD", paramSood);

                                                    //Exec Query
                                                    SetModifyfields();
                                                    if (dtSoodFields != null)
                                                    {
                                                        if (dtSoodFields.Rows.Count > 0)
                                                        {
                                                            if (dtSoodFields.Rows[0].ItemArray[0].ToString() != string.Empty)
                                                            {
                                                                txtBookMarkUp.Text = dtSoodFields.Rows[0].ItemArray[0].ToString();
                                                            }

                                                        }

                                                    }

                                                    txtWBasic.Text = txtMarkup.Text;

                                                    /*
                                                         calc1
                                                      :w_basic := (:fn_monthly_markup * 36000)/
                                                        (:w_basic*:fin_booking.fn_amt_availed);
                                                         go_field('fin_booking.fn_start_date');
                                                     * */
                                                    if (txtBookMarkUp.Text != string.Empty)
                                                    {
                                                        calc1 = Double.Parse(txtBookMarkUp.Text) * MARKUP_RATE_DAYS * 100;

                                                    }
                                                    if (txtWBasic.Text != string.Empty && txtaviledamt.Text != string.Empty)
                                                    {
                                                        calc2 = (Double.Parse(txtWBasic.Text)) * (Double.Parse(txtaviledamt.Text));
                                                    }
                                                    if (calc2 != 0.0)
                                                    {
                                                        calc3 = calc1 / calc2;


                                                        txtWBasic.Text = calc3.ToString();
                                                        DTstartdate.Focus();
                                                    }
                                                    else
                                                    {
                                                        if (txtFinType.Text != string.Empty)
                                                        {
                                                            MessageBox.Show("wBasic=0.0 ... Sorry You cant Continue");

                                                            txtFinType.Focus();
                                                            return;
                                                        }
                                                    }

                                                }

                                            }

                                            else
                                            {
                                                MessageBox.Show("YOU CANT ACCESS THIS RECORD BECAUSE PAYROLL HAS BEEN GENERATED");

                                               
                                                base.ClearForm(pnlFinanceBooking.Controls);
                                                this.Reset();
                                                txtOption.Focus();
                                                return;
                                            }

                                        }

                                    }

                                }

                                if (this.FunctionConfig.CurrentOption == Function.View)
                                {

                                    FinanceBookingView();
                                    return;
                                    

                                  

                                }


                            }
                        }

                        else
                        {
                            MessageBox.Show("NO RECORD FOUND FOR THIS TYPE");
                            base.ClearForm(pnlFinanceBooking.Controls);
                            txtOption.Focus();
                            return;

                        }


                    }


                }//////


            }
           
        }

        private void txtFinanceSubType_Validated(object sender, EventArgs e)
        {
          dtStartDate=null;
            dtfnMatDate=null;

            DateTime Date1;
            DateTime Date2;
            Double calc1 = 0.0;
            Double calc2 = 0.0;
            Double calc3 = 0.0;
            if (txtFinanceSubType.Text != string.Empty)
            {
                if (txtFinType.Text != string.Empty)
                {
                    Dictionary<string, object> paramSub = new Dictionary<string, object>();
                    paramSub.Add("FN_TYPE", txtFinType.Text);
                    DataTable dtSubTypeCount = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinSubTypeCount", paramSub);
                    if (dtSubTypeCount != null)
                    {
                        if (dtSubTypeCount.Rows.Count > 0)
                        {

                            if (this.FunctionConfig.CurrentOption == Function.Add)
                            {

                                if (txtFinanceNo.Text == string.Empty || txtFinanceNo.Text.Substring(1, 1) != txtFinType.Text.Substring(1, 1))
                                {
                                    FinanceNo_Gen(txtFinType.Text.Substring(0, 1), txtFinanceSubType.Text, txtPersonnalNo.Text);
                                }
                                DTstartdate.Focus();
                            }

                            else
                            {
                                dtStartDate = GetStartDateSubTypeCount();
                                if (dtStartDate != null)
                                {
                                    if (dtStartDate.Rows[0].ItemArray[0].ToString() == string.Empty)
                                    {
                                        MessageBox.Show("NO RECORD FOUND FOR THIS TYPE");
                                        base.ClearForm(pnlFinanceBooking.Controls);
                                        txtOption.Focus();
                                    }
                                    else
                                    {
                                        if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                                        {
                                            dtfnMatDate = GetfnMatDateSubTypeCount();
                                            if (dtfnMatDate != null)
                                            {
                                                if (dtfnMatDate.Rows.Count > 0)
                                                {
                                                    DateTime.TryParse(dtfnMatDate.Rows[0].ItemArray[0].ToString(), out Date1);
                                                    DateTime.TryParse(dtStartDate.Rows[0].ItemArray[0].ToString(), out Date2);
                                                    if (DateTime.Compare(Date1, Date2) == 0)
                                                    {
                                                        if (this.FunctionConfig.CurrentOption == Function.Delete)
                                                        {
                                                            FinanceBookingView();
                                                            
                                                         
                                                        }

                                                        else
                                                        {///Modify Fin Booking 
                                                            /// Get Some fields on the basis of prpNo     
                                                            /// 

                                                            Dictionary<string, object> paramSood = new Dictionary<string, object>();
                                                            paramSood.Add("PR_P_NO", txtPersonnalNo.Text);
                                                            paramSood.Add("FN_TYPE", txtFinType.Text);
                                                            paramSood.Add("FN_SUBTYPE", txtFinanceSubType.Text);
                                                            DataTable dtSoodFields = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinSubTypeSOOD", paramSood);

                                                            //Exec Query
                                                            SetSubTypeModifyfields();
                                                            if (dtSoodFields != null)
                                                            {
                                                                if (dtSoodFields.Rows.Count > 0)
                                                                {
                                                                    if (dtSoodFields.Rows[0].ItemArray[0].ToString() != string.Empty)
                                                                    {
                                                                        txtBookMarkUp.Text = dtSoodFields.Rows[0].ItemArray[0].ToString();
                                                                    }

                                                                }

                                                            }

                                                            txtWBasic.Text = txtMarkup.Text;


                                                            if (txtBookMarkUp.Text != string.Empty)
                                                            {
                                                                calc1 = Double.Parse(txtBookMarkUp.Text) * MARKUP_RATE_DAYS * 100;

                                                            }
                                                            if (txtWBasic.Text != string.Empty && txtaviledamt.Text != string.Empty)
                                                            {
                                                                calc2 = (Double.Parse(txtWBasic.Text)) * (Double.Parse(txtaviledamt.Text));
                                                            }
                                                            if (calc2 != 0.0)
                                                            {
                                                                calc3 = calc1 / calc2;
                                                            }

                                                            txtWBasic.Text = calc3.ToString();
                                                            DTstartdate.Focus();

                                                        }

                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("YOU CANT ACCESS THIS RECORD BECAUSE PAYROLL HAS BEEN GENERATED");

                                                        this.Reset();
                                                        base.ClearForm(pnlFinanceBooking.Controls);
                                                        txtOption.Focus();

                                                    }

                                                }

                                            }

                                        }

                                        if (this.FunctionConfig.CurrentOption == Function.View)
                                        {

                                            FinanceBookingView();
                                           
                                           
                                        }

                                    }
                                }

                                else
                                {
                                    MessageBox.Show("NO RECORD FOUND FOR THIS TYPE");
                                    base.ClearForm(pnlFinanceBooking.Controls);
                                    txtOption.Focus();

                                }



                            }


                           
                        }

                    }
                    else
                    {
                        MessageBox.Show("No Finance Sub. Type is available for this Finance Type...");

                        txtFinanceSubType.Focus();
                        return;
                    }

                }
            }
        }

        public DataTable ShowViewRecord()
        {
          String _FinType;
             String _FinNo;
            DateTime _StartDate=new DateTime();
            DateTime _ExpiryDate = new DateTime();
            DateTime _ExtraTime = new DateTime();
            Double _InstallMentAmt;
            Double _CreditRatio;
            Double _Amt;
            _ExtraTime = DateTime.Now;
            int _ID;

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnalNo.Text);
            param.Add("FN_TYPE", txtFinType.Text);
            DataTable dt = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingViewPage", param);
          

            return dt;
        }

        /// </summary>
        private void CallReport(string FN,string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath=@"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
                //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath=globalPath+FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            //txtDESNAME.Name = "txtDESNAME";
            //txtDESNAME.Text = "";      //":GLOBAL.AUDIT_PATH||FN1";
            //pnl.Controls.Add(txtDESNAME);

            //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            //txtMODE.Name = "txtMODE";
            //txtMODE.Text = "CHARACTER";
            //pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text =this.Now().ToString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "pno";
            txtPNO.Text = txtpersonal.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "fin_No";
            txtfinNo.Text = fnfinNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);

         


            frm.Controls.Add(pnl);
            frm.RptFileName = "Audit02A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }

        private void txtFN_MORTG_PROP_VALVR_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    DateTime SaveDate = new DateTime(1900, 1, 1);
                    if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                        , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {

                            if (dtExtratime.Value == null)
                            {
                                dtExtratime.Value = SaveDate;
                            }


                            //CallReport();
                            txtpersonal.Text = txtPersonnalNo.Text;
                            fnfinNo.Text = txtFinanceNo.Text;
                            CallReport("AUPR", "OLD");

                            base.DoToolbarActions(this.Controls, "Save");
                            //if (this.FunctionConfig.CurrentOption == Function.Add)
                            //{
                            //    SaveFinanceNumber();
                            //}
                           
                            CallReport("AUP0", "NEW");

                            this.Reset();
                            base.ClearForm(pnlFinanceBooking.Controls);
                            this.Cancel();
                            //call save
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {
                            this.Reset();
                            base.ClearForm(pnlFinanceBooking.Controls);
                            txtOption.Focus();

                            return;
                        }
                    }
                }
            }

        }

        private void CustomDelete()
        {
            int ID = 0;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceBookingCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceBookingCommand)this.pnlFinanceBooking.CurrentBusinessEntity;
            ID = ent.ID;
            if (ID>0 )
            {
                //ID = ID;
                colsNVals.Clear();
                colsNVals.Add("ID", ID);

                rsltCode = cmnDM.Execute("CHRIS_SP_Finance_fin_Booking_MANAGER", "Delete", colsNVals);

                if (rsltCode.isSuccessful)
                {


                    MessageBox.Show("Record deleted successfully");
                    return;
                }
            }


        }
        
    }
}