namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_MortgagePropertyCollateralEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_MortgagePropertyCollateralEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlFinBooking = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.DtExpirydt = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtStartDt = new CrplControlLibrary.SLDatePicker(this.components);
            this.LBtnFinNo = new CrplControlLibrary.LookupButton(this.components);
            this.LBtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubDescrp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDescrp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinSubType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.PnlColletral = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVColletral = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.FN_COLLAT_NO = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.FN_COLLAT_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBetween = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_PARTY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_PARTY2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_COLLAT_DT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_DOC_RCV_DT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlFinBooking.SuspendLayout();
            this.PnlColletral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVColletral)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(642, 0);
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(678, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 550);
            this.panel1.Size = new System.Drawing.Size(678, 60);
            // 
            // PnlFinBooking
            // 
            this.PnlFinBooking.ConcurrentPanels = null;
            this.PnlFinBooking.Controls.Add(this.DtExpirydt);
            this.PnlFinBooking.Controls.Add(this.dtStartDt);
            this.PnlFinBooking.Controls.Add(this.LBtnFinNo);
            this.PnlFinBooking.Controls.Add(this.LBtnPersonnel);
            this.PnlFinBooking.Controls.Add(this.label9);
            this.PnlFinBooking.Controls.Add(this.label8);
            this.PnlFinBooking.Controls.Add(this.label7);
            this.PnlFinBooking.Controls.Add(this.label6);
            this.PnlFinBooking.Controls.Add(this.label5);
            this.PnlFinBooking.Controls.Add(this.label4);
            this.PnlFinBooking.Controls.Add(this.label3);
            this.PnlFinBooking.Controls.Add(this.label2);
            this.PnlFinBooking.Controls.Add(this.label1);
            this.PnlFinBooking.Controls.Add(this.txtSubDescrp);
            this.PnlFinBooking.Controls.Add(this.txtDescrp);
            this.PnlFinBooking.Controls.Add(this.txtFinSubType);
            this.PnlFinBooking.Controls.Add(this.txtFinType);
            this.PnlFinBooking.Controls.Add(this.txtFinanceNo);
            this.PnlFinBooking.Controls.Add(this.txtFirstName);
            this.PnlFinBooking.Controls.Add(this.txtPersonnelNo);
            this.PnlFinBooking.DataManager = null;
            this.PnlFinBooking.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlFinBooking.DependentPanels = null;
            this.PnlFinBooking.DisableDependentLoad = false;
            this.PnlFinBooking.EnableDelete = true;
            this.PnlFinBooking.EnableInsert = true;
            this.PnlFinBooking.EnableQuery = false;
            this.PnlFinBooking.EnableUpdate = true;
            this.PnlFinBooking.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand";
            this.PnlFinBooking.Location = new System.Drawing.Point(12, 181);
            this.PnlFinBooking.MasterPanel = null;
            this.PnlFinBooking.Name = "PnlFinBooking";
            this.PnlFinBooking.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlFinBooking.Size = new System.Drawing.Size(654, 187);
            this.PnlFinBooking.SPName = "CHRIS_SP_FIN_BOOKING_MPC_MANAGER";
            this.PnlFinBooking.TabIndex = 0;
            this.PnlFinBooking.TabStop = true;
            // 
            // DtExpirydt
            // 
            this.DtExpirydt.CustomEnabled = false;
            this.DtExpirydt.CustomFormat = "dd/MM/yyyy";
            this.DtExpirydt.DataFieldMapping = "FN_END_DATE";
            this.DtExpirydt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtExpirydt.HasChanges = false;
            this.DtExpirydt.Location = new System.Drawing.Point(399, 95);
            this.DtExpirydt.Name = "DtExpirydt";
            this.DtExpirydt.NullValue = " ";
            this.DtExpirydt.Size = new System.Drawing.Size(100, 20);
            this.DtExpirydt.TabIndex = 20;
            this.DtExpirydt.Value = new System.DateTime(2010, 12, 24, 12, 27, 44, 262);
            // 
            // dtStartDt
            // 
            this.dtStartDt.CustomEnabled = false;
            this.dtStartDt.CustomFormat = "dd/MM/yyyy";
            this.dtStartDt.DataFieldMapping = "FN_START_DATE";
            this.dtStartDt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartDt.HasChanges = false;
            this.dtStartDt.Location = new System.Drawing.Point(139, 96);
            this.dtStartDt.Name = "dtStartDt";
            this.dtStartDt.NullValue = " ";
            this.dtStartDt.Size = new System.Drawing.Size(100, 20);
            this.dtStartDt.TabIndex = 19;
            this.dtStartDt.Value = new System.DateTime(2010, 12, 24, 12, 27, 26, 997);
            // 
            // LBtnFinNo
            // 
            this.LBtnFinNo.ActionLOVExists = "FinNo_MortageProperty_Exists";
            this.LBtnFinNo.ActionType = "FinNo_MortageProperty";
            this.LBtnFinNo.ConditionalFields = "";
            this.LBtnFinNo.CustomEnabled = true;
            this.LBtnFinNo.DataFieldMapping = "";
            this.LBtnFinNo.DependentLovControls = "";
            this.LBtnFinNo.HiddenColumns = "";
            this.LBtnFinNo.Image = ((System.Drawing.Image)(resources.GetObject("LBtnFinNo.Image")));
            this.LBtnFinNo.LoadDependentEntities = true;
            this.LBtnFinNo.Location = new System.Drawing.Point(246, 69);
            this.LBtnFinNo.LookUpTitle = null;
            this.LBtnFinNo.Name = "LBtnFinNo";
            this.LBtnFinNo.Size = new System.Drawing.Size(26, 21);
            this.LBtnFinNo.SkipValidationOnLeave = false;
            this.LBtnFinNo.SPName = "CHRIS_SP_FIN_BOOKING_MPC_MANAGER";
            this.LBtnFinNo.TabIndex = 18;
            this.LBtnFinNo.TabStop = false;
            this.LBtnFinNo.UseVisualStyleBackColor = true;
            this.LBtnFinNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LBtnFinNo_MouseDown);
            // 
            // LBtnPersonnel
            // 
            this.LBtnPersonnel.ActionLOVExists = "PersonnelNo_MortagePropertyExists";
            this.LBtnPersonnel.ActionType = "PersonnelNo_MortageProperty";
            this.LBtnPersonnel.ConditionalFields = "";
            this.LBtnPersonnel.CustomEnabled = true;
            this.LBtnPersonnel.DataFieldMapping = "";
            this.LBtnPersonnel.DependentLovControls = "";
            this.LBtnPersonnel.HiddenColumns = "FirstName|PR_P_NO";
            this.LBtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("LBtnPersonnel.Image")));
            this.LBtnPersonnel.LoadDependentEntities = false;
            this.LBtnPersonnel.Location = new System.Drawing.Point(246, 17);
            this.LBtnPersonnel.LookUpTitle = null;
            this.LBtnPersonnel.Name = "LBtnPersonnel";
            this.LBtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.LBtnPersonnel.SkipValidationOnLeave = false;
            this.LBtnPersonnel.SPName = "CHRIS_SP_FIN_BOOKING_MPC_MANAGER";
            this.LBtnPersonnel.TabIndex = 17;
            this.LBtnPersonnel.TabStop = false;
            this.LBtnPersonnel.UseVisualStyleBackColor = true;
            this.LBtnPersonnel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LBtnPersonnel_MouseDown);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(279, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Sub .Descrip.:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(307, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Descrip.:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(290, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Expiry Date :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "L/Fin Sub Type :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(39, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "L/FinType :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Start Date :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Finance No. :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "First Name :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Personnel No :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSubDescrp
            // 
            this.txtSubDescrp.AllowSpace = true;
            this.txtSubDescrp.AssociatedLookUpName = "";
            this.txtSubDescrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubDescrp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSubDescrp.ContinuationTextBox = null;
            this.txtSubDescrp.CustomEnabled = false;
            this.txtSubDescrp.DataFieldMapping = "W_SUBDESC";
            this.txtSubDescrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubDescrp.GetRecordsOnUpDownKeys = false;
            this.txtSubDescrp.IsDate = false;
            this.txtSubDescrp.Location = new System.Drawing.Point(399, 147);
            this.txtSubDescrp.Name = "txtSubDescrp";
            this.txtSubDescrp.NumberFormat = "###,###,##0.00";
            this.txtSubDescrp.Postfix = "";
            this.txtSubDescrp.Prefix = "";
            this.txtSubDescrp.Size = new System.Drawing.Size(205, 20);
            this.txtSubDescrp.SkipValidation = false;
            this.txtSubDescrp.TabIndex = 25;
            this.txtSubDescrp.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDescrp
            // 
            this.txtDescrp.AllowSpace = true;
            this.txtDescrp.AssociatedLookUpName = "";
            this.txtDescrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescrp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescrp.ContinuationTextBox = null;
            this.txtDescrp.CustomEnabled = false;
            this.txtDescrp.DataFieldMapping = "W_DESC";
            this.txtDescrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescrp.GetRecordsOnUpDownKeys = false;
            this.txtDescrp.IsDate = false;
            this.txtDescrp.Location = new System.Drawing.Point(399, 121);
            this.txtDescrp.Name = "txtDescrp";
            this.txtDescrp.NumberFormat = "###,###,##0.00";
            this.txtDescrp.Postfix = "";
            this.txtDescrp.Prefix = "";
            this.txtDescrp.Size = new System.Drawing.Size(205, 20);
            this.txtDescrp.SkipValidation = false;
            this.txtDescrp.TabIndex = 7;
            this.txtDescrp.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinSubType
            // 
            this.txtFinSubType.AllowSpace = true;
            this.txtFinSubType.AssociatedLookUpName = "";
            this.txtFinSubType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinSubType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinSubType.ContinuationTextBox = null;
            this.txtFinSubType.CustomEnabled = false;
            this.txtFinSubType.DataFieldMapping = "FN_SUBTYPE";
            this.txtFinSubType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinSubType.GetRecordsOnUpDownKeys = false;
            this.txtFinSubType.IsDate = false;
            this.txtFinSubType.Location = new System.Drawing.Point(139, 147);
            this.txtFinSubType.Name = "txtFinSubType";
            this.txtFinSubType.NumberFormat = "###,###,##0.00";
            this.txtFinSubType.Postfix = "";
            this.txtFinSubType.Prefix = "";
            this.txtFinSubType.Size = new System.Drawing.Size(100, 20);
            this.txtFinSubType.SkipValidation = false;
            this.txtFinSubType.TabIndex = 5;
            this.txtFinSubType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinType
            // 
            this.txtFinType.AllowSpace = true;
            this.txtFinType.AssociatedLookUpName = "";
            this.txtFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinType.ContinuationTextBox = null;
            this.txtFinType.CustomEnabled = false;
            this.txtFinType.DataFieldMapping = "FN_TYPE";
            this.txtFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinType.GetRecordsOnUpDownKeys = false;
            this.txtFinType.IsDate = false;
            this.txtFinType.Location = new System.Drawing.Point(139, 121);
            this.txtFinType.Name = "txtFinType";
            this.txtFinType.NumberFormat = "###,###,##0.00";
            this.txtFinType.Postfix = "";
            this.txtFinType.Prefix = "";
            this.txtFinType.Size = new System.Drawing.Size(100, 20);
            this.txtFinType.SkipValidation = false;
            this.txtFinType.TabIndex = 3;
            this.txtFinType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "LBtnFinNo";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = true;
            this.txtFinanceNo.DataFieldMapping = "FN_FIN_NO";
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.IsRequired = true;
            this.txtFinanceNo.Location = new System.Drawing.Point(139, 69);
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(100, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 2;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtFinanceNo, "Press F9 For Finance No. List");
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = false;
            this.txtFirstName.DataFieldMapping = "FirstName";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(139, 43);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(281, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 0;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "LBtnPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(139, 17);
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 1;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPersonnelNo, "Press F9 For Personnel No.  List");
            this.txtPersonnelNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNo_Validating);
            // 
            // PnlColletral
            // 
            this.PnlColletral.ConcurrentPanels = null;
            this.PnlColletral.Controls.Add(this.DGVColletral);
            this.PnlColletral.DataManager = null;
            this.PnlColletral.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlColletral.DependentPanels = null;
            this.PnlColletral.DisableDependentLoad = false;
            this.PnlColletral.EnableDelete = true;
            this.PnlColletral.EnableInsert = true;
            this.PnlColletral.EnableQuery = false;
            this.PnlColletral.EnableUpdate = true;
            this.PnlColletral.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINBookCollatCommand";
            this.PnlColletral.Location = new System.Drawing.Point(12, 381);
            this.PnlColletral.MasterPanel = null;
            this.PnlColletral.Name = "PnlColletral";
            this.PnlColletral.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlColletral.Size = new System.Drawing.Size(654, 162);
            this.PnlColletral.SPName = "CHRIS_SP_FIN_BOOK_COLLAT_MANAGER";
            this.PnlColletral.TabIndex = 1;
            // 
            // DGVColletral
            // 
            this.DGVColletral.AllowUserToResizeColumns = false;
            this.DGVColletral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVColletral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FN_COLLAT_NO,
            this.FN_COLLAT_DESC,
            this.colBetween,
            this.FN_PARTY1,
            this.colAnd,
            this.FN_PARTY2,
            this.colDated,
            this.FN_COLLAT_DT,
            this.FN_DOC_RCV_DT,
            this.ID});
            this.DGVColletral.ColumnToHide = null;
            this.DGVColletral.ColumnWidth = null;
            this.DGVColletral.CustomEnabled = true;
            this.DGVColletral.DisplayColumnWrapper = null;
            this.DGVColletral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVColletral.GridDefaultRow = 0;
            this.DGVColletral.Location = new System.Drawing.Point(0, 0);
            this.DGVColletral.Name = "DGVColletral";
            this.DGVColletral.ReadOnlyColumns = null;
            this.DGVColletral.RequiredColumns = "COLLETRAL DATE,DOC. RECEIVE DATE";
            this.DGVColletral.Size = new System.Drawing.Size(654, 162);
            this.DGVColletral.SkippingColumns = null;
            this.DGVColletral.TabIndex = 3;
            this.DGVColletral.Enter += new System.EventHandler(this.DGVColletral_Enter);
            this.DGVColletral.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGVColletral_RowValidating);
            this.DGVColletral.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVColletral_CellValidating);
            this.DGVColletral.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGVColletral_RowsAdded);
            // 
            // FN_COLLAT_NO
            // 
            this.FN_COLLAT_NO.ActionLOV = "Lov_list";
            this.FN_COLLAT_NO.ActionLOVExists = "Lov_listExists";
            this.FN_COLLAT_NO.AttachParentEntity = false;
            this.FN_COLLAT_NO.DataPropertyName = "FN_COLLAT_NO";
            this.FN_COLLAT_NO.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINBookCollatCommand";
            this.FN_COLLAT_NO.HeaderText = "Collateral Code";
            this.FN_COLLAT_NO.LookUpTitle = null;
            this.FN_COLLAT_NO.LOVFieldMapping = "FN_COLLAT_NO";
            this.FN_COLLAT_NO.Name = "FN_COLLAT_NO";
            this.FN_COLLAT_NO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FN_COLLAT_NO.SearchColumn = "FN_COLLAT_NO";
            this.FN_COLLAT_NO.SkipValidationOnLeave = false;
            this.FN_COLLAT_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FN_COLLAT_NO.SpName = "CHRIS_SP_FIN_BOOK_COLLAT_MANAGER";
            // 
            // FN_COLLAT_DESC
            // 
            this.FN_COLLAT_DESC.DataPropertyName = "FN_COLLAT_DESC";
            this.FN_COLLAT_DESC.HeaderText = "Collateral Description";
            this.FN_COLLAT_DESC.MaxInputLength = 30;
            this.FN_COLLAT_DESC.Name = "FN_COLLAT_DESC";
            this.FN_COLLAT_DESC.ReadOnly = true;
            this.FN_COLLAT_DESC.Width = 90;
            // 
            // colBetween
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            this.colBetween.DefaultCellStyle = dataGridViewCellStyle1;
            this.colBetween.HeaderText = "";
            this.colBetween.Name = "colBetween";
            this.colBetween.ReadOnly = true;
            this.colBetween.Width = 60;
            // 
            // FN_PARTY1
            // 
            this.FN_PARTY1.DataPropertyName = "FN_PARTY1";
            this.FN_PARTY1.HeaderText = "Party 1";
            this.FN_PARTY1.MaxInputLength = 30;
            this.FN_PARTY1.Name = "FN_PARTY1";
            this.FN_PARTY1.Width = 94;
            // 
            // colAnd
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            this.colAnd.DefaultCellStyle = dataGridViewCellStyle2;
            this.colAnd.HeaderText = "";
            this.colAnd.Name = "colAnd";
            this.colAnd.ReadOnly = true;
            this.colAnd.Width = 42;
            // 
            // FN_PARTY2
            // 
            this.FN_PARTY2.DataPropertyName = "FN_PARTY2";
            this.FN_PARTY2.HeaderText = "Party 2";
            this.FN_PARTY2.MaxInputLength = 30;
            this.FN_PARTY2.Name = "FN_PARTY2";
            this.FN_PARTY2.Width = 94;
            // 
            // colDated
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            this.colDated.DefaultCellStyle = dataGridViewCellStyle3;
            this.colDated.HeaderText = "";
            this.colDated.Name = "colDated";
            this.colDated.ReadOnly = true;
            this.colDated.Width = 42;
            // 
            // FN_COLLAT_DT
            // 
            this.FN_COLLAT_DT.DataPropertyName = "FN_COLLAT_DT";
            dataGridViewCellStyle4.NullValue = null;
            this.FN_COLLAT_DT.DefaultCellStyle = dataGridViewCellStyle4;
            this.FN_COLLAT_DT.HeaderText = "Collateral Date";
            this.FN_COLLAT_DT.MaxInputLength = 10;
            this.FN_COLLAT_DT.Name = "FN_COLLAT_DT";
            // 
            // FN_DOC_RCV_DT
            // 
            this.FN_DOC_RCV_DT.DataPropertyName = "FN_DOC_RCV_DT";
            dataGridViewCellStyle5.NullValue = null;
            this.FN_DOC_RCV_DT.DefaultCellStyle = dataGridViewCellStyle5;
            this.FN_DOC_RCV_DT.HeaderText = "Doc. Receive Date";
            this.FN_DOC_RCV_DT.MaxInputLength = 10;
            this.FN_DOC_RCV_DT.Name = "FN_DOC_RCV_DT";
            this.FN_DOC_RCV_DT.Width = 98;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label10);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 84);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(654, 91);
            this.pnlHead.TabIndex = 52;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(183, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(297, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "MORTGAGE PROPERTY COLLATERAL ENTRY ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(180, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "FINANCE SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(531, 60);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(531, 34);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(488, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(480, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(12, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(450, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(127, 13);
            this.label11.TabIndex = 53;
            this.label11.Text = "User Name  : CHRISTOP";
            // 
            // CHRIS_Finance_MortgagePropertyCollateralEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 610);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlColletral);
            this.Controls.Add(this.PnlFinBooking);
            this.CurrentPanelBlock = "PnlFinBooking";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.F3OptionText = "";
            this.Name = "CHRIS_Finance_MortgagePropertyCollateralEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF5Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Mortgage Property Collateral Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_MortgagePropertyCollateralEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlFinBooking, 0);
            this.Controls.SetChildIndex(this.PnlColletral, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlFinBooking.ResumeLayout(false);
            this.PnlFinBooking.PerformLayout();
            this.PnlColletral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVColletral)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlFinBooking;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtSubDescrp;
        private CrplControlLibrary.SLTextBox txtDescrp;
        private CrplControlLibrary.SLTextBox txtFinSubType;
        private CrplControlLibrary.SLTextBox txtFinType;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.LookupButton LBtnPersonnel;
        private CrplControlLibrary.LookupButton LBtnFinNo;
        private CrplControlLibrary.SLDatePicker DtExpirydt;
        private CrplControlLibrary.SLDatePicker dtStartDt;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlColletral;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVColletral;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn FN_COLLAT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_COLLAT_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBetween;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_PARTY1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_PARTY2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDated;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_COLLAT_DT;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_DOC_RCV_DT;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}