using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;

using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinLiquidationEntry : ChrisMasterDetailForm
    {
        private Double Sym1;
        private Double Sym2;
        private Double Sym3;
        private DateTime lPayGenDate;
        private Double lFnCredt;
        private Double lfnMonthMarkup;
        private Double lMarkup;
        private Double lFnInstRec;
        private Double lInstLeft;
        private Double lfnMarkupRecv;
        public int MARKUP_RATE_DAYS = -1;
        public CHRIS_Finance_FinLiquidationEntry()
        {
            InitializeComponent();
        }


        public CHRIS_Finance_FinLiquidationEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlTblDept);
            this.pnlFinanceLiquidation.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlFinanceLiquidation);
            // this.DefaultDataBlock = pnlTblBenchmark;
            this.CurrentPanelBlock = "pnlFinanceLiquidation";
        }

        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtAdd.Enabled = false;
            tbtDelete.Enabled = false;
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtAdd.Visible = false;
            tbtDelete.Visible = false;

            this.txtUserName.Text = "User Name: "+ this.UserName;
            this.PR_Segment.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);

            this.PR_Dept.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.Cont.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);





        }

        protected override bool Add()
        {


            base.Add();
        
            this.FunctionConfig.CurrentOption = Function.Modify;

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;
        }

        protected override bool Edit()
        {
            base.View();
            this.FunctionConfig.CurrentOption = Function.View;
            this.txtOption.Text = "V";
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
            return false;
        }
         
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.txtPersonnalNo.Text = "";
                this.txtFinType.Text = "";
            }
            if (actionType == "Save")
            {
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }
     
        
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }

        private void dtAdjustment_Validating(object sender, CancelEventArgs e)
        {
            double val3 = 0;
            if(dtAdjustment.Value != null)
            {
                   DateTime Date1= Convert.ToDateTime(dtAdjustment.Value);
                  DateTime Date2 = Convert.ToDateTime(DTPAY_GEN_DATE.Value);

                if(DateTime.Compare(Date1, Date2) <= 0)
                {
                      MessageBox.Show("DATE HAS TO BE THE DATE AFTER PAYROLL .....");
                    dtAdjustment.Focus();
                    e.Cancel = true;
                    return;
                }
                else
                {
                    System.TimeSpan diffResult = Date1.Subtract(Date2);
                    val3 = Math.Round(diffResult.TotalDays);
                    val3 = val3 + 1;
                    lMarkup = double.Parse(txtfnMarkup.Text == "" ? "0" : txtfnMarkup.Text);
                    lFnCredt = double.Parse(txtfnCredit.Text == "" ? "0" : txtfnCredit.Text);
                    lfnMonthMarkup=(lFnCredt * (val3 / 100 ) * lMarkup) /MARKUP_RATE_DAYS;
                    lfnMonthMarkup = Math.Round(lfnMonthMarkup, 8);
                    txtMarkupLeft.Text = lfnMonthMarkup.ToString();
                    Dictionary<string, object> param5 = new Dictionary<string, object>();
                    param5.Add("FN_FIN_NO", txtFinanceNo.Text);
                    param5.Add("PR_P_NO", txtPersonnalNo.Text);
                    param5.Add("FN_TYPE", txtFinType.Text);
                    DataTable dt6 = this.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinMarkUpReceived", param5);
                    if (dt6 != null)
                    {
                        if (dt6.Rows.Count > 0)
                        {
                            lfnMarkupRecv = double.Parse(dt6.Rows[0].ItemArray[0].ToString() == "" ? "0" : dt6.Rows[0].ItemArray[0].ToString());
                            txtMarkUpRec.Text = lfnMarkupRecv.ToString();
                        }
                    }

                    DateTime SaveDate = new DateTime(1900, 1, 1);
                    if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                        , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {

                            if (dtExtratime.Value == null)
                            {
                                dtExtratime.Value = SaveDate;
                            }
                            
                            txtpersonal.Text = txtPersonnalNo.Text;
                            fnfinNo.Text = txtFinanceNo.Text;
                            CallReport("AUPR", "OLD");

                            base.DoToolbarActions(this.Controls, "Save");

                            CallReport("AUP0", "NEW");
                            base.DoToolbarActions(this.Controls, "Cancel");
                            base.ClearForm(this.Controls);

                            
                            //call save
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {

                            this.Cancel();
                            base.ClearForm(this.Controls);
                            txtOption.Focus();

                            return;
                        }
                    }



                }

            }


            else
            {
                 MessageBox.Show("DATE HAS TO BE THE DATE AFTER PAYROLL .....");
                    dtAdjustment.Focus();
                    e.Cancel = true;
                    return;
            }
           
   
        }

      
        private void txtFinType_Validating(object sender, CancelEventArgs e)
        {
            if(txtFinType.Text !=string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                 param.Add("PR_P_NO", txtPersonnalNo.Text);
                 param.Add("FN_FIN_NO", txtFinanceNo.Text);
                 DataTable dt = this.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinFnMonthQuery", param);
                 if (dt != null)
                 {
                     if (dt.Rows.Count > 0)
                     {
                        bool aSym1=Double.TryParse(dt.Rows[0].ItemArray[0].ToString(), out Sym1);
                         bool aSym2=Double.TryParse(dt.Rows[0].ItemArray[1].ToString(), out Sym2);
                         bool aSym3=Double.TryParse(dt.Rows[0].ItemArray[2].ToString(), out Sym3);
                        if(this.FunctionConfig.CurrentOption== Function.View)
                        {
                            if(Sym1 == 0 || Sym2 != 0 || Sym3 != 0)  
                            {
                                MessageBox.Show(" ADJUSTMENT HAS NOT BEEN DONE LATELY");
                             
                                e.Cancel = true;
                                txtOption.Focus();
                                return;
                            }
                        }
                        if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit)
                            {
                              if (Sym1 == 0 || Sym2 == 0 || Sym3 !=0)
                              {

                                MessageBox.Show("WARNING:PAYROLL HAS NOT BEEN GENERATED AFTER LAST ACTION ON THIS LOAN");
                               
                               
                             

                              }

                            }
                     }


                 }

                 if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit)
                 {

                     Dictionary<string, object> param2 = new Dictionary<string, object>();
                     param2.Add("FN_FIN_NO", txtFinanceNo.Text);
                     DataTable dt2 = this.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinPayGenDate", param2);
                     if (dt2 != null)
                     {
                         if (dt2.Rows.Count > 0)
                         {
                             if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                             {
                                 if (DateTime.TryParse(dt2.Rows[0].ItemArray[0].ToString(), out lPayGenDate))
                                 {
                                     DTPAY_GEN_DATE.Value = lPayGenDate;
                                 }
                             }

                         }

                     }

                     Dictionary<string, object> param1 = new Dictionary<string, object>();
                     param1.Add("FN_FIN_NO", txtFinanceNo.Text);
                     param1.Add("PR_P_NO", txtPersonnalNo.Text);
                     param1.Add("FN_TYPE", txtFinType.Text);
                     DataTable dt1 = this.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinMonthValues", param1);
                     DataTable dtInstallments = this.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "TotalInstallments", param1);
                     if (dt1 != null)
                     {
                         Double Installments = 0;
                         if (dtInstallments != null)
                         {
                             if (dtInstallments.Rows.Count > 0)
                             {
                                double.TryParse( dtInstallments.Rows[0]["FN_PAY_SCHED"].ToString(),out Installments);
                             }
                         }


                         if (dt1.Rows.Count > 0)
                         {

                             //lFnMdate = null;
                           
                             txtfndebit.Text = "0";
                             if (dt1.Rows[0]["lFN_CREDIT"].ToString() != string.Empty)
                             {
                                 txtfnCredit.Text = double.Parse(dt1.Rows[0]["lFN_CREDIT"].ToString()).ToString();
                             }
                             txtBal.Text = "0";
                             if (dt1.Rows[0]["lINST_LEFT"].ToString() != string.Empty)
                             {
                                 lInstLeft = double.Parse(dt1.Rows[0]["lINST_LEFT"].ToString() == "" ? "0" : dt1.Rows[0]["lINST_LEFT"].ToString());
                             }
                             txtInstLeft.Text = lInstLeft.ToString();
                             if (txtInstRec.Text != string.Empty)
                             {
                                 lFnInstRec = double.Parse(txtInstRec.Text == "" ? "0" : txtInstRec.Text);

                                 lFnInstRec = lFnInstRec - lInstLeft;
                                // lFnInstRec = Installments - lInstLeft;
                             }
                             lFnInstRec = Installments - lInstLeft;
                             txtInstRec.Text = lFnInstRec.ToString();

                         }

                     }

                     txtfnliqFlag.Text = "L";




                 }
                 else
                 {

                 }

                }

            }
                
        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


        

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "PNO";
            txtPNO.Text = txtpersonal.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "FIN_NO";
            txtfinNo.Text = fnfinNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "USER";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "STATUS";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT05A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }


    }

      


      

        

      
}
      
  
