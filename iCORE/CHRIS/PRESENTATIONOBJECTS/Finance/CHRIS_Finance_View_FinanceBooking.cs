using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;

using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_View_FinanceBooking : ChrisSimpleForm
    {

        private CHRIS_Finance_FinanceBookingEntry _mainForm = null;
        DataTable dtViewDeleteMode = null;
        public CHRIS_Finance_View_FinanceBooking()
        {
            InitializeComponent();


         
        }

       
         public CHRIS_Finance_View_FinanceBooking(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Finance_FinanceBookingEntry mainForm)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            this._mainForm = mainForm;
            dtViewDeleteMode=_mainForm.ShowViewRecord();

          

        }
        public void setVal()
        {
            Double MonthlyDeduction = 0.0;
            Double CRatioPer = 0.0;
            Double AmtAvailed = 0.0;
            if (dtViewDeleteMode != null)
            {
                if (dtViewDeleteMode.Rows.Count > 0)
                {
                    this.txtFinType.Text = dtViewDeleteMode.Rows[0]["FN_TYPE"].ToString();
                    this.txtFinanceNo.Text = dtViewDeleteMode.Rows[0]["FN_FINANCE_NO"].ToString();
                    if (dtViewDeleteMode.Rows[0]["FN_START_DATE"].ToString() != string.Empty)
                    {
                        this.DTstartdate.Value = DateTime.Parse(dtViewDeleteMode.Rows[0]["FN_START_DATE"].ToString());
                    }
                    if (dtViewDeleteMode.Rows[0]["FN_END_DATE"].ToString() != string.Empty)
                    {
                        this.DTFN_END_DATE.Value = Convert.ToDateTime(dtViewDeleteMode.Rows[0]["FN_END_DATE"]);
                    }
                    if (dtViewDeleteMode.Rows[0]["FN_EXTRA_TIME"].ToString() != string.Empty)
                    {
                        this.dtExtratime.Value = Convert.ToDateTime(dtViewDeleteMode.Rows[0]["FN_EXTRA_TIME"]);

                    }
                    else
                    {
                        this.dtExtratime.Value = null;
                    }

                    Double.TryParse(dtViewDeleteMode.Rows[0]["FN_MONTHLY_DED"].ToString(), out MonthlyDeduction);
                     this.txtInstallment.Text = MonthlyDeduction.ToString();
                     Double.TryParse(dtViewDeleteMode.Rows[0]["FN_C_RATIO_PER"].ToString(), out CRatioPer);
                     this.txtCreditRatio.Text = CRatioPer.ToString();
                     Double.TryParse(dtViewDeleteMode.Rows[0]["FN_AMT_AVAILED"].ToString(), out AmtAvailed);
                    this.txtaviledamt.Text = AmtAvailed.ToString(); ;
                  //  _ID = Convert.ToInt32(dt.Rows[0]["ID"]);

                }
            }
        }
         protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = false;
            tbtDelete.Enabled = false;
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtCancel.Visible = false;
            tbtDelete.Visible = false;
            tbtAdd.Visible=false;
            tbtClose.Visible=false;
            ShowOptionTextBox = false;
            setVal();
           
          
            ShowStatusBar = false;
            ShowBottomBar = false;

            //this.txtFinType.Text = _FinType;
            //this.txtFinanceNo.Text = _FinNo;
            //this.DTstartdate.Value = _StartDate;
            //this.DTFN_END_DATE.Value = _ExpiryDate;
            //this.dtExtratime.Value = _ExtraTime;
            //this.txtCreditRatio.Text = _CreditRatio.ToString();
            //this.txtaviledamt.Text = _Amt.ToString();
            //this.txtInstallment.Text = _InstallMentAmt.ToString();
          

        }

        private void CHRIS_Finance_View_FinanceBooking_Shown(object sender, EventArgs e)
        {
            if (_mainForm.txtOption.Text == "V")
            {
                _mainForm.ShowViewMsg(this);
            }
            else if(_mainForm.txtOption.Text=="D")
            {
                _mainForm.ShowDeleteMsg(this);
            }
        }

    
     
    }
}