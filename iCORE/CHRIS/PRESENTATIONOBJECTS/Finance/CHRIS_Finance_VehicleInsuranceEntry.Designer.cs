namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_VehicleInsuranceEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_VehicleInsuranceEntry));
            this.pnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtPisssuedate = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpexpirydt = new CrplControlLibrary.SLDatePicker(this.components);
            this.LBFin = new CrplControlLibrary.LookupButton(this.components);
            this.LBPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.txtVehicleMedia = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFirstName1 = new System.Windows.Forms.Label();
            this.txtPersonnelNo1 = new System.Windows.Forms.Label();
            this.txtdescrip = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVehMake = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVehicleChasisNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVehicleEngineNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtVehicleRegistrationNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInsuranceCompnay = new CrplControlLibrary.SLTextBox(this.components);
            this.txtInsurancePolicyNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinanaceType = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlPersonnel.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(425, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Location = new System.Drawing.Point(75, 8);
            this.pnlBottom.Size = new System.Drawing.Size(461, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 535);
            this.panel1.Size = new System.Drawing.Size(634, 72);
            // 
            // pnlPersonnel
            // 
            this.pnlPersonnel.ConcurrentPanels = null;
            this.pnlPersonnel.Controls.Add(this.dtPisssuedate);
            this.pnlPersonnel.Controls.Add(this.dtpexpirydt);
            this.pnlPersonnel.Controls.Add(this.LBFin);
            this.pnlPersonnel.Controls.Add(this.LBPersonnel);
            this.pnlPersonnel.Controls.Add(this.txtVehicleMedia);
            this.pnlPersonnel.Controls.Add(this.label1);
            this.pnlPersonnel.Controls.Add(this.label13);
            this.pnlPersonnel.Controls.Add(this.label12);
            this.pnlPersonnel.Controls.Add(this.label11);
            this.pnlPersonnel.Controls.Add(this.label10);
            this.pnlPersonnel.Controls.Add(this.label9);
            this.pnlPersonnel.Controls.Add(this.label8);
            this.pnlPersonnel.Controls.Add(this.label7);
            this.pnlPersonnel.Controls.Add(this.label6);
            this.pnlPersonnel.Controls.Add(this.label5);
            this.pnlPersonnel.Controls.Add(this.label4);
            this.pnlPersonnel.Controls.Add(this.label2);
            this.pnlPersonnel.Controls.Add(this.txtFirstName1);
            this.pnlPersonnel.Controls.Add(this.txtPersonnelNo1);
            this.pnlPersonnel.Controls.Add(this.txtdescrip);
            this.pnlPersonnel.Controls.Add(this.txtVehMake);
            this.pnlPersonnel.Controls.Add(this.txtVehicleChasisNo);
            this.pnlPersonnel.Controls.Add(this.txtVehicleEngineNo);
            this.pnlPersonnel.Controls.Add(this.txtVehicleRegistrationNo);
            this.pnlPersonnel.Controls.Add(this.txtInsuranceCompnay);
            this.pnlPersonnel.Controls.Add(this.txtInsurancePolicyNo);
            this.pnlPersonnel.Controls.Add(this.txtFinanaceType);
            this.pnlPersonnel.Controls.Add(this.txtFinanceNo);
            this.pnlPersonnel.Controls.Add(this.txtFirstName);
            this.pnlPersonnel.Controls.Add(this.txtPersonnelNo);
            this.pnlPersonnel.DataManager = null;
            this.pnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonnel.DependentPanels = null;
            this.pnlPersonnel.DisableDependentLoad = false;
            this.pnlPersonnel.EnableDelete = true;
            this.pnlPersonnel.EnableInsert = true;
            this.pnlPersonnel.EnableQuery = false;
            this.pnlPersonnel.EnableUpdate = true;
            this.pnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINInsuranceCommand";
            this.pnlPersonnel.Location = new System.Drawing.Point(13, 195);
            this.pnlPersonnel.MasterPanel = null;
            this.pnlPersonnel.Name = "pnlPersonnel";
            this.pnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonnel.Size = new System.Drawing.Size(609, 334);
            this.pnlPersonnel.SPName = "CHRIS_SP_FIN_INSURANCE_MANAGER";
            this.pnlPersonnel.TabIndex = 10;
            // 
            // dtPisssuedate
            // 
            this.dtPisssuedate.CustomEnabled = true;
            this.dtPisssuedate.CustomFormat = "dd/MM/yyyy";
            this.dtPisssuedate.DataFieldMapping = "FN_POLICY_ISS_DATE";
            this.dtPisssuedate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPisssuedate.HasChanges = true;
            this.dtPisssuedate.IsRequired = true;
            this.dtPisssuedate.Location = new System.Drawing.Point(194, 171);
            this.dtPisssuedate.Name = "dtPisssuedate";
            this.dtPisssuedate.NullValue = " ";
            this.dtPisssuedate.Size = new System.Drawing.Size(100, 20);
            this.dtPisssuedate.TabIndex = 7;
            this.dtPisssuedate.Value = new System.DateTime(2010, 12, 14, 16, 31, 6, 328);
            // 
            // dtpexpirydt
            // 
            this.dtpexpirydt.CustomEnabled = true;
            this.dtpexpirydt.CustomFormat = "dd/MM/yyyy";
            this.dtpexpirydt.DataFieldMapping = "FN_POLICY_EXP_DATE";
            this.dtpexpirydt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpexpirydt.HasChanges = true;
            this.dtpexpirydt.IsRequired = true;
            this.dtpexpirydt.Location = new System.Drawing.Point(458, 170);
            this.dtpexpirydt.Name = "dtpexpirydt";
            this.dtpexpirydt.NullValue = " ";
            this.dtpexpirydt.Size = new System.Drawing.Size(113, 20);
            this.dtpexpirydt.TabIndex = 8;
            this.dtpexpirydt.Value = new System.DateTime(2010, 12, 14, 13, 10, 11, 0);
            this.dtpexpirydt.Validating += new System.ComponentModel.CancelEventHandler(this.slDatePicker1_Validating);
            // 
            // LBFin
            // 
            this.LBFin.ActionLOVExists = "Fin_LovExists";
            this.LBFin.ActionType = "Fin_Lov";
            this.LBFin.ConditionalFields = "";
            this.LBFin.CustomEnabled = true;
            this.LBFin.DataFieldMapping = "";
            this.LBFin.DependentLovControls = "";
            this.LBFin.HiddenColumns = "";
            this.LBFin.Image = ((System.Drawing.Image)(resources.GetObject("LBFin.Image")));
            this.LBFin.LoadDependentEntities = false;
            this.LBFin.Location = new System.Drawing.Point(325, 64);
            this.LBFin.LookUpTitle = null;
            this.LBFin.Name = "LBFin";
            this.LBFin.Size = new System.Drawing.Size(26, 21);
            this.LBFin.SkipValidationOnLeave = false;
            this.LBFin.SPName = "CHRIS_SP_FIN_INSURANCE_MANAGER";
            this.LBFin.TabIndex = 37;
            this.LBFin.TabStop = false;
            this.LBFin.UseVisualStyleBackColor = true;
            // 
            // LBPersonnel
            // 
            this.LBPersonnel.ActionLOVExists = "PR_No_LovExist";
            this.LBPersonnel.ActionType = "PR_No_Lov";
            this.LBPersonnel.ConditionalFields = "";
            this.LBPersonnel.CustomEnabled = true;
            this.LBPersonnel.DataFieldMapping = "";
            this.LBPersonnel.DependentLovControls = "";
            this.LBPersonnel.HiddenColumns = "";
            this.LBPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("LBPersonnel.Image")));
            this.LBPersonnel.LoadDependentEntities = false;
            this.LBPersonnel.Location = new System.Drawing.Point(303, 10);
            this.LBPersonnel.LookUpTitle = null;
            this.LBPersonnel.Name = "LBPersonnel";
            this.LBPersonnel.Size = new System.Drawing.Size(26, 21);
            this.LBPersonnel.SkipValidationOnLeave = false;
            this.LBPersonnel.SPName = "CHRIS_SP_FIN_INSURANCE_MANAGER";
            this.LBPersonnel.TabIndex = 36;
            this.LBPersonnel.TabStop = false;
            this.LBPersonnel.UseVisualStyleBackColor = true;
            // 
            // txtVehicleMedia
            // 
            this.txtVehicleMedia.AllowSpace = true;
            this.txtVehicleMedia.AssociatedLookUpName = "";
            this.txtVehicleMedia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicleMedia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehicleMedia.ContinuationTextBox = null;
            this.txtVehicleMedia.CustomEnabled = true;
            this.txtVehicleMedia.DataFieldMapping = "FN_VEHC_MODEL";
            this.txtVehicleMedia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicleMedia.GetRecordsOnUpDownKeys = false;
            this.txtVehicleMedia.IsDate = false;
            this.txtVehicleMedia.Location = new System.Drawing.Point(194, 300);
            this.txtVehicleMedia.MaxLength = 10;
            this.txtVehicleMedia.Name = "txtVehicleMedia";
            this.txtVehicleMedia.NumberFormat = "###,###,##0.00";
            this.txtVehicleMedia.Postfix = "";
            this.txtVehicleMedia.Prefix = "";
            this.txtVehicleMedia.Size = new System.Drawing.Size(100, 20);
            this.txtVehicleMedia.SkipValidation = false;
            this.txtVehicleMedia.TabIndex = 13;
            this.txtVehicleMedia.TextType = CrplControlLibrary.TextType.String;
            this.txtVehicleMedia.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtVehicleMedia_PreviewKeyDown);
            this.txtVehicleMedia.Leave += new System.EventHandler(this.txtVehicleMedia_Leave);
            this.txtVehicleMedia.Validating += new System.ComponentModel.CancelEventHandler(this.txtVehicleMedia_Validating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Vehicle Model :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(329, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Policy Expiry Date :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(318, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Descrip :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(39, 276);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Vehicle Make :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(40, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Vehicle Chasis No. :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(40, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Vehicle Engine No. :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(40, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Vehicle Registration No. :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Policy Issue Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Insurance Compnay :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Insurance Policy No. :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Finanace Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Finance No. :";
            // 
            // txtFirstName1
            // 
            this.txtFirstName1.AutoSize = true;
            this.txtFirstName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName1.Location = new System.Drawing.Point(40, 47);
            this.txtFirstName1.Name = "txtFirstName1";
            this.txtFirstName1.Size = new System.Drawing.Size(75, 13);
            this.txtFirstName1.TabIndex = 22;
            this.txtFirstName1.Text = "First Name :";
            // 
            // txtPersonnelNo1
            // 
            this.txtPersonnelNo1.AutoSize = true;
            this.txtPersonnelNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo1.Location = new System.Drawing.Point(40, 19);
            this.txtPersonnelNo1.Name = "txtPersonnelNo1";
            this.txtPersonnelNo1.Size = new System.Drawing.Size(95, 13);
            this.txtPersonnelNo1.TabIndex = 21;
            this.txtPersonnelNo1.Text = "Personnel No. :";
            // 
            // txtdescrip
            // 
            this.txtdescrip.AllowSpace = true;
            this.txtdescrip.AssociatedLookUpName = "";
            this.txtdescrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdescrip.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescrip.ContinuationTextBox = null;
            this.txtdescrip.CustomEnabled = false;
            this.txtdescrip.DataFieldMapping = "";
            this.txtdescrip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescrip.GetRecordsOnUpDownKeys = false;
            this.txtdescrip.IsDate = false;
            this.txtdescrip.Location = new System.Drawing.Point(382, 94);
            this.txtdescrip.Name = "txtdescrip";
            this.txtdescrip.NumberFormat = "###,###,##0.00";
            this.txtdescrip.Postfix = "";
            this.txtdescrip.Prefix = "";
            this.txtdescrip.Size = new System.Drawing.Size(192, 20);
            this.txtdescrip.SkipValidation = false;
            this.txtdescrip.TabIndex = 4;
            this.txtdescrip.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtVehMake
            // 
            this.txtVehMake.AllowSpace = true;
            this.txtVehMake.AssociatedLookUpName = "";
            this.txtVehMake.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehMake.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehMake.ContinuationTextBox = null;
            this.txtVehMake.CustomEnabled = true;
            this.txtVehMake.DataFieldMapping = "FN_VEHC_MAKE";
            this.txtVehMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehMake.GetRecordsOnUpDownKeys = false;
            this.txtVehMake.IsDate = false;
            this.txtVehMake.Location = new System.Drawing.Point(194, 274);
            this.txtVehMake.MaxLength = 30;
            this.txtVehMake.Name = "txtVehMake";
            this.txtVehMake.NumberFormat = "###,###,##0.00";
            this.txtVehMake.Postfix = "";
            this.txtVehMake.Prefix = "";
            this.txtVehMake.Size = new System.Drawing.Size(380, 20);
            this.txtVehMake.SkipValidation = false;
            this.txtVehMake.TabIndex = 12;
            this.txtVehMake.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtVehicleChasisNo
            // 
            this.txtVehicleChasisNo.AllowSpace = true;
            this.txtVehicleChasisNo.AssociatedLookUpName = "";
            this.txtVehicleChasisNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicleChasisNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehicleChasisNo.ContinuationTextBox = null;
            this.txtVehicleChasisNo.CustomEnabled = true;
            this.txtVehicleChasisNo.DataFieldMapping = "FN_VEHC_CHS_NO";
            this.txtVehicleChasisNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicleChasisNo.GetRecordsOnUpDownKeys = false;
            this.txtVehicleChasisNo.IsDate = false;
            this.txtVehicleChasisNo.Location = new System.Drawing.Point(194, 248);
            this.txtVehicleChasisNo.MaxLength = 30;
            this.txtVehicleChasisNo.Name = "txtVehicleChasisNo";
            this.txtVehicleChasisNo.NumberFormat = "###,###,##0.00";
            this.txtVehicleChasisNo.Postfix = "";
            this.txtVehicleChasisNo.Prefix = "";
            this.txtVehicleChasisNo.Size = new System.Drawing.Size(380, 20);
            this.txtVehicleChasisNo.SkipValidation = false;
            this.txtVehicleChasisNo.TabIndex = 11;
            this.txtVehicleChasisNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtVehicleEngineNo
            // 
            this.txtVehicleEngineNo.AllowSpace = true;
            this.txtVehicleEngineNo.AssociatedLookUpName = "";
            this.txtVehicleEngineNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicleEngineNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehicleEngineNo.ContinuationTextBox = null;
            this.txtVehicleEngineNo.CustomEnabled = true;
            this.txtVehicleEngineNo.DataFieldMapping = "FN_VEHC_ENG_NO";
            this.txtVehicleEngineNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicleEngineNo.GetRecordsOnUpDownKeys = false;
            this.txtVehicleEngineNo.IsDate = false;
            this.txtVehicleEngineNo.Location = new System.Drawing.Point(194, 222);
            this.txtVehicleEngineNo.MaxLength = 30;
            this.txtVehicleEngineNo.Name = "txtVehicleEngineNo";
            this.txtVehicleEngineNo.NumberFormat = "###,###,##0.00";
            this.txtVehicleEngineNo.Postfix = "";
            this.txtVehicleEngineNo.Prefix = "";
            this.txtVehicleEngineNo.Size = new System.Drawing.Size(380, 20);
            this.txtVehicleEngineNo.SkipValidation = false;
            this.txtVehicleEngineNo.TabIndex = 10;
            this.txtVehicleEngineNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtVehicleRegistrationNo
            // 
            this.txtVehicleRegistrationNo.AllowSpace = true;
            this.txtVehicleRegistrationNo.AssociatedLookUpName = "";
            this.txtVehicleRegistrationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicleRegistrationNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVehicleRegistrationNo.ContinuationTextBox = null;
            this.txtVehicleRegistrationNo.CustomEnabled = true;
            this.txtVehicleRegistrationNo.DataFieldMapping = "FN_VEHC_REG_NO";
            this.txtVehicleRegistrationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicleRegistrationNo.GetRecordsOnUpDownKeys = false;
            this.txtVehicleRegistrationNo.IsDate = false;
            this.txtVehicleRegistrationNo.Location = new System.Drawing.Point(194, 196);
            this.txtVehicleRegistrationNo.MaxLength = 10;
            this.txtVehicleRegistrationNo.Name = "txtVehicleRegistrationNo";
            this.txtVehicleRegistrationNo.NumberFormat = "###,###,##0.00";
            this.txtVehicleRegistrationNo.Postfix = "";
            this.txtVehicleRegistrationNo.Prefix = "";
            this.txtVehicleRegistrationNo.Size = new System.Drawing.Size(100, 20);
            this.txtVehicleRegistrationNo.SkipValidation = false;
            this.txtVehicleRegistrationNo.TabIndex = 9;
            this.txtVehicleRegistrationNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtInsuranceCompnay
            // 
            this.txtInsuranceCompnay.AllowSpace = true;
            this.txtInsuranceCompnay.AssociatedLookUpName = "";
            this.txtInsuranceCompnay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsuranceCompnay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInsuranceCompnay.ContinuationTextBox = null;
            this.txtInsuranceCompnay.CustomEnabled = true;
            this.txtInsuranceCompnay.DataFieldMapping = "FN_INSUR_COMP";
            this.txtInsuranceCompnay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsuranceCompnay.GetRecordsOnUpDownKeys = false;
            this.txtInsuranceCompnay.IsDate = false;
            this.txtInsuranceCompnay.Location = new System.Drawing.Point(194, 144);
            this.txtInsuranceCompnay.MaxLength = 25;
            this.txtInsuranceCompnay.Name = "txtInsuranceCompnay";
            this.txtInsuranceCompnay.NumberFormat = "###,###,##0.00";
            this.txtInsuranceCompnay.Postfix = "";
            this.txtInsuranceCompnay.Prefix = "";
            this.txtInsuranceCompnay.Size = new System.Drawing.Size(380, 20);
            this.txtInsuranceCompnay.SkipValidation = false;
            this.txtInsuranceCompnay.TabIndex = 6;
            this.txtInsuranceCompnay.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtInsurancePolicyNo
            // 
            this.txtInsurancePolicyNo.AllowSpace = true;
            this.txtInsurancePolicyNo.AssociatedLookUpName = "";
            this.txtInsurancePolicyNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsurancePolicyNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInsurancePolicyNo.ContinuationTextBox = null;
            this.txtInsurancePolicyNo.CustomEnabled = true;
            this.txtInsurancePolicyNo.DataFieldMapping = "FN_POLICY_NO";
            this.txtInsurancePolicyNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsurancePolicyNo.GetRecordsOnUpDownKeys = false;
            this.txtInsurancePolicyNo.IsDate = false;
            this.txtInsurancePolicyNo.Location = new System.Drawing.Point(194, 118);
            this.txtInsurancePolicyNo.MaxLength = 15;
            this.txtInsurancePolicyNo.Name = "txtInsurancePolicyNo";
            this.txtInsurancePolicyNo.NumberFormat = "###,###,##0.00";
            this.txtInsurancePolicyNo.Postfix = "";
            this.txtInsurancePolicyNo.Prefix = "";
            this.txtInsurancePolicyNo.Size = new System.Drawing.Size(100, 20);
            this.txtInsurancePolicyNo.SkipValidation = false;
            this.txtInsurancePolicyNo.TabIndex = 5;
            this.txtInsurancePolicyNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinanaceType
            // 
            this.txtFinanaceType.AllowSpace = true;
            this.txtFinanaceType.AssociatedLookUpName = "";
            this.txtFinanaceType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanaceType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanaceType.ContinuationTextBox = null;
            this.txtFinanaceType.CustomEnabled = false;
            this.txtFinanaceType.DataFieldMapping = "fn_type";
            this.txtFinanaceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanaceType.GetRecordsOnUpDownKeys = false;
            this.txtFinanaceType.IsDate = false;
            this.txtFinanaceType.Location = new System.Drawing.Point(194, 92);
            this.txtFinanaceType.Name = "txtFinanaceType";
            this.txtFinanaceType.NumberFormat = "###,###,##0.00";
            this.txtFinanaceType.Postfix = "";
            this.txtFinanaceType.Prefix = "";
            this.txtFinanaceType.Size = new System.Drawing.Size(66, 20);
            this.txtFinanaceType.SkipValidation = true;
            this.txtFinanaceType.TabIndex = 3;
            this.txtFinanaceType.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "LBFin";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = true;
            this.txtFinanceNo.DataFieldMapping = "FN_FINANCE_NO";
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.IsRequired = true;
            this.txtFinanceNo.Location = new System.Drawing.Point(194, 65);
            this.txtFinanceNo.MaxLength = 10;
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(125, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 2;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            this.txtFinanceNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtFinanceNo_Validating);
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = false;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(194, 39);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(380, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "LBPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(194, 12);
            this.txtPersonnelNo.MaxLength = 6;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNo.SkipValidation = true;
            this.txtPersonnelNo.TabIndex = 0;
            this.txtPersonnelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(13, 93);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(609, 91);
            this.pnlHead.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(168, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "VEHICLE INSURANCE ENTRY";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(165, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "FINANCE SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(498, 35);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(455, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(447, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(447, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "User  Name :";
            // 
            // CHRIS_Finance_VehicleInsuranceEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(634, 607);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlPersonnel);
            this.CurrentPanelBlock = "pnlPersonnel";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.F4OptionText = "[F4]=Delete";
            this.Name = "CHRIS_Finance_VehicleInsuranceEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Vehicle Insurance Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_VehicleInsuranceEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlPersonnel.ResumeLayout(false);
            this.pnlPersonnel.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonnel;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtFirstName1;
        private System.Windows.Forms.Label txtPersonnelNo1;
        private CrplControlLibrary.SLTextBox txtdescrip;
        private CrplControlLibrary.SLTextBox txtVehMake;
        private CrplControlLibrary.SLTextBox txtVehicleChasisNo;
        private CrplControlLibrary.SLTextBox txtVehicleEngineNo;
        private CrplControlLibrary.SLTextBox txtVehicleRegistrationNo;
        private CrplControlLibrary.SLTextBox txtInsuranceCompnay;
        private CrplControlLibrary.SLTextBox txtInsurancePolicyNo;
        private CrplControlLibrary.SLTextBox txtFinanaceType;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtVehicleMedia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.LookupButton LBFin;
        private CrplControlLibrary.LookupButton LBPersonnel;
        private CrplControlLibrary.SLDatePicker dtpexpirydt;
        private CrplControlLibrary.SLDatePicker dtPisssuedate;
        private System.Windows.Forms.Label label19;
    }
}