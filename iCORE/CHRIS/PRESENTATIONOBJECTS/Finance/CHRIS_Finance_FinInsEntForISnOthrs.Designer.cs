namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinInsEntForISnOthrs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinInsEntForISnOthrs));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtExtratime = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTEndDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.DTstartdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtaviledamt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLiquidationflag = new CrplControlLibrary.SLTextBox(this.components);
            this.txtcreditratio = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtmonded = new CrplControlLibrary.SLTextBox(this.components);
            this.txtpaymentschd = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLoantype = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrPno = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.FN_MDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_DEBIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_CREDIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_MARKUP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdFN_LOAN_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_PAY_LEFT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdFN_LIQ_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlMaster.SuspendLayout();
            this.PnlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(640, 0);
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(676, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 587);
            this.panel1.Size = new System.Drawing.Size(676, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 54);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(669, 91);
            this.pnlHead.TabIndex = 50;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(168, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Finance Installment(I.S. && Oth.)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(165, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(259, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "PAYROLL SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(498, 35);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(447, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(430, 38);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "OPTION:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(-12, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "LOCATION:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(12, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "USER:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlMaster
            // 
            this.pnlMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaster.ConcurrentPanels = null;
            this.pnlMaster.Controls.Add(this.dtExtratime);
            this.pnlMaster.Controls.Add(this.DTEndDate);
            this.pnlMaster.Controls.Add(this.DTstartdate);
            this.pnlMaster.Controls.Add(this.label21);
            this.pnlMaster.Controls.Add(this.label20);
            this.pnlMaster.Controls.Add(this.label19);
            this.pnlMaster.Controls.Add(this.label13);
            this.pnlMaster.Controls.Add(this.label11);
            this.pnlMaster.Controls.Add(this.label10);
            this.pnlMaster.Controls.Add(this.label8);
            this.pnlMaster.Controls.Add(this.label7);
            this.pnlMaster.Controls.Add(this.label6);
            this.pnlMaster.Controls.Add(this.label5);
            this.pnlMaster.Controls.Add(this.label2);
            this.pnlMaster.Controls.Add(this.txtaviledamt);
            this.pnlMaster.Controls.Add(this.txtLiquidationflag);
            this.pnlMaster.Controls.Add(this.txtcreditratio);
            this.pnlMaster.Controls.Add(this.txtFinanceNo);
            this.pnlMaster.Controls.Add(this.txtBranch);
            this.pnlMaster.Controls.Add(this.txtmonded);
            this.pnlMaster.Controls.Add(this.txtpaymentschd);
            this.pnlMaster.Controls.Add(this.txtLoantype);
            this.pnlMaster.Controls.Add(this.lbtnPersonnel);
            this.pnlMaster.Controls.Add(this.label1);
            this.pnlMaster.Controls.Add(this.txtFirstName);
            this.pnlMaster.Controls.Add(this.txtPrPno);
            this.pnlMaster.DataManager = null;
            this.pnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMaster.DependentPanels = null;
            this.pnlMaster.DisableDependentLoad = false;
            this.pnlMaster.EnableDelete = true;
            this.pnlMaster.EnableInsert = true;
            this.pnlMaster.EnableQuery = false;
            this.pnlMaster.EnableUpdate = true;
            this.pnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand";
            this.pnlMaster.Location = new System.Drawing.Point(12, 151);
            this.pnlMaster.MasterPanel = null;
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMaster.Size = new System.Drawing.Size(646, 205);
            this.pnlMaster.SPName = "CHRIS_SP_fin_Booking_MANAGER";
            this.pnlMaster.TabIndex = 1;
            // 
            // dtExtratime
            // 
            this.dtExtratime.CustomEnabled = true;
            this.dtExtratime.CustomFormat = "dd/MM/yyyy";
            this.dtExtratime.DataFieldMapping = "FN_EXTRA_TIME";
            this.dtExtratime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExtratime.HasChanges = true;
            this.dtExtratime.Location = new System.Drawing.Point(499, 120);
            this.dtExtratime.Name = "dtExtratime";
            this.dtExtratime.NullValue = " ";
            this.dtExtratime.Size = new System.Drawing.Size(100, 20);
            this.dtExtratime.TabIndex = 3;
            this.dtExtratime.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.dtExtratime.Validating += new System.ComponentModel.CancelEventHandler(this.dtExtratime_Validating);
            // 
            // DTEndDate
            // 
            this.DTEndDate.CustomEnabled = false;
            this.DTEndDate.CustomFormat = "dd/MM/yyyy";
            this.DTEndDate.DataFieldMapping = "FN_END_DATE";
            this.DTEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTEndDate.HasChanges = false;
            this.DTEndDate.Location = new System.Drawing.Point(499, 94);
            this.DTEndDate.Name = "DTEndDate";
            this.DTEndDate.NullValue = " ";
            this.DTEndDate.Size = new System.Drawing.Size(100, 20);
            this.DTEndDate.TabIndex = 45;
            this.DTEndDate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.DTEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.DTEndDate_Validating);
            // 
            // DTstartdate
            // 
            this.DTstartdate.CustomEnabled = false;
            this.DTstartdate.CustomFormat = "dd/MM/yyyy";
            this.DTstartdate.DataFieldMapping = "FN_START_DATE";
            this.DTstartdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTstartdate.HasChanges = false;
            this.DTstartdate.Location = new System.Drawing.Point(141, 91);
            this.DTstartdate.Name = "DTstartdate";
            this.DTstartdate.NullValue = " ";
            this.DTstartdate.Size = new System.Drawing.Size(100, 20);
            this.DTstartdate.TabIndex = 44;
            this.DTstartdate.Value = new System.DateTime(2010, 12, 20, 0, 0, 0, 0);
            this.DTstartdate.Validating += new System.ComponentModel.CancelEventHandler(this.DTstartdate_Validating);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(328, 138);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(138, 20);
            this.label21.TabIndex = 43;
            this.label21.Text = "Monthly Deduction :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(352, 113);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 20);
            this.label20.TabIndex = 42;
            this.label20.Text = "Extra Time :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(331, 60);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(135, 20);
            this.label19.TabIndex = 41;
            this.label19.Text = "Payment Schedule :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(352, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 20);
            this.label13.TabIndex = 40;
            this.label13.Text = "Branch :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(12, 167);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 20);
            this.label11.TabIndex = 39;
            this.label11.Text = "Liquidation Flag :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(21, 143);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 20);
            this.label10.TabIndex = 38;
            this.label10.Text = "Credit Ratio :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(21, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 20);
            this.label8.TabIndex = 37;
            this.label8.Text = "Availed Amount";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(21, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Start Date :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(21, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 20);
            this.label6.TabIndex = 35;
            this.label6.Text = "Finance No  :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(352, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 20);
            this.label5.TabIndex = 35;
            this.label5.Text = "End Date :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(21, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 34;
            this.label2.Text = "Loan Type :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtaviledamt
            // 
            this.txtaviledamt.AllowSpace = true;
            this.txtaviledamt.AssociatedLookUpName = "";
            this.txtaviledamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaviledamt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtaviledamt.ContinuationTextBox = null;
            this.txtaviledamt.CustomEnabled = true;
            this.txtaviledamt.DataFieldMapping = "FN_AMT_AVAILED";
            this.txtaviledamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaviledamt.GetRecordsOnUpDownKeys = false;
            this.txtaviledamt.IsDate = false;
            this.txtaviledamt.Location = new System.Drawing.Point(141, 117);
            this.txtaviledamt.Name = "txtaviledamt";
            this.txtaviledamt.NumberFormat = "###,###,##0.00";
            this.txtaviledamt.Postfix = "";
            this.txtaviledamt.Prefix = "";
            this.txtaviledamt.Size = new System.Drawing.Size(100, 20);
            this.txtaviledamt.SkipValidation = false;
            this.txtaviledamt.TabIndex = 2;
            this.txtaviledamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtaviledamt.TextType = CrplControlLibrary.TextType.String;
            this.txtaviledamt.Validating += new System.ComponentModel.CancelEventHandler(this.txtaviledamt_Validating);
            // 
            // txtLiquidationflag
            // 
            this.txtLiquidationflag.AllowSpace = true;
            this.txtLiquidationflag.AssociatedLookUpName = "";
            this.txtLiquidationflag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLiquidationflag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLiquidationflag.ContinuationTextBox = null;
            this.txtLiquidationflag.CustomEnabled = false;
            this.txtLiquidationflag.DataFieldMapping = "FN_LIQUIDATE";
            this.txtLiquidationflag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLiquidationflag.GetRecordsOnUpDownKeys = false;
            this.txtLiquidationflag.IsDate = false;
            this.txtLiquidationflag.Location = new System.Drawing.Point(141, 169);
            this.txtLiquidationflag.Name = "txtLiquidationflag";
            this.txtLiquidationflag.NumberFormat = "###,###,##0.00";
            this.txtLiquidationflag.Postfix = "";
            this.txtLiquidationflag.Prefix = "";
            this.txtLiquidationflag.Size = new System.Drawing.Size(47, 20);
            this.txtLiquidationflag.SkipValidation = false;
            this.txtLiquidationflag.TabIndex = 32;
            this.txtLiquidationflag.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtcreditratio
            // 
            this.txtcreditratio.AllowSpace = true;
            this.txtcreditratio.AssociatedLookUpName = "";
            this.txtcreditratio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcreditratio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcreditratio.ContinuationTextBox = null;
            this.txtcreditratio.CustomEnabled = true;
            this.txtcreditratio.DataFieldMapping = "FN_C_RATIO_PER";
            this.txtcreditratio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcreditratio.GetRecordsOnUpDownKeys = false;
            this.txtcreditratio.IsDate = false;
            this.txtcreditratio.Location = new System.Drawing.Point(141, 143);
            this.txtcreditratio.Name = "txtcreditratio";
            this.txtcreditratio.NumberFormat = "###,###,##0.00";
            this.txtcreditratio.Postfix = "";
            this.txtcreditratio.Prefix = "";
            this.txtcreditratio.Size = new System.Drawing.Size(62, 20);
            this.txtcreditratio.SkipValidation = false;
            this.txtcreditratio.TabIndex = 4;
            this.txtcreditratio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcreditratio.TextType = CrplControlLibrary.TextType.String;
            this.txtcreditratio.Validating += new System.ComponentModel.CancelEventHandler(this.txtcreditratio_Validating);
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = false;
            this.txtFinanceNo.DataFieldMapping = "FN_FIN_NO";
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.Location = new System.Drawing.Point(141, 65);
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(99, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 28;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = false;
            this.txtBranch.DataFieldMapping = "FN_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(499, 43);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.Size = new System.Drawing.Size(100, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 26;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtmonded
            // 
            this.txtmonded.AllowSpace = true;
            this.txtmonded.AssociatedLookUpName = "";
            this.txtmonded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmonded.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmonded.ContinuationTextBox = null;
            this.txtmonded.CustomEnabled = true;
            this.txtmonded.DataFieldMapping = "FN_MONTHLY_DED";
            this.txtmonded.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmonded.GetRecordsOnUpDownKeys = false;
            this.txtmonded.IsDate = false;
            this.txtmonded.Location = new System.Drawing.Point(499, 147);
            this.txtmonded.Name = "txtmonded";
            this.txtmonded.NumberFormat = "###,###,##0.00";
            this.txtmonded.Postfix = "";
            this.txtmonded.Prefix = "";
            this.txtmonded.Size = new System.Drawing.Size(100, 20);
            this.txtmonded.SkipValidation = false;
            this.txtmonded.TabIndex = 5;
            this.txtmonded.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtmonded.TextType = CrplControlLibrary.TextType.String;
            this.txtmonded.Validating += new System.ComponentModel.CancelEventHandler(this.txtmonded_Validating);
            // 
            // txtpaymentschd
            // 
            this.txtpaymentschd.AllowSpace = true;
            this.txtpaymentschd.AssociatedLookUpName = "";
            this.txtpaymentschd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpaymentschd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpaymentschd.ContinuationTextBox = null;
            this.txtpaymentschd.CustomEnabled = false;
            this.txtpaymentschd.DataFieldMapping = "FN_PAY_SCHED";
            this.txtpaymentschd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpaymentschd.GetRecordsOnUpDownKeys = false;
            this.txtpaymentschd.IsDate = false;
            this.txtpaymentschd.Location = new System.Drawing.Point(499, 67);
            this.txtpaymentschd.Name = "txtpaymentschd";
            this.txtpaymentschd.NumberFormat = "###,###,##0.00";
            this.txtpaymentschd.Postfix = "";
            this.txtpaymentschd.Prefix = "";
            this.txtpaymentschd.Size = new System.Drawing.Size(100, 20);
            this.txtpaymentschd.SkipValidation = false;
            this.txtpaymentschd.TabIndex = 24;
            this.txtpaymentschd.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLoantype
            // 
            this.txtLoantype.AllowSpace = true;
            this.txtLoantype.AssociatedLookUpName = "";
            this.txtLoantype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoantype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLoantype.ContinuationTextBox = null;
            this.txtLoantype.CustomEnabled = false;
            this.txtLoantype.DataFieldMapping = "FN_TYPE";
            this.txtLoantype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoantype.GetRecordsOnUpDownKeys = false;
            this.txtLoantype.IsDate = false;
            this.txtLoantype.Location = new System.Drawing.Point(141, 39);
            this.txtLoantype.Name = "txtLoantype";
            this.txtLoantype.NumberFormat = "###,###,##0.00";
            this.txtLoantype.Postfix = "";
            this.txtLoantype.Prefix = "";
            this.txtLoantype.Size = new System.Drawing.Size(98, 20);
            this.txtLoantype.SkipValidation = false;
            this.txtLoantype.TabIndex = 23;
            this.txtLoantype.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPersonnel
            // 
            this.lbtnPersonnel.ActionLOVExists = "FinPersonnelExists";
            this.lbtnPersonnel.ActionType = "FinPersonnel";
            this.lbtnPersonnel.ConditionalFields = "";
            this.lbtnPersonnel.CustomEnabled = true;
            this.lbtnPersonnel.DataFieldMapping = "";
            this.lbtnPersonnel.DependentLovControls = "";
            this.lbtnPersonnel.HiddenColumns = "PR_DESIG|PR_LEVEL|PR_BRANCH|PR_JOINING_DATE|FN_BRANCH|FN_PAY_SCHED|FN_START_DATE|" +
                "FN_END_DATE|FN_AMT_AVAILED|FN_EXTRA_TIME|FN_C_RATIO_PER|FN_MONTHLY_DED|FN_LIQUID" +
                "ATE";
            this.lbtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnel.Image")));
            this.lbtnPersonnel.LoadDependentEntities = true;
            this.lbtnPersonnel.Location = new System.Drawing.Point(223, 11);
            this.lbtnPersonnel.LookUpTitle = null;
            this.lbtnPersonnel.Name = "lbtnPersonnel";
            this.lbtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnel.SkipValidationOnLeave = false;
            this.lbtnPersonnel.SPName = "CHRIS_SP_fin_Booking_MANAGER";
            this.lbtnPersonnel.TabIndex = 22;
            this.lbtnPersonnel.TabStop = false;
            this.lbtnPersonnel.UseVisualStyleBackColor = true;
            this.lbtnPersonnel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnPersonnel_MouseDown);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(21, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Personnel No. :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = false;
            this.txtFirstName.DataFieldMapping = "FirstName";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(255, 11);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(291, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPrPno
            // 
            this.txtPrPno.AllowSpace = true;
            this.txtPrPno.AssociatedLookUpName = "lbtnPersonnel";
            this.txtPrPno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrPno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrPno.ContinuationTextBox = null;
            this.txtPrPno.CustomEnabled = true;
            this.txtPrPno.DataFieldMapping = "PR_P_NO";
            this.txtPrPno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrPno.GetRecordsOnUpDownKeys = false;
            this.txtPrPno.IsDate = false;
            this.txtPrPno.IsRequired = true;
            this.txtPrPno.Location = new System.Drawing.Point(141, 13);
            this.txtPrPno.Name = "txtPrPno";
            this.txtPrPno.NumberFormat = "###,###,##0.00";
            this.txtPrPno.Postfix = "";
            this.txtPrPno.Prefix = "";
            this.txtPrPno.Size = new System.Drawing.Size(76, 20);
            this.txtPrPno.SkipValidation = false;
            this.txtPrPno.TabIndex = 1;
            this.txtPrPno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrPno.TextType = CrplControlLibrary.TextType.Double;
            this.txtPrPno.Leave += new System.EventHandler(this.txtPrPno_Leave);
            this.txtPrPno.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrPno_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(200, 349);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(288, 17);
            this.label4.TabIndex = 46;
            this.label4.Text = "     Monthly Installment Information      ";
            // 
            // PnlDetail
            // 
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.DGVDetail);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = false;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FNMonthCommand";
            this.PnlDetail.Location = new System.Drawing.Point(12, 381);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(646, 203);
            this.PnlDetail.SPName = "CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment";
            this.PnlDetail.TabIndex = 1;
            // 
            // DGVDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FN_MDATE,
            this.FN_DEBIT,
            this.FN_CREDIT,
            this.FN_MARKUP,
            this.FN_BALANCE,
            this.grdFN_LOAN_BALANCE,
            this.FN_PAY_LEFT,
            this.grdFN_LIQ_FLAG,
            this.Column9});
            this.DGVDetail.ColumnToHide = null;
            this.DGVDetail.ColumnWidth = null;
            this.DGVDetail.CustomEnabled = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVDetail.DefaultCellStyle = dataGridViewCellStyle8;
            this.DGVDetail.DisplayColumnWrapper = null;
            this.DGVDetail.GridDefaultRow = 0;
            this.DGVDetail.Location = new System.Drawing.Point(6, 13);
            this.DGVDetail.Name = "DGVDetail";
            this.DGVDetail.ReadOnlyColumns = null;
            this.DGVDetail.RequiredColumns = "FN_MDATE|";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DGVDetail.Size = new System.Drawing.Size(637, 183);
            this.DGVDetail.SkippingColumns = null;
            this.DGVDetail.TabIndex = 1;
            this.DGVDetail.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVDetail_CellLeave);
            this.DGVDetail.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.DGVDetail_UserAddedRow);
            this.DGVDetail.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGVDetail_RowValidating);
            this.DGVDetail.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVDetail_CellValidated);
            this.DGVDetail.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVDetail_CellValidating);
            this.DGVDetail.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVDetail_CellEnter);
            this.DGVDetail.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DGVDetail_DataBindingComplete);
            // 
            // FN_MDATE
            // 
            this.FN_MDATE.DataPropertyName = "FN_MDATE";
            dataGridViewCellStyle2.NullValue = null;
            this.FN_MDATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.FN_MDATE.HeaderText = "Date";
            this.FN_MDATE.MaxInputLength = 10;
            this.FN_MDATE.Name = "FN_MDATE";
            this.FN_MDATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_MDATE.ToolTipText = "PRESS [down ] for new record  [F10] TO SAVE  [F6] TO EXIT W/O SAVE [F4] TO DELETE" +
                "";
            this.FN_MDATE.Width = 90;
            // 
            // FN_DEBIT
            // 
            this.FN_DEBIT.DataPropertyName = "FN_DEBIT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.FN_DEBIT.DefaultCellStyle = dataGridViewCellStyle3;
            this.FN_DEBIT.HeaderText = "Debit";
            this.FN_DEBIT.MaxInputLength = 10;
            this.FN_DEBIT.Name = "FN_DEBIT";
            this.FN_DEBIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_DEBIT.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.FN_DEBIT.Width = 90;
            // 
            // FN_CREDIT
            // 
            this.FN_CREDIT.DataPropertyName = "FN_CREDIT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.FN_CREDIT.DefaultCellStyle = dataGridViewCellStyle4;
            this.FN_CREDIT.HeaderText = "Credit";
            this.FN_CREDIT.MaxInputLength = 10;
            this.FN_CREDIT.Name = "FN_CREDIT";
            this.FN_CREDIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_CREDIT.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.FN_CREDIT.Width = 90;
            // 
            // FN_MARKUP
            // 
            this.FN_MARKUP.DataPropertyName = "FN_MARKUP";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.FN_MARKUP.DefaultCellStyle = dataGridViewCellStyle5;
            this.FN_MARKUP.HeaderText = "Markup";
            this.FN_MARKUP.MaxInputLength = 10;
            this.FN_MARKUP.Name = "FN_MARKUP";
            this.FN_MARKUP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_MARKUP.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.FN_MARKUP.Width = 90;
            // 
            // FN_BALANCE
            // 
            this.FN_BALANCE.DataPropertyName = "FN_BALANCE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.FN_BALANCE.DefaultCellStyle = dataGridViewCellStyle6;
            this.FN_BALANCE.HeaderText = "O/S Bal";
            this.FN_BALANCE.Name = "FN_BALANCE";
            this.FN_BALANCE.ReadOnly = true;
            this.FN_BALANCE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_BALANCE.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.FN_BALANCE.Width = 90;
            // 
            // grdFN_LOAN_BALANCE
            // 
            this.grdFN_LOAN_BALANCE.DataPropertyName = "FN_LOAN_BALANCE";
            this.grdFN_LOAN_BALANCE.HeaderText = "Loan Bal";
            this.grdFN_LOAN_BALANCE.Name = "grdFN_LOAN_BALANCE";
            this.grdFN_LOAN_BALANCE.ReadOnly = true;
            this.grdFN_LOAN_BALANCE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.grdFN_LOAN_BALANCE.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.grdFN_LOAN_BALANCE.Width = 90;
            // 
            // FN_PAY_LEFT
            // 
            this.FN_PAY_LEFT.DataPropertyName = "FN_PAY_LEFT";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.FN_PAY_LEFT.DefaultCellStyle = dataGridViewCellStyle7;
            this.FN_PAY_LEFT.HeaderText = "Pay";
            this.FN_PAY_LEFT.Name = "FN_PAY_LEFT";
            this.FN_PAY_LEFT.ReadOnly = true;
            this.FN_PAY_LEFT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_PAY_LEFT.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.FN_PAY_LEFT.Width = 90;
            // 
            // grdFN_LIQ_FLAG
            // 
            this.grdFN_LIQ_FLAG.DataPropertyName = "FN_LIQ_FLAG";
            this.grdFN_LIQ_FLAG.HeaderText = "Flg";
            this.grdFN_LIQ_FLAG.Name = "grdFN_LIQ_FLAG";
            this.grdFN_LIQ_FLAG.ReadOnly = true;
            this.grdFN_LIQ_FLAG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.grdFN_LIQ_FLAG.ToolTipText = "PRESS [F10] TO SAVE   [F6] TO EXIT W/O SAVE   [F4] TO DELETE";
            this.grdFN_LIQ_FLAG.Width = 70;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "ID";
            this.Column9.HeaderText = "ID";
            this.Column9.Name = "Column9";
            this.Column9.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(462, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "User Name  :";
            // 
            // CHRIS_Finance_FinInsEntForISnOthrs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 647);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlMaster);
            this.Controls.Add(this.PnlDetail);
            this.CurrentPanelBlock = "pnlMaster";
            this.F1OptionText = resources.GetString("$this.F1OptionText");
            this.Name = "CHRIS_Finance_FinInsEntForISnOthrs";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Finance Intsallment Entry For";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_FinanceIntsallmentEntryFor_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlMaster, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            this.PnlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlMaster;
        private CrplControlLibrary.LookupButton lbtnPersonnel;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPrPno;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVDetail;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtmonded;
        private CrplControlLibrary.SLTextBox txtpaymentschd;
        private CrplControlLibrary.SLTextBox txtLoantype;
        private CrplControlLibrary.SLTextBox txtaviledamt;
        private CrplControlLibrary.SLTextBox txtLiquidationflag;
        private CrplControlLibrary.SLTextBox txtcreditratio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLDatePicker DTEndDate;
        private CrplControlLibrary.SLDatePicker DTstartdate;
        private CrplControlLibrary.SLDatePicker dtExtratime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_MDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_DEBIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_CREDIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_MARKUP;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdFN_LOAN_BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_PAY_LEFT;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdFN_LIQ_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
    }
}