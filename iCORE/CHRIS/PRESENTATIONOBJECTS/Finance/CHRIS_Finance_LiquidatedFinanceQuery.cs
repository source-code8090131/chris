using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;

using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_LiquidatedFinanceQuery : ChrisMasterDetailForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        # region Constructor
        public CHRIS_Finance_LiquidatedFinanceQuery()
        {
            InitializeComponent();
        }

        public CHRIS_Finance_LiquidatedFinanceQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlDetail);
            pnlMaster.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlMaster);


        }

        #endregion
        # region Methods
        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);


            fn_fin_no.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            Column2.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            Column3.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            Column4.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            Column5.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            EXCP_FLAG.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            EXCP_REM.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            
            
            
            
            txtOption.Visible = false;
            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.FunctionConfig.F6 = Function.Quit;
            this.tbtDelete.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtList.Visible = false;
            this.label6.Text += " " + this.UserName;
            txtPrPno.Focus();
            txtPrPno.Select();
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Save")
            {
                return;
            
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }


        protected override bool Quit()
        {
            return base.Quit();
        }
        #endregion
        # region Events
        
        private void DGVDetail_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txtPrPno.Text);
                param.Add("FN_FIN_NO", DGVDetail.CurrentRow.Cells["fn_fin_no"].ToString());

                rslt = cmnDM.GetData("CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment", "Excep_FLG", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DGVDetail.CurrentRow.Cells["EXCP_FLAG"].Value = rslt.dstResult.Tables[0].Rows[e.RowIndex].ItemArray[5].ToString();
                        DGVDetail.CurrentRow.Cells["EXCP_REM"].Value = rslt.dstResult.Tables[0].Rows[e.RowIndex].ItemArray[6].ToString();
                    }
                }
            }

           
          //  base.DoToolbarActions(this.Controls, "Save");


        }
        #endregion

       }
}