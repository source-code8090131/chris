namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_LiqLoansFinanceQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_LiqLoansFinanceQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrPno = new CrplControlLibrary.SLTextBox(this.components);
            this.DGVDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.fn_fin_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXCP_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXCP_REM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlMaster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDetail)).BeginInit();
            this.PnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(617, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(653, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 458);
            this.panel1.Size = new System.Drawing.Size(653, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(9, 62);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(622, 91);
            this.pnlHead.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-3, -16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(631, 13);
            this.label5.TabIndex = 50;
            this.label5.Text = "_________________________________________________________________________________" +
                "_______________________";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(168, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Liquidated Loans / Finance Query";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(165, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Finance System";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(498, 35);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(455, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(447, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(12, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlMaster
            // 
            this.pnlMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaster.ConcurrentPanels = null;
            this.pnlMaster.Controls.Add(this.lbtnPersonnel);
            this.pnlMaster.Controls.Add(this.label1);
            this.pnlMaster.Controls.Add(this.txtFirstName);
            this.pnlMaster.Controls.Add(this.txtPrPno);
            this.pnlMaster.DataManager = null;
            this.pnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMaster.DependentPanels = null;
            this.pnlMaster.DisableDependentLoad = false;
            this.pnlMaster.EnableDelete = true;
            this.pnlMaster.EnableInsert = true;
            this.pnlMaster.EnableQuery = false;
            this.pnlMaster.EnableUpdate = true;
            this.pnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlMaster.Location = new System.Drawing.Point(12, 183);
            this.pnlMaster.MasterPanel = null;
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMaster.Size = new System.Drawing.Size(619, 41);
            this.pnlMaster.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.pnlMaster.TabIndex = 0;
            // 
            // lbtnPersonnel
            // 
            this.lbtnPersonnel.ActionLOVExists = "Personal_LovExists";
            this.lbtnPersonnel.ActionType = "Personal_Lov";
            this.lbtnPersonnel.ConditionalFields = "";
            this.lbtnPersonnel.CustomEnabled = true;
            this.lbtnPersonnel.DataFieldMapping = "";
            this.lbtnPersonnel.DependentLovControls = "";
            this.lbtnPersonnel.HiddenColumns = "PR_DESIG|PR_LEVEL|PR_BRANCH|PR_JOINING_DATE";
            this.lbtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnel.Image")));
            this.lbtnPersonnel.LoadDependentEntities = true;
            this.lbtnPersonnel.Location = new System.Drawing.Point(223, 11);
            this.lbtnPersonnel.LookUpTitle = null;
            this.lbtnPersonnel.Name = "lbtnPersonnel";
            this.lbtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnel.SkipValidationOnLeave = false;
            this.lbtnPersonnel.SPName = "CHRIS_SP_LIQUID_FIANANCE_QUERY_MANAGER";
            this.lbtnPersonnel.TabIndex = 22;
            this.lbtnPersonnel.TabStop = false;
            this.lbtnPersonnel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(21, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Personnel No. :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = false;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(287, 11);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.Size = new System.Drawing.Size(291, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPrPno
            // 
            this.txtPrPno.AllowSpace = true;
            this.txtPrPno.AssociatedLookUpName = "lbtnPersonnel";
            this.txtPrPno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrPno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrPno.ContinuationTextBox = null;
            this.txtPrPno.CustomEnabled = true;
            this.txtPrPno.DataFieldMapping = "PR_P_NO";
            this.txtPrPno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrPno.GetRecordsOnUpDownKeys = false;
            this.txtPrPno.IsDate = false;
            this.txtPrPno.Location = new System.Drawing.Point(141, 12);
            this.txtPrPno.Name = "txtPrPno";
            this.txtPrPno.NumberFormat = "###,###,##0.00";
            this.txtPrPno.Postfix = "";
            this.txtPrPno.Prefix = "";
            this.txtPrPno.Size = new System.Drawing.Size(76, 20);
            this.txtPrPno.SkipValidation = false;
            this.txtPrPno.TabIndex = 1;
            this.txtPrPno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // DGVDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fn_fin_no,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.EXCP_FLAG,
            this.EXCP_REM});
            this.DGVDetail.ColumnToHide = null;
            this.DGVDetail.ColumnWidth = null;
            this.DGVDetail.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVDetail.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGVDetail.DisplayColumnWrapper = null;
            this.DGVDetail.GridDefaultRow = 0;
            this.DGVDetail.Location = new System.Drawing.Point(4, 6);
            this.DGVDetail.Name = "DGVDetail";
            this.DGVDetail.ReadOnlyColumns = null;
            this.DGVDetail.RequiredColumns = null;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGVDetail.Size = new System.Drawing.Size(600, 223);
            this.DGVDetail.SkippingColumns = null;
            this.DGVDetail.TabIndex = 1;
            this.DGVDetail.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVDetail_CellValidating);
            // 
            // fn_fin_no
            // 
            this.fn_fin_no.DataPropertyName = "fn_fin_no";
            this.fn_fin_no.HeaderText = "Finance Number";
            this.fn_fin_no.Name = "fn_fin_no";
            this.fn_fin_no.ReadOnly = true;
            this.fn_fin_no.Width = 90;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "dbd";
            this.Column2.HeaderText = "Disbursment Date";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 90;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "dis_amt";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column3.HeaderText = "Disbursment Amount";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 90;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "liqd";
            this.Column4.HeaderText = "Liquidation Date";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 90;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "liqd_amt";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column5.HeaderText = "Liquidation Amount";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 90;
            // 
            // EXCP_FLAG
            // 
            this.EXCP_FLAG.DataPropertyName = "EXCP_FLAG";
            this.EXCP_FLAG.HeaderText = "Exception Y/N";
            this.EXCP_FLAG.Name = "EXCP_FLAG";
            this.EXCP_FLAG.ReadOnly = true;
            this.EXCP_FLAG.Width = 90;
            // 
            // EXCP_REM
            // 
            this.EXCP_REM.DataPropertyName = "EXCP_REM";
            this.EXCP_REM.HeaderText = "Exception  Remarks";
            this.EXCP_REM.Name = "EXCP_REM";
            this.EXCP_REM.ReadOnly = true;
            this.EXCP_REM.Width = 110;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(126, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 20);
            this.label4.TabIndex = 22;
            this.label4.Text = "Disbursment";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.Visible = false;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(271, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 20);
            this.label9.TabIndex = 44;
            this.label9.Text = "Liquidation";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label9.Visible = false;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(452, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 20);
            this.label12.TabIndex = 47;
            this.label12.Text = "Exception";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label12.Visible = false;
            // 
            // PnlDetail
            // 
            this.PnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.label2);
            this.PnlDetail.Controls.Add(this.label12);
            this.PnlDetail.Controls.Add(this.label4);
            this.PnlDetail.Controls.Add(this.DGVDetail);
            this.PnlDetail.Controls.Add(this.label9);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = false;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LiquidateFinanceQueryCommand";
            this.PnlDetail.Location = new System.Drawing.Point(12, 221);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(619, 234);
            this.PnlDetail.SPName = "CHRIS_SP_LIQUID_FIANANCE_QUERY_MANAGER";
            this.PnlDetail.TabIndex = 48;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(631, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "_________________________________________________________________________________" +
                "_______________________";
            this.label2.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(434, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "User Name : ";
            // 
            // CHRIS_Finance_LiqLoansFinanceQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 518);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlMaster);
            this.Controls.Add(this.PnlDetail);
            this.CurrentPanelBlock = "pnlMaster";
            this.F6OptionText = "[F6] = Exit";
            this.Name = "CHRIS_Finance_LiqLoansFinanceQuery";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Liquidated Finance Query";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.Controls.SetChildIndex(this.pnlMaster, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDetail)).EndInit();
            this.PnlDetail.ResumeLayout(false);
            this.PnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlMaster;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private CrplControlLibrary.SLTextBox txtPrPno;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVDetail;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.LookupButton lbtnPersonnel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn fn_fin_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn EXCP_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn EXCP_REM;
    }
}