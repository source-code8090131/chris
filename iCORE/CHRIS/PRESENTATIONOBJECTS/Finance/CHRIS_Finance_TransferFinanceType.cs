using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_TransferFinanceType : ChrisSimpleForm
    {
       # region constructor

        CmnDataManager cmnDM = new CmnDataManager();
        public CHRIS_Finance_TransferFinanceType()
        {
            InitializeComponent();
        }

        public CHRIS_Finance_TransferFinanceType(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
       #endregion
       #region Methods

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                return;
            }

            if (actionType == "Update")
            {
                return;
            }

            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Add")
            {
                return;
            }


            if (actionType == "Cancel")
            {
                if (txtFinNo.Text == string.Empty)
                {
                    txtFinNo.Select();
                    txtFinNo.Focus();
                    return;
                
                }


                txtPersonnelNo.IsRequired = false;
                txtFinNo.IsRequired = false;
                dtTransfer.IsRequired = false;
                base.DoToolbarActions(this.Controls, "Cancel");

                dtTransfer.Value = null;
                txtFinNo.Select();
                txtFinNo.Focus();
                
                txtPersonnelNo.IsRequired = true;
                txtFinNo.IsRequired = true;
                dtTransfer.IsRequired = true;

                
            }
        }
        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            //txtDESNAME.Name = "txtDESNAME";
            //txtDESNAME.Text = "";      //":GLOBAL.AUDIT_PATH||FN1";
            //pnl.Controls.Add(txtDESNAME);

            //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            //txtMODE.Name = "txtMODE";
            //txtMODE.Text = "CHARACTER";
            //pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "pno";
            txtPNO.Text = txtPersonnelNo.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "fin_No";
            txtfinNo.Text = txtFinNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "audit07A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }


        #endregion
       #region Events

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtOption.Visible = false;

            dtTransfer.Value = null;

            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy"); ;
            this.txtUser.Text = this.userID;
            this.tbtDelete.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtAdd.Visible = false;
            this.label11.Text += " " + this.UserName;

        }

        /* validates wheter transfer date 
         * is greater than from 
         * database date or not */
        private void dtTransfer_Validating(object sender, CancelEventArgs e)
        {
            if (txtFinNo.Text != string.Empty)
            {

                Result rslt;
                if (dtTransfer.Value != null)
                {
                    object obj = dtTransfer.Value.ToString();

                    DateTime dt1_database, dt2_user;
                    dt2_user = Convert.ToDateTime(dtTransfer.Value);


                    if (obj != null && obj.ToString() != string.Empty)
                    {
                        string value;
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("FNT_FIN_NO", txtFinNo.Text);
                            rslt = cmnDM.GetData("CHRIS_SP_FN_TRANSFER_MANAGER", "Date_check", param);
                            if (rslt.isSuccessful)
                            {
                                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    DataTable dt = rslt.dstResult.Tables[0];
                                    value = (dt.Rows[0].ItemArray[0].ToString());
                                    dt1_database = Convert.ToDateTime(value.ToString());

                                    TimeSpan ts = dt1_database.Subtract(dt2_user);
                                    int days = ts.Days;
                                    if (days > 0)

                                    //    if (dt1.Date > dt2.Date)
                                    {
                                        MessageBox.Show("Date Should Be Greater Than" + dt1_database.ToString(" dd/MM/yyyy"));
                                        e.Cancel = true;
                                    }
                                }
                            }

                        }
                    }

                }
            }
        }
        /* keep the focus to Datepicker control Named "dt_transfer"
         * */
        private void CHRIS_Finance_TransferFinanceType_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            //if (actionType == "List" || actionType == "Fin_No")
            //{
            //    //  this.dtTransfer.Value = null;
            //    this.dtTransfer.Select();
            //    this.dtTransfer.Focus();
            //}

            //if (actionType == "Fin_No")
            //{
            //    txtPersonnelNo.Select();
            //    txtPersonnelNo.Focus();

            //}

        }
        private void txtAdjAmnt_Leave(object sender, EventArgs e)
        {
           
        }     
        private void txtAdjAmnt_Validating(object sender, CancelEventArgs e)
        {
            double value = 0;

            //if ((double.TryParse(txtAdjAmnt.Text, out value)))
            //{

            //}
            //else
            //{
            //    this.txtAdjAmnt.Focus();
            //    return;
            //}

            if (this.txtFinNo.Text == "")
            {
                this.txtFinNo.Focus();
                return;
            }
            if (this.txtTransfertype.Text == "")
            {
                this.txtTransfertype.Focus();
                return;
            }
            if (this.dtTransfer.Value == null)
            {
                this.dtTransfer.Focus();
                return;
            }

            if (MessageBox.Show("Do You Want To Save The Record [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {

                if (txtAdjAmnt.Text != string.Empty)
                {
                    string amt = txtAdjAmnt.Text;

                }

                base.DoToolbarActions(this.Controls, "Save");
                CallReport("AUPR", "OLD");
                CallReport("AUP0", "NEW");
                txtPersonnelNo.IsRequired = false;
                txtFinNo.IsRequired = false;
                dtTransfer.IsRequired = false;
                 base.ClearForm(PnlDetail.Controls);
                iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FN_TransferCommand ent = new iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FN_TransferCommand();
                          txtPersonnelNo.IsRequired = true;
                txtFinNo.IsRequired = true;
                dtTransfer.IsRequired = true;



                txtFinNo.Select();
                txtFinNo.Focus();
                dtTransfer.Value = null;
            }
        }
        private void txtAdjAmnt_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //Keys st = (Keys.Shift | Keys.Tab);
            //if (e.Modifiers == Keys.Shift)
            //{
            //    if (e.KeyCode == Keys.Tab)
            //    {
                    

            //    }
            //}

            
            //else
            //{
            //    if (e.KeyData == Keys.Tab)
            //    {
            //        double value = 0;

            //        if ((double.TryParse(txtAdjAmnt.Text, out value)))
            //        {

            //        }
            //        else
            //        {
            //            this.txtAdjAmnt.Focus();
            //            return;
            //        }

            //        if (this.txtFinNo.Text == "")
            //        {
            //            this.txtFinNo.Focus();
            //            return;
            //        }
            //        if (this.txtTransfertype.Text == "")
            //        {
            //            this.txtTransfertype.Focus();
            //            return;
            //        }
            //        if (this.dtTransfer.Value == null)
            //        {
            //            this.dtTransfer.Focus();
            //            return;
            //        }

            //        if (MessageBox.Show("Do You Want To Save The Record [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //        {

            //            if (txtAdjAmnt.Text != string.Empty)
            //            {
            //                string amt = txtAdjAmnt.Text;

            //            }

            //            //base.DoToolbarActions(this.Controls, "Save");
            //            // base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));
            //            base.DoToolbarActions(this.Controls, "Save");
            //            CallReport("AUPR", "OLD");
            //            CallReport("AUP0", "NEW");
            //            txtPersonnelNo.IsRequired = false;
            //            txtFinNo.IsRequired = false;
            //            dtTransfer.IsRequired = false;


            //            //  base.ClearForm(pnlHead.Controls);
            //            base.ClearForm(PnlDetail.Controls);

            //            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FN_TransferCommand ent = new iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FN_TransferCommand();
            //            //(iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;



            //            txtPersonnelNo.IsRequired = true;
            //            txtFinNo.IsRequired = true;
            //            dtTransfer.IsRequired = true;



            //            txtFinNo.Select();
            //            txtFinNo.Focus();
            //            dtTransfer.Value = null;
            //        }
            //    }
            //}
        }
        #endregion    
    }
}