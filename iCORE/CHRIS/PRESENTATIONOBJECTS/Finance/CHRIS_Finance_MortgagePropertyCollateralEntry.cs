using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms; //audit02A
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_MortgagePropertyCollateralEntry : ChrisMasterDetailForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        # region Constructor
        public CHRIS_Finance_MortgagePropertyCollateralEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Finance_MortgagePropertyCollateralEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj): base(mainmenu, connbean_obj)
        {
            txtOption.Select();
            txtOption.Focus();

            InitializeComponent();
            
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlColletral);
            PnlFinBooking.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(PnlFinBooking);
            
        }
     #endregion
        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // txtOption.Enabled = false;
            this.txtUser.Text = this.UserName;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.tbtList.Visible = false;
             this.tbtAdd.Visible = false;
            
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            txtOption.Visible = false;
            txtPersonnelNo.Select();
            txtPersonnelNo.Focus();
        }
        protected override bool Save()
            {
                if (this.ActiveControl is SLDataGridView)
                {
                    this.DoToolbarActions(this.Controls, "Save");
                     return false; ;
                }
                // return base.Save();
                return false;
            }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "List")
            {
                if (FunctionConfig.CurrentOption == Function.None)
                {
                    return;
                }            
            }

            if (actionType == "Save")
            {
                this.txtPersonnelNo.Select();
                //this.txtPersonnelNo.Focus();
                if (!this.txtPersonnelNo.Focused)
                    return;

                if (txtFinanceNo.Text != string.Empty && txtPersonnelNo.Text != string.Empty )
                {

                    DataTable dt = (DataTable)DGVColletral.DataSource;
                    if (dt != null)
                    {
                        DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                        DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                        if (dtAdded != null || dtUpdated != null)
                        {
                            DialogResult dr = MessageBox.Show("Do you want to save changes.", "", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);
                            if (dr == DialogResult.No)
                            {
                                return;
                            }

                            else
                            {
                               // isbool = false;
                                CallReport("AUPR", "OLD");
                                base.DoToolbarActions(ctrlsCollection, actionType);
                                CallReport("AUP0", "NEW");
                                this.Cancel();
                                return;

                            }
                        }
                    } 
                               
                    
                    //base.DoToolbarActions(this.Controls, actionType);
                    //Cancel();
                    //return;
                }

                else
                {
                    return;
                
                }

            }

            if (actionType == "Delete")
            {
                if (txtFinanceNo.Text != string.Empty && txtPersonnelNo.Text != string.Empty)
                {
                   
                   //     if (DGVColletral.CurrentRow.Selected)
                        {
                            base.DoToolbarActions(this.Controls, actionType);
                            return;
                        }
                    
                }

                else
                {
                    return;

                }
                return;

            }

            if (actionType == "Cancel")
            {

                txtPersonnelNo.IsRequired = false;
                txtFinanceNo.IsRequired = false;
                base.DoToolbarActions(ctrlsCollection, actionType);
                //base.ClearForm(PnlFinBooking.Controls);
                //base.ClearForm(PnlColletral.Controls);


                txtPersonnelNo.Select();
                txtPersonnelNo.Focus();
                txtPersonnelNo.IsRequired = true;
                txtFinanceNo.IsRequired = true;
                return;
            }


            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override void CommonOnLoadMethods()
        {
            
            base.CommonOnLoadMethods();

            FN_COLLAT_NO.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_COLLAT_DESC.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_PARTY1.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_PARTY2.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_COLLAT_DT.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_DOC_RCV_DT.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
                      
            
        }
        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            //txtDESNAME.Name = "txtDESNAME";
            //txtDESNAME.Text = "";      //":GLOBAL.AUDIT_PATH||FN1";
            //pnl.Controls.Add(txtDESNAME);

            //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            //txtMODE.Name = "txtMODE";
            //txtMODE.Text = "CHARACTER";
            //pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "pno";
            txtPNO.Text = txtPersonnelNo.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "fin_No";
            txtfinNo.Text = txtFinanceNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "audit02A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }




        #endregion            
        # region Events
        private void CHRIS_Finance_MortgagePropertyCollateralEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            //if (actionType == "FinNo_MortageProperty")
            //{
            //    iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)this.PnlFinBooking.CurrentBusinessEntity;
            //    txtFinanceNo.IsRequired = true;
            //    if (FunctionConfig.CurrentOption == Function.View)
            //    {

            //        if (MessageBox.Show("Do You Want To View More Records", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //        {
            //            this.ClearForm(PnlFinBooking.Controls);
            //            this.ClearForm(PnlColletral.Controls);
            //            txtFinanceNo.IsRequired = false;
            //            txtPersonnelNo.Focus();

            //        }
            //        else
            //        {
            //            this.ClearForm(PnlFinBooking.Controls);
            //            this.ClearForm(PnlColletral.Controls);
            //            txtFinanceNo.IsRequired = false;
            //            txtOption.Select();
            //            txtOption.Clear();
            //            txtOption.Focus();
            //        }

            //    }

            //    if (FunctionConfig.CurrentOption == Function.Delete)
            //    {

            //        if (MessageBox.Show("Do You Want To Delete This Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //        {
            //            this.ClearForm(PnlFinBooking.Controls);
            //            this.ClearForm(PnlColletral.Controls);
            //            txtFinanceNo.IsRequired = false;
            //            txtPersonnelNo.Focus();

            //        }

            //        else
            //        {
            //            this.ClearForm(PnlFinBooking.Controls);
            //            this.ClearForm(PnlColletral.Controls);
            //            txtFinanceNo.IsRequired = false;
            //            txtOption.Select();
            //            txtOption.Clear();
            //            txtOption.Focus();
            //        }

            //    }
            
            //}

        }
        private void DGVColletral_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
            {

                if (e.ColumnIndex == 0)
                {
                    object obj = DGVColletral.Rows[e.RowIndex].Cells[0].EditedFormattedValue;

                    if ((DGVColletral.Rows[e.RowIndex].Cells[0].IsInEditMode) && (obj != null && obj.ToString() != string.Empty))
                    {

                        if ((e.ColumnIndex == 0) && (this.FunctionConfig.CurrentOption == Function.Add))
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("PR_P_NO", txtPersonnelNo.Text);
                            param.Add("FN_FIN_NO", txtFinanceNo.Text);
                            param.Add("FN_COLLAT_NO", DGVColletral.Rows[e.RowIndex].Cells[0].EditedFormattedValue);

                            rslt = cmnDM.GetData("CHRIS_SP_FIN_BOOK_COLLAT_MANAGER", "Collat_Chk", param);

                            if (rslt.dstResult.Tables.Count > 0)
                            {
                                if (rslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    MessageBox.Show("COLLATERAL ALREADY EXISTS...PRESS [F9] KEY FOR HELP");
                                    e.Cancel = true;
                                }
                                else
                                {
                                }
                            }
                        }


                        //if ((DGVColletral.Rows[e.RowIndex].Cells[3]dFormattedValue) || e.ColumnIndex == 4)
                        //{

                        //    object  val = DGVColletral.Rows[e.RowIndex].Cells[3].EditedFormattedValue;
                        //    if ((val.ToString() == null) || (val.ToString() == string.Empty))
                        //    {
                        //    MessageBox.Show("DATE HAS TO BE ENTERED ... ");
                        //        e.Cancel = true;
                        //    }

                        //}




                    }

                    else
                    {

                        if (DGVColletral.CurrentCell.IsInEditMode)
                        {
                        MessageBox.Show("Blank Collateral Code...PRESS [F9] KEY FOR HELP");
                         e.Cancel = true;
                        return;
                        }
                    }






                }


                    if (e.ColumnIndex == 7)
                {
                    if (DGVColletral.CurrentCell.IsInEditMode)
                    {
                       if (DGVColletral.CurrentRow.Cells["FN_COLLAT_DT"].EditedFormattedValue.ToString() == string.Empty)
                          {
                          
                          MessageBox.Show("DATE HAS TO BE ENTERED ...");
                             e.Cancel = true;

                          }
                          return;
                    }

                }

                if (e.ColumnIndex == 8)
                {
                    if (DGVColletral.CurrentCell.IsInEditMode)
                    {
                        if (DGVColletral.CurrentRow.Cells["FN_DOC_RCV_DT"].EditedFormattedValue.ToString() == string.Empty)
                        {

                            MessageBox.Show("DATE HAS TO BE ENTERED ...");
                            e.Cancel = true;

                        }
                    }

                }




        }
        /** To get the Focus To the Grid   * */
        private void DGVColletral_Enter(object sender, EventArgs e)
        {
            //DGVColletral.Rows[0].Cells[0].Selected = true;
            //DGVColletral.BeginEdit(true);

        }
        private void LBtnPersonnel_MouseDown(object sender, MouseEventArgs e)
        {

            //if (txtOption.Text == string.Empty)
            //{
            //    LBtnPersonnel.Enabled = false;
            //    LBtnPersonnel.Enabled = true;            
            
            //}
        }
        private void LBtnFinNo_MouseDown(object sender, MouseEventArgs e)
        {
            //if (txtOption.Text == string.Empty)
            //{
            //    LBtnFinNo.Enabled = false;
            //    LBtnFinNo.Enabled = true;

            //}

        }
        private void txtOption_TextChanged(object sender, EventArgs e)
        {
          
        }
        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {
            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinBookingCommand)this.PnlFinBooking.CurrentBusinessEntity;
            if (ent != null)
            {double value =0;
                if (txtPersonnelNo.Text != string.Empty)
                
                
                value = double.Parse(this.txtPersonnelNo.Text); 
                    if (value != 0)
                    {ent.PR_P_NO = double.Parse(this.txtPersonnelNo.Text);}
            }

        }
        private void DGVColletral_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!tlbMain.Items["tbtCancel"].Pressed && !tlbMain.Items["tbtClose"].Pressed )
            {

                if (((DGVColletral.CurrentRow.Cells["FN_COLLAT_NO"].EditedFormattedValue.ToString() == string.Empty)
                    || (DGVColletral.CurrentRow.Cells["FN_COLLAT_DT"].EditedFormattedValue.ToString() == string.Empty)
                    || (DGVColletral.CurrentRow.Cells["FN_DOC_RCV_DT"].EditedFormattedValue.ToString() == string.Empty)) &&
                    (!DGVColletral.CurrentRow.IsNewRow))
                {

                    MessageBox.Show("Please fill all mandatory columns.");
                    e.Cancel = true;

                }
            }

        }
        private void DGVColletral_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            this.DGVColletral.Rows[e.RowIndex].Cells["colBetween"].Value = "Between";
            this.DGVColletral.Rows[e.RowIndex].Cells["colAnd"].Value = "And";
            this.DGVColletral.Rows[e.RowIndex].Cells["colDated"].Value = "Dated";

        }
        #endregion 
    }
}