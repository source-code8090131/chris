using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Specialized;
using CrplControlLibrary;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    public partial class ChrisTabularForm : iCORE.CHRISCOMMON.PRESENTATIONOBJECTS.ChrisBaseForm
    {
        #region --Constructor Segment--

        public ChrisTabularForm()
        {
            InitializeComponent();

        }

        public ChrisTabularForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            this.connbean = connbean_obj;
            this.SetConnectionBean();
            InitializeComponent();
            this.MdiParent = mainmenu;
        }

        #endregion

        #region --Property Segment--

        [Browsable(true)]
        [Description("Gets or Sets the Form's title.")]
        [Category("FormLayout")]
        public string SetFormTitle
        {
            get { return this.lblFormTitle.Text; }
            set
            {
                this.lblFormTitle.Text = value;

                this.lblFormTitle.Top = 50;

            }
        }

        public Color FormTitleColor
        {
            set
            {
                this.lblFormTitle.ForeColor = value;
            }
        }

        #endregion

        #region --Event Segment--

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.operationMode = Mode.View;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            foreach (Control oCtrlPanel in ctrlsCollection)
            {
                if (oCtrlPanel is SLDataGridView)
                {
                    SLDataGridView dgvRecords = (SLDataGridView)oCtrlPanel;


                    switch (actionType)
                    {
                        case "Save":
                            if ((dgvRecords.Parent as SLPanel).EnableInsert || (dgvRecords.Parent as SLPanel).EnableUpdate)
                            {
                                if (dgvRecords.CurrentRow != null && !dgvRecords.CurrentRow.IsNewRow)
                                {
                                    (dgvRecords.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                                    dgvRecords.EndEdit();
                                }
                                if (dgvRecords.CurrentRow != null)
                                {
                                    dgvRecords.SLDataGridView_RowValidating(dgvRecords, new System.Windows.Forms.DataGridViewCellCancelEventArgs(0, dgvRecords.CurrentRow.Index));
                                    if (dgvRecords.CurrentRow.ErrorText.Equals(string.Empty))
                                    {
                                        dgvRecords.SaveGrid(GetCurrentPanelBlock);
                                        //dgvRecords.PopulateGrid(GetCurrentPanelBlock, true);
                                        if (this.Message.HasErrors())
                                        {
                                            operationMode = Mode.Edit;
                                        }
                                        else
                                        {
                                            operationMode = Mode.View;
                                        }
                                    }
                                    else
                                    {
                                        dgvRecords.CurrentRow.GetState(dgvRecords.CurrentRow.Index);
                                    }
                                }
                                else
                                {
                                    dgvRecords.SaveGrid(GetCurrentPanelBlock);
                                    if (this.Message.HasErrors())
                                    {
                                        operationMode = Mode.Edit;
                                    }
                                    else
                                    {
                                        operationMode = Mode.View;
                                    }
                                }
                            }
                            this.Message.ShowMessage();
                            break;
                        case "Search":
                            dgvRecords.CancelEdit();
                            dgvRecords.EndEdit();
                            dgvRecords.ResetError();
                            dgvRecords.ClearSelection();
                            dgvRecords.PopulateGrid(GetCurrentPanelBlock, false);
                            dgvRecords.CurrentMode = Enumerations.eGridMode.Search;
                            break;
                        case "Cancel":
                            if (dgvRecords.CurrentCell != null /*&& dgvRecords.CurrentCell.IsInEditMode*/)
                            {
                                dgvRecords.CancelEdit();
                                dgvRecords.EndEdit();
                                dgvRecords.ResetError();
                            }
                            if (dgvRecords.NewRowIndex != -1)
                            {
                                dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                                dgvRecords.RejectChanges();
                                //dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                                dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                                dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            }
                            else
                            {
                                if (dgvRecords.Rows.Count > 0)
                                {
                                    dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                                    dgvRecords.RejectChanges();
                                    dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                                }
                            }
                            (dgvRecords as SLDataGridView).CancelEdit();
                            (dgvRecords as SLDataGridView).EndEdit();
                            (dgvRecords as SLDataGridView).ResetError();
                            dgvRecords.GridSource.Clear();
                            operationMode = Mode.View;
                            break;
                            //currentQueryMode = Enumerations.eQueryMode.ExecuteQuery;
                            //operationMode = Mode.Cancel;
                            //if (dgvRecords.CurrentCell != null && dgvRecords.CurrentCell.IsInEditMode)
                            //{
                            //    dgvRecords.CancelEdit();
                            //    dgvRecords.EndEdit();
                            //}
                            //dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            //dgvRecords.RejectChanges();
                            ////dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            //dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                            //dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            //this.ClearForm(dgvRecords.Parent.Controls);
                            //operationMode = Mode.View;
                            //break;
                        case "Delete":
                            DialogResult response = MessageBox.Show("Are you sure you want to delete this record?", "Delete row?",
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question,
                                                        MessageBoxDefaultButton.Button2);
                            if (response == DialogResult.No)
                                return;
                            if ((dgvRecords.Parent as SLPanel).EnableDelete)
                            {
                                dgvRecords.DeleteSelectedGridRow();
                                dgvRecords.PopulateGrid(GetCurrentPanelBlock, false);
                                operationMode = Mode.Edit;
                            }
                            this.Message.ShowMessage();
                            break;
                        case "SetGridInAddMode":
                            dgvRecords.CancelEdit();
                            dgvRecords.ClearSelection();
                            dgvRecords.PopulateGrid(GetCurrentPanelBlock, true);
                            dgvRecords.CurrentMode = Enumerations.eGridMode.Add;
                            break;
                        case "SelectCurrentGridRow":
                            dgvRecords.Rows[dgvRecords.CurrentRow.Index].Selected = true;
                            break;
                        case "AddNew":
                            dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            break;
                    }
                }
                else if (oCtrlPanel is GroupBox || oCtrlPanel is SLPanelTabular)
                {
                    DoToolbarActions(oCtrlPanel.Controls, actionType);
                }
            }

            base.DoToolbarActions(ctrlsCollection, actionType);

            SetFormControls();
        }

        private void TabularForm_KeyUp(object sender, KeyEventArgs e)
        {
            base.OnFormKeyup(sender, e);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.F8)
            {
                DoToolbarActions(this.Controls, "Search");
            }
        }

        #endregion

        #region --Method Segment--

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            CreateToolbar();
            if (null != this.Controls.Owner)
            {
                this.pcbCitiGroup.Location = new Point(this.Controls.Owner.Right - 80, this.pcbCitiGroup.Top);
                this.lblFormTitle.Location = new Point(this.Controls.Owner.Width / 2 - lblFormTitle.Width / 2, this.lblFormTitle.Top);
            }

        }

        private void SetFormControls()
        {
            switch (operationMode)
            {
                case Mode.View:
                    if (tlbMain.Items.Count > 0)
                    {
                        tlbMain.Items["tbtSave"].Enabled = false;
                        //tlbMain.Items["tbtCancel"].Enabled = false;
                    }
                    break;
                case Mode.Edit:
                    if (tlbMain.Items.Count > 0)
                    {
                        tlbMain.Items["tbtSave"].Enabled = true;
                        tlbMain.Items["tbtCancel"].Enabled = true;
                    }
                    break;
            }
        }

        protected override void CreateToolbar()
        {
            // tbtSearch
            ToolStripButton tbtSearch = new ToolStripButton();
            tbtSearch.Image = iCORE.Properties.Resources.Edit_Record;
            tbtSearch.ImageScaling = ToolStripItemImageScaling.None;
            tbtSearch.ImageTransparentColor = Color.Magenta;
            tbtSearch.Name = "tbtSearch";
            tbtSearch.Size = new Size(44, 33);
            tbtSearch.Text = "&Search";
            tbtSearch.TextImageRelation = TextImageRelation.ImageAboveText;
            tlbMain.Items.Insert(3, tbtSearch);

            // tbtSearch
            tlbMain.Items.Remove(tbtAdd);
            tlbMain.Items.Remove(tbtEdit);
            tlbMain.Items.Remove(tbtList);
            SetFormControls();
        }

        /// <summary>
        /// Performs Add Function
        /// </summary>
        protected override bool Add()
        {
            bool flag = false;

            this.operationMode = Mode.Add;
            this.FunctionConfig.CurrentOption = Function.Add;
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Add";
            this.txtOption.Text = "A";

            return flag;
        }
        /// <summary>
        /// Performs Edit Function
        /// </summary>
        protected override bool Edit()
        {
            bool flag = false;

            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Modify";
            this.txtOption.Text = "M";
            this.FunctionConfig.CurrentOption = Function.Modify;

            return flag;
        }
        /// <summary>
        /// Performs Delete Function
        /// </summary>
        protected override bool Delete()
        {
            bool flag = false;

            //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtEdit"]));
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Delete";
            this.txtOption.Text = "D";
            //this.FunctionConfig.CurrentOption = Function.Modify;
            this.FunctionConfig.CurrentOption = Function.Delete;
            //this.tbtDelete.PerformClick();

            return flag;
        }
        #endregion
    }
}