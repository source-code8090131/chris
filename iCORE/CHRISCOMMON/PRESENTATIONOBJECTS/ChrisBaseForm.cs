using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using CrplControlLibrary;

namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    public partial class ChrisBaseForm : iCORE.Common.PRESENTATIONOBJECTS.Cmn.BaseForm
    {

        #region Fields

        private FunctionConfig _functionCfg = new FunctionConfig();
        private TextBox _currentOptionText = null;

        private DateTime _now = DateTime.Now;
        private string _location = "";
        private string _userName = String.Empty;
        //********DisableControlsOnNoneFunction*******
        private bool _canEnableDisableControls = false;
        private string _globalReportPath = @"C:\SPOOL\CHRIS\AUDIT\";

        #endregion

        #region Constructors

        public ChrisBaseForm()
        {
            InitializeComponent();
        }
        public ChrisBaseForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        public DateTime Now()
        {
            DateTime dTime = DateTime.Now;

            DataSet dst = new DataSet();
            DataRow drw = null;

            dst.Tables.Add(SQLManager.GetSPParams("CHRIS_SP_TODAY"));

            drw = dst.Tables["CHRIS_SP_TODAY"].NewRow();

            //if (drw.Table.Columns.Contains("LOVCodeType"))
            //    drw["LOVCodeType"] = pstrLOVType;

            dst.Tables["CHRIS_SP_TODAY"].Rows.Add(drw);
            dst.Tables["CHRIS_SP_TODAY"].AcceptChanges();

            Result rslt = SQLManager.SelectDataSet(dst);

            if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
            {
                dTime = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0][0]);
            }

            return dTime;
        }

        #region Code to intercept the Close Box (X button), which will call Validating routines

        public enum WindowMessages
        {
            WM_SYSCOMMAND = 0x0112,
            SC_CLOSE = 0xf060
        }
        protected override void WndProc(ref Message msg)
        {
            if (msg.Msg == (int)WindowMessages.WM_SYSCOMMAND)
            {
                if (msg.WParam.ToInt32() == (int)WindowMessages.SC_CLOSE)
                {
                    this.AutoValidate = AutoValidate.Disable;
                }
            }
            // Call base class function
            base.WndProc(ref msg);
        }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!this.DesignMode)
            {
                this._now = this.Now();
                this.tsItmDate.Text = this._now.ToString("dd-MM-yyyy");
                this.tsItmTime.Text = this._now.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("en-us"));//this._now.ToLongTimeString();

                this.timer1.Enabled = true;
                if (this.CanEnableDisableControls)
                {
                    IterateFormToEnableControls(this.Controls, false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            base.DoToolbarActions(ctrlsCollection, actionType);

            switch (actionType)
            {
                case "Add":
                    this.FunctionConfig.CurrentOption = Function.Add;
                    if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Add";
                    this.txtOption.Text = "A";
                    //this.operationMode = Mode.Add;
                    break;
                case "Edit":
                    if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Modify";
                    this.txtOption.Text = "M";
                    this.FunctionConfig.CurrentOption = Function.Modify;
                    break;
                case "Cancel":
                    if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "";
                    if (this.txtOption.Visible)
                    {
                        this.txtOption.Text = "";
                        this.txtOption.Select();
                        this.txtOption.Focus();
                    }
                    this.FunctionConfig.CurrentOption = Function.None;
                    if (this.txtOption.Visible)
                    {
                        if (this.CanEnableDisableControls)
                            this.IterateFormToEnableControls(this.Controls, false);
                    }
                    break;
                case "Save":
                    if (operationMode == Mode.Add)
                    {
                        if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "";
                        this.txtOption.Text = "";
                        this.txtOption.Select();
                        this.FunctionConfig.CurrentOption = Function.None;
                    }
                    break;
                case "List":
                    this.FunctionConfig.CurrentOption = Function.Modify;
                    if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Modify";
                    this.txtOption.Text = "M";
                    break;
                case "Delete":
                    //DoFunction(Keys.F1, Function.Delete);
                    this.FunctionConfig.CurrentOption = Function.None;
                    break;
            }

            //this.txtOption.Select();
            //this.txtOption.Focus();

        }

        #region Methods to handle function key's events.
        /// <summary>
        /// GetCurrentFunction
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private Function GetCurrentFunction(Keys key)
        {
            Function currentFunc = Function.None;

            switch (key)
            {
                case Keys.F1:
                    currentFunc = this.FunctionConfig.F1;
                    break;
                case Keys.F2:
                    currentFunc = this.FunctionConfig.F2;
                    break;
                case Keys.F3:
                    currentFunc = this.FunctionConfig.F3;
                    break;
                case Keys.F4:
                    currentFunc = this.FunctionConfig.F4;
                    break;
                case Keys.F5:
                    currentFunc = this.FunctionConfig.F5;
                    break;
                case Keys.F7:
                    currentFunc = this.FunctionConfig.F7;
                    break;
                case Keys.F8:
                    currentFunc = this.FunctionConfig.F8;
                    break;
                case Keys.F9:
                    currentFunc = this.FunctionConfig.F9;
                    break;
                case Keys.F10:
                    currentFunc = this.FunctionConfig.F10;
                    break;
                case Keys.F11:
                    currentFunc = this.FunctionConfig.F11;
                    break;
                case Keys.F12:
                    currentFunc = this.FunctionConfig.F12;
                    break;
                //return currentFunc;

            }
            return currentFunc;
        }

        /// <summary>
        /// Shortcut Implementation of oracle forms
        /// </summary>
        /// <param name="KeyEventArgs e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (!this.txtOption.Visible)
            {
                if (e.KeyCode == Keys.F1 && this.FunctionConfig.EnableF1)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF1;
                    this.DoFunction(this.FunctionConfig.F1);
                }
                else if (e.KeyCode == Keys.F2 && this.FunctionConfig.EnableF2)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF2;
                    this.DoFunction(this.FunctionConfig.F2);
                }
                else if (e.KeyCode == Keys.F3 && this.FunctionConfig.EnableF3)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF3;
                    this.DoFunction(this.FunctionConfig.F3);
                }
                else if (e.KeyCode == Keys.F4 && this.FunctionConfig.EnableF4)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF4;
                    this.DoFunction(this.FunctionConfig.F4);
                }
                else if (e.KeyCode == Keys.F5 && this.FunctionConfig.EnableF5)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF5;
                    this.DoFunction(this.FunctionConfig.F5);
                }
                //else if (e.KeyCode == Keys.F6 && this.FunctionConfig.EnableF6)
                //{
                //    //this.txtOption.Text = this.FunctionConfig.OptionLetterF6;
                //    this.DoFunction(this.FunctionConfig.F6);
                //}
                else if (e.KeyCode == Keys.F7 && this.FunctionConfig.EnableF7)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF7;
                    this.DoFunction(this.FunctionConfig.F7);
                }
                else if (e.KeyCode == Keys.F8 && this.FunctionConfig.EnableF8)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF8;
                    this.DoFunction(this.FunctionConfig.F8);
                }
                else if (e.KeyCode == Keys.F9 && this.FunctionConfig.EnableF9)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF9;
                    this.DoFunction(this.FunctionConfig.F9);
                }
                else if (e.KeyCode == Keys.F10 && this.FunctionConfig.EnableF10)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF10;
                    this.DoFunction(this.FunctionConfig.F10);
                }
                else if (e.KeyCode == Keys.F11 && this.FunctionConfig.EnableF11)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF11;
                    this.DoFunction(this.FunctionConfig.F11);
                }
                else if (e.KeyCode == Keys.F12 && this.FunctionConfig.EnableF12)
                {
                    this.txtOption.Text = this.FunctionConfig.OptionLetterF12;
                    this.DoFunction(this.FunctionConfig.F12);
                }

            }
            if (this.txtOption.Visible)
            {
                Function currentFunc = Function.None;
                if (e.KeyCode == Keys.F1 || e.KeyCode == Keys.F2 || e.KeyCode == Keys.F3 || e.KeyCode == Keys.F4 || e.KeyCode == Keys.F5 || e.KeyCode == Keys.F7 || e.KeyCode == Keys.F8 || e.KeyCode == Keys.F9 || e.KeyCode == Keys.F10 || e.KeyCode == Keys.F11 || e.KeyCode == Keys.F12)
                {
                    currentFunc = GetCurrentFunction(e.KeyCode);
                }

                if ((this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.Delete) && currentFunc == Function.Save)
                {
                    if (e.KeyCode == Keys.F1 && this.FunctionConfig.EnableF1)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF1;
                        this.DoFunction(this.FunctionConfig.F1);
                    }
                    else if (e.KeyCode == Keys.F2 && this.FunctionConfig.EnableF2)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF2;
                        this.DoFunction(this.FunctionConfig.F2);
                    }
                    else if (e.KeyCode == Keys.F3 && this.FunctionConfig.EnableF3)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF3;
                        this.DoFunction(this.FunctionConfig.F3);
                    }
                    else if (e.KeyCode == Keys.F4 && this.FunctionConfig.EnableF4)
                    {
                        if (this.FunctionConfig.F4 != Function.Save)
                        {
                            this.txtOption.Text = this.FunctionConfig.OptionLetterF4;
                        }
                        this.DoFunction(this.FunctionConfig.F4);
                    }
                    else if (e.KeyCode == Keys.F5 && this.FunctionConfig.EnableF5)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF5;
                        this.DoFunction(this.FunctionConfig.F5);
                    }
                    //else if (e.KeyCode == Keys.F6 && this.FunctionConfig.EnableF6)
                    //{
                    //    //this.txtOption.Text = this.FunctionConfig.OptionLetterF6;
                    //    this.DoFunction(this.FunctionConfig.F6);
                    //}
                    else if (e.KeyCode == Keys.F7 && this.FunctionConfig.EnableF7)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF7;
                        this.DoFunction(this.FunctionConfig.F7);
                    }
                    else if (e.KeyCode == Keys.F8 && this.FunctionConfig.EnableF8)
                    {
                        if (this.FunctionConfig.F8 != Function.Save)
                        {
                            this.txtOption.Text = this.FunctionConfig.OptionLetterF8;
                        }
                        this.DoFunction(this.FunctionConfig.F8);
                    }
                    else if (e.KeyCode == Keys.F9 && this.FunctionConfig.EnableF9)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF9;
                        this.DoFunction(this.FunctionConfig.F9);
                    }
                    else if (e.KeyCode == Keys.F10 && this.FunctionConfig.EnableF10)
                    {
                        if (this.FunctionConfig.F10 != Function.Save)
                        {
                            this.txtOption.Text = this.FunctionConfig.OptionLetterF10;
                        }
                        this.DoFunction(this.FunctionConfig.F10);
                    }
                    else if (e.KeyCode == Keys.F11 && this.FunctionConfig.EnableF11)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF11;
                        this.DoFunction(this.FunctionConfig.F11);
                    }
                    else if (e.KeyCode == Keys.F12 && this.FunctionConfig.EnableF12)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF12;
                        this.DoFunction(this.FunctionConfig.F12);
                    }
                }
                if (this.FunctionConfig.CurrentOption == Function.None && currentFunc != Function.Save)
                {
                    if (e.KeyCode == Keys.F1 && this.FunctionConfig.EnableF1)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF1;
                        this.DoFunction(this.FunctionConfig.F1);
                    }
                    else if (e.KeyCode == Keys.F2 && this.FunctionConfig.EnableF2)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF2;
                        this.DoFunction(this.FunctionConfig.F2);
                    }
                    else if (e.KeyCode == Keys.F3 && this.FunctionConfig.EnableF3)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF3;
                        this.DoFunction(this.FunctionConfig.F3);
                    }
                    else if (e.KeyCode == Keys.F4 && this.FunctionConfig.EnableF4)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF4;
                        this.DoFunction(this.FunctionConfig.F4);
                    }
                    else if (e.KeyCode == Keys.F5 && this.FunctionConfig.EnableF5)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF5;
                        this.DoFunction(this.FunctionConfig.F5);
                    }
                    //else if (e.KeyCode == Keys.F6 && this.FunctionConfig.EnableF6)
                    //{
                    //    //this.txtOption.Text = this.FunctionConfig.OptionLetterF6;
                    //    this.DoFunction(this.FunctionConfig.F6);
                    //}
                    else if (e.KeyCode == Keys.F7 && this.FunctionConfig.EnableF7)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF7;
                        this.DoFunction(this.FunctionConfig.F7);
                    }
                    else if (e.KeyCode == Keys.F8 && this.FunctionConfig.EnableF8)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF8;
                        this.DoFunction(this.FunctionConfig.F8);
                    }
                    else if (e.KeyCode == Keys.F9 && this.FunctionConfig.EnableF9)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF9;
                        this.DoFunction(this.FunctionConfig.F9);
                    }
                    else if (e.KeyCode == Keys.F10 && this.FunctionConfig.EnableF10)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF10;
                        this.DoFunction(this.FunctionConfig.F10);
                    }
                    else if (e.KeyCode == Keys.F11 && this.FunctionConfig.EnableF11)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF11;
                        this.DoFunction(this.FunctionConfig.F11);
                    }
                    else if (e.KeyCode == Keys.F12 && this.FunctionConfig.EnableF12)
                    {
                        this.txtOption.Text = this.FunctionConfig.OptionLetterF12;
                        this.DoFunction(this.FunctionConfig.F12);
                    }
                }
            }
            if (e.KeyCode == Keys.F6 && this.FunctionConfig.EnableF6)
            {
                //this.txtOption.Text = this.FunctionConfig.OptionLetterF6;
                this.DoFunction(this.FunctionConfig.F6);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Cancel();
            }
            //else
            {
                //base.OnKeyDown(e);
            }
        }
        /// <summary>
        /// Performs action on Function key
        /// </summary>
        /// <param name="Function function"></param>
        protected virtual void DoFunction(Function function)
        {
            //this.FunctionConfig.CurrentOption = function;
            this.txtOption.Enabled = false;
            this.txtOption.Enabled = true;
            //this.Select();
            //this.Focus();
            switch (function)
            {
                case Function.Add:
                    this.Add();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;

                case Function.Modify:
                    this.Edit();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;

                case Function.Save:
                    this.Save();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;

                case Function.Delete:
                    this.Delete();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;

                case Function.Cancel:
                    this.Cancel();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;

                case Function.View:
                    this.View();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;

                case Function.Quit:
                    this.Quit();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, false);
                    }
                    break;

                case Function.Query:
                    this.Query();
                    if (this.CanEnableDisableControls)
                    {
                        IterateFormToEnableControls(this.Controls, true);
                    }
                    break;
            }
            //this.txtOption.Enabled = true;
        }
        /// <summary>
        /// enable/disable controls of form depending upon isEnable value.
        /// </summary>
        /// <param name="oCtrls"></param>
        /// <param name="isEnable"></param>
        protected void IterateFormToEnableControls(Control.ControlCollection oCtrls, bool isEnable)
        {
            foreach (Control oLoopControl in oCtrls)
            {
                if (oLoopControl is SLPanelSimple || oLoopControl is SLPanelSimpleList || oLoopControl is GroupBox || oLoopControl is TabControl || oLoopControl is TabPage)
                {
                    IterateFormToEnableControls(oLoopControl.Controls, isEnable);
                }
                else if (oLoopControl is LookupButton || oLoopControl is SLDatePicker || oLoopControl is SLComboBox)
                {
                    if (oLoopControl is SLDatePicker)
                    {
                        if (((SLDatePicker)oLoopControl).CustomEnabled)
                            oLoopControl.Enabled = isEnable;
                    }
                    else
                        oLoopControl.Enabled = isEnable;

                }

            }
            // this.tbtCancel.Enabled = isEnable;

        }
        #endregion

        #region Methods to perform functions on function keys

        /// <summary>
        /// Performs Add Function
        /// </summary>
        protected virtual bool Add()
        {
            bool flag = false;

            //this.tbtAdd.PerformClick();
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtAdd"]));
            //if (this.FunctionConfig.CurrrentOptionTextBox != null) this.FunctionConfig.CurrrentOptionTextBox.Text = "Add";
            //this.operationMode = Mode.Add;

            return flag;
        }
        /// <summary>
        /// Performs Edit Function
        /// </summary>
        protected virtual bool Edit()
        {
            bool flag = false;

            //this.tbtEdit.PerformClick();
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtEdit"]));
            //if (this.FunctionConfig.CurrrentOptionTextBox != null) this.FunctionConfig.CurrrentOptionTextBox.Text = "Modify";
            //this.operationMode = Mode.Edit;

            return flag;
        }
        /// <summary>
        /// Performs Save Function
        /// </summary>
        protected virtual bool Save()
        {
            bool flag = false;

            //this.tbtSave.PerformClick();
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));

            return flag;
        }
        /// <summary>
        /// Performs Delete Function
        /// </summary>
        protected virtual bool Delete()
        {
            bool flag = false;

            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtEdit"]));
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "Delete";
            this.txtOption.Text = "D";
            this.FunctionConfig.CurrentOption = Function.Delete;

            //this.tbtDelete.PerformClick();

            return flag;
        }
        /// <summary>
        /// Performs Cancel/Clear Form
        /// </summary>
        protected virtual bool Cancel()
        {
            bool flag = false;

            //this.tbtCancel.PerformClick();
            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));

            return flag;
        }
        /// <summary>
        /// Performs List/Query Function
        /// </summary>
        protected virtual bool View()
        {
            bool flag = false;

            //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtList"]));
            if (this.CurrrentOptionTextBox != null) this.CurrrentOptionTextBox.Text = "View";
            this.FunctionConfig.CurrentOption = Function.View;

            return flag;
        }
        /// <summary>
        /// Performs Exit/Quit Function
        /// </summary>
        protected virtual bool Quit()
        {
            bool flag = false;
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            }
            this.txtOption.Select();
            this.txtOption.Focus();
            return flag;
        }
        /// <summary>
        /// Performs Query Function
        /// </summary>
        protected virtual bool Query()
        {
            bool flag = false;
            this.FunctionConfig.CurrentOption = Function.Query;
            return flag;
        }

        #endregion

        #endregion

        #region Properties
        public string GlobalReportPath
        {
            get { return this._globalReportPath; }
            //get
            //{
            //    if (reprintXMSFunctionID.Equals(String.Empty))
            //    {
            //        DATAOBJECTS.CmnDataManager cmnDM = new DATAOBJECTS.CmnDataManager();
            //        Dictionary<String, Object> paramWithVals = new Dictionary<String, Object>();
            //        iCORE.Common.Result rsltCode;
            //        System.Data.DataSet dsResult = new System.Data.DataSet();

            //        paramWithVals.Clear();
            //        paramWithVals.Add("NAME", String.Empty);
            //        paramWithVals.Add("VALUE", String.Empty);
            //        paramWithVals.Add("CITI_BRN_CD", CityCode);
            //        paramWithVals.Add("ID", 0);
            //        paramWithVals.Add("ActionType", "List");
            //        paramWithVals.Add("oRetVal", 0);
            //        rsltCode = cmnDM.GetData("DDS_SP_DDS_PARAMETERS_MANAGER", "List", paramWithVals);
            //        for (int index = 0; index < rsltCode.dstResult.Tables[0].Rows.Count; index++)
            //        {
            //            if (rsltCode.dstResult.Tables[0].Rows[index].ItemArray[1].ToString().Equals("REPRINT_AUTH_SCREEN_ID"))
            //            {
            //                reprintXMSFunctionID = rsltCode.dstResult.Tables[0].Rows[index].ItemArray[2].ToString();
            //                break;
            //            }
            //        }
            //        return reprintXMSFunctionID;
            //    }
            //    else
            //    {
            //        return reprintXMSFunctionID;
            //    }
            //}
            //set { reprintXMSFunctionID = value; }
        }

     


        [Browsable(true)]
        [DefaultValue(false)]
        [Description("Enables/Disables Clickable Controls(combo,lookup,datetimepicker).")]
        public bool CanEnableDisableControls
        {
            get { return this._canEnableDisableControls; }
            set { this._canEnableDisableControls = value; }
        }

        #region Miscellenous
        /// <summary>
        /// Get the User Name.
        /// </summary>
        [Browsable(true)]
        [Description("Get the User Name.")]
        public string UserName
        {
            get
            {
                if (this._userName.Equals(String.Empty))
                {
                    iCORE.CHRIS.DATAOBJECTS.CmnDataManager cmnDM = new iCORE.CHRIS.DATAOBJECTS.CmnDataManager();
                    Dictionary<String, Object> paramWithVals = new Dictionary<String, Object>();
                    iCORE.Common.Result rsltCode;
                    System.Data.DataSet dsResult = new System.Data.DataSet();

                    paramWithVals.Clear();
                    paramWithVals.Add("SOEID", (this.MdiParent != null) ? (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId() : String.Empty);
                    paramWithVals.Add("USER_NAME", String.Empty);
                    paramWithVals.Add("ID", 0);
                    paramWithVals.Add("ActionType", "GetUserName");
                    paramWithVals.Add("oRetVal", 0);
                    rsltCode = cmnDM.GetData("CHRIS_Setup_Sp_CHRIS_USERS_MANAGER", "GetUserName", paramWithVals);
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            this._userName = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        }
                    }
                    return this._userName;
                }
                else
                {
                    return this._userName;
                }
            }
        }

        /// <summary>
        /// Get/Set the status bar visible.
        /// </summary>
        [Browsable(true)]
        [Description("Gets the Current Location.")]
        public bool ShowStatusBar
        {
            get
            {
                return this.tsBottom.Visible;
            }
            set
            {
                this.tsBottom.Visible = value;
            }
        }

        /// <summary>
        /// Gets the Current Location.
        /// </summary>
        [Browsable(true)]
        [Description("Gets the Current Location.")]
        public string CurrentLocation
        {
            get
            {
                return this._location;
            }
        }

        /// <summary>
        /// Gets the Current DateTime.
        /// </summary>
        [Browsable(true)]
        [Description("Gets the Current DateTime.")]
        public DateTime CurrentDate
        {
            get
            {
                return this._now;
            }
        }

        /// <summary>
        /// Shows/Hides the bottom panle.
        /// </summary>
        [Browsable(true)]
        [Description("Shows/Hides the bottom panle.")]
        public bool ShowBottomBar
        {
            get
            {
                return this.tsBottom.Visible;
            }
            set
            {
                this.tsBottom.Visible = value;
            }
        }

        /// <summary>
        /// Allow user to configure form's function.
        /// </summary>
        [Browsable(true)]
        [Description("Allow user to configure form's function")]
        public FunctionConfig FunctionConfig
        {
            set
            {
                this._functionCfg = value;
            }
            get
            {
                //return this.stsOptions.Visible;
                return this._functionCfg;
            }
        }

        #endregion

        #region Show Function Options

        /// <summary>
        /// To display the currrent option.
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set TextBox To display the currrent option.")]
        public TextBox CurrrentOptionTextBox
        {
            set
            {
                this._currentOptionText = value;
            }
            get
            {
                return this._currentOptionText;
            }
        }

        /// <summary>
        /// Enable function keys.
        /// </summary>
        [Browsable(true)]
        [Description("Show function keys")]
        public bool ShowOptionKeys
        {
            set
            {
                this.pnlBottom.Visible = value;
                //this.txtOption.Visible = value;
            }
            get
            {
                //return this.stsOptions.Visible;
                return this.pnlBottom.Visible;
            }
        }
        /// <summary>
        /// Show Option TextBox.
        /// </summary>
        [Browsable(true)]
        [Description("Show Option TextBox.")]
        public bool ShowOptionTextBox
        {
            set
            {
                this.txtOption.Visible = value;
            }
            get
            {
                return this.txtOption.Visible;
            }
        }
        /// <summary>
        /// Show Option TextBox.
        /// </summary>
        [Browsable(true)]
        [Description("Show Option TextBox.")]
        public bool ShowTextOption
        {
            set
            {
                this.tsOptions.Visible = value;
            }
            get
            {
                return this.tsOptions.Visible;
            }
        }
        /// <summary>
        /// F1 key for add..
        /// </summary>
        [Browsable(true)]
        [Description("Show F1 Option.")]
        public bool ShowF1Option
        {
            set
            {
                this.tsF1.Visible = value;
            }
            get
            {
                return this.tsF1.Visible;
            }
        }
        /// <summary>
        /// F2 key for add..
        /// </summary>
        [Browsable(true)]
        [Description("Show F2 Option.")]
        public bool ShowF2Option
        {
            set
            {
                this.tsF2.Visible = value;
            }
            get
            {
                return this.tsF2.Visible;
            }
        }
        /// <summary>
        /// F3 key for record view.
        /// </summary>
        [Browsable(true)]
        [Description("Show F3 Option.")]
        public bool ShowF3Option
        {
            set
            {
                this.tsF3.Visible = value;
            }
            get
            {
                return this.tsF3.Visible;
            }
        }
        /// <summary>
        /// F4 key for delete.
        /// </summary>
        [Browsable(true)]
        [Description("Show F4 Option.")]
        public bool ShowF4Option
        {
            set
            {
                this.tsF4.Visible = value;
            }
            get
            {
                return this.tsF4.Visible;
            }
        }
        /// <summary>
        /// F5 key for record view.
        /// </summary>
        [Browsable(true)]
        [Description("Show F5 Option.")]
        public bool ShowF5Option
        {
            set
            {
                this.tsF5.Visible = value;
            }
            get
            {
                return this.tsF5.Visible;
            }
        }
        /// <summary>
        /// F6 key for exit the form.
        /// </summary>
        [Browsable(true)]
        [Description("Show F6 Option.")]
        public bool ShowF6Option
        {
            set
            {
                this.tsF6.Visible = value;
            }
            get
            {
                return this.tsF6.Visible;
            }
        }
        /// <summary>
        /// F7 key for modify.
        /// </summary>
        [Browsable(true)]
        [Description("Show F7 Option.")]
        public bool ShowF7Option
        {
            set
            {
                this.tsF7.Visible = value;
            }
            get
            {
                return this.tsF7.Visible;
            }
        }
        /// <summary>
        /// F8 key query.
        /// </summary>
        [Browsable(true)]
        [Description("Show F8 Option.")]
        public bool ShowF8Option
        {
            set
            {
                this.tsF8.Visible = value;
            }
            get
            {
                return this.tsF8.Visible;
            }
        }
        /// <summary>
        /// F9 key for record view.
        /// </summary>
        [Browsable(true)]
        [Description("Show F9 Option.")]
        public bool ShowF9Option
        {
            set
            {
                this.tsF9.Visible = value;
            }
            get
            {
                return this.tsF9.Visible;
            }
        }
        /// <summary>
        /// F10 key for record view.
        /// </summary>
        [Browsable(true)]
        [Description("Show F10 Option.")]
        public bool ShowF10Option
        {
            set
            {
                this.tsF10.Visible = value;
            }
            get
            {
                return this.tsF10.Visible;
            }
        }
        /// <summary>
        /// F11 key for record view.
        /// </summary>
        [Browsable(true)]
        [Description("Show F11 Option.")]
        public bool ShowF11Option
        {
            set
            {
                this.tsF11.Visible = value;
            }
            get
            {
                return this.tsF11.Visible;
            }
        }
        /// <summary>
        /// F12 key for record view.
        /// </summary>
        [Browsable(true)]
        [Description("Show F12 Option.")]
        public bool ShowF12Option
        {
            set
            {
                this.tsF12.Visible = value;
            }
            get
            {
                return this.tsF12.Visible;
            }
        }

        #endregion

        #region Function Key Caption

        /// <summary>
        /// Set F1 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F1 Option Text.")]
        public string F1OptionText
        {
            set
            {
                this.tsF1.Text = "[F1]=" + value;
            }
            get
            {
                return this.tsF1.Text;
            }

        }
        ///// <summary>
        ///// Set F2 Option Text.
        ///// </summary>
        //[Browsable(true)]
        //[Description("Set F2 Option Text.")]
        //public bool F2OptionText
        //{
        //    set
        //    {
        //        this.tsF2.Text = "[F2]=" + value;
        //    }
        //}

        /// <summary>
        /// Set F3 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F3 Option Text.")]
        public string F3OptionText
        {
            set
            {
                //this.tsF3.Text = "[F3]=" + value;
                this.tsF3.Text = value;
            }
            get
            {
                return this.tsF3.Text;
            }
        }
        /// <summary>
        /// Set F4 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F4 Option Text.")]
        public string F4OptionText
        {
            set
            {
                //this.tsF4.Text = "[F4]=" + value;
                this.tsF4.Text = value;
            }
            get
            {
                return this.tsF4.Text;
            }
        }
        ///// <summary>
        ///// Set F5 Option Text.
        ///// </summary>
        //[Browsable(true)]
        //[Description("Set F5 Option Text.")]
        //public bool F5OptionText
        //{
        //    set
        //    {
        //        this.tsF5.Text = "[F5]=" + value;
        //    }
        //}
        /// <summary>
        /// Set F6 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F6 Option Text.")]
        public string F6OptionText
        {
            set
            {
                //this.tsF6.Text = "[F6]=" + value;
                this.tsF6.Text = value;
            }
            get
            {
                return this.tsF6.Text;
            }
        }
        /// <summary>
        /// Set F7 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F7 Option Text.")]
        public string F7OptionText
        {
            set
            {
                //this.tsF7.Text = "[F7]=" + value;
                this.tsF7.Text = value;
            }
            get
            {
                return this.tsF7.Text;
            }
        }
        /// <summary>
        /// Set F8 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F8 Option Text.")]
        public string F8OptionText
        {
            set
            {
                //this.tsF8.Text = "[F8]=" + value;
                this.tsF8.Text = value;
            }
            get
            {
                return this.tsF8.Text;
            }
        }

        ///// <summary>
        ///// Set F9 Option Text.
        ///// </summary>
        //[Browsable(true)]
        //[Description("Set F9 Option Text.")]
        //public bool F9OptionText
        //{
        //    set
        //    {
        //        this.tsF9.Text = "[F9]=" + value;
        //    }
        //}
        /// <summary>
        /// Set F10 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F10 Option Text.")]
        public string F10OptionText
        {
            set
            {
                this.tsF10.Text = value;
            }
            get
            {
                return this.tsF10.Text;
            }
        }

        /// <summary>
        /// Set F11 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F11 Option Text.")]
        public string F11OptionText
        {
            set
            {
                this.tsF11.Text = value;
            }
            get
            {
                return this.tsF11.Text;
            }
        }
        /// <summary>
        /// Set F12 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F12 Option Text.")]
        public string F12OptionText
        {
            set
            {
                this.tsF12.Text = value;
            }
            get
            {
                return this.tsF12.Text;
            }
        }
        /// <summary>
        /// Set F2 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F2 Option Text.")]
        public string F2OptionText
        {
            set
            {
                this.tsF2.Text = value;
            }
            get
            {
                return this.tsF2.Text;
            }
        }
        /// <summary>
        /// Set F5 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F5 Option Text.")]
        public string F5OptionText
        {
            set
            {
                this.tsF5.Text = value;
            }
            get
            {
                return this.tsF5.Text;
            }
        }
        /// <summary>
        /// Set F9 Option Text.
        /// </summary>
        [Browsable(true)]
        [Description("Set F9 Option Text.")]
        public string F9OptionText
        {
            set
            {
                this.tsF9.Text = value;
            }
            get
            {
                return this.tsF9.Text;
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Sets the current function according to option letter.
        /// </summary>
        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
            if (txtOption.Visible && txtOption.Text == "")
            {
                this.FunctionConfig.CurrentOption = Function.None;
                e.Cancel = true;
                txtOption.Focus();
                txtOption.Select();
                return;
            }
            //string letter = txtOption.Text;

            //if (letter == this.FunctionConfig.OptionLetterF1) { this.OnKeyDown(new KeyEventArgs(Keys.F1)); }
            //if (letter == this.FunctionConfig.OptionLetterF2) { this.OnKeyDown(new KeyEventArgs(Keys.F2)); }
            //if (letter == this.FunctionConfig.OptionLetterF3) { this.OnKeyDown(new KeyEventArgs(Keys.F3)); }
            //if (letter == this.FunctionConfig.OptionLetterF4) { this.OnKeyDown(new KeyEventArgs(Keys.F4)); }
            //if (letter == this.FunctionConfig.OptionLetterF5) { this.OnKeyDown(new KeyEventArgs(Keys.F5)); }
            //if (letter == this.FunctionConfig.OptionLetterF6) { this.OnKeyDown(new KeyEventArgs(Keys.F6)); }
            //if (letter == this.FunctionConfig.OptionLetterF7) { this.OnKeyDown(new KeyEventArgs(Keys.F7)); }
            //if (letter == this.FunctionConfig.OptionLetterF8) { this.OnKeyDown(new KeyEventArgs(Keys.F8)); }
            //if (letter == this.FunctionConfig.OptionLetterF9) { this.OnKeyDown(new KeyEventArgs(Keys.F9)); }
            //if (letter == this.FunctionConfig.OptionLetterF10) { this.OnKeyDown(new KeyEventArgs(Keys.F10)); }
            //if (letter == this.FunctionConfig.OptionLetterF11) { this.OnKeyDown(new KeyEventArgs(Keys.F11)); }
            //if (letter == this.FunctionConfig.OptionLetterF12) { this.OnKeyDown(new KeyEventArgs(Keys.F12)); }


        }
        /// <summary>
        /// Handles the option text key letter.
        /// </summary>
        private void txtOption_KeyPress(object sender, KeyPressEventArgs e)
        {
            string letter = e.KeyChar.ToString().ToLower();

            if (letter == this.FunctionConfig.OptionLetterF1.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF2.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF3.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF4.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF5.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF6.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF7.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF8.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF9.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF10.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF11.ToLower()) { e.Handled = false; }
            else if (letter == this.FunctionConfig.OptionLetterF12.ToLower()) { e.Handled = false; }
            else if (e.KeyChar == '\b') { e.Handled = false; }
            else { e.Handled = true; }

            if (e.KeyChar == 13 || e.KeyChar == '\t')
            {
                letter = txtOption.Text;
                if (letter == "")
                {
                    e.Handled = false;
                    this.txtOption.Select();
                    this.txtOption.Focus();
                    return;
                }
                if (letter == this.FunctionConfig.OptionLetterF1) { this.OnKeyDown(new KeyEventArgs(Keys.F1)); }
                if (letter == this.FunctionConfig.OptionLetterF2) { this.OnKeyDown(new KeyEventArgs(Keys.F2)); }
                if (letter == this.FunctionConfig.OptionLetterF3) { this.OnKeyDown(new KeyEventArgs(Keys.F3)); }
                if (letter == this.FunctionConfig.OptionLetterF4) { this.OnKeyDown(new KeyEventArgs(Keys.F4)); }
                if (letter == this.FunctionConfig.OptionLetterF5) { this.OnKeyDown(new KeyEventArgs(Keys.F5)); }
                if (letter == this.FunctionConfig.OptionLetterF6) { this.OnKeyDown(new KeyEventArgs(Keys.F6)); }
                if (letter == this.FunctionConfig.OptionLetterF7) { this.OnKeyDown(new KeyEventArgs(Keys.F7)); }
                if (letter == this.FunctionConfig.OptionLetterF8) { this.OnKeyDown(new KeyEventArgs(Keys.F8)); }
                if (letter == this.FunctionConfig.OptionLetterF9) { this.OnKeyDown(new KeyEventArgs(Keys.F9)); }
                if (letter == this.FunctionConfig.OptionLetterF10) { this.OnKeyDown(new KeyEventArgs(Keys.F10)); }
                if (letter == this.FunctionConfig.OptionLetterF11) { this.OnKeyDown(new KeyEventArgs(Keys.F11)); }
                if (letter == this.FunctionConfig.OptionLetterF12) { this.OnKeyDown(new KeyEventArgs(Keys.F12)); }

                //SendKeys.Send("{TAB}");
            }
        }

        private void txtOption_TextChanged(object sender, EventArgs e)
        {
            String letter = String.Empty;
            if (txtOption.Visible && txtOption.Text == "")
            {
                this.FunctionConfig.CurrentOption = Function.None;
                txtOption.Focus();
                txtOption.Select();
            }
            //else
            //{
            //    letter = txtOption.Text;
            //    if (letter == this.FunctionConfig.OptionLetterF1) { this.OnKeyDown(new KeyEventArgs(Keys.F1)); }
            //    if (letter == this.FunctionConfig.OptionLetterF2) { this.OnKeyDown(new KeyEventArgs(Keys.F2)); }
            //    if (letter == this.FunctionConfig.OptionLetterF3) { this.OnKeyDown(new KeyEventArgs(Keys.F3)); }
            //    if (letter == this.FunctionConfig.OptionLetterF4) { this.OnKeyDown(new KeyEventArgs(Keys.F4)); }
            //    if (letter == this.FunctionConfig.OptionLetterF5) { this.OnKeyDown(new KeyEventArgs(Keys.F5)); }
            //    if (letter == this.FunctionConfig.OptionLetterF6) { this.OnKeyDown(new KeyEventArgs(Keys.F6)); }
            //    if (letter == this.FunctionConfig.OptionLetterF7) { this.OnKeyDown(new KeyEventArgs(Keys.F7)); }
            //    if (letter == this.FunctionConfig.OptionLetterF8) { this.OnKeyDown(new KeyEventArgs(Keys.F8)); }
            //    if (letter == this.FunctionConfig.OptionLetterF9) { this.OnKeyDown(new KeyEventArgs(Keys.F9)); }
            //    if (letter == this.FunctionConfig.OptionLetterF10) { this.OnKeyDown(new KeyEventArgs(Keys.F10)); }
            //    if (letter == this.FunctionConfig.OptionLetterF11) { this.OnKeyDown(new KeyEventArgs(Keys.F11)); }
            //    if (letter == this.FunctionConfig.OptionLetterF12) { this.OnKeyDown(new KeyEventArgs(Keys.F12)); }
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this._now = this._now.Add(new TimeSpan(0, 0, 1));
            this.tsItmTime.Text = this.tsItmTime.Text = this._now.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("en-us"));//this._now.ToLongTimeString();
        }

        private void txtOption_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
                e.IsInputKey = true;
        }
        #endregion
    }

    /// <summary>
    /// All available functions.
    /// </summary>
    public enum Function
    {
        None,
        Add,
        Modify,
        Save,
        Delete,
        Cancel,
        View,
        Quit,
        Query
    }

    /// <summary>
    /// Function configuration class.
    /// </summary>
    public class FunctionConfig : Component
    {
        #region Fields

        private Function _currentOption = Function.None;

        private Function _F1 = Function.Add;
        private Function _F2 = Function.None;
        private Function _F3 = Function.View;
        private Function _F4 = Function.Delete;
        private Function _F5 = Function.None;
        private Function _F6 = Function.Quit;
        private Function _F7 = Function.Modify;
        private Function _F8 = Function.None;
        private Function _F9 = Function.None;
        private Function _F10 = Function.None;
        private Function _F11 = Function.None;
        private Function _F12 = Function.None;

        //private Function _F1 = Function.Add;
        //private Function _F2 = Function.None;
        //private Function _F3 = Function.Modify;
        //private Function _F4 = Function.Delete;
        //private Function _F5 = Function.Add;
        //private Function _F6 = Function.Quit;
        //private Function _F7 = Function.Modify;
        //private Function _F8 = Function.None;
        //private Function _F9 = Function.None;
        //private Function _F10 = Function.None;
        //private Function _F11 = Function.None;
        //private Function _F12 = Function.None;

        private bool _enableF1 = true;
        private bool _enableF2 = true;
        private bool _enableF3 = true;
        private bool _enableF4 = true;
        private bool _enableF5 = true;
        private bool _enableF6 = true;
        private bool _enableF7 = true;
        private bool _enableF8 = true;
        private bool _enableF9 = false;
        private bool _enableF10 = false;
        private bool _enableF11 = false;
        private bool _enableF12 = false;

        private string _F1Letter = "A";
        private string _F2Letter = "";
        private string _F3Letter = "V";
        private string _F4Letter = "D";
        private string _F5Letter = "";
        private string _F6Letter = "E";
        private string _F7Letter = "M";
        private string _F8Letter = "Q";
        private string _F9Letter = "";
        private string _F10Letter = "";
        private string _F11Letter = "";
        private string _F12Letter = "";

        #endregion

        #region Properties

        #region Get/Set Function Keys

        /// <summary>
        /// F1 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F1 Function")]
        public Function F1
        {
            set
            {
                this._F1 = value;
            }
            get
            {
                return this._F1;
            }
        }
        /// <summary>
        /// F2 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F2 Function")]
        public Function F2
        {
            set
            {
                this._F2 = value;
            }
            get
            {
                return this._F2;
            }
        }
        /// <summary>
        /// F3 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F3 Function")]
        public Function F3
        {
            set
            {
                this._F3 = value;
            }
            get
            {
                return this._F3;
            }
        }
        /// <summary>
        /// F4 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F4 Function")]
        public Function F4
        {
            set
            {
                this._F4 = value;
            }
            get
            {
                return this._F4;
            }
        }
        /// <summary>
        /// F5 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F5 Function")]
        public Function F5
        {
            set
            {
                this._F5 = value;
            }
            get
            {
                return this._F5;
            }
        }
        /// <summary>
        /// F6 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F6 Function")]
        public Function F6
        {
            set
            {
                this._F6 = value;
            }
            get
            {
                return this._F6;
            }
        }
        /// <summary>
        /// F7 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F7 Function")]
        public Function F7
        {
            set
            {
                this._F7 = value;
            }
            get
            {
                return this._F7;
            }
        }
        /// <summary>
        /// F8 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F8 Function")]
        public Function F8
        {
            set
            {
                this._F8 = value;
            }
            get
            {
                return this._F8;
            }
        }
        /// <summary>
        /// F9 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F9 Function")]
        public Function F9
        {
            set
            {
                this._F9 = value;
            }
            get
            {
                return this._F9;
            }
        }
        /// <summary>
        /// F10 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F10 Function")]
        public Function F10
        {
            set
            {
                this._F10 = value;
            }
            get
            {
                return this._F10;
            }
        }
        /// <summary>
        /// F11 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F11 Function")]
        public Function F11
        {
            set
            {
                this._F11 = value;
            }
            get
            {
                return this._F11;
            }
        }
        /// <summary>
        /// F12 Function
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F12 Function")]
        public Function F12
        {
            set
            {
                this._F12 = value;
            }
            get
            {
                return this._F12;
            }
        }

        /// <summary>
        /// Current Option
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set Current Option")]
        public Function CurrentOption
        {
            set
            {
                this._currentOption = value;
            }
            get
            {
                return this._currentOption;
            }
        }

        #endregion

        #region Get/Set Option Letter

        /// <summary>
        /// Get/Set F1 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F1 Option Letter")]
        public string OptionLetterF1
        {
            set
            {
                this._F1Letter = value;
            }
            get
            {
                return this._F1Letter;
            }
        }
        /// <summary>
        /// Get/Set F2 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F2 Option Letter")]
        public string OptionLetterF2
        {
            set
            {
                this._F2Letter = value;
            }
            get
            {
                return this._F2Letter;
            }
        } /// <summary>
        /// Get/Set F3 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F3 Option Letter")]
        public string OptionLetterF3
        {
            set
            {
                this._F3Letter = value;
            }
            get
            {
                return this._F3Letter;
            }
        } /// <summary>
        /// Get/Set F4 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F4 Option Letter")]
        public string OptionLetterF4
        {
            set
            {
                this._F4Letter = value;
            }
            get
            {
                return this._F4Letter;
            }
        } /// <summary>
        /// Get/Set F5 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F5 Option Letter")]
        public string OptionLetterF5
        {
            set
            {
                this._F5Letter = value;
            }
            get
            {
                return this._F5Letter;
            }
        } /// <summary>
        /// Get/Set F6 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F6 Option Letter")]
        public string OptionLetterF6
        {
            set
            {
                this._F6Letter = value;
            }
            get
            {
                return this._F6Letter;
            }
        } /// <summary>
        /// Get/Set F7 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F7 Option Letter")]
        public string OptionLetterF7
        {
            set
            {
                this._F7Letter = value;
            }
            get
            {
                return this._F7Letter;
            }
        } /// <summary>
        /// Get/Set F8 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F8 Option Letter")]
        public string OptionLetterF8
        {
            set
            {
                this._F8Letter = value;
            }
            get
            {
                return this._F8Letter;
            }
        } /// <summary>
        /// Get/Set F9 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F9 Option Letter")]
        public string OptionLetterF9
        {
            set
            {
                this._F9Letter = value;
            }
            get
            {
                return this._F9Letter;
            }
        } /// <summary>
        /// Get/Set F10 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F10 Option Letter")]
        public string OptionLetterF10
        {
            set
            {
                this._F10Letter = value;
            }
            get
            {
                return this._F10Letter;
            }
        } /// <summary>
        /// Get/Set F11 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F11 Option Letter")]
        public string OptionLetterF11
        {
            set
            {
                this._F11Letter = value;
            }
            get
            {
                return this._F11Letter;
            }
        } /// <summary>
        /// Get/Set F12 Option Letter
        /// </summary>
        [Browsable(true)]
        [Description("Get/Set F12 Option Letter")]
        public string OptionLetterF12
        {
            set
            {
                this._F12Letter = value;
            }
            get
            {
                return this._F12Letter;
            }
        }




        #endregion

        #region Enable/Disable Function Keys

        /// <summary>
        /// Enable F1 Function
        /// </summary>
        public bool EnableF1
        {
            set
            {
                this._enableF1 = value;
            }
            get
            {
                return this._enableF1;
            }
        }
        /// <summary>
        /// Enable F2 Function
        /// </summary>
        public bool EnableF2
        {
            set
            {
                this._enableF2 = value;
            }
            get
            {
                return this._enableF2;
            }
        }
        /// <summary>
        /// Enable F3 Function
        /// </summary>
        public bool EnableF3
        {
            set
            {
                this._enableF3 = value;
            }
            get
            {
                return this._enableF3;
            }
        }
        /// <summary>
        /// Enable F4 Function
        /// </summary>
        public bool EnableF4
        {
            set
            {
                this._enableF4 = value;
            }
            get
            {
                return this._enableF4;
            }
        }
        /// <summary>
        /// Enable F5 Function
        /// </summary>
        public bool EnableF5
        {
            set
            {
                this._enableF5 = value;
            }
            get
            {
                return this._enableF5;
            }
        }
        /// <summary>
        /// Enable F6 Function
        /// </summary>
        public bool EnableF6
        {
            set
            {
                this._enableF6 = value;
            }
            get
            {
                return this._enableF6;
            }
        }
        /// <summary>
        /// Enable F7 Function
        /// </summary>
        public bool EnableF7
        {
            set
            {
                this._enableF7 = value;
            }
            get
            {
                return this._enableF7;
            }
        }
        /// <summary>
        /// Enable F8 Function
        /// </summary>
        public bool EnableF8
        {
            set
            {
                this._enableF8 = value;
            }
            get
            {
                return this._enableF8;
            }
        }
        /// <summary>
        /// Enable F9 Function
        /// </summary>
        public bool EnableF9
        {
            set
            {
                this._enableF9 = value;
            }
            get
            {
                return this._enableF9;
            }
        }
        /// <summary>
        /// Enable F10 Function
        /// </summary>
        public bool EnableF10
        {
            set
            {
                this._enableF10 = value;
            }
            get
            {
                return this._enableF10;
            }
        }
        /// <summary>
        /// Enable F11 Function
        /// </summary>
        public bool EnableF11
        {
            set
            {
                this._enableF11 = value;
            }
            get
            {
                return this._enableF11;
            }
        }
        /// <summary>
        /// Enable F12 Function
        /// </summary>
        public bool EnableF12
        {
            set
            {
                this._enableF12 = value;
            }
            get
            {
                return this._enableF12;
            }
        }

        #endregion

        #endregion
    }

}