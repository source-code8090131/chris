using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace CrplControlLibrary
{
    public partial class LookupButton : Button
    {
        #region --Field Segment--
        private char m_chrSeparator = '|';
        private string m_strDataFieldMapping = string.Empty;
        private string m_strSPName = string.Empty;
        private string m_strActionType = string.Empty;
        private string _ActionLOVExists = string.Empty;
        private bool IsEnabled = true;
        private bool m_LoadDependentEntities;
        private string m_ConditionalFields = string.Empty;
        private string m_DependentLovControls = string.Empty;
        private string m_HiddenColumns = string.Empty;
        private bool _SkipValidationOnLeave;
        private string _lookUpTitle;

        #endregion

        #region IBaseControl Members

        public string DataFieldMapping
        {
            get
            { return m_strDataFieldMapping; }
            set
            { m_strDataFieldMapping = value; }
        }

        [Browsable(true)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets Lookup Screen Title.")]
        public string LookUpTitle
        {
            get { return _lookUpTitle; }
            set { _lookUpTitle = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Gets or Sets entity name for Non-Key type of Combos.")]
        public string ActionType
        {
            get
            { return m_strActionType; }
            set
            { m_strActionType = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Set the action code for verifying esistance of LOV Value.")]
        public string ActionLOVExists
        {
            get { return _ActionLOVExists; }
            set { _ActionLOVExists = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Gets or Sets entity name for Non-Key type of Combos.")]
        public string SPName
        {
            get
            { return m_strSPName; }
            set
            { m_strSPName = value; }
        }


        [Browsable(true)]
        [Description("Text Enable/Disable")]
        [Category("Custom Behaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Gets or Sets to load dependent entities after item selection.")]
        public bool LoadDependentEntities
        {
            get { return m_LoadDependentEntities; }
            set { m_LoadDependentEntities = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Pipe seperated list of dependent fields to be included in LOV query.")]
        public string ConditionalFields
        {
            get { return m_ConditionalFields; }
            set { m_ConditionalFields = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Pipe seperated list of dependent lov text fields.")]
        public string DependentLovControls
        {
            get { return m_DependentLovControls; }
            set { m_DependentLovControls = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Pipe seperated list of hidden column fields.")]
        public string HiddenColumns
        {
            get { return m_HiddenColumns; }
            set { m_HiddenColumns = value; }
        }


        public bool SkipValidationOnLeave
        {
            get { return _SkipValidationOnLeave; }
            set { _SkipValidationOnLeave = value; }
        }
        #endregion

        public LookupButton()
        {
            InitializeComponent();

            this.HandleCreated += new EventHandler(LookupButton_HandleCreated);
            this.TabStop = false;
        }

        public LookupButton(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            this.HandleCreated += new EventHandler(LookupButton_HandleCreated);
            this.TabStop = false;
        }

        public string[] GetConditionalFields()
        {
            return ConditionalFields.Split(m_chrSeparator);
        }

        public string[] GetDependentLovFields()
        {
            return DependentLovControls.Split(m_chrSeparator);
        }
        public string[] GetHiddenColumns()
        {
            return HiddenColumns.Split(m_chrSeparator);
        }

        void LookupButton_HandleCreated(object sender, EventArgs e)
        {
            this.BackgroundImage = null;
            this.Text = "";
            this.Width = 26;
            this.Height = 21;
        }
    }
}
