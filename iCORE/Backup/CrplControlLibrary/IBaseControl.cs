﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrplControlLibrary
{
    interface IBaseControl
    {
        #region --Property Segment--
        string DataFieldMapping{ get; set; }
        bool IsRequired { get; set; }
        #endregion
    }
}
