/**************  Log Controller class to manage the error loging *****\
 * Created By:  
 * */


using System;
using System.Collections.Generic;
using System.Text;

/*****************************************************************
  Class Name:  ErrorLogController 
  Class Description: Log Controller class to manage the error loging
  Created By: Zeeshan Zahoor
  Created Date: May 22, 2007
  Version No: 1.0
  Modification: Fiaz Ali Saleemi
  Modification Date: Oct 17, 2007
  Version No: 1.1

*****************************************************************/

namespace CrplControlLibrary
{
    public class ErrorLogController
    {

        /// <summary>
        /// This method is used to manage and write the error debugging and handling.
        /// </summary>
        /// <param name="typeName"> "Class/Form/Report Name where Exception occured" </param>
        /// <param name="functionName"> "Function Name where Exception occured" </param>
        /// <param name="excep"> "Exception description" </param>
        /// <returns></returns>
        public static void LogError(string moduleName, string typeName, string functionName, Exception excep, string userId)
        {
            // code starts for creating log file starts here this code will be appended to main menu file

            string complete_path = "C:/iCORE-Logs/" + moduleName + "-Logs/";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;

            if (System.IO.Directory.Exists("C:/iCORE-Logs"))
            {
                bool_icorelogs = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory("C:/iCORE-Logs");
                bool_icorelogs = true;
            }

            if (bool_icorelogs)
            {
                if (System.IO.Directory.Exists(complete_path))
                {
                    bool_icorelogs_rps_logs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                    bool_icorelogs_rps_logs = true;
                }
            }

            if (bool_icorelogs_rps_logs)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");
                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Form/Report Name: " + typeName);
                writer.WriteLine("Function Name: " + functionName);
                writer.WriteLine("User ID: " + userId);
                writer.WriteLine("Exception Description:");
                writer.WriteLine(excep.Message);
                writer.WriteLine("Exception Stack Trace:");
                writer.WriteLine(excep.StackTrace);
                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }
        
    }
}
