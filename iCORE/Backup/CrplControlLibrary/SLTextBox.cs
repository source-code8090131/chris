﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace CrplControlLibrary
{
    public class SLTextBox : CrplTextBox, IBaseControl
    {
        private string m_strDataFieldMapping = string.Empty;
        private string m_strAssociatedLookUpName = string.Empty;
        private bool pbIsRequired;
        private bool pbIsLookUpField;
        private bool m_Date;
        private bool IsEnabled = true;
        private bool skipValidation = false;
        private bool getRecordsOnUpDownKeys = false;

        public SLTextBox(IContainer container)
            : base(container)
        {
        }

        #region IBaseControl Members

        public string DataFieldMapping
        {
            get
            {
                return m_strDataFieldMapping;
            }
            set
            {
                m_strDataFieldMapping = value;
            }
        }

        [DefaultValue(false)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets required field validation of control.")]
        public bool IsRequired
        {
            get
            { return pbIsRequired; }
            set
            {
                pbIsRequired = value;
            }
        }

        [DefaultValue(false)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets LoopUp field properties")]
        public bool IsLookUpField
        {
            get
            { return pbIsLookUpField; }
            set
            {
                pbIsLookUpField = value;
            }
        }

        [Browsable(true)]
        [Description("Gets or Sets Associated LookUp Button Name")]
        public string AssociatedLookUpName
        {
            get
            {
                return m_strAssociatedLookUpName;
            }
            set
            {
                m_strAssociatedLookUpName = value;
            }
        }
        [Browsable(true)]
        [Description("Text Enable/Disable")]
        [Category("Custom Behaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }


        [Browsable(true)]
        [Description("Gets or Sets Date Property")]
        public bool IsDate
        {
            get { return m_Date; }
            set { m_Date = value; }
        }
        #endregion

        public bool SkipValidation
        {
            get { return skipValidation; }
            set { skipValidation = value; }
        }

        public bool GetRecordsOnUpDownKeys
        {
            get { return this.getRecordsOnUpDownKeys; }
            set { this.getRecordsOnUpDownKeys = value; }
        }

        private Form GetParentForm(Control parent)
        {
            Form form = parent as Form;
            if (form != null)
            {
                return form;
            }
            if (parent != null)
            {
                // Walk up the control hierarchy
                return GetParentForm(parent.Parent);
            }
            return null; // Control is not on a Form
        }

        #region --EventHandler Segment--

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            if (!SkipValidation)
            {
                if (this.IsRequired && this.Text.Length < 1)
                {
                    MessageBox.Show("You Can't leave the field empty", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Focus();
                    return;
                }
            }
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            Form frm = this.GetParentForm(this);
            if (frm != null)
            {
                ToolStrip tls = (ToolStrip)frm.Controls["tlbMain"];
                if (tls != null && tls.Items["tbtClose"] != null && tls.Items["tbtClose"].Pressed)
                {
                    frm.AutoValidate = AutoValidate.Disable;
                    e.Cancel = true;
                    return;
                }
            }
            base.OnValidating(e);
        }

        #endregion
    }
}
