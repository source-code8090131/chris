using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace CrplControlLibrary
{
    public class SLComboBox : ComboBox, IBaseControl
    {
        #region --Field Segment--
        private string m_strDataFieldMapping = string.Empty;
        private bool pbIsRequired;
        private eComboBehaviour m_eComboBehaviour;
        private string m_strLOVType = string.Empty;
        private string m_strBusinessEntity = string.Empty;
        private string m_strSPName = string.Empty;
        private bool IsEnabled = true;
        #endregion

        #region IBaseControl Members

        public string DataFieldMapping
        {
            get
            { return m_strDataFieldMapping; }
            set
            { m_strDataFieldMapping = value; }
        }

        [DefaultValue(false)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets required field validation of control.")]
        public bool IsRequired
        {
            get
            { return pbIsRequired; }
            set
            {
                pbIsRequired = value;
            }
        }

        [DefaultValue(eComboBehaviour.Simple)]
        [Category("Custom Behaviour")]
        [Description("Gets or Sets combo behaviour.")]
        public eComboBehaviour ComboBehaviour
        {
            get
            { return m_eComboBehaviour; }
            set
            { m_eComboBehaviour = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Gets or Sets key value combo LOV type.")]
        public string LOVType
        {
            get
            { return m_strLOVType; }
            set
            { m_strLOVType = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Gets or Sets entity name for Non-Key type of Combos.")]
        public string BusinessEntity
        {
            get
            { return m_strBusinessEntity; }
            set
            { m_strBusinessEntity = value; }
        }

        [Category("Custom Behaviour")]
        [Description("Gets or Sets entity name for Non-Key type of Combos.")]
        public string SPName
        {
            get
            { return m_strSPName; }
            set
            { m_strSPName = value; }
        }
        [Browsable(true)]
        [Description("Text Enable/Disable")]
        [Category("Custom Behaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }
                
        #endregion
        
        #region --EventHandler Segment--
        protected override void OnLostFocus(EventArgs e)
        {
            System.Text.RegularExpressions.Regex rEMail = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (rEMail.IsMatch(this.Text))
            {

            }
            if (this.IsRequired && this.Text.Length < 1)
            {
                MessageBox.Show("You Can't leave field empty", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Focus();
            }
            base.OnLostFocus(e);
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            Form frm = this.GetParentForm(this);
            if (frm != null)
            {
                ToolStrip tls = (ToolStrip)frm.Controls["tlbMain"];
                if (tls != null && tls.Items["tbtClose"] != null && tls.Items["tbtClose"].Pressed)
                {
                    frm.AutoValidate = AutoValidate.Disable;
                    e.Cancel = true;
                    return;
                }
            }
            base.OnValidating(e);
        }

        #endregion

        #region Functions

        private Form GetParentForm(Control parent)
        {
            Form form = parent as Form;
            if (form != null)
            {
                return form;
            }
            if (parent != null)
            {
                // Walk up the control hierarchy
                return GetParentForm(parent.Parent);
            }
            return null; // Control is not on a Form
        }

        #endregion
    }
}
