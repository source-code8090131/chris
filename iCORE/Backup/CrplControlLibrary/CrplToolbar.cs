using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;


namespace CrplControlLibrary
{
    public delegate void CrplToolbarButtonClick(object sender, EventArgs e);

	public class CrplToolbar : System.Windows.Forms.UserControl
	{
        private SLButton btnCancel;
        private SLButton btnClose;
        private SLButton btnEdit;
        private SLButton btnAdd;
        private SLButton btnSave;
        private SLButton btnDelete;
        private SLButton btnList;
		#region Auto-Generated


        #endregion
        private ToolTip tltCrplToolbar;
        private IContainer components;

        public event CrplToolbarButtonClick ButtonClicked;
		
		#region Constructor / Destructor
		
		public CrplToolbar()
		{
			InitializeComponent();
		}
		
		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

	
		#endregion
		
		#region Component Designer generated code
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CrplToolbar));
            this.btnCancel = new CrplControlLibrary.SLButton();
            this.btnClose = new CrplControlLibrary.SLButton();
            this.btnEdit = new CrplControlLibrary.SLButton();
            this.btnAdd = new CrplControlLibrary.SLButton();
            this.btnSave = new CrplControlLibrary.SLButton();
            this.btnDelete = new CrplControlLibrary.SLButton();
            this.btnList = new CrplControlLibrary.SLButton();
            this.tltCrplToolbar = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.ActionType = "";
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancel.Location = new System.Drawing.Point(285, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(58, 40);
            this.btnCancel.TabIndex = 34;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnCancel, "Click here to Cancel.");
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClose
            // 
            this.btnClose.ActionType = "";
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Location = new System.Drawing.Point(342, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(58, 40);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "&Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnClose, "Click here to Close a form.");
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.ActionType = "Edit";
            this.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEdit.Location = new System.Drawing.Point(56, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(58, 40);
            this.btnEdit.TabIndex = 26;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnEdit, "Click here to Edit record(s).");
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ActionType = "Add";
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(58, 40);
            this.btnAdd.TabIndex = 25;
            this.btnAdd.Text = "&Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnAdd, "Click here to Add record(s).");
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.ActionType = "Save";
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSave.Location = new System.Drawing.Point(113, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 40);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnSave, "Click here to Save record(s).");
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ActionType = "Delete";
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.Location = new System.Drawing.Point(172, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(58, 40);
            this.btnDelete.TabIndex = 36;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnDelete, "Click here to Delete record(s).");
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnList
            // 
            this.btnList.ActionType = "";
            this.btnList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnList.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnList.Image = ((System.Drawing.Image)(resources.GetObject("btnList.Image")));
            this.btnList.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnList.Location = new System.Drawing.Point(228, 2);
            this.btnList.Name = "btnList";
            this.btnList.Size = new System.Drawing.Size(58, 40);
            this.btnList.TabIndex = 37;
            this.btnList.Text = "&List";
            this.btnList.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tltCrplToolbar.SetToolTip(this.btnList, "Click here to View List of record(s).");
            this.btnList.Click += new System.EventHandler(this.btnList_Click);
            // 
            // tltCrplToolbar
            // 
            this.tltCrplToolbar.IsBalloon = true;
            this.tltCrplToolbar.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // CrplToolbar
            // 
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.Controls.Add(this.btnList);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSave);
            this.Name = "CrplToolbar";
            this.Size = new System.Drawing.Size(403, 46);
            this.Load += new System.EventHandler(this.CrplToolbar_Load);
            this.ResumeLayout(false);

		}
		#endregion
		
		#region " property "

        public bool EnableSaveButton
        {
            set
            {
                this.btnSave.Enabled = value;
            }
            get
            {
                return this.btnSave.Enabled;
            }
        }

        public bool EnableAddButton
        {
            set
            {
                this.btnAdd.Enabled = value;
            }
            get
            {
                return this.btnAdd.Enabled;
            }
        }

        public bool EnableEditButton
        {
            set
            {
                this.btnEdit.Enabled = value;
            }
            get
            {
                return this.btnEdit.Enabled;
            }
        }

        public bool EnableDeleteButton
        {
            set
            {
                this.btnDelete.Enabled = value;
            }
            get
            {
                return this.btnDelete.Enabled;
            }
        }

        public bool EnableCancelButton
        {
            set
            {
                this.btnCancel.Enabled = value;
            }
            get
            {
                return this.btnCancel.Enabled;
            }
        }

        public bool EnableCloseButton
        {
            set
            {
                this.btnClose.Enabled = value;
            }
            get
            {
                return this.btnClose.Enabled;
            }
        }

        public bool EnableListButton
        {
            set
            {
                this.btnList.Enabled = value;
            }
            get
            {
                return this.btnList.Enabled;
            }
        }
		
		#endregion

		#region Toolbar Event Handler
		
		public void CrplToolbar_Load(object sender, System.EventArgs e)
		{
			
		}
		
		public void btnAdd_Click(object sender, System.EventArgs e)
		{
            ButtonClicked(sender, e);
		}
		
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
            ButtonClicked(sender, e);
		}
		
		private void btnClose_Click(object sender, System.EventArgs e)
		{
            ButtonClicked(sender, e);
		}
		
		private void btnEdit_Click(object sender, System.EventArgs e)
		{
            ButtonClicked(sender, e);
        }
        		
		private void btnSave_Click(object sender, System.EventArgs e)
		{
            ButtonClicked(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ButtonClicked(sender, e);
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            ButtonClicked(sender, e);
        }

		#endregion
	}
}
