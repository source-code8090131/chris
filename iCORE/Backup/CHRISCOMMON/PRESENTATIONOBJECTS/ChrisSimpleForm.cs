using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common;

namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    public partial class ChrisSimpleForm : iCORE.CHRISCOMMON.PRESENTATIONOBJECTS.ChrisBaseForm
    {
        public ChrisSimpleForm()
        {
            InitializeComponent();
        }

        public ChrisSimpleForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            this.connbean = connbean_obj;
            this.SetConnectionBean();
            InitializeComponent();
            this.MdiParent = mainmenu;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            CreateToolbar();
            if (null != this.Controls.Owner)
            {
                this.pcbCitiGroup.Location = new Point(this.Controls.Owner.Right - 80, this.pcbCitiGroup.Top);
            }

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            switch (actionType)
            {
                case "Save":
                    base.SkipLovValidationOnSave = true;
                    if (this.GetCurrentPanelBlock.EnableInsert || this.GetCurrentPanelBlock.EnableUpdate)
                        base.Save_Click(this.GetCurrentPanelBlock, actionType);
                    this.Message.ShowMessage();
                    base.SkipLovValidationOnSave = false;
                    break;
                case "Add":
                    base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                    this.m_intPKID = 0;
                    break;
                case "Edit":
                    base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                    break;
                case "Delete":
                    if (ShowConfirmationMessage("", ApplicationMessages.DELETE_CONFIRMATION_MESSAGE))
                    {
                        if (this.GetCurrentPanelBlock.EnableDelete)
                        {
                            base.Delete_Click(this.GetCurrentPanelBlock, actionType);
                            if (!this.Message.HasErrors())
                            {
                                base.FormAddMode(this.GetCurrentPanelBlock.Controls);
                                this.GetCurrentPanelBlock.CurrentBusinessEntity = RuntimeClassLoader.GetBusinessEntity(this.GetCurrentPanelBlock.EntityName);
                                this.m_intPKID = 0;
                            }
                            this.Message.ShowMessage();
                        }
                    }
                    break;
                case "List":
                    base.ShowList(this.GetCurrentPanelBlock, actionType);
                    base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                    break;
                case "Cancel":

                    this.AutoValidate = AutoValidate.Disable;
                    base.FormCancelMode(this.GetCurrentPanelBlock.Controls);
                    base.FormCancelMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                    base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                    base.FormEditMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                    this.m_intPKID = 0;

                    this.AutoValidate = AutoValidate.EnablePreventFocusChange;

                    break;
                case "AddNew":
                    base.FormCancelMode(this.GetCurrentPanelBlock.Controls);
                    base.FormCancelMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                    base.FormEditMode(this.GetCurrentPanelBlock.Controls);
                    base.FormEditMode(this.GetCurrentPanelBlock.ConcurrentPanels);
                    this.m_intPKID = 0;
                    break;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        private void SimpleForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
                DoToolbarActions(this.Controls, "List");
            else
                base.OnFormKeyup(sender, e);
        }
        protected override void CreateToolbar()
        {
            base.CreateToolbar();
            tbtEdit.Visible = false;
        }

    }
}