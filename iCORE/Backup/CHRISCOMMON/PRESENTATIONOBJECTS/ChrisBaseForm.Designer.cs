namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    partial class ChrisBaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.stsOptions = new System.Windows.Forms.StatusStrip();
            this.tsOptions = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF8 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF12 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsF9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtOption = new System.Windows.Forms.TextBox();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tsBottom = new System.Windows.Forms.ToolStrip();
            this.tsItmDev = new System.Windows.Forms.ToolStripLabel();
            this.tsItmTime = new System.Windows.Forms.ToolStripLabel();
            this.tsSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsItmDate = new System.Windows.Forms.ToolStripLabel();
            this.tsSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.stsOptions.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tsBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // stsOptions
            // 
            this.stsOptions.BackColor = System.Drawing.Color.Transparent;
            this.stsOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.stsOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsOptions,
            this.tsF1,
            this.tsF7,
            this.tsF3,
            this.tsF8,
            this.tsF4,
            this.tsF10,
            this.tsF6,
            this.tsF11,
            this.tsF12,
            this.tsF2,
            this.tsF5,
            this.tsF9});
            this.stsOptions.Location = new System.Drawing.Point(0, 0);
            this.stsOptions.Name = "stsOptions";
            this.stsOptions.Size = new System.Drawing.Size(706, 22);
            this.stsOptions.TabIndex = 1;
            this.stsOptions.Text = "statusStrip1";
            // 
            // tsOptions
            // 
            this.tsOptions.Name = "tsOptions";
            this.tsOptions.Size = new System.Drawing.Size(54, 17);
            this.tsOptions.Text = "Options:";
            this.tsOptions.Visible = false;
            // 
            // tsF1
            // 
            this.tsF1.Name = "tsF1";
            this.tsF1.Size = new System.Drawing.Size(58, 17);
            this.tsF1.Text = "[F1]=Add";
            this.tsF1.Visible = false;
            // 
            // tsF7
            // 
            this.tsF7.Name = "tsF7";
            this.tsF7.Size = new System.Drawing.Size(73, 17);
            this.tsF7.Text = "[F7]=Modify";
            this.tsF7.Visible = false;
            // 
            // tsF3
            // 
            this.tsF3.Name = "tsF3";
            this.tsF3.Size = new System.Drawing.Size(63, 17);
            this.tsF3.Text = "[F3]=View";
            this.tsF3.Visible = false;
            // 
            // tsF8
            // 
            this.tsF8.Name = "tsF8";
            this.tsF8.Size = new System.Drawing.Size(69, 17);
            this.tsF8.Text = "[F8]=Query";
            this.tsF8.Visible = false;
            // 
            // tsF4
            // 
            this.tsF4.Name = "tsF4";
            this.tsF4.Size = new System.Drawing.Size(73, 17);
            this.tsF4.Text = "[F4]=Delete";
            this.tsF4.Visible = false;
            // 
            // tsF10
            // 
            this.tsF10.Name = "tsF10";
            this.tsF10.Size = new System.Drawing.Size(0, 17);
            this.tsF10.Visible = false;
            // 
            // tsF6
            // 
            this.tsF6.Name = "tsF6";
            this.tsF6.Size = new System.Drawing.Size(57, 17);
            this.tsF6.Text = "[F6]=Exit";
            this.tsF6.Visible = false;
            // 
            // tsF11
            // 
            this.tsF11.Name = "tsF11";
            this.tsF11.Size = new System.Drawing.Size(0, 17);
            this.tsF11.Visible = false;
            // 
            // tsF12
            // 
            this.tsF12.Name = "tsF12";
            this.tsF12.Size = new System.Drawing.Size(0, 17);
            this.tsF12.Visible = false;
            // 
            // tsF2
            // 
            this.tsF2.Name = "tsF2";
            this.tsF2.Size = new System.Drawing.Size(0, 17);
            this.tsF2.Visible = false;
            // 
            // tsF5
            // 
            this.tsF5.Name = "tsF5";
            this.tsF5.Size = new System.Drawing.Size(0, 17);
            this.tsF5.Visible = false;
            // 
            // tsF9
            // 
            this.tsF9.Name = "tsF9";
            this.tsF9.Size = new System.Drawing.Size(0, 17);
            this.tsF9.Visible = false;
            // 
            // txtOption
            // 
            this.txtOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOption.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtOption.Location = new System.Drawing.Point(706, 0);
            this.txtOption.MaxLength = 1;
            this.txtOption.Name = "txtOption";
            this.txtOption.Size = new System.Drawing.Size(36, 20);
            this.txtOption.TabIndex = 0;
            this.txtOption.Visible = false;
            this.txtOption.TextChanged += new System.EventHandler(this.txtOption_TextChanged);
            this.txtOption.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtOption_PreviewKeyDown);
            this.txtOption.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOption_KeyPress);
            this.txtOption.Validating += new System.ComponentModel.CancelEventHandler(this.txtOption_Validating);
            // 
            // pnlBottom
            // 
            this.pnlBottom.AutoSize = true;
            this.pnlBottom.BackColor = System.Drawing.Color.Transparent;
            this.pnlBottom.Controls.Add(this.stsOptions);
            this.pnlBottom.Controls.Add(this.txtOption);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBottom.Location = new System.Drawing.Point(0, 0);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(742, 22);
            this.pnlBottom.TabIndex = 3;
            this.pnlBottom.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tsBottom);
            this.panel1.Controls.Add(this.pnlBottom);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 206);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 60);
            this.panel1.TabIndex = 4;
            // 
            // tsBottom
            // 
            this.tsBottom.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsItmDev,
            this.tsItmTime,
            this.tsSep1,
            this.tsItmDate,
            this.tsSep2});
            this.tsBottom.Location = new System.Drawing.Point(0, 35);
            this.tsBottom.Name = "tsBottom";
            this.tsBottom.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsBottom.Size = new System.Drawing.Size(742, 25);
            this.tsBottom.TabIndex = 4;
            // 
            // tsItmDev
            // 
            this.tsItmDev.Name = "tsItmDev";
            this.tsItmDev.Size = new System.Drawing.Size(263, 22);
            this.tsItmDev.Text = "Designed and Developed by Citi Bank R.T.S.";
            // 
            // tsItmTime
            // 
            this.tsItmTime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsItmTime.AutoSize = false;
            this.tsItmTime.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsItmTime.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsItmTime.Name = "tsItmTime";
            this.tsItmTime.Size = new System.Drawing.Size(100, 22);
            this.tsItmTime.Text = "Time";
            // 
            // tsSep1
            // 
            this.tsSep1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsSep1.BackColor = System.Drawing.Color.Black;
            this.tsSep1.ForeColor = System.Drawing.Color.Transparent;
            this.tsSep1.Name = "tsSep1";
            this.tsSep1.Padding = new System.Windows.Forms.Padding(2);
            this.tsSep1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsItmDate
            // 
            this.tsItmDate.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsItmDate.AutoSize = false;
            this.tsItmDate.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsItmDate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsItmDate.Name = "tsItmDate";
            this.tsItmDate.Size = new System.Drawing.Size(100, 22);
            this.tsItmDate.Text = "Date";
            // 
            // tsSep2
            // 
            this.tsSep2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsSep2.BackColor = System.Drawing.Color.Black;
            this.tsSep2.ForeColor = System.Drawing.Color.Transparent;
            this.tsSep2.Name = "tsSep2";
            this.tsSep2.Size = new System.Drawing.Size(6, 25);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ChrisBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 266);
            this.Controls.Add(this.panel1);
            this.Name = "ChrisBaseForm";
            this.Text = "`";
            this.Controls.SetChildIndex(this.panel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.stsOptions.ResumeLayout(false);
            this.stsOptions.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tsBottom.ResumeLayout(false);
            this.tsBottom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtOption;
        private System.Windows.Forms.ToolStrip tsBottom;
        private System.Windows.Forms.ToolStripLabel tsItmDev;
        private System.Windows.Forms.ToolStripLabel tsItmTime;
        private System.Windows.Forms.ToolStripLabel tsItmDate;
        private System.Windows.Forms.ToolStripSeparator tsSep1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripSeparator tsSep2;
        public System.Windows.Forms.Panel pnlBottom;
        public System.Windows.Forms.StatusStrip stsOptions;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ToolStripStatusLabel tsOptions;
        public System.Windows.Forms.ToolStripStatusLabel tsF1;
        public System.Windows.Forms.ToolStripStatusLabel tsF7;
        public System.Windows.Forms.ToolStripStatusLabel tsF3;
        public System.Windows.Forms.ToolStripStatusLabel tsF4;
        public System.Windows.Forms.ToolStripStatusLabel tsF6;
        public System.Windows.Forms.ToolStripStatusLabel tsF8;
        public System.Windows.Forms.ToolStripStatusLabel tsF10;
        public System.Windows.Forms.ToolStripStatusLabel tsF11;
        public System.Windows.Forms.ToolStripStatusLabel tsF12;
        public System.Windows.Forms.ToolStripStatusLabel tsF2;
        public System.Windows.Forms.ToolStripStatusLabel tsF5;
        public System.Windows.Forms.ToolStripStatusLabel tsF9;
    }
}