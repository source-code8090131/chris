namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    partial class ChrisTabularForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcbCitiGroup = new System.Windows.Forms.PictureBox();
            this.lblFormTitle = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(445, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(481, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 378);
            this.panel1.Size = new System.Drawing.Size(481, 60);
            // 
            // pcbCitiGroup
            // 
            this.pcbCitiGroup.Image = global::iCORE.Properties.Resources.Citi;
            this.pcbCitiGroup.Location = new System.Drawing.Point(417, 12);
            this.pcbCitiGroup.Name = "pcbCitiGroup";
            this.pcbCitiGroup.Size = new System.Drawing.Size(64, 64);
            this.pcbCitiGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pcbCitiGroup.TabIndex = 9;
            this.pcbCitiGroup.TabStop = false;
            // 
            // lblFormTitle
            // 
            this.lblFormTitle.AutoSize = true;
            this.lblFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormTitle.ForeColor = System.Drawing.Color.Red;
            this.lblFormTitle.Location = new System.Drawing.Point(175, 54);
            this.lblFormTitle.Name = "lblFormTitle";
            this.lblFormTitle.Size = new System.Drawing.Size(89, 20);
            this.lblFormTitle.TabIndex = 8;
            this.lblFormTitle.Text = "Form Title";
            // 
            // ChrisTabularForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 438);
            this.Controls.Add(this.pcbCitiGroup);
            this.Controls.Add(this.lblFormTitle);
            this.Name = "ChrisTabularForm";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "TabularForm";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblFormTitle, 0);
            this.Controls.SetChildIndex(this.pcbCitiGroup, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbCitiGroup;
        private System.Windows.Forms.Label lblFormTitle;
    }
}