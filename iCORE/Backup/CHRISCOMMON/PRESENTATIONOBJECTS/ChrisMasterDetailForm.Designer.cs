namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    partial class ChrisMasterDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcbCitiGroup = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pcbCitiGroup
            // 
            this.pcbCitiGroup.Image = global::iCORE.Properties.Resources.Citi;
            this.pcbCitiGroup.Location = new System.Drawing.Point(593, 12);
            this.pcbCitiGroup.Name = "pcbCitiGroup";
            this.pcbCitiGroup.Size = new System.Drawing.Size(64, 64);
            this.pcbCitiGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pcbCitiGroup.TabIndex = 9;
            this.pcbCitiGroup.TabStop = false;
            // 
            // ChrisMasterDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 408);
            this.Controls.Add(this.pcbCitiGroup);
            this.Name = "ChrisMasterDetailForm";
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.Text = "MasterDetailForm";
            this.Controls.SetChildIndex(this.pcbCitiGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbCitiGroup;

    }
}