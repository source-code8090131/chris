namespace iCORE.CHRISCOMMON.PRESENTATIONOBJECTS
{
    partial class ChrisSimpleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcbCitiGroup = new System.Windows.Forms.PictureBox();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(608, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(644, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 308);
            this.panel1.Size = new System.Drawing.Size(644, 60);
            // 
            // pcbCitiGroup
            // 
            this.pcbCitiGroup.Image = global::iCORE.Properties.Resources.Citi;
            this.pcbCitiGroup.Location = new System.Drawing.Point(593, 22);
            this.pcbCitiGroup.Name = "pcbCitiGroup";
            this.pcbCitiGroup.Size = new System.Drawing.Size(64, 64);
            this.pcbCitiGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pcbCitiGroup.TabIndex = 9;
            this.pcbCitiGroup.TabStop = false;
            // 
            // ChrisSimpleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 368);
            this.Controls.Add(this.pcbCitiGroup);
            this.Name = "ChrisSimpleForm";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "SimpleForm";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pcbCitiGroup, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbCitiGroup;
    }
}