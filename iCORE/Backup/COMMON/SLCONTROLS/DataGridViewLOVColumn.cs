using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using System.ComponentModel;

namespace iCORE.COMMON.SLCONTROLS
{
    /// <summary>
    /// DataGridViewLOVColumn
    /// </summary>
    public class DataGridViewLOVColumn : DataGridViewTextBoxColumn
    {
        private string _spName;
        private string _entityName;
        private string _searchColumn;
        private string _LOVFieldMapping;
        private string _ActionLOVExists;
        private string _ActionLOV;
        private bool _AttachParentEntity;
        private bool _SkipValidationOnLeave;
        private string _lookUpTitle;


        private Type _LovDataType = typeof(System.String);
        [Browsable(true)]
        [Description("ValueTypeofLovCell")]
        [DefaultValue(typeof(System.String))]
        public Type LovDataType
        {
            get
            { return _LovDataType; }
            set
            {
                //Type valType = Type.GetType(value.ToString());

                //if (valType.IsPrimitive)
                //    this._LovDataType = valType;
                this._LovDataType = value;
                this.ValueType = value;

               
            }
        }

        public string LookUpTitle
        {
            get { return _lookUpTitle; }
            set { _lookUpTitle = value; }
        }

        public string SpName
        {
            get { return _spName; }
            set { _spName = value; }
        }

        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        public string SearchColumn
        {
            get { return _searchColumn; }
            set { _searchColumn = value; }
        }

        public string LOVFieldMapping
        {
            get { return _LOVFieldMapping; }
            set { _LOVFieldMapping = value; }
        }

        public string ActionLOVExists
        {
            get { return _ActionLOVExists; }
            set { _ActionLOVExists = value; }
        }

        public string ActionLOV
        {
            get { return _ActionLOV; }
            set { _ActionLOV = value; }
        }

        public DataGridViewLOVColumn()
        {
            CellTemplate = new LOVCell();
        }

        public bool AttachParentEntity
        {
            get { return _AttachParentEntity; }
            set { _AttachParentEntity = value; }
        }

        public bool SkipValidationOnLeave
        {
            get { return _SkipValidationOnLeave; }
            set { _SkipValidationOnLeave = value; }
        }

        //public DataGridViewLOVColumn(string pSpName, string pEntityName, string pSearchColumn, string pLOVFieldMapping, string pActionLOV, string pActionLOVExists)
        //    : base(new LOVCell())
        //{
        //}
        public DataGridViewLOVColumn(string pSpName, string pEntityName, string pSearchColumn, string pLOVFieldMapping, string pActionLOV, string pActionLOVExists)
            : base()
        {
        }

        /// <summary>
        /// Overrriden CellTemplate property
        /// </summary>
        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                // Ensure that the cell used for the template is a CalendarCell.
                if (value != null &&
                    !value.GetType().IsAssignableFrom(typeof(LOVCell)))
                {
                    throw new InvalidCastException("Must be a Lov TextBox");
                }
                base.CellTemplate = value;
            }
        }

        /// <summary>
        /// Clone Column Properties
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            DataGridViewLOVColumn col = (DataGridViewLOVColumn)base.Clone();
            col.SpName = SpName;
            col.EntityName = EntityName;
            col.SearchColumn = SearchColumn;
            col.LOVFieldMapping = LOVFieldMapping;
            col.ActionLOV = ActionLOV;
            col.ActionLOVExists = ActionLOVExists;
            col._lookUpTitle = LookUpTitle;
            col.CellTemplate = (LOVCell)this.CellTemplate.Clone();            
            return col;
        }
    }

    /// <summary>
    /// LOVCell
    /// </summary>
    public class LOVCell : DataGridViewTextBoxCell
    {
        public string SpName
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).SpName; }           
        }

        public string EntityName
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).EntityName; }
        }

        public string SearchColumn
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).SearchColumn; }
        }

        public string LOVFieldMapping
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).LOVFieldMapping; }
        }

        public string ActionLOVExists
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).ActionLOVExists; }
        }

        public string ActionLOV
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).ActionLOV; }
        }

        public bool AttachParentEntity
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).AttachParentEntity; }
        }

        public bool SkipValidationOnLeave
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).SkipValidationOnLeave; }
            set { (this.OwningColumn as DataGridViewLOVColumn).SkipValidationOnLeave = value;}
        }

        public string LookUpTitle
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).LookUpTitle; }
            set { (this.OwningColumn as DataGridViewLOVColumn).LookUpTitle = value; }
        }

        public LOVCell()
            : base()
        { }

        public override Type EditType
        {
            get
            {
                return typeof(LovEditingControl);
            }
        }

        public override Type ValueType
        {
            get
            {
                // Return the type of the value that LOVCell contains.
                return typeof(string);
            }
        }

        public Type LovDataType
        {
            get { return (this.OwningColumn as DataGridViewLOVColumn).LovDataType; }
            set { (this.OwningColumn as DataGridViewLOVColumn).LovDataType = value; }
        }

    }

    /// <summary>
    /// LovEditingControl
    /// </summary>
    public class LovEditingControl : DataGridViewTextBoxEditingControl
    {
        DataGridView dataGridView;
        private bool valueChanged = false;
        int rowIndex;
        private string _spName;
        private string _entityName;
        private string _searchColumn;
        private string _LOVFieldMapping;
        private string _ActionLOVExists;
        private string _ActionLOV;

        public string SpName
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).SpName; }
        }

        public string EntityName
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).EntityName; }
        }

        public string SearchColumn
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).SearchColumn; }
        }

        public string LOVFieldMapping
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).LOVFieldMapping; }
        }

        public string ActionLOVExists
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).ActionLOVExists; }
        }

        public string ActionLOV
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).ActionLOV; }
        }

        public bool AttachParentEntity
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).AttachParentEntity; }
        }

        public bool SkipValidationOnLeave
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).SkipValidationOnLeave; }
            set { (this.EditingControlDataGridView.CurrentCell as LOVCell).SkipValidationOnLeave = value; }
        }

        public string LookUpTitle
        {
            get { return (this.EditingControlDataGridView.CurrentCell as LOVCell).LookUpTitle; }
            set { (this.EditingControlDataGridView.CurrentCell as LOVCell).LookUpTitle = value; }
        }

        public LovEditingControl()
            : base()
        {
        }

        /// <summary>
        /// F9 Key implementation
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (e.KeyCode == Keys.F9)
            {
                BindLOV();
            }
        }
        /// <summary>
        /// LOV Validation on leave
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            //try
            //{
            //    if (isOnLeave)
            //    {
            //        base.OnLeave(e);
            //        isOnLeave = false;
            //        //this.EditingControlDataGridView.CurrentCell = this.EditingControlDataGridView.CurrentCell;//.Focus();
            //        //this.EditingControlDataGridView.CurrentCell.Selected = true;
            //        //this.Select();
            //        //this.Focus();

            //        return;
            //    }

            //    isOnLeave = true;
            //    this.EditingControlDataGridView.CurrentCell.ErrorText = String.Empty;
            //    if (SkipValidationOnLeave)
            //        return;


            //    Result rslt;
            //    IDataManager entityDataManager;
            //    BusinessEntity m_oBusinessEntity;
            //    entityDataManager = RuntimeClassLoader.GetDataManager("");

            //    if (this.AttachParentEntity)
            //    {
            //        ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityProperty(this.EditingControlDataGridView.Columns[this.EditingControlDataGridView.CurrentCellAddress.X].DataPropertyName.ToString(), this.Text);

            //        m_oBusinessEntity = ((SLPanel)Utilities.FindParentSLPanel(this)).CurrentBusinessEntity;

            //        if (m_oBusinessEntity == null)
            //            rslt = entityDataManager.GetAll(SpName, ActionLOVExists, this.Text, SearchColumn);
            //        else
            //            rslt = entityDataManager.GetAll(m_oBusinessEntity, SpName, ActionLOVExists);
            //    }
            //    else
            //        rslt = entityDataManager.GetAll(SpName, ActionLOVExists, this.Text, SearchColumn);

            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables.Count > 0)
            //        {
            //            if (rslt.dstResult.Tables[0].Rows.Count > 0)
            //            {
            //                DataRow drw = rslt.dstResult.Tables[0].Rows[0];
            //                this.Text = drw[LOVFieldMapping].ToString();
            //                if (Utilities.FindParentSLPanel(this) != null)
            //                {
            //                    ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityObject(drw);
            //                    SetLOVDependentFields(drw);
            //                }
            //            }
            //            else
            //            {
            //                ClearLOVDependentFields(rslt.dstResult.Tables[0]);
            //                this.isOnLeave = BindLOV();
            //            }

            //        }
            //        else
            //            this.isOnLeave = BindLOV();
            //    }
            //    else
            //    {
            //        this.isOnLeave = BindLOV();
            //    }
            //    if (this.EditingControlDataGridView.CurrentRow != null && (this.EditingControlDataGridView as SLDataGridView).HasDataBoundItem(this.EditingControlDataGridView.CurrentRow.Index) && this.EditingControlDataGridView.CurrentRow.DataBoundItem != null)
            //        (this.EditingControlDataGridView.CurrentRow.DataBoundItem as DataRowView).EndEdit();

            //    this.isOnLeave = true;
            //    if (!this.isOnLeave)
            //    {
            //        //this.OnLeave(e);
            //        //this.EditingControlDataGridView.CurrentCell = this.EditingControlDataGridView.CurrentCell;//.Focus();
            //        //this.EditingControlDataGridView.CurrentCell.Selected = true;
            //        //this.Capture = true;
            //        //this.Select();
            //        this.isOnLeave = false;
            //    }
            //    else
            //    {
            //        //this.isOnLeave = false;
            //    }
            //}
            //finally
            //{
            //    //this.isOnLeave = false;
            //}

        }
        
        /// <summary>
        /// Clear LOV Dependent Fields
        /// </summary>
        /// <param name="dt"></param>
        public void ClearLOVDependentFields(DataTable dt)
        {
            for (int index = 0; index < this.EditingControlDataGridView.ColumnCount; index++)
            {
                if (dt.Columns.Contains(this.EditingControlDataGridView.Columns[index].DataPropertyName))
                {
                    this.EditingControlDataGridView[index, this.EditingControlDataGridView.CurrentRow.Index].Value = DBNull.Value;
                }
            }
        }

        /// <summary>
        /// Set LOV Dependent fields
        /// </summary>
        /// <param name="drw"></param>
        public void SetLOVDependentFields(DataRow drw)
        {
            for (int index = 0; index < this.EditingControlDataGridView.ColumnCount; index++)
            {
                if (drw.Table.Columns.Contains(this.EditingControlDataGridView.Columns[index].DataPropertyName))
                {
                    this.EditingControlDataGridView[index, this.EditingControlDataGridView.CurrentRow.Index].Value = drw[this.EditingControlDataGridView.Columns[index].DataPropertyName]; ;
                }
            }
        }

        /// <summary>
        /// LOV Popup
        /// </summary>
        public bool BindLOV()
        {
            bool flag = true;

            this.Focus();
            Cmn_ListOfRecords sectionList = new Cmn_ListOfRecords(SpName, ActionLOV);
            if (this.AttachParentEntity)
                sectionList.Entity = ((SLPanel)Utilities.FindParentSLPanel(this)).CurrentBusinessEntity;
            else
                sectionList.Entity = RuntimeClassLoader.GetBusinessEntity("iCORE.Common.BusinessEntity");
            sectionList.DataManagerName = "iCORE.Common.CommonDataManager";
            sectionList.ListType = ListType.SetupList;
            sectionList.Text = ((String.IsNullOrEmpty(LookUpTitle)) ? this.FindForm().Text + " List" : LookUpTitle); 
            //sectionList.SearchText = this.Text;
            //sectionList.SearchColumn = SearchColumn;
            if (sectionList.ShowDialog() == DialogResult.OK)
            {
                DataRow drw = sectionList.SelectedRecord;
                if (drw != null)
                {
                    this.Text = drw[LOVFieldMapping].ToString();
                    ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityObject(drw);
                    SetLOVDependentFields(drw);
                    this.EditingControlDataGridView.CurrentCell.ErrorText = String.Empty;
               }
            }
            else
            {
                flag = false;

                if (sectionList.RowCount > 0)
                {

                    Result rslt;
                    IDataManager entityDataManager;
                    entityDataManager = RuntimeClassLoader.GetDataManager("");
                    rslt = entityDataManager.GetAll(SpName, ActionLOVExists, this.Text, SearchColumn);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataRow drw = rslt.dstResult.Tables[0].Rows[0];
                                this.Text = drw[LOVFieldMapping].ToString();
                                ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityObject(drw);
                                SetLOVDependentFields(drw);
                                flag = true;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(this.Text))
                                {
                                    this.EditingControlDataGridView.CurrentCell.ErrorText = "Invalid LOV Value";
                                    flag = false;
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(this.Text))
                            {
                                this.EditingControlDataGridView.CurrentCell.ErrorText = "Invalid LOV Value";
                                flag = false;
                            }
                        }
                    }
                }
             
            }
            return flag;
        }
    
    }

}