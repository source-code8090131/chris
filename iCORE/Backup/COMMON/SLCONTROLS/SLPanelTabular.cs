using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using CrplControlLibrary;
using System.Data.SqlClient;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common;

namespace iCORE.COMMON.SLCONTROLS
{
    /// <summary>
    /// SLPanelTabular
    /// </summary>
    public partial class SLPanelTabular : SLPanel
    {
        #region --Constructor Segment--

        public SLPanelTabular()
        {
            InitializeComponent();
        }

        public SLPanelTabular(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        /// <summary>
        /// Load Panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        protected override void LoadPanel(SLPanel ParentPanel)
        {
            BaseForm form = (BaseForm)this.FindForm();
            FillEntity(ParentPanel);
            foreach (Control control in this.Controls)
            {
                if (control.GetType().Equals(typeof(SLDataGridView)))
                {
                    (control as SLDataGridView).PopulateGrid(this, false);
                    if ((((control as SLDataGridView).DataSource) as System.Data.DataTable).Rows.Count == 0)
                    {
                        this.ClearDependentPanels();
                    }
                }
            }
        }

        /// <summary>
        /// Clear Panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        protected override void ClearPanel(SLPanel ParentPanel)
        {
            //BaseForm form = (BaseForm)this.FindForm();
            //this.CurrentBusinessEntity = RuntimeClassLoader.GetBusinessEntity(this.EntityName);
            //foreach (Control control in this.Controls)
            //{
            //    if (control.GetType().Equals(typeof(SLDataGridView)))
            //        (control as SLDataGridView).Rows
                    
            //}
        }    

        /// <summary>
        /// Track Changes
        /// </summary>
        /// <returns></returns>
        public override bool HasChanges()
        {
            Boolean hasChanges =false;
            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is SLDataGridView)
                    hasChanges = ((ctrl as SLDataGridView).DataSource as System.Data.DataSet).HasChanges();
            }
            if (this.DependentPanels != null)
            {
                foreach (SLPanel childPanel in this.DependentPanels)
                {
                    hasChanges = childPanel.HasChanges();
                }
            }
            return hasChanges;
        }
    }
}
