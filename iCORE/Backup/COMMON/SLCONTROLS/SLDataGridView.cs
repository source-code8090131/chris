using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using CrplControlLibrary;
using System.Data.SqlClient;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.COMMON.SLCONTROLS
{
    /// <summary>
    /// SLDataGridView extends DatagridView
    /// </summary>
    public partial class SLDataGridView : DataGridView
    {
        #region --Field Segment--
        private DataTable m_oDT = new DataTable();
        private BusinessEntity m_oEntityClass;
        private IDataManager entityDataManager;
        private Cmn_TemplateCR oTemplate = new Cmn_TemplateCR();
        #endregion

        #region --Constructor Segment--
        public SLDataGridView()
        {
            InitializeComponent();
        }

        public SLDataGridView(IContainer container)
        {
            container.Add(this);
            this.AutoGenerateColumns = false;
            InitializeComponent();
            RegisterEventHandlers();
            this.MultiSelect = false;
            //!(this.Parent as SLPanelTabular).HasDependentPanels;
        }

        #endregion

        #region --Property Segment--
        [Browsable(true)]
        [Description("Grid's Mode.")]
        [Category("CustomBehavior")]
        [DefaultValue(Enumerations.eGridMode.Add)]
        public Enumerations.eGridMode CurrentMode
        {
            get { return _eCurrentMode; }
            set { _eCurrentMode = value; }
        }
        Enumerations.eGridMode _eCurrentMode;

        [Browsable(true)]
        [Description("Grid's column names to be hidden.")]
        [Category("CustomBehavior")]
        public string ColumnToHide
        {
            get { return strColumnToHide; }
            set { strColumnToHide = value; }
        }
        private string strColumnToHide;

        [Browsable(true)]
        [Description("Grid's reaonly column list.")]
        [Category("CustomBehavior")]
        public string ReadOnlyColumns
        {
            get { return strReadOnlyColumns; }
            set { strReadOnlyColumns = value; }
        }
        private string strReadOnlyColumns;

        [Browsable(true)]
        [Description("Grid's TabStop False column comma seperated list.")]
        [Category("CustomBehavior")]
        public string SkippingColumns
        {
            get { return skippingColumns; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                    skippingColumns = value.ToUpper();
                else
                    skippingColumns = value;
            }
        }
        private string skippingColumns;

        [Browsable(true)]
        [Description("Grid's column name wrapper.")]
        [Category("CustomBehavior")]
        public string DisplayColumnWrapper
        {
            get { return strDisplayColumnWrapper; }
            set { strDisplayColumnWrapper = value; }
        }
        private string strDisplayColumnWrapper;

        [Browsable(true)]
        [Description("Grid's required column name(s).")]
        [Category("CustomBehavior")]
        public string RequiredColumns
        {
            get { return strRequiredColumns; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                    strRequiredColumns = value.ToUpper();
                else
                    strRequiredColumns = value;
            }
        }
        private string strRequiredColumns;

        [Browsable(true)]
        [Description("Grid's columns width.")]
        [Category("CustomBehavior")]
        public string ColumnWidth
        {
            get { return strColumnWidth; }
            set { strColumnWidth = value; }
        }
        private string strColumnWidth;

        [Browsable(true)]
        [Description("Grid's default rows.")]
        [Category("CustomBehavior")]
        public int GridDefaultRow
        {
            get { return intGridDefaultRow; }
            set { intGridDefaultRow = value; }
        }
        private int intGridDefaultRow;

        [Browsable(true)]
        [Description("Text Enable/Disable")]
        [Category("CustomBehaviour")]
        public bool CustomEnabled
        {
            get { return IsEnabled; }
            set { IsEnabled = value; }
        }

        private bool IsEnabled = true;

        public DataTable GridSource
        {
            get { return m_oDT; }
            set { m_oDT = value; }
        }
        #endregion

        #region --Event Segment--
        /// <summary>
        /// Fill Entity from current row data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SLDataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (this.Rows[e.RowIndex].DataBoundItem != null)
            {
                (this.Parent as SLPanel).SetEntityObject((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row);
                if ((this.Parent as SLPanel).HasDependentPanels)
                {
                    foreach (SLPanel oPanel in (this.Parent as SLPanel).DependentPanels)
                    {
                        oPanel.FillEntity(this.Parent as SLPanel);
                    }
                }
            }
        }
        /// <summary>
        /// Finds first visible column for datagid
        /// </summary>
        /// <returns></returns>
        public int GetFirstVisibleColumn()
        {
            int index = 0;
            foreach (DataGridViewColumn dgColumn in this.Columns)
            {
                if (dgColumn.Visible)
                {
                    index = dgColumn.Index;
                    break;
                }
            }
            return index;
        }

        /// <summary>
        /// Property to determine DataItem
        /// </summary>
        /// <param name="RowIndex"></param>
        /// <returns></returns>
        public Boolean HasDataBoundItem(Int32 RowIndex)
        {
            try
            {
                object obj = this.Rows[RowIndex].DataBoundItem;
                if (obj == null)
                    return false;
                else
                    return true;
            }
            catch (IndexOutOfRangeException ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Load Dependent Panels
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SLDataGridView_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (this.CurrentRow == null || this.CurrentRow.Index != e.RowIndex)
            {
                if (this.Rows[e.RowIndex] != null && !this.Rows[e.RowIndex].IsNewRow)
                {
                    //Check Foreign key of panel and its dependent panel if they are same dont make the DB hit
                    if (/*CurrentRow.Selected &&*/this.HasDataBoundItem(e.RowIndex) && this.Rows[e.RowIndex].DataBoundItem != null && (this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row.RowState != DataRowState.Deleted && (this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row.RowState != DataRowState.Detached)
                    {
                        if (this.m_oDT.Rows.Count == 0)
                        {
                            (this.Parent as SLPanel).ClearDependentPanels();
                        }
                        else
                        {
                            (this.Parent as SLPanel).SetEntityObject((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row);
                            if (!(this.Parent as SLPanel).DisableDependentLoad)
                                (this.Parent as SLPanel).LoadDependentPanels();
                            (this.Parent as SLPanel).LoadConcurrentPanels((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row);
                        }
                    }
                }
                else if (this.Rows[e.RowIndex] != null && this.Rows[e.RowIndex].IsNewRow)
                {
                    (this.Parent as SLPanel).ClearDependentPanels();
                }
            }
        }

        /// <summary>
        /// Form Layout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SLDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (null != this.FindForm())
            {
                BaseForm oBaseForm = (BaseForm)this.FindForm();

                if (this.SelectedRows.Count > 0 && !this.SelectedRows[0].IsNewRow)
                {
                    oBaseForm.tlbMain.Items["tbtDelete"].Enabled = true;
                }
                else
                {
                    oBaseForm.tlbMain.Items["tbtDelete"].Enabled = false;
                }
            }
        }

        /// <summary>
        /// Set Screen Mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SLDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.CurrentCell != null && this.CurrentCell.IsInEditMode)
            {
                if (null != this.FindForm())
                    ((BaseForm)this.FindForm()).operationMode = Mode.Edit;
            }
        }

        /// <summary>
        /// Set Form Mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SLDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            if (null != this.FindForm())
            {
                BaseForm oBaseForm = (BaseForm)this.FindForm();
                oBaseForm.tlbMain.Items["tbtDelete"].Enabled = false;
                oBaseForm.operationMode = Mode.Edit;
            }
        }

        /// <summary>
        /// Set Form Layout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SLDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (null != this.FindForm())
            {
                BaseForm oBaseForm = (BaseForm)this.FindForm();

                if (this.CurrentRow.ErrorText.Equals(string.Empty) == false)
                {
                    this.CurrentRow.ErrorText = String.Empty;
                    if (this.HasDataBoundItem(this.CurrentRow.Index))
                    {
                        (this.CurrentRow.DataBoundItem as DataRowView).Row.RowError = String.Empty;
                    }
                }
                this.CurrentCell.ErrorText = String.Empty;
                if (null != oBaseForm.tlbMain.Items["tbtSearch"])
                    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = true;
                if (null != oBaseForm.tlbMain.Items["tbtSave"])
                    oBaseForm.tlbMain.Items["tbtSave"].Enabled = true;
                if (null != oBaseForm.tlbMain.Items["tbtCancel"])
                    oBaseForm.tlbMain.Items["tbtCancel"].Enabled = true;
            }
            try
            {
                if (this.HasDataBoundItem(e.RowIndex) && this.Rows[e.RowIndex].DataBoundItem != null)
                {
                    if ((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row.RowState == DataRowState.Detached)
                    {
                        (this.Parent as SLPanel).SetEntityObject((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row);
                        if ((this.Parent as SLPanel).HasDependentPanels)
                        {
                            foreach (SLPanel oPanel in (this.Parent as SLPanel).DependentPanels)
                            {
                                oPanel.FillEntity(this.Parent as SLPanel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ((BaseForm)this.FindForm()).LogError("SLDataGridView_CellEndEdit", ex);
            }
            //if (null != this.FindForm())
            //{
            //    BaseForm oBaseForm = (BaseForm)this.FindForm();

            //    if (this.CurrentRow.ErrorText.Equals(string.Empty) == false)
            //    {
            //        this.CurrentRow.ErrorText = String.Empty;
            //    }
            //    if (null != oBaseForm.tlbMain.Items["tbtSearch"])
            //        oBaseForm.tlbMain.Items["tbtSearch"].Enabled = true;
            //    if (null != oBaseForm.tlbMain.Items["tbtSave"])
            //        oBaseForm.tlbMain.Items["tbtSave"].Enabled = true;
            //    if (null != oBaseForm.tlbMain.Items["tbtCancel"])
            //        oBaseForm.tlbMain.Items["tbtCancel"].Enabled = true;
            //}
            //try
            //{
            //    if (this.HasDataBoundItem(e.RowIndex) && this.Rows[e.RowIndex].DataBoundItem != null)
            //    {
            //        if ((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row.RowState == DataRowState.Detached)
            //        {
            //            (this.Parent as SLPanel).SetEntityObject((this.Rows[e.RowIndex].DataBoundItem as DataRowView).Row);
            //            if ((this.Parent as SLPanel).HasDependentPanels)
            //            {
            //                foreach (SLPanel oPanel in (this.Parent as SLPanel).DependentPanels)
            //                {
            //                    oPanel.FillEntity(this.Parent as SLPanel);
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ((BaseForm)this.FindForm()).LogError("SLDataGridView_CellEndEdit", ex);
            //}
        }

        /// <summary>
        /// Cell Validations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SLDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((this.CurrentCell.State == DataGridViewElementStates.Selected || this.CurrentCell.State == DataGridViewElementStates.None) && this.CurrentCell.IsInEditMode && ((BaseForm)this.FindForm()).tbtClose.Pressed == false)
            {
                #region LOV Validating

                if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode && this.Rows[e.RowIndex].Cells[e.ColumnIndex] is LOVCell)
                {
                    LOVCell lovCell = (LOVCell)(this.Rows[e.RowIndex].Cells[e.ColumnIndex]);
                    LovEditingControl lovEdit = (LovEditingControl)(this.EditingControl);
                    string propName = lovCell.LovDataType.Name;


                    if (lovEdit != null)
                    {
                        this.CurrentCell.ErrorText = String.Empty;
                        if (lovEdit.SkipValidationOnLeave)
                            return;
                        if (propName.Equals("Decimal") || propName.Equals("Double"))
                        {
                            if (propName.Equals("Decimal"))
                            {
                                Decimal oDecimal;
                                if (!(Decimal.TryParse(lovEdit.Text, out oDecimal)))
                                {
                                    e.Cancel = true;// lovEdit.Text = "";
                                    return;
                                }

                            }
                            if (propName.Equals("Double"))
                            {
                                Double oDouble;
                                if (!(Double.TryParse(lovEdit.Text, out oDouble)))
                                {
                                    e.Cancel = true;//lovEdit.Text = "";
                                    return;
                                }

                            }

                        }

                        Result rslt;
                        IDataManager entityDataManager;
                        BusinessEntity m_oBusinessEntity;
                        entityDataManager = RuntimeClassLoader.GetDataManager("");

                        if (lovEdit.AttachParentEntity)
                        {
                            ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityProperty(lovEdit.EditingControlDataGridView.Columns[lovEdit.EditingControlDataGridView.CurrentCellAddress.X].DataPropertyName.ToString(), lovEdit.Text);

                            m_oBusinessEntity = ((SLPanel)Utilities.FindParentSLPanel(this)).CurrentBusinessEntity;

                            if (m_oBusinessEntity == null)
                                rslt = entityDataManager.GetAll(lovEdit.SpName, lovEdit.ActionLOVExists, this.Text, lovEdit.SearchColumn);
                            else
                                rslt = entityDataManager.GetAll(m_oBusinessEntity, lovEdit.SpName, lovEdit.ActionLOVExists);
                        }
                        else
                            rslt = entityDataManager.GetAll(lovEdit.SpName, lovEdit.ActionLOVExists, lovEdit.Text, lovEdit.SearchColumn);

                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult.Tables.Count > 0)
                            {
                                if (rslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    DataRow drw = rslt.dstResult.Tables[0].Rows[0];
                                    lovEdit.Text = drw[lovEdit.LOVFieldMapping].ToString();
                                    if (Utilities.FindParentSLPanel(this) != null)
                                    {
                                        ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityObject(drw);
                                        lovEdit.SetLOVDependentFields(drw);
                                    }
                                }
                                else
                                {
                                    lovEdit.ClearLOVDependentFields(rslt.dstResult.Tables[0]);
                                    e.Cancel = !lovEdit.BindLOV();
                                }

                            }
                            else
                                e.Cancel = !lovEdit.BindLOV();
                        }
                        else
                        {
                            e.Cancel = !lovEdit.BindLOV();
                        }
                        if (lovEdit.EditingControlDataGridView.CurrentRow != null && (lovEdit.EditingControlDataGridView as SLDataGridView).HasDataBoundItem(lovEdit.EditingControlDataGridView.CurrentRow.Index) && lovEdit.EditingControlDataGridView.CurrentRow.DataBoundItem != null)
                            (lovEdit.EditingControlDataGridView.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                    }
                    if (e.Cancel)
                        return;
                }
                #endregion

                if (!string.IsNullOrEmpty(this.RequiredColumns))
                {
                    string headerTextOriginal = this.Columns[e.ColumnIndex].HeaderText.Trim();
                    string headerText = this.Columns[e.ColumnIndex].HeaderText.ToUpper().Trim();

                    // Abort validation if cell is not in the Required Field columns list.
                    if (RequiredColumns.Contains(headerText))
                    {
                        // Confirm that the cell is not empty.
                        if (string.IsNullOrEmpty(e.FormattedValue.ToString().Trim()))
                        {
                            //this.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "You can't leave " + headerTextOriginal + " empty.";
                            this.Rows[e.RowIndex].ErrorText = "You can't leave " + headerTextOriginal + " empty.";
                            e.Cancel = true;
                            //if (null != this.FindForm())
                            //{
                            //    BaseForm oBaseForm = (BaseForm)this.FindForm();
                            //    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = false;
                            //}
                        }
                    }
                }

                // Check if data is of Integer type for the Integer type column.
                if (e.FormattedValue.ToString().Length > 0)
                {
                    if (this.Columns[e.ColumnIndex].ValueType != null)
                    {
                        if ((this.Columns[e.ColumnIndex].ValueType.Name != e.FormattedValue.GetType().Name)
                       && ((this.Columns[e.ColumnIndex].ValueType.Name == "Int32")))
                        {
                            int iStatus;
                            if (!int.TryParse(e.FormattedValue.ToString(), out iStatus))
                            {
                                this.Rows[e.RowIndex].ErrorText = ApplicationMessages.ERRORINTEGERCHARACTER;
                                //this.ShowErrorMessage("Invalid Input", ApplicationMessages.ERRORINTEGERCHARACTER);
                                e.Cancel = true;
                                //if (null != this.FindForm())
                                //{
                                //    BaseForm oBaseForm = (BaseForm)this.FindForm();
                                //    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = false;
                                //}
                            }
                        }
                    }
                }
                // Check if data is of Decimal type for the Decimal type column.
                if (e.FormattedValue.ToString().Length > 0)
                {
                    if (this.Columns[e.ColumnIndex].ValueType != null)
                    {
                        if ((this.Columns[e.ColumnIndex].ValueType.Name != e.FormattedValue.GetType().Name)
                       && ((this.Columns[e.ColumnIndex].ValueType.Name == "Decimal")))
                        {
                            Decimal dStatus;
                            if (!Decimal.TryParse(e.FormattedValue.ToString(), out dStatus))
                            {
                                this.Rows[e.RowIndex].ErrorText = ApplicationMessages.ERRORINTEGERCHARACTER;

                                e.Cancel = true;

                            }
                        }
                    }
                }
                if (e.FormattedValue.ToString().Length > 0)
                {
                    if (this.Columns[e.ColumnIndex].ValueType != null)
                    {
                        if ((this.Columns[e.ColumnIndex].ValueType.Name != e.FormattedValue.GetType().Name)
                       && ((this.Columns[e.ColumnIndex].ValueType.Name == "Double")))
                        {
                            Decimal dStatus;
                            if (!Decimal.TryParse(e.FormattedValue.ToString(), out dStatus))
                            {
                                this.Rows[e.RowIndex].ErrorText = ApplicationMessages.ERRORINTEGERCHARACTER;

                                e.Cancel = true;

                            }
                        }
                    }
                }
            }

        }

        //private void SLDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        //{
        //    if (this.CurrentCell.State == DataGridViewElementStates.Selected && this.CurrentCell.IsInEditMode && ((BaseForm)this.FindForm()).tbtClose.Pressed == false)
        //    {
        //        #region LOV Validating

        //        if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode && this.Rows[e.RowIndex].Cells[e.ColumnIndex] is LOVCell)
        //        {
        //            LOVCell lovCell = (LOVCell)(this.Rows[e.RowIndex].Cells[e.ColumnIndex]);
        //            LovEditingControl lovEdit = (LovEditingControl)(this.EditingControl);
        //            string propName = lovCell.LovDataType.Name;
                   

        //            if (lovEdit != null)
        //            {
        //                this.CurrentCell.ErrorText = String.Empty;
        //                if (lovEdit.SkipValidationOnLeave)
        //                    return;
        //                if (propName.Equals("Decimal") || propName.Equals("Double"))
        //                {
        //                    if (propName.Equals("Decimal"))
        //                    {
        //                        Decimal oDecimal;
        //                        if (!(Decimal.TryParse(lovEdit.Text, out oDecimal)))
        //                        {
        //                            e.Cancel = true;// lovEdit.Text = "";
        //                            return;
        //                        }

        //                    }
        //                    if (propName.Equals("Double"))
        //                    {
        //                        Double oDouble;
        //                        if (!(Double.TryParse(lovEdit.Text, out oDouble)))
        //                        {
        //                            e.Cancel = true;//lovEdit.Text = "";
        //                            return;
        //                        }

        //                    }

        //                }

        //                Result rslt;
        //                IDataManager entityDataManager;
        //                BusinessEntity m_oBusinessEntity;
        //                entityDataManager = RuntimeClassLoader.GetDataManager("");

        //                if (lovEdit.AttachParentEntity)
        //                {
        //                    ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityProperty(lovEdit.EditingControlDataGridView.Columns[lovEdit.EditingControlDataGridView.CurrentCellAddress.X].DataPropertyName.ToString(), lovEdit.Text);

        //                    m_oBusinessEntity = ((SLPanel)Utilities.FindParentSLPanel(this)).CurrentBusinessEntity;

        //                    if (m_oBusinessEntity == null)
        //                        rslt = entityDataManager.GetAll(lovEdit.SpName, lovEdit.ActionLOVExists, this.Text, lovEdit.SearchColumn);
        //                    else
        //                        rslt = entityDataManager.GetAll(m_oBusinessEntity, lovEdit.SpName, lovEdit.ActionLOVExists);
        //                }
        //                else
        //                    rslt = entityDataManager.GetAll(lovEdit.SpName, lovEdit.ActionLOVExists, lovEdit.Text, lovEdit.SearchColumn);

        //                if (rslt.isSuccessful)
        //                {
        //                    if (rslt.dstResult.Tables.Count > 0)
        //                    {
        //                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
        //                        {
        //                            DataRow drw = rslt.dstResult.Tables[0].Rows[0];
        //                            lovEdit.Text = drw[lovEdit.LOVFieldMapping].ToString();
        //                            if (Utilities.FindParentSLPanel(this) != null)
        //                            {
        //                                ((SLPanel)Utilities.FindParentSLPanel(this)).SetEntityObject(drw);
        //                                lovEdit.SetLOVDependentFields(drw);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            lovEdit.ClearLOVDependentFields(rslt.dstResult.Tables[0]);
        //                            e.Cancel = !lovEdit.BindLOV();
        //                        }

        //                    }
        //                    else
        //                        e.Cancel = !lovEdit.BindLOV();
        //                }
        //                else
        //                {
        //                    e.Cancel = !lovEdit.BindLOV();
        //                }
        //                if (lovEdit.EditingControlDataGridView.CurrentRow != null && (lovEdit.EditingControlDataGridView as SLDataGridView).HasDataBoundItem(lovEdit.EditingControlDataGridView.CurrentRow.Index) && lovEdit.EditingControlDataGridView.CurrentRow.DataBoundItem != null)
        //                    (lovEdit.EditingControlDataGridView.CurrentRow.DataBoundItem as DataRowView).EndEdit();
        //            }
        //            if (e.Cancel)
        //                return;
        //        }
        //        #endregion

        //        if (!string.IsNullOrEmpty(this.RequiredColumns))
        //        {
        //            string headerTextOriginal = this.Columns[e.ColumnIndex].HeaderText.Trim();
        //            string headerText = this.Columns[e.ColumnIndex].HeaderText.ToUpper().Trim();

        //            // Abort validation if cell is not in the Required Field columns list.
        //            if (RequiredColumns.Contains(headerText))
        //            {
        //                // Confirm that the cell is not empty.
        //                if (string.IsNullOrEmpty(e.FormattedValue.ToString().Trim()))
        //                {
        //                    //this.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "You can't leave " + headerTextOriginal + " empty.";
        //                    this.Rows[e.RowIndex].ErrorText = "You can't leave " + headerTextOriginal + " empty.";
        //                    e.Cancel = true;
        //                    return;
        //                    //if (null != this.FindForm())
        //                    //{
        //                    //    BaseForm oBaseForm = (BaseForm)this.FindForm();
        //                    //    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = false;
        //                    //}
        //                }
        //            }
        //        }

        //        // Check if data is of Integer type for the Integer type column.
        //        if (e.FormattedValue.ToString().Length > 0)
        //        {
        //            if (this.Columns[e.ColumnIndex].ValueType != null)
        //            {
        //                if ((this.Columns[e.ColumnIndex].ValueType.Name != e.FormattedValue.GetType().Name)
        //               && ((this.Columns[e.ColumnIndex].ValueType.Name == "Int32")))
        //                {
        //                    int iStatus;
        //                    if (!int.TryParse(e.FormattedValue.ToString(), out iStatus))
        //                    {
        //                        this.Rows[e.RowIndex].ErrorText = ApplicationMessages.ERRORINTEGERCHARACTER;
        //                        //this.ShowErrorMessage("Invalid Input", ApplicationMessages.ERRORINTEGERCHARACTER);
        //                        e.Cancel = true;
        //                        //if (null != this.FindForm())
        //                        //{
        //                        //    BaseForm oBaseForm = (BaseForm)this.FindForm();
        //                        //    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = false;
        //                        //}
        //                    }
        //                }
        //            }
        //        }
        //        // Check if data is of Decimal type for the Decimal type column.
        //        if (e.FormattedValue.ToString().Length > 0)
        //        {
        //            if (this.Columns[e.ColumnIndex].ValueType != null)
        //            {
        //                if ((this.Columns[e.ColumnIndex].ValueType.Name != e.FormattedValue.GetType().Name)
        //               && ((this.Columns[e.ColumnIndex].ValueType.Name == "Decimal")))
        //                {
        //                    Decimal dStatus;
        //                    if (!Decimal.TryParse(e.FormattedValue.ToString(), out dStatus))
        //                    {
        //                        this.Rows[e.RowIndex].ErrorText = ApplicationMessages.ERRORINTEGERCHARACTER;

        //                        e.Cancel = true;

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (!((BaseForm)this.FindForm()).tbtClose.Pressed)
        //    {
        //        if (this.Columns[e.ColumnIndex].ValueType== null)
        //            return;
                

        //        if ((this.Columns[e.ColumnIndex].ValueType.Name != e.FormattedValue.GetType().Name)
        //                  && ((this.Columns[e.ColumnIndex].ValueType.Name == "DateTime")))
        //        {
        //            DateTime dStatus;
        //            if (!DateTime.TryParse(e.FormattedValue.ToString(), out dStatus))
        //            {
        //                this.CurrentCell.ErrorText = "Not a valid date.";
        //                this.ShowCellErrors = true;
        //                e.Cancel = true;
        //            }
        //            else
        //            {
        //                this.CurrentCell.ErrorText = "";
        //            }
        //        }
        //    }

        //}

        /// <summary>
        /// Row Validations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SLDataGridView_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if ((this.FindForm() as BaseForm).currentQueryMode == Enumerations.eQueryMode.ExecuteQuery)
            {
                if ((((BaseForm)this.FindForm()).tlbMain.Items["tbtCancel"].Pressed))
                    return;
                ///Skip this functionality during save, losing focus from grid and enter query mode
                
                if ((this.RequiredColumns != null
                    && !((this.FindForm() as BaseForm).currentQueryMode == Enumerations.eQueryMode.EnterQuery)
                    && (((BaseForm)this.FindForm()).tlbMain.Items["tbtSave"].Pressed /*|| ((BaseForm)this.FindForm()).CurrentAction == "Save"*/))
                    || (this.IsCurrentRowDirty && this.CurrentCell.State == DataGridViewElementStates.Selected && ((BaseForm)this.FindForm()).tlbMain.Items["tbtClose"].Pressed == false))
                {
                    if (!string.IsNullOrEmpty(this.RequiredColumns) /*&& !skipGridValidations */  && !(this.Rows[e.RowIndex].IsNewRow))
                    {
                        char[] splitchar = new char[] {',', '|'};
                        string[] psRequiredCol = this.RequiredColumns.Split(splitchar);//",".ToCharArray()
                        if (psRequiredCol.Length > 0)
                        {
                            for (int i = 0; i < psRequiredCol.Length; i++)
                            {
                                foreach (DataGridViewColumn dgvCol in this.Columns)
                                {
                                    if (dgvCol.Name.ToUpper().Trim().Equals(psRequiredCol[i].Trim()))
                                    {
                                        if (string.IsNullOrEmpty(this.Rows[e.RowIndex].Cells[dgvCol.Index].FormattedValue.ToString()))
                                        {
                                            //this.Rows[e.RowIndex].ErrorText = "You can't leave " + psRequiredCol[i] + " empty.";
                                            this.Rows[e.RowIndex].ErrorText = "You can't leave " + dgvCol.HeaderText + " empty.";
                                            e.Cancel = true;
                                            //this.CurrentRow.Cells[dgvCol.Index].Selected = true;
                                            //if (null != this.FindForm())
                                            //{
                                            //    BaseForm oBaseForm = (BaseForm)this.FindForm();
                                            //    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = false;
                                            //}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //Validate Columns
                foreach (DataGridViewCell cell in this.Rows[e.RowIndex].Cells)
                {
                    if (!String.IsNullOrEmpty(cell.ErrorText))
                    {
                        this.Rows[e.RowIndex].ErrorText = "Invalid data.";
                        e.Cancel = true;
                    }
                }
            }
            else if (((this.FindForm() as BaseForm).currentQueryMode == Enumerations.eQueryMode.EnterQuery))
            {
                this.Focus();
                e.Cancel = true;
            }
            //if ((((BaseForm)this.FindForm()).tlbMain.Items["tbtCancel"].Pressed))
            //    return;

            //if ((this.RequiredColumns != null /*&& this.RequiredColumns.ToUpper().Contains(this.CurrentCell.OwningColumn.HeaderText.ToUpper())*/ && ((BaseForm)this.FindForm()).tlbMain.Items["tbtSave"].Pressed) || (this.IsCurrentRowDirty && this.CurrentCell.State == DataGridViewElementStates.Selected && ((BaseForm)this.FindForm()).tlbMain.Items["tbtClose"].Pressed == false))
            //{
            //    if (!string.IsNullOrEmpty(this.RequiredColumns))
            //    {
            //        string[] psRequiredCol = this.RequiredColumns.Split(",".ToCharArray());
            //        if (psRequiredCol.Length > 0)
            //        {
            //            for (int i = 0; i < psRequiredCol.Length; i++)
            //            {
            //                foreach (DataGridViewColumn dgvCol in this.Columns)
            //                {
            //                    if (dgvCol.HeaderText.ToUpper().Trim().Equals(psRequiredCol[i].Trim()))
            //                    {
            //                        if (string.IsNullOrEmpty(this.Rows[e.RowIndex].Cells[dgvCol.Index].FormattedValue.ToString()))
            //                        {
            //                            //this.Rows[e.RowIndex].ErrorText = "You can't leave " + psRequiredCol[i] + " empty.";
            //                            this.Rows[e.RowIndex].ErrorText = "You can't leave " + dgvCol.HeaderText + " empty.";
            //                            e.Cancel = true;
            //                            //this.CurrentRow.Cells[dgvCol.Index].Selected = true;
            //                            //if (null != this.FindForm())
            //                            //{
            //                            //    BaseForm oBaseForm = (BaseForm)this.FindForm();
            //                            //    oBaseForm.tlbMain.Items["tbtSearch"].Enabled = false;
            //                            //}
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            ////Validate Columns
            //foreach (DataGridViewCell cell in this.Rows[e.RowIndex].Cells)
            //{
            //    if (!String.IsNullOrEmpty(cell.ErrorText))
            //    {
            //        this.Rows[e.RowIndex].ErrorText = "Invalid data.";
            //        e.Cancel = true;
            //    }
            //}
        }

        /// <summary>
        /// Format Exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SLDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            string msg;
            msg = e.Exception.Message;
            if (e.Exception.GetType().Name == "FormatException")
            {
                if (this.Columns[e.ColumnIndex].ValueType.Name == "Double"
                    || this.Columns[e.ColumnIndex].ValueType.Name == "Decimal"
                    || this.Columns[e.ColumnIndex].ValueType.Name == "int")
                {
                    msg = ApplicationMessages.ERRORINTEGERCHARACTER;
                }
            }
            else if (e.Exception.GetType().Name == "IndexOutOfRangeException")
            {
                msg = string.Empty;
            }
            if (!string.IsNullOrEmpty(msg))
            {
                //(this.FindForm() as BaseForm).SetMessage(String.Empty, msg, String.Empty, MessageType.Error);
                MessageBox.Show(msg, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.CurrentCell.ErrorText = msg;
                e.Cancel = true;
            }
           

        }

        /// <summary>
        /// LovValueTypeValidations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SLDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is LovEditingControl)
            {
                if (this.CurrentCell.OwningColumn is DataGridViewLOVColumn)
                {
                    string TypeName = ((DataGridViewLOVColumn)this.CurrentCell.OwningColumn).LovDataType.Name;
                    if (TypeName.Equals("Int32") || TypeName.Equals("Decimal") || TypeName.Equals("Double"))
                    {
                        TextBox tb = e.Control as TextBox;
                        tb.KeyPress -= new KeyPressEventHandler(tb_KeyPress);
                        tb.KeyPress += new KeyPressEventHandler(tb_KeyPress);
                    }

                }
            }
        }

        /// <summary>
        /// Handler that restricted cell to accept numeric value only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            string TypeName = ((DataGridViewLOVColumn)this.CurrentCell.OwningColumn).LovDataType.Name;
            if (TypeName.Equals("Decimal") || TypeName.Equals("Double"))
            {
                e.Handled = this.IsInvalidDouble(e.KeyChar);
            }
            else if (TypeName.Equals("Int32"))
            {
                e.Handled = this.IsInvalidInteger(e.KeyChar);
            }

        }



        #endregion

        #region --Method Segment--

        /// <summary>
        /// Reset Error Text
        /// </summary>
        public void ResetError()
        {
            foreach (DataGridViewRow dgr in this.Rows)
            {
                dgr.ErrorText = String.Empty;
                foreach (DataGridViewColumn dgc in this.Columns)
                {
                    this[dgc.Index, dgr.Index].ErrorText = String.Empty;
                }
            }
            this.Refresh();
        }
        /// <summary>
        /// Finds first active column for datagid
        /// </summary>
        /// <returns></returns>
        public int FindFirstVisibleColumn()
        {
            int index = 0;
            foreach (DataGridViewColumn dgColumn in this.Columns)
            {
                if (dgColumn.Visible && !(dgColumn.ReadOnly))
                {
                    index = dgColumn.Index;
                    break;
                }
            }
            return index;
        }

        /// <summary>
        /// Finds last active column for datagid
        /// </summary>
        /// <returns></returns>
        public int FindLastVisibleColumn()
        {
            int retIndex = this.Columns.Count - 1;
            for (int index = this.Columns.Count - 1; index >= 0; index--)
            {
                if (this.Columns[index].Visible && !this.Columns[index].ReadOnly && !IsSkippingColumn(this.Columns[index].Name.ToUpper()))
                {
                    retIndex = index;
                    break;
                }
            }
            return retIndex;
            //int index = 0;
            //foreach (DataGridViewColumn dgColumn in this.Columns)
            //{
            //    if (dgColumn.Visible && !(dgColumn.ReadOnly) && !IsSkippingColumn(dgColumn.Name.ToUpper()))
            //    {
            //        index = dgColumn.Index;
            //    }
            //}
            //return index;
        }

        /// <summary>
        /// Finds Next active column for datagid
        /// </summary>
        /// <returns></returns>
        public int FindNextVisibleColumn(int colIndex)
        {
            int retIndex = colIndex;
            for (int index = colIndex; index < this.ColumnCount; index++)
            {
                if (this.Columns[index].Visible && !this.Columns[index].ReadOnly && !IsSkippingColumn(this.Columns[index].Name.ToUpper()))
                {
                    retIndex = index;
                }
            }
            return retIndex;
        }

        /// <summary>
        /// Register SL Datagrid Event Handlers
        /// </summary>
        private void RegisterEventHandlers()
        {
            //Common Validations like Datatype and required fields
            this.CellValidating += new DataGridViewCellValidatingEventHandler(SLDataGridView_CellValidating);

            //Formatted Error messages in case of validation errors
            this.DataError += new DataGridViewDataErrorEventHandler(SLDataGridView_DataError);

            //Verify that all required columns are filled
            this.RowValidating += new DataGridViewCellCancelEventHandler(SLDataGridView_RowValidating);

            //This event is intended to use for controling the toolbar buttons
            this.CellEndEdit += new DataGridViewCellEventHandler(SLDataGridView_CellEndEdit);

            //Use to set the panel entity. This is used when master grid is adding a new record and 
            //entity was not available at rowenter event
            //this.RowValidated += new DataGridViewCellEventHandler(SLDataGridView_RowValidated);
            //this.SelectionChanged += new EventHandler(SLDataGridView_SelectionChanged);

            //Contrls delete button on toolbar
            this.UserDeletedRow += new DataGridViewRowEventHandler(SLDataGridView_UserDeletedRow);

            //Change Forms operation mode to edit mode
            this.CellValueChanged += new DataGridViewCellEventHandler(SLDataGridView_CellValueChanged);

            ////Contrls delete button on toolbar
            this.CellMouseClick += new DataGridViewCellMouseEventHandler(SLDataGridView_CellMouseClick);

            //Set the panel entity to current row data item and load dependent panels.
            this.RowEnter += new DataGridViewCellEventHandler(SLDataGridView_RowEnter);
            //this.RowLeave += new DataGridViewCellEventHandler(SLDataGridView_RowLeave); 

            //
            this.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(SLDataGridView_EditingControlShowing);
        }


        /// <summary>
        /// Impementation of F9 Key
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            Keys key = (keyData & Keys.KeyCode);

            if (key == Keys.F9)
            {
                return this.ProcessF2Key(keyData);
            }
            else if (key == Keys.Enter || key == Keys.Tab)
            {
                int col = 0;
                int row = 0;
                if (this.CurrentCell != null)
                {
                    col = this.CurrentCell.ColumnIndex;
                    row = this.CurrentCell.RowIndex;
                }
                MyProcessTabKey(Keys.Tab, col, row);
                if (this.CurrentCell != null && !this.CurrentCell.IsInEditMode && col == this.CurrentCell.ColumnIndex && row == this.CurrentCell.RowIndex)
                    return base.ProcessDialogKey(keyData);
                return true;
                //return this.ProcessTabKey(keyData);
            }

            return base.ProcessDialogKey(keyData);
        }

        /// <summary>
        /// Perfrom Delete Operation
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected override bool ProcessDataGridViewKey(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                ((BaseForm)this.FindForm()).tbtDelete.PerformClick();
                return false;
            }
            else if (e.KeyCode == Keys.Enter || (e.KeyCode == Keys.Tab && e.Modifiers == Keys.None))
            {
                int col = 0;
                int row = 0;
                if (this.CurrentCell != null && this.CurrentCell.ColumnIndex == this.FindLastVisibleColumn() && (this.CurrentRow.Index == this.Rows.Count || this.CurrentRow.IsNewRow) && this.CurrentCell.ColumnIndex != 0 && !this.CurrentCell.IsInEditMode)
                {
                    if (this.CurrentCell != null)
                    {
                        col = this.CurrentCell.ColumnIndex;
                        row = this.CurrentCell.RowIndex;
                    }
                    this.ProcessDialogKey(Keys.Tab);
                    this.ProcessDialogKey(Keys.Tab);
                    this.CurrentCell = this.Rows[row].Cells[col];
                    return true;
                }
                col = 0;
                row = 0;
                if (this.CurrentCell != null)
                {
                    col = this.CurrentCell.ColumnIndex;
                    row = this.CurrentCell.RowIndex;
                }
                MyProcessTabKey(Keys.Tab, col, row);

                if (this.FirstDisplayedCell != null && this.CurrentCell != null)
                {
                    if (this.FirstDisplayedCell.Selected && this.CurrentCell.ReadOnly)
                        this.ProcessDialogKey(Keys.Tab);
                }
                if (this.CurrentCell != null && !this.CurrentCell.IsInEditMode && col == this.CurrentCell.ColumnIndex && row == this.CurrentCell.RowIndex)
                    return base.ProcessDataGridViewKey(e);
                return true;
                //return this.ProcessTabKey(e.KeyData);
            }
            return base.ProcessDataGridViewKey(e);
        }

        /// <summary>
        /// Skip Readonly Columns
        /// </summary>
        /// <param name="keyData"></param>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        protected bool MyProcessTabKey(Keys keyData, int col, int row)
        {
            bool retValue = base.ProcessTabKey(Keys.Tab);
            if (this.CurrentCell == null || (col == this.CurrentCell.ColumnIndex && row == this.CurrentCell.RowIndex))
                return true;
            else
            {
                col = this.CurrentCell.ColumnIndex;
                row = this.CurrentCell.RowIndex;
            }
            while (this.CurrentCell.ReadOnly || IsSkippingColumn(this.CurrentCell.OwningColumn.Name.ToUpper()))
            {
                retValue = base.ProcessTabKey(Keys.Tab);
                if (col == this.CurrentCell.ColumnIndex && row == this.CurrentCell.RowIndex)
                    return true;
                else
                {
                    col = this.CurrentCell.ColumnIndex;
                    row = this.CurrentCell.RowIndex;
                }
            }
            return retValue;
        }

        /// <summary>
        /// handling Navigation
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        bool NavKey(Keys keyData)
        {
            int col = this.CurrentCell.ColumnIndex;
            int row = this.CurrentCell.RowIndex;

            switch (keyData)
            {
                case Keys.Down:
                    col--; row++;
                    goto case Keys.Right;    //fall through
                case Keys.Right:
                case Keys.Tab:
                case Keys.Enter:
                    //if (this.CurrentRow.DataBoundItem != null)
                    //{
                    //    (this.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                    //}
                    //this.EndEdit();
                    if (!String.IsNullOrEmpty(this.CurrentCell.ErrorText))
                        return true;

                    while (row < this.Rows.Count)
                    {
                        while (++col < this.Columns.Count)
                        {
                            if (!this[col, row].ReadOnly && this[col, row].Visible)
                            {
                                this.CurrentCell = this[col, row];
                                return true;
                            }
                        }
                        col = -1;
                        row++;
                    }
                    return true;

                case Keys.Up:
                    col++; row--;
                    goto case Keys.Left;    //fall through
                case Keys.Left:
                case Keys.Tab | Keys.Shift:
                case Keys.Enter | Keys.Shift:
                    //if (this.CurrentRow.DataBoundItem != null)
                    //{
                    //    (this.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                    //}
                    //this.EndEdit();
                    if (!String.IsNullOrEmpty(this.CurrentCell.ErrorText))
                        return true;
                    while (row >= 0)
                    {
                        while (--col >= 0)
                        {
                            if (!this[col, row].ReadOnly && this[col, row].Visible)
                            {
                                this.CurrentCell = this[col, row];
                                return true;
                            }
                        }
                        col = this.Columns.Count;
                        row--;
                    }
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Reject Changes
        /// </summary>
        public void RejectChanges()
        {
            this.m_oDT.RejectChanges();
        }

        /// <summary>
        /// Method to delete selected DataGridView rows.
        /// </summary>
        public void DeleteSelectedGridRow()
        {
            foreach (DataGridViewRow oLoopDGVR in this.SelectedRows)
            {
                this.Rows.Remove(oLoopDGVR);
            }
            DoDeleteRow();
        }

        /// <summary>
        /// Method to delete selected grid's row permenantly.
        /// </summary>
        /// <returns></returns>
        public Boolean DoDeleteRow()
        {
            SLPanelTabular oPanel = new SLPanelTabular();
            BaseForm oBaseForm = (BaseForm)this.FindForm();
            Boolean rslt = false; ;
            DataTable oDTChangeSet;

            if (this.Parent is GroupBox)
                oPanel = (SLPanelTabular)this.Parent.Parent;
            else if (this.Parent is SLPanelTabular)
                oPanel = (SLPanelTabular)this.Parent;

            if (m_oDT.DataSet.HasChanges() && m_oDT.GetChanges(DataRowState.Deleted) != null)
            {
                // Get the clone datatable
                oDTChangeSet = m_oDT.GetChanges(DataRowState.Deleted);

                m_oEntityClass = oPanel.CurrentBusinessEntity;//RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
                entityDataManager = RuntimeClassLoader.GetDataManager("");

                Result recordsInserted = entityDataManager.BatchUpdate(m_oEntityClass, oPanel.SPName, oDTChangeSet);

                if (Convert.ToInt32(recordsInserted.objResult) > 0)
                {
                    // Need to be modified in order to persist datatable changes.
                    DataView dv = m_oDT.DefaultView;
                    dv.RowStateFilter = DataViewRowState.Deleted;
                    for (int index = 0; index < dv.Count; index++)
                    {
                        dv[index].Row.AcceptChanges();
                        index--;
                    }
                    dv.RowStateFilter = DataViewRowState.CurrentRows;

                    this.BindingContext[m_oDT].EndCurrentEdit();
                    rslt = true;
                    oBaseForm.SetMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Number of records affected : " + recordsInserted.objResult.ToString(), String.Empty, MessageType.Information);
                    //oBaseForm.ShowInformationMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Number of records affected : " + recordsInserted.objResult.ToString());
                }
                else
                {
                    if (recordsInserted.exp.Message == (ApplicationMessages.DBMessages.DeleteRecord.GetHashCode().ToString()) == true)
                    {
                        oBaseForm.ShowInformationMessage("", ApplicationMessages.DELETE_MASTER_RECORD);
                        //oBaseForm.ShowMessage(oBaseForm.GetCurrentPanelBlock.EntityName, ApplicationMessages.DELETE_MASTER_RECORD, String.Empty, MessageType.Error);
                        oDTChangeSet.Clear();
                        oDTChangeSet.Dispose();
                    }
                    else
                    {
                        // Flush dataset changes 
                        oDTChangeSet.Clear();
                        oDTChangeSet.Dispose();
                        oBaseForm.SetMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Error in updating records.", String.Empty, MessageType.Error);
                        //oBaseForm.ShowInformationMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Error in updating records.");
                    }
                }
            }
            else
                oBaseForm.SetMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "No changes to update: Zero records affected.", String.Empty, MessageType.Information);
            //oBaseForm.ShowInformationMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "No changes to update: Zero records affected.");
            return rslt;
        }

        /// <summary>
        /// Save Operation
        /// </summary>
        /// <param name="oPanel"></param>
        /// <returns></returns>
        public Boolean SaveGrid(SLPanel oPanel)
        {
            BaseForm oBaseForm = (BaseForm)this.FindForm();
            Boolean rslt = false; ;
            //DataTable oDTChangeSet;
            DataView oDvChangeSet;

            Int32 currentRowIndex = 0;
            if (this.CurrentRow != null)
                currentRowIndex = this.CurrentRow.Index;
            if (m_oDT != null && m_oDT.DataSet != null)
            {
                if (m_oDT.DataSet.HasChanges() && m_oDT.GetChanges() != null)
                {
                    // Get the DataTable with Rows State as RowState.Added  
                    //oDTChangeSet = m_oDT.GetChanges();
                    //oDTChangeSet = m_oDT.Copy();
                    oDvChangeSet = m_oDT.DefaultView;
                    oDvChangeSet.RowStateFilter = DataViewRowState.Added | DataViewRowState.ModifiedCurrent;
                    for (int indexer = 0; indexer < oDvChangeSet.Count; indexer++)
                    {
                        DataRow row = oDvChangeSet[indexer].Row;
                        if (row.Table.Columns.Contains("SOEID"))
                            row["SOEID"] = (this.FindForm().MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                        foreach (PropertyInfo property in (this.Parent as SLPanel).CurrentBusinessEntity.GetType().GetProperties())
                        {
                            object[] customAttributes = property.GetCustomAttributes(typeof(CustomAttributes), false);
                            foreach (object attribute in customAttributes)
                            {
                                CustomAttributes storageAttributes = (CustomAttributes)attribute;
                                if (storageAttributes.IsForeignKey)
                                {
                                    if (property.GetValue((this.Parent as SLPanel).CurrentBusinessEntity, null)!= null)
                                        row[property.Name] = property.GetValue((this.Parent as SLPanel).CurrentBusinessEntity, null);
                                    //if (string.IsNullOrEmpty(row[property.Name].ToString()))
                                    //{
                                    Control cntrl = Utilities.FindMasterPanel(oBaseForm, oPanel.Name);
                                    if (cntrl is SLPanel)
                                    {
                                        if ((cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name) != null)
                                            row[property.Name] = (cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name).GetValue((cntrl as SLPanel).CurrentBusinessEntity, null);
                                    }
                                    //}
                                }
                            }
                        }
                    }
                    //foreach (DataRow row in oDTChangeSet.Rows)
                    //{
                    //    //if (row.RowState != DataRowState.Unchanged)
                    //    //{
                    //    if (row.Table.Columns.Contains("SOEID"))
                    //        row["SOEID"] = (this.FindForm().MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                    //    foreach (PropertyInfo property in (this.Parent as SLPanel).CurrentBusinessEntity.GetType().GetProperties())
                    //    {
                    //        object[] customAttributes = property.GetCustomAttributes(typeof(CustomAttributes), false);
                    //        foreach (object attribute in customAttributes)
                    //        {
                    //            CustomAttributes storageAttributes = (CustomAttributes)attribute;
                    //            if (storageAttributes.IsForeignKey)
                    //            {
                    //                row[property.Name] = property.GetValue((this.Parent as SLPanel).CurrentBusinessEntity, null);
                    //                if (string.IsNullOrEmpty(row[property.Name].ToString()))
                    //                {
                    //                    Control cntrl = Utilities.FindMasterPanel(oBaseForm, oPanel.Name);
                    //                    if (cntrl is SLPanel)
                    //                        row[property.Name] = (cntrl as SLPanel).CurrentBusinessEntity.GetType().GetProperty(property.Name).GetValue((cntrl as SLPanel).CurrentBusinessEntity, null);
                    //                }
                    //            }
                    //        }
                    //    }
                    //    //}
                    //}

                    m_oEntityClass = oPanel.CurrentBusinessEntity;//RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
                    entityDataManager = RuntimeClassLoader.GetDataManager("");

                    Result recordsInserted = entityDataManager.BatchUpdate(m_oEntityClass, oPanel.SPName, m_oDT);

                    if (Convert.ToInt32(recordsInserted.objResult) > 0)
                    {
                        //m_oDT.AcceptChanges();
                        //m_oDT = oDTChangeSet;
                        m_oDT.AcceptChanges();
                        oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                        this.BindingContext[m_oDT].EndCurrentEdit();
                        rslt = true;
                        if (Convert.ToInt32(recordsInserted.objResult) > 0)
                            oBaseForm.SetMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Number of records affected : " + recordsInserted.objResult.ToString(), String.Empty, MessageType.Information);
                        //oBaseForm.ShowInformationMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Number of records affected : " + recordsInserted.objResult.ToString());
                        //MessageBox.Show("Number of records affected : " + recordsInserted.objResult.ToString());
                        //PopulateGrid(oPanel, false);
                    }
                    else if (recordsInserted.exp.Message.Equals(ApplicationMessages.DBMessages.DuplicateRecord.GetHashCode().ToString()) == true)
                    {
                        //this.oTemplate.ShowInformationMessage(oPanel.EntityName, ApplicationMessages.DUPLICATE_MESSAGE);
                        //this.oTemplate.ShowInformationMessage("Record ", ApplicationMessages.DUPLICATE_MESSAGE);
                        (this.FindForm() as BaseForm).LogError("SaveGrid", recordsInserted.exp);
                        oBaseForm.SetMessage("Record ", ApplicationMessages.DUPLICATE_MESSAGE, String.Empty, MessageType.Error);
                        oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                        this.BindingContext[m_oDT].EndCurrentEdit();
                    }
                    else if ((recordsInserted.exp is System.Data.SqlClient.SqlException) && (recordsInserted.exp as System.Data.SqlClient.SqlException).Number == 50000)
                    {
                        (this.FindForm() as BaseForm).LogError("SaveGrid", recordsInserted.exp);
                        oBaseForm.SetMessage(String.Empty, recordsInserted.exp.Message, String.Empty, MessageType.Error);
                        oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                        this.BindingContext[m_oDT].EndCurrentEdit();
                    }
                    else
                    {
                        (this.FindForm() as BaseForm).LogError("SaveGrid", recordsInserted.exp);
                        oBaseForm.SetMessage(String.Empty, "Error in updating records.", String.Empty, MessageType.Error);
                        oDvChangeSet.RowStateFilter = DataViewRowState.CurrentRows;
                        this.BindingContext[m_oDT].EndCurrentEdit();
                    }
                    //oBaseForm.ShowInformationMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "Error in updating records.");
                    //MessageBox.Show("Error in updating records.");
                }

                else
                {
                    if (!oBaseForm.HasMessage())
                    {
                        oBaseForm.SetMessage(String.Empty, "No changes to update: Zero records affected.", String.Empty, MessageType.Warning);
                    }

                }
                //oBaseForm.ShowInformationMessage(oBaseForm.GetCurrentPanelBlock.EntityName, "No changes to update: Zero records affected.");
                //MessageBox.Show("No changes to update: Zero records affected.");
            }
            if (this.Rows.Count > 0)
            {
                this.Rows[currentRowIndex].Cells[this.FindFirstVisibleColumn()].Selected = true;
            }
            return rslt;
        }

        /// <summary>
        /// Load Operation
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="defaultLoad"></param>
        public void PopulateGrid(SLPanel oPanel, bool defaultLoad)
        {
            IDataManager dataManager = RuntimeClassLoader.GetDataManager("");
            //BusinessEntity oBusinessEntity = RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
            Result rslt = new Result();

            rslt = dataManager.GetAll(oPanel.CurrentBusinessEntity, oPanel.SPName, "List");

            if (rslt.isSuccessful)
                m_oDT = rslt.dstResult.Tables[0];

            if (m_oDT != null)
            {
                // Clear datatable in order to display empty rows.
                if (defaultLoad)
                {
                    m_oDT.Clear();
                    m_oDT.AcceptChanges();
                }

                this.DataSource = m_oDT;

                // Method to set Grid behavior using Custom properties.
                this.SetGridCustomBehavior();
            }

            this.AllowUserToAddRows = oPanel.EnableInsert;
            this.AllowUserToDeleteRows = oPanel.EnableDelete;
        }

        /// <summary>
        /// Load Data in Enter Query Mode
        /// </summary>
        /// <param name="oPanel"></param>
        public void PopulateGrid(SLPanel oPanel)
        {
            IDataManager dataManager = RuntimeClassLoader.GetDataManager("");
            //BusinessEntity oBusinessEntity = RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
            Result rslt = new Result();

            rslt = dataManager.GetAll(oPanel.CurrentBusinessEntity, oPanel.SPName, "EnterQuery");

            if (rslt.isSuccessful && rslt.dstResult.Tables.Count > 0)
            {
                m_oDT = rslt.dstResult.Tables[0];
                if (rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("No matching data found", "Note",
                                                                     MessageBoxButtons.OK,
                                                                     MessageBoxIcon.Information);
                }
            }
            if (m_oDT != null)
            {
                // Clear datatable in order to display empty rows.
                //if (defaultLoad)
                //{
                //    m_oDT.Clear();
                //    m_oDT.AcceptChanges();
                //}

                this.DataSource = m_oDT;

                // Method to set Grid behavior using Custom properties.
                this.SetGridCustomBehavior();
            }


            this.AllowUserToAddRows = oPanel.EnableInsert;
            this.AllowUserToDeleteRows = oPanel.EnableDelete;
        }


        /// <summary>
        /// Initializes Entity and DataManager
        /// </summary>
        /// <param name="oPanel"></param>
        private void InitializeFormObject(SLPanel oPanel)
        {
            m_oEntityClass = oPanel.CurrentBusinessEntity;//RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
            entityDataManager = RuntimeClassLoader.GetDataManager(oPanel.DataManager);
        }


        /// <summary>
        /// Method to set custom behaviour for DataGridView.
        /// </summary>
        public void SetGridCustomBehavior()
        {
            HideGridColumns();
            RenameGridColumns();
            MarkReadonlyColumns();
            SetColumnsWidth();
        }

        /// <summary>
        /// Method to hide Grid's columns.
        /// </summary>
        private void HideGridColumns()
        {
            if (!string.IsNullOrEmpty(this.ColumnToHide))
            {
                string[] ColumnToHide = this.ColumnToHide.Split(',');

                int iLoop = 0;
                for (; iLoop < ColumnToHide.Length; iLoop++)
                {
                    if (this.Columns.Contains(ColumnToHide[iLoop].Trim()))
                        this.Columns[ColumnToHide[iLoop].Trim()].Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to Rename Grid's column as specified by the Dev inside DisplayColumnWrapper property.
        /// </summary>
        private void RenameGridColumns()
        {
            if (!string.IsNullOrEmpty(this.DisplayColumnWrapper))
            {
                // Get the column pair: source column with its new display name.
                string[] ColumnPair = this.DisplayColumnWrapper.Split('|');
                int iLoop = 0;
                for (; iLoop < ColumnPair.Length; iLoop++)
                {
                    // Get the column name mapping.
                    string[] ColumnNameWrapper = ColumnPair[iLoop].Split(',');
                    if (ColumnNameWrapper.Length > 1)
                    {
                        if (this.Columns.Contains(ColumnNameWrapper[0].Trim()))
                            this.Columns[ColumnNameWrapper[0].Trim()].HeaderText = ColumnNameWrapper[1];
                    }
                }

            }
        }

        /// <summary>
        /// Method to Mark Grid's columns as readonly as specified by the Dev inside ReadOnlyColumns property.
        /// </summary>
        private void MarkReadonlyColumns()
        {
            if (!string.IsNullOrEmpty(this.ReadOnlyColumns))
            {
                // Get all columns need to be marked as ReadOnly.
                string[] ReadOnlyColumns = this.ReadOnlyColumns.Split(',');
                int iLoop = 0;
                for (; iLoop < ReadOnlyColumns.Length; iLoop++)
                {
                    if (this.Columns.Contains(ReadOnlyColumns[iLoop].Trim()))
                    {
                        this.Columns[ReadOnlyColumns[iLoop].Trim()].ReadOnly = true;
                    }
                }

            }
        }

        /// <summary>
        /// Method to set columns width as specified by the user.
        /// </summary>
        private void SetColumnsWidth()
        {
            if (!string.IsNullOrEmpty(this.ColumnWidth))
            {
                // Get all columns in order to set their width.
                string[] GridColumns = this.ColumnWidth.Split('|');
                int iLoop = 0;
                for (; iLoop < GridColumns.Length; iLoop++)
                {
                    string[] ColumnWidth = GridColumns[iLoop].Split(',');

                    if (ColumnWidth.Length > 1)
                    {
                        if (this.Columns.Contains(ColumnWidth[0].Trim()))
                        {
                            this.Columns[ColumnWidth[0].Trim()].Width = Convert.ToInt32(ColumnWidth[1]);
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Return if column has a tabstop or not.
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        private Boolean IsSkippingColumn(string columnName)
        {
            Boolean retVal = false; ;
            if (!String.IsNullOrEmpty(this.SkippingColumns))
            {
                foreach (string name in this.SkippingColumns.Split(",".ToCharArray()))
                {
                    if (columnName == name)
                    {
                        retVal = true;
                        break;
                    }
                }
            }
            return retVal;
        }


        private bool IsInvalidInteger(int keyCode)
        {
            bool isInvalid = true;

            if (IsNumber(keyCode) || IsBackSpace(keyCode))
                isInvalid = false;

            return isInvalid;
        }

        private bool IsInvalidDouble(int keyCode)
        {
            bool isInvalid = true;
            Keys key = (Keys)keyCode;
            if (IsNumber(keyCode) || IsBackSpace(keyCode))
                isInvalid = false;
            else if (IsDot(keyCode))
            {
                int dotOccurrance = 0;

                foreach (char ch in this.CurrentCell.EditedFormattedValue.ToString()) //this.Text)
                {
                    if (IsDot(ch))
                        dotOccurrance++;
                }

                isInvalid = dotOccurrance > 0;
            }

            return isInvalid;
        }

        /// <summary>
        /// it validates the char type if text type is NumOnly
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsNumber(int KeyCode)
        {
            if (KeyCode > 47 && KeyCode < 58)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is BackSpace
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsBackSpace(int KeyCode)
        {
            if (KeyCode == 8)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is DotOnly
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsDot(int KeyCode)
        {
            if (KeyCode == 46)
                return true;
            else
                return false;
        }


        /// <summary>
        /// it validates the char type if text type is IsSpecialChar
        /// </summary>
        /// <param name="KeyCode"> code generated by pressing key </param>
        /// <returns> true or false </returns>
        protected bool IsSpecialChar(int KeyCode)
        {
            if (KeyCode > 32 && KeyCode < 39 || KeyCode > 39 && KeyCode < 44 || KeyCode > 44 && KeyCode < 48 || KeyCode > 57 && KeyCode < 65 || KeyCode > 90 && KeyCode < 96 || KeyCode > 122 && KeyCode < 127)
                return true;
            else
                return false;
        }
        #endregion
    }
}
