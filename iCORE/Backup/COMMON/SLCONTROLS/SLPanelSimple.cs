using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Data;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.COMMON.SLCONTROLS
{
    /// <summary>
    /// SLPanelSimple
    /// </summary>
    public partial class SLPanelSimple : SLPanel
    {
        #region --Constructor Segment--
        public SLPanelSimple()
        {
            InitializeComponent();
        }

        public SLPanelSimple(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        /// <summary>
        /// Set Entity
        /// </summary>
        /// <param name="oDR"></param>
        public override void SetEntityObject(System.Data.DataRow oDR)
        {
            base.SetEntityObject(oDR);
        }

        /// <summary>
        /// Load Panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        protected override void LoadPanel(SLPanel ParentPanel)
        {
            BaseForm form = (BaseForm)this.FindForm();
            FillEntity(ParentPanel);
            GetList();
            base.LoadPanel( ParentPanel);
        }

        /// <summary>
        /// Load Panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        /// <param name="oDR"></param>
        protected override void LoadPanel(SLPanel ParentPanel, DataRow oDR)
        {
            BaseForm oForm = (BaseForm)this.FindForm();
            oForm.SetPanelControlWithListItem(this.Controls, oDR);
        }

        /// <summary>
        /// GetList
        /// </summary>
        public void GetList()
        {
            DataTable oDT = new DataTable();
            IDataManager dataManager = RuntimeClassLoader.GetDataManager("");
            Result rslt = new Result();

            rslt = dataManager.GetAll(this.CurrentBusinessEntity, this.SPName, "List");

            if (rslt.isSuccessful)
                oDT = rslt.dstResult.Tables[0];

            if (oDT != null && oDT.Rows.Count > 0)
            {
                BaseForm oForm = (BaseForm)this.FindForm();
                oForm.SetPanelControlWithListItem(this.Controls, oDT.Rows[0]);
                this.LoadConcurrentPanels(oDT.Rows[0]);
                this.SetEntityObject(oDT.Rows[0]);
            }
            else
            {
                (this.FindForm() as BaseForm).ClearForm(this.Controls);
                this.ClearPanel(this.Parent as SLPanel);
                this.ClearConcurrentPanels();
            }
        }
        /// <summary>
        /// Track Changes
        /// </summary>
        /// <returns></returns>
        public override bool HasChanges()
        {
            return base.HasChanges();
        }
    }
}
