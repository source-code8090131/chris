using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Reflection;
using System.Collections;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;



namespace iCORE.COMMON.SLCONTROLS
{
    /// <summary>
    /// Block Type Enumerator
    /// </summary>
    public enum BlockType
    {
        ControlBlock,
        DataBlock
    }

    /// <summary>
    /// Delete Behaviour Enumerator 
    /// </summary>
    public enum DeleteRecordBehavior : int
    {
        Cascading = 0,
        Isolated = 1,
        NonIsolated = 2
    }

    /// <summary>
    /// SLPanel Base Class for Panels
    /// </summary>
    public partial class SLPanel : Panel
    {
        #region --Field Segement--
        private string m_strSPName;
        private string m_strEntityName;
        private string m_strDataManager;
        private bool m_EnableInsert;
        private bool m_EnableUpdate;
        private bool m_EnableDelete;
        private bool m_EnableQuery;
        private BlockType blockType;
        protected BusinessEntity m_oEntityClass;
        private DeleteRecordBehavior m_deleteRecordBehavior;
        private bool m_DisableDependentLoad;
        #endregion

        #region --Constructor Segment--
        public SLPanel()
        {
            blockType = BlockType.DataBlock;
            InitializeComponent();
        }

        public SLPanel(IContainer container)
        {
            container.Add(this);
            blockType = BlockType.DataBlock;
            InitializeComponent();
        }
        #endregion

        #region --Property Segment--

        [Browsable(true)]
        [Description("Gets or Sets the Panel's SP name.")]
        [Category("DataBase")]
        public string SPName
        {
            get { return m_strSPName; }
            set { m_strSPName = value; }
        }

        
        [Browsable(true)]
        [Description("Gets or Sets the Panel's corresponding Entity object.")]
        [Category("DataBase")]
        public string EntityName
        {
            get { return m_strEntityName; }
            set { m_strEntityName = value; }
        }

        [Browsable(true)]
        [Description("Gets or Sets the Panel's corresponding DataManager object.")]
        [Category("DataBase")]
        public string DataManager
        {
            get { return m_strDataManager; }
            set { m_strDataManager = value; }
        }

        private DataRow panelDataRow;
        [Obsolete("This method is obsolete. Use Current business entity instead")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DataRow PanelDataRow
        {
            get { return panelDataRow; }
            set { panelDataRow = value; }
        }
        [Browsable(true)]
        [Description("Gets or Sets the Panel's Enable Query Mode.")]
        [Category("DataBase")]
        public bool EnableQuery
        {
            get { return m_EnableQuery; }
            set { m_EnableQuery = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public BusinessEntity CurrentBusinessEntity
        {
            get
            {
                if (m_oEntityClass == null || String.IsNullOrEmpty(this.EntityName))
                {
                    m_oEntityClass = RuntimeClassLoader.GetBusinessEntity(this.EntityName);
                }
                return m_oEntityClass;
            }
            set
            {
                m_oEntityClass = value;
            }
        }
                
        [Browsable(true)]
        [Description("Gets or Sets the Panel's Behavior on Delete Action.")]
        public DeleteRecordBehavior DeleteRecordBehavior
        {
            get { return m_deleteRecordBehavior; }
            set { m_deleteRecordBehavior = value; }
        }
        
        //[Browsable(true)]
        [Description("Gets or Sets the Panel's corresponding DependentPanel object.")]
        [Category("Behaviour")]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<SLPanel> DependentPanels
        {
            [DebuggerStepThrough()]
            get { return oPanelCollection; }
            [DebuggerStepThrough()]
            set 
            { 
                oPanelCollection = value;
                SetMasterPanel();
            }
        }
        List<SLPanel> oPanelCollection;

        //[Browsable(true)]
        [Description("Gets or Sets the Panel's corresponding Concurrent object.")]
        [Category("Behaviour")]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<SLPanel> ConcurrentPanels
        {
            [DebuggerStepThrough()]
            get { return oConcurrentPanelCollection; }
            [DebuggerStepThrough()]
            set
            {
                oConcurrentPanelCollection = value;
                SetMasterPanel();
            }
        }
        List<SLPanel> oConcurrentPanelCollection;

        [Browsable(true)]
        [Description("Gets or Sets the Panel's Block Type.")]
        [Category("DataBase")]
        public BlockType PanelBlockType
        {
            get { return blockType; }
            set { blockType = value; }
        }

        [Browsable(true)]
        [Description("Gets or Sets the Panel's EnableInsert Mode.")]
        [Category("DataBase")]
        public bool EnableInsert
        {
            get { return m_EnableInsert; }
            set { m_EnableInsert = value; }
        }

        [Browsable(true)]
        [Description("Gets or Sets the Panel's EnableUpdate Mode.")]
        [Category("DataBase")]
        public bool EnableUpdate
        {
            get { return m_EnableUpdate; }
            set { m_EnableUpdate = value; }
        }

        [Browsable(true)]
        [Description("Gets or Sets the Panel's EnableDelete Mode.")]
        [Category("DataBase")]
        public bool EnableDelete
        {
            get { return m_EnableDelete; }
            set { m_EnableDelete = value; }
        }

        [Browsable(true)]
        [Description("Gets or Sets to load dependent panels on save.")]
        [Category("DataBase")]
        public bool DisableDependentLoad
        {
            get { return m_DisableDependentLoad; }
            set { m_DisableDependentLoad = value; }
        }

        [Browsable(true)]
        [Description("Gets true if the panel has one or more dependent panel(s).")]
        [Category("DataBase")]
        public bool HasDependentPanels
        {
            get 
            {
                if (this.DependentPanels == null)
                    return false;
                return this.DependentPanels.Count > 0;
            }
        }

        [Description("Gets true if the panel has one or more dependent panel(s).")]
        [Category("DataBase")]
        private SLPanel masterPanel;
        public SLPanel MasterPanel
        {
            get { return masterPanel; }
            set { masterPanel = value; }
        }

        /// <summary>
        /// Set Master Panel for each dependent panel
        /// </summary>
        private void SetMasterPanel()
        {
            if (oPanelCollection != null)
            {
                foreach (SLPanel panel in oPanelCollection)
                {
                    panel.MasterPanel = this;
                }
            }
        }

        /// <summary>
        /// Clear BusinessEntity.
        /// </summary>
        /// <returns></returns>
        public void ClearBusinessEntity()
        {
            m_oEntityClass = RuntimeClassLoader.GetBusinessEntity(this.EntityName);
        }

        /// <summary>
        /// Get or List from DB
        /// Set the Fields
        /// </summary>
        protected virtual void LoadPanel(SLPanel ParentPanel)
        {
            LoadDependentPanels();          
        }

        /// <summary>
        /// Get or List from DB
        /// Set the Fields
        /// </summary>
        protected virtual void LoadPanel(SLPanel ParentPanel, DataRow oDR)
        {
            LoadConcurrentPanels(oDR);
        }

        /// <summary>        
        /// Clear Panels
        /// </summary>
        protected virtual void ClearPanel(SLPanel ParentPanel)
        {
            ClearDependentPanels();
        }

        private bool ShouldSerializeDependentPanel()
        {
            return true;
        }

        /// <summary>
        /// Fill entity from DataRow
        /// </summary>
        /// <param name="oDR"></param>
        public virtual void SetEntityObject(DataRow oDR)
        {
            SetEntityProperties(oDR);
        }

        /// <summary>
        /// Set Entity Properties
        /// </summary>
        /// <param name="oDR"></param>
        private void SetEntityProperties(DataRow oDR)
        {
            PropertyInfo newProperty;
            string TypeName;

            foreach (DataColumn oColLoop in oDR.Table.Columns)
            {
                newProperty = CurrentBusinessEntity.GetType().GetProperty(oColLoop.ColumnName);

                if (newProperty != null && newProperty.CanWrite)
                {
                    TypeName = newProperty.PropertyType.Name;
                    if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        TypeName = newProperty.PropertyType.GetGenericArguments()[0].Name;
                    }
                    if ((newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) && oDR[oColLoop.ColumnName] == DBNull.Value)
                    {
                        newProperty.SetValue(CurrentBusinessEntity, null, null);
                    }
                    else
                    {
                        switch (TypeName)
                        {
                            case "DateTime":
                                DateTime oDateTime;
                                if (DateTime.TryParse(oDR[oColLoop.ColumnName].ToString(), out oDateTime))
                                    newProperty.SetValue(CurrentBusinessEntity, Convert.ToDateTime(oDR[oColLoop.ColumnName].ToString()), null);
                                else
                                    newProperty.SetValue(CurrentBusinessEntity, DateTime.MinValue, null);
                                break;
                            case "Int32":
                                Int32 oInt32;
                                if (Int32.TryParse(oDR[oColLoop.ColumnName].ToString(), out oInt32))
                                    newProperty.SetValue(CurrentBusinessEntity, Convert.ToInt32(oDR[oColLoop.ColumnName].ToString()), null);
                                else
                                    newProperty.SetValue(CurrentBusinessEntity, 0, null);

                                break;
                            case "Single":
                                Single oSingle;
                                if (Single.TryParse(oDR[oColLoop.ColumnName].ToString(), out oSingle))
                                    newProperty.SetValue(CurrentBusinessEntity, Convert.ToSingle(oDR[oColLoop.ColumnName].ToString()), null);
                                
                                break;

                            case "Double":
                                Double oDouble;
                                if (Double.TryParse(oDR[oColLoop.ColumnName].ToString(), out oDouble))
                                    newProperty.SetValue(CurrentBusinessEntity, Convert.ToDouble(oDR[oColLoop.ColumnName].ToString()), null);
                                else 
                                newProperty.SetValue(CurrentBusinessEntity,Convert.ToDouble(0.0), null);
                                break;
                            case "Decimal":
                                Decimal oDecimal;
                                if (Decimal.TryParse(oDR[oColLoop.ColumnName].ToString(), out oDecimal))
                                    newProperty.SetValue(CurrentBusinessEntity, Convert.ToDecimal(oDR[oColLoop.ColumnName].ToString()), null);
                                else
                                newProperty.SetValue(CurrentBusinessEntity, Convert.ToDecimal(0.0), null);
                                break;
                            case "Boolean":
                                bool oBool;
                                if (bool.TryParse(oDR[oColLoop.ColumnName].ToString(), out oBool))
                                    newProperty.SetValue(CurrentBusinessEntity, Convert.ToBoolean(oDR[oColLoop.ColumnName]), null);
                                else
                                    newProperty.SetValue(CurrentBusinessEntity, false, null);
                                break;
                            default:
                                newProperty.SetValue(CurrentBusinessEntity, oDR[oColLoop.ColumnName].ToString(), null);
                                break;
                        }
                    }
                }
            }
        }

        ///<summary>
        /// Fill entity from output parameter of result
        ///</summary>
        public void FillEntity(IEntityCommand entity, Hashtable hstOutPutParam)
        {
            try
            {
                foreach (PropertyInfo property in entity.GetType().GetProperties())
                {
                    PropertyInfo oProp = entity.GetType().GetProperty(property.Name);
                    if (null != oProp && oProp.CanWrite)
                    {
                        if (hstOutPutParam.Contains("Table0." + oProp.Name + "0"))
                        {
                            oProp.SetValue(entity, hstOutPutParam["Table0." + oProp.Name + "0"], null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        /// <summary>
        /// Copy Foreign Key property value from Parent panel
        /// </summary>
        /// <param name="ParentPanel"></param>
        public void FillEntity(SLPanel ParentPanel)
        {
            if (ParentPanel != null)
            {
                //this.CurrentBusinessEntity = RuntimeClassLoader.GetBusinessEntity(this.EntityName);
                foreach (PropertyInfo property in ParentPanel.CurrentBusinessEntity.GetType().GetProperties())
                {
                    PropertyInfo oProp = this.CurrentBusinessEntity.GetType().GetProperty(property.Name);
                    if (null != oProp && oProp.CanWrite)
                    {
                        object[] customAttributes = oProp.GetCustomAttributes(typeof(CustomAttributes), false);
                        foreach (object attribute in customAttributes)
                        {
                            CustomAttributes storageAttributes = (CustomAttributes)attribute;
                            if (storageAttributes.IsForeignKey)
                            {
                                oProp.SetValue(this.CurrentBusinessEntity, property.GetValue(ParentPanel.CurrentBusinessEntity, null), null);
                            }
                        }
                    }
                }
                if(this.DependentPanels != null)
                {
                    foreach (SLPanel childPanel in this.DependentPanels)
                    {
                        childPanel.FillEntity(this);
                    }
                }
            }
        }

        /// <summary>
        /// Set specific property
        /// </summary>
        public  void SetEntityProperty(string pstrName, string pstrValue)
        {
            //PropertyInfo newProperty = m_oEntityClass.GetType().GetProperty(pstrName, BindingFlags.IgnoreCase);
            PropertyInfo newProperty = CurrentBusinessEntity.GetType().GetProperty(pstrName);
            string TypeName;
            if (newProperty != null && newProperty.CanWrite)
            {
                TypeName = newProperty.PropertyType.Name;
                if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    TypeName = newProperty.PropertyType.GetGenericArguments()[0].Name;
                }
                switch (TypeName)
                {
                    case "DateTime":
                        DateTime oDateTime;
                        if (pstrValue.Equals("") || Convert.ToDateTime(pstrValue) == DateTime.MinValue)
                            newProperty.SetValue(m_oEntityClass, null, null);
                        else if (DateTime.TryParse(pstrValue, out oDateTime))
                            newProperty.SetValue(m_oEntityClass, Convert.ToDateTime(pstrValue), null);
                        break;
                    case "Int32":
                        Int32 oInt32;
                        if (Int32.TryParse(pstrValue, out oInt32))
                            newProperty.SetValue(m_oEntityClass, Convert.ToInt32(pstrValue), null);
                        break;
                    case "Double":
                        Double ValueDoubleType;
                        if (Double.TryParse(pstrValue, out ValueDoubleType))
                            newProperty.SetValue(m_oEntityClass, Convert.ToDouble(pstrValue), null);
                        break;
                    case "Decimal":
                        Decimal ValueDecimalType;
                        if (Decimal.TryParse(pstrValue, out ValueDecimalType))
                            newProperty.SetValue(m_oEntityClass, Convert.ToDecimal(pstrValue), null);
                        break;
                    default:
                        newProperty.SetValue(CurrentBusinessEntity, pstrValue, null);
                        break;
                }
            }
        }

        /// <summary>
        /// Load Concurrent Panels
        /// </summary>
        /// <param name="dr"></param>
        public void LoadConcurrentPanels(DataRow dr)
        {
            if (ConcurrentPanels != null && ConcurrentPanels.Count > 0)
            {
                foreach (SLPanel parallelPanel in ConcurrentPanels)
                {
                    parallelPanel.LoadPanel(this, dr);
                }
            }
        }

        /// <summary>
        /// Load dependent panel
        /// </summary>
        public void LoadDependentPanels()
        {
            if (DependentPanels != null && DependentPanels.Count > 0)
            {
                foreach (SLPanel childPanel in DependentPanels)
                {
                    childPanel.LoadPanel(this);
                }
            }
        }

        /// <summary>
        /// Reset current business entity
        /// Reset foreign key
        /// Reload dependent panels recursively
        /// </summary>
        public void ClearDependentPanels()
        {
            this.ClearBusinessEntity();
            this.FillEntity(this.MasterPanel);
            if (DependentPanels != null && DependentPanels.Count > 0)
            {
                foreach (SLPanel childPanel in DependentPanels)
                {
                    childPanel.ClearDependentPanels();
                    childPanel.LoadPanel(this);
                }
            }
        }

        /// <summary>
        /// Reset current business entity
        /// Reset foreign key
        /// Reload dependent panels recursively
        /// </summary>
        public void ClearConcurrentPanels()
        {
            this.ClearBusinessEntity();
            this.FillEntity(this.MasterPanel);
            if (ConcurrentPanels != null && ConcurrentPanels.Count > 0)
            {
                foreach (SLPanel childPanel in ConcurrentPanels)
                {
                    childPanel.LoadPanel(this);
                }
            }
        }

        /// <summary>
        /// Check if uncommitted data exists in current panel or its child panel
        /// </summary>
        /// <returns></returns>
        public virtual Boolean HasChanges()
        {
            return false;
        }
        #endregion
    }
}
