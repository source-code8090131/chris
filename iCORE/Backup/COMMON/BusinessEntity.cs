using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
//using iCORE.MMSEC.DATAOBJECTS;

namespace iCORE.Common
{
    public class BusinessEntity : IEntityCommand
    {

        #region --Field Segment--
        private const string m_strParamPreFix = "@";
        private const string m_strActionType = "ActionType";
        private string m_strBusinessEntityNamespace = "iCORE.";
        private string searchFilter;
        private string soeId;

        public string SoeId
        {
            get { return soeId; }
            set { soeId = value; }
        }

        public string SearchFilter
        {
            get { return searchFilter; }
            set { searchFilter = value; }
        }
 

        #endregion

        

        #region --Method Segment--

        /// <summary>
        /// BindLOVCombo
        /// </summary>
        /// <param name="poSLCombo"></param>
        public void BindLOVCombo(SLComboBox poSLCombo)
        {
            DataSet oDT = new DataSet();
            Result rslt = new Result();
            CommonDataManager oCmnDataMgr;
            oCmnDataMgr = new CommonDataManager();

            if (poSLCombo.ComboBehaviour == eComboBehaviour.LOVKeyCode)
            {
                rslt = oCmnDataMgr.GetLOVKeyCode("CMN_SP_GetLOVCodeByType", poSLCombo.LOVType);
            }
            else if (poSLCombo.ComboBehaviour == eComboBehaviour.LOVEntityCode)
            {
               rslt = oCmnDataMgr.GetLOVEntityCode(poSLCombo.BusinessEntity, poSLCombo.SPName, "LOV");
            }

            if (rslt.isSuccessful && rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
            {
                poSLCombo.ValueMember = "LOVCodeID";
                poSLCombo.DisplayMember = "LOVCodeName";
                poSLCombo.DataSource = rslt.dstResult.Tables[0];
            }
            else
            {
                if (poSLCombo.DataSource == null)
                {
                    poSLCombo.Items.Add("--Not Available--");
                    poSLCombo.SelectedIndex = 0;
                }
            }
        }


        #endregion

    }


}
