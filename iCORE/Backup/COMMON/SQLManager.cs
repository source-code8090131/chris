using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using iCORE.Common;

/*****************************************************************
  Class Name:  SQLManager 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Shamraiz Ahmad
  Created Date: 22-08-2007
  Version No: 1.0
  Modification: <Mention the modification in the class >
  Modified By:
  Modification Date:
  Version No: <The version No after modification>

*****************************************************************/

namespace iCORE.Common
{
    /// <summary>
    /// DB Helper class
    /// </summary>
    public class SQLManager
    {
        private static DataSet dstParams = new DataSet();
        private static XMS.DATAOBJECTS.ConnectionBean connbean = null;
        internal static string userId = null;
        private const string ActionType = "ActionType";

        /// <summary>
        /// Set Connection object
        /// </summary>
        /// <param name="connbean_obj"></param>
        public static void SetConnectionBean(XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            if (connbean == null)
                connbean = connbean_obj;
        }

        /// <summary>
        /// Return Connection Object
        /// </summary>
        /// <returns></returns>
        public static XMS.DATAOBJECTS.ConnectionBean GetConnectionBean()
        {
            return connbean;
        }

        /// <summary>
        /// Set USer ID
        /// </summary>
        /// <param name="usrId"></param>
        public static void SetFormInfo(string usrId)
        {
            if (userId == null)
                userId = usrId;
        }

        /// <summary>
        /// Execute Query
        /// </summary>
        /// <param name="dstParams"></param>
        /// <returns></returns>
        public static Result ExecuteDML(DataSet dstParams)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();

            try
            {
                conn = connbean.getDatabaseConnection();
                trans = conn.BeginTransaction();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        CreateCommandParameters(ref cmd, dtlParams, i);

                        cmd.ExecuteNonQuery();

                        foreach (SqlParameter param in cmd.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput ||
                                param.Direction == ParameterDirection.Output ||
                                param.Direction == ParameterDirection.ReturnValue)
                            {
                                dtlParams.Rows[i][param.ParameterName] =
                                    param.Value;
                                //rslt.hstOutParams.Add(GetTableName(dtlParams.TableName) + "." +
                                //    param.ParameterName.Substring(0) + i, param.Value);
                                rslt.hstOutParams.Add("Table0." + param.ParameterName.Substring(0) + i, param.Value);
                            }
                        }
                    }
                }

                trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to enable later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message.StartsWith("DELETE statement conflicted with COLUMN REFERENCE constraint") ?
                    "Reference records found in other tables." : ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                trans = null;
            }

            return rslt;
        }


        /// <summary>
        /// Execute Query with transactions
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Result ExecuteDML(DataSet dstParams, ref SqlTransaction trans)
        {
            SqlDataAdapter adp = null;
            DataSet dst = null;
            Result rslt = new Result();
            SqlCommand cmd = null;
            //SqlTransaction trans = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();
            rslt.dstResult = new DataSet();
            try
            {

                if (trans == null)
                {
                    conn = connbean.getDatabaseConnection();
                    trans = conn.BeginTransaction();
                }
                else
                {
                    conn = trans.Connection;
                }

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        CreateCommandParameters(ref cmd, dtlParams, i);

                       // cmd.ExecuteNonQuery();
                          adp = new SqlDataAdapter(cmd);
                           dst = new DataSet();
                            adp.Fill(dst, dtlParams.TableName);
                        foreach (DataTable dtl in dst.Tables)
                        {
                            rslt.dstResult.Tables.Add(dtl.Copy());
                        }
                        foreach (SqlParameter param in cmd.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput ||
                                param.Direction == ParameterDirection.Output ||
                                param.Direction == ParameterDirection.ReturnValue)
                            {
                                dtlParams.Rows[i][param.ParameterName] =
                                    param.Value;
                                //rslt.hstOutParams.Add(GetTableName(dtlParams.TableName) + "." +
                                //    param.ParameterName.Substring(0) + i, param.Value);
                                rslt.hstOutParams.Add("Table0." + param.ParameterName.Substring(0) + i, param.Value);
                            }
                        }
                    }
                }

                //trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to enable later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                //trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message.StartsWith("DELETE statement conflicted with COLUMN REFERENCE constraint") ?
                    "Reference records found in other tables." : ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                //connbean.closeConn(conn);

                conn = null;
                cmd = null;
                //trans = null;
            }

            return rslt;
        }

        /// <summary>
        /// Execute Query with transactions
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Result ExecuteDML(DataSet dstParams, ref SqlTransaction trans, int? timeout)
        {
            SqlDataAdapter adp = null;
            DataSet dst = null;
            Result rslt = new Result();
            SqlCommand cmd = null;
            //SqlTransaction trans = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();
            rslt.dstResult = new DataSet();
            try
            {

                if (timeout != null)
                    conn = connbean.getDatabaseConnection((int)timeout);
                else
                    conn = connbean.getDatabaseConnection();

                if (trans == null)
                {
                    trans = conn.BeginTransaction();
                }
                else
                {
                    conn = trans.Connection;
                }

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        CreateCommandParameters(ref cmd, dtlParams, i);

                        // cmd.ExecuteNonQuery();
                        adp = new SqlDataAdapter(cmd);
                        dst = new DataSet();
                        adp.Fill(dst, dtlParams.TableName);
                        foreach (DataTable dtl in dst.Tables)
                        {
                            rslt.dstResult.Tables.Add(dtl.Copy());
                        }
                        foreach (SqlParameter param in cmd.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput ||
                                param.Direction == ParameterDirection.Output ||
                                param.Direction == ParameterDirection.ReturnValue)
                            {
                                dtlParams.Rows[i][param.ParameterName] =
                                    param.Value;
                                //rslt.hstOutParams.Add(GetTableName(dtlParams.TableName) + "." +
                                //    param.ParameterName.Substring(0) + i, param.Value);
                                rslt.hstOutParams.Add("Table0." + param.ParameterName.Substring(0) + i, param.Value);
                            }
                        }
                    }
                }

                //trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to enable later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                //trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message.StartsWith("DELETE statement conflicted with COLUMN REFERENCE constraint") ?
                    "Reference records found in other tables." : ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                //connbean.closeConn(conn);

                conn = null;
                cmd = null;
                //trans = null;
            }

            return rslt;
        }


        /// <summary>
        /// Obsoleted Method. Logging DML
        /// </summary>
        /// <param name="dstParams"></param>
        private static void LogDMLAcitivities(DataSet dstParams)
        {
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            SqlConnection conn = null;

            try
            {
                conn = connbean.getDatabaseConnection();
                trans = conn.BeginTransaction();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand("SL_SP_DMLActivityLog_Add", conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        cmd.Parameters.Clear();

                        cmd.Parameters.Add("@TableName", SqlDbType.VarChar);
                        cmd.Parameters["@TableName"].Value = GetTableName(dtlParams.TableName);
                        cmd.Parameters.Add("@SPName", SqlDbType.VarChar);
                        cmd.Parameters["@SPName"].Value = dtlParams.TableName;
                        cmd.Parameters.Add("@RecordData", SqlDbType.VarChar);
                        cmd.Parameters["@RecordData"].Value = GetData(dtlParams.Rows[i]);
                        cmd.Parameters.Add("@UserID", SqlDbType.VarChar);
                        cmd.Parameters["@UserID"].Value = userId;
                        cmd.Parameters.Add("@Activity", SqlDbType.VarChar);
                        cmd.Parameters["@Activity"].Value =
                            dtlParams.TableName.Substring(dtlParams.TableName.LastIndexOf("_") + 1);

                        cmd.ExecuteNonQuery();
                    }
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();

                LogException("SQLManager", "LogDMLAcitivities", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                trans = null;
            }
        }

        /// <summary>
        /// Log Exceptions
        /// </summary>
        /// <param name="className"></param>
        /// <param name="functionName"></param>
        /// <param name="exp"></param>
        public static void LogException(string className, string functionName, Exception exp)
        {
            CrplControlLibrary.ErrorLogController.LogError(
                "SL", className, functionName, exp, SQLManager.userId);
        }

        /// <summary>
        /// GetTableName
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        protected internal static string GetTableName(string spName)
        {
            string tableName = "";

            tableName = "SL_" + spName.Substring(7, spName.IndexOf("_", 7) - 7);

            return tableName;
        }

        /// <summary>
        /// GetData
        /// </summary>
        /// <param name="drw"></param>
        /// <returns></returns>
        private static string GetData(DataRow drw)
        {
            string data = "";

            if (drw.Table.Columns.Contains("Id"))
            {
                data = "Id=" + drw["Id"];
            }
            else
            {
                foreach (DataColumn dcl in drw.Table.Columns)
                {
                    data += dcl.ColumnName + "=" + drw[dcl.ColumnName] + ",";
                }
            }

            return data;
        }

        /// <summary>
        /// Get ResultSet
        /// </summary>
        /// <param name="dstParams"></param>
        /// <returns></returns>
        public static Result SelectDataSet(DataSet dstParams)
        {
            return SelectDataSet(dstParams,null);
        }

        /// <summary>
        /// Get ResultSet
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static Result SelectDataSet(DataSet dstParams, int? timeout)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                if (timeout != null)                
                    conn = connbean.getDatabaseConnection((int)timeout);                
                else
                    conn = connbean.getDatabaseConnection();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (timeout != null && cmd != null) 
                        cmd.CommandTimeout = (int)timeout;


                    CreateCommandParameters(ref cmd, dtlParams);

                    adp = new SqlDataAdapter(cmd);
                    dst = new DataSet();

                    adp.Fill(dst, dtlParams.TableName);

                    foreach (DataTable dtl in dst.Tables)
                    {
                        rslt.dstResult.Tables.Add(dtl.Copy());
                    }
                }

                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// Get ResultSet with transactions
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Result SelectDataSet(DataSet dstParams, ref SqlTransaction trans)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                if (trans == null)
                {
                    conn = connbean.getDatabaseConnection();
                    trans = conn.BeginTransaction();
                }
                else
                {
                    conn = trans.Connection;
                }


                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = trans;
                    CreateCommandParameters(ref cmd, dtlParams);

                    adp = new SqlDataAdapter(cmd);
                    dst = new DataSet();

                    adp.Fill(dst, dtlParams.TableName);

                    foreach (DataTable dtl in dst.Tables)
                    {
                        rslt.dstResult.Tables.Add(dtl.Copy());
                    }
                }

                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet", ex);
            }
            finally
            {
                //connbean.closeConn(conn);

                // conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// Get ResultSet
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public static Result SelectDataSet(string spName)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(spName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                adp = new SqlDataAdapter(cmd);
                dst = new DataSet();

                adp.Fill(dst);

                foreach (DataTable dtl in dst.Tables)
                {
                    rslt.dstResult.Tables.Add(dtl.Copy());
                }

                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet(string spName)", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// Excecute Scalar
        /// </summary>
        /// <param name="dtlParams"></param>
        /// <returns></returns>
        public static Result SelectScaler(DataTable dtlParams)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(dtlParams.TableName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                CreateCommandParameters(ref cmd, dtlParams);

                rslt.objResult = cmd.ExecuteScalar();
                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectScaler", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
            }

            return rslt;
        }

        /// <summary>
        /// Excecute Scalar
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public static Result SelectScaler(string spName)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(spName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                rslt.objResult = cmd.ExecuteScalar();
                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectScaler(string spName)", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
            }

            return rslt;
        }

        /// <summary>
        /// Create Parameters
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        private static void CreateCommandParameters(ref SqlCommand cmd, DataTable dtlParams)
        {
            CreateCommandParameters(ref cmd, dtlParams, 0);
        }

        /// <summary>
        /// Create Parameters
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        /// <param name="rowIndex"></param>
        private static void CreateCommandParameters(ref SqlCommand cmd, DataTable dtlParams, int rowIndex)
        {
            int i = 0;
            SqlParameter parameter = null;

            try
            {
                cmd.Parameters.Clear();

                foreach (DataColumn dcl in dtlParams.Columns)
                {
                    parameter = new SqlParameter(dcl.ColumnName, GetDBType(dcl.DataType.ToString()));
                    parameter.Value = dtlParams.Rows[rowIndex][i];

                    if (Convert.ToBoolean(dcl.ExtendedProperties["IsOutParam"]))
                        parameter.Direction = ParameterDirection.InputOutput;                        
                    else
                        parameter.Direction = ParameterDirection.Input;

                    if (dcl.ExtendedProperties["length"] != DBNull.Value)
                        parameter.Size = Convert.ToInt32(dcl.ExtendedProperties["length"]);
                    if (dcl.ExtendedProperties["scale"] != DBNull.Value)
                        parameter.Scale = Convert.ToByte(dcl.ExtendedProperties["scale"]);
                    if (dcl.ExtendedProperties["prec"] != DBNull.Value)
                        parameter.Precision = Convert.ToByte(dcl.ExtendedProperties["prec"]);

                    cmd.Parameters.Add(parameter);
                    i++;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateCommandParameters", ex);
            }
        }

        /// <summary>
        /// SP Param Dynamic Discovery 
        /// </summary>
        /// <param name="strSPName"></param>
        /// <returns></returns>
        public static DataTable GetSPParams(string strSPName)
        {
            if (!dstParams.Tables.Contains(strSPName))
                CreateSPParams(strSPName);

            return dstParams.Tables[strSPName].Copy();
        }

        /// <summary>
        /// SP Param Data Table with extended properties
        /// </summary>
        /// <param name="strSPName"></param>
        private static void CreateSPParams(string strSPName)
        {
            DataSet dst = new DataSet();
            DataTable dtlParam = new DataTable(strSPName);
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;

            try
            {
                try
                {
                    conn = connbean.getDatabaseConnection();
                    cmd = new SqlCommand("CMN_SP_StoredProcedures_GetParams", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SPName", SqlDbType.VarChar);
                    cmd.Parameters["@SPName"].Value = strSPName;

                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dst);
                }
                catch (Exception ex)
                {
                    LogException("SQLManager", "Inner Block CreateSPParams", ex);

                    throw ex;
                }
                finally
                {
                    connbean.closeConn(conn);

                    conn = null;
                    cmd = null;
                    adp = null;
                }

                foreach (DataRow drw in dst.Tables[0].Rows)
                {
                    dtlParam.Columns.Add(drw["ParamName"].ToString(), GetSystemType(drw["ParamType"].ToString()));
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("IsOutParam",
                        drw["IsOutParam"]);
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("prec",
                        drw["prec"]);
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("scale",
                        drw["scale"]);
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("length",
                        drw["length"]);
                }

                dtlParam.AcceptChanges();
                dstParams.Tables.Add(dtlParam);
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateSPParams", ex);

                throw ex;
            }
        }

        /// <summary>
        /// Get DB Type
        /// </summary>
        /// <param name="strSystemType"></param>
        /// <returns></returns>
        private static SqlDbType GetDBType(string strSystemType)
        {
            SqlDbType dbType = SqlDbType.VarChar;

            switch (strSystemType)
            {
                case "System.String":
                    dbType = SqlDbType.VarChar;
                    break;
                case "System.Int64":
                    dbType = SqlDbType.BigInt;
                    break;
                case "System.Int32":
                    dbType = SqlDbType.Int;
                    break;
                case "System.DateTime":
                    dbType = SqlDbType.DateTime;
                    break;
                case "System.Double":
                    dbType = SqlDbType.Float;
                    break;
                case "System.Boolean":
                    dbType = SqlDbType.Bit;
                    break;
                case "System.Byte[]":
                    dbType = SqlDbType.Binary;
                    break;
                case "System.Single":
                    dbType = SqlDbType.Float;
                    break;
                case "System.Int16":
                    dbType = SqlDbType.SmallInt;
                    break;
                case "System.Decimal":
                    dbType = SqlDbType.Decimal;
                    break;
            }

            return dbType;
        }

        /// <summary>
        /// Get System Type
        /// </summary>
        /// <param name="strSQLType"></param>
        /// <returns></returns>
        private static Type GetSystemType(string strSQLType)
        {
            Type type = Type.GetType("System.String");

            switch (strSQLType)
            {
                case "text":
                    type = Type.GetType("System.String");
                    break;
                case "tinyint":
                    type = Type.GetType("System.Int32");
                    break;
                case "smallint":
                    type = Type.GetType("System.Int32");
                    break;
                case "int":
                    type = Type.GetType("System.Int32");
                    break;
                case "smalldatetime":
                    type = Type.GetType("System.DateTime");
                    break;
                case "real":
                    type = Type.GetType("System.Double");
                    break;
                case "money":
                    type = Type.GetType("System.Double");
                    break;
                case "datetime":
                    type = Type.GetType("System.DateTime");
                    break;
                case "float":
                    type = Type.GetType("System.Double");
                    break;
                case "ntext":
                    type = Type.GetType("System.String");
                    break;
                case "bit":
                    type = Type.GetType("System.Boolean");
                    break;
                case "decimal":
                    type = Type.GetType("System.Double");
                    break;
                case "numeric":
                    //type = Type.GetType("System.Int32");
                    type = Type.GetType("System.Decimal");// UPDATED BY TARIQ AFZAL 
                    break;
                case "smallmoney":
                    type = Type.GetType("System.Double");
                    break;
                case "bigint":
                    type = Type.GetType("System.Int32");
                    break;
                case "varchar":
                    type = Type.GetType("System.String");
                    break;
                case "char":
                    type = Type.GetType("System.String");
                    break;
                case "nvarchar":
                    type = Type.GetType("System.String");
                    break;
                case "nchar":
                    type = Type.GetType("System.String");
                    break;
            }

            return type;
        }

        /// <summary>
        /// Check for Boolean Value
        /// </summary>
        /// <param name="boolValue"></param>
        /// <returns></returns>
        public static bool EvaluateBool(object boolValue)
        {
            if (boolValue is System.DBNull || boolValue == null)
                boolValue = false;

            return (bool)boolValue;
        }

        /// <summary>
        /// Check for String Value
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static string EvaluateString(object stringValue)
        {
            if (stringValue is System.DBNull || stringValue == null)
                stringValue = "";

            return stringValue.ToString();
        }

        #region --New Methods--
        /// <summary>
        /// Batch Update
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="poDT"></param>
        /// <CreatedBy>S.Haider Raza</CreatedBy>
        /// <returns></returns>
        public static Result ExecuteBatchUpdate(DataSet dstParams, DataTable poDT)
        {
            Result rslt = new Result();
            SqlCommand cmdInsert = null;
            SqlCommand cmdUpdate = null;
            SqlCommand cmdDel = null;
            SqlTransaction trans = null;
            SqlConnection conn = null;
            SqlDataAdapter adpt = null;

            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();

            try
            {
                conn = connbean.getDatabaseConnection();
                //trans = conn.BeginTransaction();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmdInsert = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmdUpdate = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmdDel = new SqlCommand(dtlParams.TableName, conn, trans);

                    cmdInsert.CommandType = CommandType.StoredProcedure;
                    cmdInsert.UpdatedRowSource = UpdateRowSource.OutputParameters;
                    cmdUpdate.CommandType = CommandType.StoredProcedure;
                    cmdUpdate.UpdatedRowSource = UpdateRowSource.None;
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.UpdatedRowSource = UpdateRowSource.None;

                    CreateCommandParamsForBatch(ref cmdInsert, dstParams.Tables[0]);
                    CreateCommandParamsForBatch(ref cmdUpdate, dstParams.Tables[0]);
                    CreateCommandParamsForBatch(ref cmdDel, dstParams.Tables[0]);

                    if (cmdInsert.Parameters.Contains(ActionType))
                        cmdInsert.Parameters[ActionType].Value =
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Save);
                    else
                        cmdInsert.Parameters.Add(new SqlParameter(ActionType,
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Save)));

                    if (cmdUpdate.Parameters.Contains(ActionType))
                        cmdUpdate.Parameters[ActionType].Value =
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Update);
                    else
                        cmdUpdate.Parameters.Add(new SqlParameter(ActionType,
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Update)));

                    if (cmdDel.Parameters.Contains(ActionType))
                        cmdDel.Parameters[ActionType].Value =
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Delete);
                    else
                        cmdDel.Parameters.Add(new SqlParameter(ActionType,
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Delete)));

                    adpt = new SqlDataAdapter();
                    adpt.InsertCommand = cmdInsert;
                    adpt.UpdateCommand = cmdUpdate;
                    adpt.DeleteCommand = cmdDel;
                    rslt.objResult = adpt.Update(poDT);                    
                }

                //trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to be enabled later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                //trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = "Error in updating batch records!" + ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmdInsert = null;
                cmdUpdate = null;
                cmdDel = null;
                trans = null;
            }

            return rslt;
        }

        /// <summary>
        /// CreateCommandParamsForBatch
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        private static void CreateCommandParamsForBatch(ref SqlCommand cmd, DataTable dtlParams)
        {
            SqlParameter parameter = null;

            try
            {
                cmd.Parameters.Clear();

                foreach (DataColumn dcl in dtlParams.Columns)
                {
                    parameter = new SqlParameter(dcl.ColumnName, GetDBType(dcl.DataType.ToString()));
                    if (dcl.ColumnName != ActionType )
                    parameter.SourceColumn = dcl.ColumnName;

                    if (Convert.ToBoolean(dcl.ExtendedProperties["IsOutParam"]))
                        parameter.Direction = ParameterDirection.InputOutput;
                    else
                        parameter.Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(parameter);
                }

            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateCommandParamsForBatch", ex);
            }
        }

        #endregion
    }
}