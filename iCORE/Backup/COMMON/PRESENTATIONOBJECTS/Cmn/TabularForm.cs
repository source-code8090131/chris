using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Specialized;
using CrplControlLibrary;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    /// <summary>
    /// TabularForm
    /// </summary>
    public partial class TabularForm : BaseForm
    {

        #region --Constructor Segment--

        public TabularForm()
        {
            InitializeComponent();

        }

        public TabularForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            this.connbean = connbean_obj;
            this.SetConnectionBean();
            InitializeComponent();
            this.MdiParent = mainmenu;
        }

        #endregion

        #region --Property Segment--

        [Browsable(true)]
        [Description("Gets or Sets the Form's title.")]
        [Category("FormLayout")]
        public string SetFormTitle
        {
            get { return this.lblFormTitle.Text; }
            set
            {
                this.lblFormTitle.Text = value;

                this.lblFormTitle.Top = 50;

            }
        }

        /// <summary>
        /// Set Title Color
        /// </summary>
        public Color FormTitleColor
        {
            set
            {
                this.lblFormTitle.ForeColor = value;
            }
        }
        
        #endregion

        #region --Event Segment--

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.operationMode = Mode.View;
        }
        /// <summary>
        /// DoToolbarActions
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            foreach (Control oCtrlPanel in ctrlsCollection)
            {
                if (oCtrlPanel is SLDataGridView)
                {
                    SLDataGridView dgvRecords = (SLDataGridView)oCtrlPanel;

                    if (currentQueryMode == Enumerations.eQueryMode.ExecuteQuery && actionType == "Query")
                    {
                        dgvRecords.CancelEdit();
                        dgvRecords.PopulateGrid(oCtrlPanel.Parent as SLPanel, true);
                        (oCtrlPanel.Parent as SLPanel).ClearConcurrentPanels();
                        (oCtrlPanel.Parent as SLPanel).ClearDependentPanels();
                        if ((oCtrlPanel.Parent as SLPanel).EnableInsert == false)
                        {
                            dgvRecords.AllowUserToAddRows = true;
                        }
                        dgvRecords.CurrentCell = dgvRecords[dgvRecords.GetFirstVisibleColumn(), 0];
                        dgvRecords.CurrentCell = dgvRecords[dgvRecords.GetFirstVisibleColumn(), dgvRecords.NewRowIndex];
                        currentQueryMode = Enumerations.eQueryMode.EnterQuery;
                        return;
                    }

                    switch (actionType)
                    {
                        case "Save":
                            if ((dgvRecords.Parent as SLPanel).EnableInsert || (dgvRecords.Parent as SLPanel).EnableUpdate)
                            {
                                if (dgvRecords.CurrentRow != null && !dgvRecords.CurrentRow.IsNewRow)
                                {
                                    (dgvRecords.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                                    dgvRecords.EndEdit();
                                }
                                if (dgvRecords.CurrentRow != null)
                                {
                                    dgvRecords.SLDataGridView_RowValidating(dgvRecords, new System.Windows.Forms.DataGridViewCellCancelEventArgs(0, dgvRecords.CurrentRow.Index));
                                    if (dgvRecords.CurrentRow.ErrorText.Equals(string.Empty))
                                    {
                                        dgvRecords.SaveGrid(GetCurrentPanelBlock);
                                        //dgvRecords.PopulateGrid(GetCurrentPanelBlock, true);
                                        if (this.Message.HasErrors())
                                        {
                                            operationMode = Mode.Edit;
                                        }
                                        else
                                        {
                                            operationMode = Mode.View;
                                        }
                                    }
                                    else
                                    {
                                        dgvRecords.CurrentRow.GetState(dgvRecords.CurrentRow.Index);
                                    }
                                }
                                else
                                {
                                    dgvRecords.SaveGrid(GetCurrentPanelBlock);
                                    if (this.Message.HasErrors())
                                    {
                                        operationMode = Mode.Edit;
                                    }
                                    else
                                    {
                                        operationMode = Mode.View;
                                    }
                                }
                            }
                            this.Message.ShowMessage();
                            break;
                        case "Search":
                            dgvRecords.CancelEdit();
                            dgvRecords.ClearSelection();
                            currentQueryMode = Enumerations.eQueryMode.ExecuteQuery;
                            dgvRecords.PopulateGrid(GetCurrentPanelBlock, false);
                            dgvRecords.CurrentMode = Enumerations.eGridMode.Search;
                            break;
                           
                        case "Cancel":
                            if (dgvRecords.CurrentCell !=null && dgvRecords.CurrentCell.IsInEditMode)
                            {
                                dgvRecords.CancelEdit();
                                dgvRecords.EndEdit();
                                dgvRecords.ResetError();
                            }
                            if (dgvRecords.NewRowIndex != -1)
                            {
                                dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                                dgvRecords.RejectChanges();
                                //dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                                dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                                dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            }
                            else
                            {
                                if (dgvRecords.Rows.Count > 0)
                                {
                                    dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                                    dgvRecords.RejectChanges();
                                    dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), 0];
                                }
                            }
                            operationMode = Mode.View;
                            currentQueryMode = Enumerations.eQueryMode.ExecuteQuery;
                            break;
                        case "Delete":
                            DialogResult response = MessageBox.Show("Are you sure you want to delete this record?", "Delete row?",
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question,
                                                        MessageBoxDefaultButton.Button2);
                            if (response == DialogResult.No)
                                return;
                            if ((dgvRecords.Parent as SLPanel).EnableDelete)
                            {
                                dgvRecords.DeleteSelectedGridRow();
                                operationMode = Mode.Edit;
                            }
                            this.Message.ShowMessage();
                            break;
                        case "SetGridInAddMode":
                            dgvRecords.CancelEdit();
                            dgvRecords.ClearSelection();
                            dgvRecords.PopulateGrid(GetCurrentPanelBlock, true);
                            dgvRecords.CurrentMode = Enumerations.eGridMode.Add;
                            break;
                        case "SelectCurrentGridRow":
                            dgvRecords.Rows[dgvRecords.CurrentRow.Index].Selected = true;                           
                            break;
                        case "Query":
                            if (dgvRecords.CurrentRow != null && dgvRecords.HasDataBoundItem(dgvRecords.CurrentRow.Index))
                            {
                                (dgvRecords.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                                dgvRecords.EndEdit();
                                if (!String.IsNullOrEmpty(dgvRecords.CurrentRow.ErrorText) || !String.IsNullOrEmpty(dgvRecords.CurrentCell.ErrorText))
                                    return;
                            }
                            if ((oCtrlPanel.Parent as SLPanel).EnableQuery == true)
                            {
                                GetCurrentPanelBlock.ClearBusinessEntity();
                                this.AllGridsEndEdit(this);
                                if (dgvRecords.CurrentRow != null && !dgvRecords.CurrentRow.IsNewRow)
                                {
                                    (dgvRecords.CurrentRow.DataBoundItem as DataRowView).EndEdit();
                                    dgvRecords.EndEdit();
                                }
                                if (dgvRecords.HasDataBoundItem(0))
                                {
                                    GetCurrentPanelBlock.SetEntityObject((dgvRecords.Rows[0].DataBoundItem as DataRowView).Row);
                                }
                                currentQueryMode = Enumerations.eQueryMode.ExecuteQuery;
                                dgvRecords.PopulateGrid(GetCurrentPanelBlock);
                            }
                            break;
                        case "AddNew":
                            dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                            break;
                    }
                }
                else if (oCtrlPanel is GroupBox || oCtrlPanel is SLPanelTabular )
                {
                    DoToolbarActions(oCtrlPanel.Controls, actionType);
                }
            }

            base.DoToolbarActions(ctrlsCollection, actionType);

            SetFormControls();
        }
        /// <summary>
        /// Form Keyup Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void TabularForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7 && currentQueryMode == Enumerations.eQueryMode.ExecuteQuery)
            {
                //actionType = "SetGridInAddMode";
                if (this.GetCurrentPanelBlock.EnableQuery)
                    DoToolbarActions(this.Controls, "Query");
                else
                DoToolbarActions(this.Controls, "SetGridInAddMode");
            }
            else 
                base.OnFormKeyup(sender, e);
        }

        #endregion

        #region --Method Segment--

        /// <summary>
        /// CommonOnLoadMethods
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            CreateToolbar();
            if (null != this.Controls.Owner)
            {
                this.pcbCitiGroup.Location = new Point(this.Controls.Owner.Right - 80, this.pcbCitiGroup.Top);
                this.lblFormTitle.Location = new Point(this.Controls.Owner.Width / 2 - lblFormTitle.Width / 2, this.lblFormTitle.Top);
            }

        }
        /// <summary>
        /// SetFormControls
        /// </summary>
        private void SetFormControls()
        {
            switch (operationMode)
            {
                case Mode.View:
                    if (tlbMain.Items.Count > 0)
                    {
                        tlbMain.Items["tbtSave"].Enabled = false;
                        tlbMain.Items["tbtCancel"].Enabled = false;
                    }
                    break;
                case Mode.Edit:
                    if (tlbMain.Items.Count > 0)
                    {
                        tlbMain.Items["tbtSave"].Enabled = true;
                        tlbMain.Items["tbtCancel"].Enabled = true;
                    }
                    break;
            }
        }
        /// <summary>
        /// CreateToolbar
        /// </summary>
        protected override void CreateToolbar()
        {
            // tbtSearch
            ToolStripButton tbtSearch = new ToolStripButton();
            tbtSearch.Image = iCORE.Properties.Resources.Edit_Record;
            tbtSearch.ImageScaling = ToolStripItemImageScaling.None;
            tbtSearch.ImageTransparentColor = Color.Magenta;
            tbtSearch.Name = "tbtSearch";
            tbtSearch.Size = new Size(44, 33);
            tbtSearch.Text = "&Search";
            tbtSearch.TextImageRelation = TextImageRelation.ImageAboveText;
            tlbMain.Items.Insert(3, tbtSearch);

            // tbtSearch
            tlbMain.Items.Remove(tbtAdd);
            tlbMain.Items.Remove(tbtEdit);
            tlbMain.Items.Remove(tbtList);
            SetFormControls();
            base.CreateToolbar();
        }
        
        #endregion

    }
}