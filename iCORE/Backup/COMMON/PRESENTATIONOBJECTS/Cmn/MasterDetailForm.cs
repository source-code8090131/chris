using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrplControlLibrary;
using iCORE.COMMON.SLCONTROLS;
using System.Transactions;
using System.Collections;
using System.Diagnostics;

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    /// <summary>
    /// MasterDetailForm
    /// </summary>
    public partial class MasterDetailForm : BaseForm
    {

        [Description("List of Master Panels on Form")]
        [Category("Behaviour")]
        public List<SLPanel> IndependentPanels
        {
            [DebuggerStepThrough()]
            get { return independentPanels; }
            [DebuggerStepThrough()]
            set { independentPanels = value; }
        }
        List<SLPanel> independentPanels;


        private Boolean showSearchButton;
        [Browsable(true)]
        [Description("Show/Hide Search Button.")]
        public Boolean ShowSearchButton
        {
            get { return showSearchButton; }
            set { showSearchButton = value; }
        }

        private SLPanel defaultDataBlock;
        protected SLPanel DefaultDataBlock
        {
            get {return defaultDataBlock;}
            set {defaultDataBlock = value;}
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MasterDetailForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mainmenu"></param>
        /// <param name="connbean_obj"></param>
        public MasterDetailForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            this.connbean = connbean_obj;
            this.SetConnectionBean();
            InitializeComponent();
            this.MdiParent = mainmenu;
            this.DataBlockOnFocus += new DataBlockOnFocusEventHandler(MasterDetailForm_DataBlockOnFocus);            
            independentPanels = new List<SLPanel>();
        }

        /// <summary>
        /// Set Form Layout
        /// </summary>
        /// <param name="control"></param>
        void MasterDetailForm_DataBlockOnFocus(Control control)
        {
            if (control.Parent.GetType().Equals(typeof(SLPanelSimple)))
            {
                if ((control.Parent as SLPanel).PanelBlockType == BlockType.ControlBlock)
                {
                    if (DefaultDataBlock != null)
                    {
                        this.CurrentPanelBlock = DefaultDataBlock.Name;
                        tbtDelete.Enabled = true;
                    }
                    else
                    {
                        tbtDelete.Enabled = false;
                    }
                }
                else if ((control.Parent as SLPanel).PanelBlockType == BlockType.DataBlock)
                {
                    tbtDelete.Enabled = true;
                }
                
            }
        }        

        /// <summary>
        /// Set Screen Mode For Simple panel on Master Detail Screen
        /// </summary>
        /// <param name="oPanel"></param>
        protected virtual void SetMode(SLPanel oPanel)
        {
            System.Reflection.PropertyInfo newProperty;
            newProperty = oPanel.CurrentBusinessEntity.GetType().GetProperty(PKID);
            if (newProperty != null && newProperty.CanRead)
            {
                Int32 ID;
                Int32.TryParse(newProperty.GetValue(oPanel.CurrentBusinessEntity, null).ToString(), out ID);
                if (ID > 0)
                    this.operationMode = Mode.Edit;
                else
                    this.operationMode = Mode.Add;
            }
        }

        /// <summary>
        /// To iterate Concurrent panel control
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pblnIsSetter"></param>
        /// <param name="pblnResetter"></param>
        /// <param name="pblnRegisterHandler"></param>
        public new void IterateConcurrentPanels(SLPanel oPanel, bool pblnIsSetter, bool pblnResetter, bool pblnRegisterHandler)
        {
            if (oPanel.ConcurrentPanels != null && oPanel.ConcurrentPanels.Count > 0)
            {
                foreach (SLPanel parallelPanel in oPanel.ConcurrentPanels)
                {
                    IterateFormControls(parallelPanel.Controls, pblnIsSetter, pblnResetter, pblnRegisterHandler);
                }
            }
        }

        /// <summary>
        /// Ovverriden Simple Panel Save for Master Detail Screen
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pActionType"></param>
        /// <returns></returns>
        protected override bool Save_Click(SLPanel oPanel, string pActionType)
        {
            InitializeFormObject(oPanel);
            SetMode(oPanel);
            if (this.operationMode == Mode.Add)
            {
                IterateFormControls(oPanel.Controls, false, false, false);
                IterateConcurrentPanels(oPanel, false, false, false);
                if (this.IsValidPage)
                {
                    oPanel.CurrentBusinessEntity.SoeId = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                    rslt = entityDataManager.Add(this.m_oEntityClass, oPanel.SPName, pActionType);
                    if (rslt.isSuccessful)
                    {
                        oPanel.FillEntity(this.m_oEntityClass, rslt.hstOutParams);
                        oPanel.CurrentBusinessEntity = this.m_oEntityClass;
                        if (null != oPanel.DependentPanels)
                            foreach (SLPanel childPanel in oPanel.DependentPanels)
                            {
                                childPanel.FillEntity(oPanel);
                            }
                        this.IterateFormControls(oPanel.Controls, true, false, false);
                        this.IterateConcurrentPanels(oPanel, true, false, false);
                        this.FormEditMode(oPanel.Controls);
                        this.SetMessage("Record", ApplicationMessages.ADD_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                        //this.ShowInformationMessage(oPanel.EntityName, ApplicationMessages.ADD_SUCCESS_MESSAGE);
                    }
                    else if (rslt.exp.Message.Equals(ApplicationMessages.DBMessages.DuplicateRecord.GetHashCode().ToString()) == true)
                    {
                        this.SetMessage("Record", ApplicationMessages.DUPLICATE_MESSAGE, String.Empty, MessageType.Error);
                    }
                    else if ((rslt.exp is System.Data.SqlClient.SqlException) && (rslt.exp as System.Data.SqlClient.SqlException).Number == 50000)
                        this.SetMessage("", rslt.exp.Message, String.Empty, MessageType.Error);
                    else
                    {
                        this.SetMessage("Record", ApplicationMessages.ADD_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                    }
                }
                else
                {
                    rslt.isSuccessful = false;
                }
            }
            else if (this.operationMode == Mode.Edit)
            {

                //SetCustomEntityProperties(PKID, this.m_intPKID.ToString());
                IterateFormControls(oPanel.Controls, false, false, false);
                IterateConcurrentPanels(oPanel, false, false, false);
                if (this.IsValidPage)
                {
                    oPanel.CurrentBusinessEntity.SoeId = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                    rslt = entityDataManager.Update(this.m_oEntityClass, oPanel.SPName, "Update");

                    if (!rslt.isSuccessful)
                    {
                        if (rslt.exp.Message.Equals(ApplicationMessages.DBMessages.DuplicateRecord.GetHashCode().ToString()) == true)
                            this.SetMessage("Record", ApplicationMessages.DUPLICATE_MESSAGE, String.Empty, MessageType.Error);
                        else if ((rslt.exp is System.Data.SqlClient.SqlException) && (rslt.exp as System.Data.SqlClient.SqlException).Number == 50000)
                            this.SetMessage("", rslt.exp.Message, String.Empty, MessageType.Error);
                        else
                            this.SetMessage("Record", ApplicationMessages.UPDATE_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                    }
                    else
                    {
                        oPanel.FillEntity(this.m_oEntityClass, rslt.hstOutParams);
                        oPanel.CurrentBusinessEntity = this.m_oEntityClass;
                        if (null != oPanel.DependentPanels)
                            foreach (SLPanel childPanel in oPanel.DependentPanels)
                            {
                                childPanel.FillEntity(oPanel);
                            }
                        this.IterateFormControls(oPanel.Controls, true, false, false);
                        this.IterateConcurrentPanels(oPanel, true, false, false);
                        this.SetMessage("Record", ApplicationMessages.UPDATE_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                    }
                }
                else
                {
                    rslt.isSuccessful = false;
                }
            }           
            return rslt.isSuccessful;
        }

        #region --Method Segment--

        /// <summary>
        /// Recursive method to call performAction on all dependent panels
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="actionType"></param>
        private void PerformActionOnAllDependentPanels(SLPanel panel, string actionType)
        {
            if (!(this.Message.HasErrors()))
            {
                PerformAction(panel, actionType);
                if (panel.DependentPanels != null && panel.DependentPanels.Count > 0)
                    foreach (SLPanel oPanel in panel.DependentPanels)
                    {
                        PerformActionOnAllDependentPanels(oPanel, actionType);
                    }
                else
                    return;
            }
        }


        //protected internal override void tlbMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        //{
        //    base.tlbMain_ItemClicked(sender, e);
        //    if(GetActionType(e.ClickedItem.Text) == "Delete")
        //    {
        //        DoToolbarActions(this.Controls, "Cancel");
        //    }
        //}
        /// <summary>
        /// Toolbar Action Caller
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            try
            {
                //using (TransactionScope scope  = new TransactionScope())
                //{                
                if (actionType == "Save")
                {
                    base.AllGridsEndEdit(this);
                    if (!this.FindForm().Validate())
                        return;
                }
                else if (actionType == "Delete")
                {
                    if (operationMode != Mode.Add)
                    {
                        DialogResult response = MessageBox.Show("Are you sure you want to delete this record?", "Delete row?",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question,
                                                            MessageBoxDefaultButton.Button2);
                        if (response == DialogResult.No)
                            return;
                    }
                    else
                        return;
                }
                else if (actionType == "Cancel" || actionType=="ClearScreen")
                {
                    foreach (SLPanel oCtrlPanel in IndependentPanels)
                    {
                        if (oCtrlPanel is SLPanelTabular)
                        {
                            foreach (Control control in oCtrlPanel.Controls)
                            {
                                if (control is SLDataGridView)
                                {
                                    (control as SLDataGridView).CancelEdit();
                                    (control as SLDataGridView).EndEdit();
                                    (control as SLDataGridView).ResetError();
                                }
                            }
                        }
                        foreach (SLPanel oPanel in oCtrlPanel.DependentPanels)
                        {
                            if (oPanel is SLPanelTabular)
                            {
                                foreach (Control control in oPanel.Controls)
                                {
                                    if (control is SLDataGridView)
                                    {
                                        (control as SLDataGridView).CancelEdit();
                                        (control as SLDataGridView).EndEdit();
                                        (control as SLDataGridView).ResetError();
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (SLPanel oCtrlPanel in IndependentPanels)
                {
                    switch (actionType)
                    {
                        case "Save":
                            base.SkipLovValidationOnSave = true;
                            if (!(this.Message.HasErrors()))
                            {
                                PerformAction(oCtrlPanel, actionType);
                                if (!(this.Message.HasErrors()))
                                {
                                    foreach (SLPanel oPanel in oCtrlPanel.DependentPanels)
                                    {
                                        if (!(this.Message.HasErrors()))
                                        {
                                            PerformActionOnAllDependentPanels(oPanel, actionType);
                                        }
                                    }
                                }
                                if (!(this.Message.HasErrors()) && !(this.Message.HasWarning()))
                                    this.SetMessage("Record ", "Record saved successfully", String.Empty, MessageType.Information);
                                this.Message.ShowMessage();
                            }
                            base.SkipLovValidationOnSave = false;
                            break;
                        case "Add":
                            PerformAction(oCtrlPanel, actionType);
                            break;
                        case "Edit":
                            PerformAction(oCtrlPanel, actionType);
                            break;
                        case "List":
                            PerformAction(oCtrlPanel, actionType);
                            operationMode = Mode.View;
                            break;
                        case "Search":
                            PerformAction(oCtrlPanel, actionType);
                            operationMode = Mode.Edit;
                            break;
                        case "Cancel":
                            PerformAction(oCtrlPanel, actionType);
                            foreach (SLPanel oPanel in oCtrlPanel.DependentPanels)
                            {
                                PerformAction(oPanel, actionType);
                            }
                            break;
                        case "ClearScreen":
                            PerformAction(oCtrlPanel, actionType);
                            foreach (SLPanel oPanel in oCtrlPanel.DependentPanels)
                            {
                                PerformAction(oPanel, actionType);
                            }
                            break;
                        case "Delete":
                            if (!(this.GetCurrentPanelBlock as SLPanel).EnableDelete)
                            {
                                this.SetMessage("", "Delete is not allowed.", String.Empty, MessageType.Error);
                                this.Message.ShowMessage();
                                break;
                            }
                            PerformAction(this.GetCurrentPanelBlock, actionType);
                            if (!(this.Message.HasErrors()))
                            {
                                this.SetMessage("Record ", "Record deleted successfully", String.Empty, MessageType.Information);
                                if (this.GetCurrentPanelBlock is SLPanelSimple)
                                {
                                    this.ClearForm(this.GetCurrentPanelBlock.Controls);
                                    this.GetCurrentPanelBlock.CurrentBusinessEntity = RuntimeClassLoader.GetBusinessEntity(this.GetCurrentPanelBlock.EntityName);
                                    this.GetCurrentPanelBlock.LoadDependentPanels();
                                }
                            }
                            this.Message.ShowMessage();
                            break;
                        case "SetGridInAddMode":
                            if (this.GetCurrentPanelBlock is SLPanelTabular)
                            {
                                foreach (Control ctrl in this.GetCurrentPanelBlock.Controls)
                                {
                                    if (ctrl is SLDataGridView)
                                    {
                                        (ctrl  as SLDataGridView).CancelEdit();
                                        (ctrl as SLDataGridView).ClearSelection();
                                        (ctrl as SLDataGridView).PopulateGrid(GetCurrentPanelBlock, true);
                                        (ctrl as SLDataGridView).CurrentMode = Enumerations.eGridMode.Add;
                                    }
                                }
                            }
                            break;
                        case "SelectCurrentGridRow":
                            if (this.GetCurrentPanelBlock is SLPanelTabular)
                            {
                                foreach (Control ctrl in this.GetCurrentPanelBlock.Controls)
                                {
                                    if (ctrl is SLDataGridView)
                                    {
                                        (ctrl as SLDataGridView).Rows[(ctrl as SLDataGridView).CurrentRow.Index].Selected = true;                                        
                                    }
                                }
                            }
                            break;
                        case "AddNew":
                            PerformAction(this.GetCurrentPanelBlock, actionType);
                            break;
                    }
                }
                base.DoToolbarActions(ctrlsCollection, actionType);                
                //scope.Complete();
                //} 
            }
            catch (Exception ex)
            {
                this.ShowErrorMessage("", ex.Message);
            }
        }

        /// <summary>
        /// Peform Panel Operations
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        private void PerformAction(Control oCtrlPanel, string actionType)
        {
            bool bRowsFound = false;
            if (oCtrlPanel is SLPanelSimple)
            {
                //(oCtrlPanel as SLPanel).CurrentBusinessEntity.SoeId = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                switch (actionType)
                {
                    case "Save":
                        if ((oCtrlPanel as SLPanel).PanelBlockType == BlockType.DataBlock
                            && ((oCtrlPanel as SLPanel).EnableInsert || (oCtrlPanel as SLPanel).EnableUpdate))
                        {
                            if (Save_Click((SLPanel)oCtrlPanel, actionType))
                            {
                                operationMode = Mode.Edit;
                            }
                            else
                            {
                                if (!(this.FindForm() as BaseForm).HasMessage())
                                    this.SetMessage("", "Required Field Validation Failed", String.Empty, MessageType.Error);
                            }
                        }
                        break;
                    case "Add":
                        base.FormEditMode(oCtrlPanel.Controls);
                        break;
                    case "Edit":
                        base.FormEditMode(oCtrlPanel.Controls);
                        break;
                    case "Delete":
                        if ((oCtrlPanel as SLPanel).PanelBlockType == BlockType.DataBlock
                            && (oCtrlPanel as SLPanel).EnableDelete)
                        {
                            //if (this.GetCurrentPanelBlock == (SLPanel)oCtrlPanel)
                            if ((oCtrlPanel as SLPanel).DeleteRecordBehavior == DeleteRecordBehavior.NonIsolated)
                            {
                                if (null != (oCtrlPanel as SLPanel).DependentPanels)
                                    foreach (SLPanel dependent in (oCtrlPanel as SLPanel).DependentPanels)
                                    {
                                        foreach (Control dependentControl in dependent.Controls)
                                        {
                                            if ((dependentControl is SLDataGridView)
                                                && (dependentControl as SLDataGridView).RowCount > 1)
                                            {
                                                bRowsFound = true;
                                                break;
                                            }
                                        }
                                        if (bRowsFound)
                                            break;
                                    }
                            }
                            if (bRowsFound)
                                this.SetMessage("", ApplicationMessages.DELETE_MASTER_RECORD, String.Empty, MessageType.Error);
                                //ShowInformationMessage("", ApplicationMessages.DELETE_MASTER_RECORD);
                            else
                            {
                                base.Delete_Click((SLPanel)oCtrlPanel, actionType);
                                operationMode = Mode.Add;                               
                            }
                        }
                        break;
                    case "List":
                        base.ShowList((SLPanel)oCtrlPanel, actionType);
                        base.FormEditMode(oCtrlPanel.Controls);
                        //tbtDelete.Enabled = true;
                        break;
                    case "Search":
                        if ((oCtrlPanel as SLPanel).PanelBlockType == BlockType.ControlBlock)
                        {
                            InitializeFormObject(oCtrlPanel as SLPanel);
                            base.IterateFormControls(oCtrlPanel.Controls, false, false, false);
                            (oCtrlPanel as SLPanel).CurrentBusinessEntity = m_oEntityClass;
                            (oCtrlPanel as SLPanel).LoadDependentPanels();
                        }
                        else
                        {
                            base.ShowList((SLPanel)oCtrlPanel, "List");
                            base.FormEditMode(oCtrlPanel.Controls);
                        }
                        break;
                    case "Cancel":
                        base.FormCancelMode(this.Controls);
                        base.FormEditMode(this.Controls);
                        (oCtrlPanel as SLPanel).ClearBusinessEntity();
                        (oCtrlPanel as SLPanel).LoadDependentPanels();
                        this.m_intPKID = 0;
                        break;
                    case "ClearScreen":
                        base.FormCancelMode(this.Controls);
                        base.FormEditMode(this.Controls);
                        (oCtrlPanel as SLPanel).ClearBusinessEntity();
                        this.m_intPKID = 0;
                        (oCtrlPanel as SLPanel).LoadDependentPanels();
                        break;
                    case "AddNew":
                        base.FormCancelMode(this.Controls);
                        base.FormEditMode(this.Controls);
                        (oCtrlPanel as SLPanel).ClearBusinessEntity();
                        (oCtrlPanel as SLPanel).LoadDependentPanels();
                        break;
                }
            }
            else if (oCtrlPanel is SLPanelSimpleList)
            {
                switch (actionType)
                {
                    case "Search":
                        (oCtrlPanel as SLPanelSimpleList).GetList(oCtrlPanel as SLPanel, false);
                        base.FormEditMode(oCtrlPanel.Controls);
                        operationMode = Mode.View;
                        //tbtDelete.Enabled = true;
                        break;
                    case "List":
                        (oCtrlPanel as SLPanelSimpleList).GetList(oCtrlPanel as SLPanel, false);
                        base.FormEditMode(oCtrlPanel.Controls);
                        operationMode = Mode.View;
                        //tbtDelete.Enabled = true;
                        break;
                    case "Save":
                        if ((oCtrlPanel as SLPanel).PanelBlockType == BlockType.DataBlock
                           && ((oCtrlPanel as SLPanel).EnableInsert || (oCtrlPanel as SLPanel).EnableUpdate))
                        {
                            if ((oCtrlPanel as SLPanelSimpleList).SaveRecords(oCtrlPanel as SLPanel))
                            {
                                operationMode = Mode.Edit;
                            }
                            else
                            {
                                if (!(this.FindForm() as BaseForm).HasMessage())
                                    this.SetMessage("", "Required Field Validation Failed", String.Empty, MessageType.Error);
                            }
                        }

                        break;
                    case "Delete":
                        if ((oCtrlPanel as SLPanel).PanelBlockType == BlockType.DataBlock
                             && (oCtrlPanel as SLPanel).EnableDelete)
                        {
                            (oCtrlPanel as SLPanelSimpleList).DeleteRecords(oCtrlPanel as SLPanel);
                            this.ClearForm(this.GetCurrentPanelBlock.Controls);
                            this.GetCurrentPanelBlock.CurrentBusinessEntity = RuntimeClassLoader.GetBusinessEntity(this.GetCurrentPanelBlock.EntityName);
                            this.GetCurrentPanelBlock.LoadDependentPanels();
                        }
                        break;
                    case "Cancel":
                        base.FormCancelMode(this.Controls);
                        base.FormEditMode(this.Controls);
                        (oCtrlPanel as SLPanel).ClearBusinessEntity();
                        (oCtrlPanel as SLPanel).LoadDependentPanels();
                        this.m_intPKID = 0;
                        //currentQueryMode = Enumerations.eQueryMode.EnterQuery;
                        break;
                    //case "ClearScreen":
                    //    // base.FormCancelMode(this.Controls);
                    //    base.FormEditMode(this.Controls);
                    //    (oCtrlPanel as SLPanel).ClearBusinessEntity();
                    //    (oCtrlPanel as SLPanel).ClearPanelControls(oCtrlPanel.Controls);
                    //    this.m_intPKID = 0;
                    //    (oCtrlPanel as SLPanel).LoadDependentPanels();
                    //    // currentQueryMode = Enumerations.eQueryMode.EnterQuery;
                    //    break;
               
                }
            }
            else if (oCtrlPanel is SLDataGridView)
            {
                SLDataGridView dgvRecords = (SLDataGridView)oCtrlPanel;

                switch (actionType)
                {
                    case "Save":
                        (oCtrlPanel.Parent as SLPanel).DisableDependentLoad = true;
                        dgvRecords.SaveGrid((oCtrlPanel.Parent as SLPanel));
                        (oCtrlPanel.Parent as SLPanel).DisableDependentLoad = false;                        
                        operationMode = Mode.View;
                        break;
                    case "Search":
                        dgvRecords.PopulateGrid((oCtrlPanel.Parent as SLPanel), false);
                        break;
                    case "Cancel":
                        dgvRecords.RejectChanges();
                        //((SLPanelTabular)dgvRecords.Parent).ClearBusinessEntity();
                        //if (Utilities.FindMasterPanel(this,((SLPanelTabular)dgvRecords.Parent).Name) == null)
                        //    dgvRecords.PopulateGrid((oCtrlPanel.Parent as SLPanel), false);
                        operationMode = Mode.View;
                        break;
                    case "ClearScreen":
                        dgvRecords.RejectChanges();
                        ((SLPanelTabular)dgvRecords.Parent).ClearBusinessEntity();
                        //if (Utilities.FindMasterPanel(this, ((SLPanelTabular)dgvRecords.Parent).Name) == null)
                        dgvRecords.PopulateGrid((oCtrlPanel.Parent as SLPanel), true);
                        operationMode = Mode.View;
                        break;
                    case "AddNew":
                        dgvRecords.CurrentCell = dgvRecords[dgvRecords.FindFirstVisibleColumn(), dgvRecords.NewRowIndex];
                        break;
                    case "Delete":
                        //if (this.GetCurrentPanelBlock == (SLPanel)dgvRecords.Parent)
                        if ((dgvRecords.Parent as SLPanel).DeleteRecordBehavior == DeleteRecordBehavior.NonIsolated)
                        {
                            if (null != (dgvRecords.Parent as SLPanel).DependentPanels)
                                foreach (SLPanel dependent in (dgvRecords.Parent as SLPanel).DependentPanels)
                                {
                                    foreach (Control dependentControl in dependent.Controls)
                                    {
                                        if ((dependentControl is SLDataGridView)
                                            && (dependentControl as SLDataGridView).RowCount > 1)
                                        {
                                            bRowsFound = true;
                                            break;
                                        }
                                    }
                                    if (bRowsFound)
                                        break;
                                }
                        }
                        if (bRowsFound)
                            this.SetMessage("", ApplicationMessages.DELETE_MASTER_RECORD, String.Empty, MessageType.Error);
                        //ShowInformationMessage("", ApplicationMessages.DELETE_MASTER_RECORD);
                        else
                        {
                            dgvRecords.DeleteSelectedGridRow();
                            operationMode = Mode.Edit;
                            (dgvRecords.Parent as SLPanel).ClearDependentPanels();
                        }
                        break;
                }
            }
            else if (oCtrlPanel is GroupBox || oCtrlPanel is SLPanelTabular)
            {
                foreach (Control control in oCtrlPanel.Controls)
                {
                    PerformAction(control, actionType);
                }
            }
        }

        /// <summary>
        /// Onload Method
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            CreateToolbar();
            if (null != this.Controls.Owner)
            {
                this.pcbCitiGroup.Location = new Point(this.Controls.Owner.Right - 80, this.pcbCitiGroup.Top);
                this.pcbCitiGroup.BringToFront();
                //this.lblFormTitle.Location = new Point(this.Controls.Owner.Width / 2 - lblFormTitle.Width / 2, this.lblFormTitle.Top);
            }

        }


        /// <summary>
        /// Overridden method for Master Detail Form
        /// </summary>
        protected override void CreateToolbar()
        {
            if (ShowSearchButton)
            {
                // tbtSearch
                ToolStripButton tbtSearch = new ToolStripButton();
                tbtSearch.Image = iCORE.Properties.Resources.Edit_Record;
                tbtSearch.ImageScaling = ToolStripItemImageScaling.None;
                tbtSearch.ImageTransparentColor = Color.Magenta;
                tbtSearch.Name = "tbtSearch";
                tbtSearch.Size = new Size(44, 33);
                tbtSearch.Text = "&Search";
                tbtSearch.TextImageRelation = TextImageRelation.ImageAboveText;
                tlbMain.Items.Insert(3, tbtSearch);
                tbtList.Visible = false;
            }
            tlbMain.Items.Remove(tbtAdd);
            tlbMain.Items.Remove(tbtEdit);
        }

        #endregion

        /// <summary>
        /// Key Press Event HAndler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void MasterDetailForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
                DoToolbarActions(this.Controls, "ClearScreen");
            else
                base.OnFormKeyup(sender, e);
        }



    }
}