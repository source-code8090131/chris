namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    partial class TabularForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbtSave = new System.Windows.Forms.ToolStripButton();
            this.tbtDelete = new System.Windows.Forms.ToolStripButton();
            this.tbtCancel = new System.Windows.Forms.ToolStripButton();
            this.tbtClose = new System.Windows.Forms.ToolStripButton();
            this.lblFormTitle = new System.Windows.Forms.Label();
            this.pcbCitiGroup = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // tbtSave
            // 
            this.tbtSave.Image = global::iCORE.Properties.Resources.Save_Record;
            this.tbtSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtSave.Name = "tbtSave";
            this.tbtSave.Size = new System.Drawing.Size(35, 33);
            this.tbtSave.Text = "&Save";
            this.tbtSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtDelete
            // 
            this.tbtDelete.Enabled = false;
            this.tbtDelete.Image = global::iCORE.Properties.Resources.Delete;
            this.tbtDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtDelete.Name = "tbtDelete";
            this.tbtDelete.Size = new System.Drawing.Size(42, 33);
            this.tbtDelete.Text = "&Delete";
            this.tbtDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtCancel
            // 
            this.tbtCancel.Enabled = false;
            this.tbtCancel.Image = global::iCORE.Properties.Resources.Cancel_Record;
            this.tbtCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtCancel.Name = "tbtCancel";
            this.tbtCancel.Size = new System.Drawing.Size(43, 33);
            this.tbtCancel.Text = "Ca&ncel";
            this.tbtCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tbtClose
            // 
            this.tbtClose.Image = global::iCORE.Properties.Resources.Close;
            this.tbtClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtClose.Name = "tbtClose";
            this.tbtClose.Size = new System.Drawing.Size(37, 33);
            this.tbtClose.Text = "&Close";
            this.tbtClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // lblFormTitle
            // 
            this.lblFormTitle.AutoSize = true;
            this.lblFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormTitle.ForeColor = System.Drawing.Color.Red;
            this.lblFormTitle.Location = new System.Drawing.Point(175, 54);
            this.lblFormTitle.Name = "lblFormTitle";
            this.lblFormTitle.Size = new System.Drawing.Size(89, 20);
            this.lblFormTitle.TabIndex = 6;
            this.lblFormTitle.Text = "Form Title";
            // 
            // pcbCitiGroup
            // 
            this.pcbCitiGroup.Image = global::iCORE.Properties.Resources.Citi;
            this.pcbCitiGroup.Location = new System.Drawing.Point(417, 12);
            this.pcbCitiGroup.Name = "pcbCitiGroup";
            this.pcbCitiGroup.Size = new System.Drawing.Size(64, 64);
            this.pcbCitiGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pcbCitiGroup.TabIndex = 7;
            this.pcbCitiGroup.TabStop = false;
            // 
            // TabularForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 438);
            this.Controls.Add(this.pcbCitiGroup);
            this.Controls.Add(this.lblFormTitle);
            this.Name = "TabularForm";
            this.Text = "TabularForm";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TabularForm_KeyUp);
            this.Controls.SetChildIndex(this.lblFormTitle, 0);
            this.Controls.SetChildIndex(this.pcbCitiGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFormTitle;
        private System.Windows.Forms.PictureBox pcbCitiGroup;
        protected internal System.Windows.Forms.ToolStripButton tbtSave;
        protected internal System.Windows.Forms.ToolStripButton tbtDelete;
        protected internal System.Windows.Forms.ToolStripButton tbtCancel;
        protected internal System.Windows.Forms.ToolStripButton tbtClose;
    }
}