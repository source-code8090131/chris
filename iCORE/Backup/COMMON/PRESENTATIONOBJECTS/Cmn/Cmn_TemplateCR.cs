using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    public partial class Cmn_TemplateCR : Form
    {
        protected XMS.BUSINESSOBJECTS.ScreensCollection screencoll = null;
        protected string screenid = null;
        protected XMS.DATAOBJECTS.ConnectionBean connbean = null;
        protected string userID;
        protected ListType listOfRecordsType;

        public Mode operationMode = Mode.View;

        public string applicationTitle = "iCORE ";


        public Cmn_TemplateCR()
        {
            InitializeComponent();

            this.FormClosed += new FormClosedEventHandler(CRS_Cmn_TemplateCR_FormClosed);
        }

        /// <summary>
        ///< Method to set the connection to the database.>       
        /// </summary>
        /// <Param></Param>
        /// <returns> None </returns>

        protected void SetConnectionBean()
        {
            SQLManager.SetConnectionBean(connbean);
        }

        /// <summary>
        ///< Method to set the screen collections.>       
        /// </summary>
        /// <Param spName= Name of SP ></Param>
        /// <returns> None </returns>

        public void setScreenCollection(XMS.BUSINESSOBJECTS.ScreensCollection screencoll_obj, string screenid_obj)
        {

            this.screencoll = screencoll_obj;
            this.screenid = screenid_obj;
        }

        /// <summary>
        ///< Form close event.>       
        /// </summary>
        /// <Param sender = sender object,e = Event object></Param>
        /// <returns> None </returns>

        private void CRS_Cmn_TemplateCR_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.screencoll != null && this.screenid != null)
                this.screencoll.removefromRunningFormsCollection(this.screenid);
        }

        /// <summary>
        ///< Form load event.>       
        /// </summary>
        /// <Param sender = sender object,e = Event object></Param>
        /// <returns> None </returns>

        public void CRS_Cmn_TemplateCR_Load(object sender, EventArgs e)
        {
            XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mdiForm =
                this.MdiParent as XMS.PRESENTATIONOBJECTS.FORMS.MainMenu;

            if (mdiForm != null)
            {
                userID = mdiForm.getXmsUser().getSoeId();

                this.Top = 0;
                this.Left = 0;

                //if (userID == "ss09389")
                //{
                //    this.tbtAuthorize.Visible = true;
                //    this.listOfRecordsType = ListType.AuthUnAuthList;
                //}
                //else
                //{
                //    this.tbtAuthorize.Visible = false;
                //    this.listOfRecordsType = ListType.SetupList;
                //}
            }
            else
                this.CenterToScreen();
            //            this.tlbMain_ItemClicked(tlbMain, new ToolStripItemClickedEventArgs(tbtAdd));
            this.StartPosition = FormStartPosition.CenterParent;
            SQLManager.SetFormInfo(userID);
            this.FormAddMode(this.Controls);
            this.Activate();
            this.Focus();
            this.RegisterKeyPress(this.Controls);
        }

        /// <summary>
        /// RegisterKeyPress
        /// </summary>
        /// <param name="ctrls"></param>
        private void RegisterKeyPress(Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl.Controls.Count > 0)
                    this.RegisterKeyPress(ctrl.Controls);
                else
                {
                    if (!(ctrl is CrplControlLibrary.CrplTextBox))
                    {
                        ctrl.KeyPress += new KeyPressEventHandler(ctrl_KeyPress);
                    }
                }
            }
        }

        /// <summary>
        /// ctrl_KeyPress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctrl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                //SendKeys.Send("{TAB}");
                e.Handled = true;
            }
        }

        /// <summary>
        ///< Method to enable unauthorize toolbar button.>       
        /// </summary>
        /// <Param></Param>
        /// <returns> None </returns>

        protected internal void EnableUnAuthorizeButton()
        {
            //   this.tbtAuthorize.Text = "&UnAuthorize";
        }

        /// <summary>
        ///< Method to enable authorize toolbar button.>       
        /// </summary>
        /// <Param></Param>
        /// <returns> None </returns>

        protected internal void EnableAuthorizeButton()
        {
            //   this.tbtAuthorize.Text = "Au&thorize";
        }

        /// <summary>
        ///<  Event that is called when any toolbar button is clicked.>       
        /// </summary>
        /// <Param sender = sender object,e = ToolStripItemClickedEvent object></Param>
        /// <returns> None</returns>

        protected internal virtual void tlbMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Text)
            {
                case "&Close":
                    this.Close();
                    break;
                case "&Save":
                    this.errorProvider1.Clear();
                    break;

            }
        }

        /// <summary>
        /// LogException
        /// </summary>
        /// <param name="className"></param>
        /// <param name="functionName"></param>
        /// <param name="exp"></param>
        protected void LogException(string className, string functionName, Exception exp)
        {
            this.ShowErrorMessage("", ApplicationMessages.EXCEPTION_MESSAGE);
            this.LogError(className, functionName, exp);

        }

        /// <summary>
        /// LogError
        /// </summary>
        /// <param name="className"></param>
        /// <param name="functionName"></param>
        /// <param name="exp"></param>
        protected void LogError(string className, string functionName, Exception exp)
        {
            ExceptionLogCommand exceptionLogEntity =
                           RuntimeClassLoader.GetEntity("iCORE.Common.ExceptionLog") as ExceptionLogCommand;

            exceptionLogEntity.ClassName = className;
            exceptionLogEntity.FunctionName = functionName;
            exceptionLogEntity.Exception = exp;

            DataManager.LogException(exceptionLogEntity);
        }
        /// <summary>
        /// <Method to authorize record.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,
        /// object Name = string of object Name,id = id of selected record></Param>
        /// <returns> true if succesfully record authorized</returns>

        protected bool Authorize(string dataManagerName, string objectName, int id)
        {
            Result rslt = new Result();

            try
            {
                IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

                //if (this.roleID != "Authorizer")
                //{
                //    this.ShowErrorMessage(objectName, ApplicationMessages.AUTHORIZE_FAILURE_MESSAGE);
                //    return false;
                //}
                //else
                //{
                rslt = dataManager.Authorize(id, this.userID, DateTime.Now);

                if (rslt.isSuccessful)
                {
                    if (Convert.ToBoolean(rslt.objResult))
                    {
                        this.ShowErrorMessage(objectName, ApplicationMessages.AUTHORIZE_FAILURE_MESSAGE);
                    }
                    else
                    {
                        ShowInformationMessage(objectName, ApplicationMessages.AUTHORIZE_SUCCESSFUL_MESSAGE);
                        //this.tbtAuthorize.Enabled = false;
                        //this.EnableUnAuthorizeButton();
                    }
                }
                else
                {
                    this.ShowErrorMessage("", ApplicationMessages.EXCEPTION_MESSAGE);
                }
                //}

            }
            catch (Exception ex)
            {
                this.LogException("SL_F_" + objectName, "Authorize", ex);
            }
            return Convert.ToBoolean(rslt.objResult);
        }

        /// <summary>
        /// <Method to unauthorize record.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,
        /// object Name = string of object Name,id = id of selected record></Param>
        /// <returns> true if succesfully record authorized</returns>

        protected bool UnAuthorize(string dataManagerName, string objectName, int id)
        {
            Result rslt = new Result();

            try
            {
                IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

                //if (this.roleID != "Authorizer")
                //{
                //    this.ShowErrorMessage(objectName, ApplicationMessages.UNAUTHORIZE_FAILURE_MESSAGE);
                //    return false;
                //}
                //else
                //{
                rslt = dataManager.UnAuthorize(id);

                if (rslt.isSuccessful)
                {
                    ShowInformationMessage(objectName, ApplicationMessages.UNAUTHORIZE_SUCCESSFUL_MESSAGE);
                    this.EnableAuthorizeButton();
                }
                else
                {
                    ShowInformationMessage(objectName, ApplicationMessages.UNAUTHORIZE_DEPENDENCIES_MESSAGE);
                }
                //}

            }
            catch (Exception ex)
            {
                this.LogException("SL_F_" + objectName, "Authorize", ex);
            }
            return Convert.ToBoolean(rslt.objResult);

        }

        /// <summary>
        /// <Method to check duplicate id.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record></Param>
        /// <returns> true if duplicate exist</returns>

        protected bool IsDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsDuplicate(id, ctrl.Text);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }

        /// <summary>
        /// <Overloaded Method to check duplicate id.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record
        /// masterId = master ID of selected record></Param>
        /// <returns> true if duplicate exist</returns>

        protected bool IsDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id, int masterId)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsDuplicate(masterId, id, ctrl.Text);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }

        /// <summary>
        /// <Method to check duplicate names.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record></Param>
        /// <returns> true if duplicate exist</returns>
        protected bool IsNameDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsNameDuplicate(id, ctrl.Text);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }

        /// <summary>
        /// <Method to check duplicate names.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record></Param>
        /// <returns> true if duplicate exist</returns>
        protected bool IsNameDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id, string name)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsNameDuplicate(id, name);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }

        /// <summary>
        /// <Overloaded Method to check duplicate name.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record
        /// masterId = master ID of selected record></Param>
        /// <returns> true if duplicate exist</returns>

        protected bool IsNameDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id, int masterId)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsNameDuplicate(masterId, id, ctrl.Text);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }

        /// <summary>
        /// <Overloaded Method to check duplicate abbreviation.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record
        /// masterId = master ID of selected record></Param>
        /// <returns> true if duplicate exist</returns>

        protected bool IsAbbreviationDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsAbbreviationDuplicate(id, ctrl.Text);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }
        /// <summary>
        /// <Overloaded Method to check duplicate abbreviation.>       
        /// </summary>
        /// <Param dataMangerName = string of data manager name ,controlCaption = caption of control,
        /// ctrl = control object,erp= errorprovider object,id = id of selected record
        /// masterId = master ID of selected record></Param>
        /// <returns> true if duplicate exist</returns>

        protected bool IsAbbreviationDuplicate(string dataManagerName, string controlCaption, Control ctrl,
            ErrorProvider erp, int id, int masterId)
        {
            string message = ApplicationMessages.DUPLICATE_MESSAGE.Replace(ApplicationMessages.OBJECTIDENTIFIER, controlCaption);
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(dataManagerName);

            Result rslt = dataManager.IsAbbreviationDuplicate(masterId, id, ctrl.Text);

            if (rslt.isSuccessful)
            {
                if (Convert.ToBoolean(rslt.objResult))
                {
                    erp.SetError(ctrl, message);
                }
            }

            return Convert.ToBoolean(rslt.objResult);
        }


        /// <summary>
        /// <Method to show error message.>       
        /// </summary>
        /// <Param object name = string of object name , message = message string></Param>
        /// <returns> none </returns>

        protected void ShowErrorMessage(string objectName, string message)
        {
            if (message.Equals(ApplicationMessages.DELETE_FAILURE_MESSAGE))
            {
                this.ShowInformationMessage(objectName, message);
            }
            else
            {
                message = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);

                MessageBox.Show(message, applicationTitle + " - Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// <Method to show warning message.>       
        /// </summary>
        /// <Param object name = string of object name , message = message string></Param>
        /// <returns> none </returns>

        protected void ShowWarningMessage(string objectName, string message)
        {
            message = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);

            MessageBox.Show(message, applicationTitle + " - Warning",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// <Method to show confirmation message.>       
        /// </summary>
        /// <Param object name = string of object name , message = message string></Param>
        /// <returns> none </returns>

        protected bool ShowConfirmationMessage(string objectName, string message)
        {
            message = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);

            if (DialogResult.Yes == MessageBox.Show(message, applicationTitle + " - Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return true;
            else
                return false;

        }

        /// <summary>
        /// <Method to show information message.>       
        /// </summary>
        /// <Param object name = string of object name , message = message string></Param>
        /// <returns> none </returns>

        public void ShowInformationMessage(string objectName, string message)
        {
            message = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);

            MessageBox.Show(message, applicationTitle + " - Information",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// <Method to show question message.>       
        /// </summary>
        /// <Param object name = string of object name , message = message string></Param>
        /// <returns> none </returns>

        protected DialogResult ShowQuestionMessage(string objectName, string message)
        {
            message = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);

            return MessageBox.Show(message, applicationTitle + " - Question",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// <Method to set form controls in add mode>       
        /// </summary>
        /// <Param ctrlList = All controls list></Param>
        /// <returns> none </returns>

        protected void FormAddMode(Control.ControlCollection ctrlList)
        {
            //For Enabling All Controls in Form
            this.FormEnableDisableMode(ctrlList, true);
            //For Clearing All Controls in Form
            this.ClearForm(ctrlList);
            //Toolbar buttons
            this.tlbMain.Enabled =
            this.tbtSave.Enabled = this.tbtList.Enabled =
            this.tbtCancel.Enabled = this.tbtClose.Enabled = true;

            this.tbtAdd.Enabled = this.tbtEdit.Enabled =
                this.tbtDelete.Enabled = false;
            //this.tbtDelete.Enabled = this.tbtAuthorize.Enabled = false;
            this.operationMode = Mode.Add;
        }

        /// <summary>
        /// <Method to set form controls in edit mode>       
        /// </summary>
        /// <Param ctrlList = All controls list></Param>
        /// <returns> none </returns>

        protected void FormEditMode(Control.ControlCollection ctrlList)
        {
            //For Enabling All Controls in Form
            this.FormEnableDisableMode(ctrlList, true);
            //Toolbar buttons
            this.tlbMain.Enabled =
            this.tbtSave.Enabled = this.tbtList.Enabled =
            this.tbtCancel.Enabled = this.tbtClose.Enabled = true;
            this.operationMode = Mode.Edit;
        }

        /// <summary>
        /// <Method to set form controls in delete mode>       
        /// </summary>
        /// <Param ctrlList = All controls list></Param>
        /// <returns> none </returns>

        protected void FormDeleteMode(Control.ControlCollection ctrlList)
        {
            this.FormCancelMode(ctrlList);
            this.operationMode = Mode.View;
        }

        /// <summary>
        /// <Method to set form controls in list mode>       
        /// </summary>
        /// <Param ctrlList = All controls list></Param>
        /// <returns> none </returns>

        protected void FormListMode(Control.ControlCollection ctrlList)
        {
            //For Enabling All Controls in Form
            this.FormEnableDisableMode(ctrlList, false);
            //Toolbar buttons
            this.tlbMain.Enabled =
            this.tbtAdd.Enabled = this.tbtEdit.Enabled =
                //this.tbtDelete.Enabled = this.tbtAuthorize.Enabled =
            this.tbtDelete.Enabled =
            this.tbtList.Enabled = this.tbtCancel.Enabled =
            this.tbtClose.Enabled = true;
            this.tbtSave.Enabled = false;
            this.operationMode = Mode.View;
        }

        /// <summary>
        /// <Method to set form controls in cancel mode>       
        /// </summary>
        /// <Param ctrlList = All controls list></Param>
        /// <returns> none </returns>

        protected void FormCancelMode(Control.ControlCollection ctrlList)
        {
            //For Enabling All Controls in Form
            this.FormEnableDisableMode(ctrlList, false);
            //For Clearing All Controls in Form
            this.ClearForm(ctrlList);
            //Toolbar buttons
            this.tlbMain.Enabled =
            this.tbtAdd.Enabled = this.tbtList.Enabled =
            this.tbtClose.Enabled = true;
            this.tbtSave.Enabled = this.tbtEdit.Enabled =
            this.tbtDelete.Enabled = this.tbtCancel.Enabled = false;
            //this.tbtAuthorize.Enabled = false;
        }

        /// <summary>
        /// <Method to set form controls in disable mode>       
        /// </summary>
        /// <Param ctrlList = All controls list , FormMode = mode of the form></Param>
        /// <returns> none </returns>

        protected void FormEnableDisableMode(Control.ControlCollection ctrlList, bool FormMode)
        {
            DataGridView dgv = null;
            foreach (Control ctrl in ctrlList)
            {
                if (ctrl.Controls.Count > 0)
                {
                    if ((dgv = ctrl as DataGridView) != null)
                    {
                        if (ctrl is iCORE.COMMON.SLCONTROLS.SLDataGridView)
                            dgv.Enabled = FormMode && ((iCORE.COMMON.SLCONTROLS.SLDataGridView)ctrl).CustomEnabled;// (ctrl as CrplControlLibrary.SLDataGridView).CustomEnabled;
                    }
                    else
                    {
                        FormEnableDisableMode(ctrl.Controls, FormMode);
                    }
                }
                else
                {
                    bool required = false;
                    switch (ctrl.GetType().Name)
                    {
                        case "LookupButton":
                            ctrl.Enabled = FormMode && (ctrl as CrplControlLibrary.LookupButton).CustomEnabled;
                            break;
                        case "SLTextBox":
                            //ctrl.Enabled = FormMode && (ctrl as CrplControlLibrary.SLTextBox).CustomEnabled;
                            required = ((CrplControlLibrary.SLTextBox)ctrl).IsRequired;
                            ((CrplControlLibrary.SLTextBox)ctrl).IsRequired = false;
                            ctrl.Enabled = FormMode && (ctrl as CrplControlLibrary.SLTextBox).CustomEnabled;
                            ((CrplControlLibrary.SLTextBox)ctrl).IsRequired = required;
                            break;
                        case "SLCheckBox":
                            ctrl.Enabled = FormMode && (ctrl as CrplControlLibrary.SLCheckBox).CustomEnabled;
                            break;
                        case "SLDatePicker":
                            //ctrl.Enabled = FormMode && (ctrl as CrplControlLibrary.SLDatePicker).CustomEnabled;
                            required = ((CrplControlLibrary.SLDatePicker)ctrl).IsRequired;
                            ((CrplControlLibrary.SLDatePicker)ctrl).IsRequired = false;
                            ctrl.Enabled = FormMode && (ctrl as CrplControlLibrary.SLDatePicker).CustomEnabled;
                            ((CrplControlLibrary.SLDatePicker)ctrl).IsRequired = required;

                            break;
                        default:
                            ctrl.Enabled = FormMode;
                            break;
                    }


                    /*    if (ctrl is CrplControlLibrary.CrplTextBox)
                            (ctrl as CrplControlLibrary.CrplTextBox).Enabled = FormMode && (ctrl as CrplControlLibrary.SLTextBox).CustomEnabled;
                       */


                }
            }
        }

        /// <summary>
        /// <Method to set form controls in clear mode>       
        /// </summary>
        /// <Param ctrlList = All controls list></Param>
        /// <returns> none </returns>

        public void ClearForm(Control.ControlCollection ctrlList)
        {
            TextBox txt = null;
            DateTimePicker dtp = null;
            ComboBox cmb = null;
            CheckBox chk = null;
            DataGridView dgv = null;

            foreach (Control ctrl in ctrlList)
            {
                if (ctrl.Controls.Count > 0)
                {
                    if ((dgv = ctrl as DataGridView) != null)
                    {
                        dgv.CancelEdit();
                        dgv.EndEdit();
                        if (dgv.DataSource != null)
                            (dgv.DataSource as DataTable).Reset();
                        dgv.DataSource = null;
                    }
                    else
                    {
                        ClearForm(ctrl.Controls);
                    }
                }
                else
                {
                    if (ctrl.Tag == null || ctrl.Tag.ToString() != "1")
                    {
                        if ((txt = ctrl as TextBox) != null)
                        {
                            txt.Text = "";
                        }
                        else if ((dtp = ctrl as DateTimePicker) != null)
                        {
                            dtp.Value = DateTime.Now;
                        }
                        else if ((cmb = ctrl as ComboBox) != null)
                        {
                            if (cmb.Items.Count > 0)
                                cmb.SelectedIndex = 0;
                        }
                        else if ((chk = ctrl as CheckBox) != null)
                        {
                            chk.Checked = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <Method to set error on duplicate check or empty check.>       
        /// </summary>
        /// <Param objectName = Name of object , message = message to show on error,
        /// ctrlList = All controls list></Param>
        /// <returns> none </returns>

        protected void SetError(string objectName, string message, Control ctrl)
        {
            message = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);

            errorProvider1.SetError(ctrl, message);
        }

    }
}