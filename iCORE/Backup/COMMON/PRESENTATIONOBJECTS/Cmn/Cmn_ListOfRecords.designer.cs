namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    partial class Cmn_ListOfRecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cmn_ListOfRecords));
            this.grbListOfRecords = new System.Windows.Forms.GroupBox();
            this.dgvListOfRecords = new System.Windows.Forms.DataGridView();
            this.panelSearchListOfRecord = new System.Windows.Forms.Panel();
            this.lblToDateSearch = new System.Windows.Forms.Label();
            this.lblFromDateSearch = new System.Windows.Forms.Label();
            this.dtpToDateSearch = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpFromDateSearch = new CrplControlLibrary.SLDatePicker(this.components);
            this.chkBooleanSearch = new System.Windows.Forms.CheckBox();
            this.txtDecimalSearch = new CrplControlLibrary.CrplTextBox(this.components);
            this.txtDigitSearch = new CrplControlLibrary.CrplTextBox(this.components);
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.txbStringSearch = new CrplControlLibrary.CrplTextBox(this.components);
            this.pcbCitiGroup = new System.Windows.Forms.PictureBox();
            this.tltListOfRecords = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dgvSearch = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.grpSearch = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grbListOfRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListOfRecords)).BeginInit();
            this.panelSearchListOfRecord.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.grpSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbListOfRecords
            // 
            this.grbListOfRecords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbListOfRecords.Controls.Add(this.dgvListOfRecords);
            this.grbListOfRecords.Controls.Add(this.panelSearchListOfRecord);
            this.grbListOfRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbListOfRecords.Location = new System.Drawing.Point(16, 193);
            this.grbListOfRecords.Name = "grbListOfRecords";
            this.grbListOfRecords.Size = new System.Drawing.Size(621, 362);
            this.grbListOfRecords.TabIndex = 1;
            this.grbListOfRecords.TabStop = false;
            this.grbListOfRecords.Text = "List of Records";
            // 
            // dgvListOfRecords
            // 
            this.dgvListOfRecords.AllowUserToAddRows = false;
            this.dgvListOfRecords.AllowUserToDeleteRows = false;
            this.dgvListOfRecords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvListOfRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListOfRecords.Location = new System.Drawing.Point(12, 24);
            this.dgvListOfRecords.MultiSelect = false;
            this.dgvListOfRecords.Name = "dgvListOfRecords";
            this.dgvListOfRecords.ReadOnly = true;
            this.dgvListOfRecords.Size = new System.Drawing.Size(597, 287);
            this.dgvListOfRecords.TabIndex = 0;
            this.dgvListOfRecords.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListOfRecords_RowDoubleClick);
            this.dgvListOfRecords.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListOfRecords_RowDoubleClick);
            this.dgvListOfRecords.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvListOfRecords_RowHeaderMouseDoubleClick);
            // 
            // panelSearchListOfRecord
            // 
            this.panelSearchListOfRecord.Controls.Add(this.lblToDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.lblFromDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.dtpToDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.dtpFromDateSearch);
            this.panelSearchListOfRecord.Controls.Add(this.chkBooleanSearch);
            this.panelSearchListOfRecord.Controls.Add(this.txtDecimalSearch);
            this.panelSearchListOfRecord.Controls.Add(this.txtDigitSearch);
            this.panelSearchListOfRecord.Controls.Add(this.cmbSearch);
            this.panelSearchListOfRecord.Controls.Add(this.lblSearch);
            this.panelSearchListOfRecord.Controls.Add(this.txbStringSearch);
            this.panelSearchListOfRecord.Location = new System.Drawing.Point(6, 318);
            this.panelSearchListOfRecord.Name = "panelSearchListOfRecord";
            this.panelSearchListOfRecord.Size = new System.Drawing.Size(610, 38);
            this.panelSearchListOfRecord.TabIndex = 13;
            // 
            // lblToDateSearch
            // 
            this.lblToDateSearch.AutoSize = true;
            this.lblToDateSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDateSearch.Location = new System.Drawing.Point(420, 10);
            this.lblToDateSearch.Name = "lblToDateSearch";
            this.lblToDateSearch.Size = new System.Drawing.Size(21, 15);
            this.lblToDateSearch.TabIndex = 12;
            this.lblToDateSearch.Text = "To";
            // 
            // lblFromDateSearch
            // 
            this.lblFromDateSearch.AutoSize = true;
            this.lblFromDateSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDateSearch.Location = new System.Drawing.Point(264, 10);
            this.lblFromDateSearch.Name = "lblFromDateSearch";
            this.lblFromDateSearch.Size = new System.Drawing.Size(36, 15);
            this.lblFromDateSearch.TabIndex = 11;
            this.lblFromDateSearch.Text = "From";
            // 
            // dtpToDateSearch
            // 
            this.dtpToDateSearch.CustomEnabled = true;
            this.dtpToDateSearch.CustomFormat = "dd/MM/yyyy";
            this.dtpToDateSearch.DataFieldMapping = "";
            this.dtpToDateSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDateSearch.HasChanges = false;
            this.dtpToDateSearch.Location = new System.Drawing.Point(446, 10);
            this.dtpToDateSearch.Name = "dtpToDateSearch";
            this.dtpToDateSearch.NullValue = " ";
            this.dtpToDateSearch.Size = new System.Drawing.Size(106, 21);
            this.dtpToDateSearch.TabIndex = 10;
            this.dtpToDateSearch.Value = new System.DateTime(2011, 4, 8, 0, 0, 0, 0);
            this.dtpToDateSearch.ValueChanged += new System.EventHandler(this.dtpToDateSearch_ValueChanged);
            // 
            // dtpFromDateSearch
            // 
            this.dtpFromDateSearch.CustomEnabled = true;
            this.dtpFromDateSearch.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDateSearch.DataFieldMapping = "";
            this.dtpFromDateSearch.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDateSearch.HasChanges = false;
            this.dtpFromDateSearch.Location = new System.Drawing.Point(302, 10);
            this.dtpFromDateSearch.Name = "dtpFromDateSearch";
            this.dtpFromDateSearch.NullValue = " ";
            this.dtpFromDateSearch.Size = new System.Drawing.Size(106, 21);
            this.dtpFromDateSearch.TabIndex = 9;
            this.dtpFromDateSearch.Value = new System.DateTime(2011, 4, 8, 0, 0, 0, 0);
            this.dtpFromDateSearch.ValueChanged += new System.EventHandler(this.dtpFromDateSearch_ValueChanged);
            // 
            // chkBooleanSearch
            // 
            this.chkBooleanSearch.AutoSize = true;
            this.chkBooleanSearch.Location = new System.Drawing.Point(264, 10);
            this.chkBooleanSearch.Name = "chkBooleanSearch";
            this.chkBooleanSearch.Size = new System.Drawing.Size(95, 19);
            this.chkBooleanSearch.TabIndex = 8;
            this.chkBooleanSearch.Text = "checkBox1";
            this.chkBooleanSearch.UseVisualStyleBackColor = true;
            this.chkBooleanSearch.CheckedChanged += new System.EventHandler(this.chkBooleanSearch_CheckedChanged);
            // 
            // txtDecimalSearch
            // 
            this.txtDecimalSearch.AllowSpace = false;
            this.txtDecimalSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDecimalSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDecimalSearch.ContinuationTextBox = null;
            this.txtDecimalSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDecimalSearch.Location = new System.Drawing.Point(264, 10);
            this.txtDecimalSearch.Name = "txtDecimalSearch";
            this.txtDecimalSearch.NumberFormat = "###,###,##0.00";
            this.txtDecimalSearch.Postfix = "";
            this.txtDecimalSearch.Prefix = "";
            this.txtDecimalSearch.Size = new System.Drawing.Size(284, 20);
            this.txtDecimalSearch.TabIndex = 7;
            this.txtDecimalSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDecimalSearch.TextType = CrplControlLibrary.TextType.Double;
            this.txtDecimalSearch.Visible = false;
            this.txtDecimalSearch.TextChanged += new System.EventHandler(this.txtDecimalSearch_TextChanged);
            // 
            // txtDigitSearch
            // 
            this.txtDigitSearch.AllowSpace = false;
            this.txtDigitSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDigitSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDigitSearch.ContinuationTextBox = null;
            this.txtDigitSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDigitSearch.Location = new System.Drawing.Point(264, 10);
            this.txtDigitSearch.Name = "txtDigitSearch";
            this.txtDigitSearch.NumberFormat = "###,###,##0.00";
            this.txtDigitSearch.Postfix = "";
            this.txtDigitSearch.Prefix = "";
            this.txtDigitSearch.Size = new System.Drawing.Size(284, 20);
            this.txtDigitSearch.TabIndex = 6;
            this.txtDigitSearch.TextType = CrplControlLibrary.TextType.Integer;
            this.txtDigitSearch.Visible = false;
            this.txtDigitSearch.TextChanged += new System.EventHandler(this.txtDigitSearch_TextChanged);
            // 
            // cmbSearch
            // 
            this.cmbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSearch.FormattingEnabled = true;
            this.cmbSearch.Location = new System.Drawing.Point(60, 10);
            this.cmbSearch.Name = "cmbSearch";
            this.cmbSearch.Size = new System.Drawing.Size(200, 21);
            this.cmbSearch.TabIndex = 1;
            this.tltListOfRecords.SetToolTip(this.cmbSearch, "Select Search Criteria here.");
            this.cmbSearch.SelectedIndexChanged += new System.EventHandler(this.cmbSearch_SelectedIndexChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearch.Location = new System.Drawing.Point(12, 10);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(46, 15);
            this.lblSearch.TabIndex = 1;
            this.lblSearch.Text = "Search";
            // 
            // txbStringSearch
            // 
            this.txbStringSearch.AllowSpace = true;
            this.txbStringSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txbStringSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txbStringSearch.ContinuationTextBox = null;
            this.txbStringSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbStringSearch.Location = new System.Drawing.Point(264, 10);
            this.txbStringSearch.Name = "txbStringSearch";
            this.txbStringSearch.NumberFormat = "###,###,##0.00";
            this.txbStringSearch.Postfix = "";
            this.txbStringSearch.Prefix = "";
            this.txbStringSearch.Size = new System.Drawing.Size(284, 20);
            this.txbStringSearch.TabIndex = 3;
            this.txbStringSearch.TextType = CrplControlLibrary.TextType.String;
            this.txbStringSearch.Visible = false;
            this.txbStringSearch.TextChanged += new System.EventHandler(this.txbStringSearch_TextChanged);
            // 
            // pcbCitiGroup
            // 
            this.pcbCitiGroup.Image = ((System.Drawing.Image)(resources.GetObject("pcbCitiGroup.Image")));
            this.pcbCitiGroup.Location = new System.Drawing.Point(568, 6);
            this.pcbCitiGroup.Name = "pcbCitiGroup";
            this.pcbCitiGroup.Size = new System.Drawing.Size(64, 64);
            this.pcbCitiGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pcbCitiGroup.TabIndex = 1;
            this.pcbCitiGroup.TabStop = false;
            this.tltListOfRecords.SetToolTip(this.pcbCitiGroup, "CitiGroup");
            // 
            // tltListOfRecords
            // 
            this.tltListOfRecords.IsBalloon = true;
            this.tltListOfRecords.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(336, 558);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.tltListOfRecords.SetToolTip(this.btnCancel, "Press button to Exit.");
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(226, 558);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "&OK";
            this.tltListOfRecords.SetToolTip(this.btnOK, "Press button to Select a Record.");
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Location = new System.Drawing.Point(12, 18);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.Size = new System.Drawing.Size(597, 81);
            this.dgvSearch.TabIndex = 6;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(504, 102);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(105, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // grpSearch
            // 
            this.grpSearch.Controls.Add(this.dgvSearch);
            this.grpSearch.Controls.Add(this.btnSearch);
            this.grpSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.Location = new System.Drawing.Point(16, 65);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Size = new System.Drawing.Size(621, 132);
            this.grpSearch.TabIndex = 8;
            this.grpSearch.TabStop = false;
            this.grpSearch.Text = "Search Records";
            this.grpSearch.Visible = false;
            // 
            // Cmn_ListOfRecords
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(655, 594);
            this.Controls.Add(this.grpSearch);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pcbCitiGroup);
            this.Controls.Add(this.grbListOfRecords);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Cmn_ListOfRecords";
            this.Text = "iCORE DDS - List of Records";
            this.Load += new System.EventHandler(this.CRS_Cmn_ListOfRecords_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CRS_Cmn_ListOfRecords_KeyDown);
            this.Controls.SetChildIndex(this.grbListOfRecords, 0);
            this.Controls.SetChildIndex(this.pcbCitiGroup, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.grpSearch, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grbListOfRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListOfRecords)).EndInit();
            this.panelSearchListOfRecord.ResumeLayout(false);
            this.panelSearchListOfRecord.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCitiGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.grpSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbListOfRecords;
        private System.Windows.Forms.PictureBox pcbCitiGroup;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.DataGridView dgvListOfRecords;
        private System.Windows.Forms.ComboBox cmbSearch;
        private System.Windows.Forms.ToolTip tltListOfRecords;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private CrplControlLibrary.CrplTextBox txbStringSearch;
        private CrplControlLibrary.CrplTextBox txtDigitSearch;
        private CrplControlLibrary.CrplTextBox txtDecimalSearch;
        private System.Windows.Forms.CheckBox chkBooleanSearch;
        private CrplControlLibrary.SLDatePicker dtpToDateSearch;
        private CrplControlLibrary.SLDatePicker dtpFromDateSearch;
        private System.Windows.Forms.Label lblToDateSearch;
        private System.Windows.Forms.Label lblFromDateSearch;
        private System.Windows.Forms.DataGridView dgvSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox grpSearch;
        private System.Windows.Forms.Panel panelSearchListOfRecord;
    }
}
