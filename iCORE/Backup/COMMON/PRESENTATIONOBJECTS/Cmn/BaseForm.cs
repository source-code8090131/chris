using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using CrplControlLibrary;
using System.Data.SqlClient;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.Common.PRESENTATIONOBJECTS.Cmn
{
    /// <summary>
    /// ScreenMessage
    /// </summary>
    public class ScreenMessage
    {
        private string messageCode;

        public string MessageCode
        {
            get { return messageCode; }
            set { messageCode = value; }
        }
        private string messageText;

        public string MessageText
        {
            get { return messageText; }
            set { messageText = value; }
        }
        private MessageType type;

        public MessageType Type
        {
            get { return type; }
            set { type = value; }
        }
        /// <summary>
        /// Has Error
        /// </summary>
        /// <returns></returns>
        public Boolean HasErrors()
        {
            return (Type == MessageType.Error);
        }
        /// <summary>
        /// Has Warning
        /// </summary>
        /// <returns></returns>
        public Boolean HasWarning()
        {
            return (Type == MessageType.Warning);
        }
        /// <summary>
        /// Show Message
        /// </summary>
        public void ShowMessage()
        {
            if (!String.IsNullOrEmpty(MessageCode))
            {
                CommonDataManager oDataManager = new CommonDataManager();
                Result rslt = oDataManager.GetAll("CMN_SP_GetMessage", String.Empty, MessageCode, "MessageCode");
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        MessageText = rslt.dstResult.Tables[0].Rows[0]["APP_MESSAGE"].ToString();
                        if (rslt.dstResult.Tables[0].Rows[0]["MESSAGE_TYPE"].ToString().Equals("E", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Type = MessageType.Error;
                        }
                        else
                        {
                            Type = MessageType.Information;
                        }
                    }

                }
                //Get Message Aggainst Message COde
            }
            if (Type == MessageType.Error)
            {
                MessageBox.Show(MessageText, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Type == MessageType.Warning)
            {
                MessageBox.Show(MessageText, "Warning",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (Type == MessageType.Information)
            {
                MessageBox.Show(MessageText, "Information",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            MessageText = String.Empty;
            MessageCode = String.Empty;
            Type = MessageType.Null;
        }

    }
    public delegate void AfterLOVSelection(DataRow selectedRow, string actionType);
    public delegate void BeforeLOVShown(Cmn_ListOfRecords LOV, string actionType);

    public partial class BaseForm : Cmn_TemplateCR
    {
        public enum DBOperation : int { Transactional = 0, NonTransactional = 1, SpecialTransaction = 2 }

        #region --Field Segement--
        protected const string PKID = "ID";
        private string m_strSPName;
        private string m_strEntityName;
        private string m_strDataManager;
        private string m_strAssociatedLookupNames = string.Empty;
        private bool m_IsValidPage = true;
        protected BusinessEntity m_oEntityClass;
        protected Hashtable m_htCustomEntityProperties;

        private string m_strColumnsToHide = "";
        // START: iCore
        //private AccountCommand accountEntity;
        protected IDataManager entityDataManager;
        protected Result rslt;
        protected int m_intPKID = 0;
        protected ScreenMessage Message = new ScreenMessage();

        protected SqlTransaction trans;
        protected SqlConnection conn = null;

        protected delegate void DataBlockOnFocusEventHandler(Control control);
        protected event DataBlockOnFocusEventHandler DataBlockOnFocus;
        // END: iCore
        // Grid's
        public iCORE.COMMON.SLCONTROLS.SLDataGridView m_dgvRecords;

        //Code By Faisal 13/12/2010
        private event AfterLOVSelection _afterLOVSelection;
        private event BeforeLOVShown _beforeLOVShown;

        //m_SkipLovValidationOnSave
        protected bool m_SkipLovValidationOnSave = false;
        #endregion

        #region --Constructor Segment--

        public BaseForm()
        {
            InitializeComponent();
        }

        public BaseForm(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        {
            this.connbean = connbean_obj;
            this.SetConnectionBean();
            InitializeComponent();
            // tbtAuthorize.Enabled = false;
            this.MdiParent = mainmenu;
            //pcbCitiGroup.Location = new Point(frm.Right   - 310, pcbCitiGroup.Top);
        }

        #endregion

        #region --Property Segment--

        #region Events

        //Added by Faisal 13/12/2010
        public event AfterLOVSelection AfterLOVSelection
        {
            add
            {
                this._afterLOVSelection += value;
            }
            remove
            {
                this._afterLOVSelection -= value;
            }
        }
        //Added by Faisal 13/12/2010
        public event BeforeLOVShown BeforeLOVShown
        {
            add
            {
                this._beforeLOVShown += value;
            }
            remove
            {
                this._beforeLOVShown -= value;
            }
        }

        #endregion

        [Browsable(true)]
        [DefaultValue(true)]
        [Description("Confirms if form controls are validated successfully.")]
        public bool IsValidPage
        {
            get { return m_IsValidPage; }
        }

        [Browsable(true)]
        [Description("Set Current Panel block.")]
        [Category("FormLayout")]
        public string CurrentPanelBlock
        {
            get { return m_strCurrentPanelBlock; }
            set
            {
                m_strCurrentPanelBlock = value;

                Control oCtrl = this.Controls[this.CurrentPanelBlock];
                if (oCtrl is iCORE.COMMON.SLCONTROLS.SLPanel)
                    oCurrentPanel = (iCORE.COMMON.SLCONTROLS.SLPanel)oCtrl;
            }
        }
        private string m_strCurrentPanelBlock;

        [Browsable(false)]
        [Description("Query Mode.")]
        [Category("CustomBehavior")]
        [DefaultValue(Enumerations.eQueryMode.ExecuteQuery)]
        public Enumerations.eQueryMode currentQueryMode
        {
            get { return _eCurrentQueryMode; }
            set { _eCurrentQueryMode = value; }
        }
        Enumerations.eQueryMode _eCurrentQueryMode;
        private Boolean m_isEnterQuery;
        [Browsable(true)]
        [Description("Show/Hide EnterQueryMode.")]
        [Category("CustomBehavior")]
        public Boolean ShowQueryButton
        {
            get { return m_isEnterQuery; }
            set { m_isEnterQuery = value; }
        }

        [Browsable(false)]
        [Description("SkipsValidationOfLovExistOnSave")]
        [Category("CustomBehavior")]
        [DefaultValue(false)]
        public bool SkipLovValidationOnSave
        {
            get { return this.m_SkipLovValidationOnSave; }
            set { this.m_SkipLovValidationOnSave = value; }
        }

        public iCORE.COMMON.SLCONTROLS.SLPanel GetCurrentPanelBlock
        {
            get
            {
                Control oCtrl = this.Controls[this.CurrentPanelBlock];

                if (oCtrl == null)
                    oCtrl = Utilities.FindControlRecursive(this, this.CurrentPanelBlock);

                if (oCtrl is iCORE.COMMON.SLCONTROLS.SLPanel)
                    oCurrentPanel = (iCORE.COMMON.SLCONTROLS.SLPanel)oCtrl;

                return oCurrentPanel;
            }
        }

        private iCORE.COMMON.SLCONTROLS.SLPanel oCurrentPanel;

        #endregion

        #region --Event Segment--

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            CommonOnLoadMethods();
            if (!this.DesignMode)
            {
                UpdateLoginHistory();
                this.Text = "iCORE Centralized Human Resource Information System - " + SeparatePascalCaseWord(this.GetType().Name.Split("_".ToCharArray(), 3)[2]); //this.GetType().Name.Split("_".ToCharArray(), 3)[0] + " - " + this.GetType().Name.Split("_".ToCharArray(), 3)[2].Replace("_", " ");
            }

        }
        /// <summary>
        /// OnFormClosing
        /// </summary>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (e.Cancel == true)
            {
                e.Cancel = false;
            }
            this.Dispose(false);
            base.Dispose(true);
            this.DestroyHandle();
            base.OnFormClosing(e);
        }

        /// <summary>
        /// Updates Login History
        /// </summary>
        private void UpdateLoginHistory()
        {
            try
            {
                DataSet dst = new DataSet();
                DataRow drw = null;
                dst.Tables.Add(SQLManager.GetSPParams("UPDT_LOGIN_HISTORY_DTL"));
                drw = dst.Tables["UPDT_LOGIN_HISTORY_DTL"].NewRow();
                drw["SOEID"] = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                drw["MAPP_CD"] = string.Empty;
                drw["MMENU_CD"] = string.Empty;
                drw["MPROG_NAME"] = this.Name.Substring(1,50);
                dst.Tables["UPDT_LOGIN_HISTORY_DTL"].Rows.Add(drw);
                dst.Tables["UPDT_LOGIN_HISTORY_DTL"].AcceptChanges();
                CommonDataManager.ExecuteDataSet(dst);
            }
            catch
            {
            }
        }
        /// <summary>
        /// GetActionType
        /// </summary>
        /// <param name="pBtnText"></param>
        /// <returns></returns>
        protected string GetActionType(string pBtnText)
        {
            return pBtnText.Replace("&", "");
        }

        /// <summary>
        /// Set Current Panel Property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SLControl_GotFocus(object sender, EventArgs e)
        {
            switch (sender.GetType().Name)
            {
                case "SLTextBox":
                    if (((SLTextBox)sender).Parent is GroupBox)
                    {
                        this.CurrentPanelBlock = ((SLTextBox)sender).Parent.Parent.Name;
                    }
                    else if (((SLTextBox)sender).Parent.GetType().BaseType.Equals(typeof(SLPanel)))
                        this.CurrentPanelBlock = ((SLTextBox)sender).Parent.Name;
                    break;
                case "LookupButton":
                    if (((LookupButton)sender).Parent is GroupBox)
                    {
                        this.CurrentPanelBlock = ((LookupButton)sender).Parent.Parent.Name;
                    }
                    else if (((LookupButton)sender).Parent.GetType().BaseType.Equals(typeof(SLPanel)))
                        this.CurrentPanelBlock = ((LookupButton)sender).Parent.Name;
                    break;
                case "SLCheckBox":
                    if (((SLCheckBox)sender).Parent is GroupBox)
                    {
                        this.CurrentPanelBlock = ((SLCheckBox)sender).Parent.Parent.Name;
                    }
                    else if (((SLCheckBox)sender).Parent.GetType().BaseType.Equals(typeof(SLPanel)))
                        this.CurrentPanelBlock = ((SLCheckBox)sender).Parent.Name;
                    break;
                case "SLComboBox":
                    if (((SLComboBox)sender).Parent is GroupBox)
                    {
                        this.CurrentPanelBlock = ((SLComboBox)sender).Parent.Parent.Name;
                    }
                    else if (((SLComboBox)sender).Parent.GetType().BaseType.Equals(typeof(SLPanel)))
                        this.CurrentPanelBlock = ((SLComboBox)sender).Parent.Name;
                    break;
                case "SLDataGridView":
                    if (((iCORE.COMMON.SLCONTROLS.SLDataGridView)sender).Parent is GroupBox)
                    {
                        this.CurrentPanelBlock = ((iCORE.COMMON.SLCONTROLS.SLDataGridView)sender).Parent.Parent.Name;
                    }
                    else if (((iCORE.COMMON.SLCONTROLS.SLDataGridView)sender).Parent.GetType().BaseType.Equals(typeof(SLPanel)))
                        this.CurrentPanelBlock = ((iCORE.COMMON.SLCONTROLS.SLDataGridView)sender).Parent.Name;
                    break;
                case "SLDatePicker":
                    if (((SLDatePicker)sender).Parent is GroupBox)
                    {
                        this.CurrentPanelBlock = ((SLDatePicker)sender).Parent.Parent.Name;
                    }
                    else if (((SLDatePicker)sender).Parent.GetType().BaseType.Equals(typeof(SLPanel)))
                        this.CurrentPanelBlock = ((SLDatePicker)sender).Parent.Name;
                    break;
            }

            OnDataBlockGotFocus(sender as Control);
        }

        /// <summary>
        /// Raise Datablock[SLPanel] focus event
        /// </summary>
        /// <param name="control"></param>
        protected virtual void OnDataBlockGotFocus(Control control)
        {
            if (DataBlockOnFocus != null)
                DataBlockOnFocus(control);
        }
        /// <summary>
        /// ToolBar Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected internal override void tlbMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            base.tlbMain_ItemClicked(sender, e);
            DoToolbarActions(this.Controls, GetActionType(e.ClickedItem.Text));
        }
        /// <summary>
        /// BaseForm_VisibleChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_VisibleChanged(object sender, EventArgs e)
        {
            if (!DesignMode && !VerifyMarkUpRate())
                this.Close();
        }

        protected virtual bool VerifyMarkUpRate()
        {
                return true;
        }
        /// <summary>
        /// Controls record traversing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SlTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                if (this.GetCurrentPanelBlock is SLPanelSimple)
                {
                    DoToolbarActions(this.GetCurrentPanelBlock.Controls, "Previous");
                    (((SLTextBox)(sender))).Focus();
                }

            }
            else if (e.KeyCode == Keys.Down)
            {
                if (this.GetCurrentPanelBlock is SLPanelSimple)
                {
                    DoToolbarActions(this.GetCurrentPanelBlock.Controls, "Next");
                    (((SLTextBox)(sender))).Focus();
                }
            }
        }


        #endregion

        #region --Method Segment--

        /// <summary>
        /// Notify All AfterLOVSelection Event subcribers
        /// </summary>
        private void LOVRecordSelected(DataRow selectedRow, string actionType)
        {
            if (this._afterLOVSelection != null)
            {
                this._afterLOVSelection(selectedRow, actionType);
            }
        }
        /// <summary>
        /// Notify All BeforeLOVShown Event subcribers
        /// </summary>
        private void BeforeLOVShowing(Cmn_ListOfRecords LOV, string actionType)
        {
            if (this._beforeLOVShown != null)
            {
                this._beforeLOVShown(LOV, actionType);
            }
        }

        /// <summary>
        /// Set Message
        /// </summary>
        /// <param name="objectName"></param>
        /// <param name="message"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageType"></param>
        public void SetMessage(string objectName, string message, string messageCode, MessageType messageType)
        {
            this.Message.MessageCode = messageCode;
            this.Message.MessageText = message.Replace(ApplicationMessages.OBJECTIDENTIFIER, objectName);
            this.Message.Type = messageType;
        }

        /// <summary>
        /// Has Message
        /// </summary>
        /// <returns></returns>
        public Boolean HasMessage()
        {
            bool pbRetValue = false;
            if (this.Message.MessageText != null)
            {
                if (this.Message.MessageText.Length > 0)
                {
                    pbRetValue = true;
                }
            }
            return pbRetValue;
        }

        /// <summary>
        /// Virtual CreateToolbar method 
        /// </summary>
        protected virtual void CreateToolbar()
        {
            // Implementation over derived form.
            if (ShowQueryButton)
            {
                // tbtQuery
                ToolStripButton tbtQuery = new ToolStripButton();
                tbtQuery.Image = iCORE.Properties.Resources.Edit_Record;
                tbtQuery.ImageScaling = ToolStripItemImageScaling.None;
                tbtQuery.ImageTransparentColor = Color.Magenta;
                tbtQuery.Name = "tbtQuery";
                tbtQuery.Size = new Size(44, 33);
                tbtQuery.Text = "&Query";
                tbtQuery.TextImageRelation = TextImageRelation.ImageAboveText;
                tlbMain.Items.Insert(3, tbtQuery);

            }
        }

        /// <summary>
        /// LogError
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="ex"></param>
        public void LogError(string functionName, Exception ex)
        {
            base.LogError(this.Name, functionName, ex);
        }

        /// <summary>
        /// Creates Instance of Entity and DataManager class
        /// </summary>
        /// <param name="oPanel"></param>
        protected void InitializeFormObject(SLPanel oPanel)
        {
            try
            {
                m_oEntityClass = oPanel.CurrentBusinessEntity;//RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
                entityDataManager = RuntimeClassLoader.GetDataManager(oPanel.DataManager);
            }
            catch (Exception ex)
            {
                this.LogException("BaseForm", "InitializeFormObject", ex);
            }
        }

        /// <summary>
        /// CommonOnLoadMethods
        /// </summary>
        protected virtual void CommonOnLoadMethods()
        {
            SetFormObjects();
            BindFormLOVCombos();
            IterateFormControls(this.Controls, false, false, true);
        }

        /// <summary>
        /// SetFormObjects
        /// </summary>
        private void SetFormObjects()
        {
            try
            {
                this.Activate();
                this.FormAddMode(this.Controls);
                this.listOfRecordsType = ListType.SetupList;
            }
            catch (Exception ex)
            {
                this.LogException("BaseForm", "SetFormObjects", ex);
            }
        }

        /// <summary>
        /// DoToolbarActions
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public virtual void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            SLPanel oCtrlPanel = this.GetCurrentPanelBlock;
            if (actionType == "Previous")
            {
                if (oCtrlPanel is SLPanelSimple)
                {
                    this.GetNextOrPrevoiusRecord(oCtrlPanel, "Previous");
                    base.FormEditMode(oCtrlPanel.Controls);
                    operationMode = Mode.Edit;
                }
                else if (oCtrlPanel is SLPanelSimpleList)
                {
                    (oCtrlPanel as SLPanelSimpleList).LoadPreviousRecord(oCtrlPanel);
                }
            }
            else if (actionType == "Next")
            {
                if (oCtrlPanel is SLPanelSimple)
                {
                    this.GetNextOrPrevoiusRecord(oCtrlPanel, "Next");
                    base.FormEditMode(oCtrlPanel.Controls);
                    operationMode = Mode.Edit;
                }
                else if (oCtrlPanel is SLPanelSimpleList)
                {
                    (oCtrlPanel as SLPanelSimpleList).LoadNextRecord(oCtrlPanel);
                }
            }
            //if (actionType == "Previous")
            //{
            //    SLPanel oCtrlPanel = this.GetCurrentPanelBlock;
            //    this.GetNextOrPrevoiusRecord(oCtrlPanel, "Previous");
            //    base.FormEditMode(oCtrlPanel.Controls);
            //    operationMode = Mode.Edit;
            //}
            //else if (actionType == "Next")
            //{
            //    SLPanel oCtrlPanel = this.GetCurrentPanelBlock;
            //    this.GetNextOrPrevoiusRecord(oCtrlPanel, "Next");
            //    base.FormEditMode(oCtrlPanel.Controls);
            //    operationMode = Mode.Edit;
            //}
            if (m_htCustomEntityProperties != null && m_htCustomEntityProperties.Count > 0)
            {
                SetCustomEntityProperties();
            }
        }

        /// <summary>
        /// ShowList
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pstrActionType"></param>
        public void ShowList(SLPanel oPanel, string pstrActionType)
        {
            try
            {
                Cmn_ListOfRecords sectionList = new Cmn_ListOfRecords(oPanel.SPName, pstrActionType);

                sectionList.Entity = RuntimeClassLoader.GetBusinessEntity(oPanel.EntityName);
                sectionList.DataManagerName = oPanel.DataManager;
                sectionList.Id = this.m_intPKID;
                sectionList.Text = this.Text + " List";
                sectionList.ListType = this.listOfRecordsType;
                this.BeforeLOVShowing(sectionList, pstrActionType);
                if (sectionList.ShowDialog() == DialogResult.OK)
                {
                    DataRow drw = sectionList.SelectedRecord;
                    if (drw != null)
                    {
                        this.SetPanelControlWithListItem(oPanel.Controls, drw);
                        oPanel.LoadConcurrentPanels(drw);

                        this.FormListMode(oPanel.Controls);

                        oPanel.SetEntityObject(drw);
                        oPanel.LoadDependentPanels();
                    }
                    this.LOVRecordSelected(drw, pstrActionType);
                }
            }
            catch (Exception ex)
            {
                this.LogException("BaseForm", "ShowList", ex);
            }
        }


        /// <summary>
        /// Shortcut Implementation of oracle forms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnFormKeyup(object sender, KeyEventArgs e)
        {
            SLPanel oCtrlPanel = GetCurrentPanelBlock;
            if (e.KeyCode == Keys.F8 && currentQueryMode == Enumerations.eQueryMode.ExecuteQuery)
            {
                DoToolbarActions(this.Controls, "Search");
            }
            else if (e.KeyCode == Keys.F8 && currentQueryMode == Enumerations.eQueryMode.EnterQuery)
            {
                DoToolbarActions(this.Controls, "Query");
            }
            else if (e.KeyCode == Keys.F10)
            {
                DoToolbarActions(this.Controls, "Save");
            }
            else if (e.KeyCode == Keys.F6)
            {
                DoToolbarActions(this.Controls, "AddNew");
            }
            else if (e.KeyCode == Keys.F7 && currentQueryMode == Enumerations.eQueryMode.ExecuteQuery)
            {
                if (this.GetCurrentPanelBlock.EnableQuery)

                    DoToolbarActions(this.Controls, "Query");
                else
                    DoToolbarActions(this.Controls, "ClearScreen");


            }
            else if (oCtrlPanel is SLPanelSimpleList && e.KeyCode == Keys.Down)
            {
                if(e.Modifiers !=Keys.Alt)
                {
                    Control ctrl = this.ActiveControl;

                    if (ctrl != null)
                    {
                        if (ctrl.Parent is SLPanelSimpleList)
                        {
                            DoToolbarActions(this.GetCurrentPanelBlock.Controls, "Next");//ref actionType}
                        }
                    }
                }

                //DoToolbarActions(this.GetCurrentPanelBlock.Controls, "Next");//ref actionType
            }
            else if (oCtrlPanel is SLPanelSimpleList && e.KeyCode == Keys.Up)
            {
                Control ctrl = this.ActiveControl;

                if (ctrl != null)
                {
                    if (ctrl.Parent is SLPanelSimpleList)
                    {
                        DoToolbarActions(this.GetCurrentPanelBlock.Controls, "Previous");
                    }
                }
                //DoToolbarActions(this.GetCurrentPanelBlock.Controls, "Previous");
            }
            else
            {
                if (e.Modifiers.ToString() != "None")
                {
                    if (e.Modifiers == Keys.Shift)
                    {
                        switch (e.KeyCode)
                        {
                            case Keys.F4:
                                DoToolbarActions(this.Controls, "SelectCurrentGridRow");
                                break;
                            case Keys.F5:
                                DoToolbarActions(this.Controls, "SetGridInAddMode");
                                break;
                            case Keys.F6:
                                DoToolbarActions(this.Controls, "SelectCurrentGridRow");
                                DoToolbarActions(this.Controls, "Delete");
                                break;
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Set Control values
        /// </summary>
        /// <param name="oCtrls"></param>
        /// <param name="poDR"></param>
        public void SetPanelControlWithListItem(Control.ControlCollection oCtrls, DataRow poDR)
        {
            if (poDR.Table.Columns.Contains(PKID) && Int32.TryParse(poDR[PKID].ToString(), out m_intPKID))
                this.m_intPKID = Convert.ToInt32(poDR[PKID]);

            foreach (Control oLoopControl in oCtrls)
            {
                switch (oLoopControl.GetType().Name.ToString())
                {
                    case "GroupBox":
                        SetPanelControlWithListItem(oLoopControl.Controls, poDR);
                        break;
                    case "SLTextBox":
                        if (poDR.Table.Columns.Contains(((SLTextBox)oLoopControl).DataFieldMapping))
                            ((SLTextBox)oLoopControl).Text = poDR[((SLTextBox)oLoopControl).DataFieldMapping].ToString();
                        break;
                    case "SLComboBox":
                        if (poDR.Table.Columns.Contains(((SLComboBox)oLoopControl).DataFieldMapping))
                            ((SLComboBox)oLoopControl).SelectedValue = poDR[((SLComboBox)oLoopControl).DataFieldMapping].ToString();
                        break;
                    case "SLCheckBox":
                        if (poDR.Table.Columns.Contains(((SLCheckBox)oLoopControl).DataFieldMapping))
                            ((SLCheckBox)oLoopControl).Checked = (poDR[((SLCheckBox)oLoopControl).DataFieldMapping].ToString() == "1" || poDR[((SLCheckBox)oLoopControl).DataFieldMapping].ToString().ToUpper() == "Y" ? true : false);
                        break;
                    case "SLRadioButton":
                        if (poDR.Table.Columns.Contains(((SLRadioButton)oLoopControl).DataFieldMapping))
                            ((SLRadioButton)oLoopControl).Checked = (poDR[((SLRadioButton)oLoopControl).DataFieldMapping].ToString() == "1" ? true : false);
                        break;
                    case "SLDatePicker":
                        if (poDR.Table.Columns.Contains(((SLDatePicker)oLoopControl).DataFieldMapping))
                            ((SLDatePicker)oLoopControl).Value = poDR[((SLDatePicker)oLoopControl).DataFieldMapping] != DBNull.Value ? ((object)Convert.ToDateTime(poDR[((SLDatePicker)oLoopControl).DataFieldMapping].ToString())) : null;
                        break;
                }
            }

            if (m_htCustomEntityProperties != null && m_htCustomEntityProperties.Count > 0)
            {
                SetCustomEntityProperties();
            }
        }


        /// <summary>
        /// Save
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pActionType"></param>
        /// <returns></returns>
        protected virtual Boolean Save_Click(SLPanel oPanel, string pActionType)
        {
            InitializeFormObject(oPanel);

            if (this.operationMode == Mode.Add)
            {
                IterateFormControls(oPanel.Controls, false, false, false);
                IterateConcurrentPanels(oPanel, false, false, false);
                if (this.IsValidPage)
                {
                    oPanel.CurrentBusinessEntity.SoeId = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                    rslt = entityDataManager.Add(this.m_oEntityClass, oPanel.SPName, pActionType);
                    if (rslt.isSuccessful)
                    {
                        oPanel.FillEntity(this.m_oEntityClass, rslt.hstOutParams);
                        oPanel.CurrentBusinessEntity = this.m_oEntityClass;
                        this.IterateFormControls(oPanel.Controls, true, false, false);
                        this.IterateConcurrentPanels(oPanel, true, false, false);
                        //this.ShowInformationMessage(oPanel.EntityName, ApplicationMessages.ADD_SUCCESS_MESSAGE);
                        this.SetMessage("Record", ApplicationMessages.ADD_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                        //this.ShowInformationMessage("Record", ApplicationMessages.ADD_SUCCESS_MESSAGE);
                        this.FormAddMode(oPanel.Controls);
                    }
                    else if (rslt.exp.Message.Equals(ApplicationMessages.DBMessages.DuplicateRecord.GetHashCode().ToString()) == true)
                        this.SetMessage("Record", ApplicationMessages.DUPLICATE_MESSAGE, String.Empty, MessageType.Error);
                    //this.ShowErrorMessage("Record", ApplicationMessages.DUPLICATE_MESSAGE);
                    else if ((rslt.exp is System.Data.SqlClient.SqlException) && (rslt.exp as System.Data.SqlClient.SqlException).Number == 50000)
                        this.SetMessage("", rslt.exp.Message, String.Empty, MessageType.Error);
                    else
                        this.SetMessage("Record", ApplicationMessages.ADD_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                    //this.ShowErrorMessage("Record", ApplicationMessages.ADD_FAILURE_MESSAGE);
                    if (rslt.isSuccessful)
                    {
                        this.FormAddMode(oPanel.Controls);
                    }
                }
            }
            else if (this.operationMode == Mode.Edit)
            {

                SetCustomEntityProperties(PKID, this.m_intPKID.ToString());
                IterateConcurrentPanels(oPanel, false, false, false);

                IterateFormControls(oPanel.Controls, false, false, false);
                if (this.IsValidPage)
                {
                    oPanel.CurrentBusinessEntity.SoeId = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
                    rslt = entityDataManager.Update(this.m_oEntityClass, oPanel.SPName, "Update");

                    if (rslt.isSuccessful)
                    {
                        oPanel.FillEntity(this.m_oEntityClass, rslt.hstOutParams);
                        oPanel.CurrentBusinessEntity = this.m_oEntityClass;
                        this.IterateFormControls(oPanel.Controls, true, false, false);
                        this.IterateConcurrentPanels(oPanel, true, false, false);
                        this.SetMessage("Record ", ApplicationMessages.UPDATE_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                    }
                    //this.ShowInformationMessage("Record", ApplicationMessages.UPDATE_SUCCESS_MESSAGE);
                    else if (rslt.exp.Message.Equals(ApplicationMessages.DBMessages.DuplicateRecord.GetHashCode().ToString()) == true)
                        this.SetMessage("Record ", ApplicationMessages.DUPLICATE_MESSAGE, String.Empty, MessageType.Error);
                    //this.ShowErrorMessage("Record ", ApplicationMessages.DUPLICATE_MESSAGE);
                    else if ((rslt.exp is System.Data.SqlClient.SqlException) && (rslt.exp as System.Data.SqlClient.SqlException).Number == 50000)
                        this.SetMessage("", rslt.exp.Message, String.Empty, MessageType.Error);
                    else
                        this.SetMessage("Record ", ApplicationMessages.UPDATE_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                    //this.ShowErrorMessage("Record ", ApplicationMessages.UPDATE_FAILURE_MESSAGE);
                }
            }


            return rslt.isSuccessful;
        }

        /// <summary>
        /// Delete operation
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pstrActionType"></param>
        protected virtual void Delete_Click(SLPanel oPanel, string pstrActionType)
        {
            try
            {
                if (this.m_intPKID > 0)
                {
                    InitializeFormObject(oPanel);

                    SetCustomEntityProperties(PKID, this.m_intPKID.ToString());

                    if (m_htCustomEntityProperties != null && m_htCustomEntityProperties.Count > 0)
                    {
                        SetCustomEntityProperties();
                    }

                    rslt = entityDataManager.Delete(this.m_oEntityClass, oPanel.SPName, pstrActionType);
                    if (rslt.isSuccessful)
                    {
                        this.SetMessage("Record ", ApplicationMessages.DELETE_SUCCESS_MESSAGE, String.Empty, MessageType.Information);
                        this.FormCancelMode(oPanel.ConcurrentPanels);
                        this.FormEditMode(oPanel.ConcurrentPanels);
                    }
                    //this.ShowInformationMessage("Record", ApplicationMessages.DELETE_SUCCESS_MESSAGE);
                    else if (rslt.exp.Message.Equals(ApplicationMessages.DBMessages.DeleteRecord.GetHashCode().ToString()) == true)
                        this.SetMessage("", ApplicationMessages.DELETE_MASTER_RECORD, String.Empty, MessageType.Error);
                    //this.ShowErrorMessage("", ApplicationMessages.DELETE_MASTER_RECORD);
                    else
                        this.SetMessage("Record ", ApplicationMessages.DELETE_FAILURE_MESSAGE, String.Empty, MessageType.Error);
                    //this.ShowErrorMessage("Record", ApplicationMessages.DELETE_FAILURE_MESSAGE);

                }
            }
            catch (Exception ex)
            {
                this.LogException("BaseForm", "Delete_Click", ex);
            }

        }

        /// <summary>
        /// Custom DB Function
        /// </summary>
        /// <param name="oOpType"></param>
        /// <param name="oPanel"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        protected Result CustomDBFunction(DBOperation oOpType, SLPanel oPanel, string pstrActionType)
        {
            InitializeFormObject(oPanel);

            IterateFormControls(oPanel.Controls, false, false, false);
            if (DBOperation.SpecialTransaction.ToString() == oOpType.ToString())
            {
                try
                {
                    if ((this.trans == null) || (this.trans.Connection == null))
                    {
                        conn = this.connbean.getDatabaseConnection();
                        trans = conn.BeginTransaction();
                    }
                    rslt = entityDataManager.GetAll(this.m_oEntityClass, oPanel.SPName, pstrActionType, ref trans);

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    this.LogException("BaseForm", "CustomDBFunction", ex);
                }
            }
            return rslt;
        }

        /// <summary>
        /// Commit Custom Fucntion
        /// </summary>
        /// <returns></returns>
        protected bool CommitCustomDBFunction()
        {
            bool pbReturn = true;
            try
            {
                if ((this.trans != null) && (this.trans.Connection != null) && (this.trans.Connection.State == ConnectionState.Open))
                {
                    this.trans.Commit();
                    pbReturn = true;
                }
            }
            catch (Exception ex)
            {
                pbReturn = false;
                trans.Rollback();
                this.LogException("BaseForm", "CommitCustomDBFunction", ex);
            }
            finally
            {
                trans = null;
            }
            return pbReturn;
        }

        /// <summary>
        /// Rollback Custom Function
        /// </summary>
        protected void RollBackCustomDBFunction()
        {
            try
            {
                if ((this.trans != null) && (this.trans.Connection != null) && (this.trans.Connection.State == ConnectionState.Open))
                {
                    this.trans.Rollback();
                }
            }
            catch (Exception ex)
            {
                this.trans.Rollback();
                this.LogException("BaseForm", "RollBackCustomDBFunction", ex);
            }
            finally
            {
                //connbean.closeConn(conn);
                //conn = null;
            }
        }

        /// <summary>
        /// Bind Handlers
        /// </summary>
        private void BindFormLOVCombos()
        {
            foreach (Control oLoopControl in this.Controls)
            {
                switch (oLoopControl.GetType().Name.ToString())
                {
                    case "TabControl":
                        foreach (Control oLoopInnerControl in oLoopControl.Controls)
                        {
                            foreach (Control oLoopGroupControl in oLoopInnerControl.Controls)
                            {
                                if ("SLTextBox" == oLoopGroupControl.GetType().Name.ToString())
                                {
                                    if (((SLTextBox)oLoopGroupControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                    {
                                        ((SLTextBox)oLoopGroupControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                        ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(BaseForm_Leave);
                                        ((SLTextBox)oLoopGroupControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                    }
                                    if (((SLTextBox)oLoopGroupControl).IsRequired)
                                    {
                                        ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(RequredField_Leave);
                                    }
                                }
                                else if ("SLComboBox" == oLoopGroupControl.GetType().Name.ToString())
                                {
                                    BindLOVComboBox((SLComboBox)oLoopGroupControl);
                                }
                                else if ("LookupButton" == oLoopGroupControl.GetType().Name.ToString())
                                {
                                    oLoopGroupControl.Click += new EventHandler(HandleLookup_Click);
                                }
                                else if ("SLPanelSimple" == oLoopGroupControl.GetType().Name.ToString() || "SLPanelSimpleList" == oLoopGroupControl.GetType().Name.ToString())
                                {
                                    foreach (Control oLoopChildPanelControl in oLoopGroupControl.Controls)
                                    {
                                        switch (oLoopChildPanelControl.GetType().Name.ToString())
                                        {
                                            case "SLComboBox":
                                                BindLOVComboBox((SLComboBox)oLoopChildPanelControl);
                                                break;
                                            case "LookupButton":
                                                oLoopChildPanelControl.Click += new EventHandler(HandleLookup_Click);
                                                break;
                                            case "SLTextBox":
                                                if (((SLTextBox)oLoopChildPanelControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                                {
                                                    ((SLTextBox)oLoopChildPanelControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                                    ((SLTextBox)oLoopChildPanelControl).Leave += new EventHandler(BaseForm_Leave);
                                                    ((SLTextBox)oLoopChildPanelControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                                }
                                                if (((SLTextBox)oLoopChildPanelControl).IsRequired)
                                                {
                                                    ((SLTextBox)oLoopChildPanelControl).Leave += new EventHandler(RequredField_Leave);
                                                }
                                                break;
                                            case "GroupBox":
                                                foreach (Control oInnerLoopGroupControl in oLoopChildPanelControl.Controls)
                                                {
                                                    if ("SLTextBox" == oInnerLoopGroupControl.GetType().Name.ToString())
                                                    {
                                                        if (((SLTextBox)oInnerLoopGroupControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                                        {
                                                            ((SLTextBox)oInnerLoopGroupControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                                            ((SLTextBox)oInnerLoopGroupControl).Leave += new EventHandler(BaseForm_Leave);
                                                            ((SLTextBox)oInnerLoopGroupControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                                        }
                                                        if (((SLTextBox)oInnerLoopGroupControl).IsRequired)
                                                        {
                                                            ((SLTextBox)oInnerLoopGroupControl).Leave += new EventHandler(RequredField_Leave);
                                                        }
                                                    }
                                                    else if ("SLComboBox" == oInnerLoopGroupControl.GetType().Name.ToString())
                                                    {
                                                        BindLOVComboBox((SLComboBox)oInnerLoopGroupControl);
                                                    }
                                                    else if ("LookupButton" == oInnerLoopGroupControl.GetType().Name.ToString())
                                                    {
                                                        oInnerLoopGroupControl.Click += new EventHandler(HandleLookup_Click);
                                                    }
                                                    else if ("SLPanelSimple" == oLoopGroupControl.GetType().Name.ToString() || "SLPanelSimpleList" == oLoopGroupControl.GetType().Name.ToString())
                                                    {
                                                        foreach (Control oInnerLoopChildPanelControl in oInnerLoopGroupControl.Controls)
                                                        {
                                                            switch (oInnerLoopChildPanelControl.GetType().Name.ToString())
                                                            {
                                                                case "SLComboBox":
                                                                    BindLOVComboBox((SLComboBox)oInnerLoopChildPanelControl);
                                                                    break;
                                                                case "LookupButton":
                                                                    oInnerLoopChildPanelControl.Click += new EventHandler(HandleLookup_Click);
                                                                    break;
                                                                case "SLTextBox":
                                                                    if (((SLTextBox)oInnerLoopChildPanelControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                                                    {
                                                                        ((SLTextBox)oInnerLoopChildPanelControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                                                        ((SLTextBox)oInnerLoopChildPanelControl).Leave += new EventHandler(BaseForm_Leave);
                                                                        ((SLTextBox)oInnerLoopChildPanelControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                                                    }
                                                                    if (((SLTextBox)oInnerLoopChildPanelControl).IsRequired)
                                                                    {
                                                                        ((SLTextBox)oInnerLoopChildPanelControl).Leave += new EventHandler(RequredField_Leave);
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "GroupBox":
                        foreach (Control oLoopGroupControl in oLoopControl.Controls)
                        {
                            if ("SLTextBox" == oLoopGroupControl.GetType().Name.ToString())
                            {
                                if (((SLTextBox)oLoopGroupControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                {
                                    ((SLTextBox)oLoopGroupControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                    ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(BaseForm_Leave);
                                    ((SLTextBox)oLoopGroupControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                }
                                if (((SLTextBox)oLoopGroupControl).IsRequired)
                                {
                                    ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(RequredField_Leave);
                                }
                            }
                            else if ("SLComboBox" == oLoopGroupControl.GetType().Name.ToString())
                            {
                                BindLOVComboBox((SLComboBox)oLoopGroupControl);
                            }
                            else if ("LookupButton" == oLoopGroupControl.GetType().Name.ToString())
                            {
                                oLoopGroupControl.Click += new EventHandler(HandleLookup_Click);
                            }
                            else if ("SLPanelSimple" == oLoopGroupControl.GetType().Name.ToString() || "SLPanelSimpleList" == oLoopGroupControl.GetType().Name.ToString())
                            {
                                foreach (Control oLoopChildPanelControl in oLoopGroupControl.Controls)
                                {
                                    switch (oLoopChildPanelControl.GetType().Name.ToString())
                                    {
                                        case "SLComboBox":
                                            BindLOVComboBox((SLComboBox)oLoopChildPanelControl);
                                            break;
                                        case "LookupButton":
                                            oLoopChildPanelControl.Click += new EventHandler(HandleLookup_Click);
                                            break;
                                        case "SLTextBox":
                                            if (((SLTextBox)oLoopChildPanelControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                            {
                                                ((SLTextBox)oLoopChildPanelControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                                ((SLTextBox)oLoopChildPanelControl).Leave += new EventHandler(BaseForm_Leave);
                                                ((SLTextBox)oLoopChildPanelControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                            }
                                            if (((SLTextBox)oLoopChildPanelControl).IsRequired)
                                            {
                                                ((SLTextBox)oLoopChildPanelControl).Leave += new EventHandler(RequredField_Leave);
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                        break;
                    case "SLComboBox":
                        BindLOVComboBox((SLComboBox)oLoopControl);
                        break;
                    case "SLTextBox":
                        if (((SLTextBox)oLoopControl).AssociatedLookUpName.Equals(string.Empty) == false)
                        {
                            ((SLTextBox)oLoopControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                            ((SLTextBox)oLoopControl).Leave += new EventHandler(BaseForm_Leave);
                            ((SLTextBox)oLoopControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                        }
                        if (((SLTextBox)oLoopControl).IsRequired)
                        {
                            ((SLTextBox)oLoopControl).Leave += new EventHandler(RequredField_Leave);
                        }
                        break;
                    case "LookupButton":
                        oLoopControl.Click += new EventHandler(HandleLookup_Click);
                        break;
                    case "SLPanelSimpleList":
                        foreach (Control oLoopPanelControl in oLoopControl.Controls)
                        {
                            switch (oLoopPanelControl.GetType().Name.ToString())
                            {
                                case "GroupBox":
                                    foreach (Control oLoopGroupControl in oLoopPanelControl.Controls)
                                    {
                                        if ("SLTextBox" == oLoopGroupControl.GetType().Name.ToString())
                                        {
                                            if (((SLTextBox)oLoopGroupControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                            {
                                                ((SLTextBox)oLoopGroupControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                                ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(BaseForm_Leave);
                                                ((SLTextBox)oLoopGroupControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                            }
                                            if (((SLTextBox)oLoopGroupControl).IsRequired)
                                            {
                                                ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(RequredField_Leave);
                                            }
                                        }
                                        else if ("SLComboBox" == oLoopGroupControl.GetType().Name.ToString())
                                        {
                                            BindLOVComboBox((SLComboBox)oLoopGroupControl);
                                        }
                                        else if ("LookupButton" == oLoopGroupControl.GetType().Name.ToString())
                                        {
                                            oLoopGroupControl.Click += new EventHandler(HandleLookup_Click);
                                        }
                                    }
                                    break;
                                case "SLComboBox":
                                    BindLOVComboBox((SLComboBox)oLoopPanelControl);
                                    break;
                                case "LookupButton":
                                    oLoopPanelControl.Click += new EventHandler(HandleLookup_Click);
                                    break;
                                case "SLTextBox":
                                    if (((SLTextBox)oLoopPanelControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                    {
                                        ((SLTextBox)oLoopPanelControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                        ((SLTextBox)oLoopPanelControl).Leave += new EventHandler(BaseForm_Leave);
                                        ((SLTextBox)oLoopPanelControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                    }
                                    if (((SLTextBox)oLoopPanelControl).IsRequired)
                                    {
                                        ((SLTextBox)oLoopPanelControl).Leave += new EventHandler(RequredField_Leave);
                                    }
                                    break;
                            }
                        }
                        break;
                    case "SLPanelSimple":
                        foreach (Control oLoopPanelControl in oLoopControl.Controls)
                        {
                            switch (oLoopPanelControl.GetType().Name.ToString())
                            {
                                case "GroupBox":
                                    foreach (Control oLoopGroupControl in oLoopPanelControl.Controls)
                                    {
                                        if ("SLTextBox" == oLoopGroupControl.GetType().Name.ToString())
                                        {
                                            if (((SLTextBox)oLoopGroupControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                            {
                                                ((SLTextBox)oLoopGroupControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                                ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(BaseForm_Leave);
                                                ((SLTextBox)oLoopGroupControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                            }
                                            if (((SLTextBox)oLoopGroupControl).IsRequired)
                                            {
                                                ((SLTextBox)oLoopGroupControl).Leave += new EventHandler(RequredField_Leave);
                                            }
                                        }
                                        else if ("SLComboBox" == oLoopGroupControl.GetType().Name.ToString())
                                        {
                                            BindLOVComboBox((SLComboBox)oLoopGroupControl);
                                        }
                                        else if ("LookupButton" == oLoopGroupControl.GetType().Name.ToString())
                                        {
                                            oLoopGroupControl.Click += new EventHandler(HandleLookup_Click);
                                        }
                                    }
                                    break;
                                case "SLComboBox":
                                    BindLOVComboBox((SLComboBox)oLoopPanelControl);
                                    break;
                                case "LookupButton":
                                    oLoopPanelControl.Click += new EventHandler(HandleLookup_Click);
                                    break;
                                case "SLTextBox":
                                    if (((SLTextBox)oLoopPanelControl).AssociatedLookUpName.Equals(string.Empty) == false)
                                    {
                                        ((SLTextBox)oLoopPanelControl).KeyUp += new KeyEventHandler(BaseForm_KeyUp);
                                        ((SLTextBox)oLoopPanelControl).Leave += new EventHandler(BaseForm_Leave);
                                        ((SLTextBox)oLoopPanelControl).TextChanged += new EventHandler(BaseForm_TextChanged);
                                    }
                                    if (((SLTextBox)oLoopPanelControl).IsRequired)
                                    {
                                        ((SLTextBox)oLoopPanelControl).Leave += new EventHandler(RequredField_Leave);
                                    }
                                    break;
                            }
                        }
                        break;
                }

            }
        }

        /// <summary>
        /// Required Field On Close Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequredField_Leave(object sender, EventArgs e)
        {
            (sender as SLTextBox).SkipValidation = (this.tbtCancel.Pressed || this.tbtClose.Pressed || (sender as SLTextBox).SkipValidation);
        }

        /// <summary>
        /// Text Changed handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void BaseForm_TextChanged(object sender, EventArgs e)
        {
            Control[] ctrl = this.Controls.Find(((SLTextBox)sender).AssociatedLookUpName.Trim(), true);
            if (ctrl.Length != 0 && ctrl[0] != null)
            {
                if (!String.IsNullOrEmpty(((LookupButton)ctrl[0]).DependentLovControls))
                {
                    foreach (String controlId in ((LookupButton)ctrl[0]).GetDependentLovFields())
                    {
                        (Utilities.FindControlRecursive(((LookupButton)ctrl[0]).Parent, controlId) as TextBox).Text = String.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Bind Lov Combo
        /// </summary>
        /// <param name="poSLCombo"></param>
        private void BindLOVComboBox(SLComboBox poSLCombo)
        {
            if (poSLCombo.ComboBehaviour == eComboBehaviour.LOVKeyCode)
            {
                m_oEntityClass = new BusinessEntity();
                m_oEntityClass.BindLOVCombo(poSLCombo);
            }
            else if (poSLCombo.ComboBehaviour == eComboBehaviour.LOVEntityCode)
            {
                m_oEntityClass = RuntimeClassLoader.GetBusinessEntity(poSLCombo.BusinessEntity);
                m_oEntityClass.BindLOVCombo(poSLCombo);
            }
        }

        /// <summary>
        /// Validate LOV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_Leave(object sender, EventArgs e)
        {
            if ((this.tbtCancel.Pressed || this.tbtClose.Pressed))
                return;
            if ((sender.GetType().Name == "SLTextBox") && (((SLTextBox)sender).Text.Length > 0))
            {
                Control[] ctrl = this.Controls.Find(((SLTextBox)sender).AssociatedLookUpName.Trim(), true);

                if (ctrl.Length != 0 && ctrl[0] != null)
                {
                    if (!SkipLovValidationOnSave)
                    {
                        #region LOV Exists
                        /*LOV Exists*/
                        BusinessEntity entity = RuntimeClassLoader.GetBusinessEntity("iCORE.Common.BusinessEntity");
                        if (!String.IsNullOrEmpty(((LookupButton)ctrl[0]).ConditionalFields))
                        {
                            StringBuilder searchValue = new StringBuilder();
                            TextBox searchControl;
                            foreach (String controlId in ((LookupButton)ctrl[0]).GetConditionalFields())
                            {
                                searchControl = (Utilities.FindControlRecursive(((LookupButton)ctrl[0]).Parent, controlId) as TextBox);
                                searchValue.Append(searchControl.Text).Append("|");
                            }
                            if (searchValue.Length > 0)
                                searchValue.Remove(searchValue.Length - 1, 1);
                            entity.SearchFilter = searchValue.ToString();


                        }

                        if (((LookupButton)ctrl[0]).SkipValidationOnLeave)
                            return;
                        if (entityDataManager == null)
                            entityDataManager = new CommonDataManager();

                        rslt = entityDataManager.GetAll(((LookupButton)ctrl[0]).SPName.Trim(), ((LookupButton)ctrl[0]).ActionLOVExists, ((SLTextBox)sender).Text, ((SLTextBox)sender).DataFieldMapping, entity.SearchFilter);

                        if (rslt.isSuccessful)
                        {
                            if (rslt.dstResult.Tables.Count > 0)
                            {
                                if (rslt.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    DataRow drw = rslt.dstResult.Tables[0].Rows[0];
                                    this.SetPanelControlWithListItem(ctrl[0].Parent.Controls, drw);
                                    if (((LookupButton)ctrl[0]).LoadDependentEntities && !((ctrl[0].Parent as SLPanel).DisableDependentLoad))
                                    {
                                        this.LoadEntityAndDependentPanels(drw);
                                        this.operationMode = Mode.Edit;
                                    }
                                    LOVRecordSelected(drw, ((LookupButton)ctrl[0]).ActionLOVExists);
                                }
                                else
                                    BindLOVShortCut(((LookupButton)ctrl[0]).SPName.Trim(), (SLTextBox)sender);
                            }
                            else
                                BindLOVShortCut(((LookupButton)ctrl[0]).SPName.Trim(), (SLTextBox)sender);
                        }
                        else
                        {
                            BindLOVShortCut(((LookupButton)ctrl[0]).SPName.Trim(), (SLTextBox)sender);
                        }
                        #endregion
                    }

                }

            }
            else if ((sender.GetType().Name == "SLTextBox"))
            {
                Control[] ctrl = ((SLTextBox)sender).Parent.Controls.Find(((SLTextBox)sender).AssociatedLookUpName.Trim(), true);
                if (ctrl.Length != 0 && ctrl[0] != null)
                    (sender as SLTextBox).SkipValidation = ctrl[0].Focused;
            }
        }

        /// <summary>
        /// LOV F9 Key Implementation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F9)
            {
                Control[] ctrl = this.Controls.Find(((SLTextBox)sender).AssociatedLookUpName.Trim(), true);
                if (ctrl.Length != 0 && ctrl[0] != null)
                    BindLOVShortCut(((LookupButton)ctrl[0]).SPName.Trim(), (SLTextBox)sender);
                //BindLOVShortCut(((LookupButton)this.Controls.Find(((SLTextBox)sender).AssociatedLookUpName.Trim(), true)[0]).SPName);
            }
        }

        /// <summary>
        /// LOV Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleLookup_Click(object sender, EventArgs e)
        {
            this.BindLOVLookupButton((LookupButton)sender);
        }

        /// <summary>
        /// LOV Popup
        /// </summary>
        /// <param name="pstrSPName"></param>
        /// <param name="poSLTextBox"></param>
        protected virtual void BindLOVShortCut(string pstrSPName, SLTextBox poSLTextBox)
        {
            try
            {
                LookupButton lpButton = (poSLTextBox.Parent.Controls[poSLTextBox.AssociatedLookUpName] as LookupButton);
                Cmn_ListOfRecords sectionList = new Cmn_ListOfRecords(pstrSPName, lpButton.ActionType);

                BusinessEntity entity = RuntimeClassLoader.GetBusinessEntity("iCORE.Common.BusinessEntity");
                if (!String.IsNullOrEmpty(lpButton.ConditionalFields))
                {
                    StringBuilder searchValue = new StringBuilder();
                    TextBox searchControl;
                    foreach (String controlId in lpButton.GetConditionalFields())
                    {
                        searchControl = (Utilities.FindControlRecursive(lpButton.Parent, controlId) as TextBox);
                        searchValue.Append(searchControl.Text).Append("|");
                    }
                    if (searchValue.Length > 0)
                        searchValue.Remove(searchValue.Length - 1, 1);
                    entity.SearchFilter = searchValue.ToString();
                }
                sectionList.ColumnToHide = lpButton.HiddenColumns;
                sectionList.Entity = entity;
                sectionList.Id = this.m_intPKID;
                sectionList.Text = ((String.IsNullOrEmpty(lpButton.LookUpTitle)) ? this.Text + " List" : lpButton.LookUpTitle);
                sectionList.ListType = ListType.SetupList;
                sectionList.SearchText = poSLTextBox.Text.Trim();
                sectionList.SearchColumn = poSLTextBox.DataFieldMapping.Trim();
                this.BeforeLOVShowing(sectionList, lpButton.ActionType);
                if (sectionList.ShowDialog() == DialogResult.OK)
                {
                    DataRow drw = sectionList.SelectedRecord;
                    if (drw != null)
                    {
                        this.SetPanelControlWithListItem(poSLTextBox.Parent.Controls, drw);
                        if (lpButton.LoadDependentEntities)
                        {
                            this.LoadEntityAndDependentPanels(drw);
                            this.operationMode = Mode.Edit;
                            poSLTextBox.Focus();
                            poSLTextBox.SelectAll();
                        }
                    }
                    this.LOVRecordSelected(drw, lpButton.ActionType);
                }
                else if (sectionList.DialogResult != DialogResult.OK)
                {
                    base.ShowErrorMessage("Error", "Invalid value for field");
                    poSLTextBox.Focus();
                    poSLTextBox.SelectAll();
                    this.m_IsValidPage = false;
                }
            }
            catch (Exception ex)
            {
                this.LogException("BaseForm", "ShowList", ex);
            }
        }

        /// <summary>
        /// Lov Lookup Button Implementation
        /// </summary>
        /// <param name="poCprlLookupButton"></param>
        protected virtual void BindLOVLookupButton(LookupButton poCprlLookupButton)
        {
            try
            {
                Cmn_ListOfRecords sectionList = new Cmn_ListOfRecords(poCprlLookupButton.SPName, poCprlLookupButton.ActionType);

                BusinessEntity entity = RuntimeClassLoader.GetBusinessEntity("iCORE.Common.BusinessEntity");
                if (!String.IsNullOrEmpty(poCprlLookupButton.ConditionalFields))
                {
                    StringBuilder searchValue = new StringBuilder();
                    Control searchControl;
                    foreach (String controlId in poCprlLookupButton.GetConditionalFields())
                    {
                        searchControl = Utilities.FindControlRecursive(poCprlLookupButton.Parent, controlId);
                        if (searchControl is SLTextBox)
                            searchValue.Append(((SLTextBox)searchControl).Text).Append("|");
                        else if (searchControl is SLDatePicker)
                            searchValue.Append(((SLDatePicker)searchControl).Value == null ? "" : ((SLDatePicker)searchControl).Value.ToString()).Append("|");
                    }
                    if (searchValue.Length > 0)
                        searchValue.Remove(searchValue.Length - 1, 1);
                    entity.SearchFilter = searchValue.ToString();
                }
                sectionList.ColumnToHide = poCprlLookupButton.HiddenColumns;
                sectionList.Entity = entity;
                sectionList.Id = this.m_intPKID;
                sectionList.Text = ((String.IsNullOrEmpty(poCprlLookupButton.LookUpTitle)) ? this.Text + " List" : poCprlLookupButton.LookUpTitle);
                sectionList.ListType = ListType.SetupList;
                this.BeforeLOVShowing(sectionList, poCprlLookupButton.ActionType);
                if (sectionList.ShowDialog() == DialogResult.OK)
                {
                    DataRow drw = sectionList.SelectedRecord;
                    if (drw != null)
                    {
                        this.SetPanelControlWithListItem(poCprlLookupButton.Parent.Controls, drw);
                        if (poCprlLookupButton.LoadDependentEntities)
                        {
                            this.LoadEntityAndDependentPanels(drw);
                            this.operationMode = Mode.Edit;
                        }
                    }
                    if (this.IsDisposed)
                        return;
                    Control.ControlCollection ctrlCol = poCprlLookupButton.Parent.Controls;
                    foreach (Control oLoopControl in ctrlCol)
                    {
                        if (oLoopControl.GetType().Name.ToString() == "SLTextBox")
                        {
                            if (((SLTextBox)oLoopControl).AssociatedLookUpName.Equals(poCprlLookupButton.Name))
                            {
                                ((SLTextBox)oLoopControl).Focus();
                                ((SLTextBox)oLoopControl).SelectAll();
                            }
                        }
                    }
                    this.LOVRecordSelected(drw, poCprlLookupButton.ActionType);
                    //added to set focus on associated textbox.
                }
                else
                {
                    if (this.IsDisposed)
                        return;
                    Control.ControlCollection ctrlCol = poCprlLookupButton.Parent.Controls;
                    foreach (Control oLoopControl in ctrlCol)
                    {
                        if (oLoopControl.GetType().Name.ToString() == "SLTextBox")
                        {
                            if (((SLTextBox)oLoopControl).AssociatedLookUpName.Equals(poCprlLookupButton.Name))
                            {
                                ((SLTextBox)oLoopControl).Focus();
                                ((SLTextBox)oLoopControl).SelectAll();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogException(this.GetType().Name, "ShowList", ex);
            }
        }

        /// <summary>
        /// Set,Reset or ARegister handlers
        /// </summary>
        /// <param name="oCtrls"></param>
        /// <param name="pblnIsSetter"></param>
        /// <param name="pblnResetter"></param>
        /// <param name="pblnRegisterHandler"></param>
        protected void IterateFormControls(Control.ControlCollection oCtrls, bool pblnIsSetter, bool pblnResetter, bool pblnRegisterHandler)
        {
            this.m_IsValidPage = true;
            foreach (Control oLoopControl in oCtrls)
            {

                if (oLoopControl is SLPanelSimple || oLoopControl is SLPanelTabular || oLoopControl is GroupBox)
                {
                    IterateFormControls(oLoopControl.Controls, pblnIsSetter, pblnResetter, pblnRegisterHandler);
                }
                else if (oLoopControl is SLPanelSimpleList)
                {
                    IterateFormControls(oLoopControl.Controls, pblnIsSetter, pblnResetter, pblnRegisterHandler);
                    ((SLPanelSimpleList)oLoopControl).GetList((SLPanel)oLoopControl, true);
                }
                else if (oLoopControl is LookupButton)
                {
                    if (pblnRegisterHandler)
                        oLoopControl.GotFocus += new EventHandler(SLControl_GotFocus);
                }
                else if (oLoopControl is SLTextBox)
                {
                    if (((SLTextBox)oLoopControl).Parent is SLPanelSimple && (((SLTextBox)oLoopControl).Parent as SLPanelSimple).PanelBlockType == BlockType.DataBlock)
                    {
                        if (pblnRegisterHandler && ((SLTextBox)oLoopControl).GetRecordsOnUpDownKeys)
                        {
                            oLoopControl.KeyUp += new KeyEventHandler(SlTextBox_KeyUp);
                        }
                        //next work handler.
                    }
                    if (pblnIsSetter)
                    {
                        if (GetFormItemValue(((SLTextBox)oLoopControl).DataFieldMapping) != null)  //added 
                            ((SLTextBox)oLoopControl).Text = GetFormItemValue(((SLTextBox)oLoopControl).DataFieldMapping).ToString();
                    }
                    else if (pblnResetter)
                        ((SLTextBox)oLoopControl).Text = string.Empty;
                    else if (pblnRegisterHandler)
                        oLoopControl.GotFocus += new EventHandler(SLControl_GotFocus);
                    else
                    {
                        if (((SLTextBox)oLoopControl).AssociatedLookUpName.Equals(string.Empty) == false)
                        {
                            BaseForm_Leave(oLoopControl, null);
                        }
                        if (((SLTextBox)oLoopControl).IsRequired)
                        {
                            if (InputValidator.IsNotEmpty("", this.errorProvider1, (SLTextBox)oLoopControl))
                            {
                                SetEntityProperties(((SLTextBox)oLoopControl).DataFieldMapping, oLoopControl.Text);

                            }
                            else
                            {
                                this.m_IsValidPage = false;
                            }
                        }
                        else
                        {
                            SetEntityProperties(((SLTextBox)oLoopControl).DataFieldMapping, oLoopControl.Text);
                        }

                    }

                }
                else if (oLoopControl is SLComboBox)
                {
                    if (pblnIsSetter)
                        ((SLComboBox)oLoopControl).SelectedValue = GetFormItemValue(((SLComboBox)oLoopControl).DataFieldMapping);
                    else if (pblnResetter)
                        ((SLComboBox)oLoopControl).SelectedIndex = 0;
                    else if (pblnRegisterHandler)
                        oLoopControl.GotFocus += new EventHandler(SLControl_GotFocus);
                    else
                        SetEntityProperties(((SLComboBox)oLoopControl).DataFieldMapping, ((SLComboBox)oLoopControl).SelectedValue == null ? DBNull.Value.ToString() : ((SLComboBox)oLoopControl).SelectedValue.ToString());
                }
                else if (oLoopControl is SLCheckBox)
                {
                    if (pblnIsSetter)
                        ((SLCheckBox)oLoopControl).Checked = (GetFormItemValue(((SLCheckBox)oLoopControl).DataFieldMapping).ToString() == "1" || GetFormItemValue(((SLCheckBox)oLoopControl).DataFieldMapping).ToString().ToUpper() == "Y" ? true : false);
                    else if (pblnResetter)
                        ((SLCheckBox)oLoopControl).Checked = false;
                    else if (pblnRegisterHandler)
                        oLoopControl.GotFocus += new EventHandler(SLControl_GotFocus);
                    else
                    {
                        if (((SLCheckBox)oLoopControl).Checked)
                            SetEntityProperties(((SLCheckBox)oLoopControl).DataFieldMapping, "Y");
                        else
                            SetEntityProperties(((SLCheckBox)oLoopControl).DataFieldMapping, "N");
                    }
                }
                else if (oLoopControl is SLRadioButton)
                {
                    if (pblnIsSetter)
                        ((SLRadioButton)oLoopControl).Checked = (GetFormItemValue(((SLRadioButton)oLoopControl).DataFieldMapping).ToString() == "1" ? true : false);
                    else if (pblnResetter)
                        ((SLRadioButton)oLoopControl).Checked = false;
                    else if (pblnRegisterHandler)
                        oLoopControl.GotFocus += new EventHandler(SLControl_GotFocus);
                    else
                        SetEntityProperties(((SLRadioButton)oLoopControl).DataFieldMapping, ((SLRadioButton)oLoopControl).Checked ? "1" : "0");
                }
                else if (oLoopControl is SLDataGridView)
                {
                    if (pblnRegisterHandler)
                        oLoopControl.Click += new EventHandler(SLControl_GotFocus);

                    if (oLoopControl.Parent is SLPanelTabular)
                    {
                        // if ((oLoopControl.Parent as SLPanelTabular).MasterPanel == null)  // Comments by NAJEEB ON 16 OCT -- Skip this line because it is causing dataset to be null on Grid Record Insert
                        ((SLDataGridView)oLoopControl).PopulateGrid((SLPanel)oLoopControl.Parent, true);
                    }
                    else if (oLoopControl.Parent is GroupBox)
                    {
                        ((SLDataGridView)oLoopControl).PopulateGrid((SLPanel)oLoopControl.Parent.Parent, true);
                    }
                }
                else if (oLoopControl is SLDatePicker)
                {
                    if (pblnIsSetter)
                        ((SLDatePicker)oLoopControl).Value = Convert.ToDateTime(GetFormItemValue((((SLDatePicker)oLoopControl).DataFieldMapping).ToString()));
                    else if (pblnResetter)
                        ((SLDatePicker)oLoopControl).Value = DateTime.Now;
                    else if (pblnRegisterHandler)
                        oLoopControl.GotFocus += new EventHandler(SLControl_GotFocus);
                    else
                    {
                        if (((SLDatePicker)oLoopControl).IsRequired)
                        {
                            if (InputValidator.IsNotEmpty("", this.errorProvider1, (SLDatePicker)oLoopControl))
                            {
                                SetEntityProperties(((SLDatePicker)oLoopControl).DataFieldMapping, ((SLDatePicker)oLoopControl).Value == null ? "" : ((SLDatePicker)oLoopControl).Value.ToString());
                            }
                            else
                            {
                                this.m_IsValidPage = false;
                            }
                        }
                        else
                            SetEntityProperties(((SLDatePicker)oLoopControl).DataFieldMapping, ((SLDatePicker)oLoopControl).Value == null ? "" : ((SLDatePicker)oLoopControl).Value.ToString());
                    }
                }
            }

            if (m_htCustomEntityProperties != null && m_htCustomEntityProperties.Count > 0)
            {
                SetCustomEntityProperties();
            }
        }

        /// <summary>
        /// Set,Reset or ARegister handlers
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pblnIsSetter"></param>
        /// <param name="pblnResetter"></param>
        /// <param name="pblnRegisterHandler"></param>
        public void IterateConcurrentPanels(SLPanel oPanel, bool pblnIsSetter, bool pblnResetter, bool pblnRegisterHandler)
        {
            if (oPanel.ConcurrentPanels != null && oPanel.ConcurrentPanels.Count > 0)
            {
                foreach (SLPanel parallelPanel in oPanel.ConcurrentPanels)
                {
                    IterateFormControls(parallelPanel.Controls, pblnIsSetter, pblnResetter, pblnRegisterHandler);
                }
            }
        }

        /// <summary>
        /// Set Control Values
        /// </summary>
        /// <param name="poDR"></param>
        /// <param name="oCtrls"></param>
        protected void SetFormControlWithListItem(DataRow poDR, Control.ControlCollection oCtrls)
        {
            if (poDR.Table.Columns.Contains(PKID) && Int32.TryParse(poDR[PKID].ToString(), out m_intPKID))
                this.m_intPKID = Convert.ToInt32(poDR[PKID]);

            foreach (Control oLoopControl in oCtrls)
            {
                switch (oLoopControl.GetType().Name.ToString())
                {
                    case "GroupBox":
                        SetFormControlWithListItem(poDR, oLoopControl.Controls);
                        break;
                    case "SLTextBox":
                        if (poDR.Table.Columns.Contains(((SLTextBox)oLoopControl).DataFieldMapping))
                            ((SLTextBox)oLoopControl).Text = poDR[((SLTextBox)oLoopControl).DataFieldMapping].ToString();
                        break;
                    case "SLComboBox":
                        if (poDR.Table.Columns.Contains(((SLComboBox)oLoopControl).DataFieldMapping))
                            ((SLComboBox)oLoopControl).SelectedValue = poDR[((SLComboBox)oLoopControl).DataFieldMapping].ToString();
                        break;
                    case "SLCheckBox":
                        if (poDR.Table.Columns.Contains(((SLCheckBox)oLoopControl).DataFieldMapping))
                            ((SLCheckBox)oLoopControl).Checked = (poDR[((SLCheckBox)oLoopControl).DataFieldMapping].ToString() == "1" || poDR[((SLCheckBox)oLoopControl).DataFieldMapping].ToString().ToUpper() == "Y" ? true : false);
                        break;
                    case "SLDatePicker":
                        if (poDR.Table.Columns.Contains(((SLDatePicker)oLoopControl).DataFieldMapping))
                            ((SLDatePicker)oLoopControl).Value = Convert.ToDateTime(poDR[((SLDatePicker)oLoopControl).DataFieldMapping].ToString());
                        break;
                }
            }

            if (m_htCustomEntityProperties != null && m_htCustomEntityProperties.Count > 0)
            {
                SetCustomEntityProperties();
            }
        }

        /// <summary>
        /// Set Custom Properties
        /// </summary>
        /// <param name="pstrName"></param>
        /// <param name="pstrValue"></param>
        protected void SetCustomEntityProperties(string pstrName, string pstrValue)
        {
            if (null == m_htCustomEntityProperties)
                m_htCustomEntityProperties = new Hashtable();

            if (!m_htCustomEntityProperties.ContainsKey(pstrName))
                m_htCustomEntityProperties.Add(pstrName, pstrValue);
            else
                m_htCustomEntityProperties[pstrName] = pstrValue;
        }

        /// <summary>
        /// Set Entity Property
        /// </summary>
        /// <param name="pstrName"></param>
        /// <param name="pstrValue"></param>
        protected void SetEntityProperties(string pstrName, string pstrValue)
        {
            //PropertyInfo newProperty = m_oEntityClass.GetType().GetProperty(pstrName, BindingFlags.IgnoreCase);
            PropertyInfo newProperty = m_oEntityClass.GetType().GetProperty(pstrName);
            string TypeName;
            if (newProperty != null && newProperty.CanWrite)
            {
                TypeName = newProperty.PropertyType.Name;
                if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    TypeName = newProperty.PropertyType.GetGenericArguments()[0].Name;
                }
                switch (TypeName)
                {
                    case "DateTime":
                        DateTime oDateTime;
                        if (pstrValue.Equals("") || Convert.ToDateTime(pstrValue) == DateTime.MinValue)
                            newProperty.SetValue(m_oEntityClass, null, null);
                        else if (DateTime.TryParse(pstrValue, out oDateTime))
                            newProperty.SetValue(m_oEntityClass, Convert.ToDateTime(pstrValue), null);
                        break;
                    case "Int32":
                        Int32 oInt32;
                        if (Int32.TryParse(pstrValue, out oInt32))
                            newProperty.SetValue(m_oEntityClass, Convert.ToInt32(pstrValue), null);
                        break;
                    case "Double":
                        Double ValueDoubleType;
                        if (Double.TryParse(pstrValue, out ValueDoubleType))
                            newProperty.SetValue(m_oEntityClass, Convert.ToDouble(pstrValue), null);
                        break;
                    case "Decimal":
                        Decimal ValueDecimalType;
                        if (Decimal.TryParse(pstrValue, out ValueDecimalType))
                            newProperty.SetValue(m_oEntityClass, Convert.ToDecimal(pstrValue), null);
                        else if (newProperty.PropertyType.IsGenericType && newProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            newProperty.SetValue(m_oEntityClass, null, null);
                        }
                        break;
                    default:
                        newProperty.SetValue(m_oEntityClass, pstrValue, null);
                        break;
                }
            }
        }

        /// <summary>
        /// Set Custom Properties
        /// </summary>
        private void SetCustomEntityProperties()
        {
            foreach (DictionaryEntry oDicEntry in m_htCustomEntityProperties)
            {
                SetEntityProperties(oDicEntry.Key.ToString(), oDicEntry.Value.ToString());
            }
        }

        /// <summary>
        /// Get Value from Entity
        /// </summary>
        /// <param name="pstrName"></param>
        /// <returns></returns>
        private object GetFormItemValue(string pstrName)
        {
            //PropertyInfo newProperty = m_oEntityClass.GetType().GetProperty(pstrName, BindingFlags.IgnoreCase);
            PropertyInfo newProperty = m_oEntityClass.GetType().GetProperty(pstrName);

            if (newProperty != null && newProperty.CanRead)
            {
                return newProperty.GetValue(m_oEntityClass, null);
            }

            return string.Empty;
        }

        /// <summary>
        /// Load Dependent Panels
        /// </summary>
        /// <param name="row"></param>
        private void LoadEntityAndDependentPanels(DataRow row)
        {
            SLPanel oSLPanel = (SLPanel)Utilities.FindControlRecursive(this, CurrentPanelBlock);
            oSLPanel.SetEntityObject(row);
            oSLPanel.LoadConcurrentPanels(row);
            oSLPanel.LoadDependentPanels();
        }

        /// <summary>
        /// End Edit of all grids
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        protected virtual Control AllGridsEndEdit(Control control)
        {
            if (control is SLDataGridView && (null != (control as SLDataGridView).CurrentRow) && !(control as SLDataGridView).CurrentRow.IsNewRow)
            {
                if ((control as SLDataGridView).CurrentRow.DataBoundItem != null)
                {
                    ((control as SLDataGridView).CurrentRow.DataBoundItem as DataRowView).EndEdit();
                }
                (control as SLDataGridView).EndEdit();
            }

            foreach (Control c in control.Controls)
            {
                Control tmp = AllGridsEndEdit(c);
                if (tmp != null)
                    return tmp;
            }
            return null;
        }

        /// <summary>
        /// Set to cancel Mode
        /// </summary>
        /// <param name="panels"></param>
        protected void FormCancelMode(List<iCORE.COMMON.SLCONTROLS.SLPanel> panels)
        {
            if (panels != null)
            {
                foreach (iCORE.COMMON.SLCONTROLS.SLPanel pnl in panels)
                {
                    this.FormCancelMode(pnl.Controls);
                }
            }
        }

        /// <summary>
        /// Set to edit mode
        /// </summary>
        /// <param name="panels"></param>
        protected void FormEditMode(List<iCORE.COMMON.SLCONTROLS.SLPanel> panels)
        {
            if (panels != null)
            {
                foreach (iCORE.COMMON.SLCONTROLS.SLPanel pnl in panels)
                {
                    base.FormEnableDisableMode(pnl.Controls, true);
                }
            }

        }

        /// <summary>
        /// Get Next/Previous record
        /// </summary>
        /// <param name="oPanel"></param>
        /// <param name="pstrActionType"></param>
        protected void GetNextOrPrevoiusRecord(SLPanel oPanel, string pstrActionType)
        {
            IDataManager dataManager = RuntimeClassLoader.GetDataManager(oPanel.DataManager);

            Result rslt = new Result();
            try
            {
                rslt = dataManager.GetAll(oPanel.CurrentBusinessEntity, oPanel.SPName, pstrActionType);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0] != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataRow drw = rslt.dstResult.Tables[0].Rows[0];
                        if (drw != null)
                        {
                            this.SetPanelControlWithListItem(oPanel.Controls, drw);
                            oPanel.LoadConcurrentPanels(drw);

                            this.FormListMode(oPanel.Controls);
                            oPanel.SetEntityObject(drw);
                            oPanel.LoadDependentPanels();
                        }

                    }

                    if (rslt.dstResult.Tables[0] != null && rslt.dstResult.Tables[0].Rows.Count == 0)
                        this.ShowInformationMessage("", ApplicationMessages.NO_RECORDS_FOUND_MESSAGE);
                }
                else
                    this.ShowInformationMessage("", rslt.exp.Message);
            }
            catch (Exception e)
            {
                this.ShowInformationMessage("", ApplicationMessages.EXCEPTION_MESSAGE);
                this.LogException("BaseForm.cs", "GetNextRecord", e);
            }
        }

        /// <summary>
        /// Author: Azam Farooq
        /// Creation date: 11th Feb 2011
        /// Description: Calculate Oracle Months between Maximum and Minimum dates
        /// </summary>
        /// <param name="MaxDate">Higher Range Date</param>
        /// <param name="MinDate">Lower Range Date</param>
        public int MonthsBetweenInOracle(DateTime MaxDate, DateTime MinDate)
        {
            Int32 dResult = 0;

            //Last day of Maximum Date
            DateTime FirstDayOfMonth_MaxDate = new DateTime(MaxDate.Year, MaxDate.Month, 1);
            DateTime LastDayOfMonth_MaxDate = FirstDayOfMonth_MaxDate.AddMonths(1).AddDays(-1);

            //Last day of Minimum Date
            DateTime FirstDayOfMonth_MinDate = new DateTime(MinDate.Year, MinDate.Month, 1);
            DateTime LastDayOfMonth_MinDate = FirstDayOfMonth_MinDate.AddMonths(1).AddDays(-1);

            //Condition 1: Day of Maximum date != Day of Minimum date
            //Condition 2: "Last Day of Maximum date != Day of Maximum date" And "Last Day of Minimum date != Day of Minimum date"
            if ((MaxDate.Day == MinDate.Day) || (LastDayOfMonth_MaxDate.Day == MaxDate.Day && LastDayOfMonth_MinDate.Day == MinDate.Day))
            {
                return (12 * (MaxDate.Year - MinDate.Year) + MaxDate.Month - MinDate.Month);
            }
            else
            {
                int A = ((12 * (MaxDate.Year - MinDate.Year) + MaxDate.Month - MinDate.Month) - 1) * 31;
                A = A + (MaxDate.Day - 1) + (32 - MinDate.Day);

                Double D = A / 31.00;
                dResult = Convert.ToInt32(Math.Round(D, 0));
            }

            return dResult;
        }

        /// <summary>
        /// Author: Umair Mufti
        /// Creation date: 22th March 2011
        /// Description: Add Number of Months in the provided Date.
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="NO_OF_MONTHS"></param>
        /// <returns></returns>
        public DateTime AddMonth(DateTime Date, int NO_OF_MONTHS)
        {
            DateTime CALCULATED_DATE = new DateTime();
            int OrignalMONTH;
            int OrignalYEAR;

            int CalculatedMONTH;
            int CalculatedYEAR;

            int LastDayOfMonth_Orignal;
            int LastDayOfMonth_Calculated;

            CALCULATED_DATE = Date.AddMonths(1);
            CalculatedMONTH = CALCULATED_DATE.Month;
            CalculatedYEAR = CALCULATED_DATE.Year;

            OrignalMONTH = Date.Month;
            OrignalYEAR = Date.Year;

            LastDayOfMonth_Orignal = DateTime.DaysInMonth(OrignalYEAR, OrignalMONTH);
            LastDayOfMonth_Calculated = DateTime.DaysInMonth(CalculatedYEAR, CalculatedMONTH);


            if (Date.Day == LastDayOfMonth_Orignal)
            {
                CALCULATED_DATE = new DateTime(CalculatedYEAR, CalculatedMONTH, LastDayOfMonth_Calculated);
            }
            return CALCULATED_DATE;
        }

        /// <summary>
        /// This function converts a "Pascal Case" case string into a separate words.
        /// </summary>
        /// <param name="orginalString">Original string that is to be converted.</param>
        /// <returns>Result string with separated words</returns>
        private String SeparatePascalCaseWord(String originalString)
        {
            String oString = String.Empty;
            int iLength = originalString.Length - 1;
            int p = 0;
            Char iChar;
            bool upperFirstOccurrenceFlag = false;
            bool upperSecondOccurrenceFlag = false;
            bool underScoreOccurrenceFlag = false;
            bool firstCharacterFlag = true;

            //originalString = originalString.ToLower();
            while (p <= iLength)
            {
                //strip each character out of the string one by one
                iChar = originalString.Substring(p, 1).ToCharArray()[0];


                // If a caps character has occured 
                if (iChar.Equals(iChar.ToString().ToUpper().ToCharArray()[0]))
                {
                    upperFirstOccurrenceFlag = true;
                }
                else if (upperFirstOccurrenceFlag && iChar.Equals(iChar.ToString().ToLower().ToCharArray()[0]))
                {
                    upperSecondOccurrenceFlag = true;
                }
                else if (iChar.Equals('_'))
                {
                    underScoreOccurrenceFlag = true;
                }
                if ((upperFirstOccurrenceFlag && upperSecondOccurrenceFlag) || underScoreOccurrenceFlag)
                {
                    if (!firstCharacterFlag)
                    {
                        oString = oString.Insert(oString.Length - 1, " ");
                    }
                    upperFirstOccurrenceFlag = false;
                    upperSecondOccurrenceFlag = false;
                    underScoreOccurrenceFlag = false;
                    firstCharacterFlag = false;
                }
                oString = oString + iChar;
                p += 1;
            }
            return (oString);
        }

        /// <summary>
        /// Matched the specifed test string with the given regex pattern
        /// </summary>
        /// <param name="testValue">Value to be tested</param>
        /// <param name="regexPattern">Regular Expression</param>
        /// <returns></returns>
        public bool IsValidExpression(String testValue, String regexPattern)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(regexPattern);
            System.Text.RegularExpressions.MatchCollection matchCollection;
            matchCollection = regex.Matches(testValue);
            if (matchCollection.Count > 0)
                return (matchCollection[0].ToString().Equals(testValue));
            else
                return false;
        }

        #endregion
    }

    public enum MessageType : int { Null = 0, Error = 1, Information = 2, Warning = 3 }

   
}