using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.Common
{
    /// <summary>
    /// Utility functions
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Find Parent SL Panel
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public static Control FindParentSLPanel(Control root)
        {
            Control current = root;
            while (!(current is System.Windows.Forms.Form))
            {
                if (current == null)
                    break;
                if (current.GetType().BaseType.Equals(typeof(SLPanel)))
                    return current;
                current = current.Parent;
            }
            return null;
        }

        /// <summary>
        /// Recursive find control method
        /// </summary>
        /// <param name="cntrl"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control cntrl, string name)
        {
            if (cntrl.Name.Equals(name,StringComparison.CurrentCultureIgnoreCase))
                return cntrl;

            foreach (Control c in cntrl.Controls)
            {
                Control tmp = FindControlRecursive(c, name);
                if (tmp != null)
                    return tmp;
            }
            return null;
        }
        
        /// <summary>
        /// Find Master Panel
        /// </summary>
        /// <param name="root"></param>
        /// <param name="dependentPanelName"></param>
        /// <returns></returns>
        public static Control FindMasterPanel(Control root, string dependentPanelName)
        {
            if (root is SLPanel)
            {
                if((root as SLPanel).DependentPanels != null)
                foreach (SLPanel panel in (root as SLPanel).DependentPanels)
                {
                    if (panel.Name == dependentPanelName)
                        return root;
                }                
            }

            foreach (Control c in root.Controls)
            {
                Control tmp = FindMasterPanel(c, dependentPanelName);
                if (tmp != null)
                    return tmp;
            }
            return null;
        }
    }
}
