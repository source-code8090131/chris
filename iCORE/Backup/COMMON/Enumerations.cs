using System;
using System.Collections.Generic;
using System.Text;
using iCORE.COMMON;
using System.Reflection;
using System.ComponentModel;

namespace iCORE.Common
{
    public class Enumerations
    {
        public enum eActionType : int
        {
            Save = 1,
            Update = 2,
            Delete = 3,
            List = 4,
            LOV = 5, 
            LOVExist = 6
        }

        public enum eGridMode : int
        {
            Add = 0,
            Search = 1
        }

        /// <summary>
        /// Query Mode
        /// </summary>
        public enum eQueryMode : int
        {
            ExecuteQuery = 0,
            EnterQuery = 1
        }
    }
}
