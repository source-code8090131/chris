using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_PFundInterestEntry : ChrisTabularForm
    {
        int rcdIndex = 0;
        double pfIRange = 999.999999;

        #region Constructors

        public CHRIS_PFund_PFundInterestEntry()
        {
            InitializeComponent();
        }
        public CHRIS_PFund_PFundInterestEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            txtOption.Visible = false;

            this.CurrentPanelBlock = this.PnlDetail.Name;
            lblUserName.Text += " " + this.UserName;

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.txtOption.Visible = false;

            dgvPFInterest.Columns["PFI_RATE"].ValueType = typeof(double);
            dgvPFInterest.Columns["PFI_RES_RATE"].ValueType = typeof(double);
            dgvPFInterest.Columns["PFI_YEAR"].ValueType = typeof(double);


            tbtSave.Enabled = true;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;
            tbtEdit.Enabled = false; 

            

            base.DoToolbarActions(this.Controls, "Search");
        }
        protected override void CommonOnLoadMethods()
        {

            base.CommonOnLoadMethods();
            dgvPFInterest.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

            tbtSave.Enabled = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF10 = true;
           

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }

            
            if (actionType == "Close")
            {
                 this.Close();
            }



          
            if (actionType == "Save")
            {
                this.dgvPFInterest.CancelEdit();
              
                DataTable dt = (DataTable)dgvPFInterest.DataSource;
                if (dt != null)
                {

                    foreach (DataGridViewRow row in dgvPFInterest.Rows)
                    {

                        if (Convert.ToString(row.Cells[0].Value) != "" && (Convert.ToString(row.Cells[1].Value) == "" || Convert.ToString(row.Cells[2].Value) == "") )
                        {
                            return;
                        }

                    }
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                    DialogResult dr = MessageBox.Show("Do You Want to Save Changes.", "", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);
                    if (dr == DialogResult.No) { return; }
                    else
                    {
                        this.dgvPFInterest.SaveGrid(this.PnlDetail);
                        //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSave"]));

                    }
                    }
                }
                ///base.DoToolbarActions(this.Controls, "Save");
                return;
                
            }
            if (actionType == "Delete")
            {
                //return;
            }

            if (actionType == "Cancel")
            {
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        #endregion

        #region Event Handlers

        private void dgvPFInterest_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            if (this.tbtClose.Pressed)
                return;

            if (!this.dgvPFInterest.CurrentCell.IsInEditMode)
                return;

                if (e.ColumnIndex == 0)
                {

                    if (dgvPFInterest.CurrentRow.Cells[0].EditedFormattedValue.ToString() != "")// || dgvPFInterest.CurrentRow.Cells[2].EditedFormattedValue.ToString() != "")
                    {
                        if (dgvPFInterest.CurrentRow.Cells[0].EditedFormattedValue.ToString() == dgvPFInterest.CurrentRow.Cells[0].Value.ToString())
                            return;

                        string pfYear = dgvPFInterest.CurrentRow.Cells[0].EditedFormattedValue.ToString();
                        int year = 0;
                        int.TryParse(pfYear, out year);
                        if (year < 1753)
                        {
                            MessageBox.Show("Year should not be less then 1753.");
                            e.Cancel = true;
                            return;
                        }

                        if (pfYear.ToString() == String.Empty || pfYear.Length < 4 )
                        {
                            MessageBox.Show("Field Must Be Enter.....(YYYY)");
                            e.Cancel = true;
                        }
                        
                        else
                        {
                            rcdIndex = 0;
                            foreach (DataGridViewRow row in dgvPFInterest.Rows)
                            {

                                if (Convert.ToString(row.Cells[0].Value) == pfYear)
                                {
                                    //rcdIndex++;
                                    MessageBox.Show("Duplicate Year Not Allowed....");
                                    e.Cancel = true;
                                    return;

                                }
                            }
                            //if (rcdIndex > 1)
                            //{
                            //    MessageBox.Show("Duplicate Year Not Allowed....");
                            //    e.Cancel = true;
                            //}

                            //string dt = this.Now().ToString("dd/MM/yyyy");
                            //dgvPFInterest.CurrentRow.Cells[0].Value = dt.Substring(0,6).ToString() + pfYear;


                        } //END OF ELSE//
                    }
                   

                   
                }

           // }


             //if (dgvPFInterest.CurrentRow.Cells[1].IsInEditMode)
             //{
                 if (e.ColumnIndex == 1)
                 {
                     //if (dgvPFInterest.CurrentRow.Cells[0].EditedFormattedValue.ToString() != "" || dgvPFInterest.CurrentRow.Cells[2].EditedFormattedValue.ToString() != "")
                     if (dgvPFInterest.CurrentRow.Cells[1].EditedFormattedValue.ToString() != "")
                     {
                         string str = dgvPFInterest.CurrentRow.Cells[1].EditedFormattedValue.ToString();
                         double interestRate = 0;
                         double.TryParse(str, out interestRate);
                         if (interestRate == 0)
                         {
                             MessageBox.Show("Interest Rate Must Be > 0.......");
                             e.Cancel = true;
                             return;
                         }

                         string pfIRate = dgvPFInterest.CurrentRow.Cells[1].EditedFormattedValue.ToString();
                         if (pfIRate.ToString() == String.Empty && dgvPFInterest.CurrentRow.Cells[1].ReadOnly == false)
                         {
                             MessageBox.Show("Interest Rate Must Be > 0.......");
                             e.Cancel = true;
                         }
                     }
                     else
                     {
                         MessageBox.Show("Interest Rate Must Be > 0.......");
                         e.Cancel = true;
                     }



                     foreach (DataGridViewRow row in dgvPFInterest.Rows)
                     {
                         
                         if (Convert.ToString(row.Cells[1].Value) != String.Empty )
                         {
                             if (Convert.ToDouble(row.Cells[1].EditedFormattedValue) > pfIRange)
                             {
                                 MessageBox.Show("Field must be of the form 999.999999");
                                 e.Cancel = true;
                             }
                         }
                     }

                 }

                //}

                //if (dgvPFInterest.CurrentRow.Cells[2].IsInEditMode)
                //{

                    if (e.ColumnIndex == 2)
                    {


                        //if (dgvPFInterest.CurrentRow.Cells[0].EditedFormattedValue.ToString() != "" || dgvPFInterest.CurrentRow.Cells[1].EditedFormattedValue.ToString() != "")
                        if (dgvPFInterest.CurrentRow.Cells[2].EditedFormattedValue.ToString() != "")
                        {
                            string str = dgvPFInterest.CurrentRow.Cells[2].EditedFormattedValue.ToString();
                            double interestRate = 0;
                            double.TryParse(str, out interestRate);
                            if (interestRate == 0)
                            {
                                MessageBox.Show("Interest Rate Must Be > 0.......");
                                e.Cancel = true;
                                return;
                            }
                            string pfIRate2 = dgvPFInterest.CurrentRow.Cells[2].EditedFormattedValue.ToString();
                            if (pfIRate2.ToString() == String.Empty && dgvPFInterest.CurrentRow.Cells[2].ReadOnly == false)
                            {
                                MessageBox.Show("Interest Rate Must Be > 0.......");
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Interest Rate Must Be > 0.......");
                            e.Cancel = true;
                        }


                                               
                        
                        foreach (DataGridViewRow row in dgvPFInterest.Rows)
                        {
                              if (Convert.ToString(row.Cells[2].Value) != String.Empty)
                            {
                                if (Convert.ToDouble(row.Cells[2].EditedFormattedValue) > pfIRange)
                                {
                                    MessageBox.Show("Field must be of the form 999.999999");
                                    //row.Cells[2].Selected = true;
                                    e.Cancel = true;
                                }
                            }
                        }

                    }
                //} 

        }
        private void dgvPFInterest_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            //dgvPFInterest.CancelEdit();

        }

        #endregion
    }
}