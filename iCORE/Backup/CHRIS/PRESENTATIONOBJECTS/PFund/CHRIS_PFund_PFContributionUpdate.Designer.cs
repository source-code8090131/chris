namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_PFContributionUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PFund_PFContributionUpdate));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlPFund = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVPfund = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PFD_PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PFD_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PFD_PFUND = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PFD_PF_AREAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PFD_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtYear = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPFund.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPfund)).BeginInit();
            this.pnlMaster.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(515, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(551, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 446);
            this.panel1.Size = new System.Drawing.Size(551, 60);
            // 
            // PnlPFund
            // 
            this.PnlPFund.ConcurrentPanels = null;
            this.PnlPFund.Controls.Add(this.DGVPfund);
            this.PnlPFund.DataManager = null;
            this.PnlPFund.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPFund.DependentPanels = null;
            this.PnlPFund.DisableDependentLoad = false;
            this.PnlPFund.EnableDelete = true;
            this.PnlPFund.EnableInsert = true;
            this.PnlPFund.EnableQuery = false;
            this.PnlPFund.EnableUpdate = true;
            this.PnlPFund.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUND_DETAILCommand";
            this.PnlPFund.Location = new System.Drawing.Point(13, 183);
            this.PnlPFund.MasterPanel = null;
            this.PnlPFund.Name = "PnlPFund";
            this.PnlPFund.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPFund.Size = new System.Drawing.Size(526, 250);
            this.PnlPFund.SPName = "CHRIS_SP_PFUND_DETAIL_MANAGER";
            this.PnlPFund.TabIndex = 3;
            // 
            // DGVPfund
            // 
            this.DGVPfund.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVPfund.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPfund.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PFD_PR_P_NO,
            this.PFD_DATE,
            this.PFD_PFUND,
            this.PFD_PF_AREAR,
            this.PFD_FLAG,
            this.ID});
            this.DGVPfund.ColumnToHide = null;
            this.DGVPfund.ColumnWidth = null;
            this.DGVPfund.CustomEnabled = true;
            this.DGVPfund.DisplayColumnWrapper = null;
            this.DGVPfund.GridDefaultRow = 0;
            this.DGVPfund.Location = new System.Drawing.Point(3, 3);
            this.DGVPfund.Name = "DGVPfund";
            this.DGVPfund.ReadOnlyColumns = null;
            this.DGVPfund.RequiredColumns = "PFD_DATE|PFD_PFUND";
            this.DGVPfund.Size = new System.Drawing.Size(521, 250);
            this.DGVPfund.SkippingColumns = null;
            this.DGVPfund.TabIndex = 3;
            this.DGVPfund.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVPfund_CellValidating);
            // 
            // PFD_PR_P_NO
            // 
            this.PFD_PR_P_NO.DataPropertyName = "PFD_PR_P_NO";
            this.PFD_PR_P_NO.HeaderText = "Column1";
            this.PFD_PR_P_NO.Name = "PFD_PR_P_NO";
            this.PFD_PR_P_NO.Visible = false;
            // 
            // PFD_DATE
            // 
            this.PFD_DATE.DataPropertyName = "PFD_DATE";
            this.PFD_DATE.HeaderText = "Date";
            this.PFD_DATE.MaxInputLength = 10;
            this.PFD_DATE.Name = "PFD_DATE";
            this.PFD_DATE.Width = 110;
            // 
            // PFD_PFUND
            // 
            this.PFD_PFUND.DataPropertyName = "PFD_PFUND";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.PFD_PFUND.DefaultCellStyle = dataGridViewCellStyle1;
            this.PFD_PFUND.HeaderText = "Employee Contribution";
            this.PFD_PFUND.MaxInputLength = 20;
            this.PFD_PFUND.Name = "PFD_PFUND";
            this.PFD_PFUND.Width = 170;
            // 
            // PFD_PF_AREAR
            // 
            this.PFD_PF_AREAR.DataPropertyName = "PFD_PF_AREAR";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.PFD_PF_AREAR.DefaultCellStyle = dataGridViewCellStyle2;
            this.PFD_PF_AREAR.HeaderText = "Arear";
            this.PFD_PF_AREAR.MaxInputLength = 16;
            this.PFD_PF_AREAR.Name = "PFD_PF_AREAR";
            this.PFD_PF_AREAR.Width = 180;
            // 
            // PFD_FLAG
            // 
            this.PFD_FLAG.DataPropertyName = "PFD_FLAG";
            this.PFD_FLAG.HeaderText = "Flag";
            this.PFD_FLAG.MaxInputLength = 1;
            this.PFD_FLAG.Name = "PFD_FLAG";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // pnlMaster
            // 
            this.pnlMaster.ConcurrentPanels = null;
            this.pnlMaster.Controls.Add(this.label3);
            this.pnlMaster.Controls.Add(this.label2);
            this.pnlMaster.Controls.Add(this.label1);
            this.pnlMaster.Controls.Add(this.txtName);
            this.pnlMaster.Controls.Add(this.txtYear);
            this.pnlMaster.Controls.Add(this.txtPersonnelNo);
            this.pnlMaster.DataManager = null;
            this.pnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMaster.DependentPanels = null;
            this.pnlMaster.DisableDependentLoad = false;
            this.pnlMaster.EnableDelete = true;
            this.pnlMaster.EnableInsert = true;
            this.pnlMaster.EnableQuery = false;
            this.pnlMaster.EnableUpdate = true;
            this.pnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFSearchEntityCommand";
            this.pnlMaster.Location = new System.Drawing.Point(13, 97);
            this.pnlMaster.MasterPanel = null;
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMaster.Size = new System.Drawing.Size(527, 80);
            this.pnlMaster.SPName = "CHRIS_SP_PFUND_DETAIL_MANAGER";
            this.pnlMaster.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(49, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Year            : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(284, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Name  :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(49, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Personal No :";
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(341, 17);
            this.txtName.MaxLength = 30;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(168, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 14;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtYear
            // 
            this.txtYear.AllowSpace = true;
            this.txtYear.AssociatedLookUpName = "";
            this.txtYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtYear.ContinuationTextBox = null;
            this.txtYear.CustomEnabled = true;
            this.txtYear.DataFieldMapping = "dummy_year";
            this.txtYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYear.GetRecordsOnUpDownKeys = false;
            this.txtYear.IsDate = false;
            this.txtYear.IsRequired = true;
            this.txtYear.Location = new System.Drawing.Point(139, 43);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.NumberFormat = "###,###,##0.00";
            this.txtYear.Postfix = "";
            this.txtYear.Prefix = "";
            this.txtYear.Size = new System.Drawing.Size(100, 20);
            this.txtYear.SkipValidation = false;
            this.txtYear.TabIndex = 2;
            this.txtYear.TextType = CrplControlLibrary.TextType.Integer;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PFD_PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(139, 17);
            this.txtPersonnelNo.MaxLength = 6;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 1;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNo.TextChanged += new System.EventHandler(this.txtPersonnelNo_TextChanged);
            this.txtPersonnelNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNo_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(123, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(327, 26);
            this.label4.TabIndex = 12;
            this.label4.Text = "Provident Fund Contribution Updation";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(297, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 18;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_PFund_PFContributionUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 506);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlMaster);
            this.Controls.Add(this.PnlPFund);
            this.CurrentPanelBlock = "pnlMaster";
            this.Name = "CHRIS_PFund_PFContributionUpdate";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS :  PF Contribution Update";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlPFund, 0);
            this.Controls.SetChildIndex(this.pnlMaster, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPFund.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVPfund)).EndInit();
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlPFund;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVPfund;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlMaster;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtYear;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFD_PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFD_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFD_PFUND;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFD_PF_AREAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PFD_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}