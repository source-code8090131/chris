using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_ResignISandOtherSettlement : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PFund_ResignISandOtherSettlement()
        {
            InitializeComponent();
        }

        public CHRIS_PFund_ResignISandOtherSettlement(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.w_brn.Text = "ALL";
            this.w_year.Text = "1999";
            this.w_from.Text = "427";
            this.w_dept.Text = "ALL";
            this.termination_dt.Value = new DateTime(1999, 09, 10); 
        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PFR07";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport();
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                    DestName = @"C:\iCORE-Spool\Report";
                if (DestName != string.Empty)
                {
                    base.ExportCustomReport(DestName, "PDF");
                }
            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                RecipentName = "";

                if (DestName != string.Empty)
                {
                    base.EmailToReport(DestName, "PDF", RecipentName);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
      
         

        
    }
}