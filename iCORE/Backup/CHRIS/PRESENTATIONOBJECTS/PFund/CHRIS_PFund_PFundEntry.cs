using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_PFundEntry : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();
        bool flag = false;
        
        #region Constructor
        public CHRIS_PFund_PFundEntry()
        {
            InitializeComponent();
        }

        public CHRIS_PFund_PFundEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
                InitializeComponent();

            }
        #endregion
        
        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtOption.Visible = false;
          //  DtYear.Value = null;
            txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            ///txtUser.Text = this.UserName;
            label10.Text += "  " + this.UserName;
            txtUser.Text = this.userID;

            this.FunctionConfig.F6 = Function.Quit;
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.tbtList.Visible = false;
            this.tbtDelete.Enabled = false;

            //tbtDelete.Enabled = true; 
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                txtPersonelNo.Select();
                txtPersonelNo.Focus();
                base.ClearForm(PnlPfund.Controls);
                this.m_intPKID = 0;

                this.tbtDelete.Enabled = false;
               
                return;
            }

            if (actionType == "Save")
            {
                
                if (txtPersonelNo.Text != string.Empty && txtYear.Text != string.Empty)
                {
                    flag = true;
                    if (this.m_intPKID != 0)
                    {
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                    }
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    //txtPersonelNo.IsRequired = false;
                    txtPersonelNo.Select();
                    txtPersonelNo.Focus();
                    base.ClearForm(PnlPfund.Controls);
                    this.m_intPKID = 0;
                    //txtPersonelNo.IsRequired = true;
                    //txtPremiumAcc.Text = "1";
                    //txtPremiumAcc.Text = string.Empty;
                }
            //    DtYear.Value = null;
                return;
            }

            if (actionType == "Delete")
            {
                if (txtPersonelNo.Text != string.Empty && txtYear.Text != string.Empty)
                {
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    txtPersonelNo.Select();
                    txtPersonelNo.Focus();
                    base.ClearForm(PnlPfund.Controls);
                    this.m_intPKID = 0;
                    return;
                }

                else
                {
                    return;
                }

            }

            if (actionType == "Add")
            {
                return;

            }

            if (actionType == "List")
            {
                return;

            }

            base.DoToolbarActions(ctrlsCollection, actionType);
           
        }
        protected override bool Save()
        {
            return base.Save();
            
        }
        protected override bool Quit()
        {
            flag = true;
            return base.Quit();
        }

        #endregion

        #region Events

        private void DtYear_Validating(object sender, CancelEventArgs e)
        {
            //if (DtYear.Text != "")
            //{

            //    param.Clear();
            //    param.Add("PF_PR_P_NO", txtPersonelNo.Text);
            //    param.Add("PF_YEAR", DtYear.Value);
            //    rslt = cmnDM.GetData("CHRIS_SP_PFUND_PFundEntry_MANAGER", "Execute_query", param);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
            //        {
            //            //txtEmpCont.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
            //            //txtBankCont.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
            //            //txtAccIntrst.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
            //            //txtPremiumAcc.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();

            //            this.IterateFormControls(this.PnlPfund.Controls, true, true, false);
         
            //           // iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUNDCommand objEntity = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUNDCommand) this.PnlPfund.CurrentBusinessEntity;;

            //           // txtID.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

            //           // if (txtID.Text !="")
            //           //     objEntity.ID = Int32.Parse( txtID.Text);

            //           //// PKID = Int32.Parse(txtID.Text); 

                        

            //        }

            //    }
            //}


        }
        private void DtYear_Leave(object sender, EventArgs e)
        {
            //if (DtYear.Value == null)
            //{
            //    MessageBox.Show("Enter Valid Year.........");
            //    return;
            //}

        }
        private void txtPersonelNo_Leave(object sender, EventArgs e)
        {
           
            //if (txtPersonelNo.Text.Trim() != string.Empty)
            //{
            //    param.Clear();
            //    param.Add("PF_PR_P_NO", txtPersonelNo.Text);
            //    rslt = cmnDM.GetData("CHRIS_SP_PFUND_PFundEntry_MANAGER", "PP_VERIFY", param);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables[0].Rows.Count == 0)
            //        {
            //            MessageBox.Show("Invalid Personnel No. Entered.....Try Agan");
            //            txtPersonelNo.Focus();
            //            return;

            //        }
            //    }

            //}


            //if (txtPersonelNo.Text == "")
            //{
            //    MessageBox.Show("Enter Personnel No. or [F9] To Help or [F6] To Exit...");
            //    txtPersonelNo.Focus();
            //    return;
            //}

        }
        private void txtPremiumAcc_Validating(object sender, CancelEventArgs e)
            {
                        
            //if (flag == false)
            //{
                //MessageBox.Show("Press SAVE BUTTON To Save Record .. [F6] To Exit With Out Save..");
              //  e.Cancel = true;
            //}

        }
        private void CHRIS_PFund_PFundEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "Year")
            {
               //txtYear.Select();
               //txtYear.Focus();
               //return;
            }
        }
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtYear.Text == "")
                return;

            int i = int.Parse(this.txtYear.Text);
            try
            {
                if (txtYear.Text =="" || i < 1753) //minimum year limit of SQL 2005//
                {
                    MessageBox.Show("Field Must Be Enter - (YYYY)");
                    e.Cancel = true;
                    return;
                }
            }
            catch { }

            if (this.txtYear.Text.Length != 4 || i < 1753)
            {
                switch (this.txtYear.Text.Length)
                {
                    case 1:
                        this.txtYear.Text = "000" + this.txtYear.Text;
                        break;

                    case 2:
                        this.txtYear.Text = "00" + this.txtYear.Text;
                        break;
                    case 3:
                        this.txtYear.Text = "0" + this.txtYear.Text;
                        break;
                }
            }
            if (txtYear.Text != string.Empty)
            {

                param.Clear();
                param.Add("PF_PR_P_NO", txtPersonelNo.Text);
                param.Add("PF_YEAR", txtYear.Text);
                rslt = cmnDM.GetData("CHRIS_SP_PFUND_PFundEntry_MANAGER", "Execute_query", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        this.SetPanelControlWithListItem(this.PnlPfund.Controls, rslt.dstResult.Tables[0].Rows[0]);
                        this.tbtDelete.Enabled = true;
                    }
                    else
                    {
                        this.tbtDelete.Enabled = false;
                        this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
                        this.m_intPKID = 0;
                        txtEmpCont.Text = "";
                        txtBankCont.Text = "";
                        txtAccIntrst.Text = "";
                        txtPremiumAcc.Text = "";
                    }
                }
            }

            else
            {
                MessageBox.Show("Enter Valid Year.........");
                e.Cancel = true;


            }
        }
        private void txtPremiumAcc_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {


            Keys st = (Keys.Shift | Keys.Tab);
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    DialogResult dRes = MessageBox.Show("Press SAVE BUTTON To Save Record .. [F6] To Exit With Out Save..", "Note"
                                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        if (txtPersonelNo.Text != string.Empty && txtYear.Text != string.Empty)
                        {
                            //flag = true;
                            //if (this.m_intPKID != 0)
                            //{
                            //    this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
                            //}
                            this.DoToolbarActions(this.Controls, "Save");
                            e.IsInputKey = true;
                            //base.ClearForm(PnlPfund.Controls);
                            //this.m_intPKID = 0;
                            //txtPremiumAcc.Text = string.Empty;
                            //txtPersonelNo.Focus();
                        }

                    }
                    else if (dRes == DialogResult.No)
                    {
                        ///base.DoToolbarActions(this.Controls, "Cancel");
                        return;
                    }

                }
            }
        }

        #endregion

        private void txtPremiumAcc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }
    }
}