namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_PFundEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PFund_PFundEntry));
            this.PnlPfund = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtYear = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.LbYear = new CrplControlLibrary.LookupButton(this.components);
            this.LbPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPremiumAcc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAccIntrst = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBankCont = new CrplControlLibrary.SLTextBox(this.components);
            this.txtEmpCont = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPfund.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(585, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(621, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 426);
            this.panel1.Size = new System.Drawing.Size(621, 60);
            // 
            // PnlPfund
            // 
            this.PnlPfund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlPfund.ConcurrentPanels = null;
            this.PnlPfund.Controls.Add(this.txtYear);
            this.PnlPfund.Controls.Add(this.label9);
            this.PnlPfund.Controls.Add(this.LbYear);
            this.PnlPfund.Controls.Add(this.LbPersonnel);
            this.PnlPfund.Controls.Add(this.label7);
            this.PnlPfund.Controls.Add(this.label6);
            this.PnlPfund.Controls.Add(this.label5);
            this.PnlPfund.Controls.Add(this.label4);
            this.PnlPfund.Controls.Add(this.label3);
            this.PnlPfund.Controls.Add(this.label2);
            this.PnlPfund.Controls.Add(this.label1);
            this.PnlPfund.Controls.Add(this.txtPremiumAcc);
            this.PnlPfund.Controls.Add(this.txtAccIntrst);
            this.PnlPfund.Controls.Add(this.txtBankCont);
            this.PnlPfund.Controls.Add(this.txtEmpCont);
            this.PnlPfund.Controls.Add(this.txtName);
            this.PnlPfund.Controls.Add(this.txtPersonelNo);
            this.PnlPfund.DataManager = null;
            this.PnlPfund.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPfund.DependentPanels = null;
            this.PnlPfund.DisableDependentLoad = false;
            this.PnlPfund.EnableDelete = true;
            this.PnlPfund.EnableInsert = true;
            this.PnlPfund.EnableQuery = false;
            this.PnlPfund.EnableUpdate = true;
            this.PnlPfund.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUNDCommand";
            this.PnlPfund.Location = new System.Drawing.Point(12, 187);
            this.PnlPfund.MasterPanel = null;
            this.PnlPfund.Name = "PnlPfund";
            this.PnlPfund.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPfund.Size = new System.Drawing.Size(592, 236);
            this.PnlPfund.SPName = "CHRIS_SP_PFUND_PFundEntry_MANAGER";
            this.PnlPfund.TabIndex = 0;
            // 
            // txtYear
            // 
            this.txtYear.AllowSpace = true;
            this.txtYear.AssociatedLookUpName = "LbYear";
            this.txtYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtYear.ContinuationTextBox = null;
            this.txtYear.CustomEnabled = true;
            this.txtYear.DataFieldMapping = "PF_YEAR";
            this.txtYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYear.GetRecordsOnUpDownKeys = false;
            this.txtYear.IsDate = false;
            this.txtYear.IsLookUpField = true;
            this.txtYear.IsRequired = true;
            this.txtYear.Location = new System.Drawing.Point(212, 82);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.NumberFormat = "###,###,##0.00";
            this.txtYear.Postfix = "";
            this.txtYear.Prefix = "";
            this.txtYear.Size = new System.Drawing.Size(100, 20);
            this.txtYear.SkipValidation = true;
            this.txtYear.TabIndex = 2;
            this.txtYear.TextType = CrplControlLibrary.TextType.Integer;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(318, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "(YYYY)";
            // 
            // LbYear
            // 
            this.LbYear.ActionLOVExists = "Year_Exists";
            this.LbYear.ActionType = "Year";
            this.LbYear.ConditionalFields = "txtPersonelNo";
            this.LbYear.CustomEnabled = true;
            this.LbYear.DataFieldMapping = "";
            this.LbYear.DependentLovControls = "";
            this.LbYear.HiddenColumns = "";
            this.LbYear.Image = ((System.Drawing.Image)(resources.GetObject("LbYear.Image")));
            this.LbYear.LoadDependentEntities = false;
            this.LbYear.Location = new System.Drawing.Point(371, 82);
            this.LbYear.LookUpTitle = null;
            this.LbYear.Name = "LbYear";
            this.LbYear.Size = new System.Drawing.Size(26, 21);
            this.LbYear.SkipValidationOnLeave = true;
            this.LbYear.SPName = "CHRIS_SP_PFUND_PFundEntry_MANAGER";
            this.LbYear.TabIndex = 15;
            this.LbYear.TabStop = false;
            this.LbYear.UseVisualStyleBackColor = true;
            // 
            // LbPersonnel
            // 
            this.LbPersonnel.ActionLOVExists = "Pr_P_noExists";
            this.LbPersonnel.ActionType = "Pr_P_no";
            this.LbPersonnel.ConditionalFields = "";
            this.LbPersonnel.CustomEnabled = true;
            this.LbPersonnel.DataFieldMapping = "";
            this.LbPersonnel.DependentLovControls = "";
            this.LbPersonnel.HiddenColumns = "";
            this.LbPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("LbPersonnel.Image")));
            this.LbPersonnel.LoadDependentEntities = false;
            this.LbPersonnel.Location = new System.Drawing.Point(321, 28);
            this.LbPersonnel.LookUpTitle = null;
            this.LbPersonnel.Name = "LbPersonnel";
            this.LbPersonnel.Size = new System.Drawing.Size(26, 21);
            this.LbPersonnel.SkipValidationOnLeave = false;
            this.LbPersonnel.SPName = "CHRIS_SP_PFUND_PFundEntry_MANAGER";
            this.LbPersonnel.TabIndex = 14;
            this.LbPersonnel.TabStop = false;
            this.LbPersonnel.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(86, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "Premium Amount :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(86, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Acc. Interest :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(86, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Bank Cont.:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(86, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Employee Cont. :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(86, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "P.Fund Year :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(86, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Personnel No. :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPremiumAcc
            // 
            this.txtPremiumAcc.AllowSpace = true;
            this.txtPremiumAcc.AssociatedLookUpName = "";
            this.txtPremiumAcc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPremiumAcc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPremiumAcc.ContinuationTextBox = null;
            this.txtPremiumAcc.CustomEnabled = true;
            this.txtPremiumAcc.DataFieldMapping = "PF_PREMIUM";
            this.txtPremiumAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPremiumAcc.GetRecordsOnUpDownKeys = false;
            this.txtPremiumAcc.IsDate = false;
            this.txtPremiumAcc.Location = new System.Drawing.Point(212, 186);
            this.txtPremiumAcc.MaxLength = 12;
            this.txtPremiumAcc.Name = "txtPremiumAcc";
            this.txtPremiumAcc.NumberFormat = "###,###,##0.00";
            this.txtPremiumAcc.Postfix = "";
            this.txtPremiumAcc.Prefix = "";
            this.txtPremiumAcc.Size = new System.Drawing.Size(100, 20);
            this.txtPremiumAcc.SkipValidation = false;
            this.txtPremiumAcc.TabIndex = 6;
            this.txtPremiumAcc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPremiumAcc.TextType = CrplControlLibrary.TextType.String;
            this.txtPremiumAcc.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPremiumAcc_PreviewKeyDown);
            this.txtPremiumAcc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPremiumAcc_KeyPress);
            this.txtPremiumAcc.Validating += new System.ComponentModel.CancelEventHandler(this.txtPremiumAcc_Validating);
            // 
            // txtAccIntrst
            // 
            this.txtAccIntrst.AllowSpace = true;
            this.txtAccIntrst.AssociatedLookUpName = "";
            this.txtAccIntrst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccIntrst.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAccIntrst.ContinuationTextBox = null;
            this.txtAccIntrst.CustomEnabled = true;
            this.txtAccIntrst.DataFieldMapping = "PF_INTEREST";
            this.txtAccIntrst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccIntrst.GetRecordsOnUpDownKeys = false;
            this.txtAccIntrst.IsDate = false;
            this.txtAccIntrst.Location = new System.Drawing.Point(212, 160);
            this.txtAccIntrst.MaxLength = 12;
            this.txtAccIntrst.Name = "txtAccIntrst";
            this.txtAccIntrst.NumberFormat = "###,###,##0.00";
            this.txtAccIntrst.Postfix = "";
            this.txtAccIntrst.Prefix = "";
            this.txtAccIntrst.Size = new System.Drawing.Size(100, 20);
            this.txtAccIntrst.SkipValidation = false;
            this.txtAccIntrst.TabIndex = 5;
            this.txtAccIntrst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAccIntrst.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtBankCont
            // 
            this.txtBankCont.AllowSpace = true;
            this.txtBankCont.AssociatedLookUpName = "";
            this.txtBankCont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBankCont.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBankCont.ContinuationTextBox = null;
            this.txtBankCont.CustomEnabled = true;
            this.txtBankCont.DataFieldMapping = "PF_BNK_CONT";
            this.txtBankCont.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBankCont.GetRecordsOnUpDownKeys = false;
            this.txtBankCont.IsDate = false;
            this.txtBankCont.Location = new System.Drawing.Point(212, 134);
            this.txtBankCont.MaxLength = 12;
            this.txtBankCont.Name = "txtBankCont";
            this.txtBankCont.NumberFormat = "###,###,##0.00";
            this.txtBankCont.Postfix = "";
            this.txtBankCont.Prefix = "";
            this.txtBankCont.Size = new System.Drawing.Size(100, 20);
            this.txtBankCont.SkipValidation = false;
            this.txtBankCont.TabIndex = 4;
            this.txtBankCont.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBankCont.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtEmpCont
            // 
            this.txtEmpCont.AllowSpace = true;
            this.txtEmpCont.AssociatedLookUpName = "";
            this.txtEmpCont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmpCont.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEmpCont.ContinuationTextBox = null;
            this.txtEmpCont.CustomEnabled = true;
            this.txtEmpCont.DataFieldMapping = "PF_EMP_CONT";
            this.txtEmpCont.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmpCont.GetRecordsOnUpDownKeys = false;
            this.txtEmpCont.IsDate = false;
            this.txtEmpCont.Location = new System.Drawing.Point(212, 108);
            this.txtEmpCont.MaxLength = 12;
            this.txtEmpCont.Name = "txtEmpCont";
            this.txtEmpCont.NumberFormat = "###,###,##0.00";
            this.txtEmpCont.Postfix = "";
            this.txtEmpCont.Prefix = "";
            this.txtEmpCont.Size = new System.Drawing.Size(100, 20);
            this.txtEmpCont.SkipValidation = false;
            this.txtEmpCont.TabIndex = 3;
            this.txtEmpCont.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEmpCont.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PF_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(212, 56);
            this.txtName.MaxLength = 25;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.Size = new System.Drawing.Size(229, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 1;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonelNo
            // 
            this.txtPersonelNo.AllowSpace = true;
            this.txtPersonelNo.AssociatedLookUpName = "LbPersonnel";
            this.txtPersonelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonelNo.ContinuationTextBox = null;
            this.txtPersonelNo.CustomEnabled = true;
            this.txtPersonelNo.DataFieldMapping = "PF_PR_P_NO";
            this.txtPersonelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonelNo.IsDate = false;
            this.txtPersonelNo.IsLookUpField = true;
            this.txtPersonelNo.IsRequired = true;
            this.txtPersonelNo.Location = new System.Drawing.Point(212, 30);
            this.txtPersonelNo.MaxLength = 6;
            this.txtPersonelNo.Name = "txtPersonelNo";
            this.txtPersonelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonelNo.Postfix = "";
            this.txtPersonelNo.Prefix = "";
            this.txtPersonelNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonelNo.SkipValidation = false;
            this.txtPersonelNo.TabIndex = 0;
            this.txtPersonelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonelNo.Leave += new System.EventHandler(this.txtPersonelNo_Leave);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label8);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 90);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(592, 91);
            this.pnlHead.TabIndex = 52;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(194, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(210, 18);
            this.label8.TabIndex = 20;
            this.label8.Text = "P.Fund Entry";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(194, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(222, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Provident Fund Module";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 61);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(498, 35);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(443, 64);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date : ";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(431, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option : ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(18, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Loc : ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User : ";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(337, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 53;
            this.label10.Text = "User Name :  ";
            // 
            // CHRIS_PFund_PFundEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(621, 486);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPfund);
            this.F10OptionText = "[F10] = Save Record";
            this.F6OptionText = "[F6] = Exit";
            this.Name = "CHRIS_PFund_PFundEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - PFund Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_PFund_PFundEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlPfund, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPfund.ResumeLayout(false);
            this.PnlPfund.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPfund;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtPremiumAcc;
        private CrplControlLibrary.SLTextBox txtAccIntrst;
        private CrplControlLibrary.SLTextBox txtBankCont;
        private CrplControlLibrary.SLTextBox txtEmpCont;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtPersonelNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.LookupButton LbPersonnel;
        private CrplControlLibrary.LookupButton LbYear;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtYear;
    }
}