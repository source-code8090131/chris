namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_ResignISandOtherSettlement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PFund_ResignISandOtherSettlement));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.zakat = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.trustee2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.trustee1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.termination_dt = new CrplControlLibrary.SLDatePicker(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.w_dept = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.today = new CrplControlLibrary.SLDatePicker(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.w_year = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
            this.w_from = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(276, 355);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(63, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(209, 355);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 11;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "PERSONNEL NO. FROM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "ENTER BRANCH OR ALL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destype";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.zakat);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.trustee2);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.trustee1);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.termination_dt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.w_dept);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.today);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.w_year);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.w_brn);
            this.groupBox1.Controls.Add(this.w_from);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 387);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // zakat
            // 
            this.zakat.AllowSpace = true;
            this.zakat.AssociatedLookUpName = "";
            this.zakat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zakat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.zakat.ContinuationTextBox = null;
            this.zakat.CustomEnabled = true;
            this.zakat.DataFieldMapping = "";
            this.zakat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zakat.GetRecordsOnUpDownKeys = false;
            this.zakat.IsDate = false;
            this.zakat.Location = new System.Drawing.Point(212, 318);
            this.zakat.MaxLength = 1;
            this.zakat.Name = "zakat";
            this.zakat.NumberFormat = "###,###,##0.00";
            this.zakat.Postfix = "";
            this.zakat.Prefix = "";
            this.zakat.Size = new System.Drawing.Size(153, 20);
            this.zakat.SkipValidation = false;
            this.zakat.TabIndex = 10;
            this.zakat.TextType = CrplControlLibrary.TextType.String;
            this.zakat.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(26, 318);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 13);
            this.label13.TabIndex = 92;
            this.label13.Text = "Zakat Deduction";
            this.label13.Visible = false;
            // 
            // trustee2
            // 
            this.trustee2.AllowSpace = true;
            this.trustee2.AssociatedLookUpName = "";
            this.trustee2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trustee2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.trustee2.ContinuationTextBox = null;
            this.trustee2.CustomEnabled = true;
            this.trustee2.DataFieldMapping = "";
            this.trustee2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trustee2.GetRecordsOnUpDownKeys = false;
            this.trustee2.IsDate = false;
            this.trustee2.Location = new System.Drawing.Point(212, 292);
            this.trustee2.MaxLength = 50;
            this.trustee2.Name = "trustee2";
            this.trustee2.NumberFormat = "###,###,##0.00";
            this.trustee2.Postfix = "";
            this.trustee2.Prefix = "";
            this.trustee2.Size = new System.Drawing.Size(153, 20);
            this.trustee2.SkipValidation = false;
            this.trustee2.TabIndex = 9;
            this.trustee2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 292);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 90;
            this.label12.Text = "Trustee 2";
            // 
            // trustee1
            // 
            this.trustee1.AllowSpace = true;
            this.trustee1.AssociatedLookUpName = "";
            this.trustee1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trustee1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.trustee1.ContinuationTextBox = null;
            this.trustee1.CustomEnabled = true;
            this.trustee1.DataFieldMapping = "";
            this.trustee1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trustee1.GetRecordsOnUpDownKeys = false;
            this.trustee1.IsDate = false;
            this.trustee1.Location = new System.Drawing.Point(212, 266);
            this.trustee1.MaxLength = 50;
            this.trustee1.Name = "trustee1";
            this.trustee1.NumberFormat = "###,###,##0.00";
            this.trustee1.Postfix = "";
            this.trustee1.Prefix = "";
            this.trustee1.Size = new System.Drawing.Size(153, 20);
            this.trustee1.SkipValidation = false;
            this.trustee1.TabIndex = 8;
            this.trustee1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 88;
            this.label11.Text = "Trustee 1";
            // 
            // termination_dt
            // 
            this.termination_dt.CustomEnabled = true;
            this.termination_dt.CustomFormat = "dd/MM/yyyy";
            this.termination_dt.DataFieldMapping = "";
            this.termination_dt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.termination_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.termination_dt.HasChanges = true;
            this.termination_dt.Location = new System.Drawing.Point(213, 240);
            this.termination_dt.Name = "termination_dt";
            this.termination_dt.NullValue = " ";
            this.termination_dt.Size = new System.Drawing.Size(152, 20);
            this.termination_dt.TabIndex = 7;
            this.termination_dt.Value = new System.DateTime(2011, 1, 21, 15, 55, 46, 468);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "Termination Dt";
            // 
            // w_dept
            // 
            this.w_dept.AllowSpace = true;
            this.w_dept.AssociatedLookUpName = "";
            this.w_dept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_dept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_dept.ContinuationTextBox = null;
            this.w_dept.CustomEnabled = true;
            this.w_dept.DataFieldMapping = "";
            this.w_dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_dept.GetRecordsOnUpDownKeys = false;
            this.w_dept.IsDate = false;
            this.w_dept.Location = new System.Drawing.Point(213, 214);
            this.w_dept.MaxLength = 3;
            this.w_dept.Name = "w_dept";
            this.w_dept.NumberFormat = "###,###,##0.00";
            this.w_dept.Postfix = "";
            this.w_dept.Prefix = "";
            this.w_dept.Size = new System.Drawing.Size(152, 20);
            this.w_dept.SkipValidation = false;
            this.w_dept.TabIndex = 6;
            this.w_dept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(186, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "ENTER DEPARTMENT OR ALL";
            // 
            // today
            // 
            this.today.CustomEnabled = true;
            this.today.CustomFormat = "dd/MM/yyyy";
            this.today.DataFieldMapping = "";
            this.today.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.today.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.today.HasChanges = true;
            this.today.Location = new System.Drawing.Point(213, 108);
            this.today.Name = "today";
            this.today.NullValue = " ";
            this.today.Size = new System.Drawing.Size(152, 20);
            this.today.TabIndex = 2;
            this.today.Value = new System.DateTime(2011, 1, 21, 15, 55, 46, 468);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 82;
            this.label3.Text = "Today";
            // 
            // w_year
            // 
            this.w_year.AllowSpace = true;
            this.w_year.AssociatedLookUpName = "";
            this.w_year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_year.ContinuationTextBox = null;
            this.w_year.CustomEnabled = true;
            this.w_year.DataFieldMapping = "";
            this.w_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_year.GetRecordsOnUpDownKeys = false;
            this.w_year.IsDate = false;
            this.w_year.Location = new System.Drawing.Point(212, 134);
            this.w_year.MaxLength = 4;
            this.w_year.Name = "w_year";
            this.w_year.NumberFormat = "###,###,##0.00";
            this.w_year.Postfix = "";
            this.w_year.Prefix = "";
            this.w_year.Size = new System.Drawing.Size(153, 20);
            this.w_year.SkipValidation = false;
            this.w_year.TabIndex = 3;
            this.w_year.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 79;
            this.label2.Text = "Year (YYYY)";
            // 
            // w_brn
            // 
            this.w_brn.AllowSpace = true;
            this.w_brn.AssociatedLookUpName = "";
            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_brn.ContinuationTextBox = null;
            this.w_brn.CustomEnabled = true;
            this.w_brn.DataFieldMapping = "";
            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_brn.GetRecordsOnUpDownKeys = false;
            this.w_brn.IsDate = false;
            this.w_brn.Location = new System.Drawing.Point(213, 188);
            this.w_brn.MaxLength = 3;
            this.w_brn.Name = "w_brn";
            this.w_brn.NumberFormat = "###,###,##0.00";
            this.w_brn.Postfix = "";
            this.w_brn.Prefix = "";
            this.w_brn.Size = new System.Drawing.Size(152, 20);
            this.w_brn.SkipValidation = false;
            this.w_brn.TabIndex = 5;
            this.w_brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_from
            // 
            this.w_from.AllowSpace = true;
            this.w_from.AssociatedLookUpName = "";
            this.w_from.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_from.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_from.ContinuationTextBox = null;
            this.w_from.CustomEnabled = true;
            this.w_from.DataFieldMapping = "";
            this.w_from.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_from.GetRecordsOnUpDownKeys = false;
            this.w_from.IsDate = false;
            this.w_from.Location = new System.Drawing.Point(212, 162);
            this.w_from.MaxLength = 6;
            this.w_from.Name = "w_from";
            this.w_from.NumberFormat = "###,###,##0.00";
            this.w_from.Postfix = "";
            this.w_from.Prefix = "";
            this.w_from.Size = new System.Drawing.Size(153, 20);
            this.w_from.SkipValidation = false;
            this.w_from.TabIndex = 4;
            this.w_from.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(211, 80);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(154, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(160, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Enter Parameters For Report";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(177, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Report Parameter";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(424, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 78;
            this.pictureBox2.TabStop = false;
            // 
            // CHRIS_PFund_ResignISandOtherSettlement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 436);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PFund_ResignISandOtherSettlement";
            this.Text = "CHRIS_PersonnelReport_SegmentWiseReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox w_brn;
        private CrplControlLibrary.SLTextBox w_from;
        private CrplControlLibrary.SLTextBox w_year;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLDatePicker today;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLDatePicker termination_dt;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox w_dept;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox zakat;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox trustee2;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox trustee1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}