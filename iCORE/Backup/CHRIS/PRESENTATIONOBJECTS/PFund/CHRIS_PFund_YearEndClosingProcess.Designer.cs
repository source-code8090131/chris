namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_YearEndClosingProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlProcessStatus = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtYear = new CrplControlLibrary.SLTextBox(this.components);
            this.lblYear = new System.Windows.Forms.Label();
            this.txtStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.lblYN = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlProcessStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(613, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(649, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 336);
            this.panel1.Size = new System.Drawing.Size(649, 60);
            // 
            // pnlProcessStatus
            // 
            this.pnlProcessStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProcessStatus.ConcurrentPanels = null;
            this.pnlProcessStatus.Controls.Add(this.txtYear);
            this.pnlProcessStatus.Controls.Add(this.lblYear);
            this.pnlProcessStatus.Controls.Add(this.txtStatus);
            this.pnlProcessStatus.Controls.Add(this.lblYN);
            this.pnlProcessStatus.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlProcessStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlProcessStatus.DependentPanels = null;
            this.pnlProcessStatus.DisableDependentLoad = false;
            this.pnlProcessStatus.EnableDelete = false;
            this.pnlProcessStatus.EnableInsert = false;
            this.pnlProcessStatus.EnableQuery = false;
            this.pnlProcessStatus.EnableUpdate = false;
            this.pnlProcessStatus.EntityName = null;
            this.pnlProcessStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlProcessStatus.Location = new System.Drawing.Point(48, 144);
            this.pnlProcessStatus.MasterPanel = null;
            this.pnlProcessStatus.Name = "pnlProcessStatus";
            this.pnlProcessStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.pnlProcessStatus.Size = new System.Drawing.Size(544, 172);
            this.pnlProcessStatus.SPName = "CHRIS_SP_LEAVES_MANAGER";
            this.pnlProcessStatus.TabIndex = 18;
            // 
            // txtYear
            // 
            this.txtYear.AllowSpace = true;
            this.txtYear.AssociatedLookUpName = "";
            this.txtYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtYear.ContinuationTextBox = null;
            this.txtYear.CustomEnabled = true;
            this.txtYear.DataFieldMapping = "";
            this.txtYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYear.GetRecordsOnUpDownKeys = false;
            this.txtYear.IsDate = false;
            this.txtYear.Location = new System.Drawing.Point(310, 42);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.NumberFormat = "###,###,##0.00";
            this.txtYear.Postfix = "";
            this.txtYear.Prefix = "";
            this.txtYear.Size = new System.Drawing.Size(117, 20);
            this.txtYear.SkipValidation = false;
            this.txtYear.TabIndex = 0;
            this.txtYear.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtYear, "Enter Valid Year");
            this.txtYear.Leave += new System.EventHandler(this.txtYear_Leave);
            this.txtYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYear_KeyPress);
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYear.Location = new System.Drawing.Point(31, 45);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(243, 15);
            this.lblYear.TabIndex = 8;
            this.lblYear.Text = "Enter the Provident Fund Processing Year :";
            // 
            // txtStatus
            // 
            this.txtStatus.AllowSpace = true;
            this.txtStatus.AssociatedLookUpName = "";
            this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStatus.ContinuationTextBox = null;
            this.txtStatus.CustomEnabled = true;
            this.txtStatus.DataFieldMapping = "";
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.GetRecordsOnUpDownKeys = false;
            this.txtStatus.IsDate = false;
            this.txtStatus.Location = new System.Drawing.Point(310, 98);
            this.txtStatus.MaxLength = 3;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.NumberFormat = "###,###,##0.00";
            this.txtStatus.Postfix = "";
            this.txtStatus.Prefix = "";
            this.txtStatus.Size = new System.Drawing.Size(117, 20);
            this.txtStatus.SkipValidation = false;
            this.txtStatus.TabIndex = 1;
            this.txtStatus.TextType = CrplControlLibrary.TextType.String;
            this.txtStatus.Leave += new System.EventHandler(this.txtStatus_Leave);
            // 
            // lblYN
            // 
            this.lblYN.AutoSize = true;
            this.lblYN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYN.Location = new System.Drawing.Point(31, 101);
            this.lblYN.Name = "lblYN";
            this.lblYN.Size = new System.Drawing.Size(256, 15);
            this.lblYN.TabIndex = 6;
            this.lblYN.Text = "Do You Want to Start Processing  [Yes]/[No] :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(363, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 27;
            this.lblUserName.Text = "User Name  :";
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = true;
            this.labelHeading.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeading.Location = new System.Drawing.Point(197, 45);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(205, 45);
            this.labelHeading.TabIndex = 26;
            this.labelHeading.Text = "      PROVIDENT FUND MODULE     \n \n   Year End P.Fund Closing Process ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(443, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Date :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(491, 56);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 29;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_PFund_YearEndClosingProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(649, 396);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.pnlProcessStatus);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.labelHeading);
            this.Name = "CHRIS_PFund_YearEndClosingProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS : P.Fund Closing Process";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.labelHeading, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.pnlProcessStatus, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlProcessStatus.ResumeLayout(false);
            this.pnlProcessStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlProcessStatus;
        private CrplControlLibrary.SLTextBox txtYear;
        private System.Windows.Forms.Label lblYear;
        private CrplControlLibrary.SLTextBox txtStatus;
        private System.Windows.Forms.Label lblYN;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label labelHeading;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtDate;
    }
}