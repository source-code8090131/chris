using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_PFContributionUpdate : ChrisMasterDetailForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        
        #region Constructor

        public CHRIS_PFund_PFContributionUpdate()
        {
            InitializeComponent();
        }
        public CHRIS_PFund_PFContributionUpdate(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
                InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlPFund);
            pnlMaster.DependentPanels = lstDependentPanels;
         //   List<SLPanel> lstINDependentPanels = new List<SLPanel>();
           // lstINDependentPanels.Add(pnlMaster);
            this.IndependentPanels.Add(pnlMaster);

        }
        #endregion
       
        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            txtOption.Visible = false;
            base.OnLoad(e);
            this.lblUserName.Text = "User Name:    " + this.UserName;
            this.FunctionConfig.F6 = Function.Quit;
            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF4 = true;
            this.FunctionConfig.F4 = Function.Delete;


            DGVPfund.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

            this.tbtList.Visible = false; 


        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            this.DGVPfund.CancelEdit();
            this.DGVPfund.CancelEdit();

            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUND_DETAILCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUND_DETAILCommand)this.PnlPFund.CurrentBusinessEntity;
            if (ent != null)
            {
                this.m_intPKID = ent.ID;
               // ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
            }


            if ((txtPersonnelNo.Text == "" && actionType == "Save" && txtYear.Text == "") || (txtPersonnelNo.Text == "" && actionType == "Save") || (actionType == "Save" && txtYear.Text == "") )
            {
                return;

            }



            if (this.m_intPKID > 0)
            {
                this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            
            }
            //if (actionType == "List")
            //    return;

            //if (actionType == "Delete")
            //{                
            //    base.DoToolbarActions(ctrlsCollection, actionType);
            //    return;
            //}
            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Cancel")
            {
                this.txtPersonnelNo.Select();
                this.txtPersonnelNo.Focus();
            }
        }

        protected override bool Save()
        {
            return base.Save();
        }

        protected override bool Delete()
        {
            return base.Delete();
        }

        protected override bool Quit()
        {
            return base.Quit();
        }
        #endregion

        #region Events
        
        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PFD_PR_P_NO", txtPersonnelNo.Text);
            rslt = cmnDM.GetData("CHRIS_SP_PFUND_DETAIL_MANAGER", "GetName", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtName.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    MessageBox.Show("Invalid Personnel Number", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Cancel = true;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Invalid Personnel Number", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Cancel = true;
                return;
            }
            
            
        
        }
        private void txtYear_Validating(object sender, CancelEventArgs e)
        {
            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFSearchEntityCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFSearchEntityCommand)this.pnlMaster.CurrentBusinessEntity;

            if (txtName.Text != "" && txtYear.Text != "")
            {

                ent.dummy_year = txtYear.Text;
                ent.PFD_PR_P_NO = Int32.Parse(txtPersonnelNo.Text);

                this.pnlMaster.LoadDependentPanels();

                if (this.DGVPfund.Rows.Count <= 1)
                {
                    MessageBox.Show("Invalid Year", "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Cancel = true;
                    return;
                }
            }
          //  Result rslt;
          //  Dictionary<string, object> param = new Dictionary<string, object>();
          //  param.Add("PFD_PR_P_NO", txtPersonnelNo.Text);
          //  param.Add("dummy_year",txtYear.Text);
          // // rslt = cmnDM.GetData("CHRIS_SP_PFUND_DETAIL_MANAGER", "getyear", param);
          ////  DGVPfund.DataSource = null;
          //  if (txtYear.Text != "" )
          //  {
          //      DataTable dt = new DataTable();
          //      rslt = cmnDM.GetData("CHRIS_SP_PFUND_DETAIL_MANAGER", "GetAll_Records", param);
          //      if (rslt.isSuccessful)
          //      {

          //          if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
          //          {                        
          //                  dt = rslt.dstResult.Tables[0];
          //                  DGVPfund.DataSource= dt;
          //                  DGVPfund.GridSource = dt;
                                  
                    
          //          }             
                
          //      }
            
          //  }


        }
        private void DGVPfund_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            #region Column 1
            if (e.ColumnIndex == 1)
            {
                if (DGVPfund.CurrentRow.Cells["PFD_DATE"].IsInEditMode)
                {
                    if (DGVPfund.CurrentRow.Cells["PFD_DATE"].EditedFormattedValue != null && DGVPfund.CurrentRow.Cells["PFD_DATE"].EditedFormattedValue.ToString() != string.Empty && DGVPfund.CurrentRow.Cells["PFD_DATE"].EditedFormattedValue.ToString().ToString() != "")
                    {
                        //PFD_DATE
                        DateTime PFD_DATE = new DateTime(1900, 1, 1);


                        try
                        {
                            System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                            dtf.ShortDatePattern = "MM/dd/yyyy";

                            PFD_DATE = DateTime.Parse(DGVPfund.CurrentRow.Cells["PFD_DATE"].EditedFormattedValue.ToString());
                            int year = PFD_DATE.Year;
                            int dummy_year = Int32.Parse(txtYear.Text);
                            if (year != dummy_year)
                            {
                                MessageBox.Show("Year Should be Same as Above Year");
                                e.Cancel = true;

                           }


                        }
                        catch (Exception ex)
                        {
                            if (PFD_DATE == new DateTime(1900, 1, 1))
                            {
                                DGVPfund.ShowCellErrors = true;
                                DGVPfund.CurrentCell.ErrorText = "Incorrect date format";
                                e.Cancel = true;
                                return;
                            }

                        }
                    }
                }
            }
            #endregion 

            //#region column 2

            //if (DGVPfund.CurrentRow.Cells["PFD_FLAG"].IsInEditMode)
            //{

            //    if (DGVPfund.CurrentRow.Cells["PFD_FLAG"].EditedFormattedValue.ToString().ToUpper() != "Y" && DGVPfund.CurrentRow.Cells["PFD_FLAG"].EditedFormattedValue.ToString().ToUpper() != "N")
            //    {
            //        e.Cancel = true;
                
            //    }
            
            //}


            //#endregion
        }
        private void txtPersonnelNo_TextChanged(object sender, EventArgs e)
        {
            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFSearchEntityCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFSearchEntityCommand)this.pnlMaster.CurrentBusinessEntity;

            if (ent != null)
            {
                try
                {
                    ent.PFD_PR_P_NO = Int32.Parse(txtPersonnelNo.Text == "" ? "0" : txtPersonnelNo.Text);
                }
                catch { }
            }
        }

        #endregion

    }
}