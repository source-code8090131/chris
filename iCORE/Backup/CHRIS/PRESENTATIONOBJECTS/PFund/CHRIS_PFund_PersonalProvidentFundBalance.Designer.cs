namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_PersonalProvidentFundBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PFund_PersonalProvidentFundBalance));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.w_pto = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.mode = new CrplControlLibrary.SLComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Copies = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.desformat = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.desname = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.w_dept = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.today = new CrplControlLibrary.SLDatePicker(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.w_year = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.w_brn = new CrplControlLibrary.SLTextBox(this.components);
            this.w_from = new CrplControlLibrary.SLTextBox(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(274, 383);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(63, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(207, 383);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(63, 23);
            this.btnRun.TabIndex = 11;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 268);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "PERSONNEL NO. FROM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 327);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "ENTER BRANCH OR ALL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Destype";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.w_pto);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.mode);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.Copies);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.desformat);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.desname);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.w_dept);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.today);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.w_year);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.w_brn);
            this.groupBox1.Controls.Add(this.w_from);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRun);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 415);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // w_pto
            // 
            this.w_pto.AllowSpace = true;
            this.w_pto.AssociatedLookUpName = "";
            this.w_pto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_pto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_pto.ContinuationTextBox = null;
            this.w_pto.CustomEnabled = true;
            this.w_pto.DataFieldMapping = "";
            this.w_pto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_pto.GetRecordsOnUpDownKeys = false;
            this.w_pto.IsDate = false;
            this.w_pto.Location = new System.Drawing.Point(208, 297);
            this.w_pto.MaxLength = 6;
            this.w_pto.Name = "w_pto";
            this.w_pto.NumberFormat = "###,###,##0.00";
            this.w_pto.Postfix = "";
            this.w_pto.Prefix = "";
            this.w_pto.Size = new System.Drawing.Size(153, 20);
            this.w_pto.SkipValidation = false;
            this.w_pto.TabIndex = 8;
            this.w_pto.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 13);
            this.label13.TabIndex = 95;
            this.label13.Text = "PERSONNEL NO.  TO";
            // 
            // mode
            // 
            this.mode.BusinessEntity = "";
            this.mode.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.mode.CustomEnabled = true;
            this.mode.DataFieldMapping = "";
            this.mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mode.FormattingEnabled = true;
            this.mode.Items.AddRange(new object[] {
            "Default",
            "Bitmap",
            "Character"});
            this.mode.Location = new System.Drawing.Point(208, 238);
            this.mode.LOVType = "";
            this.mode.Name = "mode";
            this.mode.Size = new System.Drawing.Size(153, 21);
            this.mode.SPName = "";
            this.mode.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 236);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 92;
            this.label12.Text = "Mode";
            // 
            // Copies
            // 
            this.Copies.AllowSpace = true;
            this.Copies.AssociatedLookUpName = "";
            this.Copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Copies.ContinuationTextBox = null;
            this.Copies.CustomEnabled = true;
            this.Copies.DataFieldMapping = "";
            this.Copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Copies.GetRecordsOnUpDownKeys = false;
            this.Copies.IsDate = false;
            this.Copies.Location = new System.Drawing.Point(208, 186);
            this.Copies.MaxLength = 4;
            this.Copies.Name = "Copies";
            this.Copies.NumberFormat = "###,###,##0.00";
            this.Copies.Postfix = "";
            this.Copies.Prefix = "";
            this.Copies.Size = new System.Drawing.Size(153, 20);
            this.Copies.SkipValidation = false;
            this.Copies.TabIndex = 4;
            this.Copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 91;
            this.label11.Text = "Copies";
            // 
            // desformat
            // 
            this.desformat.AllowSpace = true;
            this.desformat.AssociatedLookUpName = "";
            this.desformat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.desformat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.desformat.ContinuationTextBox = null;
            this.desformat.CustomEnabled = true;
            this.desformat.DataFieldMapping = "";
            this.desformat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.desformat.GetRecordsOnUpDownKeys = false;
            this.desformat.IsDate = false;
            this.desformat.Location = new System.Drawing.Point(208, 160);
            this.desformat.MaxLength = 4;
            this.desformat.Name = "desformat";
            this.desformat.NumberFormat = "###,###,##0.00";
            this.desformat.Postfix = "";
            this.desformat.Prefix = "";
            this.desformat.Size = new System.Drawing.Size(153, 20);
            this.desformat.SkipValidation = false;
            this.desformat.TabIndex = 3;
            this.desformat.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 89;
            this.label7.Text = "Desformat";
            // 
            // desname
            // 
            this.desname.AllowSpace = true;
            this.desname.AssociatedLookUpName = "";
            this.desname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.desname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.desname.ContinuationTextBox = null;
            this.desname.CustomEnabled = true;
            this.desname.DataFieldMapping = "";
            this.desname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.desname.GetRecordsOnUpDownKeys = false;
            this.desname.IsDate = false;
            this.desname.Location = new System.Drawing.Point(208, 134);
            this.desname.MaxLength = 4;
            this.desname.Name = "desname";
            this.desname.NumberFormat = "###,###,##0.00";
            this.desname.Postfix = "";
            this.desname.Prefix = "";
            this.desname.Size = new System.Drawing.Size(153, 20);
            this.desname.SkipValidation = false;
            this.desname.TabIndex = 2;
            this.desname.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 87;
            this.label5.Text = "Desname";
            // 
            // w_dept
            // 
            this.w_dept.AllowSpace = true;
            this.w_dept.AssociatedLookUpName = "";
            this.w_dept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_dept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_dept.ContinuationTextBox = null;
            this.w_dept.CustomEnabled = true;
            this.w_dept.DataFieldMapping = "";
            this.w_dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_dept.GetRecordsOnUpDownKeys = false;
            this.w_dept.IsDate = false;
            this.w_dept.Location = new System.Drawing.Point(208, 349);
            this.w_dept.MaxLength = 3;
            this.w_dept.Name = "w_dept";
            this.w_dept.NumberFormat = "###,###,##0.00";
            this.w_dept.Postfix = "";
            this.w_dept.Prefix = "";
            this.w_dept.Size = new System.Drawing.Size(153, 20);
            this.w_dept.SkipValidation = false;
            this.w_dept.TabIndex = 10;
            this.w_dept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 351);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(186, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "ENTER DEPARTMENT OR ALL";
            // 
            // today
            // 
            this.today.CustomEnabled = true;
            this.today.CustomFormat = "dd/MM/yyyy";
            this.today.DataFieldMapping = "";
            this.today.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.today.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.today.HasChanges = true;
            this.today.Location = new System.Drawing.Point(208, 108);
            this.today.Name = "today";
            this.today.NullValue = " ";
            this.today.Size = new System.Drawing.Size(153, 20);
            this.today.TabIndex = 1;
            this.today.Value = new System.DateTime(2011, 1, 21, 15, 55, 46, 468);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 82;
            this.label3.Text = "Today";
            // 
            // w_year
            // 
            this.w_year.AllowSpace = true;
            this.w_year.AssociatedLookUpName = "";
            this.w_year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_year.ContinuationTextBox = null;
            this.w_year.CustomEnabled = true;
            this.w_year.DataFieldMapping = "";
            this.w_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_year.GetRecordsOnUpDownKeys = false;
            this.w_year.IsDate = false;
            this.w_year.Location = new System.Drawing.Point(208, 212);
            this.w_year.MaxLength = 4;
            this.w_year.Name = "w_year";
            this.w_year.NumberFormat = "###,###,##0.00";
            this.w_year.Postfix = "";
            this.w_year.Prefix = "";
            this.w_year.Size = new System.Drawing.Size(153, 20);
            this.w_year.SkipValidation = false;
            this.w_year.TabIndex = 5;
            this.w_year.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 79;
            this.label2.Text = "Year (YYYY)";
            // 
            // w_brn
            // 
            this.w_brn.AllowSpace = true;
            this.w_brn.AssociatedLookUpName = "";
            this.w_brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_brn.ContinuationTextBox = null;
            this.w_brn.CustomEnabled = true;
            this.w_brn.DataFieldMapping = "";
            this.w_brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_brn.GetRecordsOnUpDownKeys = false;
            this.w_brn.IsDate = false;
            this.w_brn.Location = new System.Drawing.Point(208, 323);
            this.w_brn.MaxLength = 3;
            this.w_brn.Name = "w_brn";
            this.w_brn.NumberFormat = "###,###,##0.00";
            this.w_brn.Postfix = "";
            this.w_brn.Prefix = "";
            this.w_brn.Size = new System.Drawing.Size(153, 20);
            this.w_brn.SkipValidation = false;
            this.w_brn.TabIndex = 9;
            this.w_brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_from
            // 
            this.w_from.AllowSpace = true;
            this.w_from.AssociatedLookUpName = "";
            this.w_from.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_from.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_from.ContinuationTextBox = null;
            this.w_from.CustomEnabled = true;
            this.w_from.DataFieldMapping = "";
            this.w_from.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_from.GetRecordsOnUpDownKeys = false;
            this.w_from.IsDate = false;
            this.w_from.Location = new System.Drawing.Point(208, 269);
            this.w_from.MaxLength = 6;
            this.w_from.Name = "w_from";
            this.w_from.NumberFormat = "###,###,##0.00";
            this.w_from.Postfix = "";
            this.w_from.Prefix = "";
            this.w_from.Size = new System.Drawing.Size(153, 20);
            this.w_from.SkipValidation = false;
            this.w_from.TabIndex = 7;
            this.w_from.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(400, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "Printer",
            "File",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(208, 80);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(153, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(160, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Enter Parameters For Report";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(177, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Report Parameter";
            // 
            // CHRIS_PFund_PersonalProvidentFundBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 466);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PFund_PersonalProvidentFundBalance";
            this.Text = "CHRIS_PersonnelReport_SegmentWiseReport";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLTextBox w_brn;
        private CrplControlLibrary.SLTextBox w_from;
        private CrplControlLibrary.SLTextBox w_year;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLDatePicker today;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox w_dept;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox desname;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox w_pto;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLComboBox mode;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox Copies;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox desformat;
        private System.Windows.Forms.Label label7;
    }
}