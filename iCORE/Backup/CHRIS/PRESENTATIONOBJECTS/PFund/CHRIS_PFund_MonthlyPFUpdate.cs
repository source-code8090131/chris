using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using iCORE.XMS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_MonthlyPFUpdate : ChrisSimpleForm 
    {

        Dictionary<string, object> objVals = new Dictionary<string, object>();
        DataTable dtPF = new DataTable();
        //DataSet ds = new DataSet();
        Result rsltPFund;
        CmnDataManager cmnDM = new CmnDataManager();
            
        
        public CHRIS_PFund_MonthlyPFUpdate()
        {
            InitializeComponent();
        }


        public CHRIS_PFund_MonthlyPFUpdate(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //lblUserName.Text += "   " + ConfigurationManager.AppSettings["database"];
            lblUserName.Text += "  "  +  this.UserName; 
            txtOption.Visible = false;
            
        }



        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //NEED TO HAVE DATA IN SAL_AREAR [SQL] IN ORDER TO TEST PROPERLY//
        private void btnProcess_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            objVals.Clear();
            if (txtPersonnelNo.Text == "")
            {
                this.txtPersonnelNo.Text = null;
            }
 
            objVals.Add("PR_P_NO", this.txtPersonnelNo.Text);
            objVals.Add("PAYROLL_DATE", this.dtpPayrollDate.Text);
            
            
            ds.Tables.Add(SQLManager.GetSPParams(slPanel2.SPName));
            cmnDM.PopulateDataTableFromObject(ds.Tables[0], "CALCULATION",objVals);
            rsltPFund = SQLManager.SelectDataSet(ds, 20000);

            
            if (rsltPFund.isSuccessful)
            {
                if (rsltPFund.dstResult != null && rsltPFund.dstResult.Tables.Count > 0 && rsltPFund.dstResult.Tables[0].Rows.Count > 0)
                {
                    dtPF = rsltPFund.dstResult.Tables[0];
                }
                if (dtPF.Rows.Count > 0)
                {

                    txtPersonnelNo2.Text = dtPF.Rows[0]["PNUM"] == string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PNUM"]);
                    txtPFAREARS.Text = dtPF.Rows[0]["PFAR"] == string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PFAR"]);
                    txtPFund.Text = dtPF.Rows[0]["PFND"] == string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PFND"]);
                    if (txtPersonnelNo2.Text != "0" || txtPersonnelNo2.Text != "")
                    {
                        MessageBox.Show("Process Has Been Completed...", "Forms");
                        return;
                    }

                }
                else
                {
                    txtPersonnelNo.Text = "";
                    dtpPayrollDate.Text = "";
                    txtPersonnelNo2.Text = "";
                    txtPFAREARS.Text = "";
                    txtPFund.Text = "";

                }
            
            }





            //rsltPFund = cmnDM.GetData("CHRIS_SP_PROVIDENTFUNDUPDATE_MANAGER", "CALCULATION", objVals);
            //if (rsltPFund.isSuccessful)
            //{
            //    if (rsltPFund.dstResult.Tables.Count > 0)
            //    {
            //        dtPF = rsltPFund.dstResult.Tables[0];

            //    }
            //    if (dtPF.Rows.Count > 0)
            //    {

            //        txtPersonnelNo2.Text  = dtPF.Rows[0]["PNUM"]==string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PNUM"]);
            //        txtPFAREARS.Text = dtPF.Rows[0]["PFAR"]==string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PFAR"]);
            //        txtPFund.Text = dtPF.Rows[0]["PFND"]==string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PFND"]);
            //        if (txtPersonnelNo2.Text != "0" || txtPersonnelNo2.Text !="" )
            //        {
            //            MessageBox.Show("Process Has Been Completed...", "Forms");
            //            return;
            //        }

            //    }
            //    else
            //    {
            //        txtPersonnelNo.Text = "";
            //        dtpPayrollDate.Text = "";
            //        txtPersonnelNo2.Text = "";
            //        txtPFAREARS.Text = "";
            //        txtPFund.Text = ""; 

            //    }
            //}

            


        }

     


    }
}