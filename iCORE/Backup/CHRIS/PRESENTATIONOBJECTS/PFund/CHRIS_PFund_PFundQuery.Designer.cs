namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    partial class CHRIS_PFund_PFundQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PFund_PFundQuery));
            this.PnlPFund = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtww_date = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbPrPNo = new CrplControlLibrary.LookupButton(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTotalPF = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotIns = new CrplControlLibrary.SLTextBox(this.components);
            this.txtyearPF = new CrplControlLibrary.SLTextBox(this.components);
            this.txtyearIns = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotBnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtyearBnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtdec_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNov_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtoct_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtsep_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtaug_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtjul_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtjun_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtapr_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMay_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMar_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfeb_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotalEmp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPremium = new CrplControlLibrary.SLTextBox(this.components);
            this.txtyearEmp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDec_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNov_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtoct_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtsep_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJul_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtaug_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJun_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMay_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtApr_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMar_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtfeb_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtjan_bnk = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJan_emp = new CrplControlLibrary.SLTextBox(this.components);
            this.txtToatal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAccIntrst = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPfBankcont = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPFbalnce = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBalance = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtname = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnel2 = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPFund.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(623, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(659, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 610);
            this.panel1.Size = new System.Drawing.Size(659, 60);
            // 
            // PnlPFund
            // 
            this.PnlPFund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlPFund.ConcurrentPanels = null;
            this.PnlPFund.Controls.Add(this.slTextBox1);
            this.PnlPFund.Controls.Add(this.dtww_date);
            this.PnlPFund.Controls.Add(this.lbPrPNo);
            this.PnlPFund.Controls.Add(this.label22);
            this.PnlPFund.Controls.Add(this.label21);
            this.PnlPFund.Controls.Add(this.label20);
            this.PnlPFund.Controls.Add(this.label19);
            this.PnlPFund.Controls.Add(this.label18);
            this.PnlPFund.Controls.Add(this.label17);
            this.PnlPFund.Controls.Add(this.label16);
            this.PnlPFund.Controls.Add(this.label15);
            this.PnlPFund.Controls.Add(this.label14);
            this.PnlPFund.Controls.Add(this.label13);
            this.PnlPFund.Controls.Add(this.label12);
            this.PnlPFund.Controls.Add(this.label11);
            this.PnlPFund.Controls.Add(this.label10);
            this.PnlPFund.Controls.Add(this.label9);
            this.PnlPFund.Controls.Add(this.label8);
            this.PnlPFund.Controls.Add(this.label7);
            this.PnlPFund.Controls.Add(this.label6);
            this.PnlPFund.Controls.Add(this.label5);
            this.PnlPFund.Controls.Add(this.label4);
            this.PnlPFund.Controls.Add(this.label3);
            this.PnlPFund.Controls.Add(this.label2);
            this.PnlPFund.Controls.Add(this.label1);
            this.PnlPFund.Controls.Add(this.txtTotalPF);
            this.PnlPFund.Controls.Add(this.txtTotIns);
            this.PnlPFund.Controls.Add(this.txtyearPF);
            this.PnlPFund.Controls.Add(this.txtyearIns);
            this.PnlPFund.Controls.Add(this.txtTotBnk);
            this.PnlPFund.Controls.Add(this.txtyearBnk);
            this.PnlPFund.Controls.Add(this.txtdec_bnk);
            this.PnlPFund.Controls.Add(this.txtNov_bnk);
            this.PnlPFund.Controls.Add(this.txtoct_bnk);
            this.PnlPFund.Controls.Add(this.txtsep_bnk);
            this.PnlPFund.Controls.Add(this.txtaug_bnk);
            this.PnlPFund.Controls.Add(this.txtjul_bnk);
            this.PnlPFund.Controls.Add(this.txtjun_bnk);
            this.PnlPFund.Controls.Add(this.txtapr_bnk);
            this.PnlPFund.Controls.Add(this.txtMay_bnk);
            this.PnlPFund.Controls.Add(this.txtMar_bnk);
            this.PnlPFund.Controls.Add(this.txtfeb_bnk);
            this.PnlPFund.Controls.Add(this.txtTotalEmp);
            this.PnlPFund.Controls.Add(this.txtPremium);
            this.PnlPFund.Controls.Add(this.txtyearEmp);
            this.PnlPFund.Controls.Add(this.txtDec_emp);
            this.PnlPFund.Controls.Add(this.txtNov_emp);
            this.PnlPFund.Controls.Add(this.txtoct_emp);
            this.PnlPFund.Controls.Add(this.txtsep_emp);
            this.PnlPFund.Controls.Add(this.txtJul_emp);
            this.PnlPFund.Controls.Add(this.txtaug_emp);
            this.PnlPFund.Controls.Add(this.txtJun_emp);
            this.PnlPFund.Controls.Add(this.txtMay_emp);
            this.PnlPFund.Controls.Add(this.txtApr_emp);
            this.PnlPFund.Controls.Add(this.txtMar_emp);
            this.PnlPFund.Controls.Add(this.txtfeb_emp);
            this.PnlPFund.Controls.Add(this.txtjan_bnk);
            this.PnlPFund.Controls.Add(this.txtJan_emp);
            this.PnlPFund.Controls.Add(this.txtToatal);
            this.PnlPFund.Controls.Add(this.txtAccIntrst);
            this.PnlPFund.Controls.Add(this.txtPfBankcont);
            this.PnlPFund.Controls.Add(this.txtPFbalnce);
            this.PnlPFund.Controls.Add(this.txtBalance);
            this.PnlPFund.Controls.Add(this.txtBranch);
            this.PnlPFund.Controls.Add(this.txtname);
            this.PnlPFund.Controls.Add(this.txtPersonnel);
            this.PnlPFund.DataManager = null;
            this.PnlPFund.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPFund.DependentPanels = null;
            this.PnlPFund.DisableDependentLoad = false;
            this.PnlPFund.EnableDelete = true;
            this.PnlPFund.EnableInsert = true;
            this.PnlPFund.EnableQuery = false;
            this.PnlPFund.EnableUpdate = true;
            this.PnlPFund.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PFUNDCommand";
            this.PnlPFund.Location = new System.Drawing.Point(12, 131);
            this.PnlPFund.MasterPanel = null;
            this.PnlPFund.Name = "PnlPFund";
            this.PnlPFund.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPFund.Size = new System.Drawing.Size(635, 474);
            this.PnlPFund.SPName = "CHRIS_SP_PFUND_MANAGER";
            this.PnlPFund.TabIndex = 0;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(297, 62);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(1, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 65;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // dtww_date
            // 
            this.dtww_date.CustomEnabled = true;
            this.dtww_date.CustomFormat = "dd/MM/yyyy";
            this.dtww_date.DataFieldMapping = "";
            this.dtww_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtww_date.HasChanges = true;
            this.dtww_date.Location = new System.Drawing.Point(449, 130);
            this.dtww_date.Name = "dtww_date";
            this.dtww_date.NullValue = " ";
            this.dtww_date.Size = new System.Drawing.Size(120, 20);
            this.dtww_date.TabIndex = 64;
            this.dtww_date.Value = new System.DateTime(2011, 1, 31, 0, 0, 0, 0);
            this.dtww_date.Visible = false;
            // 
            // lbPrPNo
            // 
            this.lbPrPNo.ActionLOVExists = "prpNoExists";
            this.lbPrPNo.ActionType = "prpNo";
            this.lbPrPNo.ConditionalFields = "";
            this.lbPrPNo.CustomEnabled = true;
            this.lbPrPNo.DataFieldMapping = "";
            this.lbPrPNo.DependentLovControls = "";
            this.lbPrPNo.HiddenColumns = "";
            this.lbPrPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbPrPNo.Image")));
            this.lbPrPNo.LoadDependentEntities = false;
            this.lbPrPNo.Location = new System.Drawing.Point(218, 11);
            this.lbPrPNo.LookUpTitle = null;
            this.lbPrPNo.Name = "lbPrPNo";
            this.lbPrPNo.Size = new System.Drawing.Size(26, 21);
            this.lbPrPNo.SkipValidationOnLeave = false;
            this.lbPrPNo.SPName = "CHRIS_SP_PFUND_MANAGER";
            this.lbPrPNo.TabIndex = 63;
            this.lbPrPNo.TabStop = false;
            this.lbPrPNo.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(308, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 13);
            this.label22.TabIndex = 62;
            this.label22.Text = "Acc . Interest";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(203, 46);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 13);
            this.label21.TabIndex = 61;
            this.label21.Text = "Bank Cont. ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(86, 46);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 60;
            this.label20.Text = "Emp. Cont .";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(32, 441);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 13);
            this.label19.TabIndex = 59;
            this.label19.Text = "Total :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 416);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 58;
            this.label18.Text = "Premium :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 391);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 57;
            this.label17.Text = "Y-To - Dt :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(38, 366);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 56;
            this.label16.Text = "Dec :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(38, 341);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 13);
            this.label15.TabIndex = 55;
            this.label15.Text = "Nov :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(41, 316);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 54;
            this.label14.Text = "Oct :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(39, 291);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 53;
            this.label13.Text = "Sep :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(39, 266);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 52;
            this.label12.Text = "Aug :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(45, 241);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Jul :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(38, 191);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "May :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(41, 216);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "Jun :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(42, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 49;
            this.label8.Text = "Apr :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "Mar :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "Feb :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(41, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "Jan :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Balance :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(477, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "Balance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(483, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Branch :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Personnel No. :";
            // 
            // txtTotalPF
            // 
            this.txtTotalPF.AllowSpace = true;
            this.txtTotalPF.AssociatedLookUpName = "";
            this.txtTotalPF.BackColor = System.Drawing.SystemColors.Control;
            this.txtTotalPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalPF.ContinuationTextBox = null;
            this.txtTotalPF.CustomEnabled = true;
            this.txtTotalPF.DataFieldMapping = "PF_TOT";
            this.txtTotalPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPF.GetRecordsOnUpDownKeys = false;
            this.txtTotalPF.IsDate = false;
            this.txtTotalPF.Location = new System.Drawing.Point(440, 437);
            this.txtTotalPF.Name = "txtTotalPF";
            this.txtTotalPF.NumberFormat = "###,###,##0.00";
            this.txtTotalPF.Postfix = "";
            this.txtTotalPF.Prefix = "";
            this.txtTotalPF.ReadOnly = true;
            this.txtTotalPF.Size = new System.Drawing.Size(129, 20);
            this.txtTotalPF.SkipValidation = false;
            this.txtTotalPF.TabIndex = 41;
            this.txtTotalPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalPF.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtTotIns
            // 
            this.txtTotIns.AllowSpace = true;
            this.txtTotIns.AssociatedLookUpName = "";
            this.txtTotIns.BackColor = System.Drawing.SystemColors.Control;
            this.txtTotIns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotIns.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotIns.ContinuationTextBox = null;
            this.txtTotIns.CustomEnabled = true;
            this.txtTotIns.DataFieldMapping = "INS_TOT";
            this.txtTotIns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotIns.GetRecordsOnUpDownKeys = false;
            this.txtTotIns.IsDate = false;
            this.txtTotIns.Location = new System.Drawing.Point(297, 437);
            this.txtTotIns.Name = "txtTotIns";
            this.txtTotIns.NumberFormat = "###,###,##0.00";
            this.txtTotIns.Postfix = "";
            this.txtTotIns.Prefix = "";
            this.txtTotIns.ReadOnly = true;
            this.txtTotIns.Size = new System.Drawing.Size(137, 20);
            this.txtTotIns.SkipValidation = false;
            this.txtTotIns.TabIndex = 40;
            this.txtTotIns.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotIns.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtyearPF
            // 
            this.txtyearPF.AllowSpace = true;
            this.txtyearPF.AssociatedLookUpName = "";
            this.txtyearPF.BackColor = System.Drawing.SystemColors.Control;
            this.txtyearPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtyearPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtyearPF.ContinuationTextBox = null;
            this.txtyearPF.CustomEnabled = true;
            this.txtyearPF.DataFieldMapping = "PF_YEAR_TOT";
            this.txtyearPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtyearPF.GetRecordsOnUpDownKeys = false;
            this.txtyearPF.IsDate = false;
            this.txtyearPF.Location = new System.Drawing.Point(439, 387);
            this.txtyearPF.Name = "txtyearPF";
            this.txtyearPF.NumberFormat = "###,###,##0.00";
            this.txtyearPF.Postfix = "";
            this.txtyearPF.Prefix = "";
            this.txtyearPF.ReadOnly = true;
            this.txtyearPF.Size = new System.Drawing.Size(129, 20);
            this.txtyearPF.SkipValidation = false;
            this.txtyearPF.TabIndex = 39;
            this.txtyearPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtyearPF.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtyearIns
            // 
            this.txtyearIns.AllowSpace = true;
            this.txtyearIns.AssociatedLookUpName = "";
            this.txtyearIns.BackColor = System.Drawing.SystemColors.Control;
            this.txtyearIns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtyearIns.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtyearIns.ContinuationTextBox = null;
            this.txtyearIns.CustomEnabled = true;
            this.txtyearIns.DataFieldMapping = "INS_YEAR_TOT";
            this.txtyearIns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtyearIns.GetRecordsOnUpDownKeys = false;
            this.txtyearIns.IsDate = false;
            this.txtyearIns.Location = new System.Drawing.Point(296, 387);
            this.txtyearIns.Name = "txtyearIns";
            this.txtyearIns.NumberFormat = "###,###,##0.00";
            this.txtyearIns.Postfix = "";
            this.txtyearIns.Prefix = "";
            this.txtyearIns.ReadOnly = true;
            this.txtyearIns.Size = new System.Drawing.Size(137, 20);
            this.txtyearIns.SkipValidation = false;
            this.txtyearIns.TabIndex = 38;
            this.txtyearIns.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtyearIns.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtTotBnk
            // 
            this.txtTotBnk.AllowSpace = true;
            this.txtTotBnk.AssociatedLookUpName = "";
            this.txtTotBnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtTotBnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotBnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotBnk.ContinuationTextBox = null;
            this.txtTotBnk.CustomEnabled = true;
            this.txtTotBnk.DataFieldMapping = "BNK_TOT";
            this.txtTotBnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotBnk.GetRecordsOnUpDownKeys = false;
            this.txtTotBnk.IsDate = false;
            this.txtTotBnk.Location = new System.Drawing.Point(188, 437);
            this.txtTotBnk.Name = "txtTotBnk";
            this.txtTotBnk.NumberFormat = "###,###,##0.00";
            this.txtTotBnk.Postfix = "";
            this.txtTotBnk.Prefix = "";
            this.txtTotBnk.ReadOnly = true;
            this.txtTotBnk.Size = new System.Drawing.Size(100, 20);
            this.txtTotBnk.SkipValidation = false;
            this.txtTotBnk.TabIndex = 37;
            this.txtTotBnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotBnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtyearBnk
            // 
            this.txtyearBnk.AllowSpace = true;
            this.txtyearBnk.AssociatedLookUpName = "";
            this.txtyearBnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtyearBnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtyearBnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtyearBnk.ContinuationTextBox = null;
            this.txtyearBnk.CustomEnabled = true;
            this.txtyearBnk.DataFieldMapping = "BNK_YEAR_TOT";
            this.txtyearBnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtyearBnk.GetRecordsOnUpDownKeys = false;
            this.txtyearBnk.IsDate = false;
            this.txtyearBnk.Location = new System.Drawing.Point(188, 387);
            this.txtyearBnk.Name = "txtyearBnk";
            this.txtyearBnk.NumberFormat = "###,###,##0.00";
            this.txtyearBnk.Postfix = "";
            this.txtyearBnk.Prefix = "";
            this.txtyearBnk.ReadOnly = true;
            this.txtyearBnk.Size = new System.Drawing.Size(100, 20);
            this.txtyearBnk.SkipValidation = false;
            this.txtyearBnk.TabIndex = 35;
            this.txtyearBnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtyearBnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtdec_bnk
            // 
            this.txtdec_bnk.AllowSpace = true;
            this.txtdec_bnk.AssociatedLookUpName = "";
            this.txtdec_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtdec_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdec_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdec_bnk.ContinuationTextBox = null;
            this.txtdec_bnk.CustomEnabled = true;
            this.txtdec_bnk.DataFieldMapping = "BNK_CONT12";
            this.txtdec_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdec_bnk.GetRecordsOnUpDownKeys = false;
            this.txtdec_bnk.IsDate = false;
            this.txtdec_bnk.Location = new System.Drawing.Point(188, 362);
            this.txtdec_bnk.Name = "txtdec_bnk";
            this.txtdec_bnk.NumberFormat = "###,###,##0.00";
            this.txtdec_bnk.Postfix = "";
            this.txtdec_bnk.Prefix = "";
            this.txtdec_bnk.ReadOnly = true;
            this.txtdec_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtdec_bnk.SkipValidation = false;
            this.txtdec_bnk.TabIndex = 34;
            this.txtdec_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdec_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtNov_bnk
            // 
            this.txtNov_bnk.AllowSpace = true;
            this.txtNov_bnk.AssociatedLookUpName = "";
            this.txtNov_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtNov_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNov_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNov_bnk.ContinuationTextBox = null;
            this.txtNov_bnk.CustomEnabled = true;
            this.txtNov_bnk.DataFieldMapping = "BNK_CONT11";
            this.txtNov_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov_bnk.GetRecordsOnUpDownKeys = false;
            this.txtNov_bnk.IsDate = false;
            this.txtNov_bnk.Location = new System.Drawing.Point(188, 337);
            this.txtNov_bnk.Name = "txtNov_bnk";
            this.txtNov_bnk.NumberFormat = "###,###,##0.00";
            this.txtNov_bnk.Postfix = "";
            this.txtNov_bnk.Prefix = "";
            this.txtNov_bnk.ReadOnly = true;
            this.txtNov_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtNov_bnk.SkipValidation = false;
            this.txtNov_bnk.TabIndex = 33;
            this.txtNov_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNov_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtoct_bnk
            // 
            this.txtoct_bnk.AllowSpace = true;
            this.txtoct_bnk.AssociatedLookUpName = "";
            this.txtoct_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtoct_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoct_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtoct_bnk.ContinuationTextBox = null;
            this.txtoct_bnk.CustomEnabled = true;
            this.txtoct_bnk.DataFieldMapping = "BNK_CONT10";
            this.txtoct_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoct_bnk.GetRecordsOnUpDownKeys = false;
            this.txtoct_bnk.IsDate = false;
            this.txtoct_bnk.Location = new System.Drawing.Point(188, 312);
            this.txtoct_bnk.Name = "txtoct_bnk";
            this.txtoct_bnk.NumberFormat = "###,###,##0.00";
            this.txtoct_bnk.Postfix = "";
            this.txtoct_bnk.Prefix = "";
            this.txtoct_bnk.ReadOnly = true;
            this.txtoct_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtoct_bnk.SkipValidation = false;
            this.txtoct_bnk.TabIndex = 32;
            this.txtoct_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtoct_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtsep_bnk
            // 
            this.txtsep_bnk.AllowSpace = true;
            this.txtsep_bnk.AssociatedLookUpName = "";
            this.txtsep_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtsep_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsep_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtsep_bnk.ContinuationTextBox = null;
            this.txtsep_bnk.CustomEnabled = true;
            this.txtsep_bnk.DataFieldMapping = "BNK_CONT9";
            this.txtsep_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsep_bnk.GetRecordsOnUpDownKeys = false;
            this.txtsep_bnk.IsDate = false;
            this.txtsep_bnk.Location = new System.Drawing.Point(188, 287);
            this.txtsep_bnk.Name = "txtsep_bnk";
            this.txtsep_bnk.NumberFormat = "###,###,##0.00";
            this.txtsep_bnk.Postfix = "";
            this.txtsep_bnk.Prefix = "";
            this.txtsep_bnk.ReadOnly = true;
            this.txtsep_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtsep_bnk.SkipValidation = false;
            this.txtsep_bnk.TabIndex = 31;
            this.txtsep_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtsep_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtaug_bnk
            // 
            this.txtaug_bnk.AllowSpace = true;
            this.txtaug_bnk.AssociatedLookUpName = "";
            this.txtaug_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtaug_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaug_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtaug_bnk.ContinuationTextBox = null;
            this.txtaug_bnk.CustomEnabled = true;
            this.txtaug_bnk.DataFieldMapping = "BNK_CONT8";
            this.txtaug_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaug_bnk.GetRecordsOnUpDownKeys = false;
            this.txtaug_bnk.IsDate = false;
            this.txtaug_bnk.Location = new System.Drawing.Point(188, 262);
            this.txtaug_bnk.Name = "txtaug_bnk";
            this.txtaug_bnk.NumberFormat = "###,###,##0.00";
            this.txtaug_bnk.Postfix = "";
            this.txtaug_bnk.Prefix = "";
            this.txtaug_bnk.ReadOnly = true;
            this.txtaug_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtaug_bnk.SkipValidation = false;
            this.txtaug_bnk.TabIndex = 30;
            this.txtaug_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtaug_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtjul_bnk
            // 
            this.txtjul_bnk.AllowSpace = true;
            this.txtjul_bnk.AssociatedLookUpName = "";
            this.txtjul_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtjul_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtjul_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtjul_bnk.ContinuationTextBox = null;
            this.txtjul_bnk.CustomEnabled = true;
            this.txtjul_bnk.DataFieldMapping = "BNK_CONT7";
            this.txtjul_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtjul_bnk.GetRecordsOnUpDownKeys = false;
            this.txtjul_bnk.IsDate = false;
            this.txtjul_bnk.Location = new System.Drawing.Point(188, 237);
            this.txtjul_bnk.Name = "txtjul_bnk";
            this.txtjul_bnk.NumberFormat = "###,###,##0.00";
            this.txtjul_bnk.Postfix = "";
            this.txtjul_bnk.Prefix = "";
            this.txtjul_bnk.ReadOnly = true;
            this.txtjul_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtjul_bnk.SkipValidation = false;
            this.txtjul_bnk.TabIndex = 29;
            this.txtjul_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtjul_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtjun_bnk
            // 
            this.txtjun_bnk.AllowSpace = true;
            this.txtjun_bnk.AssociatedLookUpName = "";
            this.txtjun_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtjun_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtjun_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtjun_bnk.ContinuationTextBox = null;
            this.txtjun_bnk.CustomEnabled = true;
            this.txtjun_bnk.DataFieldMapping = "BNK_CONT6";
            this.txtjun_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtjun_bnk.GetRecordsOnUpDownKeys = false;
            this.txtjun_bnk.IsDate = false;
            this.txtjun_bnk.Location = new System.Drawing.Point(188, 212);
            this.txtjun_bnk.Name = "txtjun_bnk";
            this.txtjun_bnk.NumberFormat = "###,###,##0.00";
            this.txtjun_bnk.Postfix = "";
            this.txtjun_bnk.Prefix = "";
            this.txtjun_bnk.ReadOnly = true;
            this.txtjun_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtjun_bnk.SkipValidation = false;
            this.txtjun_bnk.TabIndex = 28;
            this.txtjun_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtjun_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtapr_bnk
            // 
            this.txtapr_bnk.AllowSpace = true;
            this.txtapr_bnk.AssociatedLookUpName = "";
            this.txtapr_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtapr_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtapr_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtapr_bnk.ContinuationTextBox = null;
            this.txtapr_bnk.CustomEnabled = true;
            this.txtapr_bnk.DataFieldMapping = "BNK_CONT4";
            this.txtapr_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtapr_bnk.GetRecordsOnUpDownKeys = false;
            this.txtapr_bnk.IsDate = false;
            this.txtapr_bnk.Location = new System.Drawing.Point(188, 162);
            this.txtapr_bnk.Name = "txtapr_bnk";
            this.txtapr_bnk.NumberFormat = "###,###,##0.00";
            this.txtapr_bnk.Postfix = "";
            this.txtapr_bnk.Prefix = "";
            this.txtapr_bnk.ReadOnly = true;
            this.txtapr_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtapr_bnk.SkipValidation = false;
            this.txtapr_bnk.TabIndex = 27;
            this.txtapr_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtapr_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtMay_bnk
            // 
            this.txtMay_bnk.AllowSpace = true;
            this.txtMay_bnk.AssociatedLookUpName = "";
            this.txtMay_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtMay_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMay_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMay_bnk.ContinuationTextBox = null;
            this.txtMay_bnk.CustomEnabled = true;
            this.txtMay_bnk.DataFieldMapping = "BNK_CONT5";
            this.txtMay_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMay_bnk.GetRecordsOnUpDownKeys = false;
            this.txtMay_bnk.IsDate = false;
            this.txtMay_bnk.Location = new System.Drawing.Point(188, 187);
            this.txtMay_bnk.Name = "txtMay_bnk";
            this.txtMay_bnk.NumberFormat = "###,###,##0.00";
            this.txtMay_bnk.Postfix = "";
            this.txtMay_bnk.Prefix = "";
            this.txtMay_bnk.ReadOnly = true;
            this.txtMay_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtMay_bnk.SkipValidation = false;
            this.txtMay_bnk.TabIndex = 26;
            this.txtMay_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMay_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtMar_bnk
            // 
            this.txtMar_bnk.AllowSpace = true;
            this.txtMar_bnk.AssociatedLookUpName = "";
            this.txtMar_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtMar_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMar_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMar_bnk.ContinuationTextBox = null;
            this.txtMar_bnk.CustomEnabled = true;
            this.txtMar_bnk.DataFieldMapping = "BNK_CONT3";
            this.txtMar_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMar_bnk.GetRecordsOnUpDownKeys = false;
            this.txtMar_bnk.IsDate = false;
            this.txtMar_bnk.Location = new System.Drawing.Point(188, 137);
            this.txtMar_bnk.Name = "txtMar_bnk";
            this.txtMar_bnk.NumberFormat = "###,###,##0.00";
            this.txtMar_bnk.Postfix = "";
            this.txtMar_bnk.Prefix = "";
            this.txtMar_bnk.ReadOnly = true;
            this.txtMar_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtMar_bnk.SkipValidation = false;
            this.txtMar_bnk.TabIndex = 25;
            this.txtMar_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMar_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtfeb_bnk
            // 
            this.txtfeb_bnk.AllowSpace = true;
            this.txtfeb_bnk.AssociatedLookUpName = "";
            this.txtfeb_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtfeb_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfeb_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfeb_bnk.ContinuationTextBox = null;
            this.txtfeb_bnk.CustomEnabled = true;
            this.txtfeb_bnk.DataFieldMapping = "BNK_CONT2";
            this.txtfeb_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfeb_bnk.GetRecordsOnUpDownKeys = false;
            this.txtfeb_bnk.IsDate = false;
            this.txtfeb_bnk.Location = new System.Drawing.Point(188, 112);
            this.txtfeb_bnk.Name = "txtfeb_bnk";
            this.txtfeb_bnk.NumberFormat = "###,###,##0.00";
            this.txtfeb_bnk.Postfix = "";
            this.txtfeb_bnk.Prefix = "";
            this.txtfeb_bnk.ReadOnly = true;
            this.txtfeb_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtfeb_bnk.SkipValidation = false;
            this.txtfeb_bnk.TabIndex = 24;
            this.txtfeb_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfeb_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtTotalEmp
            // 
            this.txtTotalEmp.AllowSpace = true;
            this.txtTotalEmp.AssociatedLookUpName = "";
            this.txtTotalEmp.BackColor = System.Drawing.SystemColors.Control;
            this.txtTotalEmp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalEmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalEmp.ContinuationTextBox = null;
            this.txtTotalEmp.CustomEnabled = true;
            this.txtTotalEmp.DataFieldMapping = "EMP_TOT";
            this.txtTotalEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalEmp.GetRecordsOnUpDownKeys = false;
            this.txtTotalEmp.IsDate = false;
            this.txtTotalEmp.Location = new System.Drawing.Point(82, 437);
            this.txtTotalEmp.Name = "txtTotalEmp";
            this.txtTotalEmp.NumberFormat = "###,###,##0.00";
            this.txtTotalEmp.Postfix = "";
            this.txtTotalEmp.Prefix = "";
            this.txtTotalEmp.ReadOnly = true;
            this.txtTotalEmp.Size = new System.Drawing.Size(100, 20);
            this.txtTotalEmp.SkipValidation = false;
            this.txtTotalEmp.TabIndex = 23;
            this.txtTotalEmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalEmp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPremium
            // 
            this.txtPremium.AllowSpace = true;
            this.txtPremium.AssociatedLookUpName = "";
            this.txtPremium.BackColor = System.Drawing.SystemColors.Control;
            this.txtPremium.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPremium.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPremium.ContinuationTextBox = null;
            this.txtPremium.CustomEnabled = true;
            this.txtPremium.DataFieldMapping = "F_PREMIUM";
            this.txtPremium.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPremium.GetRecordsOnUpDownKeys = false;
            this.txtPremium.IsDate = false;
            this.txtPremium.Location = new System.Drawing.Point(82, 412);
            this.txtPremium.Name = "txtPremium";
            this.txtPremium.NumberFormat = "###,###,##0.00";
            this.txtPremium.Postfix = "";
            this.txtPremium.Prefix = "";
            this.txtPremium.ReadOnly = true;
            this.txtPremium.Size = new System.Drawing.Size(100, 20);
            this.txtPremium.SkipValidation = false;
            this.txtPremium.TabIndex = 22;
            this.txtPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPremium.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtyearEmp
            // 
            this.txtyearEmp.AllowSpace = true;
            this.txtyearEmp.AssociatedLookUpName = "";
            this.txtyearEmp.BackColor = System.Drawing.SystemColors.Control;
            this.txtyearEmp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtyearEmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtyearEmp.ContinuationTextBox = null;
            this.txtyearEmp.CustomEnabled = true;
            this.txtyearEmp.DataFieldMapping = "EMP_YEAR_TOT";
            this.txtyearEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtyearEmp.GetRecordsOnUpDownKeys = false;
            this.txtyearEmp.IsDate = false;
            this.txtyearEmp.Location = new System.Drawing.Point(82, 387);
            this.txtyearEmp.Name = "txtyearEmp";
            this.txtyearEmp.NumberFormat = "###,###,##0.00";
            this.txtyearEmp.Postfix = "";
            this.txtyearEmp.Prefix = "";
            this.txtyearEmp.ReadOnly = true;
            this.txtyearEmp.Size = new System.Drawing.Size(100, 20);
            this.txtyearEmp.SkipValidation = false;
            this.txtyearEmp.TabIndex = 21;
            this.txtyearEmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtyearEmp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtDec_emp
            // 
            this.txtDec_emp.AllowSpace = true;
            this.txtDec_emp.AssociatedLookUpName = "";
            this.txtDec_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtDec_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDec_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDec_emp.ContinuationTextBox = null;
            this.txtDec_emp.CustomEnabled = true;
            this.txtDec_emp.DataFieldMapping = "EMP_CONT12";
            this.txtDec_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDec_emp.GetRecordsOnUpDownKeys = false;
            this.txtDec_emp.IsDate = false;
            this.txtDec_emp.Location = new System.Drawing.Point(82, 362);
            this.txtDec_emp.Name = "txtDec_emp";
            this.txtDec_emp.NumberFormat = "###,###,##0.00";
            this.txtDec_emp.Postfix = "";
            this.txtDec_emp.Prefix = "";
            this.txtDec_emp.ReadOnly = true;
            this.txtDec_emp.Size = new System.Drawing.Size(100, 20);
            this.txtDec_emp.SkipValidation = false;
            this.txtDec_emp.TabIndex = 20;
            this.txtDec_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDec_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtNov_emp
            // 
            this.txtNov_emp.AllowSpace = true;
            this.txtNov_emp.AssociatedLookUpName = "";
            this.txtNov_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtNov_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNov_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNov_emp.ContinuationTextBox = null;
            this.txtNov_emp.CustomEnabled = true;
            this.txtNov_emp.DataFieldMapping = "EMP_CONT11";
            this.txtNov_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNov_emp.GetRecordsOnUpDownKeys = false;
            this.txtNov_emp.IsDate = false;
            this.txtNov_emp.Location = new System.Drawing.Point(82, 337);
            this.txtNov_emp.Name = "txtNov_emp";
            this.txtNov_emp.NumberFormat = "###,###,##0.00";
            this.txtNov_emp.Postfix = "";
            this.txtNov_emp.Prefix = "";
            this.txtNov_emp.ReadOnly = true;
            this.txtNov_emp.Size = new System.Drawing.Size(100, 20);
            this.txtNov_emp.SkipValidation = false;
            this.txtNov_emp.TabIndex = 19;
            this.txtNov_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNov_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtoct_emp
            // 
            this.txtoct_emp.AllowSpace = true;
            this.txtoct_emp.AssociatedLookUpName = "";
            this.txtoct_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtoct_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoct_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtoct_emp.ContinuationTextBox = null;
            this.txtoct_emp.CustomEnabled = true;
            this.txtoct_emp.DataFieldMapping = "EMP_CONT10";
            this.txtoct_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoct_emp.GetRecordsOnUpDownKeys = false;
            this.txtoct_emp.IsDate = false;
            this.txtoct_emp.Location = new System.Drawing.Point(82, 312);
            this.txtoct_emp.Name = "txtoct_emp";
            this.txtoct_emp.NumberFormat = "###,###,##0.00";
            this.txtoct_emp.Postfix = "";
            this.txtoct_emp.Prefix = "";
            this.txtoct_emp.ReadOnly = true;
            this.txtoct_emp.Size = new System.Drawing.Size(100, 20);
            this.txtoct_emp.SkipValidation = false;
            this.txtoct_emp.TabIndex = 18;
            this.txtoct_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtoct_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtsep_emp
            // 
            this.txtsep_emp.AllowSpace = true;
            this.txtsep_emp.AssociatedLookUpName = "";
            this.txtsep_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtsep_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsep_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtsep_emp.ContinuationTextBox = null;
            this.txtsep_emp.CustomEnabled = true;
            this.txtsep_emp.DataFieldMapping = "EMP_CONT9";
            this.txtsep_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsep_emp.GetRecordsOnUpDownKeys = false;
            this.txtsep_emp.IsDate = false;
            this.txtsep_emp.Location = new System.Drawing.Point(82, 287);
            this.txtsep_emp.Name = "txtsep_emp";
            this.txtsep_emp.NumberFormat = "###,###,##0.00";
            this.txtsep_emp.Postfix = "";
            this.txtsep_emp.Prefix = "";
            this.txtsep_emp.ReadOnly = true;
            this.txtsep_emp.Size = new System.Drawing.Size(100, 20);
            this.txtsep_emp.SkipValidation = false;
            this.txtsep_emp.TabIndex = 17;
            this.txtsep_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtsep_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtJul_emp
            // 
            this.txtJul_emp.AllowSpace = true;
            this.txtJul_emp.AssociatedLookUpName = "";
            this.txtJul_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtJul_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJul_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJul_emp.ContinuationTextBox = null;
            this.txtJul_emp.CustomEnabled = true;
            this.txtJul_emp.DataFieldMapping = "EMP_CONT7";
            this.txtJul_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJul_emp.GetRecordsOnUpDownKeys = false;
            this.txtJul_emp.IsDate = false;
            this.txtJul_emp.Location = new System.Drawing.Point(82, 237);
            this.txtJul_emp.Name = "txtJul_emp";
            this.txtJul_emp.NumberFormat = "###,###,##0.00";
            this.txtJul_emp.Postfix = "";
            this.txtJul_emp.Prefix = "";
            this.txtJul_emp.ReadOnly = true;
            this.txtJul_emp.Size = new System.Drawing.Size(100, 20);
            this.txtJul_emp.SkipValidation = false;
            this.txtJul_emp.TabIndex = 16;
            this.txtJul_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJul_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtaug_emp
            // 
            this.txtaug_emp.AllowSpace = true;
            this.txtaug_emp.AssociatedLookUpName = "";
            this.txtaug_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtaug_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaug_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtaug_emp.ContinuationTextBox = null;
            this.txtaug_emp.CustomEnabled = true;
            this.txtaug_emp.DataFieldMapping = "EMP_CONT8";
            this.txtaug_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaug_emp.GetRecordsOnUpDownKeys = false;
            this.txtaug_emp.IsDate = false;
            this.txtaug_emp.Location = new System.Drawing.Point(82, 262);
            this.txtaug_emp.Name = "txtaug_emp";
            this.txtaug_emp.NumberFormat = "###,###,##0.00";
            this.txtaug_emp.Postfix = "";
            this.txtaug_emp.Prefix = "";
            this.txtaug_emp.ReadOnly = true;
            this.txtaug_emp.Size = new System.Drawing.Size(100, 20);
            this.txtaug_emp.SkipValidation = false;
            this.txtaug_emp.TabIndex = 15;
            this.txtaug_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtaug_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtJun_emp
            // 
            this.txtJun_emp.AllowSpace = true;
            this.txtJun_emp.AssociatedLookUpName = "";
            this.txtJun_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtJun_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJun_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJun_emp.ContinuationTextBox = null;
            this.txtJun_emp.CustomEnabled = true;
            this.txtJun_emp.DataFieldMapping = "EMP_CONT6";
            this.txtJun_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJun_emp.GetRecordsOnUpDownKeys = false;
            this.txtJun_emp.IsDate = false;
            this.txtJun_emp.Location = new System.Drawing.Point(82, 212);
            this.txtJun_emp.Name = "txtJun_emp";
            this.txtJun_emp.NumberFormat = "###,###,##0.00";
            this.txtJun_emp.Postfix = "";
            this.txtJun_emp.Prefix = "";
            this.txtJun_emp.ReadOnly = true;
            this.txtJun_emp.Size = new System.Drawing.Size(100, 20);
            this.txtJun_emp.SkipValidation = false;
            this.txtJun_emp.TabIndex = 14;
            this.txtJun_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJun_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtMay_emp
            // 
            this.txtMay_emp.AllowSpace = true;
            this.txtMay_emp.AssociatedLookUpName = "";
            this.txtMay_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtMay_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMay_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMay_emp.ContinuationTextBox = null;
            this.txtMay_emp.CustomEnabled = true;
            this.txtMay_emp.DataFieldMapping = "EMP_CONT5";
            this.txtMay_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMay_emp.GetRecordsOnUpDownKeys = false;
            this.txtMay_emp.IsDate = false;
            this.txtMay_emp.Location = new System.Drawing.Point(82, 187);
            this.txtMay_emp.Name = "txtMay_emp";
            this.txtMay_emp.NumberFormat = "###,###,##0.00";
            this.txtMay_emp.Postfix = "";
            this.txtMay_emp.Prefix = "";
            this.txtMay_emp.ReadOnly = true;
            this.txtMay_emp.Size = new System.Drawing.Size(100, 20);
            this.txtMay_emp.SkipValidation = false;
            this.txtMay_emp.TabIndex = 13;
            this.txtMay_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMay_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtApr_emp
            // 
            this.txtApr_emp.AllowSpace = true;
            this.txtApr_emp.AssociatedLookUpName = "";
            this.txtApr_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtApr_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApr_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApr_emp.ContinuationTextBox = null;
            this.txtApr_emp.CustomEnabled = true;
            this.txtApr_emp.DataFieldMapping = "EMP_CONT4";
            this.txtApr_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApr_emp.GetRecordsOnUpDownKeys = false;
            this.txtApr_emp.IsDate = false;
            this.txtApr_emp.Location = new System.Drawing.Point(82, 162);
            this.txtApr_emp.Name = "txtApr_emp";
            this.txtApr_emp.NumberFormat = "###,###,##0.00";
            this.txtApr_emp.Postfix = "";
            this.txtApr_emp.Prefix = "";
            this.txtApr_emp.ReadOnly = true;
            this.txtApr_emp.Size = new System.Drawing.Size(100, 20);
            this.txtApr_emp.SkipValidation = false;
            this.txtApr_emp.TabIndex = 12;
            this.txtApr_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtApr_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtMar_emp
            // 
            this.txtMar_emp.AllowSpace = true;
            this.txtMar_emp.AssociatedLookUpName = "";
            this.txtMar_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtMar_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMar_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMar_emp.ContinuationTextBox = null;
            this.txtMar_emp.CustomEnabled = true;
            this.txtMar_emp.DataFieldMapping = "EMP_CONT3";
            this.txtMar_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMar_emp.GetRecordsOnUpDownKeys = false;
            this.txtMar_emp.IsDate = false;
            this.txtMar_emp.Location = new System.Drawing.Point(82, 137);
            this.txtMar_emp.Name = "txtMar_emp";
            this.txtMar_emp.NumberFormat = "###,###,##0.00";
            this.txtMar_emp.Postfix = "";
            this.txtMar_emp.Prefix = "";
            this.txtMar_emp.ReadOnly = true;
            this.txtMar_emp.Size = new System.Drawing.Size(100, 20);
            this.txtMar_emp.SkipValidation = false;
            this.txtMar_emp.TabIndex = 11;
            this.txtMar_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMar_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtfeb_emp
            // 
            this.txtfeb_emp.AllowSpace = true;
            this.txtfeb_emp.AssociatedLookUpName = "";
            this.txtfeb_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtfeb_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfeb_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfeb_emp.ContinuationTextBox = null;
            this.txtfeb_emp.CustomEnabled = true;
            this.txtfeb_emp.DataFieldMapping = "EMP_CONT2";
            this.txtfeb_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfeb_emp.GetRecordsOnUpDownKeys = false;
            this.txtfeb_emp.IsDate = false;
            this.txtfeb_emp.Location = new System.Drawing.Point(82, 112);
            this.txtfeb_emp.Name = "txtfeb_emp";
            this.txtfeb_emp.NumberFormat = "###,###,##0.00";
            this.txtfeb_emp.Postfix = "";
            this.txtfeb_emp.Prefix = "";
            this.txtfeb_emp.ReadOnly = true;
            this.txtfeb_emp.Size = new System.Drawing.Size(100, 20);
            this.txtfeb_emp.SkipValidation = false;
            this.txtfeb_emp.TabIndex = 10;
            this.txtfeb_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfeb_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtjan_bnk
            // 
            this.txtjan_bnk.AllowSpace = true;
            this.txtjan_bnk.AssociatedLookUpName = "";
            this.txtjan_bnk.BackColor = System.Drawing.SystemColors.Control;
            this.txtjan_bnk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtjan_bnk.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtjan_bnk.ContinuationTextBox = null;
            this.txtjan_bnk.CustomEnabled = true;
            this.txtjan_bnk.DataFieldMapping = "BNK_CONT1";
            this.txtjan_bnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtjan_bnk.GetRecordsOnUpDownKeys = false;
            this.txtjan_bnk.IsDate = false;
            this.txtjan_bnk.Location = new System.Drawing.Point(188, 87);
            this.txtjan_bnk.Name = "txtjan_bnk";
            this.txtjan_bnk.NumberFormat = "###,###,##0.00";
            this.txtjan_bnk.Postfix = "";
            this.txtjan_bnk.Prefix = "";
            this.txtjan_bnk.ReadOnly = true;
            this.txtjan_bnk.Size = new System.Drawing.Size(100, 20);
            this.txtjan_bnk.SkipValidation = false;
            this.txtjan_bnk.TabIndex = 9;
            this.txtjan_bnk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtjan_bnk.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtJan_emp
            // 
            this.txtJan_emp.AllowSpace = true;
            this.txtJan_emp.AssociatedLookUpName = "";
            this.txtJan_emp.BackColor = System.Drawing.SystemColors.Control;
            this.txtJan_emp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJan_emp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJan_emp.ContinuationTextBox = null;
            this.txtJan_emp.CustomEnabled = true;
            this.txtJan_emp.DataFieldMapping = "EMP_CONT1";
            this.txtJan_emp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJan_emp.GetRecordsOnUpDownKeys = false;
            this.txtJan_emp.IsDate = false;
            this.txtJan_emp.Location = new System.Drawing.Point(82, 87);
            this.txtJan_emp.Name = "txtJan_emp";
            this.txtJan_emp.NumberFormat = "###,###,##0.00";
            this.txtJan_emp.Postfix = "";
            this.txtJan_emp.Prefix = "";
            this.txtJan_emp.ReadOnly = true;
            this.txtJan_emp.Size = new System.Drawing.Size(100, 20);
            this.txtJan_emp.SkipValidation = false;
            this.txtJan_emp.TabIndex = 8;
            this.txtJan_emp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJan_emp.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtToatal
            // 
            this.txtToatal.AllowSpace = true;
            this.txtToatal.AssociatedLookUpName = "";
            this.txtToatal.BackColor = System.Drawing.SystemColors.Control;
            this.txtToatal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToatal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtToatal.ContinuationTextBox = null;
            this.txtToatal.CustomEnabled = true;
            this.txtToatal.DataFieldMapping = "PF_TOTAL";
            this.txtToatal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToatal.GetRecordsOnUpDownKeys = false;
            this.txtToatal.IsDate = false;
            this.txtToatal.Location = new System.Drawing.Point(440, 62);
            this.txtToatal.Name = "txtToatal";
            this.txtToatal.NumberFormat = "###,###,##0.00";
            this.txtToatal.Postfix = "";
            this.txtToatal.Prefix = "";
            this.txtToatal.ReadOnly = true;
            this.txtToatal.Size = new System.Drawing.Size(129, 20);
            this.txtToatal.SkipValidation = false;
            this.txtToatal.TabIndex = 7;
            this.txtToatal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtToatal.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtAccIntrst
            // 
            this.txtAccIntrst.AllowSpace = true;
            this.txtAccIntrst.AssociatedLookUpName = "";
            this.txtAccIntrst.BackColor = System.Drawing.SystemColors.Control;
            this.txtAccIntrst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccIntrst.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAccIntrst.ContinuationTextBox = null;
            this.txtAccIntrst.CustomEnabled = true;
            this.txtAccIntrst.DataFieldMapping = "PF_INTEREST";
            this.txtAccIntrst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccIntrst.GetRecordsOnUpDownKeys = false;
            this.txtAccIntrst.IsDate = false;
            this.txtAccIntrst.Location = new System.Drawing.Point(297, 62);
            this.txtAccIntrst.Name = "txtAccIntrst";
            this.txtAccIntrst.NumberFormat = "###,###,##0.00";
            this.txtAccIntrst.Postfix = "";
            this.txtAccIntrst.Prefix = "";
            this.txtAccIntrst.ReadOnly = true;
            this.txtAccIntrst.Size = new System.Drawing.Size(137, 20);
            this.txtAccIntrst.SkipValidation = false;
            this.txtAccIntrst.TabIndex = 6;
            this.txtAccIntrst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAccIntrst.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPfBankcont
            // 
            this.txtPfBankcont.AllowSpace = true;
            this.txtPfBankcont.AssociatedLookUpName = "";
            this.txtPfBankcont.BackColor = System.Drawing.SystemColors.Control;
            this.txtPfBankcont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPfBankcont.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPfBankcont.ContinuationTextBox = null;
            this.txtPfBankcont.CustomEnabled = true;
            this.txtPfBankcont.DataFieldMapping = "PF_BNK_CONT";
            this.txtPfBankcont.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPfBankcont.GetRecordsOnUpDownKeys = false;
            this.txtPfBankcont.IsDate = false;
            this.txtPfBankcont.Location = new System.Drawing.Point(188, 62);
            this.txtPfBankcont.Name = "txtPfBankcont";
            this.txtPfBankcont.NumberFormat = "###,###,##0.00";
            this.txtPfBankcont.Postfix = "";
            this.txtPfBankcont.Prefix = "";
            this.txtPfBankcont.ReadOnly = true;
            this.txtPfBankcont.Size = new System.Drawing.Size(100, 20);
            this.txtPfBankcont.SkipValidation = false;
            this.txtPfBankcont.TabIndex = 5;
            this.txtPfBankcont.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPfBankcont.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtPFbalnce
            // 
            this.txtPFbalnce.AllowSpace = true;
            this.txtPFbalnce.AssociatedLookUpName = "";
            this.txtPFbalnce.BackColor = System.Drawing.SystemColors.Control;
            this.txtPFbalnce.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPFbalnce.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPFbalnce.ContinuationTextBox = null;
            this.txtPFbalnce.CustomEnabled = true;
            this.txtPFbalnce.DataFieldMapping = "PF_EMP_CONT";
            this.txtPFbalnce.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPFbalnce.GetRecordsOnUpDownKeys = false;
            this.txtPFbalnce.IsDate = false;
            this.txtPFbalnce.Location = new System.Drawing.Point(82, 62);
            this.txtPFbalnce.Name = "txtPFbalnce";
            this.txtPFbalnce.NumberFormat = "###,###,##0.00";
            this.txtPFbalnce.Postfix = "";
            this.txtPFbalnce.Prefix = "";
            this.txtPFbalnce.ReadOnly = true;
            this.txtPFbalnce.Size = new System.Drawing.Size(100, 20);
            this.txtPFbalnce.SkipValidation = false;
            this.txtPFbalnce.TabIndex = 4;
            this.txtPFbalnce.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPFbalnce.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtBalance
            // 
            this.txtBalance.AllowSpace = true;
            this.txtBalance.AssociatedLookUpName = "";
            this.txtBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBalance.ContinuationTextBox = null;
            this.txtBalance.CustomEnabled = true;
            this.txtBalance.DataFieldMapping = "PF_YEAR";
            this.txtBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalance.GetRecordsOnUpDownKeys = false;
            this.txtBalance.IsDate = false;
            this.txtBalance.Location = new System.Drawing.Point(541, 36);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.NumberFormat = "###,###,##0.00";
            this.txtBalance.Postfix = "";
            this.txtBalance.Prefix = "";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(50, 20);
            this.txtBalance.SkipValidation = false;
            this.txtBalance.TabIndex = 3;
            this.txtBalance.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PF_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(541, 13);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(50, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 2;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtname
            // 
            this.txtname.AllowSpace = true;
            this.txtname.AssociatedLookUpName = "";
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtname.ContinuationTextBox = null;
            this.txtname.CustomEnabled = true;
            this.txtname.DataFieldMapping = "PF_NAME";
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.GetRecordsOnUpDownKeys = false;
            this.txtname.IsDate = false;
            this.txtname.Location = new System.Drawing.Point(246, 12);
            this.txtname.Name = "txtname";
            this.txtname.NumberFormat = "###,###,##0.00";
            this.txtname.Postfix = "";
            this.txtname.Prefix = "";
            this.txtname.ReadOnly = true;
            this.txtname.Size = new System.Drawing.Size(228, 20);
            this.txtname.SkipValidation = false;
            this.txtname.TabIndex = 1;
            this.txtname.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnel
            // 
            this.txtPersonnel.AllowSpace = true;
            this.txtPersonnel.AssociatedLookUpName = "lbPrPNo";
            this.txtPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnel.ContinuationTextBox = null;
            this.txtPersonnel.CustomEnabled = true;
            this.txtPersonnel.DataFieldMapping = "PF_PR_P_NO";
            this.txtPersonnel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnel.GetRecordsOnUpDownKeys = false;
            this.txtPersonnel.IsDate = false;
            this.txtPersonnel.Location = new System.Drawing.Point(117, 13);
            this.txtPersonnel.MaxLength = 6;
            this.txtPersonnel.Name = "txtPersonnel";
            this.txtPersonnel.NumberFormat = "###,###,##0.00";
            this.txtPersonnel.Postfix = "";
            this.txtPersonnel.Prefix = "";
            this.txtPersonnel.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnel.SkipValidation = false;
            this.txtPersonnel.TabIndex = 0;
            this.txtPersonnel.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnel.TextChanged += new System.EventHandler(this.txtPersonnel_TextChanged);
            this.txtPersonnel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtPersonnel_PreviewKeyDown);
            this.txtPersonnel.Leave += new System.EventHandler(this.txtPersonnel_Leave);
            this.txtPersonnel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersonnel_KeyPress);
            this.txtPersonnel.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnel_Validating);
            // 
            // txtPersonnel2
            // 
            this.txtPersonnel2.AllowSpace = true;
            this.txtPersonnel2.AssociatedLookUpName = "lbPrPNo";
            this.txtPersonnel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnel2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnel2.ContinuationTextBox = null;
            this.txtPersonnel2.CustomEnabled = false;
            this.txtPersonnel2.DataFieldMapping = "";
            this.txtPersonnel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnel2.GetRecordsOnUpDownKeys = false;
            this.txtPersonnel2.IsDate = false;
            this.txtPersonnel2.Location = new System.Drawing.Point(419, 39);
            this.txtPersonnel2.MaxLength = 6;
            this.txtPersonnel2.Name = "txtPersonnel2";
            this.txtPersonnel2.NumberFormat = "###,###,##0.00";
            this.txtPersonnel2.Postfix = "";
            this.txtPersonnel2.Prefix = "";
            this.txtPersonnel2.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnel2.SkipValidation = false;
            this.txtPersonnel2.TabIndex = 66;
            this.txtPersonnel2.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnel2.Visible = false;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label23);
            this.pnlHead.Controls.Add(this.label24);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label25);
            this.pnlHead.Controls.Add(this.label26);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label27);
            this.pnlHead.Controls.Add(this.label28);
            this.pnlHead.Location = new System.Drawing.Point(61, 54);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(533, 71);
            this.pnlHead.TabIndex = 53;
            this.pnlHead.TabStop = true;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(221, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(113, 17);
            this.label23.TabIndex = 20;
            this.label23.Text = "P.Fund Query";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(184, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(192, 20);
            this.label24.TabIndex = 19;
            this.label24.Text = "Provident Fund Module";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(443, 37);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(527, 12);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(388, 40);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 15);
            this.label25.TabIndex = 16;
            this.label25.Text = "Date : ";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(476, 14);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 15);
            this.label26.TabIndex = 15;
            this.label26.Text = "Option:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label26.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(59, 37);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(59, 11);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(3, 37);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(50, 20);
            this.label27.TabIndex = 2;
            this.label27.Text = "Loc :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(3, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 20);
            this.label28.TabIndex = 1;
            this.label28.Text = "User : ";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(368, 9);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 13);
            this.label29.TabIndex = 54;
            this.label29.Text = "User Name : ";
            // 
            // CHRIS_PFund_PFundQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 670);
            this.Controls.Add(this.txtPersonnel2);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.PnlPFund);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "PnlPFund";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_PFund_PFundQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS :  PFUND Query";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_PFund_PFundQuery_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.PnlPFund, 0);
            this.Controls.SetChildIndex(this.label29, 0);
            this.Controls.SetChildIndex(this.txtPersonnel2, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPFund.ResumeLayout(false);
            this.PnlPFund.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPFund;
        private CrplControlLibrary.SLTextBox txtjul_bnk;
        private CrplControlLibrary.SLTextBox txtjun_bnk;
        private CrplControlLibrary.SLTextBox txtapr_bnk;
        private CrplControlLibrary.SLTextBox txtMay_bnk;
        private CrplControlLibrary.SLTextBox txtMar_bnk;
        private CrplControlLibrary.SLTextBox txtfeb_bnk;
        private CrplControlLibrary.SLTextBox txtTotalEmp;
        private CrplControlLibrary.SLTextBox txtPremium;
        private CrplControlLibrary.SLTextBox txtyearEmp;
        private CrplControlLibrary.SLTextBox txtDec_emp;
        private CrplControlLibrary.SLTextBox txtNov_emp;
        private CrplControlLibrary.SLTextBox txtoct_emp;
        private CrplControlLibrary.SLTextBox txtsep_emp;
        private CrplControlLibrary.SLTextBox txtJul_emp;
        private CrplControlLibrary.SLTextBox txtaug_emp;
        private CrplControlLibrary.SLTextBox txtJun_emp;
        private CrplControlLibrary.SLTextBox txtMay_emp;
        private CrplControlLibrary.SLTextBox txtApr_emp;
        private CrplControlLibrary.SLTextBox txtMar_emp;
        private CrplControlLibrary.SLTextBox txtfeb_emp;
        private CrplControlLibrary.SLTextBox txtjan_bnk;
        private CrplControlLibrary.SLTextBox txtJan_emp;
        private CrplControlLibrary.SLTextBox txtToatal;
        private CrplControlLibrary.SLTextBox txtAccIntrst;
        private CrplControlLibrary.SLTextBox txtPfBankcont;
        private CrplControlLibrary.SLTextBox txtPFbalnce;
        private CrplControlLibrary.SLTextBox txtBalance;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtname;
        private CrplControlLibrary.SLTextBox txtPersonnel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtTotalPF;
        private CrplControlLibrary.SLTextBox txtTotIns;
        private CrplControlLibrary.SLTextBox txtyearPF;
        private CrplControlLibrary.SLTextBox txtyearIns;
        private CrplControlLibrary.SLTextBox txtTotBnk;
        private CrplControlLibrary.SLTextBox txtyearBnk;
        private CrplControlLibrary.SLTextBox txtdec_bnk;
        private CrplControlLibrary.SLTextBox txtNov_bnk;
        private CrplControlLibrary.SLTextBox txtoct_bnk;
        private CrplControlLibrary.SLTextBox txtsep_bnk;
        private CrplControlLibrary.SLTextBox txtaug_bnk;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.LookupButton lbPrPNo;
        private CrplControlLibrary.SLDatePicker dtww_date;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txtPersonnel2;

    }
}