using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PFund
{
    public partial class CHRIS_PFund_YearEndClosingProcess : ChrisSimpleForm 
    {
     
        Dictionary<string, object> objVals = new Dictionary<string, object>();
        DataTable dtYear = new DataTable();
        Result rsltYear;
        CmnDataManager cmnDM = new CmnDataManager();

        
        public CHRIS_PFund_YearEndClosingProcess()
        {
            InitializeComponent();
            
        }


        public CHRIS_PFund_YearEndClosingProcess(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            txtOption.Visible = false;
            //lblUserName.Text += "   " + ConfigurationManager.AppSettings["database"];
            lblUserName.Text += " "  + this.UserName; 
          
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(pnlProcessStatus.Controls); 
            }

        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtDelete.Visible = false;
            tbtAdd.Visible = false;
        }

        private void txtStatus_Leave(object sender, EventArgs e)
        {

            if (txtStatus.Text.Length > 0 && txtYear.Text !="")
            {
                if (txtStatus.Text == "YES" || txtStatus.Text == "NO")
                {
                    if (txtStatus.Text == "YES")
                    {

                        string pfYear = Convert.ToString(txtYear.Text);
                        CHRIS_PFund_YearEndClosingDialog YearEndClosingDialog = new CHRIS_PFund_YearEndClosingDialog(pfYear);
                        YearEndClosingDialog.ShowDialog();

                    }
                    else
                        this.Close();
                }
                else
                {

                    MessageBox.Show("Enter [Yes] To Start Processing And  [No] To Exit...");
                    txtStatus.Text = "";
                    txtStatus.Focus();
                }
            }


        }

        private void txtYear_Validating(object sender, CancelEventArgs e)
        {

            //if (txtYear.Text == "")
            //{

            //    MessageBox.Show("Processing Year Must Be Entered....");
            //    txtYear.Focus();
            //    return;
            //}
            //else if (txtYear.Text != "")
            //{


            //    if (txtYear.Text == Convert.ToString(this.CurrentDate.Year) )
            //    {
            //        txtYear.Text = "";
            //        txtYear.Focus();
            //        return;

            //    }

            //    objVals.Clear();
            //    objVals.Add("PR_YEAR", txtYear.Text);

            //    //VALIDATING PROPER YEAR//
            //    rsltYear = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "VAL_YEAR", objVals);
            //    if (rsltYear.isSuccessful)
            //    {
                
            //        if (rsltYear.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()  == "0")
            //        {
            //            txtYear.Text = "";
            //            txtYear.Focus();
            //            return;
            //        }

            //    }

            //}

        }

       

        private void txtYear_KeyPress(object sender, KeyPressEventArgs e)
        {
                      
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                
            }


        }

        private void txtYear_Leave(object sender, EventArgs e)
        {

            if (txtYear.Text == "")
            {

                MessageBox.Show("Processing Year Must Be Entered....");
                txtYear.Focus();
                return;
            }
            else if (txtYear.Text != "")
            {


                //if (txtYear.Text == Convert.ToString(this.CurrentDate.Year))
                //{
                //    txtYear.Text = "";
                //    txtYear.Focus();
                //    return;

                //}

                objVals.Clear();
                objVals.Add("PR_YEAR", txtYear.Text);

                //VALIDATING PROPER YEAR//
                rsltYear = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "VAL_YEAR", objVals);
                if (rsltYear.isSuccessful)
                {
                 
                    //if (rsltYear.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "0")
                    if (rsltYear.dstResult.Tables.Count > 0)
                    {
                        dtYear = rsltYear.dstResult.Tables[0];
                        if (dtYear.Rows.Count == 0 && Convert.ToString(dtYear.Rows[0]["W_YEAR"]) == "" || Convert.ToString(dtYear.Rows[0]["WW_DATE"]) == "")
                        {
                            MessageBox.Show("Invalid Year Entered.....Try Again");
                            txtYear.Text = "";
                            txtYear.Focus();
                            return;
                        }
                    }

                }

            }



        }

    

               


    }
}