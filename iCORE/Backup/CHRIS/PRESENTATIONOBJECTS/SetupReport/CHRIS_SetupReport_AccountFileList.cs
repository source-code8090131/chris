using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.SetupReport
{
    public partial class CHRIS_SetupReport_AccountFileList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_SetupReport_AccountFileList()
        {
            InitializeComponent();
        }
        public CHRIS_SetupReport_AccountFileList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
         }
        private void slButton1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "SPREP06";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                base.EmailToReport("c:\\iCORE-Spool\\SPREP06", "PDF");
            }

            else if (cmbDescType.Text == "File")
            {

                base.ExportCustomReport("c:\\iCORE-Spool\\SPREP06", "PDF");
            }
            else if (cmbDescType.Text == "Printer")
            {
                PrintCustomReport();
            }



        }

        private void slButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}