using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.SetupReport
{
    public partial class CHRIS_SetupReport_DeductionFileList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_SetupReport_DeductionFileList()
        {
            InitializeComponent();
        }

        public CHRIS_SetupReport_DeductionFileList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.P_DEDUCT_CODE.Text = "ALL";

        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "SPREP08"; //write your report name

            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                base.EmailToReport("c:\\iCORE-Spool\\SPREP08", "PDF");
            }
            else if (cmbDescType.Text == "File")
            {
                base.ExportCustomReport("c:\\iCORE-Spool\\SPREP08", "PDF");
            }
            else if (cmbDescType.Text == "Printer")
            {
                PrintCustomReport();
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}