using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.SetupReport
{
    public partial class CHRIS_SetupReport_InsuranceFileList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_SetupReport_InsuranceFileList()
        {
            InitializeComponent();
        }

        public CHRIS_SetupReport_InsuranceFileList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
        }


    
            private void Btn_Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "SPREP10"; //write your report name

            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                base.EmailToReport("c:\\iCORE-Spool\\SPREP10", "PDF");
            }
            else if (cmbDescType.Text == "File")
            {
                base.ExportCustomReport("c:\\iCORE-Spool\\SPREP10", "PDF");
            }

            else if (cmbDescType.Text == "Printer")
            {
                PrintCustomReport();
            }
            }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        

        

       
    }
}