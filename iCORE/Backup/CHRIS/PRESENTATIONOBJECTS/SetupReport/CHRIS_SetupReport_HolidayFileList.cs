using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.SetupReport
{
    public partial class CHRIS_SetupReport_HolidayFileList : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_SetupReport_HolidayFileList()
        {
            InitializeComponent();
        }

        public CHRIS_SetupReport_HolidayFileList(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
        }

        private void BtnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "SPREP09"; //write your report name

            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                base.EmailToReport("c:\\iCORE-Spool\\SPREP09", "PDF");
            }

            else if (cmbDescType.Text == "File")
            {
                base.ExportCustomReport("c:\\iCORE-Spool\\SPREP09", "PDF");
            }

            else if (cmbDescType.Text == "Printer")
            {
                PrintCustomReport();
            }

        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void CHRIS_SetupReport_HolidayFileList_Load_1(object sender, EventArgs e)
        {
        
        }

        

        
    }
}