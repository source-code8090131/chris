using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_LeavesBalanceReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_LeavesBalanceReport()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_LeavesBalanceReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.nocopies.Text = "1";

            Dest_name.Text = "C:\\iCORE-Spool\\";
            
        }


        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PRREP16";
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.nocopies.Text );
            }

            if (cmbDescType.Text == "File")
            {
                string d = "C:\\iCORE-Spool\\Report";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;

                base.ExportCustomReport(d, "pdf");


            }


            if (cmbDescType.Text == "Mail")
            {
                string d = "";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;


                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }

            
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.Close();
        }

        private void W_SEG_TextChanged(object sender, EventArgs e)
        {

        }

        private void BRANCH_TextChanged(object sender, EventArgs e)
        {

        }

        private void W_GROUP_TextChanged(object sender, EventArgs e)
        {

        }

        private void YR_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DT_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}