using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_NewHiresHeadCountDeptWise : CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_NewHiresHeadCountDeptWise()
        {
            InitializeComponent();
        }
        
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PersonnelReport_NewHiresHeadCountDeptWise(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            
            nocopies.Text = "1";
            Dest_Format.Text = "wide";
            w_brn.Text = "KHI";
            w_seg.Text = "GCB";
            w_desig.Text = "ALL";
            w_Group.Text = "ALL";
            w_level.Text = "ALL";
            w_dept.Text = "ALL";

            //this.PrintNoofCopiesReport(1);
            Dest_name.Text = "C:\\iCORE-Spool\\";

        }


        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "Prrep50";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        base.PrintNoofCopiesReport(no_of_copies);
                    }

                }

                //if (Dest_Type.Text == "File")
                //{
                //    if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //    {
                //        base.ExportCustomReport(Dest_name.Text, Dest_name.Text);

                //    }

                //}


                if (Dest_Type.Text == "File")
                {
                    string d = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }
            
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}