namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_EmployeesForProbation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_EmployeesForProbation));
            this.btnClose = new CrplControlLibrary.SLButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Run = new CrplControlLibrary.SLButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.w_level = new CrplControlLibrary.SLTextBox(this.components);
            this.w_desig = new CrplControlLibrary.SLTextBox(this.components);
            this.w_seg = new CrplControlLibrary.SLTextBox(this.components);
            this.W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.ActionType = "";
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(234, 288);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(141, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(179, 13);
            this.label14.TabIndex = 127;
            this.label14.Text = "Enter value for the parameters";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(150, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(160, 18);
            this.label13.TabIndex = 126;
            this.label13.Text = "Reports Parameters";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.dest_format);
            this.groupBox1.Controls.Add(this.dest_name);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.w_level);
            this.groupBox1.Controls.Add(this.w_desig);
            this.groupBox1.Controls.Add(this.w_seg);
            this.groupBox1.Controls.Add(this.W_BRN);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(475, 346);
            this.groupBox1.TabIndex = 125;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(407, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 128;
            this.pictureBox2.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 125;
            this.label9.Text = "Level Code :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 13);
            this.label8.TabIndex = 124;
            this.label8.Text = "Designation Code :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 121;
            this.label5.Text = "Branch Code :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 120;
            this.label4.Text = "Number of Copies";
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(144, 288);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 9;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 212);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 126;
            this.label10.Text = "Segment Code :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 119;
            this.label3.Text = "DestinationFormat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 118;
            this.label2.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 117;
            this.label1.Text = "Destination Type";
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(144, 158);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(165, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 4;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // dest_format
            // 
            this.dest_format.AllowSpace = true;
            this.dest_format.AssociatedLookUpName = "";
            this.dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_format.ContinuationTextBox = null;
            this.dest_format.CustomEnabled = true;
            this.dest_format.DataFieldMapping = "";
            this.dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_format.GetRecordsOnUpDownKeys = false;
            this.dest_format.IsDate = false;
            this.dest_format.Location = new System.Drawing.Point(144, 132);
            this.dest_format.Name = "dest_format";
            this.dest_format.NumberFormat = "###,###,##0.00";
            this.dest_format.Postfix = "";
            this.dest_format.Prefix = "";
            this.dest_format.Size = new System.Drawing.Size(165, 20);
            this.dest_format.SkipValidation = false;
            this.dest_format.TabIndex = 3;
            this.dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // dest_name
            // 
            this.dest_name.AllowSpace = true;
            this.dest_name.AssociatedLookUpName = "";
            this.dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_name.ContinuationTextBox = null;
            this.dest_name.CustomEnabled = true;
            this.dest_name.DataFieldMapping = "";
            this.dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_name.GetRecordsOnUpDownKeys = false;
            this.dest_name.IsDate = false;
            this.dest_name.Location = new System.Drawing.Point(144, 106);
            this.dest_name.MaxLength = 50;
            this.dest_name.Name = "dest_name";
            this.dest_name.NumberFormat = "###,###,##0.00";
            this.dest_name.Postfix = "";
            this.dest_name.Prefix = "";
            this.dest_name.Size = new System.Drawing.Size(165, 20);
            this.dest_name.SkipValidation = false;
            this.dest_name.TabIndex = 2;
            this.dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(144, 79);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(165, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 1;
            // 
            // w_level
            // 
            this.w_level.AllowSpace = true;
            this.w_level.AssociatedLookUpName = "";
            this.w_level.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_level.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_level.ContinuationTextBox = null;
            this.w_level.CustomEnabled = true;
            this.w_level.DataFieldMapping = "";
            this.w_level.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_level.GetRecordsOnUpDownKeys = false;
            this.w_level.IsDate = false;
            this.w_level.Location = new System.Drawing.Point(144, 262);
            this.w_level.MaxLength = 40;
            this.w_level.Name = "w_level";
            this.w_level.NumberFormat = "###,###,##0.00";
            this.w_level.Postfix = "";
            this.w_level.Prefix = "";
            this.w_level.Size = new System.Drawing.Size(165, 20);
            this.w_level.SkipValidation = false;
            this.w_level.TabIndex = 8;
            this.w_level.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_desig
            // 
            this.w_desig.AllowSpace = true;
            this.w_desig.AssociatedLookUpName = "";
            this.w_desig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_desig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_desig.ContinuationTextBox = null;
            this.w_desig.CustomEnabled = true;
            this.w_desig.DataFieldMapping = "";
            this.w_desig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_desig.GetRecordsOnUpDownKeys = false;
            this.w_desig.IsDate = false;
            this.w_desig.Location = new System.Drawing.Point(144, 236);
            this.w_desig.MaxLength = 40;
            this.w_desig.Name = "w_desig";
            this.w_desig.NumberFormat = "###,###,##0.00";
            this.w_desig.Postfix = "";
            this.w_desig.Prefix = "";
            this.w_desig.Size = new System.Drawing.Size(165, 20);
            this.w_desig.SkipValidation = false;
            this.w_desig.TabIndex = 7;
            this.w_desig.TextType = CrplControlLibrary.TextType.String;
            // 
            // w_seg
            // 
            this.w_seg.AllowSpace = true;
            this.w_seg.AssociatedLookUpName = "";
            this.w_seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_seg.ContinuationTextBox = null;
            this.w_seg.CustomEnabled = true;
            this.w_seg.DataFieldMapping = "";
            this.w_seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_seg.GetRecordsOnUpDownKeys = false;
            this.w_seg.IsDate = false;
            this.w_seg.Location = new System.Drawing.Point(144, 210);
            this.w_seg.MaxLength = 3;
            this.w_seg.Name = "w_seg";
            this.w_seg.NumberFormat = "###,###,##0.00";
            this.w_seg.Postfix = "";
            this.w_seg.Prefix = "";
            this.w_seg.Size = new System.Drawing.Size(165, 20);
            this.w_seg.SkipValidation = false;
            this.w_seg.TabIndex = 6;
            this.w_seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_BRN
            // 
            this.W_BRN.AllowSpace = true;
            this.W_BRN.AssociatedLookUpName = "";
            this.W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRN.ContinuationTextBox = null;
            this.W_BRN.CustomEnabled = true;
            this.W_BRN.DataFieldMapping = "";
            this.W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRN.GetRecordsOnUpDownKeys = false;
            this.W_BRN.IsDate = false;
            this.W_BRN.Location = new System.Drawing.Point(144, 184);
            this.W_BRN.MaxLength = 3;
            this.W_BRN.Name = "W_BRN";
            this.W_BRN.NumberFormat = "###,###,##0.00";
            this.W_BRN.Postfix = "";
            this.W_BRN.Prefix = "";
            this.W_BRN.Size = new System.Drawing.Size(165, 20);
            this.W_BRN.SkipValidation = false;
            this.W_BRN.TabIndex = 5;
            this.W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_PersonnelReport_EmployeesForProbation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 394);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_EmployeesForProbation";
            this.Text = "iCORE CHRIS - Employees For Probation";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLButton btnClose;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox dest_format;
        private CrplControlLibrary.SLTextBox dest_name;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private CrplControlLibrary.SLTextBox w_level;
        private CrplControlLibrary.SLTextBox w_desig;
        private CrplControlLibrary.SLTextBox w_seg;
        private CrplControlLibrary.SLTextBox W_BRN;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}