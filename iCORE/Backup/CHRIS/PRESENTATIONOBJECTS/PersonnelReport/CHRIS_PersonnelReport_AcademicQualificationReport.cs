using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport


{
    public partial class CHRIS_PersonnelReport_AcademicQualificationReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_AcademicQualificationReport()
        {
            InitializeComponent();
        }
        public CHRIS_PersonnelReport_AcademicQualificationReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cmbDescType.Items.RemoveAt(5);
            this.W_BRN.Text = "All";
            this.W_SEG.Text = "All";
            this.W_DESIG.Text = "All";
            this.w_year.Text = "1995";
            Dest_name.Text = "C:\\iCORE-Spool\\";

            

        }
        private void Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PRREP32";
            if (cmbDescType.Text == "Screen")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                string d = "";
                if (DESNAME.Text != string.Empty)
                    d = DESNAME.Text;
                base.EmailToReport("c:\\iCORE-Spool\\report", "PDF", d);
            }
            else if (cmbDescType.Text == "File")
            {

                string d = "c:\\iCORE-Spool\\PRREP32";
                if (DESNAME.Text != string.Empty)
                    d = DESNAME.Text;
                base.ExportCustomReport(d, "pdf"); 
            }


        }

        
        private void slButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

       
    }
}