using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_AppraisalDue : BaseRptForm
    {
        public CHRIS_PersonnelReport_AppraisalDue()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PersonnelReport_AppraisalDue(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            this.w_brn.Text = "NUL";
            this.s_date.Value  = null;
            this.e_date.Value = null;
            // Output_mod.Items.RemoveAt(3);
            //Dest_name.Text = "C:\\";
        }


        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            if (nocopies.Text != String.Empty)
            {
                no_of_copies = Convert.ToInt16(nocopies.Text);
            }
            else
            {
                no_of_copies = 1;
            }
            base.RptFileName = "PRREP08";
             if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);
              
            }
            if (Dest_Type.Text == "Printer")
            {
              
                base.PrintNoofCopiesReport(no_of_copies);
                
            }


              if (Dest_Type.Text == "Mail")
            {
                
                    base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf","");

            }

            if (Dest_Type.Text == "File")
            {
                    base.ExportCustomReport("C:\\iCORE-Spool\\Report", "pdf");


            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

             
      

        }

        


    }

