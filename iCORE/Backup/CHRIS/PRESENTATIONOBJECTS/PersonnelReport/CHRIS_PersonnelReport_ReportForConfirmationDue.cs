using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ReportForConfirmationDue : BaseRptForm
    {
        public CHRIS_PersonnelReport_ReportForConfirmationDue()
        {
            InitializeComponent();
        }
      
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        
        public CHRIS_PersonnelReport_ReportForConfirmationDue(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.Dest_name.Text = "prrep04.lis";
            slComboBox2.Items.RemoveAt(3);
            nocopies.Text = "1";
            Pr_New_Brn.Text = "ALL";
            Pr_Desig.Text = "ALL";
            Pr_Segment.Text = "ALL";
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }


        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "PRREP04";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                       
                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }


                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }

            }


        }

        //        if (Dest_Type.Text == "Mail")
        //        {

        //            if (DestFormat != string.Empty || Dest_name.Text != string.Empty)
        //            {
        //                base.EmailToReport(@"C:\Report", "PDF");
        //            }
        //        }
               

        //    }


        //}

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);


        }
    }
}