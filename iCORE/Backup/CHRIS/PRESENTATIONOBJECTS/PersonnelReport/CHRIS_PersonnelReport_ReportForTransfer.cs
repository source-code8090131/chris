using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ReportForTransfer : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_ReportForTransfer()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        public CHRIS_PersonnelReport_ReportForTransfer(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            Dest_Format.Text = "WIDE";
            this.w_brn.Text = "nul";
//            this.s_date.Value = new DateTime (2011, 04,01) ;
//            this.e_date.Value = new DateTime(2011, 04, 01);
            //this.PrintNoofCopiesReport(1);
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "Prrep13";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }



                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }


            }


            //base.RptFileName = "Prrep13"; //write your report name

            //if (cmbDescType.Text == "Screen")
            //{
            //    base.btnCallReport_Click(sender, e);
            //}
            //else if (cmbDescType.Text == "Mail")
            //{
            //    base.EmailToReport("c:\\PRREP13", "PDF");
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            base.btnCloseReport_Click(sender, e);
        }

        
    }
}