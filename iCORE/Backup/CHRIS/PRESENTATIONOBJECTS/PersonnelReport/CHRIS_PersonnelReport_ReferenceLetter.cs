using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ReferenceLetter : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_ReferenceLetter()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_ReferenceLetter(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.dest_format.Text = "dflt";
            this.Copies.Text = "1";
            this.P_NO.Text = "1588";
            this.AUTH_NAME.Text = "Asad Ali";
            this.AUTH_DESG.Text = "Vice President";
            this.AUTH_PLACE.Text = "Human Resource Department";
            dest_name.Text = "C:\\iCORE-Spool\\";
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PRREP23";

                if (this.cmbDescType.Text == "Screen" || this.cmbDescType.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (this.cmbDescType.Text == "Printer")
                {
                    base.PrintCustomReport(this.Copies.Text );
                }
                else if (this.cmbDescType.Text == "File")
                {

                    string DestName;
                    
                    if (this.dest_name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.dest_name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (this.dest_name.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";

                    string RecipentName;
                    if (this.dest_name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.dest_name.Text;
                    }



                    base.EmailToReport(DestName, "pdf", RecipentName);
                }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Close();
        }
    }
}