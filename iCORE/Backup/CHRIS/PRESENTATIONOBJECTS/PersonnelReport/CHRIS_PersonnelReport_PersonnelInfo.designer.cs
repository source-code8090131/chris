namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_PersonnelInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_PersonnelInfo));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dpt = new CrplControlLibrary.SLTextBox(this.components);
            this.tpno = new CrplControlLibrary.SLTextBox(this.components);
            this.spno = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.Dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Close = new System.Windows.Forms.Button();
            this.Run = new System.Windows.Forms.Button();
            this.brn = new CrplControlLibrary.SLTextBox(this.components);
            this.seg = new CrplControlLibrary.SLTextBox(this.components);
            this.yr = new CrplControlLibrary.SLTextBox(this.components);
            this.lev = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(443, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 82;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lev);
            this.groupBox1.Controls.Add(this.yr);
            this.groupBox1.Controls.Add(this.seg);
            this.groupBox1.Controls.Add(this.brn);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.dpt);
            this.groupBox1.Controls.Add(this.tpno);
            this.groupBox1.Controls.Add(this.spno);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nocopies);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.Dest_name);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(508, 403);
            this.groupBox1.TabIndex = 83;
            this.groupBox1.TabStop = false;
            // 
            // dpt
            // 
            this.dpt.AllowSpace = true;
            this.dpt.AssociatedLookUpName = "";
            this.dpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dpt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dpt.ContinuationTextBox = null;
            this.dpt.CustomEnabled = true;
            this.dpt.DataFieldMapping = "";
            this.dpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpt.GetRecordsOnUpDownKeys = false;
            this.dpt.IsDate = false;
            this.dpt.Location = new System.Drawing.Point(209, 282);
            this.dpt.MaxLength = 5;
            this.dpt.Name = "dpt";
            this.dpt.NumberFormat = "###,###,##0.00";
            this.dpt.Postfix = "";
            this.dpt.Prefix = "";
            this.dpt.Size = new System.Drawing.Size(152, 20);
            this.dpt.SkipValidation = false;
            this.dpt.TabIndex = 8;
            this.dpt.TextType = CrplControlLibrary.TextType.String;
            // 
            // tpno
            // 
            this.tpno.AllowSpace = true;
            this.tpno.AssociatedLookUpName = "";
            this.tpno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tpno.ContinuationTextBox = null;
            this.tpno.CustomEnabled = true;
            this.tpno.DataFieldMapping = "";
            this.tpno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpno.GetRecordsOnUpDownKeys = false;
            this.tpno.IsDate = false;
            this.tpno.Location = new System.Drawing.Point(209, 229);
            this.tpno.MaxLength = 6;
            this.tpno.Name = "tpno";
            this.tpno.NumberFormat = "###,###,##0.00";
            this.tpno.Postfix = "";
            this.tpno.Prefix = "";
            this.tpno.Size = new System.Drawing.Size(152, 20);
            this.tpno.SkipValidation = false;
            this.tpno.TabIndex = 6;
            this.tpno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // spno
            // 
            this.spno.AllowSpace = true;
            this.spno.AssociatedLookUpName = "";
            this.spno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.spno.ContinuationTextBox = null;
            this.spno.CustomEnabled = true;
            this.spno.DataFieldMapping = "";
            this.spno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spno.GetRecordsOnUpDownKeys = false;
            this.spno.IsDate = false;
            this.spno.Location = new System.Drawing.Point(209, 203);
            this.spno.MaxLength = 6;
            this.spno.Name = "spno";
            this.spno.NumberFormat = "###,###,##0.00";
            this.spno.Postfix = "";
            this.spno.Prefix = "";
            this.spno.Size = new System.Drawing.Size(152, 20);
            this.spno.SkipValidation = false;
            this.spno.TabIndex = 5;
            this.spno.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(14, 308);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 13);
            this.label14.TabIndex = 46;
            this.label14.Text = "No Of Years For History";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 335);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 13);
            this.label13.TabIndex = 45;
            this.label13.Text = "Enter Level Or ALL";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 282);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 13);
            this.label12.TabIndex = 44;
            this.label12.Text = "Enter Dept. Or ALL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 255);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(131, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "Enter GF/GCB Or ALL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 229);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Enter Ending P.No.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "Enter Starting P.No.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Enter Branch Or [ALL]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Number Of Copies";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Destination Format";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Destination Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Destination Type";
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(209, 151);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(152, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 3;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(209, 125);
            this.Dest_Format.MaxLength = 40;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(152, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 2;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(209, 72);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(152, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 0;
            // 
            // Dest_name
            // 
            this.Dest_name.AllowSpace = true;
            this.Dest_name.AssociatedLookUpName = "";
            this.Dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_name.ContinuationTextBox = null;
            this.Dest_name.CustomEnabled = true;
            this.Dest_name.DataFieldMapping = "";
            this.Dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_name.GetRecordsOnUpDownKeys = false;
            this.Dest_name.IsDate = false;
            this.Dest_name.Location = new System.Drawing.Point(209, 99);
            this.Dest_name.MaxLength = 50;
            this.Dest_name.Name = "Dest_name";
            this.Dest_name.NumberFormat = "###,###,##0.00";
            this.Dest_name.Postfix = "";
            this.Dest_name.Prefix = "";
            this.Dest_name.Size = new System.Drawing.Size(152, 20);
            this.Dest_name.SkipValidation = false;
            this.Dest_name.TabIndex = 1;
            this.Dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(171, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(205, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "Report Parameters";
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(286, 372);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(64, 23);
            this.Close.TabIndex = 12;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Run
            // 
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(206, 372);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(66, 23);
            this.Run.TabIndex = 11;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // brn
            // 
            this.brn.AllowSpace = true;
            this.brn.AssociatedLookUpName = "";
            this.brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.brn.ContinuationTextBox = null;
            this.brn.CustomEnabled = true;
            this.brn.DataFieldMapping = "";
            this.brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brn.GetRecordsOnUpDownKeys = false;
            this.brn.IsDate = false;
            this.brn.Location = new System.Drawing.Point(209, 177);
            this.brn.MaxLength = 3;
            this.brn.Name = "brn";
            this.brn.NumberFormat = "###,###,##0.00";
            this.brn.Postfix = "";
            this.brn.Prefix = "";
            this.brn.Size = new System.Drawing.Size(152, 20);
            this.brn.SkipValidation = false;
            this.brn.TabIndex = 4;
            this.brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // seg
            // 
            this.seg.AllowSpace = true;
            this.seg.AssociatedLookUpName = "";
            this.seg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.seg.ContinuationTextBox = null;
            this.seg.CustomEnabled = true;
            this.seg.DataFieldMapping = "";
            this.seg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seg.GetRecordsOnUpDownKeys = false;
            this.seg.IsDate = false;
            this.seg.Location = new System.Drawing.Point(209, 255);
            this.seg.MaxLength = 3;
            this.seg.Name = "seg";
            this.seg.NumberFormat = "###,###,##0.00";
            this.seg.Postfix = "";
            this.seg.Prefix = "";
            this.seg.Size = new System.Drawing.Size(152, 20);
            this.seg.SkipValidation = false;
            this.seg.TabIndex = 7;
            this.seg.TextType = CrplControlLibrary.TextType.String;
            // 
            // yr
            // 
            this.yr.AllowSpace = true;
            this.yr.AssociatedLookUpName = "";
            this.yr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.yr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.yr.ContinuationTextBox = null;
            this.yr.CustomEnabled = true;
            this.yr.DataFieldMapping = "";
            this.yr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yr.GetRecordsOnUpDownKeys = false;
            this.yr.IsDate = false;
            this.yr.Location = new System.Drawing.Point(209, 308);
            this.yr.MaxLength = 2;
            this.yr.Name = "yr";
            this.yr.NumberFormat = "###,###,##0.00";
            this.yr.Postfix = "";
            this.yr.Prefix = "";
            this.yr.Size = new System.Drawing.Size(152, 20);
            this.yr.SkipValidation = false;
            this.yr.TabIndex = 9;
            this.yr.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // lev
            // 
            this.lev.AllowSpace = true;
            this.lev.AssociatedLookUpName = "";
            this.lev.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lev.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.lev.ContinuationTextBox = null;
            this.lev.CustomEnabled = true;
            this.lev.DataFieldMapping = "";
            this.lev.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lev.GetRecordsOnUpDownKeys = false;
            this.lev.IsDate = false;
            this.lev.Location = new System.Drawing.Point(209, 333);
            this.lev.MaxLength = 3;
            this.lev.Name = "lev";
            this.lev.NumberFormat = "###,###,##0.00";
            this.lev.Postfix = "";
            this.lev.Prefix = "";
            this.lev.Size = new System.Drawing.Size(152, 20);
            this.lev.SkipValidation = false;
            this.lev.TabIndex = 10;
            this.lev.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_PersonnelReport_PersonnelInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 449);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_PersonnelInfo";
            this.Text = "CHRIS_PersonnelReport_CITI_EMPAD";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Run;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox nocopies;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLTextBox Dest_name;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox dpt;
        private CrplControlLibrary.SLTextBox tpno;
        private CrplControlLibrary.SLTextBox spno;
        private CrplControlLibrary.SLTextBox lev;
        private CrplControlLibrary.SLTextBox yr;
        private CrplControlLibrary.SLTextBox seg;
        private CrplControlLibrary.SLTextBox brn;
    }
}