using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ReportForIntern : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_ReportForIntern()
        {
            InitializeComponent();
        }
        public CHRIS_PersonnelReport_ReportForIntern(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj): base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cmbDescType.Items.RemoveAt(5);
            nocopies.Text = "1";
            Dest_Format.Text = "PDF";
            w_brn.Text = "NUL";

        }
        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;

            {
                base.RptFileName = "Prrep10";


                if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (cmbDescType.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }

                if (cmbDescType.Text == "File")
                {
                    String Rec = "";

                    if (Dest_Format.Text != String.Empty)
                        Rec = Dest_Format.Text;

                        base.ExportCustomReport(Rec, "pdf");



                }

                if (cmbDescType.Text == "Mail")
                {
                    String Rec = "";

                    if (Dest_Format.Text != String.Empty)
                        Rec = Dest_Format.Text;
                        base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF",Rec);

                }


            }

         
        }        
       

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        
    }
}