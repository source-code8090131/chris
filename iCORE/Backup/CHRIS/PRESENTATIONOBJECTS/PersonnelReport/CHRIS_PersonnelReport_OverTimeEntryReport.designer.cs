namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_OverTimeEntryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_OverTimeEntryReport));
            this.Close = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.copies1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.destype = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.W_P_NO1 = new CrplControlLibrary.SLTextBox(this.components);
            this.W_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.W_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.W_MONTH = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Close
            // 
            this.Close.ActionType = "";
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(239, 336);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 9;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click_1);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(158, 336);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 8;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.copies1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.destype);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.W_P_NO1);
            this.groupBox1.Controls.Add(this.W_P_NO);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.W_BRANCH);
            this.groupBox1.Controls.Add(this.W_MONTH);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 276);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // copies1
            // 
            this.copies1.AllowSpace = true;
            this.copies1.AssociatedLookUpName = "";
            this.copies1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies1.ContinuationTextBox = null;
            this.copies1.CustomEnabled = true;
            this.copies1.DataFieldMapping = "";
            this.copies1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies1.GetRecordsOnUpDownKeys = false;
            this.copies1.IsDate = false;
            this.copies1.Location = new System.Drawing.Point(167, 127);
            this.copies1.MaxLength = 2;
            this.copies1.Name = "copies1";
            this.copies1.NumberFormat = "###,###,##0.00";
            this.copies1.Postfix = "";
            this.copies1.Prefix = "";
            this.copies1.Size = new System.Drawing.Size(141, 20);
            this.copies1.SkipValidation = false;
            this.copies1.TabIndex = 3;
            this.copies1.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 79;
            this.label9.Text = "Copies";
            // 
            // destype
            // 
            this.destype.AllowSpace = true;
            this.destype.AssociatedLookUpName = "";
            this.destype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.destype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.destype.ContinuationTextBox = null;
            this.destype.CustomEnabled = true;
            this.destype.DataFieldMapping = "";
            this.destype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destype.GetRecordsOnUpDownKeys = false;
            this.destype.IsDate = false;
            this.destype.Location = new System.Drawing.Point(167, 101);
            this.destype.MaxLength = 50;
            this.destype.Name = "destype";
            this.destype.NumberFormat = "###,###,##0.00";
            this.destype.Postfix = "";
            this.destype.Prefix = "";
            this.destype.Size = new System.Drawing.Size(141, 20);
            this.destype.SkipValidation = false;
            this.destype.TabIndex = 2;
            this.destype.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 77;
            this.label5.Text = "Desname";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(117, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "Enter values for the parameters";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(136, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 18);
            this.label6.TabIndex = 75;
            this.label6.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(328, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 74;
            this.pictureBox2.TabStop = false;
            // 
            // W_P_NO1
            // 
            this.W_P_NO1.AllowSpace = true;
            this.W_P_NO1.AssociatedLookUpName = "";
            this.W_P_NO1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_P_NO1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_P_NO1.ContinuationTextBox = null;
            this.W_P_NO1.CustomEnabled = true;
            this.W_P_NO1.DataFieldMapping = "";
            this.W_P_NO1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_P_NO1.GetRecordsOnUpDownKeys = false;
            this.W_P_NO1.IsDate = false;
            this.W_P_NO1.Location = new System.Drawing.Point(167, 179);
            this.W_P_NO1.MaxLength = 6;
            this.W_P_NO1.Name = "W_P_NO1";
            this.W_P_NO1.NumberFormat = "###,###,##0.00";
            this.W_P_NO1.Postfix = "";
            this.W_P_NO1.Prefix = "";
            this.W_P_NO1.Size = new System.Drawing.Size(141, 20);
            this.W_P_NO1.SkipValidation = false;
            this.W_P_NO1.TabIndex = 5;
            this.W_P_NO1.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // W_P_NO
            // 
            this.W_P_NO.AllowSpace = true;
            this.W_P_NO.AssociatedLookUpName = "";
            this.W_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_P_NO.ContinuationTextBox = null;
            this.W_P_NO.CustomEnabled = true;
            this.W_P_NO.DataFieldMapping = "";
            this.W_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_P_NO.GetRecordsOnUpDownKeys = false;
            this.W_P_NO.IsDate = false;
            this.W_P_NO.Location = new System.Drawing.Point(167, 153);
            this.W_P_NO.MaxLength = 6;
            this.W_P_NO.Name = "W_P_NO";
            this.W_P_NO.NumberFormat = "###,###,##0.00";
            this.W_P_NO.Postfix = "";
            this.W_P_NO.Prefix = "";
            this.W_P_NO.Size = new System.Drawing.Size(141, 20);
            this.W_P_NO.SkipValidation = false;
            this.W_P_NO.TabIndex = 4;
            this.W_P_NO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Destype";
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(167, 74);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(141, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Enter Branch Code";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Enter Month";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Ending Personnel No.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Starting Personnel No.";
            // 
            // W_BRANCH
            // 
            this.W_BRANCH.AllowSpace = true;
            this.W_BRANCH.AssociatedLookUpName = "";
            this.W_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRANCH.ContinuationTextBox = null;
            this.W_BRANCH.CustomEnabled = true;
            this.W_BRANCH.DataFieldMapping = "";
            this.W_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRANCH.GetRecordsOnUpDownKeys = false;
            this.W_BRANCH.IsDate = false;
            this.W_BRANCH.Location = new System.Drawing.Point(167, 231);
            this.W_BRANCH.MaxLength = 3;
            this.W_BRANCH.Name = "W_BRANCH";
            this.W_BRANCH.NumberFormat = "###,###,##0.00";
            this.W_BRANCH.Postfix = "";
            this.W_BRANCH.Prefix = "";
            this.W_BRANCH.Size = new System.Drawing.Size(141, 20);
            this.W_BRANCH.SkipValidation = false;
            this.W_BRANCH.TabIndex = 7;
            this.W_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_MONTH
            // 
            this.W_MONTH.AllowSpace = true;
            this.W_MONTH.AssociatedLookUpName = "";
            this.W_MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_MONTH.ContinuationTextBox = null;
            this.W_MONTH.CustomEnabled = true;
            this.W_MONTH.DataFieldMapping = "";
            this.W_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_MONTH.GetRecordsOnUpDownKeys = false;
            this.W_MONTH.IsDate = false;
            this.W_MONTH.Location = new System.Drawing.Point(167, 205);
            this.W_MONTH.MaxLength = 2;
            this.W_MONTH.Name = "W_MONTH";
            this.W_MONTH.NumberFormat = "###,###,##0.00";
            this.W_MONTH.Postfix = "";
            this.W_MONTH.Prefix = "";
            this.W_MONTH.Size = new System.Drawing.Size(141, 20);
            this.W_MONTH.SkipValidation = false;
            this.W_MONTH.TabIndex = 6;
            this.W_MONTH.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // CHRIS_PersonnelReport_OverTimeEntryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 371);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Run);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_OverTimeEntryReport";
            this.Text = "Over Time Entry Report";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.Run, 0);
            this.Controls.SetChildIndex(this.Close, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLButton Close;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox W_BRANCH;
        private CrplControlLibrary.SLTextBox W_MONTH;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLTextBox W_P_NO;
        private CrplControlLibrary.SLTextBox W_P_NO1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox destype;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox copies1;
        private System.Windows.Forms.Label label9;

    }
}