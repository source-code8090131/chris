namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_PriorExperienceEDU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_PriorExperienceEDU));
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TODAY = new System.Windows.Forms.Label();
            this.YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.W_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Close = new System.Windows.Forms.Button();
            this.Run = new System.Windows.Forms.Button();
            this.nocopies = new CrplControlLibrary.SLTextBox(this.components);
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Type = new CrplControlLibrary.SLComboBox();
            this.slDatePicker1 = new CrplControlLibrary.SLDatePicker(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(97, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Enter values for the parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(132, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "Report Parameters";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(30, 260);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(195, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "ENTER LAST EDU. YEAR(YYYY)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(198, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "ENTER A DESIGNATION OR ALL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 208);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "ENTER A SEGMENT OR ALL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "ENTER A BRANCH CODE OR ALL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Number of Copies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Destination Name";
            // 
            // TODAY
            // 
            this.TODAY.AutoSize = true;
            this.TODAY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TODAY.Location = new System.Drawing.Point(30, 79);
            this.TODAY.Name = "TODAY";
            this.TODAY.Size = new System.Drawing.Size(42, 13);
            this.TODAY.TabIndex = 12;
            this.TODAY.Text = "Today";
            // 
            // YEAR
            // 
            this.YEAR.AllowSpace = true;
            this.YEAR.AssociatedLookUpName = "";
            this.YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.YEAR.ContinuationTextBox = null;
            this.YEAR.CustomEnabled = true;
            this.YEAR.DataFieldMapping = "";
            this.YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YEAR.GetRecordsOnUpDownKeys = false;
            this.YEAR.IsDate = false;
            this.YEAR.Location = new System.Drawing.Point(251, 258);
            this.YEAR.MaxLength = 4;
            this.YEAR.Name = "YEAR";
            this.YEAR.NumberFormat = "###,###,##0.00";
            this.YEAR.Postfix = "";
            this.YEAR.Prefix = "";
            this.YEAR.Size = new System.Drawing.Size(156, 20);
            this.YEAR.SkipValidation = false;
            this.YEAR.TabIndex = 8;
            this.YEAR.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // W_DESIG
            // 
            this.W_DESIG.AllowSpace = true;
            this.W_DESIG.AssociatedLookUpName = "";
            this.W_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_DESIG.ContinuationTextBox = null;
            this.W_DESIG.CustomEnabled = true;
            this.W_DESIG.DataFieldMapping = "";
            this.W_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_DESIG.GetRecordsOnUpDownKeys = false;
            this.W_DESIG.IsDate = false;
            this.W_DESIG.Location = new System.Drawing.Point(251, 232);
            this.W_DESIG.MaxLength = 6;
            this.W_DESIG.Name = "W_DESIG";
            this.W_DESIG.NumberFormat = "###,###,##0.00";
            this.W_DESIG.Postfix = "";
            this.W_DESIG.Prefix = "";
            this.W_DESIG.Size = new System.Drawing.Size(156, 20);
            this.W_DESIG.SkipValidation = false;
            this.W_DESIG.TabIndex = 7;
            this.W_DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TODAY);
            this.groupBox1.Controls.Add(this.Close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.YEAR);
            this.groupBox1.Controls.Add(this.W_DESIG);
            this.groupBox1.Controls.Add(this.nocopies);
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.W_BRN);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.Dest_Type);
            this.groupBox1.Controls.Add(this.slDatePicker1);
            this.groupBox1.Location = new System.Drawing.Point(9, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 356);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(405, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 77;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Destination Type";
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(332, 284);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 10;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click_1);
            // 
            // Run
            // 
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(251, 284);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 9;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // nocopies
            // 
            this.nocopies.AllowSpace = true;
            this.nocopies.AssociatedLookUpName = "";
            this.nocopies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nocopies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nocopies.ContinuationTextBox = null;
            this.nocopies.CustomEnabled = true;
            this.nocopies.DataFieldMapping = "";
            this.nocopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nocopies.GetRecordsOnUpDownKeys = false;
            this.nocopies.IsDate = false;
            this.nocopies.Location = new System.Drawing.Point(251, 154);
            this.nocopies.MaxLength = 2;
            this.nocopies.Name = "nocopies";
            this.nocopies.NumberFormat = "###,###,##0.00";
            this.nocopies.Postfix = "";
            this.nocopies.Prefix = "";
            this.nocopies.Size = new System.Drawing.Size(156, 20);
            this.nocopies.SkipValidation = false;
            this.nocopies.TabIndex = 4;
            this.nocopies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(251, 206);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(156, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 6;
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // W_BRN
            // 
            this.W_BRN.AllowSpace = true;
            this.W_BRN.AssociatedLookUpName = "";
            this.W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRN.ContinuationTextBox = null;
            this.W_BRN.CustomEnabled = true;
            this.W_BRN.DataFieldMapping = "";
            this.W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRN.GetRecordsOnUpDownKeys = false;
            this.W_BRN.IsDate = false;
            this.W_BRN.Location = new System.Drawing.Point(251, 180);
            this.W_BRN.MaxLength = 3;
            this.W_BRN.Name = "W_BRN";
            this.W_BRN.NumberFormat = "###,###,##0.00";
            this.W_BRN.Postfix = "";
            this.W_BRN.Prefix = "";
            this.W_BRN.Size = new System.Drawing.Size(156, 20);
            this.W_BRN.SkipValidation = false;
            this.W_BRN.TabIndex = 5;
            this.W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(251, 128);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(156, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 3;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Type
            // 
            this.Dest_Type.BusinessEntity = "";
            this.Dest_Type.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.Dest_Type.CustomEnabled = true;
            this.Dest_Type.DataFieldMapping = "";
            this.Dest_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Dest_Type.FormattingEnabled = true;
            this.Dest_Type.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.Dest_Type.Location = new System.Drawing.Point(251, 101);
            this.Dest_Type.LOVType = "";
            this.Dest_Type.Name = "Dest_Type";
            this.Dest_Type.Size = new System.Drawing.Size(156, 21);
            this.Dest_Type.SPName = "";
            this.Dest_Type.TabIndex = 2;
            // 
            // slDatePicker1
            // 
            this.slDatePicker1.CustomEnabled = true;
            this.slDatePicker1.CustomFormat = "";
            this.slDatePicker1.DataFieldMapping = "";
            this.slDatePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.slDatePicker1.HasChanges = true;
            this.slDatePicker1.Location = new System.Drawing.Point(251, 75);
            this.slDatePicker1.Name = "slDatePicker1";
            this.slDatePicker1.NullValue = " ";
            this.slDatePicker1.Size = new System.Drawing.Size(156, 20);
            this.slDatePicker1.TabIndex = 1;
            this.slDatePicker1.Value = new System.DateTime(2011, 2, 4, 0, 0, 0, 0);
            // 
            // CHRIS_PersonnelReport_PriorExperienceEDU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 401);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_PersonnelReport_PriorExperienceEDU";
            this.Text = "iCORE CHRIS - Prior Experience (EDU)";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TODAY;
        private CrplControlLibrary.SLTextBox YEAR;
        private CrplControlLibrary.SLTextBox W_DESIG;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Run;
        private CrplControlLibrary.SLTextBox nocopies;
        private CrplControlLibrary.SLTextBox W_SEG;
        private CrplControlLibrary.SLTextBox W_BRN;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLComboBox Dest_Type;
        private CrplControlLibrary.SLDatePicker slDatePicker1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}