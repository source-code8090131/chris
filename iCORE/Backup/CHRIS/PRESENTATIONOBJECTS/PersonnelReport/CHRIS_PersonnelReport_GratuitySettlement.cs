using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_GratuitySettlement : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_GratuitySettlement()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_GratuitySettlement(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.w_year.Text = "1999";
            this.w_brn.Text = "ALL";
            this.w_dept.Text = "ALL";
            this.w_from.Text = "427";
            this.term.Value = new DateTime(1999, 8, 31);




        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
            //this.Close();
        }

        private void Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "GRATUITY";

            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (Dest_Type.Text == "Printer")
            {
                base.PrintCustomReport();
            }
            else if (Dest_Type.Text == "File")
            {

                string DestName;

                DestName = @"C:\iCORE-Spool\Report";


                base.ExportCustomReport(DestName, "pdf");

            }
            else if (Dest_Type.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";

                string RecipentName;
                RecipentName = "";
                base.EmailToReport(DestName, "pdf", RecipentName);


            }



        }
    }
}