namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    partial class CHRIS_PersonnelReport_ReferenceLetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_PersonnelReport_ReferenceLetter));
            this.btnRun = new CrplControlLibrary.SLButton();
            this.btnClose = new CrplControlLibrary.SLButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AUTH_PLACE = new CrplControlLibrary.SLTextBox(this.components);
            this.AUTH_DESG = new CrplControlLibrary.SLTextBox(this.components);
            this.AUTH_NAME = new CrplControlLibrary.SLTextBox(this.components);
            this.P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dest_name = new CrplControlLibrary.SLTextBox(this.components);
            this.dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.Copies = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRun
            // 
            this.btnRun.ActionType = "";
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(174, 341);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 8;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnClose
            // 
            this.btnClose.ActionType = "";
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(255, 341);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Copies);
            this.groupBox1.Controls.Add(this.dest_format);
            this.groupBox1.Controls.Add(this.dest_name);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.AUTH_PLACE);
            this.groupBox1.Controls.Add(this.AUTH_DESG);
            this.groupBox1.Controls.Add(this.AUTH_NAME);
            this.groupBox1.Controls.Add(this.P_NO);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(427, 296);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 266);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "Auth Place";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "Auth Desg";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 84;
            this.label5.Text = "Auth Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 83;
            this.label2.Text = "Enter Personnel No. :";
            // 
            // AUTH_PLACE
            // 
            this.AUTH_PLACE.AllowSpace = true;
            this.AUTH_PLACE.AssociatedLookUpName = "";
            this.AUTH_PLACE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AUTH_PLACE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.AUTH_PLACE.ContinuationTextBox = null;
            this.AUTH_PLACE.CustomEnabled = true;
            this.AUTH_PLACE.DataFieldMapping = "";
            this.AUTH_PLACE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AUTH_PLACE.GetRecordsOnUpDownKeys = false;
            this.AUTH_PLACE.IsDate = false;
            this.AUTH_PLACE.Location = new System.Drawing.Point(153, 264);
            this.AUTH_PLACE.MaxLength = 50;
            this.AUTH_PLACE.Name = "AUTH_PLACE";
            this.AUTH_PLACE.NumberFormat = "###,###,##0.00";
            this.AUTH_PLACE.Postfix = "";
            this.AUTH_PLACE.Prefix = "";
            this.AUTH_PLACE.Size = new System.Drawing.Size(199, 20);
            this.AUTH_PLACE.SkipValidation = false;
            this.AUTH_PLACE.TabIndex = 7;
            this.AUTH_PLACE.TextType = CrplControlLibrary.TextType.String;
            // 
            // AUTH_DESG
            // 
            this.AUTH_DESG.AllowSpace = true;
            this.AUTH_DESG.AssociatedLookUpName = "";
            this.AUTH_DESG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AUTH_DESG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.AUTH_DESG.ContinuationTextBox = null;
            this.AUTH_DESG.CustomEnabled = true;
            this.AUTH_DESG.DataFieldMapping = "";
            this.AUTH_DESG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AUTH_DESG.GetRecordsOnUpDownKeys = false;
            this.AUTH_DESG.IsDate = false;
            this.AUTH_DESG.Location = new System.Drawing.Point(153, 238);
            this.AUTH_DESG.MaxLength = 50;
            this.AUTH_DESG.Name = "AUTH_DESG";
            this.AUTH_DESG.NumberFormat = "###,###,##0.00";
            this.AUTH_DESG.Postfix = "";
            this.AUTH_DESG.Prefix = "";
            this.AUTH_DESG.Size = new System.Drawing.Size(199, 20);
            this.AUTH_DESG.SkipValidation = false;
            this.AUTH_DESG.TabIndex = 6;
            this.AUTH_DESG.TextType = CrplControlLibrary.TextType.String;
            // 
            // AUTH_NAME
            // 
            this.AUTH_NAME.AllowSpace = true;
            this.AUTH_NAME.AssociatedLookUpName = "";
            this.AUTH_NAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AUTH_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.AUTH_NAME.ContinuationTextBox = null;
            this.AUTH_NAME.CustomEnabled = true;
            this.AUTH_NAME.DataFieldMapping = "";
            this.AUTH_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AUTH_NAME.GetRecordsOnUpDownKeys = false;
            this.AUTH_NAME.IsDate = false;
            this.AUTH_NAME.Location = new System.Drawing.Point(153, 212);
            this.AUTH_NAME.MaxLength = 50;
            this.AUTH_NAME.Name = "AUTH_NAME";
            this.AUTH_NAME.NumberFormat = "###,###,##0.00";
            this.AUTH_NAME.Postfix = "";
            this.AUTH_NAME.Prefix = "";
            this.AUTH_NAME.Size = new System.Drawing.Size(199, 20);
            this.AUTH_NAME.SkipValidation = false;
            this.AUTH_NAME.TabIndex = 5;
            this.AUTH_NAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // P_NO
            // 
            this.P_NO.AllowSpace = true;
            this.P_NO.AssociatedLookUpName = "";
            this.P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_NO.ContinuationTextBox = null;
            this.P_NO.CustomEnabled = true;
            this.P_NO.DataFieldMapping = "";
            this.P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_NO.GetRecordsOnUpDownKeys = false;
            this.P_NO.IsDate = false;
            this.P_NO.Location = new System.Drawing.Point(153, 186);
            this.P_NO.MaxLength = 6;
            this.P_NO.Name = "P_NO";
            this.P_NO.NumberFormat = "###,###,##0.00";
            this.P_NO.Postfix = "";
            this.P_NO.Prefix = "";
            this.P_NO.Size = new System.Drawing.Size(199, 20);
            this.P_NO.SkipValidation = false;
            this.P_NO.TabIndex = 4;
            this.P_NO.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(153, 83);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(199, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 77;
            this.label1.Text = "DesType";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(133, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 13);
            this.label4.TabIndex = 75;
            this.label4.Text = "Enter values for the parameters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(150, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 17);
            this.label3.TabIndex = 74;
            this.label3.Text = "Report Parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(360, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 87;
            this.pictureBox2.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 88;
            this.label8.Text = "Desname";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 136);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 89;
            this.label9.Text = "Desformat";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 90;
            this.label10.Text = "Copies";
            // 
            // dest_name
            // 
            this.dest_name.AllowSpace = true;
            this.dest_name.AssociatedLookUpName = "";
            this.dest_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_name.ContinuationTextBox = null;
            this.dest_name.CustomEnabled = true;
            this.dest_name.DataFieldMapping = "";
            this.dest_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_name.GetRecordsOnUpDownKeys = false;
            this.dest_name.IsDate = false;
            this.dest_name.Location = new System.Drawing.Point(153, 111);
            this.dest_name.MaxLength = 50;
            this.dest_name.Name = "dest_name";
            this.dest_name.NumberFormat = "###,###,##0.00";
            this.dest_name.Postfix = "";
            this.dest_name.Prefix = "";
            this.dest_name.Size = new System.Drawing.Size(199, 20);
            this.dest_name.SkipValidation = false;
            this.dest_name.TabIndex = 1;
            this.dest_name.TextType = CrplControlLibrary.TextType.String;
            // 
            // dest_format
            // 
            this.dest_format.AllowSpace = true;
            this.dest_format.AssociatedLookUpName = "";
            this.dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_format.ContinuationTextBox = null;
            this.dest_format.CustomEnabled = true;
            this.dest_format.DataFieldMapping = "";
            this.dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_format.GetRecordsOnUpDownKeys = false;
            this.dest_format.IsDate = false;
            this.dest_format.Location = new System.Drawing.Point(153, 137);
            this.dest_format.MaxLength = 50;
            this.dest_format.Name = "dest_format";
            this.dest_format.NumberFormat = "###,###,##0.00";
            this.dest_format.Postfix = "";
            this.dest_format.Prefix = "";
            this.dest_format.Size = new System.Drawing.Size(199, 20);
            this.dest_format.SkipValidation = false;
            this.dest_format.TabIndex = 2;
            this.dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // Copies
            // 
            this.Copies.AllowSpace = true;
            this.Copies.AssociatedLookUpName = "";
            this.Copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Copies.ContinuationTextBox = null;
            this.Copies.CustomEnabled = true;
            this.Copies.DataFieldMapping = "";
            this.Copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Copies.GetRecordsOnUpDownKeys = false;
            this.Copies.IsDate = false;
            this.Copies.Location = new System.Drawing.Point(153, 163);
            this.Copies.MaxLength = 2;
            this.Copies.Name = "Copies";
            this.Copies.NumberFormat = "###,###,##0.00";
            this.Copies.Postfix = "";
            this.Copies.Prefix = "";
            this.Copies.Size = new System.Drawing.Size(199, 20);
            this.Copies.SkipValidation = false;
            this.Copies.TabIndex = 3;
            this.Copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // CHRIS_PersonnelReport_ReferenceLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 367);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.btnClose);
            this.Name = "CHRIS_PersonnelReport_ReferenceLetter";
            this.Text = "CHRIS_PersonnelReport_ReferenceLetter";
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.Controls.SetChildIndex(this.btnRun, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLButton btnRun;
        private CrplControlLibrary.SLButton btnClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox AUTH_PLACE;
        private CrplControlLibrary.SLTextBox AUTH_DESG;
        private CrplControlLibrary.SLTextBox AUTH_NAME;
        private CrplControlLibrary.SLTextBox P_NO;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox Copies;
        private CrplControlLibrary.SLTextBox dest_format;
        private CrplControlLibrary.SLTextBox dest_name;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}