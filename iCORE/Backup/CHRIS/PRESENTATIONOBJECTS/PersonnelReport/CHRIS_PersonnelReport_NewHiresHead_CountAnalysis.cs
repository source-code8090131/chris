using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_NewHiresHead_CountAnalysis : BaseRptForm
    {
        public CHRIS_PersonnelReport_NewHiresHead_CountAnalysis()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";
        public CHRIS_PersonnelReport_NewHiresHead_CountAnalysis(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            w_brn.Text = "ALL";
            w_group.Text ="ALL";
            w_seg.Text = "GF";
            Dest_name.Text = "C:\\iCORE-Spool\\";

        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            no_of_copies = Convert.ToInt16(nocopies.Text);
            {
                base.RptFileName = "PRREP52";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                   
                }
                if (Dest_Type.Text == "Printer")
                {
                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        
                        base.PrintNoofCopiesReport(no_of_copies);
                    }

                }



                if (Dest_Type.Text == "File")
                {
                    string d = "c:\\iCORE-Spool\\report";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");


                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_name.Text != string.Empty)
                        d = Dest_name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }
            }

        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

       
    }
}