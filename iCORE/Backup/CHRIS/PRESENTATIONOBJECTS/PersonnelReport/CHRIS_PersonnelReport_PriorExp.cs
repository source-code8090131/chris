using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_PriorExp : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_PriorExp()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_PriorExp(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.copies1.Text = "1";
            this.w_brn.Text = "FBD";
            this.w_desig.Text = "ALL";
            this.w_seg.Text = "GF";

        }
     
        private void button1_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PRREP39";
 /*
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport();
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                    DestName = @"C:\Report";
                if (DestName != string.Empty)
                {
                    base.ExportCustomReport(DestName, "PDF");
                }
            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\Report";
                string DestFormat;
                string RecipentName;
                RecipentName = "";

                if (DestName != string.Empty)
                {
                    base.EmailToReport(DestName, "PDF", RecipentName);
                }
            }
  */
            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.copies1.Text );
            }
            else if (cmbDescType.Text == "File")
            {

                string DestName;
                string DestFormat;
                if (txtDestName.Text == string.Empty)
                {
                    DestName = @"C:\iCORE-Spool\Report";
                }

                else
                {
                    DestName = this.txtDestName.Text;
                }

                    base.ExportCustomReport(DestName, "PDF");

            }
            else if (cmbDescType.Text == "Mail")
            {
                string DestName = @"C:\iCORE-Spool\Report";
                string DestFormat;
                string RecipentName;
                if (this.txtDestName.Text == string.Empty)
                {
                    RecipentName = "";
                }

                else
                {
                    RecipentName = this.txtDestName.Text;
                }

                    base.EmailToReport(DestName, "PDF", RecipentName);

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
         

        
    }
}