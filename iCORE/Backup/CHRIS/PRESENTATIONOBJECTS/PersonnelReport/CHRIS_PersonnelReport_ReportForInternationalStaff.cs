using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_ReportForInternationalStaff : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_ReportForInternationalStaff()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_ReportForInternationalStaff(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            nocopies.Text = "1";
            Dest_Format.Text = "WIDE";
            Dest_name.Text = "C:\\iCORE-Spool\\";
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            base.RptFileName = "PRREP05";
            if (this.Dest_Type.Text == "Screen" || this.Dest_Type.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (this.Dest_Type.Text == "Printer")
            {
                base.PrintCustomReport(this.nocopies.Text );
            }

            else if (Dest_Type.Text == "File")
            {
                string d = "C:\\iCORE-Spool\\Report";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;

                base.ExportCustomReport(d, "pdf");


            }


            else if (Dest_Type.Text == "Mail")
            {
                string d = "";
                if (Dest_name.Text != string.Empty)
                    d = Dest_name.Text;
                

                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}