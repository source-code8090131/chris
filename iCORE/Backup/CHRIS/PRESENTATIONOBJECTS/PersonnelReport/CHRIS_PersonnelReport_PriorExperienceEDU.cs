using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_PriorExperienceEDU : BaseRptForm
    {
        public CHRIS_PersonnelReport_PriorExperienceEDU()
        {
            InitializeComponent();
        }
        string DestName = @"C:\iCORE-Spool\Report";
        string DestFormat = "PDF";

        public CHRIS_PersonnelReport_PriorExperienceEDU(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.nocopies.Text = "1";
            this.W_BRN.Text = "FBD";
            this.W_SEG.Text = "GF";
            this.W_DESIG.Text = "ALL";
            this.YEAR.Text = "1995";
            //Output_mod.Items.RemoveAt(3);
            Dest_Name.Text = "C:\\iCORE-Spool\\";
        }

        private void Run_Click(object sender, EventArgs e)
        {
            int no_of_copies;
            {
                base.RptFileName = "PRREP38";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);

                }
                if (Dest_Type.Text == "Printer")
                {

                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        //no_of_copies = Convert.ToInt16(nocopies.Text == "" ? "1" : nocopies.Text);

                        base.PrintNoofCopiesReport(no_of_copies);
                    }


                }

                if (Dest_Type.Text == "File")
                {
                    string d = "C:\\iCORE-Spool\\Report";
                    if (Dest_Name.Text != string.Empty)
                        d = Dest_Name.Text;

                    base.ExportCustomReport(d, "pdf");
    

                }


                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_Name.Text != string.Empty)
                        d = Dest_Name.Text;


                    base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

                }

               
            }
    }

        private void Close_Click_1(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

        #endregion

    }
}