using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_AnalysisReport : BaseRptForm
    {
        public CHRIS_PersonnelReport_AnalysisReport()
        {
            InitializeComponent();
        }
        string DestFormat = "PDF";
        
        public CHRIS_PersonnelReport_AnalysisReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(this.Dest_Type.Items.Count - 1);
            this.nocopies.Text = "1";
            this.W_BRN.Text = "FBD";
            this.W_SEG.Text = "GF";
            this.W_DESIG.Text = "ALL";
            this.YEAR.Text = "1995";
            Dest_Name.Text = "C:\\iCORE-Spool\\";

        }

        private void Run_Click_1(object sender, EventArgs e)
        {
            int no_of_copies;
            {
                base.RptFileName = "PRREP33";
                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);                   
                }
                if (Dest_Type.Text == "Printer")
                {
                    if (nocopies.Text != string.Empty)
                    {
                        no_of_copies = Convert.ToInt16(nocopies.Text);
                        base.PrintNoofCopiesReport(no_of_copies);
                    }
                }
                if (Dest_Type.Text == "File")
                {
                    string d = "c:\\iCORE-Spool\\report";
                    if (Dest_Name.Text != string.Empty)
                        d = Dest_Name.Text;
                        base.ExportCustomReport(d, "PDF");
                }
                if (Dest_Type.Text == "Mail")
                {
                    string d = "";
                    if (Dest_Name.Text != string.Empty)
                        d = Dest_Name.Text;
                    base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", d);
                }
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }
    }
}