using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_AttritionReport : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        private string DestFormat = "PDF";
        public CHRIS_PersonnelReport_AttritionReport()
        {
            InitializeComponent();
        }


        public CHRIS_PersonnelReport_AttritionReport(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        
        #region code by Irfan Farooqui

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
            this.order.Text = "order by 1 desc";
            this.FillBranch();
            this.fillsegCombo();
            this.filldeptCombo();
            this.fillgroupcombo();
            this.fillcatcombo();
            this.filltypcombo();

            this.SG.SelectedIndex = (this.SG.Items.Count > 1 ? 1 : 0);
            this.BRN.SelectedIndex = (this.BRN.Items.Count > 1 ? 1 : 0);
            this.DPT.SelectedIndex = (this.DPT.Items.Count > 1 ? 1 : 0);
            this.GP.SelectedIndex = (this.GP.Items.Count > 1 ? 1 : 0);
            this.CATCD.SelectedIndex = (this.CATCD.Items.Count > 1 ? 1 : 0);
            this.TYPCD.SelectedIndex = (this.TYPCD.Items.Count > 1 ? 1 : 0);
        }

        private void FillBranch()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCountnull");

            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
            {
                this.BRN.DisplayMember = "BRN_CODE";
                this.BRN.ValueMember = "BRN_CODE";
                this.BRN.DataSource = rsltCode.dstResult.Tables[0];
                
            }

        }
        private void fillsegCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDM.Get("CHRIS_SP_BASE_LOV_ACTION", "SEGNEWNULL");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.SG.DisplayMember = "pr_segment";
                    this.SG.ValueMember = "pr_segment";
                    this.SG.DataSource = rsltCode.dstResult.Tables[0];
                    
                }

            }

        }

        private void filldeptCombo()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDM.Get("CHRIS_SP_DEPT_MANAGER", "DEPT_CMB_null");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.DPT.DisplayMember = "PR_DEPT";
                    this.DPT.ValueMember = "PR_DEPT";
                    this.DPT.DataSource = rsltCode.dstResult.Tables[0];
                    
                }

            }

        }

        private void fillgroupcombo()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDM.Get("CHRIS_SP_DEPT_MANAGER", "GROUP_CMB_null");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.GP.DisplayMember = "SP_GROUP_D";
                    this.GP.ValueMember = "SP_GROUP_D";
                    this.GP.DataSource = rsltCode.dstResult.Tables[0];
                    
                }

            }

        }


        private void fillcatcombo()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDM.Get("CHRIS_SP_CATEGORY_MANAGER", "CAT_CMB_null");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.CATCD.DisplayMember = "ATTR_CATEGORY_DESC";
                    this.CATCD.ValueMember = "ATTR_CATEGORY_DESC";
                    this.CATCD.DataSource = rsltCode.dstResult.Tables[0];
                    
                }

            }

        }


        private void filltypcombo()
        {
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();

            Dictionary<string, object> param = new Dictionary<string, object>();
            rsltCode = cmnDM.Get("CHRIS_SP_CATEGORY_MANAGER", "TYP_CMB_null");

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    this.TYPCD.DisplayMember = "ATTR_TYPE_DESC";
                    this.TYPCD.ValueMember = "ATTR_TYPE_DESC";
                    this.TYPCD.DataSource = rsltCode.dstResult.Tables[0];
                    
                }

            }

        }
        private void Run_Click(object sender, EventArgs e)
        {
            {
                base.RptFileName = "ATTRITION_REP1";


                if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
                {

                    base.btnCallReport_Click(sender, e);
                    //if (Dest_Format.Text!= string.Empty)
                    //{
                    //   base.ExportCustomReport();
                    //}
                }
                if (Dest_Type.Text == "Printer")
                {
                    //base.MatrixReport = true;
                    ///if (Dest_Format.Text!= string.Empty)
                    ///{
                    ///base.ExportCustomReport();
                    /// }
                    base.PrintCustomReport();
                    //base.ExportCustomReport();
                    //DataSet ds = base.PrintCustomReport();

                }

                if (Dest_Type.Text == "File")
                {
                    //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                    //if (Dest_name.Text != string.Empty)
                    string d = "c:\\iCORE-Spool\\report";
                    if (Dest_Name.Text != string.Empty)
                        d = Dest_Name.Text;
                    base.ExportCustomReport(d, "pdf");




                    //base.ExportCustomReport();



                }

            }

            if (Dest_Type.Text == "Mail")
            {
                string d = "";
                if (Dest_Name.Text != string.Empty)
                    d = Dest_Name.Text;

                base.EmailToReport("C:\\iCORE-Spool\\Report", "pdf", d);



                //if (Dest_Format.Text != string.Empty || Dest_name.Text != string.Empty)
                //{
                //    base.EmailToReport(Dest_name.Text, Dest_Format.Text);
                //}
            }
            //if (Dest_Format.Text != string.Empty && Dest_Format.Text == "PDF")
            //{
            //    string Path =base.ExportReportInGivenFormat("PDF");

            //}




        }

        
        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);//this.Close();
        }

        #endregion 

    }
}


