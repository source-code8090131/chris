using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_EmployeesForProbation : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_EmployeesForProbation()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_EmployeesForProbation(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.cmbDescType.Items.RemoveAt(this.cmbDescType.Items.Count - 1);
            this.W_BRN.Text = "ALL";
            this.w_seg.Text = "ALL";
            this.w_level.Text = "ALL";
            this.w_desig.Text = "ALL";
            this.copies.Text = "1";
            this.dest_format.Text = "wide";
            dest_name.Text = "C:\\iCORE-Spool\\";
        }


        private void Run_Click(object sender, EventArgs e)
        {
            base.RptFileName = "Prrep03"; //write your report name

            if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
            {
                base.btnCallReport_Click(sender, e);
            }
            else if (cmbDescType.Text == "Mail")
            {
                string d = "";
                if (this.dest_name.Text != string.Empty)
                    d = this.dest_name.Text;

                base.EmailToReport("c:\\iCORE-Spool\\PRREP03", "PDF", d);
            }
            else if (cmbDescType.Text == "File")
            {
                string d = "c:\\iCORE-Spool\\report";
                if (this.dest_name.Text != string.Empty)
                    d = this.dest_name.Text;

                base.ExportCustomReport(d, "PDF");
            }
            else if (cmbDescType.Text == "Printer")
            {
                base.PrintCustomReport(this.copies.Text); 
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}