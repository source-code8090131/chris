using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.PersonnelReport
{
    public partial class CHRIS_PersonnelReport_EmployeesCurrentSalaryStatus : BaseRptForm 
        //iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_PersonnelReport_EmployeesCurrentSalaryStatus()
        {
            InitializeComponent();
        }

        public CHRIS_PersonnelReport_EmployeesCurrentSalaryStatus(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Dest_Type.Items.RemoveAt(5);
           // slComboBox1.Items.RemoveAt(3);
            Dest_Format.Text = "wide";
            this.copies.Text = "1";
            this.BRANCH.Text = "nul";

        }
        private void btnRUN_Click(object sender, EventArgs e)
        {
          
            base.RptFileName = "PY018";

            if (Dest_Type.Text == "Screen" || Dest_Type.Text == "Preview")
            {

                base.btnCallReport_Click(sender, e);

            }
            if (Dest_Type.Text == "Printer")
            {
                PrintCustomReport();
            }
            if (Dest_Type.Text == "File")

            {
                string d = "C:\\iCORE-Spool\\Report";
                if (this.Dest_name.Text != string.Empty)
                    d = Dest_name.Text;

                    base.ExportCustomReport(d, "pdf");



            }

            if (Dest_Type.Text == "Mail")
            {
                string d = "";
                if (this.Dest_name.Text != string.Empty)
                    d = Dest_name.Text;


                base.EmailToReport(@"C:\iCORE-Spool\Report", "PDF", d);

            }
   
        }

        private void Close_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

       
    }
}