namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_AccountEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_AccountEntry));
            this.pnlTblAccountEntry = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvAccEntry = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OPTION_DIS = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_USER = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNo = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblAccountEntry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccEntry)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(565, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(601, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 347);
            this.panel1.Size = new System.Drawing.Size(601, 60);
            // 
            // pnlTblAccountEntry
            // 
            this.pnlTblAccountEntry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTblAccountEntry.ConcurrentPanels = null;
            this.pnlTblAccountEntry.Controls.Add(this.dgvAccEntry);
            this.pnlTblAccountEntry.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblAccountEntry.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblAccountEntry.DependentPanels = null;
            this.pnlTblAccountEntry.DisableDependentLoad = false;
            this.pnlTblAccountEntry.EnableDelete = true;
            this.pnlTblAccountEntry.EnableInsert = true;
            this.pnlTblAccountEntry.EnableQuery = false;
            this.pnlTblAccountEntry.EnableUpdate = true;
            this.pnlTblAccountEntry.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AccEntCommand";
            this.pnlTblAccountEntry.Location = new System.Drawing.Point(9, 146);
            this.pnlTblAccountEntry.MasterPanel = null;
            this.pnlTblAccountEntry.Name = "pnlTblAccountEntry";
            this.pnlTblAccountEntry.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblAccountEntry.Size = new System.Drawing.Size(584, 213);
            this.pnlTblAccountEntry.SPName = "CHRIS_SP_AccEnt_ACCOUNT_MANAGER";
            this.pnlTblAccountEntry.TabIndex = 0;
            // 
            // dgvAccEntry
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAccEntry.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAccEntry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccEntry.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.AccountNo,
            this.Description,
            this.Description1,
            this.AccountType});
            this.dgvAccEntry.ColumnToHide = null;
            this.dgvAccEntry.ColumnWidth = null;
            this.dgvAccEntry.CustomEnabled = true;
            this.dgvAccEntry.DisplayColumnWrapper = null;
            this.dgvAccEntry.GridDefaultRow = 0;
            this.dgvAccEntry.Location = new System.Drawing.Point(5, 3);
            this.dgvAccEntry.Name = "dgvAccEntry";
            this.dgvAccEntry.ReadOnlyColumns = null;
            this.dgvAccEntry.RequiredColumns = null;
            this.dgvAccEntry.Size = new System.Drawing.Size(574, 207);
            this.dgvAccEntry.SkippingColumns = null;
            this.dgvAccEntry.TabIndex = 0;
            this.dgvAccEntry.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccEntry_RowEnter);
            this.dgvAccEntry.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccEntry_CellValidated);
            this.dgvAccEntry.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvAccEntry_CellValidating);
            this.dgvAccEntry.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccEntry_CellEnter);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txt_W_OPTION_DIS);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txt_W_USER);
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.label4);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.lblLocation);
            this.pnlHead.Controls.Add(this.lblUser);
            this.pnlHead.Location = new System.Drawing.Point(4, 83);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(584, 57);
            this.pnlHead.TabIndex = 13;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(485, 30);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(89, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 9;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OPTION_DIS
            // 
            this.txt_W_OPTION_DIS.AllowSpace = true;
            this.txt_W_OPTION_DIS.AssociatedLookUpName = "";
            this.txt_W_OPTION_DIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OPTION_DIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OPTION_DIS.ContinuationTextBox = null;
            this.txt_W_OPTION_DIS.CustomEnabled = true;
            this.txt_W_OPTION_DIS.DataFieldMapping = "";
            this.txt_W_OPTION_DIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OPTION_DIS.GetRecordsOnUpDownKeys = false;
            this.txt_W_OPTION_DIS.IsDate = false;
            this.txt_W_OPTION_DIS.Location = new System.Drawing.Point(485, 8);
            this.txt_W_OPTION_DIS.Name = "txt_W_OPTION_DIS";
            this.txt_W_OPTION_DIS.NumberFormat = "###,###,##0.00";
            this.txt_W_OPTION_DIS.Postfix = "";
            this.txt_W_OPTION_DIS.Prefix = "";
            this.txt_W_OPTION_DIS.ReadOnly = true;
            this.txt_W_OPTION_DIS.Size = new System.Drawing.Size(57, 20);
            this.txt_W_OPTION_DIS.SkipValidation = false;
            this.txt_W_OPTION_DIS.TabIndex = 8;
            this.txt_W_OPTION_DIS.TabStop = false;
            this.txt_W_OPTION_DIS.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_OPTION_DIS.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(81, 31);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(100, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 7;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_USER
            // 
            this.txt_W_USER.AllowSpace = true;
            this.txt_W_USER.AssociatedLookUpName = "";
            this.txt_W_USER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_USER.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_USER.ContinuationTextBox = null;
            this.txt_W_USER.CustomEnabled = true;
            this.txt_W_USER.DataFieldMapping = "";
            this.txt_W_USER.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_USER.GetRecordsOnUpDownKeys = false;
            this.txt_W_USER.IsDate = false;
            this.txt_W_USER.Location = new System.Drawing.Point(81, 8);
            this.txt_W_USER.Name = "txt_W_USER";
            this.txt_W_USER.NumberFormat = "###,###,##0.00";
            this.txt_W_USER.Postfix = "";
            this.txt_W_USER.Prefix = "";
            this.txt_W_USER.ReadOnly = true;
            this.txt_W_USER.Size = new System.Drawing.Size(100, 20);
            this.txt_W_USER.SkipValidation = false;
            this.txt_W_USER.TabIndex = 6;
            this.txt_W_USER.TabStop = false;
            this.txt_W_USER.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(443, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(435, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Option :";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(235, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "A C C O U N T   E N T R Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(274, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "S e t  U p";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocation.Location = new System.Drawing.Point(10, 31);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(64, 13);
            this.lblLocation.TabIndex = 1;
            this.lblLocation.Text = "Location :";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(33, 8);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(41, 13);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "User :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(407, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "User Name : ";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(469, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(25, 13);
            this.lblUserName.TabIndex = 14;
            this.lblUserName.Text = "      ";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            this.ID.Width = 40;
            // 
            // AccountNo
            // 
            this.AccountNo.ActionLOV = "ACCNO_LOV";
            this.AccountNo.ActionLOVExists = "";
            this.AccountNo.AttachParentEntity = false;
            this.AccountNo.DataPropertyName = "SP_ACC_NO";
            this.AccountNo.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.AccEntCommand";
            this.AccountNo.HeaderText = "Account No";
            this.AccountNo.LookUpTitle = null;
            this.AccountNo.LOVFieldMapping = "SP_ACC_NO";
            this.AccountNo.Name = "AccountNo";
            this.AccountNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AccountNo.SearchColumn = "SP_ACC_NO";
            this.AccountNo.SkipValidationOnLeave = false;
            this.AccountNo.SpName = "CHRIS_SP_AccEnt_ACCOUNT_MANAGER";
            this.AccountNo.Width = 105;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "SP_ACC_DESC";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.Width = 195;
            // 
            // Description1
            // 
            this.Description1.DataPropertyName = "SP_ACC_DESC1";
            this.Description1.HeaderText = "Description";
            this.Description1.Name = "Description1";
            this.Description1.Width = 195;
            // 
            // AccountType
            // 
            this.AccountType.DataPropertyName = "SP_ACC_TYPE";
            this.AccountType.HeaderText = "Account Type";
            this.AccountType.MaxInputLength = 1;
            this.AccountType.Name = "AccountType";
            this.AccountType.Width = 130;
            // 
            // CHRIS_Setup_AccountEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 407);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlTblAccountEntry);
            this.F1OptionText = resources.GetString("$this.F1OptionText");
            this.F3OptionText = "";
            this.F4OptionText = "";
            this.F6OptionText = "";
            this.F7OptionText = "";
            this.F8OptionText = "";
            this.Name = "CHRIS_Setup_AccountEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = " ";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlTblAccountEntry, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblAccountEntry.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccEntry)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblAccountEntry;
        private System.Windows.Forms.Panel pnlHead;
        public CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txt_W_OPTION_DIS;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txt_W_USER;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label lblUser;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvAccEntry;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn AccountNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description1;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountType;
    }
}