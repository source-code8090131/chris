namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_TestPayrollProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_TestPayrollProcess));
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlPersonnal = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.btnLog = new System.Windows.Forms.Button();
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.lblPrNum = new System.Windows.Forms.Label();
            this.lblHeader = new System.Windows.Forms.Label();
            this.groupBoxMainProcess = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtpProcDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProcName = new CrplControlLibrary.SLTextBox(this.components);
            this.dgvSFL = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.LOG_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOG_FLD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOG_ENTRY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblEducation = new System.Windows.Forms.Label();
            this.txtEducation = new CrplControlLibrary.SLTextBox(this.components);
            this.lblFunctional = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblRVP = new System.Windows.Forms.Label();
            this.txtGenStatus = new CrplControlLibrary.SLTextBox(this.components);
            this.txtNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSubsLoanTAmnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotalTaxAmnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRVP = new CrplControlLibrary.SLTextBox(this.components);
            this.lblName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxProcessRunning = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTB_EmpGroup_Answer = new CrplControlLibrary.SLTextBox(this.components);
            this.slTB_Pr_P_No = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBoxProcessCompleted = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.slTB_FinalAnswer = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlPersonnal.SuspendLayout();
            this.groupBoxMainProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSFL)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBoxProcessRunning.SuspendLayout();
            this.groupBoxProcessCompleted.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(718, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(754, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 575);
            this.panel1.Size = new System.Drawing.Size(754, 60);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(546, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 70;
            this.lblUserName.Text = "User Name :";
            // 
            // pnlPersonnal
            // 
            this.pnlPersonnal.ConcurrentPanels = null;
            this.pnlPersonnal.Controls.Add(this.btnLog);
            this.pnlPersonnal.Controls.Add(this.txtPersNo);
            this.pnlPersonnal.Controls.Add(this.lblPrNum);
            this.pnlPersonnal.DataManager = null;
            this.pnlPersonnal.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonnal.DependentPanels = null;
            this.pnlPersonnal.DisableDependentLoad = false;
            this.pnlPersonnal.EnableDelete = true;
            this.pnlPersonnal.EnableInsert = true;
            this.pnlPersonnal.EnableQuery = false;
            this.pnlPersonnal.EnableUpdate = true;
            this.pnlPersonnal.EntityName = null;
            this.pnlPersonnal.Location = new System.Drawing.Point(6, 39);
            this.pnlPersonnal.MasterPanel = null;
            this.pnlPersonnal.Name = "pnlPersonnal";
            this.pnlPersonnal.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonnal.Size = new System.Drawing.Size(240, 80);
            this.pnlPersonnal.SPName = null;
            this.pnlPersonnal.TabIndex = 0;
            this.pnlPersonnal.TabStop = true;
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(80, 49);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(75, 23);
            this.btnLog.TabIndex = 1;
            this.btnLog.Text = "Clear Log";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(62, 25);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(117, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPersNo, "Enter Valid Personnal Number");
            this.txtPersNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersNo_KeyPress);
            // 
            // lblPrNum
            // 
            this.lblPrNum.AutoSize = true;
            this.lblPrNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrNum.Location = new System.Drawing.Point(63, 8);
            this.lblPrNum.Name = "lblPrNum";
            this.lblPrNum.Size = new System.Drawing.Size(83, 13);
            this.lblPrNum.TabIndex = 79;
            this.lblPrNum.Text = "Personnel No.  :";
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(340, 66);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(190, 16);
            this.lblHeader.TabIndex = 79;
            this.lblHeader.Text = "PAYROLL             SYSTEM";
            // 
            // groupBoxMainProcess
            // 
            this.groupBoxMainProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBoxMainProcess.ConcurrentPanels = null;
            this.groupBoxMainProcess.Controls.Add(this.dtpProcDate);
            this.groupBoxMainProcess.Controls.Add(this.label5);
            this.groupBoxMainProcess.Controls.Add(this.label6);
            this.groupBoxMainProcess.Controls.Add(this.label7);
            this.groupBoxMainProcess.Controls.Add(this.label4);
            this.groupBoxMainProcess.Controls.Add(this.label3);
            this.groupBoxMainProcess.Controls.Add(this.label2);
            this.groupBoxMainProcess.Controls.Add(this.txtProcName);
            this.groupBoxMainProcess.Controls.Add(this.dgvSFL);
            this.groupBoxMainProcess.Controls.Add(this.lblEducation);
            this.groupBoxMainProcess.Controls.Add(this.txtEducation);
            this.groupBoxMainProcess.Controls.Add(this.lblFunctional);
            this.groupBoxMainProcess.Controls.Add(this.lblBranch);
            this.groupBoxMainProcess.Controls.Add(this.lblLevel);
            this.groupBoxMainProcess.Controls.Add(this.lblRVP);
            this.groupBoxMainProcess.Controls.Add(this.txtGenStatus);
            this.groupBoxMainProcess.Controls.Add(this.txtNo);
            this.groupBoxMainProcess.Controls.Add(this.txtSubsLoanTAmnt);
            this.groupBoxMainProcess.Controls.Add(this.txtTotalTaxAmnt);
            this.groupBoxMainProcess.Controls.Add(this.txtRVP);
            this.groupBoxMainProcess.Controls.Add(this.lblName);
            this.groupBoxMainProcess.DataManager = "iCORE.Common.CommonDataManager";
            this.groupBoxMainProcess.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.groupBoxMainProcess.DependentPanels = null;
            this.groupBoxMainProcess.DisableDependentLoad = false;
            this.groupBoxMainProcess.EnableDelete = true;
            this.groupBoxMainProcess.EnableInsert = true;
            this.groupBoxMainProcess.EnableQuery = false;
            this.groupBoxMainProcess.EnableUpdate = true;
            this.groupBoxMainProcess.EntityName = "";
            this.groupBoxMainProcess.Location = new System.Drawing.Point(8, 177);
            this.groupBoxMainProcess.MasterPanel = null;
            this.groupBoxMainProcess.Name = "groupBoxMainProcess";
            this.groupBoxMainProcess.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.groupBoxMainProcess.Size = new System.Drawing.Size(738, 331);
            this.groupBoxMainProcess.SPName = "";
            this.groupBoxMainProcess.TabIndex = 81;
            this.groupBoxMainProcess.TabStop = true;
            // 
            // dtpProcDate
            // 
            this.dtpProcDate.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProcDate.CustomEnabled = true;
            this.dtpProcDate.CustomFormat = "dd/MM/yyyy";
            this.dtpProcDate.DataFieldMapping = "";
            this.dtpProcDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProcDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProcDate.HasChanges = true;
            this.dtpProcDate.IsRequired = true;
            this.dtpProcDate.Location = new System.Drawing.Point(547, 10);
            this.dtpProcDate.Name = "dtpProcDate";
            this.dtpProcDate.NullValue = " ";
            this.dtpProcDate.Size = new System.Drawing.Size(121, 21);
            this.dtpProcDate.TabIndex = 2;
            this.toolTip1.SetToolTip(this.dtpProcDate, "Enter Valid Processing Date .....");
            this.dtpProcDate.Value = new System.DateTime(2011, 1, 5, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(629, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 16);
            this.label5.TabIndex = 23;
            this.label5.Text = "]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(615, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 16);
            this.label6.TabIndex = 22;
            this.label6.Text = "]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(667, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 21;
            this.label7.Text = "]";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(533, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "[";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(534, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "[";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(533, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "[";
            // 
            // txtProcName
            // 
            this.txtProcName.AllowSpace = true;
            this.txtProcName.AssociatedLookUpName = "";
            this.txtProcName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProcName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProcName.ContinuationTextBox = null;
            this.txtProcName.CustomEnabled = true;
            this.txtProcName.DataFieldMapping = "W_PROC_NAME";
            this.txtProcName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcName.GetRecordsOnUpDownKeys = false;
            this.txtProcName.IsDate = false;
            this.txtProcName.Location = new System.Drawing.Point(239, 99);
            this.txtProcName.MaxLength = 100;
            this.txtProcName.Name = "txtProcName";
            this.txtProcName.NumberFormat = "###,###,##0.00";
            this.txtProcName.Postfix = "";
            this.txtProcName.Prefix = "";
            this.txtProcName.ReadOnly = true;
            this.txtProcName.Size = new System.Drawing.Size(473, 20);
            this.txtProcName.SkipValidation = false;
            this.txtProcName.TabIndex = 5;
            this.txtProcName.TabStop = false;
            this.txtProcName.TextType = CrplControlLibrary.TextType.String;
            // 
            // dgvSFL
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSFL.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSFL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSFL.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LOG_NO,
            this.LOG_FLD,
            this.LOG_ENTRY});
            this.dgvSFL.ColumnToHide = null;
            this.dgvSFL.ColumnWidth = null;
            this.dgvSFL.CustomEnabled = true;
            this.dgvSFL.DisplayColumnWrapper = null;
            this.dgvSFL.GridDefaultRow = 0;
            this.dgvSFL.Location = new System.Drawing.Point(15, 125);
            this.dgvSFL.Name = "dgvSFL";
            this.dgvSFL.ReadOnlyColumns = null;
            this.dgvSFL.RequiredColumns = null;
            this.dgvSFL.Size = new System.Drawing.Size(706, 200);
            this.dgvSFL.SkippingColumns = null;
            this.dgvSFL.TabIndex = 6;
            // 
            // LOG_NO
            // 
            this.LOG_NO.DataPropertyName = "LOG_NO";
            this.LOG_NO.FillWeight = 20F;
            this.LOG_NO.Frozen = true;
            this.LOG_NO.HeaderText = "SNO";
            this.LOG_NO.Name = "LOG_NO";
            this.LOG_NO.ReadOnly = true;
            this.LOG_NO.Width = 90;
            // 
            // LOG_FLD
            // 
            this.LOG_FLD.DataPropertyName = "LOG_FLD";
            this.LOG_FLD.FillWeight = 20F;
            this.LOG_FLD.Frozen = true;
            this.LOG_FLD.HeaderText = "FIELD";
            this.LOG_FLD.Name = "LOG_FLD";
            this.LOG_FLD.ReadOnly = true;
            this.LOG_FLD.Width = 120;
            // 
            // LOG_ENTRY
            // 
            this.LOG_ENTRY.DataPropertyName = "LOG_ENTRY";
            this.LOG_ENTRY.FillWeight = 20F;
            this.LOG_ENTRY.HeaderText = "LOG Entry";
            this.LOG_ENTRY.Name = "LOG_ENTRY";
            this.LOG_ENTRY.ReadOnly = true;
            this.LOG_ENTRY.Width = 570;
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEducation.Location = new System.Drawing.Point(296, 226);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(72, 13);
            this.lblEducation.TabIndex = 14;
            this.lblEducation.Text = "Education :";
            // 
            // txtEducation
            // 
            this.txtEducation.AllowSpace = true;
            this.txtEducation.AssociatedLookUpName = "";
            this.txtEducation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEducation.ContinuationTextBox = null;
            this.txtEducation.CustomEnabled = true;
            this.txtEducation.DataFieldMapping = "DGREE";
            this.txtEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEducation.GetRecordsOnUpDownKeys = false;
            this.txtEducation.IsDate = false;
            this.txtEducation.Location = new System.Drawing.Point(374, 222);
            this.txtEducation.MaxLength = 10;
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.NumberFormat = "###,###,##0.00";
            this.txtEducation.Postfix = "";
            this.txtEducation.Prefix = "";
            this.txtEducation.ReadOnly = true;
            this.txtEducation.Size = new System.Drawing.Size(117, 20);
            this.txtEducation.SkipValidation = false;
            this.txtEducation.TabIndex = 15;
            this.txtEducation.TabStop = false;
            this.txtEducation.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblFunctional
            // 
            this.lblFunctional.AutoSize = true;
            this.lblFunctional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunctional.Location = new System.Drawing.Point(28, 51);
            this.lblFunctional.Name = "lblFunctional";
            this.lblFunctional.Size = new System.Drawing.Size(138, 13);
            this.lblFunctional.TabIndex = 1;
            this.lblFunctional.Text = "Subs Loan Tax Amount";
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            this.lblBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(28, 12);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(131, 13);
            this.lblBranch.TabIndex = 1;
            this.lblBranch.Text = "Total Taxable Amount";
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(248, 71);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(268, 13);
            this.lblLevel.TabIndex = 331;
            this.lblLevel.Text = "[YES] Start Generation   OR [NO]   To Exit     ";
            // 
            // lblRVP
            // 
            this.lblRVP.AutoSize = true;
            this.lblRVP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRVP.Location = new System.Drawing.Point(245, 42);
            this.lblRVP.Name = "lblRVP";
            this.lblRVP.Size = new System.Drawing.Size(246, 13);
            this.lblRVP.TabIndex = 1;
            this.lblRVP.Text = "Transportation for VP and  RVP.............. ";
            // 
            // txtGenStatus
            // 
            this.txtGenStatus.AllowSpace = true;
            this.txtGenStatus.AssociatedLookUpName = "";
            this.txtGenStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGenStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGenStatus.ContinuationTextBox = null;
            this.txtGenStatus.CustomEnabled = true;
            this.txtGenStatus.DataFieldMapping = "W_REPLY";
            this.txtGenStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGenStatus.GetRecordsOnUpDownKeys = false;
            this.txtGenStatus.IsDate = false;
            this.txtGenStatus.Location = new System.Drawing.Point(547, 67);
            this.txtGenStatus.MaxLength = 3;
            this.txtGenStatus.Name = "txtGenStatus";
            this.txtGenStatus.NumberFormat = "###,###,##0.00";
            this.txtGenStatus.Postfix = "";
            this.txtGenStatus.Prefix = "";
            this.txtGenStatus.Size = new System.Drawing.Size(67, 20);
            this.txtGenStatus.SkipValidation = false;
            this.txtGenStatus.TabIndex = 4;
            this.txtGenStatus.TextType = CrplControlLibrary.TextType.String;
            this.txtGenStatus.Leave += new System.EventHandler(this.txtGenStatus_Leave);
            this.txtGenStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGenStatus_KeyPress);
            // 
            // txtNo
            // 
            this.txtNo.AllowSpace = true;
            this.txtNo.AssociatedLookUpName = "";
            this.txtNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNo.ContinuationTextBox = null;
            this.txtNo.CustomEnabled = true;
            this.txtNo.DataFieldMapping = "PR_FUNC_TITTLE2";
            this.txtNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNo.GetRecordsOnUpDownKeys = false;
            this.txtNo.IsDate = false;
            this.txtNo.Location = new System.Drawing.Point(31, 99);
            this.txtNo.MaxLength = 10;
            this.txtNo.Name = "txtNo";
            this.txtNo.NumberFormat = "###,###,##0.00";
            this.txtNo.Postfix = "";
            this.txtNo.Prefix = "";
            this.txtNo.ReadOnly = true;
            this.txtNo.Size = new System.Drawing.Size(189, 20);
            this.txtNo.SkipValidation = false;
            this.txtNo.TabIndex = 5;
            this.txtNo.TabStop = false;
            this.txtNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSubsLoanTAmnt
            // 
            this.txtSubsLoanTAmnt.AllowSpace = true;
            this.txtSubsLoanTAmnt.AssociatedLookUpName = "";
            this.txtSubsLoanTAmnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubsLoanTAmnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSubsLoanTAmnt.ContinuationTextBox = null;
            this.txtSubsLoanTAmnt.CustomEnabled = true;
            this.txtSubsLoanTAmnt.DataFieldMapping = "SUBS_LOAN_TAX_AMT";
            this.txtSubsLoanTAmnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubsLoanTAmnt.GetRecordsOnUpDownKeys = false;
            this.txtSubsLoanTAmnt.IsDate = false;
            this.txtSubsLoanTAmnt.Location = new System.Drawing.Point(31, 67);
            this.txtSubsLoanTAmnt.MaxLength = 30;
            this.txtSubsLoanTAmnt.Name = "txtSubsLoanTAmnt";
            this.txtSubsLoanTAmnt.NumberFormat = "###,###,##0.00";
            this.txtSubsLoanTAmnt.Postfix = "";
            this.txtSubsLoanTAmnt.Prefix = "";
            this.txtSubsLoanTAmnt.ReadOnly = true;
            this.txtSubsLoanTAmnt.Size = new System.Drawing.Size(189, 20);
            this.txtSubsLoanTAmnt.SkipValidation = false;
            this.txtSubsLoanTAmnt.TabIndex = 5;
            this.txtSubsLoanTAmnt.TabStop = false;
            this.txtSubsLoanTAmnt.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTotalTaxAmnt
            // 
            this.txtTotalTaxAmnt.AllowSpace = true;
            this.txtTotalTaxAmnt.AssociatedLookUpName = "";
            this.txtTotalTaxAmnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalTaxAmnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalTaxAmnt.ContinuationTextBox = null;
            this.txtTotalTaxAmnt.CustomEnabled = true;
            this.txtTotalTaxAmnt.DataFieldMapping = "Z_TAXABLE_AMOUNT";
            this.txtTotalTaxAmnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalTaxAmnt.GetRecordsOnUpDownKeys = false;
            this.txtTotalTaxAmnt.IsDate = false;
            this.txtTotalTaxAmnt.Location = new System.Drawing.Point(31, 28);
            this.txtTotalTaxAmnt.MaxLength = 10;
            this.txtTotalTaxAmnt.Name = "txtTotalTaxAmnt";
            this.txtTotalTaxAmnt.NumberFormat = "###,###,##0.00";
            this.txtTotalTaxAmnt.Postfix = "";
            this.txtTotalTaxAmnt.Prefix = "";
            this.txtTotalTaxAmnt.ReadOnly = true;
            this.txtTotalTaxAmnt.Size = new System.Drawing.Size(189, 20);
            this.txtTotalTaxAmnt.SkipValidation = false;
            this.txtTotalTaxAmnt.TabIndex = 5;
            this.txtTotalTaxAmnt.TabStop = false;
            this.txtTotalTaxAmnt.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRVP
            // 
            this.txtRVP.AllowSpace = true;
            this.txtRVP.AssociatedLookUpName = "";
            this.txtRVP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRVP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRVP.ContinuationTextBox = null;
            this.txtRVP.CustomEnabled = true;
            this.txtRVP.DataFieldMapping = "W_VP";
            this.txtRVP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRVP.GetRecordsOnUpDownKeys = false;
            this.txtRVP.IsDate = false;
            this.txtRVP.Location = new System.Drawing.Point(547, 38);
            this.txtRVP.MaxLength = 8;
            this.txtRVP.Name = "txtRVP";
            this.txtRVP.NumberFormat = "###,###,##0.00";
            this.txtRVP.Postfix = "";
            this.txtRVP.Prefix = "";
            this.txtRVP.Size = new System.Drawing.Size(81, 20);
            this.txtRVP.SkipValidation = false;
            this.txtRVP.TabIndex = 3;
            this.txtRVP.Text = "40000";
            this.txtRVP.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(245, 13);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(284, 13);
            this.lblName.TabIndex = 111;
            this.lblName.Text = "Payroll Processing Date        (DD/MM/YYYY).....";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(266, 120);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(302, 57);
            this.panel2.TabIndex = 82;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 16);
            this.label1.TabIndex = 81;
            this.label1.Text = "PAYROLL            GENERATION";
            // 
            // groupBoxProcessRunning
            // 
            this.groupBoxProcessRunning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBoxProcessRunning.ConcurrentPanels = null;
            this.groupBoxProcessRunning.Controls.Add(this.slTB_EmpGroup_Answer);
            this.groupBoxProcessRunning.Controls.Add(this.slTB_Pr_P_No);
            this.groupBoxProcessRunning.Controls.Add(this.label8);
            this.groupBoxProcessRunning.DataManager = null;
            this.groupBoxProcessRunning.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.groupBoxProcessRunning.DependentPanels = null;
            this.groupBoxProcessRunning.DisableDependentLoad = false;
            this.groupBoxProcessRunning.EnableDelete = true;
            this.groupBoxProcessRunning.EnableInsert = true;
            this.groupBoxProcessRunning.EnableQuery = false;
            this.groupBoxProcessRunning.EnableUpdate = true;
            this.groupBoxProcessRunning.EntityName = null;
            this.groupBoxProcessRunning.Location = new System.Drawing.Point(116, 509);
            this.groupBoxProcessRunning.MasterPanel = null;
            this.groupBoxProcessRunning.Name = "groupBoxProcessRunning";
            this.groupBoxProcessRunning.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.groupBoxProcessRunning.Size = new System.Drawing.Size(507, 63);
            this.groupBoxProcessRunning.SPName = null;
            this.groupBoxProcessRunning.TabIndex = 83;
            this.groupBoxProcessRunning.TabStop = true;
            this.groupBoxProcessRunning.Visible = false;
            // 
            // slTB_EmpGroup_Answer
            // 
            this.slTB_EmpGroup_Answer.AllowSpace = true;
            this.slTB_EmpGroup_Answer.AssociatedLookUpName = "";
            this.slTB_EmpGroup_Answer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_EmpGroup_Answer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_EmpGroup_Answer.ContinuationTextBox = null;
            this.slTB_EmpGroup_Answer.CustomEnabled = true;
            this.slTB_EmpGroup_Answer.DataFieldMapping = "W_ANS";
            this.slTB_EmpGroup_Answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_EmpGroup_Answer.GetRecordsOnUpDownKeys = false;
            this.slTB_EmpGroup_Answer.IsDate = false;
            this.slTB_EmpGroup_Answer.Location = new System.Drawing.Point(303, 34);
            this.slTB_EmpGroup_Answer.MaxLength = 6;
            this.slTB_EmpGroup_Answer.Name = "slTB_EmpGroup_Answer";
            this.slTB_EmpGroup_Answer.NumberFormat = "###,###,##0.00";
            this.slTB_EmpGroup_Answer.Postfix = "";
            this.slTB_EmpGroup_Answer.Prefix = "";
            this.slTB_EmpGroup_Answer.Size = new System.Drawing.Size(90, 20);
            this.slTB_EmpGroup_Answer.SkipValidation = false;
            this.slTB_EmpGroup_Answer.TabIndex = 10;
            this.slTB_EmpGroup_Answer.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTB_Pr_P_No
            // 
            this.slTB_Pr_P_No.AllowSpace = true;
            this.slTB_Pr_P_No.AssociatedLookUpName = "";
            this.slTB_Pr_P_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_Pr_P_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_Pr_P_No.ContinuationTextBox = null;
            this.slTB_Pr_P_No.CustomEnabled = true;
            this.slTB_Pr_P_No.DataFieldMapping = "PR_P_NO";
            this.slTB_Pr_P_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_Pr_P_No.GetRecordsOnUpDownKeys = false;
            this.slTB_Pr_P_No.IsDate = false;
            this.slTB_Pr_P_No.Location = new System.Drawing.Point(303, 10);
            this.slTB_Pr_P_No.MaxLength = 6;
            this.slTB_Pr_P_No.Name = "slTB_Pr_P_No";
            this.slTB_Pr_P_No.NumberFormat = "###,###,##0.00";
            this.slTB_Pr_P_No.Postfix = "";
            this.slTB_Pr_P_No.Prefix = "";
            this.slTB_Pr_P_No.ReadOnly = true;
            this.slTB_Pr_P_No.Size = new System.Drawing.Size(90, 20);
            this.slTB_Pr_P_No.SkipValidation = false;
            this.slTB_Pr_P_No.TabIndex = 0;
            this.slTB_Pr_P_No.TabStop = false;
            this.slTB_Pr_P_No.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(78, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Processing Employ. No. :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(164, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 13);
            this.label9.TabIndex = 85;
            this.label9.Text = "PROCESS    COMPLETED";
            // 
            // groupBoxProcessCompleted
            // 
            this.groupBoxProcessCompleted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupBoxProcessCompleted.ConcurrentPanels = null;
            this.groupBoxProcessCompleted.Controls.Add(this.slTB_FinalAnswer);
            this.groupBoxProcessCompleted.Controls.Add(this.label10);
            this.groupBoxProcessCompleted.Controls.Add(this.label9);
            this.groupBoxProcessCompleted.DataManager = null;
            this.groupBoxProcessCompleted.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.groupBoxProcessCompleted.DependentPanels = null;
            this.groupBoxProcessCompleted.DisableDependentLoad = false;
            this.groupBoxProcessCompleted.EnableDelete = true;
            this.groupBoxProcessCompleted.EnableInsert = true;
            this.groupBoxProcessCompleted.EnableQuery = false;
            this.groupBoxProcessCompleted.EnableUpdate = true;
            this.groupBoxProcessCompleted.EntityName = null;
            this.groupBoxProcessCompleted.Location = new System.Drawing.Point(116, 575);
            this.groupBoxProcessCompleted.MasterPanel = null;
            this.groupBoxProcessCompleted.Name = "groupBoxProcessCompleted";
            this.groupBoxProcessCompleted.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.groupBoxProcessCompleted.Size = new System.Drawing.Size(507, 73);
            this.groupBoxProcessCompleted.SPName = null;
            this.groupBoxProcessCompleted.TabIndex = 84;
            this.groupBoxProcessCompleted.TabStop = true;
            this.groupBoxProcessCompleted.Visible = false;
            // 
            // slTB_FinalAnswer
            // 
            this.slTB_FinalAnswer.AllowSpace = true;
            this.slTB_FinalAnswer.AssociatedLookUpName = "";
            this.slTB_FinalAnswer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTB_FinalAnswer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTB_FinalAnswer.ContinuationTextBox = null;
            this.slTB_FinalAnswer.CustomEnabled = true;
            this.slTB_FinalAnswer.DataFieldMapping = "W_ANSWER";
            this.slTB_FinalAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTB_FinalAnswer.GetRecordsOnUpDownKeys = false;
            this.slTB_FinalAnswer.IsDate = false;
            this.slTB_FinalAnswer.Location = new System.Drawing.Point(318, 34);
            this.slTB_FinalAnswer.MaxLength = 6;
            this.slTB_FinalAnswer.Name = "slTB_FinalAnswer";
            this.slTB_FinalAnswer.NumberFormat = "###,###,##0.00";
            this.slTB_FinalAnswer.Postfix = "";
            this.slTB_FinalAnswer.Prefix = "";
            this.slTB_FinalAnswer.Size = new System.Drawing.Size(90, 20);
            this.slTB_FinalAnswer.SkipValidation = false;
            this.slTB_FinalAnswer.TabIndex = 0;
            this.slTB_FinalAnswer.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(69, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 39);
            this.label10.TabIndex = 9;
            this.label10.Text = "[YES] Exit with Save\n \n[NO] Exit without Save ";
            // 
            // CHRIS_Setup_TestPayrollProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 635);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBoxMainProcess);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.groupBoxProcessCompleted);
            this.Controls.Add(this.groupBoxProcessRunning);
            this.Controls.Add(this.pnlPersonnal);
            this.Controls.Add(this.lblUserName);
            this.Name = "CHRIS_Setup_TestPayrollProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCORE CHRIS - Test Payroll Process";
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.pnlPersonnal, 0);
            this.Controls.SetChildIndex(this.groupBoxProcessRunning, 0);
            this.Controls.SetChildIndex(this.groupBoxProcessCompleted, 0);
            this.Controls.SetChildIndex(this.lblHeader, 0);
            this.Controls.SetChildIndex(this.groupBoxMainProcess, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlPersonnal.ResumeLayout(false);
            this.pnlPersonnal.PerformLayout();
            this.groupBoxMainProcess.ResumeLayout(false);
            this.groupBoxMainProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSFL)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBoxProcessRunning.ResumeLayout(false);
            this.groupBoxProcessRunning.PerformLayout();
            this.groupBoxProcessCompleted.ResumeLayout(false);
            this.groupBoxProcessCompleted.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonnal;
        private System.Windows.Forms.Button btnLog;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label lblPrNum;
        private System.Windows.Forms.Label lblHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple groupBoxMainProcess;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvSFL;
        private System.Windows.Forms.Label lblEducation;
        private CrplControlLibrary.SLTextBox txtEducation;
        private System.Windows.Forms.Label lblFunctional;
        private System.Windows.Forms.Label lblBranch;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Label lblRVP;
        private CrplControlLibrary.SLTextBox txtGenStatus;
        private CrplControlLibrary.SLTextBox txtNo;
        private CrplControlLibrary.SLTextBox txtSubsLoanTAmnt;
        private CrplControlLibrary.SLTextBox txtTotalTaxAmnt;
        private CrplControlLibrary.SLTextBox txtRVP;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtProcName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker dtpProcDate;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple groupBoxProcessRunning;
        private CrplControlLibrary.SLTextBox slTB_EmpGroup_Answer;
        private CrplControlLibrary.SLTextBox slTB_Pr_P_No;
        private System.Windows.Forms.Label label8;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple groupBoxProcessCompleted;
        private CrplControlLibrary.SLTextBox slTB_FinalAnswer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOG_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOG_FLD;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOG_ENTRY;
    }
}