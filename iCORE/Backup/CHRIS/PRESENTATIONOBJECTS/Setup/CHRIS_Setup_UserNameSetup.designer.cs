
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_UserNameSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanelSimpleUser = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.slTextBoxUserId = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBoxUserName = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimpleUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanelSimpleUser
            // 
            this.slPanelSimpleUser.ConcurrentPanels = null;
            this.slPanelSimpleUser.Controls.Add(this.label2);
            this.slPanelSimpleUser.Controls.Add(this.label1);
            this.slPanelSimpleUser.Controls.Add(this.slTextBoxUserId);
            this.slPanelSimpleUser.Controls.Add(this.slTextBoxUserName);
            this.slPanelSimpleUser.DataManager = "iCORE.CHRIS.DATAOBJECTS.CmnDataManager";
            this.slPanelSimpleUser.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimpleUser.DependentPanels = null;
            this.slPanelSimpleUser.DisableDependentLoad = false;
            this.slPanelSimpleUser.EnableDelete = true;
            this.slPanelSimpleUser.EnableInsert = true;
            this.slPanelSimpleUser.EnableUpdate = true;
            this.slPanelSimpleUser.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.UserNameSetupCommand";
            this.slPanelSimpleUser.Location = new System.Drawing.Point(3, 82);
            this.slPanelSimpleUser.MasterPanel = null;
            this.slPanelSimpleUser.Name = "slPanelSimpleUser";
            this.slPanelSimpleUser.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimpleUser.Size = new System.Drawing.Size(365, 164);
            this.slPanelSimpleUser.SPName = "CHRIS_Setup_Sp_CHRIS_USERS_MANAGER";
            this.slPanelSimpleUser.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(49, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "User Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(49, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "User ID";
            // 
            // slTextBoxUserId
            // 
            this.slTextBoxUserId.AllowSpace = true;
            this.slTextBoxUserId.AssociatedLookUpName = "";
            this.slTextBoxUserId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBoxUserId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBoxUserId.ContinuationTextBox = null;
            this.slTextBoxUserId.CustomEnabled = true;
            this.slTextBoxUserId.DataFieldMapping = "SOEID";
            this.slTextBoxUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBoxUserId.GetRecordsOnUpDownKeys = false;
            this.slTextBoxUserId.IsDate = false;
            this.slTextBoxUserId.IsRequired = true;
            this.slTextBoxUserId.Location = new System.Drawing.Point(143, 35);
            this.slTextBoxUserId.MaxLength = 50;
            this.slTextBoxUserId.Name = "slTextBoxUserId";
            this.slTextBoxUserId.NumberFormat = "###,###,##0.00";
            this.slTextBoxUserId.Postfix = "";
            this.slTextBoxUserId.Prefix = "";
            this.slTextBoxUserId.Size = new System.Drawing.Size(130, 20);
            this.slTextBoxUserId.SkipValidation = false;
            this.slTextBoxUserId.TabIndex = 0;
            this.slTextBoxUserId.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.slTextBoxUserId, "Enter User ID here");
            // 
            // slTextBoxUserName
            // 
            this.slTextBoxUserName.AllowSpace = true;
            this.slTextBoxUserName.AssociatedLookUpName = "";
            this.slTextBoxUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBoxUserName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBoxUserName.ContinuationTextBox = null;
            this.slTextBoxUserName.CustomEnabled = true;
            this.slTextBoxUserName.DataFieldMapping = "USER_NAME";
            this.slTextBoxUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBoxUserName.GetRecordsOnUpDownKeys = false;
            this.slTextBoxUserName.IsDate = false;
            this.slTextBoxUserName.IsRequired = true;
            this.slTextBoxUserName.Location = new System.Drawing.Point(143, 80);
            this.slTextBoxUserName.MaxLength = 50;
            this.slTextBoxUserName.Name = "slTextBoxUserName";
            this.slTextBoxUserName.NumberFormat = "###,###,##0.00";
            this.slTextBoxUserName.Postfix = "";
            this.slTextBoxUserName.Prefix = "";
            this.slTextBoxUserName.Size = new System.Drawing.Size(130, 20);
            this.slTextBoxUserName.SkipValidation = false;
            this.slTextBoxUserName.TabIndex = 1;
            this.slTextBoxUserName.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.slTextBoxUserName, "Enter User Name  here");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(120, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "USER NAME SETUP";
            // 
            // CHRIS_Setup_UserNameSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 246);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.slPanelSimpleUser);
            this.CurrentPanelBlock = "slPanelSimpleUser";
            this.Name = "CHRIS_Setup_UserNameSetup";
            this.Text = "CHRIS - User Name Setup";
            this.Controls.SetChildIndex(this.slPanelSimpleUser, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimpleUser.ResumeLayout(false);
            this.slPanelSimpleUser.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimpleUser;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox slTextBoxUserId;
        private CrplControlLibrary.SLTextBox slTextBoxUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
