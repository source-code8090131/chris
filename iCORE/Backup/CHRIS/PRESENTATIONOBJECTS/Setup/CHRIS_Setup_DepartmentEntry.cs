using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_DepartmentEntry : ChrisTabularForm
    {
        //SP_DEPT,SP_DATE_TO_D,SP_DATE_FROM_D,SP_DEPT_HEAD,SP_DESC_D1
        #region Declarations

        /*------For custom call in database------- */
        CmnDataManager objCmnDataManager;

        /*------To store input parameters of StoredProcedure------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
        Result rslt = new Result();


        #endregion

        #region Constructor
        public CHRIS_Setup_DepartmentEntry()
        {
            InitializeComponent();

        }

        public CHRIS_Setup_DepartmentEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            DGVGroup.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);

        }
        #endregion

        #region Methods

        //protected override void OnLoad(EventArgs e)
        //{
        //    base.OnLoad(e);
        //    this.txtOption.Visible = false;
        //    this.txtUser.Text = this.userID;
        //    this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
        //    txtUserName.Text += "   " + this.UserName;
        //    ((DataGridViewLOVColumn)this.DGVGroup.Columns["SP_DEPT"]).SkipValidationOnLeave = true;
        //}
        protected override bool Quit()
        {
            if (DGVGroup.Rows.Count > 1)
            {
                if (this.DGVGroup.BeginEdit(true))
                {
                    this.DGVGroup.BeginEdit(false);
                }
                
                this.Cancel();
                
            }
            else
            {
                base.Quit();
            }
            return false;
        }
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                this.txtOption.Visible = false;
                this.txtUser.Text = this.userID;
                this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                txtUserName.Text += "   " + this.UserName;
                ((DataGridViewLOVColumn)this.DGVGroup.Columns["SP_DEPT"]).SkipValidationOnLeave = true;

                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                base.LogException(this.Name, "CommonOnLoadMethods", ex);
            }
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                DataTable dt = (DataTable)DGVGroup.DataSource;
                if (dt != null && Convert.ToString(dt.Rows[0]["SP_DATE_FROM_D"]) != string.Empty && Convert.ToString(dt.Rows[0]["SP_DATE_TO_D"]) != string.Empty)
                {

                    if (DateTime.Compare(Convert.ToDateTime(dt.Rows[0]["SP_DATE_FROM_D"]), Convert.ToDateTime(dt.Rows[0]["SP_DATE_TO_D"])) > 0)
                    {
                        MessageBox.Show(" 'To' Date Must be Greater than 'From' Date");
                        tbtSave.Enabled = false;
                        DGVGroup.Focus();
                        return;
                    }


                    if (Convert.ToString(dt.Rows[0]["SP_DESC_D1"]) != "" && Convert.ToString(dt.Rows[0]["SP_DEPT_HEAD"]) != "" )
                    {

                        dicInputParameters.Clear();
                        dicInputParameters.Add("SP_DESC_D1", dt.Rows[0]["SP_DESC_D1"]);
                        dicInputParameters.Add("SP_DEPT_HEAD", dt.Rows[0]["SP_DEPT_HEAD"]);
                       
                        //VALIDATING DUPLICATING RECORDS//
                        rslt = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "GetByParam", dicInputParameters);
                        if (rslt.isSuccessful)
                        {
                            if (Convert.ToDecimal(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()) >= 1)
                            {
                                MessageBox.Show("Record Already Exists", "Note");
                                DGVGroup.ClearSelection();
                                return;
                            }

                        }

                      }

                        base.AllGridsEndEdit(this);
                        DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                        DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                        if (dtAdded != null || dtUpdated != null)
                        {
                            DialogResult dr = MessageBox.Show("Do You Want to Save Changes.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr == DialogResult.Yes)
                            {
                                base.DoToolbarActions(this.pnlGroup.Controls, "Save");
                                //bool isSaved = DGVGroup.SaveGrid(this.pnlGroup);
                                //if (isSaved)
                                //{
                                //    MessageBox.Show("Changes Saved Successfully.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //    dt.AcceptChanges();
                                //    this.tbtSave.Enabled = false;
                                //}
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                  }
            }

          

            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Search")
            {
                for (int i = 0; i < DGVGroup.Rows.Count; i++)
                {
                    DGVGroup.Rows[i].Cells["SP_SEGMENT_D"].ReadOnly = true;
                    DGVGroup.Rows[i].Cells["SP_GROUP_D"].ReadOnly = true;
                    DGVGroup.Rows[i].Cells["SP_DEPT"].ReadOnly = true;
                    DGVGroup.Rows[i].Cells["SP_DESC_D1"].ReadOnly = true;
                    DGVGroup.Rows[i].Cells["SP_DESC_D2"].ReadOnly = true;
                }
            }
            if (actionType == "Cancel")
            {
                DGVGroup.Columns["SP_SEGMENT_D"].ReadOnly = false;
                DGVGroup.Columns["SP_GROUP_D"].ReadOnly = false;
                DGVGroup.Columns["SP_DEPT"].ReadOnly = false;
                DGVGroup.Columns["SP_DESC_D1"].ReadOnly = false;
                DGVGroup.Columns["SP_DESC_D2"].ReadOnly = false;
            }
        }


        #endregion

        #region Events
        private void DGVGroup_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }
        private void DGVGroup_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (tbtCancel.Pressed || tbtClose.Pressed)
            {
                DGVGroup.CancelEdit();
                DGVGroup.EndEdit();
                return;
            }
            if (e.ColumnIndex == 2 /*&& this.DGVGroup.CurrentCell.IsInEditMode*/)
            {
                #region If record already exists against segment,group and dept then fill grid
                dicInputParameters.Clear();
                dicInputParameters.Add("SP_SEGMENT_D", DGVGroup.Rows[e.RowIndex].Cells["SP_SEGMENT_D"].EditedFormattedValue);
                dicInputParameters.Add("SP_GROUP_D", DGVGroup.Rows[e.RowIndex].Cells["SP_GROUP_D"].EditedFormattedValue);
                dicInputParameters.Add("SP_DEPT", DGVGroup.Rows[e.RowIndex].Cells["SP_DEPT"].EditedFormattedValue);

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "CheckRecordAlreadyExist", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                if ((rslt.dstResult.Tables[0].Rows[0]["SP_DEPT"] != DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[0]["SP_DEPT"].ToString())))
                                {
                                    DGVGroup.Rows[e.RowIndex].Cells["SP_DEPT"].Value = rslt.dstResult.Tables[0].Rows[0]["SP_DEPT"];
                                }
                                if ((rslt.dstResult.Tables[0].Rows[0]["SP_DESC_D1"] != DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[0]["SP_DESC_D1"].ToString())))
                                {
                                    DGVGroup.Rows[e.RowIndex].Cells["SP_DESC_D1"].Value = rslt.dstResult.Tables[0].Rows[0]["SP_DESC_D1"];
                                }
                                if ((rslt.dstResult.Tables[0].Rows[0]["SP_DESC_D2"] != DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[0]["SP_DESC_D2"].ToString())))
                                {
                                    DGVGroup.Rows[e.RowIndex].Cells["SP_DESC_D2"].Value = rslt.dstResult.Tables[0].Rows[0]["SP_DESC_D2"];
                                }
                                if ((rslt.dstResult.Tables[0].Rows[0]["SP_DEPT_HEAD"] != DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[0]["SP_DEPT_HEAD"].ToString())))
                                {
                                    DGVGroup.Rows[e.RowIndex].Cells["SP_DEPT_HEAD"].Value = rslt.dstResult.Tables[0].Rows[0]["SP_DEPT_HEAD"];
                                }
                                if ((rslt.dstResult.Tables[0].Rows[0]["SP_DATE_FROM_D"] != DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[0]["SP_DATE_FROM_D"].ToString())))
                                {
                                    DGVGroup.Rows[e.RowIndex].Cells["SP_DATE_FROM_D"].Value = rslt.dstResult.Tables[0].Rows[0]["SP_DATE_FROM_D"];
                                }
                                if ((rslt.dstResult.Tables[0].Rows[0]["SP_DATE_TO_D"] != DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[0]["SP_DATE_TO_D"].ToString())))
                                {
                                    DGVGroup.Rows[e.RowIndex].Cells["SP_DATE_TO_D"].Value = rslt.dstResult.Tables[0].Rows[0]["SP_DATE_TO_D"];
                                }
                                if (DGVGroup.Rows[e.RowIndex].Cells["ID"].Value == DBNull.Value)
                                {
                                    MessageBox.Show("Record Already Entered", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    //e.Cancel = true;
                                    //***********JUGAR(LOGIC)***********
                                    if (!this.DGVGroup.CurrentCell.IsInEditMode)
                                        this.DGVGroup.BeginEdit(true);
                                    return;
                                    ////return;
                                }


                                //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand)this.pnlGroup.CurrentBusinessEntity;
                                //if(ent !=null)
                                //{
                                //    ent.ID = int.Parse(rslt.dstResult.Tables[0].Rows[0]["ID"].ToString());
                                //}
                            }

                        }
                    }
                }
                #endregion
            }
            if (e.ColumnIndex == 7)
            {

                if (DGVGroup.Rows[e.RowIndex].Cells["SP_DATE_TO_D"].EditedFormattedValue.ToString() != "" && DGVGroup.Rows[e.RowIndex].Cells["SP_DATE_FROM_D"].EditedFormattedValue.ToString() != "")
                {
                    if (DateTime.Parse(DGVGroup.Rows[e.RowIndex].Cells["SP_DATE_TO_D"].EditedFormattedValue.ToString()) < DateTime.Parse(DGVGroup.Rows[e.RowIndex].Cells["SP_DATE_FROM_D"].EditedFormattedValue.ToString()))
                    {
                        MessageBox.Show(" 'To' Date Must be Greater than 'From' Date");
                        tbtSave.Enabled = false;
                        DGVGroup.Focus();
                    }
                }
                //DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                //                                , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (dRes == DialogResult.Yes)
                //{                   
                //    base.DoToolbarActions(this.Controls, "Save");
                //    base.DoToolbarActions(this.Controls, "Cancel");            
                //    return;
                //}
                //else if (dRes == DialogResult.No)
                //{
                //    base.DoToolbarActions(this.Controls, "Cancel");                      
                //    return;
                //}
            }
        }
        #endregion

    }
}