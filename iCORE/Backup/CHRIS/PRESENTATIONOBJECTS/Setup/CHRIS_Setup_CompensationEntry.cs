using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_CompensationEntry : ChrisTabularForm
    {

        #region --Global Variable--
        string LvlCode = string.Empty;
        CmnDataManager cmnDM = new CmnDataManager();
        iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_CompensationEntry_GroupHead frm_GrpHead;
        bool IsValidated = true;
        double MinSal = 0;
        double MaxSal = 0;
        double MidSal = 0;
        double temp = 0;
        #endregion
        
        #region --Constructor--

        public CHRIS_Setup_CompensationEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_CompensationEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            
        }

        #endregion

        #region --Method--

        protected override void OnLoad(EventArgs e)
        {
            this.CurrentPanelBlock = pnlTblCompensationInfo.Name;

            base.OnLoad(e);
            dgvComp.ColumnHeadersDefaultCellStyle.Font.Bold.ToString();
            dgvComp.SkippingColumns = "Mid_Sal,D_lvl,D_GrpHead";

            Font newFontStyle = new Font(dgvComp.Font, FontStyle.Bold);
            dgvComp.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
        }

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                //this.tbtAdd.Visible = false;
                //this.tbtList.Visible = false;
                //this.tbtSave.Visible = false;
                //this.tbtCancel.Visible = false;
                //this.tbtDelete.Visible = false;

                //this.tlbMain.Visible    = false;
                txtOption.Visible = false;
                this.lblUserName.Text = this.UserName;


            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        #endregion

        #region --Events--

        /// <summary>
        /// Cell VAlidation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvComp_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (!dgvComp.CurrentCell.IsInEditMode )
                    return;

                if (dgvComp.Rows.Count > 1)
                {
                    dgvComp.ResetError();

                    #region --Lvl_Code--
                    if (dgvComp.CurrentCell.OwningColumn.Name == "Lvl_Code" && dgvComp.CurrentRow.Cells["Lvl_Code"].IsInEditMode && dgvComp.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                    {
                        LvlCode = dgvComp.CurrentCell.EditedFormattedValue.ToString();
                
                        if(dgvComp.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                            dgvComp.CurrentRow.Cells["D_lvl"].Value = dgvComp.CurrentCell.EditedFormattedValue.ToString().Substring(0, 1);
                    }
                    #endregion

                    #region --Grp_Head--
                    else if (dgvComp.CurrentCell.OwningColumn.Name == "Grp_Head" && dgvComp.CurrentRow.Cells["Grp_Head"].IsInEditMode && dgvComp.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                    {
                        if (dgvComp.CurrentCell.EditedFormattedValue.ToString() == "True")
                        {
                            dgvComp.CurrentRow.Cells["D_GrpHead"].Value = "Y";
                        }
                        else if ( (dgvComp.CurrentCell.EditedFormattedValue.ToString() == "False") && (dgvComp.CurrentRow.Cells[0].FormattedValue.ToString()=="" )  )
                        {
                            dgvComp.CurrentRow.Cells["D_GrpHead"].Value = "";
                            e.Cancel = true;
                            //IsValidated = true;
                            return;

                        }
                        else
                        {
                            dgvComp.CurrentRow.Cells["D_GrpHead"].Value = "N";
                        }
                    }
                    #endregion

                    #region --Min_Sal--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Min_Sal") && dgvComp.CurrentRow.Cells["Min_Sal"].IsInEditMode )//&& dgvComp.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out MinSal))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }

                        if (dgvComp.CurrentCell.Value != null)
                            try
                            {
                                MinSal = double.Parse(dgvComp.CurrentCell.EditedFormattedValue.ToString());
                            }
                            catch (FormatException ex) { e.Cancel = true; return; }


                        if (dgvComp.CurrentRow.Cells["Max_Sal"].Value != null && dgvComp.CurrentRow.Cells["Max_Sal"].Value.ToString() != string.Empty)
                            MaxSal = double.Parse(dgvComp.CurrentRow.Cells["Max_Sal"].Value.ToString());

                        if (double.Parse(dgvComp.CurrentCell.EditedFormattedValue.ToString()) <= 0)
                        {
                            MessageBox.Show("Value Must Be Greater than 0", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                        //MidSal = ((MinSal + MaxSal) / 2);
                        //dgvComp.CurrentRow.Cells["Mid_Sal"].Value = MidSal;
                        //MinSal = 0;
                        //MaxSal = 0;
                        //MidSal = 0;
                    }
                    #endregion

                    #region --Max_Sal--
                    else    if (dgvComp.CurrentCell.OwningColumn.Name == "Max_Sal" && dgvComp.CurrentRow.Cells["Max_Sal"].IsInEditMode && dgvComp.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out MinSal))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }

                        if (dgvComp.CurrentRow.Cells["Min_Sal"].FormattedValue != "")
                            MinSal = double.Parse(dgvComp.CurrentRow.Cells["Min_Sal"].Value.ToString());

                        if (dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                            MaxSal = double.Parse(dgvComp.CurrentCell.EditedFormattedValue.ToString());


                        if (double.Parse(dgvComp.CurrentCell.EditedFormattedValue.ToString()) <= 0)
                        {
                            MessageBox.Show("Value Must Be Greater than 0", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                        if (MaxSal < MinSal)
                        {
                            MessageBox.Show("Minimum Salary cannot be Greater than Maximum Salary", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }

                        //dgvComp.CurrentRow.Cells["Mid_Sal"].Value = ((MinSal + MaxSal) / 2);
                        //MinSal = 0;
                        //MaxSal = 0;
                    }
                    #endregion

                    #region --Budget_Price--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Budget_Price") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --Annual_Dep--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Annual_Dep") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --Petrol_Ltr--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Petrol_Ltr") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --Petrol_Rate--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Petrol_Rate") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --Insurance_Rate--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Insurance_Rate") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --Maintenance_Rate--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Maintenance_Rate") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --Driver_Cost--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Driver_Cost") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion

                    #region --GroupHead_Allowance--
                    else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("GroupHead_Allowance") && dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        if (!double.TryParse(dgvComp.CurrentCell.EditedFormattedValue.ToString(), out temp))
                        {
                            e.Cancel = true;
                            IsValidated = false;
                            return;
                        }
                        else
                        {
                            IsValidated = true;
                        }
                    }
                    #endregion
                }
            }
            catch(Exception exp)
            {
                LogException(this.Name, "dgvComp_CellValidating", exp);
            }
        }

        /// <summary>
        /// After Cell Validated: than Fill the MidAmt Cell.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvComp_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (!dgvComp.CurrentCell.IsInEditMode)
                return;

            if (dgvComp.Rows.Count > 1)
            {
                if(dgvComp.CurrentCell.OwningColumn.Name.Equals("Min_Sal"))
                {
                    if (dgvComp.CurrentRow.Cells["Max_Sal"].Value != null && dgvComp.CurrentRow.Cells["Max_Sal"].Value.ToString() != string.Empty)
                        MaxSal = double.Parse(dgvComp.CurrentRow.Cells["Max_Sal"].Value.ToString());

                    MidSal = ((MinSal + MaxSal) / 2);
                    dgvComp.CurrentRow.Cells["Mid_Sal"].Value = MidSal;
                    MinSal = 0;
                    MaxSal = 0;
                    MidSal = 0;
                }
                else if (dgvComp.CurrentCell.OwningColumn.Name.Equals("Max_Sal"))
                {
                    if (dgvComp.CurrentRow.Cells["Min_Sal"].FormattedValue != "")
                        MinSal = double.Parse(dgvComp.CurrentRow.Cells["Min_Sal"].Value.ToString());

                    if (dgvComp.CurrentCell.EditedFormattedValue.ToString() != "")
                        MaxSal = double.Parse(dgvComp.CurrentCell.EditedFormattedValue.ToString());

                    dgvComp.CurrentRow.Cells["Mid_Sal"].Value = ((MinSal + MaxSal) / 2);
                    MinSal = 0;
                    MaxSal = 0;
                }
            }
        }

        /// <summary>
        /// Fill FRid with Value (Call the LIST)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            Result rsltCode;
            rsltCode = cmnDM.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "List", param);
            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
            {
                dgvComp.GridSource = rsltCode.dstResult.Tables[0];
                dgvComp.DataSource = dgvComp.GridSource;
            }
        }

        /// <summary>
        /// Close Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
           
              DataTable dt = (DataTable)dgvComp.DataSource;
              if (dt != null)
              {

                  DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                  DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);


                  if (dtAdded != null || dtUpdated != null)
                  {

                      DialogResult dr = MessageBox.Show("Do You Want to Save the Changes You Have Made?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                      if (dr == DialogResult.Yes )
                      {
                           base.Save();
                      }
                  }
                
              }
              base.Quit();

        }

        /// <summary>
        /// Save Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValidated)
            {
                base.Save();
            }
            else
                return;
            
        }
     
        /// <summary>
        /// Move to First Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScroll_Up_Click(object sender, EventArgs e)
        {
            if (dgvComp.Rows.Count > 1)
            {
                dgvComp.Rows[0].Selected = true;
                dgvComp.FirstDisplayedScrollingRowIndex = dgvComp.SelectedCells[0].RowIndex;
            }
        }

        /// <summary>
        /// Move to PRevoius Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUp_Click(object sender, EventArgs e)
        {
            if (dgvComp.Rows.Count > 1)
            {
                int CurrentRow;
                CurrentRow = dgvComp.SelectedCells[0].RowIndex;
                if (CurrentRow > 0)
                {
                    dgvComp.Rows[CurrentRow - 1].Selected = true;
                    dgvComp.FirstDisplayedScrollingRowIndex = dgvComp.SelectedCells[0].RowIndex;
                }
            }
        }

        /// <summary>
        /// Move to Next Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (dgvComp.Rows.Count > 1)
            {
                int CurrentRow;
                //CurrentRow = dgvComp.SelectedRows[0].Index;
                CurrentRow = dgvComp.SelectedCells[0].RowIndex;
                if (CurrentRow < (dgvComp.RowCount - 1))
                {
                    dgvComp.Rows[CurrentRow + 1].Selected = true;
                    dgvComp.FirstDisplayedScrollingRowIndex = dgvComp.SelectedCells[0].RowIndex;
                }
            }
        }

        /// <summary>
        /// Move to Last Row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScroll_Down_Click(object sender, EventArgs e)
        {
            if (dgvComp.Rows.Count > 1)
            {
                int CurrentRow;
                CurrentRow = dgvComp.Rows.Count;
                dgvComp.Rows[CurrentRow - 1].Selected = true;
                dgvComp.FirstDisplayedScrollingRowIndex = dgvComp.SelectedCells[0].RowIndex;
            }
        }

        /// <summary>
        /// Delete the Selected ROw
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidated)
                {
                    if (dgvComp.Rows.Count > 1 && dgvComp.SelectedRows.Count > 0)
                    {
                        base.DoToolbarActions(pnlTblCompensationInfo.Controls, "Delete");
                        base.Delete();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "btnDel_Click", exp);
            }
        }

        /// <summary>
        /// Open New POPUp Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGrp_Click(object sender, EventArgs e)
        {
            try
            {
                frm_GrpHead = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_CompensationEntry_GroupHead(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
                frm_GrpHead.MdiParent = null;

                //this.Hide();
                frm_GrpHead.ShowDialog();
                if (frm_GrpHead.PopUPFormOpen == true)
                {
                    base.Quit();
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "btnGrp_Click", exp);
            }
        
        }

        /// <summary>
        /// Override the F6 functionality.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F6 && this.FunctionConfig.EnableF6)
            {
                return;
            }
            else if (e.KeyCode == Keys.F8 && this.FunctionConfig.EnableF8)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                rsltCode = cmnDM.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "List", param);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dgvComp.GridSource = rsltCode.dstResult.Tables[0];
                    dgvComp.DataSource = dgvComp.GridSource;
                }
            }
        }

        #endregion

        

    }
}