using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_DeductionEntry : ChrisMasterDetailForm
    {

        #region --Global Variable--
        public string g_ded_code = string.Empty;
        public string g_desg_d = string.Empty;
        public string g_level_d = string.Empty;
        public string g_branch_d = string.Empty;
        public string g_option = string.Empty;
        bool returnValue = false;

        iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DedDetailEntry frmDedDetail;

        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage2;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage3;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage4;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage6;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage7;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage8;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage9;

        iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DeductionEntry_MasterDetail frmDedMD;

        TextBox tbxPopUp = new TextBox();
        bool NoDetails = false;
        string lblViewPopUP = "Do You Want To View More Record [Y]es [N]o";
        string lblDeletePopUP = "Do You Want To Delete The Record [Y]es [N]o";
        string lblAddPopUP = "Do You Want To Save The Record [Y]es [N]o";
        string lblPrevPagePopUP = "Do You Want To Go To Previous Page [Y]es [N]o";
        string lblSeeDetailPopUP = "Do You Want To See The Details [Y]es [N]o";
        string lblModifyDetailPopUP = "Do You Want To Modify The Details [Y]es [N]o";
        string lblDeleteDetailPopUP = "Do You Want To Delete The Details [Y]es [N]o";

        public string ViewRecpp = string.Empty;
        public string DeleteRecpp = string.Empty;
        public string SaveRecpp = string.Empty;
        public string PrevPgpp = string.Empty;
        public string SeeDtlpp = string.Empty;
        public string ModifyDtlpp = string.Empty;
        public string DeleteDtlpp = string.Empty;


        string txtValue = string.Empty;

        #endregion

        #region --Constructor--
        public CHRIS_Setup_DeductionEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_DeductionEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(this.pnlTblDedDetail);
            lstDependentPanels.Add(this.pnlDedDetail);
            this.pnlDeductionMain.DependentPanels = lstDependentPanels;
            tbtDelete.Visible = true;
            tbtDelete.Enabled = true;
            this.IndependentPanels.Add(pnlDeductionMain);
            this.FunctionConfig.EnableF8 = true;
            this.FunctionConfig.F8 = Function.Query;
        }
        #endregion

        #region --Method--
        /// <summary>
        /// Skip Validation on ADD Mode
        /// </summary>
        /// <returns></returns>
        protected override bool Add()
        {
            this.lbtnDedection.SkipValidationOnLeave = true;
            this.txt_SP_DED_CODE.AssociatedLookUpName = "asd";
            lbtnDedection.Enabled = false;
            base.Add();
            //return base.Add();
            return false;
        }

        protected override bool Delete()
        {
            base.Delete();
            lbtnDedection.Enabled = true;
            this.lbtnDedection.SkipValidationOnLeave = false;
            this.txt_SP_DED_CODE.AssociatedLookUpName = "lbtnDedection";
            lbtnDedection.ActionType = "DUC_LOV";
            lbtnDedection.ActionLOVExists = "DUC_LOV_EXISTS";
            //return base.Delete();
            return false;
        }

        protected override bool View()
        {
            base.View();
            this.txt_SP_DED_CODE.AssociatedLookUpName = "lbtnDedection";
           lbtnDedection.Enabled = true;
            txt_SP_DED_CODE.ShortcutsEnabled = true;
            this.lbtnDedection.SkipValidationOnLeave = false;

            lbtnDedection.ActionType = "DUC_LOV";
            lbtnDedection.ActionLOVExists = "DUC_LOV_EXISTS";
            return base.View();

        }
        protected override bool Quit()
        {

            if (this.FunctionConfig.CurrentOption != Function.None)
            {
                this.DoToolbarActions(this.Controls, "Cancel");
                return false;
            }
            else
            {

                base.Quit();
                return false;
            }



        }
        protected override bool Query()
        {
            base.Query();
            lbtnDedection.SkipValidationOnLeave = true;
            frmDedMD = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DeductionEntry_MasterDetail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
            frmDedMD.MdiParent = null;
            frmDedMD.ShowDialog();

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool proc_mod_Del()
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_BRANCH_D", txt_SP_BRANCH_D.Text);
                param.Add("SP_DESG_D", txt_SP_DESG_D.Text);
                param.Add("SP_LEVEL_D", txt_SP_LEVEL_D.Text);
                param.Add("SP_CATEGORY_D", txt_SP_CATEGORY_D.Text);



                rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "CATEGORY_VALIDATED", param);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txt_SP_DED_DESC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6] != DBNull.Value)
                        txt_SP_VALID_FROM_D.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7] != DBNull.Value)
                        txt_SP_VALID_TO_D.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString());
                    txt_SP_ALL_IND.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txt_SP_DED_AMOUNT.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txt_SP_DED_PER.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txt_SP_ACCOUNT_NO_D.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();

                    return true;
                }
                else if (rsltCode.dstResult.Tables[0].Rows.Count < 0)
                {
                    if ((txtOption.Text != "A" || txtOption.Text == "M") && txt_SP_DED_CODE.Text == string.Empty)
                    {
                        base.Cancel();
                        return false;
                    }
                    else if (txtOption.Text != "A")
                    {
                        MessageBox.Show(" Record Not Found... Press [Delete] Key", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                return false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "proc_mod_Del", exp);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool proc_view_add()
        {
            try
            {
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();

                if (txt_SP_BRANCH_D.Text != string.Empty)
                    param2.Add("SP_DED_CODE", txt_SP_DED_CODE.Text);

                if (txt_SP_BRANCH_D.Text != string.Empty)
                    param2.Add("SP_BRANCH_D", txt_SP_BRANCH_D.Text);

                if (txt_SP_DESG_D.Text != string.Empty)
                    param2.Add("SP_DESG_D", txt_SP_DESG_D.Text);

                if (txt_SP_LEVEL_D.Text != string.Empty)
                    param2.Add("SP_LEVEL_D", txt_SP_LEVEL_D.Text);

                if (txt_SP_CATEGORY_D.Text != string.Empty)
                    param2.Add("SP_CATEGORY_D", txt_SP_CATEGORY_D.Text);

                rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "CATEGORY_VALIDATED", param2);
                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txt_SP_DED_DESC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6] != DBNull.Value)
                        txt_SP_VALID_FROM_D.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7] != DBNull.Value)
                        txt_SP_VALID_TO_D.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[7].ToString());
                    txt_SP_ALL_IND.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[11].ToString();
                    txt_SP_DED_AMOUNT.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    txt_SP_DED_PER.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    txt_SP_ACCOUNT_NO_D.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[10].ToString();

                    return true;
                }
                else if (rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    if ((txtOption.Text != "A" || txtOption.Text == "V") && txt_SP_DED_CODE.Text == string.Empty)
                    {
                        base.Cancel();
                        return false;
                    }
                    else if (txtOption.Text == "V")
                    {
                        MessageBox.Show(" Record Not Found...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                return true;

            }
            catch (Exception exp)
            {
                LogException(this.Name, "proc_view_add", exp);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CallReportOld()
        {
            string FN1 = string.Empty;

            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            FN1 = "AUPR" + Now() + ".LIS";   //||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';
            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = "C:\\SPOOL\\CHRIS\\AUDIT\\" + FN1;      //":GLOBAL.AUDIT_PATH||FN1";
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDED_CODE = new CrplControlLibrary.SLTextBox(comp);
            txtDED_CODE.Name = "txtDED_CODE";
            txtDED_CODE.Text = txt_SP_DED_CODE.Text;
            pnl.Controls.Add(txtDED_CODE);

            CrplControlLibrary.SLTextBox txtDED_DESG = new CrplControlLibrary.SLTextBox(comp);
            txtDED_DESG.Name = "txtDED_DESG";
            txtDED_DESG.Text = txt_SP_DED_CODE.Text;
            pnl.Controls.Add(txtDED_CODE);

            CrplControlLibrary.SLTextBox txtDED_LEVEL = new CrplControlLibrary.SLTextBox(comp);
            txtDED_LEVEL.Name = "txtDED_LEVEL";
            txtDED_LEVEL.Text = txt_SP_LEVEL_D.Text;
            pnl.Controls.Add(txtDED_LEVEL);

            CrplControlLibrary.SLTextBox txtDED_BRANCH = new CrplControlLibrary.SLTextBox(comp);
            txtDED_BRANCH.Name = "txtDED_BRANCH";
            txtDED_BRANCH.Text = txt_SP_BRANCH_D.Text;
            pnl.Controls.Add(txtDED_BRANCH);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "txtuser";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "txtST";
            txtST.Text = " OLD";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString();
            pnl.Controls.Add(txtDT);



            frm.Controls.Add(pnl);
            frm.RptFileName = "audit19A";
            frm.Owner = this;
            frm.RunReport();
        }

        /// <summary>
        /// 
        /// </summary>
        public void CallReportNew()
        {
            string FN2 = string.Empty;

            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();

            FN2 = "AUPO" + Now() + ".LIS";   //||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';
            CrplControlLibrary.SLTextBox txtDESTYPE = new CrplControlLibrary.SLTextBox(comp);
            txtDESTYPE.Name = "txtDESTYPE";
            txtDESTYPE.Text = "FILE";
            pnl.Controls.Add(txtDESTYPE);

            CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            txtDESNAME.Name = "txtDESNAME";
            txtDESNAME.Text = "C:\\SPOOL\\CHRIS\\AUDIT\\" + FN2;      //":GLOBAL.AUDIT_PATH||FN1";
            pnl.Controls.Add(txtDESNAME);

            CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            txtMODE.Name = "txtMODE";
            txtMODE.Text = "CHARACTER";
            pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDED_CODE = new CrplControlLibrary.SLTextBox(comp);
            txtDED_CODE.Name = "txtDED_CODE";
            txtDED_CODE.Text = txt_SP_DED_CODE.Text;
            pnl.Controls.Add(txtDED_CODE);

            CrplControlLibrary.SLTextBox txtDED_DESG = new CrplControlLibrary.SLTextBox(comp);
            txtDED_DESG.Name = "txtDED_DESG";
            txtDED_DESG.Text = txt_SP_DED_CODE.Text;
            pnl.Controls.Add(txtDED_CODE);

            CrplControlLibrary.SLTextBox txtDED_LEVEL = new CrplControlLibrary.SLTextBox(comp);
            txtDED_LEVEL.Name = "txtDED_LEVEL";
            txtDED_LEVEL.Text = txt_SP_LEVEL_D.Text;
            pnl.Controls.Add(txtDED_LEVEL);

            CrplControlLibrary.SLTextBox txtDED_BRANCH = new CrplControlLibrary.SLTextBox(comp);
            txtDED_BRANCH.Name = "txtDED_BRANCH";
            txtDED_BRANCH.Text = txt_SP_BRANCH_D.Text;
            pnl.Controls.Add(txtDED_BRANCH);

            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "txtuser";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "txtST";
            txtST.Text = " NEW";
            pnl.Controls.Add(txtST);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "txtDT";
            txtDT.Text = Now().ToString();
            pnl.Controls.Add(txtDT);



            frm.Controls.Add(pnl);
            frm.RptFileName = "audit19A";
            frm.Owner = this;
            frm.RunReport();
        }

        public void ClearForm()
        {
            base.Cancel();
            txt_SP_VALID_FROM_D.Value = null;
            txt_SP_VALID_TO_D.Value = null;

        }

        #endregion

        #region --Events--

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                txtDate.Text = Now().ToString("dd/MM/yyyy");
                txtOption.Select();
                txtOption.Focus();
                txt_SP_VALID_FROM_D.Value = null;
                txt_SP_VALID_TO_D.Value = null;
                this.CurrentPanelBlock = this.pnlDeductionMain.Name;
                this.llbusername.Text = this.UserName;
                Font newFontStyle = new Font(dgvDedDetail.Font, FontStyle.Bold);
                dgvDedDetail.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
                this.tbtAdd.Available = false;
                this.tbtDelete.Available = false;
                this.tbtSave.Available = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }


        /// <summary>
        /// txt_SP_DED_CODE Textbox Leave Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_DED_CODE_Leave(object sender, EventArgs e)
        {
            if (txt_SP_DED_CODE.Text == null && txt_SP_DED_CODE.Text == "   ")
            {
                txtOption.Select();
                txtOption.Focus();
            }
        }

        /// <summary>
        /// txt_SP_DESG_D Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_DESG_D_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_SP_DESG_D.Text == string.Empty)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    if (txt_SP_DED_CODE.Text != string.Empty)
                        param.Add("SP_DED_CODE", txt_SP_DED_CODE.Text);

                    if (txt_SP_BRANCH_D.Text != string.Empty)
                        param.Add("SP_BRANCH_D", txt_SP_BRANCH_D.Text);

                    rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "DESIGNATION_FROM_FILL", param);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_SP_DED_DESC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        txt_SP_VALID_FROM_D.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                        txt_SP_VALID_TO_D.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                        txt_SP_ALL_IND.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                        txt_SP_DED_PER.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                        txt_SP_ACCOUNT_NO_D.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    }

                    if (txtOption.Text == "M" || txtOption.Text == "D")
                    {
                        returnValue = proc_mod_Del();
                    }
                    if (txtOption.Text == "V" || txtOption.Text == "A")
                    {
                        returnValue = proc_view_add();
                    }

                    if (txtOption.Text == "A" && txt_SP_DED_DESC.Text != string.Empty)
                    {
                        MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        base.Cancel();
                    }
                    else if (returnValue != false)
                    {
                        txt_SP_DED_DESC.Select();
                        txt_SP_DED_DESC.Focus();
                    }
                    else
                    {
                        e.Cancel = true;
                    }

                    if (txtOption.Text == "M")
                    {
                        txt_SP_DED_CODE.Select();
                        txt_SP_DED_CODE.Focus();
                    }

                    if (txtOption.Text == "V" && returnValue != false)
                    {
                        //go_field('blk_head.w_ans5');
                        GoToPage7();

                        //frmPage7                    = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblSeeDetailPopUP);
                        //frmPage7.Text               = string.Empty;
                        //frmPage7.TextBox.MaxLength  = 1;
                        //frmPage7.TextBox.Validating += new CancelEventHandler(Ans5Pg7TextBox_Validating);
                        //frmPage7.MdiParent          = null;
                        //frmPage7.ShowDialog();
                        //frmPage7.Text               = frmPage7.Value;

                        if (SeeDtlpp == "Yes")
                        {
                            txtOption.Text = "V";
                            txt_SP_DED_CODE.Select();
                            txt_SP_DED_CODE.Focus();
                        }
                        else if (SeeDtlpp == "No")
                        {
                            txtOption.Text = "V";
                            txtOption.Focus();
                            txtOption.Select();
                        }

                        if (ViewRecpp == "Yes")
                        {
                            txt_SP_DED_CODE.Select();
                            txt_SP_DED_CODE.Focus();
                            txtOption.Text = "V";
                        }
                        else if (ViewRecpp == "No")
                        {
                            txtOption.Select();
                            txtOption.Focus();
                        }

                    }

                    if (txtOption.Text == "D" && returnValue != false)
                    {
                        //go_field('blk_head.w_ans7');
                        GoToPage9();
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_DESG_D_Validating", exp);
            }
        }

        ///// <summary>
        ///// txt_SP_DESG_D_Validated
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void txt_SP_DESG_D_Validated(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (txt_SP_DESG_D.Text != string.Empty)
        //        {

        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        LogException(this.Name, "txt_SP_DESG_D_Validated", exp);
        //    }
        //}

        /// <summary>
        /// txt_SP_CATEGORY_D Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CATEGORY_D_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                //if (txt_SP_CATEGORY_D.Text != string.Empty)
                if(txt_SP_DESG_D.Text != string.Empty)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    param.Add("SP_CATEGORY", txt_SP_CATEGORY_D.Text);
                    param.Add("SP_DESG", txt_SP_DESG_D.Text);
                    param.Add("SP_LEVEL", txt_SP_LEVEL_D.Text);

                    rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "CATEGORY_VALIDATING", param);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count < 0)
                    {
                        e.Cancel = true;
                        return;
                    }

                    Dictionary<string, object> param1 = new Dictionary<string, object>();
                    Result rsltCode1;
                    param1.Add("SP_CATEGORY_D", txt_SP_CATEGORY_D.Text);
                    param1.Add("SP_DESG_D", txt_SP_DESG_D.Text);
                    param1.Add("SP_LEVEL_D", txt_SP_LEVEL_D.Text);
                    param1.Add("SP_DED_CODE", txt_SP_DED_CODE.Text);
                    param1.Add("SP_BRANCH_D", txt_SP_BRANCH_D.Text);

                    rsltCode1 = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "CATEGORY_FROM_FILL", param1);
                    if (rsltCode1.isSuccessful && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_SP_DED_DESC.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        if (rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[1] != DBNull.Value)
                            txt_SP_VALID_FROM_D.Value = DateTime.Parse(rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                        if (rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[2] != DBNull.Value)
                            txt_SP_VALID_TO_D.Value = DateTime.Parse(rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                        txt_SP_ALL_IND.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                        txt_SP_DED_AMOUNT.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                        txt_SP_DED_PER.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                        txt_SP_ACCOUNT_NO_D.Text = rsltCode1.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();

                    }

                    if (txtOption.Text == "M" || txtOption.Text == "D")
                    {
                        returnValue = proc_mod_Del();


                    }

                    if (txtOption.Text == "V" || txtOption.Text == "A")
                        returnValue = proc_view_add();
                    if (returnValue == false)
                    {
                        e.Cancel = true;
                        //return;
                    }

                    if (txtOption.Text == "A" && txt_SP_DED_DESC.Text != string.Empty)
                    {
                        MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        base.Cancel();
                    }

                    if (txtOption.Text == "V" && returnValue != false)
                    {
                        //go_field('blk_head.w_ans5');
                        GoToPage7();

                        if (NoDetails == true)
                        {
                            e.Cancel = true;
                            return;
                        }


                        if (ViewRecpp == "Yes")
                        {
                            txt_SP_DED_CODE.Select();
                            txt_SP_DED_CODE.Focus();
                            txtOption.Text = "V";
                        }
                        else if (ViewRecpp == "No")
                        {
                            txtOption.Text = "V";
                            txtOption.Select();
                            txtOption.Focus();
                        }


                    }
                    if (txtOption.Text == "D")
                    {
                        //go_field('blk_head.w_ans7');
                        GoToPage9();

                        if (DeleteDtlpp == "Yes" && NoDetails == true)
                        {
                            e.Cancel = true;
                        }
                        else if (DeleteDtlpp == "No")
                        {

                        }

                    }

                    if (txt_SP_BRANCH_D.Text != string.Empty && txt_SP_DESG_D.Text != "CTT")
                    {
                    }
                    else
                    {
                        //MessageBox.Show("Invalid Entry", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //e.Cancel = true;
                    }
                }
                //else
                //{
                //    MessageBox.Show("Record Not Found", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    e.Cancel = true;
                //}
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_CATEGORY_D_Validating", exp);
            }
        }

        /// <summary>
        /// if not in add mode than move to from date.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_DED_DESC_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txtOption.Text != "A")
                {
                    txt_SP_VALID_FROM_D.Enabled = false;
                    txt_SP_VALID_TO_D.Select();
                    txt_SP_VALID_TO_D.Focus();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_DED_DESC_Validating", exp);
            }
        }

        /// <summary>
        /// txt_SP_VALID_FROM_D Validation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_VALID_FROM_D_Validating(object sender, CancelEventArgs e)
        {
            try
            {

                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_VALID_FROM_D", txt_SP_VALID_FROM_D.Value);
                rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "Valid_From_D", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Payroll Generated For The Given Date Range", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_VALID_FROM_D_Validating", exp);
            }
        }

        /// <summary>
        /// Validate txt_SP_VALID_TO_D: Should be greater than txt_SP_VALID_FROM_D
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_VALID_TO_D_Validating(object sender, CancelEventArgs e)
        {
            try
            {

                if (txt_SP_VALID_TO_D.Value != null && txt_SP_VALID_FROM_D.Value != null)
                {
                    if (DateTime.Compare(Convert.ToDateTime(txt_SP_VALID_TO_D.Value), Convert.ToDateTime(txt_SP_VALID_FROM_D.Value)) < 0)
                    {
                        MessageBox.Show("To Date Must Be Greater Then From Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                }

                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_VALID_TO_D", txt_SP_VALID_TO_D.Value);
                rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "Valid_To_D", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Payroll Generated For The Given Date Range", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_VALID_TO_D_Validating", exp);
            }
        }

        /// <summary>
        /// txt_SP_ALL_IND Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_ALL_IND_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_ALL_IND.Text != "A" && txt_SP_ALL_IND.Text != "I")
            {
                MessageBox.Show("Invalid Entry ... Please Enter  A  Or  I", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        /// <summary>
        /// txt_SP_ALL_IND Validated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_ALL_IND_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txt_SP_ALL_IND.Text == "I" && txtOption.Text != "M")
                {
                    //Open Popup 
                    //go_block('blktwo');clear_block(no_commit);
                    frmDedDetail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DedDetailEntry(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvDedDetail.GridSource);
                    frmDedDetail.MdiParent = null;
                    frmDedDetail.ShowDialog();


                    //if (dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].Value != string.Empty && dgvDedDetail.CurrentRow.Cells["grdName"].Value != string.Empty)
                    //{
                    //    frmPage6 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblAddPopUP);
                    //    frmPage6.Text = string.Empty;
                    //    frmPage6.TextBox.MaxLength = 1;
                    //    frmPage6.TextBox.Validating += new CancelEventHandler(Ans4Pg6TextBox_Validating);
                    //    frmPage6.MdiParent = null;
                    //    frmPage6.ShowDialog();
                    //    frmPage6.Text = frmPage6.Value;
                    //}


                    if (PrevPgpp == "Yes")
                    {
                        txt_SP_DED_AMOUNT.Select();
                        txt_SP_DED_AMOUNT.Focus();
                    }
                }
                else if (txt_SP_ALL_IND.Text == "M")
                {
                    txt_SP_DED_AMOUNT.Select();
                    txt_SP_DED_AMOUNT.Focus();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_ALL_IND_Validated", exp);
            }
        }

        /// <summary>
        /// txt_SP_DED_AMOUNT Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_DED_AMOUNT_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_DED_AMOUNT.Text != string.Empty)
            {
                txt_SP_ACCOUNT_NO_D.Select();
                txt_SP_ACCOUNT_NO_D.Focus();
                txt_SP_DED_PER.Enabled = false;
            }
            else
            {
                txt_SP_DED_PER.Enabled = true;
            }
        }

        /// <summary>
        /// txt_SP_DED_PER Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_DED_PER_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_DED_AMOUNT.Text == string.Empty && txt_SP_DED_PER.Text == string.Empty)
            {
                MessageBox.Show("Enter Percentage .......!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        /// <summary>
        /// txt_SP_ACCOUNT_NO_D Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_ACCOUNT_NO_D_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                g_ded_code = txt_SP_DED_CODE.Text;
                g_desg_d = txt_SP_DESG_D.Text;
                g_level_d = txt_SP_LEVEL_D.Text;
                g_branch_d = txt_SP_BRANCH_D.Text;

                if (txt_SP_ACCOUNT_NO_D.Text == string.Empty)
                {
                    MessageBox.Show("Enter Account Number .......!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }

                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_ACCOUNT_NO_D", txt_SP_ACCOUNT_NO_D.Text);
                rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "Account_No_D", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    txt_SP_ACCOUNT_NO_D.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txt_W_ACC_DESC.Text = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                }

                if (txtOption.Text == "M")
                {
                    //proc_modify;
                    //go_field('blk_head.W_ANS6');

                    GoToPage8();

                    if (ModifyDtlpp == "Yes")
                    {

                    }
                    else if (ModifyDtlpp == "No")
                    {
                        txt_SP_DED_CODE.Select();
                        txt_SP_DED_CODE.Focus();
                        txtOption.Text = "M";
                        txt_SP_VALID_FROM_D.Value = null;
                        txt_SP_VALID_TO_D.Value = null;
                    }


                }
                else if (txtOption.Text == "A" && txt_SP_DED_DESC.Text != string.Empty)
                {
                    //go_field('blk_head.w_ans3');

                    GoToPage4();

                    if (SaveRecpp == "Yes")
                    {
                        txt_SP_DED_CODE.Select();
                        txt_SP_DED_CODE.Focus();
                        txtOption.Text = "A";
                        txt_W_OPTION_DIS.Text = "ADD";
                        txtDate.Text = Now().ToString("dd/MM/yyyy");
                    }
                    else if (SaveRecpp == "No")
                    {
                        txtOption.Select();
                        txtOption.Focus();
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "txt_SP_ACCOUNT_NO_D_Validating", exp);
            }
        }

        /// <summary>
        /// txtOption Leave Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOption_Leave(object sender, EventArgs e)
        {
            if (txtOption.Text == "V")
                txt_W_OPTION_DIS.Text = "View";
            else if (txtOption.Text == "D")
                txt_W_OPTION_DIS.Text = "Delete";
            else if (txtOption.Text == "Q")
                txt_W_OPTION_DIS.Text = "Query";
            else if (txtOption.Text == "M")
                txt_W_OPTION_DIS.Text = "Modify";
            else if (txtOption.Text == "E")
                txt_W_OPTION_DIS.Text = "Exit";

            if (txtOption.Text == "A")
            {
                txt_W_OPTION_DIS.Text = "Add";
                lbtnDedection.Visible = false;
                lbtnDedection.SkipValidationOnLeave = true;
            }
            else
            {
                lbtnDedection.Visible = true;
                lbtnDedection.SkipValidationOnLeave = false;
            }



            g_option = txtOption.Text;

            //if (txtOption.Text == "Q")
            //{
            //    lbtnDedection.SkipValidationOnLeave = true;
            //    frmDedMD = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DeductionEntry_MasterDetail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
            //    frmDedMD.MdiParent = null;
            //    frmDedMD.ShowDialog();

            //}
        }


        #endregion

        #region --Pop Ups--

        public void GoToPage2()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To View More Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    ViewRecpp = "Yes";
                    ClearForm();

                    this.View();
                    base.IterateFormToEnableControls(this.Controls, true);
                    txtOption.Text = "V";
                    txt_W_OPTION_DIS.Text = "View";
                    txt_SP_DED_CODE.Select();
                    txt_SP_DED_CODE.Focus();

                }
                else if (dRslt == DialogResult.No)
                {
                    ViewRecpp = "No";
                    ClearForm();
                    txtOption.Select();
                    txtOption.Focus();
                }

                DeleteRecpp = string.Empty;
                SaveRecpp = string.Empty;
                PrevPgpp = string.Empty;
                SeeDtlpp = string.Empty;
                ModifyDtlpp = string.Empty;
                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage2", exp);
            }
        }

        public void GoToPage3()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Delete The Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    DeleteRecpp = "Yes";
                    this.operationMode = Mode.View;
                    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
                    //base.DoToolbarActions(this.Controls, "Delete");
                    //base.Delete();
                    //ClearForm();

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    param.Add("SP_VALID_TO_D", txt_SP_VALID_TO_D.Value);
                    param.Add("SP_VALID_FROM_D", txt_SP_VALID_FROM_D.Value);
                    rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "DELETE_W_ANS2", param);

                    if (rsltCode.isSuccessful && ((System.Data.InternalDataCollectionBase)(rsltCode.dstResult.Tables)).Count > 0)
                    {
                        MessageBox.Show("Payroll Generated For The Given Date Range", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //e.Cancel = true;
                    }

                    txt_SP_DED_CODE.Select();
                    txt_SP_DED_CODE.Focus();
                    ClearForm();
                }
                else if (dRslt == DialogResult.No)
                {
                    DeleteRecpp = "No";
                    ClearForm();
                }
                ViewRecpp = string.Empty;

                SaveRecpp = string.Empty;
                PrevPgpp = string.Empty;
                SeeDtlpp = string.Empty;
                ModifyDtlpp = string.Empty;
                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage3", exp);
            }
        }

        public void GoToPage4()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Save The Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (txtOption.Text != "M")
                {
                    if (dRslt == DialogResult.Yes)
                    {
                        SaveRecpp = "Yes";
                        base.Save();
                        //CallReportOld();
                        //CallReportNew();
                        ClearForm();

                    }
                    else if (dRslt == DialogResult.No)
                    {
                        SaveRecpp = "No";
                        ClearForm();
                        txt_SP_VALID_FROM_D.Value = null;
                        txt_SP_VALID_TO_D.Value = null;
                        txtOption.Select();
                        txtOption.Focus();
                    }
                }
                else
                {
                    if (dRslt == DialogResult.Yes)
                    {
                        SaveRecpp = "Yes";
                        base.Save();
                        ClearForm();
                        txt_SP_DED_CODE.Select();
                        txt_SP_DED_CODE.Focus();
                        txtOption.Text = "M";
                        txt_W_OPTION_DIS.Text = "MODIFY";
                        txtDate.Text = Now().ToString("dd/MM/yyyy");
                    }
                    else if (dRslt == DialogResult.No)
                    {
                        SaveRecpp = "No";
                        ClearForm();
                        txt_SP_VALID_FROM_D.Value = null;
                        txt_SP_VALID_TO_D.Value = null;
                        txtOption.Select();
                        txtOption.Focus();
                    }
                }
                ViewRecpp = string.Empty;
                DeleteRecpp = string.Empty;

                PrevPgpp = string.Empty;
                SeeDtlpp = string.Empty;
                ModifyDtlpp = string.Empty;
                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage4", exp);
            }
        }

        public void GoToPage6()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Go To Previous Page [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    PrevPgpp = "Yes";
                    //if (frmDedDetail.)
                    frmDedDetail.Hide();

                    txt_SP_DED_AMOUNT.Select();
                    txt_SP_DED_AMOUNT.Focus();
                }
                else if (dRslt == DialogResult.No)
                {
                    PrevPgpp = "No";
                    dgvDedDetail.Rows[0].Cells[0].Selected = true;
                }
                ViewRecpp = string.Empty;
                DeleteRecpp = string.Empty;
                SaveRecpp = string.Empty;

                SeeDtlpp = string.Empty;
                ModifyDtlpp = string.Empty;
                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage6", exp);
            }
        }

        public void GoToPage7()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To See The Details [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    SeeDtlpp = "Yes";

                    if (txt_SP_ALL_IND.Text == "A")
                    {
                        MessageBox.Show(" Sorry No Details Available", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NoDetails = true;
                        GoToPage7();
                        return;
                    }

                    if (NoDetails == false)
                    {
                        frmDedDetail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DedDetailEntry(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvDedDetail.GridSource);
                        frmDedDetail.MdiParent = null;
                        frmDedDetail.ShowDialog();


                        //if (txtOption.Text == "V")
                        //{
                        //    GoToPage2();
                        //}
                        if (ViewRecpp == "Yes")
                        {
                            txtOption.Text = "V";
                            txt_W_OPTION_DIS.Text = "View";
                            txtDate.Text = Now().ToString("dd/MM/yyyy");
                            txt_SP_DED_CODE.Select();
                            txt_SP_DED_CODE.Focus();
                        }
                        else if (ViewRecpp == "No")
                        {
                            txtOption.Text = "V";
                            txt_W_OPTION_DIS.Text = "View";
                            txtDate.Text = Now().ToString("dd/MM/yyyy");
                            txtOption.Select();
                            txtOption.Focus();
                        }
                    }
                    if (txtOption.Text == "D")
                    {
                        GoToPage3();
                    }
                }
                else if (dRslt == DialogResult.No)
                {
                    NoDetails = false;
                    SeeDtlpp = "No";
                    GoToPage2();

                    if (ViewRecpp == "Yes")
                    {
                        this.View();
                        base.IterateFormToEnableControls(this.Controls, true);
                        txtOption.Text = "V";
                        txt_W_OPTION_DIS.Text = "View";
                        txtDate.Text = Now().ToString("dd/MM/yyyy");
                        txt_SP_DED_CODE.Select();
                        txt_SP_DED_CODE.Focus();
                    }
                    else if (ViewRecpp == "No")
                    {
                        this.Cancel();
                        //txtOption.Text = "V";
                        //txt_W_OPTION_DIS.Text = "View";
                        txtDate.Text = Now().ToString("dd/MM/yyyy");
                        txtOption.Select();
                        txtOption.Focus();
                    }
                }

                ViewRecpp = string.Empty;
                DeleteRecpp = string.Empty;
                SaveRecpp = string.Empty;
                PrevPgpp = string.Empty;
                ModifyDtlpp = string.Empty;
                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage7", exp);
            }
        }

        public void GoToPage8()
        {
            try
            {
                DialogResult dRslt = MessageBox.Show("Do You Want To Modify The Details [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dRslt == DialogResult.Yes)
                {
                    ModifyDtlpp = "Yes";
                    if (txt_SP_ALL_IND.Text == "A")
                    {
                        MessageBox.Show(" Sorry No Details Available", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NoDetails = true;
                    }

                    if (NoDetails == false)
                    {
                        frmDedDetail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DedDetailEntry(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvDedDetail.GridSource);
                        frmDedDetail.MdiParent = null;
                        frmDedDetail.ShowDialog();
                    }

                }
                else if (dRslt == DialogResult.No)
                {
                    ModifyDtlpp = "No";
                    ClearForm();
                    txt_SP_VALID_FROM_D.Value = null;
                    txt_SP_VALID_TO_D.Value = null;

                    if (txt_SP_ALL_IND.Text == "A")
                    {
                        if (txt_SP_DED_CODE.Text != string.Empty)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Add("SP_DEDUC_CODE", txt_SP_DED_CODE.Text);
                            CmnDataManager cmnDM = new CmnDataManager();
                            Result rsltCode;

                            rsltCode = cmnDM.Execute("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "DEL_DED_DETAIL", param);
                        }
                    }
                }
                ViewRecpp = string.Empty;
                DeleteRecpp = string.Empty;
                SaveRecpp = string.Empty;
                PrevPgpp = string.Empty;
                SeeDtlpp = string.Empty;

                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage8", exp);
            }
        }

        public void GoToPage9()
        {
            try
            {
                //DialogResult dRslt = MessageBox.Show("Do You Want To Delete The Details [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //if (dRslt == DialogResult.Yes)
                {
                    DeleteRecpp = "Yes";
                    this.operationMode = Mode.View;
                    this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtDelete"]));
                    //base.DoToolbarActions(this.Controls, "Delete");
                    //base.Delete();
                    //ClearForm();

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    param.Add("SP_VALID_TO_D", txt_SP_VALID_TO_D.Value);
                    param.Add("SP_VALID_FROM_D", txt_SP_VALID_FROM_D.Value);
                    rsltCode = cmnDM.GetData("CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER", "DELETE_W_ANS2", param);

                    if (rsltCode.isSuccessful && ((System.Data.InternalDataCollectionBase)(rsltCode.dstResult.Tables)).Count > 0)
                    {
                        MessageBox.Show("Payroll Generated For The Given Date Range", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //e.Cancel = true;
                    }
                    ClearForm();
                    txt_SP_DED_CODE.Select();
                    txt_SP_DED_CODE.Focus();
                

                    if (txt_SP_ALL_IND.Text == "A")
                    {
                        MessageBox.Show(" Sorry No Details Available", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NoDetails = true;
                    }

                    //if (NoDetails == false)
                    //{
                    //    frmDedDetail = new iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DedDetailEntry(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, this, dgvDedDetail.GridSource);
                    //    frmDedDetail.MdiParent = null;
                    //    frmDedDetail.ShowDialog();
                    //}
                }
                //else if (dRslt == DialogResult.No)
                //{
                //    DeleteRecpp = "No";
                //    ClearForm();
                //}
                ViewRecpp = string.Empty;

                SaveRecpp = string.Empty;
                PrevPgpp = string.Empty;
                SeeDtlpp = string.Empty;
                ModifyDtlpp = string.Empty;
                DeleteDtlpp = string.Empty;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "GoToPage9", exp);
            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.FunctionConfig.CurrentOption = Function.None;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        #endregion

        private void txt_SP_DED_CODE_Enter(object sender, EventArgs e)
        {
            //if (txtOption.Text != "A")
            //{
            //    txt_SP_DED_CODE.AssociatedLookUpName    = "lbtnDedection";
            //    lbtnDedection.ActionType                = "DUC_LOV";
            //    lbtnDedection.ActionLOVExists           = "DUC_LOV_EXISTS";
            //    txt_SP_DED_CODE.SkipValidation          = false;
            //    txt_SP_DED_CODE.ShortcutsEnabled        = true;
            //    lbtnDedection.Visible                   = true;
            //}
            //else if (txtOption.Text == "A")
            //{
            //    txt_SP_DED_CODE.SkipValidation          = true;
            //    txt_SP_DED_CODE.ShortcutsEnabled        = false;
            //    lbtnDedection.Visible                   = false;
            //    txt_SP_DED_CODE.AssociatedLookUpName    = "sss";
            //    txt_SP_DED_CODE.IsLookUpField           = false;
            //}
        }
    }
}