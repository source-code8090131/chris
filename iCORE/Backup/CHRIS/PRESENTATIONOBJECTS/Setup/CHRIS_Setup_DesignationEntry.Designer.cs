namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_DesignationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_DesignationEntry));
            this.BtnInsert = new System.Windows.Forms.Button();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnClear = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PnlDesig = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.DtEffectiveFrm = new CrplControlLibrary.SLDatePicker(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMax = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncAmt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMid = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMin = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.Lov_BRN = new CrplControlLibrary.LookupButton(this.components);
            this.LBCat = new CrplControlLibrary.LookupButton(this.components);
            this.LbDesig = new CrplControlLibrary.LookupButton(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBrnName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCatDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesgDesc2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesgDesc1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSp_current = new CrplControlLibrary.SLTextBox(this.components);
            this.txtConfPeriod = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranchcode = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesg = new CrplControlLibrary.SLTextBox(this.components);
            this.dtsp_effecvtive_hidden = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtMAxHidden = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSp_incremnt_hidden = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMidHidden = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMinHidden = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlDesig.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(622, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(658, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 494);
            this.panel1.Size = new System.Drawing.Size(658, 60);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Location = new System.Drawing.Point(33, 457);
            this.BtnInsert.Name = "BtnInsert";
            this.BtnInsert.Size = new System.Drawing.Size(78, 23);
            this.BtnInsert.TabIndex = 43;
            this.BtnInsert.Text = "Insert";
            this.BtnInsert.UseVisualStyleBackColor = true;
            this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.Location = new System.Drawing.Point(117, 457);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(75, 23);
            this.BtnUpdate.TabIndex = 44;
            this.BtnUpdate.Text = "Update";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            this.BtnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(198, 457);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(75, 23);
            this.BtnDelete.TabIndex = 45;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.Location = new System.Drawing.Point(279, 457);
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(75, 23);
            this.BtnClear.TabIndex = 46;
            this.BtnClear.Text = "Clear";
            this.BtnClear.UseVisualStyleBackColor = true;
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(360, 457);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 47;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(441, 457);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 48;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(522, 457);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(75, 23);
            this.BtnExit.TabIndex = 49;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(438, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "User Name : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label8.Location = new System.Drawing.Point(270, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 13);
            this.label8.TabIndex = 52;
            this.label8.Text = "Designation Entry";
            // 
            // PnlDesig
            // 
            this.PnlDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlDesig.ConcurrentPanels = null;
            this.PnlDesig.Controls.Add(this.label21);
            this.PnlDesig.Controls.Add(this.label20);
            this.PnlDesig.Controls.Add(this.label19);
            this.PnlDesig.Controls.Add(this.label6);
            this.PnlDesig.Controls.Add(this.label18);
            this.PnlDesig.Controls.Add(this.label16);
            this.PnlDesig.Controls.Add(this.DtEffectiveFrm);
            this.PnlDesig.Controls.Add(this.label5);
            this.PnlDesig.Controls.Add(this.label4);
            this.PnlDesig.Controls.Add(this.label3);
            this.PnlDesig.Controls.Add(this.label2);
            this.PnlDesig.Controls.Add(this.txtMax);
            this.PnlDesig.Controls.Add(this.txtIncAmt);
            this.PnlDesig.Controls.Add(this.txtMid);
            this.PnlDesig.Controls.Add(this.txtMin);
            this.PnlDesig.Controls.Add(this.label1);
            this.PnlDesig.Controls.Add(this.Lov_BRN);
            this.PnlDesig.Controls.Add(this.LBCat);
            this.PnlDesig.Controls.Add(this.LbDesig);
            this.PnlDesig.Controls.Add(this.label15);
            this.PnlDesig.Controls.Add(this.label14);
            this.PnlDesig.Controls.Add(this.label13);
            this.PnlDesig.Controls.Add(this.label12);
            this.PnlDesig.Controls.Add(this.label11);
            this.PnlDesig.Controls.Add(this.label10);
            this.PnlDesig.Controls.Add(this.label9);
            this.PnlDesig.Controls.Add(this.label7);
            this.PnlDesig.Controls.Add(this.txtBrnName);
            this.PnlDesig.Controls.Add(this.txtCatDesc);
            this.PnlDesig.Controls.Add(this.txtDesgDesc2);
            this.PnlDesig.Controls.Add(this.txtDesgDesc1);
            this.PnlDesig.Controls.Add(this.txtSp_current);
            this.PnlDesig.Controls.Add(this.txtConfPeriod);
            this.PnlDesig.Controls.Add(this.txtBranchcode);
            this.PnlDesig.Controls.Add(this.txtCategory);
            this.PnlDesig.Controls.Add(this.txtLevel);
            this.PnlDesig.Controls.Add(this.txtDesg);
            this.PnlDesig.DataManager = null;
            this.PnlDesig.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDesig.DependentPanels = null;
            this.PnlDesig.DisableDependentLoad = false;
            this.PnlDesig.EnableDelete = true;
            this.PnlDesig.EnableInsert = true;
            this.PnlDesig.EnableQuery = false;
            this.PnlDesig.EnableUpdate = true;
            this.PnlDesig.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DESIGCommand";
            this.PnlDesig.Location = new System.Drawing.Point(12, 94);
            this.PnlDesig.MasterPanel = null;
            this.PnlDesig.Name = "PnlDesig";
            this.PnlDesig.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDesig.Size = new System.Drawing.Size(625, 350);
            this.PnlDesig.SPName = "CHRIS_SP_DESIG_MANAGER";
            this.PnlDesig.TabIndex = 51;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(551, 269);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(9, 52);
            this.label21.TabIndex = 63;
            this.label21.Text = "|\r\n|\r\n|\r\n|\r\n";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(64, 268);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(9, 52);
            this.label20.TabIndex = 62;
            this.label20.Text = "|\r\n|\r\n|\r\n|\r\n";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(65, 312);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(493, 13);
            this.label19.TabIndex = 61;
            this.label19.Text = "_________________________________________________________________________________" +
                "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(493, 13);
            this.label6.TabIndex = 60;
            this.label6.Text = "_________________________________________________________________________________" +
                "";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(117, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "Desg. Abbreviation";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label16.Location = new System.Drawing.Point(272, 220);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 51;
            this.label16.Text = "Salary Ranges";
            // 
            // DtEffectiveFrm
            // 
            this.DtEffectiveFrm.CustomEnabled = true;
            this.DtEffectiveFrm.CustomFormat = "dd/MM/yyyy";
            this.DtEffectiveFrm.DataFieldMapping = "SP_EFFECTIVE";
            this.DtEffectiveFrm.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtEffectiveFrm.HasChanges = true;
            this.DtEffectiveFrm.Location = new System.Drawing.Point(440, 290);
            this.DtEffectiveFrm.Name = "DtEffectiveFrm";
            this.DtEffectiveFrm.NullValue = " ";
            this.DtEffectiveFrm.Size = new System.Drawing.Size(90, 20);
            this.DtEffectiveFrm.TabIndex = 14;
            this.DtEffectiveFrm.Value = new System.DateTime(2011, 1, 26, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(441, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 58;
            this.label5.Text = "Effective From";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(355, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Inc Amount";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(273, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "Max";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(185, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Mid";
            // 
            // txtMax
            // 
            this.txtMax.AllowSpace = true;
            this.txtMax.AssociatedLookUpName = "";
            this.txtMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMax.ContinuationTextBox = null;
            this.txtMax.CustomEnabled = true;
            this.txtMax.DataFieldMapping = "SP_MAX";
            this.txtMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMax.GetRecordsOnUpDownKeys = false;
            this.txtMax.IsDate = false;
            this.txtMax.Location = new System.Drawing.Point(276, 290);
            this.txtMax.MaxLength = 11;
            this.txtMax.Name = "txtMax";
            this.txtMax.NumberFormat = "###,###,##0.00";
            this.txtMax.Postfix = "";
            this.txtMax.Prefix = "";
            this.txtMax.Size = new System.Drawing.Size(76, 20);
            this.txtMax.SkipValidation = false;
            this.txtMax.TabIndex = 12;
            this.txtMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMax.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtIncAmt
            // 
            this.txtIncAmt.AllowSpace = true;
            this.txtIncAmt.AssociatedLookUpName = "";
            this.txtIncAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncAmt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncAmt.ContinuationTextBox = null;
            this.txtIncAmt.CustomEnabled = true;
            this.txtIncAmt.DataFieldMapping = "SP_INCREMENT";
            this.txtIncAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncAmt.GetRecordsOnUpDownKeys = false;
            this.txtIncAmt.IsDate = false;
            this.txtIncAmt.Location = new System.Drawing.Point(358, 290);
            this.txtIncAmt.MaxLength = 5;
            this.txtIncAmt.Name = "txtIncAmt";
            this.txtIncAmt.NumberFormat = "###,###,##0.00";
            this.txtIncAmt.Postfix = "";
            this.txtIncAmt.Prefix = "";
            this.txtIncAmt.Size = new System.Drawing.Size(76, 20);
            this.txtIncAmt.SkipValidation = false;
            this.txtIncAmt.TabIndex = 13;
            this.txtIncAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIncAmt.TextType = CrplControlLibrary.TextType.Double;
            // 
            // txtMid
            // 
            this.txtMid.AllowSpace = true;
            this.txtMid.AssociatedLookUpName = "";
            this.txtMid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMid.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMid.ContinuationTextBox = null;
            this.txtMid.CustomEnabled = true;
            this.txtMid.DataFieldMapping = "SP_MID";
            this.txtMid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMid.GetRecordsOnUpDownKeys = false;
            this.txtMid.IsDate = false;
            this.txtMid.Location = new System.Drawing.Point(188, 290);
            this.txtMid.MaxLength = 11;
            this.txtMid.Name = "txtMid";
            this.txtMid.NumberFormat = "###,###,##0.00";
            this.txtMid.Postfix = "";
            this.txtMid.Prefix = "";
            this.txtMid.Size = new System.Drawing.Size(76, 20);
            this.txtMid.SkipValidation = false;
            this.txtMid.TabIndex = 11;
            this.txtMid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMid.TextType = CrplControlLibrary.TextType.Double;
            this.txtMid.Leave += new System.EventHandler(this.txtMid_Leave);
            // 
            // txtMin
            // 
            this.txtMin.AllowSpace = true;
            this.txtMin.AssociatedLookUpName = "";
            this.txtMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMin.ContinuationTextBox = null;
            this.txtMin.CustomEnabled = true;
            this.txtMin.DataFieldMapping = "SP_MIN";
            this.txtMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMin.GetRecordsOnUpDownKeys = false;
            this.txtMin.IsDate = false;
            this.txtMin.Location = new System.Drawing.Point(99, 290);
            this.txtMin.MaxLength = 11;
            this.txtMin.Name = "txtMin";
            this.txtMin.NumberFormat = "###,###,##0.00";
            this.txtMin.Postfix = "";
            this.txtMin.Prefix = "";
            this.txtMin.Size = new System.Drawing.Size(76, 20);
            this.txtMin.SkipValidation = false;
            this.txtMin.TabIndex = 10;
            this.txtMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMin.TextType = CrplControlLibrary.TextType.Double;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(102, 275);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Min";
            // 
            // Lov_BRN
            // 
            this.Lov_BRN.ActionLOVExists = "Lov_BRN_Exists";
            this.Lov_BRN.ActionType = "Lov_BRN";
            this.Lov_BRN.ConditionalFields = "";
            this.Lov_BRN.CustomEnabled = true;
            this.Lov_BRN.DataFieldMapping = "";
            this.Lov_BRN.DependentLovControls = "";
            this.Lov_BRN.HiddenColumns = "";
            this.Lov_BRN.Image = ((System.Drawing.Image)(resources.GetObject("Lov_BRN.Image")));
            this.Lov_BRN.LoadDependentEntities = false;
            this.Lov_BRN.Location = new System.Drawing.Point(211, 121);
            this.Lov_BRN.LookUpTitle = null;
            this.Lov_BRN.Name = "Lov_BRN";
            this.Lov_BRN.Size = new System.Drawing.Size(26, 21);
            this.Lov_BRN.SkipValidationOnLeave = false;
            this.Lov_BRN.SPName = "CHRIS_SP_DESIG_MANAGER";
            this.Lov_BRN.TabIndex = 32;
            this.Lov_BRN.TabStop = false;
            this.Lov_BRN.UseVisualStyleBackColor = true;
            // 
            // LBCat
            // 
            this.LBCat.ActionLOVExists = "Lov_CAT_Exists";
            this.LBCat.ActionType = "Lov_CAT";
            this.LBCat.ConditionalFields = "";
            this.LBCat.CustomEnabled = true;
            this.LBCat.DataFieldMapping = "";
            this.LBCat.DependentLovControls = "";
            this.LBCat.HiddenColumns = "";
            this.LBCat.Image = ((System.Drawing.Image)(resources.GetObject("LBCat.Image")));
            this.LBCat.LoadDependentEntities = false;
            this.LBCat.Location = new System.Drawing.Point(211, 98);
            this.LBCat.LookUpTitle = null;
            this.LBCat.Name = "LBCat";
            this.LBCat.Size = new System.Drawing.Size(26, 21);
            this.LBCat.SkipValidationOnLeave = false;
            this.LBCat.SPName = "CHRIS_SP_DESIG_MANAGER";
            this.LBCat.TabIndex = 31;
            this.LBCat.TabStop = false;
            this.LBCat.UseVisualStyleBackColor = true;
            // 
            // LbDesig
            // 
            this.LbDesig.ActionLOVExists = "Lov_DSG_Exists";
            this.LbDesig.ActionType = "Lov_DSG";
            this.LbDesig.ConditionalFields = "txtDesg|txtLevel|txtBranchcode";
            this.LbDesig.CustomEnabled = true;
            this.LbDesig.DataFieldMapping = "";
            this.LbDesig.DependentLovControls = "";
            this.LbDesig.HiddenColumns = "BRN_NAME|CAT_DISC|SP_DESC_DG1|SP_DESC_DG2|SP_CONFIRM|SP_MIN|SP_MID|SP_MAX|SP_INCR" +
                "EMENT|SP_EFFECTIVE|SP_CURRENT";
            this.LbDesig.Image = ((System.Drawing.Image)(resources.GetObject("LbDesig.Image")));
            this.LbDesig.LoadDependentEntities = false;
            this.LbDesig.Location = new System.Drawing.Point(211, 17);
            this.LbDesig.LookUpTitle = null;
            this.LbDesig.Name = "LbDesig";
            this.LbDesig.Size = new System.Drawing.Size(26, 21);
            this.LbDesig.SkipValidationOnLeave = false;
            this.LbDesig.SPName = "CHRIS_SP_DESIG_MANAGER";
            this.LbDesig.TabIndex = 30;
            this.LbDesig.TabStop = false;
            this.LbDesig.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(62, 175);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Sp Current";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(298, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Branch Name";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(49, 125);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Branch Code";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(72, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Category";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(91, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Level";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(288, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Cat. Description";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Confirmed Period";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(274, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Desig Description";
            // 
            // txtBrnName
            // 
            this.txtBrnName.AllowSpace = true;
            this.txtBrnName.AssociatedLookUpName = "";
            this.txtBrnName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBrnName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBrnName.ContinuationTextBox = null;
            this.txtBrnName.CustomEnabled = true;
            this.txtBrnName.DataFieldMapping = "BRN_NAME";
            this.txtBrnName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrnName.GetRecordsOnUpDownKeys = false;
            this.txtBrnName.IsDate = false;
            this.txtBrnName.Location = new System.Drawing.Point(387, 115);
            this.txtBrnName.MaxLength = 40;
            this.txtBrnName.Name = "txtBrnName";
            this.txtBrnName.NumberFormat = "###,###,##0.00";
            this.txtBrnName.Postfix = "";
            this.txtBrnName.Prefix = "";
            this.txtBrnName.Size = new System.Drawing.Size(190, 20);
            this.txtBrnName.SkipValidation = false;
            this.txtBrnName.TabIndex = 7;
            this.txtBrnName.TabStop = false;
            this.txtBrnName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCatDesc
            // 
            this.txtCatDesc.AllowSpace = true;
            this.txtCatDesc.AssociatedLookUpName = "";
            this.txtCatDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCatDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCatDesc.ContinuationTextBox = null;
            this.txtCatDesc.CustomEnabled = true;
            this.txtCatDesc.DataFieldMapping = "CAT_DISC";
            this.txtCatDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCatDesc.GetRecordsOnUpDownKeys = false;
            this.txtCatDesc.IsDate = false;
            this.txtCatDesc.Location = new System.Drawing.Point(387, 89);
            this.txtCatDesc.MaxLength = 20;
            this.txtCatDesc.Name = "txtCatDesc";
            this.txtCatDesc.NumberFormat = "###,###,##0.00";
            this.txtCatDesc.Postfix = "";
            this.txtCatDesc.Prefix = "";
            this.txtCatDesc.Size = new System.Drawing.Size(190, 20);
            this.txtCatDesc.SkipValidation = false;
            this.txtCatDesc.TabIndex = 5;
            this.txtCatDesc.TabStop = false;
            this.txtCatDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesgDesc2
            // 
            this.txtDesgDesc2.AllowSpace = true;
            this.txtDesgDesc2.AssociatedLookUpName = "";
            this.txtDesgDesc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesgDesc2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesgDesc2.ContinuationTextBox = null;
            this.txtDesgDesc2.CustomEnabled = true;
            this.txtDesgDesc2.DataFieldMapping = "SP_DESC_DG2";
            this.txtDesgDesc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesgDesc2.GetRecordsOnUpDownKeys = false;
            this.txtDesgDesc2.IsDate = false;
            this.txtDesgDesc2.Location = new System.Drawing.Point(387, 46);
            this.txtDesgDesc2.MaxLength = 20;
            this.txtDesgDesc2.Name = "txtDesgDesc2";
            this.txtDesgDesc2.NumberFormat = "###,###,##0.00";
            this.txtDesgDesc2.Postfix = "";
            this.txtDesgDesc2.Prefix = "";
            this.txtDesgDesc2.Size = new System.Drawing.Size(219, 20);
            this.txtDesgDesc2.SkipValidation = false;
            this.txtDesgDesc2.TabIndex = 2;
            this.txtDesgDesc2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesgDesc1
            // 
            this.txtDesgDesc1.AllowSpace = true;
            this.txtDesgDesc1.AssociatedLookUpName = "";
            this.txtDesgDesc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesgDesc1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesgDesc1.ContinuationTextBox = null;
            this.txtDesgDesc1.CustomEnabled = true;
            this.txtDesgDesc1.DataFieldMapping = "SP_DESC_DG1";
            this.txtDesgDesc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesgDesc1.GetRecordsOnUpDownKeys = false;
            this.txtDesgDesc1.IsDate = false;
            this.txtDesgDesc1.Location = new System.Drawing.Point(387, 20);
            this.txtDesgDesc1.MaxLength = 20;
            this.txtDesgDesc1.Name = "txtDesgDesc1";
            this.txtDesgDesc1.NumberFormat = "###,###,##0.00";
            this.txtDesgDesc1.Postfix = "";
            this.txtDesgDesc1.Prefix = "";
            this.txtDesgDesc1.Size = new System.Drawing.Size(219, 20);
            this.txtDesgDesc1.SkipValidation = false;
            this.txtDesgDesc1.TabIndex = 1;
            this.txtDesgDesc1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSp_current
            // 
            this.txtSp_current.AllowSpace = true;
            this.txtSp_current.AssociatedLookUpName = "";
            this.txtSp_current.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSp_current.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSp_current.ContinuationTextBox = null;
            this.txtSp_current.CustomEnabled = true;
            this.txtSp_current.DataFieldMapping = "SP_CURRENT";
            this.txtSp_current.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSp_current.GetRecordsOnUpDownKeys = false;
            this.txtSp_current.IsDate = false;
            this.txtSp_current.Location = new System.Drawing.Point(133, 173);
            this.txtSp_current.MaxLength = 1;
            this.txtSp_current.Name = "txtSp_current";
            this.txtSp_current.NumberFormat = "###,###,##0.00";
            this.txtSp_current.Postfix = "";
            this.txtSp_current.Prefix = "";
            this.txtSp_current.Size = new System.Drawing.Size(37, 20);
            this.txtSp_current.SkipValidation = false;
            this.txtSp_current.TabIndex = 9;
            this.txtSp_current.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtConfPeriod
            // 
            this.txtConfPeriod.AllowSpace = true;
            this.txtConfPeriod.AssociatedLookUpName = "";
            this.txtConfPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfPeriod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfPeriod.ContinuationTextBox = null;
            this.txtConfPeriod.CustomEnabled = true;
            this.txtConfPeriod.DataFieldMapping = "SP_CONFIRM";
            this.txtConfPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfPeriod.GetRecordsOnUpDownKeys = false;
            this.txtConfPeriod.IsDate = false;
            this.txtConfPeriod.Location = new System.Drawing.Point(133, 147);
            this.txtConfPeriod.MaxLength = 1;
            this.txtConfPeriod.Name = "txtConfPeriod";
            this.txtConfPeriod.NumberFormat = "###,###,##0.00";
            this.txtConfPeriod.Postfix = "";
            this.txtConfPeriod.Prefix = "";
            this.txtConfPeriod.Size = new System.Drawing.Size(37, 20);
            this.txtConfPeriod.SkipValidation = false;
            this.txtConfPeriod.TabIndex = 8;
            this.txtConfPeriod.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtBranchcode
            // 
            this.txtBranchcode.AllowSpace = true;
            this.txtBranchcode.AssociatedLookUpName = "Lov_BRN";
            this.txtBranchcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchcode.ContinuationTextBox = null;
            this.txtBranchcode.CustomEnabled = true;
            this.txtBranchcode.DataFieldMapping = "SP_BRANCH";
            this.txtBranchcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchcode.GetRecordsOnUpDownKeys = false;
            this.txtBranchcode.IsDate = false;
            this.txtBranchcode.Location = new System.Drawing.Point(133, 121);
            this.txtBranchcode.MaxLength = 3;
            this.txtBranchcode.Name = "txtBranchcode";
            this.txtBranchcode.NumberFormat = "###,###,##0.00";
            this.txtBranchcode.Postfix = "";
            this.txtBranchcode.Prefix = "";
            this.txtBranchcode.Size = new System.Drawing.Size(72, 20);
            this.txtBranchcode.SkipValidation = false;
            this.txtBranchcode.TabIndex = 6;
            this.txtBranchcode.TextType = CrplControlLibrary.TextType.String;
            this.txtBranchcode.Leave += new System.EventHandler(this.txtBranchcode_Leave);
            // 
            // txtCategory
            // 
            this.txtCategory.AllowSpace = true;
            this.txtCategory.AssociatedLookUpName = "LBCat";
            this.txtCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategory.ContinuationTextBox = null;
            this.txtCategory.CustomEnabled = true;
            this.txtCategory.DataFieldMapping = "SP_CATEGORY";
            this.txtCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.GetRecordsOnUpDownKeys = false;
            this.txtCategory.IsDate = false;
            this.txtCategory.Location = new System.Drawing.Point(133, 95);
            this.txtCategory.MaxLength = 1;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.NumberFormat = "###,###,##0.00";
            this.txtCategory.Postfix = "";
            this.txtCategory.Prefix = "";
            this.txtCategory.Size = new System.Drawing.Size(37, 20);
            this.txtCategory.SkipValidation = false;
            this.txtCategory.TabIndex = 4;
            this.txtCategory.TextType = CrplControlLibrary.TextType.String;
            this.txtCategory.Leave += new System.EventHandler(this.txtCategory_Leave);
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "SP_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(133, 69);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(72, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 3;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesg
            // 
            this.txtDesg.AllowSpace = true;
            this.txtDesg.AssociatedLookUpName = "LbDesig";
            this.txtDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesg.ContinuationTextBox = null;
            this.txtDesg.CustomEnabled = true;
            this.txtDesg.DataFieldMapping = "SP_DESG";
            this.txtDesg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesg.GetRecordsOnUpDownKeys = false;
            this.txtDesg.IsDate = false;
            this.txtDesg.Location = new System.Drawing.Point(133, 20);
            this.txtDesg.MaxLength = 3;
            this.txtDesg.Name = "txtDesg";
            this.txtDesg.NumberFormat = "###,###,##0.00";
            this.txtDesg.Postfix = "";
            this.txtDesg.Prefix = "";
            this.txtDesg.Size = new System.Drawing.Size(72, 20);
            this.txtDesg.SkipValidation = false;
            this.txtDesg.TabIndex = 0;
            this.txtDesg.TextType = CrplControlLibrary.TextType.String;
            // 
            // dtsp_effecvtive_hidden
            // 
            this.dtsp_effecvtive_hidden.CustomEnabled = true;
            this.dtsp_effecvtive_hidden.CustomFormat = "dd/MM/yyyy";
            this.dtsp_effecvtive_hidden.DataFieldMapping = "SP_EFFECTIVE";
            this.dtsp_effecvtive_hidden.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtsp_effecvtive_hidden.HasChanges = true;
            this.dtsp_effecvtive_hidden.Location = new System.Drawing.Point(532, 49);
            this.dtsp_effecvtive_hidden.Name = "dtsp_effecvtive_hidden";
            this.dtsp_effecvtive_hidden.NullValue = " ";
            this.dtsp_effecvtive_hidden.Size = new System.Drawing.Size(90, 20);
            this.dtsp_effecvtive_hidden.TabIndex = 45;
            this.dtsp_effecvtive_hidden.Value = new System.DateTime(2011, 1, 26, 0, 0, 0, 0);
            this.dtsp_effecvtive_hidden.Visible = false;
            // 
            // txtMAxHidden
            // 
            this.txtMAxHidden.AllowSpace = true;
            this.txtMAxHidden.AssociatedLookUpName = "";
            this.txtMAxHidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMAxHidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMAxHidden.ContinuationTextBox = null;
            this.txtMAxHidden.CustomEnabled = true;
            this.txtMAxHidden.DataFieldMapping = "SP_MAX";
            this.txtMAxHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMAxHidden.GetRecordsOnUpDownKeys = false;
            this.txtMAxHidden.IsDate = false;
            this.txtMAxHidden.Location = new System.Drawing.Point(359, 49);
            this.txtMAxHidden.MaxLength = 11;
            this.txtMAxHidden.Name = "txtMAxHidden";
            this.txtMAxHidden.NumberFormat = "###,###,##0.00";
            this.txtMAxHidden.Postfix = "";
            this.txtMAxHidden.Prefix = "";
            this.txtMAxHidden.Size = new System.Drawing.Size(76, 20);
            this.txtMAxHidden.SkipValidation = false;
            this.txtMAxHidden.TabIndex = 44;
            this.txtMAxHidden.TextType = CrplControlLibrary.TextType.String;
            this.txtMAxHidden.Visible = false;
            // 
            // txtSp_incremnt_hidden
            // 
            this.txtSp_incremnt_hidden.AllowSpace = true;
            this.txtSp_incremnt_hidden.AssociatedLookUpName = "";
            this.txtSp_incremnt_hidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSp_incremnt_hidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSp_incremnt_hidden.ContinuationTextBox = null;
            this.txtSp_incremnt_hidden.CustomEnabled = true;
            this.txtSp_incremnt_hidden.DataFieldMapping = "SP_INCREMENT";
            this.txtSp_incremnt_hidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSp_incremnt_hidden.GetRecordsOnUpDownKeys = false;
            this.txtSp_incremnt_hidden.IsDate = false;
            this.txtSp_incremnt_hidden.Location = new System.Drawing.Point(453, 49);
            this.txtSp_incremnt_hidden.MaxLength = 4;
            this.txtSp_incremnt_hidden.Name = "txtSp_incremnt_hidden";
            this.txtSp_incremnt_hidden.NumberFormat = "###,###,##0.00";
            this.txtSp_incremnt_hidden.Postfix = "";
            this.txtSp_incremnt_hidden.Prefix = "";
            this.txtSp_incremnt_hidden.Size = new System.Drawing.Size(76, 20);
            this.txtSp_incremnt_hidden.SkipValidation = false;
            this.txtSp_incremnt_hidden.TabIndex = 43;
            this.txtSp_incremnt_hidden.TextType = CrplControlLibrary.TextType.String;
            this.txtSp_incremnt_hidden.Visible = false;
            // 
            // txtMidHidden
            // 
            this.txtMidHidden.AllowSpace = true;
            this.txtMidHidden.AssociatedLookUpName = "";
            this.txtMidHidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMidHidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMidHidden.ContinuationTextBox = null;
            this.txtMidHidden.CustomEnabled = true;
            this.txtMidHidden.DataFieldMapping = "SP_MID";
            this.txtMidHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMidHidden.GetRecordsOnUpDownKeys = false;
            this.txtMidHidden.IsDate = false;
            this.txtMidHidden.Location = new System.Drawing.Point(250, 49);
            this.txtMidHidden.MaxLength = 11;
            this.txtMidHidden.Name = "txtMidHidden";
            this.txtMidHidden.NumberFormat = "###,###,##0.00";
            this.txtMidHidden.Postfix = "";
            this.txtMidHidden.Prefix = "";
            this.txtMidHidden.Size = new System.Drawing.Size(76, 20);
            this.txtMidHidden.SkipValidation = false;
            this.txtMidHidden.TabIndex = 42;
            this.txtMidHidden.TextType = CrplControlLibrary.TextType.String;
            this.txtMidHidden.Visible = false;
            // 
            // txtMinHidden
            // 
            this.txtMinHidden.AllowSpace = true;
            this.txtMinHidden.AssociatedLookUpName = "";
            this.txtMinHidden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinHidden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMinHidden.ContinuationTextBox = null;
            this.txtMinHidden.CustomEnabled = true;
            this.txtMinHidden.DataFieldMapping = "SP_MIN";
            this.txtMinHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinHidden.GetRecordsOnUpDownKeys = false;
            this.txtMinHidden.IsDate = false;
            this.txtMinHidden.Location = new System.Drawing.Point(149, 49);
            this.txtMinHidden.MaxLength = 11;
            this.txtMinHidden.Name = "txtMinHidden";
            this.txtMinHidden.NumberFormat = "###,###,##0.00";
            this.txtMinHidden.Postfix = "";
            this.txtMinHidden.Prefix = "";
            this.txtMinHidden.Size = new System.Drawing.Size(76, 20);
            this.txtMinHidden.SkipValidation = false;
            this.txtMinHidden.TabIndex = 41;
            this.txtMinHidden.TextType = CrplControlLibrary.TextType.String;
            this.txtMinHidden.Visible = false;
            // 
            // CHRIS_Setup_DesignationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 554);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.PnlDesig);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.BtnInsert);
            this.Controls.Add(this.BtnUpdate);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.BtnClear);
            this.Controls.Add(this.txtMinHidden);
            this.Controls.Add(this.txtMidHidden);
            this.Controls.Add(this.txtMAxHidden);
            this.Controls.Add(this.txtSp_incremnt_hidden);
            this.Controls.Add(this.dtsp_effecvtive_hidden);
            this.CurrentPanelBlock = "PnlDesig";
            this.Name = "CHRIS_Setup_DesignationEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Designation Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Setup_DesignationEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.dtsp_effecvtive_hidden, 0);
            this.Controls.SetChildIndex(this.txtSp_incremnt_hidden, 0);
            this.Controls.SetChildIndex(this.txtMAxHidden, 0);
            this.Controls.SetChildIndex(this.txtMidHidden, 0);
            this.Controls.SetChildIndex(this.txtMinHidden, 0);
            this.Controls.SetChildIndex(this.BtnClear, 0);
            this.Controls.SetChildIndex(this.BtnDelete, 0);
            this.Controls.SetChildIndex(this.BtnSave, 0);
            this.Controls.SetChildIndex(this.BtnUpdate, 0);
            this.Controls.SetChildIndex(this.BtnInsert, 0);
            this.Controls.SetChildIndex(this.BtnExit, 0);
            this.Controls.SetChildIndex(this.PnlDesig, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.btnView, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlDesig.ResumeLayout(false);
            this.PnlDesig.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnInsert;
        private System.Windows.Forms.Button BtnUpdate;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnClear;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label8;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlDesig;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLDatePicker DtEffectiveFrm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtMax;
        private CrplControlLibrary.SLTextBox txtIncAmt;
        private CrplControlLibrary.SLTextBox txtMid;
        private CrplControlLibrary.SLTextBox txtMin;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLDatePicker dtsp_effecvtive_hidden;
        private CrplControlLibrary.SLTextBox txtMAxHidden;
        private CrplControlLibrary.SLTextBox txtSp_incremnt_hidden;
        private CrplControlLibrary.SLTextBox txtMidHidden;
        private CrplControlLibrary.SLTextBox txtMinHidden;
        private CrplControlLibrary.LookupButton Lov_BRN;
        private CrplControlLibrary.LookupButton LBCat;
        private CrplControlLibrary.LookupButton LbDesig;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtBrnName;
        private CrplControlLibrary.SLTextBox txtCatDesc;
        private CrplControlLibrary.SLTextBox txtDesgDesc2;
        private CrplControlLibrary.SLTextBox txtDesgDesc1;
        private CrplControlLibrary.SLTextBox txtSp_current;
        private CrplControlLibrary.SLTextBox txtConfPeriod;
        private CrplControlLibrary.SLTextBox txtBranchcode;
        private CrplControlLibrary.SLTextBox txtCategory;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtDesg;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label21;
    }
}