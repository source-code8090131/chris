using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_UserNameSetup : iCORE.Common.PRESENTATIONOBJECTS.Cmn.SimpleForm
    {
        public CHRIS_Setup_UserNameSetup()
        {
            InitializeComponent();
        }
        public CHRIS_Setup_UserNameSetup(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
    }
}