using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_ContractorCodeEntry : ChrisSimpleForm
    {
        #region --Global Variable--
        //iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput frmPopUP ;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPopUP;
        TextBox tbxPopUp        = new TextBox();
        CommonDataManager cmn   = new CommonDataManager();
        string lblViewPopUP     = "Do You Want To View More Record [Y]es [N]o";
        string lblDeletePopUP   = "Do You Want To Delete The Record [Y]es [N]o";
        string lblAddPopUP      = "Do You Want To Save The Record [Y]es [N]o";
        string txtValue         = string.Empty;
        bool Validation         = true;
             
#endregion

        #region --Constructor--
        public CHRIS_Setup_ContractorCodeEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_ContractorCodeEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj )
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            
        }
        
        #endregion

        #region --Method--
        /// <summary>
        /// Shows the User Name and Time on FormLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                txt_SP_CONTRA_EXP_DATE.Value    = null;
                txt_SP_CONTRA_AGRE_DATE.Value   = null;
                lblUserName.Text                = this.UserName;
                txt_W_USER.Text                 = this.userID;
                txtLocation.Text                = this.CurrentLocation;
                txtDate.Text                    = Now().ToString("dd/MM/yyyy");
             
                this.CurrentPanelBlock          = this.pnlContractorCode.Name;
                this.tbtAdd.Available = false;
                this.tbtDelete.Available = false;
                this.tbtEdit.Available = false;
                tbtList.Available = false; 
                this.tbtSave.Available = false;
                this.txt_SP_CONTRA_NID.MaxLength = 13;
 
            }
            catch (Exception exp)
            {
                LogError(this.Name, "OnLoad", exp);
            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                txt_SP_CONTRA_EXP_DATE.Value    = null;
                txt_SP_CONTRA_AGRE_DATE.Value   = null;
                txt_W_OPTION_DIS.Text           = string.Empty;
                //txt_SP_CONTRA_CODE.SkipValidation = true;   
                ClearForm(pnlContractorCode.Controls);
                txtOption.Text                  = string.Empty;
                txt_W_OPTION_DIS.Text           = string.Empty;
                
            }
            else if (actionType == "Save")
            {
                //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ConCodEntCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ConCodEntCommand)this.pnlContractorCode.CurrentBusinessEntity;
                Validate();
                if (Validation == false)
                {
                    base.Cancel();
                    return;
                }
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        /// <summary>
        /// VAlidate the From On SAving
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            //if (txtOption.Text == "A")
            {
                if (txt_SP_CONTRA_CODE.Text == string.Empty)
                {
                    Validation = false;
                    return Validation;
                }
                else if (txt_SP_CONTRA_NAME.Text == string.Empty)
                {
                    Validation = false;
                    return Validation;
                }
                else if (txt_SP_CONTRA_COMP_NAME.Text == string.Empty)
                {
                    Validation = false;
                    return Validation;
                }
                else if (txt_SP_CONTRA_ADD1.Text == string.Empty)
                {
                    Validation = false;
                    return Validation;
                }
                else if (txt_SP_CONTRA_EXP_DATE.Value == null)
                {
                    Validation = false;
                    return Validation;
                }
                else if (txt_SP_CONTRA_AGRE_DATE.Value == null)
                {
                    Validation = false;
                    return Validation;
                }
                else if (txt_SP_CONTRA_NID.Text == string.Empty)
                {
                    Validation = false;
                    return Validation;
                }
            }
            return Validation;
        }

        #endregion

        #region --Events--

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CONTRA_CODE_Validated(object sender, EventArgs e)
        {
            if(txt_SP_CONTRA_CODE.Text == null && txt_SP_CONTRA_CODE.Text == "     ")
            {
                base.Cancel();
                txtOption.Select();
                txtOption.Focus();
            }

            if(txtOption.Text == "M" && txtOption.Text == "D" )
            {
                //proc_mod_Del();
            }
            else if(txtOption.Text == "V" && txtOption.Text == "A" )
            {
                //proc_view_add();
            }

            if (txtOption.Text == "M")
            {
                txt_SP_CONTRA_NAME.Select();
                txt_SP_CONTRA_NAME.Focus();
            }

          
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CONTRA_CODE_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                #region --ADD--
                if (txtOption.Text == "A")
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    param.Add("SP_CONTRA_CODE", txt_SP_CONTRA_CODE.Text);
                    rsltCode = cmnDM.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "CONCODE_EXISTS", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        e.Cancel = true;
                    }
                }
                #endregion

                #region --VIEW--
                if (txtOption.Text == "V")
                {
                    if (txt_SP_CONTRA_CODE.Text != string.Empty)
                    {
                        DialogResult dr = MessageBox.Show("Do You Want To View More Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dr == DialogResult.Yes)
                        {
                            base.Cancel();
                            e.Cancel = true;
                            txtOption.Text = "V";
                            txt_SP_CONTRA_EXP_DATE.Value = null;
                            txt_SP_CONTRA_AGRE_DATE.Value = null;
                            txt_SP_CONTRA_CODE.Select();
                            txt_SP_CONTRA_CODE.Focus();
                            //MessageBox.Show(" Press [F9] To Display The List...  Or Press [Enter] To Exit", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            base.Cancel();
                            txtOption.Text = "V";
                            txtOption.Select();
                            txtOption.Focus();
                            txt_SP_CONTRA_EXP_DATE.Value = null;
                            txt_SP_CONTRA_AGRE_DATE.Value = null;
                        }
                    }
                    else
                    {
                        txtOption.Select();
                        txtOption.Focus();
                    }
                }
                #endregion

                #region --DELETE--
                if (txtOption.Text == "D")
                {
                    if (txt_SP_CONTRA_CODE.Text != string.Empty)
                    {
                        DialogResult dr1 = MessageBox.Show("Do You Want To Delete The Record [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dr1 == DialogResult.Yes)
                        {
                            //base.Delete();
                            base.Delete_Click(this.GetCurrentPanelBlock, "Delete");
                            base.Cancel();
                            txt_SP_CONTRA_EXP_DATE.Value = null;
                            txt_SP_CONTRA_AGRE_DATE.Value = null;
                            txtOption.Text = "D";
                            txt_SP_CONTRA_CODE.Select();
                            txt_SP_CONTRA_CODE.Focus();
                            //MessageBox.Show(" Press [F9] To Display The List...  Or Press [Enter] To Exit", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            base.Cancel();
                            txtOption.Text = "D";
                            txtOption.Select();
                            txtOption.Focus();
                            txt_SP_CONTRA_EXP_DATE.Value = null;
                            txt_SP_CONTRA_AGRE_DATE.Value = null;
                        }
                    }
                    else
                    {
                        txtOption.Focus();
                        txtOption.Select();
                    }
                }
                #endregion
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_SP_CONTRA_CODE_Validating", exp);
            }
        }

        /// <summary>
        /// Skip LOV Validation in ADD Mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CONTRA_CODE_Leave(object sender, EventArgs e)
        {
            if (txtOption.Text == "A")
            {
                bool AgrDateChk;
                bool ExpDateChk;
                DateTime AgrDate;
                DateTime ExpDate;
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_CONTRA_CODE", txt_SP_CONTRA_CODE.Text);
                rsltCode = cmnDM.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "CONCODE_EXISTS", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0 && rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    AgrDateChk = DateTime.TryParse(rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_AGRE_DATE"].ToString(), out AgrDate);
                    ExpDateChk = DateTime.TryParse(rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_EXP_DATE"].ToString(), out ExpDate);

                    txt_SP_CONTRA_CODE.Text         = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_CODE"].ToString();
                    txt_SP_CONTRA_NAME.Text         = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_NAME"].ToString();
                    txt_SP_CONTRA_COMP_NAME.Text    = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_COMP_NAME"].ToString();
                    txt_SP_CONTRA_ADD1.Text         = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_ADD1"].ToString();
                    txt_SP_CONTRA_ADD2.Text         = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_ADD1"].ToString();
                    txt_SP_CONTRA_ADD3.Text         = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_ADD1"].ToString();
                    txt_SP_CONTRA_PHONE1.Text       = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_PHONE1"].ToString();
                    txt_SP_CONTRA_PHONE2.Text       = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_PHONE2"].ToString();

                     if (AgrDateChk == true)
                         txt_SP_CONTRA_AGRE_DATE.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_AGRE_DATE"].ToString());

                     if (ExpDateChk == true)
                         txt_SP_CONTRA_EXP_DATE.Value = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_EXP_DATE"].ToString());

                     txt_SP_CONTRA_NID.Text         = rsltCode.dstResult.Tables[0].Rows[0]["SP_CONTRA_NID"].ToString();

                    MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lbtnConCode.SkipValidationOnLeave = false;
                }
                else
                {
                    lbtnConCode.SkipValidationOnLeave = true;
                }
            }
        }

        /// <summary>
        /// VAlidate the Phone Number Both should not be same
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CONTRA_PHONE2_Validating(object sender, CancelEventArgs e)
        {
            if (txt_SP_CONTRA_PHONE1.Text != string.Empty && txt_SP_CONTRA_PHONE2.Text != string.Empty)
            {
                if (txt_SP_CONTRA_PHONE1.Text == txt_SP_CONTRA_PHONE2.Text)
                {
                    MessageBox.Show(" Both Phone Numbers Cannot Be Same", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// ExpDate should be less than Contract Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CONTRA_EXP_DATE_Validating(object sender, CancelEventArgs e)
        {
            if (DateTime.Compare(Convert.ToDateTime(txt_SP_CONTRA_AGRE_DATE.Value), Convert.ToDateTime(txt_SP_CONTRA_EXP_DATE.Value)) > 0)
            {
                MessageBox.Show(" Expiry Date Must Be Greater Then Agrement Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_SP_CONTRA_NID_Validating(object sender, CancelEventArgs e)
        {
            if (txtOption.Text == "M")
            {
                //proc_modify();
                base.Save();
            }
            else if (txtOption.Text == "A" && txt_SP_CONTRA_NAME.Text != null)
            {
                DialogResult dr = MessageBox.Show("Do You Want To Save The Record [Y]es [N]o", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    this.DoToolbarActions(this.pnlContractorCode.Controls, "Save");
                    txt_SP_CONTRA_EXP_DATE.Value = null;
                    txt_SP_CONTRA_AGRE_DATE.Value = null;
                    txtOption.Text = "A";
                    txt_SP_CONTRA_CODE.Select();
                    txt_SP_CONTRA_CODE.Focus();
                }
                else
                {
                    this.DoToolbarActions(this.pnlContractorCode.Controls, "Cancel");
                    txt_SP_CONTRA_AGRE_DATE.Value = null;
                    txt_SP_CONTRA_EXP_DATE.Value = null;
                    txtOption.Select();
                    txtOption.Focus();
                }
            }
        }

        /// <summary>
        /// Option TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOption_Validating(object sender, CancelEventArgs e)
        {
            if (txtOption.Text == "A")
                txt_W_OPTION_DIS.Text = "Add";
            else if (txtOption.Text == "M")
                txt_W_OPTION_DIS.Text = "Modify";
            else if (txtOption.Text == "V")
                txt_W_OPTION_DIS.Text = "View";
            else if (txtOption.Text == "D")
                txt_W_OPTION_DIS.Text = "Delete";
            else if (txtOption.Text == "Q")
                txt_W_OPTION_DIS.Text = "Query";

            txtDate.Text = this.Now().ToString("MM/dd/yyyy");
        }

        #endregion

        private void txt_SP_CONTRA_NID_KeyPress(object sender, KeyPressEventArgs e)
        {

            //if ( e.KeyChar >= 47 && e.KeyChar <= 58 )
            //{
             
            //    //e.Handled = false;
            //}
            //else
            //{
            //    MessageBox.Show("Field must be of form \"999 - 99 - 999999\" ");   
            //    //e.Handled = true;

            //}




        }
    }
}