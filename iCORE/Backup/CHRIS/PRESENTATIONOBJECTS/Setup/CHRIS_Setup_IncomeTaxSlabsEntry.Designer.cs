namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_IncomeTaxSlabsEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_IncomeTaxSlabsEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.PnlINCOME = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DgvIncome = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_AMT_FROM = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.SP_AMT_TO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_REBATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FREBATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_PERCEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FPERCEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_TAX_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FTAX_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_SURCHARGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FSURCHARGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_ADD_TAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_ADD_TAX_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAX_RED_PER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTAX_RED_PER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.PnlINCOME.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvIncome)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(635, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(671, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 517);
            this.panel1.Size = new System.Drawing.Size(671, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 86);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(654, 67);
            this.pnlHead.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(215, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "INCOME  TAX  SLABS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(215, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(235, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Set Up";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(558, 37);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(558, 12);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(515, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(507, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(120, 38);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(120, 12);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(44, 38);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(56, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PnlINCOME
            // 
            this.PnlINCOME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlINCOME.ConcurrentPanels = null;
            this.PnlINCOME.Controls.Add(this.label8);
            this.PnlINCOME.Controls.Add(this.label7);
            this.PnlINCOME.Controls.Add(this.label6);
            this.PnlINCOME.Controls.Add(this.label5);
            this.PnlINCOME.Controls.Add(this.label4);
            this.PnlINCOME.Controls.Add(this.label2);
            this.PnlINCOME.Controls.Add(this.label1);
            this.PnlINCOME.Controls.Add(this.DgvIncome);
            this.PnlINCOME.DataManager = null;
            this.PnlINCOME.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlINCOME.DependentPanels = null;
            this.PnlINCOME.DisableDependentLoad = false;
            this.PnlINCOME.EnableDelete = true;
            this.PnlINCOME.EnableInsert = true;
            this.PnlINCOME.EnableQuery = false;
            this.PnlINCOME.EnableUpdate = true;
            this.PnlINCOME.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.INCOMECommand";
            this.PnlINCOME.Location = new System.Drawing.Point(12, 151);
            this.PnlINCOME.MasterPanel = null;
            this.PnlINCOME.Name = "PnlINCOME";
            this.PnlINCOME.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlINCOME.Size = new System.Drawing.Size(654, 356);
            this.PnlINCOME.SPName = "CHRIS_SP_INCOME_MANAGER";
            this.PnlINCOME.TabIndex = 55;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(478, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 26);
            this.label8.TabIndex = 7;
            this.label8.Text = "Perks Fixed \r\n % Amount ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(545, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 26);
            this.label7.TabIndex = 6;
            this.label7.Text = "TAX REDUCTION % \r\n      REBATE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(398, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Surcharge % ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(295, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Fixed Tax Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(234, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tax % ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(125, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Standard Rebate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Amount";
            // 
            // DgvIncome
            // 
            this.DgvIncome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvIncome.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_AMT_FROM,
            this.SP_AMT_TO,
            this.SP_REBATE,
            this.SP_FREBATE,
            this.SP_PERCEN,
            this.SP_FPERCEN,
            this.SP_TAX_AMT,
            this.SP_FTAX_AMT,
            this.SP_SURCHARGE,
            this.SP_FSURCHARGE,
            this.SP_ADD_TAX,
            this.SP_ADD_TAX_AMT,
            this.TAX_RED_PER,
            this.FTAX_RED_PER,
            this.ID});
            this.DgvIncome.ColumnToHide = null;
            this.DgvIncome.ColumnWidth = null;
            this.DgvIncome.CustomEnabled = true;
            this.DgvIncome.DisplayColumnWrapper = null;
            this.DgvIncome.GridDefaultRow = 0;
            this.DgvIncome.Location = new System.Drawing.Point(3, 46);
            this.DgvIncome.Name = "DgvIncome";
            this.DgvIncome.ReadOnlyColumns = null;
            this.DgvIncome.RequiredColumns = null;
            this.DgvIncome.Size = new System.Drawing.Size(648, 305);
            this.DgvIncome.SkippingColumns = null;
            this.DgvIncome.TabIndex = 0;
            this.DgvIncome.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvIncome_RowEnter);
            this.DgvIncome.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DgvIncome_CellValidating);
            this.DgvIncome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DgvIncome_KeyPress);
            // 
            // SP_AMT_FROM
            // 
            this.SP_AMT_FROM.ActionLOV = "Lov_Amt";
            this.SP_AMT_FROM.ActionLOVExists = "Lov_Amt_Exists";
            this.SP_AMT_FROM.AttachParentEntity = false;
            this.SP_AMT_FROM.DataPropertyName = "SP_AMT_FROM";
            this.SP_AMT_FROM.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.INCOMECommand";
            this.SP_AMT_FROM.HeaderText = "From";
            this.SP_AMT_FROM.LookUpTitle = null;
            this.SP_AMT_FROM.LOVFieldMapping = "SP_AMT_FROM";
            this.SP_AMT_FROM.MaxInputLength = 10;
            this.SP_AMT_FROM.Name = "SP_AMT_FROM";
            this.SP_AMT_FROM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_AMT_FROM.SearchColumn = "SP_AMT_FROM";
            this.SP_AMT_FROM.SkipValidationOnLeave = false;
            this.SP_AMT_FROM.SpName = "CHRIS_SP_INCOME_MANAGER";
            this.SP_AMT_FROM.Width = 50;
            // 
            // SP_AMT_TO
            // 
            this.SP_AMT_TO.DataPropertyName = "SP_AMT_TO";
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SP_AMT_TO.DefaultCellStyle = dataGridViewCellStyle1;
            this.SP_AMT_TO.HeaderText = "To";
            this.SP_AMT_TO.MaxInputLength = 10;
            this.SP_AMT_TO.Name = "SP_AMT_TO";
            this.SP_AMT_TO.Width = 50;
            // 
            // SP_REBATE
            // 
            this.SP_REBATE.DataPropertyName = "SP_REBATE";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.SP_REBATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.SP_REBATE.HeaderText = "Male";
            this.SP_REBATE.MaxInputLength = 8;
            this.SP_REBATE.Name = "SP_REBATE";
            this.SP_REBATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.SP_REBATE.Width = 50;
            // 
            // SP_FREBATE
            // 
            this.SP_FREBATE.DataPropertyName = "SP_FREBATE";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.SP_FREBATE.DefaultCellStyle = dataGridViewCellStyle3;
            this.SP_FREBATE.HeaderText = "Female";
            this.SP_FREBATE.MaxInputLength = 8;
            this.SP_FREBATE.Name = "SP_FREBATE";
            this.SP_FREBATE.Width = 50;
            // 
            // SP_PERCEN
            // 
            this.SP_PERCEN.DataPropertyName = "SP_PERCEN";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.SP_PERCEN.DefaultCellStyle = dataGridViewCellStyle4;
            this.SP_PERCEN.HeaderText = "Male";
            this.SP_PERCEN.MaxInputLength = 6;
            this.SP_PERCEN.Name = "SP_PERCEN";
            this.SP_PERCEN.Width = 50;
            // 
            // SP_FPERCEN
            // 
            this.SP_FPERCEN.DataPropertyName = "SP_FPERCEN";
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.SP_FPERCEN.DefaultCellStyle = dataGridViewCellStyle5;
            this.SP_FPERCEN.HeaderText = "Female";
            this.SP_FPERCEN.MaxInputLength = 6;
            this.SP_FPERCEN.Name = "SP_FPERCEN";
            this.SP_FPERCEN.Width = 50;
            // 
            // SP_TAX_AMT
            // 
            this.SP_TAX_AMT.DataPropertyName = "SP_TAX_AMT";
            this.SP_TAX_AMT.HeaderText = "Male";
            this.SP_TAX_AMT.MaxInputLength = 8;
            this.SP_TAX_AMT.Name = "SP_TAX_AMT";
            this.SP_TAX_AMT.Width = 55;
            // 
            // SP_FTAX_AMT
            // 
            this.SP_FTAX_AMT.DataPropertyName = "SP_FTAX_AMT";
            this.SP_FTAX_AMT.HeaderText = "Female";
            this.SP_FTAX_AMT.MaxInputLength = 8;
            this.SP_FTAX_AMT.Name = "SP_FTAX_AMT";
            this.SP_FTAX_AMT.Width = 55;
            // 
            // SP_SURCHARGE
            // 
            this.SP_SURCHARGE.DataPropertyName = "SP_SURCHARGE";
            this.SP_SURCHARGE.HeaderText = "Male";
            this.SP_SURCHARGE.MaxInputLength = 3;
            this.SP_SURCHARGE.Name = "SP_SURCHARGE";
            this.SP_SURCHARGE.Width = 50;
            // 
            // SP_FSURCHARGE
            // 
            this.SP_FSURCHARGE.DataPropertyName = "SP_FSURCHARGE";
            this.SP_FSURCHARGE.HeaderText = "Female";
            this.SP_FSURCHARGE.MaxInputLength = 3;
            this.SP_FSURCHARGE.Name = "SP_FSURCHARGE";
            this.SP_FSURCHARGE.Width = 50;
            // 
            // SP_ADD_TAX
            // 
            this.SP_ADD_TAX.DataPropertyName = "SP_ADD_TAX";
            this.SP_ADD_TAX.HeaderText = "";
            this.SP_ADD_TAX.MaxInputLength = 8;
            this.SP_ADD_TAX.Name = "SP_ADD_TAX";
            this.SP_ADD_TAX.Width = 50;
            // 
            // SP_ADD_TAX_AMT
            // 
            this.SP_ADD_TAX_AMT.DataPropertyName = "SP_ADD_TAX_AMT";
            this.SP_ADD_TAX_AMT.HeaderText = "";
            this.SP_ADD_TAX_AMT.MaxInputLength = 8;
            this.SP_ADD_TAX_AMT.Name = "SP_ADD_TAX_AMT";
            this.SP_ADD_TAX_AMT.Width = 50;
            // 
            // TAX_RED_PER
            // 
            this.TAX_RED_PER.DataPropertyName = "TAX_RED_PER";
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.TAX_RED_PER.DefaultCellStyle = dataGridViewCellStyle6;
            this.TAX_RED_PER.HeaderText = "Male";
            this.TAX_RED_PER.MaxInputLength = 7;
            this.TAX_RED_PER.Name = "TAX_RED_PER";
            this.TAX_RED_PER.Width = 50;
            // 
            // FTAX_RED_PER
            // 
            this.FTAX_RED_PER.DataPropertyName = "FTAX_RED_PER";
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.FTAX_RED_PER.DefaultCellStyle = dataGridViewCellStyle7;
            this.FTAX_RED_PER.HeaderText = "Female";
            this.FTAX_RED_PER.MaxInputLength = 7;
            this.FTAX_RED_PER.Name = "FTAX_RED_PER";
            this.FTAX_RED_PER.Width = 52;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(427, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "UserName : ";
            // 
            // CHRIS_Setup_IncomeTaxSlabsEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 577);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.PnlINCOME);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "PnlINCOME";
            this.Name = "CHRIS_Setup_IncomeTaxSlabsEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Income Tax Slabs Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.PnlINCOME, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.PnlINCOME.ResumeLayout(false);
            this.PnlINCOME.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvIncome)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlINCOME;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DgvIncome;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_AMT_FROM;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_AMT_TO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_REBATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FREBATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_PERCEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FPERCEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_TAX_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FTAX_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_SURCHARGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FSURCHARGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_ADD_TAX;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_ADD_TAX_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn TAX_RED_PER;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTAX_RED_PER;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}