namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_GroupLifeHospitalEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_GroupLifeHospitalEntry));
            this.PnlIncGrp = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbLevel = new CrplControlLibrary.LookupButton(this.components);
            this.lbDesig = new CrplControlLibrary.LookupButton(this.components);
            this.lbsatfftype = new CrplControlLibrary.LookupButton(this.components);
            this.lbTypeCod = new CrplControlLibrary.LookupButton(this.components);
            this.txtnameIncGrp2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtRenewal = new CrplControlLibrary.SLDatePicker(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHospitalCovg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLifeCovg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMultiply = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPolicyno = new CrplControlLibrary.SLTextBox(this.components);
            this.txtnameIncGrp1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtdesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtoffical = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLifeIns = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox4 = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlIncGrp.SuspendLayout();
            this.pnlHead.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(561, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(597, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 505);
            this.panel1.Size = new System.Drawing.Size(597, 65);
            // 
            // PnlIncGrp
            // 
            this.PnlIncGrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlIncGrp.ConcurrentPanels = null;
            this.PnlIncGrp.Controls.Add(this.lbLevel);
            this.PnlIncGrp.Controls.Add(this.lbDesig);
            this.PnlIncGrp.Controls.Add(this.lbsatfftype);
            this.PnlIncGrp.Controls.Add(this.lbTypeCod);
            this.PnlIncGrp.Controls.Add(this.txtnameIncGrp2);
            this.PnlIncGrp.Controls.Add(this.label13);
            this.PnlIncGrp.Controls.Add(this.label12);
            this.PnlIncGrp.Controls.Add(this.dtRenewal);
            this.PnlIncGrp.Controls.Add(this.label11);
            this.PnlIncGrp.Controls.Add(this.label10);
            this.PnlIncGrp.Controls.Add(this.label9);
            this.PnlIncGrp.Controls.Add(this.label8);
            this.PnlIncGrp.Controls.Add(this.label7);
            this.PnlIncGrp.Controls.Add(this.label6);
            this.PnlIncGrp.Controls.Add(this.label5);
            this.PnlIncGrp.Controls.Add(this.label4);
            this.PnlIncGrp.Controls.Add(this.label2);
            this.PnlIncGrp.Controls.Add(this.label1);
            this.PnlIncGrp.Controls.Add(this.txtHospitalCovg);
            this.PnlIncGrp.Controls.Add(this.txtLifeCovg);
            this.PnlIncGrp.Controls.Add(this.txtMultiply);
            this.PnlIncGrp.Controls.Add(this.txtPolicyno);
            this.PnlIncGrp.Controls.Add(this.txtnameIncGrp1);
            this.PnlIncGrp.Controls.Add(this.txtLevel);
            this.PnlIncGrp.Controls.Add(this.txtdesignation);
            this.PnlIncGrp.Controls.Add(this.txtoffical);
            this.PnlIncGrp.Controls.Add(this.txtLifeIns);
            this.PnlIncGrp.DataManager = null;
            this.PnlIncGrp.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlIncGrp.DependentPanels = null;
            this.PnlIncGrp.DisableDependentLoad = false;
            this.PnlIncGrp.EnableDelete = true;
            this.PnlIncGrp.EnableInsert = true;
            this.PnlIncGrp.EnableQuery = false;
            this.PnlIncGrp.EnableUpdate = true;
            this.PnlIncGrp.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GroupINSCCommand";
            this.PnlIncGrp.Location = new System.Drawing.Point(12, 153);
            this.PnlIncGrp.MasterPanel = null;
            this.PnlIncGrp.Name = "PnlIncGrp";
            this.PnlIncGrp.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlIncGrp.Size = new System.Drawing.Size(572, 345);
            this.PnlIncGrp.SPName = "CHRIS_SP_GROUP_INSC_MANAGER";
            this.PnlIncGrp.TabIndex = 10;
            // 
            // lbLevel
            // 
            this.lbLevel.ActionLOVExists = "Level_In_Exists";
            this.lbLevel.ActionType = "Level_In";
            this.lbLevel.ConditionalFields = "";
            this.lbLevel.CustomEnabled = true;
            this.lbLevel.DataFieldMapping = "";
            this.lbLevel.DependentLovControls = "";
            this.lbLevel.HiddenColumns = "";
            this.lbLevel.Image = ((System.Drawing.Image)(resources.GetObject("lbLevel.Image")));
            this.lbLevel.LoadDependentEntities = false;
            this.lbLevel.Location = new System.Drawing.Point(326, 89);
            this.lbLevel.LookUpTitle = null;
            this.lbLevel.Name = "lbLevel";
            this.lbLevel.Size = new System.Drawing.Size(26, 21);
            this.lbLevel.SkipValidationOnLeave = false;
            this.lbLevel.SPName = "CHRIS_SP_GROUP_INSC_MANAGER";
            this.lbLevel.TabIndex = 27;
            this.lbLevel.TabStop = false;
            this.lbLevel.UseVisualStyleBackColor = true;
            this.lbLevel.Visible = false;
            this.lbLevel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbLevel_MouseDown);
            // 
            // lbDesig
            // 
            this.lbDesig.ActionLOVExists = "sp_desig_Exists";
            this.lbDesig.ActionType = "sp_desig";
            this.lbDesig.ConditionalFields = "";
            this.lbDesig.CustomEnabled = true;
            this.lbDesig.DataFieldMapping = "";
            this.lbDesig.DependentLovControls = "";
            this.lbDesig.HiddenColumns = "";
            this.lbDesig.Image = ((System.Drawing.Image)(resources.GetObject("lbDesig.Image")));
            this.lbDesig.LoadDependentEntities = false;
            this.lbDesig.Location = new System.Drawing.Point(326, 63);
            this.lbDesig.LookUpTitle = null;
            this.lbDesig.Name = "lbDesig";
            this.lbDesig.Size = new System.Drawing.Size(26, 21);
            this.lbDesig.SkipValidationOnLeave = false;
            this.lbDesig.SPName = "CHRIS_SP_GROUP_INSC_MANAGER";
            this.lbDesig.TabIndex = 26;
            this.lbDesig.TabStop = false;
            this.lbDesig.UseVisualStyleBackColor = true;
            this.lbDesig.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbDesig_MouseDown);
            // 
            // lbsatfftype
            // 
            this.lbsatfftype.ActionLOVExists = "StaffType_Exists";
            this.lbsatfftype.ActionType = "StaffType";
            this.lbsatfftype.ConditionalFields = "";
            this.lbsatfftype.CustomEnabled = true;
            this.lbsatfftype.DataFieldMapping = "";
            this.lbsatfftype.DependentLovControls = "";
            this.lbsatfftype.HiddenColumns = "";
            this.lbsatfftype.Image = ((System.Drawing.Image)(resources.GetObject("lbsatfftype.Image")));
            this.lbsatfftype.LoadDependentEntities = false;
            this.lbsatfftype.Location = new System.Drawing.Point(326, 37);
            this.lbsatfftype.LookUpTitle = null;
            this.lbsatfftype.Name = "lbsatfftype";
            this.lbsatfftype.Size = new System.Drawing.Size(26, 21);
            this.lbsatfftype.SkipValidationOnLeave = false;
            this.lbsatfftype.SPName = "CHRIS_SP_GROUP_INSC_MANAGER";
            this.lbsatfftype.TabIndex = 25;
            this.lbsatfftype.TabStop = false;
            this.lbsatfftype.UseVisualStyleBackColor = true;
            this.lbsatfftype.Visible = false;
            this.lbsatfftype.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbsatfftype_MouseDown);
            // 
            // lbTypeCod
            // 
            this.lbTypeCod.ActionLOVExists = "Type_code_exists";
            this.lbTypeCod.ActionType = "Type_code";
            this.lbTypeCod.ConditionalFields = "";
            this.lbTypeCod.CustomEnabled = true;
            this.lbTypeCod.DataFieldMapping = "";
            this.lbTypeCod.DependentLovControls = "";
            this.lbTypeCod.HiddenColumns = "";
            this.lbTypeCod.Image = ((System.Drawing.Image)(resources.GetObject("lbTypeCod.Image")));
            this.lbTypeCod.LoadDependentEntities = false;
            this.lbTypeCod.Location = new System.Drawing.Point(326, 10);
            this.lbTypeCod.LookUpTitle = null;
            this.lbTypeCod.Name = "lbTypeCod";
            this.lbTypeCod.Size = new System.Drawing.Size(26, 21);
            this.lbTypeCod.SkipValidationOnLeave = false;
            this.lbTypeCod.SPName = "CHRIS_SP_GROUP_INSC_MANAGER";
            this.lbTypeCod.TabIndex = 24;
            this.lbTypeCod.TabStop = false;
            this.lbTypeCod.UseVisualStyleBackColor = true;
            this.lbTypeCod.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbTypeCod_MouseDown);
            // 
            // txtnameIncGrp2
            // 
            this.txtnameIncGrp2.AllowSpace = true;
            this.txtnameIncGrp2.AssociatedLookUpName = "";
            this.txtnameIncGrp2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnameIncGrp2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnameIncGrp2.ContinuationTextBox = null;
            this.txtnameIncGrp2.CustomEnabled = true;
            this.txtnameIncGrp2.DataFieldMapping = "SP_NAME_2";
            this.txtnameIncGrp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnameIncGrp2.GetRecordsOnUpDownKeys = false;
            this.txtnameIncGrp2.IsDate = false;
            this.txtnameIncGrp2.Location = new System.Drawing.Point(280, 143);
            this.txtnameIncGrp2.MaxLength = 20;
            this.txtnameIncGrp2.Name = "txtnameIncGrp2";
            this.txtnameIncGrp2.NumberFormat = "###,###,##0.00";
            this.txtnameIncGrp2.Postfix = "";
            this.txtnameIncGrp2.Prefix = "";
            this.txtnameIncGrp2.Size = new System.Drawing.Size(148, 20);
            this.txtnameIncGrp2.SkipValidation = false;
            this.txtnameIncGrp2.TabIndex = 6;
            this.txtnameIncGrp2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(350, 284);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 13);
            this.label13.TabIndex = 22;
            this.label13.Tag = "";
            this.label13.Text = "(For Clerical Ins. Only)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(323, 240);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(173, 13);
            this.label12.TabIndex = 21;
            this.label12.Tag = "";
            this.label12.Text = "( For Officers Life lnsurance )";
            // 
            // dtRenewal
            // 
            this.dtRenewal.CustomEnabled = true;
            this.dtRenewal.CustomFormat = "dd/MM/yyyy";
            this.dtRenewal.DataFieldMapping = "SP_RENEWAL";
            this.dtRenewal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRenewal.HasChanges = true;
            this.dtRenewal.IsRequired = true;
            this.dtRenewal.Location = new System.Drawing.Point(280, 194);
            this.dtRenewal.Name = "dtRenewal";
            this.dtRenewal.NullValue = " ";
            this.dtRenewal.Size = new System.Drawing.Size(89, 20);
            this.dtRenewal.TabIndex = 8;
            this.dtRenewal.Value = new System.DateTime(2011, 1, 24, 0, 0, 0, 0);
            this.dtRenewal.Validating += new System.ComponentModel.CancelEventHandler(this.dtRenewal_Validating);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(83, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(194, 13);
            this.label11.TabIndex = 19;
            this.label11.Tag = "";
            this.label11.Text = "10) Hospitalization Coverage     :";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(83, 284);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(193, 13);
            this.label10.TabIndex = 18;
            this.label10.Tag = "";
            this.label10.Text = "9) Life Coverage                      :";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(55, 240);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(193, 13);
            this.label9.TabIndex = 17;
            this.label9.Tag = "";
            this.label9.Text = "8) Multiply Package By             :";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(83, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(194, 13);
            this.label8.TabIndex = 16;
            this.label8.Tag = "";
            this.label8.Text = "7) Renewal Date                      :";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(83, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(192, 13);
            this.label7.TabIndex = 15;
            this.label7.Tag = "";
            this.label7.Text = "6) Policy No.                           :";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(83, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(194, 13);
            this.label6.TabIndex = 14;
            this.label6.Tag = "";
            this.label6.Text = "5) Name Of Insurance Company :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(83, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(193, 13);
            this.label5.TabIndex = 13;
            this.label5.Tag = "";
            this.label5.Text = "4) Level                                  :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(83, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 13);
            this.label4.TabIndex = 12;
            this.label4.Tag = "";
            this.label4.Text = "3) Designation                         :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(83, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 13);
            this.label2.TabIndex = 11;
            this.label2.Tag = "";
            this.label2.Text = "2)  Officials/Unionized             :";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(83, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 13);
            this.label1.TabIndex = 10;
            this.label1.Tag = "";
            this.label1.Text = "1) Life [I]nsc / Hospitalization   :";
            // 
            // txtHospitalCovg
            // 
            this.txtHospitalCovg.AllowSpace = true;
            this.txtHospitalCovg.AssociatedLookUpName = "";
            this.txtHospitalCovg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHospitalCovg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHospitalCovg.ContinuationTextBox = null;
            this.txtHospitalCovg.CustomEnabled = true;
            this.txtHospitalCovg.DataFieldMapping = "SP_H_COVERAGE";
            this.txtHospitalCovg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHospitalCovg.GetRecordsOnUpDownKeys = false;
            this.txtHospitalCovg.IsDate = false;
            this.txtHospitalCovg.Location = new System.Drawing.Point(280, 306);
            this.txtHospitalCovg.MaxLength = 7;
            this.txtHospitalCovg.Name = "txtHospitalCovg";
            this.txtHospitalCovg.NumberFormat = "###,###,##0.00";
            this.txtHospitalCovg.Postfix = "";
            this.txtHospitalCovg.Prefix = "";
            this.txtHospitalCovg.Size = new System.Drawing.Size(63, 20);
            this.txtHospitalCovg.SkipValidation = false;
            this.txtHospitalCovg.TabIndex = 11;
            this.txtHospitalCovg.TextType = CrplControlLibrary.TextType.String;
            this.txtHospitalCovg.Leave += new System.EventHandler(this.txtHospitalCovg_Leave);
            this.txtHospitalCovg.Validating += new System.ComponentModel.CancelEventHandler(this.txtHospitalCovg_Validating);
            // 
            // txtLifeCovg
            // 
            this.txtLifeCovg.AllowSpace = true;
            this.txtLifeCovg.AssociatedLookUpName = "";
            this.txtLifeCovg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLifeCovg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLifeCovg.ContinuationTextBox = null;
            this.txtLifeCovg.CustomEnabled = true;
            this.txtLifeCovg.DataFieldMapping = "SP_L_COVERAGE";
            this.txtLifeCovg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLifeCovg.GetRecordsOnUpDownKeys = false;
            this.txtLifeCovg.IsDate = false;
            this.txtLifeCovg.Location = new System.Drawing.Point(280, 280);
            this.txtLifeCovg.MaxLength = 7;
            this.txtLifeCovg.Name = "txtLifeCovg";
            this.txtLifeCovg.NumberFormat = "###,###,##0.00";
            this.txtLifeCovg.Postfix = "";
            this.txtLifeCovg.Prefix = "";
            this.txtLifeCovg.Size = new System.Drawing.Size(63, 20);
            this.txtLifeCovg.SkipValidation = false;
            this.txtLifeCovg.TabIndex = 10;
            this.txtLifeCovg.TextType = CrplControlLibrary.TextType.String;
            this.txtLifeCovg.Validating += new System.ComponentModel.CancelEventHandler(this.txtLifeCovg_Validating);
            // 
            // txtMultiply
            // 
            this.txtMultiply.AllowSpace = true;
            this.txtMultiply.AssociatedLookUpName = "";
            this.txtMultiply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMultiply.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMultiply.ContinuationTextBox = null;
            this.txtMultiply.CustomEnabled = true;
            this.txtMultiply.DataFieldMapping = "SP_MULT_PACK";
            this.txtMultiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMultiply.GetRecordsOnUpDownKeys = false;
            this.txtMultiply.IsDate = false;
            this.txtMultiply.Location = new System.Drawing.Point(280, 236);
            this.txtMultiply.MaxLength = 1;
            this.txtMultiply.Name = "txtMultiply";
            this.txtMultiply.NumberFormat = "###,###,##0.00";
            this.txtMultiply.Postfix = "";
            this.txtMultiply.Prefix = "";
            this.txtMultiply.Size = new System.Drawing.Size(37, 20);
            this.txtMultiply.SkipValidation = false;
            this.txtMultiply.TabIndex = 9;
            this.txtMultiply.TextType = CrplControlLibrary.TextType.Integer;
            this.txtMultiply.Leave += new System.EventHandler(this.txtMultiply_Leave);
            this.txtMultiply.Validating += new System.ComponentModel.CancelEventHandler(this.txtMultiply_Validating);
            // 
            // txtPolicyno
            // 
            this.txtPolicyno.AllowSpace = true;
            this.txtPolicyno.AssociatedLookUpName = "";
            this.txtPolicyno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPolicyno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPolicyno.ContinuationTextBox = null;
            this.txtPolicyno.CustomEnabled = true;
            this.txtPolicyno.DataFieldMapping = "SP_POLICY_NO";
            this.txtPolicyno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPolicyno.GetRecordsOnUpDownKeys = false;
            this.txtPolicyno.IsDate = false;
            this.txtPolicyno.IsRequired = true;
            this.txtPolicyno.Location = new System.Drawing.Point(280, 169);
            this.txtPolicyno.MaxLength = 20;
            this.txtPolicyno.Name = "txtPolicyno";
            this.txtPolicyno.NumberFormat = "###,###,##0.00";
            this.txtPolicyno.Postfix = "";
            this.txtPolicyno.Prefix = "";
            this.txtPolicyno.Size = new System.Drawing.Size(148, 20);
            this.txtPolicyno.SkipValidation = false;
            this.txtPolicyno.TabIndex = 7;
            this.txtPolicyno.TextType = CrplControlLibrary.TextType.String;
            this.txtPolicyno.Validating += new System.ComponentModel.CancelEventHandler(this.txtPolicyno_Validating);
            // 
            // txtnameIncGrp1
            // 
            this.txtnameIncGrp1.AllowSpace = true;
            this.txtnameIncGrp1.AssociatedLookUpName = "";
            this.txtnameIncGrp1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnameIncGrp1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnameIncGrp1.ContinuationTextBox = null;
            this.txtnameIncGrp1.CustomEnabled = true;
            this.txtnameIncGrp1.DataFieldMapping = "SP_NAME_1";
            this.txtnameIncGrp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnameIncGrp1.GetRecordsOnUpDownKeys = false;
            this.txtnameIncGrp1.IsDate = false;
            this.txtnameIncGrp1.IsRequired = true;
            this.txtnameIncGrp1.Location = new System.Drawing.Point(280, 117);
            this.txtnameIncGrp1.MaxLength = 20;
            this.txtnameIncGrp1.Name = "txtnameIncGrp1";
            this.txtnameIncGrp1.NumberFormat = "###,###,##0.00";
            this.txtnameIncGrp1.Postfix = "";
            this.txtnameIncGrp1.Prefix = "";
            this.txtnameIncGrp1.Size = new System.Drawing.Size(148, 20);
            this.txtnameIncGrp1.SkipValidation = false;
            this.txtnameIncGrp1.TabIndex = 5;
            this.txtnameIncGrp1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "lbLevel";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "SP_LEVEL_IN";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(280, 91);
            this.txtLevel.MaxLength = 3;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.Size = new System.Drawing.Size(37, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 4;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            this.txtLevel.Leave += new System.EventHandler(this.txtLevel_Leave);
            this.txtLevel.Validating += new System.ComponentModel.CancelEventHandler(this.txtLevel_Validating);
            // 
            // txtdesignation
            // 
            this.txtdesignation.AllowSpace = true;
            this.txtdesignation.AssociatedLookUpName = "lbDesig";
            this.txtdesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdesignation.ContinuationTextBox = null;
            this.txtdesignation.CustomEnabled = true;
            this.txtdesignation.DataFieldMapping = "SP_DESIG_IN";
            this.txtdesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdesignation.GetRecordsOnUpDownKeys = false;
            this.txtdesignation.IsDate = false;
            this.txtdesignation.Location = new System.Drawing.Point(280, 65);
            this.txtdesignation.MaxLength = 3;
            this.txtdesignation.Name = "txtdesignation";
            this.txtdesignation.NumberFormat = "###,###,##0.00";
            this.txtdesignation.Postfix = "";
            this.txtdesignation.Prefix = "";
            this.txtdesignation.Size = new System.Drawing.Size(37, 20);
            this.txtdesignation.SkipValidation = false;
            this.txtdesignation.TabIndex = 3;
            this.txtdesignation.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtdesignation, "Invalid Entry... Press [F9] To Display The List");
            this.txtdesignation.Leave += new System.EventHandler(this.txtdesignation_Leave);
            this.txtdesignation.Validating += new System.ComponentModel.CancelEventHandler(this.txtdesignation_Validating);
            // 
            // txtoffical
            // 
            this.txtoffical.AllowSpace = true;
            this.txtoffical.AssociatedLookUpName = "lbsatfftype";
            this.txtoffical.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoffical.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtoffical.ContinuationTextBox = null;
            this.txtoffical.CustomEnabled = true;
            this.txtoffical.DataFieldMapping = "SP_STAFF_TYPE";
            this.txtoffical.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoffical.GetRecordsOnUpDownKeys = false;
            this.txtoffical.IsDate = false;
            this.txtoffical.IsRequired = true;
            this.txtoffical.Location = new System.Drawing.Point(280, 39);
            this.txtoffical.MaxLength = 1;
            this.txtoffical.Name = "txtoffical";
            this.txtoffical.NumberFormat = "###,###,##0.00";
            this.txtoffical.Postfix = "";
            this.txtoffical.Prefix = "";
            this.txtoffical.Size = new System.Drawing.Size(28, 20);
            this.txtoffical.SkipValidation = false;
            this.txtoffical.TabIndex = 2;
            this.txtoffical.TextType = CrplControlLibrary.TextType.String;
            this.txtoffical.Validating += new System.ComponentModel.CancelEventHandler(this.txtoffical_Validating);
            // 
            // txtLifeIns
            // 
            this.txtLifeIns.AllowSpace = true;
            this.txtLifeIns.AssociatedLookUpName = "lbTypeCod";
            this.txtLifeIns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLifeIns.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLifeIns.ContinuationTextBox = null;
            this.txtLifeIns.CustomEnabled = true;
            this.txtLifeIns.DataFieldMapping = "SP_TYPE_CODE";
            this.txtLifeIns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLifeIns.GetRecordsOnUpDownKeys = false;
            this.txtLifeIns.IsDate = false;
            this.txtLifeIns.IsRequired = true;
            this.txtLifeIns.Location = new System.Drawing.Point(280, 13);
            this.txtLifeIns.MaxLength = 1;
            this.txtLifeIns.Name = "txtLifeIns";
            this.txtLifeIns.NumberFormat = "###,###,##0.00";
            this.txtLifeIns.Postfix = "";
            this.txtLifeIns.Prefix = "";
            this.txtLifeIns.Size = new System.Drawing.Size(28, 20);
            this.txtLifeIns.SkipValidation = true;
            this.txtLifeIns.TabIndex = 1;
            this.txtLifeIns.TextType = CrplControlLibrary.TextType.String;
            this.txtLifeIns.Validating += new System.ComponentModel.CancelEventHandler(this.txtLifeIns_Validating);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.label14);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label15);
            this.pnlHead.Controls.Add(this.label16);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label17);
            this.pnlHead.Controls.Add(this.label18);
            this.pnlHead.Location = new System.Drawing.Point(12, 88);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(573, 61);
            this.pnlHead.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(169, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(248, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Group Life / Group Hospitalization";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(169, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(235, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Set Up";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(460, 40);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(460, 14);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(417, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(409, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 42);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 16);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(15, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(362, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 13);
            this.label19.TabIndex = 55;
            this.label19.Text = "UserName :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.slTextBox1);
            this.panel2.Controls.Add(this.slTextBox2);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.slTextBox3);
            this.panel2.Controls.Add(this.slTextBox4);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Location = new System.Drawing.Point(12, 88);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(573, 61);
            this.panel2.TabIndex = 54;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(169, 35);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(248, 18);
            this.label20.TabIndex = 20;
            this.label20.Text = "Group Life / Group Hospitalization";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(169, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(235, 20);
            this.label21.TabIndex = 19;
            this.label21.Text = "Set Up";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(460, 35);
            this.slTextBox1.MaxLength = 10;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(80, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 18;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(460, 9);
            this.slTextBox2.MaxLength = 6;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.ReadOnly = true;
            this.slTextBox2.Size = new System.Drawing.Size(80, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 17;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(417, 37);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 15);
            this.label22.TabIndex = 16;
            this.label22.Text = "Date:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(409, 11);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 15);
            this.label23.TabIndex = 15;
            this.label23.Text = "Option:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "";
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.Location = new System.Drawing.Point(79, 37);
            this.slTextBox3.MaxLength = 10;
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.ReadOnly = true;
            this.slTextBox3.Size = new System.Drawing.Size(80, 20);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 14;
            this.slTextBox3.TabStop = false;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox3.Visible = false;
            // 
            // slTextBox4
            // 
            this.slTextBox4.AllowSpace = true;
            this.slTextBox4.AssociatedLookUpName = "";
            this.slTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox4.ContinuationTextBox = null;
            this.slTextBox4.CustomEnabled = true;
            this.slTextBox4.DataFieldMapping = "";
            this.slTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox4.GetRecordsOnUpDownKeys = false;
            this.slTextBox4.IsDate = false;
            this.slTextBox4.Location = new System.Drawing.Point(79, 11);
            this.slTextBox4.MaxLength = 10;
            this.slTextBox4.Name = "slTextBox4";
            this.slTextBox4.NumberFormat = "###,###,##0.00";
            this.slTextBox4.Postfix = "";
            this.slTextBox4.Prefix = "";
            this.slTextBox4.ReadOnly = true;
            this.slTextBox4.Size = new System.Drawing.Size(80, 20);
            this.slTextBox4.SkipValidation = false;
            this.slTextBox4.TabIndex = 13;
            this.slTextBox4.TabStop = false;
            this.slTextBox4.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox4.Visible = false;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(3, 37);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 20);
            this.label24.TabIndex = 2;
            this.label24.Text = "Location:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label24.Visible = false;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(15, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(58, 20);
            this.label25.TabIndex = 1;
            this.label25.Text = "User:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label25.Visible = false;
            // 
            // CHRIS_Setup_GroupLifeHospitalEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(597, 570);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlIncGrp);
            this.CurrentPanelBlock = "PnlIncGrp";
            this.Name = "CHRIS_Setup_GroupLifeHospitalEntry";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF7Option = true;
            this.ShowF8Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Group Life Hospital Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlIncGrp, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlIncGrp.ResumeLayout(false);
            this.PnlIncGrp.PerformLayout();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlIncGrp;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtHospitalCovg;
        private CrplControlLibrary.SLTextBox txtLifeCovg;
        private CrplControlLibrary.SLTextBox txtMultiply;
        private CrplControlLibrary.SLTextBox txtPolicyno;
        private CrplControlLibrary.SLTextBox txtnameIncGrp1;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtdesignation;
        private CrplControlLibrary.SLTextBox txtoffical;
        private CrplControlLibrary.SLTextBox txtLifeIns;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLDatePicker dtRenewal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtnameIncGrp2;
        private CrplControlLibrary.LookupButton lbTypeCod;
        private CrplControlLibrary.LookupButton lbLevel;
        private CrplControlLibrary.LookupButton lbDesig;
        private CrplControlLibrary.LookupButton lbsatfftype;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLTextBox slTextBox4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
    }
}