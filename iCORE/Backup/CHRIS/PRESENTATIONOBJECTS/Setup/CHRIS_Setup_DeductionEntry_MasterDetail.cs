using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_DeductionEntry_MasterDetail : ChrisMasterDetailForm
    {
        #region --Constructor--
        public CHRIS_Setup_DeductionEntry_MasterDetail()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_DeductionEntry_MasterDetail(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(this.pnlTblDedDetail);

            this.pnlDeductionMain.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDeductionMain);

        }
        #endregion

        #region --Method--
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                txtOption.Visible = false;
                tbtAdd.Visible = false;
                tbtCancel.Visible = false;
                tbtClose.Visible = false;
                tbtDelete.Visible = false;
                tbtEdit.Visible = false;
                tbtList.Visible = false;
                tbtSave.Visible = false;
                this.llbusername.Text = this.UserName;
                //txtOption.Visible           = false;
                txt_SP_VALID_FROM_D.Value = null;
                txt_SP_VALID_TO_D.Value = null;
                Font newFontStyle = new Font(dgvDedDetail.Font, FontStyle.Bold);
                dgvDedDetail.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        #endregion

        #region --Events
       
        #endregion

        private void CHRIS_Setup_DeductionEntry_MasterDetail_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            txt_SP_ACCOUNT_NO_D.ReadOnly = true;
            txt_SP_DED_AMOUNT.ReadOnly  = true;
            txt_SP_DED_CODE.ReadOnly    = true;
            txt_SP_DED_DESC.ReadOnly    = true;
            //txt_SP_VALID_FROM_D.Enabled = false;
            //txt_SP_VALID_TO_D.Enabled   = false;

            //txt_SP_ACCOUNT_NO_D.Enabled = false;
            //txt_SP_DED_AMOUNT.Enabled   = false;
            //txt_SP_DED_CODE.Enabled     = false;
            //txt_SP_DED_DESC.Enabled     = false;

            //dgvDedDetail.Rows[0].Cells[0].DataGridView.Select();
            //dgvDedDetail.Rows[0].Cells[0].DataGridView.Focus();

            //dgvDedDetail.Select();
            //dgvDedDetail.Focus();
        }

        
    }
}