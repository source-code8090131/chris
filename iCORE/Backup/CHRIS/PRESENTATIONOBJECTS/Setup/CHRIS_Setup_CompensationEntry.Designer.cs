namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_CompensationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_CompensationEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlTblCompensationInfo = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvComp = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Lvl_Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grp_Head = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Min_Sal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mid_Sal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Max_Sal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Budget_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Annual_Dep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Petrol_Ltr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Petrol_Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Insurance_Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Maintenance_Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Driver_Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupHead_Allowance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_lvl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_GrpHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlButton = new System.Windows.Forms.Panel();
            this.btnGrp = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnScroll_Down = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnScroll_Up = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblCompensationInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComp)).BeginInit();
            this.pnlButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(630, 0);
            this.txtOption.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(666, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 360);
            this.panel1.Size = new System.Drawing.Size(666, 60);
            // 
            // pnlTblCompensationInfo
            // 
            this.pnlTblCompensationInfo.ConcurrentPanels = null;
            this.pnlTblCompensationInfo.Controls.Add(this.dgvComp);
            this.pnlTblCompensationInfo.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblCompensationInfo.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblCompensationInfo.DependentPanels = null;
            this.pnlTblCompensationInfo.DisableDependentLoad = false;
            this.pnlTblCompensationInfo.EnableDelete = true;
            this.pnlTblCompensationInfo.EnableInsert = true;
            this.pnlTblCompensationInfo.EnableQuery = false;
            this.pnlTblCompensationInfo.EnableUpdate = true;
            this.pnlTblCompensationInfo.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CompEntCommand";
            this.pnlTblCompensationInfo.Location = new System.Drawing.Point(12, 96);
            this.pnlTblCompensationInfo.MasterPanel = null;
            this.pnlTblCompensationInfo.Name = "pnlTblCompensationInfo";
            this.pnlTblCompensationInfo.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblCompensationInfo.Size = new System.Drawing.Size(644, 230);
            this.pnlTblCompensationInfo.SPName = "CHRIS_SP_CompEnt_COMP_MANAGER";
            this.pnlTblCompensationInfo.TabIndex = 0;
            // 
            // dgvComp
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvComp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvComp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Lvl_Code,
            this.Grp_Head,
            this.Min_Sal,
            this.Mid_Sal,
            this.Max_Sal,
            this.Budget_Price,
            this.Annual_Dep,
            this.Petrol_Ltr,
            this.Petrol_Rate,
            this.Insurance_Rate,
            this.Maintenance_Rate,
            this.Driver_Cost,
            this.GroupHead_Allowance,
            this.D_lvl,
            this.D_GrpHead});
            this.dgvComp.ColumnToHide = null;
            this.dgvComp.ColumnWidth = null;
            this.dgvComp.CustomEnabled = true;
            this.dgvComp.DisplayColumnWrapper = null;
            this.dgvComp.GridDefaultRow = 0;
            this.dgvComp.Location = new System.Drawing.Point(3, 3);
            this.dgvComp.Name = "dgvComp";
            this.dgvComp.ReadOnlyColumns = null;
            this.dgvComp.RequiredColumns = null;
            this.dgvComp.Size = new System.Drawing.Size(637, 224);
            this.dgvComp.SkippingColumns = null;
            this.dgvComp.TabIndex = 0;
            this.dgvComp.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComp_CellValidated);
            this.dgvComp.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvComp_CellValidating);
            // 
            // Lvl_Code
            // 
            this.Lvl_Code.DataPropertyName = "LVL_CODE";
            dataGridViewCellStyle2.NullValue = null;
            this.Lvl_Code.DefaultCellStyle = dataGridViewCellStyle2;
            this.Lvl_Code.HeaderText = "Lvl Code";
            this.Lvl_Code.MaxInputLength = 3;
            this.Lvl_Code.Name = "Lvl_Code";
            this.Lvl_Code.Width = 40;
            // 
            // Grp_Head
            // 
            this.Grp_Head.DataPropertyName = "GRPHD";
            this.Grp_Head.FalseValue = "N";
            this.Grp_Head.HeaderText = "Grp Head";
            this.Grp_Head.Name = "Grp_Head";
            this.Grp_Head.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grp_Head.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Grp_Head.TrueValue = "Y";
            this.Grp_Head.Width = 40;
            // 
            // Min_Sal
            // 
            this.Min_Sal.DataPropertyName = "MIN_SAL";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.NullValue = null;
            this.Min_Sal.DefaultCellStyle = dataGridViewCellStyle3;
            this.Min_Sal.HeaderText = "Min Sal";
            this.Min_Sal.MaxInputLength = 13;
            this.Min_Sal.Name = "Min_Sal";
            this.Min_Sal.Width = 80;
            // 
            // Mid_Sal
            // 
            this.Mid_Sal.DataPropertyName = "MID_SAL";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.Mid_Sal.DefaultCellStyle = dataGridViewCellStyle4;
            this.Mid_Sal.HeaderText = "Mid Sal";
            this.Mid_Sal.MaxInputLength = 12;
            this.Mid_Sal.Name = "Mid_Sal";
            this.Mid_Sal.ReadOnly = true;
            this.Mid_Sal.Width = 85;
            // 
            // Max_Sal
            // 
            this.Max_Sal.DataPropertyName = "MAX_SAL";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.NullValue = null;
            this.Max_Sal.DefaultCellStyle = dataGridViewCellStyle5;
            this.Max_Sal.HeaderText = "Max Sal";
            this.Max_Sal.MaxInputLength = 12;
            this.Max_Sal.Name = "Max_Sal";
            this.Max_Sal.Width = 80;
            // 
            // Budget_Price
            // 
            this.Budget_Price.DataPropertyName = "BDGT_PRICE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.NullValue = null;
            this.Budget_Price.DefaultCellStyle = dataGridViewCellStyle6;
            this.Budget_Price.HeaderText = "Budget Price";
            this.Budget_Price.MaxInputLength = 13;
            this.Budget_Price.Name = "Budget_Price";
            this.Budget_Price.Width = 80;
            // 
            // Annual_Dep
            // 
            this.Annual_Dep.DataPropertyName = "ANL_DEPC";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = null;
            this.Annual_Dep.DefaultCellStyle = dataGridViewCellStyle7;
            this.Annual_Dep.HeaderText = "Annual Dep";
            this.Annual_Dep.MaxInputLength = 7;
            this.Annual_Dep.Name = "Annual_Dep";
            this.Annual_Dep.Width = 50;
            // 
            // Petrol_Ltr
            // 
            this.Petrol_Ltr.DataPropertyName = "PTRL_LTR";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.NullValue = null;
            this.Petrol_Ltr.DefaultCellStyle = dataGridViewCellStyle8;
            this.Petrol_Ltr.HeaderText = "Petrol Ltr";
            this.Petrol_Ltr.MaxInputLength = 6;
            this.Petrol_Ltr.Name = "Petrol_Ltr";
            this.Petrol_Ltr.Width = 50;
            // 
            // Petrol_Rate
            // 
            this.Petrol_Rate.DataPropertyName = "PTRL_RATE";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = null;
            this.Petrol_Rate.DefaultCellStyle = dataGridViewCellStyle9;
            this.Petrol_Rate.HeaderText = "Petrol Rate";
            this.Petrol_Rate.MaxInputLength = 8;
            this.Petrol_Rate.Name = "Petrol_Rate";
            this.Petrol_Rate.Width = 60;
            // 
            // Insurance_Rate
            // 
            this.Insurance_Rate.DataPropertyName = "INS_RATE";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.NullValue = null;
            this.Insurance_Rate.DefaultCellStyle = dataGridViewCellStyle10;
            this.Insurance_Rate.HeaderText = "Insurance Rate";
            this.Insurance_Rate.MaxInputLength = 11;
            this.Insurance_Rate.Name = "Insurance_Rate";
            this.Insurance_Rate.Width = 80;
            // 
            // Maintenance_Rate
            // 
            this.Maintenance_Rate.DataPropertyName = "MNTNC_AMT";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.NullValue = null;
            this.Maintenance_Rate.DefaultCellStyle = dataGridViewCellStyle11;
            this.Maintenance_Rate.HeaderText = "Maintenance Amt";
            this.Maintenance_Rate.MaxInputLength = 11;
            this.Maintenance_Rate.Name = "Maintenance_Rate";
            this.Maintenance_Rate.Width = 80;
            // 
            // Driver_Cost
            // 
            this.Driver_Cost.DataPropertyName = "DRVR_COST";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.NullValue = null;
            this.Driver_Cost.DefaultCellStyle = dataGridViewCellStyle12;
            this.Driver_Cost.HeaderText = "Driver Cost";
            this.Driver_Cost.MaxInputLength = 12;
            this.Driver_Cost.Name = "Driver_Cost";
            this.Driver_Cost.Width = 80;
            // 
            // GroupHead_Allowance
            // 
            this.GroupHead_Allowance.DataPropertyName = "GRPHD_ALL";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.NullValue = null;
            this.GroupHead_Allowance.DefaultCellStyle = dataGridViewCellStyle13;
            this.GroupHead_Allowance.HeaderText = "Grouphead Allowance";
            this.GroupHead_Allowance.MaxInputLength = 13;
            this.GroupHead_Allowance.Name = "GroupHead_Allowance";
            this.GroupHead_Allowance.Width = 80;
            // 
            // D_lvl
            // 
            this.D_lvl.DataPropertyName = "LVL_CODE";
            this.D_lvl.HeaderText = "Lvl Code";
            this.D_lvl.MaxInputLength = 2;
            this.D_lvl.Name = "D_lvl";
            this.D_lvl.ReadOnly = true;
            this.D_lvl.Width = 50;
            // 
            // D_GrpHead
            // 
            this.D_GrpHead.DataPropertyName = "GRPHD";
            this.D_GrpHead.HeaderText = "Grp Head";
            this.D_GrpHead.MaxInputLength = 2;
            this.D_GrpHead.Name = "D_GrpHead";
            this.D_GrpHead.ReadOnly = true;
            this.D_GrpHead.Width = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(188, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Compensation Information Entry";
            // 
            // pnlButton
            // 
            this.pnlButton.Controls.Add(this.btnGrp);
            this.pnlButton.Controls.Add(this.btnQuery);
            this.pnlButton.Controls.Add(this.btnSave);
            this.pnlButton.Controls.Add(this.btnExit);
            this.pnlButton.Controls.Add(this.btnDel);
            this.pnlButton.Controls.Add(this.btnScroll_Down);
            this.pnlButton.Controls.Add(this.btnDown);
            this.pnlButton.Controls.Add(this.btnUp);
            this.pnlButton.Controls.Add(this.btnScroll_Up);
            this.pnlButton.Location = new System.Drawing.Point(58, 332);
            this.pnlButton.Name = "pnlButton";
            this.pnlButton.Size = new System.Drawing.Size(539, 50);
            this.pnlButton.TabIndex = 9;
            // 
            // btnGrp
            // 
            this.btnGrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrp.Location = new System.Drawing.Point(456, 18);
            this.btnGrp.Name = "btnGrp";
            this.btnGrp.Size = new System.Drawing.Size(73, 23);
            this.btnGrp.TabIndex = 8;
            this.btnGrp.Text = "Group Head";
            this.btnGrp.UseVisualStyleBackColor = true;
            this.btnGrp.Click += new System.EventHandler(this.btnGrp_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuery.Location = new System.Drawing.Point(306, 18);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(73, 23);
            this.btnQuery.TabIndex = 6;
            this.btnQuery.Text = "Query";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(381, 18);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(231, 18);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(73, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.Location = new System.Drawing.Point(155, 18);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(73, 23);
            this.btnDel.TabIndex = 4;
            this.btnDel.Text = "Remove";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnScroll_Down
            // 
            this.btnScroll_Down.Location = new System.Drawing.Point(104, 18);
            this.btnScroll_Down.Name = "btnScroll_Down";
            this.btnScroll_Down.Size = new System.Drawing.Size(32, 23);
            this.btnScroll_Down.TabIndex = 3;
            this.btnScroll_Down.Text = ">>";
            this.btnScroll_Down.UseVisualStyleBackColor = true;
            this.btnScroll_Down.Click += new System.EventHandler(this.btnScroll_Down_Click);
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(71, 18);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(32, 23);
            this.btnDown.TabIndex = 2;
            this.btnDown.Text = ">";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUp
            // 
            this.btnUp.Location = new System.Drawing.Point(37, 18);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(32, 23);
            this.btnUp.TabIndex = 1;
            this.btnUp.Text = "<";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnScroll_Up
            // 
            this.btnScroll_Up.Location = new System.Drawing.Point(3, 18);
            this.btnScroll_Up.Name = "btnScroll_Up";
            this.btnScroll_Up.Size = new System.Drawing.Size(32, 23);
            this.btnScroll_Up.TabIndex = 0;
            this.btnScroll_Up.Text = "<<";
            this.btnScroll_Up.UseVisualStyleBackColor = true;
            this.btnScroll_Up.Click += new System.EventHandler(this.btnScroll_Up_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(414, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "User Name :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(479, 12);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 11;
            this.lblUserName.Text = "User Name :";
            // 
            // CHRIS_Setup_CompensationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 420);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pnlButton);
            this.Controls.Add(this.pnlTblCompensationInfo);
            this.Controls.Add(this.label1);
            this.F6OptionText = "";
            this.Name = "CHRIS_Setup_CompensationEntry";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS - Compensation Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.pnlTblCompensationInfo, 0);
            this.Controls.SetChildIndex(this.pnlButton, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblCompensationInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvComp)).EndInit();
            this.pnlButton.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblCompensationInfo;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvComp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlButton;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnScroll_Down;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnScroll_Up;
        private System.Windows.Forms.Button btnGrp;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lvl_Code;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Grp_Head;
        private System.Windows.Forms.DataGridViewTextBoxColumn Min_Sal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mid_Sal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Max_Sal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Budget_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Annual_Dep;
        private System.Windows.Forms.DataGridViewTextBoxColumn Petrol_Ltr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Petrol_Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Insurance_Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Maintenance_Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Driver_Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupHead_Allowance;
        private System.Windows.Forms.DataGridViewTextBoxColumn D_lvl;
        private System.Windows.Forms.DataGridViewTextBoxColumn D_GrpHead;
        //private System.Windows.Forms.DataGridViewTextBoxColumn MinSal;
        //private System.Windows.Forms.DataGridViewTextBoxColumn MidSal;
        //private System.Windows.Forms.DataGridViewTextBoxColumn MaxSal;
    }
}