using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_Category_Entry : ChrisTabularForm
    {
       CmnDataManager cmnDM = new CmnDataManager();
        #region Constructors

        public CHRIS_Setup_Category_Entry()
        {
            InitializeComponent();
        }
        public CHRIS_Setup_Category_Entry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            label7.Text += "   " + this.UserName;
            DGVCategory.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold); 
            
        
        }

        #endregion
        #region Methods
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
           // this.CurrrentOptionTextBox = this.txtCurrOption;
            this.txtOption.Visible = false; 

        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                DataTable dt = (DataTable)DGVCategory.DataSource;
                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                        DialogResult dr = MessageBox.Show("Do You Want to Save the Record [Y/N]", "", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                        if (dr == DialogResult.No) { return; }
                        else
                        {
                            this.DGVCategory.SaveGrid(this.PnlDetail);
                            this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
                            return;
                        }
                    }
                }
                return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        protected override void CommonOnLoadMethods()
        {

            base.CommonOnLoadMethods();
            Category_Code.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            Cat_Desc.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);

        }

        private bool Exists(string code)
        {
            bool flag = false;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_cat_code", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "LovCatCodeExisit", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }



        #endregion
        #region Events
        private void DGVCategory_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //DataTable dt = (DataTable)this.DGVCategory.DataSource;
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    if (e.RowIndex < dt.Rows.Count - 1)
            //    {
            //        if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
            //        {
            //            Category_Code.ReadOnly = true;
            //        }
            //        else
            //        {
            //            Category_Code.ReadOnly = false;
            //        }
            //    }
            //    else
            //    {
            //        Category_Code.ReadOnly = true;
            //    }
            //}
            //else
            //{
            //    Category_Code.ReadOnly= true;
            //}
        }
        private void DGVCategory_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (DGVCategory.CurrentRow.Cells[0].IsInEditMode)
                {


                    if (DGVCategory.CurrentCell.EditedFormattedValue.ToString() == "" || DGVCategory.CurrentCell.Value == null )
                    {
                        e.Cancel = true;
                        return;
                    }

                    if (((DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() == "B")
                         || (DGVCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToUpper() == "A")))
                    //&& DGVCategory.Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString() == string.Empty
                    {
                        MessageBox.Show("Category Code Should Start From [C] .....!");
                        e.Cancel = true;
                    }

                   

                    object obj = DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue;
                    if (obj != null && obj.ToString() != string.Empty)
                    {
                        if (this.Exists(obj.ToString()))
                        {
                            MessageBox.Show("Record Already Entered");
                            this.Message.ShowMessage();

                           // DGVCategory.Rows[e.RowIndex].Cells[0].Value = "";
                          //  DGVCategory.Rows[e.RowIndex].Cells[1].Value = "";

                            e.Cancel = true;
                            return;

                        }
                    }
                  
                
                }
                           
            }




            if (e.ColumnIndex == 1)
            {
                if (DGVCategory.CurrentRow.Cells[1].IsInEditMode)
                {


                    if ( (DGVCategory.CurrentCell.EditedFormattedValue.ToString() != "" || DGVCategory.CurrentCell.Value != null)  && DGVCategory.CurrentRow.Cells[0].FormattedValue.ToString() =="" )
                    {
                        e.Cancel = true;
                        //DGVCategory.CurrentRow.Cells[0].Selected = true;
                        return;
                    }
                }
            }



        }
        private void DGVCategory_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int index = 0; index < DGVCategory.Rows.Count - 1; index++)
            { DGVCategory.Rows[index].Cells[0].ReadOnly = true; }
        }
        #endregion

        private void DGVCategory_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }



        }

        private void DGVCategory_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {


            if (this.FunctionConfig.CurrentOption != Function.Add)
            {
                if ((DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString().ToUpper() != String.Empty) && (DGVCategory.Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString() == string.Empty))
                {
                    DGVCategory.Rows[e.RowIndex].Cells[1].Value = proc_view_add(DGVCategory.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString().ToUpper());

                }
            }


        }

        private String proc_view_add(string Pram)
        {
            string sp_Desc = string.Empty;
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Add("SP_CAT_CODE", Pram);

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_CATEGORY_MANAGER", "proc_view_add", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                    sp_Desc = dt.Rows[0][1].ToString();

                }
            }
            return sp_Desc;
        }


    }
}