namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_BranchSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_BranchSetup));
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnbrnCode = new CrplControlLibrary.LookupButton(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBranchAdd3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranchAdd2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDrAccount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCrAccount = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranchAdd1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranchName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranchCode = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(488, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(524, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 378);
            this.panel1.Size = new System.Drawing.Size(524, 60);
            // 
            // pnlDetail
            // 
            this.pnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.lbtnbrnCode);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.label6);
            this.pnlDetail.Controls.Add(this.txtBranchAdd3);
            this.pnlDetail.Controls.Add(this.txtBranchAdd2);
            this.pnlDetail.Controls.Add(this.txtDrAccount);
            this.pnlDetail.Controls.Add(this.txtCrAccount);
            this.pnlDetail.Controls.Add(this.txtBranchAdd1);
            this.pnlDetail.Controls.Add(this.txtBranchName);
            this.pnlDetail.Controls.Add(this.txtBranchCode);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.label3);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.DataManager = null;
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BranchCommand";
            this.pnlDetail.Location = new System.Drawing.Point(12, 142);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(500, 231);
            this.pnlDetail.SPName = "CHRIS_SP_BRANCH_MANAGER";
            this.pnlDetail.TabIndex = 11;
            // 
            // lbtnbrnCode
            // 
            this.lbtnbrnCode.ActionLOVExists = "BranchCodeLOVExists";
            this.lbtnbrnCode.ActionType = "BranchCodeLOV";
            this.lbtnbrnCode.ConditionalFields = "";
            this.lbtnbrnCode.CustomEnabled = true;
            this.lbtnbrnCode.DataFieldMapping = "";
            this.lbtnbrnCode.DependentLovControls = "";
            this.lbtnbrnCode.HiddenColumns = "BRN_ADD1|BRN_ADD2|BRN_ADD3|BRN_CR_AC|BRN_DR_AC";
            this.lbtnbrnCode.Image = ((System.Drawing.Image)(resources.GetObject("lbtnbrnCode.Image")));
            this.lbtnbrnCode.LoadDependentEntities = true;
            this.lbtnbrnCode.Location = new System.Drawing.Point(255, 21);
            this.lbtnbrnCode.LookUpTitle = null;
            this.lbtnbrnCode.Name = "lbtnbrnCode";
            this.lbtnbrnCode.Size = new System.Drawing.Size(26, 21);
            this.lbtnbrnCode.SkipValidationOnLeave = false;
            this.lbtnbrnCode.SPName = "CHRIS_SP_BRANCH_MANAGER";
            this.lbtnbrnCode.TabIndex = 14;
            this.lbtnbrnCode.TabStop = false;
            this.lbtnbrnCode.Tag = "";
            this.lbtnbrnCode.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(128, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = " :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(128, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = " :";
            // 
            // txtBranchAdd3
            // 
            this.txtBranchAdd3.AllowSpace = true;
            this.txtBranchAdd3.AssociatedLookUpName = "";
            this.txtBranchAdd3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchAdd3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchAdd3.ContinuationTextBox = null;
            this.txtBranchAdd3.CustomEnabled = true;
            this.txtBranchAdd3.DataFieldMapping = "BRN_ADD3";
            this.txtBranchAdd3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchAdd3.GetRecordsOnUpDownKeys = false;
            this.txtBranchAdd3.IsDate = false;
            this.txtBranchAdd3.Location = new System.Drawing.Point(147, 130);
            this.txtBranchAdd3.MaxLength = 30;
            this.txtBranchAdd3.Name = "txtBranchAdd3";
            this.txtBranchAdd3.NumberFormat = "###,###,##0.00";
            this.txtBranchAdd3.Postfix = "";
            this.txtBranchAdd3.Prefix = "";
            this.txtBranchAdd3.Size = new System.Drawing.Size(261, 20);
            this.txtBranchAdd3.SkipValidation = false;
            this.txtBranchAdd3.TabIndex = 11;
            this.txtBranchAdd3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranchAdd2
            // 
            this.txtBranchAdd2.AllowSpace = true;
            this.txtBranchAdd2.AssociatedLookUpName = "";
            this.txtBranchAdd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchAdd2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchAdd2.ContinuationTextBox = null;
            this.txtBranchAdd2.CustomEnabled = true;
            this.txtBranchAdd2.DataFieldMapping = "BRN_ADD2";
            this.txtBranchAdd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchAdd2.GetRecordsOnUpDownKeys = false;
            this.txtBranchAdd2.IsDate = false;
            this.txtBranchAdd2.Location = new System.Drawing.Point(147, 104);
            this.txtBranchAdd2.MaxLength = 30;
            this.txtBranchAdd2.Name = "txtBranchAdd2";
            this.txtBranchAdd2.NumberFormat = "###,###,##0.00";
            this.txtBranchAdd2.Postfix = "";
            this.txtBranchAdd2.Prefix = "";
            this.txtBranchAdd2.Size = new System.Drawing.Size(261, 20);
            this.txtBranchAdd2.SkipValidation = false;
            this.txtBranchAdd2.TabIndex = 10;
            this.txtBranchAdd2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDrAccount
            // 
            this.txtDrAccount.AllowSpace = true;
            this.txtDrAccount.AssociatedLookUpName = "";
            this.txtDrAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDrAccount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDrAccount.ContinuationTextBox = null;
            this.txtDrAccount.CustomEnabled = true;
            this.txtDrAccount.DataFieldMapping = "BRN_DR_AC";
            this.txtDrAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDrAccount.GetRecordsOnUpDownKeys = false;
            this.txtDrAccount.IsDate = false;
            this.txtDrAccount.IsRequired = true;
            this.txtDrAccount.Location = new System.Drawing.Point(147, 188);
            this.txtDrAccount.MaxLength = 10;
            this.txtDrAccount.Name = "txtDrAccount";
            this.txtDrAccount.NumberFormat = "###,###,##0.00";
            this.txtDrAccount.Postfix = "";
            this.txtDrAccount.Prefix = "";
            this.txtDrAccount.Size = new System.Drawing.Size(100, 20);
            this.txtDrAccount.SkipValidation = false;
            this.txtDrAccount.TabIndex = 13;
            this.txtDrAccount.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCrAccount
            // 
            this.txtCrAccount.AllowSpace = true;
            this.txtCrAccount.AssociatedLookUpName = "";
            this.txtCrAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCrAccount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCrAccount.ContinuationTextBox = null;
            this.txtCrAccount.CustomEnabled = true;
            this.txtCrAccount.DataFieldMapping = "BRN_CR_AC";
            this.txtCrAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCrAccount.GetRecordsOnUpDownKeys = false;
            this.txtCrAccount.IsDate = false;
            this.txtCrAccount.IsRequired = true;
            this.txtCrAccount.Location = new System.Drawing.Point(147, 160);
            this.txtCrAccount.MaxLength = 10;
            this.txtCrAccount.Name = "txtCrAccount";
            this.txtCrAccount.NumberFormat = "###,###,##0.00";
            this.txtCrAccount.Postfix = "";
            this.txtCrAccount.Prefix = "";
            this.txtCrAccount.Size = new System.Drawing.Size(100, 20);
            this.txtCrAccount.SkipValidation = false;
            this.txtCrAccount.TabIndex = 12;
            this.txtCrAccount.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranchAdd1
            // 
            this.txtBranchAdd1.AllowSpace = true;
            this.txtBranchAdd1.AssociatedLookUpName = "";
            this.txtBranchAdd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchAdd1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchAdd1.ContinuationTextBox = null;
            this.txtBranchAdd1.CustomEnabled = true;
            this.txtBranchAdd1.DataFieldMapping = "BRN_ADD1";
            this.txtBranchAdd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchAdd1.GetRecordsOnUpDownKeys = false;
            this.txtBranchAdd1.IsDate = false;
            this.txtBranchAdd1.IsRequired = true;
            this.txtBranchAdd1.Location = new System.Drawing.Point(147, 78);
            this.txtBranchAdd1.MaxLength = 30;
            this.txtBranchAdd1.Name = "txtBranchAdd1";
            this.txtBranchAdd1.NumberFormat = "###,###,##0.00";
            this.txtBranchAdd1.Postfix = "";
            this.txtBranchAdd1.Prefix = "";
            this.txtBranchAdd1.Size = new System.Drawing.Size(261, 20);
            this.txtBranchAdd1.SkipValidation = false;
            this.txtBranchAdd1.TabIndex = 7;
            this.txtBranchAdd1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranchName
            // 
            this.txtBranchName.AllowSpace = true;
            this.txtBranchName.AssociatedLookUpName = "";
            this.txtBranchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchName.ContinuationTextBox = null;
            this.txtBranchName.CustomEnabled = true;
            this.txtBranchName.DataFieldMapping = "BRN_NAME";
            this.txtBranchName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchName.GetRecordsOnUpDownKeys = false;
            this.txtBranchName.IsDate = false;
            this.txtBranchName.IsRequired = true;
            this.txtBranchName.Location = new System.Drawing.Point(147, 50);
            this.txtBranchName.MaxLength = 40;
            this.txtBranchName.Name = "txtBranchName";
            this.txtBranchName.NumberFormat = "###,###,##0.00";
            this.txtBranchName.Postfix = "";
            this.txtBranchName.Prefix = "";
            this.txtBranchName.Size = new System.Drawing.Size(311, 20);
            this.txtBranchName.SkipValidation = false;
            this.txtBranchName.TabIndex = 6;
            this.txtBranchName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranchCode
            // 
            this.txtBranchCode.AllowSpace = true;
            this.txtBranchCode.AssociatedLookUpName = "lbtnbrnCode";
            this.txtBranchCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchCode.ContinuationTextBox = null;
            this.txtBranchCode.CustomEnabled = true;
            this.txtBranchCode.DataFieldMapping = "BRN_CODE";
            this.txtBranchCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchCode.GetRecordsOnUpDownKeys = false;
            this.txtBranchCode.IsDate = false;
            this.txtBranchCode.IsRequired = true;
            this.txtBranchCode.Location = new System.Drawing.Point(147, 21);
            this.txtBranchCode.MaxLength = 3;
            this.txtBranchCode.Name = "txtBranchCode";
            this.txtBranchCode.NumberFormat = "###,###,##0.00";
            this.txtBranchCode.Postfix = "";
            this.txtBranchCode.Prefix = "";
            this.txtBranchCode.Size = new System.Drawing.Size(100, 20);
            this.txtBranchCode.SkipValidation = false;
            this.txtBranchCode.TabIndex = 5;
            this.txtBranchCode.TextType = CrplControlLibrary.TextType.String;
            this.txtBranchCode.Validated += new System.EventHandler(this.txtBranchCode_Validated);
            this.txtBranchCode.Leave += new System.EventHandler(this.txtBranchCode_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(31, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Branch Dr. Account :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Branch Cr. Account :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Branch Address :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Branch Description :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Branch Code :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(204, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Personnel System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(214, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Branch Setup";
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(69, 116);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(84, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 20;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            this.W_LOC.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(69, 87);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 19;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(27, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Loc :";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(26, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "User :";
            this.label12.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(404, 116);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 16;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(356, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Date :";
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(280, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(72, 13);
            this.txtUserName.TabIndex = 120;
            this.txtUserName.Text = "User  Name : ";
            // 
            // CHRIS_Setup_BranchSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 438);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.W_LOC);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtUser);
            this.CurrentPanelBlock = "pnlDetail";
            this.F4OptionText = "[F4]=Save Record";
            this.Name = "CHRIS_Setup_BranchSetup";
            this.ShowBottomBar = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Branch Setup";
            this.Load += new System.EventHandler(this.CHRIS_Setup_BranchSetup_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.W_LOC, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtDrAccount;
        private CrplControlLibrary.SLTextBox txtCrAccount;
        private CrplControlLibrary.SLTextBox txtBranchAdd1;
        private CrplControlLibrary.SLTextBox txtBranchName;
        private CrplControlLibrary.SLTextBox txtBranchCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtBranchAdd3;
        private CrplControlLibrary.SLTextBox txtBranchAdd2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.LookupButton lbtnbrnCode;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox W_LOC;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label txtUserName;
    }
}