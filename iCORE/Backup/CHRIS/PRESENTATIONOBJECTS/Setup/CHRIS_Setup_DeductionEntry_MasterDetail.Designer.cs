namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_DeductionEntry_MasterDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_DeductionEntry_MasterDetail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlDeductionMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnDedection = new CrplControlLibrary.LookupButton(this.components);
            this.txt_SP_VALID_TO_D = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_VALID_FROM_D = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_SP_DED_DESC = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_ACCOUNT_NO_D = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_AMOUNT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_SP_DED_CODE = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTblDedDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDedDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.grdDeducCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdPersonnelNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.llbusername = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDeductionMain.SuspendLayout();
            this.pnlTblDedDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(587, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(623, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 429);
            this.panel1.Size = new System.Drawing.Size(623, 60);
            // 
            // pnlDeductionMain
            // 
            this.pnlDeductionMain.ConcurrentPanels = null;
            this.pnlDeductionMain.Controls.Add(this.lbtnDedection);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_VALID_TO_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_VALID_FROM_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_DESC);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_ACCOUNT_NO_D);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_AMOUNT);
            this.pnlDeductionMain.Controls.Add(this.txt_SP_DED_CODE);
            this.pnlDeductionMain.Controls.Add(this.label8);
            this.pnlDeductionMain.Controls.Add(this.label7);
            this.pnlDeductionMain.Controls.Add(this.label6);
            this.pnlDeductionMain.Controls.Add(this.label2);
            this.pnlDeductionMain.Controls.Add(this.label1);
            this.pnlDeductionMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDeductionMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDeductionMain.DependentPanels = null;
            this.pnlDeductionMain.DisableDependentLoad = false;
            this.pnlDeductionMain.EnableDelete = true;
            this.pnlDeductionMain.EnableInsert = true;
            this.pnlDeductionMain.EnableQuery = false;
            this.pnlDeductionMain.EnableUpdate = true;
            this.pnlDeductionMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeducEntCommand";
            this.pnlDeductionMain.Location = new System.Drawing.Point(12, 80);
            this.pnlDeductionMain.MasterPanel = null;
            this.pnlDeductionMain.Name = "pnlDeductionMain";
            this.pnlDeductionMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDeductionMain.Size = new System.Drawing.Size(599, 86);
            this.pnlDeductionMain.SPName = "CHRIS_SP_DeducEnt_DEDUCTION_MANAGER";
            this.pnlDeductionMain.TabIndex = 0;
            // 
            // lbtnDedection
            // 
            this.lbtnDedection.ActionLOVExists = "";
            this.lbtnDedection.ActionType = "DUC_DETAIL_LOV1";
            this.lbtnDedection.ConditionalFields = "";
            this.lbtnDedection.CustomEnabled = true;
            this.lbtnDedection.DataFieldMapping = "";
            this.lbtnDedection.DependentLovControls = "";
            this.lbtnDedection.HiddenColumns = "SP_VALID_FROM_D|SP_VALID_TO_D|SP_DED_AMOUNT|SP_ACCOUNT_NO_D";
            this.lbtnDedection.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDedection.Image")));
            this.lbtnDedection.LoadDependentEntities = true;
            this.lbtnDedection.Location = new System.Drawing.Point(205, 11);
            this.lbtnDedection.LookUpTitle = null;
            this.lbtnDedection.Name = "lbtnDedection";
            this.lbtnDedection.Size = new System.Drawing.Size(26, 21);
            this.lbtnDedection.SkipValidationOnLeave = true;
            this.lbtnDedection.SPName = "CHRIS_SP_DEDUCENT_DEDUCTION_MANAGER";
            this.lbtnDedection.TabIndex = 26;
            this.lbtnDedection.TabStop = false;
            this.lbtnDedection.UseVisualStyleBackColor = true;
            // 
            // txt_SP_VALID_TO_D
            // 
            this.txt_SP_VALID_TO_D.CustomEnabled = false;
            this.txt_SP_VALID_TO_D.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_VALID_TO_D.DataFieldMapping = "SP_VALID_TO_D";
            this.txt_SP_VALID_TO_D.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_VALID_TO_D.HasChanges = false;
            this.txt_SP_VALID_TO_D.Location = new System.Drawing.Point(257, 36);
            this.txt_SP_VALID_TO_D.Name = "txt_SP_VALID_TO_D";
            this.txt_SP_VALID_TO_D.NullValue = " ";
            this.txt_SP_VALID_TO_D.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_VALID_TO_D.TabIndex = 10;
            this.txt_SP_VALID_TO_D.TabStop = false;
            this.txt_SP_VALID_TO_D.Value = new System.DateTime(2011, 2, 4, 0, 0, 0, 0);
            // 
            // txt_SP_VALID_FROM_D
            // 
            this.txt_SP_VALID_FROM_D.CustomEnabled = false;
            this.txt_SP_VALID_FROM_D.CustomFormat = "dd/MM/yyyy";
            this.txt_SP_VALID_FROM_D.DataFieldMapping = "SP_VALID_FROM_D";
            this.txt_SP_VALID_FROM_D.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_SP_VALID_FROM_D.HasChanges = false;
            this.txt_SP_VALID_FROM_D.Location = new System.Drawing.Point(131, 36);
            this.txt_SP_VALID_FROM_D.Name = "txt_SP_VALID_FROM_D";
            this.txt_SP_VALID_FROM_D.NullValue = " ";
            this.txt_SP_VALID_FROM_D.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_VALID_FROM_D.TabIndex = 9;
            this.txt_SP_VALID_FROM_D.TabStop = false;
            this.txt_SP_VALID_FROM_D.Value = new System.DateTime(2011, 2, 4, 0, 0, 0, 0);
            // 
            // txt_SP_DED_DESC
            // 
            this.txt_SP_DED_DESC.AllowSpace = true;
            this.txt_SP_DED_DESC.AssociatedLookUpName = "";
            this.txt_SP_DED_DESC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_DESC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_DESC.ContinuationTextBox = null;
            this.txt_SP_DED_DESC.CustomEnabled = false;
            this.txt_SP_DED_DESC.DataFieldMapping = "SP_DED_DESC";
            this.txt_SP_DED_DESC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_DESC.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_DESC.IsDate = false;
            this.txt_SP_DED_DESC.Location = new System.Drawing.Point(257, 13);
            this.txt_SP_DED_DESC.Name = "txt_SP_DED_DESC";
            this.txt_SP_DED_DESC.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_DESC.Postfix = "";
            this.txt_SP_DED_DESC.Prefix = "";
            this.txt_SP_DED_DESC.Size = new System.Drawing.Size(271, 20);
            this.txt_SP_DED_DESC.SkipValidation = false;
            this.txt_SP_DED_DESC.TabIndex = 8;
            this.txt_SP_DED_DESC.TabStop = false;
            this.txt_SP_DED_DESC.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_ACCOUNT_NO_D
            // 
            this.txt_SP_ACCOUNT_NO_D.AllowSpace = true;
            this.txt_SP_ACCOUNT_NO_D.AssociatedLookUpName = "";
            this.txt_SP_ACCOUNT_NO_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_ACCOUNT_NO_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_ACCOUNT_NO_D.ContinuationTextBox = null;
            this.txt_SP_ACCOUNT_NO_D.CustomEnabled = false;
            this.txt_SP_ACCOUNT_NO_D.DataFieldMapping = "SP_ACCOUNT_NO_D";
            this.txt_SP_ACCOUNT_NO_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_ACCOUNT_NO_D.GetRecordsOnUpDownKeys = false;
            this.txt_SP_ACCOUNT_NO_D.IsDate = false;
            this.txt_SP_ACCOUNT_NO_D.Location = new System.Drawing.Point(131, 59);
            this.txt_SP_ACCOUNT_NO_D.Name = "txt_SP_ACCOUNT_NO_D";
            this.txt_SP_ACCOUNT_NO_D.NumberFormat = "###,###,##0.00";
            this.txt_SP_ACCOUNT_NO_D.Postfix = "";
            this.txt_SP_ACCOUNT_NO_D.Prefix = "";
            this.txt_SP_ACCOUNT_NO_D.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_ACCOUNT_NO_D.SkipValidation = false;
            this.txt_SP_ACCOUNT_NO_D.TabIndex = 7;
            this.txt_SP_ACCOUNT_NO_D.TabStop = false;
            this.txt_SP_ACCOUNT_NO_D.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_DED_AMOUNT
            // 
            this.txt_SP_DED_AMOUNT.AllowSpace = true;
            this.txt_SP_DED_AMOUNT.AssociatedLookUpName = "";
            this.txt_SP_DED_AMOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_AMOUNT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_AMOUNT.ContinuationTextBox = null;
            this.txt_SP_DED_AMOUNT.CustomEnabled = false;
            this.txt_SP_DED_AMOUNT.DataFieldMapping = "SP_DED_AMOUNT";
            this.txt_SP_DED_AMOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_AMOUNT.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_AMOUNT.IsDate = false;
            this.txt_SP_DED_AMOUNT.Location = new System.Drawing.Point(428, 36);
            this.txt_SP_DED_AMOUNT.Name = "txt_SP_DED_AMOUNT";
            this.txt_SP_DED_AMOUNT.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_AMOUNT.Postfix = "";
            this.txt_SP_DED_AMOUNT.Prefix = "";
            this.txt_SP_DED_AMOUNT.Size = new System.Drawing.Size(100, 20);
            this.txt_SP_DED_AMOUNT.SkipValidation = false;
            this.txt_SP_DED_AMOUNT.TabIndex = 6;
            this.txt_SP_DED_AMOUNT.TabStop = false;
            this.txt_SP_DED_AMOUNT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_SP_DED_CODE
            // 
            this.txt_SP_DED_CODE.AllowSpace = true;
            this.txt_SP_DED_CODE.AssociatedLookUpName = "lbtnDedection";
            this.txt_SP_DED_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_SP_DED_CODE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_SP_DED_CODE.ContinuationTextBox = null;
            this.txt_SP_DED_CODE.CustomEnabled = false;
            this.txt_SP_DED_CODE.DataFieldMapping = "SP_DED_CODE";
            this.txt_SP_DED_CODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SP_DED_CODE.GetRecordsOnUpDownKeys = false;
            this.txt_SP_DED_CODE.IsDate = false;
            this.txt_SP_DED_CODE.Location = new System.Drawing.Point(131, 13);
            this.txt_SP_DED_CODE.MaxLength = 3;
            this.txt_SP_DED_CODE.Name = "txt_SP_DED_CODE";
            this.txt_SP_DED_CODE.NumberFormat = "###,###,##0.00";
            this.txt_SP_DED_CODE.Postfix = "";
            this.txt_SP_DED_CODE.Prefix = "";
            this.txt_SP_DED_CODE.Size = new System.Drawing.Size(62, 20);
            this.txt_SP_DED_CODE.SkipValidation = false;
            this.txt_SP_DED_CODE.TabIndex = 5;
            this.txt_SP_DED_CODE.TabStop = false;
            this.txt_SP_DED_CODE.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(373, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Amount";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(237, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "/";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(29, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Account No.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "From / To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code";
            // 
            // pnlTblDedDetail
            // 
            this.pnlTblDedDetail.ConcurrentPanels = null;
            this.pnlTblDedDetail.Controls.Add(this.dgvDedDetail);
            this.pnlTblDedDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDedDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDedDetail.DependentPanels = null;
            this.pnlTblDedDetail.DisableDependentLoad = false;
            this.pnlTblDedDetail.EnableDelete = true;
            this.pnlTblDedDetail.EnableInsert = true;
            this.pnlTblDedDetail.EnableQuery = false;
            this.pnlTblDedDetail.EnableUpdate = true;
            this.pnlTblDedDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DedDetailEntCommand";
            this.pnlTblDedDetail.Location = new System.Drawing.Point(12, 172);
            this.pnlTblDedDetail.MasterPanel = null;
            this.pnlTblDedDetail.Name = "pnlTblDedDetail";
            this.pnlTblDedDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDedDetail.Size = new System.Drawing.Size(599, 222);
            this.pnlTblDedDetail.SPName = "CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER";
            this.pnlTblDedDetail.TabIndex = 1;
            // 
            // dgvDedDetail
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDedDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDedDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdDeducCode,
            this.grdPersonnelNo,
            this.grdName,
            this.grdRemarks});
            this.dgvDedDetail.ColumnToHide = null;
            this.dgvDedDetail.ColumnWidth = null;
            this.dgvDedDetail.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDedDetail.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDedDetail.DisplayColumnWrapper = null;
            this.dgvDedDetail.GridDefaultRow = 0;
            this.dgvDedDetail.Location = new System.Drawing.Point(3, 3);
            this.dgvDedDetail.Name = "dgvDedDetail";
            this.dgvDedDetail.ReadOnlyColumns = null;
            this.dgvDedDetail.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDedDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDedDetail.Size = new System.Drawing.Size(593, 216);
            this.dgvDedDetail.SkippingColumns = "GRDNAME,GRDREMARKS";
            this.dgvDedDetail.TabIndex = 1;
            // 
            // grdDeducCode
            // 
            this.grdDeducCode.DataPropertyName = "SP_DED_CODE";
            this.grdDeducCode.HeaderText = "";
            this.grdDeducCode.Name = "grdDeducCode";
            this.grdDeducCode.Visible = false;
            // 
            // grdPersonnelNo
            // 
            this.grdPersonnelNo.DataPropertyName = "SP_P_NO";
            this.grdPersonnelNo.HeaderText = "Personnel No.";
            this.grdPersonnelNo.MaxInputLength = 6;
            this.grdPersonnelNo.Name = "grdPersonnelNo";
            this.grdPersonnelNo.ReadOnly = true;
            this.grdPersonnelNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPersonnelNo.Width = 120;
            // 
            // grdName
            // 
            this.grdName.DataPropertyName = "Name";
            this.grdName.HeaderText = "Name";
            this.grdName.MaxInputLength = 25;
            this.grdName.Name = "grdName";
            this.grdName.ReadOnly = true;
            this.grdName.Width = 220;
            // 
            // grdRemarks
            // 
            this.grdRemarks.DataPropertyName = "SP_REMARKS";
            this.grdRemarks.HeaderText = "Remarks";
            this.grdRemarks.MaxInputLength = 30;
            this.grdRemarks.Name = "grdRemarks";
            this.grdRemarks.ReadOnly = true;
            this.grdRemarks.Width = 210;
            // 
            // llbusername
            // 
            this.llbusername.AutoSize = true;
            this.llbusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llbusername.Location = new System.Drawing.Point(447, 9);
            this.llbusername.Name = "llbusername";
            this.llbusername.Size = new System.Drawing.Size(66, 13);
            this.llbusername.TabIndex = 28;
            this.llbusername.Text = "User Name :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(382, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "User Name :";
            // 
            // CHRIS_Setup_DeductionEntry_MasterDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 489);
            this.Controls.Add(this.llbusername);
            this.Controls.Add(this.pnlTblDedDetail);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.pnlDeductionMain);
            this.Name = "CHRIS_Setup_DeductionEntry_MasterDetail";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS - Deduction Entry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Setup_DeductionEntry_MasterDetail_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDeductionMain, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.pnlTblDedDetail, 0);
            this.Controls.SetChildIndex(this.llbusername, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDeductionMain.ResumeLayout(false);
            this.pnlDeductionMain.PerformLayout();
            this.pnlTblDedDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDedDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDeductionMain;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDedDetail;
        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDedDetail;
        private CrplControlLibrary.SLTextBox txt_SP_DED_DESC;
        private CrplControlLibrary.SLTextBox txt_SP_ACCOUNT_NO_D;
        private CrplControlLibrary.SLTextBox txt_SP_DED_AMOUNT;
        private CrplControlLibrary.SLTextBox txt_SP_DED_CODE;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLDatePicker txt_SP_VALID_TO_D;
        private CrplControlLibrary.SLDatePicker txt_SP_VALID_FROM_D;
        private CrplControlLibrary.LookupButton lbtnDedection;
        private System.Windows.Forms.Label llbusername;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdDeducCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdPersonnelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdName;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdRemarks;
    }
}