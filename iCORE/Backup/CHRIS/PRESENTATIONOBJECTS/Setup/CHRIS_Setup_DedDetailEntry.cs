using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_DedDetailEntry : TabularForm
    {

        #region --Global Variable--

        private DataTable dtDetail;
        int g_sw = 0;
        string pr_p_no = string.Empty;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DeductionEntry frmDedEntry = new CHRIS_Setup_DeductionEntry();

        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPopUP;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage2;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage3;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage4;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage6;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage7;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage8;
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPage9;


        TextBox tbxPopUp            = new TextBox();
        string lblViewPopUP         = "Do You Want To View More Record [Y]es [N]o";
        string lblDeletePopUP       = "Do You Want To Delete The Record [Y]es [N]o";
        string lblAddPopUP          = "Do You Want To Save The Record [Y]es [N]o";
        string lblPrevPagePopUP     = "Do You Want To Go To Previous Page [Y]es [N]o";
        string lblSeeDetailPopUP    = "Do You Want To See The Details [Y]es [N]o";
        string lblModifyDetailPopUP = "Do You Want To Modify The Details [Y]es [N]o";
        string lblDeleteDetailPopUP = "Do You Want To Delete The Details [Y]es [N]o";
        string txtValue             = string.Empty;

        #endregion

        #region --Local Variable--
        DataTable dt = new DataTable();
        bool EmpAlreadyUpdated = false;
        private iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DeductionEntry _mainForm = null;
        //private iCORE.CHRIS.PRESENTATIONOBJECTS.Setup.CHRIS_Setup_DedDetailEntry _frmDedDetail = null;

        #endregion

        #region --Constructor--
        public CHRIS_Setup_DedDetailEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_DedDetailEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj, CHRIS_Setup_DeductionEntry mainForm, DataTable dgvTable)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            dtDetail = dgvTable;
            this._mainForm = mainForm;
        }

        #endregion

        #region --Method--

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                //if (dtDetail.Rows.Count == 0)
                //{
                //    dtDetail.Rows.Add();
                //}

                Font newFontStyle = new Font(dgvDedDetail.Font, FontStyle.Bold);
                dgvDedDetail.ColumnHeadersDefaultCellStyle.Font = newFontStyle;

                dgvDedDetail.GridSource = dtDetail;
                dgvDedDetail.DataSource = dgvDedDetail.GridSource;
                
                if(_mainForm.txtOption.Text == "V")
                    dgvDedDetail.ReadOnly = true;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                this.tbtAdd.Visible = false;
                this.tbtList.Visible = false;
                this.tbtSave.Visible = false;
                this.tbtCancel.Visible = false;
                this.tlbMain.Visible = false;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }


        /// <summary>
        /// CHECK_PRESENT(): CHECK WEATHER CURRENT EMPLOYEE ALREADY UPDATED OR NOT.
        /// </summary>
        /// <returns></returns>
        private bool check_present()
        {
            try
            {
                string strDeducCode = string.Empty;
                string strPersonnelNo = string.Empty;

                if (dgvDedDetail.RowCount > 1)
                {
                    strDeducCode = dgvDedDetail.CurrentRow.Cells["grdDeducCode"].Value.ToString();
                    strPersonnelNo = dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].Value.ToString();
                }
                Dictionary<string, object> param = new Dictionary<string, object>();
                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                param.Add("SP_DEDUC_CODE", strDeducCode);
                param.Add("SP_P_NO", strPersonnelNo);
                rsltCode = cmnDM.GetData("CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER", "CHECK_PRESENT", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Individual Deduction Code Should Be Unique This Deduction Is Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].Selected = true;
                    return true;
                }

                return false;

            }
            catch (Exception exp)
            {
                LogException(this.Name, "check_present", exp);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void go_fieldPage2()
        {
            try
            {
                DialogResult dr = MessageBox.Show("Do You Want To View More Record [Y]es [N]o", "Note", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    _mainForm.txt_SP_VALID_FROM_D.Value = null;
                    _mainForm.txt_SP_VALID_TO_D.Value = null;
                    _mainForm.txt_SP_DED_CODE.Select();
                    _mainForm.txt_SP_DED_CODE.Focus();
                }
                else if (dr == DialogResult.No)
                {
                    _mainForm.txt_SP_VALID_FROM_D.Value = null;
                    _mainForm.txt_SP_VALID_TO_D.Value = null;
                    _mainForm.txtOption.Text = "V";
                    _mainForm.txtOption.Focus();
                    _mainForm.txtOption.Select();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "go_fieldPage2", exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void go_fieldPage4()
        {
            try
            {
                DialogResult dr = MessageBox.Show("Do You Want To Go To Previous Page [Y]es [N]o", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dr == DialogResult.Yes)
                {
                    _mainForm.txt_SP_DED_CODE.Select();
                    _mainForm.txt_SP_DED_CODE.Focus();
                }
                else if (dr == DialogResult.No)
                {
                    dgvDedDetail.Rows[0].Cells[0].Selected = true;
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "go_fieldPage4", exp);
            }
        }

        #endregion

        #region --Events--

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvDedDetail_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dgvDedDetail.RowCount > 1)
                {
                    if (dgvDedDetail.CurrentCell.OwningColumn.Name == "grdPersonnelNo")
                    {
                        pr_p_no = dgvDedDetail.CurrentCell.EditedFormattedValue.ToString();
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        param.Add("SP_P_NO", pr_p_no);
                        rsltCode = cmnDM.GetData("CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER", "PR_P_NO", param);

                        g_sw = 0;
                        if (_mainForm.txtOption.Text == "M")
                        {
                            g_sw = 1;
                        }

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                        {
                            if (_mainForm.txtOption.Text == "V")
                            {
                                //go_fieldPage2();
                                this.Hide();
                                _mainForm.GoToPage2();
                            }
                            else if (_mainForm.txtOption.Text == "A")
                            {
                                //go_fieldPage4();
                                _mainForm.GoToPage6();
                            }
                            else if (_mainForm.txtOption.Text == "M")
                            {
                                this.Hide();
                                _mainForm.GoToPage4();
                                //clear_blkone_details(false,no_commit);
                                //clear_record;clear_block;
                                //go_block('blkone');clear_record;clear_block;
                            }
                            else if (_mainForm.txtOption.Text == "D")
                            {
                                //clear_blkone_details(false,no_commit);
                                //delete_record;
                                //clear_record;clear_block;
                                //go_block('blkone');clear_record;clear_block;
                            }
                        }
                        else if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            //dgvDedDetail.CurrentRow.Cells["grdName"].ReadOnly = true;
                            //dgvDedDetail.CurrentRow.Cells["grdRemarks"].DataGridView.Focus();// = false;
                            //dgvDedDetail.CurrentRow.Cells["grdRemarks"].Selected = true;
                            //dgvDedDetail.SelectedCells[0].DataGridView.EditingControl.Focus();
                            //dgvDedDetail.CurrentCell = dgvDedDetail[2, dgvDedDetail.CurrentRow.Index];
                        }
                    }
                    else if (dgvDedDetail.CurrentCell.OwningColumn.Name == "grdRemarks")
                    {
                        if (dgvDedDetail.CurrentCell.OwningColumn.Name == "grdRemarks" )
                        {
                            if (_mainForm.txtOption.Text == "A")
                            {
                                dt = dgvDedDetail.GridSource;
                                DataRow[] dRow = dt.Select("SP_P_NO = '" + dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].EditedFormattedValue.ToString().ToUpper() + "'");

                                if (dt != null && e.RowIndex <= dt.Rows.Count)
                                {
                                    if (dRow.Length > 0)
                                    {
                                        MessageBox.Show("This Employee Has Already Entered For This Deduction Code", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //e.Cancel = true;
                                        //dgvDedDetail.SelectedCells[0].DataGridView.EditingControl.Focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "dgvDedDetail_CellValidating", exp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvDedDetail_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDedDetail.RowCount > 1)
            {
                if (dgvDedDetail.CurrentCell.OwningColumn.Name == "grdPersonnelNo")
                {
                    //if (dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].Value.ToString() == string.Empty && dgvDedDetail.CurrentRow.Cells["grdRemarks"].Value.ToString() == string.Empty && dgvDedDetail.CurrentRow.Cells["grdName"].Value.ToString() == string.Empty)
                    {
                        //Page6 View Previous Page
                        //if (frmDedEntry.txtOption.Text == "A")
                        {
                            //go_field('blk_head.W_ANS4');
                            //frmPage6 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblPrevPagePopUP);
                            //frmPage6.Text = string.Empty;
                            //frmPage6.TextBox.MaxLength = 1;
                            //frmPage6.TextBox.Validating += new CancelEventHandler(Ans4Pg6TextBox_Validating);
                            //frmPage6.MdiParent = null;
                            //this.Hide();
                            //frmPage6.ShowDialog();
                            //frmPage6.Text = frmPage6.Value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvDedDetail_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                DataTable dt = (DataTable)this.dgvDedDetail.DataSource;
                if (dt != null && e.RowIndex < dt.Rows.Count)
                {
                    if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
                    {
                        this.grdPersonnelNo.SkipValidationOnLeave = true;
                    }
                    else
                    {
                        this.grdPersonnelNo.SkipValidationOnLeave = false;
                    }
                }
                else
                {
                    this.grdPersonnelNo.SkipValidationOnLeave = true;
                }
            }
        }

        /// <summary>
        /// AnsPg2TextBox_Validating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void AnsPg2TextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)sender;

                if (txt.Text != "Y" && txt.Text != "N")
                {
                    e.Cancel = true;
                }
                else if (txt.Text == "Y")
                {
                    frmPage2.Hide();
                    frmPage7.Hide();
                    //ClearForm(pnlDeductionMain.Controls);
                    _mainForm.txt_SP_DED_CODE.Select();
                    _mainForm.txt_SP_DED_CODE.Focus();
                }
                else if (txt.Text == "N")
                {
                    frmPage2.Hide();
                    frmPage7.Hide();
                    //ClearForm(pnlDeductionMain.Controls);
                    _mainForm.txtOption.Text = "V";
                    _mainForm.txtOption.Focus();
                    _mainForm.txtOption.Select();

                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "AnsPg2TextBox_Validating", exp);
            }
        }

        /*
       /// <summary>
       /// 
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
       //private void dgvDedDetail_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
       //{
       //    if (dgvDedDetail.RowCount > 1)
       //    {
       //        if (dgvDedDetail.CurrentCell.OwningColumn.Name == "grdPersonnelNo")
       //        {
       //            pr_p_no = dgvDedDetail.CurrentCell.EditedFormattedValue.ToString();

       //            if (_mainForm.g_option == "M")
       //            {
       //                g_sw = 1;
       //            }

       //            Dictionary<string, object> param = new Dictionary<string, object>();
       //            Result rsltCode;
       //            CmnDataManager cmnDM = new CmnDataManager();
       //            param.Add("PR_P_NO", pr_p_no);
       //            rsltCode = cmnDM.GetData("CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER", "PR_P_NO", param);

       //            if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
       //            {
       //                MessageBox.Show("Payroll Generated For The Given Date Range", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
       //            }
       //            else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count < 1)
       //            {
       //                if (_mainForm.txtOption.Text == "V")
       //                {
       //                    frmPage2 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2(lblViewPopUP);
       //                    frmPage2.Text = string.Empty;
       //                    frmPage2.TextBox.MaxLength = 1;
       //                    frmPage2.TextBox.Validating += new CancelEventHandler(AnsPg2TextBox_Validating);
       //                    frmPage2.MdiParent = null;
       //                    frmPage2.ShowDialog();
       //                    frmPage2.Text = frmPage2.Value;

       //                }
       //                else if (_mainForm.txtOption.Text == "A")
       //                {
       //                    //go_field('blk_head.W_ANS4');
       //                    frmPage6 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2( lblAddPopUP);
       //                    frmPage6.Text = string.Empty;
       //                    frmPage6.TextBox.MaxLength = 1;
       //                    frmPage6.TextBox.Validating += new CancelEventHandler(Ans4Pg6TextBox_Validating);
       //                    frmPage6.MdiParent = null;
       //                    frmPage6.ShowDialog();
       //                    frmPage6.Text = frmPage6.Value;
       //                }
       //                else if (_mainForm.txtOption.Text == "M")
       //                {
       //                    //frmDedEntry.Cancel();
       //                }
       //                else if (_mainForm.txtOption.Text == "D")
       //                {
       //                    //base.Delete();
       //                    //base.Cancel();
       //                }
       //            }
       //        }

       //        else if (dgvDedDetail.CurrentCell.OwningColumn.Name == "grdRemarks")
       //        {
       //            if (_mainForm.g_option == "A")
       //            {
       //                dt = dgvDedDetail.GridSource;
       //                DataRow[] dRow = dt.Select("SP_P_NO = " + dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].EditedFormattedValue);
       //                if (dRow[0].Table.Rows.Count > 1)
       //                {
       //                    MessageBox.Show("Individual Deduction Code Should Be Unique This Deduction Is Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
       //                    dgvDedDetail.SelectedCells[1].DataGridView.EditingControl.Focus();
       //                }
       //            }

       //            if (_mainForm.g_option == "M" && g_sw == 1)
       //            {
       //                 dt = dgvDedDetail.GridSource;
       //                 DataRow[] dRow = dt.Select("SP_P_NO = " + dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].EditedFormattedValue);
       //                 if (dRow[0].Table.Rows.Count > 1)
       //                 {
       //                     MessageBox.Show("Individual Deduction Code Should Be Unique This Deduction Is Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
       //                     dgvDedDetail.SelectedCells[1].DataGridView.EditingControl.Focus();
       //                     //dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].Selected = true;
       //                 }

       //                if (EmpAlreadyUpdated == false)
       //                {
       //                    //base.Save();
       //                }
       //            }
       //        }

       //        if (dgvDedDetail.CurrentRow.Cells["grdPersonnelNo"].Value == string.Empty && dgvDedDetail.CurrentRow.Cells["grdRemarks"].Value == string.Empty && dgvDedDetail.CurrentRow.Cells["grdName"].Value == string.Empty)
       //        {
       //            //Page6 View Previous Page
       //        }
       //    }
       //}
       */

        /// <summary>
        /// Ans4Pg6TextBox_Validating
        ///// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Ans4Pg6TextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                TextBox Ans4 = (TextBox)sender;
                if (Ans4.Text != "Y" && Ans4.Text != "N")
                {
                    MessageBox.Show(" Please Enter [Y]es  Or  [N]o", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else if (Ans4.Text == "Y")
                {
                    frmPage6.Hide();
                    frmDedEntry.txt_SP_DED_CODE.Select();
                    frmDedEntry.txt_SP_DED_CODE.Focus();
                }
                else if (Ans4.Text == "N")
                {
                    frmPage6.Hide();
                    this.Show();
                    dgvDedDetail.Rows[0].Cells[0].Selected = true;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Ans4Pg6TextBox_Validating", exp);
            }
        }

        #endregion

        private void dgvDedDetail_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDedDetail.CurrentRow != null && dgvDedDetail.CurrentCell.ColumnIndex == 3)
            {
                dgvDedDetail.CurrentCell = dgvDedDetail.CurrentRow.Cells[4];
                dgvDedDetail.BeginEdit(true);
                //dgvDedDetail.EditingControl.Select();
                //dgvDedDetail.EditingControl.Focus();
                //dgvDedDetail.EndEdit();
            }
        }

      

        private void dgvDedDetail_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3)
            {
                //dgvDedDetail.CurrentCell = dgvDedDetail.CurrentRow.Cells[4];
                //dgvDedDetail.BeginEdit(true);
                //dgvDedDetail.EditingControl.Select();
                //dgvDedDetail.EditingControl.Focus();
                //dgvDedDetail.EndEdit();
            }
        }

    }
}