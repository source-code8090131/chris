using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_AccountEntry : ChrisTabularForm
    {
        #region --Global Variable
        iCORE.CHRIS.PRESENTATIONOBJECTS.Personnel.CHRIS_Personnel_RegularStaffHiringEnt_Page2 frmPopUP;
        TextBox tbxPopUp        = new TextBox();
        CommonDataManager cmn   = new CommonDataManager();
        string lblViewPopUP     = "Do You Want To View More Record [Y]es [N]o";
        string lblDeletePopUP   = "Do You Want To Delete The Record [Y]es [N]o";
        string lblAddPopUP      = "Do You Want To Save The Record [Y]es [N]o";
        string txtValue         = string.Empty;        
        #endregion

        #region --Construstor--
        public CHRIS_Setup_AccountEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Setup_AccountEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            this.pnlHead.SendToBack();
            label1.Text += "    " + this.UserName;

            
        }
        #endregion

        #region Methods

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                DataTable dt = (DataTable)dgvAccEntry.GridSource;
                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                        DialogResult dr = MessageBox.Show("Do you want to save changes.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            bool isSaved = dgvAccEntry.SaveGrid(this.pnlTblAccountEntry);
                            if (isSaved)
                            {
                                MessageBox.Show("Changes Saved Successfully.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dt.AcceptChanges();
                                this.tbtSave.Enabled = false;
                            }
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
            if (actionType == "Cancel")
            {
                //this.dgvAccEntry.RejectChanges();
                //dgvAccEntry.EndEdit();
                //ClearForm(pnlTblAccountEntry.Controls);
                //return;
            }

            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        #endregion

        #region --Events--


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                Font newFontStyle = new Font(dgvAccEntry.Font, FontStyle.Bold);
                dgvAccEntry.ColumnHeadersDefaultCellStyle.Font = newFontStyle;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }


        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();
                tbtList.Visible         = false;
                this.txtOption.Visible  = false;
                this.FunctionConfig.EnableF1 = false;
                this.AccountNo.SkipValidationOnLeave = true;
                txtDate.Text            = this.Now().ToString("dd/MM/yyyy");
                this.CurrentPanelBlock  = this.pnlTblAccountEntry.Name;
                this.lblUserName.Text   = this.UserName;
                txtOption.Visible       = false;
                txt_W_USER.Text         = this.userID;
                txtLocation.Text        = this.CurrentLocation;

            }
            catch (Exception exp)
            {
                LogException(this.Name, "CommonOnLoadMethods", exp);
            }
        }

        
        protected override bool Quit()
        {

            if (dgvAccEntry.Rows.Count > 1)
            {
                this.Cancel();
            }
            else
            {
                base.Quit();
            }
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAccEntry_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (!dgvAccEntry.CurrentCell.IsInEditMode || operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Cancel)
                return;

            if (dgvAccEntry.RowCount > 1)
            {
                DataTable dt = (DataTable)this.dgvAccEntry.DataSource;

                if (dgvAccEntry.CurrentCell.OwningColumn.Name == "AccountNo")
                {
                    if (dgvAccEntry.CurrentCell.EditedFormattedValue.ToString() == string.Empty)
                    {
                        e.Cancel = true;
                    }

                    if (dgvAccEntry.CurrentCell.OwningColumn.Name == "AccountNo" && dgvAccEntry.CurrentCell.IsInEditMode == true )
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        param.Add( "SP_ACC_NO", dgvAccEntry.CurrentRow.Cells["AccountNo"].EditedFormattedValue.ToString().ToUpper());
                        rsltCode = cmnDM.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "CheckAccount", param);
                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                        }
                        else
                        {
                            if (dt != null)
                            {
                                DataRow[] dRow = dt.Select("SP_ACC_NO = '" + dgvAccEntry.CurrentRow.Cells["AccountNo"].EditedFormattedValue.ToString().ToUpper() + "'");

                                if (dt != null && e.RowIndex <= dt.Rows.Count)
                                {
                                    if (dRow.Length > 0)
                                    {
                                        MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        e.Cancel = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (dgvAccEntry.CurrentCell.OwningColumn.Name == "AccountType")
                {
                    if (dgvAccEntry.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "E" && dgvAccEntry.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "I" && dgvAccEntry.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "L" && dgvAccEntry.CurrentCell.EditedFormattedValue.ToString().ToUpper() != "A")
                    {
                        MessageBox.Show("Invalid Entry...Please Entry [E], [I], [L] or [A]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
        }

        private void dgvAccEntry_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvAccEntry.RowCount > 0)
            //{
            //    dgvAccEntry.EndEdit();
            //    DataTable dt = (DataTable)this.dgvAccEntry.DataSource;

            //    if (dgvAccEntry.CurrentCell.OwningColumn.Name == "AccountNo" && dt != null )
            //    {
            //        DataRow[] dRow = dt.Select("SP_ACC_NO = '"  + dgvAccEntry.CurrentRow.Cells["AccountNo"].EditedFormattedValue + "'");

            //        if (dgvAccEntry.CurrentRow.Cells["Description"].Value != null && dRow.Length > 0)
            //        {
            //            //if (dt.Rows[e.RowIndex- 1].RowState == DataRowState.Added)
            //            {
            //                MessageBox.Show("Record Already Entered", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //                base.Cancel();
            //            }
            //        }
            //    }
            //}
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAccEntry_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvAccEntry.CurrentCell.OwningColumn.Name == "AccountType")
            //{
            //    MessageBox.Show("Invalid Entry...Please Entry [E], [I], [L] or [A]", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }
        
        #endregion

        private void dgvAccEntry_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                DataTable dt = (DataTable)this.dgvAccEntry.DataSource;
                if (dt != null && e.RowIndex < dt.Rows.Count)
                {
                    if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
                    {
                        this.AccountNo.SkipValidationOnLeave = true;
                    }
                    else
                    {
                        this.AccountNo.SkipValidationOnLeave = false;
                    }
                }
                else
                {
                    this.AccountNo.SkipValidationOnLeave = true;
                }
            }
        }
    }
}