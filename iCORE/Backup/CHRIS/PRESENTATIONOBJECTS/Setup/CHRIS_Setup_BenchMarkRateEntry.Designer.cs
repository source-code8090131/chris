namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_BenchMarkRateEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_BenchMarkRateEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlsimpleSearch = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtben_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtnbenchDate = new CrplControlLibrary.LookupButton(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTblBenchmark = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDtGrdViwBenchmark = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.BEN_FIN_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.w_description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BEN_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ben_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.W_LOC = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlsimpleSearch.SuspendLayout();
            this.pnlTblBenchmark.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDtGrdViwBenchmark)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(538, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(574, 22);
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(574, 60);
            // 
            // pnlsimpleSearch
            // 
            this.pnlsimpleSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlsimpleSearch.ConcurrentPanels = null;
            this.pnlsimpleSearch.Controls.Add(this.dtben_Date);
            this.pnlsimpleSearch.Controls.Add(this.lbtnbenchDate);
            this.pnlsimpleSearch.Controls.Add(this.label1);
            this.pnlsimpleSearch.DataManager = null;
            this.pnlsimpleSearch.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlsimpleSearch.DependentPanels = null;
            this.pnlsimpleSearch.DisableDependentLoad = false;
            this.pnlsimpleSearch.EnableDelete = true;
            this.pnlsimpleSearch.EnableInsert = true;
            this.pnlsimpleSearch.EnableQuery = false;
            this.pnlsimpleSearch.EnableUpdate = true;
            this.pnlsimpleSearch.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SearchBenchmarkCommand";
            this.pnlsimpleSearch.Location = new System.Drawing.Point(12, 134);
            this.pnlsimpleSearch.MasterPanel = null;
            this.pnlsimpleSearch.Name = "pnlsimpleSearch";
            this.pnlsimpleSearch.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.pnlsimpleSearch.Size = new System.Drawing.Size(551, 34);
            this.pnlsimpleSearch.SPName = null;
            this.pnlsimpleSearch.TabIndex = 10;
            // 
            // dtben_Date
            // 
            this.dtben_Date.CustomEnabled = true;
            this.dtben_Date.CustomFormat = "dd/MM/yyyy";
            this.dtben_Date.DataFieldMapping = "BEN_DATE";
            this.dtben_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtben_Date.HasChanges = true;
            this.dtben_Date.IsRequired = true;
            this.dtben_Date.Location = new System.Drawing.Point(224, 5);
            this.dtben_Date.Name = "dtben_Date";
            this.dtben_Date.NullValue = " ";
            this.dtben_Date.Size = new System.Drawing.Size(93, 20);
            this.dtben_Date.TabIndex = 29;
            this.dtben_Date.Value = new System.DateTime(2010, 9, 28, 0, 0, 0, 0);
            this.dtben_Date.Leave += new System.EventHandler(this.dtben_Date_Leave);
            // 
            // lbtnbenchDate
            // 
            this.lbtnbenchDate.ActionLOVExists = "BenchDateLOVExists";
            this.lbtnbenchDate.ActionType = "BenchDateLOV";
            this.lbtnbenchDate.ConditionalFields = "";
            this.lbtnbenchDate.CustomEnabled = true;
            this.lbtnbenchDate.DataFieldMapping = "";
            this.lbtnbenchDate.DependentLovControls = "";
            this.lbtnbenchDate.HiddenColumns = "";
            this.lbtnbenchDate.Image = ((System.Drawing.Image)(resources.GetObject("lbtnbenchDate.Image")));
            this.lbtnbenchDate.LoadDependentEntities = true;
            this.lbtnbenchDate.Location = new System.Drawing.Point(323, 4);
            this.lbtnbenchDate.LookUpTitle = null;
            this.lbtnbenchDate.Name = "lbtnbenchDate";
            this.lbtnbenchDate.Size = new System.Drawing.Size(26, 21);
            this.lbtnbenchDate.SkipValidationOnLeave = true;
            this.lbtnbenchDate.SPName = "CHRIS_SP_BENCHMARK_MANAGER";
            this.lbtnbenchDate.TabIndex = 28;
            this.lbtnbenchDate.TabStop = false;
            this.lbtnbenchDate.Tag = "";
            this.lbtnbenchDate.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(185, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Date :";
            // 
            // pnlTblBenchmark
            // 
            this.pnlTblBenchmark.ConcurrentPanels = null;
            this.pnlTblBenchmark.Controls.Add(this.slDtGrdViwBenchmark);
            this.pnlTblBenchmark.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblBenchmark.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblBenchmark.DependentPanels = null;
            this.pnlTblBenchmark.DisableDependentLoad = false;
            this.pnlTblBenchmark.EnableDelete = true;
            this.pnlTblBenchmark.EnableInsert = true;
            this.pnlTblBenchmark.EnableQuery = false;
            this.pnlTblBenchmark.EnableUpdate = true;
            this.pnlTblBenchmark.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BenchmarkCommand";
            this.pnlTblBenchmark.Location = new System.Drawing.Point(12, 166);
            this.pnlTblBenchmark.MasterPanel = null;
            this.pnlTblBenchmark.Name = "pnlTblBenchmark";
            this.pnlTblBenchmark.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblBenchmark.Size = new System.Drawing.Size(550, 166);
            this.pnlTblBenchmark.SPName = "CHRIS_SP_BENCHMARK_MANAGER";
            this.pnlTblBenchmark.TabIndex = 11;
            // 
            // slDtGrdViwBenchmark
            // 
            this.slDtGrdViwBenchmark.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDtGrdViwBenchmark.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.slDtGrdViwBenchmark.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDtGrdViwBenchmark.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BEN_FIN_TYPE,
            this.w_description,
            this.BEN_RATE,
            this.ID,
            this.Ben_Date});
            this.slDtGrdViwBenchmark.ColumnToHide = null;
            this.slDtGrdViwBenchmark.ColumnWidth = null;
            this.slDtGrdViwBenchmark.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.slDtGrdViwBenchmark.DefaultCellStyle = dataGridViewCellStyle4;
            this.slDtGrdViwBenchmark.DisplayColumnWrapper = null;
            this.slDtGrdViwBenchmark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slDtGrdViwBenchmark.GridDefaultRow = 0;
            this.slDtGrdViwBenchmark.Location = new System.Drawing.Point(0, 0);
            this.slDtGrdViwBenchmark.Name = "slDtGrdViwBenchmark";
            this.slDtGrdViwBenchmark.ReadOnlyColumns = null;
            this.slDtGrdViwBenchmark.RequiredColumns = null;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDtGrdViwBenchmark.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.slDtGrdViwBenchmark.Size = new System.Drawing.Size(550, 166);
            this.slDtGrdViwBenchmark.SkippingColumns = null;
            this.slDtGrdViwBenchmark.TabIndex = 0;
            this.slDtGrdViwBenchmark.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.slDtGrdViwBenchmark_CellValidating);
            // 
            // BEN_FIN_TYPE
            // 
            this.BEN_FIN_TYPE.DataPropertyName = "BEN_FIN_TYPE";
            this.BEN_FIN_TYPE.HeaderText = "Finance Type";
            this.BEN_FIN_TYPE.MaxInputLength = 6;
            this.BEN_FIN_TYPE.Name = "BEN_FIN_TYPE";
            this.BEN_FIN_TYPE.ReadOnly = true;
            this.BEN_FIN_TYPE.Width = 220;
            // 
            // w_description
            // 
            this.w_description.DataPropertyName = "w_description";
            this.w_description.HeaderText = "Description";
            this.w_description.MaxInputLength = 20;
            this.w_description.Name = "w_description";
            this.w_description.ReadOnly = true;
            this.w_description.Width = 225;
            // 
            // BEN_RATE
            // 
            this.BEN_RATE.DataPropertyName = "BEN_RATE";
            dataGridViewCellStyle2.Format = "##0.000000";
            dataGridViewCellStyle2.NullValue = null;
            this.BEN_RATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.BEN_RATE.HeaderText = "Benchmark Rate(%)";
            this.BEN_RATE.MaxInputLength = 6;
            this.BEN_RATE.Name = "BEN_RATE";
            this.BEN_RATE.Width = 135;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            this.ID.Width = 10;
            // 
            // Ben_Date
            // 
            this.Ben_Date.DataPropertyName = "BEN_DATE";
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.Ben_Date.DefaultCellStyle = dataGridViewCellStyle3;
            this.Ben_Date.HeaderText = "Ben_Date";
            this.Ben_Date.Name = "Ben_Date";
            this.Ben_Date.Visible = false;
            this.Ben_Date.Width = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(220, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Benchmark Rate Entry";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(468, 100);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(84, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 20;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(420, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Date :";
            // 
            // W_LOC
            // 
            this.W_LOC.AllowSpace = true;
            this.W_LOC.AssociatedLookUpName = "";
            this.W_LOC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LOC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LOC.ContinuationTextBox = null;
            this.W_LOC.CustomEnabled = true;
            this.W_LOC.DataFieldMapping = "";
            this.W_LOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LOC.GetRecordsOnUpDownKeys = false;
            this.W_LOC.IsDate = false;
            this.W_LOC.Location = new System.Drawing.Point(62, 101);
            this.W_LOC.Name = "W_LOC";
            this.W_LOC.NumberFormat = "###,###,##0.00";
            this.W_LOC.Postfix = "";
            this.W_LOC.Prefix = "";
            this.W_LOC.ReadOnly = true;
            this.W_LOC.Size = new System.Drawing.Size(84, 20);
            this.W_LOC.SkipValidation = false;
            this.W_LOC.TabIndex = 24;
            this.W_LOC.TabStop = false;
            this.W_LOC.TextType = CrplControlLibrary.TextType.String;
            this.W_LOC.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(62, 73);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(86, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 23;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(22, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Loc :";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(22, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "User :";
            this.label12.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(234, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Personnel System";
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(371, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(78, 13);
            this.txtUserName.TabIndex = 119;
            this.txtUserName.Text = "User  Name :   ";
            // 
            // CHRIS_Setup_BenchMarkRateEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 408);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.W_LOC);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pnlTblBenchmark);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pnlsimpleSearch);
            this.Controls.Add(this.txtDate);
            this.CurrentPanelBlock = "pnlsimpleSearch";
            this.F4OptionText = "[F4]=Save Record";
            this.Name = "CHRIS_Setup_BenchMarkRateEntry";
            this.ShowBottomBar = true;
            this.ShowF4Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Benchmark Rate Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.pnlsimpleSearch, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.pnlTblBenchmark, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.W_LOC, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlsimpleSearch.ResumeLayout(false);
            this.pnlsimpleSearch.PerformLayout();
            this.pnlTblBenchmark.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDtGrdViwBenchmark)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlsimpleSearch;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblBenchmark;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox W_LOC;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDtGrdViwBenchmark;
        private CrplControlLibrary.LookupButton lbtnbenchDate;
        private CrplControlLibrary.SLDatePicker dtben_Date;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BEN_FIN_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn w_description;
        private System.Windows.Forms.DataGridViewTextBoxColumn BEN_RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ben_Date;
    }
}