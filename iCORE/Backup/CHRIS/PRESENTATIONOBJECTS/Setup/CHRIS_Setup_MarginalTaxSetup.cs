using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_MarginalTaxSetup : ChrisTabularForm
    {

        bool valid = false;
        public CHRIS_Setup_MarginalTaxSetup()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_MarginalTaxSetup(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                if (this.validate())
                {
                    base.DoToolbarActions(ctrlsCollection, "Save");

                }
                else
                {
                    MessageBox.Show("Please Provides Missing Values.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            if (actionType == "Search")
            {
                tbtCancel.Enabled = true;
            }
            
            if (actionType != "Save")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
            }


        }


        private bool validate()
        {
            valid = true;
            if (dgvRecords.Rows.Count > 0)
            {
                for (int j = 0; j < dgvRecords.Rows.Count -1; j++)
                {
                    if (!this.dgvRecords.Rows[j].IsNewRow)
                    {
                        
                        for (int i = 0; i < 5; i++)
                        {
                            if ((this.dgvRecords[i,j].Value == DBNull.Value))
                            {
                                valid = false;
                                return valid;
                            }
                        }
                    }
                }
                return valid;

            }
            return valid;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.dgvRecords.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            this.FormTitleColor = Color.Black;
            this.txtUserName.Text += "  " + this.UserName;
            tbtCancel.Enabled = true;

            dgvRecords.Enabled = true;
            txtOption.Visible = false;
            //this.FunctionConfig.EnableF10 = true;
            //this.FunctionConfig.F10 = Function.Save;

        }



        protected override bool Quit()
        {

            if (dgvRecords.Rows.Count > 1)
            {
                this.Cancel();
            }
            else
            {
                base.Quit();
            }
            return false;
        }


        private void dgvRecords_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
           
            //if (dgvRecords.CurrentCell.OwningColumn == dgvRecords.Columns["Percentage"])
            //{
            //    if (dgvRecords.CurrentCell != null && !String.IsNullOrEmpty(dgvRecords.CurrentCell.EditedFormattedValue.ToString()))
            //    {
            //        double val = 0;
            //        if (double.TryParse(dgvRecords.CurrentCell.EditedFormattedValue.ToString(), out val))
            //        {
            //            if (val > 100)
            //            {
            //                MessageBox.Show("Percentage should not be greater than 100", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //                e.Cancel = true;
            //            }
            //        }
            //    }
            //}
        }
    }
}