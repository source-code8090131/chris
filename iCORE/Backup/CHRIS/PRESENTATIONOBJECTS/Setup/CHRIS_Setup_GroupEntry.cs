using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_GroupEntry : ChrisTabularForm
    {
        public CHRIS_Setup_GroupEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_GroupEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            DGVGroup.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
             
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            txtUserName.Text += "   " + this.UserName;
            tbtSave.Enabled = false;
            
        }

        protected override bool Quit()
        {

            if (DGVGroup.Rows.Count > 1)
            {
                this.Cancel();
            }
            else
            {
                base.Quit();
            }
            return false;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                DataTable dt = (DataTable)DGVGroup.DataSource;
                if (dt != null)
                {
                    //ALL MENDATORY FIELDS CHECKING//
                    if (dt.Rows.Count > 0 && Convert.ToString(dt.Rows[0]["SP_DATE_FROM"]) != string.Empty && Convert.ToString(dt.Rows[0]["SP_DATE_TO"]) != string.Empty && Convert.ToString(dt.Rows[0]["SP_SEGMENT"]) != string.Empty && Convert.ToString(dt.Rows[0]["SP_GROUP_CODE"]) != string.Empty && Convert.ToString(dt.Rows[0]["SP_DESC_1"]) != string.Empty && Convert.ToString(dt.Rows[0]["SP_GROUP_HEAD"]) != string.Empty)
                    {
        
                        if (DateTime.Compare(Convert.ToDateTime(dt.Rows[0]["SP_DATE_FROM"]), Convert.ToDateTime(dt.Rows[0]["SP_DATE_TO"])) > 0)
                        {
                            MessageBox.Show("\"To\"  Date Must be Greater Than the  \"From\"  Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            tbtSave.Enabled = false;
                            return;
                        }
                        else
                        {
                            tbtSave.Enabled = true;
                        }


                        DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                        DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                        if (dtAdded != null || dtUpdated != null)
                        {

                            for (int i = 0; i < DGVGroup.Rows.Count - 1; i++)
                            {
                                if (DGVGroup.Rows[i].Cells[0].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[i].Cells[1].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[i].Cells[2].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[i].Cells[4].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[i].Cells[5].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[i].Cells[6].EditedFormattedValue.ToString() == "")
                                {
                                    DGVGroup.Rows[i].Selected =true;
                                    return;
                                }

                            }
                            
                           
                            //foreach (DataGridViewRow row in DGVGroup.Rows)
                            //{
                            //    if ((Convert.ToString(row.Cells[0].Value) == "") || (Convert.ToString(row.Cells[1].Value) == "") || (Convert.ToString(row.Cells[2].Value) == "") || (Convert.ToString(row.Cells[4].Value) == "") || (Convert.ToString(row.Cells[5].Value) == "") || (Convert.ToString(row.Cells[6].Value) == ""))
                            //    {
                            //        //tbtSave.Enabled = false;
                            //        DGVGroup.Focus();
                            //        return;
                            //    }
                           
                            //}

                            DialogResult dr = MessageBox.Show("Do You Want to Save Changes.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr == DialogResult.Yes)
                            {
                                bool isSaved = DGVGroup.SaveGrid(this.pnlGroup);
                                if (isSaved)
                                {
                                    MessageBox.Show("Changes Saved Successfully.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    dt.AcceptChanges();
                                    this.tbtSave.Enabled = false;
                                }
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        return; //Incase of Blank Dates//
                    }
                }
            }
                
            if (actionType != "Close" && actionType != "Cancel" && this.txtOption.Focused)
            {
                return;
            }
       

            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        private void DGVGroup_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {

            bool ValidateGrid = true;
            bool istrueJoiningDate = false;
            bool istrueFromDate = false;
            bool istrueDateTo = false;
            DateTime Job_From;
            DateTime Job_To;

            if (e.ColumnIndex == 0)
            {
                if (DGVGroup.Rows[e.RowIndex].Cells[0].IsInEditMode)
                {

                    if ((DGVGroup.Rows[e.RowIndex].Cells["SP_SEGMENT"].EditedFormattedValue.ToString().ToUpper() != "GF" && DGVGroup.Rows[e.RowIndex].Cells["SP_SEGMENT"].EditedFormattedValue.ToString().ToUpper() != "GCB") || DGVGroup.Rows[e.RowIndex].Cells["SP_SEGMENT"].EditedFormattedValue.ToString() == string.Empty)
                    {
                        MessageBox.Show("Invalid Entry, Enter GCB Or GF");

                        e.Cancel = true;

                    }
                }
                DGVGroup.Rows[e.RowIndex].Cells["SP_SEGMENT"].Value = DGVGroup.Rows[e.RowIndex].Cells["SP_SEGMENT"].EditedFormattedValue.ToString().ToUpper();

            }



            if (e.ColumnIndex == 5)
            {
                if (DGVGroup.Rows[e.RowIndex].Cells[5].IsInEditMode)
                {
                    tbtSave.Enabled = false;
                }

            }


            if (DGVGroup.CurrentCell.OwningColumn.Name == "SP_DATE_TO" && DGVGroup.CurrentCell.Value != null)
            {
                if (DGVGroup.CurrentCell.EditedFormattedValue != "")
                {
                    istrueDateTo = IsValidExpression(DGVGroup.CurrentCell.EditedFormattedValue.ToString(), InputValidator.DATE_REGEX);
                    istrueFromDate = IsValidExpression(DGVGroup.CurrentRow.Cells["SP_DATE_FROM"].EditedFormattedValue.ToString(), InputValidator.DATE_REGEX);

                    if (istrueDateTo == true && istrueFromDate == true )
                    {
                        Job_To = Convert.ToDateTime(DGVGroup.CurrentCell.EditedFormattedValue.ToString());
                        Job_From = Convert.ToDateTime(DGVGroup.CurrentRow.Cells["SP_DATE_FROM"].EditedFormattedValue.ToString());


                        if (DateTime.Compare(Job_To, Job_From) < 0)
                        {
                            MessageBox.Show("\"To\"  Date Must be Greater Than the  \"From\"  Date", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ValidateGrid = false;
                            e.Cancel = true;
                            tbtSave.Enabled = false;
                        }
                    }
                }
            }





        }

        private void DGVGroup_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                DataTable dt = (DataTable)this.DGVGroup.DataSource;
                if (dt != null && e.RowIndex < dt.Rows.Count)
                {
                    if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
                    {
                        this.SP_SEGMENT.SkipValidationOnLeave = true;
                    }
                    else
                    {
                        this.SP_SEGMENT.SkipValidationOnLeave = false;
                    }
                }
                else
                {
                    this.SP_SEGMENT.SkipValidationOnLeave = true;
                }
            }
        }

        private void DGVGroup_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {


            if (DGVGroup.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[e.RowIndex].Cells[2].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[e.RowIndex].Cells[4].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[e.RowIndex].Cells[5].EditedFormattedValue.ToString() == "" || DGVGroup.Rows[e.RowIndex].Cells[6].EditedFormattedValue.ToString() == "")
            {
                //e.Cancel = true;
                return;
            }


        }

      


    }
}