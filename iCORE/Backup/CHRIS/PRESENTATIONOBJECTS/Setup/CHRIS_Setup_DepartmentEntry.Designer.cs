namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_DepartmentEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_DepartmentEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlGroup = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.DGVGroup = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_SEGMENT_D = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.SP_GROUP_D = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.SP_DEPT = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.SP_DESC_D1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DESC_D2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DEPT_HEAD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DATE_FROM_D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_DATE_TO_D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVGroup)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 508);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlGroup
            // 
            this.pnlGroup.ConcurrentPanels = null;
            this.pnlGroup.Controls.Add(this.DGVGroup);
            this.pnlGroup.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlGroup.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlGroup.DependentPanels = null;
            this.pnlGroup.DisableDependentLoad = false;
            this.pnlGroup.EnableDelete = true;
            this.pnlGroup.EnableInsert = true;
            this.pnlGroup.EnableQuery = false;
            this.pnlGroup.EnableUpdate = true;
            this.pnlGroup.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand";
            this.pnlGroup.Location = new System.Drawing.Point(9, 157);
            this.pnlGroup.MasterPanel = null;
            this.pnlGroup.Name = "pnlGroup";
            this.pnlGroup.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlGroup.Size = new System.Drawing.Size(650, 345);
            this.pnlGroup.SPName = "CHRIS_SP_DEPT_MANAGER";
            this.pnlGroup.TabIndex = 15;
            // 
            // DGVGroup
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_SEGMENT_D,
            this.SP_GROUP_D,
            this.SP_DEPT,
            this.SP_DESC_D1,
            this.SP_DESC_D2,
            this.SP_DEPT_HEAD,
            this.SP_DATE_FROM_D,
            this.SP_DATE_TO_D,
            this.ID});
            this.DGVGroup.ColumnToHide = null;
            this.DGVGroup.ColumnWidth = null;
            this.DGVGroup.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVGroup.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVGroup.DisplayColumnWrapper = null;
            this.DGVGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVGroup.GridDefaultRow = 0;
            this.DGVGroup.Location = new System.Drawing.Point(0, 0);
            this.DGVGroup.Name = "DGVGroup";
            this.DGVGroup.ReadOnlyColumns = null;
            this.DGVGroup.RequiredColumns = "SP_SEGMENT_D,SP_GROUP_D,SP_DEPT,SP_DESC_D1,SP_DEPT_HEAD,SP_DATE_FROM_D,SP_DATE_TO" +
                "_D";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVGroup.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVGroup.Size = new System.Drawing.Size(650, 345);
            this.DGVGroup.SkippingColumns = null;
            this.DGVGroup.TabIndex = 1;
            this.DGVGroup.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVGroup_CellValidating);
            this.DGVGroup.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGVGroup_EditingControlShowing);
            // 
            // SP_SEGMENT_D
            // 
            this.SP_SEGMENT_D.ActionLOV = "sp_Segment_LOVs";
            this.SP_SEGMENT_D.ActionLOVExists = "sp_Segment_LOVsExists";
            this.SP_SEGMENT_D.AttachParentEntity = false;
            this.SP_SEGMENT_D.DataPropertyName = "SP_SEGMENT_D";
            this.SP_SEGMENT_D.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand";
            this.SP_SEGMENT_D.HeaderText = "Segment";
            this.SP_SEGMENT_D.LookUpTitle = "Segment";
            this.SP_SEGMENT_D.LOVFieldMapping = "SP_SEGMENT_D";
            this.SP_SEGMENT_D.MaxInputLength = 3;
            this.SP_SEGMENT_D.MinimumWidth = 10;
            this.SP_SEGMENT_D.Name = "SP_SEGMENT_D";
            this.SP_SEGMENT_D.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_SEGMENT_D.SearchColumn = "SP_SEGMENT_D";
            this.SP_SEGMENT_D.SkipValidationOnLeave = false;
            this.SP_SEGMENT_D.SpName = "CHRIS_SP_DEPT_MANAGER";
            this.SP_SEGMENT_D.Width = 60;
            // 
            // SP_GROUP_D
            // 
            this.SP_GROUP_D.ActionLOV = "SP_Group_LOVs";
            this.SP_GROUP_D.ActionLOVExists = "SP_Group_LOVsExists";
            this.SP_GROUP_D.AttachParentEntity = false;
            this.SP_GROUP_D.DataPropertyName = "SP_GROUP_D";
            this.SP_GROUP_D.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand";
            this.SP_GROUP_D.HeaderText = "Group  Abb.";
            this.SP_GROUP_D.LookUpTitle = "Group";
            this.SP_GROUP_D.LOVFieldMapping = "sp_group_d";
            this.SP_GROUP_D.MaxInputLength = 5;
            this.SP_GROUP_D.Name = "SP_GROUP_D";
            this.SP_GROUP_D.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_GROUP_D.SearchColumn = "sp_group_d";
            this.SP_GROUP_D.SkipValidationOnLeave = false;
            this.SP_GROUP_D.SpName = "CHRIS_SP_DEPT_MANAGER";
            this.SP_GROUP_D.Width = 70;
            // 
            // SP_DEPT
            // 
            this.SP_DEPT.ActionLOV = "SP_Dept_LOV3";
            this.SP_DEPT.ActionLOVExists = "SP_Dept_LOV3Exists";
            this.SP_DEPT.AttachParentEntity = false;
            this.SP_DEPT.DataPropertyName = "SP_DEPT";
            this.SP_DEPT.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DepartmentEntryCommand";
            this.SP_DEPT.HeaderText = "Dept. Abb. ";
            this.SP_DEPT.LookUpTitle = "Dept Abb";
            this.SP_DEPT.LOVFieldMapping = "sp_dept";
            this.SP_DEPT.MaxInputLength = 5;
            this.SP_DEPT.Name = "SP_DEPT";
            this.SP_DEPT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SP_DEPT.SearchColumn = "sp_dept";
            this.SP_DEPT.SkipValidationOnLeave = false;
            this.SP_DEPT.SpName = "CHRIS_SP_DEPT_MANAGER";
            this.SP_DEPT.Width = 70;
            // 
            // SP_DESC_D1
            // 
            this.SP_DESC_D1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SP_DESC_D1.DataPropertyName = "SP_DESC_D1";
            this.SP_DESC_D1.HeaderText = "Description1";
            this.SP_DESC_D1.MaxInputLength = 20;
            this.SP_DESC_D1.Name = "SP_DESC_D1";
            // 
            // SP_DESC_D2
            // 
            this.SP_DESC_D2.DataPropertyName = "SP_DESC_D2";
            this.SP_DESC_D2.HeaderText = "Description2";
            this.SP_DESC_D2.MaxInputLength = 20;
            this.SP_DESC_D2.Name = "SP_DESC_D2";
            // 
            // SP_DEPT_HEAD
            // 
            this.SP_DEPT_HEAD.DataPropertyName = "SP_DEPT_HEAD";
            this.SP_DEPT_HEAD.HeaderText = "Dept.Head";
            this.SP_DEPT_HEAD.MaxInputLength = 20;
            this.SP_DEPT_HEAD.Name = "SP_DEPT_HEAD";
            // 
            // SP_DATE_FROM_D
            // 
            this.SP_DATE_FROM_D.DataPropertyName = "SP_DATE_FROM_D";
            this.SP_DATE_FROM_D.HeaderText = "From";
            this.SP_DATE_FROM_D.MaxInputLength = 10;
            this.SP_DATE_FROM_D.Name = "SP_DATE_FROM_D";
            this.SP_DATE_FROM_D.Width = 90;
            // 
            // SP_DATE_TO_D
            // 
            this.SP_DATE_TO_D.DataPropertyName = "SP_DATE_TO_D";
            this.SP_DATE_TO_D.HeaderText = "To";
            this.SP_DATE_TO_D.MaxInputLength = 10;
            this.SP_DATE_TO_D.Name = "SP_DATE_TO_D";
            this.SP_DATE_TO_D.Width = 90;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            this.ID.Width = 40;
            // 
            // pnlHead
            // 
            this.pnlHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(9, 76);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(650, 65);
            this.pnlHead.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(228, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(225, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "DEPARTMENT ENTRY";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(245, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(194, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Set Up";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(534, 36);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(491, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 36);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 10);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(416, 9);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(75, 13);
            this.txtUserName.TabIndex = 122;
            this.txtUserName.Text = "User  Name :  ";
            // 
            // CHRIS_Setup_DepartmentEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 568);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.pnlGroup);
            this.CurrentPanelBlock = "pnlGroup";
            this.Name = "CHRIS_Setup_DepartmentEntry";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS :  Department Entry";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlGroup, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVGroup)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlGroup;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtUserName;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVGroup;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_SEGMENT_D;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_GROUP_D;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn SP_DEPT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DESC_D1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DESC_D2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DEPT_HEAD;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DATE_FROM_D;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_DATE_TO_D;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}