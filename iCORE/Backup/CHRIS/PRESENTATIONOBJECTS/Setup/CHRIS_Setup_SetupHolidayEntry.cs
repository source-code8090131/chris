using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_SetupHolidayEntry : ChrisTabularForm
    {


        Dictionary<string, object> objVals = new Dictionary<string, object>();
        DataTable dtHday = new DataTable();
        Result rslt;
        CmnDataManager cmnDM = new CmnDataManager();

        
        
        public CHRIS_Setup_SetupHolidayEntry()
        {
            InitializeComponent();
        }


        public CHRIS_Setup_SetupHolidayEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            DGVHoliday.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            

        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            this.txtUserName.Text += "  " + this.UserName;
            
            this.txtOption.Visible = false;
            this.SP_HOL_DATE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);

            this.SP_H_DESC.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold);
            this.SP_H_DESC.Width = 520;

            //forcefully changed for restricting the LOV window to open//
            SP_HOL_DATE.SkipValidationOnLeave = true;

           
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            if (actionType == "Save")
            {
                tbtSave.Enabled = true; 
                DataTable dt = (DataTable)DGVHoliday.DataSource;

                if (dt != null)
                {

                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);


                    if (dtAdded != null)
                    {

                        if (Convert.ToString(dtAdded.Rows[0]["SP_HOL_DATE"]) !=string.Empty  && Convert.ToString(dtAdded.Rows[0]["SP_H_DESC"]) != "" && Convert.ToString(dtAdded.Rows[0]["SP_H_DESC"]) != string.Empty)
                        {
                            DialogResult dr = MessageBox.Show("Do You Want to Save the Record [Y]es or [N]o ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (dr == DialogResult.No)
                            {
                                return;
                            }
                            else
                            {
                                this.DGVHoliday.SaveGrid(this.pnltblHoliday);
                                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
                                return;

                            }
                        }
                        else
                        {
                            return;
                        }

                    }
                    if (dtUpdated != null)
                    {

                        if (Convert.ToString(dtUpdated.Rows[0]["SP_HOL_DATE"]) != string.Empty && Convert.ToString(dtUpdated.Rows[0]["SP_H_DESC"]) != "" && Convert.ToString(dtUpdated.Rows[0]["SP_H_DESC"]) != string.Empty)
                        {
                            DialogResult dr = MessageBox.Show("Do You Want to Save the Record [Y]es or [N]o ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (dr == DialogResult.No)
                            {

                                return;

                            }
                            else
                            {

                                this.DGVHoliday.SaveGrid(this.pnltblHoliday);
                                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtSearch"]));
                                return;

                            }
                        }
                        else
                        {
                            return;
                        }

                    }
                }
                else
                {
                    return;
                   
                }
                
            }

            //if (actionType != "Close" && actionType != "Cancel" )
            //{
            //    return;
            //}
            if (actionType == "Cancel")
            {
                //this.ClearForm(pnltblHoliday.Controls);
                ////this.DGVHoliday.RejectChanges();
                //return;
            }
            //else
            //{
                base.DoToolbarActions(ctrlsCollection, actionType);
            //}

        }




        private bool Exists(DateTime code)
        {
            bool flag = false;
            DataTable dt = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("SP_HOL_DATE", code);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData("CHRIS_SP_HOLIDAY_MANAGER", "LovDateExist", param);

            flag = (rsltCode.isSuccessful
                      && rsltCode.dstResult.Tables.Count > 0
                      && rsltCode.dstResult.Tables[0].Rows.Count > 0);

            return flag;
        }

        private void DGVHoliday_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DateTime dTime = new DateTime(1900, 1, 1);
            DateTime dtHoliday;
            System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
            dtf.ShortDatePattern = "dd/MM/yyyy";
           
            
            if (this.tbtClose.Pressed)
                return;


            //if (e.ColumnIndex == 1)
            //{

            //    if (DGVHoliday.CurrentRow.Cells[1].EditedFormattedValue.ToString() != "")
            //    {
            //        tbtSave.Enabled = true;
            //    }

            //}

            if (DGVHoliday.CurrentCell.OwningColumn.Name == "SP_HOL_DATE" && DGVHoliday.CurrentCell.EditedFormattedValue.ToString() != string.Empty)
            {
                if (DGVHoliday.CurrentCell.EditedFormattedValue.ToString() != "")
                {

           
                    try
                    {

                        if (DGVHoliday.CurrentRow.Cells[0].IsInEditMode)
                        {

                            //tbtSave.Enabled = false;

                            dTime = DateTime.Parse((DGVHoliday.CurrentCell.EditedFormattedValue.ToString()), dtf);
                            dtHoliday = DateTime.Parse(DGVHoliday.CurrentCell.EditedFormattedValue.ToString(), dtf);


                            if (dtHoliday != null && dtHoliday.ToString() != string.Empty)
                            {


                                objVals.Clear();
                                objVals.Add("SP_HOL_DATE", dtHoliday.ToShortDateString());
                                rslt = cmnDM.GetData("CHRIS_SP_HOLIDAY_MANAGER", "RecordMatch", objVals);
                                if (rslt.isSuccessful)
                                {
                                    dtHday = rslt.dstResult.Tables[0];
                                    if (Convert.ToDecimal(dtHday.Rows[0]["HolCount"]) > 0)
                                    {
                                        MessageBox.Show("Record Already Entered.");
                                        e.Cancel = true;
                                        return;
                                    }

                                }
                               
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        if (dTime == new DateTime(1900, 1, 1))
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }





            if (DGVHoliday.CurrentRow.Cells[0].IsInEditMode)
            {

                if (e.ColumnIndex == 0)
                {
                  
                    if (DGVHoliday.CurrentRow.Cells[0].EditedFormattedValue.ToString() == "")
                    {
                        e.Cancel = true;
                        return;
                    }

                    if (DGVHoliday.CurrentRow.Cells[0].EditedFormattedValue.ToString() != "")
                    {
                        dTime = DateTime.Parse((DGVHoliday.CurrentCell.EditedFormattedValue.ToString()), dtf);
                        dtHoliday = DateTime.Parse(DGVHoliday.CurrentCell.EditedFormattedValue.ToString(), dtf);

                    
                    if ( (dtHoliday != null && dtHoliday.ToString() != string.Empty) || (DGVHoliday.CurrentRow.Cells[0].EditedFormattedValue.ToString()) != "")
                    {

                        objVals.Clear();
                        objVals.Add("SP_HOL_DATE", DGVHoliday.CurrentRow.Cells[0].EditedFormattedValue.ToString());
                        rslt = cmnDM.GetData("CHRIS_SP_HOLIDAY_MANAGER", "FridayMatch", objVals);
                        if (rslt.isSuccessful)
                        {

                            
                            if (rslt.dstResult.Tables[0].Rows.Count > 0 && rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "Friday")
                            {
                                MessageBox.Show("Given Date Is Friday");
                                e.Cancel = true;
                                return;
                            }

                        }
                    }
                  }

                }
            
            }




            

        }


        private void DGVHoliday_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dt = (DataTable)this.DGVHoliday.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                if (e.RowIndex < dt.Rows.Count - 1)
                {
                    if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
                    {
                        //SP_HOL_DATE.SkipValidationOnLeave = true;
                       
                    }
                    else
                    {
                        //SP_HOL_DATE.SkipValidationOnLeave = false;
                    }
                }
                else
                {
                    //SP_HOL_DATE.SkipValidationOnLeave = true;
                }
            }
            else
            {
                //SP_HOL_DATE.SkipValidationOnLeave = true;
            }
        }

        private void DGVHoliday_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null)
            {
                e.Value = e.Value.ToString().ToUpper();
                e.FormattingApplied = true;

            }



        }

        private void DGVHoliday_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {

            if (tlbMain.Items["tbtSave"].Pressed )
            {

                if ((DGVHoliday.CurrentRow.Cells["SP_H_DESC"].EditedFormattedValue.ToString() == string.Empty)
                     && (!DGVHoliday.CurrentRow.IsNewRow) )
                {
                    //MessageBox.Show("Please fill all mandatory columns.");
                    e.Cancel = true;
         

                }
            }


        }
     

    }
}