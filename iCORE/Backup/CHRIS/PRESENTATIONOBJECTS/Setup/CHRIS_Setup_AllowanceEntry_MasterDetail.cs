using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_AllowanceEntry_MasterDetail : ChrisMasterDetailForm
    {

        #region --Constructor--
        public CHRIS_Setup_AllowanceEntry_MasterDetail()
        {
            InitializeComponent();
        }



        public CHRIS_Setup_AllowanceEntry_MasterDetail(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(this.pnlTblDedDetail);

            this.pnlAllowanceMain.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlAllowanceMain);

        }
        #endregion


        #region --Method--
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                txtOption.Visible = false;
                tbtAdd.Visible = false;
                tbtCancel.Visible = false;
                tbtClose.Visible = false;
                tbtDelete.Visible = false;
                tbtEdit.Visible = false;
                tbtList.Visible = false;
                tbtSave.Visible = false;

                //txtOption.Visible           = false;
                txt_SP_VALID_FROM.Value = null;
                txt_SP_VALID_TO.Value = null;

            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        #endregion

     
    }
}