using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_GlobalParameterSetup : ChrisTabularForm
    {
        bool hasAllUserWarningBeenShown = false; 

        #region Constructor

        public CHRIS_Setup_GlobalParameterSetup()
        {
            InitializeComponent();
            this.SetFormTitle = String.Empty;
        }

        public CHRIS_Setup_GlobalParameterSetup(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Overrides Base Common Onload Method
        /// </summary>
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtAdd.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            txtOption.Visible = false;
            txtOption.Text = "V";
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Cancel;
            BindParameterNames();
        }

        /// <summary>
        /// Overrides Do Toolbar Action method of the base class
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType.ToLower().Equals("search"))
            {
                int globalParamId = 0;
                globalParamId = (cmbParamName.SelectedValue != null && !cmbParamName.SelectedValue.ToString().Equals(String.Empty)) ? Convert.ToInt32(cmbParamName.SelectedValue.ToString()) : 0;
                ((BUSINESSOBJECTS.ENTITIES.GlobalParameterMappingCommand)slPnlTabulerDDSParam.CurrentBusinessEntity).GLOBAL_PARAM_ID = globalParamId;
                hasAllUserWarningBeenShown = false;
            }
            else if (actionType.ToLower().Equals("save"))
            {
                string globalParamName = cmbParamName.SelectedValue != null ? cmbParamName.Text : string.Empty;
                if (globalParamName.ToLower().Equals("markup_rate"))
                {
                    if (isDuplicateMarkUpRate())
                    {
                        actionType = "Search";
                        base.DoToolbarActions(ctrlsCollection, actionType);
                        return;
                    }
                }
                if (!hasAllUserWarningBeenShown && isAllUsersAlreadyAssigned())
                {
                    MessageBox.Show("The value of this global parameter has been set for All Users.\n Please note that the other entries will be ignored.", "Parameter Value already assigned to All Users", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    hasAllUserWarningBeenShown = true;
                }
            }
            else if (actionType.ToLower().Equals("delete"))
            {
                string globalParamName = cmbParamName.SelectedValue != null ? cmbParamName.Text : string.Empty;
                if (slDgvParameterValues.Rows.Count.Equals(2) && globalParamName.ToLower().Equals("markup_rate"))
                {
                    MessageBox.Show("Mark Up Rate cannot be deleted.");
                    return;
                }

            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        /// <summary>
        /// Handles the data grid view row enter event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slDgvParameterValues_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (this.slDgvParameterValues.Rows[e.RowIndex] != null && this.slDgvParameterValues.Rows[e.RowIndex].IsNewRow)
            {
                int globalParamId = 0;
                globalParamId = (cmbParamName.SelectedValue != null && !cmbParamName.SelectedValue.ToString().Equals(String.Empty)) ? Convert.ToInt32(cmbParamName.SelectedValue.ToString()) : 0;
                this.slDgvParameterValues.Rows[e.RowIndex].Cells[3].Value = globalParamId;
                if (!isAllUsersAlreadyAssigned())
                    this.slDgvParameterValues.Rows[e.RowIndex].Cells[0].Value = "All";
            }
            hasAllUserWarningBeenShown = false;
        }

        private bool isAllUsersAlreadyAssigned()
        {
            bool isAllUsersAlreadyAssigned = false;
            if (slDgvParameterValues.Rows.Count.Equals(1))
                return isAllUsersAlreadyAssigned;

            foreach (DataGridViewRow globalParamRow in slDgvParameterValues.Rows)
            {
                if (!globalParamRow.IsNewRow && globalParamRow.Cells[0].Value.ToString().ToLower().Equals("all"))
                {
                    isAllUsersAlreadyAssigned = true;
                    break;
                }
            }
            return isAllUsersAlreadyAssigned;
            
        }
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        private bool isDuplicateMarkUpRate()
        {
            bool isDuplicateMarkUpRate = false;
            if (slDgvParameterValues.Rows.Count.Equals(1))
                return isDuplicateMarkUpRate;
            foreach (DataGridViewRow globalParamRow in slDgvParameterValues.Rows)
            {
                if (!globalParamRow.IsNewRow && string.IsNullOrEmpty(globalParamRow.Cells[2].Value.ToString()))
                {
                    isDuplicateMarkUpRate = true;
                    MessageBox.Show("Duplicate Records Found for Mark Up Rate.");
                    break;
                }
            }
            return isDuplicateMarkUpRate;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Binds the parameter name combo box with data.
        /// </summary>
        private void BindParameterNames ()
        {
            Result rslt;
            DataSet dstResult = new DataSet();
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<String, Object> paramWithVals = new Dictionary<String, Object>();
            rslt = cmnDM.GetData(slPnlTabulerDDSParam.SPName, "GetGlobalParametersList", paramWithVals);
            cmbParamName.DataSource = rslt.dstResult.Tables[0];
            cmbParamName.DisplayMember = "GLOBAL_PARAM_NAME";
            cmbParamName.ValueMember = "GLOBAL_PARAM_ID";
            cmbParamName.Enabled = true;
        }

        #endregion

    }
}