using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    public partial class CHRIS_Setup_IncomeTaxSlabsEntry : ChrisTabularForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        double temp = 0;


        # region constructor
        public CHRIS_Setup_IncomeTaxSlabsEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Setup_IncomeTaxSlabsEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            DgvIncome.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            label9.Text+= "   " + this.UserName;
        }

        #endregion
        # region Methods

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();

            SP_AMT_FROM.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_AMT_TO.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_REBATE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_FREBATE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_FPERCEN.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_PERCEN.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_FTAX_AMT.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_TAX_AMT.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_SURCHARGE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            SP_FSURCHARGE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            TAX_RED_PER.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FTAX_RED_PER.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);

            //forcefully changed for restricting the LOV window to open//
            SP_AMT_FROM.SkipValidationOnLeave = true;

        }


        protected override bool Quit()
        {

            if (DgvIncome.Rows.Count > 1)
            {
                this.Cancel();
            }
            else
            {
                base.Quit();
            }
            return false;
        }


        protected override bool Add()
        {
            return base.Add();
        }
        
        
        
        void proc_view_add()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_amt_from",DgvIncome.CurrentRow.Cells["sp_amt_from"].EditedFormattedValue.ToString());
            param.Add("sp_amt_to", DgvIncome.CurrentRow.Cells["sp_amt_to"].EditedFormattedValue.ToString());
            rslt = cmnDM.GetData("CHRIS_SP_INCOME_MANAGER", "proc_view_add", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {                
                
                
                }
            
            }        
        
        }
        void proc_mod_Del()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_amt_from", DgvIncome.CurrentRow.Cells["sp_amt_from"].EditedFormattedValue.ToString());
            param.Add("sp_amt_to", DgvIncome.CurrentRow.Cells["sp_amt_to"].EditedFormattedValue.ToString());
            rslt = cmnDM.GetData("CHRIS_SP_INCOME_MANAGER", "proc_mod_Del", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {


                }

            }  
        
        }
        void proc_modify()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_amt_from", DgvIncome.CurrentRow.Cells["sp_amt_from"].EditedFormattedValue.ToString());
            param.Add("sp_amt_to", DgvIncome.CurrentRow.Cells["sp_amt_to"].EditedFormattedValue.ToString());
            rslt = cmnDM.GetData("CHRIS_SP_INCOME_MANAGER", "proc_modify", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {

                }
            } 
        
        }

        void proc_delete()
        {

            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("sp_amt_from", DgvIncome.CurrentRow.Cells["sp_amt_from"].EditedFormattedValue.ToString());
            param.Add("sp_amt_to", DgvIncome.CurrentRow.Cells["sp_amt_to"].EditedFormattedValue.ToString());
            rslt = cmnDM.GetData("CHRIS_SP_INCOME_MANAGER", "proc_delete", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                }

            } 
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {
                DgvIncome.EndEdit();
                DataTable dt = (DataTable)DgvIncome.DataSource;
                if (dt != null)
                {
                    DataTable dtAdded = dt.GetChanges(DataRowState.Added);
                    DataTable dtUpdated = dt.GetChanges(DataRowState.Modified);
                    if (dtAdded != null || dtUpdated != null)
                    {
                        DialogResult dr = MessageBox.Show("Do You Want to Save Changes.", "", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                        if (dr == DialogResult.No)
                        {
                            return;
                        }
                    }
                }
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CurrentPanelBlock = "PnlINCOME";
            txtOption.Visible = false;
            txtUser.Text = this.UserName;
            ///this.label9.Text += this.UserName;
            this.txtDate.Text = this.CurrentDate.ToString("dd/MM/yyyy");
            this.txtUser.Text = this.userID;
        }
        #endregion       
        #region Events

        private void DgvIncome_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            # region Coulmn 1


            if (e.ColumnIndex == 0)
            {


                if (DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].IsInEditMode)
                {
                    if (!(double.TryParse(DgvIncome.CurrentCell.EditedFormattedValue.ToString(), out temp))  )
                    {
                        e.Cancel = true;
                        return;
                    }
                }

            }
            
            
            if (e.ColumnIndex == 1)
            { 

                if (DgvIncome.CurrentRow.Cells["SP_AMT_TO"].IsInEditMode)
                {
                    if ((double.TryParse(DgvIncome.CurrentCell.EditedFormattedValue.ToString(), out temp)) && (DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString() != "") && (DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString() != ""))
                    {
                        double SP_AMT_FROM = double.Parse(DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString() == "" ? "0" : DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString());
                        double SP_AMT_TO = double.Parse(DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString() == "" ? "0" : DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString());
                    
                if (SP_AMT_TO < SP_AMT_FROM)
                { 
                    MessageBox.Show("\" To\""  +" Amount Must Be Greater Than "+ "\"From \" "+" Amount");
                    e.Cancel = true;                
                }
            }

                if ((FunctionConfig.CurrentOption == Function.Modify) || (FunctionConfig.CurrentOption == Function.Delete))
                {
                //proc_mod_Del();               
                
                }
               
                     
                    if ((FunctionConfig.CurrentOption == Function.View) || (FunctionConfig.CurrentOption == Function.Add))
                {
                //proc_view_add();               
                
                }


                    if ((FunctionConfig.CurrentOption == Function.Add) && (DgvIncome.CurrentRow.Cells["sp_rebate"].EditedFormattedValue.ToString() != string.Empty))
                    {                     
                        MessageBox.Show("Record Already Entered");
                        base.ClearForm(PnlINCOME.Controls);                   
                    }


                    if (FunctionConfig.CurrentOption == Function.View)
                    { 
                     //go_field('blk_head.w_ans');
                    }


                    if (FunctionConfig.CurrentOption == Function.Delete)
                    { 
                    //go_field('blk_head.w_ans2');                    
                    }

                }

            }
            #endregion
            # region coulmn 2
            if (e.ColumnIndex == 2)
            {

                if (DgvIncome.CurrentRow.Cells["SP_REBATE"].IsInEditMode)
                {
                  
                    
                    
                    if (DgvIncome.CurrentRow.Cells["SP_REBATE"].EditedFormattedValue.ToString() != string.Empty)
                    {

                        if (!(double.TryParse(DgvIncome.CurrentCell.EditedFormattedValue.ToString(), out temp)))
                        {
                            e.Cancel = true;
                            return;
                        }

                        
                        double SP_AMT_FROM = double.Parse(DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString()== "" ? "0" : DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString());
                        double SP_AMT_TO = double.Parse(DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString() == "" ? "0" :DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString());
                        double SP_REBATE = double.Parse(DgvIncome.CurrentRow.Cells["SP_REBATE"].EditedFormattedValue.ToString() == "" ? "0" : DgvIncome.CurrentRow.Cells["SP_REBATE"].EditedFormattedValue.ToString());

                        //double SP_REBATE = double.Parse(DgvIncome.CurrentRow.Cells["SP_REBATE"].Value.ToString() == "" ? "0" : DgvIncome.CurrentRow.Cells["SP_REBATE"].Value.ToString());
                        

                        if (SP_REBATE < SP_AMT_FROM)
                        {
                            MessageBox.Show("Rebate Amount Not Less Than The  " +""+ "\"From Amount\"" +""+"" + "   Try Again....");
                            e.Cancel = true;
                        }


                        if (SP_REBATE > SP_AMT_TO)
                        {
                            MessageBox.Show("Rebate Amount Not Greater Than The " + "" + "\"To Amount\"" + "" + "" + "    Try Again....");
                            e.Cancel = true;
                        }
                    }
                }


            }
            #endregion
            # region coulmn 3
            if (e.ColumnIndex == 3)
            {

                if (DgvIncome.CurrentRow.Cells["SP_FREBATE"].IsInEditMode)
                {
                    if (DgvIncome.CurrentRow.Cells["SP_REBATE"].EditedFormattedValue.ToString() != string.Empty)
                    {

                        if (!(double.TryParse(DgvIncome.CurrentCell.EditedFormattedValue.ToString(), out temp)))
                        {
                            e.Cancel = true;
                            return;
                        }
                        
                        double SP_AMT_FROM = double.Parse(DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString() == "" ? "0" :DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString());
                        double SP_AMT_TO = double.Parse(DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString() == "" ? "0"  :  DgvIncome.CurrentRow.Cells["SP_AMT_TO"].EditedFormattedValue.ToString());
                        double SP_FREBATE = double.Parse(DgvIncome.CurrentRow.Cells["SP_FREBATE"].EditedFormattedValue.ToString() == "" ? "0" : DgvIncome.CurrentRow.Cells["SP_FREBATE"].EditedFormattedValue.ToString());

                        if (SP_FREBATE < SP_AMT_FROM)
                        {
                            MessageBox.Show("Rebate Amount Not Less Than The  " + "" + "\" From Amount\"" + "" + "" + "  Try Again....");
                            e.Cancel = true;
                        }


                        if (SP_FREBATE > SP_AMT_TO)
                        {
                            MessageBox.Show("Rebate Amount Not Greater Than The " + "" + "\" To Amount\"" + "" + "" + "  Try Again....");
                            e.Cancel = true;
                        }
                    }
                }


            }
            #endregion
            # region coulmn 13
            if (e.ColumnIndex == 13)
            {
                string SP_AMT_FROM = DgvIncome.CurrentRow.Cells["SP_AMT_FROM"].EditedFormattedValue.ToString();

                if (DgvIncome.CurrentRow.Cells["TAX_RED_PER"].IsInEditMode)
                {

                    if (!(double.TryParse(DgvIncome.CurrentCell.EditedFormattedValue.ToString(), out temp)))
                    {
                        e.Cancel = true;
                        return;
                    }
                    
                    
                    
                    if (FunctionConfig.CurrentOption == Function.Modify)
                    {
                        //proc_modify();
                    }
                    else
                        if (FunctionConfig.CurrentOption == Function.Add && SP_AMT_FROM != string.Empty)
                        {
                            //go_field('blk_head.w_ans3');
                        }

                }


            }
            #endregion

        }    
        private void DgvIncome_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dt = (DataTable)this.DgvIncome.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                if (e.RowIndex < dt.Rows.Count - 1)
                {
                    if (dt.Rows[e.RowIndex].RowState == DataRowState.Added)
                    {
                        //SP_AMT_FROM.SkipValidationOnLeave = true;
                    }
                    else
                    {
                        //SP_AMT_FROM.SkipValidationOnLeave = false;
                    }
                }
                else
                {
                    ///SP_AMT_FROM.SkipValidationOnLeave = true;
                }
            }
            else
            {
               /// SP_AMT_FROM.SkipValidationOnLeave = true;
            }
        }
        private void DgvIncome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 47 && e.KeyChar <= 58) || (e.KeyChar == 46) || (e.KeyChar == 8))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }
        #endregion
    }
}