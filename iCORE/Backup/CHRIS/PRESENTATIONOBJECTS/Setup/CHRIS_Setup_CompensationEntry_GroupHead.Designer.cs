namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Setup
{
    partial class CHRIS_Setup_CompensationEntry_GroupHead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Setup_CompensationEntry_GroupHead));
            this.pnlTblGrpHead = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvGrpHead = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.gpHead = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnScrollDown = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUP = new System.Windows.Forms.Button();
            this.btnScroll_UP = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblusername = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlTblGrpHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrpHead)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(697, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(733, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 360);
            this.panel1.Size = new System.Drawing.Size(733, 60);
            // 
            // pnlTblGrpHead
            // 
            this.pnlTblGrpHead.ConcurrentPanels = null;
            this.pnlTblGrpHead.Controls.Add(this.dgvGrpHead);
            this.pnlTblGrpHead.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblGrpHead.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblGrpHead.DependentPanels = null;
            this.pnlTblGrpHead.DisableDependentLoad = false;
            this.pnlTblGrpHead.EnableDelete = true;
            this.pnlTblGrpHead.EnableInsert = true;
            this.pnlTblGrpHead.EnableQuery = false;
            this.pnlTblGrpHead.EnableUpdate = true;
            this.pnlTblGrpHead.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_HEADCommand";
            this.pnlTblGrpHead.Location = new System.Drawing.Point(111, 77);
            this.pnlTblGrpHead.MasterPanel = null;
            this.pnlTblGrpHead.Name = "pnlTblGrpHead";
            this.pnlTblGrpHead.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblGrpHead.Size = new System.Drawing.Size(479, 230);
            this.pnlTblGrpHead.SPName = "CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER";
            this.pnlTblGrpHead.TabIndex = 0;
            // 
            // dgvGrpHead
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGrpHead.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGrpHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGrpHead.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gpHead,
            this.ID,
            this.Name1});
            this.dgvGrpHead.ColumnToHide = null;
            this.dgvGrpHead.ColumnWidth = null;
            this.dgvGrpHead.CustomEnabled = true;
            this.dgvGrpHead.DisplayColumnWrapper = null;
            this.dgvGrpHead.GridDefaultRow = 0;
            this.dgvGrpHead.Location = new System.Drawing.Point(3, 3);
            this.dgvGrpHead.Name = "dgvGrpHead";
            this.dgvGrpHead.ReadOnlyColumns = null;
            this.dgvGrpHead.RequiredColumns = null;
            this.dgvGrpHead.Size = new System.Drawing.Size(473, 224);
            this.dgvGrpHead.SkippingColumns = "NAME1";
            this.dgvGrpHead.TabIndex = 0;
            this.dgvGrpHead.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvGrpHead_CellValidating);
            // 
            // gpHead
            // 
            this.gpHead.ActionLOV = "Group_Head";
            this.gpHead.ActionLOVExists = "GROUP_HEAD_EXISTS";
            this.gpHead.AttachParentEntity = false;
            this.gpHead.DataPropertyName = "GRHD_NO";
            this.gpHead.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_HEADCommand";
            this.gpHead.HeaderText = "Group Head No";
            this.gpHead.LookUpTitle = null;
            this.gpHead.LOVFieldMapping = "GRHD_NO";
            this.gpHead.Name = "gpHead";
            this.gpHead.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.gpHead.SearchColumn = "GRHD_NO";
            this.gpHead.SkipValidationOnLeave = false;
            this.gpHead.SpName = "CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER";
            this.gpHead.Width = 130;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // Name1
            // 
            this.Name1.DataPropertyName = "Name";
            this.Name1.HeaderText = "Name";
            this.Name1.Name = "Name1";
            this.Name1.ReadOnly = true;
            this.Name1.Width = 300;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnBack);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.btnQuery);
            this.panel2.Controls.Add(this.btnExit);
            this.panel2.Controls.Add(this.btnRemove);
            this.panel2.Controls.Add(this.btnScrollDown);
            this.panel2.Controls.Add(this.btnDown);
            this.panel2.Controls.Add(this.btnUP);
            this.panel2.Controls.Add(this.btnScroll_UP);
            this.panel2.Location = new System.Drawing.Point(111, 323);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(479, 41);
            this.panel2.TabIndex = 10;
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(407, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(55, 23);
            this.btnBack.TabIndex = 9;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(351, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(55, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuery.Location = new System.Drawing.Point(295, 3);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(55, 23);
            this.btnQuery.TabIndex = 7;
            this.btnQuery.Text = "Query";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(239, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(55, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(171, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(67, 23);
            this.btnRemove.TabIndex = 4;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnScrollDown
            // 
            this.btnScrollDown.Location = new System.Drawing.Point(126, 3);
            this.btnScrollDown.Name = "btnScrollDown";
            this.btnScrollDown.Size = new System.Drawing.Size(39, 23);
            this.btnScrollDown.TabIndex = 3;
            this.btnScrollDown.Text = ">>";
            this.btnScrollDown.UseVisualStyleBackColor = true;
            this.btnScrollDown.Click += new System.EventHandler(this.btnScrollDown_Click);
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(85, 3);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(39, 23);
            this.btnDown.TabIndex = 2;
            this.btnDown.Text = ">";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUP
            // 
            this.btnUP.Location = new System.Drawing.Point(44, 3);
            this.btnUP.Name = "btnUP";
            this.btnUP.Size = new System.Drawing.Size(39, 23);
            this.btnUP.TabIndex = 1;
            this.btnUP.Text = "<";
            this.btnUP.UseVisualStyleBackColor = true;
            this.btnUP.Click += new System.EventHandler(this.btnUP_Click);
            // 
            // btnScroll_UP
            // 
            this.btnScroll_UP.Location = new System.Drawing.Point(3, 3);
            this.btnScroll_UP.Name = "btnScroll_UP";
            this.btnScroll_UP.Size = new System.Drawing.Size(39, 23);
            this.btnScroll_UP.TabIndex = 0;
            this.btnScroll_UP.Text = "<<";
            this.btnScroll_UP.UseVisualStyleBackColor = true;
            this.btnScroll_UP.Click += new System.EventHandler(this.btnScroll_UP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(114, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "                               Group Head Entry Form";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(413, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "User Name :";
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Location = new System.Drawing.Point(475, 13);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(66, 13);
            this.lblusername.TabIndex = 13;
            this.lblusername.Text = "User Name :";
            // 
            // CHRIS_Setup_CompensationEntry_GroupHead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 420);
            this.Controls.Add(this.lblusername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlTblGrpHead);
            this.Name = "CHRIS_Setup_CompensationEntry_GroupHead";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Setup_CompensationEntry_GroupHead";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlTblGrpHead, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblusername, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlTblGrpHead.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrpHead)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblGrpHead;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnScrollDown;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUP;
        private System.Windows.Forms.Button btnScroll_UP;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label1;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvGrpHead;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblusername;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn gpHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name1;
    }
}