namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_FPHistoryAnswerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanel1 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.txtAnswerDialog = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.slPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanel1
            // 
            this.slPanel1.ConcurrentPanels = null;
            this.slPanel1.Controls.Add(this.txtAnswerDialog);
            this.slPanel1.Controls.Add(this.label3);
            this.slPanel1.DataManager = null;
            this.slPanel1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel1.DependentPanels = null;
            this.slPanel1.DisableDependentLoad = false;
            this.slPanel1.EnableDelete = true;
            this.slPanel1.EnableInsert = true;
            this.slPanel1.EnableQuery = false;
            this.slPanel1.EnableUpdate = true;
            this.slPanel1.EntityName = null;
            this.slPanel1.Location = new System.Drawing.Point(36, 9);
            this.slPanel1.MasterPanel = null;
            this.slPanel1.Name = "slPanel1";
            this.slPanel1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel1.Size = new System.Drawing.Size(593, 86);
            this.slPanel1.SPName = null;
            this.slPanel1.TabIndex = 13;
            // 
            // txtAnswerDialog
            // 
            this.txtAnswerDialog.AllowSpace = true;
            this.txtAnswerDialog.AssociatedLookUpName = "";
            this.txtAnswerDialog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnswerDialog.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnswerDialog.ContinuationTextBox = null;
            this.txtAnswerDialog.CustomEnabled = true;
            this.txtAnswerDialog.DataFieldMapping = "";
            this.txtAnswerDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnswerDialog.GetRecordsOnUpDownKeys = false;
            this.txtAnswerDialog.IsDate = false;
            this.txtAnswerDialog.Location = new System.Drawing.Point(387, 31);
            this.txtAnswerDialog.MaxLength = 3;
            this.txtAnswerDialog.Name = "txtAnswerDialog";
            this.txtAnswerDialog.NumberFormat = "###,###,##0.00";
            this.txtAnswerDialog.Postfix = "";
            this.txtAnswerDialog.Prefix = "";
            this.txtAnswerDialog.Size = new System.Drawing.Size(62, 20);
            this.txtAnswerDialog.SkipValidation = false;
            this.txtAnswerDialog.TabIndex = 10;
            this.txtAnswerDialog.TextType = CrplControlLibrary.TextType.String;
            this.txtAnswerDialog.TextChanged += new System.EventHandler(this.txtAnswerDialog_TextChanged);
            this.txtAnswerDialog.Leave += new System.EventHandler(this.txtAnswerDialog_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(66, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(288, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = " [YES] Exit With Save and  [NO]   Exit Without Save ";
            // 
            // CHRIS_Processing_FPHistoryAnswerDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(667, 107);
            this.ControlBox = false;
            this.Controls.Add(this.slPanel1);
            this.Name = "CHRIS_Processing_FPHistoryAnswerDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.slPanel1.ResumeLayout(false);
            this.slPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel1;
        private CrplControlLibrary.SLTextBox txtAnswerDialog;
        private System.Windows.Forms.Label label3;


    }
}