using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using System.Data.SqlClient;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_PayrollGeneration : SimpleForm
    {
        #region Feilds

        //*******dtPersonnels will contain personnel records,fetched via cursor in oracle.*******
        DataTable dtPersonnels = null;
        //******* not included in current logic.*******
        DataTable dtEmpPayRollDetail = null;
        CmnDataManager cmnDM = new CmnDataManager();
        //*******for Pre-processing like term-set.*******
        Result rslt;
        //******* used for main processing.*******
        Result flowRslt;
        //*******Payroll processing status,"Failure" will abort processing.******* 
        public enum FormMode { Processing, Failure, NotStarted, Successfull, PreReqPassed };
        private FormMode processState;
        //ds used for data manipulation and management while processing. 
        Dictionary<string, object> dicEmpPayRollDetail = new Dictionary<string, object>();
        Dictionary<string, object> paramList = new Dictionary<string, object>();
        DataManager DM = new CommonDataManager();

        /********payroll form once entered in "Processing" mode ,will use this SQL trans,and if any
         error/exception occurs whole process will be aborted and payroll mode will be "Failure" ******/
        SqlTransaction processTrans = null;
        SqlConnection proCon = new SqlConnection();
        private static XMS.DATAOBJECTS.ConnectionBean connbean = null;
        private String GLOBAL_W_10_AMT;
        private String GLOBAL_W_INC_AMT;
        private String GLOBAL_PERKS;

        bool SetFieldStatue = true;
        public int MARKUP_RATE_DAYS = -1;
        #endregion

        #region Constructors
        public CHRIS_Processing_PayrollGeneration()
        {
            InitializeComponent();

        }
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_TERM_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }
        public CHRIS_Processing_PayrollGeneration(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //connbean = connbean_obj;

        }

        #endregion

        #region Overriden Methods

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            this.Text = "iCORE CHRIS-Payroll Generation";
            tbtAdd.Visible = false;
            tbtCancel.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
            tbtClose.Visible = true;
            //slTB_Pay_Date.Text = System.DateTime.Now.ToShortDateString();
            SetFormInInitialState();
            this.GLOBAL_W_10_AMT = "0.00"; // Should be Assigned with constant
            this.GLOBAL_W_INC_AMT = "0.00"; // Should be Assigned with constant
            this.GLOBAL_PERKS = "0.00"; // Should be Assigned with constant


            try
            {
                rslt = cmnDM.Get("CHRIS_SP_TERM_MANAGER", "GetStatus");
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        SetFieldStatue = false;
                        chkResetTermFlag.Checked = true;
                    }
                }
            }
            catch (Exception exp)
            {
                LogError("CommonOnLoadMethods", exp);
            }

        }

        #endregion

        #region Methods

        private void InitializeProcessingFlow()
        {
            PreProcessing();
        }

        private void SetFormInInitialState()
        {
            slTB_PF_Exempt.Text = "100,000";
            slTB_Transport_Vp.Text = "40,000";
            slTBMarginal_Impact.Text = "Y";
            groupBoxProcessCompleted.Visible = false;
            groupBoxProcessRunning.Visible = false;

            slTB_Pr_P_No.Text = string.Empty;
            slTB_TotalTaxableAmount.Text = string.Empty;
            slTB_Status.Text = string.Empty;
            slTB_No.Text = string.Empty;
            slTB_LoanTaxAmount.Text = string.Empty;
            this.processState = FormMode.NotStarted;
        }

        private void SetFormInProcessingState()
        {
            groupBoxProcessRunning.Visible = true;
            //this.groupBoxMainProcess.Enabled = false;
        }

        private void StartProcessing()
        {
            GetAllEmployees();
            //GetInitialData();
            InitializeList();
            this.processState = FormMode.Processing;
            InitiateProcess();
            if (processState == FormMode.Failure)
            {
                
            }


        }

        private void InitializeList()
        {
            dicEmpPayRollDetail.Clear();
            dicEmpPayRollDetail.Add("PR_P_NO", null);
            dicEmpPayRollDetail.Add("W_AN_PACK", null);
            dicEmpPayRollDetail.Add("W_CATE", null);
            dicEmpPayRollDetail.Add("W_CONF_FLAG", null);
            dicEmpPayRollDetail.Add("W_DESIG", null);
            dicEmpPayRollDetail.Add("W_LEVEL", null);
            dicEmpPayRollDetail.Add("W_ZAKAT", null);
            dicEmpPayRollDetail.Add("W_PRV_INC", null);
            dicEmpPayRollDetail.Add("W_PRV_TAX_PD", null);
            dicEmpPayRollDetail.Add("W_TAX_USED", null);
            dicEmpPayRollDetail.Add("W_TRANS_DATE", null);
            dicEmpPayRollDetail.Add("W_PR_MONTH", null);
            dicEmpPayRollDetail.Add("W_JOINING_DATE", null);
            dicEmpPayRollDetail.Add("W_BRANCH", null);
            dicEmpPayRollDetail.Add("W_PR_DATE", null);
            dicEmpPayRollDetail.Add("W_REFUND", null);
            dicEmpPayRollDetail.Add("W_ATT_FLAG", null);
            dicEmpPayRollDetail.Add("W_PAY_FLAG", null);
            dicEmpPayRollDetail.Add("W_CONFIRM_ON", null);


            dicEmpPayRollDetail.Add("W_ASR", 0.00);
            dicEmpPayRollDetail.Add("W_HRENT", 0.00);
            dicEmpPayRollDetail.Add("W_CONV", 0.00);
            dicEmpPayRollDetail.Add("W_GRAT", 0.00);
            dicEmpPayRollDetail.Add("W_GOVT_ALL", 0.00);
            dicEmpPayRollDetail.Add("MDAYS", 0.00);
            dicEmpPayRollDetail.Add("W_DAYS", 0.00);
            dicEmpPayRollDetail.Add("W_DIVISOR", 0.00);
            dicEmpPayRollDetail.Add("WW_BASIC", 0.00);
            dicEmpPayRollDetail.Add("WW_CONV", 0.00);
            dicEmpPayRollDetail.Add("WW_HOUSE", 0.00);
            dicEmpPayRollDetail.Add("W_FLAG", null);
            dicEmpPayRollDetail.Add("W_DEPT_FT", null);
            dicEmpPayRollDetail.Add("W_SW_PAY", 1.00);

            dicEmpPayRollDetail.Add("WW_DATE", null);
            dicEmpPayRollDetail.Add("W_DATE", null);

            dicEmpPayRollDetail.Add("W_SAL_ADV", 0.00);
            dicEmpPayRollDetail.Add("W_PROMOTED", null);
            dicEmpPayRollDetail.Add("W_PFUND", 0.00);
            dicEmpPayRollDetail.Add("GLOBAL_FLAG", null);
            //dicEmpPayRollDetail.Add("W_PR_MONTH", null);
            dicEmpPayRollDetail.Add("GLOBAL_PERKS", this.GLOBAL_PERKS);
            dicEmpPayRollDetail.Add("W_OT_ASR", 0.00); 
            //proc_6
            dicEmpPayRollDetail.Add("W_PA_DATE", null);
            dicEmpPayRollDetail.Add("W_INC_MONTHS", 0.00);
            dicEmpPayRollDetail.Add("W_BASIC_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_HOUSE_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_CONV_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_PF_AREAR", 0.00);
            dicEmpPayRollDetail.Add("GLOBAL_W_FOUND", null);
            //Sal_advance
            dicEmpPayRollDetail.Add("W_WORK_DAY", null);
            //dicEmpPayRollDetail.Add("W_SAL_ADV", 0.00);
            //FIN BOOK
            dicEmpPayRollDetail.Add("W_MARKUP_L", 0.00);
            dicEmpPayRollDetail.Add("W_INST_L", 0.00);
            dicEmpPayRollDetail.Add("W_MARKUP_F", 0.00);
            dicEmpPayRollDetail.Add("W_INST_F", 0.00);
            dicEmpPayRollDetail.Add("W_EXCISE", 0.00);
            dicEmpPayRollDetail.Add("W_FINANCE", null);
            dicEmpPayRollDetail.Add("W_MONTH_DED", 0.00);
            dicEmpPayRollDetail.Add("W_EXTRA", null);
            dicEmpPayRollDetail.Add("W_ITAX", 0.00);

            dicEmpPayRollDetail.Add("WPER_DAY", null);

            // Z_TAX
            dicEmpPayRollDetail.Add("W_PF_EXEMPT", slTB_PF_Exempt.Text);
            dicEmpPayRollDetail.Add("W_VP", slTB_Transport_Vp.Text);
            dicEmpPayRollDetail.Add("W_SYS", slDatePicker_Sys_Date.Value);
            dicEmpPayRollDetail.Add("GLOBAL_W_10_AMT", this.GLOBAL_W_10_AMT);
            dicEmpPayRollDetail.Add("GLOBAL_W_INC_AMT", this.GLOBAL_W_INC_AMT);
            dicEmpPayRollDetail.Add("SUBS_LOAN_TAX_AMT", 0.00);
            dicEmpPayRollDetail.Add("Z_TAXABLE_AMOUNT", 0.00);


        }

        private void GetInitialData()
        {
            //fetch all employees with condition described in cursor emp_no(oracle form,Valid_Pno). 

            rslt = cmnDM.GetData("CHRIS_SP_INITIALIZE_VALUES", "");

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null)
                {
                    dtEmpPayRollDetail = rslt.dstResult.Tables[0];
                }
            }
        }

        private void FillPayRollDataWithPersonnelBasicInfo(int pRow)
        {
            try
            {

                for (int pCol = 0; pCol < dtPersonnels.Columns.Count; pCol++)
                {
                    if (dicEmpPayRollDetail.ContainsKey(dtPersonnels.Columns[pCol].ColumnName))
                    {
                        if(dtPersonnels.Columns[pCol].ColumnName.ToUpper()!="W_DATE")
                        dicEmpPayRollDetail[dtPersonnels.Columns[pCol].ColumnName] = dtPersonnels.Rows[pRow][pCol];
                    }
                    //if (dtEmpPayRollDetail.Columns.Contains(dtPersonnels.Columns[pCol].ColumnName))
                    //    {dtEmpPayRollDetail.Rows[pRow][dtPersonnels.Columns[pCol].ColumnName] = dtPersonnels.Rows[pRow][pCol];}
                }
            }
            catch (Exception e)
            {
                base.LogException(this.Name, "FillPayRollDataWithPersonnelBasicInfo(int personnelNumberRow)", e);
                //error in copying data.
            }
        }

        /// <summary>
        /// returns true if payroll is already generated for user specified month, else returns false.
        /// </summary>
        /// <returns></returns>
        private bool IsPayRollAlreadyGenerated()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Clear();
            param.Add("W_Date", slDatePicker_Sys_Date.Value);

            rslt = cmnDM.GetData("CHRIS_SP_TERM_MANAGER", "IsPayRollGenerated", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                    this.processState = FormMode.PreReqPassed;
                    return false;
                }
                else
                {
                    this.processState = FormMode.Failure;
                    DateTime dt = Convert.ToDateTime(slDatePicker_Sys_Date.Value);
                    MessageBox.Show("PAYROLL HAS ALREADY BEEN GENERATED FOR " + dt.ToString("MMM-yyyy").ToUpper(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return true;
                }
            }
            return true;
        }

        private void PreProcessing()
        {
            if (!IsPayRollAlreadyGenerated())
            {
                TermSet();
                if (this.processState == FormMode.PreReqPassed)
                {
                    StartProcessing();
                }
            }
            else
            {
                SetFormInInitialState();
            }
        }

        private void GetAllEmployees()
        {
            //fetch all employees with condition described in cursor emp_no(oracle form,Valid_Pno). 

            rslt = cmnDM.GetData("CHRIS_SP_GET_PERSONNEL_INFO", "");

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    dtPersonnels = rslt.dstResult.Tables[0];
                }
            }

        }

        private void TermSet()
        {
            //*************will check if processing is going on another terminal.************
            rslt = cmnDM.Get("CHRIS_SP_TERM_MANAGER", "GetTermStatus");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                {
                    this.rslt = cmnDM.Get("CHRIS_SP_TERM_MANAGER", "UpdateToProcess");
                    if (this.rslt.isSuccessful)
                    {
                        this.processState = FormMode.PreReqPassed;
                        SetFormInProcessingState();
                    }

                }
                else
                {
                    this.processState = FormMode.Failure;
                    MessageBox.Show("Processing Is In Progress On Other Terminal .......!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                this.processState = FormMode.Failure;
                MessageBox.Show("Processing Is In Progress On Other Terminal .......!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

        }

        private void TermSet_Clear()
        {
            this.rslt = cmnDM.Get("CHRIS_SP_TERM_MANAGER", "UpdateToNull");
            if (this.rslt.isSuccessful)
            {
                this.processState = FormMode.NotStarted;
                SetFormInInitialState();
            }
        }

        private void SetStatusBar(string StatusMessage)
        {
            slTB_Status.Text = StatusMessage;   // "not initialized";
            labelStatusBar.Text = StatusMessage; // "not initialized";
        }

        /// <summary>
        /// Manual Logging 
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="functionName"></param>
        public void Logging(string typeName, string functionName)
        {
            // code starts for creating log file starts here this code will be appended to main menu file
            string moduleName = "Payroll";

            string complete_path = "C:/iCORE-Logs/" + moduleName + "-Logs/";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;

            if (System.IO.Directory.Exists("C:/iCORE-Logs"))
            {
                bool_icorelogs = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory("C:/Payroll-Logs");
                bool_icorelogs = true;
            }

            if (bool_icorelogs)
            {
                if (System.IO.Directory.Exists(complete_path))
                {
                    bool_icorelogs_rps_logs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                    bool_icorelogs_rps_logs = true;
                }
            }

            if (bool_icorelogs_rps_logs)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");
                
                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Form/Report Name: " + typeName);
                writer.WriteLine("Function Name: " + functionName);
                
                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }

        private void InitiateProcess()
        {
            try
            {
                connbean = SQLManager.GetConnectionBean();
                if (connbean != null)
                {
                    proCon                  = connbean.getDatabaseConnection();
                    processTrans            = proCon.BeginTransaction();
                    int personnel_Cursor    = 0;
                    DateTime dt             = Convert.ToDateTime(slDatePicker_Sys_Date.Value);
                    DateTime dtWW_DATE      = DateTime.Now;
                    int W_SW_PAY            = 1;
                    dicEmpPayRollDetail["W_DATE"] = dt;

                    if (dtPersonnels != null && processState == FormMode.Processing)
                    {


                        for (int rowPersonnelNum = 0; rowPersonnelNum < dtPersonnels.Rows.Count; rowPersonnelNum++)
                        {
                            RefreshProgressControls();
                            string msg = string.Empty;
                            try
                            {
                                slTB_Pr_P_No.Text = dtPersonnels.Rows[rowPersonnelNum]["PR_P_NO"].ToString();
                                InitializeList();
                                FillPayRollDataWithPersonnelBasicInfo(rowPersonnelNum);//(rowPersonnelNum);
                                dicEmpPayRollDetail["W_DATE"] = dt;
                                dicEmpPayRollDetail["WW_DATE"] = dtWW_DATE;
                                dicEmpPayRollDetail["W_SW_PAY"] = W_SW_PAY;
                                #region promotion_proc
                                //********************calling procedure to check promotion****************************
                                slTB_No.Text = "";
                                paramList.Clear();
                                paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_PR_DATE", dicEmpPayRollDetail["W_PR_DATE"]);
                                paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                paramList.Add("W_PROMOTED", dicEmpPayRollDetail["W_PROMOTED"]);
                                flowRslt = cmnDM.Execute("CHRIS_SP_PROMOTION_PROC", "", paramList, ref processTrans, 60);
                                SetStatusBar("Processing in Promotion (promotion_proc)");
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables.Count > 0 && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }
                                #endregion
                            }
                            catch (Exception exp)
                            {
                                LogException(this.Name, "InitiateProcess--CHRIS_SP_PROMOTION_PROC", exp);
                            }
                            finally
                            {
                                Logging(this.Name, "PROMOTION_PROC PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                            }
                            #region Govt_allowance

                            if (flowRslt.isSuccessful && processState == FormMode.Processing)
                            {
                                //********************calling procedure for govt. allowance amount ------**********
                                try
                                {

                                    slTB_No.Text = "";
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_GRAT", dicEmpPayRollDetail["W_GRAT"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    flowRslt = cmnDM.Execute("CHRIS_SP_GOVT_ALL_AND_ALLOWANCES", "", paramList, ref processTrans, 60);
                                    SetStatusBar("Processing in Govt. Allowance (govt_all)");
                                    RefreshProgressControls();
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_GOVT_ALL_AND_ALLOWANCES", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "GOVT_ALLOWANCE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }
                            }
                            #endregion


                            #region Commented Code (Please Omit)
                            //#region Valid P_No Part 1

                            //String prCategory;
                            //Double prNewAnnualPack; 
                            //Double wAsr;
                            //Double wHrent;
                            //Double wConv; 
                            //Double wGrat; 

                            //prCategory = (dicEmpPayRollDetail["W_CATE"] == null || dicEmpPayRollDetail["W_CATE"].ToString().Equals(String.Empty))? String.Empty: dicEmpPayRollDetail["W_CATE"].ToString();
                            //prNewAnnualPack = (dicEmpPayRollDetail["W_AN_PACK"] == null || dicEmpPayRollDetail["W_AN_PACK"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_AN_PACK"].ToString());

                            //if (prCategory.ToUpper().Equals("C"))
                            //{
                            //    dicEmpPayRollDetail["W_ASR"] = prNewAnnualPack / 12;
                            //    ----- Govt. Allowance Proc Call Here ---------------
                            
                            //    dicEmpPayRollDetail["W_HRENT"] = 0;
                            //    dicEmpPayRollDetail["W_CONV"] = 0;
                            //    wAsr = (dicEmpPayRollDetail["W_ASR"] == null || dicEmpPayRollDetail["W_ASR"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_ASR"].ToString());
                            //    dicEmpPayRollDetail["W_GRAT"] = wAsr / 12;
                            //}
                            //else
                            //{
                            //    dicEmpPayRollDetail["W_ASR"] = ((prNewAnnualPack / 150.00) * 100.00) / 12.00;
                            //    dicEmpPayRollDetail["W_HRENT"] = ((prNewAnnualPack / 150.00) * 40.00) / 12.00;

                            //    dicEmpPayRollDetail["W_CONV"] = ((prNewAnnualPack / 150.00) * 10.00) / 12.00;
                            //    dicEmpPayRollDetail["W_GRAT"] = ((prNewAnnualPack / 150.00) * 100.00 * 1.30) / 12.00;
                            //    //------MAKE G V FOR PKS

                            //    wAsr = (dicEmpPayRollDetail["W_ASR"] == null || dicEmpPayRollDetail["W_ASR"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_ASR"].ToString());
                            //    wHrent = (dicEmpPayRollDetail["W_HRENT"] == null || dicEmpPayRollDetail["W_HRENT"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_HRENT"].ToString());
                            //    wConv = (dicEmpPayRollDetail["W_CONV"] == null || dicEmpPayRollDetail["W_CONV"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_CONV"].ToString());
                            //    wGrat = (dicEmpPayRollDetail["W_GRAT"] == null || dicEmpPayRollDetail["W_GRAT"].ToString().Equals(String.Empty)) ? 0 : Convert.ToDouble(dicEmpPayRollDetail["W_GRAT"].ToString());
                            //    dicEmpPayRollDetail["GLOBAL_PERKS"] = wAsr + wHrent + wConv + wGrat;
                            //}
                            //#endregion
                            #endregion


                            #region Provident Fund
                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //--************* Provident Fund Calculation **************************--
                                    slTB_No.Text = "5000412";
                                    paramList.Clear();
                                    paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_CONFIRM_ON", dicEmpPayRollDetail["W_CONFIRM_ON"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);

                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROVIDENT_FUND", "", paramList, ref processTrans, 60);
                                    SetStatusBar("Calculating P.F. (Valid_pno)");
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PROVIDENT_FUND", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "PROVIDENT FUND PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }
                            }
                            #endregion

                            #region Proc Shift
                            if (flowRslt.isSuccessful)
                            {
                                //--************* shift PROC FOR ALLOWANCE TABLE UPDATION**************************--

                                try
                                {
                                    slTB_No.Text = "5000416";
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);

                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROC_SHIFT", "", paramList, ref processTrans, 60);
                                    SetStatusBar("Processing in Allowance (proc_shift)");
                                    RefreshProgressControls();
                                    if (flowRslt.isSuccessful)
                                    {//if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        //{
                                        //    FillListWithOutputValues();
                                        //}
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PROC_SHIFT", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "PROC SHIFT PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }
                            }


                            #endregion

                            #region Overtime & inc_promo_check
                            if (flowRslt.isSuccessful)
                            {
                                //--************* -- check to calculate overtime for ctt to officers --  *******************--

                                DateTime pr_date, temp_w_date;
                                if (dicEmpPayRollDetail["W_PR_DATE"] != null && dicEmpPayRollDetail["W_PR_DATE"].ToString() != string.Empty)
                                {
                                    pr_date = Convert.ToDateTime(dicEmpPayRollDetail["W_PR_DATE"]);
                                    temp_w_date = Convert.ToDateTime(dicEmpPayRollDetail["W_DATE"]);
                                    if (pr_date.Month == temp_w_date.Month && pr_date.Year == temp_w_date.Year)
                                    {
                                        //--  flag up because inc_promo_check calls forcasted procedure to --********
                                        //slTB_No.Text = "";
                                        try
                                        {
                                            dicEmpPayRollDetail["GLOBAL_FLAG"] = "T";
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("GLOBAL_FLAG", dicEmpPayRollDetail["GLOBAL_FLAG"]);
                                            paramList.Add("W_ATT_FLAG", dicEmpPayRollDetail["W_ATT_FLAG"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                            paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                            paramList.Add("W_PR_DATE", dicEmpPayRollDetail["W_PR_DATE"]);
                                            paramList.Add("W_PROMOTED", dicEmpPayRollDetail["W_PROMOTED"]);
                                            paramList.Add("W_OT_ASR", dicEmpPayRollDetail["W_OT_ASR"]);
                                            paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                            flowRslt = cmnDM.Execute("CHRIS_SP_INC_PROMO_CHECK", "", paramList, ref processTrans, 60);
                                            SetStatusBar("Calculate OT For Ctt to Officers (inc_promo_check)");
                                            RefreshProgressControls();
                                            if (rslt.isSuccessful)
                                            {
                                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    FillListWithOutputValues();
                                                }
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                            LogException(this.Name, "InitiateProcess--CHRIS_SP_INC_PROMO_CHECK", exp);
                                            msg = exp.ToString();
                                        }
                                        finally
                                        {
                                            Logging(this.Name, "INC_PROMO_CHECK PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                        }

                                    }
                                    else
                                    {
                                        //****************-- calculate ot for regular --*******************
                                        //slTB_No.Text = "";
                                        try
                                        {
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                            paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                            paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                            flowRslt = cmnDM.Execute("CHRIS_SP_OVER_TIME", "", paramList, ref processTrans, 60);
                                            SetStatusBar("Processing in Overtime (over_time)");
                                            RefreshProgressControls();
                                            if (flowRslt.isSuccessful)
                                            {
                                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    FillListWithOutputValues();
                                                }
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                            LogException(this.Name, "InitiateProcess--CHRIS_SP_OVER_TIME", exp);
                                            msg = exp.ToString();
                                        }
                                        finally
                                        {
                                            Logging(this.Name, "OVER_TIME PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                        }
                                    }
                                }
                                else
                                {
                                    //****************-- calculate ot for regular --*******************
                                    //slTB_No.Text = "";
                                    try
                                    {
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                        paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                        paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                        flowRslt = cmnDM.Execute("CHRIS_SP_OVER_TIME", "", paramList, ref processTrans, 60);
                                        SetStatusBar("Processing in Overtime (over_time)");
                                        RefreshProgressControls();
                                        if (flowRslt.isSuccessful)
                                        {
                                            if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                            {
                                                FillListWithOutputValues();
                                            }
                                        }
                                    }
                                    catch (Exception exp)
                                    {
                                        LogException(this.Name, "InitiateProcess--CHRIS_SP_OVER_TIME", exp);
                                        msg = exp.ToString();
                                    }
                                    finally
                                    {
                                        Logging(this.Name, "OVER_TIME ESLE CASE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                    }
                                }

                            }
                            #endregion

                            #region Proc_allow

                            if (flowRslt.isSuccessful)
                            {
                                if (dicEmpPayRollDetail["W_PR_MONTH"] != null && dicEmpPayRollDetail["W_PR_MONTH"].ToString() != string.Empty)
                                {
                                    int temp_month = Convert.ToInt32(dicEmpPayRollDetail["W_PR_MONTH"]);
                                    if (temp_month == 12 && dt.Month == 1) //i.e. W_date.monthName =="JAN"
                                    {
                                        try
                                        {
                                            slTB_No.Text = "50002";
                                            SetStatusBar("Processing in Yearly Award (proc_allow)");
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                            paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                            RefreshProgressControls();
                                            flowRslt = cmnDM.Execute("CHRIS_SP_PROC_ALLOW", "", paramList, ref processTrans, 60);

                                        }
                                        catch (Exception exp)
                                        {
                                            LogException(this.Name, "InitiateProcess--CHRIS_SP_PROC_ALLOW", exp);
                                            msg = exp.ToString();
                                        }
                                        finally
                                        {
                                            Logging(this.Name, "PROC_ALLOW PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region att_award
                            if (flowRslt.isSuccessful)
                            {
                                if (dicEmpPayRollDetail["W_CATE"] != null && dicEmpPayRollDetail["W_CATE"].ToString() != string.Empty)
                                {
                                    if (dicEmpPayRollDetail["W_CATE"].ToString().Equals("C"))
                                    {
                                        try
                                        {
                                            slTB_No.Text = "50007";
                                            SetStatusBar("Processing in Att. Award (att_award)");
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_ATT_FLAG", dicEmpPayRollDetail["W_ATT_FLAG"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                            flowRslt = cmnDM.Execute("CHRIS_SP_ATT_AWARD", "", paramList, ref processTrans, 60);
                                            RefreshProgressControls();
                                            if (flowRslt.isSuccessful)
                                            {
                                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    FillListWithOutputValues();
                                                }
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                            LogException(this.Name, "InitiateProcess--CHRIS_SP_ATT_AWARD", exp);
                                            msg = exp.ToString();
                                        }
                                        finally
                                        {
                                            Logging(this.Name, "ATT_AWARD PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                        }

                                    }
                                }
                            }
                            #endregion

                            #region proc_1

                            if (flowRslt.isSuccessful)
                            {
                                //Processing for Last Payroll Date (proc_1)
                                try
                                {
                                    slTB_No.Text = "500016";
                                    SetStatusBar("Processing for Last Payroll Date (proc_1)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_SW_PAY", dicEmpPayRollDetail["W_SW_PAY"]);
                                    paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                    paramList.Add("W_DIVISOR", dicEmpPayRollDetail["W_DIVISOR"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                    paramList.Add("WW_BASIC", dicEmpPayRollDetail["WW_BASIC"]);
                                    paramList.Add("WW_CONV", dicEmpPayRollDetail["WW_CONV"]);
                                    paramList.Add("WW_HOUSE", dicEmpPayRollDetail["WW_HOUSE"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROC_1", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PROC_1", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "PROC_1 PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }

                            }
                            #endregion

                            #region Proc_6
                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO CALCULATE BACK DATED SALARY AREARS --
                                    slTB_No.Text = "500018";
                                    SetStatusBar("Processing in Salary Arears (proc_6)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_PA_DATE", dicEmpPayRollDetail["W_PA_DATE"]);
                                    paramList.Add("W_INC_MONTHS", dicEmpPayRollDetail["W_INC_MONTHS"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("W_BASIC_AREAR", dicEmpPayRollDetail["W_BASIC_AREAR"]);
                                    paramList.Add("W_HOUSE_AREAR", dicEmpPayRollDetail["W_HOUSE_AREAR"]);
                                    paramList.Add("W_CONV_AREAR", dicEmpPayRollDetail["W_CONV_AREAR"]);
                                    paramList.Add("W_PF_AREAR", dicEmpPayRollDetail["W_PF_AREAR"]);
                                    paramList.Add("GLOBAL_W_FOUND", dicEmpPayRollDetail["GLOBAL_W_FOUND"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROC_6", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables.Count > 0 && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PROC_6", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "PROC_6 PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }

                            }
                            #endregion

                            #region Pro-Rate Income Or Income-Tax

                            if (flowRslt.isSuccessful)
                            {
                                //--*********** TO PICK ALLOWANCES IN CASE OF LAST JOIN AND JOIN B/W THE MONTH*********************--

                                //if (dicEmpPayRollDetail["W_FLAG"] != null && dicEmpPayRollDetail["W_FLAG"].ToString() != string.Empty)
                                //{
                                    #region Pro-rate Income
                                    if (dicEmpPayRollDetail["W_FLAG"].ToString() == "L" || dicEmpPayRollDetail["W_FLAG"].ToString() == "J")
                                    {
                                        try
                                        {
                                            //--  ****Processing in Allowance for L,J (pro_rate_income)****
                                            slTB_No.Text = "500021";
                                            SetStatusBar("Processing in Allowance for L,J (pro_rate_income)");
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                            paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                            paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                            paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                            paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                            paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                            paramList.Add("W_DIVISOR", dicEmpPayRollDetail["W_DIVISOR"]);
                                            paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                            RefreshProgressControls();
                                            flowRslt = cmnDM.Execute("CHRIS_SP_PRO_RATE_INCOME", "", paramList, ref processTrans, 60);

                                            //if (rslt.isSuccessful)
                                            //{if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                            //    {FillListWithOutputValues();}}
                                        }
                                        catch (Exception exp)
                                        {
                                            LogException(this.Name, "InitiateProcess--CHRIS_SP_PRO_RATE_INCOME", exp);
                                            msg = exp.ToString();
                                        }
                                        finally
                                        {
                                            Logging(this.Name, "PRO-RATE INCOME PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                        }
                                    }
                                    #endregion

                                    #region Income-Tax
                                    else
                                    {
                                        try
                                        {
                                            //**************Processing in Allowance (income_tax)*********************
                                            SetStatusBar("Processing in Allowance (income_tax)");
                                            slTB_No.Text = "500023";
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                            paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                            paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                            RefreshProgressControls();
                                            flowRslt = cmnDM.Execute("CHRIS_SP_INCOME_TAX", "", paramList, ref processTrans, 60);
                                        }
                                        catch (Exception exp)
                                        {
                                            LogException(this.Name, "InitiateProcess--CHRIS_SP_INCOME_TAX", exp);
                                            msg = exp.ToString();
                                        }
                                        finally
                                        {
                                            Logging(this.Name, "Income-Tax PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                        }
                                    }
                                    #endregion
                                //}

                            }

                            #endregion

                            #region Salary_advance

                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- SALARY ADVANCE IF GIVEN BEFORE PAYROLL--
                                    slTB_No.Text = "5000261";
                                    SetStatusBar("Processing in Salary Advance(salary_advance)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_SAL_ADV", dicEmpPayRollDetail["W_SAL_ADV"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_SALARY_ADVANCE", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_SALARY_ADVANCE", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "SALARY_ADVANCE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }



                            }
                            #endregion

                            #region Up Deduction

                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO PICK DEDUCTION--
                                    slTB_No.Text = "5000271";
                                    SetStatusBar("Processing in Deduction (up_deduction)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_UP_DEDUCTION", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        //if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        //{
                                        //    FillListWithOutputValues();
                                        //}
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_UP_DEDUCTION", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "UP DEDUCTION PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }
                            }

                            #endregion

                            #region Fin-Book

                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO CALCULATE MONTH INSTALLMENT AND CALL FIN_CALC
                                    slTB_No.Text = "500029";
                                    SetStatusBar("Processing in Finance (fin_book)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_MARKUP_L", dicEmpPayRollDetail["W_MARKUP_L"]);
                                    paramList.Add("W_INST_L", dicEmpPayRollDetail["W_INST_L"]);
                                    paramList.Add("W_MARKUP_F", dicEmpPayRollDetail["W_MARKUP_F"]);
                                    paramList.Add("W_INST_F", dicEmpPayRollDetail["W_INST_F"]);
                                    paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                    paramList.Add("W_FINANCE", dicEmpPayRollDetail["W_FINANCE"]);
                                    paramList.Add("W_MONTH_DED", dicEmpPayRollDetail["W_MONTH_DED"]);
                                    paramList.Add("W_EXTRA", dicEmpPayRollDetail["W_EXTRA"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_FIN_BOOK", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }

                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_FIN_BOOK", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "FIN-BOOK PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }
                            }
                            #endregion

                            #region Valid PNo. Calculations

                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO CALCULATE BACK DATED SALARY AREARS --
                                    slTB_No.Text = "500030";
                                    SetStatusBar("Processing in P.F. Arears (Valid_pno)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                    paramList.Add("W_CONFIRM_ON", dicEmpPayRollDetail["W_CONFIRM_ON"]);
                                    paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                    paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                    paramList.Add("WW_BASIC", dicEmpPayRollDetail["WW_BASIC"]);
                                    paramList.Add("WW_CONV", dicEmpPayRollDetail["WW_CONV"]);
                                    paramList.Add("WW_HOUSE", dicEmpPayRollDetail["WW_HOUSE"]);
                                    paramList.Add("W_BASIC_AREAR", dicEmpPayRollDetail["W_BASIC_AREAR"]);
                                    paramList.Add("W_HOUSE_AREAR", dicEmpPayRollDetail["W_HOUSE_AREAR"]);
                                    paramList.Add("W_CONV_AREAR", dicEmpPayRollDetail["W_CONV_AREAR"]);
                                    paramList.Add("W_PF_AREAR", dicEmpPayRollDetail["W_PF_AREAR"]);
                                    paramList.Add("GLOBAL_W_FOUND", dicEmpPayRollDetail["GLOBAL_W_FOUND"]);
                                    paramList.Add("MDAYS", dicEmpPayRollDetail["MDAYS"]);
                                    paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                    paramList.Add("WPER_DAY", dicEmpPayRollDetail["WPER_DAY"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_WORK_DAY", dicEmpPayRollDetail["W_WORK_DAY"]);
                                    // 8 DEC,2011 | COM-530 | W_PAY_FLAG missing in parameter list and that causes PF Calculation issues.
                                    paramList.Add("W_PAY_FLAG", dicEmpPayRollDetail["W_PAY_FLAG"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_CALCULATIONS", "", paramList, ref processTrans, 0);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PAYROLL_CALCULATIONS", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "Valid PNo. Calculations PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }

                            }


                            #endregion

                            #region Z_TAX
                            if (flowRslt.isSuccessful)
                            {
                                //--*********** inserting record in payroll table ******** --
                                try
                                {
                                    slTB_No.Text = "500034";
                                    SetStatusBar("Processing Processing in Tax (z_tax)");
                                    paramList.Clear();

                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_ZAKAT", dicEmpPayRollDetail["W_ZAKAT"]);
                                    paramList.Add("W_PRV_INC", dicEmpPayRollDetail["W_PRV_INC"]);
                                    paramList.Add("W_TAX_USED", dicEmpPayRollDetail["W_TAX_USED"]);
                                    paramList.Add("W_REFUND", dicEmpPayRollDetail["W_REFUND"]);
                                    paramList.Add("W_PRV_TAX_PD", dicEmpPayRollDetail["W_PRV_TAX_PD"]);
                                    paramList.Add("W_ITAX", dicEmpPayRollDetail["W_ITAX"]);
                                    paramList.Add("W_PF_EXEMPT", dicEmpPayRollDetail["W_PF_EXEMPT"]);
                                    paramList.Add("W_SYS", dicEmpPayRollDetail["W_SYS"]);
                                    paramList.Add("GLOBAL_W_10_AMT", dicEmpPayRollDetail["GLOBAL_W_10_AMT"]);
                                    paramList.Add("GLOBAL_W_INC_AMT", dicEmpPayRollDetail["GLOBAL_W_INC_AMT"]);
                                    paramList.Add("W_VP", dicEmpPayRollDetail["W_VP"]);
                                    paramList.Add("SCREEN_TYPE", "PG".ToUpper());
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_GENERATION_Z_TAX", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                    slTB_TotalTaxableAmount.Text = dicEmpPayRollDetail["Z_TAXABLE_AMOUNT"].ToString();
                                    slTB_LoanTaxAmount.Text = dicEmpPayRollDetail["SUBS_LOAN_TAX_AMT"].ToString();
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PAYROLL_GENERATION_Z_TAX", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "Z_TAX PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }

                            }
                            #endregion

                            #region Payroll_insertions

                            if (flowRslt.isSuccessful)
                            {
                                //--*********** inserting record in payroll table ******** --
                                slTB_No.Text = "500033";
                                SetStatusBar("Processing in P.F. Arears (Valid_pno)");

                                Double W_ASR; 
                                Double W_HRENT; 
                                Double W_CONV; 
                                Double W_PFUND; 
                                Double W_GRAT; 
                                Double W_ITAX; 
                                Double W_SAL_ADV; 
                                Double W_MARKUP_F; 
                                Double W_INST_F; 
                                Double W_MARKUP_L; 
                                Double W_INST_L; 
                                Double W_EXCISE;
                                Double W_ZAKAT;

                                if (Double.TryParse(dicEmpPayRollDetail["W_ASR"].ToString(), out W_ASR))
                                    dicEmpPayRollDetail["W_ASR"] = Math.Round(W_ASR, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_HRENT"].ToString(), out W_HRENT))
                                    dicEmpPayRollDetail["W_HRENT"] = Math.Round(W_HRENT, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_CONV"].ToString(), out W_CONV))
                                    dicEmpPayRollDetail["W_CONV"] = Math.Round(W_CONV, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_PFUND"].ToString(), out W_PFUND))
                                    dicEmpPayRollDetail["W_PFUND"] = Math.Round(W_PFUND, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_GRAT"].ToString(), out W_GRAT))
                                    dicEmpPayRollDetail["W_GRAT"] = Math.Round(W_GRAT, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_ITAX"].ToString(), out W_ITAX))
                                    dicEmpPayRollDetail["W_ITAX"] = Math.Round(W_ITAX, 0);

                                if (Double.TryParse(dicEmpPayRollDetail["W_SAL_ADV"].ToString(), out W_SAL_ADV))
                                    dicEmpPayRollDetail["W_SAL_ADV"] = Math.Round(W_SAL_ADV, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_MARKUP_F"].ToString(), out W_MARKUP_F))
                                    dicEmpPayRollDetail["W_MARKUP_F"] = Math.Round(W_MARKUP_F, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_INST_F"].ToString(), out W_INST_F))
                                    dicEmpPayRollDetail["W_INST_F"] = Math.Round(W_INST_F, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_MARKUP_L"].ToString(), out W_MARKUP_L))
                                    dicEmpPayRollDetail["W_MARKUP_L"] = Math.Round(W_MARKUP_L, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_INST_L"].ToString(), out W_INST_L))
                                    dicEmpPayRollDetail["W_INST_L"] = Math.Round(W_INST_L, 0);

                                if (Double.TryParse(dicEmpPayRollDetail["W_EXCISE"].ToString(), out W_EXCISE))
                                    dicEmpPayRollDetail["W_EXCISE"] = Math.Round(W_EXCISE, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_ZAKAT"].ToString(), out W_ZAKAT))
                                    dicEmpPayRollDetail["W_ZAKAT"] = Math.Round(W_ZAKAT, 2);

                                try
                                {
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO", dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_WORK_DAY", dicEmpPayRollDetail["W_WORK_DAY"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                    paramList.Add("W_GRAT", dicEmpPayRollDetail["W_GRAT"]);
                                    paramList.Add("W_ITAX", dicEmpPayRollDetail["W_ITAX"]);
                                    paramList.Add("W_SAL_ADV", dicEmpPayRollDetail["W_SAL_ADV"]);
                                    paramList.Add("W_MARKUP_F", dicEmpPayRollDetail["W_MARKUP_F"]);
                                    paramList.Add("W_INST_F", dicEmpPayRollDetail["W_INST_F"]);
                                    paramList.Add("W_MARKUP_L", dicEmpPayRollDetail["W_MARKUP_L"]);
                                    paramList.Add("W_INST_L", dicEmpPayRollDetail["W_INST_L"]);
                                    paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                    paramList.Add("W_ZAKAT", dicEmpPayRollDetail["W_ZAKAT"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    RefreshProgressControls();
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_INSERTIONS", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        //YOOOOOOOOOOOOOO
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogException(this.Name, "InitiateProcess--CHRIS_SP_PAYROLL_INSERTIONS", exp);
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                    Logging(this.Name, "PAYROLL_INSERTIONS PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                }

                            }
                            #endregion

                            #region If flowRslt is False
                            else if (!flowRslt.isSuccessful)
                            {
                                processState = FormMode.Failure;
                                if (processTrans != null && processTrans.Connection != null)
                                    processTrans.Rollback();
                                //if (conn.State != ConnectionState.Closed)
                                //    conn.Close();
                                base.LogError(this.Name, "InitiateProcess()", flowRslt.exp);
                                MessageBox.Show(flowRslt.exp.Message, "Error during Payroll Generation", MessageBoxButtons.OK);
                                return;// break;
                            }
                            #endregion


                            DateTime.TryParse(dicEmpPayRollDetail["WW_DATE"].ToString(), out dtWW_DATE);
                            Int32.TryParse(dicEmpPayRollDetail["W_SW_PAY"].ToString(), out W_SW_PAY);

                        }
                        if (processState != FormMode.Failure)
                            processState = FormMode.Successfull;
                    }
                    else
                        processState = FormMode.Failure;

                    if (processState == FormMode.Failure)
                    {
                        MessageBox.Show("Payroll cannot be generated.Check Log for Error Details", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        SetFormInInitialState();

                    }
                    else if (processState == FormMode.Successfull)
                    {
                        groupBoxProcessCompleted.Visible = true;
                        groupBoxProcessRunning.Enabled = false;
                        groupBoxMainProcess.Enabled = false;
                        //slTB_FinalAnswer.Focus();
                        //when done then TermSet_Clear();
                    }
                }

            }
            catch (Exception dbEcxep)
            {
                processState = FormMode.Failure;
                if (processTrans != null && processTrans.Connection != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
                base.LogException(this.Name, "InitiateProcess()", dbEcxep);
            }
            finally
            {
                slTB_Reply_For_Generation.Text = String.Empty;
            }

        }

        /// <summary>
        /// Fill DS with Updated Values.
        /// </summary>
        private void FillListWithOutputValues()
        {
            try
            {
                for (int tableIndex = 0; tableIndex < flowRslt.dstResult.Tables.Count; tableIndex++)
                {
                    for (int outVarCount = 0; outVarCount < flowRslt.dstResult.Tables[tableIndex].Columns.Count; outVarCount++)
                    {
                        if (dicEmpPayRollDetail.ContainsKey(flowRslt.dstResult.Tables[tableIndex].Columns[outVarCount].ColumnName))
                        {
                            dicEmpPayRollDetail[flowRslt.dstResult.Tables[tableIndex].Columns[outVarCount].ColumnName] = flowRslt.dstResult.Tables[tableIndex].Rows[0][outVarCount];
                        }
                    }
                }
            }
            catch (Exception fillValue)
            {
                processState = FormMode.Failure;
                base.LogException(this.Name, "FillListWithOutputValues", fillValue);
            }
        }


        /// <summary>
        /// Refreshes/repaints the controls that display the overall progress.
        /// </summary>
        private void RefreshProgressControls()
        {
            this.Refresh();
            slTB_Status.Refresh();
            labelStatusBar.Refresh();
            slTB_Pr_P_No.Refresh();
            slTB_TotalTaxableAmount.Refresh();
            slTB_LoanTaxAmount.Refresh();
        }

        #endregion

        #region Events


        private void slTB_Reply_For_Generation_Validating(object sender, CancelEventArgs e)
        {
            if (slTB_Reply_For_Generation.Text != string.Empty)
            {
                if (slTB_Reply_For_Generation.Text.ToUpper() == "YES")
                {
                    PreProcessing();
                    //decimal d; 
                    //if (decimal.TryParse(slTB_Transport_Vp.Text, out d))
                    //{MessageBox.Show("abcd");}

                }
                else if (slTB_Reply_For_Generation.Text.ToUpper() == "NO")
                {
                    this.Close();
                }
                else
                {
                    e.Cancel = true;

                }
            }
        }

        private void slTBMarginal_Impact_Validating(object sender, CancelEventArgs e)
        {
            if (slTBMarginal_Impact.Text.Length > 0)
            {
                if (slTBMarginal_Impact.Text.ToUpper() != "Y" && slTBMarginal_Impact.Text.ToUpper() != "N")
                {
                    MessageBox.Show(" Please Select Y for Yes OR N for No", "Question", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    e.Cancel = true;
                    // if (DialogResult.OK == MessageBox.Show(" Please Select Y for Yes OR N for No", "Question", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1))
                    //{

                    //}
                    //else
                    //{ }

                }

            }
            //else
            //    e.Cancel = true;
        }


        //private void slTB_FinalAnswer_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (slTB_FinalAnswer.Text != string.Empty)
        //        {
        //            if (slTB_FinalAnswer.Text.ToUpper() == "YES")
        //            {
        //                if (processState == FormMode.Successfull)
        //                    processTrans.Commit();
        //                TermSet_Clear();
        //                this.Close();//commit trans

        //            }
        //            else if (slTB_FinalAnswer.Text.ToUpper() == "NO")
        //            {
        //                if (processState == FormMode.Successfull)
        //                    processTrans.Rollback();
        //                TermSet_Clear();
        //                this.Close();
        //                //rollback trans
        //            }
        //            else
        //            {
        //                //  e.Cancel = true;

        //            }
        //        }
        //    }
        //    catch (Exception exce)
        //    {
        //        if (processTrans != null)
        //            processTrans.Rollback();
        //        //if (conn.State != ConnectionState.Closed)
        //        //    conn.Close();
        //        base.LogException(this.Name, "slTB_FinalAnswer_Validating", exce);
        //    }
        //}

        private void CHRIS_Processing_PayrollGeneration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (processTrans != null && processTrans.Connection != null)
                processTrans.Rollback();
        }

        private void slTB_FinalAnswer_Leave(object sender, EventArgs e)
        {
            try
            {
                if (slTB_FinalAnswer.Text != string.Empty)
                {
                    if (slTB_FinalAnswer.Text.ToUpper() == "YES")
                    {
                        if (processState == FormMode.Successfull)
                            processTrans.Commit();
                        TermSet_Clear();
                        this.Close();//commit trans

                    }
                    else if (slTB_FinalAnswer.Text.ToUpper() == "NO")
                    {
                        if (processState == FormMode.Successfull)
                            processTrans.Rollback();
                        TermSet_Clear();
                        this.Close();
                        //rollback trans
                    }
                    else
                    {
                        //  e.Cancel = true;

                    }
                }
            }
            catch (Exception exce)
            {
                if (processTrans != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
                base.LogException(this.Name, "slTB_FinalAnswer_Validating", exce);
            }

        }

        private void slTB_FinalAnswer_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar.ToString().Equals("\r"))
                {
                    if (slTB_FinalAnswer.Text != string.Empty)
                    {
                        if (slTB_FinalAnswer.Text.ToUpper() == "YES")
                        {
                            if (processState == FormMode.Successfull)
                                processTrans.Commit();
                            TermSet_Clear();
                            this.Close();//commit trans

                        }
                        else if (slTB_FinalAnswer.Text.ToUpper() == "NO")
                        {
                            if (processState == FormMode.Successfull)
                                processTrans.Rollback();
                            TermSet_Clear();
                            this.Close();
                            //rollback trans
                        }
                        else
                        {
                            //  e.Cancel = true;

                        }
                    }
                }
            }
            catch (Exception exce)
            {
                if (processTrans != null && processTrans.Connection != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
                base.LogException(this.Name, "slTB_FinalAnswer_Validating", exce);
                this.Close();

            }
        }

        private void chkResetTermFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (SetFieldStatue == true)
                {
                    //if (chkResetTermFlag.Checked)
                    {
                        this.rslt = cmnDM.Get("CHRIS_SP_TERM_MANAGER", "UpdateToNull");
                    }
                }
                
            }
            catch (Exception exp)
            {
                LogError("chkResetTermFlag_CheckedChanged", exp);
            }
        }

        private void CHRIS_Processing_PayrollGeneration_Load(object sender, EventArgs e)
        {
            
        }

        private void btnFlagReset_Click(object sender, EventArgs e)
        {
            if (pnlTermFlag.Visible == false)
            {
                SetFieldStatue = true;
                pnlTermFlag.Visible = true;

            }
            else
            {
                SetFieldStatue = false;
                pnlTermFlag.Visible = false;

            }

        }

        

        //private void slTB_Reply_For_Generation_TextChanged(object sender, EventArgs e)
        //{

        //}

        #endregion



    }
}