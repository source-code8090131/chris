namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_POSTDUMPProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanel1 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.slPanel2 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.btnClose = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.openBackUpDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveBackUpDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanel1.SuspendLayout();
            this.slPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(405, 0);
            // 
            // slPanel1
            // 
            this.slPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.slPanel1.ConcurrentPanels = null;
            this.slPanel1.Controls.Add(this.slPanel2);
            this.slPanel1.DataManager = null;
            this.slPanel1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel1.DependentPanels = null;
            this.slPanel1.DisableDependentLoad = false;
            this.slPanel1.EnableDelete = true;
            this.slPanel1.EnableInsert = true;
            this.slPanel1.EnableUpdate = true;
            this.slPanel1.EntityName = null;
            this.slPanel1.Location = new System.Drawing.Point(21, 71);
            this.slPanel1.MasterPanel = null;
            this.slPanel1.Name = "slPanel1";
            this.slPanel1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel1.Size = new System.Drawing.Size(355, 224);
            this.slPanel1.SPName = null;
            this.slPanel1.TabIndex = 0;
            // 
            // slPanel2
            // 
            this.slPanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.slPanel2.ConcurrentPanels = null;
            this.slPanel2.Controls.Add(this.btnClose);
            this.slPanel2.Controls.Add(this.btnProcess);
            this.slPanel2.Controls.Add(this.groupBox1);
            this.slPanel2.DataManager = null;
            this.slPanel2.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel2.DependentPanels = null;
            this.slPanel2.DisableDependentLoad = false;
            this.slPanel2.EnableDelete = true;
            this.slPanel2.EnableInsert = true;
            this.slPanel2.EnableUpdate = true;
            this.slPanel2.EntityName = null;
            this.slPanel2.Location = new System.Drawing.Point(10, 5);
            this.slPanel2.MasterPanel = null;
            this.slPanel2.Name = "slPanel2";
            this.slPanel2.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel2.Size = new System.Drawing.Size(332, 209);
            this.slPanel2.SPName = "CHRIS_SP_POSTDUMPPROCESS";
            this.slPanel2.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(89, 135);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(165, 41);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "EXIT";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(89, 88);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(165, 41);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "PROCESS START";
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 48);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Post Dump Process";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(240, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 16;
            this.lblUserName.Text = "User Name  :";
            // 
            // openBackUpDialog
            // 
            this.openBackUpDialog.Filter = "Backup File|*.*";
            // 
            // saveBackUpDialog
            // 
            this.saveBackUpDialog.Filter = "Backup File|*.bak";
            // 
            // CHRIS_Processing_POSTDUMPProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(441, 374);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.slPanel1);
            this.Name = "CHRIS_Processing_POSTDUMPProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS -  Post Dump Process";
            this.Controls.SetChildIndex(this.slPanel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanel1.ResumeLayout(false);
            this.slPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel1;
        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel2;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.OpenFileDialog openBackUpDialog;
        private System.Windows.Forms.SaveFileDialog saveBackUpDialog;
    }
}