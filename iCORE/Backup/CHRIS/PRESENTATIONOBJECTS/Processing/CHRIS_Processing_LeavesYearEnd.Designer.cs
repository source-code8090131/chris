namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_LeavesYearEnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblUserName = new System.Windows.Forms.Label();
            this.slPanelLeavesEnd = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.slTxtBoxProcess = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelLeavesEnd.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(485, 0);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(348, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 15;
            this.lblUserName.Text = "User Name  :";
            // 
            // slPanelLeavesEnd
            // 
            this.slPanelLeavesEnd.ConcurrentPanels = null;
            this.slPanelLeavesEnd.Controls.Add(this.textBoxDate);
            this.slPanelLeavesEnd.Controls.Add(this.slTxtBoxProcess);
            this.slPanelLeavesEnd.Controls.Add(this.label2);
            this.slPanelLeavesEnd.Controls.Add(this.label1);
            this.slPanelLeavesEnd.Controls.Add(this.labelHeading);
            this.slPanelLeavesEnd.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelLeavesEnd.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelLeavesEnd.DependentPanels = null;
            this.slPanelLeavesEnd.DisableDependentLoad = false;
            this.slPanelLeavesEnd.EnableDelete = false;
            this.slPanelLeavesEnd.EnableInsert = false;
            this.slPanelLeavesEnd.EnableUpdate = false;
            this.slPanelLeavesEnd.EntityName = null;
            this.slPanelLeavesEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slPanelLeavesEnd.Location = new System.Drawing.Point(34, 90);
            this.slPanelLeavesEnd.MasterPanel = null;
            this.slPanelLeavesEnd.Name = "slPanelLeavesEnd";
            this.slPanelLeavesEnd.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.slPanelLeavesEnd.Size = new System.Drawing.Size(444, 224);
            this.slPanelLeavesEnd.SPName = "CHRIS_SP_LEAVES_MANAGER";
            this.slPanelLeavesEnd.TabIndex = 16;
            // 
            // textBoxDate
            // 
            this.textBoxDate.Location = new System.Drawing.Point(169, 111);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.ReadOnly = true;
            this.textBoxDate.Size = new System.Drawing.Size(100, 20);
            this.textBoxDate.TabIndex = 13;
            this.textBoxDate.TabStop = false;
            // 
            // slTxtBoxProcess
            // 
            this.slTxtBoxProcess.AllowSpace = true;
            this.slTxtBoxProcess.AssociatedLookUpName = "";
            this.slTxtBoxProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBoxProcess.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBoxProcess.ContinuationTextBox = null;
            this.slTxtBoxProcess.CustomEnabled = true;
            this.slTxtBoxProcess.DataFieldMapping = "";
            this.slTxtBoxProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBoxProcess.GetRecordsOnUpDownKeys = false;
            this.slTxtBoxProcess.IsDate = false;
            this.slTxtBoxProcess.Location = new System.Drawing.Point(278, 152);
            this.slTxtBoxProcess.MaxLength = 3;
            this.slTxtBoxProcess.Name = "slTxtBoxProcess";
            this.slTxtBoxProcess.NumberFormat = "###,###,##0.00";
            this.slTxtBoxProcess.Postfix = "";
            this.slTxtBoxProcess.Prefix = "";
            this.slTxtBoxProcess.Size = new System.Drawing.Size(51, 20);
            this.slTxtBoxProcess.SkipValidation = false;
            this.slTxtBoxProcess.TabIndex = 0;
            this.slTxtBoxProcess.TextType = CrplControlLibrary.TextType.String;
            this.slTxtBoxProcess.TextChanged += new System.EventHandler(this.slTxtBoxProcess_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(236, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = " [YES] Start Processing OR [NO] To Exit";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "TODAY IS";
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = true;
            this.labelHeading.Location = new System.Drawing.Point(125, 14);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(195, 39);
            this.labelHeading.TabIndex = 0;
            this.labelHeading.Text = "          PAYROLL SYSTEM \n \n (LEAVES YEAR END PROCESS)";
            // 
            // CHRIS_Processing_LeavesYearEnd
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(521, 359);
            this.Controls.Add(this.slPanelLeavesEnd);
            this.Controls.Add(this.lblUserName);
            this.CurrentPanelBlock = "slPanelLeavesEnd";
            this.Name = "CHRIS_Processing_LeavesYearEnd";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.Text = "LeavesYearEnd";
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.slPanelLeavesEnd, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelLeavesEnd.ResumeLayout(false);
            this.slPanelLeavesEnd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelLeavesEnd;
        private System.Windows.Forms.Label labelHeading;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTxtBoxProcess;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDate;

    }
}