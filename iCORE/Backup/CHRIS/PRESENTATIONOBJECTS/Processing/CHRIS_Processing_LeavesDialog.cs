using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using System.Collections;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;





namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_LeavesDialog : Form
    {
        CmnDataManager cmnDM = new CmnDataManager();
        bool isCompleted = false;
        public CHRIS_Processing_LeavesDialog()
        {
            InitializeComponent();
        }
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        private void CHRIS_Processing_LeavesDialog_Load(object sender, EventArgs e)
        {
             panelInProgress.Visible = true;
             panelProcessCompleted.Visible = true;       
             PerformLeavesYearEndProcess();
               
        }

        private void PerformLeavesYearEndProcess()
        {
            try
            {
                Dictionary<String, Object> paramWithVals = new Dictionary<String, Object>();
                Result rsltCode;
                DataSet dsResult = new DataSet();

                rsltCode = cmnDM.Execute("CHRIS_SP_LEAVES_MANAGER", "Delete",paramWithVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.hstOutParams["Table0.ORETVAL0"].ToString() == "-40")
                    {
                       // MessageBox.Show("Processing Is In Progress On Other Terminal .......!", "Information", MessageBoxButtons.OK);
                       if(DialogResult.OK== MessageBox.Show("Processing Is In Progress On Other Terminal .......!", "Information", MessageBoxButtons.OK)) // panelProcessCompleted.Visible = false;
                        this.Close();
                    }
                    else
                    {
                        panelProcessCompleted.Visible = true;
                        panelInProgress.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show(rsltCode.exp.Message, "Error", MessageBoxButtons.OK);

                }
            }
            catch (Exception e)
            {
               
            }
        }

        private void slTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
            //Table0.ORETVAL0//hstOutPutParam[Table0.ORETVAL0]
                this.isCompleted = true;
                this.Close();
            }
        }

       
    }
}