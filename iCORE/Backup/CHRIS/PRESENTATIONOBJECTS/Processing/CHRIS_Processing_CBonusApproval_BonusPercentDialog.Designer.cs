namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_CBonusApproval_BonusPercentDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.slTxtBonusPercent = new CrplControlLibrary.SLTextBox(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(99, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ENTER THE % FOR 10_C BONUS .............";
            // 
            // slTxtBonusPercent
            // 
            this.slTxtBonusPercent.AllowSpace = true;
            this.slTxtBonusPercent.AssociatedLookUpName = "";
            this.slTxtBonusPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBonusPercent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBonusPercent.ContinuationTextBox = null;
            this.slTxtBonusPercent.CustomEnabled = true;
            this.slTxtBonusPercent.DataFieldMapping = "";
            this.slTxtBonusPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBonusPercent.GetRecordsOnUpDownKeys = false;
            this.slTxtBonusPercent.IsDate = false;
            this.slTxtBonusPercent.Location = new System.Drawing.Point(366, 5);
            this.slTxtBonusPercent.MaxLength = 5;
            this.slTxtBonusPercent.Name = "slTxtBonusPercent";
            this.slTxtBonusPercent.NumberFormat = "###,###,##0.00";
            this.slTxtBonusPercent.Postfix = "";
            this.slTxtBonusPercent.Prefix = "";
            this.slTxtBonusPercent.Size = new System.Drawing.Size(41, 20);
            this.slTxtBonusPercent.SkipValidation = false;
            this.slTxtBonusPercent.TabIndex = 1;
            this.slTxtBonusPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.slTxtBonusPercent.TextType = CrplControlLibrary.TextType.Amount;
            this.slTxtBonusPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.slTxtBonusPercent_KeyPress);
            // 
            // CHRIS_Processing_CBonusApproval_BonusPercentDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(492, 31);
            this.ControlBox = false;
            this.Controls.Add(this.slTxtBonusPercent);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Processing_CBonusApproval_BonusPercentDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CHRIS_Processing_CBonusApproval_BonusPercentDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTxtBonusPercent;
    }
}