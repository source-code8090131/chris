namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_LeavesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelProcessCompleted = new System.Windows.Forms.Panel();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.panelInProgress = new System.Windows.Forms.Panel();
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panelProcessCompleted.SuspendLayout();
            this.panelInProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelProcessCompleted
            // 
            this.panelProcessCompleted.Controls.Add(this.slTextBox1);
            this.panelProcessCompleted.Controls.Add(this.label3);
            this.panelProcessCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelProcessCompleted.Location = new System.Drawing.Point(24, 31);
            this.panelProcessCompleted.Name = "panelProcessCompleted";
            this.panelProcessCompleted.Size = new System.Drawing.Size(434, 53);
            this.panelProcessCompleted.TabIndex = 9;
            this.panelProcessCompleted.Visible = false;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(265, 21);
            this.slTextBox1.MaxLength = 1;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(51, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 8;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.slTextBox1_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 39);
            this.label3.TabIndex = 1;
            this.label3.Text = " PROCESS COMPLETED \r\n \r\n Press Enter To Continue......";
            // 
            // panelInProgress
            // 
            this.panelInProgress.Controls.Add(this.slTextBox2);
            this.panelInProgress.Controls.Add(this.label1);
            this.panelInProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelInProgress.Location = new System.Drawing.Point(42, 31);
            this.panelInProgress.Name = "panelInProgress";
            this.panelInProgress.Size = new System.Drawing.Size(434, 53);
            this.panelInProgress.TabIndex = 10;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(265, 21);
            this.slTextBox2.MaxLength = 1;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(51, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 8;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = " Processing         Press Wait......";
            // 
            // CHRIS_Processing_LeavesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(539, 99);
            this.ControlBox = false;
            this.Controls.Add(this.panelInProgress);
            this.Controls.Add(this.panelProcessCompleted);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Processing_LeavesDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CHRIS_Processing_LeavesDialog_Load);
            this.panelProcessCompleted.ResumeLayout(false);
            this.panelProcessCompleted.PerformLayout();
            this.panelInProgress.ResumeLayout(false);
            this.panelInProgress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelProcessCompleted;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelInProgress;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private System.Windows.Forms.Label label1;
    }
}