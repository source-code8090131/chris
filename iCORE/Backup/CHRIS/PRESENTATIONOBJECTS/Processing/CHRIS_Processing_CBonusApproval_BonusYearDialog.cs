using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_CBonusApproval_BonusYearDialog : Form
    {

        #region Declaration
        //CHRIS_Processing_CBonusApproval_BonusPercentDialog objCHRIS_Processing_CBonusApproval_BonusPercentDialog;
        #endregion

        #region Properties
        bool isCompleted = false;
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

        private string m_BonusYear;
        public String BonusYear
        {
            get { return m_BonusYear; }
            set { m_BonusYear = value; }
        }
        #endregion

        #region Constructor
        public CHRIS_Processing_CBonusApproval_BonusYearDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void CHRIS_Processing_CBonusApproval_BonusYearDialog_Load(object sender, EventArgs e)
        {

        }
        private void slTxtBonusYear_KeyPress(object sender, KeyPressEventArgs e)       
        {
            if ((this.slTxtBonusYear.Text == "") && (e.KeyChar == '\r'))
            {
                MessageBox.Show("Please Enter A Valid Year ......", "Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if ((this.slTxtBonusYear.Text != "") && (e.KeyChar == '\r'))
            {
                //this.slTxtBonusYear.Text = this.slTxtBonusYear.Text.PadLeft(4,"20");
                if (Convert.ToInt32(this.slTxtBonusYear.Text) <= System.DateTime.Now.Year)
                {
                    isCompleted = true;
                    BonusYear = this.slTxtBonusYear.Text;
                    this.Close();
                    //objCHRIS_Processing_CBonusApproval_BonusPercentDialog = new CHRIS_Processing_CBonusApproval_BonusPercentDialog();
                    //objCHRIS_Processing_CBonusApproval_BonusPercentDialog.BonusYear = this.slTxtBonusYear.Text;
                    //objCHRIS_Processing_CBonusApproval_BonusPercentDialog.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Please Enter A Valid Year ......", "Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }
        #endregion
    }
}