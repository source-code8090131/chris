namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_CBonusApproval_BonusYearDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.slTxtBonusYear = new CrplControlLibrary.SLTextBox(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(352, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter Year For Which Bonus Has To Be Calculated .............";
            // 
            // slTxtBonusYear
            // 
            this.slTxtBonusYear.AllowSpace = true;
            this.slTxtBonusYear.AssociatedLookUpName = "";
            this.slTxtBonusYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBonusYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBonusYear.ContinuationTextBox = null;
            this.slTxtBonusYear.CustomEnabled = true;
            this.slTxtBonusYear.DataFieldMapping = "";
            this.slTxtBonusYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBonusYear.GetRecordsOnUpDownKeys = false;
            this.slTxtBonusYear.IsDate = false;
            this.slTxtBonusYear.Location = new System.Drawing.Point(401, 5);
            this.slTxtBonusYear.MaxLength = 4;
            this.slTxtBonusYear.Name = "slTxtBonusYear";
            this.slTxtBonusYear.NumberFormat = "###,###,##0.00";
            this.slTxtBonusYear.Postfix = "";
            this.slTxtBonusYear.Prefix = "";
            this.slTxtBonusYear.Size = new System.Drawing.Size(40, 20);
            this.slTxtBonusYear.SkipValidation = false;
            this.slTxtBonusYear.TabIndex = 1;
            this.slTxtBonusYear.TextType = CrplControlLibrary.TextType.Integer;
            this.slTxtBonusYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.slTxtBonusYear_KeyPress);
            // 
            // CHRIS_Processing_CBonusApproval_BonusYearDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(492, 31);
            this.ControlBox = false;
            this.Controls.Add(this.slTxtBonusYear);
            this.Controls.Add(this.label1);
            this.Name = "CHRIS_Processing_CBonusApproval_BonusYearDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CHRIS_Processing_CBonusApproval_BonusYearDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox slTxtBonusYear;
    }
}