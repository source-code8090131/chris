using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_CBonusApproval : ChrisSimpleForm
    {

        #region Declaration
        CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog objCHRIS_Processing_CBonusApproval_BonusSaveRecordDialog;
        CHRIS_Processing_CBonusApproval_BonusYearDialog objCHRIS_Processing_CBonusApproval_BonusYearDialog;
        CHRIS_Processing_CBonusApproval_BonusPercentDialog objCHRIS_Processing_CBonusApproval_BonusPercentDialog;
        CHRIS_Processing_CBonusApproval_Bonus objCHRIS_Processing_CBonusApproval_Bonus;
        #endregion

        #region Constructor
        public CHRIS_Processing_CBonusApproval()
        {
            InitializeComponent();
        }
        public CHRIS_Processing_CBonusApproval(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region Events

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            this.tbtClose.Visible = true;
        }

        private void CHRIS_Processing_CBonusApproval_Load(object sender, EventArgs e)
        {
            this.tlbMain.Visible = false;
        }             
        private void slTxtStartCalculation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.slTxtStartCalculation.Text.Trim() != String.Empty) && (e.KeyChar == '\r'))
            {

                if (this.slTxtStartCalculation.Text.ToLower().Equals("no"))
                {
                    tbtClose.PerformClick();
                    return;
                }
                objCHRIS_Processing_CBonusApproval_BonusSaveRecordDialog = new CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog();
                
                //open BonusSaveRecordDialog
                objCHRIS_Processing_CBonusApproval_BonusSaveRecordDialog.ShowDialog();

                if (objCHRIS_Processing_CBonusApproval_BonusSaveRecordDialog.IsCompleted)
                {
                    //close BonusSaveRecordDialog
                    objCHRIS_Processing_CBonusApproval_BonusSaveRecordDialog.Close();
                                
                    objCHRIS_Processing_CBonusApproval_BonusYearDialog = new CHRIS_Processing_CBonusApproval_BonusYearDialog();
                    
                    //Open BonusYearDialog
                    objCHRIS_Processing_CBonusApproval_BonusYearDialog.ShowDialog();

                    if (objCHRIS_Processing_CBonusApproval_BonusYearDialog.IsCompleted)
                    {                                        
                        objCHRIS_Processing_CBonusApproval_BonusPercentDialog = new CHRIS_Processing_CBonusApproval_BonusPercentDialog();
                        objCHRIS_Processing_CBonusApproval_BonusPercentDialog.BonusYear = objCHRIS_Processing_CBonusApproval_BonusYearDialog.BonusYear;
                        
                        //close BonusYearDialog
                        objCHRIS_Processing_CBonusApproval_BonusYearDialog.Close();

                        //open BonusPercentDialog
                        objCHRIS_Processing_CBonusApproval_BonusPercentDialog.ShowDialog();

                        if (objCHRIS_Processing_CBonusApproval_BonusPercentDialog.IsCompleted)
                        {
                            objCHRIS_Processing_CBonusApproval_Bonus = new CHRIS_Processing_CBonusApproval_Bonus();

                            objCHRIS_Processing_CBonusApproval_Bonus.BonusYear = objCHRIS_Processing_CBonusApproval_BonusPercentDialog.BonusYear;
                            objCHRIS_Processing_CBonusApproval_Bonus.BonusPercent = objCHRIS_Processing_CBonusApproval_BonusPercentDialog.BonusPercent;

                            //close BonusPercentDialog
                            objCHRIS_Processing_CBonusApproval_BonusPercentDialog.Close();

                            this.Close();

                            //open Bonus
                            objCHRIS_Processing_CBonusApproval_Bonus.ShowDialog();
                            
                        }

                        
                    }
                      
                }
            }
        }
        #endregion

        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            slTxtStartCalculation.Focus();
            this.txtOption.Visible = false;
            
        }
        #endregion

    }
}