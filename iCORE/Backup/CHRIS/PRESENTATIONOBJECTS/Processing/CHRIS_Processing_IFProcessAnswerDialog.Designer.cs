namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_IFProcessAnswerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.slPanel2 = new iCORE.COMMON.SLCONTROLS.SLPanel(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txtAnswer = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.slPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // slPanel2
            // 
            this.slPanel2.ConcurrentPanels = null;
            this.slPanel2.Controls.Add(this.label2);
            this.slPanel2.Controls.Add(this.txtAnswer);
            this.slPanel2.Controls.Add(this.label1);
            this.slPanel2.DataManager = null;
            this.slPanel2.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanel2.DependentPanels = null;
            this.slPanel2.DisableDependentLoad = false;
            this.slPanel2.EnableDelete = true;
            this.slPanel2.EnableInsert = true;
            this.slPanel2.EnableUpdate = true;
            this.slPanel2.EntityName = null;
            this.slPanel2.Location = new System.Drawing.Point(38, 10);
            this.slPanel2.MasterPanel = null;
            this.slPanel2.Name = "slPanel2";
            this.slPanel2.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanel2.Size = new System.Drawing.Size(593, 86);
            this.slPanel2.SPName = null;
            this.slPanel2.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(225, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "PROCESS COMPLETED";
            // 
            // txtAnswer
            // 
            this.txtAnswer.AllowSpace = true;
            this.txtAnswer.AssociatedLookUpName = "";
            this.txtAnswer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnswer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnswer.ContinuationTextBox = null;
            this.txtAnswer.CustomEnabled = true;
            this.txtAnswer.DataFieldMapping = "";
            this.txtAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnswer.GetRecordsOnUpDownKeys = false;
            this.txtAnswer.IsDate = false;
            this.txtAnswer.Location = new System.Drawing.Point(367, 45);
            this.txtAnswer.MaxLength = 1;
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.NumberFormat = "###,###,##0.00";
            this.txtAnswer.Postfix = "";
            this.txtAnswer.Prefix = "";
            this.txtAnswer.Size = new System.Drawing.Size(62, 20);
            this.txtAnswer.SkipValidation = false;
            this.txtAnswer.TabIndex = 0;
            this.txtAnswer.TextType = CrplControlLibrary.TextType.String;
            this.txtAnswer.TextChanged += new System.EventHandler(this.txtAnswer_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(73, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = " Press Any Key to Go Back to  Menu ";
            // 
            // CHRIS_Processing_IFProcessAnswerDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(667, 107);
            this.ControlBox = false;
            this.Controls.Add(this.slPanel2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_Processing_IFProcessAnswerDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.slPanel2.ResumeLayout(false);
            this.slPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanel slPanel2;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtAnswer;
        private System.Windows.Forms.Label label1;

    }
}