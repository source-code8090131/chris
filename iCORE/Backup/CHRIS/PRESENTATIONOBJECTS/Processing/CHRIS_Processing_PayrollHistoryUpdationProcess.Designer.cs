namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    partial class CHRIS_Processing_PayrollHistoryUpdationProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Processing_PayrollHistoryUpdationProcess));
            this.pnlProcessStatus = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lblProcessing = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.dtpProcessingDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.lblPNum = new System.Windows.Forms.Label();
            this.lblPDate = new System.Windows.Forms.Label();
            this.slTxtBoxProcess = new CrplControlLibrary.SLTextBox(this.components);
            this.lblYN = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblLoc = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlFinalAnswer = new System.Windows.Forms.Panel();
            this.txtAnswerDialog = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlProcessStatus.SuspendLayout();
            this.pnlFinalAnswer.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(633, 0);
            this.txtOption.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(669, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 515);
            this.panel1.Size = new System.Drawing.Size(669, 60);
            // 
            // pnlProcessStatus
            // 
            this.pnlProcessStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProcessStatus.ConcurrentPanels = null;
            this.pnlProcessStatus.Controls.Add(this.lblProcessing);
            this.pnlProcessStatus.Controls.Add(this.lbtnPNo);
            this.pnlProcessStatus.Controls.Add(this.txtPersNo);
            this.pnlProcessStatus.Controls.Add(this.dtpProcessingDate);
            this.pnlProcessStatus.Controls.Add(this.lblPNum);
            this.pnlProcessStatus.Controls.Add(this.lblPDate);
            this.pnlProcessStatus.Controls.Add(this.slTxtBoxProcess);
            this.pnlProcessStatus.Controls.Add(this.lblYN);
            this.pnlProcessStatus.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlProcessStatus.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlProcessStatus.DependentPanels = null;
            this.pnlProcessStatus.DisableDependentLoad = false;
            this.pnlProcessStatus.EnableDelete = false;
            this.pnlProcessStatus.EnableInsert = false;
            this.pnlProcessStatus.EnableQuery = false;
            this.pnlProcessStatus.EnableUpdate = false;
            this.pnlProcessStatus.EntityName = null;
            this.pnlProcessStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlProcessStatus.Location = new System.Drawing.Point(74, 202);
            this.pnlProcessStatus.MasterPanel = null;
            this.pnlProcessStatus.Name = "pnlProcessStatus";
            this.pnlProcessStatus.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.pnlProcessStatus.Size = new System.Drawing.Size(544, 234);
            this.pnlProcessStatus.SPName = "CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER";
            this.pnlProcessStatus.TabIndex = 0;
            // 
            // lblProcessing
            // 
            this.lblProcessing.AutoSize = true;
            this.lblProcessing.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessing.Location = new System.Drawing.Point(224, 202);
            this.lblProcessing.Name = "lblProcessing";
            this.lblProcessing.Size = new System.Drawing.Size(84, 15);
            this.lblProcessing.TabIndex = 0;
            this.lblProcessing.Text = "Processing.....";
            this.lblProcessing.Visible = false;
            this.lblProcessing.VisibleChanged += new System.EventHandler(this.lblProcessing_VisibleChanged);
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "";
            this.lbtnPNo.ActionType = "P_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = false;
            this.lbtnPNo.Location = new System.Drawing.Point(413, 91);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER";
            this.lbtnPNo.TabIndex = 0;
            this.lbtnPNo.TabStop = false;
            this.toolTip1.SetToolTip(this.lbtnPNo, "Select Desired Personnel Number");
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(294, 91);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(117, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 2;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // dtpProcessingDate
            // 
            this.dtpProcessingDate.CalendarFont = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProcessingDate.CustomEnabled = true;
            this.dtpProcessingDate.CustomFormat = "dd/MM/yyyy";
            this.dtpProcessingDate.DataFieldMapping = "W_DATE";
            this.dtpProcessingDate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpProcessingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProcessingDate.HasChanges = true;
            this.dtpProcessingDate.Location = new System.Drawing.Point(294, 36);
            this.dtpProcessingDate.Name = "dtpProcessingDate";
            this.dtpProcessingDate.NullValue = " ";
            this.dtpProcessingDate.Size = new System.Drawing.Size(117, 21);
            this.dtpProcessingDate.TabIndex = 1;
            this.dtpProcessingDate.Value = new System.DateTime(2010, 11, 26, 0, 0, 0, 0);
            // 
            // lblPNum
            // 
            this.lblPNum.AutoSize = true;
            this.lblPNum.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPNum.Location = new System.Drawing.Point(15, 94);
            this.lblPNum.Name = "lblPNum";
            this.lblPNum.Size = new System.Drawing.Size(201, 15);
            this.lblPNum.TabIndex = 8;
            this.lblPNum.Text = "Enter Personnel No. ...........................";
            // 
            // lblPDate
            // 
            this.lblPDate.AutoSize = true;
            this.lblPDate.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPDate.Location = new System.Drawing.Point(15, 39);
            this.lblPDate.Name = "lblPDate";
            this.lblPDate.Size = new System.Drawing.Size(241, 15);
            this.lblPDate.TabIndex = 7;
            this.lblPDate.Text = "Processing Date           (dd/mm/yyyy).............";
            // 
            // slTxtBoxProcess
            // 
            this.slTxtBoxProcess.AllowSpace = true;
            this.slTxtBoxProcess.AssociatedLookUpName = "";
            this.slTxtBoxProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTxtBoxProcess.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTxtBoxProcess.ContinuationTextBox = null;
            this.slTxtBoxProcess.CustomEnabled = true;
            this.slTxtBoxProcess.DataFieldMapping = "";
            this.slTxtBoxProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTxtBoxProcess.GetRecordsOnUpDownKeys = false;
            this.slTxtBoxProcess.IsDate = false;
            this.slTxtBoxProcess.Location = new System.Drawing.Point(294, 147);
            this.slTxtBoxProcess.MaxLength = 3;
            this.slTxtBoxProcess.Name = "slTxtBoxProcess";
            this.slTxtBoxProcess.NumberFormat = "###,###,##0.00";
            this.slTxtBoxProcess.Postfix = "";
            this.slTxtBoxProcess.Prefix = "";
            this.slTxtBoxProcess.Size = new System.Drawing.Size(117, 20);
            this.slTxtBoxProcess.SkipValidation = false;
            this.slTxtBoxProcess.TabIndex = 3;
            this.slTxtBoxProcess.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.slTxtBoxProcess, "Enter [Yes/No]");
            this.slTxtBoxProcess.TextChanged += new System.EventHandler(this.slTxtBoxProcess_TextChanged);
            this.slTxtBoxProcess.Leave += new System.EventHandler(this.slTxtBoxProcess_Leave);
            // 
            // lblYN
            // 
            this.lblYN.AutoSize = true;
            this.lblYN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYN.Location = new System.Drawing.Point(15, 150);
            this.lblYN.Name = "lblYN";
            this.lblYN.Size = new System.Drawing.Size(263, 15);
            this.lblYN.TabIndex = 6;
            this.lblYN.Text = " [YES] Start Processing       OR      [NO] To Exit";
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = true;
            this.labelHeading.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeading.Location = new System.Drawing.Point(243, 55);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(183, 75);
            this.labelHeading.TabIndex = 0;
            this.labelHeading.Text = "          PAYROLL SYSTEM \n \n  FINANCE GENERATION FOR \n \n  IS AND FAST STAFF ONLY";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(22, 68);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(40, 15);
            this.lblUser.TabIndex = 21;
            this.lblUser.Text = "User :";
            // 
            // lblLoc
            // 
            this.lblLoc.AutoSize = true;
            this.lblLoc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoc.Location = new System.Drawing.Point(12, 94);
            this.lblLoc.Name = "lblLoc";
            this.lblLoc.Size = new System.Drawing.Size(64, 15);
            this.lblLoc.TabIndex = 22;
            this.lblLoc.Text = "Location : ";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(383, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 25;
            this.lblUserName.Text = "User Name  :";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(82, 91);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 0;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(82, 65);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 0;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlFinalAnswer
            // 
            this.pnlFinalAnswer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFinalAnswer.Controls.Add(this.txtAnswerDialog);
            this.pnlFinalAnswer.Controls.Add(this.label3);
            this.pnlFinalAnswer.Location = new System.Drawing.Point(74, 443);
            this.pnlFinalAnswer.Name = "pnlFinalAnswer";
            this.pnlFinalAnswer.Size = new System.Drawing.Size(544, 42);
            this.pnlFinalAnswer.TabIndex = 0;
            this.pnlFinalAnswer.Visible = false;
            // 
            // txtAnswerDialog
            // 
            this.txtAnswerDialog.AllowSpace = true;
            this.txtAnswerDialog.AssociatedLookUpName = "";
            this.txtAnswerDialog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnswerDialog.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnswerDialog.ContinuationTextBox = null;
            this.txtAnswerDialog.CustomEnabled = true;
            this.txtAnswerDialog.DataFieldMapping = "";
            this.txtAnswerDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnswerDialog.GetRecordsOnUpDownKeys = false;
            this.txtAnswerDialog.IsDate = false;
            this.txtAnswerDialog.Location = new System.Drawing.Point(294, 8);
            this.txtAnswerDialog.MaxLength = 3;
            this.txtAnswerDialog.Name = "txtAnswerDialog";
            this.txtAnswerDialog.NumberFormat = "###,###,##0.00";
            this.txtAnswerDialog.Postfix = "";
            this.txtAnswerDialog.Prefix = "";
            this.txtAnswerDialog.Size = new System.Drawing.Size(117, 20);
            this.txtAnswerDialog.SkipValidation = false;
            this.txtAnswerDialog.TabIndex = 4;
            this.txtAnswerDialog.TextType = CrplControlLibrary.TextType.String;
            this.txtAnswerDialog.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAnswerDialog_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(279, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = " [YES] Exit With Save and [NO] Exit Without Save ";
            // 
            // CHRIS_Processing_PayrollHistoryUpdationProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(669, 575);
            this.Controls.Add(this.pnlFinalAnswer);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.pnlProcessStatus);
            this.Controls.Add(this.labelHeading);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblLoc);
            this.Name = "CHRIS_Processing_PayrollHistoryUpdationProcess";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRIS -  Payroll History Updation Process";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblLoc, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.Controls.SetChildIndex(this.labelHeading, 0);
            this.Controls.SetChildIndex(this.pnlProcessStatus, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.pnlFinalAnswer, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlProcessStatus.ResumeLayout(false);
            this.pnlProcessStatus.PerformLayout();
            this.pnlFinalAnswer.ResumeLayout(false);
            this.pnlFinalAnswer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlProcessStatus;
        private CrplControlLibrary.SLTextBox slTxtBoxProcess;
        private System.Windows.Forms.Label lblYN;
        private System.Windows.Forms.Label labelHeading;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblLoc;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label lblPNum;
        private System.Windows.Forms.Label lblPDate;
        private CrplControlLibrary.SLDatePicker dtpProcessingDate;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblProcessing;
        private System.Windows.Forms.Panel pnlFinalAnswer;
        private CrplControlLibrary.SLTextBox txtAnswerDialog;
        private System.Windows.Forms.Label label3;
    }
}