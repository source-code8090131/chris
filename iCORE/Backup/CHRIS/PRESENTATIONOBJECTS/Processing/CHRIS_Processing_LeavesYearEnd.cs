using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_LeavesYearEnd : ChrisSimpleForm
    {
       

        public CHRIS_Processing_LeavesYearEnd()
        {
            InitializeComponent();
        }

        public CHRIS_Processing_LeavesYearEnd(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {

            InitializeComponent();
            this.ShowOptionKeys = false;
            this.ShowOptionTextBox = false;
            tbtAdd.Visible = false;
            tbtDelete.Visible = false;
            tbtEdit.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
            tbtCancel.Visible = false;
            lblUserName.Text += "   " + ConfigurationManager.AppSettings["database"];
            textBoxDate.Text = DateTime.Today.ToShortDateString();
            
        }

      
       
   
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            textBoxDate.Text = DateTime.Today.ToShortDateString();
        }

       
        private void slTxtBoxProcess_Validating(object sender, CancelEventArgs e)
        {
            //if (this.slTxtBoxProcess.Text.Length > 0)
            //{
            //    if (this.slTxtBoxProcess.Text == "YES" || this.slTxtBoxProcess.Text == "NO")
            //    {
            //        if (this.slTxtBoxProcess.Text == "YES")
            //        {
            //            CHRIS_Processing_LeavesDialog process = new CHRIS_Processing_LeavesDialog();
            //            process.ShowDialog();
            //            if (process.IsCompleted)
            //            {
            //                this.Close();
            //            }
            //            slTxtBoxProcess.Focus();
            //        }
            //        else
            //            this.Close();
            //    }
            //    else
            //        e.Cancel = true;
            //}
            //else
            //    e.Cancel = true;


        }

        private void slTxtBoxProcess_TextChanged(object sender, EventArgs e)
        {
            if (this.slTxtBoxProcess.Text.Length > 0)
            {
                if (this.slTxtBoxProcess.Text == "YES" || this.slTxtBoxProcess.Text == "NO")
                {
                    if (this.slTxtBoxProcess.Text == "YES")
                    {
                        CHRIS_Processing_LeavesDialog process = new CHRIS_Processing_LeavesDialog();
                        process.ShowDialog();
                        if (process.IsCompleted)
                        {
                            this.Close();
                        }
                        slTxtBoxProcess.Focus();
                    }
                    else
                        this.Close();
                }
            }

        }

       
    }
}