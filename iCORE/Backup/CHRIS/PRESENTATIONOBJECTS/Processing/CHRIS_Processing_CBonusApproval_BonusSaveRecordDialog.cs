using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog : Form
    {

        #region Declaration
        //CHRIS_Processing_CBonusApproval_BonusYearDialog objCHRIS_Processing_CBonusApproval_BonusYearDialog;
        CmnDataManager objCmnDataManager;
        #endregion

        #region Properties
        bool isCompleted = false;
        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }
        #endregion

        #region Constructor
        public CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void CHRIS_Processing_CBonusApproval_BonusSaveRecordDialog_Load(object sender, EventArgs e)
        {

        }
        private void slTxtSaveRecord_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.slTxtSaveRecord.Text == "Y") && (e.KeyChar == '\r'))
            {
                SetTermCheckStatus();
                isCompleted = true;             
                this.Close();
                //this.Hide();
                //objCHRIS_Processing_CBonusApproval_BonusYearDialog = new CHRIS_Processing_CBonusApproval_BonusYearDialog();
                //objCHRIS_Processing_CBonusApproval_BonusYearDialog.ShowDialog();
                
            }
            else if ((this.slTxtSaveRecord.Text == "N") && (e.KeyChar == '\r'))
            {
                //this.Hide();
                //objCHRIS_Processing_CBonusApproval_BonusYearDialog = new CHRIS_Processing_CBonusApproval_BonusYearDialog();
                //objCHRIS_Processing_CBonusApproval_BonusYearDialog.ShowDialog();
                isCompleted = true;
                this.Close();    
            }
            

             
        }
        #endregion

        #region Methods
        private void SetTermCheckStatus()
        {      
            objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BonusApproval_Manager", "UpdateTermCheck");
        }
        #endregion
     
    }
}