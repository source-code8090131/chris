using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;




namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_CBonusApproval_Bonus : ChrisTabularForm
    {

        #region Declaration
        CmnDataManager objCmnDataManager;
        #endregion

        #region Properties

        private string m_BonusYear;
        public String BonusYear
        {
            get { return m_BonusYear; }
            set { m_BonusYear = value;}
        }
        
        private string m_BonusPercent;
        public String BonusPercent
        {
            get { return m_BonusPercent; }
            set { m_BonusPercent = value; }
            
        }

        #endregion

        #region Constructor
        public CHRIS_Processing_CBonusApproval_Bonus()
        {
            InitializeComponent();
            this.SetFormTitle = "";
        }
        #endregion

        #region Methods

        void PopulateDataGridView()
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("w_10_c", (object)BonusPercent);
                d.Add("w_year", (object)BonusYear);

                objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BonusApproval_Manager", "list", d);

                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        this.sldgvBonus.DataSource = rsltCode.dstResult.Tables[0];
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        void CheckTermFlagStatus()
        {
            try
            {
                objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BonusApproval_Manager", "CheckTermFlagStatus");

                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {

                        //If exist then show exception message
                        MessageBox.Show("Processing Is In Progress On Other Terminal .......!");
                        return;

                    }
                    else
                    {
                        //if not exist then populate grid
                        PopulateDataGridView();
                        SetApproveBy();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void SetApproveBy()
        {
            for (int i = 0; i < this.sldgvBonus.Rows.Count-1; i++)
            {
                this.sldgvBonus.Rows[i].Cells[4].Value = "Y";
            }
        }

        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();


                this.tbtDelete.Visible = false;
                tlbMain.Items["tbtSearch"].Visible = false;

                //Bold DataGridViewHeader
                Font newFontStyle = new Font(sldgvBonus.Font, FontStyle.Bold);
                sldgvBonus.ColumnHeadersDefaultCellStyle.Font = newFontStyle;

                CheckTermFlagStatus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            if (actionType == "Save")
            {
                bool isDataSaved = false;
                sldgvBonus.BeginEdit(true);
                foreach (DataGridViewRow bonusRow in this.sldgvBonus.Rows)
                {
                    bonusRow.Cells[4].Value = (bonusRow.Cells[4].EditedFormattedValue.ToString().Trim().Equals(String.Empty)) ? DBNull.Value : bonusRow.Cells[4].EditedFormattedValue;
                }


                base.AllGridsEndEdit(sldgvBonus); 

                for (int i = 0; i < sldgvBonus.Rows.Count - 1; i++)
                {
                    if (this.sldgvBonus.Rows[i].Cells[4].Value != null)
                    {
                        if (this.sldgvBonus.Rows[i].Cells[4].Value.ToString() == "Y")
                        {
                            #region Check Record in BonusTab
                            Dictionary<string, object> d = new Dictionary<string, object>();
                            d.Add("BO_P_NO", this.sldgvBonus.Rows[i].Cells["BO_P_NO"].Value);
                            d.Add("BO_10_D", (object)System.DateTime.Now.Date.ToString("dd/MM/yyyy"));
                            d.Add("BO_10C_BONUS", this.sldgvBonus.Rows[i].Cells["BO_10C_BONUS"].Value);
                            d.Add("BO_10C_APP", this.sldgvBonus.Rows[i].Cells["BO_10C_APP"].Value);

                            objCmnDataManager = new CmnDataManager();
                            Result rsltCode = objCmnDataManager.Execute("CHRIS_SP_BonusApproval_Manager", "CheckRecord_bonus_tab", d);
                            isDataSaved = true;
                            #endregion
                        }
                    }
                }
                if (isDataSaved)
                    MessageBox.Show("Record saved successfully");
            }
            else
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
            }


        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;

            this.txtOption.Visible = false;
            lblUserName.Text += "   " + base.UserName;
            slTextBox1.Text = base.UserName;
        }

        #endregion

        #region Events

        //protected override 

        private void sldgvBonus_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 6)
                {
                    #region Arrear
                    Decimal fBasic = 0, fGov = 0, fArear = 0, fBonusPercent, f10Cbonus;

                    if (this.sldgvBonus.CurrentRow.Cells["W_BASIC"].Value != null)
                    {
                        fBasic = Decimal.Parse(this.sldgvBonus.CurrentRow.Cells["W_BASIC"].Value.ToString());
                    }
                    if (this.sldgvBonus.CurrentRow.Cells["W_GOVT"].Value != null)
                    {
                        fGov = Decimal.Parse(this.sldgvBonus.CurrentRow.Cells["W_GOVT"].Value.ToString());
                    }
                    if (this.sldgvBonus.CurrentRow.Cells["W_AREAR"].EditedFormattedValue != null)
                    {
                        fArear = Decimal.Parse(this.sldgvBonus.CurrentRow.Cells["W_AREAR"].EditedFormattedValue.ToString());
                    }

                    fBonusPercent = Decimal.Parse(BonusPercent);

                    f10Cbonus = (((fBasic + fGov + fArear) / 100) * fBonusPercent);
                    this.sldgvBonus.CurrentRow.Cells["BO_10C_BONUS"].Value = f10Cbonus.ToString();
                    #endregion
                }
                else if (e.ColumnIndex == 4)
                {
                    //this.sldgvBonus.CurrentRow.Cells["BO_10C_APP"].Value = "Y";
                    //check it to true for insert/update
                    this.sldgvBonus.CurrentRow.Cells["CheckRecord"].Value = "Y";
                }    
         
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }       
        private void sldgvBonus_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)(e.Control)).CharacterCasing = CharacterCasing.Upper; 
            }
        }     
        
        #endregion
    }
}