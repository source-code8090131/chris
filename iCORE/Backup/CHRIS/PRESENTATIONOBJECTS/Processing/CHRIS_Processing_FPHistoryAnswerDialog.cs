using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;



namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Processing
{
    public partial class CHRIS_Processing_FPHistoryAnswerDialog : Form
    {

        string PDT;
        int PNUM;
        bool isCompleted = false;
        DataTable dtCalc = new DataTable();
        Dictionary<String, Object> objValues = new Dictionary<String, Object>();
        
        public CHRIS_Processing_FPHistoryAnswerDialog(string pdt, int pNum)
        {
            InitializeComponent();
            this.PDT = pdt;
            this.PNUM = pNum;
        }

        public bool IsCompleted
        {
            get { return this.isCompleted; }
            set { this.isCompleted = value; }
        }

         /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private DataTable ExecuteCalculation()
        {


            objValues.Clear();
            objValues.Add("W_DTE", this.PDT);
            objValues.Add("PR_P_NUM", this.PNUM);
            DataTable dtFPH = new DataTable();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            //rsltCode = cmnDM.Execute("CHRIS_SP_FIN_INSURANCE_MANAGER", "FIN_BOOK", objValues);
            rsltCode = cmnDM.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "FIN_BOOK", objValues);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    dtFPH = rsltCode.dstResult.Tables[0];
                }
            }

            return dtFPH;
            //MessageBox.Show("DO PROCESSING");


        }

        private void txtAnswerDialog_TextChanged(object sender, EventArgs e)
        {

            if (this.txtAnswerDialog.Text == "YES" || this.txtAnswerDialog.Text == "NO")
            {
                if (this.txtAnswerDialog.Text == "YES")
                {
                    //execution of Booking + Calculation Procedure [finally insert in FN_MONTH] //
                    dtCalc = ExecuteCalculation();

                    //executing valid_Pno -third screen procedure//
                    if (dtCalc.Rows.Count > 0 || dtCalc.Rows.Count == 0)
                    {
                        //Opens at Backend for an Instance with Screen 2//
                        //CHRIS_Processing_FPHistoryFinanceDialog FPHFinanceDialog = new CHRIS_Processing_FPHistoryFinanceDialog();
                        //FPHFinanceDialog.ShowDialog();

                        DataTable dtPHF = new DataTable();
                        Result rslt;
                        CmnDataManager cmnDataManager = new CmnDataManager();
                        //rslt = cmnDataManager.Execute("CHRIS_SP_FIN_INSURANCE_MANAGER", "VALID_PNO", objValues);
                        rslt = cmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "VALID_PNO", objValues);

                        if (rslt.isSuccessful)
                        {
                            //if (rslt.hstOutParams.Count > 0)
                            if (rslt.dstResult.Tables.Count > 0)
                            {
                                dtPHF = rslt.dstResult.Tables[0];
                            }
                        }


                    }

                }
                else
                    this.Close();
            }

            
        }

        private void txtAnswerDialog_Leave(object sender, EventArgs e)
        {

            


        }



    }
}