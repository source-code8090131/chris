using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_VehicleInsuranceEntry : ChrisSimpleForm
    {
        CmnDataManager cmnDM = new CmnDataManager();
        # region constructor
        public CHRIS_Finance_VehicleInsuranceEntry()
        {
            InitializeComponent();
        }
        public CHRIS_Finance_VehicleInsuranceEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion
        #region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtLocation.Text = this.CurrentLocation;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy"); ;
            this.txtUser.Text = this.UserName;
            dtpexpirydt.Value = null;
            dtPisssuedate.Value = null;
            this.tbtDelete.Visible = false;
            this.tbtList.Visible = false;
            this.tbtAdd.Visible = false;
            this.label19.Text += " " + this.UserName;

        }
        private bool Execute_query()
        {
            bool flag = false;
            Result rslt_query;
            Dictionary<string, object> param_query = new Dictionary<string, object>();
            param_query.Add("PR_P_NO", txtPersonnelNo.Text);
            param_query.Add("FN_FINANCE_NO", txtFinanceNo.Text);
            rslt_query = cmnDM.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Execute_Query", param_query);

            if (rslt_query.isSuccessful)
            {

                if (rslt_query.dstResult.Tables.Count > 0 && rslt_query.dstResult.Tables[0].Rows.Count > 0)
                {

                    txtInsurancePolicyNo.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtInsuranceCompnay.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    dtPisssuedate.Value = Convert.ToDateTime(rslt_query.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                    dtpexpirydt.Value = Convert.ToDateTime(rslt_query.dstResult.Tables[0].Rows[0].ItemArray[3].ToString());
                    txtVehicleRegistrationNo.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                    txtVehicleEngineNo.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();
                    txtVehicleChasisNo.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();
                    txtVehMake.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();
                    txtVehicleMedia.Text = rslt_query.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    flag = true;

                }
            }
            
                return flag;
            
        
        
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "List")
            {
                if (FunctionConfig.CurrentOption == Function.None)
                {
                    return;
                
                }           
            
            }



            if (actionType == "Save")
            {
                base.DoToolbarActions(this.Controls, "Save");
                CallReport("AUPR", "OLD");
                CallReport("AUP0", "NEW");
                base.ClearForm(pnlPersonnel.Controls);
                dtpexpirydt.Value = null;
                dtPisssuedate.Value = null;
                this.FunctionConfig.CurrentOption = Function.None;
                return;
            
            }

            if (actionType == "Cancel")
            {              
                //base.ClearForm(pnlPersonnel.Controls);
                base.DoToolbarActions(ctrlsCollection, actionType);
                dtPisssuedate.Value = null;
                dtpexpirydt.Value = null;
                txtFinanceNo.IsRequired = false;
                dtpexpirydt.IsRequired = false;
                dtPisssuedate.IsRequired = false;
                txtOption.Select();
                txtOption.Focus();
                txtFinanceNo.IsRequired = true;
                dtpexpirydt.IsRequired = true;
                dtPisssuedate.IsRequired = true;
                return;
            }

          
            base.DoToolbarActions(ctrlsCollection, actionType);

        }
        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            //txtDESNAME.Name = "txtDESNAME";
            //txtDESNAME.Text = "";      //":GLOBAL.AUDIT_PATH||FN1";
            //pnl.Controls.Add(txtDESNAME);

            //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            //txtMODE.Name = "txtMODE";
            //txtMODE.Text = "CHARACTER";
            //pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToShortDateString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "pno";
            txtPNO.Text = txtPersonnelNo.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "fin_No";
            txtfinNo.Text = txtFinanceNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "audit02A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }
        //protected override bool Save()
        //{
        //    base.DoToolbarActions(this.Controls, "Save");
            
        //    return false;
        //}

        #endregion
        # region Events Handlers
        // if :FIN_INSURANCE.fn_FINANCE_NO IS NOT NULL AND :w_option = 'A' THEN
        private void CHRIS_Finance_VehicleInsuranceEntry_AfterLOVSelection(DataRow selected, string actionType)
        {

            //if (actionType == "Fin_Lov")
            //{


            //    if ((txtFinanceNo != null || txtFinanceNo.Text != string.Empty) && (this.FunctionConfig.CurrentOption == Function.Add))
            //    {
            //    }
            //    else
            //      {
                     
            //    }

            //}
        }
        private void txtVehicleMedia_Validating(object sender, CancelEventArgs e)
        {
            //if ((this.FunctionConfig.CurrentOption == Function.Modify) || (this.FunctionConfig.CurrentOption == Function.Modify))
            //{
            //    if (MessageBox.Show(" Do you want to save this record? ", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //   {
            //       base.DoToolbarActions(this.Controls, "Save");
            //       CallReport("AUPR", "OLD");
            //       CallReport("AUP0", "NEW");
            //       base.ClearForm(pnlPersonnel.Controls);
            //       dtpexpirydt.Value = null;
            //       dtPisssuedate.Value = null;
            //       this.FunctionConfig.CurrentOption = Function.None;

            //  }


            //  else
            //  {
            //      this.DoToolbarActions(this.Controls, "Cancel");

            //  }
            //}

        }
        private void slDatePicker1_Validating(object sender, CancelEventArgs e)
        {
            // DateTime date2 = new DateTime(2009, 8, 1, 12, 0, 0);

            if (dtPisssuedate.Value != null && dtpexpirydt.Value != null)
            {
                DateTime dtIssDate = (DateTime)(this.dtPisssuedate.Value);
                DateTime dtExpDate = (DateTime)(this.dtpexpirydt.Value);

                TimeSpan ts = dtExpDate.Subtract(dtIssDate);
                int days = ts.Days;
                if (days <= 0)
                {
                    MessageBox.Show(" Policy Expiry Date should be greater than Policy Issue Date ");
                    e.Cancel = true;

                }
            }
        }
        private void txtVehicleMedia_Leave(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Do You Want to Save these Changes !!", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{
            //    base.DoToolbarActions(this.Controls, "Save");
            //    base.ClearForm(pnlHead.Controls);
            //    base.ClearForm(pnlPersonnel.Controls);
            //    this.FunctionConfig.CurrentOption = Function.None;
            //}
        }
        private void txtFinanceNo_Validating(object sender, CancelEventArgs e)
        {

            #region Fn_desc
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("fn_type", txtFinanaceType.Text);
            rslt = cmnDM.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Fin_desc", param);

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtdescrip.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }

            #endregion

            #region Modify or Del
            if (FunctionConfig.CurrentOption != Function.Add && txtFinanceNo.Text != string.Empty)
            {
                Result rslt1;
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("PR_P_NO", txtPersonnelNo.Text);
                param1.Add("FN_FINANCE_NO", txtFinanceNo.Text);
                rslt1 = cmnDM.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Fin_Insurance", param1);

                if (rslt1.isSuccessful)
                {
                    if (rslt1.dstResult.Tables.Count > 0 && rslt1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                        dtPisssuedate.Value = Convert.ToDateTime(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    }
                }


                if (FunctionConfig.CurrentOption == Function.Modify || FunctionConfig.CurrentOption == Function.Delete)
                {
                    Result rslt2;
                    Dictionary<string, object> param2 = new Dictionary<string, object>();
                    param2.Add("PR_P_NO", txtPersonnelNo.Text);
                    param2.Add("FN_FINANCE_NO", txtFinanceNo.Text);
                    rslt2 = cmnDM.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Fin_Expirydate", param2);

                    if (rslt2.isSuccessful)
                    {
                        if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)

                            dtpexpirydt.Value = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        }
                    }


                    if (dtPisssuedate.Value != null && dtpexpirydt.Value != null)
                    {
                        DateTime dtIssDate = (DateTime)(this.dtPisssuedate.Value);
                        DateTime dtExpDate = (DateTime)(this.dtpexpirydt.Value);

                        TimeSpan ts = dtExpDate.Subtract(dtIssDate);
                        int days1 = ts.Days;
                        if (days1 == 0)
                        {
                            if (FunctionConfig.CurrentOption == Function.Delete)
                            {
                                Execute_query();
                                //if (MessageBox.Show("Do You Want To delete this Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    base.DoToolbarActions(this.Controls, "Delete");

                                }
                            }
                            else
                            {
                                Execute_query();
                            }
                        }

                        else
                        {

                            MessageBox.Show("YOU CANT ACCESS THIS RECORD BECAUSE PAYROLL HAS BEEN GENERATED");
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                       // e.Cancel = true;
                        txtFinanceNo.Text = "";
                        txtFinanceNo.Select();
                        txtFinanceNo.Focus();
                        return;
                    }


            }
            #region View 
            if (FunctionConfig.CurrentOption == Function.View)
            {
               bool flag =  Execute_query();

               if (flag == true)
               {
                   if (MessageBox.Show("Do You Want To View More Record", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                   {
                       base.ClearForm(pnlPersonnel.Controls);
                       txtPersonnelNo.Select();
                       txtPersonnelNo.Focus();
                       dtpexpirydt.Value = null;
                       dtPisssuedate.Value = null;

                   }

                   else
                   {
                       base.ClearForm(pnlPersonnel.Controls);
                       txtOption.Select();
                       txtOption.Focus();
                       dtpexpirydt.Value = null;
                       dtPisssuedate.Value = null;
                           
                   }
               }
               else
               {

                   base.ClearForm(pnlPersonnel.Controls);
                   dtpexpirydt.Value = null;
                   dtPisssuedate.Value = null;
                   txtOption.Text = "";
                   txtOption.Select();
                   txtOption.Focus();


                   //  e.Cancel = true;
               }
            }
            #endregion

        }

            #endregion
    }
        private void txtVehicleMedia_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Keys st = (Keys.Shift | Keys.Tab);
            if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {


                }
            }


            else
            {
                if (e.KeyData == Keys.Tab)
                {

                    if (FunctionConfig.CurrentOption == Function.Add)
                    {

                        if (MessageBox.Show(" Do you want to save this record ?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            this.DoToolbarActions(this.Controls, "Save");
                        }
                        else
                        {
                            this.DoToolbarActions(this.Controls, "Cancel");

                        }
                    }

                }
            }
        }
       #endregion 
    }
}