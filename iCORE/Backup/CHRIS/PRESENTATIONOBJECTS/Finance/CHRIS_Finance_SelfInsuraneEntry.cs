using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_SelfInsuraneEntry : ChrisSimpleForm
    {
        public CHRIS_Finance_SelfInsuraneEntry()
        {
            InitializeComponent();
        }


        public CHRIS_Finance_SelfInsuraneEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }


        protected override void OnLoad(EventArgs e)
        {
            this.txtOption.Select();
            this.txtOption.Focus();

            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtAdd.Enabled = false;
            tbtDelete.Enabled = false;
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtAdd.Visible = false;
            tbtDelete.Visible = false;
           
            this.txtUserName.Text = "User Name: "+ this.UserName;

            this.txtOption.Select();
            this.txtOption.Focus();

            txtComp.CharacterCasing = CharacterCasing.Normal;

            //this.dtLastPay.Value = null;
            //this.dtPay.Value = null;

            this.LBPersonnel.SkipValidationOnLeave = false;
           
        
       
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.AutoValidate = AutoValidate.Disable;
                this.dtLastPay.IsRequired = false;
                this.dtPay.IsRequired = false;
                this.txtPersonnelNo.Text = "";
                this.txtPersonnelNo.SkipValidation = true;
                this.txtPersonnelNo.IsRequired = false;

                this.FunctionConfig.CurrentOption = Function.None;


               // this.dtLastPay.Value = null;
                //this.dtPay.Value = null;
                
                base.DoToolbarActions(ctrlsCollection, actionType);
                
                this.dtLastPay.IsRequired = true;
                this.dtPay.IsRequired = true;
                this.txtPersonnelNo.SkipValidation = false;
                this.txtPersonnelNo.IsRequired = true;

                this.txtInsuranceAnuualPremuim.Enabled = false;
                this.txtAnuualReduction.Enabled = false;

                this.AutoValidate = AutoValidate.EnablePreventFocusChange;

                //this.txtPersonnelNo.Select();
                //this.txtPersonnelNo.Focus();

                return;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        protected override bool Add()
        {


             base.Add();
             this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
             return false;
        }

        protected override bool Edit()
        {
            base.Edit();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;

        }

        protected override bool Delete()
        {
             base.Delete();
             this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
             return false;
        }

        protected override bool View()
        {
            base.View();

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
            return false;
        }

        private DataTable IsValidNo()
        {

            DataTable dt = null;

            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();

            colsNVals.Add("FNI_P_NO", txtPersonnelNo.Text);



            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "check_valid_pr_p_no", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }

        private void slTextBox1_Validated(object sender, EventArgs e)
        {
            if (this.txtInsuranceType.Text.Length > 0)
            {

                if (txtInsuranceType.Text.ToUpper() != "M" && txtInsuranceType.Text.ToUpper() != "L")
                {
                    MessageBox.Show("Invalid finance type, Valid choices are (M/L)...");
                    txtInsuranceType.Text = "";
                    txtInsuranceType.Focus();
                    return;
                }


            }
            else
            {
                MessageBox.Show("Invalid finance type, Valid choices are (M/L)...");
                txtInsuranceType.Focus();
                return;
            }
            if (this.txtInsuranceType.Text.ToUpper() == "M")
            {
                txtAnuualReduction.CustomEnabled = true;
                txtAnuualReduction.Enabled = true;
                txtInsuranceAnuualPremuim.CustomEnabled = false;
                txtInsuranceAnuualPremuim.Text = "";
                txtInsuranceAnuualPremuim.Enabled = false;
            }
            else
            {
                txtAnuualReduction.CustomEnabled = false;
                txtInsuranceAnuualPremuim.CustomEnabled = true;
                txtAnuualReduction.Enabled = false;
                txtAnuualReduction.Text = "";
                txtInsuranceAnuualPremuim.Enabled = true;
            }
        }

        private void slTextBox2_Validated(object sender, EventArgs e)
        {
            txtInsuranceAnuualPremuim.Text = "0.00";
            if (txtAnuualReduction.Text == string.Empty)
            {
                MessageBox.Show("Please Enter The Amount ......!");
                txtAnuualReduction.Focus();
                return;
            }
        }

        private void dtLastPay_Validated(object sender, EventArgs e)
        {
            DateTime dt1;
            DateTime dt2;
            dt1=Convert.ToDateTime(dtPay.Value);
            dt2=Convert.ToDateTime(dtLastPay.Value);
            if (dtLastPay.Value != null)
            {

                if (dtPay.Value != null)
                {

                    if (DateTime.Compare(dt2,dt1)<0)
                    {
                        MessageBox.Show("Last Payment Date Should be Greater Then The Payment Date");
                        dtLastPay.Focus();
                        return;
                    }
                }
            }
        }

    
        private void check_Payroll()
        {
            DataTable dt = null;

            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();

            colsNVals.Add("FNI_P_NO", txtPersonnelNo.Text);



            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "check_Payroll_Date", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                    int month = int.Parse(dt.Rows[0][0].ToString());
                    this.txtVehicleChasisNo.Text = month.ToString("00");
                    this.txtPaDate.Text = dt.Rows[0][1].ToString() == null ? new DateTime(1900, 1, 1).ToShortDateString() : dt.Rows[0][1].ToString();

                    double insPrem = double.Parse(dt.Rows[0][2] == null ? "0" : dt.Rows[0][2].ToString());
                    this.txtPA_Premium.Text = insPrem.ToString("0");
                    
                    //colsNVals.Add("MONTH", this.txtVehicleChasisNo.Text);
                    //rsltCode = cmnDM.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "check_Ins_Premium", colsNVals);
                    //if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    //{
                        
                    //}
                }
                else
                {
                    if (FunctionConfig.CurrentOption == Function.Modify)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                           , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            base.DoToolbarActions(this.Controls, "Save");
                            this.Cancel();
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            //call save
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {
                            this.Cancel();
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            return;
                        }
                    }
                }

            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txtOptionMonth.Text = dt.Rows[0].ItemArray[0].ToString();
                    if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit && this.FunctionConfig.CurrentOption== Function.Modify )
                    {
                        txtPA_Premium.Select();
                        txtPA_Premium.Focus();
                        return;

                    }

                    else if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to view more records [Y/N]", "Note"
                                      , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            //this.Cancel();
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            txtPersonnelNo.Focus();

                            //call view
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {
                            txtOption.Select();
                            txtOption.Focus();
                            
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            this.Cancel();
 

                            return;
                        }

                    }
                    else if (this.FunctionConfig.CurrentOption == Function.Delete)
                    {

                        DialogResult dRes = MessageBox.Show("Do you want to delete this record [Y/N]", "Note"
                                       , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            //base.DoToolbarActions(this.Controls, "Delete");
                            CustomDelete();
                           
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            this.Cancel();
                            //call save
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {
                          
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            this.Cancel();
                           

                            return;
                        }

                    }
                }
            }


            else
            {
                MessageBox.Show("Payroll not generated for this employee...");
                if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit && this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                  , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        base.DoToolbarActions(this.Controls, "Save");
                        this.Cancel();
                        this.Reset();

                        //call save
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        this.Reset();

                        return;
                    }


                }
                else if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View)
                {
                    DialogResult dRes = MessageBox.Show("Do you want to view more records [Y/N]", "Note"
                                  , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        this.Reset();
                        txtPersonnelNo.Focus();

                        //call view
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        this.Cancel();
                        this.Reset();
                        txtOption.Focus();

                        return;
                    }

                }
                else if (this.FunctionConfig.CurrentOption == Function.Delete)
                {

                    DialogResult dRes = MessageBox.Show("Do you want to delete this record [Y/N]", "Note"
                                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        //this.Reset();

                        //base.DoToolbarActions(this.Controls, "Delete");
                        CustomDelete();
                        this.Cancel();

                        //call save
                        return;

                    }
                    else if (dRes == DialogResult.No)
                    {
                        this.Reset();
                        this.Cancel();
                       

                        return;
                    }

                }

            }
        }

         private void Reset()
        {
            this.txtPersonnelNo.Text = "";
            this.txtFirstName.Text = "";
            this.txtComp.Text = "";
            this.txtPolicyNumber.Text = "";
            this.txtInsuranceType.Text = "";
            this.txtInsuranceAmount.Text = "";
            this.txtAnuualReduction.Text = "";
            //this.dtLastPay.Value = null;

            //this.dtPay.Value = null;
            this.txtAssignedBank.Text = "";
            this.txtVehicleEngineNo.Text = "";
          
          

          
        }

        private void check_for_data()
        {

            

            DataTable dt = null;

            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();

            colsNVals.Add("FNI_P_NO", txtPersonnelNo.Text);



            rsltCode = cmnDM.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "check_Payroll_Data", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        //base.DoToolbarActions(this.Controls, "List");


                    }
                    else if (this.FunctionConfig.CurrentOption == Function.Add)
                    {
                        MessageBox.Show("Record already present...");
                        this.Cancel();
                        return;


                    }


                }
                else
                {
                    if (this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        MessageBox.Show("Record not found...");
                        return;



                    }

                }
            }
        }

     

        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {
            if ((txtPersonnelNo.Text != string.Empty))
            {
                DataTable dt = IsValidNo();
                if (dt != null)
                {

                    if (dt.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        MessageBox.Show("Record does not exist/Employee terminated/Out of Country, Press [List]...");
                    }
                }
                check_for_data();
                if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Delete)
                {
                    check_Payroll();
                    
                }

            }
            else
            {
                //e.Cancel = true;
                return;
            }
        }

        private void txtInsuranceAnuualPremuim_Validated(object sender, EventArgs e)
        {
            if (txtInsuranceAnuualPremuim.Text == string.Empty)
            {
                MessageBox.Show("Please Enter The Amount ......!");
                txtInsuranceAnuualPremuim.Focus();
                return;
            }
        }

        private void lbtnCode_MouseDown(object sender, MouseEventArgs e)
        {

            LookupButton btn = (LookupButton)sender;
            ///Code by FAIsal Iqbal
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                btn.Enabled = false;
                btn.Enabled = true;

            }
        }

     

        private void txtAnuualReduction_Validating(object sender, CancelEventArgs e)
        {
            txtInsuranceAnuualPremuim.Text = "0.00";
            if (txtAnuualReduction.Text == string.Empty)
            {
                MessageBox.Show("Please Enter The Amount ......!");
                txtAnuualReduction.Focus();
                e.Cancel = true;
               
            }
        }

      

        private void CHRIS_Finance_SelfInsuraneEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "PersonalLov" || actionType == "PersonalLovExists")
            {
                if (this.FunctionConfig.CurrentOption != Function.Add)
                {
                    if (selectedRow["ID"] == null || selectedRow["ID"].ToString() == "")
                    {
                        MessageBox.Show("Record not found.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.txtPersonnelNo.Select();
                        this.txtPersonnelNo.Focus();
                    }
                    else
                    {
                        DeleteID.Text = selectedRow["ID"].ToString();
                    }
                }
            }
        }

        private void txtAssignedBank_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    
                    if (this.txtAssignedBank.Text.Length > 0)
                    {
                        if (txtAssignedBank.Text != "Y" && txtAssignedBank.Text != "y" && txtAssignedBank.Text != "n" && txtAssignedBank.Text != "N")
                        {
                            MessageBox.Show("Type Y/N ...");
                            txtAssignedBank.Text = "";
                            txtAssignedBank.Focus();
                            e.IsInputKey = true;
                            return;
                        }
                        if (this.FunctionConfig.CurrentOption != Function.Add)
                        {
                            check_Payroll();
                            e.IsInputKey = true;
                            return;
                        }
                        else
                        {
                            if (this.FunctionConfig.CurrentOption == Function.Add)
                            {
                                DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                            , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dRes == DialogResult.Yes)
                                {
                                    this.AutoValidate = AutoValidate.Disable;
                                    txtOption.Focus();
                                    base.DoToolbarActions(this.Controls, "Save");
                                 
                                    this.DoToolbarActions(this.Controls, "Cancel");                         
                                    //base.ClearForm(pnlPersonnel.Controls);
                                    //txtAssignedBank.IsRequired = false;
                                   // txtOption.Focus();
                                    this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                                    e.IsInputKey = true;
                                   
                                    //call save
                                    return;

                                }
                                else if (dRes == DialogResult.No)
                                {
                                    this.Reset();
                                    base.ClearForm(pnlPersonnel.Controls);
                                    this.Cancel();
                                    return;
                                }
                               // this.Cancel();
                            }

                        }

                    }
                   


                }

            }
        }

        private void txtPA_Premium_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {

                    if (this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            base.DoToolbarActions(this.Controls, "Save");
                            this.Cancel();
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);
                            //call save
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {
                            this.Cancel();
                            this.Reset();
                            base.ClearForm(pnlPersonnel.Controls);

                            return;
                        }
                    }

                }
            }
        }



        private void CustomDelete()
        {
            int ID = 0;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SelfInsuranceCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SelfInsuranceCommand)this.pnlPersonnel.CurrentBusinessEntity;
            if (int.TryParse(DeleteID.Text, out ID))
            {
                //ID = ID;
                colsNVals.Clear();
                colsNVals.Add("ID", ID);

                rsltCode = cmnDM.Execute("CHRIS_SP_FN_INSURANCE_MANAGER", "Delete", colsNVals);

                if (rsltCode.isSuccessful)
                {


                    MessageBox.Show("Record deleted successfully");
                    return;
                }
            }


        }

        private void txtInsuranceAmount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    if (txtInsuranceType.Text == "M")
                    {
                        txtAnuualReduction.Focus();
                        e.IsInputKey = true;
                        return;

                    }

                    else
                    {
                        txtAnuualReduction.Text = "0";
                        txtInsuranceAnuualPremuim.Focus();
                        e.IsInputKey = true;
                        return;

                    }

                }

            }

        }




        //private void txtInsuranceAmount_Validated(object sender, EventArgs e)
        //{
        //    if (txtInsuranceType.Text == "M")
        //    {
        //        txtAnuualReduction.Focus();
        //        return;

        //    }

        //    else
        //    {
        //        txtAnuualReduction.Text = "0";
        //        txtInsuranceAnuualPremuim.Focus();
        //        return;

        //    }
        //}

      


       
    }
}