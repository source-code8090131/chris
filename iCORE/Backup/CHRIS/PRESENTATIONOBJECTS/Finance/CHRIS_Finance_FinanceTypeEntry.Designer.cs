namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinanceTypeEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinanceTypeEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.f_Type = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtEligibilty = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtConfirm = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFN_LESS_AMT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFN_EX_GREAT = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtFN_EX_LESS = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txtfnCRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.lbtnPersonnal = new CrplControlLibrary.LookupButton(this.components);
            this.txtFN_SCHEDULE = new CrplControlLibrary.SLTextBox(this.components);
            this.txtMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDescription = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlTblSubType = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dtGridSubType = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.txtFinTypeMaster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFN_SUBTYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFN_SUBDESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpBoxFinanceSubType = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlNavigation = new System.Windows.Forms.Panel();
            this.lblNavigation = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.pnlTblSubType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridSubType)).BeginInit();
            this.grpBoxFinanceSubType.SuspendLayout();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(436, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.None;
            this.pnlBottom.Location = new System.Drawing.Point(34, 0);
            this.pnlBottom.Size = new System.Drawing.Size(472, 22);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlNavigation);
            this.panel1.Location = new System.Drawing.Point(0, 437);
            this.panel1.Size = new System.Drawing.Size(660, 91);
            this.panel1.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.panel1.Controls.SetChildIndex(this.pnlBottom, 0);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.f_Type);
            this.pnlHead.Location = new System.Drawing.Point(4, 77);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(651, 76);
            this.pnlHead.TabIndex = 39;
            // 
            // f_Type
            // 
            this.f_Type.AllowSpace = true;
            this.f_Type.AssociatedLookUpName = "";
            this.f_Type.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.f_Type.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.f_Type.ContinuationTextBox = null;
            this.f_Type.CustomEnabled = true;
            this.f_Type.DataFieldMapping = "";
            this.f_Type.Enabled = false;
            this.f_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.f_Type.GetRecordsOnUpDownKeys = false;
            this.f_Type.IsDate = false;
            this.f_Type.Location = new System.Drawing.Point(172, 20);
            this.f_Type.MaxLength = 20;
            this.f_Type.Name = "f_Type";
            this.f_Type.NumberFormat = "###,###,##0.00";
            this.f_Type.Postfix = "";
            this.f_Type.Prefix = "";
            this.f_Type.Size = new System.Drawing.Size(66, 20);
            this.f_Type.SkipValidation = false;
            this.f_Type.TabIndex = 49;
            this.f_Type.TabStop = false;
            this.f_Type.TextType = CrplControlLibrary.TextType.String;
            this.f_Type.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(52, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mark Up % :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(33, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Payment :";
            // 
            // pnlDetail
            // 
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.label4);
            this.pnlDetail.Controls.Add(this.txtEligibilty);
            this.pnlDetail.Controls.Add(this.label7);
            this.pnlDetail.Controls.Add(this.label8);
            this.pnlDetail.Controls.Add(this.txtConfirm);
            this.pnlDetail.Controls.Add(this.groupBox1);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.label12);
            this.pnlDetail.Controls.Add(this.label11);
            this.pnlDetail.Controls.Add(this.txtFN_LESS_AMT);
            this.pnlDetail.Controls.Add(this.txtFN_EX_GREAT);
            this.pnlDetail.Controls.Add(this.label10);
            this.pnlDetail.Controls.Add(this.txtFN_EX_LESS);
            this.pnlDetail.Controls.Add(this.label9);
            this.pnlDetail.Controls.Add(this.txtfnCRatio);
            this.pnlDetail.Controls.Add(this.label6);
            this.pnlDetail.Controls.Add(this.lbtnPersonnal);
            this.pnlDetail.Controls.Add(this.txtFN_SCHEDULE);
            this.pnlDetail.Controls.Add(this.txtMarkup);
            this.pnlDetail.Controls.Add(this.txtDescription);
            this.pnlDetail.Controls.Add(this.txtFinType);
            this.pnlDetail.Controls.Add(this.label5);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label19);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand";
            this.pnlDetail.Location = new System.Drawing.Point(9, 174);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(397, 257);
            this.pnlDetail.SPName = "CHRIS_SP_FIN_TYPE_MANAGER";
            this.pnlDetail.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Eligibility :";
            // 
            // txtEligibilty
            // 
            this.txtEligibilty.AllowSpace = true;
            this.txtEligibilty.AssociatedLookUpName = "";
            this.txtEligibilty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEligibilty.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEligibilty.ContinuationTextBox = null;
            this.txtEligibilty.CustomEnabled = true;
            this.txtEligibilty.DataFieldMapping = "FN_ELIG";
            this.txtEligibilty.Enabled = false;
            this.txtEligibilty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEligibilty.GetRecordsOnUpDownKeys = false;
            this.txtEligibilty.IsDate = false;
            this.txtEligibilty.Location = new System.Drawing.Point(99, 108);
            this.txtEligibilty.MaxLength = 3;
            this.txtEligibilty.Name = "txtEligibilty";
            this.txtEligibilty.NumberFormat = "###,###,##0.00";
            this.txtEligibilty.Postfix = "";
            this.txtEligibilty.Prefix = "";
            this.txtEligibilty.Size = new System.Drawing.Size(27, 20);
            this.txtEligibilty.SkipValidation = false;
            this.txtEligibilty.TabIndex = 6;
            this.txtEligibilty.TextType = CrplControlLibrary.TextType.Integer;
            this.txtEligibilty.Validated += new System.EventHandler(this.txtEligibilty_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(243, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "On Confirmation :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(132, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "(In Months)   OR";
            // 
            // txtConfirm
            // 
            this.txtConfirm.AllowSpace = true;
            this.txtConfirm.AssociatedLookUpName = "";
            this.txtConfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfirm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtConfirm.ContinuationTextBox = null;
            this.txtConfirm.CustomEnabled = false;
            this.txtConfirm.DataFieldMapping = "FN_CONFIRM";
            this.txtConfirm.Enabled = false;
            this.txtConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirm.GetRecordsOnUpDownKeys = false;
            this.txtConfirm.IsDate = false;
            this.txtConfirm.Location = new System.Drawing.Point(349, 108);
            this.txtConfirm.MaxLength = 1;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.NumberFormat = "###,###,##0.00";
            this.txtConfirm.Postfix = "";
            this.txtConfirm.Prefix = "";
            this.txtConfirm.Size = new System.Drawing.Size(27, 20);
            this.txtConfirm.SkipValidation = false;
            this.txtConfirm.TabIndex = 38;
            this.txtConfirm.TabStop = false;
            this.txtConfirm.TextType = CrplControlLibrary.TextType.String;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(20, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(372, 42);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(269, 199);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "And";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(220, 217);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "On Amount Greater. :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(207, 175);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "On Amount Less Than :";
            // 
            // txtFN_LESS_AMT
            // 
            this.txtFN_LESS_AMT.AllowSpace = true;
            this.txtFN_LESS_AMT.AssociatedLookUpName = "";
            this.txtFN_LESS_AMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_LESS_AMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_LESS_AMT.ContinuationTextBox = null;
            this.txtFN_LESS_AMT.CustomEnabled = true;
            this.txtFN_LESS_AMT.DataFieldMapping = "FN_LESS_AMT";
            this.txtFN_LESS_AMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_LESS_AMT.GetRecordsOnUpDownKeys = false;
            this.txtFN_LESS_AMT.IsDate = false;
            this.txtFN_LESS_AMT.Location = new System.Drawing.Point(348, 171);
            this.txtFN_LESS_AMT.MaxLength = 5;
            this.txtFN_LESS_AMT.Name = "txtFN_LESS_AMT";
            this.txtFN_LESS_AMT.NumberFormat = "###,###,##0.00";
            this.txtFN_LESS_AMT.Postfix = "";
            this.txtFN_LESS_AMT.Prefix = "";
            this.txtFN_LESS_AMT.Size = new System.Drawing.Size(44, 20);
            this.txtFN_LESS_AMT.SkipValidation = false;
            this.txtFN_LESS_AMT.TabIndex = 9;
            this.txtFN_LESS_AMT.TextType = CrplControlLibrary.TextType.Integer;
            this.txtFN_LESS_AMT.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtFN_LESS_AMT_PreviewKeyDown);
            this.txtFN_LESS_AMT.Validating += new System.ComponentModel.CancelEventHandler(this.txtFN_LESS_AMT_Validating);
            // 
            // txtFN_EX_GREAT
            // 
            this.txtFN_EX_GREAT.AllowSpace = true;
            this.txtFN_EX_GREAT.AssociatedLookUpName = "";
            this.txtFN_EX_GREAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_EX_GREAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_EX_GREAT.ContinuationTextBox = null;
            this.txtFN_EX_GREAT.CustomEnabled = true;
            this.txtFN_EX_GREAT.DataFieldMapping = "FN_EX_GREAT";
            this.txtFN_EX_GREAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_EX_GREAT.GetRecordsOnUpDownKeys = false;
            this.txtFN_EX_GREAT.IsDate = false;
            this.txtFN_EX_GREAT.Location = new System.Drawing.Point(99, 196);
            this.txtFN_EX_GREAT.MaxLength = 2;
            this.txtFN_EX_GREAT.Name = "txtFN_EX_GREAT";
            this.txtFN_EX_GREAT.NumberFormat = "###,###,##0.00";
            this.txtFN_EX_GREAT.Postfix = "";
            this.txtFN_EX_GREAT.Prefix = "";
            this.txtFN_EX_GREAT.Size = new System.Drawing.Size(44, 20);
            this.txtFN_EX_GREAT.SkipValidation = false;
            this.txtFN_EX_GREAT.TabIndex = 10;
            this.txtFN_EX_GREAT.TextType = CrplControlLibrary.TextType.Integer;
            this.txtFN_EX_GREAT.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtFN_EX_GREAT_PreviewKeyDown);
            this.txtFN_EX_GREAT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFN_EX_GREAT_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "Excise Levy % :";
            // 
            // txtFN_EX_LESS
            // 
            this.txtFN_EX_LESS.AllowSpace = true;
            this.txtFN_EX_LESS.AssociatedLookUpName = "";
            this.txtFN_EX_LESS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_EX_LESS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_EX_LESS.ContinuationTextBox = null;
            this.txtFN_EX_LESS.CustomEnabled = true;
            this.txtFN_EX_LESS.DataFieldMapping = "FN_EX_LESS";
            this.txtFN_EX_LESS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_EX_LESS.GetRecordsOnUpDownKeys = false;
            this.txtFN_EX_LESS.IsDate = false;
            this.txtFN_EX_LESS.Location = new System.Drawing.Point(99, 171);
            this.txtFN_EX_LESS.MaxLength = 2;
            this.txtFN_EX_LESS.Name = "txtFN_EX_LESS";
            this.txtFN_EX_LESS.NumberFormat = "###,###,##0.00";
            this.txtFN_EX_LESS.Postfix = "";
            this.txtFN_EX_LESS.Prefix = "";
            this.txtFN_EX_LESS.Size = new System.Drawing.Size(44, 20);
            this.txtFN_EX_LESS.SkipValidation = false;
            this.txtFN_EX_LESS.TabIndex = 8;
            this.txtFN_EX_LESS.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Excise Levy % :";
            // 
            // txtfnCRatio
            // 
            this.txtfnCRatio.AllowSpace = true;
            this.txtfnCRatio.AssociatedLookUpName = "";
            this.txtfnCRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfnCRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtfnCRatio.ContinuationTextBox = null;
            this.txtfnCRatio.CustomEnabled = true;
            this.txtfnCRatio.DataFieldMapping = "FN_C_RATIO";
            this.txtfnCRatio.Enabled = false;
            this.txtfnCRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfnCRatio.GetRecordsOnUpDownKeys = false;
            this.txtfnCRatio.IsDate = false;
            this.txtfnCRatio.IsRequired = true;
            this.txtfnCRatio.Location = new System.Drawing.Point(348, 72);
            this.txtfnCRatio.MaxLength = 6;
            this.txtfnCRatio.Name = "txtfnCRatio";
            this.txtfnCRatio.NumberFormat = "##0.00";
            this.txtfnCRatio.Postfix = "";
            this.txtfnCRatio.Prefix = "";
            this.txtfnCRatio.Size = new System.Drawing.Size(27, 20);
            this.txtfnCRatio.SkipValidation = false;
            this.txtfnCRatio.TabIndex = 5;
            this.txtfnCRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfnCRatio.TextType = CrplControlLibrary.TextType.Amount;
            this.txtfnCRatio.Validating += new System.ComponentModel.CancelEventHandler(this.txtfnCRatio_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(252, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Credit Ratio % :";
            // 
            // lbtnPersonnal
            // 
            this.lbtnPersonnal.ActionLOVExists = "FinanceTypeLov_EXISTS";
            this.lbtnPersonnal.ActionType = "FinanceTypeLov";
            this.lbtnPersonnal.ConditionalFields = "";
            this.lbtnPersonnal.CustomEnabled = true;
            this.lbtnPersonnal.DataFieldMapping = "";
            this.lbtnPersonnal.DependentLovControls = "";
            this.lbtnPersonnal.HiddenColumns = "";
            this.lbtnPersonnal.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnal.Image")));
            this.lbtnPersonnal.LoadDependentEntities = true;
            this.lbtnPersonnal.Location = new System.Drawing.Point(161, 18);
            this.lbtnPersonnal.LookUpTitle = null;
            this.lbtnPersonnal.Name = "lbtnPersonnal";
            this.lbtnPersonnal.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnal.SkipValidationOnLeave = true;
            this.lbtnPersonnal.SPName = "CHRIS_SP_FIN_TYPE_MANAGER";
            this.lbtnPersonnal.TabIndex = 1;
            this.lbtnPersonnal.TabStop = false;
            this.lbtnPersonnal.Tag = "";
            this.lbtnPersonnal.UseVisualStyleBackColor = true;
            this.lbtnPersonnal.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnCode_MouseDown);
            // 
            // txtFN_SCHEDULE
            // 
            this.txtFN_SCHEDULE.AllowSpace = true;
            this.txtFN_SCHEDULE.AssociatedLookUpName = "";
            this.txtFN_SCHEDULE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFN_SCHEDULE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFN_SCHEDULE.ContinuationTextBox = null;
            this.txtFN_SCHEDULE.CustomEnabled = true;
            this.txtFN_SCHEDULE.DataFieldMapping = "FN_SCHEDULE";
            this.txtFN_SCHEDULE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFN_SCHEDULE.GetRecordsOnUpDownKeys = false;
            this.txtFN_SCHEDULE.IsDate = false;
            this.txtFN_SCHEDULE.IsRequired = true;
            this.txtFN_SCHEDULE.Location = new System.Drawing.Point(99, 147);
            this.txtFN_SCHEDULE.MaxLength = 3;
            this.txtFN_SCHEDULE.Name = "txtFN_SCHEDULE";
            this.txtFN_SCHEDULE.NumberFormat = "###,###,##0.00";
            this.txtFN_SCHEDULE.Postfix = "";
            this.txtFN_SCHEDULE.Prefix = "";
            this.txtFN_SCHEDULE.Size = new System.Drawing.Size(44, 20);
            this.txtFN_SCHEDULE.SkipValidation = false;
            this.txtFN_SCHEDULE.TabIndex = 7;
            this.txtFN_SCHEDULE.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtMarkup
            // 
            this.txtMarkup.AllowSpace = true;
            this.txtMarkup.AssociatedLookUpName = "";
            this.txtMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkup.ContinuationTextBox = null;
            this.txtMarkup.CustomEnabled = true;
            this.txtMarkup.DataFieldMapping = "FN_MARKUP";
            this.txtMarkup.Enabled = false;
            this.txtMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkup.GetRecordsOnUpDownKeys = false;
            this.txtMarkup.IsDate = false;
            this.txtMarkup.Location = new System.Drawing.Point(99, 72);
            this.txtMarkup.MaxLength = 3;
            this.txtMarkup.Name = "txtMarkup";
            this.txtMarkup.NumberFormat = "###,###,##0.00";
            this.txtMarkup.Postfix = "";
            this.txtMarkup.Prefix = "";
            this.txtMarkup.Size = new System.Drawing.Size(27, 20);
            this.txtMarkup.SkipValidation = false;
            this.txtMarkup.TabIndex = 4;
            this.txtMarkup.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txtDescription
            // 
            this.txtDescription.AllowSpace = true;
            this.txtDescription.AssociatedLookUpName = "";
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescription.ContinuationTextBox = null;
            this.txtDescription.CustomEnabled = true;
            this.txtDescription.DataFieldMapping = "FN_DESC";
            this.txtDescription.Enabled = false;
            this.txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.GetRecordsOnUpDownKeys = false;
            this.txtDescription.IsDate = false;
            this.txtDescription.IsRequired = true;
            this.txtDescription.Location = new System.Drawing.Point(99, 45);
            this.txtDescription.MaxLength = 20;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.NumberFormat = "###,###,##0.00";
            this.txtDescription.Postfix = "";
            this.txtDescription.Prefix = "";
            this.txtDescription.Size = new System.Drawing.Size(253, 20);
            this.txtDescription.SkipValidation = false;
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextType = CrplControlLibrary.TextType.String;
            this.txtDescription.Validating += new System.ComponentModel.CancelEventHandler(this.txtDescription_Validating);
            // 
            // txtFinType
            // 
            this.txtFinType.AllowSpace = true;
            this.txtFinType.AssociatedLookUpName = "lbtnPersonnal";
            this.txtFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinType.ContinuationTextBox = null;
            this.txtFinType.CustomEnabled = true;
            this.txtFinType.DataFieldMapping = "FN_TYPE";
            this.txtFinType.Enabled = false;
            this.txtFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinType.GetRecordsOnUpDownKeys = false;
            this.txtFinType.IsDate = false;
            this.txtFinType.IsRequired = true;
            this.txtFinType.Location = new System.Drawing.Point(99, 19);
            this.txtFinType.MaxLength = 6;
            this.txtFinType.Name = "txtFinType";
            this.txtFinType.NumberFormat = "###,###,##0.00";
            this.txtFinType.Postfix = "";
            this.txtFinType.Prefix = "";
            this.txtFinType.Size = new System.Drawing.Size(52, 20);
            this.txtFinType.SkipValidation = false;
            this.txtFinType.TabIndex = 1;
            this.txtFinType.TextType = CrplControlLibrary.TextType.String;
            this.txtFinType.Validating += new System.ComponentModel.CancelEventHandler(this.txtFinType_Validating);
            // 
            // pnlTblSubType
            // 
            this.pnlTblSubType.ConcurrentPanels = null;
            this.pnlTblSubType.Controls.Add(this.dtGridSubType);
            this.pnlTblSubType.Cursor = System.Windows.Forms.Cursors.Default;
            this.pnlTblSubType.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblSubType.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Cascading;
            this.pnlTblSubType.DependentPanels = null;
            this.pnlTblSubType.DisableDependentLoad = false;
            this.pnlTblSubType.EnableDelete = true;
            this.pnlTblSubType.EnableInsert = true;
            this.pnlTblSubType.EnableQuery = false;
            this.pnlTblSubType.EnableUpdate = true;
            this.pnlTblSubType.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINSUBTYPECommand";
            this.pnlTblSubType.Location = new System.Drawing.Point(3, 13);
            this.pnlTblSubType.MasterPanel = this.pnlDetail;
            this.pnlTblSubType.Name = "pnlTblSubType";
            this.pnlTblSubType.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblSubType.Size = new System.Drawing.Size(245, 132);
            this.pnlTblSubType.SPName = "CHRIS_SP_FIN_SUBTYPE_MANAGER";
            this.pnlTblSubType.TabIndex = 41;
            // 
            // dtGridSubType
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridSubType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtGridSubType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridSubType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtFinTypeMaster,
            this.txtFN_SUBTYPE,
            this.txtFN_SUBDESC,
            this.ID});
            this.dtGridSubType.ColumnToHide = null;
            this.dtGridSubType.ColumnWidth = null;
            this.dtGridSubType.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtGridSubType.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtGridSubType.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtGridSubType.DisplayColumnWrapper = null;
            this.dtGridSubType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGridSubType.GridDefaultRow = 0;
            this.dtGridSubType.Location = new System.Drawing.Point(0, 0);
            this.dtGridSubType.Name = "dtGridSubType";
            this.dtGridSubType.ReadOnlyColumns = null;
            this.dtGridSubType.RequiredColumns = "FN_SUBTYPE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridSubType.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtGridSubType.Size = new System.Drawing.Size(245, 132);
            this.dtGridSubType.SkippingColumns = null;
            this.dtGridSubType.TabIndex = 0;
            this.dtGridSubType.TabStop = false;
            this.dtGridSubType.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dtGridSubType_CellBeginEdit);
            this.dtGridSubType.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dtGridSubType_CellValidating);
            this.dtGridSubType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtGridSubType_KeyDown);
            this.dtGridSubType.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtGridSubType_CellEnter);
            // 
            // txtFinTypeMaster
            // 
            this.txtFinTypeMaster.HeaderText = "txtFinTypeMaster";
            this.txtFinTypeMaster.Name = "txtFinTypeMaster";
            this.txtFinTypeMaster.Visible = false;
            // 
            // txtFN_SUBTYPE
            // 
            this.txtFN_SUBTYPE.DataPropertyName = "FN_SUBTYPE";
            this.txtFN_SUBTYPE.HeaderText = "";
            this.txtFN_SUBTYPE.MaxInputLength = 2;
            this.txtFN_SUBTYPE.Name = "txtFN_SUBTYPE";
            // 
            // txtFN_SUBDESC
            // 
            this.txtFN_SUBDESC.DataPropertyName = "FN_SUBDESC";
            this.txtFN_SUBDESC.HeaderText = "";
            this.txtFN_SUBDESC.MaxInputLength = 20;
            this.txtFN_SUBDESC.Name = "txtFN_SUBDESC";
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // grpBoxFinanceSubType
            // 
            this.grpBoxFinanceSubType.Controls.Add(this.pnlTblSubType);
            this.grpBoxFinanceSubType.Controls.Add(this.label20);
            this.grpBoxFinanceSubType.Location = new System.Drawing.Point(406, 193);
            this.grpBoxFinanceSubType.Name = "grpBoxFinanceSubType";
            this.grpBoxFinanceSubType.Size = new System.Drawing.Size(246, 158);
            this.grpBoxFinanceSubType.TabIndex = 42;
            this.grpBoxFinanceSubType.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(86, -1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "Finance Sub-Type";
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.Location = new System.Drawing.Point(423, 10);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(133, 13);
            this.txtUserName.TabIndex = 43;
            this.txtUserName.Text = "User  Name :   CHRISTOP";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(194, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 18);
            this.label3.TabIndex = 30;
            this.label3.Text = "TYPE ENTRY";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(192, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 20);
            this.label14.TabIndex = 29;
            this.label14.Text = "FINANCE SYSTEM";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = false;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(567, 121);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 28;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = false;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Enabled = false;
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(567, 95);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 27;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(524, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 15);
            this.label15.TabIndex = 26;
            this.label15.Text = "Date:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(516, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 25;
            this.label16.Text = "Option:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = false;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Enabled = false;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(88, 121);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 24;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = false;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Enabled = false;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(88, 95);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 23;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(12, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 20);
            this.label17.TabIndex = 22;
            this.label17.Text = "Location:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(12, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 21;
            this.label18.Text = "User:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.lblNavigation);
            this.pnlNavigation.Controls.Add(this.label21);
            this.pnlNavigation.Location = new System.Drawing.Point(34, 25);
            this.pnlNavigation.Name = "pnlNavigation";
            this.pnlNavigation.Size = new System.Drawing.Size(390, 19);
            this.pnlNavigation.TabIndex = 5;
            // 
            // lblNavigation
            // 
            this.lblNavigation.AutoSize = true;
            this.lblNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNavigation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNavigation.Location = new System.Drawing.Point(113, 0);
            this.lblNavigation.Name = "lblNavigation";
            this.lblNavigation.Size = new System.Drawing.Size(239, 13);
            this.lblNavigation.TabIndex = 1;
            this.lblNavigation.Text = "[Ctrl+PgDn]=Navigate to Sub-Type Block";
            this.lblNavigation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Left;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Navigation Option:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CHRIS_Finance_FinanceTypeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanEnableDisableControls = true;
            this.ClientSize = new System.Drawing.Size(660, 528);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.txtCurrOption);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.grpBoxFinanceSubType);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlDetail";
            this.CurrrentOptionTextBox = this.txtCurrOption;
            this.Name = "CHRIS_Finance_FinanceTypeEntry";
            this.ShowBottomBar = true;
            this.ShowF10Option = true;
            this.ShowF11Option = true;
            this.ShowF12Option = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF3Option = true;
            this.ShowF4Option = true;
            this.ShowF5Option = true;
            this.ShowF6Option = true;
            this.ShowF7Option = true;
            this.ShowF9Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.ShowTextOption = true;
            this.Text = "CHRIS_Finance_FinanceTypeEntry";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_FinanceTypeEntry_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.grpBoxFinanceSubType, 0);
            this.Controls.SetChildIndex(this.label18, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.txtUser, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.txtCurrOption, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.txtUserName, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.pnlTblSubType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtGridSubType)).EndInit();
            this.grpBoxFinanceSubType.ResumeLayout(false);
            this.grpBoxFinanceSubType.PerformLayout();
            this.pnlNavigation.ResumeLayout(false);
            this.pnlNavigation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtFinType;
        private CrplControlLibrary.SLTextBox txtDescription;
        private CrplControlLibrary.SLTextBox txtMarkup;
        private CrplControlLibrary.SLTextBox txtFN_SCHEDULE;
        private CrplControlLibrary.LookupButton lbtnPersonnal;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private CrplControlLibrary.SLTextBox txtfnCRatio;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtEligibilty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtConfirm;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtFN_EX_GREAT;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtFN_EX_LESS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtFN_LESS_AMT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblSubType;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dtGridSubType;
        private System.Windows.Forms.GroupBox grpBoxFinanceSubType;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pnlNavigation;
        private System.Windows.Forms.Label lblNavigation;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFinTypeMaster;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFN_SUBTYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFN_SUBDESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private CrplControlLibrary.SLTextBox f_Type;
    }
}