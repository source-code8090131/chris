namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinanceQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinanceQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCat = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLongPkg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnualPack = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.lbtnFinType = new CrplControlLibrary.LookupButton(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.txtMarkup = new CrplControlLibrary.SLTextBox(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.txtSchedule = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtType = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.pnlFNDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txtCreditRatioPer = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotalMonthInstall = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txtCanBeAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.txtRatioAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMinCap = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFactor = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtAvailable = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txtCreditRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.pnlFNMonthDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvFnMoths = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFinNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMonthlyDeduction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMarkup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLiquidate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.txtRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.pnlPersonnel.SuspendLayout();
            this.pnlFNDetail.SuspendLayout();
            this.pnlFNMonthDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFnMoths)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(605, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(641, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 532);
            this.panel1.Size = new System.Drawing.Size(641, 60);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label5);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label3);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label1);
            this.pnlHead.Controls.Add(this.label2);
            this.pnlHead.Location = new System.Drawing.Point(12, 52);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(617, 88);
            this.pnlHead.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(165, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(276, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "Finance Query";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(165, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(276, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "FINANCE SYSTEM";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(498, 37);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(452, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(79, 61);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(79, 35);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "User:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPersonnel
            // 
            this.pnlPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPersonnel.ConcurrentPanels = null;
            this.pnlPersonnel.Controls.Add(this.txtFirstName);
            this.pnlPersonnel.Controls.Add(this.label18);
            this.pnlPersonnel.Controls.Add(this.txtLevel);
            this.pnlPersonnel.Controls.Add(this.txtBranch);
            this.pnlPersonnel.Controls.Add(this.txtCat);
            this.pnlPersonnel.Controls.Add(this.txtLongPkg);
            this.pnlPersonnel.Controls.Add(this.txtAnnualPack);
            this.pnlPersonnel.Controls.Add(this.label8);
            this.pnlPersonnel.Controls.Add(this.lbtnPNo);
            this.pnlPersonnel.Controls.Add(this.txtPersNo);
            this.pnlPersonnel.Controls.Add(this.label7);
            this.pnlPersonnel.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPersonnel.DependentPanels = null;
            this.pnlPersonnel.DisableDependentLoad = false;
            this.pnlPersonnel.EnableDelete = true;
            this.pnlPersonnel.EnableInsert = true;
            this.pnlPersonnel.EnableQuery = false;
            this.pnlPersonnel.EnableUpdate = true;
            this.pnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlPersonnel.Location = new System.Drawing.Point(12, 146);
            this.pnlPersonnel.MasterPanel = null;
            this.pnlPersonnel.Name = "pnlPersonnel";
            this.pnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPersonnel.Size = new System.Drawing.Size(617, 104);
            this.pnlPersonnel.SPName = "CHRIS_SP_TERMINATION_MANAGER";
            this.pnlPersonnel.TabIndex = 1;
            this.pnlPersonnel.TabStop = true;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(93, 42);
            this.txtFirstName.MaxLength = 40;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(303, 20);
            this.txtFirstName.SkipValidation = true;
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.TabStop = false;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(494, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(104, 20);
            this.label18.TabIndex = 65;
            this.label18.Text = "Loan Pack.";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Enabled = false;
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(367, 16);
            this.txtLevel.MaxLength = 50;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(106, 20);
            this.txtLevel.SkipValidation = true;
            this.txtLevel.TabIndex = 63;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            this.txtLevel.Visible = false;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PR_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(255, 16);
            this.txtBranch.MaxLength = 10;
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(106, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 62;
            this.txtBranch.TabStop = false;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            this.txtBranch.Visible = false;
            // 
            // txtCat
            // 
            this.txtCat.AllowSpace = true;
            this.txtCat.AssociatedLookUpName = "";
            this.txtCat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCat.ContinuationTextBox = null;
            this.txtCat.CustomEnabled = true;
            this.txtCat.DataFieldMapping = "PR_CATEGORY";
            this.txtCat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCat.GetRecordsOnUpDownKeys = false;
            this.txtCat.IsDate = false;
            this.txtCat.Location = new System.Drawing.Point(367, -1);
            this.txtCat.MaxLength = 20;
            this.txtCat.Name = "txtCat";
            this.txtCat.NumberFormat = "###,###,##0.00";
            this.txtCat.Postfix = "";
            this.txtCat.Prefix = "";
            this.txtCat.ReadOnly = true;
            this.txtCat.Size = new System.Drawing.Size(106, 20);
            this.txtCat.SkipValidation = false;
            this.txtCat.TabIndex = 61;
            this.txtCat.TabStop = false;
            this.txtCat.TextType = CrplControlLibrary.TextType.String;
            this.txtCat.Visible = false;
            // 
            // txtLongPkg
            // 
            this.txtLongPkg.AllowSpace = true;
            this.txtLongPkg.AssociatedLookUpName = "";
            this.txtLongPkg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLongPkg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLongPkg.ContinuationTextBox = null;
            this.txtLongPkg.CustomEnabled = true;
            this.txtLongPkg.DataFieldMapping = "LOAN_PKG";
            this.txtLongPkg.Enabled = false;
            this.txtLongPkg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLongPkg.GetRecordsOnUpDownKeys = false;
            this.txtLongPkg.IsDate = false;
            this.txtLongPkg.Location = new System.Drawing.Point(492, 42);
            this.txtLongPkg.MaxLength = 1;
            this.txtLongPkg.Name = "txtLongPkg";
            this.txtLongPkg.NumberFormat = "###,###,##0.00";
            this.txtLongPkg.Postfix = "";
            this.txtLongPkg.Prefix = "";
            this.txtLongPkg.ReadOnly = true;
            this.txtLongPkg.Size = new System.Drawing.Size(106, 20);
            this.txtLongPkg.SkipValidation = true;
            this.txtLongPkg.TabIndex = 60;
            this.txtLongPkg.TabStop = false;
            this.txtLongPkg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLongPkg.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAnnualPack
            // 
            this.txtAnnualPack.AllowSpace = true;
            this.txtAnnualPack.AssociatedLookUpName = "";
            this.txtAnnualPack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualPack.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualPack.ContinuationTextBox = null;
            this.txtAnnualPack.CustomEnabled = true;
            this.txtAnnualPack.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtAnnualPack.Enabled = false;
            this.txtAnnualPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualPack.GetRecordsOnUpDownKeys = false;
            this.txtAnnualPack.IsDate = false;
            this.txtAnnualPack.Location = new System.Drawing.Point(371, 42);
            this.txtAnnualPack.MaxLength = 1;
            this.txtAnnualPack.Name = "txtAnnualPack";
            this.txtAnnualPack.NumberFormat = "###,###,##0.00";
            this.txtAnnualPack.Postfix = "";
            this.txtAnnualPack.Prefix = "";
            this.txtAnnualPack.ReadOnly = true;
            this.txtAnnualPack.Size = new System.Drawing.Size(100, 20);
            this.txtAnnualPack.SkipValidation = true;
            this.txtAnnualPack.TabIndex = 59;
            this.txtAnnualPack.TabStop = false;
            this.txtAnnualPack.TextType = CrplControlLibrary.TextType.String;
            this.txtAnnualPack.Visible = false;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(7, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "Name:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "LOV_PR_P_NOExists";
            this.lbtnPNo.ActionType = "LOV_PR_P_NO";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.HiddenColumns = "PR_NEW_ANNUAL_PACK|PR_CATEGORY|PR_LEVEL|PR_BRANCH";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(164, 11);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_FINANCEQUERY_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            this.lbtnPNo.Enter += new System.EventHandler(this.lbtnPNo_Enter);
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.IsLookUpField = true;
            this.txtPersNo.IsRequired = true;
            this.txtPersNo.Location = new System.Drawing.Point(93, 12);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(65, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Enter += new System.EventHandler(this.txtPersNo_Enter);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(7, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "P. No.:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbtnFinType
            // 
            this.lbtnFinType.ActionLOVExists = "LOV_FinTypeExists";
            this.lbtnFinType.ActionType = "LOV_FinType";
            this.lbtnFinType.ConditionalFields = "";
            this.lbtnFinType.CustomEnabled = true;
            this.lbtnFinType.DataFieldMapping = "";
            this.lbtnFinType.DependentLovControls = "";
            this.lbtnFinType.HiddenColumns = "FN_Schedule|FN_Markup|FN_C_Ratio";
            this.lbtnFinType.Image = ((System.Drawing.Image)(resources.GetObject("lbtnFinType.Image")));
            this.lbtnFinType.LoadDependentEntities = false;
            this.lbtnFinType.Location = new System.Drawing.Point(163, 6);
            this.lbtnFinType.LookUpTitle = null;
            this.lbtnFinType.Name = "lbtnFinType";
            this.lbtnFinType.Size = new System.Drawing.Size(26, 21);
            this.lbtnFinType.SkipValidationOnLeave = false;
            this.lbtnFinType.SPName = "CHRIS_SP_FINANCEQUERY_MANAGER";
            this.lbtnFinType.TabIndex = 1;
            this.lbtnFinType.TabStop = false;
            this.lbtnFinType.UseVisualStyleBackColor = true;
            this.lbtnFinType.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbtnFinType_MouseDown);
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(497, 5);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 20);
            this.label29.TabIndex = 64;
            this.label29.Text = "Markup:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMarkup
            // 
            this.txtMarkup.AllowSpace = true;
            this.txtMarkup.AssociatedLookUpName = "";
            this.txtMarkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMarkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMarkup.ContinuationTextBox = null;
            this.txtMarkup.CustomEnabled = true;
            this.txtMarkup.DataFieldMapping = "FN_MARKUP";
            this.txtMarkup.Enabled = false;
            this.txtMarkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarkup.GetRecordsOnUpDownKeys = false;
            this.txtMarkup.IsDate = false;
            this.txtMarkup.Location = new System.Drawing.Point(562, 5);
            this.txtMarkup.MaxLength = 1;
            this.txtMarkup.Name = "txtMarkup";
            this.txtMarkup.NumberFormat = "###,###,##0.00";
            this.txtMarkup.Postfix = "";
            this.txtMarkup.Prefix = "";
            this.txtMarkup.ReadOnly = true;
            this.txtMarkup.Size = new System.Drawing.Size(35, 20);
            this.txtMarkup.SkipValidation = true;
            this.txtMarkup.TabIndex = 4;
            this.txtMarkup.TabStop = false;
            this.txtMarkup.TextType = CrplControlLibrary.TextType.String;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(401, 5);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 20);
            this.label28.TabIndex = 62;
            this.label28.Text = "Repay:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSchedule
            // 
            this.txtSchedule.AllowSpace = true;
            this.txtSchedule.AssociatedLookUpName = "";
            this.txtSchedule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSchedule.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSchedule.ContinuationTextBox = null;
            this.txtSchedule.CustomEnabled = true;
            this.txtSchedule.DataFieldMapping = "FN_SCHEDULE";
            this.txtSchedule.Enabled = false;
            this.txtSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSchedule.GetRecordsOnUpDownKeys = false;
            this.txtSchedule.IsDate = false;
            this.txtSchedule.Location = new System.Drawing.Point(461, 6);
            this.txtSchedule.MaxLength = 1;
            this.txtSchedule.Name = "txtSchedule";
            this.txtSchedule.NumberFormat = "###,###,##0.00";
            this.txtSchedule.Postfix = "";
            this.txtSchedule.Prefix = "";
            this.txtSchedule.ReadOnly = true;
            this.txtSchedule.Size = new System.Drawing.Size(30, 20);
            this.txtSchedule.SkipValidation = true;
            this.txtSchedule.TabIndex = 3;
            this.txtSchedule.TabStop = false;
            this.txtSchedule.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesc
            // 
            this.txtDesc.AllowSpace = true;
            this.txtDesc.AssociatedLookUpName = "";
            this.txtDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesc.ContinuationTextBox = null;
            this.txtDesc.CustomEnabled = true;
            this.txtDesc.DataFieldMapping = "FN_DESC";
            this.txtDesc.Enabled = false;
            this.txtDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesc.GetRecordsOnUpDownKeys = false;
            this.txtDesc.IsDate = false;
            this.txtDesc.Location = new System.Drawing.Point(268, 6);
            this.txtDesc.MaxLength = 30;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.NumberFormat = "###,###,##0.00";
            this.txtDesc.Postfix = "";
            this.txtDesc.Prefix = "";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(127, 20);
            this.txtDesc.SkipValidation = true;
            this.txtDesc.TabIndex = 2;
            this.txtDesc.TabStop = false;
            this.txtDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtType
            // 
            this.txtType.AllowSpace = true;
            this.txtType.AssociatedLookUpName = "lbtnFinType";
            this.txtType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtType.ContinuationTextBox = null;
            this.txtType.CustomEnabled = true;
            this.txtType.DataFieldMapping = "FN_TYPE";
            this.txtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.GetRecordsOnUpDownKeys = false;
            this.txtType.IsDate = false;
            this.txtType.IsLookUpField = true;
            this.txtType.IsRequired = true;
            this.txtType.Location = new System.Drawing.Point(92, 7);
            this.txtType.MaxLength = 6;
            this.txtType.Name = "txtType";
            this.txtType.NumberFormat = "###,###,##0.00";
            this.txtType.Postfix = "";
            this.txtType.Prefix = "";
            this.txtType.Size = new System.Drawing.Size(65, 20);
            this.txtType.SkipValidation = false;
            this.txtType.TabIndex = 0;
            this.txtType.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtType, "Press <F9> Key to Display The List");
            this.txtType.Validated += new System.EventHandler(this.txtType_Validated);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(6, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 20);
            this.label9.TabIndex = 28;
            this.label9.Text = "Type:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlFNDetail
            // 
            this.pnlFNDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFNDetail.ConcurrentPanels = null;
            this.pnlFNDetail.Controls.Add(this.label10);
            this.pnlFNDetail.Controls.Add(this.txtCreditRatioPer);
            this.pnlFNDetail.Controls.Add(this.txtTotalMonthInstall);
            this.pnlFNDetail.Controls.Add(this.label11);
            this.pnlFNDetail.Controls.Add(this.txtCanBeAvail);
            this.pnlFNDetail.Controls.Add(this.label16);
            this.pnlFNDetail.Controls.Add(this.txtRatioAvail);
            this.pnlFNDetail.Controls.Add(this.label17);
            this.pnlFNDetail.Controls.Add(this.label15);
            this.pnlFNDetail.Controls.Add(this.txtMinCap);
            this.pnlFNDetail.Controls.Add(this.txtFactor);
            this.pnlFNDetail.Controls.Add(this.label12);
            this.pnlFNDetail.Controls.Add(this.txtAvailable);
            this.pnlFNDetail.Controls.Add(this.label13);
            this.pnlFNDetail.Controls.Add(this.txtCreditRatio);
            this.pnlFNDetail.Controls.Add(this.label14);
            this.pnlFNDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlFNDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlFNDetail.DependentPanels = null;
            this.pnlFNDetail.DisableDependentLoad = false;
            this.pnlFNDetail.EnableDelete = true;
            this.pnlFNDetail.EnableInsert = true;
            this.pnlFNDetail.EnableQuery = false;
            this.pnlFNDetail.EnableUpdate = true;
            this.pnlFNDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlFNDetail.Location = new System.Drawing.Point(12, 248);
            this.pnlFNDetail.MasterPanel = null;
            this.pnlFNDetail.Name = "pnlFNDetail";
            this.pnlFNDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlFNDetail.Size = new System.Drawing.Size(617, 120);
            this.pnlFNDetail.SPName = "CHRIS_SP_TERMINATION_MANAGER";
            this.pnlFNDetail.TabIndex = 11;
            this.pnlFNDetail.TabStop = true;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(266, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(191, 20);
            this.label10.TabIndex = 74;
            this.label10.Text = "Credit Ratio %:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCreditRatioPer
            // 
            this.txtCreditRatioPer.AllowSpace = true;
            this.txtCreditRatioPer.AssociatedLookUpName = "";
            this.txtCreditRatioPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditRatioPer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreditRatioPer.ContinuationTextBox = null;
            this.txtCreditRatioPer.CustomEnabled = true;
            this.txtCreditRatioPer.DataFieldMapping = "CREDIT_RATIO_PER";
            this.txtCreditRatioPer.Enabled = false;
            this.txtCreditRatioPer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditRatioPer.GetRecordsOnUpDownKeys = false;
            this.txtCreditRatioPer.IsDate = false;
            this.txtCreditRatioPer.Location = new System.Drawing.Point(462, 87);
            this.txtCreditRatioPer.MaxLength = 500;
            this.txtCreditRatioPer.Name = "txtCreditRatioPer";
            this.txtCreditRatioPer.NumberFormat = "###,###,##0.00";
            this.txtCreditRatioPer.Postfix = "";
            this.txtCreditRatioPer.Prefix = "";
            this.txtCreditRatioPer.ReadOnly = true;
            this.txtCreditRatioPer.Size = new System.Drawing.Size(78, 20);
            this.txtCreditRatioPer.SkipValidation = true;
            this.txtCreditRatioPer.TabIndex = 73;
            this.txtCreditRatioPer.TabStop = false;
            this.txtCreditRatioPer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditRatioPer.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtTotalMonthInstall
            // 
            this.txtTotalMonthInstall.AllowSpace = true;
            this.txtTotalMonthInstall.AssociatedLookUpName = "";
            this.txtTotalMonthInstall.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalMonthInstall.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalMonthInstall.ContinuationTextBox = null;
            this.txtTotalMonthInstall.CustomEnabled = true;
            this.txtTotalMonthInstall.DataFieldMapping = "TOT_MONTHLY_INSTALL";
            this.txtTotalMonthInstall.Enabled = false;
            this.txtTotalMonthInstall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMonthInstall.GetRecordsOnUpDownKeys = false;
            this.txtTotalMonthInstall.IsDate = false;
            this.txtTotalMonthInstall.Location = new System.Drawing.Point(462, 61);
            this.txtTotalMonthInstall.MaxLength = 500;
            this.txtTotalMonthInstall.Name = "txtTotalMonthInstall";
            this.txtTotalMonthInstall.NumberFormat = "###,###,##0.00";
            this.txtTotalMonthInstall.Postfix = "";
            this.txtTotalMonthInstall.Prefix = "";
            this.txtTotalMonthInstall.ReadOnly = true;
            this.txtTotalMonthInstall.Size = new System.Drawing.Size(78, 20);
            this.txtTotalMonthInstall.SkipValidation = true;
            this.txtTotalMonthInstall.TabIndex = 69;
            this.txtTotalMonthInstall.TabStop = false;
            this.txtTotalMonthInstall.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalMonthInstall.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(266, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(191, 20);
            this.label11.TabIndex = 72;
            this.label11.Text = "Total Monthly Installments:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCanBeAvail
            // 
            this.txtCanBeAvail.AllowSpace = true;
            this.txtCanBeAvail.AssociatedLookUpName = "";
            this.txtCanBeAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCanBeAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCanBeAvail.ContinuationTextBox = null;
            this.txtCanBeAvail.CustomEnabled = true;
            this.txtCanBeAvail.DataFieldMapping = "CAN_BE";
            this.txtCanBeAvail.Enabled = false;
            this.txtCanBeAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCanBeAvail.GetRecordsOnUpDownKeys = false;
            this.txtCanBeAvail.IsDate = false;
            this.txtCanBeAvail.Location = new System.Drawing.Point(462, 35);
            this.txtCanBeAvail.MaxLength = 500;
            this.txtCanBeAvail.Name = "txtCanBeAvail";
            this.txtCanBeAvail.NumberFormat = "###,###,##0.00";
            this.txtCanBeAvail.Postfix = "";
            this.txtCanBeAvail.Prefix = "";
            this.txtCanBeAvail.ReadOnly = true;
            this.txtCanBeAvail.Size = new System.Drawing.Size(78, 20);
            this.txtCanBeAvail.SkipValidation = true;
            this.txtCanBeAvail.TabIndex = 68;
            this.txtCanBeAvail.TabStop = false;
            this.txtCanBeAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCanBeAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(266, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(191, 20);
            this.label16.TabIndex = 71;
            this.label16.Text = "Amt Of Loan Can Be Availed:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRatioAvail
            // 
            this.txtRatioAvail.AllowSpace = true;
            this.txtRatioAvail.AssociatedLookUpName = "lbtnPNo";
            this.txtRatioAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatioAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRatioAvail.ContinuationTextBox = null;
            this.txtRatioAvail.CustomEnabled = true;
            this.txtRatioAvail.DataFieldMapping = "RATIO_AVAILED";
            this.txtRatioAvail.Enabled = false;
            this.txtRatioAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatioAvail.GetRecordsOnUpDownKeys = false;
            this.txtRatioAvail.IsDate = false;
            this.txtRatioAvail.Location = new System.Drawing.Point(462, 9);
            this.txtRatioAvail.MaxLength = 500;
            this.txtRatioAvail.Name = "txtRatioAvail";
            this.txtRatioAvail.NumberFormat = "###,###,##0.00";
            this.txtRatioAvail.Postfix = "";
            this.txtRatioAvail.Prefix = "";
            this.txtRatioAvail.ReadOnly = true;
            this.txtRatioAvail.Size = new System.Drawing.Size(78, 20);
            this.txtRatioAvail.SkipValidation = true;
            this.txtRatioAvail.TabIndex = 67;
            this.txtRatioAvail.TabStop = false;
            this.txtRatioAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRatioAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(266, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(191, 20);
            this.label17.TabIndex = 70;
            this.label17.Text = "Less Ratio Availed:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(13, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 20);
            this.label15.TabIndex = 66;
            this.label15.Text = "Min.Cap.:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMinCap
            // 
            this.txtMinCap.AllowSpace = true;
            this.txtMinCap.AssociatedLookUpName = "";
            this.txtMinCap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinCap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMinCap.ContinuationTextBox = null;
            this.txtMinCap.CustomEnabled = true;
            this.txtMinCap.DataFieldMapping = "MIN_CAP";
            this.txtMinCap.Enabled = false;
            this.txtMinCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinCap.GetRecordsOnUpDownKeys = false;
            this.txtMinCap.IsDate = false;
            this.txtMinCap.Location = new System.Drawing.Point(117, 90);
            this.txtMinCap.MaxLength = 500;
            this.txtMinCap.Name = "txtMinCap";
            this.txtMinCap.NumberFormat = "###,###,##0.00";
            this.txtMinCap.Postfix = "";
            this.txtMinCap.Prefix = "";
            this.txtMinCap.ReadOnly = true;
            this.txtMinCap.Size = new System.Drawing.Size(78, 20);
            this.txtMinCap.SkipValidation = true;
            this.txtMinCap.TabIndex = 65;
            this.txtMinCap.TabStop = false;
            this.txtMinCap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinCap.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtFactor
            // 
            this.txtFactor.AllowSpace = true;
            this.txtFactor.AssociatedLookUpName = "";
            this.txtFactor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFactor.ContinuationTextBox = null;
            this.txtFactor.CustomEnabled = true;
            this.txtFactor.DataFieldMapping = "FACTOR";
            this.txtFactor.Enabled = false;
            this.txtFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactor.GetRecordsOnUpDownKeys = false;
            this.txtFactor.IsDate = false;
            this.txtFactor.Location = new System.Drawing.Point(117, 64);
            this.txtFactor.MaxLength = 500;
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.NumberFormat = "###,###,##0.000000";
            this.txtFactor.Postfix = "";
            this.txtFactor.Prefix = "";
            this.txtFactor.ReadOnly = true;
            this.txtFactor.Size = new System.Drawing.Size(78, 20);
            this.txtFactor.SkipValidation = true;
            this.txtFactor.TabIndex = 3;
            this.txtFactor.TabStop = false;
            this.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFactor.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(13, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 20);
            this.label12.TabIndex = 28;
            this.label12.Text = "Factor:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAvailable
            // 
            this.txtAvailable.AllowSpace = true;
            this.txtAvailable.AssociatedLookUpName = "";
            this.txtAvailable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvailable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvailable.ContinuationTextBox = null;
            this.txtAvailable.CustomEnabled = true;
            this.txtAvailable.DataFieldMapping = "AVAILABLE";
            this.txtAvailable.Enabled = false;
            this.txtAvailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvailable.GetRecordsOnUpDownKeys = false;
            this.txtAvailable.IsDate = false;
            this.txtAvailable.Location = new System.Drawing.Point(117, 38);
            this.txtAvailable.MaxLength = 500;
            this.txtAvailable.Name = "txtAvailable";
            this.txtAvailable.NumberFormat = "###,###,##0.00";
            this.txtAvailable.Postfix = "";
            this.txtAvailable.Prefix = "";
            this.txtAvailable.ReadOnly = true;
            this.txtAvailable.Size = new System.Drawing.Size(78, 20);
            this.txtAvailable.SkipValidation = true;
            this.txtAvailable.TabIndex = 2;
            this.txtAvailable.TabStop = false;
            this.txtAvailable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAvailable.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(13, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "Availab Ratio:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCreditRatio
            // 
            this.txtCreditRatio.AllowSpace = true;
            this.txtCreditRatio.AssociatedLookUpName = "lbtnPNo";
            this.txtCreditRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreditRatio.ContinuationTextBox = null;
            this.txtCreditRatio.CustomEnabled = true;
            this.txtCreditRatio.DataFieldMapping = "CREDIT_RATIO";
            this.txtCreditRatio.Enabled = false;
            this.txtCreditRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditRatio.GetRecordsOnUpDownKeys = false;
            this.txtCreditRatio.IsDate = false;
            this.txtCreditRatio.Location = new System.Drawing.Point(117, 12);
            this.txtCreditRatio.MaxLength = 500;
            this.txtCreditRatio.Name = "txtCreditRatio";
            this.txtCreditRatio.NumberFormat = "###,###,##0.00";
            this.txtCreditRatio.Postfix = "";
            this.txtCreditRatio.Prefix = "";
            this.txtCreditRatio.ReadOnly = true;
            this.txtCreditRatio.Size = new System.Drawing.Size(78, 20);
            this.txtCreditRatio.SkipValidation = true;
            this.txtCreditRatio.TabIndex = 0;
            this.txtCreditRatio.TabStop = false;
            this.txtCreditRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditRatio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(13, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 23;
            this.label14.Text = "Credit Ratio:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlFNMonthDetail
            // 
            this.pnlFNMonthDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFNMonthDetail.ConcurrentPanels = null;
            this.pnlFNMonthDetail.Controls.Add(this.dgvFnMoths);
            this.pnlFNMonthDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlFNMonthDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlFNMonthDetail.DependentPanels = null;
            this.pnlFNMonthDetail.DisableDependentLoad = false;
            this.pnlFNMonthDetail.EnableDelete = true;
            this.pnlFNMonthDetail.EnableInsert = true;
            this.pnlFNMonthDetail.EnableQuery = false;
            this.pnlFNMonthDetail.EnableUpdate = true;
            this.pnlFNMonthDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FNMonthCommand";
            this.pnlFNMonthDetail.Location = new System.Drawing.Point(12, 368);
            this.pnlFNMonthDetail.MasterPanel = null;
            this.pnlFNMonthDetail.Name = "pnlFNMonthDetail";
            this.pnlFNMonthDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlFNMonthDetail.Size = new System.Drawing.Size(617, 177);
            this.pnlFNMonthDetail.SPName = "CHRIS_SP_FNQUERY_MONTH_MANAGER";
            this.pnlFNMonthDetail.TabIndex = 12;
            // 
            // dgvFnMoths
            // 
            this.dgvFnMoths.AllowUserToAddRows = false;
            this.dgvFnMoths.AllowUserToDeleteRows = false;
            this.dgvFnMoths.AllowUserToResizeRows = false;
            this.dgvFnMoths.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFnMoths.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFnMoths.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFnMoths.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.colFinNo,
            this.colMonthlyDeduction,
            this.colMarkup,
            this.colBalance,
            this.colLiquidate,
            this.colCredit});
            this.dgvFnMoths.ColumnToHide = null;
            this.dgvFnMoths.ColumnWidth = null;
            this.dgvFnMoths.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFnMoths.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFnMoths.DisplayColumnWrapper = null;
            this.dgvFnMoths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFnMoths.GridDefaultRow = 0;
            this.dgvFnMoths.Location = new System.Drawing.Point(0, 0);
            this.dgvFnMoths.Name = "dgvFnMoths";
            this.dgvFnMoths.ReadOnlyColumns = null;
            this.dgvFnMoths.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFnMoths.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFnMoths.RowHeadersVisible = false;
            this.dgvFnMoths.Size = new System.Drawing.Size(615, 175);
            this.dgvFnMoths.SkippingColumns = null;
            this.dgvFnMoths.TabIndex = 0;
            this.dgvFnMoths.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvFnMoths_UserAddedRow);
            this.dgvFnMoths.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFnMoths_RowEnter);
            this.dgvFnMoths.Leave += new System.EventHandler(this.dgvFnMoths_Leave);
            this.dgvFnMoths.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFnMoths_RowLeave);
            this.dgvFnMoths.Validating += new System.ComponentModel.CancelEventHandler(this.dgvFnMoths_Validating);
            this.dgvFnMoths.SelectionChanged += new System.EventHandler(this.dgvFnMoths_SelectionChanged);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // colFinNo
            // 
            this.colFinNo.DataPropertyName = "FN_FIN_NO";
            this.colFinNo.HeaderText = "Fin./Loan Number";
            this.colFinNo.Name = "colFinNo";
            this.colFinNo.ReadOnly = true;
            this.colFinNo.Width = 155;
            // 
            // colMonthlyDeduction
            // 
            this.colMonthlyDeduction.DataPropertyName = "FN_CREDIT";
            this.colMonthlyDeduction.HeaderText = "Monthly Deduction";
            this.colMonthlyDeduction.Name = "colMonthlyDeduction";
            this.colMonthlyDeduction.ReadOnly = true;
            this.colMonthlyDeduction.Width = 150;
            // 
            // colMarkup
            // 
            this.colMarkup.DataPropertyName = "FN_MARKUP";
            this.colMarkup.HeaderText = "Markup";
            this.colMarkup.Name = "colMarkup";
            this.colMarkup.ReadOnly = true;
            this.colMarkup.Width = 130;
            // 
            // colBalance
            // 
            this.colBalance.DataPropertyName = "FN_BALANCE";
            this.colBalance.HeaderText = "Balance";
            this.colBalance.Name = "colBalance";
            this.colBalance.ReadOnly = true;
            this.colBalance.Width = 140;
            // 
            // colLiquidate
            // 
            this.colLiquidate.DataPropertyName = "FN_LIQ_FLAG";
            this.colLiquidate.HeaderText = "Liquidate[L]";
            this.colLiquidate.Name = "colLiquidate";
            this.colLiquidate.Width = 120;
            // 
            // colCredit
            // 
            this.colCredit.DataPropertyName = "FN_CREDIT";
            this.colCredit.HeaderText = "Credit";
            this.colCredit.Name = "colCredit";
            this.colCredit.Visible = false;
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.label19);
            this.slPanelSimple1.Controls.Add(this.lbtnFinType);
            this.slPanelSimple1.Controls.Add(this.label9);
            this.slPanelSimple1.Controls.Add(this.label29);
            this.slPanelSimple1.Controls.Add(this.txtType);
            this.slPanelSimple1.Controls.Add(this.txtMarkup);
            this.slPanelSimple1.Controls.Add(this.txtDesc);
            this.slPanelSimple1.Controls.Add(this.label28);
            this.slPanelSimple1.Controls.Add(this.txtSchedule);
            this.slPanelSimple1.Controls.Add(this.txtRatio);
            this.slPanelSimple1.DataManager = "iCORE.Common.CommonDataManager";
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand";
            this.slPanelSimple1.Location = new System.Drawing.Point(14, 214);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(613, 34);
            this.slPanelSimple1.SPName = "CHRIS_SP_TERMINATION_MANAGER";
            this.slPanelSimple1.TabIndex = 2;
            this.slPanelSimple1.TabStop = true;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(195, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 20);
            this.label19.TabIndex = 66;
            this.label19.Text = "Descrip:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRatio
            // 
            this.txtRatio.AllowSpace = true;
            this.txtRatio.AssociatedLookUpName = "lbtnPNo";
            this.txtRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRatio.ContinuationTextBox = null;
            this.txtRatio.CustomEnabled = false;
            this.txtRatio.DataFieldMapping = "FN_C_Ratio";
            this.txtRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatio.GetRecordsOnUpDownKeys = false;
            this.txtRatio.IsDate = false;
            this.txtRatio.IsLookUpField = true;
            this.txtRatio.IsRequired = true;
            this.txtRatio.Location = new System.Drawing.Point(563, 5);
            this.txtRatio.MaxLength = 6;
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.NumberFormat = "###,###,##0.00";
            this.txtRatio.Postfix = "";
            this.txtRatio.Prefix = "";
            this.txtRatio.Size = new System.Drawing.Size(33, 20);
            this.txtRatio.SkipValidation = false;
            this.txtRatio.TabIndex = 65;
            this.txtRatio.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtRatio, "Press <F9> Key to Display The List");
            this.txtRatio.Visible = false;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(415, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 21;
            this.lblUserName.Text = "User Name:";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CHRIS_Finance_FinanceQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 592);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.slPanelSimple1);
            this.Controls.Add(this.pnlFNMonthDetail);
            this.Controls.Add(this.pnlFNDetail);
            this.Controls.Add(this.pnlPersonnel);
            this.Controls.Add(this.pnlHead);
            this.CurrentPanelBlock = "pnlPersonnel";
            this.Name = "CHRIS_Finance_FinanceQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCORE CHRISTOP - Finance Query";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_FinanceQuery_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.pnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlFNDetail, 0);
            this.Controls.SetChildIndex(this.pnlFNMonthDetail, 0);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.pnlPersonnel.ResumeLayout(false);
            this.pnlPersonnel.PerformLayout();
            this.pnlFNDetail.ResumeLayout(false);
            this.pnlFNDetail.PerformLayout();
            this.pnlFNMonthDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFnMoths)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPersonnel;
        private CrplControlLibrary.SLTextBox txtType;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtDesc;
        private CrplControlLibrary.SLTextBox txtLongPkg;
        private CrplControlLibrary.SLTextBox txtAnnualPack;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLTextBox txtSchedule;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txtMarkup;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlFNDetail;
        private CrplControlLibrary.SLTextBox txtFactor;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txtAvailable;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtCreditRatio;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtMinCap;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txtCreditRatioPer;
        private CrplControlLibrary.SLTextBox txtTotalMonthInstall;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtCanBeAvail;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txtRatioAvail;
        private System.Windows.Forms.Label label17;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlFNMonthDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvFnMoths;
        private CrplControlLibrary.LookupButton lbtnFinType;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private CrplControlLibrary.SLTextBox txtCat;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtRatio;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFinNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMonthlyDeduction;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMarkup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLiquidate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCredit;
    }
}