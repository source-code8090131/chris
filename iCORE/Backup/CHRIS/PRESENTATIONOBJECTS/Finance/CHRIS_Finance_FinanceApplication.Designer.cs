namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_FinanceApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Finance_FinanceApplication));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_Pr_NIC = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtprLoanPack = new CrplControlLibrary.SLTextBox(this.components);
            this.lpBtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.DtJoining = new CrplControlLibrary.SLDatePicker(this.components);
            this.DtBirth = new CrplControlLibrary.SLDatePicker(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnPackage = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCBonus = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPerName = new CrplControlLibrary.SLTextBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label39 = new System.Windows.Forms.Label();
            this.PnlFinance = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lBtnFinNo = new CrplControlLibrary.LookupButton(this.components);
            this.DtEndDate = new CrplControlLibrary.SLDatePicker(this.components);
            this.DtSatartDt = new CrplControlLibrary.SLDatePicker(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txtFnschdl = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFnMrkup = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFnratio = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFnDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.txtFinanceType = new CrplControlLibrary.SLTextBox(this.components);
            this.DGVFinance = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtCurrOption = new CrplControlLibrary.SLTextBox(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.PnlCalAmnt = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.dtFN_SDATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.txtInsCovAmnt = new CrplControlLibrary.SLTextBox(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.txtFactor = new CrplControlLibrary.SLTextBox(this.components);
            this.label29 = new System.Windows.Forms.Label();
            this.txtAvailable = new CrplControlLibrary.SLTextBox(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.txtRatioAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.txtCreditRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.txtRmks = new CrplControlLibrary.SLTextBox(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.txtExcpRem = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.txtExcepRemOpt = new CrplControlLibrary.SLTextBox(this.components);
            this.label23 = new System.Windows.Forms.Label();
            this.txtExcpFlg = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.txtTotalMonth = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.txtDisbursmntAmt = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txtCapAmt = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txtCanBeAvail = new CrplControlLibrary.SLTextBox(this.components);
            this.PnlFinGrid = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label32 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_INSTALL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_LIQ_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_REDUCE_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_NEW_INST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_NEW_OS_BAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FN_PAY_LEFT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            this.PnlFinance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVFinance)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.PnlCalAmnt.SuspendLayout();
            this.PnlFinGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(617, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(653, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 610);
            this.panel1.Size = new System.Drawing.Size(653, 60);
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.txt_Pr_NIC);
            this.PnlPersonnel.Controls.Add(this.slTextBox2);
            this.PnlPersonnel.Controls.Add(this.slTextBox1);
            this.PnlPersonnel.Controls.Add(this.txtprLoanPack);
            this.PnlPersonnel.Controls.Add(this.lpBtnPersonnel);
            this.PnlPersonnel.Controls.Add(this.DtJoining);
            this.PnlPersonnel.Controls.Add(this.DtBirth);
            this.PnlPersonnel.Controls.Add(this.txtLevel);
            this.PnlPersonnel.Controls.Add(this.txtAnnPackage);
            this.PnlPersonnel.Controls.Add(this.txtDesg);
            this.PnlPersonnel.Controls.Add(this.txtBranch);
            this.PnlPersonnel.Controls.Add(this.txtCategory);
            this.PnlPersonnel.Controls.Add(this.txtCBonus);
            this.PnlPersonnel.Controls.Add(this.label10);
            this.PnlPersonnel.Controls.Add(this.label9);
            this.PnlPersonnel.Controls.Add(this.label8);
            this.PnlPersonnel.Controls.Add(this.label7);
            this.PnlPersonnel.Controls.Add(this.label6);
            this.PnlPersonnel.Controls.Add(this.label5);
            this.PnlPersonnel.Controls.Add(this.label4);
            this.PnlPersonnel.Controls.Add(this.label3);
            this.PnlPersonnel.Controls.Add(this.label2);
            this.PnlPersonnel.Controls.Add(this.txtPerName);
            this.PnlPersonnel.Controls.Add(this.label1);
            this.PnlPersonnel.Controls.Add(this.txtPersonnelNo);
            this.PnlPersonnel.DataManager = null;
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = true;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(12, 115);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(628, 101);
            this.PnlPersonnel.SPName = "CHRIS_SP_PERSONNELFinanceApplicaton_MANAGER";
            this.PnlPersonnel.TabIndex = 0;
            // 
            // txt_Pr_NIC
            // 
            this.txt_Pr_NIC.AllowSpace = true;
            this.txt_Pr_NIC.AssociatedLookUpName = "";
            this.txt_Pr_NIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pr_NIC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pr_NIC.ContinuationTextBox = null;
            this.txt_Pr_NIC.CustomEnabled = false;
            this.txt_Pr_NIC.DataFieldMapping = "pr_id_card_no";
            this.txt_Pr_NIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pr_NIC.GetRecordsOnUpDownKeys = false;
            this.txt_Pr_NIC.IsDate = false;
            this.txt_Pr_NIC.Location = new System.Drawing.Point(584, 12);
            this.txt_Pr_NIC.Name = "txt_Pr_NIC";
            this.txt_Pr_NIC.NumberFormat = "###,###,##0.00";
            this.txt_Pr_NIC.Postfix = "";
            this.txt_Pr_NIC.Prefix = "";
            this.txt_Pr_NIC.Size = new System.Drawing.Size(21, 20);
            this.txt_Pr_NIC.SkipValidation = false;
            this.txt_Pr_NIC.TabIndex = 80;
            this.txt_Pr_NIC.TabStop = false;
            this.txt_Pr_NIC.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pr_NIC.Visible = false;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "PR_JOINING_DATE";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(115, 66);
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.ReadOnly = true;
            this.slTextBox2.Size = new System.Drawing.Size(100, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 21;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox2.TextChanged += new System.EventHandler(this.slTextBox2_TextChanged);
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "PR_D_BIRTH";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(115, 40);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.ReadOnly = true;
            this.slTextBox1.Size = new System.Drawing.Size(100, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 20;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox1.TextChanged += new System.EventHandler(this.slTextBox1_TextChanged);
            // 
            // txtprLoanPack
            // 
            this.txtprLoanPack.AllowSpace = true;
            this.txtprLoanPack.AssociatedLookUpName = "";
            this.txtprLoanPack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprLoanPack.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtprLoanPack.ContinuationTextBox = null;
            this.txtprLoanPack.CustomEnabled = false;
            this.txtprLoanPack.DataFieldMapping = "PR_LOAN_PACK";
            this.txtprLoanPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprLoanPack.GetRecordsOnUpDownKeys = false;
            this.txtprLoanPack.IsDate = false;
            this.txtprLoanPack.Location = new System.Drawing.Point(421, 80);
            this.txtprLoanPack.Name = "txtprLoanPack";
            this.txtprLoanPack.NumberFormat = "###,###,##0.00";
            this.txtprLoanPack.Postfix = "";
            this.txtprLoanPack.Prefix = "";
            this.txtprLoanPack.Size = new System.Drawing.Size(100, 20);
            this.txtprLoanPack.SkipValidation = false;
            this.txtprLoanPack.TabIndex = 79;
            this.txtprLoanPack.TextType = CrplControlLibrary.TextType.String;
            this.txtprLoanPack.Visible = false;
            // 
            // lpBtnPersonnel
            // 
            this.lpBtnPersonnel.ActionLOVExists = "Pesonnel_FinanceApp_Exists";
            this.lpBtnPersonnel.ActionType = "Pesonnel_FinanceApp";
            this.lpBtnPersonnel.ConditionalFields = "";
            this.lpBtnPersonnel.CustomEnabled = true;
            this.lpBtnPersonnel.DataFieldMapping = "";
            this.lpBtnPersonnel.DependentLovControls = "";
            this.lpBtnPersonnel.HiddenColumns = "";
            this.lpBtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lpBtnPersonnel.Image")));
            this.lpBtnPersonnel.LoadDependentEntities = false;
            this.lpBtnPersonnel.Location = new System.Drawing.Point(221, 11);
            this.lpBtnPersonnel.LookUpTitle = null;
            this.lpBtnPersonnel.Name = "lpBtnPersonnel";
            this.lpBtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lpBtnPersonnel.SkipValidationOnLeave = false;
            this.lpBtnPersonnel.SPName = "CHRIS_SP_PERSONNELFinanceApplicaton_MANAGER";
            this.lpBtnPersonnel.TabIndex = 22;
            this.lpBtnPersonnel.TabStop = false;
            this.lpBtnPersonnel.UseVisualStyleBackColor = true;
            // 
            // DtJoining
            // 
            this.DtJoining.CustomEnabled = false;
            this.DtJoining.CustomFormat = "dd/MM/yyyy";
            this.DtJoining.DataFieldMapping = "PR_JOINING_DATE";
            this.DtJoining.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtJoining.HasChanges = false;
            this.DtJoining.Location = new System.Drawing.Point(9, 78);
            this.DtJoining.Name = "DtJoining";
            this.DtJoining.NullValue = " ";
            this.DtJoining.Size = new System.Drawing.Size(100, 20);
            this.DtJoining.TabIndex = 21;
            this.DtJoining.Value = new System.DateTime(2010, 12, 27, 0, 0, 0, 0);
            this.DtJoining.Visible = false;
            // 
            // DtBirth
            // 
            this.DtBirth.CustomEnabled = false;
            this.DtBirth.CustomFormat = "dd/MM/yyyy";
            this.DtBirth.DataFieldMapping = "PR_D_BIRTH";
            this.DtBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtBirth.HasChanges = false;
            this.DtBirth.Location = new System.Drawing.Point(7, 52);
            this.DtBirth.Name = "DtBirth";
            this.DtBirth.NullValue = " ";
            this.DtBirth.Size = new System.Drawing.Size(100, 20);
            this.DtBirth.TabIndex = 20;
            this.DtBirth.Value = new System.DateTime(2010, 12, 27, 0, 0, 0, 0);
            this.DtBirth.Visible = false;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(437, 38);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(40, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 19;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAnnPackage
            // 
            this.txtAnnPackage.AllowSpace = true;
            this.txtAnnPackage.AssociatedLookUpName = "";
            this.txtAnnPackage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnPackage.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnPackage.ContinuationTextBox = null;
            this.txtAnnPackage.CustomEnabled = true;
            this.txtAnnPackage.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtAnnPackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnPackage.GetRecordsOnUpDownKeys = false;
            this.txtAnnPackage.IsDate = false;
            this.txtAnnPackage.Location = new System.Drawing.Point(334, 64);
            this.txtAnnPackage.Name = "txtAnnPackage";
            this.txtAnnPackage.NumberFormat = "###,###,##";
            this.txtAnnPackage.Postfix = "";
            this.txtAnnPackage.Prefix = "";
            this.txtAnnPackage.Size = new System.Drawing.Size(68, 20);
            this.txtAnnPackage.SkipValidation = false;
            this.txtAnnPackage.TabIndex = 2;
            this.txtAnnPackage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnnPackage.TextType = CrplControlLibrary.TextType.Amount;
            this.txtAnnPackage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtAnnPackage_MouseDown);
            this.txtAnnPackage.Validating += new System.ComponentModel.CancelEventHandler(this.txtAnnPackage_Validating);
            // 
            // txtDesg
            // 
            this.txtDesg.AllowSpace = true;
            this.txtDesg.AssociatedLookUpName = "";
            this.txtDesg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesg.ContinuationTextBox = null;
            this.txtDesg.CustomEnabled = true;
            this.txtDesg.DataFieldMapping = "PR_DESIG";
            this.txtDesg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesg.ForeColor = System.Drawing.Color.Black;
            this.txtDesg.GetRecordsOnUpDownKeys = false;
            this.txtDesg.IsDate = false;
            this.txtDesg.Location = new System.Drawing.Point(334, 40);
            this.txtDesg.Name = "txtDesg";
            this.txtDesg.NumberFormat = "###,###,##0.00";
            this.txtDesg.Postfix = "";
            this.txtDesg.Prefix = "";
            this.txtDesg.ReadOnly = true;
            this.txtDesg.Size = new System.Drawing.Size(45, 20);
            this.txtDesg.SkipValidation = false;
            this.txtDesg.TabIndex = 17;
            this.txtDesg.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "PR_NEW_BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(546, 12);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(32, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 16;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCategory
            // 
            this.txtCategory.AllowSpace = true;
            this.txtCategory.AssociatedLookUpName = "";
            this.txtCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategory.ContinuationTextBox = null;
            this.txtCategory.CustomEnabled = true;
            this.txtCategory.DataFieldMapping = "PR_CATEGORY";
            this.txtCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.GetRecordsOnUpDownKeys = false;
            this.txtCategory.IsDate = false;
            this.txtCategory.Location = new System.Drawing.Point(546, 40);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.NumberFormat = "###,###,##0.00";
            this.txtCategory.Postfix = "";
            this.txtCategory.Prefix = "";
            this.txtCategory.ReadOnly = true;
            this.txtCategory.Size = new System.Drawing.Size(32, 20);
            this.txtCategory.SkipValidation = false;
            this.txtCategory.TabIndex = 15;
            this.txtCategory.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCBonus
            // 
            this.txtCBonus.AllowSpace = true;
            this.txtCBonus.AssociatedLookUpName = "";
            this.txtCBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCBonus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCBonus.ContinuationTextBox = null;
            this.txtCBonus.CustomEnabled = true;
            this.txtCBonus.DataFieldMapping = "W_10_C";
            this.txtCBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCBonus.GetRecordsOnUpDownKeys = false;
            this.txtCBonus.IsDate = false;
            this.txtCBonus.IsRequired = true;
            this.txtCBonus.Location = new System.Drawing.Point(546, 66);
            this.txtCBonus.Name = "txtCBonus";
            this.txtCBonus.NumberFormat = "###,###,##0.00";
            this.txtCBonus.Postfix = "";
            this.txtCBonus.Prefix = "";
            this.txtCBonus.ReadOnly = true;
            this.txtCBonus.Size = new System.Drawing.Size(51, 20);
            this.txtCBonus.SkipValidation = false;
            this.txtCBonus.TabIndex = 14;
            this.txtCBonus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCBonus.TextType = CrplControlLibrary.TextType.Double;
            this.txtCBonus.Validating += new System.ComponentModel.CancelEventHandler(this.txtCBonus_Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(599, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "%";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(385, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Level :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(224, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Annual Package  :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(250, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Designation :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(469, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "10 C Bonus :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(484, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Category :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(494, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Branch :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Date Of Joining :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Date Of Birth :";
            // 
            // txtPerName
            // 
            this.txtPerName.AllowSpace = true;
            this.txtPerName.AssociatedLookUpName = "";
            this.txtPerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPerName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPerName.ContinuationTextBox = null;
            this.txtPerName.CustomEnabled = true;
            this.txtPerName.DataFieldMapping = "NAME";
            this.txtPerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPerName.GetRecordsOnUpDownKeys = false;
            this.txtPerName.IsDate = false;
            this.txtPerName.Location = new System.Drawing.Point(253, 12);
            this.txtPerName.Name = "txtPerName";
            this.txtPerName.NumberFormat = "###,###,##0.00";
            this.txtPerName.Postfix = "";
            this.txtPerName.Prefix = "";
            this.txtPerName.ReadOnly = true;
            this.txtPerName.Size = new System.Drawing.Size(224, 20);
            this.txtPerName.SkipValidation = false;
            this.txtPerName.TabIndex = 2;
            this.txtPerName.TextType = CrplControlLibrary.TextType.String;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Personnel No. :";
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "lpBtnPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(115, 12);
            this.txtPersonnelNo.MaxLength = 6;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 1;
            this.txtPersonnelNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.toolTip1.SetToolTip(this.txtPersonnelNo, "Personnel Number Required  PRESS F9");
            this.txtPersonnelNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNo_Validating);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(233, 110);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(177, 13);
            this.label39.TabIndex = 56;
            this.label39.Text = "      >> Personnel Info<<        ";
            // 
            // PnlFinance
            // 
            this.PnlFinance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlFinance.ConcurrentPanels = null;
            this.PnlFinance.Controls.Add(this.lBtnFinNo);
            this.PnlFinance.Controls.Add(this.DtEndDate);
            this.PnlFinance.Controls.Add(this.DtSatartDt);
            this.PnlFinance.Controls.Add(this.label15);
            this.PnlFinance.Controls.Add(this.txtFnschdl);
            this.PnlFinance.Controls.Add(this.label13);
            this.PnlFinance.Controls.Add(this.label11);
            this.PnlFinance.Controls.Add(this.txtFnMrkup);
            this.PnlFinance.Controls.Add(this.txtFnratio);
            this.PnlFinance.Controls.Add(this.label14);
            this.PnlFinance.Controls.Add(this.label16);
            this.PnlFinance.Controls.Add(this.label17);
            this.PnlFinance.Controls.Add(this.label19);
            this.PnlFinance.Controls.Add(this.txtFnDesc);
            this.PnlFinance.Controls.Add(this.label20);
            this.PnlFinance.Controls.Add(this.txtFinanceType);
            this.PnlFinance.DataManager = null;
            this.PnlFinance.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlFinance.DependentPanels = null;
            this.PnlFinance.DisableDependentLoad = false;
            this.PnlFinance.EnableDelete = true;
            this.PnlFinance.EnableInsert = true;
            this.PnlFinance.EnableQuery = false;
            this.PnlFinance.EnableUpdate = true;
            this.PnlFinance.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand";
            this.PnlFinance.Location = new System.Drawing.Point(12, 209);
            this.PnlFinance.MasterPanel = null;
            this.PnlFinance.Name = "PnlFinance";
            this.PnlFinance.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlFinance.Size = new System.Drawing.Size(628, 64);
            this.PnlFinance.SPName = "CHRIS_SP_FIN_TYPE_MANAGER";
            this.PnlFinance.TabIndex = 3;
            // 
            // lBtnFinNo
            // 
            this.lBtnFinNo.ActionLOVExists = "FinNo_FinanceApp_Exists";
            this.lBtnFinNo.ActionType = "FinNo_FinanceApp";
            this.lBtnFinNo.ConditionalFields = "";
            this.lBtnFinNo.CustomEnabled = true;
            this.lBtnFinNo.DataFieldMapping = "";
            this.lBtnFinNo.DependentLovControls = "";
            this.lBtnFinNo.HiddenColumns = "";
            this.lBtnFinNo.Image = ((System.Drawing.Image)(resources.GetObject("lBtnFinNo.Image")));
            this.lBtnFinNo.LoadDependentEntities = false;
            this.lBtnFinNo.Location = new System.Drawing.Point(192, 12);
            this.lBtnFinNo.LookUpTitle = null;
            this.lBtnFinNo.Name = "lBtnFinNo";
            this.lBtnFinNo.Size = new System.Drawing.Size(26, 21);
            this.lBtnFinNo.SkipValidationOnLeave = false;
            this.lBtnFinNo.SPName = "CHRIS_SP_FIN_TYPE_MANAGER";
            this.lBtnFinNo.TabIndex = 26;
            this.lBtnFinNo.TabStop = false;
            this.lBtnFinNo.UseVisualStyleBackColor = true;
            // 
            // DtEndDate
            // 
            this.DtEndDate.CustomEnabled = false;
            this.DtEndDate.CustomFormat = "dd/MM/yyyy";
            this.DtEndDate.DataFieldMapping = "FN_END_DATE";
            this.DtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtEndDate.HasChanges = false;
            this.DtEndDate.Location = new System.Drawing.Point(334, 38);
            this.DtEndDate.Name = "DtEndDate";
            this.DtEndDate.NullValue = " ";
            this.DtEndDate.Size = new System.Drawing.Size(100, 20);
            this.DtEndDate.TabIndex = 25;
            this.DtEndDate.Value = new System.DateTime(2010, 12, 27, 0, 0, 0, 0);
            // 
            // DtSatartDt
            // 
            this.DtSatartDt.CustomEnabled = true;
            this.DtSatartDt.CustomFormat = "dd/MM/yyyy";
            this.DtSatartDt.DataFieldMapping = "FN_START_DATE";
            this.DtSatartDt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtSatartDt.HasChanges = true;
            this.DtSatartDt.Location = new System.Drawing.Point(116, 38);
            this.DtSatartDt.Name = "DtSatartDt";
            this.DtSatartDt.NullValue = " ";
            this.DtSatartDt.Size = new System.Drawing.Size(100, 20);
            this.DtSatartDt.TabIndex = 5;
            this.DtSatartDt.Value = new System.DateTime(2010, 12, 27, 0, 0, 0, 0);
            this.DtSatartDt.Validated += new System.EventHandler(this.DtSatartDt_Validated);
            this.DtSatartDt.Validating += new System.ComponentModel.CancelEventHandler(this.DtSatartDt_Validating);
            this.DtSatartDt.Leave += new System.EventHandler(this.DtSatartDt_Leave);
            this.DtSatartDt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DtSatartDt_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(371, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "Tenure :";
            // 
            // txtFnschdl
            // 
            this.txtFnschdl.AllowSpace = true;
            this.txtFnschdl.AssociatedLookUpName = "";
            this.txtFnschdl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnschdl.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnschdl.ContinuationTextBox = null;
            this.txtFnschdl.CustomEnabled = true;
            this.txtFnschdl.DataFieldMapping = "FN_SCHEDULE";
            this.txtFnschdl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnschdl.GetRecordsOnUpDownKeys = false;
            this.txtFnschdl.IsDate = false;
            this.txtFnschdl.Location = new System.Drawing.Point(437, 13);
            this.txtFnschdl.Name = "txtFnschdl";
            this.txtFnschdl.NumberFormat = "###,###,##0.00";
            this.txtFnschdl.Postfix = "";
            this.txtFnschdl.Prefix = "";
            this.txtFnschdl.ReadOnly = true;
            this.txtFnschdl.Size = new System.Drawing.Size(40, 20);
            this.txtFnschdl.SkipValidation = false;
            this.txtFnschdl.TabIndex = 22;
            this.txtFnschdl.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(589, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(589, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "%";
            // 
            // txtFnMrkup
            // 
            this.txtFnMrkup.AllowSpace = true;
            this.txtFnMrkup.AssociatedLookUpName = "";
            this.txtFnMrkup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnMrkup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnMrkup.ContinuationTextBox = null;
            this.txtFnMrkup.CustomEnabled = true;
            this.txtFnMrkup.DataFieldMapping = "FN_MARKUP";
            this.txtFnMrkup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnMrkup.GetRecordsOnUpDownKeys = false;
            this.txtFnMrkup.IsDate = false;
            this.txtFnMrkup.Location = new System.Drawing.Point(546, 13);
            this.txtFnMrkup.Name = "txtFnMrkup";
            this.txtFnMrkup.NumberFormat = "###,###,##0.00";
            this.txtFnMrkup.Postfix = "";
            this.txtFnMrkup.Prefix = "";
            this.txtFnMrkup.ReadOnly = true;
            this.txtFnMrkup.Size = new System.Drawing.Size(32, 20);
            this.txtFnMrkup.SkipValidation = false;
            this.txtFnMrkup.TabIndex = 16;
            this.txtFnMrkup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFnMrkup.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtFnratio
            // 
            this.txtFnratio.AllowSpace = true;
            this.txtFnratio.AssociatedLookUpName = "";
            this.txtFnratio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnratio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnratio.ContinuationTextBox = null;
            this.txtFnratio.CustomEnabled = true;
            this.txtFnratio.DataFieldMapping = "FN_C_RATIO";
            this.txtFnratio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnratio.GetRecordsOnUpDownKeys = false;
            this.txtFnratio.IsDate = false;
            this.txtFnratio.Location = new System.Drawing.Point(546, 38);
            this.txtFnratio.Name = "txtFnratio";
            this.txtFnratio.NumberFormat = "###,###,##0.00";
            this.txtFnratio.Postfix = "";
            this.txtFnratio.Prefix = "";
            this.txtFnratio.ReadOnly = true;
            this.txtFnratio.Size = new System.Drawing.Size(32, 20);
            this.txtFnratio.SkipValidation = false;
            this.txtFnratio.TabIndex = 15;
            this.txtFnratio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFnratio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(250, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "End Date :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(499, 43);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Ratio :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(483, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Mark up :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(31, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Start Date :";
            // 
            // txtFnDesc
            // 
            this.txtFnDesc.AllowSpace = true;
            this.txtFnDesc.AssociatedLookUpName = "";
            this.txtFnDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFnDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFnDesc.ContinuationTextBox = null;
            this.txtFnDesc.CustomEnabled = true;
            this.txtFnDesc.DataFieldMapping = "FN_DESC";
            this.txtFnDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFnDesc.GetRecordsOnUpDownKeys = false;
            this.txtFnDesc.IsDate = false;
            this.txtFnDesc.Location = new System.Drawing.Point(221, 13);
            this.txtFnDesc.Name = "txtFnDesc";
            this.txtFnDesc.NumberFormat = "###,###,##0.00";
            this.txtFnDesc.Postfix = "";
            this.txtFnDesc.Prefix = "";
            this.txtFnDesc.ReadOnly = true;
            this.txtFnDesc.Size = new System.Drawing.Size(144, 20);
            this.txtFnDesc.SkipValidation = false;
            this.txtFnDesc.TabIndex = 2;
            this.txtFnDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(14, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Finance Type :";
            // 
            // txtFinanceType
            // 
            this.txtFinanceType.AllowSpace = true;
            this.txtFinanceType.AssociatedLookUpName = "lBtnFinNo";
            this.txtFinanceType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceType.ContinuationTextBox = null;
            this.txtFinanceType.CustomEnabled = true;
            this.txtFinanceType.DataFieldMapping = "FN_TYPE";
            this.txtFinanceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceType.GetRecordsOnUpDownKeys = false;
            this.txtFinanceType.IsDate = false;
            this.txtFinanceType.IsRequired = true;
            this.txtFinanceType.Location = new System.Drawing.Point(115, 13);
            this.txtFinanceType.Name = "txtFinanceType";
            this.txtFinanceType.NumberFormat = "###,###,##0.00";
            this.txtFinanceType.Postfix = "";
            this.txtFinanceType.Prefix = "";
            this.txtFinanceType.Size = new System.Drawing.Size(74, 20);
            this.txtFinanceType.SkipValidation = false;
            this.txtFinanceType.TabIndex = 4;
            this.txtFinanceType.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtFinanceType, "Fin Type Required Press F9 ");
            // 
            // DGVFinance
            // 
            this.DGVFinance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVFinance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.FN_INSTALL,
            this.FN_BALANCE,
            this.FN_LIQ_FLAG,
            this.FN_REDUCE_AMT,
            this.FN_NEW_INST,
            this.FN_NEW_OS_BAL,
            this.Column8,
            this.Column9,
            this.FN_PAY_LEFT});
            this.DGVFinance.ColumnToHide = null;
            this.DGVFinance.ColumnWidth = null;
            this.DGVFinance.CustomEnabled = true;
            this.DGVFinance.DisplayColumnWrapper = null;
            this.DGVFinance.GridDefaultRow = 0;
            this.DGVFinance.Location = new System.Drawing.Point(7, 0);
            this.DGVFinance.Name = "DGVFinance";
            this.DGVFinance.ReadOnlyColumns = null;
            this.DGVFinance.RequiredColumns = null;
            this.DGVFinance.Size = new System.Drawing.Size(605, 144);
            this.DGVFinance.SkippingColumns = "FN_NEW_INST,FN_NEW_OS_BAL";
            this.DGVFinance.TabIndex = 21;
            this.DGVFinance.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVFinance_CellLeave);
            this.DGVFinance.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGVFinance_CellValidating);
            this.DGVFinance.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGVFinance_EditingControlShowing);
            this.DGVFinance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGVFinance_KeyDown);
            this.DGVFinance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DGVFinance_KeyPress);
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label33);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.txtCurrOption);
            this.pnlHead.Controls.Add(this.label34);
            this.pnlHead.Controls.Add(this.label35);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label36);
            this.pnlHead.Controls.Add(this.label37);
            this.pnlHead.Location = new System.Drawing.Point(12, 65);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(628, 44);
            this.pnlHead.TabIndex = 51;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(152, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 20);
            this.label33.TabIndex = 19;
            this.label33.Text = "Finance Application";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(496, 16);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtCurrOption
            // 
            this.txtCurrOption.AllowSpace = true;
            this.txtCurrOption.AssociatedLookUpName = "";
            this.txtCurrOption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrOption.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrOption.ContinuationTextBox = null;
            this.txtCurrOption.CustomEnabled = true;
            this.txtCurrOption.DataFieldMapping = "";
            this.txtCurrOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrOption.GetRecordsOnUpDownKeys = false;
            this.txtCurrOption.IsDate = false;
            this.txtCurrOption.Location = new System.Drawing.Point(501, 77);
            this.txtCurrOption.MaxLength = 6;
            this.txtCurrOption.Name = "txtCurrOption";
            this.txtCurrOption.NumberFormat = "###,###,##0.00";
            this.txtCurrOption.Postfix = "";
            this.txtCurrOption.Prefix = "";
            this.txtCurrOption.ReadOnly = true;
            this.txtCurrOption.Size = new System.Drawing.Size(80, 20);
            this.txtCurrOption.SkipValidation = false;
            this.txtCurrOption.TabIndex = 17;
            this.txtCurrOption.TabStop = false;
            this.txtCurrOption.TextType = CrplControlLibrary.TextType.String;
            this.txtCurrOption.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(449, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 15);
            this.label34.TabIndex = 16;
            this.label34.Text = "Date:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(450, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 15);
            this.label35.TabIndex = 15;
            this.label35.Text = "Option:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label35.Visible = false;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(83, 87);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(80, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            this.txtLocation.Visible = false;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(83, 61);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(80, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            this.txtUser.Visible = false;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(7, 87);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 20);
            this.label36.TabIndex = 2;
            this.label36.Text = "Location:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(16, 61);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 20);
            this.label37.TabIndex = 1;
            this.label37.Text = "User:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label37.Visible = false;
            // 
            // PnlCalAmnt
            // 
            this.PnlCalAmnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlCalAmnt.ConcurrentPanels = null;
            this.PnlCalAmnt.Controls.Add(this.dtFN_SDATE);
            this.PnlCalAmnt.Controls.Add(this.label27);
            this.PnlCalAmnt.Controls.Add(this.txtInsCovAmnt);
            this.PnlCalAmnt.Controls.Add(this.label28);
            this.PnlCalAmnt.Controls.Add(this.txtFactor);
            this.PnlCalAmnt.Controls.Add(this.label29);
            this.PnlCalAmnt.Controls.Add(this.txtAvailable);
            this.PnlCalAmnt.Controls.Add(this.label30);
            this.PnlCalAmnt.Controls.Add(this.txtRatioAvail);
            this.PnlCalAmnt.Controls.Add(this.label31);
            this.PnlCalAmnt.Controls.Add(this.txtCreditRatio);
            this.PnlCalAmnt.Controls.Add(this.label26);
            this.PnlCalAmnt.Controls.Add(this.txtRmks);
            this.PnlCalAmnt.Controls.Add(this.label25);
            this.PnlCalAmnt.Controls.Add(this.txtExcpRem);
            this.PnlCalAmnt.Controls.Add(this.label24);
            this.PnlCalAmnt.Controls.Add(this.txtExcepRemOpt);
            this.PnlCalAmnt.Controls.Add(this.label23);
            this.PnlCalAmnt.Controls.Add(this.txtExcpFlg);
            this.PnlCalAmnt.Controls.Add(this.label22);
            this.PnlCalAmnt.Controls.Add(this.txtTotalMonth);
            this.PnlCalAmnt.Controls.Add(this.label21);
            this.PnlCalAmnt.Controls.Add(this.txtDisbursmntAmt);
            this.PnlCalAmnt.Controls.Add(this.label18);
            this.PnlCalAmnt.Controls.Add(this.txtCapAmt);
            this.PnlCalAmnt.Controls.Add(this.label12);
            this.PnlCalAmnt.Controls.Add(this.txtCanBeAvail);
            this.PnlCalAmnt.DataManager = null;
            this.PnlCalAmnt.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlCalAmnt.DependentPanels = null;
            this.PnlCalAmnt.DisableDependentLoad = false;
            this.PnlCalAmnt.EnableDelete = true;
            this.PnlCalAmnt.EnableInsert = true;
            this.PnlCalAmnt.EnableQuery = false;
            this.PnlCalAmnt.EnableUpdate = true;
            this.PnlCalAmnt.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinanceAmtCanAvialCommand";
            this.PnlCalAmnt.Location = new System.Drawing.Point(12, 420);
            this.PnlCalAmnt.MasterPanel = null;
            this.PnlCalAmnt.Name = "PnlCalAmnt";
            this.PnlCalAmnt.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlCalAmnt.Size = new System.Drawing.Size(628, 212);
            this.PnlCalAmnt.SPName = "CHRIS_SP_FINANCE_APP_PROC_4";
            this.PnlCalAmnt.TabIndex = 52;
            // 
            // dtFN_SDATE
            // 
            this.dtFN_SDATE.CustomEnabled = false;
            this.dtFN_SDATE.CustomFormat = "dd/MM/yyyy";
            this.dtFN_SDATE.DataFieldMapping = "FN_SDATE";
            this.dtFN_SDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFN_SDATE.HasChanges = false;
            this.dtFN_SDATE.Location = new System.Drawing.Point(503, 139);
            this.dtFN_SDATE.Name = "dtFN_SDATE";
            this.dtFN_SDATE.NullValue = " ";
            this.dtFN_SDATE.Size = new System.Drawing.Size(105, 20);
            this.dtFN_SDATE.TabIndex = 78;
            this.dtFN_SDATE.Value = new System.DateTime(2010, 12, 28, 0, 0, 0, 0);
            this.dtFN_SDATE.Visible = false;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(367, 111);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(136, 13);
            this.label27.TabIndex = 76;
            this.label27.Text = "Ins Coverage Amount :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInsCovAmnt
            // 
            this.txtInsCovAmnt.AllowSpace = true;
            this.txtInsCovAmnt.AssociatedLookUpName = "";
            this.txtInsCovAmnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsCovAmnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInsCovAmnt.ContinuationTextBox = null;
            this.txtInsCovAmnt.CustomEnabled = true;
            this.txtInsCovAmnt.DataFieldMapping = "INS_COV_AMT";
            this.txtInsCovAmnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsCovAmnt.GetRecordsOnUpDownKeys = false;
            this.txtInsCovAmnt.IsDate = false;
            this.txtInsCovAmnt.Location = new System.Drawing.Point(503, 109);
            this.txtInsCovAmnt.Name = "txtInsCovAmnt";
            this.txtInsCovAmnt.NumberFormat = "###,###,##0.00";
            this.txtInsCovAmnt.Postfix = "";
            this.txtInsCovAmnt.Prefix = "";
            this.txtInsCovAmnt.Size = new System.Drawing.Size(100, 20);
            this.txtInsCovAmnt.SkipValidation = false;
            this.txtInsCovAmnt.TabIndex = 75;
            this.txtInsCovAmnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInsCovAmnt.TextType = CrplControlLibrary.TextType.Amount;
            this.txtInsCovAmnt.Validating += new System.ComponentModel.CancelEventHandler(this.txtInsCovAmnt_Validating);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(446, 87);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 13);
            this.label28.TabIndex = 74;
            this.label28.Text = "Factor:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFactor
            // 
            this.txtFactor.AllowSpace = true;
            this.txtFactor.AssociatedLookUpName = "";
            this.txtFactor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFactor.ContinuationTextBox = null;
            this.txtFactor.CustomEnabled = true;
            this.txtFactor.DataFieldMapping = "FACTOR";
            this.txtFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactor.GetRecordsOnUpDownKeys = false;
            this.txtFactor.IsDate = false;
            this.txtFactor.Location = new System.Drawing.Point(503, 85);
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.NumberFormat = "###,###,##0.00";
            this.txtFactor.Postfix = "";
            this.txtFactor.Prefix = "";
            this.txtFactor.ReadOnly = true;
            this.txtFactor.Size = new System.Drawing.Size(100, 20);
            this.txtFactor.SkipValidation = false;
            this.txtFactor.TabIndex = 73;
            this.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFactor.TextType = CrplControlLibrary.TextType.String;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(402, 57);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 13);
            this.label29.TabIndex = 72;
            this.label29.Text = "Available Ratio:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAvailable
            // 
            this.txtAvailable.AllowSpace = true;
            this.txtAvailable.AssociatedLookUpName = "";
            this.txtAvailable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvailable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvailable.ContinuationTextBox = null;
            this.txtAvailable.CustomEnabled = true;
            this.txtAvailable.DataFieldMapping = "AVAILABLE_RATIO";
            this.txtAvailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvailable.GetRecordsOnUpDownKeys = false;
            this.txtAvailable.IsDate = false;
            this.txtAvailable.Location = new System.Drawing.Point(503, 57);
            this.txtAvailable.Name = "txtAvailable";
            this.txtAvailable.NumberFormat = "###,###,##0.00";
            this.txtAvailable.Postfix = "";
            this.txtAvailable.Prefix = "";
            this.txtAvailable.ReadOnly = true;
            this.txtAvailable.Size = new System.Drawing.Size(100, 20);
            this.txtAvailable.SkipValidation = false;
            this.txtAvailable.TabIndex = 71;
            this.txtAvailable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAvailable.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(379, 31);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(121, 13);
            this.label30.TabIndex = 70;
            this.label30.Text = "Less Ratio Availed :";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRatioAvail
            // 
            this.txtRatioAvail.AllowSpace = true;
            this.txtRatioAvail.AssociatedLookUpName = "";
            this.txtRatioAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatioAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRatioAvail.ContinuationTextBox = null;
            this.txtRatioAvail.CustomEnabled = true;
            this.txtRatioAvail.DataFieldMapping = "LESS_RATIO_AVAILED";
            this.txtRatioAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatioAvail.GetRecordsOnUpDownKeys = false;
            this.txtRatioAvail.IsDate = false;
            this.txtRatioAvail.Location = new System.Drawing.Point(503, 29);
            this.txtRatioAvail.Name = "txtRatioAvail";
            this.txtRatioAvail.NumberFormat = "###,###,##0.00";
            this.txtRatioAvail.Postfix = "";
            this.txtRatioAvail.Prefix = "";
            this.txtRatioAvail.ReadOnly = true;
            this.txtRatioAvail.Size = new System.Drawing.Size(100, 20);
            this.txtRatioAvail.SkipValidation = false;
            this.txtRatioAvail.TabIndex = 69;
            this.txtRatioAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRatioAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(418, 11);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 13);
            this.label31.TabIndex = 68;
            this.label31.Text = "Credit Ratio:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCreditRatio
            // 
            this.txtCreditRatio.AllowSpace = true;
            this.txtCreditRatio.AssociatedLookUpName = "";
            this.txtCreditRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreditRatio.ContinuationTextBox = null;
            this.txtCreditRatio.CustomEnabled = true;
            this.txtCreditRatio.DataFieldMapping = "CREDIT_RATIO";
            this.txtCreditRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditRatio.GetRecordsOnUpDownKeys = false;
            this.txtCreditRatio.IsDate = false;
            this.txtCreditRatio.Location = new System.Drawing.Point(503, 4);
            this.txtCreditRatio.Name = "txtCreditRatio";
            this.txtCreditRatio.NumberFormat = "###,###,##0.00";
            this.txtCreditRatio.Postfix = "";
            this.txtCreditRatio.Prefix = "";
            this.txtCreditRatio.ReadOnly = true;
            this.txtCreditRatio.Size = new System.Drawing.Size(100, 20);
            this.txtCreditRatio.SkipValidation = false;
            this.txtCreditRatio.TabIndex = 67;
            this.txtCreditRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditRatio.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(116, 190);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 13);
            this.label26.TabIndex = 66;
            this.label26.Text = "Remarks:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRmks
            // 
            this.txtRmks.AllowSpace = true;
            this.txtRmks.AssociatedLookUpName = "";
            this.txtRmks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRmks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRmks.ContinuationTextBox = null;
            this.txtRmks.CustomEnabled = false;
            this.txtRmks.DataFieldMapping = "REMARKS";
            this.txtRmks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmks.GetRecordsOnUpDownKeys = false;
            this.txtRmks.IsDate = false;
            this.txtRmks.Location = new System.Drawing.Point(192, 188);
            this.txtRmks.MaxLength = 80;
            this.txtRmks.Name = "txtRmks";
            this.txtRmks.NumberFormat = "###,###,##0.00";
            this.txtRmks.Postfix = "";
            this.txtRmks.Prefix = "";
            this.txtRmks.Size = new System.Drawing.Size(411, 20);
            this.txtRmks.SkipValidation = false;
            this.txtRmks.TabIndex = 65;
            this.txtRmks.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtRmks, "Press[F10] To Print Application or [F6] To Exit .");
            this.txtRmks.Validating += new System.ComponentModel.CancelEventHandler(this.txtRmks_Validating);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(65, 164);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(124, 13);
            this.label25.TabIndex = 64;
            this.label25.Text = "Exception Remarks:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExcpRem
            // 
            this.txtExcpRem.AllowSpace = true;
            this.txtExcpRem.AssociatedLookUpName = "";
            this.txtExcpRem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExcpRem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtExcpRem.ContinuationTextBox = null;
            this.txtExcpRem.CustomEnabled = true;
            this.txtExcpRem.DataFieldMapping = "EXCP_REM";
            this.txtExcpRem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExcpRem.GetRecordsOnUpDownKeys = false;
            this.txtExcpRem.IsDate = false;
            this.txtExcpRem.Location = new System.Drawing.Point(192, 162);
            this.txtExcpRem.Name = "txtExcpRem";
            this.txtExcpRem.NumberFormat = "###,###,##0.00";
            this.txtExcpRem.Postfix = "";
            this.txtExcpRem.Prefix = "";
            this.txtExcpRem.ReadOnly = true;
            this.txtExcpRem.Size = new System.Drawing.Size(205, 20);
            this.txtExcpRem.SkipValidation = false;
            this.txtExcpRem.TabIndex = 63;
            this.txtExcpRem.TextType = CrplControlLibrary.TextType.String;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(49, 139);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(141, 13);
            this.label24.TabIndex = 62;
            this.label24.Text = "Exception Rem Option:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExcepRemOpt
            // 
            this.txtExcepRemOpt.AllowSpace = true;
            this.txtExcepRemOpt.AssociatedLookUpName = "";
            this.txtExcepRemOpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExcepRemOpt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtExcepRemOpt.ContinuationTextBox = null;
            this.txtExcepRemOpt.CustomEnabled = false;
            this.txtExcepRemOpt.DataFieldMapping = "EXP_REM_OPT";
            this.txtExcepRemOpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExcepRemOpt.GetRecordsOnUpDownKeys = false;
            this.txtExcepRemOpt.IsDate = false;
            this.txtExcepRemOpt.Location = new System.Drawing.Point(192, 137);
            this.txtExcepRemOpt.MaxLength = 1;
            this.txtExcepRemOpt.Name = "txtExcepRemOpt";
            this.txtExcepRemOpt.NumberFormat = "###,###,##0.00";
            this.txtExcepRemOpt.Postfix = "";
            this.txtExcepRemOpt.Prefix = "";
            this.txtExcepRemOpt.Size = new System.Drawing.Size(100, 20);
            this.txtExcepRemOpt.SkipValidation = false;
            this.txtExcepRemOpt.TabIndex = 61;
            this.txtExcepRemOpt.TextType = CrplControlLibrary.TextType.Integer;
            this.txtExcepRemOpt.Validating += new System.ComponentModel.CancelEventHandler(this.txtExcepRemOpt_Validating);
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(85, 111);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 13);
            this.label23.TabIndex = 60;
            this.label23.Text = "Exception Flag:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExcpFlg
            // 
            this.txtExcpFlg.AllowSpace = true;
            this.txtExcpFlg.AssociatedLookUpName = "";
            this.txtExcpFlg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExcpFlg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtExcpFlg.ContinuationTextBox = null;
            this.txtExcpFlg.CustomEnabled = false;
            this.txtExcpFlg.DataFieldMapping = "EXCP_FLAG";
            this.txtExcpFlg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExcpFlg.GetRecordsOnUpDownKeys = false;
            this.txtExcpFlg.IsDate = false;
            this.txtExcpFlg.Location = new System.Drawing.Point(192, 111);
            this.txtExcpFlg.MaxLength = 1;
            this.txtExcpFlg.Name = "txtExcpFlg";
            this.txtExcpFlg.NumberFormat = "###,###,##0.00";
            this.txtExcpFlg.Postfix = "";
            this.txtExcpFlg.Prefix = "";
            this.txtExcpFlg.Size = new System.Drawing.Size(100, 20);
            this.txtExcpFlg.SkipValidation = false;
            this.txtExcpFlg.TabIndex = 59;
            this.txtExcpFlg.TextType = CrplControlLibrary.TextType.String;
            this.txtExcpFlg.Validating += new System.ComponentModel.CancelEventHandler(this.txtExcpFlg_Validating);
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(68, 87);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(121, 13);
            this.label22.TabIndex = 58;
            this.label22.Text = "Total Month Install:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTotalMonth
            // 
            this.txtTotalMonth.AllowSpace = true;
            this.txtTotalMonth.AssociatedLookUpName = "";
            this.txtTotalMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalMonth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalMonth.ContinuationTextBox = null;
            this.txtTotalMonth.CustomEnabled = true;
            this.txtTotalMonth.DataFieldMapping = "TOT_MONTH_INST";
            this.txtTotalMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMonth.GetRecordsOnUpDownKeys = false;
            this.txtTotalMonth.IsDate = false;
            this.txtTotalMonth.Location = new System.Drawing.Point(192, 87);
            this.txtTotalMonth.Name = "txtTotalMonth";
            this.txtTotalMonth.NumberFormat = "###,###,##0.00";
            this.txtTotalMonth.Postfix = "";
            this.txtTotalMonth.Prefix = "";
            this.txtTotalMonth.ReadOnly = true;
            this.txtTotalMonth.Size = new System.Drawing.Size(100, 20);
            this.txtTotalMonth.SkipValidation = false;
            this.txtTotalMonth.TabIndex = 57;
            this.txtTotalMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalMonth.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(53, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(137, 13);
            this.label21.TabIndex = 56;
            this.label21.Text = "Disbursement Amount:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDisbursmntAmt
            // 
            this.txtDisbursmntAmt.AllowSpace = true;
            this.txtDisbursmntAmt.AssociatedLookUpName = "";
            this.txtDisbursmntAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDisbursmntAmt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDisbursmntAmt.ContinuationTextBox = null;
            this.txtDisbursmntAmt.CustomEnabled = false;
            this.txtDisbursmntAmt.DataFieldMapping = "FN_AMT_AVAILED";
            this.txtDisbursmntAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisbursmntAmt.GetRecordsOnUpDownKeys = false;
            this.txtDisbursmntAmt.IsDate = false;
            this.txtDisbursmntAmt.Location = new System.Drawing.Point(192, 59);
            this.txtDisbursmntAmt.Name = "txtDisbursmntAmt";
            this.txtDisbursmntAmt.NumberFormat = "###,###,##0.00";
            this.txtDisbursmntAmt.Postfix = "";
            this.txtDisbursmntAmt.Prefix = "";
            this.txtDisbursmntAmt.Size = new System.Drawing.Size(100, 20);
            this.txtDisbursmntAmt.SkipValidation = false;
            this.txtDisbursmntAmt.TabIndex = 55;
            this.txtDisbursmntAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDisbursmntAmt.TextType = CrplControlLibrary.TextType.String;
            this.txtDisbursmntAmt.Validating += new System.ComponentModel.CancelEventHandler(this.txtDisbursmntAmt_Validating);
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(97, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 54;
            this.label18.Text = "Cap Amount:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCapAmt
            // 
            this.txtCapAmt.AllowSpace = true;
            this.txtCapAmt.AssociatedLookUpName = "";
            this.txtCapAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCapAmt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCapAmt.ContinuationTextBox = null;
            this.txtCapAmt.CustomEnabled = true;
            this.txtCapAmt.DataFieldMapping = "AMT_OF_LOAN_CAN_AVAIL_CAP";
            this.txtCapAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapAmt.GetRecordsOnUpDownKeys = false;
            this.txtCapAmt.IsDate = false;
            this.txtCapAmt.Location = new System.Drawing.Point(192, 31);
            this.txtCapAmt.Name = "txtCapAmt";
            this.txtCapAmt.NumberFormat = "###,###,##0.00";
            this.txtCapAmt.Postfix = "";
            this.txtCapAmt.Prefix = "";
            this.txtCapAmt.ReadOnly = true;
            this.txtCapAmt.Size = new System.Drawing.Size(100, 20);
            this.txtCapAmt.SkipValidation = false;
            this.txtCapAmt.TabIndex = 53;
            this.txtCapAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCapAmt.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(42, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(148, 13);
            this.label12.TabIndex = 52;
            this.label12.Text = "Amt of Loan Can Availd:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCanBeAvail
            // 
            this.txtCanBeAvail.AllowSpace = true;
            this.txtCanBeAvail.AssociatedLookUpName = "";
            this.txtCanBeAvail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCanBeAvail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCanBeAvail.ContinuationTextBox = null;
            this.txtCanBeAvail.CustomEnabled = true;
            this.txtCanBeAvail.DataFieldMapping = "AMT_OF_LOAN_CAN_AVAIL";
            this.txtCanBeAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCanBeAvail.GetRecordsOnUpDownKeys = false;
            this.txtCanBeAvail.IsDate = false;
            this.txtCanBeAvail.Location = new System.Drawing.Point(192, 6);
            this.txtCanBeAvail.Name = "txtCanBeAvail";
            this.txtCanBeAvail.NumberFormat = "###,###,##0.00";
            this.txtCanBeAvail.Postfix = "";
            this.txtCanBeAvail.Prefix = "";
            this.txtCanBeAvail.ReadOnly = true;
            this.txtCanBeAvail.Size = new System.Drawing.Size(100, 20);
            this.txtCanBeAvail.SkipValidation = false;
            this.txtCanBeAvail.TabIndex = 51;
            this.txtCanBeAvail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCanBeAvail.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // PnlFinGrid
            // 
            this.PnlFinGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlFinGrid.ConcurrentPanels = null;
            this.PnlFinGrid.Controls.Add(this.DGVFinance);
            this.PnlFinGrid.DataManager = null;
            this.PnlFinGrid.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlFinGrid.DependentPanels = null;
            this.PnlFinGrid.DisableDependentLoad = false;
            this.PnlFinGrid.EnableDelete = true;
            this.PnlFinGrid.EnableInsert = true;
            this.PnlFinGrid.EnableQuery = false;
            this.PnlFinGrid.EnableUpdate = true;
            this.PnlFinGrid.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FinAppCommand";
            this.PnlFinGrid.Location = new System.Drawing.Point(12, 270);
            this.PnlFinGrid.MasterPanel = null;
            this.PnlFinGrid.Name = "PnlFinGrid";
            this.PnlFinGrid.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlFinGrid.Size = new System.Drawing.Size(628, 150);
            this.PnlFinGrid.SPName = "CHRIS_SP_PERSONNEL_FinApp_MANAGER";
            this.PnlFinGrid.TabIndex = 53;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(415, 13);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(66, 13);
            this.label32.TabIndex = 54;
            this.label32.Text = "User Name :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(231, 203);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(184, 13);
            this.label38.TabIndex = 57;
            this.label38.Text = "      >> Finance Type<<           ";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "FN_FIN_NO";
            this.Column1.HeaderText = "Finanace No.";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FN_INSTALL
            // 
            this.FN_INSTALL.DataPropertyName = "FN_INSTALL";
            this.FN_INSTALL.HeaderText = "Inst.";
            this.FN_INSTALL.Name = "FN_INSTALL";
            this.FN_INSTALL.ReadOnly = true;
            this.FN_INSTALL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FN_BALANCE
            // 
            this.FN_BALANCE.DataPropertyName = "FN_BALANCE";
            this.FN_BALANCE.HeaderText = "O/S Balance";
            this.FN_BALANCE.MaxInputLength = 15;
            this.FN_BALANCE.Name = "FN_BALANCE";
            this.FN_BALANCE.ReadOnly = true;
            this.FN_BALANCE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FN_LIQ_FLAG
            // 
            this.FN_LIQ_FLAG.DataPropertyName = "FN_LIQ_FLAG";
            this.FN_LIQ_FLAG.HeaderText = "";
            this.FN_LIQ_FLAG.MaxInputLength = 1;
            this.FN_LIQ_FLAG.Name = "FN_LIQ_FLAG";
            this.FN_LIQ_FLAG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_LIQ_FLAG.ToolTipText = "Press [L] = Liquidate  [R] = Reduce [A] = New  [E] = Enhancement [F3] Enter Disbu" +
                "rsement Amt";
            this.FN_LIQ_FLAG.Width = 50;
            // 
            // FN_REDUCE_AMT
            // 
            this.FN_REDUCE_AMT.DataPropertyName = "FN_REDUCE_AMT";
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.FN_REDUCE_AMT.DefaultCellStyle = dataGridViewCellStyle1;
            this.FN_REDUCE_AMT.HeaderText = "Liq. Reduce Amt";
            this.FN_REDUCE_AMT.Name = "FN_REDUCE_AMT";
            this.FN_REDUCE_AMT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FN_NEW_INST
            // 
            this.FN_NEW_INST.DataPropertyName = "FN_NEW_INST";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.FN_NEW_INST.DefaultCellStyle = dataGridViewCellStyle2;
            this.FN_NEW_INST.HeaderText = "Mnth Inst";
            this.FN_NEW_INST.Name = "FN_NEW_INST";
            this.FN_NEW_INST.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FN_NEW_OS_BAL
            // 
            this.FN_NEW_OS_BAL.DataPropertyName = "FN_NEW_OS_BAL";
            this.FN_NEW_OS_BAL.HeaderText = "O/S Balance";
            this.FN_NEW_OS_BAL.Name = "FN_NEW_OS_BAL";
            this.FN_NEW_OS_BAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_NEW_OS_BAL.Width = 112;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "ID";
            this.Column8.HeaderText = "ID";
            this.Column8.Name = "Column8";
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Visible = false;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "FN_SDATE";
            this.Column9.HeaderText = "fn_sdate";
            this.Column9.Name = "Column9";
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Visible = false;
            // 
            // FN_PAY_LEFT
            // 
            this.FN_PAY_LEFT.DataPropertyName = "FN_PAY_LEFT";
            this.FN_PAY_LEFT.HeaderText = "Column2";
            this.FN_PAY_LEFT.Name = "FN_PAY_LEFT";
            this.FN_PAY_LEFT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FN_PAY_LEFT.Visible = false;
            // 
            // CHRIS_Finance_FinanceApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 670);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.PnlFinGrid);
            this.Controls.Add(this.PnlCalAmnt);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPersonnel);
            this.Controls.Add(this.PnlFinance);
            this.Name = "CHRIS_Finance_FinanceApplication";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "ICORE CHRISTOP - Finance Finance Application";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Finance_FinanceApplication_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlFinance, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.PnlCalAmnt, 0);
            this.Controls.SetChildIndex(this.PnlFinGrid, 0);
            this.Controls.SetChildIndex(this.label32, 0);
            this.Controls.SetChildIndex(this.label39, 0);
            this.Controls.SetChildIndex(this.label38, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            this.PnlPersonnel.PerformLayout();
            this.PnlFinance.ResumeLayout(false);
            this.PnlFinance.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVFinance)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.PnlCalAmnt.ResumeLayout(false);
            this.PnlCalAmnt.PerformLayout();
            this.PnlFinGrid.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPersonnel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtPerName;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtAnnPackage;
        private CrplControlLibrary.SLTextBox txtDesg;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtCategory;
        private CrplControlLibrary.SLTextBox txtCBonus;
        private System.Windows.Forms.Label label10;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlFinance;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txtFnschdl;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txtFnratio;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtFnDesc;
        private System.Windows.Forms.Label label20;
        private CrplControlLibrary.SLDatePicker DtJoining;
        private CrplControlLibrary.SLDatePicker DtBirth;
        private CrplControlLibrary.SLDatePicker DtEndDate;
        private CrplControlLibrary.SLDatePicker DtSatartDt;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView DGVFinance;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label33;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtCurrOption;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlCalAmnt;
        private System.Windows.Forms.Label label27;
        private CrplControlLibrary.SLTextBox txtInsCovAmnt;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLTextBox txtFactor;
        private System.Windows.Forms.Label label29;
        private CrplControlLibrary.SLTextBox txtAvailable;
        private System.Windows.Forms.Label label30;
        private CrplControlLibrary.SLTextBox txtRatioAvail;
        private System.Windows.Forms.Label label31;
        private CrplControlLibrary.SLTextBox txtCreditRatio;
        private System.Windows.Forms.Label label26;
        private CrplControlLibrary.SLTextBox txtRmks;
        private System.Windows.Forms.Label label25;
        private CrplControlLibrary.SLTextBox txtExcpRem;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txtExcepRemOpt;
        private System.Windows.Forms.Label label23;
        private CrplControlLibrary.SLTextBox txtExcpFlg;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txtCapAmt;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.LookupButton lpBtnPersonnel;
        private CrplControlLibrary.LookupButton lBtnFinNo;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlFinGrid;
        private CrplControlLibrary.SLDatePicker dtFN_SDATE;
        private CrplControlLibrary.SLTextBox txtprLoanPack;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        public CrplControlLibrary.SLTextBox txtPersonnelNo;
        public CrplControlLibrary.SLTextBox txtFinanceType;
        public CrplControlLibrary.SLTextBox txtFnMrkup;
        public CrplControlLibrary.SLTextBox txtTotalMonth;
        public CrplControlLibrary.SLTextBox txtCanBeAvail;
        public CrplControlLibrary.SLTextBox txtDisbursmntAmt;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox txt_Pr_NIC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_INSTALL;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_LIQ_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_REDUCE_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_NEW_INST;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_NEW_OS_BAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn FN_PAY_LEFT;
    }
}