namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    partial class CHRIS_Finance_View_FinanceBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtFinType = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtFinanceNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.DTstartdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.DTFN_END_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.dtExtratime = new CrplControlLibrary.SLDatePicker(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.txtInstallment = new CrplControlLibrary.SLTextBox(this.components);
            this.txtaviledamt = new CrplControlLibrary.SLTextBox(this.components);
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCreditRatio = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(486, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(522, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 256);
            this.panel1.Size = new System.Drawing.Size(522, 60);
            // 
            // txtFinType
            // 
            this.txtFinType.AllowSpace = true;
            this.txtFinType.AssociatedLookUpName = "";
            this.txtFinType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinType.ContinuationTextBox = null;
            this.txtFinType.CustomEnabled = true;
            this.txtFinType.DataFieldMapping = "";
            this.txtFinType.Enabled = false;
            this.txtFinType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinType.GetRecordsOnUpDownKeys = false;
            this.txtFinType.IsDate = false;
            this.txtFinType.Location = new System.Drawing.Point(121, 3);
            this.txtFinType.MaxLength = 3;
            this.txtFinType.Name = "txtFinType";
            this.txtFinType.NumberFormat = "###,###,##0.00";
            this.txtFinType.Postfix = "";
            this.txtFinType.Prefix = "";
            this.txtFinType.Size = new System.Drawing.Size(100, 20);
            this.txtFinType.SkipValidation = false;
            this.txtFinType.TabIndex = 30;
            this.txtFinType.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "L/Fin Type :";
            // 
            // txtFinanceNo
            // 
            this.txtFinanceNo.AllowSpace = true;
            this.txtFinanceNo.AssociatedLookUpName = "";
            this.txtFinanceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinanceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFinanceNo.ContinuationTextBox = null;
            this.txtFinanceNo.CustomEnabled = true;
            this.txtFinanceNo.DataFieldMapping = "";
            this.txtFinanceNo.Enabled = false;
            this.txtFinanceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinanceNo.GetRecordsOnUpDownKeys = false;
            this.txtFinanceNo.IsDate = false;
            this.txtFinanceNo.Location = new System.Drawing.Point(121, 29);
            this.txtFinanceNo.MaxLength = 3;
            this.txtFinanceNo.Name = "txtFinanceNo";
            this.txtFinanceNo.NumberFormat = "###,###,##0.00";
            this.txtFinanceNo.Postfix = "";
            this.txtFinanceNo.Prefix = "";
            this.txtFinanceNo.Size = new System.Drawing.Size(100, 20);
            this.txtFinanceNo.SkipValidation = false;
            this.txtFinanceNo.TabIndex = 40;
            this.txtFinanceNo.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(48, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "L/Fin No. :";
            // 
            // DTstartdate
            // 
            this.DTstartdate.CustomEnabled = false;
            this.DTstartdate.CustomFormat = "dd/MM/yyyy";
            this.DTstartdate.DataFieldMapping = "";
            this.DTstartdate.Enabled = false;
            this.DTstartdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTstartdate.HasChanges = false;
            this.DTstartdate.Location = new System.Drawing.Point(121, 55);
            this.DTstartdate.Name = "DTstartdate";
            this.DTstartdate.NullValue = " ";
            this.DTstartdate.Size = new System.Drawing.Size(100, 20);
            this.DTstartdate.TabIndex = 59;
            this.DTstartdate.Value = new System.DateTime(2010, 12, 20, 13, 32, 19, 730);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(44, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 58;
            this.label12.Text = "Start Date :";
            // 
            // DTFN_END_DATE
            // 
            this.DTFN_END_DATE.CustomEnabled = false;
            this.DTFN_END_DATE.CustomFormat = "dd/MM/yyyy";
            this.DTFN_END_DATE.DataFieldMapping = "";
            this.DTFN_END_DATE.Enabled = false;
            this.DTFN_END_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTFN_END_DATE.HasChanges = false;
            this.DTFN_END_DATE.Location = new System.Drawing.Point(121, 81);
            this.DTFN_END_DATE.Name = "DTFN_END_DATE";
            this.DTFN_END_DATE.NullValue = " ";
            this.DTFN_END_DATE.Size = new System.Drawing.Size(100, 20);
            this.DTFN_END_DATE.TabIndex = 61;
            this.DTFN_END_DATE.Value = new System.DateTime(2010, 12, 20, 13, 32, 19, 730);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(37, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 60;
            this.label13.Text = "Expiry Date :";
            // 
            // dtExtratime
            // 
            this.dtExtratime.CustomEnabled = false;
            this.dtExtratime.CustomFormat = "dd/MM/yyyy";
            this.dtExtratime.DataFieldMapping = "";
            this.dtExtratime.Enabled = false;
            this.dtExtratime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExtratime.HasChanges = false;
            this.dtExtratime.Location = new System.Drawing.Point(121, 107);
            this.dtExtratime.Name = "dtExtratime";
            this.dtExtratime.NullValue = " ";
            this.dtExtratime.Size = new System.Drawing.Size(100, 20);
            this.dtExtratime.TabIndex = 63;
            this.dtExtratime.Value = new System.DateTime(2010, 12, 20, 13, 32, 19, 730);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(42, 111);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 62;
            this.label19.Text = "Extra Time :";
            // 
            // txtInstallment
            // 
            this.txtInstallment.AllowSpace = true;
            this.txtInstallment.AssociatedLookUpName = "";
            this.txtInstallment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInstallment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInstallment.ContinuationTextBox = null;
            this.txtInstallment.CustomEnabled = true;
            this.txtInstallment.DataFieldMapping = "";
            this.txtInstallment.Enabled = false;
            this.txtInstallment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstallment.GetRecordsOnUpDownKeys = false;
            this.txtInstallment.IsDate = false;
            this.txtInstallment.Location = new System.Drawing.Point(121, 157);
            this.txtInstallment.Name = "txtInstallment";
            this.txtInstallment.NumberFormat = "###,###,##0.00";
            this.txtInstallment.Postfix = "";
            this.txtInstallment.Prefix = "";
            this.txtInstallment.Size = new System.Drawing.Size(100, 20);
            this.txtInstallment.SkipValidation = false;
            this.txtInstallment.TabIndex = 67;
            this.txtInstallment.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtaviledamt
            // 
            this.txtaviledamt.AllowSpace = true;
            this.txtaviledamt.AssociatedLookUpName = "";
            this.txtaviledamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaviledamt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtaviledamt.ContinuationTextBox = null;
            this.txtaviledamt.CustomEnabled = true;
            this.txtaviledamt.DataFieldMapping = "FN_AMT_AVAILED";
            this.txtaviledamt.Enabled = false;
            this.txtaviledamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaviledamt.GetRecordsOnUpDownKeys = false;
            this.txtaviledamt.IsDate = false;
            this.txtaviledamt.Location = new System.Drawing.Point(121, 133);
            this.txtaviledamt.Name = "txtaviledamt";
            this.txtaviledamt.NumberFormat = "###,###,##0.00";
            this.txtaviledamt.Postfix = "";
            this.txtaviledamt.Prefix = "";
            this.txtaviledamt.Size = new System.Drawing.Size(100, 20);
            this.txtaviledamt.SkipValidation = false;
            this.txtaviledamt.TabIndex = 66;
            this.txtaviledamt.TextType = CrplControlLibrary.TextType.String;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(16, 159);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 13);
            this.label22.TabIndex = 65;
            this.label22.Text = "Installment Amt :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(20, 137);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 13);
            this.label21.TabIndex = 64;
            this.label21.Text = "Amount Taken :";
            // 
            // txtCreditRatio
            // 
            this.txtCreditRatio.AllowSpace = true;
            this.txtCreditRatio.AssociatedLookUpName = "";
            this.txtCreditRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditRatio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreditRatio.ContinuationTextBox = null;
            this.txtCreditRatio.CustomEnabled = true;
            this.txtCreditRatio.DataFieldMapping = "FN_AMT_AVAILED";
            this.txtCreditRatio.Enabled = false;
            this.txtCreditRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditRatio.GetRecordsOnUpDownKeys = false;
            this.txtCreditRatio.IsDate = false;
            this.txtCreditRatio.Location = new System.Drawing.Point(121, 183);
            this.txtCreditRatio.Name = "txtCreditRatio";
            this.txtCreditRatio.NumberFormat = "###,###,##0.00";
            this.txtCreditRatio.Postfix = "";
            this.txtCreditRatio.Prefix = "";
            this.txtCreditRatio.Size = new System.Drawing.Size(100, 20);
            this.txtCreditRatio.SkipValidation = false;
            this.txtCreditRatio.TabIndex = 69;
            this.txtCreditRatio.TextType = CrplControlLibrary.TextType.String;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(20, 185);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 13);
            this.label24.TabIndex = 68;
            this.label24.Text = "Credit Ratio % :";
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.txtFinanceNo);
            this.slPanelSimple1.Controls.Add(this.label8);
            this.slPanelSimple1.Controls.Add(this.txtFinType);
            this.slPanelSimple1.Controls.Add(this.label4);
            this.slPanelSimple1.Controls.Add(this.txtCreditRatio);
            this.slPanelSimple1.Controls.Add(this.label12);
            this.slPanelSimple1.Controls.Add(this.label24);
            this.slPanelSimple1.Controls.Add(this.DTstartdate);
            this.slPanelSimple1.Controls.Add(this.txtInstallment);
            this.slPanelSimple1.Controls.Add(this.label13);
            this.slPanelSimple1.Controls.Add(this.txtaviledamt);
            this.slPanelSimple1.Controls.Add(this.DTFN_END_DATE);
            this.slPanelSimple1.Controls.Add(this.label22);
            this.slPanelSimple1.Controls.Add(this.label19);
            this.slPanelSimple1.Controls.Add(this.label21);
            this.slPanelSimple1.Controls.Add(this.dtExtratime);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = true;
            this.slPanelSimple1.EnableInsert = true;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = true;
            this.slPanelSimple1.EntityName = null;
            this.slPanelSimple1.Location = new System.Drawing.Point(12, 39);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(493, 211);
            this.slPanelSimple1.SPName = null;
            this.slPanelSimple1.TabIndex = 70;
            // 
            // CHRIS_Finance_View_FinanceBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(522, 316);
            this.Controls.Add(this.slPanelSimple1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CHRIS_Finance_View_FinanceBooking";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Shown += new System.EventHandler(this.CHRIS_Finance_View_FinanceBooking_Shown);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrplControlLibrary.SLTextBox txtFinType;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtFinanceNo;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLDatePicker DTstartdate;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLDatePicker DTFN_END_DATE;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLDatePicker dtExtratime;
        private System.Windows.Forms.Label label19;
        private CrplControlLibrary.SLTextBox txtInstallment;
        private CrplControlLibrary.SLTextBox txtaviledamt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private CrplControlLibrary.SLTextBox txtCreditRatio;
        private System.Windows.Forms.Label label24;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
    }
}