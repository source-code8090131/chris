using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinanceTypeEntry : ChrisMasterDetailForm
    {
        public CHRIS_Finance_FinanceTypeEntry()
        {
            InitializeComponent();
        }



        public CHRIS_Finance_FinanceTypeEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(pnlTblSubType);
            this.pnlDetail.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlDetail);

           


        }

   

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            //tbtSave.Enabled = false;


          
            //tbtDelete.Visible = false;
            //tbtCancel.Enabled = false;
            tbtSave.Available = false;
            tbtDelete.Available = true;
            tbtList.Available = false;
            tbtDelete.Enabled = false;
            this.txtUserName.Text = "User Name: " + this.UserName;
            txtFN_SUBTYPE.Width = 120;
            txtFN_SUBDESC.Width = 120;
            pnlTblSubType.Enabled = false;
            pnlNavigation.Visible = false;
            this.FunctionConfig.EnableF8 = false;
            txtDescription.CharacterCasing = CharacterCasing.Normal;
        
  
        }

        protected override bool Add()
        {
             base.Add();
             this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
             this.lbtnPersonnal.SkipValidationOnLeave = true;
             tbtDelete.Enabled = false;
             return false;

        }
        //protected override bool Quit()
        //{
        //    base.Cancel();
        //    base.Quit();
           
        //    return false;

        //}

        protected override bool Edit()
        {
             base.Edit();
             this.lbtnPersonnal.SkipValidationOnLeave = false;
             this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
             tbtDelete.Enabled = false;

             return false;
        }
        protected override bool View()
        {
            base.View();
            this.lbtnPersonnal.SkipValidationOnLeave = false;
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
            this.txtOption.Text = "V";
            tbtDelete.Enabled = false;

            return false;
        }

        protected override bool Delete()
        {
            base.Delete();
            this.lbtnPersonnal.SkipValidationOnLeave = false;
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            tbtDelete.Enabled = false;

            return false;
        }
     

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            bool IsAdd = false;
            bool IsView = false;
            bool IsDelete = false;

            if (actionType == "Cancel")
            {

                this.txtFinType.Text = "";

                base.DoToolbarActions(ctrlsCollection, actionType);

                this.txtUser.Text = this.userID;
                this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");

                this.txtUserName.Text = "User Name: " + this.UserName;
                txtOption.Focus();
                return;
            }

            if (actionType == "List")
            {
                if (this.FunctionConfig.CurrentOption == Function.None)
                {
                    return;
                }
                if (this.FunctionConfig.CurrentOption == Function.Add)
                {
                    IsAdd = true;
                }
                if (this.FunctionConfig.CurrentOption == Function.View)
                {
                    IsView = true;
                }
                if (this.FunctionConfig.CurrentOption == Function.Delete)
                {
                    IsDelete = true;
                }

             
            }
            if (actionType == "Delete")
            {

                if (!(this.ActiveControl is SLDataGridView))
                {
                    
                    return ;
                }
            }
           
          
            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "List")
            {

                if (this.FunctionConfig.CurrentOption == Function.Modify && IsView==true)
                {
                   this.View();
                }
                if (this.FunctionConfig.CurrentOption == Function.Modify && IsAdd==true)

                {
                    txtOption.Focus();
                    this.Cancel();
                }
                if (this.FunctionConfig.CurrentOption == Function.Modify && IsDelete == true)
                {
                    this.Cancel();
                }

            }
           
            


         
        }



        private DataTable getFinanceTypes()
        {

            DataTable dt = null;

            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();

            colsNVals.Add("FN_TYPE", txtFinType.Text);



            rsltCode = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "GetFinanceTypes", colsNVals);

            if (rsltCode.isSuccessful)
            {

                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {

                    dt = rsltCode.dstResult.Tables[0];

                }

            }

            return dt;

        }


      

        private void txtEligibilty_Validated(object sender, EventArgs e)
        {
            if (txtEligibilty.Text.Length== 0 || txtEligibilty.Text== "0")
            {

                txtConfirm.Text = "Y";
            }
            if (this.FunctionConfig.CurrentOption == Function.Modify)
            {
                txtFN_EX_LESS.Focus();

            }
        }

       


        private void Reset()
        {
            this.txtFinType.Text = "";
            this.txtDescription.Text = "";
            this.txtMarkup.Text = "";
            this.txtfnCRatio.Text = "";
            this.txtEligibilty.Text = "";
            this.txtConfirm.Text = "";
            this.txtFN_SCHEDULE.Text = "";
            this.txtFN_EX_LESS.Text = "";
            this.pnlNavigation.Visible = false;
            this.txtFN_EX_GREAT.Text = "";
            this.txtFN_LESS_AMT.Text = "";
            base.ClearForm(this.pnlTblSubType.Controls);
            dtGridSubType.DataSource = null;
           

        }

        private void txtFinType_Validating(object sender, CancelEventArgs e)
        {
            tbtDelete.Enabled = false;
            if (txtFinType.Text != string.Empty)
            {
                DataTable dt = null;
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();

                Result rsltCode;

                CmnDataManager cmnDM = new CmnDataManager();
                colsNVals.Add("FN_TYPE", txtFinType.Text);


                rsltCode = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "GetFinanceTypes", colsNVals);

                if (rsltCode.isSuccessful)
                {

                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {

                        dt = rsltCode.dstResult.Tables[0];

                    }

                }
                if (this.FunctionConfig.CurrentOption== Function.Add)
                {

                    if (dt != null)
                    {

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox.Show("Record already entered...");
                            e.Cancel = true;
                            this.Reset();
                           
                            txtFinType.Focus();

                            return;

                        }
                      

                    }
                    else
                    {
                        txtDescription.Focus();

                    }





                }
                else
                {
                    if (dt != null)
                    {
                        if (dt.Rows.Count == 0)
                        {

                            MessageBox.Show("Record not present...");
                            this.Cancel();
                            txtOption.Focus();
                            return;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Record not present...");
                        this.Cancel();
                        txtOption.Focus();
                        return;
                    }

                }


                if (this.FunctionConfig.CurrentOption != Function.Add)
                {
                    if (this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.View)
                    {

                        if (this.FunctionConfig.CurrentOption == Function.View)
                        {
                            if (MessageBox.Show("Do You Want to view more records   [Y/N]", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                this.Reset();
                                txtFinType.Focus();

                                return;

                            }
                            else
                            {
                                this.Reset();
                                txtOption.Focus();
                                return;

                            }
                        }


                    }

                    if (this.FunctionConfig.CurrentOption == Function.Delete)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to Delete the record [Y/N]..", "Note"
                                , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {

                            if (DeleteSubTypeExists())
                            {
                                CustomDelete();
                                this.Reset();
                                this.Cancel();
                                txtOption.Focus();
                                //call save
                                return;
                            }
                            else
                            {
                                MessageBox.Show("RECORD CAN NOT BE DELETED, SUBTYPE EXISTS ...");
                                this.Cancel();
                                txtOption.Focus();
                                return;

                            }

                        }
                        else if (dRes == DialogResult.No)
                        {
                            this.Reset();
                            this.Cancel();
                            txtOption.Focus();

                            return;
                        }

                    }
                }

            }
            tbtDelete.Enabled = false;

        }


    
        private void lbtnCode_MouseDown(object sender, MouseEventArgs e)
        {

            LookupButton btn = (LookupButton)sender;
            ///Code by FAIsal Iqbal
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                btn.Enabled = false;
                btn.Enabled = true;

            }
        }

        private void CHRIS_Finance_FinanceTypeEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "FinanceTypeLov")
            {

                //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand)this.pnlDetail.CurrentBusinessEntity;
                //ent.FN_TYPE = txtFinType.Text;
                tbtDelete.Enabled = false;
                if (this.FunctionConfig.CurrentOption == Function.Add)
                {
                    this.AddReset();
                }
            }
        }

        private void AddReset()
        {
            
            this.txtDescription.Text = "";
            this.txtMarkup.Text = "";
            this.txtfnCRatio.Text = "";
            this.txtEligibilty.Text = "";
            this.txtConfirm.Text = "";
            this.txtFN_SCHEDULE.Text = "";
            this.txtFN_EX_LESS.Text = "";

            this.txtFN_EX_GREAT.Text = "";
            this.txtFN_LESS_AMT.Text = "";
            base.ClearForm(this.pnlTblSubType.Controls);
            dtGridSubType.DataSource = null;
            


        }

    

        private void txtFN_LESS_AMT_Validating(object sender, CancelEventArgs e)
        {
          //  this.pnlNavigation.Visible = true;


        }

        private void ShortCutKey_Press(object sender, KeyEventArgs e)
        {
           
            if (e.Modifiers == Keys.Control)
            {
                switch (e.KeyValue)
                {
                    case 34:

                        pnlTblSubType.Enabled = true;
                        pnlDetail.Enabled = false;
                        pnlHead.Enabled = false;
                    
                        pnlNavigation.Enabled = false;
                        txtOption.Enabled = false;
                        lblNavigation.Text = "[Ctrl+PgDn]=Navigate out of Sub-Type Block";
                        tbtCancel.Enabled = false;
                        tbtList.Enabled = false;
                        this.FunctionConfig.EnableF6 = false;
                        pnlTblSubType.Focus();
                        dtGridSubType.Focus();
                        if (this.FunctionConfig.CurrentOption == Function.Modify)
                        {
                            if (dtGridSubType.Rows.Count > 1)
                            {

                                tbtDelete.Enabled = true;
                                pnlDetail.EnableDelete = false;
                            }
                        }
                        break;

                }

            }

         
          
        }

        private bool CheckFirstLetter()
        {
            char testChar='0';
            bool returnValue = false;
           Char.TryParse(txtDescription.Text.Substring(0,1),out testChar);
            if (testChar != '0')
            {
                if (char.IsLetter(testChar))
                {
                    txtMarkup.Focus();
                }
                else
                {
                    returnValue = true;
                }
            }
            return returnValue;

        }

        private void txtFN_EX_GREAT_KeyDown(object sender, KeyEventArgs e)
        {
            ShortCutKey_Press(sender, e);

        }

        private void dtGridSubType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                switch (e.KeyValue)
                {
                    case 34:

                        pnlTblSubType.Enabled = false;
                        
                        pnlDetail.Enabled = true;
                        pnlHead.Enabled = true;

                        pnlNavigation.Enabled = true;
                        txtOption.Enabled = true;
                        lblNavigation.Text = "[Ctrl+PgDn]=Navigate to Sub-Type Block";
                        pnlNavigation.Visible = false;
                        tbtCancel.Enabled = true;
                        tbtList.Enabled = true;
                        this.FunctionConfig.EnableF6 = true;
                        txtFN_EX_GREAT.Focus();

                        tbtDelete.Enabled = false;
                        this.FunctionConfig.CurrentOption = Function.Modify;
                        this.pnlDetail.EnableDelete = true;
                        break;

                }

            }
           

           
             
        }

        private bool DeleteSubTypeExists()
        {

                DataTable dt = null;
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();

                Result rsltCode;
                bool returnValue = true;
                CmnDataManager cmnDM = new CmnDataManager();
                colsNVals.Add("FN_TYPE", txtFinType.Text);


                rsltCode = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "SUBTYPE_EXISTS", colsNVals);

                if (rsltCode.isSuccessful)
                {

                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {

                        dt = rsltCode.dstResult.Tables[0];

                    }

                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        returnValue = false;
                    }
                }


                return returnValue;
        }

     


  

        private void txtFN_EX_GREAT_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Keys st = (Keys.Shift | Keys.Tab);
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                        , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            if (txtFinType.Text != string.Empty)
                            {
                                f_Type.Text = txtFinType.Text;
                            }

                            CallReport("AUPR", "OLD");

                            base.DoToolbarActions(this.Controls, "Save");

                           
                            CallReport("AUP0", "NEW");
                            this.Reset();
                            //call save
                            return;

                        }
                        else if (dRes == DialogResult.No)
                        {
                           
                            this.pnlNavigation.Visible = false;
                            txtOption.Select();
                            txtOption.Focus();
                            base.DoToolbarActions(this.Controls, "Cancel");

                            return;
                        }

                    }
                }

            }
            else
            {
            if (e.KeyData == st)
                {

                    this.pnlNavigation.Visible = false;
                }
            }
        }

        private void txtDescription_Validating(object sender, CancelEventArgs e)
        {
            if (txtDescription.Text != string.Empty)
            {
                bool returnValue = false;
                if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                {

                    returnValue = CheckFirstLetter();
                    if (returnValue)
                    {

                        e.Cancel = true;
                        MessageBox.Show("First letter of description should be a character...");
                        txtDescription.Focus();
                    }
                }
            }
        }

        private void txtFN_LESS_AMT_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Tab)
                {
                    this.pnlNavigation.Visible = true;
                }
            }
           
        }

        private void txtfnCRatio_Validating(object sender, CancelEventArgs e)
        {
            double creditRatio = 0.0;
            string[] splistNo;
            string integerPart = "";
            string decimalPart = "";
            if (txtfnCRatio.Text != string.Empty)
            {
                creditRatio = double.Parse(txtfnCRatio.Text);
                //Math.Truncate(creditRatio);
                if(txtfnCRatio.Text.Contains("."))
                {
                splistNo = txtfnCRatio.Text.Split('.');
                if (splistNo.Length == 2)
                {
                    integerPart = splistNo[0];
                    decimalPart = splistNo[1];
                    if (integerPart.Length > 3 || decimalPart.Length > 2)
                    {

                        e.Cancel = true;
                        MessageBox.Show("Field must be of form 999.99");
                        txtfnCRatio.Focus();
                    }

                }
                }

            }

        }



        private void CustomDelete()
        {
            int ID = 0;

                 Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();

                iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.FINTypeCommand)this.pnlDetail.CurrentBusinessEntity;
                ID = ent.ID;
                if (ID > 0)
                {
                    colsNVals.Clear();
                    colsNVals.Add("ID", ID);

                    rsltCode = cmnDM.Execute("CHRIS_SP_FIN_TYPE_DELETE", "TYPE_DELETE", colsNVals);

                    if (rsltCode.isSuccessful)
                    {


                        MessageBox.Show("Record deleted successfully");
                        return;
                    }
                }

            
        }

        private void dtGridSubType_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (pnlDetail.Enabled == false)
            {
                if (tbtCancel.Enabled)
                {
                    tbtCancel.Enabled = false;
                    return;
                }
            }
        }

        private void dtGridSubType_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //if (tbtCancel.Enabled)
            //{
            //    tbtCancel.Enabled = false;
            //    return;
            //}
        }

        private void dtGridSubType_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (pnlDetail.Enabled == false)
            {
                if (tbtCancel.Enabled)
                {
                    tbtCancel.Enabled = false;
                    return;
                }
            }
        }


        ///// </summary>
        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToString();
            pnl.Controls.Add(txtDT);


            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "f_type";
            txtfinNo.Text = f_Type.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT01A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }


       
        

      

       
      

     
       

    }
}