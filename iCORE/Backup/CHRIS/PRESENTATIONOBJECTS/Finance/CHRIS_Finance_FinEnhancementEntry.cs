using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Setup;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinEnhancementEntry : ChrisSimpleForm
    {

        private double DDebit;
        private double CCredit;
        private double OPayLeft;
        private double BBal;
        private double wBBal;
        private double Sym1;
        private double Sym2;
        private double Sym3;
        private DateTime lPayGenDate;
        public int MARKUP_RATE_DAYS = -1;


        iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput frm1 = null;
        public CHRIS_Finance_FinEnhancementEntry()
        {
            InitializeComponent();
        }

        public CHRIS_Finance_FinEnhancementEntry(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        protected override bool VerifyMarkUpRate()
        {
            CmnDataManager cmnDM = new CmnDataManager();
            bool isMarkUp_Rate_Valid = false;
            isMarkUp_Rate_Valid = cmnDM.SetMarkUp_Rate("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", ref MARKUP_RATE_DAYS);
            if (!isMarkUp_Rate_Valid)
            {
                MessageBox.Show(ApplicationMessages.MARKUPRATEDAYS);
                return false;
            }
            else
                return true;
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.txtUser.Text = this.userID;
            this.txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = false;
            tbtDelete.Enabled = false;
            tbtList.Visible = false;
            tbtAdd.Visible = false;
            tbtSave.Visible = false;
            tbtDelete.Visible = false;
           

            this.txtUserName.Text = "User Name: " +this.UserName;

        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Cancel")
            {
                this.txtPersonnalNo.Text = "";
                this.txtFinType.Text = "";
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }


        protected override bool Add()
        {


            base.Add();

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add;
            return false;
        }

        protected override bool Edit()
        {
            base.Edit();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;

        }

        protected override bool Delete()
        {
            base.Delete();
            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Edit;
            return false;
        }

        protected override bool View()
        {
            base.View();

            this.operationMode = iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View;
            return false;
        }


        private void CHRIS_Finance_FinEnhancementEntry_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
          
           
         //   if (actionType == "FinTypeLov")
         //   {
         //       Dictionary<string, object> paramDummy = new Dictionary<string, object>();
         //        paramDummy.Add("PR_P_NO", txtPersonnalNo.Text);
         //        paramDummy.Add("FN_FIN_NO", txtFinanceNo.Text);
         //        paramDummy.Add("FN_TYPE", txtFinType.Text);
         //        DataTable dtDummy = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthDummy", paramDummy);
         //        if (dtDummy != null)
         //        {

         //            if (dtDummy.Rows.Count > 0)
         //            {


         //                Dictionary<string, object> param = new Dictionary<string, object>();
         //                param.Add("PR_P_NO", txtPersonnalNo.Text);
         //                param.Add("FN_FIN_NO", txtFinanceNo.Text);

         //                DataTable dtFinFnMonth = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthQuery", param);
         //                //if (dtFinFnMonth != null)
         //                //{
         //                //    if (dtFinFnMonth.Rows.Count > 0)
         //                //    {
         //                //        bool aSym1 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[0].ToString(), out Sym1);
         //                //        bool aSym2 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[1].ToString(), out Sym2);
         //                //        bool aSym3 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[2].ToString(), out Sym3);
         //                //        if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
         //                //        {
         //                //            if (Sym1 != 0 || Sym2 == 0 || Sym3 <= 0)
         //                //            {
         //                //                MessageBox.Show(" ENHANCEMENT HAS NOT BEEN DONE LATELY .YOU CANT");
                                        
         //                //                txtOption.Focus();
         //                //                return;

         //                //            }
         //                //        }
                                 
         //                //    }
         //                //}

         //                 Dictionary<string, object> paramBooking = new Dictionary<string, object>();
         //                paramBooking.Add("FN_FIN_NO", txtFinanceNo.Text);
         //                DataTable dtFinFnBooking = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthBookingValues", paramBooking);
         //                if (dtFinFnBooking != null)
         //                {
         //                    if (dtFinFnMonth.Rows.Count > 0)
         //                    {
         //                        DTstartdate.Value = DateTime.Parse(dtFinFnBooking.Rows[0].ItemArray[0].ToString());
         //                        DTFN_END_DATE.Value = DateTime.Parse(dtFinFnBooking.Rows[0].ItemArray[1].ToString());
         //                        if (dtFinFnBooking.Rows[0].ItemArray[2].ToString() != string.Empty)
         //                        {
         //                            dtExtratime.Value = DateTime.Parse(dtFinFnBooking.Rows[0].ItemArray[2].ToString() );
         //                        }
                                 
         //                    }
         //                }


         //                if (this.operationMode == iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.Add)
         //                {
         //                    Dictionary<string, object> param2 = new Dictionary<string, object>();
         //                    param2.Add("FN_FIN_NO", txtFinanceNo.Text);
         //                    DataTable dt2 = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayGenDate", param2);
         //                    if (dt2 != null)
         //                    {
         //                        if (dt2.Rows.Count > 0)
         //                        {

         //                            lPayGenDate = Convert.ToDateTime(dt2.Rows[0].ItemArray[0].ToString());
         //                            DTPAY_GEN_DATE.Value = lPayGenDate;

         //                        }

         //                    }


         //                    Dictionary<string, object> param1 = new Dictionary<string, object>();
         //                    param1.Add("FN_FIN_NO", txtFinanceNo.Text);
         //                    param1.Add("PR_P_NO", txtPersonnalNo.Text);
         //                    param1.Add("FN_TYPE", txtFinType.Text);
         //                    DataTable dt1 = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthValues", param1);

         //                    if (dt1 != null)
         //                    {
         //                        if (dt1.Rows.Count > 0)
         //                        {

         //                            //lFnMdate = null;
         //                            dtEnhancementDate.Value = null;
         //                            DDebit = double.Parse(dt1.Rows[0]["lFN_DEBIT"].ToString());
         //                            CCredit = double.Parse(dt1.Rows[0]["lFN_CREDIT"].ToString());
         //                            OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
         //                            BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
         //                            txtfnBalance.Text = BBal.ToString();
         //                            txtFnPAyLeft.Text = OPayLeft.ToString();
         //                        }

         //                    }

         //                }

         //                else
         //                {
         //                    Dictionary<string, object> param1 = new Dictionary<string, object>();
         //                    param1.Add("FN_FIN_NO", txtFinanceNo.Text);
         //                    param1.Add("PR_P_NO", txtPersonnalNo.Text);
         //                    param1.Add("FN_TYPE", txtFinType.Text);
         //                    DataTable dt1 = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthValues", param1);

         //                    if (dt1 != null)
         //                    {
         //                        if (dt1.Rows.Count > 0)
         //                        {

         //                            //lFnMdate = null;
         //                            dtEnhancementDate.Value = null;
         //                            DDebit = double.Parse(dt1.Rows[0]["lFN_DEBIT"].ToString());
         //                            BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
         //                            OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
         //                            wBBal = double.Parse(dt1.Rows[0]["lFN_DEBIT"].ToString());
         //                            txtfnBalance.Text = BBal.ToString();
         //                            txtFnPAyLeft.Text = OPayLeft.ToString();


         //                        }

         //                    }
         //                    Dictionary<string, object> paramGenDate = new Dictionary<string, object>();
         //                    paramGenDate.Add("FN_FIN_NO", txtFinanceNo.Text);
         //                    DataTable dtPayGenDate = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayGenDate", paramGenDate);
         //                    if (dtPayGenDate != null)
         //                    {
         //                        if (dtPayGenDate.Rows.Count > 0)
         //                        {

         //                            lPayGenDate = Convert.ToDateTime(dtPayGenDate.Rows[0].ItemArray[0].ToString());
         //                            DTPAY_GEN_DATE.Value = lPayGenDate;

         //                        }

         //                    }

         //                    Dictionary<string, object> paramMatDate = new Dictionary<string, object>();
         //                    paramMatDate.Add("FN_FIN_NO", txtFinanceNo.Text);
         //                    paramMatDate.Add("PR_P_NO", txtPersonnalNo.Text);
         //                    paramMatDate.Add("FN_TYPE", txtFinType.Text);
         //                    DataTable dtFinPayMatDate = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayMatDate", paramMatDate);
         //                    if (dtFinPayMatDate != null)
         //                    {
         //                        if (dtFinPayMatDate.Rows.Count > 0)
         //                        {

         //                            lPayGenDate = Convert.ToDateTime(dtFinPayMatDate.Rows[0].ItemArray[0].ToString());
         //                            dtEnhancementDate.Value = lPayGenDate;

         //                        }

         //                    }

         //                    EnhancementDateValidate();
         //                    txtENHAN_AMT.Focus();
         //                     /* go_field('FN_MONTH.FN_MDATE');
         //execute_trigger('key-nxtfld');
         //GO_FIELD('ENHAN_AMT');
         //proc_2;*/

                            


         //                }

         //            }
         //        }

         //   }
        }


        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;
            
        }

        
        private DataTable GetDataObject(string sp, string actionType)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;
            
        }

        private void dtEnhancementDate_Validating(object sender, CancelEventArgs e)
        {
            double val3 = 0.0;
            int last_Day = 0;
            double val4 = 0.0;
             int transferCount = 0;
             if (txtpr_transfer.Text != string.Empty)
             {
                 int.TryParse(txtpr_transfer.Text, out transferCount);
             }
            if(dtEnhancementDate.Value !=null)

            {
                   DateTime dtDate1 = Convert.ToDateTime(DTPAY_GEN_DATE.Value);//dat2
                   DateTime dtDate2 = Convert.ToDateTime(dtEnhancementDate.Value);
                   if (DateTime.Compare(dtDate2.Date, dtDate1.Date) <= 0 && transferCount < 6)
                  {
                      e.Cancel = true;
                  
                    MessageBox.Show("DATE HAS TO BE GREATER THAN PAYROLL DATE");
                    dtEnhancementDate.Focus();
                    return;

              
                }
                  else
                  {
                      DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if ((!(dtDate2 >= dtDate1 && dtDate2 <= dt5)) && transferCount < 6)
                    {
                        MessageBox.Show("DATE HAS TO BE THE WITHIN THE MONTH AFTER PAYROLL DATE");
                        dtEnhancementDate.Focus();
                        e.Cancel = true;
                        return;
                    }
                       else{
                            if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                            {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);

                            }

                            else
                            
                            {
                                
                                last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;



                            }


                       }

                       if (val3 > 30 && transferCount < 6)
                        {
                            MessageBox.Show("DATE HAS TO BE WITHIN THE MONTH AFTER PAYROLL DATE");
                            dtEnhancementDate.Focus();
                            e.Cancel = true;
                            return;
                        }

                        txtVal3.Text = val3.ToString();
  
    //go_block('blk_body');
                        PROC_1();
                  }

            }
            else
            {
                             
                   
                    MessageBox.Show("DATE HAS TO BE GREATER THAN PAYROLL DATE");
                    dtEnhancementDate.Focus();
                    e.Cancel = true;
                    return;

            }
          
        }



        private void EnhancementDateValidate()
        {

            double val3 = 0;
            int last_Day = 0;
            double val4 = 0;
            int transferCount = 0;
            if(txtpr_transfer.Text !=string.Empty)
            {
                int.TryParse(txtpr_transfer.Text, out transferCount);
            }
            if (dtEnhancementDate.Value != null)
            {
                DateTime dtDate1 = Convert.ToDateTime(DTPAY_GEN_DATE.Value);//dat2
                DateTime dtDate2 = Convert.ToDateTime(dtEnhancementDate.Value);
                if (DateTime.Compare(dtDate2.Date, dtDate1.Date) <= 0 && transferCount < 6)
                {
                   
                    MessageBox.Show("DATE HAS TO BE GREATER THAN PAYROLL DATE");
                    dtEnhancementDate.Focus();
                    return;

                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if ((!(dtDate2 >= dtDate1 && dtDate2 <= dt5)) && transferCount < 6)
                    {
                        MessageBox.Show("DATE HAS TO BE THE WITHIN THE MONTH AFTER PAYROLL DATE");
                        dtEnhancementDate.Focus();
                        return;
                    }
                    else
                    {
                        if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                        {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);

                        }

                        else
                        {

                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;



                        }


                    }

                    if (val3 > 30 && transferCount < 6)
                    {
                        MessageBox.Show("DATE HAS TO BE WITHIN THE MONTH AFTER PAYROLL DATE");
                        dtEnhancementDate.Focus();
                        return;
                    }

                    txtVal3.Text = val3.ToString();

                    //go_block('blk_body');
                    PROC_1();
                }

            }
            else
            {

               
                MessageBox.Show("DATE HAS TO BE GREATER THAN PAYROLL DATE");
                dtEnhancementDate.Focus();
                return;

            }
          
        }


        /*** This Procedure Checks If House loan Has Been Taken ***/
        private void PROC_3(string FinT)
        {
            if (txtFinType.Text != string.Empty)
            {
                if (txtFinType.Text.Length > 0)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FinType", FinT.Substring(0, 1));
                    DataTable dt = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "HouseLoanEntryCheck", param);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            MessageBox.Show("More than One House Loan Entries");
                        }
                    }
                }
            }

        }



        private void PROC_1()
        {

            double lCREDIT_RATIO;
            double lCREDIT_RATIO_MONTH;
            int lCOUNTER;
            double lLESS_RATIO_AVAILED;
            double lAVAILABLE_RATIO;
            double lFACTOR=0;
            double lAMT_OF_LOAN_CAN_AVAIL=0;
            double lTOTAL_MONTHLY_INSTALLMENT;
            int CREDIT_RATIO_PER;
            int AUX1;
            double AUX2;
            double AUX3;
            int AUX4;
            int AUX5;
            int AUX7;
            double AUX8;
            double AUX9;
            double dis_repay;

            AUX2 = 0;
            AUX7 = 0;
            /// Monthly Deduction in AUX2
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnalNo.Text);
            DataTable dtMonthlyDeduction = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "MonthlyDeduction", param);
            if (dtMonthlyDeduction != null && dtMonthlyDeduction.Rows.Count > 0)
            {
                AUX2 = double.Parse(dtMonthlyDeduction.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtMonthlyDeduction.Rows[0].ItemArray[0].ToString());
            }

             AUX9 = double.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text);

             DataTable dtfnBookCount = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnBookingProcCount", param);
             if (dtfnBookCount != null)
             {
                 if (dtfnBookCount.Rows.Count > 1)
                 {
                     this.PROC_3(this.txtFinType.Text);
                 }
             }

             lCREDIT_RATIO = (AUX9 / 100) * double.Parse(txtfnCRatio.Text == "" ? "0" : txtfnCRatio.Text);
                lCREDIT_RATIO_MONTH = lCREDIT_RATIO / 12;

                lCREDIT_RATIO_MONTH = Math.Round(lCREDIT_RATIO_MONTH, 2, MidpointRounding.AwayFromZero); 
                lLESS_RATIO_AVAILED = AUX2;

                lAVAILABLE_RATIO = lCREDIT_RATIO_MONTH - lLESS_RATIO_AVAILED;

                if (txtfnMarkup.Text != string.Empty && txtfnMarkup.Text != "0")
                {
                    AUX8 = double.Parse(txtfnMarkup.Text == "" ? "0" : txtfnMarkup.Text) / 1200;
                    AUX8 = AUX8 + 1;
                    AUX3 = Convert.ToDouble(Math.Pow(AUX8,- double.Parse(txtFnPAyLeft.Text == "" ? "0" : txtFnPAyLeft.Text)));

                    lFACTOR = (AUX8 - 1) / (1 - AUX3) * 1000;
                }
                else
                {
                    if (txtFactor.Text != "0")
                    {
                        if (txtFnPAyLeft.Text != string.Empty)
                        {
                            lFACTOR = 1000 / double.Parse(txtFnPAyLeft.Text == "" ? "0" : txtFnPAyLeft.Text);
                        }
                    }
                }

                txtFactor.Text = lFACTOR.ToString();
                txtCreditRatio.Text = lCREDIT_RATIO_MONTH.ToString();
                txtRatioAvail.Text = lLESS_RATIO_AVAILED.ToString();
                txtAvailable.Text = lAVAILABLE_RATIO.ToString();
                txtFactor.Text = lFACTOR.ToString();
                if (txtFactor.Text != "0")
                {
                    lAMT_OF_LOAN_CAN_AVAIL = (lAVAILABLE_RATIO * 1000) / Math.Round(lFACTOR, 6);
                }
                txtCanBeAvail.Text = lAMT_OF_LOAN_CAN_AVAIL.ToString();
                if (lAMT_OF_LOAN_CAN_AVAIL != 0)
                {
                    dtPayG.Focus();

                }
                else
                {
                    MessageBox.Show("AVAILABLE RATIO = 0 THEREFORE ENHENCEMENT NOT POSSIBLE");
                    this.Reset();
                    return;
                }
            

        }


        /// <summary> 

        /// Get the last day of the month for any 

        /// full date 

        /// </summary> 

        /// <param name="dtDate"></param> 

        /// <returns></returns> 

        private int GetLastDayOfMonth(DateTime dtDate)
        {

            // set return value to the last day of the month 

            // for any date passed in to the method 



            // create a datetime variable set to the passed in date 

            DateTime dtTo = dtDate;



            // overshoot the date by a month 

            dtTo = dtTo.AddMonths(1);



            // remove all of the days in the next month 

            // to get bumped down to the last day of the 

            // previous month 

            dtTo = dtTo.AddDays(-(dtTo.Day));



            // return the last day of the month 

            return dtTo.Day;

        }

        private void dtPayG_Validating(object sender, CancelEventArgs e)
        {
            double val3 = 0.0;
            int last_Day = 0;
            double val4 = 0.0;
             if(dtPayG.Value !=null)

            {
               
                    DateTime dtDate1 = Convert.ToDateTime(dtEnhancementDate.Value);//dat2
                    DateTime dtDate2 = Convert.ToDateTime(dtPayG.Value);
                    if(DateTime.Compare(dtDate1,dtDate2)>=0  )
                    {
                   
                    MessageBox.Show("DATE HAS TO BE GREATER THAN ENHANCEMENT DATE");
                    dtPayG.Focus();
                    e.Cancel = true;
                    return;
              
                    }

                    else
                      {

            
                       DateTime dt5 = new DateTime();
                        dt5 = dtDate1.AddMonths(1);// dat2
    
                           if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                                {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);

                            }

                            else
                            
                            {
                                
                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;



                            }

                       if (val3 > 30  )
                        {
                            MessageBox.Show("DATE HAS TO BE WITHIN THE MONTH AFTER ENHANCEMENT DATE");
                            dtEnhancementDate.Focus();
                            e.Cancel = true;
                            return;
                        }
                        wval5.Text = val3.ToString();
                      txtENHAN_AMT.Focus();

                  }


             }


           
          
        }

        private void txtENHAN_AMT_Validated(object sender, EventArgs e)
        {
            //string lFinType="";
            //if (txtENHAN_AMT.Text != string.Empty)
            //{
            //    if (this.FunctionConfig.CurrentOption !=  Function.View && this.FunctionConfig.CurrentOption!=Function.Delete)
            //    {

            //        if (txtENHAN_AMT.Text != string.Empty && txtCanBeAvail.Text != string.Empty && txtCanBeAvail.Text != "INFINITY")
            //        {
            //            if (double.Parse(txtENHAN_AMT.Text) > double.Parse(txtCanBeAvail.Text))
            //            {
            //                MessageBox.Show("VALUE HAS TO BE ENTERED & SHOULD BE <  " + txtCanBeAvail.Text);


            //                txtENHAN_AMT.Focus();
            //                return;


            //            }

            //        }

            //    }
            //    double AUX44;
            //    double AUX55;
            //    double aux99;
            //    int AUXX1;
            //    int AUXX2;
            //    double w_Bal = 0.0;
            //    double FinMarkup;
            //    int val3;
            //    double finBal;
            //    double tot_month_inst;
            //    double lFactor;
            //    double fnCredit;
            //    double lamt_of_loan_can_avail;
            //    double lENHAN_AMT;
            //    double lAvailableRatio;
            //    double lCREDIT_RATIO;
            //    double lLESS_RATIO_AVAILed;
            //    double lTOT_MONTH_INST;
            //    double LMarkup;
            //    double lFinMarkup;
            //    double lwwval5;
            //    double lFinCredit;
            //    double lFnCRatioBooking;
            //    double llFinMarkup;
            //    double ClericalBonus;
            //    double spAllowanceAmt;

            //    aux99 = double.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text);
            //    if (this.FunctionConfig.CurrentOption ==  Function.Add)
            //    {
            //        w_Bal = 0.0;
            //    }
            //    val3 = Convert.ToInt32(txtVal3.Text);
            //    finBal = double.Parse(txtfnBalance.Text == "" ? "0" : txtfnBalance.Text);
            //    lFactor = double.Parse(txtFactor.Text == "" ? "0" : txtFactor.Text);
            //    lENHAN_AMT = double.Parse(txtENHAN_AMT.Text == "" ? "0" : txtENHAN_AMT.Text);
            //    llFinMarkup = double.Parse(txtfnMarkup.Text == "" ? "0" : txtfnMarkup.Text);
            //    //----   * Markup Pending


            //    FinMarkup = ((finBal - w_Bal) * val3 * llFinMarkup) / 36000;

            //    tot_month_inst = (finBal - w_Bal) * (lFactor / 1000);

            //    fnCredit = tot_month_inst - FinMarkup;

            //    lamt_of_loan_can_avail = double.Parse((txtCanBeAvail.Text == "INFINITY" || txtCanBeAvail.Text == "") ? "0" : txtCanBeAvail.Text);
               
            //    lamt_of_loan_can_avail = lamt_of_loan_can_avail - lENHAN_AMT;

            //    lCREDIT_RATIO = double.Parse(txtCreditRatio.Text == "" ? "0" : txtCreditRatio.Text);

            //    lAvailableRatio = lamt_of_loan_can_avail * lFactor / 1000;
            //    lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAvailableRatio;

            //    finBal = (lENHAN_AMT + finBal - w_Bal);
            //    LMarkup = double.Parse(txtfnMarkup.Text == "" ? "0" : txtfnMarkup.Text);
            //    lTOT_MONTH_INST = tot_month_inst + ((lENHAN_AMT * lFactor) / 1000);
            //    lwwval5 = double.Parse(wval5.Text == "" ? "0" : wval5.Text);
            //    lFinMarkup = FinMarkup + (finBal * lwwval5 * LMarkup) / 36000;
            //    txtFnMonthDebit.Text = lENHAN_AMT.ToString();
            //    AUX55 = aux99 / 12;

            //    if (txtpr_category.Text == "C")
            //    {
            //        //coding pending

            //        ClericalBonus = Double.Parse(txtWTenCBonus.Text == "" ? "0" : txtWTenCBonus.Text);
            //        AUX44 = (ClericalBonus / 100) + 1;
            //        AUX55 = AUX55 * AUX44;

            //        Dictionary<string, object> param = new Dictionary<string, object>();
            //        param.Clear();
            //        param.Add("category", txtpr_category.Text);
            //        param.Add("level", txtpr_level.Text);
            //        param.Add("fn_branch", txtfnBranch.Text);
            //        DataTable dtClericalAllowance = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "ClericalStaffCheck", param);
            //        if (dtClericalAllowance != null)
            //        {
            //            if (dtClericalAllowance.Rows.Count > 0)
            //            {
            //                spAllowanceAmt = double.Parse(dtClericalAllowance.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtClericalAllowance.Rows[0].ItemArray[0].ToString());
            //                AUX55 = AUX55 + spAllowanceAmt;
            //            }
            //        }

            //        else
            //        {
            //            MessageBox.Show("UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE");
            //            this.Reset();
            //            this.Cancel();
            //            txtOption.Focus();

            //            CHRIS_Setup_AllowanceEnter AllowanceDialog = new CHRIS_Setup_AllowanceEnter(null, null);
            //            AllowanceDialog.ShowDialog();
            //        }
            //    }

            //    lFinCredit = (lTOT_MONTH_INST - lFinMarkup);
            //    txtFIN_CREDIT.Text = lFinCredit.ToString();
            //    txtFnMonthCredit.Text = "0";
            //    txtFnMonthMarkup.Text = "0";
            //    lFnCRatioBooking = (lLESS_RATIO_AVAILed / AUX55) * 100;
            //    txtBookRatio.Text = lFnCRatioBooking.ToString();
            //    //lFinType = txtFinType.Text.Replace("\\", @"\");
            //    //SETValues
            //    txtMarkup.Text = lFinMarkup.ToString();

            //    txtRatioAvail.Text = lLESS_RATIO_AVAILed.ToString();

            //    txtAvailable.Text = lAvailableRatio.ToString();

            //    txtCanBeAvail.Text = lamt_of_loan_can_avail.ToString();

            //    txtTotalMonthInstall.Text = lTOT_MONTH_INST.ToString();

            //    txtfnBalance.Text = finBal.ToString();

            //    Dictionary<string, object> paramMonth = new Dictionary<string, object>();
            //    paramMonth.Add("FN_FINANCE_NO", txtFinanceNo.Text);
            //    paramMonth.Add("PR_P_NO", txtPersonnalNo.Text);


            //    paramMonth.Add("FinType", txtFinType.Text.Replace("\\", @"\"));
            //    DataTable dtFinPayMonth = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnMonthValues", paramMonth);
            //    if (dtFinPayMonth != null)
            //    {
            //        if (dtFinPayMonth.Rows.Count > 0)
            //        {
            //            txtTOT_INST.Text = dtFinPayMonth.Rows[0].ItemArray[0].ToString();
            //            txtMARKUP_REC.Text = dtFinPayMonth.Rows[0].ItemArray[1].ToString();


            //        }

            //    }
            //    if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
            //    {
            //        DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
            //                           , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //        if (dRes == DialogResult.Yes)
            //        {
            //            txtpersonal.Text = txtPersonnalNo.Text;
            //            fnfinNo.Text = txtFinanceNo.Text;
            //            base.DoToolbarActions(this.Controls, "Save");

            //            CallReport("AUPR", "OLD");
            //            CallReport("AUP0", "NEW");
            //            this.Reset();
            //            base.ClearForm(this.pnlFinanceEnhancement.Controls);

            //            txtOption.Focus();
            //            //call save
            //            return;

            //        }
            //        else if (dRes == DialogResult.No)
            //        {
            //            this.Reset();
            //            base.ClearForm(this.pnlFinanceEnhancement.Controls);
            //            txtOption.Focus();

            //            return;
            //        }

            //    }

            //}
                

         }



        private void Reset()
        {
            this.txtPersonnalNo.Text = "";
            this.txtPersonnalName.Text = "";
            this.txtFinType.Text = "";
            this.txtMarkup.Text = "";
            this.txtFinanceNo.Text = "";

            this.txtENHAN_AMT.Text = "";
            this.txtFIN_CREDIT.Text = "";
            this.txtMARKUP_REC.Text = "";
            this.txtTOT_INST.Text = "";
            this.txtfnBalance.Text = "";
            this.txtTotalMonthInstall.Text = "";
            this.txtCanBeAvail.Text = "";
            this.txtFactor.Text = "";
            this.txtAvailable.Text = "";
            this.txtRatioAvail.Text = "";



        }


        private void txtFinType_Validated(object sender, EventArgs e)
        {
            if (txtFinType.Text != string.Empty)
            {
                DateTime StartDate = new DateTime();
                DateTime EndDate = new DateTime();
                DateTime ExtraTime = new DateTime();

                bool returnValue = true;
                Dictionary<string, object> paramDummy = new Dictionary<string, object>();
                paramDummy.Add("PR_P_NO", txtPersonnalNo.Text);
                paramDummy.Add("FN_FIN_NO", txtFinanceNo.Text);
                paramDummy.Add("FN_TYPE", txtFinType.Text);
                DataTable dtDummy = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthDummy", paramDummy);
                if (dtDummy != null)
                {

                    if (dtDummy.Rows.Count > 0)
                    {


                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", txtPersonnalNo.Text);
                        param.Add("FN_FIN_NO", txtFinanceNo.Text);

                        DataTable dtFinFnMonth = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthQuery", param);
                        if (dtFinFnMonth != null)
                        {
                            if (dtFinFnMonth.Rows.Count == 1  )
                            {
                                bool aSym1 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[0].ToString(), out Sym1);
                                bool aSym2 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[1].ToString(), out Sym2);
                                bool aSym3 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[2].ToString(), out Sym3);
                                if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete)
                                {
                                    if (Sym1 != 0 || Sym2 == 0 || Sym3 <= 0)
                                    {
                                        MessageBox.Show(" ENHANCEMENT HAS NOT BEEN DONE LATELY .YOU CANT " + txtCurrOption.Text);

                                        txtOption.Focus();
                                        base.ClearForm(this.Controls);
                                        base.Cancel();
                                        return;

                                    }
                                }

                            }
                            if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Modify || this.FunctionConfig.CurrentOption == Function.Delete || this.FunctionConfig.CurrentOption == Function.Add)
                            {
                                if (dtFinFnMonth.Rows.Count > 1)
                                {
                                    MessageBox.Show("TOO MANY ROWS .....");
                                    this.txtFinType.Focus();
                                    return;

                                }
                            }
                        }

                        Dictionary<string, object> paramBooking = new Dictionary<string, object>();
                        paramBooking.Add("FN_FIN_NO", txtFinanceNo.Text);
                        DataTable dtFinFnBooking = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthBookingValues", paramBooking);
                        if (dtFinFnBooking != null)
                        {
                            if (dtFinFnBooking.Rows.Count > 0)
                            {
                                if (dtFinFnBooking.Rows[0].ItemArray[0].ToString() != string.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[0].ToString(), out StartDate))
                                    {
                                        DTstartdate.Value = StartDate;
                                    }
                                }
                                if (dtFinFnBooking.Rows[0].ItemArray[1].ToString() != String.Empty)
                                {

                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[1].ToString(), out EndDate))
                                    {
                                        DTFN_END_DATE.Value = EndDate;
                                    }
                                }
                                if (dtFinFnBooking.Rows[0].ItemArray[2].ToString() != string.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[2].ToString(), out ExtraTime))
                                    {
                                        dtExtratime.Value = ExtraTime;
                                    }

                                }
                                else
                                {
                                    dtExtratime.Value = null;
                                }


                            }



                            if (this.FunctionConfig.CurrentOption == Function.Add)
                            {
                                //Dictionary<string, object> param2 = new Dictionary<string, object>();
                                //param2.Add("FN_FIN_NO", txtFinanceNo.Text);
                                DataTable dt2 = this.GetDataObject("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayGenDate");
                                if (dt2 != null)
                                {
                                    if (dt2.Rows.Count > 0)
                                    {
                                        if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {
                                            lPayGenDate = Convert.ToDateTime(dt2.Rows[0].ItemArray[0].ToString());
                                            DTPAY_GEN_DATE.Value = lPayGenDate;

                                        }
                                    }

                                }


                                Dictionary<string, object> param1 = new Dictionary<string, object>();
                                param1.Add("FN_FIN_NO", txtFinanceNo.Text);
                                param1.Add("PR_P_NO", txtPersonnalNo.Text);
                                param1.Add("FN_TYPE", txtFinType.Text);
                                DataTable dt1 = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthValues", param1);

                                if (dt1 != null)
                                {
                                    if (dt1.Rows.Count > 0)
                                    {

                                        //lFnMdate = null;
                                      
                                        if (dt1.Rows[0]["lFN_DEBIT"].ToString() != string.Empty)
                                        {
                                            DDebit = double.Parse(dt1.Rows[0]["lFN_DEBIT"].ToString());
                                        }
                                        if (dt1.Rows[0]["lFN_CREDIT"].ToString() != string.Empty)
                                        {
                                            CCredit = double.Parse(dt1.Rows[0]["lFN_CREDIT"].ToString());
                                        }
                                        if (dt1.Rows[0]["FN_PAY_LEFT"].ToString() != string.Empty)
                                        {
                                            OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
                                        }

                                        if (dt1.Rows[0]["FN_BALANCE"].ToString() != string.Empty)
                                        {

                                            BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
                                        }
                                        txtfnBalance.Text = BBal.ToString();
                                        txtFnPAyLeft.Text = OPayLeft.ToString();
                                    }

                                }

                            }

                            else
                            {
                                Dictionary<string, object> param1 = new Dictionary<string, object>();
                                param1.Add("FN_FIN_NO", txtFinanceNo.Text);
                                param1.Add("PR_P_NO", txtPersonnalNo.Text);
                                param1.Add("FN_TYPE", txtFinType.Text);
                                DataTable dt1 = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthValuesDMV", param1);

                                if (dt1 != null)
                                {
                                    if (dt1.Rows.Count > 0)
                                    {

                                        //lFnMdate = null;
                                      
                                        if (dt1.Rows[0]["fn_debit"].ToString() != string.Empty)
                                        {
                                            DDebit = double.Parse(dt1.Rows[0]["fn_debit"].ToString());
                                        }
                                        if (dt1.Rows[0]["FN_BALANCE"].ToString() != string.Empty)
                                        {
                                            BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
                                        }
                                        if (dt1.Rows[0]["FN_PAY_LEFT"].ToString() != string.Empty)
                                        {
                                            OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
                                        }

                                        if (dt1.Rows[0]["fn_debit"].ToString() != string.Empty)
                                        {
                                            wBBal = double.Parse(dt1.Rows[0]["fn_debit"].ToString());
                                        }
                                        txtfnBalance.Text = BBal.ToString();
                                        txtFnPAyLeft.Text = OPayLeft.ToString();
                                        W_Bal.Text = wBBal.ToString();
                                        txtENHAN_AMT.Text = DDebit.ToString();

                                    }

                                }
                                DataTable dt2 = this.GetDataObject("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayGenDate");
                                if (dt2 != null)
                                {
                                    if (dt2.Rows.Count > 0)
                                    {
                                        if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {
                                            lPayGenDate = Convert.ToDateTime(dt2.Rows[0].ItemArray[0].ToString());
                                            DTPAY_GEN_DATE.Value = lPayGenDate;

                                        }
                                    }

                                }

                                Dictionary<string, object> paramMatDate = new Dictionary<string, object>();
                                paramMatDate.Add("FN_FIN_NO", txtFinanceNo.Text);
                                paramMatDate.Add("PR_P_NO", txtPersonnalNo.Text);
                                paramMatDate.Add("FN_TYPE", txtFinType.Text);
                                DataTable dtFinPayMatDate = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayMatDate", paramMatDate);
                                if (dtFinPayMatDate != null)
                                {
                                    if (dtFinPayMatDate.Rows.Count > 0)
                                    {
                                        if (dtFinPayMatDate.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {

                                            lPayGenDate = Convert.ToDateTime(dtFinPayMatDate.Rows[0].ItemArray[0].ToString());
                                        }
                                        dtEnhancementDate.Value = lPayGenDate;

                                    }

                                }
                                dtEnhancementDate.Focus();
                                EnhancementDateValidate();
                                txtENHAN_AMT.Focus();
                                returnValue = Proc_2();
                                if (returnValue == false)
                                {
                                    this.Cancel();

                                }
                                /* go_field('FN_MONTH.FN_MDATE');
           execute_trigger('key-nxtfld');
           GO_FIELD('ENHAN_AMT');
           proc_2;*/




                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("THIS TYPE IS NOT VALID " );

                txtFinType.Focus();
                return;


            }
        }



        protected void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            CrplControlLibrary.SLTextBox txt = (CrplControlLibrary.SLTextBox)sender;
            if (txt != null && txt.Text != "" && txt.Text != "0")
            {
                if (e.KeyChar == '\r' || e.KeyChar == '\t')
                {
                    this.frm1.Close();



                }
            }
        }


        private void txtPersonnalNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtPersonnalNo.Text != string.Empty)
            {

                Proc_Level();
                if (txtpr_category.Text == "C" && (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify))
                {
                    if (this.operationMode != iCORE.Common.PRESENTATIONOBJECTS.Cmn.Mode.View)
                    {
                        frm1 = new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput("ENTER THE % OF 10_C_BONUS  ......", CrplControlLibrary.TextType.Double);
                        frm1.TextBox.MaxLength = 5;
                        this.frm1.TextBox.KeyPress += new KeyPressEventHandler(txt_KeyPress);
                        frm1.ShowDialog();
                        txtWTenCBonus.Text = frm1.Value;
                    }
                }
                
                this.BindLOVLookupButton(lbType);
                          
                //lbType.ConditionalFields = "txtPersonnalNo";
                //lbType.PerformClick();
              
            }
        }


        public void Proc_Level()
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnalNo.Text);
            DataTable dtProcLevel = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinProc_Level", param);
            if (dtProcLevel != null)
            {
                if (dtProcLevel.Rows.Count > 0)
                {
                    if (dtProcLevel.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        txtpr_level.Text = dtProcLevel.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

        }


        public bool Proc_2()
        {
            double EnhancementAmt=0.0;
            double lAmountofLoanCanAvail=0.0;
            double AUX44 = 0.0;
            double AUX55 = 0.0;
            double AUX99 = 0.0;
            double lFACTOR = 0.0;
            double lAVAILABLE_RATIO = 0.0;
            double lLESS_RATIO_AVAILed = 0.0;
            double lCREDIT_RATIO=0.0;
            double lTOT_MONTH_INST = 0.0;
            double lfnBalance=0.0;
            double lFiN_MARKUP = 0.0;
            double lMARKUP=0.0;
            double WVal3=0.0;
            double lFiN_CREDIT = 0.0;
            double lTOT_INST = 0.0;
            double lMarkupRec = 0.0;
            bool returnValue=true;
            if (this.FunctionConfig.CurrentOption != Function.View && this.FunctionConfig.CurrentOption != Function.Delete)
            {
                if (txtENHAN_AMT.Text == string.Empty)
                {
                    return returnValue = false;

                }
                else
                {
                    if (txtENHAN_AMT.Text != string.Empty)
                    {
                        double.TryParse(txtENHAN_AMT.Text, out EnhancementAmt);
                    }
                    if (txtCanBeAvail.Text != string.Empty)
                    {
                        double.TryParse(txtCanBeAvail.Text, out lAmountofLoanCanAvail);
                    }
                    if (EnhancementAmt > lAmountofLoanCanAvail)
                    {
                       return returnValue = false;
                    }
                }
            }

                if (this.FunctionConfig.CurrentOption == Function.Add)
            {
                if (txtENHAN_AMT.Text != string.Empty)
                {
                    double.TryParse(txtENHAN_AMT.Text, out EnhancementAmt);
                }
                if (txtCanBeAvail.Text != string.Empty)
                {
                    double.TryParse(txtCanBeAvail.Text, out lAmountofLoanCanAvail);
                }
                lAmountofLoanCanAvail = lAmountofLoanCanAvail - EnhancementAmt;
                if (txtFactor.Text != string.Empty)
                {
                    double.TryParse(txtFactor.Text, out lFACTOR);
                }
                lAVAILABLE_RATIO = (lAmountofLoanCanAvail * lFACTOR) / 1000;
                if (txtCreditRatio.Text != string.Empty)
                {
                    double.TryParse(txtCreditRatio.Text, out  lCREDIT_RATIO);
                }
                lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAVAILABLE_RATIO;
                if (txtfnBalance.Text != string.Empty)
                {
                    double.TryParse(txtfnBalance.Text, out lfnBalance);
                }
                lTOT_MONTH_INST = (lfnBalance * lFACTOR) / 1000;
                if (txtVal3.Text != string.Empty)
                {
                    double.TryParse(txtVal3.Text, out WVal3);
                }
                if (txtfnMarkup.Text != string.Empty)
                {
                    double.TryParse(txtfnMarkup.Text, out lMARKUP);
                }
                lFiN_MARKUP = (lfnBalance * (WVal3 / 100 ) * lMARKUP) / MARKUP_RATE_DAYS;
                lFiN_CREDIT = lTOT_MONTH_INST - lFiN_MARKUP;
                txtMarkup.Text = "0";
                txtFIN_CREDIT.Text = "0";


            }

                Dictionary<string, object> paramsFin = new Dictionary<string, object>();
                paramsFin.Add("FN_FIN_NO", txtFinanceNo.Text);
                paramsFin.Add("PR_P_NO", txtPersonnalNo.Text);
               
                DataTable dtFinBook = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinProc2", paramsFin);
                if (dtFinBook != null)
                {
                    if (dtFinBook.Rows.Count > 0)
                    {
                        if (dtFinBook.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            txtBookRatio.Text = dtFinBook.Rows[0].ItemArray[0].ToString();
                        }

                    }

                }

                if (this.FunctionConfig.CurrentOption == Function.View || this.FunctionConfig.CurrentOption == Function.Delete)
                {
                 DataTable dtFinMonthlyDed = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinBookMonthlyDed", paramsFin);
                     if (dtFinMonthlyDed != null)
                     {
                         if (dtFinMonthlyDed.Rows.Count > 0)
                         {
                             if(dtFinMonthlyDed.Rows[0].ItemArray[0].ToString() != string.Empty)
                             {
                                 if (double.TryParse(dtFinMonthlyDed.Rows[0].ItemArray[0].ToString(), out lTOT_MONTH_INST))
                                 {
                                     txtTotalMonthInstall.Text = lTOT_MONTH_INST.ToString();
                                 }
                             }

                         }

                     }
                }




                Dictionary<string, object> Installparams = new Dictionary<string, object>();
                Installparams.Add("FN_FIN_NO", txtFinanceNo.Text);
                Installparams.Add("PR_P_NO", txtPersonnalNo.Text);
                Installparams.Add("FN_TYPE", txtFinType.Text);

                DataTable dtFinInstll = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthlyInstall", Installparams);
                if (dtFinInstll != null)
                {
                    if (dtFinInstll.Rows.Count > 0)
                    {
                        if (dtFinInstll.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {

                            if (double.TryParse(dtFinInstll.Rows[0].ItemArray[0].ToString(), out lTOT_INST))
                            {
                                txtTOT_INST.Text = lTOT_INST.ToString();
                            }
                           
                        }

                        if (dtFinInstll.Rows[0].ItemArray[1].ToString() != string.Empty)
                        {
                            if (double.TryParse(dtFinInstll.Rows[0].ItemArray[1].ToString(), out lMarkupRec))
                            {
                                txtMARKUP_REC.Text = lMarkupRec.ToString();
                            }
                        }

                    }

                }
                if (this.FunctionConfig.CurrentOption == Function.Modify)
                {
                    dtEnhancementDate.Focus();
                }
                if (this.FunctionConfig.CurrentOption == Function.Delete)
                {

                    DialogResult dRes = MessageBox.Show("DO YOU WANT TO DELETE THE RECORD [Y]es [N]o", "Note"
                                , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            Dictionary<string, object> paramDelete = new Dictionary<string, object>();

                            paramDelete.Add("FN_FIN_NO", txtFinanceNo.Text);
                            paramDelete.Add("PR_P_NO", txtPersonnalNo.Text);
                           // paramDelete.Add("FN_TYPE", txtFinType.Text);
                            Result rsltCode;
                            CmnDataManager cmnDM = new CmnDataManager();
                            rsltCode = cmnDM.Execute("CHRIS_SP_Fin_EnhancementFN_Month_DELETE_ALL", "DELETE_ALL", paramDelete);
           
                            //base.DoToolbarActions(this.Controls, "Delete");
                            base.DoToolbarActions(this.Controls, "Cancel");
                            this.Reset();
                            base.ClearForm(this.Controls);
                            txtOption.Focus();
                            //call save
                            return returnValue;

                        }
                        else if (dRes == DialogResult.No)
                        {
                            this.Reset();
                            base.ClearForm(this.Controls);
                            txtOption.Focus();

                            return returnValue;
                        }

                    
                }
                if (this.FunctionConfig.CurrentOption == Function.View)
                {
                    DialogResult dRes = MessageBox.Show("DO YOU WANT TO VIEW MORE RECORDS [Y]es [N]o", "Note"
                                , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        this.Reset();
                        base.ClearForm(this.pnlFinanceEnhancement.Controls);
                        txtPersonnalNo.Focus();
                        return returnValue;
                    }
                    else if (dRes == DialogResult.No)
                    {
                        this.Reset();
                        base.ClearForm(this.pnlFinanceEnhancement.Controls);
                        base.DoToolbarActions(this.Controls, "Cancel");
                        txtOption.Focus();

                    }
                     


                }
                    return returnValue;
      //        IF :W_OPTION = 'M' THEN
      //   go_block('fn_month');
      //   go_field('fn_month.fn_mdate');
      //END IF;
      //IF :W_OPTION = 'D' THEN
      //   GO_FIELD('W_ANSWER3');
      //END IF;
      //IF :W_OPTION = 'V' THEN
      //   GO_FIELD('W_ANSWER2');
      //END IF;

        }



        private void CallReport(string FN, string Status)
        {
            iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm frm =
                new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, connbean);

            System.ComponentModel.IContainer comp = new System.ComponentModel.Container();

            GroupBox pnl = new GroupBox();
            string globalPath = @"C:\SPOOL\CHRIS\AUDIT\";
            string FN1 = FN + DateTime.Now.ToString("yyyyMMddHms");
            //+ DateTime.Now.Date.ToString("YYYYMMDDHHMISS");
            string FullPath = globalPath + FN1;
            //FN1 = 'AUPR'||TO_CHAR(SYSDATE,'YYYYMMDDHHMISS')||'.LIS';


            //CrplControlLibrary.SLTextBox txtDESNAME = new CrplControlLibrary.SLTextBox(comp);
            //txtDESNAME.Name = "txtDESNAME";
            //txtDESNAME.Text = "";      //":GLOBAL.AUDIT_PATH||FN1";
            //pnl.Controls.Add(txtDESNAME);

            //CrplControlLibrary.SLTextBox txtMODE = new CrplControlLibrary.SLTextBox(comp);
            //txtMODE.Name = "txtMODE";
            //txtMODE.Text = "CHARACTER";
            //pnl.Controls.Add(txtMODE);

            CrplControlLibrary.SLTextBox txtDT = new CrplControlLibrary.SLTextBox(comp);
            txtDT.Name = "dt";
            txtDT.Text = this.Now().ToString();
            pnl.Controls.Add(txtDT);

            CrplControlLibrary.SLTextBox txtPNO = new CrplControlLibrary.SLTextBox(comp);
            txtPNO.Name = "pno";
            txtPNO.Text = txtpersonal.Text;
            pnl.Controls.Add(txtPNO);

            CrplControlLibrary.SLTextBox txtfinNo = new CrplControlLibrary.SLTextBox(comp);
            txtfinNo.Name = "fin_No";
            txtfinNo.Text = fnfinNo.Text;
            pnl.Controls.Add(txtfinNo);


            CrplControlLibrary.SLTextBox txtuser = new CrplControlLibrary.SLTextBox(comp);
            txtuser.Name = "user";
            txtuser.Text = (this.MdiParent as iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu).getXmsUser().getSoeId();
            pnl.Controls.Add(txtuser);

            CrplControlLibrary.SLTextBox txtST = new CrplControlLibrary.SLTextBox(comp);
            txtST.Name = "st";
            txtST.Text = Status;
            pnl.Controls.Add(txtST);




            frm.Controls.Add(pnl);
            frm.RptFileName = "AUDIT03A";
            frm.Owner = this;
            //frm.ExportCustomReportToTXT(FullPath, "TXT");
            frm.ExportCustomReportToTXT(FullPath, "TXT");
        }

        private void txtENHAN_AMT_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {Keys st = (Keys.Shift | Keys.Tab);
        if (e.Modifiers == Keys.Shift)
        {
            if (e.KeyCode == Keys.Tab)
            {


            }
        }
             else
            {
                if (e.KeyData == Keys.Tab)
                {

                    string lFinType = "";
                    if (txtENHAN_AMT.Text == "0.00")
                    {

                    }

                    if (txtENHAN_AMT.Text != string.Empty)
                    {
                        if (this.FunctionConfig.CurrentOption != Function.View && this.FunctionConfig.CurrentOption != Function.Delete)
                        {

                            if (txtENHAN_AMT.Text != string.Empty && txtCanBeAvail.Text != string.Empty && txtCanBeAvail.Text != "INFINITY" )
                            {
                                if (double.Parse(txtENHAN_AMT.Text) > double.Parse(txtCanBeAvail.Text))
                                {
                                    MessageBox.Show("VALUE HAS TO BE ENTERED & SHOULD BE <  " + txtCanBeAvail.Text);

                                   
                                    txtENHAN_AMT.Focus();
                                    e.IsInputKey = true;

                                    return;


                                }

                            }

                        }
                        double AUX44;
                        double AUX55;
                        double aux99;
                        int AUXX1;
                        int AUXX2;
                        double w_Bal = 0.0;
                        double FinMarkup;
                        int val3;
                        double finBal;
                        double tot_month_inst;
                        double lFactor;
                        double fnCredit;
                        double lamt_of_loan_can_avail;
                        double lENHAN_AMT;
                        double lAvailableRatio;
                        double lCREDIT_RATIO;
                        double lLESS_RATIO_AVAILed;
                        double lTOT_MONTH_INST;
                        double LMarkup;
                        double lFinMarkup;
                        double lwwval5;
                        double lFinCredit;
                        double lFnCRatioBooking;
                        double llFinMarkup;
                        double ClericalBonus;
                        double spAllowanceAmt;

                        aux99 = double.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text);
                        if (this.FunctionConfig.CurrentOption == Function.Add)
                        {
                            w_Bal = 0.0;
                        }
                        else
                        {
                            if(W_Bal.Text !=string.Empty)
                            {
                                w_Bal = double.Parse(W_Bal.Text);

                            }
                        }
                        val3 = Convert.ToInt32(txtVal3.Text);
                        finBal = double.Parse(txtfnBalance.Text == "" ? "0" : txtfnBalance.Text);
                        lFactor = double.Parse(txtFactor.Text == "" ? "0" : txtFactor.Text);
                        lENHAN_AMT = double.Parse(txtENHAN_AMT.Text == "" ? "0" : txtENHAN_AMT.Text);
                        llFinMarkup = double.Parse(txtfnMarkup.Text == "" ? "0" : txtfnMarkup.Text);
                        //----   * Markup Pending


                        FinMarkup = ((finBal - w_Bal) * (val3 / 100 ) * llFinMarkup) / MARKUP_RATE_DAYS;

                        tot_month_inst = (finBal - w_Bal) * (lFactor / 1000);

                        fnCredit = tot_month_inst - FinMarkup;

                        lamt_of_loan_can_avail = double.Parse((txtCanBeAvail.Text == "INFINITY" || txtCanBeAvail.Text == "") ? "0" : txtCanBeAvail.Text);

                        lamt_of_loan_can_avail = lamt_of_loan_can_avail - lENHAN_AMT;

                        lCREDIT_RATIO = double.Parse(txtCreditRatio.Text == "" ? "0" : txtCreditRatio.Text);

                        lAvailableRatio = lamt_of_loan_can_avail * lFactor / 1000;
                        lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAvailableRatio;

                        finBal = (lENHAN_AMT + finBal - w_Bal);
                        LMarkup = double.Parse(txtfnMarkup.Text == "" ? "0" : txtfnMarkup.Text);
                        lTOT_MONTH_INST = tot_month_inst + ((lENHAN_AMT * lFactor) / 1000);
                        lwwval5 = double.Parse(wval5.Text == "" ? "0" : wval5.Text);
                        lFinMarkup = FinMarkup + (finBal * ( lwwval5 /100) * LMarkup) / MARKUP_RATE_DAYS;
                        txtFnMonthDebit.Text = lENHAN_AMT.ToString();
                        AUX55 = aux99 / 12;

                        if (txtpr_category.Text == "C")
                        {
                            //coding pending

                            ClericalBonus = Double.Parse(txtWTenCBonus.Text == "" ? "0" : txtWTenCBonus.Text);
                            AUX44 = (ClericalBonus / 100) + 1;
                            AUX55 = AUX55 * AUX44;

                            Dictionary<string, object> param = new Dictionary<string, object>();
                            param.Clear();
                            param.Add("category", txtpr_category.Text);
                            param.Add("level", txtpr_level.Text);
                            param.Add("fn_branch", txtfnBranch.Text);
                            DataTable dtClericalAllowance = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "ClericalStaffCheck", param);
                            if (dtClericalAllowance != null)
                            {
                                if (dtClericalAllowance.Rows.Count > 0)
                                {
                                    spAllowanceAmt = double.Parse(dtClericalAllowance.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtClericalAllowance.Rows[0].ItemArray[0].ToString());
                                    AUX55 = AUX55 + spAllowanceAmt;
                                }
                            }

                            else
                            {
                                MessageBox.Show("UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE");
                                this.Reset();
                                this.Cancel();
                                txtOption.Focus();

                                CHRIS_Setup_AllowanceEnter AllowanceDialog = new CHRIS_Setup_AllowanceEnter(null, null);
                                AllowanceDialog.ShowDialog();
                            }
                        }

                        lFinCredit = (lTOT_MONTH_INST - lFinMarkup);
                        txtFIN_CREDIT.Text = lFinCredit.ToString();
                        txtFnMonthCredit.Text = "0";
                        txtFnMonthMarkup.Text = "0";
                        lFnCRatioBooking = (lLESS_RATIO_AVAILed / AUX55) * 100;
                        txtBookRatio.Text = lFnCRatioBooking.ToString();
                        //lFinType = txtFinType.Text.Replace("\\", @"\");
                        //SETValues
                        txtMarkup.Text = lFinMarkup.ToString();

                        txtRatioAvail.Text = lLESS_RATIO_AVAILed.ToString();

                        txtAvailable.Text = lAvailableRatio.ToString();

                        txtCanBeAvail.Text = lamt_of_loan_can_avail.ToString();

                        txtTotalMonthInstall.Text = lTOT_MONTH_INST.ToString();

                        txtfnBalance.Text = finBal.ToString();

                        Dictionary<string, object> paramMonth = new Dictionary<string, object>();
                        paramMonth.Add("FN_FINANCE_NO", txtFinanceNo.Text);
                        paramMonth.Add("PR_P_NO", txtPersonnalNo.Text);


                        paramMonth.Add("FinType", txtFinType.Text.Replace("\\", @"\"));
                        DataTable dtFinPayMonth = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnMonthValues", paramMonth);
                        if (dtFinPayMonth != null)
                        {
                            if (dtFinPayMonth.Rows.Count > 0)
                            {
                                txtTOT_INST.Text = dtFinPayMonth.Rows[0].ItemArray[0].ToString();
                                txtMARKUP_REC.Text = dtFinPayMonth.Rows[0].ItemArray[1].ToString();


                            }

                        }
                        if (this.FunctionConfig.CurrentOption == Function.Add || this.FunctionConfig.CurrentOption == Function.Modify)
                        {
                            DialogResult dRes = MessageBox.Show("Do you want to save this record [Y/N]..", "Note"
                                               , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dRes == DialogResult.Yes)
                            {
                                txtpersonal.Text = txtPersonnalNo.Text;
                                fnfinNo.Text = txtFinanceNo.Text;

                                CallReport("AUPR", "OLD");
                                base.DoToolbarActions(this.Controls, "Save");

                              
                                CallReport("AUP0", "NEW");
                                this.Reset();
                                base.ClearForm(this.pnlFinanceEnhancement.Controls);
                                this.Cancel();
                                txtOption.Focus();
                                //call save
                                return;

                            }
                            else if (dRes == DialogResult.No)
                            {
                                this.Reset();
                                base.ClearForm(this.pnlFinanceEnhancement.Controls);
                                this.Cancel();
                                txtOption.Focus();

                                return;
                            }

                        }

                    }


                }
            
            }
        }
     

    }
}