using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;

using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Setup;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinanceQuery : ChrisSimpleForm
    {

        #region Fields

        private double _ratio = 0;
        private int _pppp = 0;
        private int _cnting = 0;
        private int _w_all = 0;
        //private string _cat = "";
        private string _level = "";
        private string _level1 = "";
        private double _W_10_C = 0;
        private double _min_cap = 0;

        iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput _inputForm = null;

        #endregion

        #region Constructors

        public CHRIS_Finance_FinanceQuery()
        {
            InitializeComponent();
        }
        public CHRIS_Finance_FinanceQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //List<SLPanel> depPanels = new List<SLPanel>();
            //depPanels.Add(this.pnlFNMonthDetail);
            //this.pnlPersonnel.DependentPanels = depPanels;

            //List<SLPanel> conCurrPanels = new List<SLPanel>();
            //conCurrPanels.Add(this.pnlFNDetail);
            //this.pnlPersonnel.ConcurrentPanels = conCurrPanels;

            this.CurrentPanelBlock = this.pnlPersonnel.Name;
            
            this.pnlHead.SendToBack();
            this.ShowOptionKeys = false;
            this.ShowOptionTextBox = false;
            this.txtOption.Enabled = false;

            DateTime now = this.Now();
            this.txtDate.Text = now.ToString("dd/MM/yyyy");
            this.dgvFnMoths.AllowUserToAddRows = false;
            this.dgvFnMoths.StandardTab = true;
            this.FunctionConfig.F8 = Function.View;

            this.lblUserName.Text = "User Name :   "+this.UserName;
            this.txtUser.Text = this.UserName;
            this.txtLocation.Text = this.CurrentLocation;

            this.tbtList.Available = false;
            this.tbtSave.Available = false;
            this.tbtDelete.Available = false;
            this.tbtAdd.Available = false;

            Font f = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);

            this.colBalance.HeaderCell.Style.Font = f;
            this.colCredit.HeaderCell.Style.Font = f;
            this.colFinNo.HeaderCell.Style.Font = f;
            this.colLiquidate.HeaderCell.Style.Font = f;
            this.colMarkup.HeaderCell.Style.Font = f;
            this.colMonthlyDeduction.HeaderCell.Style.Font = f;


            //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtList"]));
                    
            //this.CommonOnLoadMethods();

        }
        protected override void OnClosing(CancelEventArgs e)
        {
            //this.AutoValidate = AutoValidate.Disable;
            this.ResetLiquidation();
            base.OnClosing(e);
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            //if (actionType == "List")
            //{
            //    if (this.txtPersNo.Focused)
            //    {
            //        this.lbtnPNo.PerformClick();
            //    }
            //    if (this.txtType.Focused)
            //    {
            //        this.lbtnFinType.PerformClick();
            //    }
            //}
            if (actionType == "Close")
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
            }
            if (actionType == "Save")
            {
                //this.dgvFnMoths.SaveGrid(this.pnlFNMonthDetail);
            }
            if (actionType == "Cancel")
            {
                this.AutoValidate = AutoValidate.Disable;

                this.ResetLiquidation();

                //this.lbtnFinType.SkipValidationOnLeave = true;
                this.txtType.SkipValidation = false;
                this.txtType.CustomEnabled = false;
               
                this.Reset();

                //this.ClearForm(this.slPanelSimple1.Controls);
                //this.ClearForm(this.pnlPersonnel.Controls);
                //this.ClearForm(this.pnlFNDetail.Controls);

                this.txtPersNo.Select();
                this.txtPersNo.Focus();

                //this.lbtnFinType.SkipValidationOnLeave = false;
                this.txtType.CustomEnabled = true;
                this.txtType.SkipValidation = true;

                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
            }
            //base.DoToolbarActions(ctrlsCollection, actionType);
        }
        protected override bool View()
        {

            this.Enabled = false;
            DialogResult dRes = MessageBox.Show("Do you want to Continue.", "Note"
                                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                //this.ResetLiquidation();
                //this.Reset();
                //this.txtPersNo.Select();
                //this.txtPersNo.Focus();
                this.tlbMain_ItemClicked(this, new ToolStripItemClickedEventArgs(this.tlbMain.Items["tbtCancel"]));
            }
            else if (dRes == DialogResult.No)
            {
                ////this.ResetLiquidation();
                //this.Reset();
                //this.Quit();
                this.tlbMain_ItemClicked(this, new ToolStripItemClickedEventArgs(this.tlbMain.Items["tbtCancel"]));
                this.Quit();
            }
            this.Enabled = true;
 
            return true;
        }
        protected override bool Quit()
        {
            this.AutoValidate = AutoValidate.Disable;
            return base.Quit();
        }

        private void Reset()
        {
            //this.txtType.SkipValidation = true;
            //this.lbtnFinType.SkipValidationOnLeave = true;
            
            this.txtPersNo.Select();
            this.txtPersNo.Focus();

            this.txtPersNo.Text = "";
            this.txtFirstName.Text = "";
            this.txtAnnualPack.Text = "";
            this.txtLongPkg.Text = "";

            //this.txtType.Enabled = false;

            //this.txtPersNo.Select();
            //this.txtPersNo.Focus();


            this.txtType.Text = "";
            this.txtDesc.Text = "";
            this.txtSchedule.Text = "";
            this.txtMarkup.Text = "";

            this.txtCreditRatio.Text = "";
            this.txtAvailable.Text = "";
            this.txtFactor.Text = "";
            this.txtMinCap.Text = "";

            this.txtRatioAvail.Text = "";
            this.txtCanBeAvail.Text = "";
            this.txtTotalMonthInstall.Text = "";
            this.txtCreditRatioPer.Text = "";

            this._W_10_C = 0;

            DataTable dt = (DataTable)this.dgvFnMoths.DataSource;
            if (dt != null)
            {
                dt.Rows.Clear();
                dt.AcceptChanges();
            }

            //this.txtPersNo.Select();
            //this.txtPersNo.Focus();

            //this.txtType.SkipValidation = false;
            //this.lbtnFinType.SkipValidationOnLeave = false;

            //this.txtType.Enabled = true;
        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;
            
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }
        
        private void PROC_1()
        {
            FINTypeCommand ent = (FINTypeCommand)(this.slPanelSimple1.CurrentBusinessEntity);
            this._ratio = (double)(ent.FN_C_RATIO);
            double min_cap = 0;
            double totalMonthlyInstall = 0;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", this.txtPersNo.Text);
            param.Add("PR_NEW_ANNUAL_PACK", this.txtAnnualPack.Text);
            param.Add("RATIO", this.txtRatio.Text == "" ? "0" : this.txtRatio.Text);
            param.Add("MARKUP", this.txtMarkup.Text == "" ? "0" : this.txtMarkup.Text);
            param.Add("SCHEDULE", this.txtSchedule.Text == "" ? "0" : this.txtSchedule.Text);

            param.Add("CAT", this.txtCat.Text);
            //param.Add("W_ALL", this._w_all);
            param.Add("LEVEL", this.txtLevel.Text);
            param.Add("BRANCH", this.txtBranch.Text);
            param.Add("W_DATE", this.txtDate.Text);

            param.Add("W_10_C", this._W_10_C);
            //param.Add("MIN_CAP", min_cap);
            //param.Add("MIN_CAP", this.txtSchedule.Text == "" ? "0" : this.txtSchedule.Text);
            param.Add("LOAN_PKG", this.txtMinCap.Text == "" ? "0" : this.txtMinCap.Text);
            //param.Add("TOT_MONTHLY_INSTALL", totalMonthlyInstall);
            param.Add("TYPE", this.txtType.Text);

            DataTable dt = this.GetData("CHRIS_SP_FINANCEQUERY_PROC_1", "", param);

            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["CREDIT_RATIO"].ToString() == "SP007")
                {
                    //this.Reset();
                    //UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE
                    MessageBox.Show("UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE"
                                    , "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Show The Allowance Form(SP007)
                    //CALL('SP007');
                    this.Cancel();
                    //CHRIS_Setup_AllowanceEnter frmAllowance = new CHRIS_Setup_AllowanceEnter((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)this.ParentForm, this.connbean);
                    //frmAllowance.Show();
                    return;
                }
                this.txtCreditRatio.Text = dt.Rows[0]["CREDIT_RATIO"].ToString();
                this.txtRatioAvail.Text = dt.Rows[0]["RATIO_AVAILED"].ToString();
                this.txtAvailable.Text = dt.Rows[0]["AVAILABLE"].ToString();
                this.txtCanBeAvail.Text = dt.Rows[0]["CAN_BE"].ToString();
                this.txtFactor.Text = dt.Rows[0]["FACTOR"].ToString();
                this.txtCreditRatioPer.Text = dt.Rows[0]["CREDIT_RATIO_PER"].ToString();
                this.txtTotalMonthInstall.Text = dt.Rows[0]["TOT_MONTHLY_INSTALL"].ToString();

                this._min_cap = double.Parse(dt.Rows[0]["MIN_CAP"].ToString() == "" ? "0" : dt.Rows[0]["MIN_CAP"].ToString());
                this.txtMinCap.Text = dt.Rows[0]["MIN_CAP"].ToString();

                this.txtLongPkg.Text = dt.Rows[0]["loan_pkg"].ToString();

                this.PROC_3();
            }
        }
        private void PROC_3()
        {
            double creditRatio = double.Parse(this.txtCreditRatio.Text == "" ? "0": this.txtCreditRatio.Text);
            double ratioAvailed = double.Parse(this.txtRatioAvail.Text == "" ? "0" : this.txtRatioAvail.Text);
            double available = double.Parse(this.txtAvailable.Text == "" ? "0" : this.txtAvailable.Text);
            double canBe = double.Parse(this.txtCanBeAvail.Text == "" ? "0" : this.txtCanBeAvail.Text);
            double factor = double.Parse(this.txtFactor.Text == "" ? "0" : this.txtFactor.Text);
            double creditRatioPer = double.Parse(this.txtCreditRatioPer.Text == "" ? "0" : this.txtCreditRatioPer.Text);
            double monthlyInstall = double.Parse(this.txtTotalMonthInstall.Text == "" ? "0" : this.txtTotalMonthInstall.Text);

            if (creditRatio < 0) { this.txtCreditRatio.Text = "0"; }
            if (ratioAvailed < 0) { this.txtRatioAvail.Text = "0"; }
            if (available < 0) { this.txtAvailable.Text = "0"; }
            if (canBe < 0) { this.txtCanBeAvail.Text = "0"; }
            if (factor < 0) { this.txtFactor.Text = "0"; }
            if (creditRatioPer < 0) { this.txtCreditRatioPer.Text = "0"; }
            if (monthlyInstall < 0) { this.txtTotalMonthInstall.Text = "0"; }
        }

        private void ExecuteQuery()
        {
            if (this.txtType.Text != "")
            {
                this.PROC_1();

                List<SLPanel> depPanels = new List<SLPanel>();
                depPanels.Add(this.pnlFNMonthDetail);
                this.pnlPersonnel.DependentPanels = depPanels;
                this.pnlPersonnel.LoadDependentPanels();

                DataTable dt1 = (DataTable)this.dgvFnMoths.DataSource;
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    dgvFnMoths.Select();
                    dgvFnMoths.Focus();
                    dgvFnMoths.CurrentCell = dgvFnMoths.Rows[0].Cells["colLiquidate"];
                }

                Dictionary<string, object> param = new Dictionary<string, object>();
                int pr_P_No = (this.txtPersNo.Text == "" ? 0 : (int.Parse(this.txtPersNo.Text)));
                param.Add("PR_P_NO", pr_P_No);
                DataTable dt = this.GetData("CHRIS_SP_FINANCEQUERY_MANAGER", "Get_PPPP", param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this._pppp = int.Parse(dt.Rows[0][0] == null ? "0" : dt.Rows[0][0].ToString());
                    if (this._pppp == 0)
                    {
                        DialogResult dRes = MessageBox.Show("Do you want to Continue or Exit.", "Note"
                                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dRes == DialogResult.Yes)
                        {
                            //this.txtType.SkipValidation = true;
                            //this.lbtnFinType.SkipValidationOnLeave = true;
                            //this.Reset();
                            //this.txtPersNo.Select();
                            //this.txtPersNo.Focus();
                            this.DoToolbarActions(this.pnlPersonnel.Controls, "Cancel");
                            //this.tlbMain_ItemClicked(this, new ToolStripItemClickedEventArgs(this.tlbMain.Items["tbtCancel"]));
                            //this.txtType.Text = "A";
                            //this.txtPersNo.Select();
                            //this.txtPersNo.Focus();
                            //this.txtType.Text = "";
                            //this.lbtnFinType.SkipValidationOnLeave = false;
                            //this.txtType.SkipValidation = false;
                            return;
                        }
                        else if (dRes == DialogResult.No)
                        {
                            //this.txtType.SkipValidation = true;
                            //this.lbtnFinType.SkipValidationOnLeave = true;
                            //this.Reset();
                            
                            //this.tlbMain_ItemClicked(this, new ToolStripItemClickedEventArgs(this.tlbMain.Items["tbtCancel"]));
                            //this.Quit();
                            this.AutoValidate = AutoValidate.Disable;
                            this.Quit();
                            

                            //this.txtType.Text = "A";
                            //this.txtPersNo.Select();
                            //this.txtPersNo.Focus();
                            //this.txtType.Text = "";
                            //this.lbtnFinType.SkipValidationOnLeave = false;
                            //this.txtType.SkipValidation = false;
                            return;
                        }
                    }
                    else
                    {
                        dt = this.GetData("CHRIS_SP_FINANCEQUERY_MANAGER", "Get_cnting", param);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            this._cnting = int.Parse(dt.Rows[0][0] == null ? "0" : dt.Rows[0][0].ToString());
                        }
                        if (dgvFnMoths.Rows.Count == 0)
                        {
                            //DialogResult dRes = MessageBox.Show("Do you want to Continue or Exit.", "Note"
                            //        , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            //if (dRes == DialogResult.Yes)
                            //{
                            //    this.Cancel();
                            //    return;
                            //}
                            //else if (dRes == DialogResult.No)
                            //{
                            //    this.Cancel();
                            //    this.Quit();
                            //    return;
                            //}
                        }
                    }
                }

            }

        }
        private void ResetLiquidation()
        {
            this.dgvFnMoths.Enabled = false;
            foreach (DataGridViewRow dr in dgvFnMoths.Rows)
            {
                if (dr.Cells["ID"].Value == null || dr.Cells["ID"].Value.ToString() == "")
                {
                    continue;
                }
                {
                    double pNo = double.Parse(this.txtPersNo.Text == "" ? "0" : this.txtPersNo.Text);
                    object finNo = dr.Cells["colFinNo"].Value;
                    object liqFlag = dr.Cells["colLiquidate"].Value;

                    Dictionary<string, object> param = new Dictionary<string, object>();

                    param.Add("PR_P_NO", pNo);
                    param.Add("FN_FIN_NO", finNo);
                    param.Add("FN_LIQ_FLAG", liqFlag);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);

                    if (rsltCode.isSuccessful)
                    {
                        this.PROC_1();
                        param.Remove("FN_LIQ_FLAG");
                        param.Add("FN_LIQ_FLAG", "");
                        rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);
                    }
                }
            }
            this.dgvFnMoths.Enabled = true;
        }
        private void UpdateLiquidation(double pNo, object finNo, object liqFlag)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();

            param.Add("PR_P_NO", pNo);
            param.Add("FN_FIN_NO", finNo);
            param.Add("FN_LIQ_FLAG", liqFlag);

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);

            if (rsltCode.isSuccessful)
            {
                this.PROC_1();
                param.Remove("FN_LIQ_FLAG");
                param.Add("FN_LIQ_FLAG", "");
                rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);
            }
        }

        #endregion

        #region Event Handlers

        private void lbtnFinType_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.txtPersNo.Text == "")
            {
                this.lbtnFinType.Enabled = false;
            }
            this.lbtnFinType.Enabled = true;
        }
        private void lbtnPNo_Enter(object sender, EventArgs e)
        {
            //this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            this.pnlPersonnel.DependentPanels = null;
        }
        
        private void txtType_Validated(object sender, EventArgs e)
        {
            if (this.txtPersNo.Text == "")
                return;

            this.ExecuteQuery();
        }
        private void txtPersNo_Enter(object sender, EventArgs e)
        {
            this.pnlPersonnel.DependentPanels = null;
        }
        protected void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            CrplControlLibrary.SLTextBox txt = (CrplControlLibrary.SLTextBox)sender;
            if (txt != null && txt.Text != "" && txt.Text != "0")
            {
                if (e.KeyChar == '\r' || e.KeyChar == '\t')
                {
                    this._inputForm.Close();
                }
            }
        }
        
        private void dgvFnMoths_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvFnMoths.CurrentRow.Cells["ID"].Value == null || this.dgvFnMoths.CurrentRow.Cells["ID"].Value.ToString() == "")
            {
                return;
            }
            //if ( this.dgvFnMoths.CurrentRow.Cells[5].IsInEditMode)
            {
                double pNo = double.Parse(this.txtPersNo.Text == "" ? "0" : this.txtPersNo.Text);
                object finNo = dgvFnMoths.CurrentRow.Cells["colFinNo"].Value;
                object liqFlag = dgvFnMoths.CurrentRow.Cells["colLiquidate"].EditedFormattedValue;

                Dictionary<string, object> param = new Dictionary<string, object>();

                param.Add("PR_P_NO", pNo);
                param.Add("FN_FIN_NO", finNo);
                param.Add("FN_LIQ_FLAG", liqFlag);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);

                if (rsltCode.isSuccessful)
                {
                    this.PROC_1();
                    //param.Remove("FN_LIQ_FLAG");
                    //param.Add("FN_LIQ_FLAG", "");
                    //rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);
                }
            }
        }
        private void dgvFnMoths_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            this.dgvFnMoths.CancelEdit();
        }
        private void dgvFnMoths_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvFnMoths.CurrentRow != null)
            {
                dgvFnMoths.CurrentCell = dgvFnMoths.CurrentRow.Cells["colLiquidate"];
            }
        }
        private void dgvFnMoths_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFnMoths.CurrentRow != null )
            {
                dgvFnMoths.ClearSelection();
                dgvFnMoths.CurrentCell = dgvFnMoths.CurrentRow.Cells["colLiquidate"];
                //dgvFnMoths.CurrentCell.Selected = true;
                //dgvFnMoths.BeginEdit(true);
                //dgvFnMoths.EditingControl.Select();
                //dgvFnMoths.EditingControl.Focus();
            }
        }
        private void dgvFnMoths_Leave(object sender, EventArgs e)
        {

            //DataTable dt = (DataTable)this.dgvFnMoths.DataSource;
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    if (this.txtPersNo.Text != "")
            //    {
            //        MessageBox.Show("[F6]=EXIT   [F8]=GOTO P.NO.");
            //        dgvFnMoths.Select();
            //        dgvFnMoths.Focus();
            //        //dgvFnMoths.CurrentCell = dgvFnMoths.Rows[0].Cells["colLiquidate"];
            //    }
            //}
        }
        private void dgvFnMoths_Validating(object sender, CancelEventArgs e)
        {
           DataTable dt = (DataTable)this.dgvFnMoths.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                if (this.txtPersNo.Text != "")
                {
                    MessageBox.Show("[F6]=EXIT   [F8]=GOTO P.NO.");
                    dgvFnMoths.Select();
                    dgvFnMoths.Focus();
                    //dgvFnMoths.CurrentCell = dgvFnMoths.Rows[0].Cells["colLiquidate"];
                }
            }
        }

        private void CHRIS_Finance_FinanceQuery_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            if (actionType == "LOV_FinType")
            {
                this.ExecuteQuery();
            }
            if (actionType == "LOV_PR_P_NO" || actionType == "LOV_PR_P_NOExists")
            {
                if (this._W_10_C == 0 && this.txtCat.Text == "C")
                {
                    this._inputForm = new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.frmInput("ENTER THE % FOR 10-C BONUS ......", CrplControlLibrary.TextType.Double);
                    this._inputForm.TextBox.MaxLength = 5;

                    this._inputForm.TextBox.KeyPress += new KeyPressEventHandler(txt_KeyPress);

                    this._inputForm.TextBox.Text = "";

                    this._inputForm.ShowDialog(this);
                    this._W_10_C = double.Parse(this._inputForm.Value == "" ? "0" : this._inputForm.Value);
                    //this.ExecuteQuery();
                }
            }
        }

        #endregion
    }
}