using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

using iCORE.CHRIS.PRESENTATIONOBJECTS.FinanceReports;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Finance
{
    public partial class CHRIS_Finance_FinanceApplication : ChrisMasterDetailForm
    {
        #region --Variables--
        double LESS_RATIO_AVAILED = 0;
        double FN_C_RATIO = 0;
        CmnDataManager cmnDM = new CmnDataManager();
        DateTime dtStart, dtSdate;
        bool iscancel = false;
        int sw = 0;
        bool chkF3 = false;
        bool chkHomeLoan = false;
        #endregion
        # region Contructor
        public CHRIS_Finance_FinanceApplication()
        {
            InitializeComponent();
        }

        public CHRIS_Finance_FinanceApplication(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            //List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(PnlDetail);
            //pnlMaster.DependentPanels = lstDependentPanels;
            //this.IndependentPanels.Add(pnlMaster);

            //List<SLPanel> lstConcurrentPanels = new List<SLPanel>();
            //lstConcurrentPanels.Add(PnlCalAmnt);
            //PnlFinance.ConcurrentPanels = lstConcurrentPanels;
            //PnlPersonnel.ConcurrentPanels = lstConcurrentPanels;
        }
        # endregion
        # region Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CurrentPanelBlock = "PnlPersonnel";
            txtCBonus.CustomEnabled = false;
            txtOption.Visible = false;
            this.txtDate.Text = this.Now().ToString("MM/dd/yyyy");
            this.label32.Text += " " + this.UserName;
            this.IndependentPanels.Add(this.PnlPersonnel);

            this.FunctionConfig.EnableF3 = true;
            this.FunctionConfig.F3 = Function.Modify;

            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.View;

            Column1.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_INSTALL.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_BALANCE.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_REDUCE_AMT.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_NEW_INST.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_NEW_OS_BAL.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
            FN_PAY_LEFT.HeaderCell.Style.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);

            DtSatartDt.Value = null;
            DGVFinance.Columns["FN_REDUCE_AMT"].ValueType = typeof(double);
            DGVFinance.Columns["FN_PAY_LEFT"].ValueType = typeof(double);
            DGVFinance.Columns["FN_NEW_OS_BAL"].ValueType = typeof(double);
            DGVFinance.Columns["FN_NEW_INST"].ValueType = typeof(double);
            this.tbtDelete.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
        }
        protected override bool Edit()
        {
            if (this.ActiveControl is SLDataGridView)
            {
                this.DGVFinance.EndEdit();
                this.txtDisbursmntAmt.CustomEnabled = true;
                this.txtDisbursmntAmt.Enabled = true;
                this.txtDisbursmntAmt.Select();
                this.txtDisbursmntAmt.Focus();
            }
            return base.Edit();
        }
        /* Trigger used in P_RP_NO  Next ITEM  In OracleForm Builder   
            Fill all the text box of personnel pannel 
         */
        protected override bool View()
        {
            if (txtRmks.Focused)
            {
                PROC_4();
                iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm brpt = new iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm(null, this.connbean);
                brpt.RptFileName = "FNREPAP";
                brpt.PrintCustomReport();

                CHRIS_FinanceReports_Loan_Asim frm = new CHRIS_FinanceReports_Loan_Asim(null, this.connbean, this);
                frm.ShowInTaskbar = false;
                frm.ShowDialog(this);
            }
            return false;
        }
        protected override bool Quit()
        {
            this.Close();
            return true;
        }
        protected void ProcLevel()
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnelNo.Text);
            rslt = cmnDM.GetData("CHRIS_SP_PERSONNELFinanceApplicaton_MANAGER", "Personnel_FinanceApp_Desig", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    txtLevel.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    txtDesg.Text = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                }
            }

        }
        private void PROC_0()
        {
            DateTime dt;
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", txtPersonnelNo.Text);
            rslt = cmnDM.GetData("CHRIS_SP_PERSONNEL_FinApp_MANAGER", "LIST", param);
            if (rslt.isSuccessful && rslt.dstResult != null)
            {
                if (DGVFinance.EndEdit())
                {
                    DGVFinance.DataSource = rslt.dstResult.Tables[0];
                    if (rslt.dstResult.Tables[0].Rows.Count > 0)

                        if (rslt.dstResult.Tables[0].Rows[0][4].ToString() != string.Empty)
                        { dtSdate = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0][4].ToString()); }
                    DGVFinance.CurrentCell = DGVFinance.Rows[0].Cells[3];
                    DGVFinance.Focus();
                }
            }
        }
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "List")
            {
                //base.DoToolbarActions(ctrlsCollection, actionType);
                //txtPersonnelNo.Select();
                //txtPersonnelNo.Focus();
                return;
            }

            if (actionType == "Save")
            {
                return;
            }

            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                // base.DoToolbarActions(ctrlsCollection, actionType);
                iscancel = true;
                base.ClearForm(PnlPersonnel.Controls);
                base.ClearForm(PnlFinance.Controls);
                base.ClearForm(PnlFinGrid.Controls);
                base.ClearForm(PnlCalAmnt.Controls);
                DtSatartDt.Value = null;
                LESS_RATIO_AVAILED = 0;
                FN_C_RATIO = 0;
                //txtPersonnelNo.Select();
                //txtPersonnelNo.Focus();
                txtExcpRem.Enabled = false;
                txtRmks.Enabled = false;
                txtDisbursmntAmt.Enabled = false;
                txtExcepRemOpt.Enabled = false;
                txtExcpFlg.Enabled = false;
                txtPersonnelNo.Select();
                txtPersonnelNo.Focus();
                // txtPersonnelNo.Focused;
                return;

            }

            if (actionType == "Close")
            {
                this.Quit();
                return;

            }

            base.DoToolbarActions(ctrlsCollection, actionType);

        }
        /// <summary>
        /// Proc 1
        /// </summary>
        //private void NewPROC_1()
        //{
        //    try
        //    {
        //        int intSchedule = int.Parse(txtFnschdl.Text);
        //        int Tenur = int.Parse(txtFnschdl.Text);

        //        if (Tenur != 300 && txtFinanceType.Text.Substring(0, 1) == "H")
        //        {
        //            intSchedule = intSchedule - 2;
        //            txtFnschdl.Text = intSchedule.ToString();
        //        }


        //        double LessRatioAvailed = 0;
        //        double FnRatio = 0;
        //        LessRatioAvailed = PROC_3();
        //        if (txtFnratio.Text == string.Empty)
        //        { FnRatio = Proc_2();
        //        txtFnratio.Text = FnRatio.ToString();
        //        }
        //        Dictionary<string, object> param = new Dictionary<string, object>();
        //        param.Add("PR_P_NO", txtPersonnelNo.Text);
        //        param.Add("PR_LOAN_PACK", this.txtprLoanPack.Text);
        //      //  param.Add("PR_NEW_ANNUAL_PACK", this.txtAnnPackage.Text);
        //        param.Add("LESS_RATIO_AVAILED", LessRatioAvailed.ToString() == "" ? "0" : LessRatioAvailed.ToString());
        //        param.Add("FN_RATIO", txtFnratio.Text);
        //        param.Add("FN_MARKUP", this.txtFnMrkup.Text == "" ? "0" : this.txtFnMrkup.Text);
        //        param.Add("FN_SCHEDULE", this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text);
        //        param.Add("FN_TYPE", this.txtFinanceType.Text);
        //        param.Add("PR_CATEGORY", this.txtCategory.Text);
        //        param.Add("PR_LEVEL", this.txtLevel.Text);




        //        rslt = cmnDM.GetData("CHRIS_SP_FINANCE_APP_PROC_1", "proc1", param);
        //        if (rslt.isSuccessful)
        //        {
        //            if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
        //            {
        //                this.txtCreditRatio.Text    = rslt.dstResult.Tables[0].Rows[0]["CREDIT_RATIO"].ToString();
        //                //this.txtRatioAvail.Text = rslt.dstResult.Tables[0].Rows[0]["RATIO_AVAILED"].ToString();
        //                this.txtAvailable.Text      = rslt.dstResult.Tables[0].Rows[0]["AVAILABLE"].ToString();
        //                this.txtFactor.Text         = rslt.dstResult.Tables[0].Rows[0]["FACTOR"].ToString();
        //                this.txtCanBeAvail.Text     = rslt.dstResult.Tables[0].Rows[0]["AMT_Can_Avail"].ToString();
        //                this.txtCapAmt.Text         = rslt.dstResult.Tables[0].Rows[0]["Cap_Amt"].ToString();
        //              //  this.txtFnratio.Text        = FnRatio.ToString();

        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        LogException(this.Name, "PROC_1", exp);
        //    }
        //}

        private void NewPROC_1()
        {
            try
            {
                int intSchedule = txtFnschdl.Text != String.Empty ? int.Parse(txtFnschdl.Text) : 0;
                int Tenur = txtFnschdl.Text != string.Empty ? int.Parse(txtFnschdl.Text) : 0;

                if ((Tenur != 300 && txtFinanceType.Text.Substring(0, 1) == "H") && sw == 0)
                {
                    intSchedule     = intSchedule - 2;
                    txtFnschdl.Text = intSchedule.ToString();
                    sw = 1;
                }


                double LessRatioAvailed = 0;
                double FnRatio = 0;
                LessRatioAvailed = Math.Round(PROC_3(), 2);

                //if (txtFnratio.Text == string.Empty)
                //if (chkHomeLoan == true)
                {
                    FnRatio         = Proc_2();
                    txtFnratio.Text = FnRatio.ToString();
                }

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txtPersonnelNo.Text);
                param.Add("PR_LOAN_PACK", this.txtprLoanPack.Text);
                //param.Add("PR_NEW_ANNUAL_PACK", this.txtAnnPackage.Text);
                param.Add("LESS_RATIO_AVAILED", LessRatioAvailed.ToString() == "" ? "0" : LessRatioAvailed.ToString());
                param.Add("FN_RATIO", txtFnratio.Text);
                param.Add("FN_MARKUP", this.txtFnMrkup.Text == "" ? "0" : this.txtFnMrkup.Text);
                param.Add("FN_SCHEDULE", this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text);
                param.Add("FN_TYPE", this.txtFinanceType.Text);
                param.Add("PR_CATEGORY", this.txtCategory.Text);
                param.Add("PR_LEVEL", this.txtLevel.Text);

                /*SP Parameter*/
                double PR_P_NO;
                double FN_RATIO;
                double FN_MARKUP = 0;
                if (txtFnMrkup.Text != string.Empty)
                    FN_MARKUP = double.Parse(txtFnMrkup.Text);
                double FN_SCHEDULE = 0;
                if (txtFnschdl.Text != string.Empty)
                    FN_SCHEDULE = double.Parse(txtFnschdl.Text);
                string PR_LEVEL;
                string FN_TYPE = txtFinanceType.Text.Substring(0, 1);

                double PR_CATEGORY;

                /*Declare*/
                FN_RATIO = double.Parse(txtFnratio.Text);
                double CREDIT_RATIO;
                double CREDIT_RATIO_MONTH = 0;
                float COUNTER;
                double AVAILABLE_RATIO = 0;
                double FACTOR;
                double AMT_OF_LOAN_CAN_AVAIL;
                double TOTAL_MONTHLY_INSTALLMENT;
                float CREDIT_RATIO_PER;
                float AUX1;
                float AUX2;
                double AUX3;
                float AUX4;
                float AUX5;
                float AUX7;
                double AUX8;
                float AUX9;
                string FIN;
                float PR_LOAN_PACK = 0;
                float.TryParse(txtprLoanPack.Text, out PR_LOAN_PACK);
                double AMT_OF_LOAN_CAN_AVAIL_CAP = 0;


                AUX2 = 0;
                AUX7 = 0;
                AUX9 = 0;
                float.TryParse(this.txtprLoanPack.Text, out AUX9);

                CREDIT_RATIO = Math.Round(AUX9 / 100.0 * FN_RATIO, 1);
                CREDIT_RATIO_MONTH = Math.Round(CREDIT_RATIO / 12, 2);
                double available = Math.Round(CREDIT_RATIO_MONTH - LessRatioAvailed, 2);
                //available = Math.Round(available, 2);
                txtAvailable.Text = available.ToString();// Convert.ToString(CREDIT_RATIO_MONTH - LESS_RATIO_AVAILED);

                if (FN_MARKUP != 0)
                {
                    AUX8 = (FN_MARKUP / 1200.0);
                    AUX8 = AUX8 + 1;
                    AUX3 = Math.Round(Math.Pow(AUX8, (-FN_SCHEDULE)), 10);
                    FACTOR = (AUX8 - 1) / (1 - AUX3) * 1000;
                }
                else
                {
                    FACTOR = 1000 / FN_SCHEDULE;
                }

                FACTOR = Math.Round(FACTOR, 6);
                txtFactor.Text = FACTOR.ToString();
                txtCreditRatio.Text = CREDIT_RATIO_MONTH.ToString();
                txtRatioAvail.Text = LESS_RATIO_AVAILED.ToString();
                //txtAvailable.Text   = AVAILABLE_RATIO.ToString();

                AMT_OF_LOAN_CAN_AVAIL = Math.Round(available * 1000 / FACTOR, 10);
                txtCanBeAvail.Text = AMT_OF_LOAN_CAN_AVAIL.ToString();


                if (AMT_OF_LOAN_CAN_AVAIL != 0)
                {
                    /*********************** ADD FOR CAP LIMITS *******************/
                    FIN = FN_TYPE;

                    if (FIN == "E")
                    {
                        if (txtCategory.Text == "C")
                        {
                            AMT_OF_LOAN_CAN_AVAIL_CAP = ((PR_LOAN_PACK / 12) * 5);
                            txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                        }
                        else
                        {
                            AMT_OF_LOAN_CAN_AVAIL_CAP = ((PR_LOAN_PACK / 12) * 3);
                            txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                        }
                    }
                    if (FIN == "S")
                    {
                        AMT_OF_LOAN_CAN_AVAIL_CAP = ((PR_LOAN_PACK / 12) * 3);
                        txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                    }

                    if (FIN == "H")
                    {
                        if (txtLevel.Text != "T" && txtLevel.Text != "U" && txtLevel.Text != "V" && txtLevel.Text != "W")
                        {
                            if (FN_MARKUP == 3)
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 2000000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                            else
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 300000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                        }
                        else if (txtLevel.Text == "T")
                        {
                            if (FN_MARKUP == 3)
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 3500000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                            else
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 500000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                        }

                        else if (txtLevel.Text == "U")
                        {
                            if (FN_MARKUP == 3)
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 4000000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                            else
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 1000000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                        }

                        else if (txtLevel.Text == "V")
                        {
                            if (FN_MARKUP == 3)
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 5000000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                            else
                            {
                                AMT_OF_LOAN_CAN_AVAIL_CAP = 1000000;
                                txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                            }
                        }
                    }

                    if (AMT_OF_LOAN_CAN_AVAIL < AMT_OF_LOAN_CAN_AVAIL_CAP)
                    {
                        AMT_OF_LOAN_CAN_AVAIL_CAP = AMT_OF_LOAN_CAN_AVAIL;
                        txtCapAmt.Text = AMT_OF_LOAN_CAN_AVAIL_CAP.ToString();
                    }
                }
                else
                {
                    MessageBox.Show(" THE CREDIT RATIO HAS BEEN FULLY AVAILED .PRESS [ENTER] TO CONT ...", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }




                //txtless

                //rslt = cmnDM.GetData("CHRIS_SP_FINANCE_APP_PROC_1", "proc1", param);
                //if (rslt.isSuccessful)
                //{
                //    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                //    {
                //        this.txtCreditRatio.Text    = rslt.dstResult.Tables[0].Rows[0]["CREDIT_RATIO"].ToString();
                //        //this.txtRatioAvail.Text = rslt.dstResult.Tables[0].Rows[0]["RATIO_AVAILED"].ToString();
                //        this.txtAvailable.Text      = rslt.dstResult.Tables[0].Rows[0]["AVAILABLE"].ToString();
                //        this.txtFactor.Text         = rslt.dstResult.Tables[0].Rows[0]["FACTOR"].ToString();
                //        this.txtCanBeAvail.Text     = rslt.dstResult.Tables[0].Rows[0]["AMT_Can_Avail"].ToString();
                //        this.txtCapAmt.Text         = rslt.dstResult.Tables[0].Rows[0]["Cap_Amt"].ToString();
                //      //  this.txtFnratio.Text        = FnRatio.ToString();




                //    }
                //}
            }
            catch (Exception exp)
            {
                LogException(this.Name, "PROC_1", exp);
            }
        }

        /// <summary>
        /// PROC_3 return LESS_RATIO_AVAILED
        /// </summary>
        /// <returns></returns>
        private double PROC_3()
        {
            try
            {
                LESS_RATIO_AVAILED = 0;

                foreach (DataGridViewRow dr in DGVFinance.Rows)
                {

                    double FN_NEW_INST = 0;

                    if (dr.Cells["FN_NEW_INST"].Value != null && dr.Cells["FN_NEW_INST"].Value.ToString() != "")
                    {
                        FN_NEW_INST = double.Parse(dr.Cells["FN_NEW_INST"].Value.ToString());
                        LESS_RATIO_AVAILED += Math.Round(FN_NEW_INST, 12);
                    }
                }
                this.txtRatioAvail.Text = LESS_RATIO_AVAILED.ToString();

                return LESS_RATIO_AVAILED;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "PROC_3", exp);
                return LESS_RATIO_AVAILED;
            }
        }
        /// <summary>
        /// Proc2 return FN_C_RATIO
        /// </summary>
        /// <returns></returns>
        private double Proc_2()
        {
            try
            {

                int Proc2_count = 0;

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", this.txtPersonnelNo.Text);
                rslt = cmnDM.GetData("CHRIS_SP_PERSONNELFinanceApplicaton_MANAGER", "Proc2_Count", param);
                if (rslt.isSuccessful && rslt.dstResult.Tables[0].Rows.Count > 0)
                    Proc2_count = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                if (Proc2_count == 0)
                {
                    Dictionary<string, object> param1 = new Dictionary<string, object>();
                    param1.Add("FN_TYPE", this.txtFinanceType.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "PROC2_COUNT_IFZERO", param1);
                    if (rslt.isSuccessful && rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        FN_C_RATIO  = double.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        chkHomeLoan = false;
                    }
                }
                else if (Proc2_count > 0)
                {
                    Dictionary<string, object> param2 = new Dictionary<string, object>();
                    param2.Add("FN_TYPE", this.txtFinanceType.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "PROC2_COUNT_IFNONZERO", param2);
                    if (rslt.isSuccessful && rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        FN_C_RATIO  = double.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                        chkHomeLoan = true;
                    }
                }

                return FN_C_RATIO;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "Proc_2", exp);
                return FN_C_RATIO;
            }
        }
        private void RefreshForm()
        {
            base.ClearForm(PnlFinance.Controls);
            base.ClearForm(PnlFinGrid.Controls);
            base.ClearForm(PnlCalAmnt.Controls);
            LESS_RATIO_AVAILED = 0;
            FN_C_RATIO = 0;
        }
        private void PROC_4()
        {

            try
            {

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txtPersonnelNo.Text);
                param.Add("NAME", txtPerName.Text);
                param.Add("PR_LEVEL", this.txtLevel.Text);
                param.Add("pr_desig", txtDesg.Text);
                param.Add("PR_BRANCH", txtBranch.Text);
                param.Add("PR_D_BIRTH", DtBirth.Value);
                param.Add("PR_CATEGORY", this.txtCategory.Text);
                param.Add("PR_LOAN_PACK", this.txtprLoanPack.Text);
                param.Add("PR_NEW_ANNUAL_PACK", this.txtAnnPackage.Text);
                param.Add("pr_joining_date", DtJoining.Value);
                param.Add("available_ratio", txtAvailable.Text);
                param.Add("amt_of_loan_can_avail", txtCanBeAvail.Text);
                param.Add("fn_amt_availed", txtDisbursmntAmt.Text);
                param.Add("FN_SCHEDULE", this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text);
                param.Add("tot_month_inst", txtTotalMonth.Text);
                param.Add("FN_MARKUP", this.txtFnMrkup.Text == "" ? "0" : this.txtFnMrkup.Text);
                param.Add("fn_start_date", dtFN_SDATE.Value);
                param.Add("fn_end_date", DtEndDate.Value);
                param.Add("REMARKS", this.txtRmks.Text);
                param.Add("EXCP_FLAG", txtExcpFlg.Text);
                param.Add("EXCP_REM", txtExcpRem.Text);
                param.Add("pr_nic", txt_Pr_NIC.Text);

                rslt = cmnDM.GetData("CHRIS_SP_FINANCE_APP_PROC_4", "PROC4", param);
                if (rslt.isSuccessful)
                {

                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "PROC_4", exp);
            }



        }



        #region comment

        
        //private void PROC_1()
        //{
        //  int head_sw = 0;

        //    int Fn_schdl = Int32.Parse(this.txtFnschdl.Text);

        //    if ((Fn_schdl != 300) && (this.txtFinanceType.Text.Substring(0, 1) == "H"))  
        //     {

        //         head_sw = 0;
        //         Fn_schdl =- 2;
        //         head_sw = 1;
        //         this.txtFnschdl.Text = Fn_schdl.ToString();

        //     }

        //    PROC_3();
        //    Result rslt;
        //    Dictionary<string, object> param = new Dictionary<string, object>();
        //    param.Add("PR_P_NO", this.txtPersonnelNo.Text);
        //    param.Add("PR_NEW_ANNUAL_PACK", this.txtAnnPackage.Text);
        //    param.Add("FN_RATIO", this.txtFnratio.Text == "" ? "0" : this.txtFnratio.Text);
        //    param.Add("FN_MARKUP", this.txtFnMrkup.Text == "" ? "0" : this.txtFnMrkup.Text);
        //    param.Add("FN_SCHEDULE", this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text);

        //    param.Add("PR_LEVEL", this.txtLevel.Text);

        //    param.Add("FN_TYPE", this.txtFinanceType.Text);
        //    param.Add("PR_LOAN_PACK",this.txtprLoanPack.Text);
        //    param.Add("LESS_RATIO_AVAILED", this.txtRatioAvail.Text == "" ? "0" : this.txtRatioAvail.Text);
        //    param.Add("PR_CATEGORY", this.txtCategory.Text);


        //    rslt = cmnDM.GetData("CHRIS_SP_FINANCE_APPLICATON_PROC_1", "", param);
        //    if (rslt.isSuccessful)
        //    {
        //        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
        //        {
        //            this.txtCreditRatio.Text = rslt.dstResult.Tables[0].Rows[0]["CREDIT_RATIO"].ToString();
        //            this.txtRatioAvail.Text = rslt.dstResult.Tables[0].Rows[0]["RATIO_AVAILED"].ToString();
        //            this.txtAvailable.Text = rslt.dstResult.Tables[0].Rows[0]["AVAILABLE"].ToString();
        //            this.txtFactor.Text = rslt.dstResult.Tables[0].Rows[0]["FACTOR"].ToString();
        //            this.txtCanBeAvail.Text = rslt.dstResult.Tables[0].Rows[0]["AMT_Can_Avail"].ToString();
        //            this.txtCapAmt.Text = rslt.dstResult.Tables[0].Rows[0]["Cap_Amt"].ToString();
        //        }
        //    }
        //}

        #endregion
        #endregion
        # region Events

        /* Trigger used in "New Annual Pack"  Next ITEM  In OracleForm Builder  
         *
         * if Annual Package is text box value is loess than   INSCOVAMT_Limit then assign "800000" to annual package 
         * otherwise multiply it by 3
         */
        private void txtAnnPackage_Validating(object sender, CancelEventArgs e)
        {
            if ((txtAnnPackage.Text != string.Empty) || (txtAnnPackage.Text != ""))
            {
                double Ann_value = double.Parse(txtAnnPackage.Text);
                const int ANN_VALUE_CHK = 3;
                const int INSCOVAMT_Limit = 800000;

                if (Ann_value != 0)
                    if (Ann_value <= 0)
                    {
                        MessageBox.Show("Annual Package Must Be Grater Then Zero......");
                        e.Cancel = true;
                    }

                if ((Ann_value * ANN_VALUE_CHK) < INSCOVAMT_Limit)
                {
                    txtInsCovAmnt.Text = "800000";

                }
                else
                {
                    txtInsCovAmnt.Text = Convert.ToString(Ann_value * ANN_VALUE_CHK);
                }

                txtprLoanPack.Text = txtAnnPackage.Text;
                txtInsCovAmnt.Select();
                txtInsCovAmnt.Focus();
            }
        }

        /* Trigger used in P_RP_NO  Next ITEM  In OracleForm Builder  
         * if category code is equals to "C" then  enable it other wise set it ot custom enabled false
         
         */
        private void CHRIS_Finance_FinanceApplication_AfterLOVSelection(DataRow selectedRow, string actionType)
        {


            //if (actionType == "Pesonnel_FinanceApp")
            //{
            //    txtPersonnelNo.Select();
            //    txtPersonnelNo.Focus();
            //    return;

            //}

            //if (actionType == "FinNo_FinanceApp")
            //{
            //    txtFinanceType.Select();
            //    txtFinanceType.Focus();

            //}


        }

        /* Trigger used in P_RP_NO  Next ITEM  In OracleForm Builder 
         * Fill Personnel Pannel Lov (used when user used Keyboard to tab out ) 
         
         */
        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {
            if (txtCategory.Text == "C")
            {
                txtCBonus.CustomEnabled = true;
                txtCBonus.Enabled = true;
                txtCBonus.ReadOnly = false;
                // txtCBonus.Focus();
            }
            else
            {
                txtCBonus.CustomEnabled = false;
                txtCBonus.Enabled = false;
                txtCBonus.ReadOnly = false;
            }



            ProcLevel();
            sw = 0;


        }

        /* Trigger used in StartDate(Fin Type)  Next ITEM  In OracleForm Builder  
         *  if date is less than the currrent year 
         * then show this message
         */
        private void DtSatartDt_Validating(object sender, CancelEventArgs e)
        {
            if (DtSatartDt.Value == null)
            {
                DtSatartDt.Select();
                DtSatartDt.Focus();
                return;
            }
        }

        /*Trigger called on NEXT ITEM of LIQ_FLG  Fin Type Pannel
         * 
         * Work on Different Conditions of FLGs(L,R,E,A) 
         * */

        private void DGVFinance_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //if (DGVFinance.CurrentRow.Cells["Column9"] != null)
                {
                    if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                    {
                        dtStart = Convert.ToDateTime(DtSatartDt.Value);

                        TimeSpan ts = dtStart.Subtract(dtSdate);

                        int days = ts.Days;

                        //int months = days / 30;
                        int months = MonthsBetweenInOracle(Convert.ToDateTime(DtSatartDt.Value), Convert.ToDateTime(DGVFinance.CurrentRow.Cells["Column9"].Value));

                        #region column 3

                        if (DGVFinance.Rows[e.RowIndex].Cells[3].IsInEditMode)
                        {
                            if (e.ColumnIndex == DGVFinance.Columns["FN_LIQ_FLAG"].Index)
                            {
                                string finFlag = DGVFinance.Rows[e.RowIndex].Cells[3].EditedFormattedValue.ToString().ToUpper();

                                if (finFlag == string.Empty)
                                { }
                                else
                                {

                                    if (finFlag != "L" && finFlag != "R" && finFlag != "E" && finFlag != "A")
                                    {
                                        MessageBox.Show("Please Enter [R] = Reduction Of Finance or [L] = Liqudate");
                                        e.Cancel = true;
                                    }

                                    if (finFlag == "L")
                                    {
                                        if ((months < 12) && ((txtFinanceType.Text.Substring(1, 1) == "E") || (txtFinanceType.Text.Substring(1, 1) == "S")))
                                        {
                                            MessageBox.Show("Exception Loan Disbusment Date is less then One Year");
                                        }

                                        else
                                        {
                                            DGVFinance.CurrentRow.Cells[4].Value = 0;
                                            DGVFinance.CurrentRow.Cells[5].Value = 0;
                                            DGVFinance.CurrentRow.Cells[6].Value = 0;
                                            NewPROC_1();
                                        }
                                        this.DGVFinance.SkippingColumns = "FN_REDUCE_AMT,FN_NEW_INST,FN_NEW_OS_BAL";

                                        //if (!chkF3)
                                        //{
                                        //    e.Cancel = true;
                                        //    //chkF3 = false;
                                        //}

                                    }
                                    else if (finFlag == "E")
                                    {
                                        string Tenure = DGVFinance.CurrentRow.Cells["FN_PAY_LEFT"].EditedFormattedValue.ToString();
                                        MessageBox.Show("Tenur Left is " + Tenure);
                                        DGVFinance.CurrentRow.Cells["FN_REDUCE_AMT"].ReadOnly = true;
                                        DGVFinance.CurrentRow.Cells["FN_NEW_INST"].ReadOnly = true;
                                        DGVFinance.CurrentRow.Cells["FN_NEW_OS_BAL"].ReadOnly = true;
                                        txtFnschdl.Text = Tenure;
                                        //fn_schedule := :fn_new_tenure;
                                        sw = 0;


                                        NewPROC_1();

                                        //this.DGVFinance.SkippingColumns = "FN_REDUCE_AMT,FN_NEW_INST,FN_NEW_OS_BAL";
                                    }
                                    else
                                    {
                                        this.DGVFinance.SkippingColumns = "FN_NEW_INST,FN_NEW_OS_BAL";
                                    }

                                    //      else if (finFlag == "A")
                                    //      { 

                                    //         int rowcount = DGVFinance.RowCount;

                                    //         DGVFinance.CurrentCell= DGVFinance.Rows[2].Cells[3];
                                    ////   DGVFinance.CurrentCell = DGVFinance.Rows[0].Cells[3];
                                    //          DGVFinance.Focus();
                                    //      }

                                }
                            }
                        }

                        #endregion
                        #region  Column 4
                        if (DGVFinance.Rows[e.RowIndex].Cells[4].IsInEditMode)
                        {

                            if (e.ColumnIndex == DGVFinance.Columns["FN_REDUCE_AMT"].Index)
                            {
                                double value;

                                Double Col_Reduce = 0;
                                Double Col_Balance = 0;
                                Double Col_Install = 0;

                                if (double.TryParse(DGVFinance.CurrentRow.Cells[4].EditedFormattedValue.ToString(), out value))
                                {
                                    Col_Reduce = double.Parse(DGVFinance.CurrentRow.Cells[4].EditedFormattedValue.ToString() == "" ? "0" : Convert.ToString(DGVFinance.CurrentRow.Cells[4].EditedFormattedValue.ToString()));
                                }

                                if (double.TryParse(DGVFinance.CurrentRow.Cells[2].EditedFormattedValue.ToString(), out value))
                                {
                                    Col_Balance = Convert.ToDouble(DGVFinance.CurrentRow.Cells[2].EditedFormattedValue.ToString() == "" ? "0" : DGVFinance.CurrentRow.Cells[2].EditedFormattedValue.ToString());

                                }

                                if (double.TryParse(DGVFinance.CurrentRow.Cells["FN_INSTALL"].EditedFormattedValue.ToString(), out value))
                                {
                                    Col_Install = Convert.ToDouble(DGVFinance.CurrentRow.Cells["FN_INSTALL"].EditedFormattedValue.ToString() == "" ? "0" : Convert.ToString(DGVFinance.CurrentRow.Cells["FN_INSTALL"].EditedFormattedValue.ToString()));
                                }


                                if ((Col_Reduce <= 0) || (Col_Reduce >= Col_Balance))
                                {
                                    MessageBox.Show("Reduced Amount Must Be Grater then Zero and Less then O/S Balance ");
                                    e.Cancel = true;
                                }

                                else
                                {
                                    //:fn_new_inst := ((nvl(:fn_install,0)/nvl(:fn_balance,0))* (nvl(:fn_balance,0) - nvl(:fn_reduce_amt,0)));
                                    //:fn_new_os_bal := (nvl(:fn_balance,0) - nvl(:fn_reduce_amt,0));



                                    DGVFinance.CurrentRow.Cells[5].Value = (((Col_Install) / (Col_Balance)) * ((Col_Balance) - (Col_Reduce)));
                                    DGVFinance.CurrentRow.Cells[6].Value = (Col_Balance) - (Col_Reduce);
                                    NewPROC_1();

                                }

                            }

                        }
                        #endregion
                        #region  Column 5
                        if (DGVFinance.Rows[e.RowIndex].Cells[5].IsInEditMode)
                        {


                            if (e.ColumnIndex == DGVFinance.Columns["FN_NEW_INST"].Index)
                            {
                                Double Col_NewInst = 0;
                                double value;
                                if (double.TryParse(DGVFinance.CurrentRow.Cells["FN_NEW_INST"].EditedFormattedValue.ToString(), out value))
                                {
                                    Col_NewInst = Convert.ToDouble(Convert.ToString(DGVFinance.CurrentRow.Cells[5].EditedFormattedValue.ToString()));
                                }
                                if (Col_NewInst <= 0)
                                {
                                    MessageBox.Show("Installment Amount Must Be Grater Then Zero.");
                                    e.Cancel = true;
                                }
                                else
                                {
                                    DGVFinance.CurrentRow.Cells["FN_NEW_INST"].Value = DGVFinance.CurrentRow.Cells["FN_NEW_INST"].EditedFormattedValue;
                                    NewPROC_1();
                                }

                            }
                        }


                        #endregion
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "DGVFinance_CellValidating", exp);
            }
        }

        /*Trigger called on NEXT ITEM of startdate of Fin Type Pannel
        * 
        * Fills the Grid  Accordingly 
        * */
        private void DtSatartDt_Leave(object sender, EventArgs e)
        {
            if (DtSatartDt.Value == null)
            {
                DtSatartDt.Select();
                DtSatartDt.Focus();
                return;
            }

            else
            {

                DateTime dtpStartDate;
                DateTime dtpBirthDate;
                DateTime dtpEndDate;
                DateTime dtTemp;
                dtpStartDate = Convert.ToDateTime(DtSatartDt.Value);
                dtpBirthDate = Convert.ToDateTime(DtBirth.Value);
                //dtTemp = dtpStartDate.AddMonths(int.Parse(txtFnschdl.Text == "" ? "0" : txtFnschdl.Text));
                //TimeSpan ts = dtTemp.Subtract(dtpBirthDate);
                //DateTime dt2 = new DateTime(ts.Ticks);

                //double w_sys = (dt2.Year * 12) + dt2.Month;

                double w_sys = 0;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("pr_d_birth", dtpBirthDate);
                param.Add("fn_schedule", this.txtFnschdl.Text);
                rslt = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "Date_MonthAdd", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        w_sys = double.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                    }
                }


                

                DateTime dtRslt = dtpStartDate.AddMonths(int.Parse(this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text));
                w_sys = base.MonthsBetweenInOracle(dtRslt, dtpBirthDate);

                if (w_sys > 696)
                {
                    w_sys = w_sys - 696.0;
                    int schedule = int.Parse(this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text);
                    double temp = (schedule - w_sys);
                    this.txtFnschdl.Text = temp.ToString();
                }
                //dtpStartDate = Now();

                //dtpStartDate = Now();
                dtpEndDate = dtpStartDate.AddMonths(int.Parse(txtFnschdl.Text == "" ? "0" : txtFnschdl.Text));
                this.DtEndDate.Value = dtpEndDate;
                //  DtEndDate = dtpEndDate.Date;

                //if (ts.Days > 1)
                //{
                //    MessageBox.Show("DATE HAS TO BE ENTERED AND >= THE CURRENT YEAR");

                //}

                this.PROC_0();
                //this.NewPROC_1();


                //DateTime dt;
                //Result rslt;
                //Dictionary<string, object> param = new Dictionary<string, object>();
                //param.Add("PR_P_NO", txtPersonnelNo.Text);
                //rslt = cmnDM.GetData("CHRIS_SP_PERSONNEL_FinApp_MANAGER", "LIST", param);
                //if(rslt.isSuccessful && rslt.dstResult  !=null)
                //{
                //    if (DGVFinance.EndEdit())
                //    {
                //        DGVFinance.DataSource = rslt.dstResult.Tables[0];
                //        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                //            dtSdate = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0][4].ToString());
                //        DGVFinance.CurrentCell = DGVFinance.Rows[0].Cells[3];
                //        DGVFinance.Focus();
                //    }
                //}
            }
        }
        private void DtSatartDt_Validated(object sender, EventArgs e)
        {
            NewPROC_1();
        }
        private void txtAnnPackage_MouseDown(object sender, MouseEventArgs e)
        {
            ActiveControl.Select();
            ActiveControl.Focus();
        }
        private void DGVFinance_KeyDown(object sender, KeyEventArgs e)
        {
            if (DGVFinance.CurrentCell != null && DGVFinance.CurrentCell.OwningColumn.Name.Equals("FN_REDUCE_AMT"))
            {
                if (e.KeyCode == Keys.F3)
                {
                    chkF3 = true;
                }
            } 

        }
        private void txtInsCovAmnt_Validating(object sender, CancelEventArgs e)
        {
            DtSatartDt.Value = this.CurrentDate;

            if (txtCategory.Text == "C")
            {
                txtCBonus.Focus();

            }
            else
            {
                txtFinanceType.Select();
                txtFinanceType.Focus();
            }



        }
        private void txtDisbursmntAmt_Validating(object sender, CancelEventArgs e)
        {
            double FN_AMT_AVAILED = 0;
            double AMT_OF_LOAN_CAN_AVAIL = 0;

            if (txtDisbursmntAmt.Text != string.Empty)
            {
                double value;

                if (double.TryParse(txtDisbursmntAmt.Text, out value))
                {
                    FN_AMT_AVAILED = double.Parse(txtDisbursmntAmt.Text);
                }

                else
                {
                    MessageBox.Show("Field must be of form 999999999.99.");
                    txtDisbursmntAmt.Text = "";
                    txtDisbursmntAmt.Select();
                    txtDisbursmntAmt.Focus();
                    return;
                }


            }
            if (txtCanBeAvail.Text != string.Empty)
                AMT_OF_LOAN_CAN_AVAIL = double.Parse(txtCanBeAvail.Text);

            if (FN_AMT_AVAILED > AMT_OF_LOAN_CAN_AVAIL)
            {
                MessageBox.Show("VALUE HAS TO BE ENTERED & SHOULD BE <" + txtCapAmt.Text);
                e.Cancel = true;
            }
            else
            {

                if (txtFactor.Text != string.Empty)
                {
                    double factor = 0;
                    if (double.TryParse(txtFactor.Text, out factor))
                    {

                        factor = double.Parse(this.txtFactor.Text);
                        // :TOT_MONTH_INST := nvl(:blk_body.FN_AMT_AVAILED,0) * :FACTOR /1000;
                        double value = (FN_AMT_AVAILED * factor / 1000);

                        txtTotalMonth.Text = value.ToString();
                        this.txtExcpFlg.CustomEnabled = true;
                        this.txtExcpFlg.Enabled = true;
                        this.txtExcpFlg.Select();
                        this.txtExcpFlg.Focus();
                        this.txtExcpFlg.Text = "N";
                    }
                }

            }

        }
        private void txtExcpFlg_Validating(object sender, CancelEventArgs e)
        {

            if (!tlbMain.Items["tbtCancel"].Pressed)
            {
                if (txtExcpFlg.Text == string.Empty)
                {
                    MessageBox.Show("Value Must Be Entered  ");
                    e.Cancel = true;

                }

                if (txtExcpFlg.Text != "Y" && txtExcpFlg.Text != "N")
                {
                    MessageBox.Show("Invalid Value . " + "" + " \" Y\"  " + "\" NO\"  " + "" + "Try Again....");
                    e.Cancel = true;
                }
                if (txtExcpFlg.Text == "Y")
                {
                    this.txtExcepRemOpt.CustomEnabled = true;
                    this.txtExcepRemOpt.Enabled = true;
                    this.txtExcepRemOpt.Select();
                    this.txtExcepRemOpt.Focus();

                }

                else if (txtExcpFlg.Text == "N")
                {
                    this.txtExcepRemOpt.Text = "";
                    this.txtExcepRemOpt.CustomEnabled = false;
                    this.txtExcpRem.Text = "";
                    this.txtExcpRem.CustomEnabled = false;

                    this.txtRmks.CustomEnabled = true;
                    this.txtRmks.Enabled = true;
                    this.txtRmks.Select();
                    this.txtRmks.Focus();
                }


            }
        }
        private void txtExcepRemOpt_Validating(object sender, CancelEventArgs e)
        {
            if (txtExcpFlg.Text == "Y" && txtExcepRemOpt.Text == string.Empty)
            {
                MessageBox.Show("'Invalid Value . Valid Options Ares 1 to 4'");
            }


            if (txtExcepRemOpt.Text != "1" && txtExcepRemOpt.Text != "2" && txtExcepRemOpt.Text != "3" && txtExcepRemOpt.Text != "4")
            {
                MessageBox.Show("Invalid Value . Valid Options Ares 1 to 4");
                e.Cancel = true;
            }

            switch (txtExcepRemOpt.Text)
            {
                case "1":
                    txtExcpRem.Text = " Exceed Credit Ratio ";
                    break;
                case "2":
                    txtExcpRem.Text = " Is Less Eligibility Time Frame";
                    break;
                case "3":
                    txtExcpRem.Text = " Unconfirmed Employee";
                    break;
                case "4":
                    txtExcpRem.Text = " Exceed Maximium Cap";
                    break;
            }
            this.txtRmks.CustomEnabled = true;
            this.txtRmks.Enabled = true;
            this.txtRmks.Select();
            this.txtRmks.Focus();

        }
        private void DGVFinance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (DGVFinance.CurrentCell != null && DGVFinance.CurrentCell.OwningColumn.Name.Equals("FN_REDUCE_AMT"))
            {
                //if (e.KeyCode == Keys.F3)
                //{
                //    chkF3 = true;
                //}
            } 
        }
        private void DGVFinance_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //e.Control.KeyPress -= new KeyPressEventHandler(DGVFinance_KeyPress);
            e.Control.KeyPress += new KeyPressEventHandler(DGVFinance_KeyPress);

            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }

        }
        private void txtCBonus_Validating(object sender, CancelEventArgs e)
        {
            this.txtprLoanPack.Text = Convert.ToString(double.Parse(this.txtAnnPackage.Text == "" ? "0" : this.txtAnnPackage.Text) + 2400);
            this.txtprLoanPack.Text = Convert.ToString(double.Parse(this.txtprLoanPack.Text == "" ? "0" : this.txtprLoanPack.Text)
                                + (double.Parse(this.txtprLoanPack.Text == "" ? "0" : this.txtprLoanPack.Text)
                                * double.Parse(this.txtCBonus.Text == "" ? "0" : this.txtCBonus.Text) / 100));
        }
        private void txtRmks_Validating(object sender, CancelEventArgs e)
        {


            if (!tlbMain.Items["tbtCancel"].Pressed)
                //       MessageBox.Show("Press[F10] To Print Application or [F6] To Exit .");
                e.Cancel = true;
        }
        # endregion

        private void slTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (slTextBox1.Text != string.Empty)
            {
                DateTime dt_Birth = new DateTime();
                DateTime.TryParse(slTextBox1.Text, out dt_Birth);
                slTextBox1.Text = dt_Birth.ToString("dd/MM/yyyy");
            }

        }

        private void slTextBox2_TextChanged(object sender, EventArgs e)
        {
            if (slTextBox2.Text != string.Empty)
            {
                DateTime dt_Join = new DateTime();
                DateTime.TryParse(slTextBox2.Text, out dt_Join);
                slTextBox2.Text = dt_Join.ToString("dd/MM/yyyy");
            }

        }

        private void DtSatartDt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (DtSatartDt.Value == null)
                {
                    DtSatartDt.Select();
                    DtSatartDt.Focus();
                    return;
                }
                else
                {
                    DateTime dtpStartDate;
                    DateTime dtpBirthDate;
                    DateTime dtpEndDate;
                    DateTime dtTemp;
                    dtpStartDate = Convert.ToDateTime(DtSatartDt.Value);
                    dtpBirthDate = Convert.ToDateTime(DtBirth.Value);

                    double w_sys = 0;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("pr_d_birth", dtpBirthDate);
                    param.Add("fn_schedule", this.txtFnschdl.Text);
                    rslt = cmnDM.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "Date_MonthAdd", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            w_sys = double.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        }
                    }

                    

                    DateTime dtRslt = dtpStartDate.AddMonths(int.Parse(this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text));
                    w_sys = base.MonthsBetweenInOracle(dtRslt, dtpBirthDate);


                    if (w_sys > 696)
                    {
                        w_sys = w_sys - 696.0;
                        int schedule = int.Parse(this.txtFnschdl.Text == "" ? "0" : this.txtFnschdl.Text);
                        double temp = (schedule - w_sys);
                        this.txtFnschdl.Text = temp.ToString();
                    }

                    dtpEndDate = dtpStartDate.AddMonths(int.Parse(txtFnschdl.Text == "" ? "0" : txtFnschdl.Text));
                    this.DtEndDate.Value = dtpEndDate;
                    this.PROC_0();
                }
            }
        }

        private void DGVFinance_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}