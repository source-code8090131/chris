namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_PerClaimQr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_PerClaimQr));
            this.pnlOmiMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_W_Year = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_ENROLL = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_MARRIAGE_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_W_STATUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_JOINING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_W_CAT = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.dgv = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colOmi_Month = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOMI_SELF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOMI_SPOUSE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COLOMI_CHILD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmi_Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmi_Total_Claim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmi_Total_Recev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmi_P_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmi_TOTAL_CLAIMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_Omi_Child = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txt_Omi_Spouse = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.txt_Omi_Self = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.txt_W_Child = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.txt_W_Spouse = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txt_W_Self = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.txt_W_Os_Balance = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.txt_Omi_Total_Claim = new CrplControlLibrary.SLTextBox(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.txt_W_Omi_Limit = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txt_W_Branch = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txt_W_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txt_W_PNO = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlOmiDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.txt_W_Total_Recv = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Total = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.dgvOmiDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.OMI_MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Bill_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Patient_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Relation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Claim_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Recev_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_P_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Omi_Claim_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTblDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPrDNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSeg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colContr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblUser = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlOmiMaster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.pnlOmiDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOmiDetail)).BeginInit();
            this.pnlTblDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(589, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(625, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 501);
            this.panel1.Size = new System.Drawing.Size(625, 60);
            // 
            // pnlOmiMaster
            // 
            this.pnlOmiMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOmiMaster.ConcurrentPanels = null;
            this.pnlOmiMaster.Controls.Add(this.txt_W_Year);
            this.pnlOmiMaster.Controls.Add(this.txt_ENROLL);
            this.pnlOmiMaster.Controls.Add(this.txt_W_MARRIAGE_DATE);
            this.pnlOmiMaster.Controls.Add(this.txt_W_STATUS);
            this.pnlOmiMaster.Controls.Add(this.txt_W_JOINING_DATE);
            this.pnlOmiMaster.Controls.Add(this.txt_W_CAT);
            this.pnlOmiMaster.Controls.Add(this.lbtnPersonnel);
            this.pnlOmiMaster.Controls.Add(this.dgv);
            this.pnlOmiMaster.Controls.Add(this.label16);
            this.pnlOmiMaster.Controls.Add(this.txt_Omi_Child);
            this.pnlOmiMaster.Controls.Add(this.label17);
            this.pnlOmiMaster.Controls.Add(this.txt_Omi_Spouse);
            this.pnlOmiMaster.Controls.Add(this.label18);
            this.pnlOmiMaster.Controls.Add(this.txt_Omi_Self);
            this.pnlOmiMaster.Controls.Add(this.label12);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Child);
            this.pnlOmiMaster.Controls.Add(this.label14);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Spouse);
            this.pnlOmiMaster.Controls.Add(this.label15);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Self);
            this.pnlOmiMaster.Controls.Add(this.label11);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Os_Balance);
            this.pnlOmiMaster.Controls.Add(this.label10);
            this.pnlOmiMaster.Controls.Add(this.txt_Omi_Total_Claim);
            this.pnlOmiMaster.Controls.Add(this.label9);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Omi_Limit);
            this.pnlOmiMaster.Controls.Add(this.label8);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Branch);
            this.pnlOmiMaster.Controls.Add(this.label6);
            this.pnlOmiMaster.Controls.Add(this.txt_W_Name);
            this.pnlOmiMaster.Controls.Add(this.label7);
            this.pnlOmiMaster.Controls.Add(this.txt_W_PNO);
            this.pnlOmiMaster.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlOmiMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlOmiMaster.DependentPanels = null;
            this.pnlOmiMaster.DisableDependentLoad = false;
            this.pnlOmiMaster.EnableDelete = true;
            this.pnlOmiMaster.EnableInsert = true;
            this.pnlOmiMaster.EnableQuery = false;
            this.pnlOmiMaster.EnableUpdate = true;
            this.pnlOmiMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMCommand";
            this.pnlOmiMaster.Location = new System.Drawing.Point(4, 110);
            this.pnlOmiMaster.MasterPanel = null;
            this.pnlOmiMaster.Name = "pnlOmiMaster";
            this.pnlOmiMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlOmiMaster.Size = new System.Drawing.Size(614, 186);
            this.pnlOmiMaster.SPName = "CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER";
            this.pnlOmiMaster.TabIndex = 9;
            // 
            // txt_W_Year
            // 
            this.txt_W_Year.AllowSpace = true;
            this.txt_W_Year.AssociatedLookUpName = "";
            this.txt_W_Year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Year.ContinuationTextBox = null;
            this.txt_W_Year.CustomEnabled = true;
            this.txt_W_Year.DataFieldMapping = "OMI_YEAR";
            this.txt_W_Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Year.GetRecordsOnUpDownKeys = false;
            this.txt_W_Year.IsDate = false;
            this.txt_W_Year.Location = new System.Drawing.Point(275, 158);
            this.txt_W_Year.MaxLength = 1;
            this.txt_W_Year.Name = "txt_W_Year";
            this.txt_W_Year.NumberFormat = "###,###,##0.00";
            this.txt_W_Year.Postfix = "";
            this.txt_W_Year.Prefix = "";
            this.txt_W_Year.ReadOnly = true;
            this.txt_W_Year.Size = new System.Drawing.Size(36, 20);
            this.txt_W_Year.SkipValidation = false;
            this.txt_W_Year.TabIndex = 104;
            this.txt_W_Year.TabStop = false;
            this.txt_W_Year.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Year.Visible = false;
            // 
            // txt_ENROLL
            // 
            this.txt_ENROLL.AllowSpace = true;
            this.txt_ENROLL.AssociatedLookUpName = "";
            this.txt_ENROLL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ENROLL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ENROLL.ContinuationTextBox = null;
            this.txt_ENROLL.CustomEnabled = true;
            this.txt_ENROLL.DataFieldMapping = "ENROLL";
            this.txt_ENROLL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ENROLL.GetRecordsOnUpDownKeys = false;
            this.txt_ENROLL.IsDate = false;
            this.txt_ENROLL.Location = new System.Drawing.Point(239, 158);
            this.txt_ENROLL.MaxLength = 1;
            this.txt_ENROLL.Name = "txt_ENROLL";
            this.txt_ENROLL.NumberFormat = "###,###,##0.00";
            this.txt_ENROLL.Postfix = "";
            this.txt_ENROLL.Prefix = "";
            this.txt_ENROLL.ReadOnly = true;
            this.txt_ENROLL.Size = new System.Drawing.Size(36, 20);
            this.txt_ENROLL.SkipValidation = false;
            this.txt_ENROLL.TabIndex = 103;
            this.txt_ENROLL.TabStop = false;
            this.txt_ENROLL.TextType = CrplControlLibrary.TextType.String;
            this.txt_ENROLL.Visible = false;
            // 
            // txt_W_MARRIAGE_DATE
            // 
            this.txt_W_MARRIAGE_DATE.CustomEnabled = false;
            this.txt_W_MARRIAGE_DATE.CustomFormat = "dd/MM/yyyy";
            this.txt_W_MARRIAGE_DATE.DataFieldMapping = "W_MARRIAGE_DATE";
            this.txt_W_MARRIAGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_W_MARRIAGE_DATE.HasChanges = false;
            this.txt_W_MARRIAGE_DATE.Location = new System.Drawing.Point(159, 158);
            this.txt_W_MARRIAGE_DATE.Name = "txt_W_MARRIAGE_DATE";
            this.txt_W_MARRIAGE_DATE.NullValue = " ";
            this.txt_W_MARRIAGE_DATE.Size = new System.Drawing.Size(80, 20);
            this.txt_W_MARRIAGE_DATE.TabIndex = 102;
            this.txt_W_MARRIAGE_DATE.TabStop = false;
            this.txt_W_MARRIAGE_DATE.Value = new System.DateTime(2011, 6, 15, 0, 0, 0, 0);
            this.txt_W_MARRIAGE_DATE.Visible = false;
            // 
            // txt_W_STATUS
            // 
            this.txt_W_STATUS.AllowSpace = true;
            this.txt_W_STATUS.AssociatedLookUpName = "";
            this.txt_W_STATUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_STATUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_STATUS.ContinuationTextBox = null;
            this.txt_W_STATUS.CustomEnabled = true;
            this.txt_W_STATUS.DataFieldMapping = "W_STATUS";
            this.txt_W_STATUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_STATUS.GetRecordsOnUpDownKeys = false;
            this.txt_W_STATUS.IsDate = false;
            this.txt_W_STATUS.Location = new System.Drawing.Point(123, 158);
            this.txt_W_STATUS.MaxLength = 1;
            this.txt_W_STATUS.Name = "txt_W_STATUS";
            this.txt_W_STATUS.NumberFormat = "###,###,##0.00";
            this.txt_W_STATUS.Postfix = "";
            this.txt_W_STATUS.Prefix = "";
            this.txt_W_STATUS.ReadOnly = true;
            this.txt_W_STATUS.Size = new System.Drawing.Size(36, 20);
            this.txt_W_STATUS.SkipValidation = false;
            this.txt_W_STATUS.TabIndex = 101;
            this.txt_W_STATUS.TabStop = false;
            this.txt_W_STATUS.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_STATUS.Visible = false;
            // 
            // txt_W_JOINING_DATE
            // 
            this.txt_W_JOINING_DATE.CustomEnabled = false;
            this.txt_W_JOINING_DATE.CustomFormat = "dd/MM/yyyy";
            this.txt_W_JOINING_DATE.DataFieldMapping = "W_JOINING_DATE";
            this.txt_W_JOINING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_W_JOINING_DATE.HasChanges = false;
            this.txt_W_JOINING_DATE.Location = new System.Drawing.Point(43, 158);
            this.txt_W_JOINING_DATE.Name = "txt_W_JOINING_DATE";
            this.txt_W_JOINING_DATE.NullValue = " ";
            this.txt_W_JOINING_DATE.Size = new System.Drawing.Size(80, 20);
            this.txt_W_JOINING_DATE.TabIndex = 100;
            this.txt_W_JOINING_DATE.TabStop = false;
            this.txt_W_JOINING_DATE.Value = new System.DateTime(2011, 6, 15, 0, 0, 0, 0);
            this.txt_W_JOINING_DATE.Visible = false;
            // 
            // txt_W_CAT
            // 
            this.txt_W_CAT.AllowSpace = true;
            this.txt_W_CAT.AssociatedLookUpName = "";
            this.txt_W_CAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_CAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_CAT.ContinuationTextBox = null;
            this.txt_W_CAT.CustomEnabled = true;
            this.txt_W_CAT.DataFieldMapping = "W_CAT";
            this.txt_W_CAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_CAT.GetRecordsOnUpDownKeys = false;
            this.txt_W_CAT.IsDate = false;
            this.txt_W_CAT.Location = new System.Drawing.Point(7, 158);
            this.txt_W_CAT.MaxLength = 1;
            this.txt_W_CAT.Name = "txt_W_CAT";
            this.txt_W_CAT.NumberFormat = "###,###,##0.00";
            this.txt_W_CAT.Postfix = "";
            this.txt_W_CAT.Prefix = "";
            this.txt_W_CAT.ReadOnly = true;
            this.txt_W_CAT.Size = new System.Drawing.Size(36, 20);
            this.txt_W_CAT.SkipValidation = false;
            this.txt_W_CAT.TabIndex = 99;
            this.txt_W_CAT.TabStop = false;
            this.txt_W_CAT.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_CAT.Visible = false;
            // 
            // lbtnPersonnel
            // 
            this.lbtnPersonnel.ActionLOVExists = "LOV_PNO_EXISTS";
            this.lbtnPersonnel.ActionType = "LOV_PNO";
            this.lbtnPersonnel.ConditionalFields = "";
            this.lbtnPersonnel.CustomEnabled = true;
            this.lbtnPersonnel.DataFieldMapping = "";
            this.lbtnPersonnel.DependentLovControls = "";
            this.lbtnPersonnel.HiddenColumns = "";
            this.lbtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnel.Image")));
            this.lbtnPersonnel.LoadDependentEntities = true;
            this.lbtnPersonnel.Location = new System.Drawing.Point(165, 10);
            this.lbtnPersonnel.LookUpTitle = null;
            this.lbtnPersonnel.Name = "lbtnPersonnel";
            this.lbtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnel.SkipValidationOnLeave = false;
            this.lbtnPersonnel.SPName = "CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER";
            this.lbtnPersonnel.TabIndex = 97;
            this.lbtnPersonnel.TabStop = false;
            this.lbtnPersonnel.UseVisualStyleBackColor = true;
            // 
            // dgv
            // 
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colOmi_Month,
            this.colOMI_SELF,
            this.colOMI_SPOUSE,
            this.COLOMI_CHILD,
            this.colOmi_Year,
            this.colOmi_Total_Claim,
            this.colOmi_Total_Recev,
            this.colOmi_P_No,
            this.colOmi_TOTAL_CLAIMS});
            this.dgv.ColumnToHide = null;
            this.dgv.ColumnWidth = null;
            this.dgv.CustomEnabled = true;
            this.dgv.DisplayColumnWrapper = null;
            this.dgv.GridDefaultRow = 0;
            this.dgv.Location = new System.Drawing.Point(470, 156);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnlyColumns = null;
            this.dgv.RequiredColumns = null;
            this.dgv.Size = new System.Drawing.Size(61, 25);
            this.dgv.SkippingColumns = null;
            this.dgv.TabIndex = 1;
            this.dgv.Visible = false;
            // 
            // colOmi_Month
            // 
            this.colOmi_Month.DataPropertyName = "Omi_Month";
            this.colOmi_Month.HeaderText = "Month";
            this.colOmi_Month.Name = "colOmi_Month";
            this.colOmi_Month.Width = 50;
            // 
            // colOMI_SELF
            // 
            this.colOMI_SELF.DataPropertyName = "OMI_SELF";
            this.colOMI_SELF.HeaderText = "OMI_SELF";
            this.colOMI_SELF.Name = "colOMI_SELF";
            this.colOMI_SELF.Visible = false;
            // 
            // colOMI_SPOUSE
            // 
            this.colOMI_SPOUSE.DataPropertyName = "OMI_SPOUSE";
            this.colOMI_SPOUSE.HeaderText = "OMI_SPOUSE";
            this.colOMI_SPOUSE.Name = "colOMI_SPOUSE";
            this.colOMI_SPOUSE.Visible = false;
            // 
            // COLOMI_CHILD
            // 
            this.COLOMI_CHILD.DataPropertyName = "OMI_CHILD";
            this.COLOMI_CHILD.HeaderText = "OMI_CHILD";
            this.COLOMI_CHILD.Name = "COLOMI_CHILD";
            this.COLOMI_CHILD.Visible = false;
            // 
            // colOmi_Year
            // 
            this.colOmi_Year.DataPropertyName = "Omi_Year";
            this.colOmi_Year.HeaderText = "Year";
            this.colOmi_Year.Name = "colOmi_Year";
            this.colOmi_Year.Width = 60;
            // 
            // colOmi_Total_Claim
            // 
            this.colOmi_Total_Claim.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colOmi_Total_Claim.DataPropertyName = "Omi_Total_Claim";
            this.colOmi_Total_Claim.HeaderText = "Total Claim";
            this.colOmi_Total_Claim.Name = "colOmi_Total_Claim";
            // 
            // colOmi_Total_Recev
            // 
            this.colOmi_Total_Recev.DataPropertyName = "Omi_Total_Recev";
            this.colOmi_Total_Recev.HeaderText = "Total Recived";
            this.colOmi_Total_Recev.Name = "colOmi_Total_Recev";
            this.colOmi_Total_Recev.Width = 70;
            // 
            // colOmi_P_No
            // 
            this.colOmi_P_No.DataPropertyName = "Omi_P_No";
            this.colOmi_P_No.HeaderText = "OMI_P_NO";
            this.colOmi_P_No.Name = "colOmi_P_No";
            this.colOmi_P_No.Visible = false;
            // 
            // colOmi_TOTAL_CLAIMS
            // 
            this.colOmi_TOTAL_CLAIMS.DataPropertyName = "Omi_TOTAL_CLAIMS";
            this.colOmi_TOTAL_CLAIMS.HeaderText = "Omi_TOTAL_CLAIMS";
            this.colOmi_TOTAL_CLAIMS.Name = "colOmi_TOTAL_CLAIMS";
            this.colOmi_TOTAL_CLAIMS.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(474, 142);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 96;
            this.label16.Text = "Childern";
            // 
            // txt_Omi_Child
            // 
            this.txt_Omi_Child.AllowSpace = true;
            this.txt_Omi_Child.AssociatedLookUpName = "";
            this.txt_Omi_Child.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Omi_Child.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Omi_Child.ContinuationTextBox = null;
            this.txt_Omi_Child.CustomEnabled = true;
            this.txt_Omi_Child.DataFieldMapping = "Omi_Child";
            this.txt_Omi_Child.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Omi_Child.GetRecordsOnUpDownKeys = false;
            this.txt_Omi_Child.IsDate = false;
            this.txt_Omi_Child.Location = new System.Drawing.Point(533, 137);
            this.txt_Omi_Child.MaxLength = 1;
            this.txt_Omi_Child.Name = "txt_Omi_Child";
            this.txt_Omi_Child.NumberFormat = "###,###,##0.00";
            this.txt_Omi_Child.Postfix = "";
            this.txt_Omi_Child.Prefix = "";
            this.txt_Omi_Child.ReadOnly = true;
            this.txt_Omi_Child.Size = new System.Drawing.Size(71, 20);
            this.txt_Omi_Child.SkipValidation = false;
            this.txt_Omi_Child.TabIndex = 95;
            this.txt_Omi_Child.TabStop = false;
            this.txt_Omi_Child.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(478, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 94;
            this.label17.Text = "Spouse";
            // 
            // txt_Omi_Spouse
            // 
            this.txt_Omi_Spouse.AllowSpace = true;
            this.txt_Omi_Spouse.AssociatedLookUpName = "";
            this.txt_Omi_Spouse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Omi_Spouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Omi_Spouse.ContinuationTextBox = null;
            this.txt_Omi_Spouse.CustomEnabled = true;
            this.txt_Omi_Spouse.DataFieldMapping = "Omi_Spouse";
            this.txt_Omi_Spouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Omi_Spouse.GetRecordsOnUpDownKeys = false;
            this.txt_Omi_Spouse.IsDate = false;
            this.txt_Omi_Spouse.Location = new System.Drawing.Point(533, 117);
            this.txt_Omi_Spouse.MaxLength = 1;
            this.txt_Omi_Spouse.Name = "txt_Omi_Spouse";
            this.txt_Omi_Spouse.NumberFormat = "###,###,##0.00";
            this.txt_Omi_Spouse.Postfix = "";
            this.txt_Omi_Spouse.Prefix = "";
            this.txt_Omi_Spouse.ReadOnly = true;
            this.txt_Omi_Spouse.Size = new System.Drawing.Size(71, 20);
            this.txt_Omi_Spouse.SkipValidation = false;
            this.txt_Omi_Spouse.TabIndex = 93;
            this.txt_Omi_Spouse.TabStop = false;
            this.txt_Omi_Spouse.TextType = CrplControlLibrary.TextType.String;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(498, 100);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 92;
            this.label18.Text = "Self";
            // 
            // txt_Omi_Self
            // 
            this.txt_Omi_Self.AllowSpace = true;
            this.txt_Omi_Self.AssociatedLookUpName = "";
            this.txt_Omi_Self.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Omi_Self.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Omi_Self.ContinuationTextBox = null;
            this.txt_Omi_Self.CustomEnabled = true;
            this.txt_Omi_Self.DataFieldMapping = "Omi_Self";
            this.txt_Omi_Self.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Omi_Self.GetRecordsOnUpDownKeys = false;
            this.txt_Omi_Self.IsDate = false;
            this.txt_Omi_Self.Location = new System.Drawing.Point(533, 97);
            this.txt_Omi_Self.MaxLength = 1;
            this.txt_Omi_Self.Name = "txt_Omi_Self";
            this.txt_Omi_Self.NumberFormat = "###,###,##0.00";
            this.txt_Omi_Self.Postfix = "";
            this.txt_Omi_Self.Prefix = "";
            this.txt_Omi_Self.ReadOnly = true;
            this.txt_Omi_Self.Size = new System.Drawing.Size(71, 20);
            this.txt_Omi_Self.SkipValidation = false;
            this.txt_Omi_Self.TabIndex = 91;
            this.txt_Omi_Self.TabStop = false;
            this.txt_Omi_Self.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(335, 141);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 90;
            this.label12.Text = "Childern";
            // 
            // txt_W_Child
            // 
            this.txt_W_Child.AllowSpace = true;
            this.txt_W_Child.AssociatedLookUpName = "";
            this.txt_W_Child.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Child.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Child.ContinuationTextBox = null;
            this.txt_W_Child.CustomEnabled = true;
            this.txt_W_Child.DataFieldMapping = "W_Child";
            this.txt_W_Child.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Child.GetRecordsOnUpDownKeys = false;
            this.txt_W_Child.IsDate = false;
            this.txt_W_Child.Location = new System.Drawing.Point(394, 137);
            this.txt_W_Child.MaxLength = 1;
            this.txt_W_Child.Name = "txt_W_Child";
            this.txt_W_Child.NumberFormat = "###,###,##0.00";
            this.txt_W_Child.Postfix = "";
            this.txt_W_Child.Prefix = "";
            this.txt_W_Child.ReadOnly = true;
            this.txt_W_Child.Size = new System.Drawing.Size(72, 20);
            this.txt_W_Child.SkipValidation = false;
            this.txt_W_Child.TabIndex = 89;
            this.txt_W_Child.TabStop = false;
            this.txt_W_Child.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(339, 120);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 88;
            this.label14.Text = "Spouse";
            // 
            // txt_W_Spouse
            // 
            this.txt_W_Spouse.AllowSpace = true;
            this.txt_W_Spouse.AssociatedLookUpName = "";
            this.txt_W_Spouse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Spouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Spouse.ContinuationTextBox = null;
            this.txt_W_Spouse.CustomEnabled = true;
            this.txt_W_Spouse.DataFieldMapping = "W_Spouse";
            this.txt_W_Spouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Spouse.GetRecordsOnUpDownKeys = false;
            this.txt_W_Spouse.IsDate = false;
            this.txt_W_Spouse.Location = new System.Drawing.Point(394, 117);
            this.txt_W_Spouse.MaxLength = 1;
            this.txt_W_Spouse.Name = "txt_W_Spouse";
            this.txt_W_Spouse.NumberFormat = "###,###,##0.00";
            this.txt_W_Spouse.Postfix = "";
            this.txt_W_Spouse.Prefix = "";
            this.txt_W_Spouse.ReadOnly = true;
            this.txt_W_Spouse.Size = new System.Drawing.Size(72, 20);
            this.txt_W_Spouse.SkipValidation = false;
            this.txt_W_Spouse.TabIndex = 87;
            this.txt_W_Spouse.TabStop = false;
            this.txt_W_Spouse.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(359, 99);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 86;
            this.label15.Text = "Self";
            // 
            // txt_W_Self
            // 
            this.txt_W_Self.AllowSpace = true;
            this.txt_W_Self.AssociatedLookUpName = "";
            this.txt_W_Self.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Self.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Self.ContinuationTextBox = null;
            this.txt_W_Self.CustomEnabled = true;
            this.txt_W_Self.DataFieldMapping = "W_Self";
            this.txt_W_Self.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Self.GetRecordsOnUpDownKeys = false;
            this.txt_W_Self.IsDate = false;
            this.txt_W_Self.Location = new System.Drawing.Point(394, 97);
            this.txt_W_Self.MaxLength = 1;
            this.txt_W_Self.Name = "txt_W_Self";
            this.txt_W_Self.NumberFormat = "###,###,##0.00";
            this.txt_W_Self.Postfix = "";
            this.txt_W_Self.Prefix = "";
            this.txt_W_Self.ReadOnly = true;
            this.txt_W_Self.Size = new System.Drawing.Size(72, 20);
            this.txt_W_Self.SkipValidation = false;
            this.txt_W_Self.TabIndex = 85;
            this.txt_W_Self.TabStop = false;
            this.txt_W_Self.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(24, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 13);
            this.label11.TabIndex = 84;
            this.label11.Text = "Outstanding Balance";
            // 
            // txt_W_Os_Balance
            // 
            this.txt_W_Os_Balance.AllowSpace = true;
            this.txt_W_Os_Balance.AssociatedLookUpName = "";
            this.txt_W_Os_Balance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Os_Balance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Os_Balance.ContinuationTextBox = null;
            this.txt_W_Os_Balance.CustomEnabled = true;
            this.txt_W_Os_Balance.DataFieldMapping = "W_Os_Balance";
            this.txt_W_Os_Balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Os_Balance.GetRecordsOnUpDownKeys = false;
            this.txt_W_Os_Balance.IsDate = false;
            this.txt_W_Os_Balance.Location = new System.Drawing.Point(156, 132);
            this.txt_W_Os_Balance.MaxLength = 1;
            this.txt_W_Os_Balance.Name = "txt_W_Os_Balance";
            this.txt_W_Os_Balance.NumberFormat = "###,###,##0.00";
            this.txt_W_Os_Balance.Postfix = "";
            this.txt_W_Os_Balance.Prefix = "";
            this.txt_W_Os_Balance.ReadOnly = true;
            this.txt_W_Os_Balance.Size = new System.Drawing.Size(84, 20);
            this.txt_W_Os_Balance.SkipValidation = false;
            this.txt_W_Os_Balance.TabIndex = 83;
            this.txt_W_Os_Balance.TabStop = false;
            this.txt_W_Os_Balance.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(4, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 13);
            this.label10.TabIndex = 82;
            this.label10.Text = "Total Amt Claimed/Recv";
            // 
            // txt_Omi_Total_Claim
            // 
            this.txt_Omi_Total_Claim.AllowSpace = true;
            this.txt_Omi_Total_Claim.AssociatedLookUpName = "";
            this.txt_Omi_Total_Claim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Omi_Total_Claim.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Omi_Total_Claim.ContinuationTextBox = null;
            this.txt_Omi_Total_Claim.CustomEnabled = true;
            this.txt_Omi_Total_Claim.DataFieldMapping = "Omi_Total_Claim";
            this.txt_Omi_Total_Claim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Omi_Total_Claim.GetRecordsOnUpDownKeys = false;
            this.txt_Omi_Total_Claim.IsDate = false;
            this.txt_Omi_Total_Claim.Location = new System.Drawing.Point(156, 112);
            this.txt_Omi_Total_Claim.MaxLength = 1;
            this.txt_Omi_Total_Claim.Name = "txt_Omi_Total_Claim";
            this.txt_Omi_Total_Claim.NumberFormat = "###,###,##0.00";
            this.txt_Omi_Total_Claim.Postfix = "";
            this.txt_Omi_Total_Claim.Prefix = "";
            this.txt_Omi_Total_Claim.ReadOnly = true;
            this.txt_Omi_Total_Claim.Size = new System.Drawing.Size(84, 20);
            this.txt_Omi_Total_Claim.SkipValidation = false;
            this.txt_Omi_Total_Claim.TabIndex = 81;
            this.txt_Omi_Total_Claim.TabStop = false;
            this.txt_Omi_Total_Claim.TextType = CrplControlLibrary.TextType.String;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(56, 95);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 80;
            this.label9.Text = "Total OMI Limit";
            // 
            // txt_W_Omi_Limit
            // 
            this.txt_W_Omi_Limit.AllowSpace = true;
            this.txt_W_Omi_Limit.AssociatedLookUpName = "";
            this.txt_W_Omi_Limit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Omi_Limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Omi_Limit.ContinuationTextBox = null;
            this.txt_W_Omi_Limit.CustomEnabled = true;
            this.txt_W_Omi_Limit.DataFieldMapping = "W_Omi_Limit";
            this.txt_W_Omi_Limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Omi_Limit.GetRecordsOnUpDownKeys = false;
            this.txt_W_Omi_Limit.IsDate = false;
            this.txt_W_Omi_Limit.Location = new System.Drawing.Point(156, 92);
            this.txt_W_Omi_Limit.MaxLength = 1;
            this.txt_W_Omi_Limit.Name = "txt_W_Omi_Limit";
            this.txt_W_Omi_Limit.NumberFormat = "###,###,##0.00";
            this.txt_W_Omi_Limit.Postfix = "";
            this.txt_W_Omi_Limit.Prefix = "";
            this.txt_W_Omi_Limit.ReadOnly = true;
            this.txt_W_Omi_Limit.Size = new System.Drawing.Size(84, 20);
            this.txt_W_Omi_Limit.SkipValidation = false;
            this.txt_W_Omi_Limit.TabIndex = 79;
            this.txt_W_Omi_Limit.TabStop = false;
            this.txt_W_Omi_Limit.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Omi_Limit.Enter += new System.EventHandler(this.txt_W_Omi_Limit_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(47, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 78;
            this.label8.Text = "Branch";
            // 
            // txt_W_Branch
            // 
            this.txt_W_Branch.AllowSpace = true;
            this.txt_W_Branch.AssociatedLookUpName = "";
            this.txt_W_Branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Branch.ContinuationTextBox = null;
            this.txt_W_Branch.CustomEnabled = true;
            this.txt_W_Branch.DataFieldMapping = "W_Branch";
            this.txt_W_Branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Branch.GetRecordsOnUpDownKeys = false;
            this.txt_W_Branch.IsDate = false;
            this.txt_W_Branch.Location = new System.Drawing.Point(99, 54);
            this.txt_W_Branch.MaxLength = 1;
            this.txt_W_Branch.Name = "txt_W_Branch";
            this.txt_W_Branch.NumberFormat = "###,###,##0.00";
            this.txt_W_Branch.Postfix = "";
            this.txt_W_Branch.Prefix = "";
            this.txt_W_Branch.ReadOnly = true;
            this.txt_W_Branch.Size = new System.Drawing.Size(141, 20);
            this.txt_W_Branch.SkipValidation = false;
            this.txt_W_Branch.TabIndex = 77;
            this.txt_W_Branch.TabStop = false;
            this.txt_W_Branch.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(55, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 76;
            this.label6.Text = "Name";
            // 
            // txt_W_Name
            // 
            this.txt_W_Name.AllowSpace = true;
            this.txt_W_Name.AssociatedLookUpName = "";
            this.txt_W_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Name.ContinuationTextBox = null;
            this.txt_W_Name.CustomEnabled = true;
            this.txt_W_Name.DataFieldMapping = "W_Name";
            this.txt_W_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Name.GetRecordsOnUpDownKeys = false;
            this.txt_W_Name.IsDate = false;
            this.txt_W_Name.Location = new System.Drawing.Point(99, 34);
            this.txt_W_Name.MaxLength = 1;
            this.txt_W_Name.Name = "txt_W_Name";
            this.txt_W_Name.NumberFormat = "###,###,##0.00";
            this.txt_W_Name.Postfix = "";
            this.txt_W_Name.Prefix = "";
            this.txt_W_Name.ReadOnly = true;
            this.txt_W_Name.Size = new System.Drawing.Size(185, 20);
            this.txt_W_Name.SkipValidation = false;
            this.txt_W_Name.TabIndex = 75;
            this.txt_W_Name.TabStop = false;
            this.txt_W_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 74;
            this.label7.Text = "Personnel No.";
            // 
            // txt_W_PNO
            // 
            this.txt_W_PNO.AllowSpace = true;
            this.txt_W_PNO.AssociatedLookUpName = "lbtnPersonnel";
            this.txt_W_PNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_PNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_PNO.ContinuationTextBox = null;
            this.txt_W_PNO.CustomEnabled = true;
            this.txt_W_PNO.DataFieldMapping = "OMI_P_NO";
            this.txt_W_PNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_PNO.GetRecordsOnUpDownKeys = false;
            this.txt_W_PNO.IsDate = false;
            this.txt_W_PNO.Location = new System.Drawing.Point(99, 14);
            this.txt_W_PNO.MaxLength = 5;
            this.txt_W_PNO.Name = "txt_W_PNO";
            this.txt_W_PNO.NumberFormat = "###,###,##0.00";
            this.txt_W_PNO.Postfix = "";
            this.txt_W_PNO.Prefix = "";
            this.txt_W_PNO.Size = new System.Drawing.Size(60, 20);
            this.txt_W_PNO.SkipValidation = false;
            this.txt_W_PNO.TabIndex = 73;
            this.txt_W_PNO.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_PNO.Validated += new System.EventHandler(this.txt_W_PNO_Validated);
            this.txt_W_PNO.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_PNO_Validating);
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(501, 25);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(82, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 55;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(73, 3);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(82, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 53;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(262, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 60;
            this.label5.Text = "OMI CLAIM QUERY";
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(73, 25);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(82, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 54;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(250, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 13);
            this.label4.TabIndex = 59;
            this.label4.Text = "OUT PATIENT MEDICAL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "User  :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(458, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 58;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Location :";
            // 
            // pnlOmiDetail
            // 
            this.pnlOmiDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOmiDetail.ConcurrentPanels = null;
            this.pnlOmiDetail.Controls.Add(this.txt_W_Total_Recv);
            this.pnlOmiDetail.Controls.Add(this.txt_W_Total);
            this.pnlOmiDetail.Controls.Add(this.label19);
            this.pnlOmiDetail.Controls.Add(this.dgvOmiDetail);
            this.pnlOmiDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlOmiDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlOmiDetail.DependentPanels = null;
            this.pnlOmiDetail.DisableDependentLoad = false;
            this.pnlOmiDetail.EnableDelete = true;
            this.pnlOmiDetail.EnableInsert = true;
            this.pnlOmiDetail.EnableQuery = false;
            this.pnlOmiDetail.EnableUpdate = true;
            this.pnlOmiDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMDETAILCommand";
            this.pnlOmiDetail.Location = new System.Drawing.Point(4, 296);
            this.pnlOmiDetail.MasterPanel = null;
            this.pnlOmiDetail.Name = "pnlOmiDetail";
            this.pnlOmiDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlOmiDetail.Size = new System.Drawing.Size(614, 203);
            this.pnlOmiDetail.SPName = "CHRIS_SP_PreClaim_OMI_CLAIM_DETAIL_MANAGER";
            this.pnlOmiDetail.TabIndex = 10;
            // 
            // txt_W_Total_Recv
            // 
            this.txt_W_Total_Recv.AllowSpace = true;
            this.txt_W_Total_Recv.AssociatedLookUpName = "";
            this.txt_W_Total_Recv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Total_Recv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Total_Recv.ContinuationTextBox = null;
            this.txt_W_Total_Recv.CustomEnabled = true;
            this.txt_W_Total_Recv.DataFieldMapping = "W_Total_Recv";
            this.txt_W_Total_Recv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Total_Recv.GetRecordsOnUpDownKeys = false;
            this.txt_W_Total_Recv.IsDate = false;
            this.txt_W_Total_Recv.Location = new System.Drawing.Point(547, 176);
            this.txt_W_Total_Recv.MaxLength = 1;
            this.txt_W_Total_Recv.Name = "txt_W_Total_Recv";
            this.txt_W_Total_Recv.NumberFormat = "###,###,##0.00";
            this.txt_W_Total_Recv.Postfix = "";
            this.txt_W_Total_Recv.Prefix = "";
            this.txt_W_Total_Recv.ReadOnly = true;
            this.txt_W_Total_Recv.Size = new System.Drawing.Size(60, 20);
            this.txt_W_Total_Recv.SkipValidation = false;
            this.txt_W_Total_Recv.TabIndex = 98;
            this.txt_W_Total_Recv.TabStop = false;
            this.txt_W_Total_Recv.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Total
            // 
            this.txt_W_Total.AllowSpace = true;
            this.txt_W_Total.AssociatedLookUpName = "";
            this.txt_W_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Total.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Total.ContinuationTextBox = null;
            this.txt_W_Total.CustomEnabled = true;
            this.txt_W_Total.DataFieldMapping = "W_Total";
            this.txt_W_Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Total.GetRecordsOnUpDownKeys = false;
            this.txt_W_Total.IsDate = false;
            this.txt_W_Total.Location = new System.Drawing.Point(481, 176);
            this.txt_W_Total.MaxLength = 1;
            this.txt_W_Total.Name = "txt_W_Total";
            this.txt_W_Total.NumberFormat = "###,###,##0.00";
            this.txt_W_Total.Postfix = "";
            this.txt_W_Total.Prefix = "";
            this.txt_W_Total.ReadOnly = true;
            this.txt_W_Total.Size = new System.Drawing.Size(60, 20);
            this.txt_W_Total.SkipValidation = false;
            this.txt_W_Total.TabIndex = 97;
            this.txt_W_Total.TabStop = false;
            this.txt_W_Total.TextType = CrplControlLibrary.TextType.String;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(428, 180);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 97;
            this.label19.Text = "Total";
            // 
            // dgvOmiDetail
            // 
            this.dgvOmiDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOmiDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OMI_MONTH,
            this.s_no,
            this.Omi_Year,
            this.Omi_Bill_Date,
            this.Omi_Patient_Name,
            this.Omi_Relation,
            this.Omi_Claim_Amount,
            this.Omi_Recev_Amount,
            this.Omi_P_No,
            this.Omi_Claim_No});
            this.dgvOmiDetail.ColumnToHide = null;
            this.dgvOmiDetail.ColumnWidth = null;
            this.dgvOmiDetail.CustomEnabled = true;
            this.dgvOmiDetail.DisplayColumnWrapper = null;
            this.dgvOmiDetail.GridDefaultRow = 0;
            this.dgvOmiDetail.Location = new System.Drawing.Point(4, 3);
            this.dgvOmiDetail.Name = "dgvOmiDetail";
            this.dgvOmiDetail.ReadOnlyColumns = null;
            this.dgvOmiDetail.RequiredColumns = null;
            this.dgvOmiDetail.Size = new System.Drawing.Size(598, 169);
            this.dgvOmiDetail.SkippingColumns = null;
            this.dgvOmiDetail.TabIndex = 0;
            // 
            // OMI_MONTH
            // 
            this.OMI_MONTH.DataPropertyName = "OMI_MONTH";
            this.OMI_MONTH.HeaderText = "Mth";
            this.OMI_MONTH.Name = "OMI_MONTH";
            this.OMI_MONTH.ReadOnly = true;
            this.OMI_MONTH.Width = 40;
            // 
            // s_no
            // 
            this.s_no.DataPropertyName = "s_no";
            this.s_no.HeaderText = "s_no";
            this.s_no.Name = "s_no";
            this.s_no.Visible = false;
            // 
            // Omi_Year
            // 
            this.Omi_Year.DataPropertyName = "Omi_Year";
            this.Omi_Year.HeaderText = "Yr";
            this.Omi_Year.Name = "Omi_Year";
            this.Omi_Year.ReadOnly = true;
            this.Omi_Year.Width = 50;
            // 
            // Omi_Bill_Date
            // 
            this.Omi_Bill_Date.DataPropertyName = "Omi_Bill_Date";
            this.Omi_Bill_Date.HeaderText = "Bill Date";
            this.Omi_Bill_Date.Name = "Omi_Bill_Date";
            this.Omi_Bill_Date.ReadOnly = true;
            this.Omi_Bill_Date.Width = 80;
            // 
            // Omi_Patient_Name
            // 
            this.Omi_Patient_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Omi_Patient_Name.DataPropertyName = "Omi_Patient_Name";
            this.Omi_Patient_Name.HeaderText = "Patient Name";
            this.Omi_Patient_Name.Name = "Omi_Patient_Name";
            this.Omi_Patient_Name.ReadOnly = true;
            // 
            // Omi_Relation
            // 
            this.Omi_Relation.DataPropertyName = "Omi_Relation";
            this.Omi_Relation.HeaderText = "Relation";
            this.Omi_Relation.Name = "Omi_Relation";
            this.Omi_Relation.ReadOnly = true;
            // 
            // Omi_Claim_Amount
            // 
            this.Omi_Claim_Amount.DataPropertyName = "Omi_Claim_Amount";
            this.Omi_Claim_Amount.HeaderText = "Claim Amount";
            this.Omi_Claim_Amount.Name = "Omi_Claim_Amount";
            this.Omi_Claim_Amount.ReadOnly = true;
            this.Omi_Claim_Amount.Width = 70;
            // 
            // Omi_Recev_Amount
            // 
            this.Omi_Recev_Amount.DataPropertyName = "Omi_Recev_Amount";
            this.Omi_Recev_Amount.HeaderText = "Recv";
            this.Omi_Recev_Amount.Name = "Omi_Recev_Amount";
            this.Omi_Recev_Amount.ReadOnly = true;
            this.Omi_Recev_Amount.Width = 70;
            // 
            // Omi_P_No
            // 
            this.Omi_P_No.DataPropertyName = "Omi_P_No";
            this.Omi_P_No.HeaderText = "Omi_P_No";
            this.Omi_P_No.Name = "Omi_P_No";
            this.Omi_P_No.ReadOnly = true;
            this.Omi_P_No.Visible = false;
            // 
            // Omi_Claim_No
            // 
            this.Omi_Claim_No.DataPropertyName = "Omi_Claim_No";
            this.Omi_Claim_No.HeaderText = "Omi_Claim_No";
            this.Omi_Claim_No.Name = "Omi_Claim_No";
            this.Omi_Claim_No.ReadOnly = true;
            this.Omi_Claim_No.Visible = false;
            // 
            // pnlTblDept
            // 
            this.pnlTblDept.ConcurrentPanels = null;
            this.pnlTblDept.Controls.Add(this.dgvDept);
            this.pnlTblDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept.DependentPanels = null;
            this.pnlTblDept.DisableDependentLoad = false;
            this.pnlTblDept.EnableDelete = true;
            this.pnlTblDept.EnableInsert = true;
            this.pnlTblDept.EnableQuery = false;
            this.pnlTblDept.EnableUpdate = true;
            this.pnlTblDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptContCommand";
            this.pnlTblDept.Location = new System.Drawing.Point(346, 116);
            this.pnlTblDept.MasterPanel = null;
            this.pnlTblDept.Name = "pnlTblDept";
            this.pnlTblDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept.Size = new System.Drawing.Size(260, 85);
            this.pnlTblDept.SPName = "CHRIS_SP_OMIDE05_DEPT_CONT_MANAGER";
            this.pnlTblDept.TabIndex = 111;
            // 
            // dgvDept
            // 
            this.dgvDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPrDNo,
            this.colSeg,
            this.colDept,
            this.colContr});
            this.dgvDept.ColumnToHide = null;
            this.dgvDept.ColumnWidth = null;
            this.dgvDept.CustomEnabled = true;
            this.dgvDept.DisplayColumnWrapper = null;
            this.dgvDept.GridDefaultRow = 0;
            this.dgvDept.Location = new System.Drawing.Point(3, 0);
            this.dgvDept.Name = "dgvDept";
            this.dgvDept.ReadOnlyColumns = null;
            this.dgvDept.RequiredColumns = null;
            this.dgvDept.Size = new System.Drawing.Size(253, 81);
            this.dgvDept.SkippingColumns = null;
            this.dgvDept.TabIndex = 0;
            // 
            // colPrDNo
            // 
            this.colPrDNo.DataPropertyName = "PR_P_NO";
            this.colPrDNo.HeaderText = "PrDNo";
            this.colPrDNo.Name = "colPrDNo";
            this.colPrDNo.ReadOnly = true;
            this.colPrDNo.Visible = false;
            // 
            // colSeg
            // 
            this.colSeg.DataPropertyName = "PR_SEGMENT";
            this.colSeg.HeaderText = "Seg";
            this.colSeg.Name = "colSeg";
            this.colSeg.ReadOnly = true;
            this.colSeg.Width = 50;
            // 
            // colDept
            // 
            this.colDept.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDept.DataPropertyName = "PR_DEPT";
            this.colDept.HeaderText = "Dept.";
            this.colDept.Name = "colDept";
            this.colDept.ReadOnly = true;
            // 
            // colContr
            // 
            this.colContr.DataPropertyName = "PR_CONTRIB";
            this.colContr.HeaderText = "Contb.";
            this.colContr.Name = "colContr";
            this.colContr.ReadOnly = true;
            this.colContr.Width = 50;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(426, 9);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(43, 13);
            this.lblUser.TabIndex = 98;
            this.lblUser.Text = "USER :";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtUser);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtLocation);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(4, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(614, 54);
            this.panel2.TabIndex = 99;
            // 
            // CHRIS_MedicalInsurance_PerClaimQr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 561);
            this.Controls.Add(this.pnlTblDept);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.pnlOmiDetail);
            this.Controls.Add(this.pnlOmiMaster);
            this.CurrentPanelBlock = "pnlOmiMaster";
            this.Name = "CHRIS_MedicalInsurance_PerClaimQr";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_PerClaimQr";
            this.Controls.SetChildIndex(this.pnlOmiMaster, 0);
            this.Controls.SetChildIndex(this.pnlOmiDetail, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.pnlTblDept, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlOmiMaster.ResumeLayout(false);
            this.pnlOmiMaster.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.pnlOmiDetail.ResumeLayout(false);
            this.pnlOmiDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOmiDetail)).EndInit();
            this.pnlTblDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlOmiMaster;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlOmiDetail;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtLocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txt_W_PNO;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txt_W_Name;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txt_W_Branch;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txt_Omi_Child;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox txt_Omi_Spouse;
        private System.Windows.Forms.Label label18;
        private CrplControlLibrary.SLTextBox txt_Omi_Self;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox txt_W_Child;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txt_W_Spouse;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox txt_W_Self;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox txt_W_Os_Balance;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txt_Omi_Total_Claim;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLTextBox txt_W_Omi_Limit;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvOmiDetail;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDept;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDept;
        private CrplControlLibrary.SLTextBox txt_W_Total_Recv;
        private CrplControlLibrary.SLTextBox txt_W_Total;
        private System.Windows.Forms.Label label19;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgv;
        private CrplControlLibrary.LookupButton lbtnPersonnel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmi_Month;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOMI_SELF;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOMI_SPOUSE;
        private System.Windows.Forms.DataGridViewTextBoxColumn COLOMI_CHILD;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmi_Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmi_Total_Claim;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmi_Total_Recev;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmi_P_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmi_TOTAL_CLAIMS;
        private System.Windows.Forms.Label lblUser;
        private CrplControlLibrary.SLTextBox txt_W_CAT;
        private CrplControlLibrary.SLDatePicker txt_W_MARRIAGE_DATE;
        private CrplControlLibrary.SLTextBox txt_W_STATUS;
        private CrplControlLibrary.SLDatePicker txt_W_JOINING_DATE;
        private CrplControlLibrary.SLTextBox txt_ENROLL;
        private CrplControlLibrary.SLTextBox txt_W_Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrDNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeg;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContr;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn OMI_MONTH;
        private System.Windows.Forms.DataGridViewTextBoxColumn s_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Bill_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Patient_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Relation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Claim_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Recev_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_P_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Omi_Claim_No;
    }
}