namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_EnrLettRep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_EnrLettRep));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.wdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.wdept = new CrplControlLibrary.SLTextBox(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.wdesig = new CrplControlLibrary.SLTextBox(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.name = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.wtype = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.wseg = new CrplControlLibrary.SLTextBox(this.components);
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.fdate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.wcat = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.wdate);
            this.groupBox1.Controls.Add(this.wdept);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.wdesig);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.wtype);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.wseg);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.dest_format);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tdate);
            this.groupBox1.Controls.Add(this.fdate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.wcat);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 447);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // wdate
            // 
            this.wdate.CustomEnabled = true;
            this.wdate.CustomFormat = "dd/MM/yyyy";
            this.wdate.DataFieldMapping = "";
            this.wdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.wdate.HasChanges = true;
            this.wdate.Location = new System.Drawing.Point(193, 72);
            this.wdate.Name = "wdate";
            this.wdate.NullValue = " ";
            this.wdate.Size = new System.Drawing.Size(173, 20);
            this.wdate.TabIndex = 0;
            this.wdate.Value = new System.DateTime(2011, 4, 13, 0, 0, 0, 0);
            // 
            // wdept
            // 
            this.wdept.AllowSpace = true;
            this.wdept.AssociatedLookUpName = "";
            this.wdept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wdept.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wdept.ContinuationTextBox = null;
            this.wdept.CustomEnabled = true;
            this.wdept.DataFieldMapping = "";
            this.wdept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wdept.GetRecordsOnUpDownKeys = false;
            this.wdept.IsDate = false;
            this.wdept.Location = new System.Drawing.Point(194, 335);
            this.wdept.MaxLength = 50;
            this.wdept.Name = "wdept";
            this.wdept.NumberFormat = "###,###,##0.00";
            this.wdept.Postfix = "";
            this.wdept.Prefix = "";
            this.wdept.Size = new System.Drawing.Size(172, 20);
            this.wdept.SkipValidation = false;
            this.wdept.TabIndex = 10;
            this.wdept.TextType = CrplControlLibrary.TextType.String;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(19, 335);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 101;
            this.label17.Text = "Department";
            // 
            // wdesig
            // 
            this.wdesig.AllowSpace = true;
            this.wdesig.AssociatedLookUpName = "";
            this.wdesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wdesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wdesig.ContinuationTextBox = null;
            this.wdesig.CustomEnabled = true;
            this.wdesig.DataFieldMapping = "";
            this.wdesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wdesig.GetRecordsOnUpDownKeys = false;
            this.wdesig.IsDate = false;
            this.wdesig.Location = new System.Drawing.Point(194, 309);
            this.wdesig.MaxLength = 50;
            this.wdesig.Name = "wdesig";
            this.wdesig.NumberFormat = "###,###,##0.00";
            this.wdesig.Postfix = "";
            this.wdesig.Prefix = "";
            this.wdesig.Size = new System.Drawing.Size(172, 20);
            this.wdesig.SkipValidation = false;
            this.wdesig.TabIndex = 9;
            this.wdesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(19, 309);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 13);
            this.label16.TabIndex = 99;
            this.label16.Text = "Designation";
            // 
            // name
            // 
            this.name.AllowSpace = true;
            this.name.AssociatedLookUpName = "";
            this.name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.name.ContinuationTextBox = null;
            this.name.CustomEnabled = true;
            this.name.DataFieldMapping = "";
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.GetRecordsOnUpDownKeys = false;
            this.name.IsDate = false;
            this.name.Location = new System.Drawing.Point(194, 283);
            this.name.MaxLength = 50;
            this.name.Name = "name";
            this.name.NumberFormat = "###,###,##0.00";
            this.name.Postfix = "";
            this.name.Prefix = "";
            this.name.Size = new System.Drawing.Size(172, 20);
            this.name.SkipValidation = false;
            this.name.TabIndex = 8;
            this.name.TextType = CrplControlLibrary.TextType.String;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(19, 283);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 97;
            this.label15.Text = "Signature";
            // 
            // wtype
            // 
            this.wtype.AllowSpace = true;
            this.wtype.AssociatedLookUpName = "";
            this.wtype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wtype.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wtype.ContinuationTextBox = null;
            this.wtype.CustomEnabled = true;
            this.wtype.DataFieldMapping = "";
            this.wtype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wtype.GetRecordsOnUpDownKeys = false;
            this.wtype.IsDate = false;
            this.wtype.Location = new System.Drawing.Point(194, 256);
            this.wtype.MaxLength = 1;
            this.wtype.Name = "wtype";
            this.wtype.NumberFormat = "###,###,##0.00";
            this.wtype.Postfix = "";
            this.wtype.Prefix = "";
            this.wtype.Size = new System.Drawing.Size(172, 20);
            this.wtype.SkipValidation = false;
            this.wtype.TabIndex = 7;
            this.wtype.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 256);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 13);
            this.label14.TabIndex = 95;
            this.label14.Text = "Enter Type Of Letter";
            // 
            // wseg
            // 
            this.wseg.AllowSpace = true;
            this.wseg.AssociatedLookUpName = "";
            this.wseg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wseg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wseg.ContinuationTextBox = null;
            this.wseg.CustomEnabled = true;
            this.wseg.DataFieldMapping = "";
            this.wseg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wseg.GetRecordsOnUpDownKeys = false;
            this.wseg.IsDate = false;
            this.wseg.Location = new System.Drawing.Point(194, 230);
            this.wseg.MaxLength = 3;
            this.wseg.Name = "wseg";
            this.wseg.NumberFormat = "###,###,##0.00";
            this.wseg.Postfix = "";
            this.wseg.Prefix = "";
            this.wseg.Size = new System.Drawing.Size(172, 20);
            this.wseg.SkipValidation = false;
            this.wseg.TabIndex = 6;
            this.wseg.TextType = CrplControlLibrary.TextType.String;
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(194, 178);
            this.copies.MaxLength = 50;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(172, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 4;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // dest_format
            // 
            this.dest_format.AllowSpace = true;
            this.dest_format.AssociatedLookUpName = "";
            this.dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_format.ContinuationTextBox = null;
            this.dest_format.CustomEnabled = true;
            this.dest_format.DataFieldMapping = "";
            this.dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_format.GetRecordsOnUpDownKeys = false;
            this.dest_format.IsDate = false;
            this.dest_format.Location = new System.Drawing.Point(194, 152);
            this.dest_format.MaxLength = 50;
            this.dest_format.Name = "dest_format";
            this.dest_format.NumberFormat = "###,###,##0.00";
            this.dest_format.Postfix = "";
            this.dest_format.Prefix = "";
            this.dest_format.Size = new System.Drawing.Size(172, 20);
            this.dest_format.SkipValidation = false;
            this.dest_format.TabIndex = 3;
            this.dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(19, 179);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 13);
            this.label13.TabIndex = 91;
            this.label13.Text = "Number of Copies";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(19, 155);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 90;
            this.label12.Text = "Destination Format";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 88;
            this.label11.Text = "Wdate";
            // 
            // tdate
            // 
            this.tdate.CustomEnabled = true;
            this.tdate.CustomFormat = "dd/MM/yyyy";
            this.tdate.DataFieldMapping = "";
            this.tdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tdate.HasChanges = true;
            this.tdate.Location = new System.Drawing.Point(193, 387);
            this.tdate.Name = "tdate";
            this.tdate.NullValue = " ";
            this.tdate.Size = new System.Drawing.Size(173, 20);
            this.tdate.TabIndex = 12;
            this.tdate.Value = new System.DateTime(2011, 4, 13, 0, 0, 0, 0);
            // 
            // fdate
            // 
            this.fdate.CustomEnabled = true;
            this.fdate.CustomFormat = "dd/MM/yyyy";
            this.fdate.DataFieldMapping = "";
            this.fdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fdate.HasChanges = true;
            this.fdate.Location = new System.Drawing.Point(194, 361);
            this.fdate.Name = "fdate";
            this.fdate.NullValue = " ";
            this.fdate.Size = new System.Drawing.Size(173, 20);
            this.fdate.TabIndex = 11;
            this.fdate.Value = new System.DateTime(2011, 4, 13, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 387);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "To/Blank For Not Enrolled";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 361);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 13);
            this.label2.TabIndex = 84;
            this.label2.Text = "From/Blank For Not Enrolled";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(160, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameters";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(118, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(377, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(275, 413);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 14;
            this.slButton1.Text = "Close";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(194, 413);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 13;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Enter Segment";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "[C] = Clerks [O] = Officers :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Destination Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Destination Type";
            // 
            // wcat
            // 
            this.wcat.AllowSpace = true;
            this.wcat.AssociatedLookUpName = "";
            this.wcat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wcat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wcat.ContinuationTextBox = null;
            this.wcat.CustomEnabled = true;
            this.wcat.DataFieldMapping = "";
            this.wcat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wcat.GetRecordsOnUpDownKeys = false;
            this.wcat.IsDate = false;
            this.wcat.Location = new System.Drawing.Point(194, 204);
            this.wcat.MaxLength = 1;
            this.wcat.Name = "wcat";
            this.wcat.NumberFormat = "###,###,##0.00";
            this.wcat.Postfix = "";
            this.wcat.Prefix = "";
            this.wcat.Size = new System.Drawing.Size(172, 20);
            this.wcat.SkipValidation = false;
            this.wcat.TabIndex = 5;
            this.wcat.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(194, 125);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(172, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 2;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(194, 98);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(172, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 1;
            // 
            // CHRIS_MedicalInsurance_EnrLettRep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 499);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_MedicalInsurance_EnrLettRep";
            this.Text = "CHRIS_MedicalInsurance_EnrLettRep";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLDatePicker tdate;
        private CrplControlLibrary.SLDatePicker fdate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLButton slButton1;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox wcat;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label11;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox dest_format;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox wseg;
        private CrplControlLibrary.SLDatePicker wdate;
        private CrplControlLibrary.SLTextBox wdept;
        private System.Windows.Forms.Label label17;
        private CrplControlLibrary.SLTextBox wdesig;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox name;
        private System.Windows.Forms.Label label15;
        private CrplControlLibrary.SLTextBox wtype;
    }
}