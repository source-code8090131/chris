namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_EnrLetRepG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_EnrLetRepG));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.name = new CrplControlLibrary.SLTextBox(this.components);
            this.PDATE_TO = new CrplControlLibrary.SLDatePicker(this.components);
            this.P_DATE_FROM = new CrplControlLibrary.SLDatePicker(this.components);
            this.P_SEG = new CrplControlLibrary.SLComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.P_BRANCH = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DESIG);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.PDATE_TO);
            this.groupBox1.Controls.Add(this.P_DATE_FROM);
            this.groupBox1.Controls.Add(this.P_SEG);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.P_BRANCH);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 328);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // DESIG
            // 
            this.DESIG.AllowSpace = true;
            this.DESIG.AssociatedLookUpName = "";
            this.DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.DESIG.ContinuationTextBox = null;
            this.DESIG.CustomEnabled = true;
            this.DESIG.DataFieldMapping = "";
            this.DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DESIG.GetRecordsOnUpDownKeys = false;
            this.DESIG.IsDate = false;
            this.DESIG.Location = new System.Drawing.Point(120, 260);
            this.DESIG.MaxLength = 50;
            this.DESIG.Name = "DESIG";
            this.DESIG.NumberFormat = "###,###,##0.00";
            this.DESIG.Postfix = "";
            this.DESIG.Prefix = "";
            this.DESIG.Size = new System.Drawing.Size(172, 20);
            this.DESIG.SkipValidation = false;
            this.DESIG.TabIndex = 7;
            this.DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // name
            // 
            this.name.AllowSpace = true;
            this.name.AssociatedLookUpName = "";
            this.name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.name.ContinuationTextBox = null;
            this.name.CustomEnabled = true;
            this.name.DataFieldMapping = "";
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.GetRecordsOnUpDownKeys = false;
            this.name.IsDate = false;
            this.name.Location = new System.Drawing.Point(120, 234);
            this.name.MaxLength = 50;
            this.name.Name = "name";
            this.name.NumberFormat = "###,###,##0.00";
            this.name.Postfix = "";
            this.name.Prefix = "";
            this.name.Size = new System.Drawing.Size(172, 20);
            this.name.SkipValidation = false;
            this.name.TabIndex = 6;
            this.name.TextType = CrplControlLibrary.TextType.String;
            // 
            // PDATE_TO
            // 
            this.PDATE_TO.CustomEnabled = true;
            this.PDATE_TO.CustomFormat = "dd/MM/yyyy";
            this.PDATE_TO.DataFieldMapping = "";
            this.PDATE_TO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PDATE_TO.HasChanges = true;
            this.PDATE_TO.Location = new System.Drawing.Point(121, 208);
            this.PDATE_TO.Name = "PDATE_TO";
            this.PDATE_TO.NullValue = " ";
            this.PDATE_TO.Size = new System.Drawing.Size(173, 20);
            this.PDATE_TO.TabIndex = 5;
            this.PDATE_TO.Value = new System.DateTime(2011, 4, 13, 0, 0, 0, 0);
            // 
            // P_DATE_FROM
            // 
            this.P_DATE_FROM.CustomEnabled = true;
            this.P_DATE_FROM.CustomFormat = "dd/MM/yyyy";
            this.P_DATE_FROM.DataFieldMapping = "";
            this.P_DATE_FROM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.P_DATE_FROM.HasChanges = false;
            this.P_DATE_FROM.Location = new System.Drawing.Point(120, 182);
            this.P_DATE_FROM.Name = "P_DATE_FROM";
            this.P_DATE_FROM.NullValue = " ";
            this.P_DATE_FROM.Size = new System.Drawing.Size(173, 20);
            this.P_DATE_FROM.TabIndex = 4;
            this.P_DATE_FROM.Value = new System.DateTime(2011, 4, 13, 0, 0, 0, 0);
            // 
            // P_SEG
            // 
            this.P_SEG.BusinessEntity = "";
            this.P_SEG.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.P_SEG.CustomEnabled = true;
            this.P_SEG.DataFieldMapping = "";
            this.P_SEG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.P_SEG.FormattingEnabled = true;
            this.P_SEG.Location = new System.Drawing.Point(121, 154);
            this.P_SEG.LOVType = "";
            this.P_SEG.Name = "P_SEG";
            this.P_SEG.Size = new System.Drawing.Size(172, 21);
            this.P_SEG.SPName = "";
            this.P_SEG.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 260);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 87;
            this.label10.Text = "Designation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "Date To";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 84;
            this.label2.Text = "Date From";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(160, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameters";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(118, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(377, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(208, 296);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 8;
            this.slButton1.Text = "Close";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(121, 296);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 7;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Segment";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Branch";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "DesName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "DesType";
            // 
            // P_BRANCH
            // 
            this.P_BRANCH.AllowSpace = true;
            this.P_BRANCH.AssociatedLookUpName = "";
            this.P_BRANCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_BRANCH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_BRANCH.ContinuationTextBox = null;
            this.P_BRANCH.CustomEnabled = true;
            this.P_BRANCH.DataFieldMapping = "";
            this.P_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_BRANCH.GetRecordsOnUpDownKeys = false;
            this.P_BRANCH.IsDate = false;
            this.P_BRANCH.Location = new System.Drawing.Point(121, 126);
            this.P_BRANCH.MaxLength = 3;
            this.P_BRANCH.Name = "P_BRANCH";
            this.P_BRANCH.NumberFormat = "###,###,##0.00";
            this.P_BRANCH.Postfix = "";
            this.P_BRANCH.Prefix = "";
            this.P_BRANCH.Size = new System.Drawing.Size(172, 20);
            this.P_BRANCH.SkipValidation = false;
            this.P_BRANCH.TabIndex = 2;
            this.P_BRANCH.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(121, 100);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(172, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 1;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(121, 73);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(172, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            this.cmbDescType.SelectedIndexChanged += new System.EventHandler(this.cmbDescType_SelectedIndexChanged);
            // 
            // CHRIS_MedicalInsurance_EnrollmentLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 376);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_MedicalInsurance_EnrollmentLetter";
            this.Text = "CHRIS_MedicalInsurance_GLI03";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLButton slButton1;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox P_BRANCH;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLDatePicker P_DATE_FROM;
        private CrplControlLibrary.SLComboBox P_SEG;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox DESIG;
        private CrplControlLibrary.SLTextBox name;
        private CrplControlLibrary.SLDatePicker PDATE_TO;
    }
}