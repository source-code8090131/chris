namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_PremInfo_GLIDE02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_PremInfo_GLIDE02));
            this.pnlLifeInso = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnType = new CrplControlLibrary.LookupButton(this.components);
            this.txtID = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPaymentDate = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnPolicyNo = new CrplControlLibrary.LookupButton(this.components);
            this.dtp_Pre_Payment_Date = new CrplControlLibrary.SLDatePicker(this.components);
            this.txt_abc = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Pre_Subject = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pre_Mc_No = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pre_Bill_No = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pre_Premium_Amt = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Type_Desc = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Type = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Policy = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlLifeInso.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(562, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(598, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 369);
            this.panel1.Size = new System.Drawing.Size(598, 60);
            // 
            // pnlLifeInso
            // 
            this.pnlLifeInso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLifeInso.ConcurrentPanels = null;
            this.pnlLifeInso.Controls.Add(this.lbtnType);
            this.pnlLifeInso.Controls.Add(this.txtID);
            this.pnlLifeInso.Controls.Add(this.lbtnPaymentDate);
            this.pnlLifeInso.Controls.Add(this.lbtnPolicyNo);
            this.pnlLifeInso.Controls.Add(this.dtp_Pre_Payment_Date);
            this.pnlLifeInso.Controls.Add(this.txt_abc);
            this.pnlLifeInso.Controls.Add(this.label13);
            this.pnlLifeInso.Controls.Add(this.label12);
            this.pnlLifeInso.Controls.Add(this.label11);
            this.pnlLifeInso.Controls.Add(this.label10);
            this.pnlLifeInso.Controls.Add(this.label9);
            this.pnlLifeInso.Controls.Add(this.label8);
            this.pnlLifeInso.Controls.Add(this.label7);
            this.pnlLifeInso.Controls.Add(this.label6);
            this.pnlLifeInso.Controls.Add(this.label5);
            this.pnlLifeInso.Controls.Add(this.label4);
            this.pnlLifeInso.Controls.Add(this.label3);
            this.pnlLifeInso.Controls.Add(this.label2);
            this.pnlLifeInso.Controls.Add(this.label1);
            this.pnlLifeInso.Controls.Add(this.txt_Pre_Subject);
            this.pnlLifeInso.Controls.Add(this.txt_Pre_Mc_No);
            this.pnlLifeInso.Controls.Add(this.txt_Pre_Bill_No);
            this.pnlLifeInso.Controls.Add(this.txt_Pre_Premium_Amt);
            this.pnlLifeInso.Controls.Add(this.txt_W_Type_Desc);
            this.pnlLifeInso.Controls.Add(this.txt_W_Type);
            this.pnlLifeInso.Controls.Add(this.txt_W_Policy);
            this.pnlLifeInso.Controls.Add(this.txtDate);
            this.pnlLifeInso.Controls.Add(this.slTextBox2);
            this.pnlLifeInso.Controls.Add(this.slTextBox1);
            this.pnlLifeInso.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlLifeInso.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlLifeInso.DependentPanels = null;
            this.pnlLifeInso.DisableDependentLoad = false;
            this.pnlLifeInso.EnableDelete = true;
            this.pnlLifeInso.EnableInsert = true;
            this.pnlLifeInso.EnableQuery = false;
            this.pnlLifeInso.EnableUpdate = true;
            this.pnlLifeInso.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PremInfoCommand";
            this.pnlLifeInso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlLifeInso.Location = new System.Drawing.Point(7, 84);
            this.pnlLifeInso.MasterPanel = null;
            this.pnlLifeInso.Name = "pnlLifeInso";
            this.pnlLifeInso.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlLifeInso.Size = new System.Drawing.Size(567, 279);
            this.pnlLifeInso.SPName = "CHRIS_SP_PREMINFO_PREMIUM_MASTER_MANAGER";
            this.pnlLifeInso.TabIndex = 10;
            // 
            // lbtnType
            // 
            this.lbtnType.ActionLOVExists = "OCTYPE_EXISTS";
            this.lbtnType.ActionType = "OCTYPE";
            this.lbtnType.ConditionalFields = "txt_W_Policy";
            this.lbtnType.CustomEnabled = true;
            this.lbtnType.DataFieldMapping = "";
            this.lbtnType.DependentLovControls = "";
            this.lbtnType.HiddenColumns = "PRE_PREMIUM_AMT|PRE_BILL_NO|PRE_MC_NO|PRE_SUBJECT|ID";
            this.lbtnType.Image = ((System.Drawing.Image)(resources.GetObject("lbtnType.Image")));
            this.lbtnType.LoadDependentEntities = false;
            this.lbtnType.Location = new System.Drawing.Point(418, 117);
            this.lbtnType.LookUpTitle = null;
            this.lbtnType.Name = "lbtnType";
            this.lbtnType.Size = new System.Drawing.Size(26, 21);
            this.lbtnType.SkipValidationOnLeave = true;
            this.lbtnType.SPName = "CHRIS_SP_PREMINFO_PREMIUM_MASTER_MANAGER";
            this.lbtnType.TabIndex = 54;
            this.lbtnType.TabStop = false;
            this.lbtnType.UseVisualStyleBackColor = true;
            // 
            // txtID
            // 
            this.txtID.AllowSpace = true;
            this.txtID.AssociatedLookUpName = "";
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.ContinuationTextBox = null;
            this.txtID.CustomEnabled = true;
            this.txtID.DataFieldMapping = "ID";
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.GetRecordsOnUpDownKeys = false;
            this.txtID.IsDate = false;
            this.txtID.Location = new System.Drawing.Point(8, 98);
            this.txtID.Name = "txtID";
            this.txtID.NumberFormat = "###,###,##0.00";
            this.txtID.Postfix = "";
            this.txtID.Prefix = "";
            this.txtID.Size = new System.Drawing.Size(38, 20);
            this.txtID.SkipValidation = false;
            this.txtID.TabIndex = 27;
            this.txtID.TabStop = false;
            this.txtID.TextType = CrplControlLibrary.TextType.String;
            this.txtID.Visible = false;
            // 
            // lbtnPaymentDate
            // 
            this.lbtnPaymentDate.ActionLOVExists = "";
            this.lbtnPaymentDate.ActionType = "PAYMENT_DATE";
            this.lbtnPaymentDate.ConditionalFields = "txt_W_Policy";
            this.lbtnPaymentDate.CustomEnabled = true;
            this.lbtnPaymentDate.DataFieldMapping = "";
            this.lbtnPaymentDate.DependentLovControls = "";
            this.lbtnPaymentDate.HiddenColumns = "PRE_PREMIUM_AMT|PRE_BILL_NO|PRE_MC_NO|PRE_SUBJECT|ID";
            this.lbtnPaymentDate.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPaymentDate.Image")));
            this.lbtnPaymentDate.LoadDependentEntities = false;
            this.lbtnPaymentDate.Location = new System.Drawing.Point(343, 145);
            this.lbtnPaymentDate.LookUpTitle = null;
            this.lbtnPaymentDate.Name = "lbtnPaymentDate";
            this.lbtnPaymentDate.Size = new System.Drawing.Size(26, 21);
            this.lbtnPaymentDate.SkipValidationOnLeave = true;
            this.lbtnPaymentDate.SPName = "CHRIS_SP_PREMINFO_PREMIUM_MASTER_MANAGER";
            this.lbtnPaymentDate.TabIndex = 26;
            this.lbtnPaymentDate.TabStop = false;
            this.lbtnPaymentDate.UseVisualStyleBackColor = true;
            // 
            // lbtnPolicyNo
            // 
            this.lbtnPolicyNo.ActionLOVExists = "POLICY_LOV_EXISTS";
            this.lbtnPolicyNo.ActionType = "POLICY_LOV";
            this.lbtnPolicyNo.ConditionalFields = "";
            this.lbtnPolicyNo.CustomEnabled = true;
            this.lbtnPolicyNo.DataFieldMapping = "";
            this.lbtnPolicyNo.DependentLovControls = "";
            this.lbtnPolicyNo.HiddenColumns = "";
            this.lbtnPolicyNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPolicyNo.Image")));
            this.lbtnPolicyNo.LoadDependentEntities = false;
            this.lbtnPolicyNo.Location = new System.Drawing.Point(418, 93);
            this.lbtnPolicyNo.LookUpTitle = null;
            this.lbtnPolicyNo.Name = "lbtnPolicyNo";
            this.lbtnPolicyNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPolicyNo.SkipValidationOnLeave = false;
            this.lbtnPolicyNo.SPName = "CHRIS_SP_PREMINFO_PREMIUM_MASTER_MANAGER";
            this.lbtnPolicyNo.TabIndex = 25;
            this.lbtnPolicyNo.TabStop = false;
            this.lbtnPolicyNo.UseVisualStyleBackColor = true;
            // 
            // dtp_Pre_Payment_Date
            // 
            this.dtp_Pre_Payment_Date.CustomEnabled = true;
            this.dtp_Pre_Payment_Date.CustomFormat = "dd/MM/yyyy";
            this.dtp_Pre_Payment_Date.DataFieldMapping = "PRE_PAYMENT_DATE";
            this.dtp_Pre_Payment_Date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_Pre_Payment_Date.HasChanges = true;
            this.dtp_Pre_Payment_Date.Location = new System.Drawing.Point(217, 145);
            this.dtp_Pre_Payment_Date.Name = "dtp_Pre_Payment_Date";
            this.dtp_Pre_Payment_Date.NullValue = " ";
            this.dtp_Pre_Payment_Date.Size = new System.Drawing.Size(119, 20);
            this.dtp_Pre_Payment_Date.TabIndex = 2;
            this.dtp_Pre_Payment_Date.Value = new System.DateTime(2011, 4, 16, 0, 0, 0, 0);
            this.dtp_Pre_Payment_Date.Validating += new System.ComponentModel.CancelEventHandler(this.dtp_Pre_Payment_Date_Validating);
            // 
            // txt_abc
            // 
            this.txt_abc.AllowSpace = true;
            this.txt_abc.AssociatedLookUpName = "";
            this.txt_abc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_abc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_abc.ContinuationTextBox = null;
            this.txt_abc.CustomEnabled = true;
            this.txt_abc.DataFieldMapping = "ABC";
            this.txt_abc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_abc.GetRecordsOnUpDownKeys = false;
            this.txt_abc.IsDate = false;
            this.txt_abc.Location = new System.Drawing.Point(8, 71);
            this.txt_abc.Name = "txt_abc";
            this.txt_abc.NumberFormat = "###,###,##0.00";
            this.txt_abc.Postfix = "";
            this.txt_abc.Prefix = "";
            this.txt_abc.Size = new System.Drawing.Size(38, 20);
            this.txt_abc.SkipValidation = false;
            this.txt_abc.TabIndex = 24;
            this.txt_abc.TabStop = false;
            this.txt_abc.TextType = CrplControlLibrary.TextType.String;
            this.txt_abc.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(-3, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(693, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "_________________________________________________________________________________" +
                "_________________";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(80, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Subject";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(80, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "MC No.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(80, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Bill No.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(80, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Premium Amount";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(80, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Payment Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(80, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "[C]lerical / [O]fficer :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(80, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Policy No.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(213, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "PREMIUM INFORMATION";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(238, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "LIFE INSURANCE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(426, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // txt_Pre_Subject
            // 
            this.txt_Pre_Subject.AllowSpace = true;
            this.txt_Pre_Subject.AssociatedLookUpName = "";
            this.txt_Pre_Subject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pre_Subject.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pre_Subject.ContinuationTextBox = null;
            this.txt_Pre_Subject.CustomEnabled = true;
            this.txt_Pre_Subject.DataFieldMapping = "PRE_SUBJECT";
            this.txt_Pre_Subject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pre_Subject.GetRecordsOnUpDownKeys = false;
            this.txt_Pre_Subject.IsDate = false;
            this.txt_Pre_Subject.Location = new System.Drawing.Point(217, 249);
            this.txt_Pre_Subject.MaxLength = 50;
            this.txt_Pre_Subject.Name = "txt_Pre_Subject";
            this.txt_Pre_Subject.NumberFormat = "###,###,##0.00";
            this.txt_Pre_Subject.Postfix = "";
            this.txt_Pre_Subject.Prefix = "";
            this.txt_Pre_Subject.Size = new System.Drawing.Size(345, 20);
            this.txt_Pre_Subject.SkipValidation = false;
            this.txt_Pre_Subject.TabIndex = 6;
            this.txt_Pre_Subject.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pre_Subject.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pre_Subject_Validating);
            // 
            // txt_Pre_Mc_No
            // 
            this.txt_Pre_Mc_No.AllowSpace = true;
            this.txt_Pre_Mc_No.AssociatedLookUpName = "";
            this.txt_Pre_Mc_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pre_Mc_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pre_Mc_No.ContinuationTextBox = null;
            this.txt_Pre_Mc_No.CustomEnabled = true;
            this.txt_Pre_Mc_No.DataFieldMapping = "PRE_MC_NO";
            this.txt_Pre_Mc_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pre_Mc_No.GetRecordsOnUpDownKeys = false;
            this.txt_Pre_Mc_No.IsDate = false;
            this.txt_Pre_Mc_No.Location = new System.Drawing.Point(217, 223);
            this.txt_Pre_Mc_No.MaxLength = 25;
            this.txt_Pre_Mc_No.Name = "txt_Pre_Mc_No";
            this.txt_Pre_Mc_No.NumberFormat = "###,###,##0.00";
            this.txt_Pre_Mc_No.Postfix = "";
            this.txt_Pre_Mc_No.Prefix = "";
            this.txt_Pre_Mc_No.Size = new System.Drawing.Size(177, 20);
            this.txt_Pre_Mc_No.SkipValidation = false;
            this.txt_Pre_Mc_No.TabIndex = 5;
            this.txt_Pre_Mc_No.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pre_Bill_No
            // 
            this.txt_Pre_Bill_No.AllowSpace = true;
            this.txt_Pre_Bill_No.AssociatedLookUpName = "";
            this.txt_Pre_Bill_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pre_Bill_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pre_Bill_No.ContinuationTextBox = null;
            this.txt_Pre_Bill_No.CustomEnabled = true;
            this.txt_Pre_Bill_No.DataFieldMapping = "PRE_BILL_NO";
            this.txt_Pre_Bill_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pre_Bill_No.GetRecordsOnUpDownKeys = false;
            this.txt_Pre_Bill_No.IsDate = false;
            this.txt_Pre_Bill_No.Location = new System.Drawing.Point(217, 197);
            this.txt_Pre_Bill_No.MaxLength = 25;
            this.txt_Pre_Bill_No.Name = "txt_Pre_Bill_No";
            this.txt_Pre_Bill_No.NumberFormat = "###,###,##0.00";
            this.txt_Pre_Bill_No.Postfix = "";
            this.txt_Pre_Bill_No.Prefix = "";
            this.txt_Pre_Bill_No.Size = new System.Drawing.Size(177, 20);
            this.txt_Pre_Bill_No.SkipValidation = false;
            this.txt_Pre_Bill_No.TabIndex = 4;
            this.txt_Pre_Bill_No.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pre_Bill_No.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pre_Bill_No_Validating);
            // 
            // txt_Pre_Premium_Amt
            // 
            this.txt_Pre_Premium_Amt.AllowSpace = true;
            this.txt_Pre_Premium_Amt.AssociatedLookUpName = "";
            this.txt_Pre_Premium_Amt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pre_Premium_Amt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pre_Premium_Amt.ContinuationTextBox = null;
            this.txt_Pre_Premium_Amt.CustomEnabled = true;
            this.txt_Pre_Premium_Amt.DataFieldMapping = "PRE_PREMIUM_AMT";
            this.txt_Pre_Premium_Amt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pre_Premium_Amt.GetRecordsOnUpDownKeys = false;
            this.txt_Pre_Premium_Amt.IsDate = false;
            this.txt_Pre_Premium_Amt.Location = new System.Drawing.Point(217, 171);
            this.txt_Pre_Premium_Amt.MaxLength = 12;
            this.txt_Pre_Premium_Amt.Name = "txt_Pre_Premium_Amt";
            this.txt_Pre_Premium_Amt.NumberFormat = "###,###,##0.00";
            this.txt_Pre_Premium_Amt.Postfix = "";
            this.txt_Pre_Premium_Amt.Prefix = "";
            this.txt_Pre_Premium_Amt.Size = new System.Drawing.Size(119, 20);
            this.txt_Pre_Premium_Amt.SkipValidation = false;
            this.txt_Pre_Premium_Amt.TabIndex = 3;
            this.txt_Pre_Premium_Amt.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pre_Premium_Amt.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pre_Premium_Amt_Validating);
            // 
            // txt_W_Type_Desc
            // 
            this.txt_W_Type_Desc.AllowSpace = true;
            this.txt_W_Type_Desc.AssociatedLookUpName = "";
            this.txt_W_Type_Desc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Type_Desc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Type_Desc.ContinuationTextBox = null;
            this.txt_W_Type_Desc.CustomEnabled = true;
            this.txt_W_Type_Desc.DataFieldMapping = "W_Type_Desc";
            this.txt_W_Type_Desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Type_Desc.GetRecordsOnUpDownKeys = false;
            this.txt_W_Type_Desc.IsDate = false;
            this.txt_W_Type_Desc.Location = new System.Drawing.Point(252, 119);
            this.txt_W_Type_Desc.Name = "txt_W_Type_Desc";
            this.txt_W_Type_Desc.NumberFormat = "###,###,##0.00";
            this.txt_W_Type_Desc.Postfix = "";
            this.txt_W_Type_Desc.Prefix = "";
            this.txt_W_Type_Desc.ReadOnly = true;
            this.txt_W_Type_Desc.Size = new System.Drawing.Size(160, 20);
            this.txt_W_Type_Desc.SkipValidation = false;
            this.txt_W_Type_Desc.TabIndex = 5;
            this.txt_W_Type_Desc.TabStop = false;
            this.txt_W_Type_Desc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Type
            // 
            this.txt_W_Type.AllowSpace = true;
            this.txt_W_Type.AssociatedLookUpName = "lbtnType";
            this.txt_W_Type.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Type.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Type.ContinuationTextBox = null;
            this.txt_W_Type.CustomEnabled = true;
            this.txt_W_Type.DataFieldMapping = "W_Type";
            this.txt_W_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Type.GetRecordsOnUpDownKeys = false;
            this.txt_W_Type.IsDate = false;
            this.txt_W_Type.Location = new System.Drawing.Point(217, 119);
            this.txt_W_Type.MaxLength = 1;
            this.txt_W_Type.Name = "txt_W_Type";
            this.txt_W_Type.NumberFormat = "###,###,##0.00";
            this.txt_W_Type.Postfix = "";
            this.txt_W_Type.Prefix = "";
            this.txt_W_Type.Size = new System.Drawing.Size(29, 20);
            this.txt_W_Type.SkipValidation = false;
            this.txt_W_Type.TabIndex = 1;
            this.txt_W_Type.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Type.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Type_Validating);
            // 
            // txt_W_Policy
            // 
            this.txt_W_Policy.AllowSpace = true;
            this.txt_W_Policy.AssociatedLookUpName = "lbtnPolicyNo";
            this.txt_W_Policy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Policy.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Policy.ContinuationTextBox = null;
            this.txt_W_Policy.CustomEnabled = true;
            this.txt_W_Policy.DataFieldMapping = "PRE_POLICY";
            this.txt_W_Policy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Policy.GetRecordsOnUpDownKeys = false;
            this.txt_W_Policy.IsDate = false;
            this.txt_W_Policy.IsRequired = true;
            this.txt_W_Policy.Location = new System.Drawing.Point(217, 93);
            this.txt_W_Policy.MaxLength = 25;
            this.txt_W_Policy.Name = "txt_W_Policy";
            this.txt_W_Policy.NumberFormat = "###,###,##0.00";
            this.txt_W_Policy.Postfix = "";
            this.txt_W_Policy.Prefix = "";
            this.txt_W_Policy.Size = new System.Drawing.Size(195, 20);
            this.txt_W_Policy.SkipValidation = false;
            this.txt_W_Policy.TabIndex = 0;
            this.txt_W_Policy.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Policy.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Policy_Validating);
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(469, 10);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(82, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 2;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.Location = new System.Drawing.Point(48, 31);
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "###,###,##0.00";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(82, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 1;
            this.slTextBox2.TabStop = false;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox2.Visible = false;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.Location = new System.Drawing.Point(48, 8);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(82, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 0;
            this.slTextBox1.TabStop = false;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox1.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(372, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "UserName :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(436, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(41, 13);
            this.lblUserName.TabIndex = 26;
            this.lblUserName.Text = "label15";
            // 
            // CHRIS_MedicalInsurance_PremInfo_GLIDE02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 429);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pnlLifeInso);
            this.Name = "CHRIS_MedicalInsurance_PremInfo_GLIDE02";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_PremInfo_GLIDE02";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_MedicalInsurance_PremInfo_GLIDE02_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlLifeInso, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlLifeInso.ResumeLayout(false);
            this.pnlLifeInso.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlLifeInso;
        private CrplControlLibrary.SLTextBox txt_Pre_Subject;
        private CrplControlLibrary.SLTextBox txt_Pre_Mc_No;
        private CrplControlLibrary.SLTextBox txt_Pre_Bill_No;
        private CrplControlLibrary.SLTextBox txt_Pre_Premium_Amt;
        private CrplControlLibrary.SLTextBox txt_W_Type_Desc;
        private CrplControlLibrary.SLTextBox txt_W_Type;
        private CrplControlLibrary.SLTextBox txt_W_Policy;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txt_abc;
        private CrplControlLibrary.SLDatePicker dtp_Pre_Payment_Date;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblUserName;
        private CrplControlLibrary.LookupButton lbtnPolicyNo;
        private CrplControlLibrary.LookupButton lbtnPaymentDate;
        private CrplControlLibrary.SLTextBox txtID;
        private CrplControlLibrary.LookupButton lbtnType;
    }
}