using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_EnrollmentLetterPage2 : ChrisTabularForm
    {
        #region --Variable--
        int pr_p_no = 0;
        DataTable dt = new DataTable();
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_EnrollmentLetterPage2()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_EnrollmentLetterPage2(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                tbtAdd.Visible      = false;
                tbtDelete.Visible   = false;
                tbtEdit.Visible     = false;
                tbtAdd.Visible      = false;
                tbtList.Visible     = false;
                tbtSave.Visible     = false;
                tlbMain.Items["tbtSearch"].Available = false;
                tlbMain.Items["tbtSearch"].Enabled = false;

                txtOption.Visible   = false;
                string dummy = string.Empty;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", pr_p_no.ToString());

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_EnrLettPERSONNEL_MANAGER", "Personal", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt                      = rsltCode.dstResult.Tables[0];
                    dgvPersonal.GridSource  = dt;
                    dgvPersonal.DataSource  = dgvPersonal.GridSource;
                    dgvPersonal.Select();
                    dgvPersonal.Focus();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }
    }
}