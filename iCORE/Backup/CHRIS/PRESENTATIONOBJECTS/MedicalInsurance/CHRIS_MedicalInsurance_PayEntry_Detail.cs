using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PayEntry_Detail : ChrisTabularForm
    {

        private CHRIS_MedicalInsurance_PayEntry _mainForm = null;
        DataTable dtClaim;
        decimal _omi_TotalAmt=0;
        decimal _omi_SELF=0;
        decimal _omi_SPOUSE=0;
        decimal _omi_CHILD=0;
        private decimal _TotalRecvAmount=0;

        public decimal getTotal
        {

            get
            {
                return _TotalRecvAmount;
            }
        }




        public CHRIS_MedicalInsurance_PayEntry_Detail()
        {
            InitializeComponent();
        }


        public CHRIS_MedicalInsurance_PayEntry_Detail(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj,  DataTable _dtClaim, decimal omi_TotalAmt, decimal omi_SELF, decimal omi_SPOUSE, decimal omi_CHILD)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
           
            //this._mainForm = mainForm;
            dtClaim = _dtClaim;
            _omi_TotalAmt = omi_TotalAmt;
            _omi_SELF = omi_SELF;
            _omi_SPOUSE = omi_SPOUSE;
            _omi_CHILD = omi_CHILD;

        }


        /// <summary>
        /// REmove the Add Update Save Cancel Button
        /// </summary>
        protected override void CommonOnLoadMethods()
        {

            base.CommonOnLoadMethods();
            this.tbtAdd.Visible = false;
            this.tbtList.Visible = false;
            this.tbtSave.Visible = false;
            this.tbtCancel.Visible = false;
            this.tlbMain.Visible = false;
            this.txtOption.Visible = false;
            Font newFontStyle = new Font(DGVClaim.Font, FontStyle.Bold);
            DGVClaim.ColumnHeadersDefaultCellStyle.Font = newFontStyle;

           

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);

            DGVClaim.GridSource = dtClaim;
            DGVClaim.DataSource = DGVClaim.GridSource;


            this.FunctionConfig.EnableF10 = true;
            this.FunctionConfig.F10 = Function.Save;
            this.FunctionConfig.EnableF6 = true;
            this.FunctionConfig.F6 = Function.Quit;
            this.FunctionConfig.CurrentOption = Function.Modify;



        }

        private void DGVClaim_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (!this.DGVClaim.CurrentCell.IsInEditMode)
            {

                return;
            }
            decimal OmitotalClaim = 0;
            decimal OmiRecvClaim = 0;
            if (e.ColumnIndex == 4)
            {
                if (DGVClaim.Rows[e.RowIndex].Cells["ClaimAmount"].Value.ToString() != string.Empty)
                {
                    OmitotalClaim = decimal.Parse(DGVClaim.Rows[e.RowIndex].Cells["ClaimAmount"].Value.ToString());
                }

                if (DGVClaim.Rows[e.RowIndex].Cells["RecvAmount"].Value.ToString() != string.Empty)
                {
                    OmiRecvClaim = decimal.Parse(DGVClaim.Rows[e.RowIndex].Cells["RecvAmount"].Value.ToString());
                }
                if (OmiRecvClaim > OmitotalClaim)
                {

                    MessageBox.Show("AMOUNT RECEIVED IS GREATER THEN THE CLAIMED AMOUNT");

                    e.Cancel = true;


                }
            }
        }


        protected override bool Save()
        {

            decimal REC = 0;
            decimal omi_recv_Amt = 0;
            string Relation = "";
            decimal omi_claim_Amt = 0;
            decimal diff = 0;

            decimal finalSelf=0;
            decimal finalspouse = 0;
            decimal finalChild = 0;
            decimal omi_P_no = 0;
            decimal mMonth = 0;
            decimal mYear = 0;
          
            foreach (DataGridViewRow rs in DGVClaim.Rows)
            {
                if (rs.Cells[0].Value != null && rs.Cells[0].Value.ToString() !=string.Empty)
                {
                    if (rs.Cells["RecvAmount"].Value.ToString() != string.Empty)
                    {
                        omi_recv_Amt = decimal.Parse(rs.Cells["RecvAmount"].Value.ToString());
                    }

                    if (rs.Cells["ClaimAmount"].Value.ToString() != string.Empty)
                    {
                        omi_claim_Amt = decimal.Parse(rs.Cells["ClaimAmount"].Value.ToString());
                    }

                    if (rs.Cells["wdiffernce"].Value.ToString() != string.Empty)
                    {
                        diff = decimal.Parse(rs.Cells["wdiffernce"].Value.ToString());
                    }
                    Relation = rs.Cells["Relation"].Value.ToString();


                    if (rs.Cells["PNo"].Value.ToString() != string.Empty)
                    {
                        omi_P_no = decimal.Parse(rs.Cells["PNo"].Value.ToString());
                    }


                    if (rs.Cells["Mmonth"].Value.ToString() != string.Empty)
                    {
                        mMonth = decimal.Parse(rs.Cells["Mmonth"].Value.ToString());
                    }

                    if (rs.Cells["Yyear"].Value.ToString() != string.Empty)
                    {
                        mYear = decimal.Parse(rs.Cells["Yyear"].Value.ToString());
                    }


                    REC = REC + omi_recv_Amt;



                    if (Relation == "Self" && omi_recv_Amt != omi_claim_Amt)
                    {
                        finalSelf = ((_omi_SELF - diff) + (omi_claim_Amt - omi_recv_Amt));

                    }

                    if ((Relation == "Wife" || Relation == "Husband") && omi_recv_Amt != omi_claim_Amt)
                    {
                        finalspouse = ((_omi_SPOUSE - diff) + (omi_claim_Amt - omi_recv_Amt));

                    }
                    if ((Relation == "Son" || Relation == "Daughter") && omi_recv_Amt != omi_claim_Amt)
                    {
                        finalChild = ((_omi_CHILD - diff) + (omi_claim_Amt - omi_recv_Amt));

                    }


                }

                


            }

            if (REC != _omi_TotalAmt)
            {
                MessageBox.Show("Total Of Detail <> Recived Amount. " + REC.ToString());
            }
            else
            {
                _TotalRecvAmount=REC;
                base.Save();
                Result rsltCode1;
                Dictionary<string, object> colsNVals1 = new Dictionary<string, object>();
                CmnDataManager cmnDM = new CmnDataManager();

                colsNVals1.Clear();
                colsNVals1.Add("OMI_P_NO", omi_P_no);
                colsNVals1.Add("OMI_YEAR", mYear);
                colsNVals1.Add("OMI_MONTH", mMonth);
                colsNVals1.Add("OMI_SELF", finalSelf);
                colsNVals1.Add("OMI_SPOUSE", finalspouse);
                colsNVals1.Add("OMI_CHILD", finalChild);
                colsNVals1.Add("OMI_TOTAL_RECEV", REC);


                rsltCode1 = cmnDM.Execute("CHRIS_SP_OMI_CLAIM_MANAGER", "UpdateMaster", colsNVals1);


            }


             return false;
        }
      
    }
}