using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn;
using iCORE.COMMON.SLCONTROLS;
using CrplControlLibrary;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_CFEntry_OMIDE03 : ChrisMasterDetailForm
    {
        #region --Global Varibale--
        CmnDataManager cmnDM = new CmnDataManager();
        string g_Child = string.Empty;
        int s_no = 0;
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_CFEntry_OMIDE03()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_CFEntry_OMIDE03(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            //lstDependentPanels.Add(this.pnlTblDept);
            lstDependentPanels.Add(this.pnltblDetl);
            this.pnlMain.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlMain);
            //this.pnlMain.LoadDependentPanels();
            colBillDate.AttachParentEntity = true;
            this.CurrentPanelBlock = pnlMain.Name;
        }
        #endregion

        #region--Override Method--
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                base.IterateFormToEnableControls(pnlMain.Controls, true);
                
                this.lblUserName.Text           = this.userID;
                this.txtOption.Visible          = false;
                this.txtDate.Text               = this.Now().ToString("dd/MM/yyyy");
                this.FunctionConfig.EnableF10   = true;
                this.FunctionConfig.F10         = Function.Save;
                tbtAdd.Visible                  = false;
                //this.lbtnPersonnel.SkipValidationOnLeave = false;
                txt_W_Pno.Select();
                txt_W_Pno.Focus();
                this.pnlMain.LoadDependentPanels();
                this.CurrentPanelBlock = pnlMain.Name;

                txt_W_OMI_CLAIM.Text        = txt_W_OMI_CLAIM.Text == string.Empty ? "0" : txt_W_OMI_CLAIM.Text;
                txt_OMI_TOTAL_CALIM1.Text   = txt_OMI_TOTAL_CALIM1.Text == string.Empty ? "0" : txt_OMI_TOTAL_CALIM1.Text;
                txt_W_OS_BALANCE.Text       = txt_W_OS_BALANCE.Text == string.Empty ? "0" : txt_W_OS_BALANCE.Text;
                txt_W_SELF.Text             = txt_W_SELF.Text == string.Empty ? "0" : txt_W_SELF.Text;
                txt_W_SPOUSE.Text           = txt_W_SPOUSE.Text == string.Empty ? "0" : txt_W_SPOUSE.Text;
                txt_W_CHILD.Text            = txt_W_CHILD.Text == string.Empty ? "0" : txt_W_CHILD.Text;
                txt_OMI_SELF.Text           = txt_OMI_SELF.Text == string.Empty ? "0" : txt_OMI_SELF.Text;
                txt_OMI_SPOUSE.Text         = txt_OMI_SPOUSE.Text == string.Empty ? "0" : txt_OMI_SPOUSE.Text;
                txt_OMI_CHILD.Text          = txt_OMI_CHILD.Text == string.Empty ? "0" : txt_OMI_CHILD.Text;
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Save")
            {

                txt_OMI_MONTH.Text = txt_W_MONTH.Text;
                txt_OMI_YEAR.Text = txt_W_YEAR.Text;


                int Total_No_of_Claims = dgvOmiDetail.RowCount - 1;
                iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMCommand)(this.pnlMain.CurrentBusinessEntity);
                ent.OMI_YEAR            = Convert.ToDecimal(txt_W_YEAR.Text);
                ent.OMI_MONTH           = Convert.ToDecimal(txt_W_MONTH.Text);
                ent.OMI_TOTAL_CLAIM     = Convert.ToDecimal(txt_W_Total.Text);
                ent.OMI_TOTAL_CLAIMS    = Convert.ToDecimal(txt_OMI_CLAIM_NO.Text);
                ent.ID                  = Convert.ToInt32(txt_ID.Text);


                iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMDETAILCommand entDtl = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMDETAILCommand)(this.pnltblDetl.CurrentBusinessEntity);
                entDtl.OMI_YEAR             = Convert.ToDecimal(txt_W_YEAR.Text);
                entDtl.OMI_MONTH            = Convert.ToDecimal(txt_W_MONTH.Text);
                entDtl.OMI_PATIENTS_NAME    = dgvOmiDetail.CurrentRow.Cells["OMI_PATIENTS_NAME"].Value.ToString();
                entDtl.OMI_CLAIM_NO         = Convert.ToDecimal(dgvOmiDetail.CurrentRow.Index);
                


                base.DoToolbarActions(ctrlsCollection, actionType);
            }
            else if(actionType == "Cancel")
            {
                dgvDept.GridSource = null;
                dgvDept.DataSource = dgvDept.GridSource;
                int i = 0;
                if (dgvDept.Rows.Count > 0)
                {
                    dgvDept.Rows[0].Cells["colprdno"].Value = null;
                    dgvDept.Rows[0].Cells["colseg"].Value   = null;
                    dgvDept.Rows[0].Cells["coldept"].Value  = null;
                    dgvDept.Rows[0].Cells["colcontr"].Value = null;
                }
                base.DoToolbarActions(ctrlsCollection, actionType);
                txt_W_Pno.Focus();
                txt_W_Pno.Select();
            }
            else
            {
                base.DoToolbarActions(ctrlsCollection, actionType);
            }
        }

        #endregion

        #region --Method--

        public void Proc_1()
        {
            try
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO",   txt_W_Pno.Text);
                param.Add("OMI_MONTH",  txt_OMI_MONTH.Text);
                param.Add("OMI_YEAR",   txt_OMI_YEAR.Text);

                rslt = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC_1", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_OMI_CLAIM_NO.Text = rslt.dstResult.Tables[0].Rows[0]["OMI_CLAIM_NO"].ToString();
                    }
                }

                txt_OMI_CLAIM_NO.Text = txt_OMI_CLAIM_NO.Text == string.Empty ? "1" : txt_OMI_CLAIM_NO.Text;
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc_1", exp);
                throw;
            }
        }

        public void PROC_2()
        {
            try
            {
                string omi_relation = dgvOmiDetail.CurrentRow.Cells["colRelation"].Value.ToString();
                if(omi_relation.Substring(0,1) == "D")
                {

                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_2", exp);
                
            }
        }

        public void PROC_3()
        {
            try
            {
                
            }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_3", exp);

            }
        }

        public bool PROC_4()
        {
            try
            {
                int str_S_no = 0;
                str_S_no = s_no;
                int amount = Convert.ToInt16(dgvOmiDetail.CurrentRow.Cells["colAmount"].EditedFormattedValue.ToString());
                string self = dgvOmiDetail.CurrentRow.Cells["colPatientName"].Value.ToString();

                /* FOR SELF CHEKING LIMITS */
                if (str_S_no == 1)
                {
                    txt_OMI_SELF.Text = Convert.ToString(Convert.ToInt32(txt_OMI_SELF.Text) - amount);

                    if (str_S_no == 1 && (txt_OMI_SELF.Text) == "0")
                    {
                        MessageBox.Show("OMI LIMIT FOR SELF IS  " + self + " REMAINING LIMIT IS " + txt_OMI_SELF.Text, "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;

                    }
                }



                /* FOR SPOUSE CHECKING LIMITS */
                if (str_S_no == 2)
                {
                    txt_OMI_SPOUSE.Text = Convert.ToString(Convert.ToInt32(txt_OMI_SPOUSE.Text) - amount);

                    if (str_S_no == 2 && (txt_OMI_SPOUSE.Text) == "0")
                    {
                        MessageBox.Show("OMI LIMIT FOR SPOUSE IS " + self + " REMAINING LIMIT IS " + txt_OMI_SPOUSE.Text, "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }



                /* FOR CHILDREN CHECKING LIMITS */
                if (str_S_no > 2)
                {
                    txt_OMI_CHILD.Text = Convert.ToString(Convert.ToInt32(txt_OMI_CHILD.Text) - amount);

                    if (str_S_no > 2 && (txt_OMI_CHILD.Text) == "0")
                    {
                        MessageBox.Show("OMI LIMIT FOR CHILDREN IS " + self + " REMAINING LIMIT IS " + txt_OMI_CHILD.Text, "Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_4", exp);
                return false;
            }
        }

        public void PROC_5()
        {
            try
            {
                int WW_SELF             = 0;
                int WW_SPOUSE           = 0;
                DateTime dtYearDate     = Convert.ToDateTime("31-Dec-" + txt_W_YEAR.Text);
                DateTime dtJoiningDate  = Convert.ToDateTime(dtp_W_JOINING_DATE.Value);
                DateTime dtMarriageDate = Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value);
                bool wSelfChk           = true;
                bool wSpouseChk         = true;
                int W_SELF              = 0;
                int W_SPOUSE            = 0;


                if ((Convert.ToDateTime(dtp_W_JOINING_DATE.Value).Year) == (Convert.ToDateTime(dtYearDate).Year) && 
                    txt_W_STATUS.Text == "M" && 
                    (Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value) < (Convert.ToDateTime(dtp_W_JOINING_DATE.Value))))
                {
                    wSelfChk    = int.TryParse(txt_W_SELF.Text, out W_SELF);
                    wSpouseChk  = int.TryParse(txt_W_SPOUSE.Text, out W_SPOUSE);
                    
                    if(wSelfChk == true && wSpouseChk == true )
                    {
                        WW_SELF     = (W_SELF / 12)   * ((base.MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtp_W_JOINING_DATE.Value))));
                        WW_SPOUSE   = (W_SPOUSE / 12) * ((base.MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtp_W_JOINING_DATE.Value))));
                    }
                }


                /* MARRIED AFTER JOINING AND MARRIAGE IS IN CURRENT YEAR */
                if (txt_W_STATUS.Text == "M" && 
                    Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value) > Convert.ToDateTime(dtp_W_JOINING_DATE.Value) && 
                    dtMarriageDate.Year.ToString() == txt_W_YEAR.Text)
                {
                    wSelfChk    = int.TryParse(txt_W_SELF.Text, out W_SELF);
                    wSpouseChk  = int.TryParse(txt_W_SPOUSE.Text, out W_SPOUSE);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = W_SELF;
                        WW_SPOUSE = (W_SPOUSE / 12) * ((base.MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value))));
                    }
                }


                /* MARRIED AND JOIND BEFOR CURRENT YEAR                  */
                if (txt_W_STATUS.Text == "M" && 
                    ((Convert.ToDateTime(dtp_W_JOINING_DATE.Value).Year) < (Convert.ToDateTime(dtYearDate).Year)) && 
                    ((Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value).Year < Convert.ToDateTime(dtYearDate).Year) || dtp_W_MARRIAGE_DATE.Value == null))
                {
                    wSelfChk = int.TryParse(txt_W_SELF.Text, out W_SELF);
                    wSpouseChk = int.TryParse(txt_W_SPOUSE.Text, out W_SPOUSE);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = W_SELF;
                        WW_SPOUSE = W_SPOUSE;//(W_SPOUSE / 12) * ((base.MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value))));
                    }
                }

                /* SINGLE AND JOINED IN THE CURRENT YEAR                */
                if ((txt_W_STATUS.Text == "S" || txt_W_STATUS.Text == "D" || txt_W_STATUS.Text == "W") && 
                    ((Convert.ToDateTime(dtp_W_JOINING_DATE.Value).Year) == Convert.ToDateTime(dtYearDate).Year))
                {
                     wSelfChk = int.TryParse(txt_W_SELF.Text, out W_SELF);
                    wSpouseChk = int.TryParse(txt_W_SPOUSE.Text, out W_SPOUSE);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = ((W_SELF/12) * (base.MonthsBetweenInOracle((Convert.ToDateTime(dtYearDate)) , (Convert.ToDateTime(dtp_W_JOINING_DATE.Value)))));
                    }
                }

                /* SINGLE AND JOINED BEFORE CURRENT YEAR                */
                if ((txt_W_STATUS.Text == "S" || txt_W_STATUS.Text == "D" || txt_W_STATUS.Text == "W") && 
                    ((Convert.ToDateTime(dtp_W_JOINING_DATE.Value).Year) < Convert.ToDateTime(dtYearDate).Year))
                {
                     wSelfChk = int.TryParse(txt_W_SELF.Text, out W_SELF);

                    if (wSelfChk)
                    {
                        WW_SELF = W_SELF;
                    }
                }


                
                txt_W_SELF.Text     = WW_SELF == null   ? "0" : WW_SELF.ToString();
                txt_W_SPOUSE.Text   = WW_SPOUSE == null ? "0" : WW_SPOUSE.ToString();

                if (txt_OMI_SPOUSE.Text == string.Empty && WW_SPOUSE > 0)
                {
                    txt_OMI_SPOUSE.Text = W_SPOUSE.ToString();
                }


            }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_5", exp);
            }
        }

        public void PROC_6()
        {
            try
            {
                int ww_child = 0;
                int w_omi   = 0;
                int var1    = 0;
                int var2    = 0;
                DateTime dtYearDate     = Convert.ToDateTime("31-Dec-" + txt_W_YEAR.Text);
                DateTime dtDateBirth = new DateTime();

                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_Pno.Text);
                rslt = cmnDM.GetData("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "OMI_CHILD", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in rslt.dstResult.Tables[0].Rows)
                        {
                            dtDateBirth     = Convert.ToDateTime(dr["PR_DATE_BIRTH"].ToString());

                            if (dtDateBirth.Year < dtYearDate.Year)
                            {
                                if ((Convert.ToDateTime(dtp_W_JOINING_DATE.Value).Year) < dtYearDate.Year)
                                {
                                    ww_child = ww_child + int.Parse(txt_W_CHILD.Text);
                                }
                                else
                                {
                                    ww_child = ((int.Parse(txt_W_CHILD.Text)) / 12) * (base.MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtp_W_JOINING_DATE.Value))) + ww_child;
                                }
                            }
                            else if (dtDateBirth.Year == dtYearDate.Year)
                            {
                                var1 = base.MonthsBetweenInOracle((Convert.ToDateTime(dtYearDate)), (Convert.ToDateTime(dtDateBirth)));
                                var2 = base.MonthsBetweenInOracle((Convert.ToDateTime(dtYearDate)), (Convert.ToDateTime(dtp_W_JOINING_DATE.Value)));

                                if (var1 > var2)
                                {
                                    ww_child = (((int.Parse(txt_W_CHILD.Text)) / 12) * var2 + ww_child);
                                }
                                else if (var1 < var2)
                                {
                                    ww_child = (((int.Parse(txt_W_CHILD.Text)) / 12) * var1 + ww_child);
                                }
                                else if (var1 == var2)
                                {
                                    ww_child = (((int.Parse(txt_W_CHILD.Text)) / 12) * var1 + ww_child);
                                }
                            }
                        }
                    }
                    txt_W_CHILD.Text    = Convert.ToString((ww_child - w_omi) + w_omi);
                    txt_OMI_CHILD.Text  = txt_W_CHILD.Text;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_6", exp);
            }
        }

        public void PROC_7()
        {
            try
            {
                string year = Now().Year.ToString();
                int DUMMY = 0;
                Result rslt;
                Result rsltIns;
                Result rsltUpd;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_OMI_P_NO.Text);
                param.Add("OMI_YEAR", year);
                rslt = cmnDM.GetData("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "PROC_7", param);
                param.Clear();
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count == 0)
                    {
                        rsltIns = cmnDM.Execute("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "PROC_7_INSERT", param);
                        param.Clear();

                        txt_OMI_SELF.Text   = txt_W_SELF.Text;
                        txt_OMI_SPOUSE.Text = txt_W_SPOUSE.Text;
                        txt_OMI_CHILD.Text  = txt_W_CHILD.Text;
                    }
                    else if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DUMMY = int.Parse(rslt.dstResult.Tables[0].Rows[0]["OMI_LIMIT"].ToString());

                        if (DUMMY != int.Parse(txt_W_OMI_LIMIT.Text))
                        {
                            param.Add("OMI_TOTAL_CLAIMS", txt_OMI_P_NO.Text);
                            param.Add("OMI_YEAR", year);
                            rsltUpd = cmnDM.Execute("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "PROC_7_UPDATE", param);
                            param.Clear();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_7", exp);
            }
        }

        public void PROC_8()
        {
            try
            {
                int SELF = 0;
                int WIFE = 0;
                int SON = 0;

                /*Self*/
                Dictionary<string, object> paramSelf = new Dictionary<string, object>();
                paramSelf.Add("OMI_P_NO", txt_W_Pno.Text);
                paramSelf.Add("OMI_YEAR", txt_W_YEAR.Text);

                Result rsltCodeS;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCodeS = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC8_SELF", paramSelf);

                if (rsltCodeS.isSuccessful && rsltCodeS.dstResult.Tables.Count > 0 && rsltCodeS.dstResult.Tables[0].Rows.Count > 0)
                {
                    SELF = Convert.ToInt32(rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString() == string.Empty ? "0" : rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString());
                }


                /*WIFE*/
                Dictionary<string, object> paramWife = new Dictionary<string, object>();
                paramWife.Add("OMI_P_NO", txt_W_Pno.Text);
                paramWife.Add("OMI_YEAR", txt_W_YEAR.Text);

                Result rsltCodeW;
                rsltCodeW = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC8_WIFE", paramWife);

                if (rsltCodeW.isSuccessful && rsltCodeW.dstResult.Tables.Count > 0 && rsltCodeW.dstResult.Tables[0].Rows.Count > 0)
                {
                    WIFE = Convert.ToInt32(rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString() == string.Empty ? "0" : rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramSon = new Dictionary<string, object>();
                paramSon.Add("OMI_P_NO", txt_W_Pno.Text);
                paramSon.Add("OMI_YEAR", txt_W_YEAR.Text);

                Result rsltCodeSon;
                rsltCodeSon = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC8_SON", paramSon);

                if (rsltCodeSon.isSuccessful && rsltCodeSon.dstResult.Tables.Count > 0 && rsltCodeSon.dstResult.Tables[0].Rows.Count > 0)
                {
                    SON = Convert.ToInt32(rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString() == string.Empty ? "0" : rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString());
                }

                txt_OMI_SELF.Text           = Convert.ToString((Convert.ToInt32(txt_W_SELF.Text == string.Empty ? "0" : txt_W_SELF.Text)) + SELF);
                txt_OMI_SPOUSE.Text         = Convert.ToString((Convert.ToInt32(txt_W_SPOUSE.Text == string.Empty ? "0" : txt_W_SPOUSE.Text)) + WIFE);
                txt_OMI_CHILD.Text          = Convert.ToString((Convert.ToInt32(txt_W_CHILD.Text == string.Empty ? "0" : txt_W_CHILD.Text)) + SON);
                txt_OMI_TOTAL_CALIM1.Text   = Convert.ToString(SELF + WIFE + SON);

                }
            catch (Exception exp)
            {
                LogError(this.Name, "PROC_8", exp);
            }
        }

        #endregion

        #region --Events--
        private void txt_W_MONTH_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_W_MONTH.Text != string.Empty)
                {
                    int month = Convert.ToInt32(txt_W_MONTH.Text.ToString());

                    if (month < 0 || month > 12)
                    {
                        MessageBox.Show("ENTER A VALID MONTH OR   ...... [F6]=EXIT", "Form",MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_MONTH_Validating", exp);
            }
        }

        private void txt_W_YEAR_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (txt_W_YEAR.Text != string.Empty)
                {
                    int Year = Convert.ToInt32(txt_W_YEAR.Text.ToString());

                    if (Year == 0)
                    {
                        MessageBox.Show("ENTER YEAR OR [F6]=EXIT", "Form",MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_YEAR_Validating", exp);
            }
        }

        private void txt_W_OMI_TOTAL_CLAIM_Enter(object sender, EventArgs e)
        {
            
        }

        private void dgvOmiDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
             {
                 int index = 0;
                //dgvOmiDetail.CurrentRow.Cells["colOmiPNo"].Value = txt_PR_P_NO.Text;

                OMICLAIMDETAILCommand ent = (OMICLAIMDETAILCommand)(this.pnltblDetl.CurrentBusinessEntity);
                ent.OMI_P_NO = Convert.ToDecimal(txt_W_Pno.Text);

                if (dgvOmiDetail.CurrentCell.OwningColumn.Name.Equals("colBillDate"))
                {
                    index = Convert.ToInt32(dgvOmiDetail.CurrentRow.Index.ToString());

                    dgvOmiDetail.CurrentRow.Cells["Serial_Num"].Value = index + 1;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "dgvOmiDetail_EditingControlShowing", exp);
            }
        }

        private void dgvOmiDetail_Validated(object sender, EventArgs e)
        {
            try
            {
                if (dgvOmiDetail.CurrentCell.OwningColumn.Name.Equals("colBillDate"))
                {
                    dgvOmiDetail.CurrentRow.Cells["colSNo"].Value = dgvOmiDetail.CurrentRow.Index.ToString();
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "dgvOmiDetail_Validated", exp);
            }
        }

        private void dgvOmiDetail_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dgvOmiDetail.CurrentCell.OwningColumn.Name.Equals("colBillDate"))
                {
                    if (dgvDept.CurrentCell.Value != null)
                    {
                        if (dgvDept.CurrentCell.EditedFormattedValue != "")
                        {
                            bool isCorrectDate = IsValidExpression(dgvDept.CurrentCell.EditedFormattedValue.ToString(), InputValidator.DATETIME_REGEX);

                            if (!isCorrectDate)
                            {
                                e.Cancel = true;
                            }
                        }
                    }
                }
                else if (dgvOmiDetail.CurrentCell.OwningColumn.Name.Equals("colAmount"))
                {
                    s_no = Convert.ToInt32(dgvOmiDetail.CurrentRow.Cells["colSNo"].Value.ToString());

                    if (dgvOmiDetail.CurrentCell.EditedFormattedValue == string.Empty)
                    {
                        MessageBox.Show("ENTER  CLAIM AMOUNT ......", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                    else
                    {
                        /* WARNING FOR OMI LIMIT IS BURSTING */
                        if (Convert.ToInt32(txt_OMI_TOTAL_CALIM1.Text == string.Empty ? "0" : txt_OMI_TOTAL_CALIM1.Text) > Convert.ToInt32(txt_W_OMI_LIMIT.Text == string.Empty ? "0" : txt_W_OMI_LIMIT.Text))
                        {
                            MessageBox.Show("TOTAL CLAIM AMOUNT IS EXCEEDING THE OMI LIMIT", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }

                        int Total                   = Convert.ToInt32(dgvOmiDetail.CurrentCell.EditedFormattedValue) + Convert.ToInt32(txt_W_Total.Text == string.Empty ? "0" : txt_W_Total.Text);
                        txt_W_Total.Text            = Total.ToString();
                        int TotalClaim1             = Convert.ToInt32(txt_OMI_TOTAL_CALIM1.Text == string.Empty ? "0" : txt_OMI_TOTAL_CALIM1.Text) + Convert.ToInt32(dgvOmiDetail.CurrentCell.EditedFormattedValue);
                        txt_OMI_TOTAL_CALIM1.Text   = TotalClaim1.ToString();

                       
                        int OS_Balance = Convert.ToInt32(txt_W_OMI_LIMIT.Text == string.Empty ? "0" : txt_W_OMI_LIMIT.Text) - Convert.ToInt32(txt_OMI_TOTAL_CALIM1.Text == string.Empty ? "0" : txt_OMI_TOTAL_CALIM1.Text);
                        txt_W_OS_BALANCE.Text = OS_Balance.ToString();

                        /* CHEKING OMI LIMITS   */
                       bool chk = PROC_4();
                       //if (!chk)
                       //    e.Cancel = true;
                        
                        /* END OF CHECKING */

                    }
                }
                
            }
            catch (Exception exp)
            {
                LogError(this.Name, "dgvOmiDetail_Validating", exp);
            }
        }

        #endregion

        private void txt_W_OMI_LIMIT_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txt_W_YEAR.Text != string.Empty && txt_W_MONTH.Text != string.Empty)
                {
                    string Date = "01/" + txt_W_MONTH.Text + "/" + txt_W_YEAR.Text;



                    Result rsltLIST;
                    Dictionary<string, object> param6 = new Dictionary<string, object>();
                    param6.Add("OMI_P_NO", txt_W_Pno.Text);
                    param6.Add("OMI_MONTH", txt_W_MONTH.Text);
                    param6.Add("OMI_YEAR", txt_W_YEAR.Text);

                    rsltLIST = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntry_OMI_CLAIM_DETAIL_MANAGER", "List", param6);
                    if (rsltLIST.isSuccessful)
                    {
                        if (rsltLIST.dstResult != null && rsltLIST.dstResult.Tables[0].Rows.Count > 0)
                        {
                            dgvOmiDetail.DataSource = rsltLIST.dstResult.Tables[0];
                            int Total = Convert.ToInt32(dgvOmiDetail.CurrentRow.Cells["colAmount"].EditedFormattedValue) + Convert.ToInt32(txt_W_Total.Text == string.Empty ? "0" : txt_W_Total.Text);
                            txt_W_Total.Text        = Total.ToString();
                        }
                    }
                    

                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("SEARCHFILTER", Date);
                    rslt = cmnDM.GetData("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "OMI_LIMIT", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            //:W_OMI_LIMIT,:W_SELF,:W_SPOUSE,:W_CHILD
                            txt_W_OMI_LIMIT.Text = rslt.dstResult.Tables[0].Rows[0]["W_OMI_LIMIT"].ToString();
                            txt_W_SELF.Text = rslt.dstResult.Tables[0].Rows[0]["W_SELF"].ToString();
                            txt_W_SPOUSE.Text = rslt.dstResult.Tables[0].Rows[0]["W_SPOUSE"].ToString();
                            txt_W_CHILD.Text = rslt.dstResult.Tables[0].Rows[0]["W_CHILD"].ToString();
                            txt_PR_P_NO.Text = txt_OMI_P_NO.Text;
                        }
                        else if (rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            txt_W_OMI_LIMIT.Text = "0";
                            txt_W_SELF.Text = "0";
                            txt_W_SPOUSE.Text = "0";
                            txt_W_CHILD.Text = "0";
                            txt_PR_P_NO.Text = txt_OMI_P_NO.Text;
                        }
                    }

                    /* CALLING PROCEDURE TO PRO-RATE OMI LIMIT */
                    if (txt_OMI_CHILD.Text == string.Empty)
                    {
                        g_Child = "0";
                    }
                    else
                    {
                        g_Child = txt_OMI_CHILD.Text;
                    }


                    PROC_5();

                    PROC_6();

                    int spouse = txt_W_SPOUSE.Text == "" ? 0 : int.Parse(txt_W_SPOUSE.Text);
                    int self = txt_W_SELF.Text == "" ? 0 : int.Parse(txt_W_SELF.Text);
                    int child = txt_W_CHILD.Text == "" ? 0 : int.Parse(txt_W_CHILD.Text);

                    txt_W_OMI_LIMIT.Text = Convert.ToString(spouse + self + child);


                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            txt_OMI_SELF.Text = txt_W_SELF.Text;
                            txt_OMI_SPOUSE.Text = txt_W_SPOUSE.Text;
                            txt_OMI_CHILD.Text = txt_W_CHILD.Text;
                        }
                    }


                    PROC_8();
                    /* UPDATE OMI LIMITS EVERY TIME DUE TO MARRIAGE OR BIRTH OF CHILD*/
                    PROC_7();
                    dgvOmiDetail.Focus();

                    OMICLAIMDETAILCommand ent = (OMICLAIMDETAILCommand)(this.pnltblDetl.CurrentBusinessEntity);
                    ent.OMI_P_NO = Convert.ToDecimal(txt_W_Pno.Text);

                    int OS_Balance = 0;
                    OS_Balance = Convert.ToInt32(txt_W_OMI_LIMIT.Text == string.Empty ? "0" : txt_W_OMI_LIMIT.Text) - Convert.ToInt32(txt_OMI_TOTAL_CALIM1.Text == string.Empty ? "0" : txt_OMI_TOTAL_CALIM1.Text);
                    txt_W_OS_BALANCE.Text = OS_Balance.ToString();

                    Proc_1();
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_OMI_LIMIT_Enter", exp);
            }
        }

        private void txt_W_Pno_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_Pno.Text != string.Empty)
            {

                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", txt_W_Pno.Text);
                param.Add("pr_transfer_date", dtp_W_TRANSFER_DATE.Value.ToString());
                DataTable dt = new DataTable();

                rslt = cmnDM.GetData("CHRIS_SP_Fin_Liquidate_DEPT_CONT_GETALL", "", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dt                  = rslt.dstResult.Tables[0];
                        dgvDept.GridSource  = dt;
                        int i = 0;
                        foreach (DataRow dr in dt.Rows)
                        {

                            dgvDept.Rows[i].Cells["colprdno"].Value = dt.Rows[i]["pr_d_no"].ToString();
                            dgvDept.Rows[i].Cells["colseg"].Value   = dt.Rows[i]["pr_segment"].ToString();
                            dgvDept.Rows[i].Cells["coldept"].Value  = dt.Rows[i]["pr_dept"].ToString();
                            dgvDept.Rows[i].Cells["colcontr"].Value = dt.Rows[i]["pr_contrib"].ToString();
                            i++;
                        }
                    }
                }
            }
        }
    }
}