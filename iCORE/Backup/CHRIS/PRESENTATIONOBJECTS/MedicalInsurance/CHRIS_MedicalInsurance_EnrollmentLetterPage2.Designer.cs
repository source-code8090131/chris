namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_EnrollmentLetterPage2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_EnrollmentLetterPage2));
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.pnlEnrollLetter = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvPersonal = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPr_P_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDJoining = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEnrolled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            this.pnlEnrollLetter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonal)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(473, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(509, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 298);
            this.panel1.Size = new System.Drawing.Size(509, 60);
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // pnlEnrollLetter
            // 
            this.pnlEnrollLetter.ConcurrentPanels = null;
            this.pnlEnrollLetter.Controls.Add(this.dgvPersonal);
            this.pnlEnrollLetter.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlEnrollLetter.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlEnrollLetter.DependentPanels = null;
            this.pnlEnrollLetter.DisableDependentLoad = false;
            this.pnlEnrollLetter.EnableDelete = true;
            this.pnlEnrollLetter.EnableInsert = true;
            this.pnlEnrollLetter.EnableQuery = false;
            this.pnlEnrollLetter.EnableUpdate = true;
            this.pnlEnrollLetter.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RegStHiEntPERSONALCommand";
            this.pnlEnrollLetter.Location = new System.Drawing.Point(12, 77);
            this.pnlEnrollLetter.MasterPanel = null;
            this.pnlEnrollLetter.Name = "pnlEnrollLetter";
            this.pnlEnrollLetter.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlEnrollLetter.Size = new System.Drawing.Size(489, 217);
            this.pnlEnrollLetter.SPName = "CHRIS_SP_EnrLettPERSONNEL_MANAGER";
            this.pnlEnrollLetter.TabIndex = 10;
            // 
            // dgvPersonal
            // 
            this.dgvPersonal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersonal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPr_P_No,
            this.colName,
            this.colDJoining,
            this.colEnrolled});
            this.dgvPersonal.ColumnToHide = "colPr_P_No|colName|colDJoining";
            this.dgvPersonal.ColumnWidth = null;
            this.dgvPersonal.CustomEnabled = true;
            this.dgvPersonal.DisplayColumnWrapper = null;
            this.dgvPersonal.GridDefaultRow = 0;
            this.dgvPersonal.Location = new System.Drawing.Point(3, 3);
            this.dgvPersonal.Name = "dgvPersonal";
            this.dgvPersonal.ReadOnlyColumns = null;
            this.dgvPersonal.RequiredColumns = null;
            this.dgvPersonal.Size = new System.Drawing.Size(484, 211);
            this.dgvPersonal.SkippingColumns = null;
            this.dgvPersonal.TabIndex = 0;
            // 
            // colPr_P_No
            // 
            this.colPr_P_No.DataPropertyName = "PR_PA_NO";
            this.colPr_P_No.HeaderText = "P.No.";
            this.colPr_P_No.Name = "colPr_P_No";
            this.colPr_P_No.ReadOnly = true;
            this.colPr_P_No.Width = 70;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "NAME";
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colDJoining
            // 
            this.colDJoining.DataPropertyName = "PR_JOINING_DATE";
            this.colDJoining.HeaderText = "Date of Joining";
            this.colDJoining.Name = "colDJoining";
            this.colDJoining.ReadOnly = true;
            this.colDJoining.Width = 130;
            // 
            // colEnrolled
            // 
            this.colEnrolled.DataPropertyName = "PR_BANK_ID";
            this.colEnrolled.HeaderText = "Enrolled [Y/N]";
            this.colEnrolled.MaxInputLength = 1;
            this.colEnrolled.Name = "colEnrolled";
            // 
            // CHRIS_MedicalInsurance_EnrollmentLetterPage2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 358);
            this.Controls.Add(this.pnlEnrollLetter);
            this.Name = "CHRIS_MedicalInsurance_EnrollmentLetterPage2";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_EnrollmentLetterPage2";
            this.Controls.SetChildIndex(this.pnlEnrollLetter, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            this.pnlEnrollLetter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Diagnostics.EventLog eventLog1;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlEnrollLetter;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPr_P_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDJoining;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEnrolled;
    }
}