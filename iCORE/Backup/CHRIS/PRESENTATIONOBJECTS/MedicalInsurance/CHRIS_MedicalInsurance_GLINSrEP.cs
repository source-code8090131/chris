using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_GLInsRep : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_MedicalInsurance_GLInsRep()
        {
            InitializeComponent();
        }
        public CHRIS_MedicalInsurance_GLInsRep(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.cmbDescType.Items.RemoveAt(5);
            this.dest_format.Text = "wide";
            this.copies.Text = "1";

        }

        private void Run_Click(object sender, EventArgs e)
        {
            {
                base.RptFileName = "gli01";
                //base.btnCallReport_Click(sender, e);

                if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (cmbDescType.Text == "Printer")
                {
                    base.PrintCustomReport(this.copies.Text);
                }
                else if (cmbDescType.Text == "File")
                {

                    string DestName;

                    if (this.Dest_Name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_Name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (cmbDescType.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";

                    string RecipentName;
                    if (this.Dest_Name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_Name.Text;
                    }



                    base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }

        }

        private void slButton1_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }

        private void cmbDescType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}