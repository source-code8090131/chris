namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_EnrollmentLetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_EnrollmentLetter));
            this.pnlEnrollEntryMain = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDataGridView1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDJoining = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEnrolled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.txtChoice = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlEnrollEntryMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(604, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(640, 22);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(0, 440);
            this.panel1.Size = new System.Drawing.Size(640, 60);
            this.panel1.Controls.SetChildIndex(this.label4, 0);
            this.panel1.Controls.SetChildIndex(this.pnlBottom, 0);
            // 
            // pnlEnrollEntryMain
            // 
            this.pnlEnrollEntryMain.ConcurrentPanels = null;
            this.pnlEnrollEntryMain.Controls.Add(this.slDataGridView1);
            this.pnlEnrollEntryMain.Controls.Add(this.label5);
            this.pnlEnrollEntryMain.Controls.Add(this.txtDate);
            this.pnlEnrollEntryMain.Controls.Add(this.label3);
            this.pnlEnrollEntryMain.Controls.Add(this.label2);
            this.pnlEnrollEntryMain.Controls.Add(this.label1);
            this.pnlEnrollEntryMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlEnrollEntryMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlEnrollEntryMain.DependentPanels = null;
            this.pnlEnrollEntryMain.DisableDependentLoad = false;
            this.pnlEnrollEntryMain.EnableDelete = true;
            this.pnlEnrollEntryMain.EnableInsert = true;
            this.pnlEnrollEntryMain.EnableQuery = false;
            this.pnlEnrollEntryMain.EnableUpdate = true;
            this.pnlEnrollEntryMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlEnrollEntryMain.Location = new System.Drawing.Point(5, 77);
            this.pnlEnrollEntryMain.MasterPanel = null;
            this.pnlEnrollEntryMain.Name = "pnlEnrollEntryMain";
            this.pnlEnrollEntryMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlEnrollEntryMain.Size = new System.Drawing.Size(635, 321);
            this.pnlEnrollEntryMain.SPName = "CHRIS_SP_EnrLettPERSONNEL_MANAGER";
            this.pnlEnrollEntryMain.TabIndex = 10;
            // 
            // slDataGridView1
            // 
            this.slDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPNo,
            this.colName,
            this.colDJoining,
            this.colEnrolled});
            this.slDataGridView1.ColumnToHide = null;
            this.slDataGridView1.ColumnWidth = null;
            this.slDataGridView1.CustomEnabled = true;
            this.slDataGridView1.DisplayColumnWrapper = null;
            this.slDataGridView1.GridDefaultRow = 0;
            this.slDataGridView1.Location = new System.Drawing.Point(11, 77);
            this.slDataGridView1.Name = "slDataGridView1";
            this.slDataGridView1.ReadOnlyColumns = null;
            this.slDataGridView1.RequiredColumns = null;
            this.slDataGridView1.Size = new System.Drawing.Size(615, 241);
            this.slDataGridView1.SkippingColumns = null;
            this.slDataGridView1.TabIndex = 5;
            // 
            // colPNo
            // 
            this.colPNo.HeaderText = "P.No.";
            this.colPNo.Name = "colPNo";
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.Width = 250;
            // 
            // colDJoining
            // 
            this.colDJoining.HeaderText = "Date of Joining";
            this.colDJoining.Name = "colDJoining";
            this.colDJoining.Width = 120;
            // 
            // colEnrolled
            // 
            this.colEnrolled.HeaderText = "Enrolled [Y/N]";
            this.colEnrolled.Name = "colEnrolled";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(616, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "_________________________________________________________________________________" +
                "______";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(541, 7);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(81, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 3;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(493, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(249, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ENROLLMENT ENTRY";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(260, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "LIFE INSURANCE";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(495, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 11;
            this.lblUserName.Text = "UserName :";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(430, 9);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(63, 13);
            this.lblUser.TabIndex = 12;
            this.lblUser.Text = "UserName :";
            // 
            // txtChoice
            // 
            this.txtChoice.AllowSpace = true;
            this.txtChoice.AssociatedLookUpName = "";
            this.txtChoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChoice.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtChoice.ContinuationTextBox = null;
            this.txtChoice.CustomEnabled = true;
            this.txtChoice.DataFieldMapping = "";
            this.txtChoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoice.GetRecordsOnUpDownKeys = false;
            this.txtChoice.IsDate = false;
            this.txtChoice.Location = new System.Drawing.Point(581, 419);
            this.txtChoice.MaxLength = 1;
            this.txtChoice.Name = "txtChoice";
            this.txtChoice.NumberFormat = "###,###,##0.00";
            this.txtChoice.Postfix = "";
            this.txtChoice.Prefix = "";
            this.txtChoice.Size = new System.Drawing.Size(56, 20);
            this.txtChoice.SkipValidation = false;
            this.txtChoice.TabIndex = 0;
            this.txtChoice.TextType = CrplControlLibrary.TextType.String;
            this.txtChoice.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtChoice_PreviewKeyDown);
            this.txtChoice.Validating += new System.ComponentModel.CancelEventHandler(this.txtChoice_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 423);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "1) DATE OF JOINING RANGE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(213, 423);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "2) IND.EMPLOYEE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(342, 423);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(216, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "3) ALL NOT INROLLED IN LIFE INS.";
            // 
            // CHRIS_MedicalInsurance_EnrollmentLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 500);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtChoice);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlEnrollEntryMain);
            this.Name = "CHRIS_MedicalInsurance_EnrollmentLetter";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_EnrollmentLetter";
            this.Controls.SetChildIndex(this.pnlEnrollEntryMain, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.Controls.SetChildIndex(this.txtChoice, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlEnrollEntryMain.ResumeLayout(false);
            this.pnlEnrollEntryMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlEnrollEntryMain;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblUser;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDJoining;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEnrolled;
        private CrplControlLibrary.SLTextBox txtChoice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;

    }
}