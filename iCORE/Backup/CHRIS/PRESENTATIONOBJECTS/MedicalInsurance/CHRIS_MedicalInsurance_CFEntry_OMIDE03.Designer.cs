namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_CFEntry_OMIDE03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_CFEntry_OMIDE03));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMain = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_ID = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_CLAIM_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.dtp_W_MARRIAGE_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtp_W_TRANSFER_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtp_W_JOINING_DATE = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtnMonthYear = new CrplControlLibrary.LookupButton(this.components);
            this.lbtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.txt_OMI_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_MONTH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OMI_LIMIT = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_STATUS = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_SNO = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_CAT = new CrplControlLibrary.SLTextBox(this.components);
            this.label38 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_OMI_TOTAL_CLAIMS = new CrplControlLibrary.SLTextBox(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_OMI_CHILD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_SPOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_SELF = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_CHILD = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_SPOUSE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_SELF = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OS_BALANCE = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_OMI_TOTAL_CALIM1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_OMI_CLAIM = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_MONTH = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_BNAME = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Pno = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlTblDept = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDept = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPrDNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSeg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colContr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnltblDetl = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.txt_PR_P_NO = new CrplControlLibrary.SLTextBox(this.components);
            this.dgvOmiDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.Serial_Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBillDate = new iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn();
            this.OMI_PATIENTS_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRelation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmiPNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmiMonth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOmiYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_W_Total = new CrplControlLibrary.SLTextBox(this.components);
            this.label37 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.pnlTblDept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).BeginInit();
            this.pnltblDetl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOmiDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(611, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(647, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 583);
            this.panel1.Size = new System.Drawing.Size(647, 60);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.ConcurrentPanels = null;
            this.pnlMain.Controls.Add(this.txt_ID);
            this.pnlMain.Controls.Add(this.txt_OMI_CLAIM_NO);
            this.pnlMain.Controls.Add(this.dtp_W_MARRIAGE_DATE);
            this.pnlMain.Controls.Add(this.dtp_W_TRANSFER_DATE);
            this.pnlMain.Controls.Add(this.dtp_W_JOINING_DATE);
            this.pnlMain.Controls.Add(this.lbtnMonthYear);
            this.pnlMain.Controls.Add(this.lbtnPersonnel);
            this.pnlMain.Controls.Add(this.txt_OMI_YEAR);
            this.pnlMain.Controls.Add(this.txt_OMI_MONTH);
            this.pnlMain.Controls.Add(this.txt_OMI_P_NO);
            this.pnlMain.Controls.Add(this.txt_W_OMI_LIMIT);
            this.pnlMain.Controls.Add(this.txt_W_STATUS);
            this.pnlMain.Controls.Add(this.txt_W_SNO);
            this.pnlMain.Controls.Add(this.txt_W_CAT);
            this.pnlMain.Controls.Add(this.label38);
            this.pnlMain.Controls.Add(this.label18);
            this.pnlMain.Controls.Add(this.label19);
            this.pnlMain.Controls.Add(this.label20);
            this.pnlMain.Controls.Add(this.label34);
            this.pnlMain.Controls.Add(this.label35);
            this.pnlMain.Controls.Add(this.label36);
            this.pnlMain.Controls.Add(this.label33);
            this.pnlMain.Controls.Add(this.label32);
            this.pnlMain.Controls.Add(this.label31);
            this.pnlMain.Controls.Add(this.label30);
            this.pnlMain.Controls.Add(this.label29);
            this.pnlMain.Controls.Add(this.label28);
            this.pnlMain.Controls.Add(this.label27);
            this.pnlMain.Controls.Add(this.label26);
            this.pnlMain.Controls.Add(this.label25);
            this.pnlMain.Controls.Add(this.label24);
            this.pnlMain.Controls.Add(this.label23);
            this.pnlMain.Controls.Add(this.label22);
            this.pnlMain.Controls.Add(this.txt_OMI_TOTAL_CLAIMS);
            this.pnlMain.Controls.Add(this.label21);
            this.pnlMain.Controls.Add(this.label17);
            this.pnlMain.Controls.Add(this.label16);
            this.pnlMain.Controls.Add(this.label15);
            this.pnlMain.Controls.Add(this.label12);
            this.pnlMain.Controls.Add(this.label11);
            this.pnlMain.Controls.Add(this.label10);
            this.pnlMain.Controls.Add(this.label9);
            this.pnlMain.Controls.Add(this.label8);
            this.pnlMain.Controls.Add(this.txt_OMI_CHILD);
            this.pnlMain.Controls.Add(this.txt_OMI_SPOUSE);
            this.pnlMain.Controls.Add(this.txt_OMI_SELF);
            this.pnlMain.Controls.Add(this.txt_W_CHILD);
            this.pnlMain.Controls.Add(this.txt_W_SPOUSE);
            this.pnlMain.Controls.Add(this.txt_W_SELF);
            this.pnlMain.Controls.Add(this.txt_W_OS_BALANCE);
            this.pnlMain.Controls.Add(this.txt_OMI_TOTAL_CALIM1);
            this.pnlMain.Controls.Add(this.txt_W_OMI_CLAIM);
            this.pnlMain.Controls.Add(this.txt_W_YEAR);
            this.pnlMain.Controls.Add(this.txt_W_MONTH);
            this.pnlMain.Controls.Add(this.txt_W_BNAME);
            this.pnlMain.Controls.Add(this.txt_W_Name);
            this.pnlMain.Controls.Add(this.txt_W_Pno);
            this.pnlMain.Controls.Add(this.label7);
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.label13);
            this.pnlMain.Controls.Add(this.label14);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.txtDate);
            this.pnlMain.Controls.Add(this.txtLocation);
            this.pnlMain.Controls.Add(this.txtUser);
            this.pnlMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMain.DependentPanels = null;
            this.pnlMain.DisableDependentLoad = false;
            this.pnlMain.EnableDelete = true;
            this.pnlMain.EnableInsert = true;
            this.pnlMain.EnableQuery = false;
            this.pnlMain.EnableUpdate = true;
            this.pnlMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMCommand";
            this.pnlMain.Location = new System.Drawing.Point(12, 74);
            this.pnlMain.MasterPanel = null;
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMain.Size = new System.Drawing.Size(628, 283);
            this.pnlMain.SPName = "CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER";
            this.pnlMain.TabIndex = 0;
            // 
            // txt_ID
            // 
            this.txt_ID.AllowSpace = true;
            this.txt_ID.AssociatedLookUpName = "";
            this.txt_ID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_ID.ContinuationTextBox = null;
            this.txt_ID.CustomEnabled = true;
            this.txt_ID.DataFieldMapping = "ID";
            this.txt_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ID.GetRecordsOnUpDownKeys = false;
            this.txt_ID.IsDate = false;
            this.txt_ID.Location = new System.Drawing.Point(679, 202);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.NumberFormat = "###,###,##0.00";
            this.txt_ID.Postfix = "";
            this.txt_ID.Prefix = "";
            this.txt_ID.Size = new System.Drawing.Size(33, 20);
            this.txt_ID.SkipValidation = false;
            this.txt_ID.TabIndex = 130;
            this.txt_ID.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_CLAIM_NO
            // 
            this.txt_OMI_CLAIM_NO.AllowSpace = true;
            this.txt_OMI_CLAIM_NO.AssociatedLookUpName = "";
            this.txt_OMI_CLAIM_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_CLAIM_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_CLAIM_NO.ContinuationTextBox = null;
            this.txt_OMI_CLAIM_NO.CustomEnabled = true;
            this.txt_OMI_CLAIM_NO.DataFieldMapping = "OMI_CLAIM_NO";
            this.txt_OMI_CLAIM_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_CLAIM_NO.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_CLAIM_NO.IsDate = false;
            this.txt_OMI_CLAIM_NO.Location = new System.Drawing.Point(676, 101);
            this.txt_OMI_CLAIM_NO.Name = "txt_OMI_CLAIM_NO";
            this.txt_OMI_CLAIM_NO.NumberFormat = "###,###,##0.00";
            this.txt_OMI_CLAIM_NO.Postfix = "";
            this.txt_OMI_CLAIM_NO.Prefix = "";
            this.txt_OMI_CLAIM_NO.Size = new System.Drawing.Size(33, 20);
            this.txt_OMI_CLAIM_NO.SkipValidation = false;
            this.txt_OMI_CLAIM_NO.TabIndex = 129;
            this.txt_OMI_CLAIM_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // dtp_W_MARRIAGE_DATE
            // 
            this.dtp_W_MARRIAGE_DATE.CustomEnabled = true;
            this.dtp_W_MARRIAGE_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtp_W_MARRIAGE_DATE.DataFieldMapping = "W_MARRIAGE_DATE";
            this.dtp_W_MARRIAGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_W_MARRIAGE_DATE.HasChanges = true;
            this.dtp_W_MARRIAGE_DATE.Location = new System.Drawing.Point(635, 121);
            this.dtp_W_MARRIAGE_DATE.Name = "dtp_W_MARRIAGE_DATE";
            this.dtp_W_MARRIAGE_DATE.NullValue = " ";
            this.dtp_W_MARRIAGE_DATE.Size = new System.Drawing.Size(77, 20);
            this.dtp_W_MARRIAGE_DATE.TabIndex = 128;
            this.dtp_W_MARRIAGE_DATE.Value = new System.DateTime(2011, 5, 4, 0, 0, 0, 0);
            // 
            // dtp_W_TRANSFER_DATE
            // 
            this.dtp_W_TRANSFER_DATE.CustomEnabled = true;
            this.dtp_W_TRANSFER_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtp_W_TRANSFER_DATE.DataFieldMapping = "W_TRANSFER_DATE";
            this.dtp_W_TRANSFER_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_W_TRANSFER_DATE.HasChanges = true;
            this.dtp_W_TRANSFER_DATE.Location = new System.Drawing.Point(635, 161);
            this.dtp_W_TRANSFER_DATE.Name = "dtp_W_TRANSFER_DATE";
            this.dtp_W_TRANSFER_DATE.NullValue = " ";
            this.dtp_W_TRANSFER_DATE.Size = new System.Drawing.Size(77, 20);
            this.dtp_W_TRANSFER_DATE.TabIndex = 127;
            this.dtp_W_TRANSFER_DATE.Value = new System.DateTime(2011, 5, 4, 0, 0, 0, 0);
            // 
            // dtp_W_JOINING_DATE
            // 
            this.dtp_W_JOINING_DATE.CustomEnabled = true;
            this.dtp_W_JOINING_DATE.CustomFormat = "dd/MM/yyyy";
            this.dtp_W_JOINING_DATE.DataFieldMapping = "W_JOINING_DATE";
            this.dtp_W_JOINING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_W_JOINING_DATE.HasChanges = true;
            this.dtp_W_JOINING_DATE.Location = new System.Drawing.Point(635, 141);
            this.dtp_W_JOINING_DATE.Name = "dtp_W_JOINING_DATE";
            this.dtp_W_JOINING_DATE.NullValue = " ";
            this.dtp_W_JOINING_DATE.Size = new System.Drawing.Size(77, 20);
            this.dtp_W_JOINING_DATE.TabIndex = 126;
            this.dtp_W_JOINING_DATE.Value = new System.DateTime(2011, 5, 4, 0, 0, 0, 0);
            // 
            // lbtnMonthYear
            // 
            this.lbtnMonthYear.ActionLOVExists = "MONTHYEAR_LOVEXISTS";
            this.lbtnMonthYear.ActionType = "MONTHYEAR_LOV";
            this.lbtnMonthYear.ConditionalFields = "txt_W_Pno";
            this.lbtnMonthYear.CustomEnabled = true;
            this.lbtnMonthYear.DataFieldMapping = "";
            this.lbtnMonthYear.DependentLovControls = "";
            this.lbtnMonthYear.HiddenColumns = "";
            this.lbtnMonthYear.Image = ((System.Drawing.Image)(resources.GetObject("lbtnMonthYear.Image")));
            this.lbtnMonthYear.LoadDependentEntities = true;
            this.lbtnMonthYear.Location = new System.Drawing.Point(291, 159);
            this.lbtnMonthYear.LookUpTitle = null;
            this.lbtnMonthYear.Name = "lbtnMonthYear";
            this.lbtnMonthYear.Size = new System.Drawing.Size(26, 21);
            this.lbtnMonthYear.SkipValidationOnLeave = false;
            this.lbtnMonthYear.SPName = "CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER";
            this.lbtnMonthYear.TabIndex = 124;
            this.lbtnMonthYear.TabStop = false;
            this.lbtnMonthYear.UseVisualStyleBackColor = true;
            // 
            // lbtnPersonnel
            // 
            this.lbtnPersonnel.ActionLOVExists = "PERSONNEL_LOVEXISTS";
            this.lbtnPersonnel.ActionType = "PERSONNEL_LOV";
            this.lbtnPersonnel.ConditionalFields = "";
            this.lbtnPersonnel.CustomEnabled = true;
            this.lbtnPersonnel.DataFieldMapping = "";
            this.lbtnPersonnel.DependentLovControls = "";
            this.lbtnPersonnel.HiddenColumns = "W_CAT|W_JOINING_DATE|W_STATUS|W_MARRIAGE_DATE|W_BNAME|W_TRANSFER_DATE";
            this.lbtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnel.Image")));
            this.lbtnPersonnel.LoadDependentEntities = true;
            this.lbtnPersonnel.Location = new System.Drawing.Point(185, 83);
            this.lbtnPersonnel.LookUpTitle = null;
            this.lbtnPersonnel.Name = "lbtnPersonnel";
            this.lbtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnel.SkipValidationOnLeave = false;
            this.lbtnPersonnel.SPName = "CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER";
            this.lbtnPersonnel.TabIndex = 123;
            this.lbtnPersonnel.TabStop = false;
            this.lbtnPersonnel.UseVisualStyleBackColor = true;
            // 
            // txt_OMI_YEAR
            // 
            this.txt_OMI_YEAR.AllowSpace = true;
            this.txt_OMI_YEAR.AssociatedLookUpName = "";
            this.txt_OMI_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_YEAR.ContinuationTextBox = null;
            this.txt_OMI_YEAR.CustomEnabled = true;
            this.txt_OMI_YEAR.DataFieldMapping = "OMI_YEAR";
            this.txt_OMI_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_YEAR.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_YEAR.IsDate = false;
            this.txt_OMI_YEAR.Location = new System.Drawing.Point(676, 81);
            this.txt_OMI_YEAR.Name = "txt_OMI_YEAR";
            this.txt_OMI_YEAR.NumberFormat = "###,###,##0.00";
            this.txt_OMI_YEAR.Postfix = "";
            this.txt_OMI_YEAR.Prefix = "";
            this.txt_OMI_YEAR.Size = new System.Drawing.Size(33, 20);
            this.txt_OMI_YEAR.SkipValidation = false;
            this.txt_OMI_YEAR.TabIndex = 122;
            this.txt_OMI_YEAR.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_MONTH
            // 
            this.txt_OMI_MONTH.AllowSpace = true;
            this.txt_OMI_MONTH.AssociatedLookUpName = "";
            this.txt_OMI_MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_MONTH.ContinuationTextBox = null;
            this.txt_OMI_MONTH.CustomEnabled = true;
            this.txt_OMI_MONTH.DataFieldMapping = "OMI_MONTH";
            this.txt_OMI_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_MONTH.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_MONTH.IsDate = false;
            this.txt_OMI_MONTH.Location = new System.Drawing.Point(676, 61);
            this.txt_OMI_MONTH.Name = "txt_OMI_MONTH";
            this.txt_OMI_MONTH.NumberFormat = "###,###,##0.00";
            this.txt_OMI_MONTH.Postfix = "";
            this.txt_OMI_MONTH.Prefix = "";
            this.txt_OMI_MONTH.Size = new System.Drawing.Size(33, 20);
            this.txt_OMI_MONTH.SkipValidation = false;
            this.txt_OMI_MONTH.TabIndex = 121;
            this.txt_OMI_MONTH.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_P_NO
            // 
            this.txt_OMI_P_NO.AllowSpace = true;
            this.txt_OMI_P_NO.AssociatedLookUpName = "";
            this.txt_OMI_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_P_NO.ContinuationTextBox = null;
            this.txt_OMI_P_NO.CustomEnabled = true;
            this.txt_OMI_P_NO.DataFieldMapping = "OMI_P_NO";
            this.txt_OMI_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_P_NO.IsDate = false;
            this.txt_OMI_P_NO.Location = new System.Drawing.Point(635, 201);
            this.txt_OMI_P_NO.Name = "txt_OMI_P_NO";
            this.txt_OMI_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_OMI_P_NO.Postfix = "";
            this.txt_OMI_P_NO.Prefix = "";
            this.txt_OMI_P_NO.Size = new System.Drawing.Size(33, 20);
            this.txt_OMI_P_NO.SkipValidation = false;
            this.txt_OMI_P_NO.TabIndex = 120;
            this.txt_OMI_P_NO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OMI_LIMIT
            // 
            this.txt_W_OMI_LIMIT.AllowSpace = true;
            this.txt_W_OMI_LIMIT.AssociatedLookUpName = "";
            this.txt_W_OMI_LIMIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OMI_LIMIT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OMI_LIMIT.ContinuationTextBox = null;
            this.txt_W_OMI_LIMIT.CustomEnabled = true;
            this.txt_W_OMI_LIMIT.DataFieldMapping = "W_OMI_LIMIT";
            this.txt_W_OMI_LIMIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OMI_LIMIT.GetRecordsOnUpDownKeys = false;
            this.txt_W_OMI_LIMIT.IsDate = false;
            this.txt_W_OMI_LIMIT.Location = new System.Drawing.Point(185, 185);
            this.txt_W_OMI_LIMIT.Name = "txt_W_OMI_LIMIT";
            this.txt_W_OMI_LIMIT.NumberFormat = "###,###,##0.00";
            this.txt_W_OMI_LIMIT.Postfix = "";
            this.txt_W_OMI_LIMIT.Prefix = "";
            this.txt_W_OMI_LIMIT.Size = new System.Drawing.Size(100, 20);
            this.txt_W_OMI_LIMIT.SkipValidation = false;
            this.txt_W_OMI_LIMIT.TabIndex = 66;
            this.txt_W_OMI_LIMIT.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_OMI_LIMIT.Enter += new System.EventHandler(this.txt_W_OMI_LIMIT_Enter);
            // 
            // txt_W_STATUS
            // 
            this.txt_W_STATUS.AllowSpace = true;
            this.txt_W_STATUS.AssociatedLookUpName = "";
            this.txt_W_STATUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_STATUS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_STATUS.ContinuationTextBox = null;
            this.txt_W_STATUS.CustomEnabled = true;
            this.txt_W_STATUS.DataFieldMapping = "W_STATUS";
            this.txt_W_STATUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_STATUS.GetRecordsOnUpDownKeys = false;
            this.txt_W_STATUS.IsDate = false;
            this.txt_W_STATUS.Location = new System.Drawing.Point(635, 101);
            this.txt_W_STATUS.Name = "txt_W_STATUS";
            this.txt_W_STATUS.NumberFormat = "###,###,##0.00";
            this.txt_W_STATUS.Postfix = "";
            this.txt_W_STATUS.Prefix = "";
            this.txt_W_STATUS.Size = new System.Drawing.Size(33, 20);
            this.txt_W_STATUS.SkipValidation = false;
            this.txt_W_STATUS.TabIndex = 115;
            this.txt_W_STATUS.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_SNO
            // 
            this.txt_W_SNO.AllowSpace = true;
            this.txt_W_SNO.AssociatedLookUpName = "";
            this.txt_W_SNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_SNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_SNO.ContinuationTextBox = null;
            this.txt_W_SNO.CustomEnabled = true;
            this.txt_W_SNO.DataFieldMapping = "W_SNO";
            this.txt_W_SNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_SNO.GetRecordsOnUpDownKeys = false;
            this.txt_W_SNO.IsDate = false;
            this.txt_W_SNO.Location = new System.Drawing.Point(635, 81);
            this.txt_W_SNO.Name = "txt_W_SNO";
            this.txt_W_SNO.NumberFormat = "###,###,##0.00";
            this.txt_W_SNO.Postfix = "";
            this.txt_W_SNO.Prefix = "";
            this.txt_W_SNO.Size = new System.Drawing.Size(33, 20);
            this.txt_W_SNO.SkipValidation = false;
            this.txt_W_SNO.TabIndex = 114;
            this.txt_W_SNO.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_CAT
            // 
            this.txt_W_CAT.AllowSpace = true;
            this.txt_W_CAT.AssociatedLookUpName = "";
            this.txt_W_CAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_CAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_CAT.ContinuationTextBox = null;
            this.txt_W_CAT.CustomEnabled = true;
            this.txt_W_CAT.DataFieldMapping = "W_CAT";
            this.txt_W_CAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_CAT.GetRecordsOnUpDownKeys = false;
            this.txt_W_CAT.IsDate = false;
            this.txt_W_CAT.Location = new System.Drawing.Point(635, 61);
            this.txt_W_CAT.Name = "txt_W_CAT";
            this.txt_W_CAT.NumberFormat = "###,###,##0.00";
            this.txt_W_CAT.Postfix = "";
            this.txt_W_CAT.Prefix = "";
            this.txt_W_CAT.Size = new System.Drawing.Size(33, 20);
            this.txt_W_CAT.SkipValidation = false;
            this.txt_W_CAT.TabIndex = 113;
            this.txt_W_CAT.TextType = CrplControlLibrary.TextType.String;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(226, 163);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 13);
            this.label38.TabIndex = 111;
            this.label38.Text = "/";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(383, 236);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 13);
            this.label18.TabIndex = 109;
            this.label18.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(383, 211);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 108;
            this.label19.Text = ":";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(383, 186);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 13);
            this.label20.TabIndex = 107;
            this.label20.Text = ":";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(327, 236);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 106;
            this.label34.Text = "Childern";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(327, 211);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(49, 13);
            this.label35.TabIndex = 105;
            this.label35.Text = "Spouse";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(327, 186);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 13);
            this.label36.TabIndex = 104;
            this.label36.Text = "Self";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(540, 234);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(11, 13);
            this.label33.TabIndex = 103;
            this.label33.Text = ":";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(540, 209);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(11, 13);
            this.label32.TabIndex = 102;
            this.label32.Text = ":";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(540, 184);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(11, 13);
            this.label31.TabIndex = 101;
            this.label31.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(168, 238);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(11, 13);
            this.label30.TabIndex = 100;
            this.label30.Text = ":";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(168, 213);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(11, 13);
            this.label29.TabIndex = 99;
            this.label29.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(168, 188);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(11, 13);
            this.label28.TabIndex = 98;
            this.label28.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(168, 163);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(11, 13);
            this.label27.TabIndex = 97;
            this.label27.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(124, 138);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(11, 13);
            this.label26.TabIndex = 96;
            this.label26.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(124, 113);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(11, 13);
            this.label25.TabIndex = 95;
            this.label25.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(124, 87);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 94;
            this.label24.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(498, 163);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 13);
            this.label23.TabIndex = 90;
            this.label23.Text = "Balance";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(327, 163);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 89;
            this.label22.Text = "OMI Limit";
            // 
            // txt_OMI_TOTAL_CLAIMS
            // 
            this.txt_OMI_TOTAL_CLAIMS.AllowSpace = true;
            this.txt_OMI_TOTAL_CLAIMS.AssociatedLookUpName = "";
            this.txt_OMI_TOTAL_CLAIMS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_TOTAL_CLAIMS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_TOTAL_CLAIMS.ContinuationTextBox = null;
            this.txt_OMI_TOTAL_CLAIMS.CustomEnabled = true;
            this.txt_OMI_TOTAL_CLAIMS.DataFieldMapping = "OMI_TOTAL_CLAIMS";
            this.txt_OMI_TOTAL_CLAIMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_TOTAL_CLAIMS.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_TOTAL_CLAIMS.IsDate = false;
            this.txt_OMI_TOTAL_CLAIMS.Location = new System.Drawing.Point(336, 83);
            this.txt_OMI_TOTAL_CLAIMS.Name = "txt_OMI_TOTAL_CLAIMS";
            this.txt_OMI_TOTAL_CLAIMS.NumberFormat = "###,###,##0.00";
            this.txt_OMI_TOTAL_CLAIMS.Postfix = "";
            this.txt_OMI_TOTAL_CLAIMS.Prefix = "";
            this.txt_OMI_TOTAL_CLAIMS.Size = new System.Drawing.Size(56, 20);
            this.txt_OMI_TOTAL_CLAIMS.SkipValidation = false;
            this.txt_OMI_TOTAL_CLAIMS.TabIndex = 88;
            this.txt_OMI_TOTAL_CLAIMS.TextType = CrplControlLibrary.TextType.String;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(252, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 13);
            this.label21.TabIndex = 87;
            this.label21.Text = "Total Claims :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(484, 234);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 83;
            this.label17.Text = "Childern";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(484, 209);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 82;
            this.label16.Text = "Spouse";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(484, 184);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 81;
            this.label15.Text = "Self";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 238);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 13);
            this.label12.TabIndex = 80;
            this.label12.Text = "Outstanding Balance";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 213);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(149, 13);
            this.label11.TabIndex = 79;
            this.label11.Text = "Total Amt Claimed/Recv.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 188);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 78;
            this.label10.Text = "Total OMI Limit";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 13);
            this.label9.TabIndex = 77;
            this.label9.Text = "Claim For The Month Of";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 76;
            this.label8.Text = "Branch";
            // 
            // txt_OMI_CHILD
            // 
            this.txt_OMI_CHILD.AllowSpace = true;
            this.txt_OMI_CHILD.AssociatedLookUpName = "";
            this.txt_OMI_CHILD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_CHILD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_CHILD.ContinuationTextBox = null;
            this.txt_OMI_CHILD.CustomEnabled = true;
            this.txt_OMI_CHILD.DataFieldMapping = "OMI_CHILD";
            this.txt_OMI_CHILD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_CHILD.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_CHILD.IsDate = false;
            this.txt_OMI_CHILD.Location = new System.Drawing.Point(559, 234);
            this.txt_OMI_CHILD.Name = "txt_OMI_CHILD";
            this.txt_OMI_CHILD.NumberFormat = "###,###,##0.00";
            this.txt_OMI_CHILD.Postfix = "";
            this.txt_OMI_CHILD.Prefix = "";
            this.txt_OMI_CHILD.Size = new System.Drawing.Size(56, 20);
            this.txt_OMI_CHILD.SkipValidation = false;
            this.txt_OMI_CHILD.TabIndex = 75;
            this.txt_OMI_CHILD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_SPOUSE
            // 
            this.txt_OMI_SPOUSE.AllowSpace = true;
            this.txt_OMI_SPOUSE.AssociatedLookUpName = "";
            this.txt_OMI_SPOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_SPOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_SPOUSE.ContinuationTextBox = null;
            this.txt_OMI_SPOUSE.CustomEnabled = true;
            this.txt_OMI_SPOUSE.DataFieldMapping = "OMI_SPOUSE";
            this.txt_OMI_SPOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_SPOUSE.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_SPOUSE.IsDate = false;
            this.txt_OMI_SPOUSE.Location = new System.Drawing.Point(559, 209);
            this.txt_OMI_SPOUSE.Name = "txt_OMI_SPOUSE";
            this.txt_OMI_SPOUSE.NumberFormat = "###,###,##0.00";
            this.txt_OMI_SPOUSE.Postfix = "";
            this.txt_OMI_SPOUSE.Prefix = "";
            this.txt_OMI_SPOUSE.Size = new System.Drawing.Size(56, 20);
            this.txt_OMI_SPOUSE.SkipValidation = false;
            this.txt_OMI_SPOUSE.TabIndex = 74;
            this.txt_OMI_SPOUSE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_SELF
            // 
            this.txt_OMI_SELF.AllowSpace = true;
            this.txt_OMI_SELF.AssociatedLookUpName = "";
            this.txt_OMI_SELF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_SELF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_SELF.ContinuationTextBox = null;
            this.txt_OMI_SELF.CustomEnabled = true;
            this.txt_OMI_SELF.DataFieldMapping = "OMI_SELF";
            this.txt_OMI_SELF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_SELF.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_SELF.IsDate = false;
            this.txt_OMI_SELF.Location = new System.Drawing.Point(559, 184);
            this.txt_OMI_SELF.Name = "txt_OMI_SELF";
            this.txt_OMI_SELF.NumberFormat = "###,###,##0.00";
            this.txt_OMI_SELF.Postfix = "";
            this.txt_OMI_SELF.Prefix = "";
            this.txt_OMI_SELF.Size = new System.Drawing.Size(56, 20);
            this.txt_OMI_SELF.SkipValidation = false;
            this.txt_OMI_SELF.TabIndex = 73;
            this.txt_OMI_SELF.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_CHILD
            // 
            this.txt_W_CHILD.AllowSpace = true;
            this.txt_W_CHILD.AssociatedLookUpName = "";
            this.txt_W_CHILD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_CHILD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_CHILD.ContinuationTextBox = null;
            this.txt_W_CHILD.CustomEnabled = true;
            this.txt_W_CHILD.DataFieldMapping = "W_CHILD";
            this.txt_W_CHILD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_CHILD.GetRecordsOnUpDownKeys = false;
            this.txt_W_CHILD.IsDate = false;
            this.txt_W_CHILD.Location = new System.Drawing.Point(401, 234);
            this.txt_W_CHILD.Name = "txt_W_CHILD";
            this.txt_W_CHILD.NumberFormat = "###,###,##0.00";
            this.txt_W_CHILD.Postfix = "";
            this.txt_W_CHILD.Prefix = "";
            this.txt_W_CHILD.Size = new System.Drawing.Size(56, 20);
            this.txt_W_CHILD.SkipValidation = false;
            this.txt_W_CHILD.TabIndex = 72;
            this.txt_W_CHILD.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_SPOUSE
            // 
            this.txt_W_SPOUSE.AllowSpace = true;
            this.txt_W_SPOUSE.AssociatedLookUpName = "";
            this.txt_W_SPOUSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_SPOUSE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_SPOUSE.ContinuationTextBox = null;
            this.txt_W_SPOUSE.CustomEnabled = true;
            this.txt_W_SPOUSE.DataFieldMapping = "W_SPOUSE";
            this.txt_W_SPOUSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_SPOUSE.GetRecordsOnUpDownKeys = false;
            this.txt_W_SPOUSE.IsDate = false;
            this.txt_W_SPOUSE.Location = new System.Drawing.Point(401, 209);
            this.txt_W_SPOUSE.Name = "txt_W_SPOUSE";
            this.txt_W_SPOUSE.NumberFormat = "###,###,##0.00";
            this.txt_W_SPOUSE.Postfix = "";
            this.txt_W_SPOUSE.Prefix = "";
            this.txt_W_SPOUSE.Size = new System.Drawing.Size(56, 20);
            this.txt_W_SPOUSE.SkipValidation = false;
            this.txt_W_SPOUSE.TabIndex = 71;
            this.txt_W_SPOUSE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_SELF
            // 
            this.txt_W_SELF.AllowSpace = true;
            this.txt_W_SELF.AssociatedLookUpName = "";
            this.txt_W_SELF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_SELF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_SELF.ContinuationTextBox = null;
            this.txt_W_SELF.CustomEnabled = true;
            this.txt_W_SELF.DataFieldMapping = "W_SELF";
            this.txt_W_SELF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_SELF.GetRecordsOnUpDownKeys = false;
            this.txt_W_SELF.IsDate = false;
            this.txt_W_SELF.Location = new System.Drawing.Point(401, 184);
            this.txt_W_SELF.Name = "txt_W_SELF";
            this.txt_W_SELF.NumberFormat = "###,###,##0.00";
            this.txt_W_SELF.Postfix = "";
            this.txt_W_SELF.Prefix = "";
            this.txt_W_SELF.Size = new System.Drawing.Size(56, 20);
            this.txt_W_SELF.SkipValidation = false;
            this.txt_W_SELF.TabIndex = 70;
            this.txt_W_SELF.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OS_BALANCE
            // 
            this.txt_W_OS_BALANCE.AllowSpace = true;
            this.txt_W_OS_BALANCE.AssociatedLookUpName = "";
            this.txt_W_OS_BALANCE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OS_BALANCE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OS_BALANCE.ContinuationTextBox = null;
            this.txt_W_OS_BALANCE.CustomEnabled = true;
            this.txt_W_OS_BALANCE.DataFieldMapping = "W_OS_BALANCE";
            this.txt_W_OS_BALANCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OS_BALANCE.GetRecordsOnUpDownKeys = false;
            this.txt_W_OS_BALANCE.IsDate = false;
            this.txt_W_OS_BALANCE.Location = new System.Drawing.Point(185, 234);
            this.txt_W_OS_BALANCE.Name = "txt_W_OS_BALANCE";
            this.txt_W_OS_BALANCE.NumberFormat = "###,###,##0.00";
            this.txt_W_OS_BALANCE.Postfix = "";
            this.txt_W_OS_BALANCE.Prefix = "";
            this.txt_W_OS_BALANCE.Size = new System.Drawing.Size(100, 20);
            this.txt_W_OS_BALANCE.SkipValidation = false;
            this.txt_W_OS_BALANCE.TabIndex = 68;
            this.txt_W_OS_BALANCE.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_OMI_TOTAL_CALIM1
            // 
            this.txt_OMI_TOTAL_CALIM1.AllowSpace = true;
            this.txt_OMI_TOTAL_CALIM1.AssociatedLookUpName = "";
            this.txt_OMI_TOTAL_CALIM1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_OMI_TOTAL_CALIM1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_OMI_TOTAL_CALIM1.ContinuationTextBox = null;
            this.txt_OMI_TOTAL_CALIM1.CustomEnabled = true;
            this.txt_OMI_TOTAL_CALIM1.DataFieldMapping = "OMI_TOTAL_CALIM1";
            this.txt_OMI_TOTAL_CALIM1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_OMI_TOTAL_CALIM1.GetRecordsOnUpDownKeys = false;
            this.txt_OMI_TOTAL_CALIM1.IsDate = false;
            this.txt_OMI_TOTAL_CALIM1.Location = new System.Drawing.Point(185, 209);
            this.txt_OMI_TOTAL_CALIM1.Name = "txt_OMI_TOTAL_CALIM1";
            this.txt_OMI_TOTAL_CALIM1.NumberFormat = "###,###,##0.00";
            this.txt_OMI_TOTAL_CALIM1.Postfix = "";
            this.txt_OMI_TOTAL_CALIM1.Prefix = "";
            this.txt_OMI_TOTAL_CALIM1.Size = new System.Drawing.Size(100, 20);
            this.txt_OMI_TOTAL_CALIM1.SkipValidation = false;
            this.txt_OMI_TOTAL_CALIM1.TabIndex = 67;
            this.txt_OMI_TOTAL_CALIM1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_OMI_CLAIM
            // 
            this.txt_W_OMI_CLAIM.AllowSpace = true;
            this.txt_W_OMI_CLAIM.AssociatedLookUpName = "";
            this.txt_W_OMI_CLAIM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_OMI_CLAIM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_OMI_CLAIM.ContinuationTextBox = null;
            this.txt_W_OMI_CLAIM.CustomEnabled = true;
            this.txt_W_OMI_CLAIM.DataFieldMapping = "W_OMI_CLAIM";
            this.txt_W_OMI_CLAIM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_OMI_CLAIM.GetRecordsOnUpDownKeys = false;
            this.txt_W_OMI_CLAIM.IsDate = false;
            this.txt_W_OMI_CLAIM.Location = new System.Drawing.Point(635, 227);
            this.txt_W_OMI_CLAIM.Name = "txt_W_OMI_CLAIM";
            this.txt_W_OMI_CLAIM.NumberFormat = "###,###,##0.00";
            this.txt_W_OMI_CLAIM.Postfix = "";
            this.txt_W_OMI_CLAIM.Prefix = "";
            this.txt_W_OMI_CLAIM.Size = new System.Drawing.Size(100, 20);
            this.txt_W_OMI_CLAIM.SkipValidation = false;
            this.txt_W_OMI_CLAIM.TabIndex = 119;
            this.txt_W_OMI_CLAIM.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_OMI_CLAIM.Enter += new System.EventHandler(this.txt_W_OMI_TOTAL_CLAIM_Enter);
            // 
            // txt_W_YEAR
            // 
            this.txt_W_YEAR.AllowSpace = true;
            this.txt_W_YEAR.AssociatedLookUpName = "lbtnMonthYear";
            this.txt_W_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_YEAR.ContinuationTextBox = null;
            this.txt_W_YEAR.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_W_YEAR.CustomEnabled = true;
            this.txt_W_YEAR.DataFieldMapping = "W_YEAR";
            this.txt_W_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_YEAR.GetRecordsOnUpDownKeys = false;
            this.txt_W_YEAR.IsDate = false;
            this.txt_W_YEAR.IsRequired = true;
            this.txt_W_YEAR.Location = new System.Drawing.Point(239, 159);
            this.txt_W_YEAR.MaxLength = 4;
            this.txt_W_YEAR.Name = "txt_W_YEAR";
            this.txt_W_YEAR.NumberFormat = "###,###,##0.00";
            this.txt_W_YEAR.Postfix = "";
            this.txt_W_YEAR.Prefix = "";
            this.txt_W_YEAR.Size = new System.Drawing.Size(46, 20);
            this.txt_W_YEAR.SkipValidation = false;
            this.txt_W_YEAR.TabIndex = 65;
            this.txt_W_YEAR.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_W_YEAR.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_YEAR_Validating);
            // 
            // txt_W_MONTH
            // 
            this.txt_W_MONTH.AllowSpace = true;
            this.txt_W_MONTH.AssociatedLookUpName = "lbtnMonthYear";
            this.txt_W_MONTH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_MONTH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_MONTH.ContinuationTextBox = null;
            this.txt_W_MONTH.CustomEnabled = true;
            this.txt_W_MONTH.DataFieldMapping = "W_MONTH";
            this.txt_W_MONTH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_MONTH.GetRecordsOnUpDownKeys = false;
            this.txt_W_MONTH.IsDate = false;
            this.txt_W_MONTH.IsRequired = true;
            this.txt_W_MONTH.Location = new System.Drawing.Point(185, 159);
            this.txt_W_MONTH.MaxLength = 2;
            this.txt_W_MONTH.Name = "txt_W_MONTH";
            this.txt_W_MONTH.NumberFormat = "###,###,##0.00";
            this.txt_W_MONTH.Postfix = "";
            this.txt_W_MONTH.Prefix = "";
            this.txt_W_MONTH.Size = new System.Drawing.Size(39, 20);
            this.txt_W_MONTH.SkipValidation = false;
            this.txt_W_MONTH.TabIndex = 64;
            this.txt_W_MONTH.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_W_MONTH.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_MONTH_Validating);
            // 
            // txt_W_BNAME
            // 
            this.txt_W_BNAME.AllowSpace = true;
            this.txt_W_BNAME.AssociatedLookUpName = "";
            this.txt_W_BNAME.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_BNAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_BNAME.ContinuationTextBox = null;
            this.txt_W_BNAME.CustomEnabled = true;
            this.txt_W_BNAME.DataFieldMapping = "W_BNAME";
            this.txt_W_BNAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_BNAME.GetRecordsOnUpDownKeys = false;
            this.txt_W_BNAME.IsDate = false;
            this.txt_W_BNAME.Location = new System.Drawing.Point(141, 134);
            this.txt_W_BNAME.Name = "txt_W_BNAME";
            this.txt_W_BNAME.NumberFormat = "###,###,##0.00";
            this.txt_W_BNAME.Postfix = "";
            this.txt_W_BNAME.Prefix = "";
            this.txt_W_BNAME.ReadOnly = true;
            this.txt_W_BNAME.Size = new System.Drawing.Size(191, 20);
            this.txt_W_BNAME.SkipValidation = false;
            this.txt_W_BNAME.TabIndex = 63;
            this.txt_W_BNAME.TabStop = false;
            this.txt_W_BNAME.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Name
            // 
            this.txt_W_Name.AllowSpace = true;
            this.txt_W_Name.AssociatedLookUpName = "";
            this.txt_W_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Name.ContinuationTextBox = null;
            this.txt_W_Name.CustomEnabled = true;
            this.txt_W_Name.DataFieldMapping = "Name";
            this.txt_W_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Name.GetRecordsOnUpDownKeys = false;
            this.txt_W_Name.IsDate = false;
            this.txt_W_Name.Location = new System.Drawing.Point(141, 109);
            this.txt_W_Name.Name = "txt_W_Name";
            this.txt_W_Name.NumberFormat = "###,###,##0.00";
            this.txt_W_Name.Postfix = "";
            this.txt_W_Name.Prefix = "";
            this.txt_W_Name.ReadOnly = true;
            this.txt_W_Name.Size = new System.Drawing.Size(251, 20);
            this.txt_W_Name.SkipValidation = false;
            this.txt_W_Name.TabIndex = 62;
            this.txt_W_Name.TabStop = false;
            this.txt_W_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_W_Pno
            // 
            this.txt_W_Pno.AllowSpace = true;
            this.txt_W_Pno.AssociatedLookUpName = "lbtnPersonnel";
            this.txt_W_Pno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Pno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Pno.ContinuationTextBox = null;
            this.txt_W_Pno.CustomEnabled = true;
            this.txt_W_Pno.DataFieldMapping = "OMI_P_NO";
            this.txt_W_Pno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Pno.GetRecordsOnUpDownKeys = false;
            this.txt_W_Pno.IsDate = false;
            this.txt_W_Pno.Location = new System.Drawing.Point(141, 83);
            this.txt_W_Pno.MaxLength = 4;
            this.txt_W_Pno.Name = "txt_W_Pno";
            this.txt_W_Pno.NumberFormat = "###,###,##0.00";
            this.txt_W_Pno.Postfix = "";
            this.txt_W_Pno.Prefix = "";
            this.txt_W_Pno.Size = new System.Drawing.Size(38, 20);
            this.txt_W_Pno.SkipValidation = false;
            this.txt_W_Pno.TabIndex = 61;
            this.txt_W_Pno.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Pno.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Pno_Validating);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 60;
            this.label7.Text = "Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 59;
            this.label6.Text = "Personnel No.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(-25, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(889, 13);
            this.label13.TabIndex = 58;
            this.label13.Text = "_________________________________________________________________________________" +
                "_____________________________________________";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(244, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 13);
            this.label14.TabIndex = 57;
            this.label14.Text = "OUT PATIENT MEDICAL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(253, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 13);
            this.label5.TabIndex = 56;
            this.label5.Text = "CLAIM FORM ENTRY";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(490, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 54;
            this.label2.Text = "Location :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "User :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(533, 25);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(82, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 52;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(85, 25);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(73, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 51;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(85, 3);
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(73, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 50;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // pnlTblDept
            // 
            this.pnlTblDept.ConcurrentPanels = null;
            this.pnlTblDept.Controls.Add(this.dgvDept);
            this.pnlTblDept.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlTblDept.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlTblDept.DependentPanels = null;
            this.pnlTblDept.DisableDependentLoad = false;
            this.pnlTblDept.EnableDelete = true;
            this.pnlTblDept.EnableInsert = true;
            this.pnlTblDept.EnableQuery = false;
            this.pnlTblDept.EnableUpdate = true;
            this.pnlTblDept.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DeptCommand";
            this.pnlTblDept.Location = new System.Drawing.Point(416, 140);
            this.pnlTblDept.MasterPanel = null;
            this.pnlTblDept.Name = "pnlTblDept";
            this.pnlTblDept.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlTblDept.Size = new System.Drawing.Size(210, 97);
            this.pnlTblDept.SPName = "CHRIS_SP_Fin_Liquidate_DEPT_CONT_MANAGER";
            this.pnlTblDept.TabIndex = 110;
            // 
            // dgvDept
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPrDNo,
            this.colSeg,
            this.colDept,
            this.colContr});
            this.dgvDept.ColumnToHide = null;
            this.dgvDept.ColumnWidth = null;
            this.dgvDept.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDept.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDept.DisplayColumnWrapper = null;
            this.dgvDept.GridDefaultRow = 0;
            this.dgvDept.Location = new System.Drawing.Point(3, 0);
            this.dgvDept.Name = "dgvDept";
            this.dgvDept.ReadOnly = true;
            this.dgvDept.ReadOnlyColumns = null;
            this.dgvDept.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDept.Size = new System.Drawing.Size(203, 94);
            this.dgvDept.SkippingColumns = null;
            this.dgvDept.TabIndex = 0;
            // 
            // colPrDNo
            // 
            this.colPrDNo.DataPropertyName = "PR_D_NO";
            this.colPrDNo.HeaderText = "PrDNo";
            this.colPrDNo.Name = "colPrDNo";
            this.colPrDNo.ReadOnly = true;
            this.colPrDNo.Visible = false;
            // 
            // colSeg
            // 
            this.colSeg.DataPropertyName = "PR_SEGMENT";
            this.colSeg.HeaderText = "Seg";
            this.colSeg.Name = "colSeg";
            this.colSeg.ReadOnly = true;
            this.colSeg.Width = 50;
            // 
            // colDept
            // 
            this.colDept.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDept.DataPropertyName = "PR_DEPT";
            this.colDept.HeaderText = "Dept.";
            this.colDept.Name = "colDept";
            this.colDept.ReadOnly = true;
            // 
            // colContr
            // 
            this.colContr.DataPropertyName = "PR_CONTRIB";
            this.colContr.HeaderText = "Contb.";
            this.colContr.Name = "colContr";
            this.colContr.ReadOnly = true;
            this.colContr.Width = 50;
            // 
            // pnltblDetl
            // 
            this.pnltblDetl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnltblDetl.ConcurrentPanels = null;
            this.pnltblDetl.Controls.Add(this.txt_PR_P_NO);
            this.pnltblDetl.Controls.Add(this.dgvOmiDetail);
            this.pnltblDetl.Controls.Add(this.txt_W_Total);
            this.pnltblDetl.Controls.Add(this.label37);
            this.pnltblDetl.DataManager = "iCORE.Common.CommonDataManager";
            this.pnltblDetl.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnltblDetl.DependentPanels = null;
            this.pnltblDetl.DisableDependentLoad = false;
            this.pnltblDetl.EnableDelete = true;
            this.pnltblDetl.EnableInsert = true;
            this.pnltblDetl.EnableQuery = false;
            this.pnltblDetl.EnableUpdate = true;
            this.pnltblDetl.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMDETAILCommand";
            this.pnltblDetl.Location = new System.Drawing.Point(12, 363);
            this.pnltblDetl.MasterPanel = null;
            this.pnltblDetl.Name = "pnltblDetl";
            this.pnltblDetl.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnltblDetl.Size = new System.Drawing.Size(628, 217);
            this.pnltblDetl.SPName = "CHRIS_MedicalInsurance_CFEntry_OMI_CLAIM_DETAIL_MANAGER";
            this.pnltblDetl.TabIndex = 10;
            // 
            // txt_PR_P_NO
            // 
            this.txt_PR_P_NO.AllowSpace = true;
            this.txt_PR_P_NO.AssociatedLookUpName = "";
            this.txt_PR_P_NO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_PR_P_NO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_PR_P_NO.ContinuationTextBox = null;
            this.txt_PR_P_NO.CustomEnabled = true;
            this.txt_PR_P_NO.DataFieldMapping = "OMI_P_NO";
            this.txt_PR_P_NO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PR_P_NO.GetRecordsOnUpDownKeys = false;
            this.txt_PR_P_NO.IsDate = false;
            this.txt_PR_P_NO.Location = new System.Drawing.Point(323, 185);
            this.txt_PR_P_NO.Name = "txt_PR_P_NO";
            this.txt_PR_P_NO.NumberFormat = "###,###,##0.00";
            this.txt_PR_P_NO.Postfix = "";
            this.txt_PR_P_NO.Prefix = "";
            this.txt_PR_P_NO.Size = new System.Drawing.Size(33, 20);
            this.txt_PR_P_NO.SkipValidation = false;
            this.txt_PR_P_NO.TabIndex = 121;
            this.txt_PR_P_NO.TextType = CrplControlLibrary.TextType.String;
            this.txt_PR_P_NO.Visible = false;
            // 
            // dgvOmiDetail
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOmiDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvOmiDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOmiDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Serial_Num,
            this.colSNo,
            this.colBillDate,
            this.OMI_PATIENTS_NAME,
            this.colRelation,
            this.colAmount,
            this.colOmiPNo,
            this.colOmiMonth,
            this.colOmiYear});
            this.dgvOmiDetail.ColumnToHide = null;
            this.dgvOmiDetail.ColumnWidth = null;
            this.dgvOmiDetail.CustomEnabled = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvOmiDetail.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvOmiDetail.DisplayColumnWrapper = null;
            this.dgvOmiDetail.GridDefaultRow = 0;
            this.dgvOmiDetail.Location = new System.Drawing.Point(3, 3);
            this.dgvOmiDetail.Name = "dgvOmiDetail";
            this.dgvOmiDetail.ReadOnlyColumns = null;
            this.dgvOmiDetail.RequiredColumns = null;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOmiDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvOmiDetail.Size = new System.Drawing.Size(620, 171);
            this.dgvOmiDetail.SkippingColumns = "COLSNO,COLPATIENTNAME,COLRELATION";
            this.dgvOmiDetail.TabIndex = 0;
            this.dgvOmiDetail.Validated += new System.EventHandler(this.dgvOmiDetail_Validated);
            this.dgvOmiDetail.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvOmiDetail_CellValidating);
            this.dgvOmiDetail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvOmiDetail_EditingControlShowing);
            // 
            // Serial_Num
            // 
            this.Serial_Num.DataPropertyName = "OMI_CLAIM_NO";
            this.Serial_Num.HeaderText = "S.no.";
            this.Serial_Num.Name = "Serial_Num";
            // 
            // colSNo
            // 
            this.colSNo.DataPropertyName = "s_no";
            this.colSNo.HeaderText = "Serial_NUM";
            this.colSNo.Name = "colSNo";
            this.colSNo.ReadOnly = true;
            this.colSNo.Visible = false;
            this.colSNo.Width = 50;
            // 
            // colBillDate
            // 
            this.colBillDate.ActionLOV = "MONTHYEAR_LOV";
            this.colBillDate.ActionLOVExists = null;
            this.colBillDate.AttachParentEntity = false;
            this.colBillDate.DataPropertyName = "OMI_BILL_DATE";
            dataGridViewCellStyle5.NullValue = null;
            this.colBillDate.DefaultCellStyle = dataGridViewCellStyle5;
            this.colBillDate.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.OMICLAIMDETAILCommand";
            this.colBillDate.HeaderText = "Bill Date";
            this.colBillDate.LookUpTitle = null;
            this.colBillDate.LOVFieldMapping = "OMI_BILL_DATE";
            this.colBillDate.MaxInputLength = 10;
            this.colBillDate.Name = "colBillDate";
            this.colBillDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colBillDate.SearchColumn = "OMI_BILL_DATE";
            this.colBillDate.SkipValidationOnLeave = false;
            this.colBillDate.SpName = "CHRIS_MedicalInsurance_CFEntry_OMI_CLAIM_DETAIL_MANAGER";
            // 
            // OMI_PATIENTS_NAME
            // 
            this.OMI_PATIENTS_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OMI_PATIENTS_NAME.DataPropertyName = "OMI_PATIENTS_NAME";
            this.OMI_PATIENTS_NAME.HeaderText = "Patient`s Name";
            this.OMI_PATIENTS_NAME.Name = "OMI_PATIENTS_NAME";
            // 
            // colRelation
            // 
            this.colRelation.DataPropertyName = "OMI_RELATION";
            this.colRelation.HeaderText = "Relation";
            this.colRelation.Name = "colRelation";
            // 
            // colAmount
            // 
            this.colAmount.DataPropertyName = "OMI_CLAIM_AMOUNT";
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            // 
            // colOmiPNo
            // 
            this.colOmiPNo.DataPropertyName = "OMI_P_NO";
            this.colOmiPNo.HeaderText = "Omi_P_NO";
            this.colOmiPNo.Name = "colOmiPNo";
            this.colOmiPNo.Visible = false;
            // 
            // colOmiMonth
            // 
            this.colOmiMonth.DataPropertyName = "OMI_MONTH";
            this.colOmiMonth.HeaderText = "Omi_Month";
            this.colOmiMonth.Name = "colOmiMonth";
            this.colOmiMonth.Visible = false;
            // 
            // colOmiYear
            // 
            this.colOmiYear.DataPropertyName = "OMI_YEAR";
            this.colOmiYear.HeaderText = "OmiYear";
            this.colOmiYear.Name = "colOmiYear";
            this.colOmiYear.Visible = false;
            // 
            // txt_W_Total
            // 
            this.txt_W_Total.AllowSpace = true;
            this.txt_W_Total.AssociatedLookUpName = "";
            this.txt_W_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Total.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Total.ContinuationTextBox = null;
            this.txt_W_Total.CustomEnabled = true;
            this.txt_W_Total.DataFieldMapping = "W_Total";
            this.txt_W_Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Total.GetRecordsOnUpDownKeys = false;
            this.txt_W_Total.IsDate = false;
            this.txt_W_Total.Location = new System.Drawing.Point(533, 188);
            this.txt_W_Total.Name = "txt_W_Total";
            this.txt_W_Total.NumberFormat = "###,###,##0.00";
            this.txt_W_Total.Postfix = "";
            this.txt_W_Total.Prefix = "";
            this.txt_W_Total.Size = new System.Drawing.Size(82, 20);
            this.txt_W_Total.SkipValidation = false;
            this.txt_W_Total.TabIndex = 111;
            this.txt_W_Total.TextType = CrplControlLibrary.TextType.String;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 192);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(238, 13);
            this.label37.TabIndex = 111;
            this.label37.Text = "Total     =========================>";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(497, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(41, 13);
            this.lblUserName.TabIndex = 30;
            this.lblUserName.Text = "label15";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(433, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "UserName :";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "OMI_CLAIM_NO";
            this.dataGridViewTextBoxColumn1.HeaderText = "S.no.";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "OMI_PATIENT_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Patient`s Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "OMI_RELATION";
            this.dataGridViewTextBoxColumn3.HeaderText = "Relation";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "OMI_CLAIM_AMOUNT";
            this.dataGridViewTextBoxColumn4.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "OMI_P_NO";
            this.dataGridViewTextBoxColumn5.HeaderText = "Omi_P_NO";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "OMI_MONTH";
            this.dataGridViewTextBoxColumn6.HeaderText = "Omi_Month";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "OMI_YEAR";
            this.dataGridViewTextBoxColumn7.HeaderText = "OmiYear";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "PrDNo";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Seg";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.HeaderText = "Dept.";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Contb.";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 50;
            // 
            // CHRIS_MedicalInsurance_CFEntry_OMIDE03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 643);
            this.Controls.Add(this.pnlTblDept);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnltblDetl);
            this.CurrentPanelBlock = "pnlMain";
            this.Name = "CHRIS_MedicalInsurance_CFEntry_OMIDE03";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_CFEntry_OMIDE03";
            this.Controls.SetChildIndex(this.pnltblDetl, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlTblDept, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlTblDept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDept)).EndInit();
            this.pnltblDetl.ResumeLayout(false);
            this.pnltblDetl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOmiDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlMain;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnltblDetl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txt_W_CHILD;
        private CrplControlLibrary.SLTextBox txt_W_SPOUSE;
        private CrplControlLibrary.SLTextBox txt_W_SELF;
        private CrplControlLibrary.SLTextBox txt_W_OS_BALANCE;
        private CrplControlLibrary.SLTextBox txt_OMI_TOTAL_CALIM1;
        private CrplControlLibrary.SLTextBox txt_W_OMI_CLAIM;
        private CrplControlLibrary.SLTextBox txt_W_YEAR;
        private CrplControlLibrary.SLTextBox txt_W_MONTH;
        private CrplControlLibrary.SLTextBox txt_W_BNAME;
        private CrplControlLibrary.SLTextBox txt_W_Name;
        private CrplControlLibrary.SLTextBox txt_W_Pno;
        private CrplControlLibrary.SLTextBox txt_OMI_CHILD;
        private CrplControlLibrary.SLTextBox txt_OMI_SPOUSE;
        private CrplControlLibrary.SLTextBox txt_OMI_SELF;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private CrplControlLibrary.SLTextBox txt_OMI_TOTAL_CLAIMS;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlTblDept;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDept;
        private CrplControlLibrary.SLTextBox txt_W_Total;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private CrplControlLibrary.SLTextBox txt_W_STATUS;
        private CrplControlLibrary.SLTextBox txt_W_SNO;
        private CrplControlLibrary.SLTextBox txt_W_CAT;
        private CrplControlLibrary.SLTextBox txt_W_OMI_LIMIT;
        private CrplControlLibrary.SLTextBox txt_OMI_P_NO;
        private CrplControlLibrary.SLTextBox txt_OMI_YEAR;
        private CrplControlLibrary.SLTextBox txt_OMI_MONTH;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvOmiDetail;
        private CrplControlLibrary.LookupButton lbtnPersonnel;
        private CrplControlLibrary.LookupButton lbtnMonthYear;
        private CrplControlLibrary.SLDatePicker dtp_W_TRANSFER_DATE;
        private CrplControlLibrary.SLDatePicker dtp_W_JOINING_DATE;
        private CrplControlLibrary.SLDatePicker dtp_W_MARRIAGE_DATE;
        private CrplControlLibrary.SLTextBox txt_PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private CrplControlLibrary.SLTextBox txt_OMI_CLAIM_NO;
        private CrplControlLibrary.SLTextBox txt_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serial_Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSNo;
        private iCORE.COMMON.SLCONTROLS.DataGridViewLOVColumn colBillDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn OMI_PATIENTS_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRelation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmiPNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmiMonth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOmiYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrDNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeg;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContr;
    }
}