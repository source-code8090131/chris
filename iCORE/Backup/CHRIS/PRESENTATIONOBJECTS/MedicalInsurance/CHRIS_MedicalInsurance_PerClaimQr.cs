using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PerClaimQr : ChrisMasterDetailForm
    {
        #region --Variable--
        int  g_Child = 0;
        string str_Omi_Month = string.Empty;
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_PerClaimQr()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_PerClaimQr(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(this.pnlOmiDetail);
            lstDependentPanels.Add(this.pnlTblDept);

            this.pnlOmiMaster.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(pnlOmiMaster);
            //this.pnlOmiMaster.LoadDependentPanels();

        }
        #endregion

        #region --Methods--
        /// <summary>
        /// Ovverridden OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtUser.Text            = this.UserName;
            txtDate.Text            = this.Now().ToString("dd/MM/yyyy");
            lblUser.Text            = "UserName : " + this.userID;
            txtOption.Visible       = false;
            this.txtOption.Visible  = false;
            this.txt_W_PNO.Focus();
            this.tbtDelete.Visible  = false;
            this.tbtAdd.Visible     = false;
            this.tbtSave.Visible    = false;
            this.txt_W_PNO.Focus();
            this.txt_W_PNO.Select();
            //dgvOmiDetail.OMI_PATIENT_NAME.attac
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {

            base.DoToolbarActions(ctrlsCollection, actionType);

            if (actionType == "Cancel")
            {
                ClearForm(pnlOmiDetail.Controls);
                dgvDept.GridSource      = null;
                dgvOmiDetail.GridSource = null;
                txt_W_PNO.Focus();
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        

        public void Proc5()
        {
            try
            {
                double ww_Self      = 0;
                double ww_Spouse    = 0;
                DateTime dt_Joining = Convert.ToDateTime(txt_W_JOINING_DATE.Value);
                DateTime dt_Marriage = Convert.ToDateTime(txt_W_MARRIAGE_DATE.Value);

                string YearEnd      = "31/12/" + txt_W_Year.Text;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                /* JOINED IN THE CURRENT YEAR AND MARRIED AT THE TIME OF JOINING */
                if (dt_Joining.Year == Convert.ToInt32(txt_W_Year.Text) && txt_W_STATUS.Text == "M" && Convert.ToDateTime(txt_W_MARRIAGE_DATE.Value) < Convert.ToDateTime(txt_W_JOINING_DATE.Value))
                {
                    ww_Self = ((Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_JOINING_DATE.Value))))));
                    ww_Spouse = ((Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_JOINING_DATE.Value))))));
                }
                else if (txt_W_STATUS.Text == "M" && Convert.ToDateTime(txt_W_MARRIAGE_DATE.Value) > Convert.ToDateTime(txt_W_JOINING_DATE.Value) && dt_Marriage.Year == Convert.ToInt32(txt_W_Year.Text))
                {
                    /* MARRIED AFTER JOINING AND MARRIAGE IS IN CURRENT YEAR */

                    ww_Self = Convert.ToDouble(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text);
                    ww_Spouse = ((Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_MARRIAGE_DATE.Value))))));
                }
                else if (txt_W_STATUS.Text == "M" && dt_Joining.Year < Convert.ToInt32(txt_W_Year.Text) && ((Convert.ToDateTime(txt_W_MARRIAGE_DATE.Value).Year) < Convert.ToInt32(txt_W_Year.Text) || dt_Marriage == null))
                {
                    ww_Self     = Convert.ToDouble(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text);
                    ww_Spouse   = Convert.ToDouble(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text);
                }
                else if ((txt_W_STATUS.Text == "S" || txt_W_STATUS.Text == "D" || txt_W_STATUS.Text == "W") && (dt_Joining.Year == Convert.ToInt32(txt_W_Year.Text)))
                {
                    /* SINGLE AND JOINED IN THE CURRENT YEAR */
                    ww_Self = ((Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text) / 12) * (Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_JOINING_DATE.Value))))));
                }
                else if ((txt_W_STATUS.Text == "S" || txt_W_STATUS.Text == "D" || txt_W_STATUS.Text == "W") && dt_Joining.Year < Convert.ToInt32(txt_W_Year.Text))
                {
                    /* SINGLE AND JOINED BEFORE CURRENT YEAR*/
                    ww_Self = Convert.ToDouble(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text);
                }

                txt_W_Self.Text     = ww_Self.ToString();
                txt_W_Spouse.Text   = ww_Spouse.ToString();


                if ((Convert.ToInt32(txt_Omi_Spouse.Text == string.Empty ? "0" : txt_Omi_Spouse.Text) == 0) && (Convert.ToInt32(txt_Omi_Spouse.Text == string.Empty ? "0" : txt_Omi_Spouse.Text) > 0))
                {
                    txt_Omi_Spouse.Text = txt_W_Spouse.Text;
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc5", exp);
            }
        }

        public void Proc6()
        {
            try
            {
                string YearEnd      = "31/12/" + txt_W_Year.Text;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_PNO.Text);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode                = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC6", param);
                double WW_CHILD = 0;
                int W_OMI = 0;
                W_OMI = Convert.ToInt32(g_Child);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 )
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        

                        foreach (DataRow dr in rsltCode.dstResult.Tables[0].Rows)
                        {
                            if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year < Convert.ToInt32(txt_W_Year.Text))
                            {
                                WW_CHILD = WW_CHILD + Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text);
                            }
                            else if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year == Convert.ToInt32(txt_W_Year.Text))
                            {
                                WW_CHILD = (Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text) / 12) * Math.Round(Convert.ToDouble(base.MonthsBetweenInOracle(dt_YearEnd, Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"])))) + WW_CHILD;
                            }

                            txt_W_Child.Text = Convert.ToString((WW_CHILD - W_OMI) + W_OMI);
                            txt_Omi_Child.Text = txt_W_Child.Text;
                        }
                    }
                    else
                    {
                        txt_W_Child.Text = Convert.ToString((WW_CHILD - W_OMI) + W_OMI);
                        txt_Omi_Child.Text = txt_W_Child.Text;
                    }
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc6", exp);
            }
        }

        public void Proc9()
        {
            try
            {
                DateTime PRO_DATE = new DateTime();
                int NO_MONTH = 0;
                string YearEnd      = "31/12/" + txt_W_Year.Text;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_PNO.Text);
                param.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCode;
                CmnDataManager cmnDM    = new CmnDataManager();
                rsltCode                = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC9", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 1)
                {
                    PRO_DATE = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0]["PR_EFFECTIVE"].ToString());
                    NO_MONTH =  base.MonthsBetweenInOracle(Convert.ToDateTime(YearEnd), Convert.ToDateTime(PRO_DATE));

                    if (Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text) > 0)
                    {
                        txt_W_Self.Text = Convert.ToString((Convert.ToInt32(txt_W_Self.Text) * NO_MONTH) / 12);
                    }

                    if (Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text) > 0)
                    {
                        txt_W_Spouse.Text = Convert.ToString((Convert.ToInt32(txt_W_Spouse.Text) * NO_MONTH) / 12);
                    }

                    if (Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text) > 0)
                    {
                        txt_W_Child.Text = Convert.ToString((Convert.ToInt32(txt_W_Child.Text) * NO_MONTH) / 12);
                    }

                    int Omi_Limit = Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text) + Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text) + Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text);
                    txt_W_Omi_Limit.Text = Omi_Limit.ToString();

                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc9", exp);
            }
        }

        public void Proc8()
        {
            try
            {
                int SELF = 0;
                int WIFE = 0;
                int SON = 0;

                /*Self*/
                Dictionary<string, object> paramSelf = new Dictionary<string, object>();
                paramSelf.Add("OMI_P_NO", txt_W_PNO.Text);
                paramSelf.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCodeS;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCodeS = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC8_SELF", paramSelf);

                if (rsltCodeS.isSuccessful && rsltCodeS.dstResult.Tables.Count > 0 && rsltCodeS.dstResult.Tables[0].Rows.Count > 0)
                {
                    SELF = Convert.ToInt32(rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString() == string.Empty ? "0" : rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString());
                }


                /*WIFE*/
                Dictionary<string, object> paramWife = new Dictionary<string, object>();
                paramWife.Add("OMI_P_NO", txt_W_PNO.Text);
                paramWife.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCodeW;
                rsltCodeW = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC8_WIFE", paramWife);

                if (rsltCodeW.isSuccessful && rsltCodeW.dstResult.Tables.Count > 0 && rsltCodeW.dstResult.Tables[0].Rows.Count > 0)
                {
                    WIFE = Convert.ToInt32(rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString() == string.Empty ? "0" : rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramSon = new Dictionary<string, object>();
                paramSon.Add("OMI_P_NO", txt_W_PNO.Text);
                paramSon.Add("OMI_YEAR", txt_W_Year.Text);

                Result rsltCodeSon;
                rsltCodeSon = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC8_SON", paramSon);

                if (rsltCodeSon.isSuccessful && rsltCodeSon.dstResult.Tables.Count > 0 && rsltCodeSon.dstResult.Tables[0].Rows.Count > 0)
                {
                    SON = Convert.ToInt32(rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString() == string.Empty ? "0" : rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString());
                }

                txt_Omi_Self.Text       = Convert.ToString((Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text)) + SELF);
                txt_Omi_Spouse.Text     = Convert.ToString((Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text)) + WIFE);
                txt_Omi_Child.Text      = Convert.ToString((Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text)) + SON);
                txt_Omi_Total_Claim.Text = Convert.ToString(SELF + WIFE + SON);

            }
            catch (Exception exp)
            {
                LogError(this.Name, "Proc8", exp);
            }
        }

        #endregion

        #region --Events--

        private void txt_W_PNO_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (!lbtnPersonnel.Focused)
                {
                    txt_W_Self.Text = string.Empty;
                    txt_W_Spouse.Text = string.Empty;
                    txt_W_Child.Text = string.Empty;
                    txt_Omi_Self.Text = string.Empty;
                    txt_Omi_Spouse.Text = string.Empty;
                    txt_Omi_Child.Text = string.Empty;
                    txt_W_Omi_Limit.Text = string.Empty;
                    txt_Omi_Total_Claim.Text = string.Empty;
                    txt_W_Os_Balance.Text = string.Empty;

                    if (txt_ENROLL.Text == "N")
                    {
                        MessageBox.Show(txt_W_Name.Text + " IS NOT ENROLLED IN OMI", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("OMI_P_NO", txt_W_PNO.Text);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_PRECLAIM_OMI_CLAIM_MANAGER", "OMI_YEAR", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_W_Year.Text = rsltCode.dstResult.Tables[0].Rows[0]["OMI_YEAR"].ToString();
                    }

                    if (txt_W_Year.Text == string.Empty)
                    {
                        txt_W_Year.Text = Now().Year.ToString();
                    }

                    param.Clear();
                    param.Add("OMI_P_NO", txt_W_PNO.Text);
                    param.Add("OMI_YEAR", txt_W_Year.Text);

                    
                    rsltCode = cmnDM.GetData("CHRIS_SP_PRECLAIM_OMI_CLAIM_MANAGER", "OMI_MONTH", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        str_Omi_Month = rsltCode.dstResult.Tables[0].Rows[0]["OMI_MONTH"].ToString();
                    }

                    txt_W_Year.Text = txt_W_Year.Text == string.Empty ? Now().Year.ToString() : txt_W_Year.Text;

                    OMICLAIMCommand ent = (OMICLAIMCommand)(this.pnlOmiMaster.CurrentBusinessEntity);
                    ent.OMI_YEAR = Convert.ToDecimal(txt_W_Year.Text);

                    DeptContCommand entdpt = (DeptContCommand)(this.pnlTblDept.CurrentBusinessEntity);
                    entdpt.PR_P_NO = Convert.ToDouble(txt_W_PNO.Text == string.Empty ? "0" : txt_W_PNO.Text);


                    this.pnlOmiMaster.LoadDependentPanels();
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "", exp);
            }
        }

        private void txt_W_Omi_Limit_Enter(object sender, EventArgs e)
        {
            
        }

        private void txt_W_PNO_Validated(object sender, EventArgs e)
        {
            try
            {
                if (!lbtnPersonnel.Focused)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("W_CAT", txt_W_CAT.Text);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_PRECLAIM_OMI_CLAIM_MANAGER", "W_OMI_LIMIT", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_W_Omi_Limit.Text = rsltCode.dstResult.Tables[0].Rows[0]["W_OMI_LIMIT"].ToString();
                        txt_W_Self.Text = rsltCode.dstResult.Tables[0].Rows[0]["W_SELF"].ToString();
                        txt_W_Spouse.Text = rsltCode.dstResult.Tables[0].Rows[0]["W_SPOUSE"].ToString();
                        txt_W_Child.Text = rsltCode.dstResult.Tables[0].Rows[0]["W_CHILD"].ToString();
                    }

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 1)
                    {
                        txt_W_Omi_Limit.Text = "0";
                    }
                    else if (rsltCode.isSuccessful)
                    {
                        txt_W_Omi_Limit.Text = "0";
                    }

                    dgvOmiDetail.Rows[0].Cells[1].Value = txt_W_Year.Text;
                    dgvOmiDetail.Rows[0].Cells[0].Value = str_Omi_Month;

                    Proc5();
                    Proc6();

                    int omi_Limit = Convert.ToInt32(txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text) + Convert.ToInt32(txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text) + Convert.ToInt32(txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text);
                    txt_W_Omi_Limit.Text = omi_Limit.ToString();


                    //IF MESSAGE_CODE  = 40350  THEN
                    //   :BLKONE.OMI_SELF         := :W_SELF;
                    //   :BLKONE.OMI_SPOUSE       := :W_SPOUSE;
                    //   :BLKONE.OMI_CHILD        := :W_CHILD;
                    //END IF;

                    Proc9();
                    Proc8();

                    int os_Balance = Convert.ToInt16(txt_W_Omi_Limit.Text == string.Empty ? "0" : txt_W_Omi_Limit.Text) + Convert.ToInt16(txt_Omi_Total_Claim.Text == string.Empty ? "0" : txt_Omi_Total_Claim.Text);
                    txt_W_Os_Balance.Text = os_Balance.ToString();

                    int Total = 0;
                    int Total_Recv = 0;

                    foreach (DataGridViewRow dr in dgvOmiDetail.Rows)
                    {
                        Total = Total + Convert.ToInt32(dr.Cells["OMI_CLAIM_AMOUNT"].EditedFormattedValue == "" ? "0" : dr.Cells["OMI_CLAIM_AMOUNT"].Value);
                        Total_Recv = Total_Recv + Convert.ToInt32(dr.Cells["OMI_RECEV_AMOUNT"].EditedFormattedValue == "" ? "0" : dr.Cells["OMI_RECEV_AMOUNT"].Value);
                    }

                    txt_W_Total.Text = Total.ToString();
                    txt_W_Total_Recv.Text = Total_Recv.ToString();

                    txt_W_Self.Text = txt_W_Self.Text == string.Empty ? "0" : txt_W_Self.Text;
                    txt_W_Spouse.Text = txt_W_Spouse.Text == string.Empty ? "0" : txt_W_Spouse.Text;
                    txt_W_Child.Text = txt_W_Child.Text == string.Empty ? "0" : txt_W_Child.Text;
                    txt_Omi_Self.Text = txt_Omi_Self.Text == string.Empty ? "0" : txt_Omi_Self.Text;
                    txt_Omi_Spouse.Text = txt_Omi_Spouse.Text == string.Empty ? "0" : txt_Omi_Spouse.Text;
                    txt_Omi_Child.Text = txt_Omi_Child.Text == string.Empty ? "0" : txt_Omi_Child.Text;
                    txt_W_Omi_Limit.Text = txt_W_Omi_Limit.Text == string.Empty ? "0" : txt_W_Omi_Limit.Text;
                    txt_Omi_Total_Claim.Text = txt_Omi_Total_Claim.Text == string.Empty ? "0" : txt_Omi_Total_Claim.Text;
                    txt_W_Os_Balance.Text = txt_W_Os_Balance.Text == string.Empty ? "0" : txt_W_Os_Balance.Text;

                    txtUser.Text = this.UserName;
                    txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                    lblUser.Text = "UserName : " + this.userID;
                    txtOption.Visible = false;
                    this.txtOption.Visible = false;
                    this.txt_W_PNO.Focus();
                }
            }
            catch (Exception exp)
            {
                LogError(this.Name, "txt_W_PNO_Validated", exp);
            }
        }
        
        #endregion

        
    }
}