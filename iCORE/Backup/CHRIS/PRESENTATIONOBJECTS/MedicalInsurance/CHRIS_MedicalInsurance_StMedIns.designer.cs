namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_StMedIns
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_StMedIns));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.today = new CrplControlLibrary.SLDatePicker(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.P_COPIES = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Format = new CrplControlLibrary.SLTextBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.w_year = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.close = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.today);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.P_COPIES);
            this.groupBox1.Controls.Add(this.Dest_Format);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.w_year);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.close);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(431, 287);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(363, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 101;
            this.pictureBox1.TabStop = false;
            // 
            // today
            // 
            this.today.CustomEnabled = true;
            this.today.CustomFormat = "dd/MM/yyyy";
            this.today.DataFieldMapping = "";
            this.today.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.today.HasChanges = true;
            this.today.Location = new System.Drawing.Point(110, 95);
            this.today.Name = "today";
            this.today.NullValue = " ";
            this.today.Size = new System.Drawing.Size(172, 20);
            this.today.TabIndex = 2;
            this.today.Value = new System.DateTime(2011, 4, 25, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 95;
            this.label5.Text = "Today";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // P_COPIES
            // 
            this.P_COPIES.AllowSpace = true;
            this.P_COPIES.AssociatedLookUpName = "";
            this.P_COPIES.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.P_COPIES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.P_COPIES.ContinuationTextBox = null;
            this.P_COPIES.CustomEnabled = true;
            this.P_COPIES.DataFieldMapping = "";
            this.P_COPIES.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P_COPIES.GetRecordsOnUpDownKeys = false;
            this.P_COPIES.IsDate = false;
            this.P_COPIES.Location = new System.Drawing.Point(110, 172);
            this.P_COPIES.MaxLength = 2;
            this.P_COPIES.Name = "P_COPIES";
            this.P_COPIES.NumberFormat = "###,###,##0.00";
            this.P_COPIES.Postfix = "";
            this.P_COPIES.Prefix = "";
            this.P_COPIES.Size = new System.Drawing.Size(172, 20);
            this.P_COPIES.SkipValidation = false;
            this.P_COPIES.TabIndex = 5;
            this.P_COPIES.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // Dest_Format
            // 
            this.Dest_Format.AllowSpace = true;
            this.Dest_Format.AssociatedLookUpName = "";
            this.Dest_Format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Format.ContinuationTextBox = null;
            this.Dest_Format.CustomEnabled = true;
            this.Dest_Format.DataFieldMapping = "";
            this.Dest_Format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Format.GetRecordsOnUpDownKeys = false;
            this.Dest_Format.IsDate = false;
            this.Dest_Format.Location = new System.Drawing.Point(110, 146);
            this.Dest_Format.MaxLength = 40;
            this.Dest_Format.Name = "Dest_Format";
            this.Dest_Format.NumberFormat = "###,###,##0.00";
            this.Dest_Format.Postfix = "";
            this.Dest_Format.Prefix = "";
            this.Dest_Format.Size = new System.Drawing.Size(172, 20);
            this.Dest_Format.SkipValidation = false;
            this.Dest_Format.TabIndex = 4;
            this.Dest_Format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 91;
            this.label4.Text = "Copies";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 89;
            this.label2.Text = "DesFormat";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // w_year
            // 
            this.w_year.AllowSpace = true;
            this.w_year.AssociatedLookUpName = "";
            this.w_year.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.w_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.w_year.ContinuationTextBox = null;
            this.w_year.CustomEnabled = true;
            this.w_year.DataFieldMapping = "";
            this.w_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.w_year.GetRecordsOnUpDownKeys = false;
            this.w_year.IsDate = false;
            this.w_year.Location = new System.Drawing.Point(110, 198);
            this.w_year.MaxLength = 4;
            this.w_year.Name = "w_year";
            this.w_year.NumberFormat = "###,###,##0.00";
            this.w_year.Postfix = "";
            this.w_year.Prefix = "";
            this.w_year.Size = new System.Drawing.Size(172, 20);
            this.w_year.SkipValidation = false;
            this.w_year.TabIndex = 6;
            this.w_year.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(16, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 87;
            this.label10.Text = "FOR YEAR";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(41, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(362, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameters";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(41, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(362, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // close
            // 
            this.close.ActionType = "";
            this.close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.Location = new System.Drawing.Point(197, 235);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 8;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(110, 235);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 7;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "DesName";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "DesType";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(110, 121);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(172, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 3;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(110, 69);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(172, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 1;
            // 
            // CHRIS_MedicalInsurance_StMedIns
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 336);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_MedicalInsurance_StMedIns";
            this.Text = "CHRIS_MedicalInsurance_StMedIns";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrplControlLibrary.SLButton close;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox w_year;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox P_COPIES;
        private CrplControlLibrary.SLTextBox Dest_Format;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLDatePicker today;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}