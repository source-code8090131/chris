namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_EnrEntry_GHIDE01
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_EnrEntry_GHIDE01));
            this.pnlInsuEntMain = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.slTextBox4 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvGrpHosp = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.colPR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDJoining = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEnrolled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtChoice = new CrplControlLibrary.SLTextBox(this.components);
            this.lblUser = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlInsuEntMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrpHosp)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(591, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(627, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 413);
            this.panel1.Size = new System.Drawing.Size(627, 60);
            // 
            // pnlInsuEntMain
            // 
            this.pnlInsuEntMain.ConcurrentPanels = null;
            this.pnlInsuEntMain.Controls.Add(this.label5);
            this.pnlInsuEntMain.Controls.Add(this.slTextBox4);
            this.pnlInsuEntMain.Controls.Add(this.slTextBox3);
            this.pnlInsuEntMain.Controls.Add(this.txtDate);
            this.pnlInsuEntMain.Controls.Add(this.label3);
            this.pnlInsuEntMain.Controls.Add(this.label2);
            this.pnlInsuEntMain.Controls.Add(this.label1);
            this.pnlInsuEntMain.Controls.Add(this.dgvGrpHosp);
            this.pnlInsuEntMain.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlInsuEntMain.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlInsuEntMain.DependentPanels = null;
            this.pnlInsuEntMain.DisableDependentLoad = false;
            this.pnlInsuEntMain.EnableDelete = true;
            this.pnlInsuEntMain.EnableInsert = true;
            this.pnlInsuEntMain.EnableQuery = false;
            this.pnlInsuEntMain.EnableUpdate = true;
            this.pnlInsuEntMain.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlInsuEntMain.Location = new System.Drawing.Point(12, 86);
            this.pnlInsuEntMain.MasterPanel = null;
            this.pnlInsuEntMain.Name = "pnlInsuEntMain";
            this.pnlInsuEntMain.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlInsuEntMain.Size = new System.Drawing.Size(610, 301);
            this.pnlInsuEntMain.SPName = "CHRIS_SP_EnrLettPERSONNEL_MANAGER";
            this.pnlInsuEntMain.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(728, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "_________________________________________________________________________________" +
                "______________________";
            // 
            // slTextBox4
            // 
            this.slTextBox4.AllowSpace = true;
            this.slTextBox4.AssociatedLookUpName = "";
            this.slTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox4.ContinuationTextBox = null;
            this.slTextBox4.CustomEnabled = true;
            this.slTextBox4.DataFieldMapping = "";
            this.slTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox4.GetRecordsOnUpDownKeys = false;
            this.slTextBox4.IsDate = false;
            this.slTextBox4.Location = new System.Drawing.Point(33, 28);
            this.slTextBox4.Name = "slTextBox4";
            this.slTextBox4.NumberFormat = "###,###,##0.00";
            this.slTextBox4.Postfix = "";
            this.slTextBox4.Prefix = "";
            this.slTextBox4.Size = new System.Drawing.Size(81, 20);
            this.slTextBox4.SkipValidation = false;
            this.slTextBox4.TabIndex = 9;
            this.slTextBox4.TabStop = false;
            this.slTextBox4.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox4.Visible = false;
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "";
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.Location = new System.Drawing.Point(33, 6);
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.Size = new System.Drawing.Size(81, 20);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 8;
            this.slTextBox3.TabStop = false;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox3.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(523, 6);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(81, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 7;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(475, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(234, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "ENROLLMENTENTRY";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(221, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "GROUP HOSPITALIZATION";
            // 
            // dgvGrpHosp
            // 
            this.dgvGrpHosp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGrpHosp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPR_P_NO,
            this.colName,
            this.colDJoining,
            this.colEnrolled});
            this.dgvGrpHosp.ColumnToHide = "colPR_P_NO|colName|colDJoining";
            this.dgvGrpHosp.ColumnWidth = null;
            this.dgvGrpHosp.CustomEnabled = true;
            this.dgvGrpHosp.DisplayColumnWrapper = null;
            this.dgvGrpHosp.GridDefaultRow = 0;
            this.dgvGrpHosp.Location = new System.Drawing.Point(3, 74);
            this.dgvGrpHosp.Name = "dgvGrpHosp";
            this.dgvGrpHosp.ReadOnlyColumns = null;
            this.dgvGrpHosp.RequiredColumns = null;
            this.dgvGrpHosp.Size = new System.Drawing.Size(601, 224);
            this.dgvGrpHosp.SkippingColumns = null;
            this.dgvGrpHosp.TabIndex = 0;
            this.dgvGrpHosp.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvGrpHosp_EditingControlShowing);
            // 
            // colPR_P_NO
            // 
            this.colPR_P_NO.DataPropertyName = "PR_P_NO";
            this.colPR_P_NO.HeaderText = "P.No.";
            this.colPR_P_NO.Name = "colPR_P_NO";
            this.colPR_P_NO.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "NAME";
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colDJoining
            // 
            this.colDJoining.DataPropertyName = "PR_JOINING_DATE";
            this.colDJoining.HeaderText = "Date of Joining";
            this.colDJoining.Name = "colDJoining";
            this.colDJoining.ReadOnly = true;
            this.colDJoining.Width = 130;
            // 
            // colEnrolled
            // 
            this.colEnrolled.DataPropertyName = "PR_BANK_ID";
            this.colEnrolled.HeaderText = "lEnrolled [Y/N]";
            this.colEnrolled.Name = "colEnrolled";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(201, 397);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "2) IND.EMPLOYEE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(324, 397);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(216, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "3) ALL NOT INROLLED IN LIFE INS.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 397);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "1) DATE OF JOINING RANGE";
            // 
            // txtChoice
            // 
            this.txtChoice.AllowSpace = true;
            this.txtChoice.AssociatedLookUpName = "";
            this.txtChoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChoice.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtChoice.ContinuationTextBox = null;
            this.txtChoice.CustomEnabled = true;
            this.txtChoice.DataFieldMapping = "";
            this.txtChoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChoice.GetRecordsOnUpDownKeys = false;
            this.txtChoice.IsDate = false;
            this.txtChoice.Location = new System.Drawing.Point(560, 390);
            this.txtChoice.MaxLength = 1;
            this.txtChoice.Name = "txtChoice";
            this.txtChoice.NumberFormat = "###,###,##0.00";
            this.txtChoice.Postfix = "";
            this.txtChoice.Prefix = "";
            this.txtChoice.Size = new System.Drawing.Size(56, 20);
            this.txtChoice.SkipValidation = false;
            this.txtChoice.TabIndex = 0;
            this.txtChoice.TextType = CrplControlLibrary.TextType.String;
            this.txtChoice.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtChoice_PreviewKeyDown);
            this.txtChoice.Validating += new System.ComponentModel.CancelEventHandler(this.txtChoice_Validating);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(416, 9);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(63, 13);
            this.lblUser.TabIndex = 14;
            this.lblUser.Text = "UserName :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(481, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 13;
            this.lblUserName.Text = "UserName :";
            // 
            // CHRIS_MedicalInsurance_EnrEntry_GHIDE01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 473);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlInsuEntMain);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtChoice);
            this.Name = "CHRIS_MedicalInsurance_EnrEntry_GHIDE01";
            this.SetFormTitle = "";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_EnrEntry_GHIDE01";
            this.Controls.SetChildIndex(this.txtChoice, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.pnlInsuEntMain, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblUser, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlInsuEntMain.ResumeLayout(false);
            this.pnlInsuEntMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrpHosp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlInsuEntMain;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvGrpHosp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox txtChoice;
        private CrplControlLibrary.SLTextBox slTextBox4;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDJoining;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEnrolled;
    }
}