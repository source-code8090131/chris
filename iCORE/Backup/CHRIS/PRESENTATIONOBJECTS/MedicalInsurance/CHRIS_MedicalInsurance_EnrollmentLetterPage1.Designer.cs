namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_EnrollmentLetterPage1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPr_P_No = new CrplControlLibrary.SLTextBox(this.components);
            this.pnl1 = new System.Windows.Forms.Panel();
            this.dtpFrom = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtpTo = new CrplControlLibrary.SLDatePicker(this.components);
            this.pnl2 = new System.Windows.Forms.Panel();
            this.pnl1.SuspendLayout();
            this.pnl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(16, 30);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(106, 13);
            this.lbl2.TabIndex = 3;
            this.lbl2.Text = "To                    :";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(16, 7);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(106, 13);
            this.lbl1.TabIndex = 2;
            this.lbl1.Text = "From                 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Enter Personnel No.        :";
            // 
            // txtPr_P_No
            // 
            this.txtPr_P_No.AllowSpace = true;
            this.txtPr_P_No.AssociatedLookUpName = "";
            this.txtPr_P_No.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPr_P_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPr_P_No.ContinuationTextBox = null;
            this.txtPr_P_No.CustomEnabled = true;
            this.txtPr_P_No.DataFieldMapping = "";
            this.txtPr_P_No.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPr_P_No.GetRecordsOnUpDownKeys = false;
            this.txtPr_P_No.IsDate = false;
            this.txtPr_P_No.Location = new System.Drawing.Point(184, 6);
            this.txtPr_P_No.MaxLength = 6;
            this.txtPr_P_No.Name = "txtPr_P_No";
            this.txtPr_P_No.NumberFormat = "###,###,##0.00";
            this.txtPr_P_No.Postfix = "";
            this.txtPr_P_No.Prefix = "";
            this.txtPr_P_No.Size = new System.Drawing.Size(61, 20);
            this.txtPr_P_No.SkipValidation = false;
            this.txtPr_P_No.TabIndex = 0;
            this.txtPr_P_No.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPr_P_No.Validating += new System.ComponentModel.CancelEventHandler(this.txtPr_P_No_Validating);
            // 
            // pnl1
            // 
            this.pnl1.Controls.Add(this.dtpFrom);
            this.pnl1.Controls.Add(this.dtpTo);
            this.pnl1.Controls.Add(this.lbl1);
            this.pnl1.Controls.Add(this.lbl2);
            this.pnl1.Location = new System.Drawing.Point(107, 7);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(286, 49);
            this.pnl1.TabIndex = 6;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomEnabled = true;
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.DataFieldMapping = "";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.HasChanges = true;
            this.dtpFrom.Location = new System.Drawing.Point(129, 3);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.NullValue = " ";
            this.dtpFrom.Size = new System.Drawing.Size(108, 20);
            this.dtpFrom.TabIndex = 4;
            this.dtpFrom.Value = new System.DateTime(2011, 4, 14, 0, 0, 0, 0);
            this.dtpFrom.Validating += new System.ComponentModel.CancelEventHandler(this.dtpFrom_Validating);
            // 
            // dtpTo
            // 
            this.dtpTo.CustomEnabled = true;
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.DataFieldMapping = "";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.HasChanges = true;
            this.dtpTo.Location = new System.Drawing.Point(128, 26);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.NullValue = " ";
            this.dtpTo.Size = new System.Drawing.Size(108, 20);
            this.dtpTo.TabIndex = 5;
            this.dtpTo.Value = new System.DateTime(2011, 4, 14, 0, 0, 0, 0);
            this.dtpTo.Validating += new System.ComponentModel.CancelEventHandler(this.dtpTo_Validating);
            // 
            // pnl2
            // 
            this.pnl2.Controls.Add(this.txtPr_P_No);
            this.pnl2.Controls.Add(this.label2);
            this.pnl2.Location = new System.Drawing.Point(108, 15);
            this.pnl2.Name = "pnl2";
            this.pnl2.Size = new System.Drawing.Size(286, 35);
            this.pnl2.TabIndex = 7;
            // 
            // CHRIS_MedicalInsurance_EnrollmentLetterPage1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(521, 58);
            this.Controls.Add(this.pnl2);
            this.Controls.Add(this.pnl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CHRIS_MedicalInsurance_EnrollmentLetterPage1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHRIS_MedicalInsurance_EnrollmentLetterPage1";
            this.Load += new System.EventHandler(this.CHRIS_MedicalInsurance_EnrollmentLetterPage1_Load);
            this.pnl1.ResumeLayout(false);
            this.pnl1.PerformLayout();
            this.pnl2.ResumeLayout(false);
            this.pnl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.Panel pnl2;
        public CrplControlLibrary.SLTextBox txtPr_P_No;
        public CrplControlLibrary.SLDatePicker dtpFrom;
        public CrplControlLibrary.SLDatePicker dtpTo;

    }
}