namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_CFEntry_GHIDE03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_CFEntry_GHIDE03));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUsername = new CrplControlLibrary.SLTextBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.slPanelTabular1 = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDataGridView1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.CLA_S_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLA_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLA_CLAIMED_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLA_REC_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slPanelSimple1 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lkupPersonnelNo = new CrplControlLibrary.LookupButton(this.components);
            this.lkupPolicy = new CrplControlLibrary.LookupButton(this.components);
            this.slTextBox5 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox4 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox3 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox2 = new CrplControlLibrary.SLTextBox(this.components);
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel2.SuspendLayout();
            this.slPanelTabular1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).BeginInit();
            this.slPanelSimple1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 535);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Controls.Add(this.txtLocation);
            this.panel2.Controls.Add(this.txtUsername);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(40, 86);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(593, 77);
            this.panel2.TabIndex = 10;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(468, 15);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(100, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 7;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(81, 41);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(100, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 6;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUsername
            // 
            this.txtUsername.AllowSpace = true;
            this.txtUsername.AssociatedLookUpName = "";
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsername.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUsername.ContinuationTextBox = null;
            this.txtUsername.CustomEnabled = true;
            this.txtUsername.DataFieldMapping = "";
            this.txtUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.GetRecordsOnUpDownKeys = false;
            this.txtUsername.IsDate = false;
            this.txtUsername.Location = new System.Drawing.Point(81, 15);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.NumberFormat = "###,###,##0.00";
            this.txtUsername.Postfix = "";
            this.txtUsername.Prefix = "";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(100, 20);
            this.txtUsername.SkipValidation = false;
            this.txtUsername.TabIndex = 5;
            this.txtUsername.TabStop = false;
            this.txtUsername.TextType = CrplControlLibrary.TextType.String;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(427, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Location:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "User:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(235, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "CLAIM FORM ENTRY";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(220, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "GROUP HOSPITALIZATION";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(433, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "User Name:";
            // 
            // slPanelTabular1
            // 
            this.slPanelTabular1.ConcurrentPanels = null;
            this.slPanelTabular1.Controls.Add(this.slDataGridView1);
            this.slPanelTabular1.DataManager = null;
            this.slPanelTabular1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelTabular1.DependentPanels = null;
            this.slPanelTabular1.DisableDependentLoad = false;
            this.slPanelTabular1.EnableDelete = true;
            this.slPanelTabular1.EnableInsert = true;
            this.slPanelTabular1.EnableQuery = false;
            this.slPanelTabular1.EnableUpdate = true;
            this.slPanelTabular1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MedicalInsuranceClaimDetailCommand";
            this.slPanelTabular1.Location = new System.Drawing.Point(42, 245);
            this.slPanelTabular1.MasterPanel = null;
            this.slPanelTabular1.Name = "slPanelTabular1";
            this.slPanelTabular1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPanelTabular1.Size = new System.Drawing.Size(585, 273);
            this.slPanelTabular1.SPName = "CHRIS_SP_CLAIM_GHI_MANAGER";
            this.slPanelTabular1.TabIndex = 16;
            // 
            // slDataGridView1
            // 
            this.slDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CLA_S_NO,
            this.CLA_DATE,
            this.CLA_CLAIMED_AMT,
            this.CLA_REC_AMT});
            this.slDataGridView1.ColumnToHide = null;
            this.slDataGridView1.ColumnWidth = null;
            this.slDataGridView1.CustomEnabled = true;
            this.slDataGridView1.DisplayColumnWrapper = null;
            this.slDataGridView1.GridDefaultRow = 0;
            this.slDataGridView1.Location = new System.Drawing.Point(3, 3);
            this.slDataGridView1.Name = "slDataGridView1";
            this.slDataGridView1.ReadOnlyColumns = null;
            this.slDataGridView1.RequiredColumns = "DATE|CLAIMED AMOUNT|CLA_DATE|CLA_CLAIMED_AMT";
            this.slDataGridView1.Size = new System.Drawing.Size(579, 267);
            this.slDataGridView1.SkippingColumns = null;
            this.slDataGridView1.TabIndex = 0;
            this.slDataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.slDataGridView1_RowEnter);
            this.slDataGridView1.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.slDataGridView1_RowValidating);
            this.slDataGridView1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.slDataGridView1_CellValidating);
            // 
            // CLA_S_NO
            // 
            this.CLA_S_NO.DataPropertyName = "CLA_S_NO";
            this.CLA_S_NO.HeaderText = "S.N0.";
            this.CLA_S_NO.Name = "CLA_S_NO";
            this.CLA_S_NO.ReadOnly = true;
            this.CLA_S_NO.Width = 50;
            // 
            // CLA_DATE
            // 
            this.CLA_DATE.DataPropertyName = "CLA_DATE";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.CLA_DATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.CLA_DATE.HeaderText = "Date";
            this.CLA_DATE.Name = "CLA_DATE";
            this.CLA_DATE.Width = 160;
            // 
            // CLA_CLAIMED_AMT
            // 
            this.CLA_CLAIMED_AMT.DataPropertyName = "CLA_CLAIMED_AMT";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.CLA_CLAIMED_AMT.DefaultCellStyle = dataGridViewCellStyle2;
            this.CLA_CLAIMED_AMT.HeaderText = "Claimed Amount";
            this.CLA_CLAIMED_AMT.MaxInputLength = 10;
            this.CLA_CLAIMED_AMT.Name = "CLA_CLAIMED_AMT";
            this.CLA_CLAIMED_AMT.Width = 160;
            // 
            // CLA_REC_AMT
            // 
            this.CLA_REC_AMT.DataPropertyName = "CLA_REC_AMT";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.CLA_REC_AMT.DefaultCellStyle = dataGridViewCellStyle3;
            this.CLA_REC_AMT.HeaderText = "Amount Received";
            this.CLA_REC_AMT.MaxInputLength = 10;
            this.CLA_REC_AMT.Name = "CLA_REC_AMT";
            this.CLA_REC_AMT.Width = 160;
            // 
            // slPanelSimple1
            // 
            this.slPanelSimple1.ConcurrentPanels = null;
            this.slPanelSimple1.Controls.Add(this.lkupPersonnelNo);
            this.slPanelSimple1.Controls.Add(this.lkupPolicy);
            this.slPanelSimple1.Controls.Add(this.slTextBox5);
            this.slPanelSimple1.Controls.Add(this.slTextBox4);
            this.slPanelSimple1.Controls.Add(this.slTextBox3);
            this.slPanelSimple1.Controls.Add(this.slTextBox2);
            this.slPanelSimple1.Controls.Add(this.slTextBox1);
            this.slPanelSimple1.Controls.Add(this.label10);
            this.slPanelSimple1.Controls.Add(this.label9);
            this.slPanelSimple1.Controls.Add(this.label8);
            this.slPanelSimple1.Controls.Add(this.label7);
            this.slPanelSimple1.DataManager = null;
            this.slPanelSimple1.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPanelSimple1.DependentPanels = null;
            this.slPanelSimple1.DisableDependentLoad = false;
            this.slPanelSimple1.EnableDelete = false;
            this.slPanelSimple1.EnableInsert = false;
            this.slPanelSimple1.EnableQuery = false;
            this.slPanelSimple1.EnableUpdate = false;
            this.slPanelSimple1.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ClaimEntryCommand";
            this.slPanelSimple1.Location = new System.Drawing.Point(42, 172);
            this.slPanelSimple1.MasterPanel = null;
            this.slPanelSimple1.Name = "slPanelSimple1";
            this.slPanelSimple1.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.slPanelSimple1.Size = new System.Drawing.Size(585, 65);
            this.slPanelSimple1.SPName = "CHRIS_MedicalInsurance_ClaimEntry_Search_MANAGER";
            this.slPanelSimple1.TabIndex = 15;
            // 
            // lkupPersonnelNo
            // 
            this.lkupPersonnelNo.ActionLOVExists = "PERSONNELEXISTS";
            this.lkupPersonnelNo.ActionType = "PERSONNEL";
            this.lkupPersonnelNo.ConditionalFields = "";
            this.lkupPersonnelNo.CustomEnabled = true;
            this.lkupPersonnelNo.DataFieldMapping = "";
            this.lkupPersonnelNo.DependentLovControls = "";
            this.lkupPersonnelNo.HiddenColumns = "";
            this.lkupPersonnelNo.Image = ((System.Drawing.Image)(resources.GetObject("lkupPersonnelNo.Image")));
            this.lkupPersonnelNo.LoadDependentEntities = true;
            this.lkupPersonnelNo.Location = new System.Drawing.Point(153, 32);
            this.lkupPersonnelNo.LookUpTitle = "PERSONNEL NO.\'S LIST";
            this.lkupPersonnelNo.Name = "lkupPersonnelNo";
            this.lkupPersonnelNo.Size = new System.Drawing.Size(26, 21);
            this.lkupPersonnelNo.SkipValidationOnLeave = false;
            this.lkupPersonnelNo.SPName = "CHRIS_MedicalInsurance_ClaimEntry_Search_MANAGER";
            this.lkupPersonnelNo.TabIndex = 11;
            this.lkupPersonnelNo.TabStop = false;
            this.lkupPersonnelNo.UseVisualStyleBackColor = true;
            // 
            // lkupPolicy
            // 
            this.lkupPolicy.ActionLOVExists = "POLICYEXISTS";
            this.lkupPolicy.ActionType = "POLICY";
            this.lkupPolicy.ConditionalFields = "";
            this.lkupPolicy.CustomEnabled = true;
            this.lkupPolicy.DataFieldMapping = "";
            this.lkupPolicy.DependentLovControls = "";
            this.lkupPolicy.HiddenColumns = "";
            this.lkupPolicy.Image = ((System.Drawing.Image)(resources.GetObject("lkupPolicy.Image")));
            this.lkupPolicy.LoadDependentEntities = true;
            this.lkupPolicy.Location = new System.Drawing.Point(292, 5);
            this.lkupPolicy.LookUpTitle = "VALID POLICY NO.\'s";
            this.lkupPolicy.Name = "lkupPolicy";
            this.lkupPolicy.Size = new System.Drawing.Size(26, 21);
            this.lkupPolicy.SkipValidationOnLeave = false;
            this.lkupPolicy.SPName = "CHRIS_MedicalInsurance_ClaimEntry_Search_MANAGER";
            this.lkupPolicy.TabIndex = 9;
            this.lkupPolicy.TabStop = false;
            this.lkupPolicy.UseVisualStyleBackColor = true;
            // 
            // slTextBox5
            // 
            this.slTextBox5.AllowSpace = true;
            this.slTextBox5.AssociatedLookUpName = "";
            this.slTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox5.ContinuationTextBox = null;
            this.slTextBox5.CustomEnabled = true;
            this.slTextBox5.DataFieldMapping = "";
            this.slTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox5.GetRecordsOnUpDownKeys = false;
            this.slTextBox5.IsDate = false;
            this.slTextBox5.Location = new System.Drawing.Point(485, 6);
            this.slTextBox5.Name = "slTextBox5";
            this.slTextBox5.NumberFormat = "###,###,##0.00";
            this.slTextBox5.Postfix = "";
            this.slTextBox5.Prefix = "";
            this.slTextBox5.ReadOnly = true;
            this.slTextBox5.Size = new System.Drawing.Size(97, 20);
            this.slTextBox5.SkipValidation = false;
            this.slTextBox5.TabIndex = 8;
            this.slTextBox5.TabStop = false;
            this.slTextBox5.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox4
            // 
            this.slTextBox4.AllowSpace = true;
            this.slTextBox4.AssociatedLookUpName = "";
            this.slTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox4.ContinuationTextBox = null;
            this.slTextBox4.CustomEnabled = true;
            this.slTextBox4.DataFieldMapping = "Pol_Ctt_Off";
            this.slTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox4.GetRecordsOnUpDownKeys = false;
            this.slTextBox4.IsDate = false;
            this.slTextBox4.Location = new System.Drawing.Point(452, 6);
            this.slTextBox4.Name = "slTextBox4";
            this.slTextBox4.NumberFormat = "###,###,##0.00";
            this.slTextBox4.Postfix = "";
            this.slTextBox4.Prefix = "";
            this.slTextBox4.Size = new System.Drawing.Size(26, 20);
            this.slTextBox4.SkipValidation = false;
            this.slTextBox4.TabIndex = 4;
            this.slTextBox4.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox4.TextChanged += new System.EventHandler(this.slTextBox4_TextChanged);
            // 
            // slTextBox3
            // 
            this.slTextBox3.AllowSpace = true;
            this.slTextBox3.AssociatedLookUpName = "";
            this.slTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox3.ContinuationTextBox = null;
            this.slTextBox3.CustomEnabled = true;
            this.slTextBox3.DataFieldMapping = "Person_Name";
            this.slTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox3.GetRecordsOnUpDownKeys = false;
            this.slTextBox3.IsDate = false;
            this.slTextBox3.Location = new System.Drawing.Point(294, 32);
            this.slTextBox3.Name = "slTextBox3";
            this.slTextBox3.NumberFormat = "###,###,##0.00";
            this.slTextBox3.Postfix = "";
            this.slTextBox3.Prefix = "";
            this.slTextBox3.ReadOnly = true;
            this.slTextBox3.Size = new System.Drawing.Size(288, 20);
            this.slTextBox3.SkipValidation = false;
            this.slTextBox3.TabIndex = 6;
            this.slTextBox3.TabStop = false;
            this.slTextBox3.TextType = CrplControlLibrary.TextType.String;
            // 
            // slTextBox2
            // 
            this.slTextBox2.AllowSpace = true;
            this.slTextBox2.AssociatedLookUpName = "lkupPersonnelNo";
            this.slTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox2.ContinuationTextBox = null;
            this.slTextBox2.CustomEnabled = true;
            this.slTextBox2.DataFieldMapping = "CLA_P_NO";
            this.slTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox2.GetRecordsOnUpDownKeys = false;
            this.slTextBox2.IsDate = false;
            this.slTextBox2.IsLookUpField = true;
            this.slTextBox2.IsRequired = true;
            this.slTextBox2.Location = new System.Drawing.Point(103, 32);
            this.slTextBox2.MaxLength = 6;
            this.slTextBox2.Name = "slTextBox2";
            this.slTextBox2.NumberFormat = "######";
            this.slTextBox2.Postfix = "";
            this.slTextBox2.Prefix = "";
            this.slTextBox2.Size = new System.Drawing.Size(44, 20);
            this.slTextBox2.SkipValidation = false;
            this.slTextBox2.TabIndex = 5;
            this.slTextBox2.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "lkupPolicy";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "CLA_POLICY";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.IsLookUpField = true;
            this.slTextBox1.IsRequired = true;
            this.slTextBox1.Location = new System.Drawing.Point(103, 6);
            this.slTextBox1.MaxLength = 25;
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(185, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 1;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(329, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "[C]lerical/[O]fficer:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(192, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Insured Person:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Personnel No:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Policy No:";
            // 
            // CHRIS_MedicalInsurance_CFEntry_GHIDE03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 595);
            this.Controls.Add(this.slPanelTabular1);
            this.Controls.Add(this.slPanelSimple1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label6);
            this.CurrentPanelBlock = "slPanelSimple1";
            this.Name = "CHRIS_MedicalInsurance_CFEntry_GHIDE03";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowSearchButton = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_CFEntry_GHIDE03";
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.slPanelSimple1, 0);
            this.Controls.SetChildIndex(this.slPanelTabular1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.slPanelTabular1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).EndInit();
            this.slPanelSimple1.ResumeLayout(false);
            this.slPanelSimple1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox txtDate;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPanelTabular1;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPanelSimple1;
        private CrplControlLibrary.LookupButton lkupPersonnelNo;
        private CrplControlLibrary.LookupButton lkupPolicy;
        private CrplControlLibrary.SLTextBox slTextBox5;
        private CrplControlLibrary.SLTextBox slTextBox4;
        private CrplControlLibrary.SLTextBox slTextBox3;
        private CrplControlLibrary.SLTextBox slTextBox2;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLA_S_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLA_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLA_CLAIMED_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLA_REC_AMT;
    }
}