namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_GHosRepOff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_GHosRepOff));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rundate = new CrplControlLibrary.SLDatePicker(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.label6 = new System.Windows.Forms.Label();
            this.Run = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.wcat = new CrplControlLibrary.SLTextBox(this.components);
            this.wseg = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.brn = new CrplControlLibrary.SLTextBox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.brn);
            this.groupBox1.Controls.Add(this.rundate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dest_format);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.wcat);
            this.groupBox1.Controls.Add(this.wseg);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 322);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // rundate
            // 
            this.rundate.CustomEnabled = true;
            this.rundate.CustomFormat = "dd/MM/yyyy";
            this.rundate.DataFieldMapping = "";
            this.rundate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.rundate.HasChanges = false;
            this.rundate.Location = new System.Drawing.Point(190, 75);
            this.rundate.Name = "rundate";
            this.rundate.NullValue = " ";
            this.rundate.Size = new System.Drawing.Size(172, 20);
            this.rundate.TabIndex = 0;
            this.rundate.Value = new System.DateTime(2011, 4, 18, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 86;
            this.label1.Text = "Run Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 85;
            this.label2.Text = "Desformat";
            // 
            // dest_format
            // 
            this.dest_format.AllowSpace = true;
            this.dest_format.AssociatedLookUpName = "";
            this.dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_format.ContinuationTextBox = null;
            this.dest_format.CustomEnabled = true;
            this.dest_format.DataFieldMapping = "";
            this.dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_format.GetRecordsOnUpDownKeys = false;
            this.dest_format.IsDate = false;
            this.dest_format.Location = new System.Drawing.Point(190, 126);
            this.dest_format.MaxLength = 50;
            this.dest_format.Name = "dest_format";
            this.dest_format.NumberFormat = "###,###,##0.00";
            this.dest_format.Postfix = "";
            this.dest_format.Prefix = "";
            this.dest_format.Size = new System.Drawing.Size(172, 20);
            this.dest_format.SkipValidation = false;
            this.dest_format.TabIndex = 2;
            this.dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(160, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameters";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(118, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(377, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Copies";
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(277, 260);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 8;
            this.slButton1.Text = "Close";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 233);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "[C] = Clerk\'s  [O] = Officers  :";
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(190, 260);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 7;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Enter Segment :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Enter Branch Code  :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Desname";
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(190, 153);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(172, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 3;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // wcat
            // 
            this.wcat.AllowSpace = true;
            this.wcat.AssociatedLookUpName = "";
            this.wcat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wcat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wcat.ContinuationTextBox = null;
            this.wcat.CustomEnabled = true;
            this.wcat.DataFieldMapping = "";
            this.wcat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wcat.GetRecordsOnUpDownKeys = false;
            this.wcat.IsDate = false;
            this.wcat.Location = new System.Drawing.Point(190, 231);
            this.wcat.MaxLength = 1;
            this.wcat.Name = "wcat";
            this.wcat.NumberFormat = "###,###,##0.00";
            this.wcat.Postfix = "";
            this.wcat.Prefix = "";
            this.wcat.Size = new System.Drawing.Size(172, 20);
            this.wcat.SkipValidation = false;
            this.wcat.TabIndex = 6;
            this.wcat.TextType = CrplControlLibrary.TextType.String;
            // 
            // wseg
            // 
            this.wseg.AllowSpace = true;
            this.wseg.AssociatedLookUpName = "";
            this.wseg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wseg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.wseg.ContinuationTextBox = null;
            this.wseg.CustomEnabled = true;
            this.wseg.DataFieldMapping = "";
            this.wseg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wseg.GetRecordsOnUpDownKeys = false;
            this.wseg.IsDate = false;
            this.wseg.Location = new System.Drawing.Point(190, 205);
            this.wseg.MaxLength = 3;
            this.wseg.Name = "wseg";
            this.wseg.NumberFormat = "###,###,##0.00";
            this.wseg.Postfix = "";
            this.wseg.Prefix = "";
            this.wseg.Size = new System.Drawing.Size(172, 20);
            this.wseg.SkipValidation = false;
            this.wseg.TabIndex = 5;
            this.wseg.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(190, 100);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(172, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 1;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // brn
            // 
            this.brn.AllowSpace = true;
            this.brn.AssociatedLookUpName = "";
            this.brn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.brn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.brn.ContinuationTextBox = null;
            this.brn.CustomEnabled = true;
            this.brn.DataFieldMapping = "";
            this.brn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brn.GetRecordsOnUpDownKeys = false;
            this.brn.IsDate = false;
            this.brn.Location = new System.Drawing.Point(190, 181);
            this.brn.MaxLength = 3;
            this.brn.Name = "brn";
            this.brn.NumberFormat = "###,###,##0.00";
            this.brn.Postfix = "";
            this.brn.Prefix = "";
            this.brn.Size = new System.Drawing.Size(172, 20);
            this.brn.SkipValidation = false;
            this.brn.TabIndex = 4;
            this.brn.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_MedicalInsurance_GHosRepOff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 371);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_MedicalInsurance_GHosRepOff";
            this.Text = "CHRIS_MedicalInsurance_GLI03";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLButton slButton1;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox copies;
        private CrplControlLibrary.SLTextBox wcat;
        private CrplControlLibrary.SLTextBox wseg;
        
        private CrplControlLibrary.SLTextBox Dest_Name;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox dest_format;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CrplControlLibrary.SLDatePicker rundate;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox brn;
        
        
    }
}