namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_PolicyInfo_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_PolicyInfo_Detail));
            this.pnlPolicyInfoDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txt_Pol_Add4 = new CrplControlLibrary.SLTextBox(this.components);
            this.dtp_Pol_To = new CrplControlLibrary.SLDatePicker(this.components);
            this.dtp_Pol_From = new CrplControlLibrary.SLDatePicker(this.components);
            this.lbtnBranch = new CrplControlLibrary.LookupButton(this.components);
            this.txt_W_Opt = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPolicyNo = new CrplControlLibrary.LookupButton(this.components);
            this.txt_Pol_Segment = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Branch = new CrplControlLibrary.SLTextBox(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_Pol_Add3 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Add2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Add1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Com_Name2 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Com_Name1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Contact_Per = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Pol_Ctt_Off_Description = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Ctt_Off = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Policy_Type = new CrplControlLibrary.SLTextBox(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_W_Policy = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_User1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Loc1 = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_W_Opt_Desc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlPage6 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_Pol_No_Depen = new CrplControlLibrary.SLTextBox(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_Pol_Omi_Limit = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Per_Depen = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Omi_Self = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlPage5 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_Pol_Room_Chg = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Delv_Limit = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Ailment = new CrplControlLibrary.SLTextBox(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlPage3 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_Pol_Max_Acc = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Max_Limit = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Cov_Multi = new CrplControlLibrary.SLTextBox(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlPage4 = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txt_Pol_Ctt_Acc = new CrplControlLibrary.SLTextBox(this.components);
            this.txt_Pol_Ctt_Cov = new CrplControlLibrary.SLTextBox(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txt_Pol_From = new CrplControlLibrary.SLDatePicker(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlPolicyInfoDetail.SuspendLayout();
            this.pnlPage6.SuspendLayout();
            this.pnlPage5.SuspendLayout();
            this.pnlPage3.SuspendLayout();
            this.pnlPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(574, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(610, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 559);
            this.panel1.Size = new System.Drawing.Size(610, 60);
            // 
            // pnlPolicyInfoDetail
            // 
            this.pnlPolicyInfoDetail.ConcurrentPanels = null;
            this.pnlPolicyInfoDetail.Controls.Add(this.label26);
            this.pnlPolicyInfoDetail.Controls.Add(this.label30);
            this.pnlPolicyInfoDetail.Controls.Add(this.label29);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Add4);
            this.pnlPolicyInfoDetail.Controls.Add(this.dtp_Pol_To);
            this.pnlPolicyInfoDetail.Controls.Add(this.dtp_Pol_From);
            this.pnlPolicyInfoDetail.Controls.Add(this.lbtnBranch);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_W_Opt);
            this.pnlPolicyInfoDetail.Controls.Add(this.lbtnPolicyNo);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Segment);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Branch);
            this.pnlPolicyInfoDetail.Controls.Add(this.label15);
            this.pnlPolicyInfoDetail.Controls.Add(this.label14);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Add3);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Add2);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Add1);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Com_Name2);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Com_Name1);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Contact_Per);
            this.pnlPolicyInfoDetail.Controls.Add(this.label13);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Ctt_Off_Description);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Ctt_Off);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_Pol_Policy_Type);
            this.pnlPolicyInfoDetail.Controls.Add(this.label12);
            this.pnlPolicyInfoDetail.Controls.Add(this.label11);
            this.pnlPolicyInfoDetail.Controls.Add(this.label10);
            this.pnlPolicyInfoDetail.Controls.Add(this.label9);
            this.pnlPolicyInfoDetail.Controls.Add(this.label8);
            this.pnlPolicyInfoDetail.Controls.Add(this.label7);
            this.pnlPolicyInfoDetail.Controls.Add(this.label6);
            this.pnlPolicyInfoDetail.Controls.Add(this.label5);
            this.pnlPolicyInfoDetail.Controls.Add(this.label4);
            this.pnlPolicyInfoDetail.Controls.Add(this.label3);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_W_Policy);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_W_User1);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_W_Loc1);
            this.pnlPolicyInfoDetail.Controls.Add(this.txt_W_Opt_Desc);
            this.pnlPolicyInfoDetail.Controls.Add(this.txtDate);
            this.pnlPolicyInfoDetail.Controls.Add(this.label2);
            this.pnlPolicyInfoDetail.Controls.Add(this.label1);
            this.pnlPolicyInfoDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPolicyInfoDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPolicyInfoDetail.DependentPanels = null;
            this.pnlPolicyInfoDetail.DisableDependentLoad = false;
            this.pnlPolicyInfoDetail.EnableDelete = true;
            this.pnlPolicyInfoDetail.EnableInsert = true;
            this.pnlPolicyInfoDetail.EnableQuery = false;
            this.pnlPolicyInfoDetail.EnableUpdate = true;
            this.pnlPolicyInfoDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PolicyInfoCommand";
            this.pnlPolicyInfoDetail.Location = new System.Drawing.Point(2, 83);
            this.pnlPolicyInfoDetail.MasterPanel = null;
            this.pnlPolicyInfoDetail.Name = "pnlPolicyInfoDetail";
            this.pnlPolicyInfoDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPolicyInfoDetail.Size = new System.Drawing.Size(601, 377);
            this.pnlPolicyInfoDetail.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.pnlPolicyInfoDetail.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(7, 0);
            this.label26.MinimumSize = new System.Drawing.Size(0, 2);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(591, 13);
            this.label26.TabIndex = 47;
            this.label26.Text = "_________________________________________________________________________________" +
                "__________________";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(7, 71);
            this.label30.MinimumSize = new System.Drawing.Size(0, 2);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(591, 13);
            this.label30.TabIndex = 46;
            this.label30.Text = "_________________________________________________________________________________" +
                "__________________";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(7, 358);
            this.label29.MaximumSize = new System.Drawing.Size(200, 15);
            this.label29.MinimumSize = new System.Drawing.Size(0, 15);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(189, 15);
            this.label29.TabIndex = 44;
            this.label29.Text = ":";
            // 
            // txt_Pol_Add4
            // 
            this.txt_Pol_Add4.AllowSpace = true;
            this.txt_Pol_Add4.AssociatedLookUpName = "";
            this.txt_Pol_Add4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Add4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Add4.ContinuationTextBox = null;
            this.txt_Pol_Add4.CustomEnabled = true;
            this.txt_Pol_Add4.DataFieldMapping = "POL_ADD4";
            this.txt_Pol_Add4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Add4.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Add4.IsDate = false;
            this.txt_Pol_Add4.Location = new System.Drawing.Point(201, 355);
            this.txt_Pol_Add4.MaxLength = 35;
            this.txt_Pol_Add4.Name = "txt_Pol_Add4";
            this.txt_Pol_Add4.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Add4.Postfix = "";
            this.txt_Pol_Add4.Prefix = "";
            this.txt_Pol_Add4.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Add4.SkipValidation = false;
            this.txt_Pol_Add4.TabIndex = 14;
            this.txt_Pol_Add4.TextType = CrplControlLibrary.TextType.String;
            // 
            // dtp_Pol_To
            // 
            this.dtp_Pol_To.CustomEnabled = true;
            this.dtp_Pol_To.CustomFormat = "dd/MM/yyyy";
            this.dtp_Pol_To.DataFieldMapping = "POL_TO";
            this.dtp_Pol_To.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_Pol_To.HasChanges = true;
            this.dtp_Pol_To.IsRequired = true;
            this.dtp_Pol_To.Location = new System.Drawing.Point(315, 187);
            this.dtp_Pol_To.Name = "dtp_Pol_To";
            this.dtp_Pol_To.NullValue = " ";
            this.dtp_Pol_To.Size = new System.Drawing.Size(91, 20);
            this.dtp_Pol_To.TabIndex = 7;
            this.dtp_Pol_To.Value = new System.DateTime(2011, 4, 11, 0, 0, 0, 0);
            this.dtp_Pol_To.Validating += new System.ComponentModel.CancelEventHandler(this.dtp_Pol_To_Validating);
            // 
            // dtp_Pol_From
            // 
            this.dtp_Pol_From.CustomEnabled = true;
            this.dtp_Pol_From.CustomFormat = "dd/MM/yyyy";
            this.dtp_Pol_From.DataFieldMapping = "POL_FROM";
            this.dtp_Pol_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_Pol_From.HasChanges = true;
            this.dtp_Pol_From.IsRequired = true;
            this.dtp_Pol_From.Location = new System.Drawing.Point(201, 187);
            this.dtp_Pol_From.Name = "dtp_Pol_From";
            this.dtp_Pol_From.NullValue = " ";
            this.dtp_Pol_From.Size = new System.Drawing.Size(91, 20);
            this.dtp_Pol_From.TabIndex = 6;
            this.dtp_Pol_From.Value = new System.DateTime(2011, 4, 11, 0, 0, 0, 0);
            // 
            // lbtnBranch
            // 
            this.lbtnBranch.ActionLOVExists = "";
            this.lbtnBranch.ActionType = "Branch_LOV";
            this.lbtnBranch.ConditionalFields = "";
            this.lbtnBranch.CustomEnabled = true;
            this.lbtnBranch.DataFieldMapping = "";
            this.lbtnBranch.DependentLovControls = "";
            this.lbtnBranch.HiddenColumns = "";
            this.lbtnBranch.Image = ((System.Drawing.Image)(resources.GetObject("lbtnBranch.Image")));
            this.lbtnBranch.LoadDependentEntities = false;
            this.lbtnBranch.Location = new System.Drawing.Point(565, 94);
            this.lbtnBranch.LookUpTitle = null;
            this.lbtnBranch.Name = "lbtnBranch";
            this.lbtnBranch.Size = new System.Drawing.Size(26, 21);
            this.lbtnBranch.SkipValidationOnLeave = true;
            this.lbtnBranch.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.lbtnBranch.TabIndex = 41;
            this.lbtnBranch.TabStop = false;
            this.lbtnBranch.UseVisualStyleBackColor = true;
            // 
            // txt_W_Opt
            // 
            this.txt_W_Opt.AllowSpace = true;
            this.txt_W_Opt.AssociatedLookUpName = "";
            this.txt_W_Opt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Opt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Opt.ContinuationTextBox = null;
            this.txt_W_Opt.CustomEnabled = true;
            this.txt_W_Opt.DataFieldMapping = "";
            this.txt_W_Opt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Opt.GetRecordsOnUpDownKeys = false;
            this.txt_W_Opt.IsDate = false;
            this.txt_W_Opt.Location = new System.Drawing.Point(5, 48);
            this.txt_W_Opt.Name = "txt_W_Opt";
            this.txt_W_Opt.NumberFormat = "###,###,##0.00";
            this.txt_W_Opt.Postfix = "";
            this.txt_W_Opt.Prefix = "";
            this.txt_W_Opt.Size = new System.Drawing.Size(49, 20);
            this.txt_W_Opt.SkipValidation = false;
            this.txt_W_Opt.TabIndex = 40;
            this.txt_W_Opt.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Opt.Visible = false;
            // 
            // lbtnPolicyNo
            // 
            this.lbtnPolicyNo.ActionLOVExists = "W_POLICY_LOV_EXISTS";
            this.lbtnPolicyNo.ActionType = "W_POLICY_LOV";
            this.lbtnPolicyNo.ConditionalFields = "txt_W_Opt";
            this.lbtnPolicyNo.CustomEnabled = true;
            this.lbtnPolicyNo.DataFieldMapping = "";
            this.lbtnPolicyNo.DependentLovControls = "";
            this.lbtnPolicyNo.HiddenColumns = resources.GetString("lbtnPolicyNo.HiddenColumns");
            this.lbtnPolicyNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPolicyNo.Image")));
            this.lbtnPolicyNo.LoadDependentEntities = true;
            this.lbtnPolicyNo.Location = new System.Drawing.Point(414, 94);
            this.lbtnPolicyNo.LookUpTitle = null;
            this.lbtnPolicyNo.Name = "lbtnPolicyNo";
            this.lbtnPolicyNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPolicyNo.SkipValidationOnLeave = true;
            this.lbtnPolicyNo.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.lbtnPolicyNo.TabIndex = 31;
            this.lbtnPolicyNo.TabStop = false;
            this.lbtnPolicyNo.UseVisualStyleBackColor = true;
            // 
            // txt_Pol_Segment
            // 
            this.txt_Pol_Segment.AllowSpace = true;
            this.txt_Pol_Segment.AssociatedLookUpName = "";
            this.txt_Pol_Segment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Segment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Segment.ContinuationTextBox = null;
            this.txt_Pol_Segment.CustomEnabled = true;
            this.txt_Pol_Segment.DataFieldMapping = "POL_SEGMENT";
            this.txt_Pol_Segment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Segment.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Segment.IsDate = false;
            this.txt_Pol_Segment.Location = new System.Drawing.Point(516, 117);
            this.txt_Pol_Segment.MaxLength = 3;
            this.txt_Pol_Segment.Name = "txt_Pol_Segment";
            this.txt_Pol_Segment.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Segment.Postfix = "";
            this.txt_Pol_Segment.Prefix = "";
            this.txt_Pol_Segment.Size = new System.Drawing.Size(42, 20);
            this.txt_Pol_Segment.SkipValidation = false;
            this.txt_Pol_Segment.TabIndex = 3;
            this.txt_Pol_Segment.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pol_Segment.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_Segment_Validating);
            // 
            // txt_Pol_Branch
            // 
            this.txt_Pol_Branch.AllowSpace = true;
            this.txt_Pol_Branch.AssociatedLookUpName = "lbtnBranch";
            this.txt_Pol_Branch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Branch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Branch.ContinuationTextBox = null;
            this.txt_Pol_Branch.CustomEnabled = true;
            this.txt_Pol_Branch.DataFieldMapping = "POL_BRANCH";
            this.txt_Pol_Branch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Branch.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Branch.IsDate = false;
            this.txt_Pol_Branch.Location = new System.Drawing.Point(516, 94);
            this.txt_Pol_Branch.MaxLength = 3;
            this.txt_Pol_Branch.Name = "txt_Pol_Branch";
            this.txt_Pol_Branch.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Branch.Postfix = "";
            this.txt_Pol_Branch.Prefix = "";
            this.txt_Pol_Branch.Size = new System.Drawing.Size(42, 20);
            this.txt_Pol_Branch.SkipValidation = false;
            this.txt_Pol_Branch.TabIndex = 1;
            this.txt_Pol_Branch.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pol_Branch.Enter += new System.EventHandler(this.txt_Pol_Branch_Enter);
            this.txt_Pol_Branch.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_Branch_Validating);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(448, 121);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Segment  :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(457, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Branch  :";
            // 
            // txt_Pol_Add3
            // 
            this.txt_Pol_Add3.AllowSpace = true;
            this.txt_Pol_Add3.AssociatedLookUpName = "";
            this.txt_Pol_Add3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Add3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Add3.ContinuationTextBox = null;
            this.txt_Pol_Add3.CustomEnabled = true;
            this.txt_Pol_Add3.DataFieldMapping = "POL_ADD3";
            this.txt_Pol_Add3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Add3.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Add3.IsDate = false;
            this.txt_Pol_Add3.Location = new System.Drawing.Point(201, 331);
            this.txt_Pol_Add3.MaxLength = 35;
            this.txt_Pol_Add3.Name = "txt_Pol_Add3";
            this.txt_Pol_Add3.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Add3.Postfix = "";
            this.txt_Pol_Add3.Prefix = "";
            this.txt_Pol_Add3.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Add3.SkipValidation = false;
            this.txt_Pol_Add3.TabIndex = 13;
            this.txt_Pol_Add3.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pol_Add2
            // 
            this.txt_Pol_Add2.AllowSpace = true;
            this.txt_Pol_Add2.AssociatedLookUpName = "";
            this.txt_Pol_Add2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Add2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Add2.ContinuationTextBox = null;
            this.txt_Pol_Add2.CustomEnabled = true;
            this.txt_Pol_Add2.DataFieldMapping = "POL_ADD2";
            this.txt_Pol_Add2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Add2.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Add2.IsDate = false;
            this.txt_Pol_Add2.Location = new System.Drawing.Point(201, 307);
            this.txt_Pol_Add2.MaxLength = 35;
            this.txt_Pol_Add2.Name = "txt_Pol_Add2";
            this.txt_Pol_Add2.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Add2.Postfix = "";
            this.txt_Pol_Add2.Prefix = "";
            this.txt_Pol_Add2.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Add2.SkipValidation = false;
            this.txt_Pol_Add2.TabIndex = 12;
            this.txt_Pol_Add2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pol_Add1
            // 
            this.txt_Pol_Add1.AllowSpace = true;
            this.txt_Pol_Add1.AssociatedLookUpName = "";
            this.txt_Pol_Add1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Add1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Add1.ContinuationTextBox = null;
            this.txt_Pol_Add1.CustomEnabled = true;
            this.txt_Pol_Add1.DataFieldMapping = "POL_ADD1";
            this.txt_Pol_Add1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Add1.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Add1.IsDate = false;
            this.txt_Pol_Add1.IsRequired = true;
            this.txt_Pol_Add1.Location = new System.Drawing.Point(201, 283);
            this.txt_Pol_Add1.MaxLength = 35;
            this.txt_Pol_Add1.Name = "txt_Pol_Add1";
            this.txt_Pol_Add1.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Add1.Postfix = "";
            this.txt_Pol_Add1.Prefix = "";
            this.txt_Pol_Add1.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Add1.SkipValidation = false;
            this.txt_Pol_Add1.TabIndex = 11;
            this.txt_Pol_Add1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pol_Com_Name2
            // 
            this.txt_Pol_Com_Name2.AllowSpace = true;
            this.txt_Pol_Com_Name2.AssociatedLookUpName = "";
            this.txt_Pol_Com_Name2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Com_Name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Com_Name2.ContinuationTextBox = null;
            this.txt_Pol_Com_Name2.CustomEnabled = true;
            this.txt_Pol_Com_Name2.DataFieldMapping = "POL_COMP_NAME2";
            this.txt_Pol_Com_Name2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Com_Name2.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Com_Name2.IsDate = false;
            this.txt_Pol_Com_Name2.Location = new System.Drawing.Point(201, 259);
            this.txt_Pol_Com_Name2.MaxLength = 35;
            this.txt_Pol_Com_Name2.Name = "txt_Pol_Com_Name2";
            this.txt_Pol_Com_Name2.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Com_Name2.Postfix = "";
            this.txt_Pol_Com_Name2.Prefix = "";
            this.txt_Pol_Com_Name2.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Com_Name2.SkipValidation = false;
            this.txt_Pol_Com_Name2.TabIndex = 10;
            this.txt_Pol_Com_Name2.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pol_Com_Name1
            // 
            this.txt_Pol_Com_Name1.AllowSpace = true;
            this.txt_Pol_Com_Name1.AssociatedLookUpName = "";
            this.txt_Pol_Com_Name1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Com_Name1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Com_Name1.ContinuationTextBox = null;
            this.txt_Pol_Com_Name1.CustomEnabled = true;
            this.txt_Pol_Com_Name1.DataFieldMapping = "POL_COMP_NAME1";
            this.txt_Pol_Com_Name1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Com_Name1.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Com_Name1.IsDate = false;
            this.txt_Pol_Com_Name1.IsRequired = true;
            this.txt_Pol_Com_Name1.Location = new System.Drawing.Point(201, 235);
            this.txt_Pol_Com_Name1.MaxLength = 35;
            this.txt_Pol_Com_Name1.Name = "txt_Pol_Com_Name1";
            this.txt_Pol_Com_Name1.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Com_Name1.Postfix = "";
            this.txt_Pol_Com_Name1.Prefix = "";
            this.txt_Pol_Com_Name1.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Com_Name1.SkipValidation = false;
            this.txt_Pol_Com_Name1.TabIndex = 9;
            this.txt_Pol_Com_Name1.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pol_Contact_Per
            // 
            this.txt_Pol_Contact_Per.AllowSpace = true;
            this.txt_Pol_Contact_Per.AssociatedLookUpName = "";
            this.txt_Pol_Contact_Per.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Contact_Per.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Contact_Per.ContinuationTextBox = null;
            this.txt_Pol_Contact_Per.CustomEnabled = true;
            this.txt_Pol_Contact_Per.DataFieldMapping = "POL_CONTACT_PER";
            this.txt_Pol_Contact_Per.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Contact_Per.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Contact_Per.IsDate = false;
            this.txt_Pol_Contact_Per.IsRequired = true;
            this.txt_Pol_Contact_Per.Location = new System.Drawing.Point(201, 211);
            this.txt_Pol_Contact_Per.MaxLength = 35;
            this.txt_Pol_Contact_Per.Name = "txt_Pol_Contact_Per";
            this.txt_Pol_Contact_Per.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Contact_Per.Postfix = "";
            this.txt_Pol_Contact_Per.Prefix = "";
            this.txt_Pol_Contact_Per.Size = new System.Drawing.Size(259, 20);
            this.txt_Pol_Contact_Per.SkipValidation = false;
            this.txt_Pol_Contact_Per.TabIndex = 8;
            this.txt_Pol_Contact_Per.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(293, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = " / ";
            // 
            // txt_Pol_Ctt_Off_Description
            // 
            this.txt_Pol_Ctt_Off_Description.AllowSpace = true;
            this.txt_Pol_Ctt_Off_Description.AssociatedLookUpName = "";
            this.txt_Pol_Ctt_Off_Description.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Ctt_Off_Description.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Ctt_Off_Description.ContinuationTextBox = null;
            this.txt_Pol_Ctt_Off_Description.CustomEnabled = true;
            this.txt_Pol_Ctt_Off_Description.DataFieldMapping = "Pol_Ctt_Off_Description";
            this.txt_Pol_Ctt_Off_Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Ctt_Off_Description.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Ctt_Off_Description.IsDate = false;
            this.txt_Pol_Ctt_Off_Description.Location = new System.Drawing.Point(242, 163);
            this.txt_Pol_Ctt_Off_Description.Name = "txt_Pol_Ctt_Off_Description";
            this.txt_Pol_Ctt_Off_Description.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Ctt_Off_Description.Postfix = "";
            this.txt_Pol_Ctt_Off_Description.Prefix = "";
            this.txt_Pol_Ctt_Off_Description.ReadOnly = true;
            this.txt_Pol_Ctt_Off_Description.Size = new System.Drawing.Size(164, 20);
            this.txt_Pol_Ctt_Off_Description.SkipValidation = false;
            this.txt_Pol_Ctt_Off_Description.TabIndex = 7;
            this.txt_Pol_Ctt_Off_Description.TabStop = false;
            this.txt_Pol_Ctt_Off_Description.TextType = CrplControlLibrary.TextType.String;
            // 
            // txt_Pol_Ctt_Off
            // 
            this.txt_Pol_Ctt_Off.AllowSpace = true;
            this.txt_Pol_Ctt_Off.AssociatedLookUpName = "";
            this.txt_Pol_Ctt_Off.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Ctt_Off.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Ctt_Off.ContinuationTextBox = null;
            this.txt_Pol_Ctt_Off.CustomEnabled = true;
            this.txt_Pol_Ctt_Off.DataFieldMapping = "POL_CTT_OFF";
            this.txt_Pol_Ctt_Off.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Ctt_Off.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Ctt_Off.IsDate = false;
            this.txt_Pol_Ctt_Off.Location = new System.Drawing.Point(201, 163);
            this.txt_Pol_Ctt_Off.MaxLength = 1;
            this.txt_Pol_Ctt_Off.Name = "txt_Pol_Ctt_Off";
            this.txt_Pol_Ctt_Off.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Ctt_Off.Postfix = "";
            this.txt_Pol_Ctt_Off.Prefix = "";
            this.txt_Pol_Ctt_Off.Size = new System.Drawing.Size(33, 20);
            this.txt_Pol_Ctt_Off.SkipValidation = false;
            this.txt_Pol_Ctt_Off.TabIndex = 5;
            this.txt_Pol_Ctt_Off.TextType = CrplControlLibrary.TextType.String;
            this.txt_Pol_Ctt_Off.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_Ctt_Off_Validating);
            // 
            // txt_Pol_Policy_Type
            // 
            this.txt_Pol_Policy_Type.AllowSpace = true;
            this.txt_Pol_Policy_Type.AssociatedLookUpName = "";
            this.txt_Pol_Policy_Type.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Policy_Type.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Policy_Type.ContinuationTextBox = null;
            this.txt_Pol_Policy_Type.CustomEnabled = true;
            this.txt_Pol_Policy_Type.DataFieldMapping = "POL_POLICY_TYPE";
            this.txt_Pol_Policy_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Policy_Type.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Policy_Type.IsDate = false;
            this.txt_Pol_Policy_Type.IsRequired = true;
            this.txt_Pol_Policy_Type.Location = new System.Drawing.Point(201, 139);
            this.txt_Pol_Policy_Type.MaxLength = 25;
            this.txt_Pol_Policy_Type.Name = "txt_Pol_Policy_Type";
            this.txt_Pol_Policy_Type.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Policy_Type.Postfix = "";
            this.txt_Pol_Policy_Type.Prefix = "";
            this.txt_Pol_Policy_Type.Size = new System.Drawing.Size(205, 20);
            this.txt_Pol_Policy_Type.SkipValidation = false;
            this.txt_Pol_Policy_Type.TabIndex = 4;
            this.txt_Pol_Policy_Type.TextType = CrplControlLibrary.TextType.String;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(7, 334);
            this.label12.MaximumSize = new System.Drawing.Size(200, 15);
            this.label12.MinimumSize = new System.Drawing.Size(0, 15);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(189, 15);
            this.label12.TabIndex = 16;
            this.label12.Text = ":";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(7, 310);
            this.label11.MaximumSize = new System.Drawing.Size(200, 15);
            this.label11.MinimumSize = new System.Drawing.Size(0, 15);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(189, 15);
            this.label11.TabIndex = 15;
            this.label11.Text = ":";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 262);
            this.label10.MaximumSize = new System.Drawing.Size(200, 15);
            this.label10.MinimumSize = new System.Drawing.Size(0, 15);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(189, 15);
            this.label10.TabIndex = 14;
            this.label10.Text = ":";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 286);
            this.label9.MaximumSize = new System.Drawing.Size(200, 15);
            this.label9.MinimumSize = new System.Drawing.Size(0, 15);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(189, 15);
            this.label9.TabIndex = 13;
            this.label9.Text = "Address  :";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 238);
            this.label8.MaximumSize = new System.Drawing.Size(200, 15);
            this.label8.MinimumSize = new System.Drawing.Size(0, 15);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(189, 15);
            this.label8.TabIndex = 12;
            this.label8.Text = "Company Name  :";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 214);
            this.label7.MaximumSize = new System.Drawing.Size(200, 15);
            this.label7.MinimumSize = new System.Drawing.Size(0, 15);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(189, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "Contact Person  :";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 190);
            this.label6.MaximumSize = new System.Drawing.Size(200, 15);
            this.label6.MinimumSize = new System.Drawing.Size(0, 15);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(189, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "From / To  :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 166);
            this.label5.MaximumSize = new System.Drawing.Size(200, 15);
            this.label5.MinimumSize = new System.Drawing.Size(0, 15);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(189, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "[C]lerical / [O]fficer  :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 142);
            this.label4.MaximumSize = new System.Drawing.Size(200, 15);
            this.label4.MinimumSize = new System.Drawing.Size(0, 15);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(189, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Policy Type  :";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 97);
            this.label3.MaximumSize = new System.Drawing.Size(200, 15);
            this.label3.MinimumSize = new System.Drawing.Size(0, 15);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(189, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Policy No.  :";
            // 
            // txt_W_Policy
            // 
            this.txt_W_Policy.AllowSpace = true;
            this.txt_W_Policy.AssociatedLookUpName = "lbtnPolicyNo";
            this.txt_W_Policy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Policy.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Policy.ContinuationTextBox = null;
            this.txt_W_Policy.CustomEnabled = true;
            this.txt_W_Policy.DataFieldMapping = "POL_POLICY_NO";
            this.txt_W_Policy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Policy.GetRecordsOnUpDownKeys = false;
            this.txt_W_Policy.IsDate = false;
            this.txt_W_Policy.IsRequired = true;
            this.txt_W_Policy.Location = new System.Drawing.Point(201, 94);
            this.txt_W_Policy.MaxLength = 25;
            this.txt_W_Policy.Name = "txt_W_Policy";
            this.txt_W_Policy.NumberFormat = "###,###,##0.00";
            this.txt_W_Policy.Postfix = "";
            this.txt_W_Policy.Prefix = "";
            this.txt_W_Policy.Size = new System.Drawing.Size(205, 20);
            this.txt_W_Policy.SkipValidation = false;
            this.txt_W_Policy.TabIndex = 0;
            this.txt_W_Policy.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Policy.Leave += new System.EventHandler(this.txt_W_Policy_Leave);
            this.txt_W_Policy.Validating += new System.ComponentModel.CancelEventHandler(this.txt_W_Policy_Validating);
            // 
            // txt_W_User1
            // 
            this.txt_W_User1.AllowSpace = true;
            this.txt_W_User1.AssociatedLookUpName = "";
            this.txt_W_User1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_User1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_User1.ContinuationTextBox = null;
            this.txt_W_User1.CustomEnabled = true;
            this.txt_W_User1.DataFieldMapping = "";
            this.txt_W_User1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_User1.GetRecordsOnUpDownKeys = false;
            this.txt_W_User1.IsDate = false;
            this.txt_W_User1.Location = new System.Drawing.Point(74, 22);
            this.txt_W_User1.Name = "txt_W_User1";
            this.txt_W_User1.NumberFormat = "###,###,##0.00";
            this.txt_W_User1.Postfix = "";
            this.txt_W_User1.Prefix = "";
            this.txt_W_User1.Size = new System.Drawing.Size(59, 20);
            this.txt_W_User1.SkipValidation = false;
            this.txt_W_User1.TabIndex = 5;
            this.txt_W_User1.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_User1.Visible = false;
            // 
            // txt_W_Loc1
            // 
            this.txt_W_Loc1.AllowSpace = true;
            this.txt_W_Loc1.AssociatedLookUpName = "";
            this.txt_W_Loc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Loc1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Loc1.ContinuationTextBox = null;
            this.txt_W_Loc1.CustomEnabled = true;
            this.txt_W_Loc1.DataFieldMapping = "";
            this.txt_W_Loc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Loc1.GetRecordsOnUpDownKeys = false;
            this.txt_W_Loc1.IsDate = false;
            this.txt_W_Loc1.Location = new System.Drawing.Point(74, 48);
            this.txt_W_Loc1.Name = "txt_W_Loc1";
            this.txt_W_Loc1.NumberFormat = "###,###,##0.00";
            this.txt_W_Loc1.Postfix = "";
            this.txt_W_Loc1.Prefix = "";
            this.txt_W_Loc1.Size = new System.Drawing.Size(59, 20);
            this.txt_W_Loc1.SkipValidation = false;
            this.txt_W_Loc1.TabIndex = 4;
            this.txt_W_Loc1.TextType = CrplControlLibrary.TextType.String;
            this.txt_W_Loc1.Visible = false;
            // 
            // txt_W_Opt_Desc
            // 
            this.txt_W_Opt_Desc.AllowSpace = true;
            this.txt_W_Opt_Desc.AssociatedLookUpName = "";
            this.txt_W_Opt_Desc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_W_Opt_Desc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_W_Opt_Desc.ContinuationTextBox = null;
            this.txt_W_Opt_Desc.CustomEnabled = true;
            this.txt_W_Opt_Desc.DataFieldMapping = "";
            this.txt_W_Opt_Desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_W_Opt_Desc.GetRecordsOnUpDownKeys = false;
            this.txt_W_Opt_Desc.IsDate = false;
            this.txt_W_Opt_Desc.Location = new System.Drawing.Point(160, 47);
            this.txt_W_Opt_Desc.Name = "txt_W_Opt_Desc";
            this.txt_W_Opt_Desc.NumberFormat = "###,###,##0.00";
            this.txt_W_Opt_Desc.Postfix = "";
            this.txt_W_Opt_Desc.Prefix = "";
            this.txt_W_Opt_Desc.Size = new System.Drawing.Size(414, 20);
            this.txt_W_Opt_Desc.SkipValidation = false;
            this.txt_W_Opt_Desc.TabIndex = 0;
            this.txt_W_Opt_Desc.TabStop = false;
            this.txt_W_Opt_Desc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(474, 20);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.Size = new System.Drawing.Size(100, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 2;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(426, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(223, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SET UP ENTRY";
            // 
            // pnlPage6
            // 
            this.pnlPage6.ConcurrentPanels = null;
            this.pnlPage6.Controls.Add(this.txt_Pol_No_Depen);
            this.pnlPage6.Controls.Add(this.label24);
            this.pnlPage6.Controls.Add(this.label23);
            this.pnlPage6.Controls.Add(this.label25);
            this.pnlPage6.Controls.Add(this.label22);
            this.pnlPage6.Controls.Add(this.txt_Pol_Omi_Limit);
            this.pnlPage6.Controls.Add(this.txt_Pol_Per_Depen);
            this.pnlPage6.Controls.Add(this.txt_Pol_Omi_Self);
            this.pnlPage6.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPage6.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPage6.DependentPanels = null;
            this.pnlPage6.DisableDependentLoad = false;
            this.pnlPage6.EnableDelete = true;
            this.pnlPage6.EnableInsert = true;
            this.pnlPage6.EnableQuery = false;
            this.pnlPage6.EnableUpdate = true;
            this.pnlPage6.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PolicyInfoCommand";
            this.pnlPage6.Location = new System.Drawing.Point(10, 460);
            this.pnlPage6.MasterPanel = null;
            this.pnlPage6.Name = "pnlPage6";
            this.pnlPage6.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPage6.Size = new System.Drawing.Size(320, 97);
            this.pnlPage6.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.pnlPage6.TabIndex = 39;
            // 
            // txt_Pol_No_Depen
            // 
            this.txt_Pol_No_Depen.AllowSpace = true;
            this.txt_Pol_No_Depen.AssociatedLookUpName = "";
            this.txt_Pol_No_Depen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_No_Depen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_No_Depen.ContinuationTextBox = null;
            this.txt_Pol_No_Depen.CustomEnabled = true;
            this.txt_Pol_No_Depen.DataFieldMapping = "POL_NO_DEPEN";
            this.txt_Pol_No_Depen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_No_Depen.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_No_Depen.IsDate = false;
            this.txt_Pol_No_Depen.IsRequired = true;
            this.txt_Pol_No_Depen.Location = new System.Drawing.Point(192, 71);
            this.txt_Pol_No_Depen.MaxLength = 2;
            this.txt_Pol_No_Depen.Name = "txt_Pol_No_Depen";
            this.txt_Pol_No_Depen.NumberFormat = "###,###,##0.00";
            this.txt_Pol_No_Depen.Postfix = "";
            this.txt_Pol_No_Depen.Prefix = "";
            this.txt_Pol_No_Depen.Size = new System.Drawing.Size(40, 20);
            this.txt_Pol_No_Depen.SkipValidation = false;
            this.txt_Pol_No_Depen.TabIndex = 3;
            this.txt_Pol_No_Depen.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_Pol_No_Depen.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_No_Depen_Validating);
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(2, 5);
            this.label24.MaximumSize = new System.Drawing.Size(200, 15);
            this.label24.MinimumSize = new System.Drawing.Size(0, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(186, 15);
            this.label24.TabIndex = 33;
            this.label24.Text = "Maximum Limit  :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1, 28);
            this.label23.MaximumSize = new System.Drawing.Size(200, 15);
            this.label23.MinimumSize = new System.Drawing.Size(0, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(186, 15);
            this.label23.TabIndex = 34;
            this.label23.Text = "Self  :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1, 74);
            this.label25.MaximumSize = new System.Drawing.Size(200, 15);
            this.label25.MinimumSize = new System.Drawing.Size(0, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(187, 15);
            this.label25.TabIndex = 38;
            this.label25.Text = "Maximum Dependent Covered  :";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(2, 51);
            this.label22.MaximumSize = new System.Drawing.Size(200, 15);
            this.label22.MinimumSize = new System.Drawing.Size(0, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(185, 15);
            this.label22.TabIndex = 35;
            this.label22.Text = "Per Dependent Amount  :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Pol_Omi_Limit
            // 
            this.txt_Pol_Omi_Limit.AllowSpace = true;
            this.txt_Pol_Omi_Limit.AssociatedLookUpName = "";
            this.txt_Pol_Omi_Limit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Omi_Limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Omi_Limit.ContinuationTextBox = null;
            this.txt_Pol_Omi_Limit.CustomEnabled = true;
            this.txt_Pol_Omi_Limit.DataFieldMapping = "POL_OMI_LIMIT";
            this.txt_Pol_Omi_Limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Omi_Limit.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Omi_Limit.IsDate = false;
            this.txt_Pol_Omi_Limit.IsRequired = true;
            this.txt_Pol_Omi_Limit.Location = new System.Drawing.Point(192, 2);
            this.txt_Pol_Omi_Limit.MaxLength = 7;
            this.txt_Pol_Omi_Limit.Name = "txt_Pol_Omi_Limit";
            this.txt_Pol_Omi_Limit.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Omi_Limit.Postfix = "";
            this.txt_Pol_Omi_Limit.Prefix = "";
            this.txt_Pol_Omi_Limit.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Omi_Limit.SkipValidation = false;
            this.txt_Pol_Omi_Limit.TabIndex = 0;
            this.txt_Pol_Omi_Limit.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_Pol_Per_Depen
            // 
            this.txt_Pol_Per_Depen.AllowSpace = true;
            this.txt_Pol_Per_Depen.AssociatedLookUpName = "";
            this.txt_Pol_Per_Depen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Per_Depen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Per_Depen.ContinuationTextBox = null;
            this.txt_Pol_Per_Depen.CustomEnabled = true;
            this.txt_Pol_Per_Depen.DataFieldMapping = "POL_PER_DEPEN";
            this.txt_Pol_Per_Depen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Per_Depen.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Per_Depen.IsDate = false;
            this.txt_Pol_Per_Depen.IsRequired = true;
            this.txt_Pol_Per_Depen.Location = new System.Drawing.Point(192, 48);
            this.txt_Pol_Per_Depen.MaxLength = 7;
            this.txt_Pol_Per_Depen.Name = "txt_Pol_Per_Depen";
            this.txt_Pol_Per_Depen.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Per_Depen.Postfix = "";
            this.txt_Pol_Per_Depen.Prefix = "";
            this.txt_Pol_Per_Depen.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Per_Depen.SkipValidation = false;
            this.txt_Pol_Per_Depen.TabIndex = 2;
            this.txt_Pol_Per_Depen.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_Pol_Omi_Self
            // 
            this.txt_Pol_Omi_Self.AllowSpace = true;
            this.txt_Pol_Omi_Self.AssociatedLookUpName = "";
            this.txt_Pol_Omi_Self.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Omi_Self.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Omi_Self.ContinuationTextBox = null;
            this.txt_Pol_Omi_Self.CustomEnabled = true;
            this.txt_Pol_Omi_Self.DataFieldMapping = "POL_OMI_SELF";
            this.txt_Pol_Omi_Self.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Omi_Self.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Omi_Self.IsDate = false;
            this.txt_Pol_Omi_Self.IsRequired = true;
            this.txt_Pol_Omi_Self.Location = new System.Drawing.Point(192, 25);
            this.txt_Pol_Omi_Self.MaxLength = 7;
            this.txt_Pol_Omi_Self.Name = "txt_Pol_Omi_Self";
            this.txt_Pol_Omi_Self.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Omi_Self.Postfix = "";
            this.txt_Pol_Omi_Self.Prefix = "";
            this.txt_Pol_Omi_Self.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Omi_Self.SkipValidation = false;
            this.txt_Pol_Omi_Self.TabIndex = 1;
            this.txt_Pol_Omi_Self.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // pnlPage5
            // 
            this.pnlPage5.ConcurrentPanels = null;
            this.pnlPage5.Controls.Add(this.txt_Pol_Room_Chg);
            this.pnlPage5.Controls.Add(this.txt_Pol_Delv_Limit);
            this.pnlPage5.Controls.Add(this.txt_Pol_Ailment);
            this.pnlPage5.Controls.Add(this.label19);
            this.pnlPage5.Controls.Add(this.label20);
            this.pnlPage5.Controls.Add(this.label21);
            this.pnlPage5.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPage5.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPage5.DependentPanels = null;
            this.pnlPage5.DisableDependentLoad = false;
            this.pnlPage5.EnableDelete = true;
            this.pnlPage5.EnableInsert = true;
            this.pnlPage5.EnableQuery = false;
            this.pnlPage5.EnableUpdate = true;
            this.pnlPage5.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PolicyInfoCommand";
            this.pnlPage5.Location = new System.Drawing.Point(7, 460);
            this.pnlPage5.MasterPanel = null;
            this.pnlPage5.Name = "pnlPage5";
            this.pnlPage5.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPage5.Size = new System.Drawing.Size(298, 84);
            this.pnlPage5.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.pnlPage5.TabIndex = 38;
            // 
            // txt_Pol_Room_Chg
            // 
            this.txt_Pol_Room_Chg.AllowSpace = true;
            this.txt_Pol_Room_Chg.AssociatedLookUpName = "";
            this.txt_Pol_Room_Chg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Room_Chg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Room_Chg.ContinuationTextBox = null;
            this.txt_Pol_Room_Chg.CustomEnabled = true;
            this.txt_Pol_Room_Chg.DataFieldMapping = "POL_ROOM_CHG";
            this.txt_Pol_Room_Chg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Room_Chg.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Room_Chg.IsDate = false;
            this.txt_Pol_Room_Chg.IsRequired = true;
            this.txt_Pol_Room_Chg.Location = new System.Drawing.Point(196, 50);
            this.txt_Pol_Room_Chg.MaxLength = 7;
            this.txt_Pol_Room_Chg.Name = "txt_Pol_Room_Chg";
            this.txt_Pol_Room_Chg.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Room_Chg.Postfix = "";
            this.txt_Pol_Room_Chg.Prefix = "";
            this.txt_Pol_Room_Chg.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Room_Chg.SkipValidation = false;
            this.txt_Pol_Room_Chg.TabIndex = 2;
            this.txt_Pol_Room_Chg.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_Pol_Room_Chg.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_Room_Chg_Validating);
            // 
            // txt_Pol_Delv_Limit
            // 
            this.txt_Pol_Delv_Limit.AllowSpace = true;
            this.txt_Pol_Delv_Limit.AssociatedLookUpName = "";
            this.txt_Pol_Delv_Limit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Delv_Limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Delv_Limit.ContinuationTextBox = null;
            this.txt_Pol_Delv_Limit.CustomEnabled = true;
            this.txt_Pol_Delv_Limit.DataFieldMapping = "POL_DELV_LIMIT";
            this.txt_Pol_Delv_Limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Delv_Limit.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Delv_Limit.IsDate = false;
            this.txt_Pol_Delv_Limit.IsRequired = true;
            this.txt_Pol_Delv_Limit.Location = new System.Drawing.Point(196, 26);
            this.txt_Pol_Delv_Limit.MaxLength = 7;
            this.txt_Pol_Delv_Limit.Name = "txt_Pol_Delv_Limit";
            this.txt_Pol_Delv_Limit.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Delv_Limit.Postfix = "";
            this.txt_Pol_Delv_Limit.Prefix = "";
            this.txt_Pol_Delv_Limit.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Delv_Limit.SkipValidation = false;
            this.txt_Pol_Delv_Limit.TabIndex = 1;
            this.txt_Pol_Delv_Limit.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_Pol_Ailment
            // 
            this.txt_Pol_Ailment.AllowSpace = true;
            this.txt_Pol_Ailment.AssociatedLookUpName = "";
            this.txt_Pol_Ailment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Ailment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Ailment.ContinuationTextBox = null;
            this.txt_Pol_Ailment.CustomEnabled = true;
            this.txt_Pol_Ailment.DataFieldMapping = "POL_AILMENT";
            this.txt_Pol_Ailment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Ailment.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Ailment.IsDate = false;
            this.txt_Pol_Ailment.IsRequired = true;
            this.txt_Pol_Ailment.Location = new System.Drawing.Point(196, 2);
            this.txt_Pol_Ailment.MaxLength = 7;
            this.txt_Pol_Ailment.Name = "txt_Pol_Ailment";
            this.txt_Pol_Ailment.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Ailment.Postfix = "";
            this.txt_Pol_Ailment.Prefix = "";
            this.txt_Pol_Ailment.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Ailment.SkipValidation = false;
            this.txt_Pol_Ailment.TabIndex = 0;
            this.txt_Pol_Ailment.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 50);
            this.label19.MaximumSize = new System.Drawing.Size(200, 15);
            this.label19.MinimumSize = new System.Drawing.Size(0, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(187, 15);
            this.label19.TabIndex = 35;
            this.label19.Text = "Room Charge Per Day  :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 28);
            this.label20.MaximumSize = new System.Drawing.Size(200, 15);
            this.label20.MinimumSize = new System.Drawing.Size(0, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(187, 15);
            this.label20.TabIndex = 34;
            this.label20.Text = "For Normal Delivery Max Limit  :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(5, 5);
            this.label21.MaximumSize = new System.Drawing.Size(200, 15);
            this.label21.MinimumSize = new System.Drawing.Size(0, 15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(185, 15);
            this.label21.TabIndex = 33;
            this.label21.Text = "Maximum Limit Per Ailmont  :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPage3
            // 
            this.pnlPage3.ConcurrentPanels = null;
            this.pnlPage3.Controls.Add(this.txt_Pol_Max_Acc);
            this.pnlPage3.Controls.Add(this.txt_Pol_Max_Limit);
            this.pnlPage3.Controls.Add(this.txt_Pol_Cov_Multi);
            this.pnlPage3.Controls.Add(this.label18);
            this.pnlPage3.Controls.Add(this.label17);
            this.pnlPage3.Controls.Add(this.label16);
            this.pnlPage3.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPage3.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPage3.DependentPanels = null;
            this.pnlPage3.DisableDependentLoad = false;
            this.pnlPage3.EnableDelete = true;
            this.pnlPage3.EnableInsert = true;
            this.pnlPage3.EnableQuery = false;
            this.pnlPage3.EnableUpdate = true;
            this.pnlPage3.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PolicyInfoCommand";
            this.pnlPage3.Location = new System.Drawing.Point(3, 460);
            this.pnlPage3.MasterPanel = null;
            this.pnlPage3.Name = "pnlPage3";
            this.pnlPage3.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPage3.Size = new System.Drawing.Size(298, 78);
            this.pnlPage3.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.pnlPage3.TabIndex = 11;
            // 
            // txt_Pol_Max_Acc
            // 
            this.txt_Pol_Max_Acc.AllowSpace = true;
            this.txt_Pol_Max_Acc.AssociatedLookUpName = "";
            this.txt_Pol_Max_Acc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Max_Acc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Max_Acc.ContinuationTextBox = null;
            this.txt_Pol_Max_Acc.CustomEnabled = true;
            this.txt_Pol_Max_Acc.DataFieldMapping = "POL_MAX_ACC";
            this.txt_Pol_Max_Acc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Max_Acc.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Max_Acc.IsDate = false;
            this.txt_Pol_Max_Acc.IsRequired = true;
            this.txt_Pol_Max_Acc.Location = new System.Drawing.Point(200, 50);
            this.txt_Pol_Max_Acc.MaxLength = 2;
            this.txt_Pol_Max_Acc.Name = "txt_Pol_Max_Acc";
            this.txt_Pol_Max_Acc.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Max_Acc.Postfix = "";
            this.txt_Pol_Max_Acc.Prefix = "";
            this.txt_Pol_Max_Acc.Size = new System.Drawing.Size(45, 20);
            this.txt_Pol_Max_Acc.SkipValidation = false;
            this.txt_Pol_Max_Acc.TabIndex = 2;
            this.txt_Pol_Max_Acc.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_Pol_Max_Acc.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_Max_Acc_Validating);
            // 
            // txt_Pol_Max_Limit
            // 
            this.txt_Pol_Max_Limit.AllowSpace = true;
            this.txt_Pol_Max_Limit.AssociatedLookUpName = "";
            this.txt_Pol_Max_Limit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Max_Limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Max_Limit.ContinuationTextBox = null;
            this.txt_Pol_Max_Limit.CustomEnabled = true;
            this.txt_Pol_Max_Limit.DataFieldMapping = "POL_MAX_LIMIT";
            this.txt_Pol_Max_Limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Max_Limit.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Max_Limit.IsDate = false;
            this.txt_Pol_Max_Limit.IsRequired = true;
            this.txt_Pol_Max_Limit.Location = new System.Drawing.Point(200, 26);
            this.txt_Pol_Max_Limit.MaxLength = 7;
            this.txt_Pol_Max_Limit.Name = "txt_Pol_Max_Limit";
            this.txt_Pol_Max_Limit.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Max_Limit.Postfix = "";
            this.txt_Pol_Max_Limit.Prefix = "";
            this.txt_Pol_Max_Limit.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Max_Limit.SkipValidation = false;
            this.txt_Pol_Max_Limit.TabIndex = 1;
            this.txt_Pol_Max_Limit.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // txt_Pol_Cov_Multi
            // 
            this.txt_Pol_Cov_Multi.AllowSpace = true;
            this.txt_Pol_Cov_Multi.AssociatedLookUpName = "";
            this.txt_Pol_Cov_Multi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Cov_Multi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Cov_Multi.ContinuationTextBox = null;
            this.txt_Pol_Cov_Multi.CustomEnabled = true;
            this.txt_Pol_Cov_Multi.DataFieldMapping = "POL_COV_MULTI";
            this.txt_Pol_Cov_Multi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Cov_Multi.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Cov_Multi.IsDate = false;
            this.txt_Pol_Cov_Multi.IsRequired = true;
            this.txt_Pol_Cov_Multi.Location = new System.Drawing.Point(200, 2);
            this.txt_Pol_Cov_Multi.MaxLength = 2;
            this.txt_Pol_Cov_Multi.Name = "txt_Pol_Cov_Multi";
            this.txt_Pol_Cov_Multi.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Cov_Multi.Postfix = "";
            this.txt_Pol_Cov_Multi.Prefix = "";
            this.txt_Pol_Cov_Multi.Size = new System.Drawing.Size(45, 20);
            this.txt_Pol_Cov_Multi.SkipValidation = false;
            this.txt_Pol_Cov_Multi.TabIndex = 0;
            this.txt_Pol_Cov_Multi.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 51);
            this.label18.MaximumSize = new System.Drawing.Size(200, 15);
            this.label18.MinimumSize = new System.Drawing.Size(0, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(192, 15);
            this.label18.TabIndex = 35;
            this.label18.Text = "Minimum Limit Of Accidental  :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1, 27);
            this.label17.MaximumSize = new System.Drawing.Size(200, 15);
            this.label17.MinimumSize = new System.Drawing.Size(0, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(194, 15);
            this.label17.TabIndex = 34;
            this.label17.Text = "Minimum Limit Of Life Coverage :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(2, 2);
            this.label16.MaximumSize = new System.Drawing.Size(200, 15);
            this.label16.MinimumSize = new System.Drawing.Size(0, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(192, 15);
            this.label16.TabIndex = 33;
            this.label16.Text = "Coverage (ASR multiples)  :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPage4
            // 
            this.pnlPage4.ConcurrentPanels = null;
            this.pnlPage4.Controls.Add(this.txt_Pol_Ctt_Acc);
            this.pnlPage4.Controls.Add(this.txt_Pol_Ctt_Cov);
            this.pnlPage4.Controls.Add(this.label27);
            this.pnlPage4.Controls.Add(this.label28);
            this.pnlPage4.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlPage4.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPage4.DependentPanels = null;
            this.pnlPage4.DisableDependentLoad = false;
            this.pnlPage4.EnableDelete = true;
            this.pnlPage4.EnableInsert = true;
            this.pnlPage4.EnableQuery = false;
            this.pnlPage4.EnableUpdate = true;
            this.pnlPage4.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PolicyInfoCommand";
            this.pnlPage4.Location = new System.Drawing.Point(-2, 463);
            this.pnlPage4.MasterPanel = null;
            this.pnlPage4.Name = "pnlPage4";
            this.pnlPage4.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPage4.Size = new System.Drawing.Size(298, 54);
            this.pnlPage4.SPName = "CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER";
            this.pnlPage4.TabIndex = 36;
            // 
            // txt_Pol_Ctt_Acc
            // 
            this.txt_Pol_Ctt_Acc.AllowSpace = true;
            this.txt_Pol_Ctt_Acc.AssociatedLookUpName = "";
            this.txt_Pol_Ctt_Acc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Ctt_Acc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Ctt_Acc.ContinuationTextBox = null;
            this.txt_Pol_Ctt_Acc.CustomEnabled = true;
            this.txt_Pol_Ctt_Acc.DataFieldMapping = "POL_CTT_ACC";
            this.txt_Pol_Ctt_Acc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Ctt_Acc.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Ctt_Acc.IsDate = false;
            this.txt_Pol_Ctt_Acc.IsRequired = true;
            this.txt_Pol_Ctt_Acc.Location = new System.Drawing.Point(204, 25);
            this.txt_Pol_Ctt_Acc.MaxLength = 7;
            this.txt_Pol_Ctt_Acc.Name = "txt_Pol_Ctt_Acc";
            this.txt_Pol_Ctt_Acc.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Ctt_Acc.Postfix = "";
            this.txt_Pol_Ctt_Acc.Prefix = "";
            this.txt_Pol_Ctt_Acc.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Ctt_Acc.SkipValidation = false;
            this.txt_Pol_Ctt_Acc.TabIndex = 1;
            this.txt_Pol_Ctt_Acc.TextType = CrplControlLibrary.TextType.Integer;
            this.txt_Pol_Ctt_Acc.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Pol_Ctt_Acc_Validating);
            // 
            // txt_Pol_Ctt_Cov
            // 
            this.txt_Pol_Ctt_Cov.AllowSpace = true;
            this.txt_Pol_Ctt_Cov.AssociatedLookUpName = "";
            this.txt_Pol_Ctt_Cov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Pol_Ctt_Cov.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Pol_Ctt_Cov.ContinuationTextBox = null;
            this.txt_Pol_Ctt_Cov.CustomEnabled = true;
            this.txt_Pol_Ctt_Cov.DataFieldMapping = "POL_CTT_COV";
            this.txt_Pol_Ctt_Cov.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Pol_Ctt_Cov.GetRecordsOnUpDownKeys = false;
            this.txt_Pol_Ctt_Cov.IsDate = false;
            this.txt_Pol_Ctt_Cov.IsRequired = true;
            this.txt_Pol_Ctt_Cov.Location = new System.Drawing.Point(204, 0);
            this.txt_Pol_Ctt_Cov.MaxLength = 7;
            this.txt_Pol_Ctt_Cov.Name = "txt_Pol_Ctt_Cov";
            this.txt_Pol_Ctt_Cov.NumberFormat = "###,###,##0.00";
            this.txt_Pol_Ctt_Cov.Postfix = "";
            this.txt_Pol_Ctt_Cov.Prefix = "";
            this.txt_Pol_Ctt_Cov.Size = new System.Drawing.Size(85, 20);
            this.txt_Pol_Ctt_Cov.SkipValidation = false;
            this.txt_Pol_Ctt_Cov.TabIndex = 0;
            this.txt_Pol_Ctt_Cov.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 28);
            this.label27.MaximumSize = new System.Drawing.Size(200, 15);
            this.label27.MinimumSize = new System.Drawing.Size(0, 3);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(197, 15);
            this.label27.TabIndex = 34;
            this.label27.Text = "Coverage (Accidental) :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 3);
            this.label28.MaximumSize = new System.Drawing.Size(200, 15);
            this.label28.MinimumSize = new System.Drawing.Size(0, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(194, 15);
            this.label28.TabIndex = 33;
            this.label28.Text = "Coverage (Life)  :";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Pol_From
            // 
            this.txt_Pol_From.CustomEnabled = true;
            this.txt_Pol_From.CustomFormat = "dd/MM/yyyy";
            this.txt_Pol_From.DataFieldMapping = "Pol_From";
            this.txt_Pol_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_Pol_From.HasChanges = true;
            this.txt_Pol_From.Location = new System.Drawing.Point(201, 168);
            this.txt_Pol_From.Name = "txt_Pol_From";
            this.txt_Pol_From.NullValue = " ";
            this.txt_Pol_From.Size = new System.Drawing.Size(91, 20);
            this.txt_Pol_From.TabIndex = 42;
            this.txt_Pol_From.Value = new System.DateTime(2011, 4, 11, 0, 0, 0, 0);
            // 
            // CHRIS_MedicalInsurance_PolicyInfo_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 619);
            this.Controls.Add(this.pnlPage5);
            this.Controls.Add(this.pnlPage3);
            this.Controls.Add(this.pnlPage6);
            this.Controls.Add(this.pnlPage4);
            this.Controls.Add(this.pnlPolicyInfoDetail);
            this.CurrentPanelBlock = "pnlPolicyInfoDetail";
            this.Name = "CHRIS_MedicalInsurance_PolicyInfo_Detail";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_MedicalInsurance_PolicyInfo_Detail";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_MedicalInsurance_PolicyInfo_Detail_AfterLOVSelection);
            this.Controls.SetChildIndex(this.pnlPolicyInfoDetail, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlPage4, 0);
            this.Controls.SetChildIndex(this.pnlPage6, 0);
            this.Controls.SetChildIndex(this.pnlPage3, 0);
            this.Controls.SetChildIndex(this.pnlPage5, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlPolicyInfoDetail.ResumeLayout(false);
            this.pnlPolicyInfoDetail.PerformLayout();
            this.pnlPage6.ResumeLayout(false);
            this.pnlPage6.PerformLayout();
            this.pnlPage5.ResumeLayout(false);
            this.pnlPage5.PerformLayout();
            this.pnlPage3.ResumeLayout(false);
            this.pnlPage3.PerformLayout();
            this.pnlPage4.ResumeLayout(false);
            this.pnlPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPolicyInfoDetail;
        private CrplControlLibrary.SLTextBox txt_W_Opt_Desc;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txt_W_User1;
        private CrplControlLibrary.SLTextBox txt_W_Loc1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CrplControlLibrary.SLTextBox txt_W_Policy;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txt_Pol_Ctt_Off_Description;
        private CrplControlLibrary.SLTextBox txt_Pol_Ctt_Off;
        private CrplControlLibrary.SLTextBox txt_Pol_Policy_Type;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox txt_Pol_Add3;
        private CrplControlLibrary.SLTextBox txt_Pol_Add2;
        private CrplControlLibrary.SLTextBox txt_Pol_Add1;
        private CrplControlLibrary.SLTextBox txt_Pol_Com_Name2;
        private CrplControlLibrary.SLTextBox txt_Pol_Com_Name1;
        private CrplControlLibrary.SLTextBox txt_Pol_Contact_Per;
        private CrplControlLibrary.SLTextBox txt_Pol_Segment;
        private CrplControlLibrary.SLTextBox txt_Pol_Branch;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPage3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private CrplControlLibrary.SLTextBox txt_Pol_Max_Acc;
        private CrplControlLibrary.SLTextBox txt_Pol_Max_Limit;
        private CrplControlLibrary.SLTextBox txt_Pol_Cov_Multi;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPage5;
        private CrplControlLibrary.SLTextBox txt_Pol_Room_Chg;
        private CrplControlLibrary.SLTextBox txt_Pol_Delv_Limit;
        private CrplControlLibrary.SLTextBox txt_Pol_Ailment;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPage6;
        private CrplControlLibrary.SLTextBox txt_Pol_Per_Depen;
        private CrplControlLibrary.SLTextBox txt_Pol_Omi_Self;
        private CrplControlLibrary.SLTextBox txt_Pol_Omi_Limit;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private CrplControlLibrary.SLTextBox txt_Pol_No_Depen;
        private System.Windows.Forms.Label label25;
        private CrplControlLibrary.LookupButton lbtnPolicyNo;
        private CrplControlLibrary.SLTextBox txt_W_Opt;
        //private CrplControlLibrary.SLDatePicker txt_Pol_From1;
        private CrplControlLibrary.LookupButton lbtnBranch;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlPage4;
        private CrplControlLibrary.SLTextBox txt_Pol_Ctt_Acc;
        private CrplControlLibrary.SLTextBox txt_Pol_Ctt_Cov;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private CrplControlLibrary.SLDatePicker dtp_Pol_To;
        private CrplControlLibrary.SLDatePicker dtp_Pol_From;
        private CrplControlLibrary.SLDatePicker txt_Pol_From;
        private CrplControlLibrary.SLTextBox txt_Pol_Add4;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
    }
}