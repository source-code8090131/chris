namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    partial class CHRIS_MedicalInsurance_DsgWisOMI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_MedicalInsurance_DsgWisOMI));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.W_YEAR = new CrplControlLibrary.SLTextBox(this.components);
            this.W_LEVEL = new CrplControlLibrary.SLTextBox(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.W_DESIG = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.W_DEPT = new CrplControlLibrary.SLTextBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.W_SEG = new CrplControlLibrary.SLTextBox(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.copies = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.dest_format = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.slButton1 = new CrplControlLibrary.SLButton();
            this.Run = new CrplControlLibrary.SLButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.W_BRN = new CrplControlLibrary.SLTextBox(this.components);
            this.Dest_Name = new CrplControlLibrary.SLTextBox(this.components);
            this.cmbDescType = new CrplControlLibrary.SLComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.W_YEAR);
            this.groupBox1.Controls.Add(this.W_LEVEL);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.W_DESIG);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.W_DEPT);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.W_SEG);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.copies);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dest_format);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.slButton1);
            this.groupBox1.Controls.Add(this.Run);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.W_BRN);
            this.groupBox1.Controls.Add(this.Dest_Name);
            this.groupBox1.Controls.Add(this.cmbDescType);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 367);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 310);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 13);
            this.label12.TabIndex = 96;
            this.label12.Text = "OMI Year (YYYY)";
            // 
            // W_YEAR
            // 
            this.W_YEAR.AllowSpace = true;
            this.W_YEAR.AssociatedLookUpName = "";
            this.W_YEAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_YEAR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_YEAR.ContinuationTextBox = null;
            this.W_YEAR.CustomEnabled = true;
            this.W_YEAR.DataFieldMapping = "";
            this.W_YEAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_YEAR.GetRecordsOnUpDownKeys = false;
            this.W_YEAR.IsDate = false;
            this.W_YEAR.Location = new System.Drawing.Point(170, 308);
            this.W_YEAR.MaxLength = 4;
            this.W_YEAR.Name = "W_YEAR";
            this.W_YEAR.NumberFormat = "###,###,##0.00";
            this.W_YEAR.Postfix = "";
            this.W_YEAR.Prefix = "";
            this.W_YEAR.Size = new System.Drawing.Size(172, 20);
            this.W_YEAR.SkipValidation = false;
            this.W_YEAR.TabIndex = 9;
            this.W_YEAR.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // W_LEVEL
            // 
            this.W_LEVEL.AllowSpace = true;
            this.W_LEVEL.AssociatedLookUpName = "";
            this.W_LEVEL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_LEVEL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_LEVEL.ContinuationTextBox = null;
            this.W_LEVEL.CustomEnabled = true;
            this.W_LEVEL.DataFieldMapping = "";
            this.W_LEVEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_LEVEL.GetRecordsOnUpDownKeys = false;
            this.W_LEVEL.IsDate = false;
            this.W_LEVEL.Location = new System.Drawing.Point(170, 282);
            this.W_LEVEL.MaxLength = 5;
            this.W_LEVEL.Name = "W_LEVEL";
            this.W_LEVEL.NumberFormat = "###,###,##0.00";
            this.W_LEVEL.Postfix = "";
            this.W_LEVEL.Prefix = "";
            this.W_LEVEL.Size = new System.Drawing.Size(172, 20);
            this.W_LEVEL.SkipValidation = false;
            this.W_LEVEL.TabIndex = 8;
            this.W_LEVEL.TextType = CrplControlLibrary.TextType.String;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 284);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 13);
            this.label11.TabIndex = 93;
            this.label11.Text = "Valid Level Or [ALL]";
            // 
            // W_DESIG
            // 
            this.W_DESIG.AllowSpace = true;
            this.W_DESIG.AssociatedLookUpName = "";
            this.W_DESIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_DESIG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_DESIG.ContinuationTextBox = null;
            this.W_DESIG.CustomEnabled = true;
            this.W_DESIG.DataFieldMapping = "";
            this.W_DESIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_DESIG.GetRecordsOnUpDownKeys = false;
            this.W_DESIG.IsDate = false;
            this.W_DESIG.Location = new System.Drawing.Point(170, 256);
            this.W_DESIG.MaxLength = 5;
            this.W_DESIG.Name = "W_DESIG";
            this.W_DESIG.NumberFormat = "###,###,##0.00";
            this.W_DESIG.Postfix = "";
            this.W_DESIG.Prefix = "";
            this.W_DESIG.Size = new System.Drawing.Size(172, 20);
            this.W_DESIG.SkipValidation = false;
            this.W_DESIG.TabIndex = 7;
            this.W_DESIG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 258);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 13);
            this.label10.TabIndex = 91;
            this.label10.Text = "Valid Desig. Or [ALL]";
            // 
            // W_DEPT
            // 
            this.W_DEPT.AllowSpace = true;
            this.W_DEPT.AssociatedLookUpName = "";
            this.W_DEPT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_DEPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_DEPT.ContinuationTextBox = null;
            this.W_DEPT.CustomEnabled = true;
            this.W_DEPT.DataFieldMapping = "";
            this.W_DEPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_DEPT.GetRecordsOnUpDownKeys = false;
            this.W_DEPT.IsDate = false;
            this.W_DEPT.Location = new System.Drawing.Point(170, 230);
            this.W_DEPT.MaxLength = 5;
            this.W_DEPT.Name = "W_DEPT";
            this.W_DEPT.NumberFormat = "###,###,##0.00";
            this.W_DEPT.Postfix = "";
            this.W_DEPT.Prefix = "";
            this.W_DEPT.Size = new System.Drawing.Size(172, 20);
            this.W_DEPT.SkipValidation = false;
            this.W_DEPT.TabIndex = 6;
            this.W_DEPT.TextType = CrplControlLibrary.TextType.String;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 232);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 89;
            this.label7.Text = "Valid Deptt. Or [ALL]";
            // 
            // W_SEG
            // 
            this.W_SEG.AllowSpace = true;
            this.W_SEG.AssociatedLookUpName = "";
            this.W_SEG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_SEG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_SEG.ContinuationTextBox = null;
            this.W_SEG.CustomEnabled = true;
            this.W_SEG.DataFieldMapping = "";
            this.W_SEG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_SEG.GetRecordsOnUpDownKeys = false;
            this.W_SEG.IsDate = false;
            this.W_SEG.Location = new System.Drawing.Point(170, 204);
            this.W_SEG.MaxLength = 3;
            this.W_SEG.Name = "W_SEG";
            this.W_SEG.NumberFormat = "###,###,##0.00";
            this.W_SEG.Postfix = "";
            this.W_SEG.Prefix = "";
            this.W_SEG.Size = new System.Drawing.Size(172, 20);
            this.W_SEG.SkipValidation = false;
            this.W_SEG.TabIndex = 5;
            this.W_SEG.TextType = CrplControlLibrary.TextType.String;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 87;
            this.label6.Text = "Copies";
            // 
            // copies
            // 
            this.copies.AllowSpace = true;
            this.copies.AssociatedLookUpName = "";
            this.copies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copies.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.copies.ContinuationTextBox = null;
            this.copies.CustomEnabled = true;
            this.copies.DataFieldMapping = "";
            this.copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copies.GetRecordsOnUpDownKeys = false;
            this.copies.IsDate = false;
            this.copies.Location = new System.Drawing.Point(170, 152);
            this.copies.MaxLength = 2;
            this.copies.Name = "copies";
            this.copies.NumberFormat = "###,###,##0.00";
            this.copies.Postfix = "";
            this.copies.Prefix = "";
            this.copies.Size = new System.Drawing.Size(172, 20);
            this.copies.SkipValidation = false;
            this.copies.TabIndex = 3;
            this.copies.TextType = CrplControlLibrary.TextType.Integer;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 85;
            this.label2.Text = "Desformat";
            // 
            // dest_format
            // 
            this.dest_format.AllowSpace = true;
            this.dest_format.AssociatedLookUpName = "";
            this.dest_format.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dest_format.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.dest_format.ContinuationTextBox = null;
            this.dest_format.CustomEnabled = true;
            this.dest_format.DataFieldMapping = "";
            this.dest_format.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dest_format.GetRecordsOnUpDownKeys = false;
            this.dest_format.IsDate = false;
            this.dest_format.Location = new System.Drawing.Point(170, 126);
            this.dest_format.MaxLength = 50;
            this.dest_format.Name = "dest_format";
            this.dest_format.NumberFormat = "###,###,##0.00";
            this.dest_format.Postfix = "";
            this.dest_format.Prefix = "";
            this.dest_format.Size = new System.Drawing.Size(172, 20);
            this.dest_format.SkipValidation = false;
            this.dest_format.TabIndex = 2;
            this.dest_format.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(160, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Report Parameters";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(118, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 16);
            this.label9.TabIndex = 83;
            this.label9.Text = "Enter values for the parameters";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(377, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(67, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 81;
            this.pictureBox2.TabStop = false;
            // 
            // slButton1
            // 
            this.slButton1.ActionType = "";
            this.slButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slButton1.Location = new System.Drawing.Point(253, 334);
            this.slButton1.Name = "slButton1";
            this.slButton1.Size = new System.Drawing.Size(75, 23);
            this.slButton1.TabIndex = 11;
            this.slButton1.Text = "Close";
            this.slButton1.UseVisualStyleBackColor = true;
            this.slButton1.Click += new System.EventHandler(this.slButton1_Click);
            // 
            // Run
            // 
            this.Run.ActionType = "";
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.Location = new System.Drawing.Point(166, 334);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(75, 23);
            this.Run.TabIndex = 10;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Valid Segment(GF/GCB)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Valid Branch Or [ALL]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Desname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Destype";
            // 
            // W_BRN
            // 
            this.W_BRN.AllowSpace = true;
            this.W_BRN.AssociatedLookUpName = "";
            this.W_BRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.W_BRN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.W_BRN.ContinuationTextBox = null;
            this.W_BRN.CustomEnabled = true;
            this.W_BRN.DataFieldMapping = "";
            this.W_BRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.W_BRN.GetRecordsOnUpDownKeys = false;
            this.W_BRN.IsDate = false;
            this.W_BRN.Location = new System.Drawing.Point(170, 178);
            this.W_BRN.MaxLength = 3;
            this.W_BRN.Name = "W_BRN";
            this.W_BRN.NumberFormat = "###,###,##0.00";
            this.W_BRN.Postfix = "";
            this.W_BRN.Prefix = "";
            this.W_BRN.Size = new System.Drawing.Size(172, 20);
            this.W_BRN.SkipValidation = false;
            this.W_BRN.TabIndex = 4;
            this.W_BRN.TextType = CrplControlLibrary.TextType.String;
            // 
            // Dest_Name
            // 
            this.Dest_Name.AllowSpace = true;
            this.Dest_Name.AssociatedLookUpName = "";
            this.Dest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Dest_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Dest_Name.ContinuationTextBox = null;
            this.Dest_Name.CustomEnabled = true;
            this.Dest_Name.DataFieldMapping = "";
            this.Dest_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Name.GetRecordsOnUpDownKeys = false;
            this.Dest_Name.IsDate = false;
            this.Dest_Name.Location = new System.Drawing.Point(170, 100);
            this.Dest_Name.MaxLength = 50;
            this.Dest_Name.Name = "Dest_Name";
            this.Dest_Name.NumberFormat = "###,###,##0.00";
            this.Dest_Name.Postfix = "";
            this.Dest_Name.Prefix = "";
            this.Dest_Name.Size = new System.Drawing.Size(172, 20);
            this.Dest_Name.SkipValidation = false;
            this.Dest_Name.TabIndex = 1;
            this.Dest_Name.TextType = CrplControlLibrary.TextType.String;
            // 
            // cmbDescType
            // 
            this.cmbDescType.BusinessEntity = "";
            this.cmbDescType.ComboBehaviour = CrplControlLibrary.eComboBehaviour.LOVKeyCode;
            this.cmbDescType.CustomEnabled = true;
            this.cmbDescType.DataFieldMapping = "";
            this.cmbDescType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescType.FormattingEnabled = true;
            this.cmbDescType.Items.AddRange(new object[] {
            "Screen",
            "File",
            "Printer",
            "Mail",
            "Preview"});
            this.cmbDescType.Location = new System.Drawing.Point(170, 73);
            this.cmbDescType.LOVType = "";
            this.cmbDescType.Name = "cmbDescType";
            this.cmbDescType.Size = new System.Drawing.Size(172, 21);
            this.cmbDescType.SPName = "";
            this.cmbDescType.TabIndex = 0;
            // 
            // CHRIS_MedicalInsurance_DsgWisOMI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 417);
            this.Controls.Add(this.groupBox1);
            this.Name = "CHRIS_MedicalInsurance_DsgWisOMI";
            this.Text = "CHRIS_MedicalInsurance_DsgWisOMI";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private CrplControlLibrary.SLButton slButton1;
        private CrplControlLibrary.SLButton Run;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox W_BRN;
        private CrplControlLibrary.SLTextBox Dest_Name;
        private CrplControlLibrary.SLComboBox cmbDescType;
        private System.Windows.Forms.Label label6;
        private CrplControlLibrary.SLTextBox copies;
        private System.Windows.Forms.Label label2;
        private CrplControlLibrary.SLTextBox dest_format;
        private CrplControlLibrary.SLTextBox W_DESIG;
        private System.Windows.Forms.Label label10;
        private CrplControlLibrary.SLTextBox W_DEPT;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox W_SEG;
        private System.Windows.Forms.Label label12;
        private CrplControlLibrary.SLTextBox W_YEAR;
        private CrplControlLibrary.SLTextBox W_LEVEL;
        private System.Windows.Forms.Label label11;
    }
}