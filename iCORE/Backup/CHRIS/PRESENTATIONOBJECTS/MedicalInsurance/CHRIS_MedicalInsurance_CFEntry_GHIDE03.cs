using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_CFEntry_GHIDE03 : ChrisMasterDetailForm
    {
        /// <summary>
        /// Defaut Contructor
        /// </summary>
        public CHRIS_MedicalInsurance_CFEntry_GHIDE03()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Overloaded Constructutor
        /// </summary>
        /// <param name="mainmenu"></param>
        /// <param name="connbean_obj"></param>
        public CHRIS_MedicalInsurance_CFEntry_GHIDE03(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(this.slPanelTabular1);
            this.slPanelSimple1.DependentPanels = lstDependentPanels;
            this.IndependentPanels.Add(slPanelSimple1);


        }


        /// <summary>
        /// Ovverridden OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtUsername.Text = this.UserName;
            txtDate.Text = this.Now().ToString("dd/MM/yyyy");
            label6.Text = label6.Text + " " + this.userID;
            this.txtOption.Visible = false;
            slTextBox1.Select();
            slTextBox1.Focus();
        }

        /// <summary>
        /// Key Next Item Trigger
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slTextBox4_TextChanged(object sender, EventArgs e)
        {
            if (slTextBox4.Text.Equals("C"))
            {
                slTextBox5.Text = "LERICAL";
            }
            else if (slTextBox4.Text.Equals("O"))
            {
                slTextBox5.Text = "FFICER";
            }
        }

        /// <summary>
        /// Generate Sequence
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slDataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (slDataGridView1.Rows[e.RowIndex] != null && slDataGridView1.Rows[e.RowIndex].IsNewRow)
                slDataGridView1.Rows[e.RowIndex].Cells["CLA_S_NO"].Value = e.RowIndex + 1;
        }

        /// <summary>
        /// Validate Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slDataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.Cancel)
                return;
            if (slDataGridView1.CurrentCell.IsInEditMode && slDataGridView1.CurrentCell.OwningColumn.Name.Equals("CLA_DATE"))
            {
                DateTime claimDate;
                if (DateTime.TryParse(e.FormattedValue.ToString(), out claimDate))
                {
                    slPanelTabular1.FillEntity(slPanelTabular1.MasterPanel);
                    (slPanelTabular1.CurrentBusinessEntity as MedicalInsuranceClaimDetailCommand).CLA_DATE = claimDate;
                    CmnDataManager dataManager = new CmnDataManager();
                    DataSet ds = new DataSet();
                    ds.Tables.Add(SQLManager.GetSPParams(slPanelTabular1.SPName));
                    dataManager.PopulateDataTableFromObject(slPanelTabular1.CurrentBusinessEntity, ds.Tables[0], "ValidateDate");
                    Result rslt = SQLManager.SelectDataSet(ds);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show("WARNING: CLAIM ON THE GIVEN DATE IS ALREADY ENTERD ......!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            else if (slDataGridView1.CurrentCell.IsInEditMode && slDataGridView1.CurrentCell.OwningColumn.Name.Equals("CLA_REC_AMT"))
            {
                Decimal receivedAmount;
                Decimal claimedAmount;
                if (Decimal.TryParse(e.FormattedValue.ToString(), out receivedAmount))
                {
                    Decimal.TryParse(slDataGridView1.CurrentRow.Cells["CLA_CLAIMED_AMT"].Value.ToString(), out claimedAmount);
                    if (receivedAmount > claimedAmount)
                    {
                        MessageBox.Show("AMOUNT RECEIVED CAN NOT BE GREATER THEN CLAIMED AMOUNT ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                    }
                    else if (receivedAmount == 0)
                    {
                        MessageBox.Show("WARNING:RECEIVED AMOUNT IS LEFT BLANK ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

        }

        /// <summary>
        /// Validate Amount
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slDataGridView1_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.Cancel)
                return;
            Decimal receivedAmount = 0;
            Decimal claimedAmount = 0;
            if (!slDataGridView1.IsCurrentRowDirty)
                return;
            if(slDataGridView1.CurrentRow.Cells["CLA_REC_AMT"].Value != null)
                Decimal.TryParse(slDataGridView1.CurrentRow.Cells["CLA_REC_AMT"].Value.ToString(), out receivedAmount);
            if (slDataGridView1.CurrentRow.Cells["CLA_CLAIMED_AMT"].Value != null)
                Decimal.TryParse(slDataGridView1.CurrentRow.Cells["CLA_CLAIMED_AMT"].Value.ToString(), out claimedAmount);
            if (receivedAmount > claimedAmount)
            {
                MessageBox.Show("AMOUNT RECEIVED CAN NOT BE GREATER THEN CLAIMED AMOUNT ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else if (receivedAmount == 0)
            {
                MessageBox.Show("WARNING:RECEIVED AMOUNT IS LEFT BLANK ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Set User and Date on Cancel
        /// </summary>
        /// <param name="ctrlsCollection"></param>
        /// <param name="actionType"></param>
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            base.DoToolbarActions(ctrlsCollection, actionType);
            if (actionType == "Cancel")
            {
                txtUsername.Text = this.UserName;
                txtDate.Text = this.Now().ToString("dd/MM/yyyy");
                label6.Text = label6.Text + " " + this.userID;
                slTextBox1.Select();
                slTextBox1.Focus();
            }
        }
    }
}