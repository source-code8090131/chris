using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_EnrLettRep : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm
    {
        public CHRIS_MedicalInsurance_EnrLettRep(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.cmbDescType.Items.RemoveAt(5);
            this.dest_format.Text = "dflt";
            this.copies.Text = "1";
            this.wtype.Text = "A";

            this.name.Text  = "Asad Ali";
            this.wdesig.Text  = "Assistant Manager";
            this.wdept.Text = "Human Resources Department";
            this.fdate.Value = null;
            this.tdate.Value = null;
            

        }

        private void Run_Click(object sender, EventArgs e)
        {
            {

                base.RptFileName = "ghi02";
                //base.btnCallReport_Click(sender, e);

                if (cmbDescType.Text == "Screen" || cmbDescType.Text == "Preview")
                {
                    base.btnCallReport_Click(sender, e);
                }
                else if (cmbDescType.Text == "Printer")
                {
                    base.PrintCustomReport();
                }
                else if (cmbDescType.Text == "File")
                {

                    string DestName;

                    if (this.Dest_Name.Text == string.Empty)
                    {
                        DestName = @"C:\iCORE-Spool\Report";
                    }

                    else
                    {
                        DestName = this.Dest_Name.Text;
                    }


                    base.ExportCustomReport(DestName, "pdf");

                }
                else if (cmbDescType.Text == "Mail")
                {
                    string DestName = @"C:\iCORE-Spool\Report";

                    string RecipentName;
                    if (this.Dest_Name.Text == string.Empty)
                    {
                        RecipentName = "";
                    }

                    else
                    {
                        RecipentName = this.Dest_Name.Text;
                    }



                    base.EmailToReport(DestName, "pdf", RecipentName);


                }


            }
        }

        private void slButton1_Click(object sender, EventArgs e)
        {
            base.btnCloseReport_Click(sender, e);
        }


    }
}