using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_PolicyInformation : ChrisSimpleForm
    {
        #region --Variable--
        iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_PolicyInfo_Detail frm_PolDetl;
        string OptDescGI = "POLICY INFORMATION FOR LIFE INSURANCE";
        string OptDescGP = "POLICY INFORMATION FOR GROUP HOSPITALIZATION";
        string OptDescOPM = "POLICY INFORMATION FOR OUT PATIENT MEDICAL";
        #endregion

        #region --Constructor--
        public CHRIS_MedicalInsurance_PolicyInformation()
        {
            InitializeComponent();
        }

        public CHRIS_MedicalInsurance_PolicyInformation(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            
            //this.IndependentPanels.Add(pnlPolicyInfoMain);
        }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                base.IterateFormToEnableControls(pnlPolicyInfoMain.Controls, true);
                txtDate.Text        = this.Now().ToString("dd/MM/yyyy");
                txtOption.Visible   = false;
                tbtAdd.Visible      = false;
                tbtDelete.Visible   = false;
                tbtEdit.Visible     = false;
                tbtList.Visible     = false;
                tbtSave.Visible     = false;
                lblUserName.Text    = userID.ToString();
                txt_W_Opt.Select();
                txt_W_Opt.Focus();
            }
            catch (Exception exp)
            {
                LogError(this.Name, "OnLoad", exp);
            }
        }

        private void txt_W_Opt_Validating(object sender, CancelEventArgs e)
        {
            if (txt_W_Opt.Text == null || (txt_W_Opt.Text != "01" && txt_W_Opt.Text != "02" && txt_W_Opt.Text != "03" && txt_W_Opt.Text != "00"))
            {
                MessageBox.Show("ENTER A VALID OPTION ...........!", "Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else if (txt_W_Opt.Text == "00")
            {
                ClearForm(pnlPolicyInfoMain.Controls);
                Close();
            }
            //else 
        }

        private void txt_W_Opt_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (txt_W_Opt.Text == "01")
                {
                    frm_PolDetl = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_PolicyInfo_Detail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, OptDescGI, txt_W_Opt.Text);
                    //frm_PolDetl.MdiParent   = null;
                    frm_PolDetl.Enabled = true;
                    frm_PolDetl.Show();
                }
                else if (txt_W_Opt.Text == "02")
                {
                    frm_PolDetl = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_PolicyInfo_Detail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, OptDescGP, txt_W_Opt.Text);
                    //frm_PolDetl.MdiParent   = null;
                    frm_PolDetl.Enabled = true;
                    frm_PolDetl.Show();
                }
                else if (txt_W_Opt.Text == "03")
                {
                    frm_PolDetl = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_PolicyInfo_Detail(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean, OptDescOPM, txt_W_Opt.Text);
                    //frm_PolDetl.MdiParent   = null;
                    frm_PolDetl.Enabled = true;
                    frm_PolDetl.Show();
                }
            }
        }
    }
}