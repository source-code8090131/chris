using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.COMMON.SLCONTROLS;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance
{
    public partial class CHRIS_MedicalInsurance_EnrEntry_GHIDE01 : ChrisTabularForm
    {
        #region --Variable--
        iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage1 frm_Enrol1;
        iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage2 frm_Enrol2;
        string pnlPage1 = "pnl1";
        string pnlPage2 = "pnl2";
        DateTime dFrom  = new DateTime();
        DateTime dTo    = new DateTime();
        string sPr_P_No = string.Empty;
        DataTable dt    = new DataTable();
        #endregion


        #region --Constructor--
        public CHRIS_MedicalInsurance_EnrEntry_GHIDE01()
        {
            InitializeComponent();
        }

         public CHRIS_MedicalInsurance_EnrEntry_GHIDE01(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
        : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
        }
        #endregion

        #region --Method--
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                this.lblUserName.Text   = this.UserName;
                this.txtOption.Visible  = false;
                txtDate.Text            = this.Now().ToString("dd/MM/yyyy");
                dgvGrpHosp.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            }
            catch (Exception exp)
            {
                LogException(this.Name, "OnLoad", exp);
            }
        }

        public void BindDGVFromDate(DateTime dFrom, DateTime dTo)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("SearchFilter", dFrom.ToString() + "|" + dTo.ToString());
                //param.Add("SEARCHFILTER", dTo.ToString());

                Result rsltCode;
                CmnDataManager cmnDM        = new CmnDataManager();
                rsltCode                    = cmnDM.GetData("CHRIS_SP_ENRLETTPERSONNEL_MANAGER", "BINDDGVWITHEDATE", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt                      = rsltCode.dstResult.Tables[0];
                    dgvGrpHosp.GridSource   = dt;
                    dgvGrpHosp.DataSource   = dgvGrpHosp.GridSource;
                    dgvGrpHosp.Select();
                    dgvGrpHosp.Focus();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "BindDGV", exp);
            }
        }

        public void BindDGVPrPNo(string strPrPNo)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", dFrom.ToString());
                param.Add("PR_P_NO", dTo.ToString());

                Result rsltCode;
                CmnDataManager cmnDM        = new CmnDataManager();
                rsltCode                    = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "BINDDGV", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt                      = rsltCode.dstResult.Tables[0];
                    dgvGrpHosp.GridSource   = dt;
                    dgvGrpHosp.DataSource   = dgvGrpHosp.GridSource;
                    dgvGrpHosp.Select();
                    dgvGrpHosp.Focus();
                }
            }
            catch (Exception exp)
            {
                LogException(this.Name, "BindDGV", exp);
            }
        }

        #endregion

        #region --Events--
        private void txtChoice_Validating(object sender, CancelEventArgs e)
        {
            if (txtChoice.Text != string.Empty)
            {
                int OptionNo = Convert.ToInt32(txtChoice.Text);

                if (OptionNo > 3)
                {
                    MessageBox.Show("ENTER A VALID OPTION ...........!", "From", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtChoice_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter) && txtChoice.Text != string.Empty)
            {
                int OptionNo = Convert.ToInt32(txtChoice.Text);

                if (OptionNo == 1)
                {
                    frm_Enrol1          = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage1(pnlPage1);
                    frm_Enrol1.Enabled  = true;
                    frm_Enrol1.ShowDialog();
                    dFrom               = Convert.ToDateTime(frm_Enrol1.dtpFrom.Value);
                    dTo                 = Convert.ToDateTime(frm_Enrol1.dtpTo.Value);
                    BindDGVFromDate(dFrom, dTo);
                }
                else if (OptionNo == 2)
                {
                    frm_Enrol1          = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage1(pnlPage2);
                    frm_Enrol1.Enabled  = true;
                    frm_Enrol1.ShowDialog();
                    sPr_P_No            = frm_Enrol1.txtPr_P_No.Text;

                    BindDGVPrPNo(sPr_P_No);
                }
                else if (OptionNo == 3)
                {
                    frm_Enrol2          = new iCORE.CHRIS.PRESENTATIONOBJECTS.MedicalInsurance.CHRIS_MedicalInsurance_EnrollmentLetterPage2(((iCORE.XMS.PRESENTATIONOBJECTS.FORMS.MainMenu)(this.MdiParent)), this.connbean);
                    frm_Enrol2.Enabled  = true;
                    frm_Enrol2.Show();
                }
            }
        }
        
        private void dgvGrpHosp_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox)
            {
                ((TextBox)e.Control).CharacterCasing = CharacterCasing.Upper;
            }
        }

        #endregion
    }
}