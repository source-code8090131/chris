namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_TaxInformationIncome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_TaxInformationIncome));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvIncome = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.SP_AMT_FROM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_AMT_TO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_REBATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FREBATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_PERCEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FPERCEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_TAX_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FTAX_AMT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_SURCHARGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_FSURCHARGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncome)).BeginInit();
            this.PnlDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(679, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(715, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 238);
            this.panel1.Size = new System.Drawing.Size(715, 60);
            // 
            // dgvIncome
            // 
            this.dgvIncome.AllowUserToAddRows = false;
            this.dgvIncome.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncome.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvIncome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIncome.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_AMT_FROM,
            this.SP_AMT_TO,
            this.SP_REBATE,
            this.SP_FREBATE,
            this.SP_PERCEN,
            this.SP_FPERCEN,
            this.SP_TAX_AMT,
            this.SP_FTAX_AMT,
            this.SP_SURCHARGE,
            this.SP_FSURCHARGE});
            this.dgvIncome.ColumnToHide = null;
            this.dgvIncome.ColumnWidth = null;
            this.dgvIncome.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIncome.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvIncome.DisplayColumnWrapper = null;
            this.dgvIncome.GridDefaultRow = 0;
            this.dgvIncome.Location = new System.Drawing.Point(11, 5);
            this.dgvIncome.Name = "dgvIncome";
            this.dgvIncome.ReadOnly = true;
            this.dgvIncome.ReadOnlyColumns = null;
            this.dgvIncome.RequiredColumns = "";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncome.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvIncome.Size = new System.Drawing.Size(683, 178);
            this.dgvIncome.SkippingColumns = null;
            this.dgvIncome.TabIndex = 0;
            // 
            // SP_AMT_FROM
            // 
            this.SP_AMT_FROM.DataPropertyName = "SP_AMT_FROM";
            dataGridViewCellStyle2.NullValue = null;
            this.SP_AMT_FROM.DefaultCellStyle = dataGridViewCellStyle2;
            this.SP_AMT_FROM.HeaderText = "From";
            this.SP_AMT_FROM.Name = "SP_AMT_FROM";
            this.SP_AMT_FROM.ReadOnly = true;
            this.SP_AMT_FROM.Width = 65;
            // 
            // SP_AMT_TO
            // 
            this.SP_AMT_TO.DataPropertyName = "SP_AMT_TO";
            this.SP_AMT_TO.HeaderText = "To";
            this.SP_AMT_TO.Name = "SP_AMT_TO";
            this.SP_AMT_TO.ReadOnly = true;
            this.SP_AMT_TO.Width = 65;
            // 
            // SP_REBATE
            // 
            this.SP_REBATE.DataPropertyName = "SP_REBATE";
            this.SP_REBATE.HeaderText = "Rebate Male";
            this.SP_REBATE.Name = "SP_REBATE";
            this.SP_REBATE.ReadOnly = true;
            this.SP_REBATE.Width = 70;
            // 
            // SP_FREBATE
            // 
            this.SP_FREBATE.DataPropertyName = "SP_FREBATE";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.SP_FREBATE.DefaultCellStyle = dataGridViewCellStyle3;
            this.SP_FREBATE.HeaderText = "Female";
            this.SP_FREBATE.Name = "SP_FREBATE";
            this.SP_FREBATE.ReadOnly = true;
            this.SP_FREBATE.Width = 70;
            // 
            // SP_PERCEN
            // 
            this.SP_PERCEN.DataPropertyName = "SP_PERCEN";
            this.SP_PERCEN.HeaderText = "% M.";
            this.SP_PERCEN.Name = "SP_PERCEN";
            this.SP_PERCEN.ReadOnly = true;
            this.SP_PERCEN.Width = 35;
            // 
            // SP_FPERCEN
            // 
            this.SP_FPERCEN.DataPropertyName = "SP_FPERCEN";
            this.SP_FPERCEN.HeaderText = "F.";
            this.SP_FPERCEN.Name = "SP_FPERCEN";
            this.SP_FPERCEN.ReadOnly = true;
            this.SP_FPERCEN.Width = 35;
            // 
            // SP_TAX_AMT
            // 
            this.SP_TAX_AMT.DataPropertyName = "SP_TAX_AMT";
            this.SP_TAX_AMT.HeaderText = "Tax Amnt Male";
            this.SP_TAX_AMT.Name = "SP_TAX_AMT";
            this.SP_TAX_AMT.ReadOnly = true;
            this.SP_TAX_AMT.Width = 70;
            // 
            // SP_FTAX_AMT
            // 
            this.SP_FTAX_AMT.DataPropertyName = "SP_FTAX_AMT";
            this.SP_FTAX_AMT.HeaderText = "Tax Amnt Female";
            this.SP_FTAX_AMT.Name = "SP_FTAX_AMT";
            this.SP_FTAX_AMT.ReadOnly = true;
            this.SP_FTAX_AMT.Width = 70;
            // 
            // SP_SURCHARGE
            // 
            this.SP_SURCHARGE.DataPropertyName = "SP_SURCHARGE";
            this.SP_SURCHARGE.HeaderText = "Surcharge M.";
            this.SP_SURCHARGE.Name = "SP_SURCHARGE";
            this.SP_SURCHARGE.ReadOnly = true;
            this.SP_SURCHARGE.Width = 70;
            // 
            // SP_FSURCHARGE
            // 
            this.SP_FSURCHARGE.DataPropertyName = "SP_FSURCHARGE";
            this.SP_FSURCHARGE.HeaderText = "Surcharge F.";
            this.SP_FSURCHARGE.Name = "SP_FSURCHARGE";
            this.SP_FSURCHARGE.ReadOnly = true;
            this.SP_FSURCHARGE.Width = 70;
            // 
            // PnlDetail
            // 
            this.PnlDetail.ConcurrentPanels = null;
            this.PnlDetail.Controls.Add(this.dgvIncome);
            this.PnlDetail.DataManager = null;
            this.PnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDetail.DependentPanels = null;
            this.PnlDetail.DisableDependentLoad = false;
            this.PnlDetail.EnableDelete = true;
            this.PnlDetail.EnableInsert = true;
            this.PnlDetail.EnableQuery = false;
            this.PnlDetail.EnableUpdate = true;
            this.PnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BonusTabQueryCommand";
            this.PnlDetail.Location = new System.Drawing.Point(8, 39);
            this.PnlDetail.MasterPanel = null;
            this.PnlDetail.Name = "PnlDetail";
            this.PnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlDetail.Size = new System.Drawing.Size(695, 193);
            this.PnlDetail.SPName = "CHRIS_SP_BONUS_TAB_MANAGER";
            this.PnlDetail.TabIndex = 0;
            this.PnlDetail.TabStop = true;
            // 
            // CHRIS_Query_TaxInformationIncome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(715, 298);
            this.Controls.Add(this.PnlDetail);
            this.Name = "CHRIS_Query_TaxInformationIncome";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Tax Information Income";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDetail, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncome)).EndInit();
            this.PnlDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvIncome;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_AMT_FROM;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_AMT_TO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_REBATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FREBATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_PERCEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FPERCEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_TAX_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FTAX_AMT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_SURCHARGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_FSURCHARGE;
    }
}