using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_TaxInformationAllowance : ChrisSimpleForm 
    {
        
        string FYEAR;
        int PRONUM;
        String PayrollDate;
        DataTable dtAllow = new DataTable();
        Dictionary<String, Object> objValues = new Dictionary<String, Object>();
        Result rsltAllow;
        CmnDataManager cmnDM = new CmnDataManager();
            
        public CHRIS_Query_TaxInformationAllowance(string frmYear, int pNum, String payrollDate)
        {
            InitializeComponent();
            this.ShowBottomBar.Equals(false);

            this.FYEAR = frmYear;
            this.PRONUM = pNum;
            this.PayrollDate = payrollDate;
          
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;

            objValues.Clear();
            objValues.Add("PR_P_NO", this.PRONUM);
            objValues.Add("W_TAX_FROM", this.FYEAR);
            objValues.Add("PAYROLL_DATE", this.PayrollDate);


            rsltAllow = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRLALW_FILL", objValues);

            if (rsltAllow.isSuccessful)
            {
                if (rsltAllow.dstResult.Tables.Count > 0)
                {
                    dtAllow = rsltAllow.dstResult.Tables[0];

                    this.dgvAllowance.DataSource = dtAllow;
                }
                else
                {
                    this.dgvAllowance.DataSource = null;
                    this.dgvAllowance.Refresh();

                }
            }

        }


        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtDelete.Visible = false;
            tbtCancel.Visible = false;
            tbtAdd.Visible = false;
        }

        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(pnlAllowance.Controls);
            }
        }


    }
}