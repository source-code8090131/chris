using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRIS.PRESENTATIONOBJECTS;
using iCORE.Common;
using iCORE.COMMON.SLCONTROLS;
using iCORE.Common.PRESENTATIONOBJECTS.Cmn;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using CrplControlLibrary;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_SalaryHistoryQuery : ChrisMasterDetailForm
    {

        #region Declaration
        CmnDataManager objCmnDataManager;
        #endregion

        #region Constructor
        public CHRIS_Query_SalaryHistoryQuery()
        {
            InitializeComponent();
        }

        public CHRIS_Query_SalaryHistoryQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();

            //Dependent panel
            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(slPnlTabularSalaryDetail);    
            this.slPnlsimpleSalaryTop.DependentPanels = lstDependentPanels;

            //Independent panel
            this.IndependentPanels.Add(slPnlsimpleSalaryTop);

        }
        #endregion

        #region Methods
        protected override void CommonOnLoadMethods()
        {
            try
            {
                base.CommonOnLoadMethods();

                this.txtOption.Visible = false;
                this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                lblUserName.Text += this.UserName;
                this.slDgvSalaryDetail.ReadOnly = true;
                tbtDelete.Visible = false;
                tbtSave.Visible = false;
                EnableOptions(false);
                txtPersonnelNo.Focus();
                txtPersonnelNo.Select();
                this.FunctionConfig.F6 = Function.Quit;
                this.FunctionConfig.EnableF6 = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }     
        //protected override void OnLoad(EventArgs e)
        //{
        //    base.OnLoad(e);       
        //    this.txtOption.Visible = false;
        //    this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
        //    lblUserName.Text += this.UserName;      
        //}
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            try
            {
                base.DoToolbarActions(ctrlsCollection, actionType);

                if (actionType == "List")
                {
                    if (this.txtPersonnelNo.Text != "")
                    {
                        FillSalaryDetail();
                        //this.operationMode = Mode.Edit;
                        //tbtDelete.Enabled = false;
                        slPnlsimpleSalaryTop.LoadDependentPanels();
                        EnableOptions(false);
                    }

                }
                else if (actionType == "Cancel")
                {
                    txtPersonnelNo.Focus();
                    txtPersonnelNo.Select();
                }
                this.txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void FillSalaryDetail()
        {
            try
            {

                #region Fill Salary Top Block
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_P_NO", (object)txtPersonnelNo.Text);

                objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SALARYHISTORYQUERY_PERSONNEL_MANAGER", "GetSalaryTopBlock", d);

                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            #region Exist
                            if (rsltCode.dstResult.Tables[0].Rows[0]["pr_joining_date"] != null)
                            {
                                this.txtJoiningDate.Text = rsltCode.dstResult.Tables[0].Rows[0]["pr_joining_date"].ToString();
                            }
                            if (rsltCode.dstResult.Tables[0].Rows[0]["pr_level"] != null)
                            {
                                this.txtLevel.Text = rsltCode.dstResult.Tables[0].Rows[0]["pr_level"].ToString();
                            }
                            if (rsltCode.dstResult.Tables[0].Rows[0]["PR_ANNUAL_PACK"] != null)
                            {
                                this.txtPackage.Text = rsltCode.dstResult.Tables[0].Rows[0]["PR_ANNUAL_PACK"].ToString();
                            }
                            if (rsltCode.dstResult.Tables[0].Rows[0]["w_desig"] != null)
                            {
                                this.txtDesignation.Text = rsltCode.dstResult.Tables[0].Rows[0]["w_desig"].ToString();
                            }
                            #endregion
                        }
                    }            
                }
                #endregion
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void EnableOptions(bool isEnable)
        {
            this.tbtDelete.Enabled = isEnable;
            this.tbtSave.Enabled = isEnable;
            this.tbtAdd.Enabled = isEnable;
            this.tbtEdit.Enabled = isEnable;
        }
        #endregion

        private void txtPersonnelNo_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtPersonnelNo.Text != "")
            {
                FillSalaryDetail();
                //this.operationMode = Mode.Edit;
                //tbtDelete.Enabled = false;
                slPnlsimpleSalaryTop.LoadDependentPanels();
                EnableOptions(false);
            }

        }

        protected override bool Quit()
        {
            foreach (Control tx in this.slPnlsimpleSalaryTop.Controls)
            {

                if (tx != null)
                    if (tx is SLTextBox)
                        if (tx.Text != string.Empty)
                            this.FunctionConfig.CurrentOption = Function.Query;

            }
            bool flag = false;
            if (this.FunctionConfig.CurrentOption == Function.None)
            {
                if (base.tlbMain.Items.Count > 0)
                {
                    base.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtClose"]));
                    //base.Close();
                    //this.Dispose(true);
                }
                //else

            }
            else
            {
                this.FunctionConfig.CurrentOption = Function.None;
                this.tlbMain_ItemClicked(this.tlbMain, new ToolStripItemClickedEventArgs(base.tlbMain.Items["tbtCancel"]));
            }
            
            return flag;
           // base.Quit();
            //return false;
        }
    }
}