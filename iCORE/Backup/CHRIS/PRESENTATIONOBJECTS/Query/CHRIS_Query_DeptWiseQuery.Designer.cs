namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_DeptWiseQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_DeptWiseQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlDepart = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.slTextBox1 = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnDept = new CrplControlLibrary.LookupButton(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtDesc = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtSeg = new CrplControlLibrary.SLTextBox(this.components);
            this.txtBranchCode = new CrplControlLibrary.SLTextBox(this.components);
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDataGridView1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_P_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_FIRST_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_LAST_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_CATEGORY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_LEVEL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DESIG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MA_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHead = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtLocation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtUser = new CrplControlLibrary.SLTextBox(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlDepart.SuspendLayout();
            this.PnlPersonnel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).BeginInit();
            this.pnlHead.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(622, 0);
            this.txtOption.Size = new System.Drawing.Size(12, 20);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(634, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 425);
            this.panel1.Size = new System.Drawing.Size(634, 60);
            // 
            // PnlDepart
            // 
            this.PnlDepart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlDepart.ConcurrentPanels = null;
            this.PnlDepart.Controls.Add(this.label5);
            this.PnlDepart.Controls.Add(this.slTextBox1);
            this.PnlDepart.Controls.Add(this.lbtnDept);
            this.PnlDepart.Controls.Add(this.label4);
            this.PnlDepart.Controls.Add(this.txtDesc);
            this.PnlDepart.Controls.Add(this.label3);
            this.PnlDepart.Controls.Add(this.label2);
            this.PnlDepart.Controls.Add(this.label1);
            this.PnlDepart.Controls.Add(this.txtName);
            this.PnlDepart.Controls.Add(this.txtSeg);
            this.PnlDepart.Controls.Add(this.txtBranchCode);
            this.PnlDepart.DataManager = null;
            this.PnlDepart.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlDepart.DependentPanels = null;
            this.PnlDepart.DisableDependentLoad = false;
            this.PnlDepart.EnableDelete = false;
            this.PnlDepart.EnableInsert = false;
            this.PnlDepart.EnableQuery = false;
            this.PnlDepart.EnableUpdate = false;
            this.PnlDepart.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelSearchCommand";
            this.PnlDepart.Location = new System.Drawing.Point(13, 132);
            this.PnlDepart.MasterPanel = null;
            this.PnlDepart.Name = "PnlDepart";
            this.PnlDepart.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.ControlBlock;
            this.PnlDepart.Size = new System.Drawing.Size(617, 89);
            this.PnlDepart.SPName = "CHRIS_SP_QUERY_dept_MANAGER";
            this.PnlDepart.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(179, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "/";
            // 
            // slTextBox1
            // 
            this.slTextBox1.AllowSpace = true;
            this.slTextBox1.AssociatedLookUpName = "lbtnDept";
            this.slTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.slTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.slTextBox1.ContinuationTextBox = null;
            this.slTextBox1.CustomEnabled = true;
            this.slTextBox1.DataFieldMapping = "D_NO";
            this.slTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slTextBox1.GetRecordsOnUpDownKeys = false;
            this.slTextBox1.IsDate = false;
            this.slTextBox1.IsLookUpField = true;
            this.slTextBox1.Location = new System.Drawing.Point(197, 52);
            this.slTextBox1.Name = "slTextBox1";
            this.slTextBox1.NumberFormat = "###,###,##0.00";
            this.slTextBox1.Postfix = "";
            this.slTextBox1.Prefix = "";
            this.slTextBox1.Size = new System.Drawing.Size(52, 20);
            this.slTextBox1.SkipValidation = false;
            this.slTextBox1.TabIndex = 2;
            this.slTextBox1.TextType = CrplControlLibrary.TextType.String;
            this.slTextBox1.Leave += new System.EventHandler(this.slTextBox1_Leave);
            this.slTextBox1.Validating += new System.ComponentModel.CancelEventHandler(this.slTextBox1_Validating);
            // 
            // lbtnDept
            // 
            this.lbtnDept.ActionLOVExists = "D_NO_LOV_EXIST";
            this.lbtnDept.ActionType = "D_NO_LOV";
            this.lbtnDept.CausesValidation = false;
            this.lbtnDept.ConditionalFields = "txtSeg|txtBranchCode";
            this.lbtnDept.CustomEnabled = true;
            this.lbtnDept.DataFieldMapping = "";
            this.lbtnDept.DependentLovControls = "";
            this.lbtnDept.HiddenColumns = "W_BRANCH";
            this.lbtnDept.Image = ((System.Drawing.Image)(resources.GetObject("lbtnDept.Image")));
            this.lbtnDept.LoadDependentEntities = true;
            this.lbtnDept.Location = new System.Drawing.Point(255, 52);
            this.lbtnDept.LookUpTitle = null;
            this.lbtnDept.Name = "lbtnDept";
            this.lbtnDept.Size = new System.Drawing.Size(26, 21);
            this.lbtnDept.SkipValidationOnLeave = false;
            this.lbtnDept.SPName = "CHRIS_SP_QUERY_dept_cont_MANAGER";
            this.lbtnDept.TabIndex = 8;
            this.lbtnDept.TabStop = false;
            this.lbtnDept.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(288, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Description : ";
            // 
            // txtDesc
            // 
            this.txtDesc.AllowSpace = true;
            this.txtDesc.AssociatedLookUpName = "";
            this.txtDesc.BackColor = System.Drawing.SystemColors.Control;
            this.txtDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesc.ContinuationTextBox = null;
            this.txtDesc.CustomEnabled = true;
            this.txtDesc.DataFieldMapping = "W_BNAME";
            this.txtDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesc.GetRecordsOnUpDownKeys = false;
            this.txtDesc.IsDate = false;
            this.txtDesc.Location = new System.Drawing.Point(386, 22);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.NumberFormat = "###,###,##0.00";
            this.txtDesc.Postfix = "";
            this.txtDesc.Prefix = "";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(144, 20);
            this.txtDesc.SkipValidation = false;
            this.txtDesc.TabIndex = 6;
            this.txtDesc.TabStop = false;
            this.txtDesc.TextType = CrplControlLibrary.TextType.String;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(320, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Name : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Seg./Dept. Code :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Branch Code :";
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BackColor = System.Drawing.SystemColors.Control;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "D_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(386, 52);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(217, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 3;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtSeg
            // 
            this.txtSeg.AllowSpace = true;
            this.txtSeg.AssociatedLookUpName = "";
            this.txtSeg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSeg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSeg.ContinuationTextBox = null;
            this.txtSeg.CustomEnabled = true;
            this.txtSeg.DataFieldMapping = "D_SEG";
            this.txtSeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeg.GetRecordsOnUpDownKeys = false;
            this.txtSeg.IsDate = false;
            this.txtSeg.Location = new System.Drawing.Point(120, 52);
            this.txtSeg.MaxLength = 3;
            this.txtSeg.Name = "txtSeg";
            this.txtSeg.NumberFormat = "###,###,##0.00";
            this.txtSeg.Postfix = "";
            this.txtSeg.Prefix = "";
            this.txtSeg.Size = new System.Drawing.Size(52, 20);
            this.txtSeg.SkipValidation = false;
            this.txtSeg.TabIndex = 1;
            this.txtSeg.TextType = CrplControlLibrary.TextType.String;
            this.txtSeg.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeg_validating);
            // 
            // txtBranchCode
            // 
            this.txtBranchCode.AllowSpace = true;
            this.txtBranchCode.AssociatedLookUpName = "";
            this.txtBranchCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranchCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranchCode.ContinuationTextBox = null;
            this.txtBranchCode.CustomEnabled = true;
            this.txtBranchCode.DataFieldMapping = "W_BRANCH";
            this.txtBranchCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchCode.GetRecordsOnUpDownKeys = false;
            this.txtBranchCode.IsDate = false;
            this.txtBranchCode.Location = new System.Drawing.Point(120, 24);
            this.txtBranchCode.MaxLength = 3;
            this.txtBranchCode.Name = "txtBranchCode";
            this.txtBranchCode.NumberFormat = "###,###,##0.00";
            this.txtBranchCode.Postfix = "";
            this.txtBranchCode.Prefix = "";
            this.txtBranchCode.Size = new System.Drawing.Size(100, 20);
            this.txtBranchCode.SkipValidation = false;
            this.txtBranchCode.TabIndex = 0;
            this.txtBranchCode.TextType = CrplControlLibrary.TextType.String;
            this.txtBranchCode.Leave += new System.EventHandler(this.txtBranchCode_Leave);
            this.txtBranchCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtBranchCode_Validating);
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.slDataGridView1);
            this.PnlPersonnel.DataManager = null;
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = true;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelQueryCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(13, 223);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(617, 196);
            this.PnlPersonnel.SPName = "CHRIS_SP_QUERY_PERSONNEL_MANAGER";
            this.PnlPersonnel.TabIndex = 3;
            // 
            // slDataGridView1
            // 
            this.slDataGridView1.AllowUserToAddRows = false;
            this.slDataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.slDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_P_NO,
            this.PR_FIRST_NAME,
            this.PR_LAST_NAME,
            this.PR_CATEGORY,
            this.PR_LEVEL,
            this.PR_DESIG,
            this.MA_FLAG});
            this.slDataGridView1.ColumnToHide = null;
            this.slDataGridView1.ColumnWidth = null;
            this.slDataGridView1.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.slDataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.slDataGridView1.DisplayColumnWrapper = null;
            this.slDataGridView1.GridDefaultRow = 0;
            this.slDataGridView1.Location = new System.Drawing.Point(0, 3);
            this.slDataGridView1.Name = "slDataGridView1";
            this.slDataGridView1.ReadOnly = true;
            this.slDataGridView1.ReadOnlyColumns = null;
            this.slDataGridView1.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.slDataGridView1.Size = new System.Drawing.Size(620, 177);
            this.slDataGridView1.SkippingColumns = null;
            this.slDataGridView1.TabIndex = 3;
            // 
            // PR_P_NO
            // 
            this.PR_P_NO.DataPropertyName = "PR_P_NO";
            this.PR_P_NO.HeaderText = "Personnel No.";
            this.PR_P_NO.Name = "PR_P_NO";
            this.PR_P_NO.ReadOnly = true;
            this.PR_P_NO.Width = 125;
            // 
            // PR_FIRST_NAME
            // 
            this.PR_FIRST_NAME.DataPropertyName = "PR_FIRST_NAME";
            this.PR_FIRST_NAME.HeaderText = "Name";
            this.PR_FIRST_NAME.Name = "PR_FIRST_NAME";
            this.PR_FIRST_NAME.ReadOnly = true;
            // 
            // PR_LAST_NAME
            // 
            this.PR_LAST_NAME.DataPropertyName = "PR_LAST_NAME";
            this.PR_LAST_NAME.HeaderText = "";
            this.PR_LAST_NAME.Name = "PR_LAST_NAME";
            this.PR_LAST_NAME.ReadOnly = true;
            this.PR_LAST_NAME.Width = 90;
            // 
            // PR_CATEGORY
            // 
            this.PR_CATEGORY.DataPropertyName = "PR_CATEGORY";
            this.PR_CATEGORY.HeaderText = "Category";
            this.PR_CATEGORY.Name = "PR_CATEGORY";
            this.PR_CATEGORY.ReadOnly = true;
            this.PR_CATEGORY.Width = 90;
            // 
            // PR_LEVEL
            // 
            this.PR_LEVEL.DataPropertyName = "PR_LEVEL";
            this.PR_LEVEL.HeaderText = "Level";
            this.PR_LEVEL.Name = "PR_LEVEL";
            this.PR_LEVEL.ReadOnly = true;
            this.PR_LEVEL.Width = 90;
            // 
            // PR_DESIG
            // 
            this.PR_DESIG.DataPropertyName = "PR_DESIG";
            this.PR_DESIG.HeaderText = "Designation";
            this.PR_DESIG.Name = "PR_DESIG";
            this.PR_DESIG.ReadOnly = true;
            this.PR_DESIG.Width = 90;
            // 
            // MA_FLAG
            // 
            this.MA_FLAG.DataPropertyName = "MA_FLAG";
            this.MA_FLAG.HeaderText = "";
            this.MA_FLAG.Name = "MA_FLAG";
            this.MA_FLAG.ReadOnly = true;
            this.MA_FLAG.Width = 90;
            // 
            // pnlHead
            // 
            this.pnlHead.Controls.Add(this.label6);
            this.pnlHead.Controls.Add(this.label7);
            this.pnlHead.Controls.Add(this.txtDate);
            this.pnlHead.Controls.Add(this.label8);
            this.pnlHead.Controls.Add(this.txtLocation);
            this.pnlHead.Controls.Add(this.txtUser);
            this.pnlHead.Controls.Add(this.label10);
            this.pnlHead.Controls.Add(this.label11);
            this.pnlHead.Location = new System.Drawing.Point(13, 56);
            this.pnlHead.Name = "pnlHead";
            this.pnlHead.Size = new System.Drawing.Size(617, 72);
            this.pnlHead.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(178, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(270, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = " Of Employee\'s";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(189, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(242, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "Department Wise Query";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(510, 32);
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(80, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 18;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(455, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "Date : ";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLocation
            // 
            this.txtLocation.AllowSpace = true;
            this.txtLocation.AssociatedLookUpName = "";
            this.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLocation.ContinuationTextBox = null;
            this.txtLocation.CustomEnabled = true;
            this.txtLocation.DataFieldMapping = "";
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.GetRecordsOnUpDownKeys = false;
            this.txtLocation.IsDate = false;
            this.txtLocation.Location = new System.Drawing.Point(81, 38);
            this.txtLocation.MaxLength = 10;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.NumberFormat = "###,###,##0.00";
            this.txtLocation.Postfix = "";
            this.txtLocation.Prefix = "";
            this.txtLocation.ReadOnly = true;
            this.txtLocation.Size = new System.Drawing.Size(90, 20);
            this.txtLocation.SkipValidation = false;
            this.txtLocation.TabIndex = 14;
            this.txtLocation.TabStop = false;
            this.txtLocation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtUser
            // 
            this.txtUser.AllowSpace = true;
            this.txtUser.AssociatedLookUpName = "";
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUser.ContinuationTextBox = null;
            this.txtUser.CustomEnabled = true;
            this.txtUser.DataFieldMapping = "";
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.GetRecordsOnUpDownKeys = false;
            this.txtUser.IsDate = false;
            this.txtUser.Location = new System.Drawing.Point(81, 13);
            this.txtUser.MaxLength = 10;
            this.txtUser.Name = "txtUser";
            this.txtUser.NumberFormat = "###,###,##0.00";
            this.txtUser.Postfix = "";
            this.txtUser.Prefix = "";
            this.txtUser.ReadOnly = true;
            this.txtUser.Size = new System.Drawing.Size(90, 20);
            this.txtUser.SkipValidation = false;
            this.txtUser.TabIndex = 13;
            this.txtUser.TabStop = false;
            this.txtUser.TextType = CrplControlLibrary.TextType.String;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(9, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Location : ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(16, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "User : ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(378, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 70;
            this.lblUserName.Text = "User Name :";
            // 
            // CHRIS_Query_DeptWiseQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(634, 485);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlHead);
            this.Controls.Add(this.PnlPersonnel);
            this.Controls.Add(this.PnlDepart);
            this.CurrentPanelBlock = "PnlDepart";
            this.F6OptionText = "";
            this.Name = "CHRIS_Query_DeptWiseQuery";
            this.ShowBottomBar = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Department Wise Query";
            this.BeforeLOVShown += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.BeforeLOVShown(this.CHRIS_Query_DeptWiseQuery_BeforeLOVShown);
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Query_DeptWiseQuery_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlDepart, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.pnlHead, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlDepart.ResumeLayout(false);
            this.PnlDepart.PerformLayout();
            this.PnlPersonnel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).EndInit();
            this.pnlHead.ResumeLayout(false);
            this.pnlHead.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlDepart;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlPersonnel;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDataGridView1;
        private CrplControlLibrary.SLTextBox txtSeg;
        private CrplControlLibrary.SLTextBox txtBranchCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtName;
        private System.Windows.Forms.Label label4;
        private CrplControlLibrary.SLTextBox txtDesc;
        private System.Windows.Forms.Label label5;
        private CrplControlLibrary.SLTextBox slTextBox1;
        private CrplControlLibrary.LookupButton lbtnDept;
        private System.Windows.Forms.Panel pnlHead;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtLocation;
        private CrplControlLibrary.SLTextBox txtUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_P_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_FIRST_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_LAST_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_CATEGORY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_LEVEL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DESIG;
        private System.Windows.Forms.DataGridViewTextBoxColumn MA_FLAG;
    }
}