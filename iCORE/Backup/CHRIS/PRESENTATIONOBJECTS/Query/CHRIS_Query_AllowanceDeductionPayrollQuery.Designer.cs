namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_AllowanceDeductionPayrollQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_AllowanceDeductionPayrollQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pnlPayroll_Deduc = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvDeduc = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PD_DED_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_DED_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDeductionHeader = new System.Windows.Forms.Label();
            this.lblNewCriteria = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnlMaster = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlPayroll_Allow = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvAllowance = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PW_ALL_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW_PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW_ALL_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PW_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAllowanceHeader = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pnlPayroll = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.dgvPFundBranchBasic = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PA_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PA_PFUND = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PA_BRANCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PA_BASIC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PA_NET_PAY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.pnlPayroll_Deduc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeduc)).BeginInit();
            this.pnlMaster.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlPayroll_Allow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllowance)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.pnlPayroll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPFundBranchBasic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 587);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pnlPayroll_Deduc);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(82, 427);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(490, 156);
            this.groupBox2.TabIndex = 54;
            this.groupBox2.TabStop = false;
            // 
            // pnlPayroll_Deduc
            // 
            this.pnlPayroll_Deduc.ConcurrentPanels = null;
            this.pnlPayroll_Deduc.Controls.Add(this.dgvDeduc);
            this.pnlPayroll_Deduc.DataManager = null;
            this.pnlPayroll_Deduc.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPayroll_Deduc.DependentPanels = null;
            this.pnlPayroll_Deduc.DisableDependentLoad = false;
            this.pnlPayroll_Deduc.EnableDelete = true;
            this.pnlPayroll_Deduc.EnableInsert = true;
            this.pnlPayroll_Deduc.EnableQuery = false;
            this.pnlPayroll_Deduc.EnableUpdate = true;
            this.pnlPayroll_Deduc.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLDEUCCommand";
            this.pnlPayroll_Deduc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPayroll_Deduc.Location = new System.Drawing.Point(13, 21);
            this.pnlPayroll_Deduc.MasterPanel = null;
            this.pnlPayroll_Deduc.Name = "pnlPayroll_Deduc";
            this.pnlPayroll_Deduc.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPayroll_Deduc.Size = new System.Drawing.Size(461, 130);
            this.pnlPayroll_Deduc.SPName = "CHRIS_SP_PAYROLL_DEDUC_MANAGER";
            this.pnlPayroll_Deduc.TabIndex = 15;
            this.pnlPayroll_Deduc.TabStop = true;
            // 
            // dgvDeduc
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Format = "dd-MMM-yyyy";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDeduc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDeduc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeduc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PD_DED_CODE,
            this.PD_PAY_DATE,
            this.PD_DED_AMOUNT,
            this.PD_ACCOUNT});
            this.dgvDeduc.ColumnToHide = null;
            this.dgvDeduc.ColumnWidth = null;
            this.dgvDeduc.CustomEnabled = true;
            this.dgvDeduc.DisplayColumnWrapper = null;
            this.dgvDeduc.GridDefaultRow = 0;
            this.dgvDeduc.Location = new System.Drawing.Point(3, 3);
            this.dgvDeduc.Name = "dgvDeduc";
            this.dgvDeduc.ReadOnlyColumns = null;
            this.dgvDeduc.RequiredColumns = null;
            this.dgvDeduc.Size = new System.Drawing.Size(445, 120);
            this.dgvDeduc.SkippingColumns = null;
            this.dgvDeduc.TabIndex = 25;
            // 
            // PD_DED_CODE
            // 
            this.PD_DED_CODE.DataPropertyName = "PD_DED_CODE";
            this.PD_DED_CODE.HeaderText = "Deduction Code";
            this.PD_DED_CODE.Name = "PD_DED_CODE";
            this.PD_DED_CODE.ReadOnly = true;
            this.PD_DED_CODE.Width = 140;
            // 
            // PD_PAY_DATE
            // 
            this.PD_PAY_DATE.DataPropertyName = "PD_PAY_DATE";
            dataGridViewCellStyle2.Format = "dd-MMM-yyyy";
            this.PD_PAY_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.PD_PAY_DATE.HeaderText = "Pay Date";
            this.PD_PAY_DATE.Name = "PD_PAY_DATE";
            this.PD_PAY_DATE.ReadOnly = true;
            this.PD_PAY_DATE.Width = 105;
            // 
            // PD_DED_AMOUNT
            // 
            this.PD_DED_AMOUNT.DataPropertyName = "PD_DED_AMOUNT";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.PD_DED_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            this.PD_DED_AMOUNT.HeaderText = "Amount";
            this.PD_DED_AMOUNT.Name = "PD_DED_AMOUNT";
            this.PD_DED_AMOUNT.ReadOnly = true;
            this.PD_DED_AMOUNT.Width = 105;
            // 
            // PD_ACCOUNT
            // 
            this.PD_ACCOUNT.DataPropertyName = "PD_ACCOUNT";
            this.PD_ACCOUNT.HeaderText = "Account";
            this.PD_ACCOUNT.Name = "PD_ACCOUNT";
            this.PD_ACCOUNT.ReadOnly = true;
            this.PD_ACCOUNT.Width = 110;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(31, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 63;
            this.label1.Text = "Deductions";
            // 
            // lblDeductionHeader
            // 
            this.lblDeductionHeader.AutoSize = true;
            this.lblDeductionHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeductionHeader.ForeColor = System.Drawing.Color.Blue;
            this.lblDeductionHeader.Location = new System.Drawing.Point(109, 490);
            this.lblDeductionHeader.Name = "lblDeductionHeader";
            this.lblDeductionHeader.Size = new System.Drawing.Size(144, 29);
            this.lblDeductionHeader.TabIndex = 1;
            this.lblDeductionHeader.Text = "Deductions";
            // 
            // lblNewCriteria
            // 
            this.lblNewCriteria.AutoSize = true;
            this.lblNewCriteria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewCriteria.Location = new System.Drawing.Point(568, 421);
            this.lblNewCriteria.Name = "lblNewCriteria";
            this.lblNewCriteria.Size = new System.Drawing.Size(101, 13);
            this.lblNewCriteria.TabIndex = 55;
            this.lblNewCriteria.Text = "F1= New Criteria";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.Location = new System.Drawing.Point(596, 448);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(49, 13);
            this.lblExit.TabIndex = 56;
            this.lblExit.Text = "F6=Exit";
            // 
            // pnlMaster
            // 
            this.pnlMaster.ConcurrentPanels = null;
            this.pnlMaster.Controls.Add(this.lbtnPNo);
            this.pnlMaster.Controls.Add(this.txtPersNo);
            this.pnlMaster.Controls.Add(this.label14);
            this.pnlMaster.Controls.Add(this.txtName);
            this.pnlMaster.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlMaster.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlMaster.DependentPanels = null;
            this.pnlMaster.DisableDependentLoad = false;
            this.pnlMaster.EnableDelete = true;
            this.pnlMaster.EnableInsert = true;
            this.pnlMaster.EnableQuery = false;
            this.pnlMaster.EnableUpdate = true;
            this.pnlMaster.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlMaster.Location = new System.Drawing.Point(82, 39);
            this.pnlMaster.MasterPanel = null;
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlMaster.Size = new System.Drawing.Size(489, 43);
            this.pnlMaster.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.pnlMaster.TabIndex = 0;
            this.pnlMaster.TabStop = true;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PP_NO_EXISTS";
            this.lbtnPNo.ActionType = "PP_LOV";
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(232, 11);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_ALLOWANCEDEDUCTIONPAYROLL_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.toolTip1.SetToolTip(this.lbtnPNo, "Select Desired Personnel Number");
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(109, 11);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(117, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPersNo, "Press <F9> Key to Display The List");
            this.txtPersNo.Leave += new System.EventHandler(this.txtPersNo_Leave_1);
            this.txtPersNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersNo_KeyPress);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label14.Location = new System.Drawing.Point(4, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "Personnel No";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "name";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(264, 11);
            this.txtName.MaxLength = 10;
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(200, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlPayroll_Allow);
            this.groupBox1.Controls.Add(this.lblAllowanceHeader);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(82, 264);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(490, 160);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            // 
            // pnlPayroll_Allow
            // 
            this.pnlPayroll_Allow.ConcurrentPanels = null;
            this.pnlPayroll_Allow.Controls.Add(this.dgvAllowance);
            this.pnlPayroll_Allow.DataManager = null;
            this.pnlPayroll_Allow.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPayroll_Allow.DependentPanels = null;
            this.pnlPayroll_Allow.DisableDependentLoad = false;
            this.pnlPayroll_Allow.EnableDelete = true;
            this.pnlPayroll_Allow.EnableInsert = true;
            this.pnlPayroll_Allow.EnableQuery = false;
            this.pnlPayroll_Allow.EnableUpdate = true;
            this.pnlPayroll_Allow.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLALLOWCommand";
            this.pnlPayroll_Allow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPayroll_Allow.Location = new System.Drawing.Point(11, 22);
            this.pnlPayroll_Allow.MasterPanel = null;
            this.pnlPayroll_Allow.Name = "pnlPayroll_Allow";
            this.pnlPayroll_Allow.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPayroll_Allow.Size = new System.Drawing.Size(468, 129);
            this.pnlPayroll_Allow.SPName = "CHRIS_SP_PAYROLL_ALLOW_MANAGER";
            this.pnlPayroll_Allow.TabIndex = 65;
            this.pnlPayroll_Allow.TabStop = true;
            // 
            // dgvAllowance
            // 
            this.dgvAllowance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllowance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PW_ALL_CODE,
            this.PW_PAY_DATE,
            this.PW_ALL_AMOUNT,
            this.PW_ACCOUNT});
            this.dgvAllowance.ColumnToHide = null;
            this.dgvAllowance.ColumnWidth = null;
            this.dgvAllowance.CustomEnabled = true;
            this.dgvAllowance.DisplayColumnWrapper = null;
            this.dgvAllowance.GridDefaultRow = 0;
            this.dgvAllowance.Location = new System.Drawing.Point(7, 3);
            this.dgvAllowance.Name = "dgvAllowance";
            this.dgvAllowance.ReadOnlyColumns = null;
            this.dgvAllowance.RequiredColumns = null;
            this.dgvAllowance.Size = new System.Drawing.Size(440, 120);
            this.dgvAllowance.SkippingColumns = null;
            this.dgvAllowance.TabIndex = 4;
            // 
            // PW_ALL_CODE
            // 
            this.PW_ALL_CODE.DataPropertyName = "PW_ALL_CODE";
            this.PW_ALL_CODE.HeaderText = "Allowance Code";
            this.PW_ALL_CODE.Name = "PW_ALL_CODE";
            this.PW_ALL_CODE.ReadOnly = true;
            this.PW_ALL_CODE.Width = 150;
            // 
            // PW_PAY_DATE
            // 
            this.PW_PAY_DATE.DataPropertyName = "PW_PAY_DATE";
            dataGridViewCellStyle4.Format = "dd-MMM-yyyy";
            this.PW_PAY_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            this.PW_PAY_DATE.HeaderText = "Pay Date";
            this.PW_PAY_DATE.Name = "PW_PAY_DATE";
            this.PW_PAY_DATE.ReadOnly = true;
            this.PW_PAY_DATE.Width = 110;
            // 
            // PW_ALL_AMOUNT
            // 
            this.PW_ALL_AMOUNT.DataPropertyName = "PW_ALL_AMOUNT";
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.PW_ALL_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            this.PW_ALL_AMOUNT.HeaderText = "Amount";
            this.PW_ALL_AMOUNT.Name = "PW_ALL_AMOUNT";
            this.PW_ALL_AMOUNT.ReadOnly = true;
            this.PW_ALL_AMOUNT.Width = 105;
            // 
            // PW_ACCOUNT
            // 
            this.PW_ACCOUNT.DataPropertyName = "PW_ACCOUNT";
            this.PW_ACCOUNT.HeaderText = "Account";
            this.PW_ACCOUNT.Name = "PW_ACCOUNT";
            this.PW_ACCOUNT.ReadOnly = true;
            this.PW_ACCOUNT.Width = 105;
            // 
            // lblAllowanceHeader
            // 
            this.lblAllowanceHeader.AutoSize = true;
            this.lblAllowanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllowanceHeader.ForeColor = System.Drawing.Color.Blue;
            this.lblAllowanceHeader.Location = new System.Drawing.Point(31, 0);
            this.lblAllowanceHeader.Name = "lblAllowanceHeader";
            this.lblAllowanceHeader.Size = new System.Drawing.Size(87, 16);
            this.lblAllowanceHeader.TabIndex = 62;
            this.lblAllowanceHeader.Text = "Allowances";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(431, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 70;
            this.lblUserName.Text = "User Name :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pnlPayroll);
            this.groupBox3.Location = new System.Drawing.Point(58, 88);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(555, 173);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // pnlPayroll
            // 
            this.pnlPayroll.ConcurrentPanels = null;
            this.pnlPayroll.Controls.Add(this.dgvPFundBranchBasic);
            this.pnlPayroll.DataManager = null;
            this.pnlPayroll.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlPayroll.DependentPanels = null;
            this.pnlPayroll.DisableDependentLoad = false;
            this.pnlPayroll.EnableDelete = true;
            this.pnlPayroll.EnableInsert = true;
            this.pnlPayroll.EnableQuery = false;
            this.pnlPayroll.EnableUpdate = true;
            this.pnlPayroll.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PAYROLLCommand";
            this.pnlPayroll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlPayroll.Location = new System.Drawing.Point(3, 9);
            this.pnlPayroll.MasterPanel = null;
            this.pnlPayroll.Name = "pnlPayroll";
            this.pnlPayroll.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlPayroll.Size = new System.Drawing.Size(545, 155);
            this.pnlPayroll.SPName = "CHRIS_SP_PAYROLL_MANAGER";
            this.pnlPayroll.TabIndex = 3;
            this.pnlPayroll.TabStop = true;
            // 
            // dgvPFundBranchBasic
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPFundBranchBasic.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvPFundBranchBasic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPFundBranchBasic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PA_DATE,
            this.PA_PFUND,
            this.PA_BRANCH,
            this.PA_BASIC,
            this.PA_NET_PAY});
            this.dgvPFundBranchBasic.ColumnToHide = null;
            this.dgvPFundBranchBasic.ColumnWidth = null;
            this.dgvPFundBranchBasic.CustomEnabled = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPFundBranchBasic.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvPFundBranchBasic.DisplayColumnWrapper = null;
            this.dgvPFundBranchBasic.GridDefaultRow = 0;
            this.dgvPFundBranchBasic.Location = new System.Drawing.Point(6, 9);
            this.dgvPFundBranchBasic.Name = "dgvPFundBranchBasic";
            this.dgvPFundBranchBasic.ReadOnlyColumns = null;
            this.dgvPFundBranchBasic.RequiredColumns = null;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPFundBranchBasic.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvPFundBranchBasic.Size = new System.Drawing.Size(531, 140);
            this.dgvPFundBranchBasic.SkippingColumns = null;
            this.dgvPFundBranchBasic.TabIndex = 3;
            // 
            // PA_DATE
            // 
            this.PA_DATE.DataPropertyName = "PA_DATE";
            dataGridViewCellStyle7.Format = "dd-MMM-yyyy";
            this.PA_DATE.DefaultCellStyle = dataGridViewCellStyle7;
            this.PA_DATE.HeaderText = "Date";
            this.PA_DATE.Name = "PA_DATE";
            this.PA_DATE.ReadOnly = true;
            this.PA_DATE.Width = 120;
            // 
            // PA_PFUND
            // 
            this.PA_PFUND.DataPropertyName = "PA_PFUND";
            this.PA_PFUND.HeaderText = "PFund";
            this.PA_PFUND.Name = "PA_PFUND";
            this.PA_PFUND.ReadOnly = true;
            this.PA_PFUND.Width = 120;
            // 
            // PA_BRANCH
            // 
            this.PA_BRANCH.DataPropertyName = "PA_BRANCH";
            this.PA_BRANCH.HeaderText = "Branch";
            this.PA_BRANCH.Name = "PA_BRANCH";
            this.PA_BRANCH.ReadOnly = true;
            this.PA_BRANCH.Width = 120;
            // 
            // PA_BASIC
            // 
            this.PA_BASIC.DataPropertyName = "PA_BASIC";
            this.PA_BASIC.HeaderText = "Basic";
            this.PA_BASIC.Name = "PA_BASIC";
            this.PA_BASIC.ReadOnly = true;
            this.PA_BASIC.Width = 120;
            // 
            // PA_NET_PAY
            // 
            this.PA_NET_PAY.DataPropertyName = "PA_NET_PAY";
            this.PA_NET_PAY.HeaderText = "Net Pay";
            this.PA_NET_PAY.Name = "PA_NET_PAY";
            this.PA_NET_PAY.ReadOnly = true;
            this.PA_NET_PAY.Width = 115;
            // 
            // CHRIS_Query_AllowanceDeductionPayrollQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(669, 647);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pnlMaster);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.lblNewCriteria);
            this.Controls.Add(this.groupBox2);
            this.Name = "CHRIS_Query_AllowanceDeductionPayrollQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.lblNewCriteria, 0);
            this.Controls.SetChildIndex(this.lblExit, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.pnlMaster, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlPayroll_Deduc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeduc)).EndInit();
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlPayroll_Allow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllowance)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.pnlPayroll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPFundBranchBasic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNewCriteria;
        private System.Windows.Forms.Label lblExit;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlMaster;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label label14;
        private CrplControlLibrary.SLTextBox txtName;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDeduc;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlPayroll_Deduc;
        private System.Windows.Forms.Label lblDeductionHeader;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblAllowanceHeader;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlPayroll_Allow;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvAllowance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.GroupBox groupBox3;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular pnlPayroll;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvPFundBranchBasic;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_PFUND;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_BRANCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_BASIC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_NET_PAY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_DED_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_PAY_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_DED_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_ACCOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_ALL_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_PAY_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_ALL_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PW_ACCOUNT;
    }
}