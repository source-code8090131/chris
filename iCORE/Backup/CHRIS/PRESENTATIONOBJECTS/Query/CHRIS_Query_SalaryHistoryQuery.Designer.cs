namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_SalaryHistoryQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_SalaryHistoryQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.slPnlTabularSalaryDetail = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDgvSalaryDetail = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_INC_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DESIG_PRESENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_IN_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_EFFECTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_LEVEL_PRESENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_DESIG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_ANNUAL_PRESENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slPnlsimpleSalaryTop = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnPersonnel = new CrplControlLibrary.LookupButton(this.components);
            this.txtPackage = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesignation = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJoiningDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersonnelNo = new CrplControlLibrary.SLTextBox(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gboSalaryTop = new System.Windows.Forms.GroupBox();
            this.gboSalaryDetail = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.slPnlTabularSalaryDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDgvSalaryDetail)).BeginInit();
            this.slPnlsimpleSalaryTop.SuspendLayout();
            this.gboSalaryTop.SuspendLayout();
            this.gboSalaryDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 608);
            // 
            // slPnlTabularSalaryDetail
            // 
            this.slPnlTabularSalaryDetail.ConcurrentPanels = null;
            this.slPnlTabularSalaryDetail.Controls.Add(this.slDgvSalaryDetail);
            this.slPnlTabularSalaryDetail.DataManager = "";
            this.slPnlTabularSalaryDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlTabularSalaryDetail.DependentPanels = null;
            this.slPnlTabularSalaryDetail.DisableDependentLoad = false;
            this.slPnlTabularSalaryDetail.EnableDelete = true;
            this.slPnlTabularSalaryDetail.EnableInsert = true;
            this.slPnlTabularSalaryDetail.EnableQuery = false;
            this.slPnlTabularSalaryDetail.EnableUpdate = true;
            this.slPnlTabularSalaryDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.IncPromotionCommand";
            this.slPnlTabularSalaryDetail.Location = new System.Drawing.Point(9, 16);
            this.slPnlTabularSalaryDetail.MasterPanel = null;
            this.slPnlTabularSalaryDetail.Name = "slPnlTabularSalaryDetail";
            this.slPnlTabularSalaryDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlTabularSalaryDetail.Size = new System.Drawing.Size(603, 323);
            this.slPnlTabularSalaryDetail.SPName = "CHRIS_SP_SALARYHISTORYQUERY_INC_PROMOTION_MANAGER";
            this.slPnlTabularSalaryDetail.TabIndex = 1;
            // 
            // slDgvSalaryDetail
            // 
            this.slDgvSalaryDetail.AllowUserToAddRows = false;
            this.slDgvSalaryDetail.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDgvSalaryDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.slDgvSalaryDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDgvSalaryDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.PR_INC_TYPE,
            this.PR_DESIG_PRESENT,
            this.PR_IN_NO,
            this.PR_EFFECTIVE,
            this.PR_DESC,
            this.PR_LEVEL_PRESENT,
            this.PR_DESIG,
            this.PR_ANNUAL_PRESENT});
            this.slDgvSalaryDetail.ColumnToHide = null;
            this.slDgvSalaryDetail.ColumnWidth = null;
            this.slDgvSalaryDetail.CustomEnabled = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.slDgvSalaryDetail.DefaultCellStyle = dataGridViewCellStyle4;
            this.slDgvSalaryDetail.DisplayColumnWrapper = null;
            this.slDgvSalaryDetail.GridDefaultRow = 0;
            this.slDgvSalaryDetail.Location = new System.Drawing.Point(19, 16);
            this.slDgvSalaryDetail.Name = "slDgvSalaryDetail";
            this.slDgvSalaryDetail.ReadOnlyColumns = null;
            this.slDgvSalaryDetail.RequiredColumns = null;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDgvSalaryDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.slDgvSalaryDetail.Size = new System.Drawing.Size(566, 291);
            this.slDgvSalaryDetail.SkippingColumns = null;
            this.slDgvSalaryDetail.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // PR_INC_TYPE
            // 
            this.PR_INC_TYPE.DataPropertyName = "PR_INC_TYPE";
            this.PR_INC_TYPE.HeaderText = "PR_INC_TYPE";
            this.PR_INC_TYPE.Name = "PR_INC_TYPE";
            this.PR_INC_TYPE.Visible = false;
            // 
            // PR_DESIG_PRESENT
            // 
            this.PR_DESIG_PRESENT.DataPropertyName = "PR_DESIG_PRESENT";
            this.PR_DESIG_PRESENT.HeaderText = "PR_DESIG_PRESENT";
            this.PR_DESIG_PRESENT.Name = "PR_DESIG_PRESENT";
            this.PR_DESIG_PRESENT.Visible = false;
            // 
            // PR_IN_NO
            // 
            this.PR_IN_NO.DataPropertyName = "PR_IN_NO";
            this.PR_IN_NO.HeaderText = "PR_IN_NO";
            this.PR_IN_NO.Name = "PR_IN_NO";
            this.PR_IN_NO.Visible = false;
            // 
            // PR_EFFECTIVE
            // 
            this.PR_EFFECTIVE.DataPropertyName = "PR_EFFECTIVE";
            dataGridViewCellStyle2.NullValue = null;
            this.PR_EFFECTIVE.DefaultCellStyle = dataGridViewCellStyle2;
            this.PR_EFFECTIVE.HeaderText = "Date";
            this.PR_EFFECTIVE.MaxInputLength = 30;
            this.PR_EFFECTIVE.Name = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.Width = 90;
            // 
            // PR_DESC
            // 
            this.PR_DESC.DataPropertyName = "pr_desc";
            this.PR_DESC.HeaderText = "Increment Type";
            this.PR_DESC.MaxInputLength = 30;
            this.PR_DESC.Name = "PR_DESC";
            this.PR_DESC.ReadOnly = true;
            this.PR_DESC.Width = 120;
            // 
            // PR_LEVEL_PRESENT
            // 
            this.PR_LEVEL_PRESENT.DataPropertyName = "PR_LEVEL_PRESENT";
            this.PR_LEVEL_PRESENT.HeaderText = "Level";
            this.PR_LEVEL_PRESENT.MaxInputLength = 3;
            this.PR_LEVEL_PRESENT.Name = "PR_LEVEL_PRESENT";
            this.PR_LEVEL_PRESENT.ReadOnly = true;
            this.PR_LEVEL_PRESENT.Width = 40;
            // 
            // PR_DESIG
            // 
            this.PR_DESIG.DataPropertyName = "pr_desig";
            this.PR_DESIG.HeaderText = "Designation";
            this.PR_DESIG.MaxInputLength = 100;
            this.PR_DESIG.Name = "PR_DESIG";
            this.PR_DESIG.ReadOnly = true;
            this.PR_DESIG.Width = 170;
            // 
            // PR_ANNUAL_PRESENT
            // 
            this.PR_ANNUAL_PRESENT.DataPropertyName = "PR_ANNUAL_PRESENT";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.PR_ANNUAL_PRESENT.DefaultCellStyle = dataGridViewCellStyle3;
            this.PR_ANNUAL_PRESENT.HeaderText = "Annual Package";
            this.PR_ANNUAL_PRESENT.MaxInputLength = 30;
            this.PR_ANNUAL_PRESENT.Name = "PR_ANNUAL_PRESENT";
            this.PR_ANNUAL_PRESENT.ReadOnly = true;
            // 
            // slPnlsimpleSalaryTop
            // 
            this.slPnlsimpleSalaryTop.ConcurrentPanels = null;
            this.slPnlsimpleSalaryTop.Controls.Add(this.lbtnPersonnel);
            this.slPnlsimpleSalaryTop.Controls.Add(this.txtPackage);
            this.slPnlsimpleSalaryTop.Controls.Add(this.txtLevel);
            this.slPnlsimpleSalaryTop.Controls.Add(this.txtName);
            this.slPnlsimpleSalaryTop.Controls.Add(this.txtDesignation);
            this.slPnlsimpleSalaryTop.Controls.Add(this.txtJoiningDate);
            this.slPnlsimpleSalaryTop.Controls.Add(this.txtPersonnelNo);
            this.slPnlsimpleSalaryTop.Controls.Add(this.label3);
            this.slPnlsimpleSalaryTop.Controls.Add(this.label6);
            this.slPnlsimpleSalaryTop.Controls.Add(this.label5);
            this.slPnlsimpleSalaryTop.Controls.Add(this.label4);
            this.slPnlsimpleSalaryTop.Controls.Add(this.label2);
            this.slPnlsimpleSalaryTop.Controls.Add(this.label1);
            this.slPnlsimpleSalaryTop.DataManager = "";
            this.slPnlsimpleSalaryTop.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.slPnlsimpleSalaryTop.DependentPanels = null;
            this.slPnlsimpleSalaryTop.DisableDependentLoad = false;
            this.slPnlsimpleSalaryTop.EnableDelete = true;
            this.slPnlsimpleSalaryTop.EnableInsert = true;
            this.slPnlsimpleSalaryTop.EnableQuery = false;
            this.slPnlsimpleSalaryTop.EnableUpdate = true;
            this.slPnlsimpleSalaryTop.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelDesigCommand";
            this.slPnlsimpleSalaryTop.Location = new System.Drawing.Point(25, 19);
            this.slPnlsimpleSalaryTop.MasterPanel = null;
            this.slPnlsimpleSalaryTop.Name = "slPnlsimpleSalaryTop";
            this.slPnlsimpleSalaryTop.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.slPnlsimpleSalaryTop.Size = new System.Drawing.Size(583, 92);
            this.slPnlsimpleSalaryTop.SPName = "CHRIS_SP_SALARYHISTORYQUERY_PERSONNEL_MANAGER";
            this.slPnlsimpleSalaryTop.TabIndex = 0;
            // 
            // lbtnPersonnel
            // 
            this.lbtnPersonnel.ActionLOVExists = "LovExistsPersonnel";
            this.lbtnPersonnel.ActionType = "LovPersonnel";
            this.lbtnPersonnel.ConditionalFields = "";
            this.lbtnPersonnel.CustomEnabled = true;
            this.lbtnPersonnel.DataFieldMapping = "";
            this.lbtnPersonnel.DependentLovControls = "";
            this.lbtnPersonnel.HiddenColumns = "";
            this.lbtnPersonnel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonnel.Image")));
            this.lbtnPersonnel.LoadDependentEntities = true;
            this.lbtnPersonnel.Location = new System.Drawing.Point(181, 12);
            this.lbtnPersonnel.LookUpTitle = null;
            this.lbtnPersonnel.Name = "lbtnPersonnel";
            this.lbtnPersonnel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonnel.SkipValidationOnLeave = false;
            this.lbtnPersonnel.SPName = "CHRIS_SP_SALARYHISTORYQUERY_PERSONNEL_MANAGER";
            this.lbtnPersonnel.TabIndex = 13;
            this.lbtnPersonnel.TabStop = false;
            this.lbtnPersonnel.UseVisualStyleBackColor = true;
            // 
            // txtPackage
            // 
            this.txtPackage.AllowSpace = true;
            this.txtPackage.AssociatedLookUpName = "";
            this.txtPackage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPackage.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPackage.ContinuationTextBox = null;
            this.txtPackage.CustomEnabled = true;
            this.txtPackage.DataFieldMapping = "PR_ANNUAL_PACK";
            this.txtPackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPackage.GetRecordsOnUpDownKeys = false;
            this.txtPackage.IsDate = false;
            this.txtPackage.Location = new System.Drawing.Point(452, 36);
            this.txtPackage.Name = "txtPackage";
            this.txtPackage.NumberFormat = "###,###,##0.0";
            this.txtPackage.Postfix = "";
            this.txtPackage.Prefix = "";
            this.txtPackage.ReadOnly = true;
            this.txtPackage.Size = new System.Drawing.Size(111, 20);
            this.txtPackage.SkipValidation = false;
            this.txtPackage.TabIndex = 12;
            this.txtPackage.TabStop = false;
            this.txtPackage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPackage.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "pr_level";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(277, 36);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(30, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 11;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "PR_NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(277, 13);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(286, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 10;
            this.txtName.TabStop = false;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtDesignation
            // 
            this.txtDesignation.AllowSpace = true;
            this.txtDesignation.AssociatedLookUpName = "";
            this.txtDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesignation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesignation.ContinuationTextBox = null;
            this.txtDesignation.CustomEnabled = true;
            this.txtDesignation.DataFieldMapping = "w_desig";
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.GetRecordsOnUpDownKeys = false;
            this.txtDesignation.IsDate = false;
            this.txtDesignation.Location = new System.Drawing.Point(115, 59);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.NumberFormat = "###,###,##0.00";
            this.txtDesignation.Postfix = "";
            this.txtDesignation.Prefix = "";
            this.txtDesignation.ReadOnly = true;
            this.txtDesignation.Size = new System.Drawing.Size(283, 20);
            this.txtDesignation.SkipValidation = false;
            this.txtDesignation.TabIndex = 9;
            this.txtDesignation.TabStop = false;
            this.txtDesignation.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtJoiningDate
            // 
            this.txtJoiningDate.AllowSpace = true;
            this.txtJoiningDate.AssociatedLookUpName = "";
            this.txtJoiningDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJoiningDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJoiningDate.ContinuationTextBox = null;
            this.txtJoiningDate.CustomEnabled = true;
            this.txtJoiningDate.DataFieldMapping = "pr_joining_date";
            this.txtJoiningDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJoiningDate.GetRecordsOnUpDownKeys = false;
            this.txtJoiningDate.IsDate = false;
            this.txtJoiningDate.Location = new System.Drawing.Point(115, 36);
            this.txtJoiningDate.Name = "txtJoiningDate";
            this.txtJoiningDate.NumberFormat = "###,###,##0.00";
            this.txtJoiningDate.Postfix = "";
            this.txtJoiningDate.Prefix = "";
            this.txtJoiningDate.ReadOnly = true;
            this.txtJoiningDate.Size = new System.Drawing.Size(100, 20);
            this.txtJoiningDate.SkipValidation = false;
            this.txtJoiningDate.TabIndex = 8;
            this.txtJoiningDate.TabStop = false;
            this.txtJoiningDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersonnelNo
            // 
            this.txtPersonnelNo.AllowSpace = true;
            this.txtPersonnelNo.AssociatedLookUpName = "lbtnPersonnel";
            this.txtPersonnelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersonnelNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersonnelNo.ContinuationTextBox = null;
            this.txtPersonnelNo.CustomEnabled = true;
            this.txtPersonnelNo.DataFieldMapping = "PR_P_NO";
            this.txtPersonnelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonnelNo.GetRecordsOnUpDownKeys = false;
            this.txtPersonnelNo.IsDate = false;
            this.txtPersonnelNo.IsLookUpField = true;
            this.txtPersonnelNo.IsRequired = true;
            this.txtPersonnelNo.Location = new System.Drawing.Point(115, 13);
            this.txtPersonnelNo.MaxLength = 6;
            this.txtPersonnelNo.Name = "txtPersonnelNo";
            this.txtPersonnelNo.NumberFormat = "###,###,##0.00";
            this.txtPersonnelNo.Postfix = "";
            this.txtPersonnelNo.Prefix = "";
            this.txtPersonnelNo.Size = new System.Drawing.Size(60, 20);
            this.txtPersonnelNo.SkipValidation = false;
            this.txtPersonnelNo.TabIndex = 7;
            this.txtPersonnelNo.TabStop = false;
            this.txtPersonnelNo.TextType = CrplControlLibrary.TextType.Integer;
            this.txtPersonnelNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersonnelNo_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Designation :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(338, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Annual Package :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(227, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Level :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(225, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Joining Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Personnel No :";
            // 
            // gboSalaryTop
            // 
            this.gboSalaryTop.Controls.Add(this.slPnlsimpleSalaryTop);
            this.gboSalaryTop.Location = new System.Drawing.Point(23, 113);
            this.gboSalaryTop.Name = "gboSalaryTop";
            this.gboSalaryTop.Size = new System.Drawing.Size(620, 125);
            this.gboSalaryTop.TabIndex = 2;
            this.gboSalaryTop.TabStop = false;
            // 
            // gboSalaryDetail
            // 
            this.gboSalaryDetail.Controls.Add(this.slPnlTabularSalaryDetail);
            this.gboSalaryDetail.Location = new System.Drawing.Point(23, 242);
            this.gboSalaryDetail.Name = "gboSalaryDetail";
            this.gboSalaryDetail.Size = new System.Drawing.Size(620, 358);
            this.gboSalaryDetail.TabIndex = 10;
            this.gboSalaryDetail.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(267, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Salary History Query";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(491, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Date :";
            // 
            // txtDate
            // 
            this.txtDate.AllowSpace = true;
            this.txtDate.AssociatedLookUpName = "";
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDate.ContinuationTextBox = null;
            this.txtDate.CustomEnabled = true;
            this.txtDate.DataFieldMapping = "";
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.GetRecordsOnUpDownKeys = false;
            this.txtDate.IsDate = false;
            this.txtDate.Location = new System.Drawing.Point(543, 94);
            this.txtDate.Name = "txtDate";
            this.txtDate.NumberFormat = "###,###,##0.00";
            this.txtDate.Postfix = "";
            this.txtDate.Prefix = "";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(100, 20);
            this.txtDate.SkipValidation = false;
            this.txtDate.TabIndex = 13;
            this.txtDate.TabStop = false;
            this.txtDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(347, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(69, 13);
            this.lblUserName.TabIndex = 102;
            this.lblUserName.Text = "User Name  :";
            // 
            // CHRIS_Query_SalaryHistoryQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 668);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.gboSalaryTop);
            this.Controls.Add(this.gboSalaryDetail);
            this.Name = "CHRIS_Query_SalaryHistoryQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "CHRIS_Query_SalaryHistoryQuery";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.gboSalaryDetail, 0);
            this.Controls.SetChildIndex(this.gboSalaryTop, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.txtDate, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.slPnlTabularSalaryDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDgvSalaryDetail)).EndInit();
            this.slPnlsimpleSalaryTop.ResumeLayout(false);
            this.slPnlsimpleSalaryTop.PerformLayout();
            this.gboSalaryTop.ResumeLayout(false);
            this.gboSalaryDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple slPnlsimpleSalaryTop;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular slPnlTabularSalaryDetail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtPackage;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtDesignation;
        private CrplControlLibrary.SLTextBox txtJoiningDate;
        private CrplControlLibrary.SLTextBox txtPersonnelNo;
        private System.Windows.Forms.GroupBox gboSalaryTop;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDgvSalaryDetail;
        private System.Windows.Forms.GroupBox gboSalaryDetail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private CrplControlLibrary.SLTextBox txtDate;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_INC_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DESIG_PRESENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_IN_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_EFFECTIVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_LEVEL_PRESENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_DESIG;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_ANNUAL_PRESENT;
        private CrplControlLibrary.LookupButton lbtnPersonnel;
    }
}