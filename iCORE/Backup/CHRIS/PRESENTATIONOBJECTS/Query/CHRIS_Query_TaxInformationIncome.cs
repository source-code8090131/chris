using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;


namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_TaxInformationIncome : ChrisSimpleForm
    {

        int PRONUM;
        DataTable dtIncome = new DataTable();
        Dictionary<String, Object> objValues = new Dictionary<String, Object>();
        Result rsltIncome;
        CmnDataManager cmnDM = new CmnDataManager();


        public CHRIS_Query_TaxInformationIncome(int pNum)
        {
            InitializeComponent();
            this.ShowBottomBar.Equals(false);
                       
            this.PRONUM = pNum;
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.txtOption.Visible = false;
            tbtSave.Enabled = false;
            tbtList.Enabled = false;
            tbtCancel.Enabled = true;
            tbtDelete.Enabled = false;



            objValues.Clear();
            objValues.Add("PR_P_NO", this.PRONUM);
            

            rsltIncome = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "INCOME_FILL", objValues);

            if (rsltIncome.isSuccessful)
            {
                if (rsltIncome.dstResult.Tables.Count > 0)
                {
                    dtIncome = rsltIncome.dstResult.Tables[0];

                    this.dgvIncome.DataSource = dtIncome;
                }
                else
                {
                    this.dgvIncome.DataSource = null;
                    this.dgvIncome.Refresh();

                }
            }

          
        }

        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtSave.Visible = false;
            tbtList.Visible = false;
            tbtDelete.Visible = false;
            tbtCancel.Visible = false;
            tbtAdd.Visible = false;
        }


        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            if (actionType == "Edit")
            {
                return;
            }
            if (actionType == "Save")
            {
                return;
            }
            if (actionType == "Delete")
            {
                return;
            }

            if (actionType == "Cancel")
            {
                this.ClearForm(PnlDetail.Controls);             
            }
        }






    }
}