namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_TaxInformationQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_TaxInformationQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pnlDetail = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.txtForcastAll = new CrplControlLibrary.SLTextBox(this.components);
            this.lblFoAl = new System.Windows.Forms.Label();
            this.lblCMTax = new System.Windows.Forms.Label();
            this.txtCurrMntTax = new CrplControlLibrary.SLTextBox(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.lblTotalIncome = new System.Windows.Forms.Label();
            this.txtTaxPaidUpdate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotalIncome = new CrplControlLibrary.SLTextBox(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.lblTaxRFnd = new System.Windows.Forms.Label();
            this.lblAnnTax = new System.Windows.Forms.Label();
            this.lblTaxInc = new System.Windows.Forms.Label();
            this.txtTaxRfnd = new CrplControlLibrary.SLTextBox(this.components);
            this.lblFCMnths = new System.Windows.Forms.Label();
            this.lblAvgMnths = new System.Windows.Forms.Label();
            this.txtAnnTax = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxInc = new CrplControlLibrary.SLTextBox(this.components);
            this.txtForcastMnt = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAvMnt = new CrplControlLibrary.SLTextBox(this.components);
            this.lblTOTCost = new System.Windows.Forms.Label();
            this.lblFCOT = new System.Windows.Forms.Label();
            this.lblOTArears = new System.Windows.Forms.Label();
            this.txtTOTCost = new CrplControlLibrary.SLTextBox(this.components);
            this.lblAvgOT = new System.Windows.Forms.Label();
            this.lblAOTCost = new System.Windows.Forms.Label();
            this.txtForcastOT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOTArear = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAvgOT = new CrplControlLibrary.SLTextBox(this.components);
            this.txtOTCost = new CrplControlLibrary.SLTextBox(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIncBonus = new System.Windows.Forms.Label();
            this.lblSLT = new System.Windows.Forms.Label();
            this.lblGovtAllow = new System.Windows.Forms.Label();
            this.txtTaxPeriod2 = new CrplControlLibrary.SLTextBox(this.components);
            this.dgvTDASR = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PR_INC_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_EFFECTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_ANNUAL_PREVIOUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PR_ANNUAL_PRESENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDT = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.PA_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PA_INCOME_TAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEducation = new CrplControlLibrary.SLTextBox(this.components);
            this.lbtnPNo = new CrplControlLibrary.LookupButton(this.components);
            this.lblPyrDate = new System.Windows.Forms.Label();
            this.lblGHABonus = new System.Windows.Forms.Label();
            this.lblTaxPkg = new System.Windows.Forms.Label();
            this.txtAccNum = new CrplControlLibrary.SLTextBox(this.components);
            this.lblTP = new System.Windows.Forms.Label();
            this.lblTaxAllow = new System.Windows.Forms.Label();
            this.lblJDate = new System.Windows.Forms.Label();
            this.txtNatTaxNum = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPersNo = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnBasic = new CrplControlLibrary.SLTextBox(this.components);
            this.lblAnnPkg = new System.Windows.Forms.Label();
            this.txtSalArear = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTotal = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxAllow = new CrplControlLibrary.SLTextBox(this.components);
            this.txtRVPTrans = new CrplControlLibrary.SLTextBox(this.components);
            this.lblAnnBasic = new System.Windows.Forms.Label();
            this.lblRVPTrans = new System.Windows.Forms.Label();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblSalArears = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtAnnPkge = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLastName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtJoinDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtGEID = new CrplControlLibrary.SLTextBox(this.components);
            this.txtIncrDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lblCategory = new System.Windows.Forms.Label();
            this.txtCategory = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.lblIncrDate = new System.Windows.Forms.Label();
            this.txtPayrollDate = new CrplControlLibrary.SLTextBox(this.components);
            this.lblPromoDate = new System.Windows.Forms.Label();
            this.txtPromoDate = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTaxPeriod = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFunctional = new CrplControlLibrary.SLTextBox(this.components);
            this.lbLSlash = new System.Windows.Forms.Label();
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtFirstName = new CrplControlLibrary.SLTextBox(this.components);
            this.lblDesig = new System.Windows.Forms.Label();
            this.lblPrNum = new System.Windows.Forms.Label();
            this.lblTaxQueryHeader = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.lblSave = new System.Windows.Forms.Label();
            this.txtMSG = new CrplControlLibrary.SLTextBox(this.components);
            this.txtTEST1 = new CrplControlLibrary.SLTextBox(this.components);
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTDASR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDT)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(648, 0);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(684, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 613);
            this.panel1.Size = new System.Drawing.Size(684, 60);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(454, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 69;
            this.lblUserName.Text = "User Name :";
            // 
            // pnlDetail
            // 
            this.pnlDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetail.ConcurrentPanels = null;
            this.pnlDetail.Controls.Add(this.txtForcastAll);
            this.pnlDetail.Controls.Add(this.lblFoAl);
            this.pnlDetail.Controls.Add(this.lblCMTax);
            this.pnlDetail.Controls.Add(this.txtCurrMntTax);
            this.pnlDetail.Controls.Add(this.label14);
            this.pnlDetail.Controls.Add(this.lblTotalIncome);
            this.pnlDetail.Controls.Add(this.txtTaxPaidUpdate);
            this.pnlDetail.Controls.Add(this.txtTotalIncome);
            this.pnlDetail.Controls.Add(this.label13);
            this.pnlDetail.Controls.Add(this.lblTaxRFnd);
            this.pnlDetail.Controls.Add(this.lblAnnTax);
            this.pnlDetail.Controls.Add(this.lblTaxInc);
            this.pnlDetail.Controls.Add(this.txtTaxRfnd);
            this.pnlDetail.Controls.Add(this.lblFCMnths);
            this.pnlDetail.Controls.Add(this.lblAvgMnths);
            this.pnlDetail.Controls.Add(this.txtAnnTax);
            this.pnlDetail.Controls.Add(this.txtTaxInc);
            this.pnlDetail.Controls.Add(this.txtForcastMnt);
            this.pnlDetail.Controls.Add(this.txtAvMnt);
            this.pnlDetail.Controls.Add(this.lblTOTCost);
            this.pnlDetail.Controls.Add(this.lblFCOT);
            this.pnlDetail.Controls.Add(this.lblOTArears);
            this.pnlDetail.Controls.Add(this.txtTOTCost);
            this.pnlDetail.Controls.Add(this.lblAvgOT);
            this.pnlDetail.Controls.Add(this.lblAOTCost);
            this.pnlDetail.Controls.Add(this.txtForcastOT);
            this.pnlDetail.Controls.Add(this.txtOTArear);
            this.pnlDetail.Controls.Add(this.txtAvgOT);
            this.pnlDetail.Controls.Add(this.txtOTCost);
            this.pnlDetail.Controls.Add(this.label2);
            this.pnlDetail.Controls.Add(this.label1);
            this.pnlDetail.Controls.Add(this.lblIncBonus);
            this.pnlDetail.Controls.Add(this.lblSLT);
            this.pnlDetail.Controls.Add(this.lblGovtAllow);
            this.pnlDetail.Controls.Add(this.txtTaxPeriod2);
            this.pnlDetail.Controls.Add(this.dgvTDASR);
            this.pnlDetail.Controls.Add(this.dgvDT);
            this.pnlDetail.Controls.Add(this.txtEducation);
            this.pnlDetail.Controls.Add(this.lbtnPNo);
            this.pnlDetail.Controls.Add(this.lblPyrDate);
            this.pnlDetail.Controls.Add(this.lblGHABonus);
            this.pnlDetail.Controls.Add(this.lblTaxPkg);
            this.pnlDetail.Controls.Add(this.txtAccNum);
            this.pnlDetail.Controls.Add(this.lblTP);
            this.pnlDetail.Controls.Add(this.lblTaxAllow);
            this.pnlDetail.Controls.Add(this.lblJDate);
            this.pnlDetail.Controls.Add(this.txtNatTaxNum);
            this.pnlDetail.Controls.Add(this.txtPersNo);
            this.pnlDetail.Controls.Add(this.txtAnnBasic);
            this.pnlDetail.Controls.Add(this.lblAnnPkg);
            this.pnlDetail.Controls.Add(this.txtSalArear);
            this.pnlDetail.Controls.Add(this.txtTotal);
            this.pnlDetail.Controls.Add(this.txtTaxAllow);
            this.pnlDetail.Controls.Add(this.txtRVPTrans);
            this.pnlDetail.Controls.Add(this.lblAnnBasic);
            this.pnlDetail.Controls.Add(this.lblRVPTrans);
            this.pnlDetail.Controls.Add(this.lblLevel);
            this.pnlDetail.Controls.Add(this.lblSalArears);
            this.pnlDetail.Controls.Add(this.lblTotal);
            this.pnlDetail.Controls.Add(this.txtAnnPkge);
            this.pnlDetail.Controls.Add(this.txtLastName);
            this.pnlDetail.Controls.Add(this.txtJoinDate);
            this.pnlDetail.Controls.Add(this.txtGEID);
            this.pnlDetail.Controls.Add(this.txtIncrDate);
            this.pnlDetail.Controls.Add(this.lblCategory);
            this.pnlDetail.Controls.Add(this.txtCategory);
            this.pnlDetail.Controls.Add(this.txtLevel);
            this.pnlDetail.Controls.Add(this.lblIncrDate);
            this.pnlDetail.Controls.Add(this.txtPayrollDate);
            this.pnlDetail.Controls.Add(this.lblPromoDate);
            this.pnlDetail.Controls.Add(this.txtPromoDate);
            this.pnlDetail.Controls.Add(this.txtTaxPeriod);
            this.pnlDetail.Controls.Add(this.txtFunctional);
            this.pnlDetail.Controls.Add(this.lbLSlash);
            this.pnlDetail.Controls.Add(this.txtDesig);
            this.pnlDetail.Controls.Add(this.txtFirstName);
            this.pnlDetail.Controls.Add(this.lblDesig);
            this.pnlDetail.Controls.Add(this.lblPrNum);
            this.pnlDetail.DataManager = "iCORE.Common.CommonDataManager";
            this.pnlDetail.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.pnlDetail.DependentPanels = null;
            this.pnlDetail.DisableDependentLoad = false;
            this.pnlDetail.EnableDelete = true;
            this.pnlDetail.EnableInsert = true;
            this.pnlDetail.EnableQuery = false;
            this.pnlDetail.EnableUpdate = true;
            this.pnlDetail.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.pnlDetail.Location = new System.Drawing.Point(8, 57);
            this.pnlDetail.MasterPanel = null;
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.pnlDetail.Size = new System.Drawing.Size(670, 515);
            this.pnlDetail.SPName = "CHRIS_SP_PERSONNEL_MANAGER";
            this.pnlDetail.TabIndex = 0;
            this.pnlDetail.TabStop = true;
            // 
            // txtForcastAll
            // 
            this.txtForcastAll.AllowSpace = true;
            this.txtForcastAll.AssociatedLookUpName = "";
            this.txtForcastAll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtForcastAll.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtForcastAll.ContinuationTextBox = null;
            this.txtForcastAll.CustomEnabled = true;
            this.txtForcastAll.DataFieldMapping = "W_FORCAST_ALL";
            this.txtForcastAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForcastAll.GetRecordsOnUpDownKeys = false;
            this.txtForcastAll.IsDate = false;
            this.txtForcastAll.Location = new System.Drawing.Point(402, 324);
            this.txtForcastAll.MaxLength = 30;
            this.txtForcastAll.Name = "txtForcastAll";
            this.txtForcastAll.NumberFormat = "###,###,##0.00";
            this.txtForcastAll.Postfix = "";
            this.txtForcastAll.Prefix = "";
            this.txtForcastAll.ReadOnly = true;
            this.txtForcastAll.Size = new System.Drawing.Size(66, 20);
            this.txtForcastAll.SkipValidation = false;
            this.txtForcastAll.TabIndex = 101;
            this.txtForcastAll.TabStop = false;
            this.txtForcastAll.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblFoAl
            // 
            this.lblFoAl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFoAl.Location = new System.Drawing.Point(353, 328);
            this.lblFoAl.Name = "lblFoAl";
            this.lblFoAl.Size = new System.Drawing.Size(52, 13);
            this.lblFoAl.TabIndex = 100;
            this.lblFoAl.Text = "Fo. Al. :";
            this.lblFoAl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCMTax
            // 
            this.lblCMTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCMTax.Location = new System.Drawing.Point(185, 491);
            this.lblCMTax.Name = "lblCMTax";
            this.lblCMTax.Size = new System.Drawing.Size(105, 18);
            this.lblCMTax.TabIndex = 46;
            this.lblCMTax.Text = "Cur. Month Tax :";
            this.lblCMTax.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCurrMntTax
            // 
            this.txtCurrMntTax.AllowSpace = true;
            this.txtCurrMntTax.AssociatedLookUpName = "";
            this.txtCurrMntTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrMntTax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrMntTax.ContinuationTextBox = null;
            this.txtCurrMntTax.CustomEnabled = true;
            this.txtCurrMntTax.DataFieldMapping = "W_ITAX";
            this.txtCurrMntTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrMntTax.GetRecordsOnUpDownKeys = false;
            this.txtCurrMntTax.IsDate = false;
            this.txtCurrMntTax.Location = new System.Drawing.Point(292, 488);
            this.txtCurrMntTax.MaxLength = 10;
            this.txtCurrMntTax.Name = "txtCurrMntTax";
            this.txtCurrMntTax.NumberFormat = "###,###,##0.00";
            this.txtCurrMntTax.Postfix = "";
            this.txtCurrMntTax.Prefix = "";
            this.txtCurrMntTax.ReadOnly = true;
            this.txtCurrMntTax.Size = new System.Drawing.Size(58, 20);
            this.txtCurrMntTax.SkipValidation = false;
            this.txtCurrMntTax.TabIndex = 36;
            this.txtCurrMntTax.TabStop = false;
            this.txtCurrMntTax.TextType = CrplControlLibrary.TextType.String;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(2, 495);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "Tax Paid Update  :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotalIncome
            // 
            this.lblTotalIncome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalIncome.Location = new System.Drawing.Point(27, 471);
            this.lblTotalIncome.Name = "lblTotalIncome";
            this.lblTotalIncome.Size = new System.Drawing.Size(89, 13);
            this.lblTotalIncome.TabIndex = 42;
            this.lblTotalIncome.Text = "Total Income :";
            this.lblTotalIncome.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTaxPaidUpdate
            // 
            this.txtTaxPaidUpdate.AllowSpace = true;
            this.txtTaxPaidUpdate.AssociatedLookUpName = "";
            this.txtTaxPaidUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxPaidUpdate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxPaidUpdate.ContinuationTextBox = null;
            this.txtTaxPaidUpdate.CustomEnabled = true;
            this.txtTaxPaidUpdate.DataFieldMapping = "W_TAX_PAID";
            this.txtTaxPaidUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxPaidUpdate.GetRecordsOnUpDownKeys = false;
            this.txtTaxPaidUpdate.IsDate = false;
            this.txtTaxPaidUpdate.Location = new System.Drawing.Point(118, 490);
            this.txtTaxPaidUpdate.MaxLength = 10;
            this.txtTaxPaidUpdate.Name = "txtTaxPaidUpdate";
            this.txtTaxPaidUpdate.NumberFormat = "###,###,##0.00";
            this.txtTaxPaidUpdate.Postfix = "";
            this.txtTaxPaidUpdate.Prefix = "";
            this.txtTaxPaidUpdate.ReadOnly = true;
            this.txtTaxPaidUpdate.Size = new System.Drawing.Size(65, 20);
            this.txtTaxPaidUpdate.SkipValidation = false;
            this.txtTaxPaidUpdate.TabIndex = 35;
            this.txtTaxPaidUpdate.TabStop = false;
            this.txtTaxPaidUpdate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTotalIncome
            // 
            this.txtTotalIncome.AllowSpace = true;
            this.txtTotalIncome.AssociatedLookUpName = "";
            this.txtTotalIncome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalIncome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotalIncome.ContinuationTextBox = null;
            this.txtTotalIncome.CustomEnabled = true;
            this.txtTotalIncome.DataFieldMapping = "W_INCOME";
            this.txtTotalIncome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalIncome.GetRecordsOnUpDownKeys = false;
            this.txtTotalIncome.IsDate = false;
            this.txtTotalIncome.Location = new System.Drawing.Point(118, 466);
            this.txtTotalIncome.MaxLength = 10;
            this.txtTotalIncome.Name = "txtTotalIncome";
            this.txtTotalIncome.NumberFormat = "###,###,##0.00";
            this.txtTotalIncome.Postfix = "";
            this.txtTotalIncome.Prefix = "";
            this.txtTotalIncome.ReadOnly = true;
            this.txtTotalIncome.Size = new System.Drawing.Size(65, 20);
            this.txtTotalIncome.SkipValidation = false;
            this.txtTotalIncome.TabIndex = 34;
            this.txtTotalIncome.TabStop = false;
            this.txtTotalIncome.TextType = CrplControlLibrary.TextType.String;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 448);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(357, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "__________________________________________________";
            // 
            // lblTaxRFnd
            // 
            this.lblTaxRFnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxRFnd.Location = new System.Drawing.Point(194, 428);
            this.lblTaxRFnd.Name = "lblTaxRFnd";
            this.lblTaxRFnd.Size = new System.Drawing.Size(81, 13);
            this.lblTaxRFnd.TabIndex = 37;
            this.lblTaxRFnd.Text = "Tax Refund :";
            this.lblTaxRFnd.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAnnTax
            // 
            this.lblAnnTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnnTax.Location = new System.Drawing.Point(196, 403);
            this.lblAnnTax.Name = "lblAnnTax";
            this.lblAnnTax.Size = new System.Drawing.Size(79, 13);
            this.lblAnnTax.TabIndex = 36;
            this.lblAnnTax.Text = "Annual Tax :";
            this.lblAnnTax.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxInc
            // 
            this.lblTaxInc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxInc.Location = new System.Drawing.Point(189, 378);
            this.lblTaxInc.Name = "lblTaxInc";
            this.lblTaxInc.Size = new System.Drawing.Size(86, 13);
            this.lblTaxInc.TabIndex = 38;
            this.lblTaxInc.Text = "Taxable Inc. :";
            this.lblTaxInc.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTaxRfnd
            // 
            this.txtTaxRfnd.AllowSpace = true;
            this.txtTaxRfnd.AssociatedLookUpName = "";
            this.txtTaxRfnd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxRfnd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxRfnd.ContinuationTextBox = null;
            this.txtTaxRfnd.CustomEnabled = true;
            this.txtTaxRfnd.DataFieldMapping = "W_REFUND";
            this.txtTaxRfnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxRfnd.GetRecordsOnUpDownKeys = false;
            this.txtTaxRfnd.IsDate = false;
            this.txtTaxRfnd.Location = new System.Drawing.Point(275, 424);
            this.txtTaxRfnd.MaxLength = 10;
            this.txtTaxRfnd.Name = "txtTaxRfnd";
            this.txtTaxRfnd.NumberFormat = "###,###,##0.00";
            this.txtTaxRfnd.Postfix = "";
            this.txtTaxRfnd.Prefix = "";
            this.txtTaxRfnd.ReadOnly = true;
            this.txtTaxRfnd.Size = new System.Drawing.Size(75, 20);
            this.txtTaxRfnd.SkipValidation = false;
            this.txtTaxRfnd.TabIndex = 33;
            this.txtTaxRfnd.TabStop = false;
            this.txtTaxRfnd.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblFCMnths
            // 
            this.lblFCMnths.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFCMnths.Location = new System.Drawing.Point(185, 353);
            this.lblFCMnths.Name = "lblFCMnths";
            this.lblFCMnths.Size = new System.Drawing.Size(90, 18);
            this.lblFCMnths.TabIndex = 34;
            this.lblFCMnths.Text = "Forcast Mon. :";
            this.lblFCMnths.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAvgMnths
            // 
            this.lblAvgMnths.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgMnths.Location = new System.Drawing.Point(219, 328);
            this.lblAvgMnths.Name = "lblAvgMnths";
            this.lblAvgMnths.Size = new System.Drawing.Size(99, 13);
            this.lblAvgMnths.TabIndex = 30;
            this.lblAvgMnths.Text = "Average Months :";
            this.lblAvgMnths.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAnnTax
            // 
            this.txtAnnTax.AllowSpace = true;
            this.txtAnnTax.AssociatedLookUpName = "";
            this.txtAnnTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnTax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnTax.ContinuationTextBox = null;
            this.txtAnnTax.CustomEnabled = true;
            this.txtAnnTax.DataFieldMapping = "W_A_TAX";
            this.txtAnnTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnTax.GetRecordsOnUpDownKeys = false;
            this.txtAnnTax.IsDate = false;
            this.txtAnnTax.Location = new System.Drawing.Point(275, 399);
            this.txtAnnTax.MaxLength = 10;
            this.txtAnnTax.Name = "txtAnnTax";
            this.txtAnnTax.NumberFormat = "###,###,##0.00";
            this.txtAnnTax.Postfix = "";
            this.txtAnnTax.Prefix = "";
            this.txtAnnTax.ReadOnly = true;
            this.txtAnnTax.Size = new System.Drawing.Size(75, 20);
            this.txtAnnTax.SkipValidation = false;
            this.txtAnnTax.TabIndex = 31;
            this.txtAnnTax.TabStop = false;
            this.txtAnnTax.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTaxInc
            // 
            this.txtTaxInc.AllowSpace = true;
            this.txtTaxInc.AssociatedLookUpName = "";
            this.txtTaxInc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxInc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxInc.ContinuationTextBox = null;
            this.txtTaxInc.CustomEnabled = true;
            this.txtTaxInc.DataFieldMapping = "W_TAXABLE_INC";
            this.txtTaxInc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxInc.GetRecordsOnUpDownKeys = false;
            this.txtTaxInc.IsDate = false;
            this.txtTaxInc.Location = new System.Drawing.Point(275, 374);
            this.txtTaxInc.MaxLength = 10;
            this.txtTaxInc.Name = "txtTaxInc";
            this.txtTaxInc.NumberFormat = "###,###,##0.00";
            this.txtTaxInc.Postfix = "";
            this.txtTaxInc.Prefix = "";
            this.txtTaxInc.ReadOnly = true;
            this.txtTaxInc.Size = new System.Drawing.Size(75, 20);
            this.txtTaxInc.SkipValidation = false;
            this.txtTaxInc.TabIndex = 29;
            this.txtTaxInc.TabStop = false;
            this.txtTaxInc.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtForcastMnt
            // 
            this.txtForcastMnt.AllowSpace = true;
            this.txtForcastMnt.AssociatedLookUpName = "";
            this.txtForcastMnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtForcastMnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtForcastMnt.ContinuationTextBox = null;
            this.txtForcastMnt.CustomEnabled = true;
            this.txtForcastMnt.DataFieldMapping = "W_FORCAST_MONTHS";
            this.txtForcastMnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForcastMnt.GetRecordsOnUpDownKeys = false;
            this.txtForcastMnt.IsDate = false;
            this.txtForcastMnt.Location = new System.Drawing.Point(275, 349);
            this.txtForcastMnt.MaxLength = 10;
            this.txtForcastMnt.Name = "txtForcastMnt";
            this.txtForcastMnt.NumberFormat = "###,###,##0.00";
            this.txtForcastMnt.Postfix = "";
            this.txtForcastMnt.Prefix = "";
            this.txtForcastMnt.ReadOnly = true;
            this.txtForcastMnt.Size = new System.Drawing.Size(75, 20);
            this.txtForcastMnt.SkipValidation = false;
            this.txtForcastMnt.TabIndex = 27;
            this.txtForcastMnt.TabStop = false;
            this.txtForcastMnt.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAvMnt
            // 
            this.txtAvMnt.AllowSpace = true;
            this.txtAvMnt.AssociatedLookUpName = "";
            this.txtAvMnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvMnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvMnt.ContinuationTextBox = null;
            this.txtAvMnt.CustomEnabled = true;
            this.txtAvMnt.DataFieldMapping = "W_OT_MONTHS";
            this.txtAvMnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvMnt.GetRecordsOnUpDownKeys = false;
            this.txtAvMnt.IsDate = false;
            this.txtAvMnt.Location = new System.Drawing.Point(318, 324);
            this.txtAvMnt.MaxLength = 30;
            this.txtAvMnt.Name = "txtAvMnt";
            this.txtAvMnt.NumberFormat = "###,###,##0.00";
            this.txtAvMnt.Postfix = "";
            this.txtAvMnt.Prefix = "";
            this.txtAvMnt.ReadOnly = true;
            this.txtAvMnt.Size = new System.Drawing.Size(32, 20);
            this.txtAvMnt.SkipValidation = false;
            this.txtAvMnt.TabIndex = 25;
            this.txtAvMnt.TabStop = false;
            this.txtAvMnt.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblTOTCost
            // 
            this.lblTOTCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTOTCost.Location = new System.Drawing.Point(7, 435);
            this.lblTOTCost.Name = "lblTOTCost";
            this.lblTOTCost.Size = new System.Drawing.Size(94, 13);
            this.lblTOTCost.TabIndex = 27;
            this.lblTOTCost.Text = "Total OT Cost :";
            this.lblTOTCost.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblFCOT
            // 
            this.lblFCOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFCOT.Location = new System.Drawing.Point(23, 409);
            this.lblFCOT.Name = "lblFCOT";
            this.lblFCOT.Size = new System.Drawing.Size(78, 13);
            this.lblFCOT.TabIndex = 26;
            this.lblFCOT.Text = "Forcast OT :";
            this.lblFCOT.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOTArears
            // 
            this.lblOTArears.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOTArears.Location = new System.Drawing.Point(29, 383);
            this.lblOTArears.Name = "lblOTArears";
            this.lblOTArears.Size = new System.Drawing.Size(72, 13);
            this.lblOTArears.TabIndex = 28;
            this.lblOTArears.Text = "OT Arears :";
            this.lblOTArears.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTOTCost
            // 
            this.txtTOTCost.AllowSpace = true;
            this.txtTOTCost.AssociatedLookUpName = "";
            this.txtTOTCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTOTCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTOTCost.ContinuationTextBox = null;
            this.txtTOTCost.CustomEnabled = true;
            this.txtTOTCost.DataFieldMapping = "W_FORCAST";
            this.txtTOTCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTOTCost.GetRecordsOnUpDownKeys = false;
            this.txtTOTCost.IsDate = false;
            this.txtTOTCost.Location = new System.Drawing.Point(102, 428);
            this.txtTOTCost.MaxLength = 10;
            this.txtTOTCost.Name = "txtTOTCost";
            this.txtTOTCost.NumberFormat = "###,###,##0.00";
            this.txtTOTCost.Postfix = "";
            this.txtTOTCost.Prefix = "";
            this.txtTOTCost.ReadOnly = true;
            this.txtTOTCost.Size = new System.Drawing.Size(81, 20);
            this.txtTOTCost.SkipValidation = false;
            this.txtTOTCost.TabIndex = 32;
            this.txtTOTCost.TabStop = false;
            this.txtTOTCost.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblAvgOT
            // 
            this.lblAvgOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvgOT.Location = new System.Drawing.Point(18, 355);
            this.lblAvgOT.Name = "lblAvgOT";
            this.lblAvgOT.Size = new System.Drawing.Size(83, 13);
            this.lblAvgOT.TabIndex = 24;
            this.lblAvgOT.Text = "Average OT :";
            this.lblAvgOT.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAOTCost
            // 
            this.lblAOTCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAOTCost.Location = new System.Drawing.Point(0, 329);
            this.lblAOTCost.Name = "lblAOTCost";
            this.lblAOTCost.Size = new System.Drawing.Size(101, 13);
            this.lblAOTCost.TabIndex = 20;
            this.lblAOTCost.Text = "Actual OT Cost :";
            this.lblAOTCost.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtForcastOT
            // 
            this.txtForcastOT.AllowSpace = true;
            this.txtForcastOT.AssociatedLookUpName = "";
            this.txtForcastOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtForcastOT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtForcastOT.ContinuationTextBox = null;
            this.txtForcastOT.CustomEnabled = true;
            this.txtForcastOT.DataFieldMapping = "W_FORCAST_OT";
            this.txtForcastOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForcastOT.GetRecordsOnUpDownKeys = false;
            this.txtForcastOT.IsDate = false;
            this.txtForcastOT.Location = new System.Drawing.Point(102, 402);
            this.txtForcastOT.MaxLength = 10;
            this.txtForcastOT.Name = "txtForcastOT";
            this.txtForcastOT.NumberFormat = "###,###,##0.00";
            this.txtForcastOT.Postfix = "";
            this.txtForcastOT.Prefix = "";
            this.txtForcastOT.ReadOnly = true;
            this.txtForcastOT.Size = new System.Drawing.Size(81, 20);
            this.txtForcastOT.SkipValidation = false;
            this.txtForcastOT.TabIndex = 30;
            this.txtForcastOT.TabStop = false;
            this.txtForcastOT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtOTArear
            // 
            this.txtOTArear.AllowSpace = true;
            this.txtOTArear.AssociatedLookUpName = "";
            this.txtOTArear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOTArear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOTArear.ContinuationTextBox = null;
            this.txtOTArear.CustomEnabled = true;
            this.txtOTArear.DataFieldMapping = "W_AREARS";
            this.txtOTArear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOTArear.GetRecordsOnUpDownKeys = false;
            this.txtOTArear.IsDate = false;
            this.txtOTArear.Location = new System.Drawing.Point(101, 376);
            this.txtOTArear.MaxLength = 10;
            this.txtOTArear.Name = "txtOTArear";
            this.txtOTArear.NumberFormat = "###,###,##0.00";
            this.txtOTArear.Postfix = "";
            this.txtOTArear.Prefix = "";
            this.txtOTArear.ReadOnly = true;
            this.txtOTArear.Size = new System.Drawing.Size(82, 20);
            this.txtOTArear.SkipValidation = false;
            this.txtOTArear.TabIndex = 28;
            this.txtOTArear.TabStop = false;
            this.txtOTArear.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAvgOT
            // 
            this.txtAvgOT.AllowSpace = true;
            this.txtAvgOT.AssociatedLookUpName = "";
            this.txtAvgOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvgOT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAvgOT.ContinuationTextBox = null;
            this.txtAvgOT.CustomEnabled = true;
            this.txtAvgOT.DataFieldMapping = "W_AVG";
            this.txtAvgOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvgOT.GetRecordsOnUpDownKeys = false;
            this.txtAvgOT.IsDate = false;
            this.txtAvgOT.Location = new System.Drawing.Point(102, 350);
            this.txtAvgOT.MaxLength = 10;
            this.txtAvgOT.Name = "txtAvgOT";
            this.txtAvgOT.NumberFormat = "###,###,##0.00";
            this.txtAvgOT.Postfix = "";
            this.txtAvgOT.Prefix = "";
            this.txtAvgOT.ReadOnly = true;
            this.txtAvgOT.Size = new System.Drawing.Size(81, 20);
            this.txtAvgOT.SkipValidation = false;
            this.txtAvgOT.TabIndex = 26;
            this.txtAvgOT.TabStop = false;
            this.txtAvgOT.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtOTCost
            // 
            this.txtOTCost.AllowSpace = true;
            this.txtOTCost.AssociatedLookUpName = "";
            this.txtOTCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOTCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOTCost.ContinuationTextBox = null;
            this.txtOTCost.CustomEnabled = true;
            this.txtOTCost.DataFieldMapping = "W_ACTUAL_OT";
            this.txtOTCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOTCost.GetRecordsOnUpDownKeys = false;
            this.txtOTCost.IsDate = false;
            this.txtOTCost.Location = new System.Drawing.Point(102, 324);
            this.txtOTCost.MaxLength = 30;
            this.txtOTCost.Name = "txtOTCost";
            this.txtOTCost.NumberFormat = "###,###,##0.00";
            this.txtOTCost.Postfix = "";
            this.txtOTCost.Prefix = "";
            this.txtOTCost.ReadOnly = true;
            this.txtOTCost.Size = new System.Drawing.Size(111, 20);
            this.txtOTCost.SkipValidation = false;
            this.txtOTCost.TabIndex = 24;
            this.txtOTCost.TabStop = false;
            this.txtOTCost.TextType = CrplControlLibrary.TextType.String;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 306);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(448, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "_______________________________________________________________";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(469, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "__________________________________________________________________";
            // 
            // lblIncBonus
            // 
            this.lblIncBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncBonus.Location = new System.Drawing.Point(273, 264);
            this.lblIncBonus.Name = "lblIncBonus";
            this.lblIncBonus.Size = new System.Drawing.Size(76, 13);
            this.lblIncBonus.TabIndex = 14;
            this.lblIncBonus.Text = "Inc. Bonus :";
            this.lblIncBonus.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSLT
            // 
            this.lblSLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSLT.Location = new System.Drawing.Point(249, 238);
            this.lblSLT.Name = "lblSLT";
            this.lblSLT.Size = new System.Drawing.Size(100, 13);
            this.lblSLT.TabIndex = 14;
            this.lblSLT.Text = "Subs-Loan Tax :";
            this.lblSLT.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblGovtAllow
            // 
            this.lblGovtAllow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGovtAllow.Location = new System.Drawing.Point(245, 212);
            this.lblGovtAllow.Name = "lblGovtAllow";
            this.lblGovtAllow.Size = new System.Drawing.Size(104, 13);
            this.lblGovtAllow.TabIndex = 14;
            this.lblGovtAllow.Text = "Govt Allowance :";
            this.lblGovtAllow.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTaxPeriod2
            // 
            this.txtTaxPeriod2.AllowSpace = true;
            this.txtTaxPeriod2.AssociatedLookUpName = "";
            this.txtTaxPeriod2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxPeriod2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxPeriod2.ContinuationTextBox = null;
            this.txtTaxPeriod2.CustomEnabled = true;
            this.txtTaxPeriod2.DataFieldMapping = "";
            this.txtTaxPeriod2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxPeriod2.GetRecordsOnUpDownKeys = false;
            this.txtTaxPeriod2.IsDate = false;
            this.txtTaxPeriod2.Location = new System.Drawing.Point(384, 36);
            this.txtTaxPeriod2.MaxLength = 4;
            this.txtTaxPeriod2.Name = "txtTaxPeriod2";
            this.txtTaxPeriod2.NumberFormat = "###,###,##0.00";
            this.txtTaxPeriod2.Postfix = "";
            this.txtTaxPeriod2.Prefix = "";
            this.txtTaxPeriod2.Size = new System.Drawing.Size(80, 20);
            this.txtTaxPeriod2.SkipValidation = false;
            this.txtTaxPeriod2.TabIndex = 4;
            this.txtTaxPeriod2.TextType = CrplControlLibrary.TextType.String;
            this.txtTaxPeriod2.Leave += new System.EventHandler(this.txtTaxPeriod2_Leave);
            this.txtTaxPeriod2.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxPeriod2_Validating);
            // 
            // dgvTDASR
            // 
            this.dgvTDASR.AllowUserToAddRows = false;
            this.dgvTDASR.AllowUserToDeleteRows = false;
            this.dgvTDASR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTDASR.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTDASR.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTDASR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTDASR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PR_INC_TYPE,
            this.PR_EFFECTIVE,
            this.PR_ANNUAL_PREVIOUS,
            this.PR_ANNUAL_PRESENT});
            this.dgvTDASR.ColumnToHide = null;
            this.dgvTDASR.ColumnWidth = null;
            this.dgvTDASR.CustomEnabled = true;
            this.dgvTDASR.DisplayColumnWrapper = null;
            this.dgvTDASR.GridDefaultRow = 0;
            this.dgvTDASR.Location = new System.Drawing.Point(361, 346);
            this.dgvTDASR.Name = "dgvTDASR";
            this.dgvTDASR.ReadOnlyColumns = null;
            this.dgvTDASR.RequiredColumns = null;
            this.dgvTDASR.Size = new System.Drawing.Size(303, 160);
            this.dgvTDASR.SkippingColumns = null;
            this.dgvTDASR.TabIndex = 5;
            this.toolTip1.SetToolTip(this.dgvTDASR, "[F7] Tax Slab [F3] Allowance Details  [F6] Exit  [Up/Down] See More Record");
            // 
            // PR_INC_TYPE
            // 
            this.PR_INC_TYPE.DataPropertyName = "PR_INC_TYPE";
            this.PR_INC_TYPE.FillWeight = 20F;
            this.PR_INC_TYPE.HeaderText = "Type";
            this.PR_INC_TYPE.Name = "PR_INC_TYPE";
            this.PR_INC_TYPE.ReadOnly = true;
            this.PR_INC_TYPE.Width = 60;
            // 
            // PR_EFFECTIVE
            // 
            this.PR_EFFECTIVE.DataPropertyName = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.FillWeight = 20F;
            this.PR_EFFECTIVE.HeaderText = "Date";
            this.PR_EFFECTIVE.Name = "PR_EFFECTIVE";
            this.PR_EFFECTIVE.ReadOnly = true;
            this.PR_EFFECTIVE.Width = 59;
            // 
            // PR_ANNUAL_PREVIOUS
            // 
            this.PR_ANNUAL_PREVIOUS.DataPropertyName = "PR_ANNUAL_PREVIOUS";
            this.PR_ANNUAL_PREVIOUS.HeaderText = "Prev ASR";
            this.PR_ANNUAL_PREVIOUS.Name = "PR_ANNUAL_PREVIOUS";
            this.PR_ANNUAL_PREVIOUS.ReadOnly = true;
            this.PR_ANNUAL_PREVIOUS.Width = 58;
            // 
            // PR_ANNUAL_PRESENT
            // 
            this.PR_ANNUAL_PRESENT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PR_ANNUAL_PRESENT.DataPropertyName = "PR_ANNUAL_PRESENT";
            this.PR_ANNUAL_PRESENT.HeaderText = "Present ASR";
            this.PR_ANNUAL_PRESENT.Name = "PR_ANNUAL_PRESENT";
            this.PR_ANNUAL_PRESENT.ReadOnly = true;
            // 
            // dgvDT
            // 
            this.dgvDT.AllowUserToAddRows = false;
            this.dgvDT.AllowUserToDeleteRows = false;
            this.dgvDT.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDT.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDT.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PA_DATE,
            this.PA_INCOME_TAX});
            this.dgvDT.ColumnToHide = null;
            this.dgvDT.ColumnWidth = null;
            this.dgvDT.CustomEnabled = true;
            this.dgvDT.DisplayColumnWrapper = null;
            this.dgvDT.GridDefaultRow = 0;
            this.dgvDT.Location = new System.Drawing.Point(472, 12);
            this.dgvDT.Name = "dgvDT";
            this.dgvDT.ReadOnlyColumns = null;
            this.dgvDT.RequiredColumns = null;
            this.dgvDT.Size = new System.Drawing.Size(192, 330);
            this.dgvDT.SkippingColumns = null;
            this.dgvDT.TabIndex = 99;
            // 
            // PA_DATE
            // 
            this.PA_DATE.DataPropertyName = "PA_DATE";
            this.PA_DATE.FillWeight = 20F;
            this.PA_DATE.HeaderText = "Date";
            this.PA_DATE.Name = "PA_DATE";
            this.PA_DATE.ReadOnly = true;
            // 
            // PA_INCOME_TAX
            // 
            this.PA_INCOME_TAX.DataPropertyName = "PA_INCOME_TAX";
            this.PA_INCOME_TAX.FillWeight = 20F;
            this.PA_INCOME_TAX.HeaderText = "Tax Paid";
            this.PA_INCOME_TAX.Name = "PA_INCOME_TAX";
            this.PA_INCOME_TAX.ReadOnly = true;
            // 
            // txtEducation
            // 
            this.txtEducation.AllowSpace = true;
            this.txtEducation.AssociatedLookUpName = "";
            this.txtEducation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEducation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEducation.ContinuationTextBox = null;
            this.txtEducation.CustomEnabled = true;
            this.txtEducation.DataFieldMapping = "W_INC_BONUS";
            this.txtEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEducation.GetRecordsOnUpDownKeys = false;
            this.txtEducation.IsDate = false;
            this.txtEducation.Location = new System.Drawing.Point(351, 260);
            this.txtEducation.MaxLength = 10;
            this.txtEducation.Name = "txtEducation";
            this.txtEducation.NumberFormat = "###,###,##0.00";
            this.txtEducation.Postfix = "";
            this.txtEducation.Prefix = "";
            this.txtEducation.ReadOnly = true;
            this.txtEducation.Size = new System.Drawing.Size(117, 20);
            this.txtEducation.SkipValidation = false;
            this.txtEducation.TabIndex = 22;
            this.txtEducation.TabStop = false;
            this.txtEducation.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbtnPNo
            // 
            this.lbtnPNo.ActionLOVExists = "PP_NO_EXISTS";
            this.lbtnPNo.ActionType = "PP_LOV";
            this.lbtnPNo.CausesValidation = false;
            this.lbtnPNo.ConditionalFields = "";
            this.lbtnPNo.CustomEnabled = true;
            this.lbtnPNo.DataFieldMapping = "";
            this.lbtnPNo.DependentLovControls = "";
            this.lbtnPNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtnPNo.HiddenColumns = "";
            this.lbtnPNo.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPNo.Image")));
            this.lbtnPNo.LoadDependentEntities = true;
            this.lbtnPNo.Location = new System.Drawing.Point(220, 12);
            this.lbtnPNo.LookUpTitle = null;
            this.lbtnPNo.Name = "lbtnPNo";
            this.lbtnPNo.Size = new System.Drawing.Size(26, 21);
            this.lbtnPNo.SkipValidationOnLeave = false;
            this.lbtnPNo.SPName = "CHRIS_SP_TAXINFORMATION_MANAGER";
            this.lbtnPNo.TabIndex = 1;
            this.lbtnPNo.TabStop = false;
            this.lbtnPNo.UseVisualStyleBackColor = true;
            // 
            // lblPyrDate
            // 
            this.lblPyrDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPyrDate.Location = new System.Drawing.Point(11, 40);
            this.lblPyrDate.Name = "lblPyrDate";
            this.lblPyrDate.Size = new System.Drawing.Size(84, 13);
            this.lblPyrDate.TabIndex = 1;
            this.lblPyrDate.Text = "Payroll Date :";
            this.lblPyrDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblGHABonus
            // 
            this.lblGHABonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHABonus.Location = new System.Drawing.Point(234, 186);
            this.lblGHABonus.Name = "lblGHABonus";
            this.lblGHABonus.Size = new System.Drawing.Size(115, 13);
            this.lblGHABonus.TabIndex = 12;
            this.lblGHABonus.Text = "GHA/+10C Bonus :";
            this.lblGHABonus.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxPkg
            // 
            this.lblTaxPkg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxPkg.Location = new System.Drawing.Point(235, 160);
            this.lblTaxPkg.Name = "lblTaxPkg";
            this.lblTaxPkg.Size = new System.Drawing.Size(114, 13);
            this.lblTaxPkg.TabIndex = 1;
            this.lblTaxPkg.Text = "Taxable Package :";
            this.lblTaxPkg.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAccNum
            // 
            this.txtAccNum.AllowSpace = true;
            this.txtAccNum.AssociatedLookUpName = "";
            this.txtAccNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAccNum.ContinuationTextBox = null;
            this.txtAccNum.CustomEnabled = true;
            this.txtAccNum.DataFieldMapping = "SUBS_LOAN_TAXAMT";
            this.txtAccNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccNum.GetRecordsOnUpDownKeys = false;
            this.txtAccNum.IsDate = false;
            this.txtAccNum.Location = new System.Drawing.Point(351, 234);
            this.txtAccNum.MaxLength = 10;
            this.txtAccNum.Name = "txtAccNum";
            this.txtAccNum.NumberFormat = "###,###,##0.00";
            this.txtAccNum.Postfix = "";
            this.txtAccNum.Prefix = "";
            this.txtAccNum.ReadOnly = true;
            this.txtAccNum.Size = new System.Drawing.Size(117, 20);
            this.txtAccNum.SkipValidation = false;
            this.txtAccNum.TabIndex = 20;
            this.txtAccNum.TabStop = false;
            this.txtAccNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblTP
            // 
            this.lblTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTP.Location = new System.Drawing.Point(182, 40);
            this.lblTP.Name = "lblTP";
            this.lblTP.Size = new System.Drawing.Size(104, 13);
            this.lblTP.TabIndex = 1;
            this.lblTP.Text = "Tax Period (YY) :";
            this.lblTP.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxAllow
            // 
            this.lblTaxAllow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxAllow.Location = new System.Drawing.Point(15, 238);
            this.lblTaxAllow.Name = "lblTaxAllow";
            this.lblTaxAllow.Size = new System.Drawing.Size(94, 13);
            this.lblTaxAllow.TabIndex = 10;
            this.lblTaxAllow.Text = "Taxable Allow :";
            this.lblTaxAllow.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblJDate
            // 
            this.lblJDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJDate.Location = new System.Drawing.Point(174, 66);
            this.lblJDate.Name = "lblJDate";
            this.lblJDate.Size = new System.Drawing.Size(86, 13);
            this.lblJDate.TabIndex = 1;
            this.lblJDate.Text = "Joining Date :";
            this.lblJDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNatTaxNum
            // 
            this.txtNatTaxNum.AllowSpace = true;
            this.txtNatTaxNum.AssociatedLookUpName = "";
            this.txtNatTaxNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNatTaxNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNatTaxNum.ContinuationTextBox = null;
            this.txtNatTaxNum.CustomEnabled = true;
            this.txtNatTaxNum.DataFieldMapping = "W_GOVT_DISP";
            this.txtNatTaxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNatTaxNum.GetRecordsOnUpDownKeys = false;
            this.txtNatTaxNum.IsDate = false;
            this.txtNatTaxNum.Location = new System.Drawing.Point(351, 208);
            this.txtNatTaxNum.MaxLength = 10;
            this.txtNatTaxNum.Name = "txtNatTaxNum";
            this.txtNatTaxNum.NumberFormat = "###,###,##0.00";
            this.txtNatTaxNum.Postfix = "";
            this.txtNatTaxNum.Prefix = "";
            this.txtNatTaxNum.ReadOnly = true;
            this.txtNatTaxNum.Size = new System.Drawing.Size(117, 20);
            this.txtNatTaxNum.SkipValidation = false;
            this.txtNatTaxNum.TabIndex = 18;
            this.txtNatTaxNum.TabStop = false;
            this.txtNatTaxNum.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPersNo
            // 
            this.txtPersNo.AllowSpace = true;
            this.txtPersNo.AssociatedLookUpName = "lbtnPNo";
            this.txtPersNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPersNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPersNo.ContinuationTextBox = null;
            this.txtPersNo.CustomEnabled = true;
            this.txtPersNo.DataFieldMapping = "PR_P_NO";
            this.txtPersNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersNo.GetRecordsOnUpDownKeys = false;
            this.txtPersNo.IsDate = false;
            this.txtPersNo.Location = new System.Drawing.Point(98, 12);
            this.txtPersNo.MaxLength = 6;
            this.txtPersNo.Name = "txtPersNo";
            this.txtPersNo.NumberFormat = "###,###,##0.00";
            this.txtPersNo.Postfix = "";
            this.txtPersNo.Prefix = "";
            this.txtPersNo.Size = new System.Drawing.Size(117, 20);
            this.txtPersNo.SkipValidation = false;
            this.txtPersNo.TabIndex = 0;
            this.txtPersNo.TextType = CrplControlLibrary.TextType.String;
            this.txtPersNo.Leave += new System.EventHandler(this.txtPersNo_Leave);
            this.txtPersNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersNo_KeyPress);
            this.txtPersNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPersNo_Validating);
            // 
            // txtAnnBasic
            // 
            this.txtAnnBasic.AllowSpace = true;
            this.txtAnnBasic.AssociatedLookUpName = "";
            this.txtAnnBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnBasic.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnBasic.ContinuationTextBox = null;
            this.txtAnnBasic.CustomEnabled = true;
            this.txtAnnBasic.DataFieldMapping = "W_ANNUAL_INCOME";
            this.txtAnnBasic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnBasic.GetRecordsOnUpDownKeys = false;
            this.txtAnnBasic.IsDate = false;
            this.txtAnnBasic.Location = new System.Drawing.Point(110, 180);
            this.txtAnnBasic.MaxLength = 10;
            this.txtAnnBasic.Name = "txtAnnBasic";
            this.txtAnnBasic.NumberFormat = "###,###,##0.00";
            this.txtAnnBasic.Postfix = "";
            this.txtAnnBasic.Prefix = "";
            this.txtAnnBasic.ReadOnly = true;
            this.txtAnnBasic.Size = new System.Drawing.Size(117, 20);
            this.txtAnnBasic.SkipValidation = false;
            this.txtAnnBasic.TabIndex = 15;
            this.txtAnnBasic.TabStop = false;
            this.txtAnnBasic.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblAnnPkg
            // 
            this.lblAnnPkg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnnPkg.Location = new System.Drawing.Point(1, 158);
            this.lblAnnPkg.Name = "lblAnnPkg";
            this.lblAnnPkg.Size = new System.Drawing.Size(108, 13);
            this.lblAnnPkg.TabIndex = 8;
            this.lblAnnPkg.Text = "Annual Package :";
            this.lblAnnPkg.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSalArear
            // 
            this.txtSalArear.AllowSpace = true;
            this.txtSalArear.AssociatedLookUpName = "";
            this.txtSalArear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSalArear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSalArear.ContinuationTextBox = null;
            this.txtSalArear.CustomEnabled = true;
            this.txtSalArear.DataFieldMapping = "W_BASIC_AREAR";
            this.txtSalArear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalArear.GetRecordsOnUpDownKeys = false;
            this.txtSalArear.IsDate = false;
            this.txtSalArear.Location = new System.Drawing.Point(110, 286);
            this.txtSalArear.MaxLength = 10;
            this.txtSalArear.Name = "txtSalArear";
            this.txtSalArear.NumberFormat = "###,###,##0.00";
            this.txtSalArear.Postfix = "";
            this.txtSalArear.Prefix = "";
            this.txtSalArear.ReadOnly = true;
            this.txtSalArear.Size = new System.Drawing.Size(117, 20);
            this.txtSalArear.SkipValidation = false;
            this.txtSalArear.TabIndex = 23;
            this.txtSalArear.TabStop = false;
            this.txtSalArear.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTotal
            // 
            this.txtTotal.AllowSpace = true;
            this.txtTotal.AssociatedLookUpName = "";
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTotal.ContinuationTextBox = null;
            this.txtTotal.CustomEnabled = true;
            this.txtTotal.DataFieldMapping = "W_TOTAL";
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.GetRecordsOnUpDownKeys = false;
            this.txtTotal.IsDate = false;
            this.txtTotal.Location = new System.Drawing.Point(110, 260);
            this.txtTotal.MaxLength = 10;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.NumberFormat = "###,###,##0.00";
            this.txtTotal.Postfix = "";
            this.txtTotal.Prefix = "";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(117, 20);
            this.txtTotal.SkipValidation = false;
            this.txtTotal.TabIndex = 21;
            this.txtTotal.TabStop = false;
            this.txtTotal.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTaxAllow
            // 
            this.txtTaxAllow.AllowSpace = true;
            this.txtTaxAllow.AssociatedLookUpName = "";
            this.txtTaxAllow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxAllow.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxAllow.ContinuationTextBox = null;
            this.txtTaxAllow.CustomEnabled = true;
            this.txtTaxAllow.DataFieldMapping = "W_OTHER_ALL";
            this.txtTaxAllow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAllow.GetRecordsOnUpDownKeys = false;
            this.txtTaxAllow.IsDate = false;
            this.txtTaxAllow.Location = new System.Drawing.Point(110, 234);
            this.txtTaxAllow.MaxLength = 10;
            this.txtTaxAllow.Name = "txtTaxAllow";
            this.txtTaxAllow.NumberFormat = "###,###,##0.00";
            this.txtTaxAllow.Postfix = "";
            this.txtTaxAllow.Prefix = "";
            this.txtTaxAllow.ReadOnly = true;
            this.txtTaxAllow.Size = new System.Drawing.Size(117, 20);
            this.txtTaxAllow.SkipValidation = false;
            this.txtTaxAllow.TabIndex = 19;
            this.txtTaxAllow.TabStop = false;
            this.txtTaxAllow.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtRVPTrans
            // 
            this.txtRVPTrans.AllowSpace = true;
            this.txtRVPTrans.AssociatedLookUpName = "";
            this.txtRVPTrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRVPTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRVPTrans.ContinuationTextBox = null;
            this.txtRVPTrans.CustomEnabled = true;
            this.txtRVPTrans.DataFieldMapping = "W_VP";
            this.txtRVPTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRVPTrans.GetRecordsOnUpDownKeys = false;
            this.txtRVPTrans.IsDate = false;
            this.txtRVPTrans.Location = new System.Drawing.Point(110, 208);
            this.txtRVPTrans.MaxLength = 10;
            this.txtRVPTrans.Name = "txtRVPTrans";
            this.txtRVPTrans.NumberFormat = "###,###,##0.00";
            this.txtRVPTrans.Postfix = "";
            this.txtRVPTrans.Prefix = "";
            this.txtRVPTrans.ReadOnly = true;
            this.txtRVPTrans.Size = new System.Drawing.Size(117, 20);
            this.txtRVPTrans.SkipValidation = false;
            this.txtRVPTrans.TabIndex = 17;
            this.txtRVPTrans.TabStop = false;
            this.txtRVPTrans.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblAnnBasic
            // 
            this.lblAnnBasic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnnBasic.Location = new System.Drawing.Point(20, 184);
            this.lblAnnBasic.Name = "lblAnnBasic";
            this.lblAnnBasic.Size = new System.Drawing.Size(89, 13);
            this.lblAnnBasic.TabIndex = 1;
            this.lblAnnBasic.Text = "Annual Basic :";
            this.lblAnnBasic.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRVPTrans
            // 
            this.lblRVPTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRVPTrans.Location = new System.Drawing.Point(11, 212);
            this.lblRVPTrans.Name = "lblRVPTrans";
            this.lblRVPTrans.Size = new System.Drawing.Size(98, 13);
            this.lblRVPTrans.TabIndex = 1;
            this.lblRVPTrans.Text = "VP/RVP Trans :";
            this.lblRVPTrans.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblLevel
            // 
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(49, 93);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(46, 13);
            this.lblLevel.TabIndex = 1;
            this.lblLevel.Text = "Level :";
            this.lblLevel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSalArears
            // 
            this.lblSalArears.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalArears.Location = new System.Drawing.Point(19, 290);
            this.lblSalArears.Name = "lblSalArears";
            this.lblSalArears.Size = new System.Drawing.Size(90, 13);
            this.lblSalArears.TabIndex = 1;
            this.lblSalArears.Text = "Salary Arears :";
            this.lblSalArears.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(65, 264);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(44, 13);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "Total :";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAnnPkge
            // 
            this.txtAnnPkge.AllowSpace = true;
            this.txtAnnPkge.AssociatedLookUpName = "";
            this.txtAnnPkge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnPkge.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnPkge.ContinuationTextBox = null;
            this.txtAnnPkge.CustomEnabled = true;
            this.txtAnnPkge.DataFieldMapping = "W_AN_PACK";
            this.txtAnnPkge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnPkge.GetRecordsOnUpDownKeys = false;
            this.txtAnnPkge.IsDate = false;
            this.txtAnnPkge.Location = new System.Drawing.Point(110, 154);
            this.txtAnnPkge.MaxLength = 10;
            this.txtAnnPkge.Name = "txtAnnPkge";
            this.txtAnnPkge.NumberFormat = "###,###,##0.00";
            this.txtAnnPkge.Postfix = "";
            this.txtAnnPkge.Prefix = "";
            this.txtAnnPkge.ReadOnly = true;
            this.txtAnnPkge.Size = new System.Drawing.Size(117, 20);
            this.txtAnnPkge.SkipValidation = false;
            this.txtAnnPkge.TabIndex = 13;
            this.txtAnnPkge.TabStop = false;
            this.txtAnnPkge.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLastName
            // 
            this.txtLastName.AllowSpace = true;
            this.txtLastName.AssociatedLookUpName = "";
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.ContinuationTextBox = null;
            this.txtLastName.CustomEnabled = true;
            this.txtLastName.DataFieldMapping = "PR_LAST_NAME";
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.GetRecordsOnUpDownKeys = false;
            this.txtLastName.IsDate = false;
            this.txtLastName.Location = new System.Drawing.Point(361, 12);
            this.txtLastName.MaxLength = 10;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.NumberFormat = "###,###,##0.00";
            this.txtLastName.Postfix = "";
            this.txtLastName.Prefix = "";
            this.txtLastName.ReadOnly = true;
            this.txtLastName.Size = new System.Drawing.Size(105, 20);
            this.txtLastName.SkipValidation = false;
            this.txtLastName.TabIndex = 34;
            this.txtLastName.TabStop = false;
            this.txtLastName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtJoinDate
            // 
            this.txtJoinDate.AllowSpace = true;
            this.txtJoinDate.AssociatedLookUpName = "";
            this.txtJoinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJoinDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJoinDate.ContinuationTextBox = null;
            this.txtJoinDate.CustomEnabled = true;
            this.txtJoinDate.DataFieldMapping = "W_JOIN_DATE";
            this.txtJoinDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJoinDate.GetRecordsOnUpDownKeys = false;
            this.txtJoinDate.IsDate = false;
            this.txtJoinDate.Location = new System.Drawing.Point(263, 63);
            this.txtJoinDate.MaxLength = 10;
            this.txtJoinDate.Name = "txtJoinDate";
            this.txtJoinDate.NumberFormat = "###,###,##0.00";
            this.txtJoinDate.Postfix = "";
            this.txtJoinDate.Prefix = "";
            this.txtJoinDate.ReadOnly = true;
            this.txtJoinDate.Size = new System.Drawing.Size(117, 20);
            this.txtJoinDate.SkipValidation = false;
            this.txtJoinDate.TabIndex = 8;
            this.txtJoinDate.TabStop = false;
            this.txtJoinDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtGEID
            // 
            this.txtGEID.AllowSpace = true;
            this.txtGEID.AssociatedLookUpName = "";
            this.txtGEID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGEID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGEID.ContinuationTextBox = null;
            this.txtGEID.CustomEnabled = true;
            this.txtGEID.DataFieldMapping = "W_10_BONUS";
            this.txtGEID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGEID.GetRecordsOnUpDownKeys = false;
            this.txtGEID.IsDate = false;
            this.txtGEID.Location = new System.Drawing.Point(351, 182);
            this.txtGEID.MaxLength = 10;
            this.txtGEID.Name = "txtGEID";
            this.txtGEID.NumberFormat = "###,###,##0.00";
            this.txtGEID.Postfix = "";
            this.txtGEID.Prefix = "";
            this.txtGEID.ReadOnly = true;
            this.txtGEID.Size = new System.Drawing.Size(117, 20);
            this.txtGEID.SkipValidation = false;
            this.txtGEID.TabIndex = 16;
            this.txtGEID.TabStop = false;
            this.txtGEID.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtIncrDate
            // 
            this.txtIncrDate.AllowSpace = true;
            this.txtIncrDate.AssociatedLookUpName = "";
            this.txtIncrDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIncrDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIncrDate.ContinuationTextBox = null;
            this.txtIncrDate.CustomEnabled = true;
            this.txtIncrDate.DataFieldMapping = "W_INC_EFF";
            this.txtIncrDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIncrDate.GetRecordsOnUpDownKeys = false;
            this.txtIncrDate.IsDate = false;
            this.txtIncrDate.Location = new System.Drawing.Point(263, 115);
            this.txtIncrDate.MaxLength = 10;
            this.txtIncrDate.Name = "txtIncrDate";
            this.txtIncrDate.NumberFormat = "###,###,##0.00";
            this.txtIncrDate.Postfix = "";
            this.txtIncrDate.Prefix = "";
            this.txtIncrDate.ReadOnly = true;
            this.txtIncrDate.Size = new System.Drawing.Size(117, 20);
            this.txtIncrDate.SkipValidation = false;
            this.txtIncrDate.TabIndex = 12;
            this.txtIncrDate.TabStop = false;
            this.txtIncrDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblCategory
            // 
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(30, 119);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(65, 13);
            this.lblCategory.TabIndex = 1;
            this.lblCategory.Text = "Category :";
            this.lblCategory.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCategory
            // 
            this.txtCategory.AllowSpace = true;
            this.txtCategory.AssociatedLookUpName = "";
            this.txtCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCategory.ContinuationTextBox = null;
            this.txtCategory.CustomEnabled = true;
            this.txtCategory.DataFieldMapping = "W_CATE";
            this.txtCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategory.GetRecordsOnUpDownKeys = false;
            this.txtCategory.IsDate = false;
            this.txtCategory.Location = new System.Drawing.Point(98, 115);
            this.txtCategory.MaxLength = 1;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.NumberFormat = "###,###,##0.00";
            this.txtCategory.Postfix = "";
            this.txtCategory.Prefix = "";
            this.txtCategory.ReadOnly = true;
            this.txtCategory.Size = new System.Drawing.Size(43, 20);
            this.txtCategory.SkipValidation = false;
            this.txtCategory.TabIndex = 11;
            this.txtCategory.TabStop = false;
            this.txtCategory.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "W_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(98, 89);
            this.txtLevel.MaxLength = 1;
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(43, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 9;
            this.txtLevel.TabStop = false;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblIncrDate
            // 
            this.lblIncrDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncrDate.Location = new System.Drawing.Point(158, 119);
            this.lblIncrDate.Name = "lblIncrDate";
            this.lblIncrDate.Size = new System.Drawing.Size(102, 13);
            this.lblIncrDate.TabIndex = 1;
            this.lblIncrDate.Text = "Increment Date :";
            this.lblIncrDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPayrollDate
            // 
            this.txtPayrollDate.AllowSpace = true;
            this.txtPayrollDate.AssociatedLookUpName = "";
            this.txtPayrollDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayrollDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPayrollDate.ContinuationTextBox = null;
            this.txtPayrollDate.CustomEnabled = true;
            this.txtPayrollDate.DataFieldMapping = "PA_DATE";
            this.txtPayrollDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayrollDate.GetRecordsOnUpDownKeys = false;
            this.txtPayrollDate.IsDate = false;
            this.txtPayrollDate.Location = new System.Drawing.Point(98, 36);
            this.txtPayrollDate.MaxLength = 10;
            this.txtPayrollDate.Name = "txtPayrollDate";
            this.txtPayrollDate.NumberFormat = "###,###,##0.00";
            this.txtPayrollDate.Postfix = "";
            this.txtPayrollDate.Prefix = "";
            this.txtPayrollDate.Size = new System.Drawing.Size(80, 20);
            this.txtPayrollDate.SkipValidation = false;
            this.txtPayrollDate.TabIndex = 2;
            this.txtPayrollDate.TextType = CrplControlLibrary.TextType.String;
            this.toolTip1.SetToolTip(this.txtPayrollDate, "Enter Last Payroll Date (DD/MM/YYYY)");
            this.txtPayrollDate.Leave += new System.EventHandler(this.txtPayrollDate_Leave);
            // 
            // lblPromoDate
            // 
            this.lblPromoDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromoDate.Location = new System.Drawing.Point(158, 93);
            this.lblPromoDate.Name = "lblPromoDate";
            this.lblPromoDate.Size = new System.Drawing.Size(102, 13);
            this.lblPromoDate.TabIndex = 1;
            this.lblPromoDate.Text = "Promotion Date :";
            this.lblPromoDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPromoDate
            // 
            this.txtPromoDate.AllowSpace = true;
            this.txtPromoDate.AssociatedLookUpName = "";
            this.txtPromoDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPromoDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPromoDate.ContinuationTextBox = null;
            this.txtPromoDate.CustomEnabled = true;
            this.txtPromoDate.DataFieldMapping = "W_PR_DATE";
            this.txtPromoDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPromoDate.GetRecordsOnUpDownKeys = false;
            this.txtPromoDate.IsDate = false;
            this.txtPromoDate.Location = new System.Drawing.Point(263, 89);
            this.txtPromoDate.MaxLength = 10;
            this.txtPromoDate.Name = "txtPromoDate";
            this.txtPromoDate.NumberFormat = "###,###,##0.00";
            this.txtPromoDate.Postfix = "";
            this.txtPromoDate.Prefix = "";
            this.txtPromoDate.ReadOnly = true;
            this.txtPromoDate.Size = new System.Drawing.Size(117, 20);
            this.txtPromoDate.SkipValidation = false;
            this.txtPromoDate.TabIndex = 10;
            this.txtPromoDate.TabStop = false;
            this.txtPromoDate.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTaxPeriod
            // 
            this.txtTaxPeriod.AllowSpace = true;
            this.txtTaxPeriod.AssociatedLookUpName = "";
            this.txtTaxPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxPeriod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTaxPeriod.ContinuationTextBox = null;
            this.txtTaxPeriod.CustomEnabled = true;
            this.txtTaxPeriod.DataFieldMapping = "";
            this.txtTaxPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxPeriod.GetRecordsOnUpDownKeys = false;
            this.txtTaxPeriod.IsDate = false;
            this.txtTaxPeriod.Location = new System.Drawing.Point(289, 36);
            this.txtTaxPeriod.MaxLength = 4;
            this.txtTaxPeriod.Name = "txtTaxPeriod";
            this.txtTaxPeriod.NumberFormat = "###,###,##0.00";
            this.txtTaxPeriod.Postfix = "";
            this.txtTaxPeriod.Prefix = "";
            this.txtTaxPeriod.Size = new System.Drawing.Size(80, 20);
            this.txtTaxPeriod.SkipValidation = false;
            this.txtTaxPeriod.TabIndex = 3;
            this.txtTaxPeriod.TextType = CrplControlLibrary.TextType.String;
            this.txtTaxPeriod.Leave += new System.EventHandler(this.txtTaxPeriod_Leave);
            this.txtTaxPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxPeriod_Validating);
            // 
            // txtFunctional
            // 
            this.txtFunctional.AllowSpace = true;
            this.txtFunctional.AssociatedLookUpName = "";
            this.txtFunctional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFunctional.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFunctional.ContinuationTextBox = null;
            this.txtFunctional.CustomEnabled = true;
            this.txtFunctional.DataFieldMapping = "W_TAX_PACK";
            this.txtFunctional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFunctional.GetRecordsOnUpDownKeys = false;
            this.txtFunctional.IsDate = false;
            this.txtFunctional.Location = new System.Drawing.Point(351, 156);
            this.txtFunctional.MaxLength = 30;
            this.txtFunctional.Name = "txtFunctional";
            this.txtFunctional.NumberFormat = "###,###,##0.00";
            this.txtFunctional.Postfix = "";
            this.txtFunctional.Prefix = "";
            this.txtFunctional.ReadOnly = true;
            this.txtFunctional.Size = new System.Drawing.Size(117, 20);
            this.txtFunctional.SkipValidation = false;
            this.txtFunctional.TabIndex = 14;
            this.txtFunctional.TabStop = false;
            this.txtFunctional.TextType = CrplControlLibrary.TextType.String;
            // 
            // lbLSlash
            // 
            this.lbLSlash.AutoSize = true;
            this.lbLSlash.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLSlash.Location = new System.Drawing.Point(367, 40);
            this.lbLSlash.Name = "lbLSlash";
            this.lbLSlash.Size = new System.Drawing.Size(21, 13);
            this.lbLSlash.TabIndex = 6;
            this.lbLSlash.Text = " / ";
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "W_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(98, 62);
            this.txtDesig.MaxLength = 10;
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(43, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 7;
            this.txtDesig.TabStop = false;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AllowSpace = true;
            this.txtFirstName.AssociatedLookUpName = "";
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.ContinuationTextBox = null;
            this.txtFirstName.CustomEnabled = true;
            this.txtFirstName.DataFieldMapping = "PR_FIRST_NAME";
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.GetRecordsOnUpDownKeys = false;
            this.txtFirstName.IsDate = false;
            this.txtFirstName.Location = new System.Drawing.Point(252, 12);
            this.txtFirstName.MaxLength = 10;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.NumberFormat = "###,###,##0.00";
            this.txtFirstName.Postfix = "";
            this.txtFirstName.Prefix = "";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(108, 20);
            this.txtFirstName.SkipValidation = false;
            this.txtFirstName.TabIndex = 33;
            this.txtFirstName.TextType = CrplControlLibrary.TextType.String;
            // 
            // lblDesig
            // 
            this.lblDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesig.Location = new System.Drawing.Point(48, 66);
            this.lblDesig.Name = "lblDesig";
            this.lblDesig.Size = new System.Drawing.Size(47, 13);
            this.lblDesig.TabIndex = 1;
            this.lblDesig.Text = "Desig :";
            this.lblDesig.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPrNum
            // 
            this.lblPrNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrNum.Location = new System.Drawing.Point(0, 16);
            this.lblPrNum.Name = "lblPrNum";
            this.lblPrNum.Size = new System.Drawing.Size(95, 13);
            this.lblPrNum.TabIndex = 1;
            this.lblPrNum.Text = "Personnel No. :";
            this.lblPrNum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxQueryHeader
            // 
            this.lblTaxQueryHeader.AutoSize = true;
            this.lblTaxQueryHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxQueryHeader.Location = new System.Drawing.Point(17, 35);
            this.lblTaxQueryHeader.Name = "lblTaxQueryHeader";
            this.lblTaxQueryHeader.Size = new System.Drawing.Size(674, 18);
            this.lblTaxQueryHeader.TabIndex = 70;
            this.lblTaxQueryHeader.Text = "_____________________________     TAX QUERY     _____________________________";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.Location = new System.Drawing.Point(278, 575);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(54, 13);
            this.lblExit.TabIndex = 72;
            this.lblExit.Text = "[F6] Exit";
            // 
            // lblSave
            // 
            this.lblSave.AutoSize = true;
            this.lblSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSave.Location = new System.Drawing.Point(383, 575);
            this.lblSave.Name = "lblSave";
            this.lblSave.Size = new System.Drawing.Size(69, 13);
            this.lblSave.TabIndex = 72;
            this.lblSave.Text = "[F10] Save";
            // 
            // txtMSG
            // 
            this.txtMSG.AllowSpace = true;
            this.txtMSG.AssociatedLookUpName = "";
            this.txtMSG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMSG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMSG.ContinuationTextBox = null;
            this.txtMSG.CustomEnabled = true;
            this.txtMSG.DataFieldMapping = "MSG";
            this.txtMSG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMSG.GetRecordsOnUpDownKeys = false;
            this.txtMSG.IsDate = false;
            this.txtMSG.Location = new System.Drawing.Point(12, 590);
            this.txtMSG.MaxLength = 10;
            this.txtMSG.Name = "txtMSG";
            this.txtMSG.NumberFormat = "###,###,##0.00";
            this.txtMSG.Postfix = "";
            this.txtMSG.Prefix = "";
            this.txtMSG.ReadOnly = true;
            this.txtMSG.Size = new System.Drawing.Size(330, 20);
            this.txtMSG.SkipValidation = false;
            this.txtMSG.TabIndex = 37;
            this.txtMSG.TabStop = false;
            this.txtMSG.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtTEST1
            // 
            this.txtTEST1.AllowSpace = true;
            this.txtTEST1.AssociatedLookUpName = "";
            this.txtTEST1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTEST1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTEST1.ContinuationTextBox = null;
            this.txtTEST1.CustomEnabled = true;
            this.txtTEST1.DataFieldMapping = "TEST1";
            this.txtTEST1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTEST1.GetRecordsOnUpDownKeys = false;
            this.txtTEST1.IsDate = false;
            this.txtTEST1.Location = new System.Drawing.Point(348, 590);
            this.txtTEST1.MaxLength = 10;
            this.txtTEST1.Name = "txtTEST1";
            this.txtTEST1.NumberFormat = "###,###,##0.00";
            this.txtTEST1.Postfix = "";
            this.txtTEST1.Prefix = "";
            this.txtTEST1.ReadOnly = true;
            this.txtTEST1.Size = new System.Drawing.Size(330, 20);
            this.txtTEST1.SkipValidation = false;
            this.txtTEST1.TabIndex = 38;
            this.txtTEST1.TabStop = false;
            this.txtTEST1.TextType = CrplControlLibrary.TextType.String;
            // 
            // CHRIS_Query_TaxInformationQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(684, 673);
            this.Controls.Add(this.txtTEST1);
            this.Controls.Add(this.txtMSG);
            this.Controls.Add(this.lblSave);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.lblTaxQueryHeader);
            this.Controls.Add(this.pnlDetail);
            this.CurrentPanelBlock = "pnlDetail";
            this.Name = "CHRIS_Query_TaxInformationQuery";
            this.ShowBottomBar = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - Tax Information Query";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pnlDetail, 0);
            this.Controls.SetChildIndex(this.lblTaxQueryHeader, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblExit, 0);
            this.Controls.SetChildIndex(this.lblSave, 0);
            this.Controls.SetChildIndex(this.txtMSG, 0);
            this.Controls.SetChildIndex(this.txtTEST1, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTDASR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private iCORE.COMMON.SLCONTROLS.SLPanelSimple pnlDetail;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvDT;
        private System.Windows.Forms.Label lblGovtAllow;
        private CrplControlLibrary.SLTextBox txtEducation;
        private System.Windows.Forms.Label lblGHABonus;
        private CrplControlLibrary.SLTextBox txtAccNum;
        private System.Windows.Forms.Label lblTaxAllow;
        private CrplControlLibrary.SLTextBox txtNatTaxNum;
        private System.Windows.Forms.Label lblAnnPkg;
        private CrplControlLibrary.SLTextBox txtAnnPkge;
        private System.Windows.Forms.Label lbLSlash;
        private CrplControlLibrary.SLTextBox txtGEID;
        private CrplControlLibrary.LookupButton lbtnPNo;
        private System.Windows.Forms.Label lblAnnBasic;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblIncrDate;
        private System.Windows.Forms.Label lblPyrDate;
        private System.Windows.Forms.Label lblPromoDate;
        private System.Windows.Forms.Label lblTP;
        private System.Windows.Forms.Label lblTaxPkg;
        private System.Windows.Forms.Label lblJDate;
        private CrplControlLibrary.SLTextBox txtPersNo;
        private System.Windows.Forms.Label lblLevel;
        private CrplControlLibrary.SLTextBox txtLastName;
        private System.Windows.Forms.Label lblCategory;
        private CrplControlLibrary.SLTextBox txtCategory;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtRVPTrans;
        private CrplControlLibrary.SLTextBox txtAnnBasic;
        private CrplControlLibrary.SLTextBox txtIncrDate;
        private CrplControlLibrary.SLTextBox txtPayrollDate;
        private CrplControlLibrary.SLTextBox txtJoinDate;
        private CrplControlLibrary.SLTextBox txtPromoDate;
        private CrplControlLibrary.SLTextBox txtFunctional;
        private CrplControlLibrary.SLTextBox txtTaxPeriod;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtFirstName;
        private System.Windows.Forms.Label lblDesig;
        private System.Windows.Forms.Label lblPrNum;
        private System.Windows.Forms.Label lblTaxQueryHeader;
        private CrplControlLibrary.SLTextBox txtTaxPeriod2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label lblSave;
        private CrplControlLibrary.SLTextBox txtSalArear;
        private CrplControlLibrary.SLTextBox txtTotal;
        private CrplControlLibrary.SLTextBox txtTaxAllow;
        private System.Windows.Forms.Label lblRVPTrans;
        private System.Windows.Forms.Label lblSalArears;
        private System.Windows.Forms.Label lblIncBonus;
        private System.Windows.Forms.Label lblSLT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTaxRFnd;
        private System.Windows.Forms.Label lblAnnTax;
        private System.Windows.Forms.Label lblTaxInc;
        private CrplControlLibrary.SLTextBox txtTaxRfnd;
        private System.Windows.Forms.Label lblFCMnths;
        private System.Windows.Forms.Label lblAvgMnths;
        private CrplControlLibrary.SLTextBox txtAnnTax;
        private CrplControlLibrary.SLTextBox txtTaxInc;
        private CrplControlLibrary.SLTextBox txtForcastMnt;
        private CrplControlLibrary.SLTextBox txtAvMnt;
        private System.Windows.Forms.Label lblTOTCost;
        private System.Windows.Forms.Label lblFCOT;
        private System.Windows.Forms.Label lblOTArears;
        private CrplControlLibrary.SLTextBox txtTOTCost;
        private System.Windows.Forms.Label lblAvgOT;
        private System.Windows.Forms.Label lblAOTCost;
        private CrplControlLibrary.SLTextBox txtForcastOT;
        private CrplControlLibrary.SLTextBox txtOTArear;
        private CrplControlLibrary.SLTextBox txtAvgOT;
        private CrplControlLibrary.SLTextBox txtOTCost;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView dgvTDASR;
        private System.Windows.Forms.Label lblCMTax;
        private CrplControlLibrary.SLTextBox txtCurrMntTax;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblTotalIncome;
        private CrplControlLibrary.SLTextBox txtTaxPaidUpdate;
        private CrplControlLibrary.SLTextBox txtTotalIncome;
        private System.Windows.Forms.Label label13;
        private CrplControlLibrary.SLTextBox txtMSG;
        private CrplControlLibrary.SLTextBox txtTEST1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PA_INCOME_TAX;
        private CrplControlLibrary.SLTextBox txtForcastAll;
        private System.Windows.Forms.Label lblFoAl;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_INC_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_EFFECTIVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_ANNUAL_PREVIOUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PR_ANNUAL_PRESENT;
    }
}