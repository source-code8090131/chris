using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iCORE.COMMON.SLCONTROLS;
using iCORE.CHRIS.DATAOBJECTS;
using iCORE.CHRISCOMMON.PRESENTATIONOBJECTS;
using iCORE.Common;
using System.Data.Common;
using System.Reflection;
using CrplControlLibrary;
using System.Configuration;
using System.Collections;

using iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES;

namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    public partial class CHRIS_Query_DeptWiseQuery : ChrisMasterDetailForm
    //ChrisTabularForm
    {

        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();
        CmnDataManager cmnDm = new CmnDataManager();
        
        #region Methods
        
        public CHRIS_Query_DeptWiseQuery()
        {
            InitializeComponent();
        }

        public CHRIS_Query_DeptWiseQuery(XMS.PRESENTATIONOBJECTS.FORMS.MainMenu mainmenu, XMS.DATAOBJECTS.ConnectionBean connbean_obj)
            : base(mainmenu, connbean_obj)
        {
            InitializeComponent();
            lblUserName.Text += " " + this.UserName;
            txtUser.Text = this.userID;


        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtOption.Visible = false;

            List<SLPanel> lstDependentPanels = new List<SLPanel>();
            lstDependentPanels.Add(PnlPersonnel);

            PnlDepart.DependentPanels = lstDependentPanels;


            this.IndependentPanels.Add(PnlDepart);

            this.pnlHead.SendToBack();

            this.txtUser.Text = this.userID;
            DateTime now = this.CurrentDate;
            this.txtDate.Text = now.ToString("dd/MM/yyyy");
            this.txtLocation.Text = this.CurrentLocation;

            slDataGridView1.AllowUserToOrderColumns = false;


        }

        protected override bool Quit()
        {
            foreach (Control tx in this.PnlDepart.Controls)
            {

                if (tx != null)
                    if (tx is SLTextBox)
                        if (tx.Text != string.Empty)
                            this.FunctionConfig.CurrentOption = Function.Query;

            }
            base.Quit();
            this.txtDesc.Text = "";
            lblUserName.Text += " " + this.UserName;
            this.txtUser.Text = this.userID;
            DateTime now = this.CurrentDate;
            this.txtDate.Text = now.ToString("dd/MM/yyyy");
            this.txtLocation.Text = this.CurrentLocation;
            this.txtBranchCode.Focus();
            return false;
        }
                
        protected override void CommonOnLoadMethods()
        {
            base.CommonOnLoadMethods();
            tbtAdd.Visible = false;
            tbtDelete.Visible = false;
            tbtList.Visible = false;
            tbtSave.Visible = false;
        }
        
        public override void DoToolbarActions(Control.ControlCollection ctrlsCollection, string actionType)
        {
            switch (actionType)
            {
                case "Close":
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    break;
                case "Cancel":
                    ///base.DoToolbarActions(ctrlsCollection, actionType);
                    this.ClearForm(PnlDepart.Controls);
                    this.ClearForm(PnlPersonnel.Controls);
                    txtBranchCode.Focus();
                    break;
                case "Search":
                    base.DoToolbarActions(ctrlsCollection, actionType);
                    break;
            }
            base.DoToolbarActions(ctrlsCollection, actionType);
        }

        #endregion

        #region Events 
        private void txtBranchCode_Validating(object sender, CancelEventArgs e)
        {
            //if (txtBranchCode.Text == "KHI")
            //{
            //    txtDesc.Text = "KARACHI";
            //}
            //else if (txtBranchCode.Text == "LHR")
            //{
            //    txtDesc.Text = "LAHORE";

            //}
            //else if (txtBranchCode.Text == "RWP")
            //{
            //    txtDesc.Text = "RAWALPINDI";
            //}
            //else if (txtBranchCode.Text == "FBD")
            //{
            //    txtDesc.Text = "FAISALABAD";
            //}
            //// DestFormat != string.Empty && DestName != string.Empty
            //else if (txtBranchCode.Text == "ALL")
            //{
            //    txtDesc.Text = "ALL PAKISTAN";
            //}
            //else if (txtBranchCode.Text != "KHI" && txtBranchCode.Text != "LHR" && txtBranchCode.Text != "RWP" && txtBranchCode.Text != "FBD" && txtBranchCode.Text != "ALL" && txtDate.Text!="" )
            //{
            //    MessageBox.Show("Invalid Branch Code Enter ......", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    txtBranchCode.Focus();
            //    e.Cancel = true;
            //}
        }

        private void txtSeg_validating(object sender, CancelEventArgs e)
        {
            if (txtSeg.Text != "GF" && txtSeg.Text != "GCB")
            {
                MessageBox.Show("Valid Segment is [GF] or [GCB]......", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                e.Cancel = true;

            }
        }

        private void CHRIS_Query_DeptWiseQuery_BeforeLOVShown(iCORE.Common.PRESENTATIONOBJECTS.Cmn.Cmn_ListOfRecords LOV, string actionType)
        {
            //PersonnelSearchCommand ent = (PersonnelSearchCommand)this.PnlDepart.CurrentBusinessEntity;
            //if (ent != null)
            //{
            //    ent.W_BRANCH = this.txtBranchCode.Text;
            //}   
        }

        private void CHRIS_Query_DeptWiseQuery_AfterLOVSelection(DataRow selectedRow, string actionType)
        {
            //if (slDataGridView1.Rows.Count == 0)
            //{
            //    MessageBox.Show("No Record Found In This Branch/Segment/Department..'");
            //    // slDataGridView1.Focus();

            //}
        }

        private void txtBranchCode_Leave(object sender, EventArgs e)
        {
            if (txtBranchCode.Text == "KHI")
            {

                txtDesc.Text = "KARACHI";
            }
            else if (txtBranchCode.Text == "LHR")
            {
                txtDesc.Text = "LAHORE";
            }
            else if (txtBranchCode.Text == "RWP")
            {

                txtDesc.Text = "RAWALPINDI";
            }
            else if (txtBranchCode.Text == "FBD")
            {

                txtDesc.Text = "FAISALABAD";
            }
            else if (txtBranchCode.Text == string.Empty)
            {

                txtDesc.Text = "ALL PAKISTAN";
            }
            else if (txtBranchCode.Text != "KHI" && txtBranchCode.Text != "LHR" && txtBranchCode.Text != "RWP" && txtBranchCode.Text != "FBD" && txtBranchCode.Text != "ALL" && txtDate.Text != "")
            {

                MessageBox.Show("Invalid Branch Code Enter ......", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtBranchCode.Focus();
                return;
            }

        }

        private void slTextBox1_Leave(object sender, EventArgs e)
        {
            //if (slTextBox1.Text != "" && txtSeg.Text != "" && txtBranchCode.Text != "")
            //{
            //    param.Clear();
            //    param.Add("W_BRANCH", txtBranchCode.Text);
            //    param.Add("D_SEG", txtSeg.Text);
            //    param.Add("D_NO", slTextBox1.Text);
            //    rslt = cmnDm.GetData("CHRIS_SP_QUERY_dept_cont_MANAGER", "DETAIL_VERIFY", param);
            //    if (rslt.isSuccessful)
            //    {
            //        if (rslt.dstResult.Tables[0].Rows.Count == 0)
            //        {
            //            MessageBox.Show("No Record Found In This Branch/Segment/Department..");
            //            //MessageBox.Show("YOU HAVE ENTERED AN INVALID CODE");
            //            slTextBox1.Focus();
            //        }
            //    }
            //}
        }

        private void slTextBox1_Validating(object sender, CancelEventArgs e)
        {
            if (slTextBox1.Text != "" && txtSeg.Text != "" && txtBranchCode.Text != "")
            {

                param.Clear();
                param.Add("W_BRANCH", txtBranchCode.Text);
                param.Add("D_SEG", txtSeg.Text);
                param.Add("D_NO", slTextBox1.Text);

                rslt = cmnDm.GetData("CHRIS_SP_QUERY_dept_cont_MANAGER", "DETAIL_VERIFY", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("No Record Found In This Branch/Segment/Department..");
                        slTextBox1.Select();
                        slTextBox1.Focus();
                        e.Cancel = true;
                    }
                    else
                    {
                        slDataGridView1.Focus();
                    }
                }
            }
            else if (slTextBox1.Text.Equals(String.Empty) && (txtSeg.Text != "" || txtBranchCode.Text != ""))
            {
                MessageBox.Show("YOU HAVE ENTERED AN INVALID CODE");
                slTextBox1.Select();
                slTextBox1.Focus();
                e.Cancel = true;
            }
        }

        #endregion
    }
}