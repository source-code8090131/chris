namespace iCORE.CHRIS.PRESENTATIONOBJECTS.Query
{
    partial class CHRIS_Query_RankHistoryQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CHRIS_Query_RankHistoryQuery));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlRank = new iCORE.COMMON.SLCONTROLS.SLPanelTabular(this.components);
            this.slDataGridView1 = new iCORE.COMMON.SLCONTROLS.SLDataGridView(this.components);
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R_QUARTER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R_RANK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlPersonnel = new iCORE.COMMON.SLCONTROLS.SLPanelSimple(this.components);
            this.lbtnPersonel = new CrplControlLibrary.LookupButton(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBranch = new CrplControlLibrary.SLTextBox(this.components);
            this.txtLevel = new CrplControlLibrary.SLTextBox(this.components);
            this.txtName = new CrplControlLibrary.SLTextBox(this.components);
            this.txtAnnualPack = new CrplControlLibrary.SLTextBox(this.components);
            this.txtDesig = new CrplControlLibrary.SLTextBox(this.components);
            this.txtPrNo = new CrplControlLibrary.SLTextBox(this.components);
            this.RankQuery = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblNewCriteria = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnlBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.PnlRank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).BeginInit();
            this.PnlPersonnel.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOption
            // 
            this.txtOption.Location = new System.Drawing.Point(560, 0);
            this.txtOption.Size = new System.Drawing.Size(38, 20);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Size = new System.Drawing.Size(598, 22);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 460);
            this.panel1.Size = new System.Drawing.Size(598, 60);
            // 
            // PnlRank
            // 
            this.PnlRank.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PnlRank.ConcurrentPanels = null;
            this.PnlRank.Controls.Add(this.slDataGridView1);
            this.PnlRank.DataManager = null;
            this.PnlRank.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlRank.DependentPanels = null;
            this.PnlRank.DisableDependentLoad = false;
            this.PnlRank.EnableDelete = true;
            this.PnlRank.EnableInsert = true;
            this.PnlRank.EnableQuery = false;
            this.PnlRank.EnableUpdate = true;
            this.PnlRank.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.RANKCommand";
            this.PnlRank.Location = new System.Drawing.Point(113, 198);
            this.PnlRank.MasterPanel = this.PnlPersonnel;
            this.PnlRank.Name = "PnlRank";
            this.PnlRank.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlRank.Size = new System.Drawing.Size(410, 235);
            this.PnlRank.SPName = "CHRIS_SP_QUERY_RANK_RANK_MANAGER";
            this.PnlRank.TabIndex = 6;
            // 
            // slDataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.slDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.YEAR,
            this.R_QUARTER,
            this.R_RANK});
            this.slDataGridView1.ColumnToHide = null;
            this.slDataGridView1.ColumnWidth = null;
            this.slDataGridView1.CustomEnabled = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.slDataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.slDataGridView1.DisplayColumnWrapper = null;
            this.slDataGridView1.GridDefaultRow = 0;
            this.slDataGridView1.Location = new System.Drawing.Point(3, 1);
            this.slDataGridView1.Name = "slDataGridView1";
            this.slDataGridView1.ReadOnlyColumns = null;
            this.slDataGridView1.RequiredColumns = null;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.slDataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.slDataGridView1.Size = new System.Drawing.Size(400, 230);
            this.slDataGridView1.SkippingColumns = null;
            this.slDataGridView1.TabIndex = 1;
            // 
            // YEAR
            // 
            this.YEAR.DataPropertyName = "YEAR";
            this.YEAR.HeaderText = "Year";
            this.YEAR.MaxInputLength = 4;
            this.YEAR.Name = "YEAR";
            this.YEAR.Width = 140;
            // 
            // R_QUARTER
            // 
            this.R_QUARTER.DataPropertyName = "R_QUARTER";
            this.R_QUARTER.HeaderText = "Qtr";
            this.R_QUARTER.MaxInputLength = 3;
            this.R_QUARTER.Name = "R_QUARTER";
            this.R_QUARTER.Width = 140;
            // 
            // R_RANK
            // 
            this.R_RANK.DataPropertyName = "R_RANK";
            this.R_RANK.HeaderText = "Rank";
            this.R_RANK.MaxInputLength = 5;
            this.R_RANK.Name = "R_RANK";
            this.R_RANK.Width = 145;
            // 
            // PnlPersonnel
            // 
            this.PnlPersonnel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PnlPersonnel.ConcurrentPanels = null;
            this.PnlPersonnel.Controls.Add(this.lbtnPersonel);
            this.PnlPersonnel.Controls.Add(this.label6);
            this.PnlPersonnel.Controls.Add(this.label5);
            this.PnlPersonnel.Controls.Add(this.label4);
            this.PnlPersonnel.Controls.Add(this.label3);
            this.PnlPersonnel.Controls.Add(this.label2);
            this.PnlPersonnel.Controls.Add(this.label1);
            this.PnlPersonnel.Controls.Add(this.txtBranch);
            this.PnlPersonnel.Controls.Add(this.txtLevel);
            this.PnlPersonnel.Controls.Add(this.txtName);
            this.PnlPersonnel.Controls.Add(this.txtAnnualPack);
            this.PnlPersonnel.Controls.Add(this.txtDesig);
            this.PnlPersonnel.Controls.Add(this.txtPrNo);
            this.PnlPersonnel.DataManager = null;
            this.PnlPersonnel.DeleteRecordBehavior = iCORE.COMMON.SLCONTROLS.DeleteRecordBehavior.Isolated;
            this.PnlPersonnel.DependentPanels = null;
            this.PnlPersonnel.DisableDependentLoad = false;
            this.PnlPersonnel.EnableDelete = true;
            this.PnlPersonnel.EnableInsert = true;
            this.PnlPersonnel.EnableQuery = false;
            this.PnlPersonnel.EnableUpdate = true;
            this.PnlPersonnel.EntityName = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelCommand";
            this.PnlPersonnel.Location = new System.Drawing.Point(12, 94);
            this.PnlPersonnel.MasterPanel = null;
            this.PnlPersonnel.Name = "PnlPersonnel";
            this.PnlPersonnel.PanelBlockType = iCORE.COMMON.SLCONTROLS.BlockType.DataBlock;
            this.PnlPersonnel.Size = new System.Drawing.Size(578, 98);
            this.PnlPersonnel.SPName = "CHRIS_SP_QUERY_RANK_PERSONNEL_PERSONNELMANAGER";
            this.PnlPersonnel.TabIndex = 0;
            this.PnlPersonnel.TabStop = true;
            // 
            // lbtnPersonel
            // 
            this.lbtnPersonel.ActionLOVExists = "P_NO_LOV_EXIST";
            this.lbtnPersonel.ActionType = "P_NO_LOV";
            this.lbtnPersonel.ConditionalFields = "";
            this.lbtnPersonel.CustomEnabled = true;
            this.lbtnPersonel.DataFieldMapping = "";
            this.lbtnPersonel.DependentLovControls = "";
            this.lbtnPersonel.HiddenColumns = "PR_BRANCH|NAME|PR_DESIG|PR_LEVEL|PR_CATEGORY|PR_ANNUAL_PACK|PR_PROMOTION_DATE|BRA" +
                "NCH|PR_NEW_ANNUAL_PACK";
            this.lbtnPersonel.Image = ((System.Drawing.Image)(resources.GetObject("lbtnPersonel.Image")));
            this.lbtnPersonel.LoadDependentEntities = true;
            this.lbtnPersonel.Location = new System.Drawing.Point(185, 22);
            this.lbtnPersonel.LookUpTitle = null;
            this.lbtnPersonel.Name = "lbtnPersonel";
            this.lbtnPersonel.Size = new System.Drawing.Size(26, 21);
            this.lbtnPersonel.SkipValidationOnLeave = false;
            this.lbtnPersonel.SPName = "CHRIS_SP_QUERY_RANK_PERSONNEL_PERSONNEL_MANAGER";
            this.lbtnPersonel.TabIndex = 12;
            this.lbtnPersonel.TabStop = false;
            this.lbtnPersonel.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(377, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Branch";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(377, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Level";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(212, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Annual Package";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Desig";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Pr No";
            // 
            // txtBranch
            // 
            this.txtBranch.AllowSpace = true;
            this.txtBranch.AssociatedLookUpName = "";
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBranch.ContinuationTextBox = null;
            this.txtBranch.CustomEnabled = true;
            this.txtBranch.DataFieldMapping = "BRANCH";
            this.txtBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranch.GetRecordsOnUpDownKeys = false;
            this.txtBranch.IsDate = false;
            this.txtBranch.Location = new System.Drawing.Point(430, 66);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.NumberFormat = "###,###,##0.00";
            this.txtBranch.Postfix = "";
            this.txtBranch.Prefix = "";
            this.txtBranch.ReadOnly = true;
            this.txtBranch.Size = new System.Drawing.Size(132, 20);
            this.txtBranch.SkipValidation = false;
            this.txtBranch.TabIndex = 6;
            this.txtBranch.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtLevel
            // 
            this.txtLevel.AllowSpace = true;
            this.txtLevel.AssociatedLookUpName = "";
            this.txtLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLevel.ContinuationTextBox = null;
            this.txtLevel.CustomEnabled = true;
            this.txtLevel.DataFieldMapping = "PR_LEVEL";
            this.txtLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel.GetRecordsOnUpDownKeys = false;
            this.txtLevel.IsDate = false;
            this.txtLevel.Location = new System.Drawing.Point(430, 43);
            this.txtLevel.Name = "txtLevel";
            this.txtLevel.NumberFormat = "###,###,##0.00";
            this.txtLevel.Postfix = "";
            this.txtLevel.Prefix = "";
            this.txtLevel.ReadOnly = true;
            this.txtLevel.Size = new System.Drawing.Size(132, 20);
            this.txtLevel.SkipValidation = false;
            this.txtLevel.TabIndex = 4;
            this.txtLevel.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtName
            // 
            this.txtName.AllowSpace = true;
            this.txtName.AssociatedLookUpName = "";
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.ContinuationTextBox = null;
            this.txtName.CustomEnabled = true;
            this.txtName.DataFieldMapping = "NAME";
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.GetRecordsOnUpDownKeys = false;
            this.txtName.IsDate = false;
            this.txtName.Location = new System.Drawing.Point(253, 21);
            this.txtName.Name = "txtName";
            this.txtName.NumberFormat = "###,###,##0.00";
            this.txtName.Postfix = "";
            this.txtName.Prefix = "";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(309, 20);
            this.txtName.SkipValidation = false;
            this.txtName.TabIndex = 2;
            this.txtName.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtAnnualPack
            // 
            this.txtAnnualPack.AllowSpace = true;
            this.txtAnnualPack.AssociatedLookUpName = "";
            this.txtAnnualPack.BackColor = System.Drawing.SystemColors.Control;
            this.txtAnnualPack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAnnualPack.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAnnualPack.ContinuationTextBox = null;
            this.txtAnnualPack.CustomEnabled = true;
            this.txtAnnualPack.DataFieldMapping = "PR_NEW_ANNUAL_PACK";
            this.txtAnnualPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnualPack.GetRecordsOnUpDownKeys = false;
            this.txtAnnualPack.IsDate = false;
            this.txtAnnualPack.Location = new System.Drawing.Point(119, 68);
            this.txtAnnualPack.Name = "txtAnnualPack";
            this.txtAnnualPack.NumberFormat = "###,###,##0";
            this.txtAnnualPack.Postfix = "";
            this.txtAnnualPack.Prefix = "";
            this.txtAnnualPack.ReadOnly = true;
            this.txtAnnualPack.Size = new System.Drawing.Size(100, 20);
            this.txtAnnualPack.SkipValidation = false;
            this.txtAnnualPack.TabIndex = 5;
            this.txtAnnualPack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAnnualPack.TextType = CrplControlLibrary.TextType.Amount;
            // 
            // txtDesig
            // 
            this.txtDesig.AllowSpace = true;
            this.txtDesig.AssociatedLookUpName = "";
            this.txtDesig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDesig.ContinuationTextBox = null;
            this.txtDesig.CustomEnabled = true;
            this.txtDesig.DataFieldMapping = "PR_DESIG";
            this.txtDesig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesig.GetRecordsOnUpDownKeys = false;
            this.txtDesig.IsDate = false;
            this.txtDesig.Location = new System.Drawing.Point(119, 46);
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.NumberFormat = "###,###,##0.00";
            this.txtDesig.Postfix = "";
            this.txtDesig.Prefix = "";
            this.txtDesig.ReadOnly = true;
            this.txtDesig.Size = new System.Drawing.Size(153, 20);
            this.txtDesig.SkipValidation = false;
            this.txtDesig.TabIndex = 3;
            this.txtDesig.TextType = CrplControlLibrary.TextType.String;
            // 
            // txtPrNo
            // 
            this.txtPrNo.AllowSpace = true;
            this.txtPrNo.AssociatedLookUpName = "lbtnPersonel";
            this.txtPrNo.BackColor = System.Drawing.SystemColors.Control;
            this.txtPrNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrNo.ContinuationTextBox = null;
            this.txtPrNo.CustomEnabled = true;
            this.txtPrNo.DataFieldMapping = "PR_P_NO";
            this.txtPrNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrNo.GetRecordsOnUpDownKeys = false;
            this.txtPrNo.IsDate = false;
            this.txtPrNo.Location = new System.Drawing.Point(119, 24);
            this.txtPrNo.MaxLength = 6;
            this.txtPrNo.Name = "txtPrNo";
            this.txtPrNo.NumberFormat = "###,###,##0.00";
            this.txtPrNo.Postfix = "";
            this.txtPrNo.Prefix = "";
            this.txtPrNo.Size = new System.Drawing.Size(65, 20);
            this.txtPrNo.SkipValidation = false;
            this.txtPrNo.TabIndex = 0;
            this.txtPrNo.TextType = CrplControlLibrary.TextType.String;
            this.txtPrNo.Leave += new System.EventHandler(this.txtPrNo_Leave);
            this.txtPrNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrNo_KeyPress);
            // 
            // RankQuery
            // 
            this.RankQuery.AutoSize = true;
            this.RankQuery.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.RankQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RankQuery.Location = new System.Drawing.Point(247, 45);
            this.RankQuery.Name = "RankQuery";
            this.RankQuery.Size = new System.Drawing.Size(116, 26);
            this.RankQuery.TabIndex = 10;
            this.RankQuery.Text = "RankQuery";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Location = new System.Drawing.Point(332, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 70;
            this.lblUserName.Text = "User Name :";
            // 
            // lblNewCriteria
            // 
            this.lblNewCriteria.AutoSize = true;
            this.lblNewCriteria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewCriteria.Location = new System.Drawing.Point(297, 436);
            this.lblNewCriteria.Name = "lblNewCriteria";
            this.lblNewCriteria.Size = new System.Drawing.Size(147, 20);
            this.lblNewCriteria.TabIndex = 71;
            this.lblNewCriteria.Text = "New Criteria = F1";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.Location = new System.Drawing.Point(196, 436);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(80, 20);
            this.lblExit.TabIndex = 73;
            this.lblExit.Text = "Exit = F6";
            // 
            // CHRIS_Query_RankHistoryQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(598, 520);
            this.Controls.Add(this.lblNewCriteria);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.RankQuery);
            this.Controls.Add(this.PnlRank);
            this.Controls.Add(this.PnlPersonnel);
            this.CurrentPanelBlock = "PnlPersonnel";
            this.Name = "CHRIS_Query_RankHistoryQuery";
            this.ShowBottomBar = true;
            this.ShowF1Option = true;
            this.ShowF2Option = true;
            this.ShowF6Option = true;
            this.ShowOptionKeys = true;
            this.ShowOptionTextBox = true;
            this.ShowStatusBar = true;
            this.Text = "iCore CHRIS - RankHistoryQuery";
            this.AfterLOVSelection += new iCORE.Common.PRESENTATIONOBJECTS.Cmn.AfterLOVSelection(this.CHRIS_Query_RankHistoryQuery_AfterLOVSelection);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.PnlPersonnel, 0);
            this.Controls.SetChildIndex(this.PnlRank, 0);
            this.Controls.SetChildIndex(this.RankQuery, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblExit, 0);
            this.Controls.SetChildIndex(this.lblNewCriteria, 0);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.PnlRank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slDataGridView1)).EndInit();
            this.PnlPersonnel.ResumeLayout(false);
            this.PnlPersonnel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iCORE.COMMON.SLCONTROLS.SLPanelSimple PnlPersonnel;
        private iCORE.COMMON.SLCONTROLS.SLPanelTabular PnlRank;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CrplControlLibrary.SLTextBox txtBranch;
        private CrplControlLibrary.SLTextBox txtLevel;
        private CrplControlLibrary.SLTextBox txtName;
        private CrplControlLibrary.SLTextBox txtAnnualPack;
        private CrplControlLibrary.SLTextBox txtDesig;
        private CrplControlLibrary.SLTextBox txtPrNo;
        private iCORE.COMMON.SLCONTROLS.SLDataGridView slDataGridView1;
        private CrplControlLibrary.LookupButton lbtnPersonel;
        private System.Windows.Forms.Label RankQuery;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblNewCriteria;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn R_QUARTER;
        private System.Windows.Forms.DataGridViewTextBoxColumn R_RANK;
    }
}